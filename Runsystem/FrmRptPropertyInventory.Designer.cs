﻿namespace RunSystem
{
    partial class FrmRptPropertyInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.LblRegistrationDt = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.ChkPropertyCategory = new DevExpress.XtraEditors.CheckEdit();
            this.LblPropertyCategory = new System.Windows.Forms.Label();
            this.LuePropertyCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.LblPropertyInventoryCode = new System.Windows.Forms.Label();
            this.TxtPropertyInventoryCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkPropertyInventoryCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkSite = new DevExpress.XtraEditors.CheckEdit();
            this.LblSite = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPropertyCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPropertyInventoryCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkSite);
            this.panel2.Controls.Add(this.LblSite);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.LblPropertyInventoryCode);
            this.panel2.Controls.Add(this.TxtPropertyInventoryCode);
            this.panel2.Controls.Add(this.ChkPropertyInventoryCode);
            this.panel2.Controls.Add(this.ChkPropertyCategory);
            this.panel2.Controls.Add(this.LblPropertyCategory);
            this.panel2.Controls.Add(this.LuePropertyCategory);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LblRegistrationDt);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Size = new System.Drawing.Size(672, 124);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 349);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(223, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 18;
            this.label3.Text = "-";
            // 
            // LblRegistrationDt
            // 
            this.LblRegistrationDt.AutoSize = true;
            this.LblRegistrationDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRegistrationDt.ForeColor = System.Drawing.Color.Black;
            this.LblRegistrationDt.Location = new System.Drawing.Point(15, 19);
            this.LblRegistrationDt.Name = "LblRegistrationDt";
            this.LblRegistrationDt.Size = new System.Drawing.Size(101, 14);
            this.LblRegistrationDt.TabIndex = 19;
            this.LblRegistrationDt.Text = "Registration Date";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(238, 16);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt2.TabIndex = 21;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(120, 16);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt1.TabIndex = 20;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // ChkPropertyCategory
            // 
            this.ChkPropertyCategory.Location = new System.Drawing.Point(352, 38);
            this.ChkPropertyCategory.Name = "ChkPropertyCategory";
            this.ChkPropertyCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPropertyCategory.Properties.Appearance.Options.UseFont = true;
            this.ChkPropertyCategory.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPropertyCategory.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPropertyCategory.Properties.Caption = " ";
            this.ChkPropertyCategory.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPropertyCategory.Size = new System.Drawing.Size(20, 22);
            this.ChkPropertyCategory.TabIndex = 24;
            this.ChkPropertyCategory.ToolTip = "Remove filter";
            this.ChkPropertyCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPropertyCategory.ToolTipTitle = "Run System";
            this.ChkPropertyCategory.CheckedChanged += new System.EventHandler(this.ChkPropertyCategoryCode_CheckedChanged);
            // 
            // LblPropertyCategory
            // 
            this.LblPropertyCategory.AutoSize = true;
            this.LblPropertyCategory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyCategory.ForeColor = System.Drawing.Color.Black;
            this.LblPropertyCategory.Location = new System.Drawing.Point(9, 41);
            this.LblPropertyCategory.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPropertyCategory.Name = "LblPropertyCategory";
            this.LblPropertyCategory.Size = new System.Drawing.Size(107, 14);
            this.LblPropertyCategory.TabIndex = 22;
            this.LblPropertyCategory.Text = "Property Category";
            this.LblPropertyCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePropertyCategory
            // 
            this.LuePropertyCategory.EnterMoveNextControl = true;
            this.LuePropertyCategory.Location = new System.Drawing.Point(120, 39);
            this.LuePropertyCategory.Margin = new System.Windows.Forms.Padding(5);
            this.LuePropertyCategory.Name = "LuePropertyCategory";
            this.LuePropertyCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.Appearance.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePropertyCategory.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropertyCategory.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePropertyCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePropertyCategory.Properties.DropDownRows = 30;
            this.LuePropertyCategory.Properties.NullText = "[Empty]";
            this.LuePropertyCategory.Properties.PopupWidth = 250;
            this.LuePropertyCategory.Size = new System.Drawing.Size(229, 20);
            this.LuePropertyCategory.TabIndex = 23;
            this.LuePropertyCategory.ToolTip = "F4 : Show/hide list";
            this.LuePropertyCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePropertyCategory.EditValueChanged += new System.EventHandler(this.LuePropertyCategory_EditValueChanged);
            // 
            // LblPropertyInventoryCode
            // 
            this.LblPropertyInventoryCode.AutoSize = true;
            this.LblPropertyInventoryCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPropertyInventoryCode.Location = new System.Drawing.Point(5, 65);
            this.LblPropertyInventoryCode.Name = "LblPropertyInventoryCode";
            this.LblPropertyInventoryCode.Size = new System.Drawing.Size(111, 14);
            this.LblPropertyInventoryCode.TabIndex = 25;
            this.LblPropertyInventoryCode.Text = "Property Inventory";
            // 
            // TxtPropertyInventoryCode
            // 
            this.TxtPropertyInventoryCode.EnterMoveNextControl = true;
            this.TxtPropertyInventoryCode.Location = new System.Drawing.Point(120, 62);
            this.TxtPropertyInventoryCode.Name = "TxtPropertyInventoryCode";
            this.TxtPropertyInventoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPropertyInventoryCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPropertyInventoryCode.Properties.MaxLength = 250;
            this.TxtPropertyInventoryCode.Size = new System.Drawing.Size(229, 20);
            this.TxtPropertyInventoryCode.TabIndex = 26;
            this.TxtPropertyInventoryCode.Validated += new System.EventHandler(this.TxtPropertyInventoryCode_Validated);
            // 
            // ChkPropertyInventoryCode
            // 
            this.ChkPropertyInventoryCode.Location = new System.Drawing.Point(352, 62);
            this.ChkPropertyInventoryCode.Name = "ChkPropertyInventoryCode";
            this.ChkPropertyInventoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPropertyInventoryCode.Properties.Appearance.Options.UseFont = true;
            this.ChkPropertyInventoryCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPropertyInventoryCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPropertyInventoryCode.Properties.Caption = " ";
            this.ChkPropertyInventoryCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPropertyInventoryCode.Size = new System.Drawing.Size(20, 22);
            this.ChkPropertyInventoryCode.TabIndex = 27;
            this.ChkPropertyInventoryCode.ToolTip = "Remove filter";
            this.ChkPropertyInventoryCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPropertyInventoryCode.ToolTipTitle = "Run System";
            this.ChkPropertyInventoryCode.CheckedChanged += new System.EventHandler(this.ChkPropertyInventoryCode_CheckedChanged);
            // 
            // ChkSite
            // 
            this.ChkSite.Location = new System.Drawing.Point(352, 84);
            this.ChkSite.Name = "ChkSite";
            this.ChkSite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSite.Properties.Appearance.Options.UseFont = true;
            this.ChkSite.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSite.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSite.Properties.Caption = " ";
            this.ChkSite.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSite.Size = new System.Drawing.Size(20, 22);
            this.ChkSite.TabIndex = 30;
            this.ChkSite.ToolTip = "Remove filter";
            this.ChkSite.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSite.ToolTipTitle = "Run System";
            this.ChkSite.Click += new System.EventHandler(this.ChkSite_CheckedChanged);
            // 
            // LblSite
            // 
            this.LblSite.AutoSize = true;
            this.LblSite.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSite.ForeColor = System.Drawing.Color.Black;
            this.LblSite.Location = new System.Drawing.Point(89, 87);
            this.LblSite.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSite.Name = "LblSite";
            this.LblSite.Size = new System.Drawing.Size(28, 14);
            this.LblSite.TabIndex = 28;
            this.LblSite.Text = "Site";
            this.LblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(121, 85);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 250;
            this.LueSiteCode.Size = new System.Drawing.Size(228, 20);
            this.LueSiteCode.TabIndex = 29;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSite_EditValueChanged);
            // 
            // FrmRptPropertyInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmRptPropertyInventory";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPropertyCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropertyCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPropertyInventoryCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPropertyInventoryCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LblRegistrationDt;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private DevExpress.XtraEditors.CheckEdit ChkPropertyCategory;
        private System.Windows.Forms.Label LblPropertyCategory;
        private DevExpress.XtraEditors.LookUpEdit LuePropertyCategory;
        private System.Windows.Forms.Label LblPropertyInventoryCode;
        private DevExpress.XtraEditors.TextEdit TxtPropertyInventoryCode;
        private DevExpress.XtraEditors.CheckEdit ChkPropertyInventoryCode;
        private DevExpress.XtraEditors.CheckEdit ChkSite;
        private System.Windows.Forms.Label LblSite;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
    }
}