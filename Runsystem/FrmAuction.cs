﻿#region Update
/*
    07/04/2020 [DITA/YK] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAuction : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = string.Empty;
        internal bool mIsFilterBySite = false;

        iGCell fCell;
        bool fAccept;
        internal FrmAuctionFind FrmFind;
        #endregion

        #region Constructor

        public FrmAuction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Auction";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueYr(LueYr, "");
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueSiteCode(ref LueSiteCode, string.Empty);
                Sl.SetLueOption(ref LueAuctionType, "AuctionType");
                LueSiteCode.Visible = false;
                LueAuctionType.Visible = false;
                SetFormControl(mState.View);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Site Code",
                        "Site",
                        "Auction Type Code",
                        "Auction Type",
                        "Amount",

                        //6
                        "Remark",

                       

                    },
                     new int[] 
                    {
                        //0
                        0,
                        
                        //1-5
                        0, 150, 0, 100, 100, 

                        //6
                        200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueYr, MeeCancelReason, DteDocDt, MeeRemark, 
                    }, true);

                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6});
                    ChkCancelInd.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueYr, DteDocDt, MeeRemark, 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 4, 5, 6 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { 
                      MeeCancelReason
                    }, false);
                    ChkCancelInd.Enabled = true;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueYr, MeeCancelReason, DteDocDt, MeeRemark, TxtDocNo
            });
            ChkCancelInd.Checked = false;
            ClearGrd();

        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAuctionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            InsertData();
        }

        private void InsertData()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 2, 4, 5 }, e.ColIndex))
                    {
                       if (e.ColIndex == 2) LueRequestEdit(Grd1, LueSiteCode, ref fCell, ref fAccept, e);
                       if (e.ColIndex == 4) LueRequestEdit(Grd1, LueAuctionType, ref fCell, ref fAccept, e);
                       Sm.GrdRequestEdit(Grd1, e.RowIndex);
                       Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
                    }

                }

            }
          
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {

            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5 }, e);
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Auction", "TblAuctionHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAuctionHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveAuctionDtl(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo);

        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsYrDuplicated() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsYrDuplicated()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblAuctionHdr ");
            SQL.AppendLine("Where Yr = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), LueYr.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Duplicated.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Site is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Auction Type is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Amount should be bigger than 0.00.")
                    ) return true;
            }

            for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
            {
                for (int r2 = (r1 + 1); r2 < Grd1.Rows.Count; r2++)
                {
                    if ((Sm.GetGrdStr(Grd1, r1, 2) == Sm.GetGrdStr(Grd1, r2, 2))
                        &&
                        (Sm.GetGrdStr(Grd1, r1, 4) == Sm.GetGrdStr(Grd1, r2, 4))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate Site: " + Sm.GetGrdStr(Grd1, r2, 2) + " with Auction Type: "+ Sm.GetGrdStr(Grd1, r2, 4) + " found");
                        Sm.FocusGrd(Grd1, r2, 2);
                        return true;
                    }
                }
            }

            return false;
        }


        private MySqlCommand SaveAuctionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAuctionHdr(DocNo, CancelInd, CancelReason, DocDt, Yr, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DocNo, 'N', @CancelReason, @DocDt, @Yr, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAuctionDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAuctionDtl(DocNo, DNo, SiteCode, AuctionType, Amt, Remark, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @SiteCode, @AuctionType, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@AuctionType", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAuctionHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Auction#", false) ||
                IsDocumentAlreadyCancel();
        }

        private bool IsDocumentAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblAuctionHdr Where DocNo=@DocNo And CancelInd='Y'"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel");
                return true;
            }
            return false;
        }

        private MySqlCommand EditAuctionHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAuctionHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason,");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowAuctionHdr(DocNo);
                ShowAuctionDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAuctionHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DocDt, CancelReason, Cancelind, Yr, Remark ");
            SQL.AppendLine("From TblAuctionHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "Yr", "Remark", 
                        
 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);

                    }, true
                );
        }


        private void ShowAuctionDtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DNo, A.SiteCode, B.SiteName, A.AuctionType, C.OptDesc AuctionTypeDesc, A.Amt, A.Remark ");
            SQL.AppendLine("FROM TblAuctionDtl A ");
            SQL.AppendLine("INNER JOIN TblSite B ON A.SiteCode = B.SiteCode ");
            SQL.AppendLine("INNER JOIN TblOption C ON A.AuctionType = C.OptCode And C.OptCat = 'AuctionType' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "SiteCode", "SiteName", "AuctionType", "AuctionTypeDesc", "Amt", 
                    
                    //6
                    "Remark"
 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 6);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");

        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event


        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
            }
        }

        private void LueSiteCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueSiteCode_Leave(object sender, EventArgs e)
        {
            if (LueSiteCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueSiteCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value =
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueSiteCode);
                    Grd1.Cells[fCell.RowIndex, 2].Value = LueSiteCode.GetColumnValue("Col2");
                }
                LueSiteCode.Visible = false;
            }
        }

        private void LueAuctionType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueAuctionType, new Sm.RefreshLue2(Sl.SetLueOption), "AuctionType");
            }
        }

        private void LueAuctionType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueAuctionType_Leave(object sender, EventArgs e)
        {
            if (LueAuctionType.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueAuctionType).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 3].Value =
                    Grd1.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueAuctionType);
                    Grd1.Cells[fCell.RowIndex, 4].Value = LueAuctionType.GetColumnValue("Col2");
                }
                LueAuctionType.Visible = false;
            }
        }

        #endregion


        #endregion
    }
}
