﻿#region Update
/*
    19/05/2021 [RDH/KSM] new apps
    06/09/2021 [TKG/KSM] tambah filter item category
    08/09/2021 [SET/KSM] Penyesuaian Print Out pada menu Total Sales Order Amount by Customer dan excelkan
    10/11/2021 [MYA/KSM] Customer By IDR : menambahkan kolom yang include Harga After Tax 
    12/11/2021 [SET/KSM] BY IDR & QTY : menambahkan kolom yang include Harga After Tax
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesContractAmt : RunSystem.FrmBase16
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSalesContractAmt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 0;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    "No",

                    "Customer",
                    "Total Price",
                    "Total Price After Tax"
                },
                new int[] 
                {
                    40,

                    300, 200, 200
                }
            );

            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3 },0);
            #endregion

            #region Grid 2
            Grd2.Cols.Count = 4;
            Grd2.FrozenArea.ColCount = 0;

            Sm.GrdHdrWithColWidth(
                Grd2, new string[] 
                {
                    "No",

                    "Customer",
                    "Quantity",
                    "Total Price"
                },
                new int[] 
                {
                    40,

                    300, 150, 200
                }
            );

            Sm.SetGrdProperty(Grd2, false);
            Sm.GrdFormatDec(Grd2, new int[] { 2, 3 }, 0);

            #endregion

            #region Grid 3
            Grd3.Cols.Count = 8;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Item",
                    "Category",
                    "Qty",
                    "Price",
                    "Total",

                    //6-7
                    "Price Atfer Tax",
                    "Total After Tax"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    200, 200, 120, 130, 130,

                    //6-7
                    120, 120
                }
            );

            Sm.SetGrdProperty(Grd3, false);
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4, 5, 6, 7 }, 0);
            #endregion
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            Sm.ClearGrd(Grd2, false);
            Sm.ClearGrd(Grd3, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                var l = new List<Grd01>();
                var l2 = new List<Grd02>();
                var l3 = new List<Grd03>();

                PrepData1(ref l);
                PrepData2(ref l2);
                PrepData3(ref l3);

                if (l.Count > 0) SetData1(ref l);
                if (l2.Count > 0) SetData2(ref l2);
                if (l3.Count > 0) SetData3(ref l3);

                l.Clear(); l2.Clear(); l3.Clear();
            }
            catch (Exception ex)
            {
                Sm.ShowErrorMsg(ex);
            }
            finally
            {
                tabControl1.SelectedTab = tabPage1;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        virtual protected void PrintData(object sender, EventArgs e)
        {
            try
            {
                if (Grd1.Rows.Count == 0 && Grd2.Rows.Count == 0 && Grd3.Rows.Count == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }
                iGSubtotalManager.ForeColor = Color.Black;

                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ParPrint()
        {
            string[] TableName = { "Grd01", "Grd02", "Grd03" };

            string DocTitle = Sm.GetParameter("DocTitle");
            var l1 = new List<Grd01>();
            var l2 = new List<Grd02>();
            var l3 = new List<Grd03>();

            //string Doctitle = Sm.GetParameter("DocTitle");

            List<IList> myLists = new List<IList>();

            #region Grd1

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            string Filter1 = " ", Filter2 = " ";
            Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "E.ItName" });
            Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCtCode), "E.ItCtCode", true);

            SQL.AppendLine("Select CtName, Amt ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select C.CtName, Sum(D.Qty*D.UPrice) As Amt ");
            SQL.AppendLine("    From TblSalesContract A  ");
            SQL.AppendLine("    Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo  ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status In ('O', 'A') ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("    Inner Join TblCustomer C ON B.CtCode=C.CtCode  ");
            SQL.AppendLine("    Inner Join TblSalesMemoDtl D ON B.DocNo=D.Docno ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By C.CtName ");
            SQL.AppendLine(" ) T Where Amt<>0.00 ");
            SQL.AppendLine("Order By Amt Desc; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                //Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CtName",

                    //1
                    "Amt" 
                });

                int nomor = 0;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        nomor = nomor + 1;
                        l1.Add(new Grd01()
                        {
                            No = nomor,
                            CtName = Sm.DrStr(dr, c[0]),
                            TotalPrice = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l1);

            #endregion

            #region Grd2

            var SQL02 = new StringBuilder();
            
            string Filter021 = " ", Filter022 = " ";
            Sm.FilterStr(ref Filter022, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });
            Sm.FilterStr(ref Filter021, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
            Sm.FilterStr(ref Filter022, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);

            SQL02.AppendLine("Select CtName, Amt, Qty ");
            SQL02.AppendLine("From ( ");
            SQL02.AppendLine("    Select E.CtName, Sum(C.Qty*C.UPrice) As Amt, Sum(C.Qty) As Qty ");
            SQL02.AppendLine("    From TblSalesContract A  ");
            SQL02.AppendLine("    Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo ");
            SQL02.AppendLine("        And B.CancelInd='N' ");
            SQL02.AppendLine("        And B.Status In ('O', 'A') ");
            SQL02.AppendLine(Filter021);
            SQL02.AppendLine("    Inner Join TblSalesMemoDtl C ON B.DocNo=C.DocNo ");
            SQL02.AppendLine("    Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL02.AppendLine(Filter022);
            SQL02.AppendLine("    Inner Join TblCustomer E ON B.CtCode=E.CtCode  ");
            SQL02.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL02.AppendLine("    And A.CancelInd='N' ");
            SQL02.AppendLine("    And A.Status In ('O', 'A') ");
            SQL02.AppendLine("    Group By E.CtName ");
            SQL02.AppendLine(") T Where Amt<>0.00 Or Qty<>0.00 Order By Qty Desc; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL02.ToString();
                //Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                //Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CtName",

                    //1-2
                    "Qty",
                    "Amt"
                    
                });

                int nomor = 0;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        nomor = nomor + 1;
                        l2.Add(new Grd02()
                        {
                            No = nomor,
                            CtName = Sm.DrStr(dr, c[0]),
                            Qty = Sm.DrDec(dr, c[1]),
                            TotalPrice = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l2);

            #endregion

            #region Grd3

            var SQL03 = new StringBuilder();

            string Filter031 = " ", Filter032 = " ";
            Sm.FilterStr(ref Filter032, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });
            Sm.FilterStr(ref Filter031, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
            Sm.FilterStr(ref Filter032, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);

            SQL03.AppendLine("Select ItName, ItCtName, UPrice, Qty ");
            SQL03.AppendLine("From ( ");
            SQL03.AppendLine("    Select D.ItName, E.ItCtName, C.UPrice, Sum(C.Qty) As Qty ");
            SQL03.AppendLine("    From TblSalesContract A  ");
            SQL03.AppendLine("    Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo ");
            SQL03.AppendLine("        And B.CancelInd='N' ");
            SQL03.AppendLine("        And B.Status In ('O', 'A') ");
            SQL03.AppendLine(Filter031);
            SQL03.AppendLine("    Inner Join TblSalesMemoDtl C ON B.DocNo=C.DocNo ");
            SQL03.AppendLine("    Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL03.AppendLine(Filter032);
            SQL03.AppendLine("    Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL03.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL03.AppendLine("    And A.CancelInd='N' ");
            SQL03.AppendLine("    And A.Status In ('O', 'A') ");
            SQL03.AppendLine("    Group By D.ItName, E.ItCtName, C.UPrice ");
            SQL03.AppendLine(") T Where Qty<>0.00 Order By Qty, ItName Desc; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL03.ToString();
                //Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                //Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "ItName",

                    //1-3
                    "ItCtName",
                    "Qty",
                    "UPrice"
                    
                });

                int nomor = 0;
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        nomor = nomor + 1;
                        l3.Add(new Grd03()
                        {
                            No = nomor,
                            ItName = Sm.DrStr(dr, c[0]),
                            ItCtName = Sm.DrStr(dr, c[1]),
                            Qty = Sm.DrDec(dr, c[2]),
                            UPrice = Sm.DrDec(dr, c[3]),
                            Total = Sm.DrDec(dr, c[3])*Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l3);

            #endregion

            Sm.PrintReport(string.Concat("RptSalesContractAmt" + DocTitle), myLists, TableName, false);
        }

        private void PrintToExcel(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
            Sm.ExportToExcel(Grd2);
            Sm.ExportToExcel(Grd3);
        }

        private void PrepData1(ref List<Grd01> l)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT CtCode, CtName, SUM(UPrice) AS TotalPrice FROM ");
            //SQL.AppendLine("(");
            //SQL.AppendLine("     SELECT C.CtCode,C.CtName, (F.Qty * F.UPrice) Uprice, A.DocDt, G.ItCode, G.ItName");
            //SQL.AppendLine("     From TblSalesContract A  ");
            //SQL.AppendLine("     Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo  ");
            //SQL.AppendLine("     Left Join TblCustomer C ON B.CtCode=C.CtCode  ");
            //SQL.AppendLine("     Left Join TblSite D ON B.SiteCode=D.SiteCode  ");
            //SQL.AppendLine("     Left Join TblCustomerCategory E ON C.CtCtCode=E.CtCtCode  ");
            //SQL.AppendLine("     INNER Join TblSalesMemoDtl F ON F.DocNo=B.Docno ");
            //SQL.AppendLine("     left Join TblItem G On F.ItCode=G.ItCode ");
            //SQL.AppendLine("     left JOIN tblpaymentterm H ON H.PtCode=A.PtCode  ");
            //SQL.AppendLine(" )T1 ");
            //SQL.AppendLine("Where 1=1 ");
            
            //mSQL = SQL.ToString();

            string Filter1 = " ", Filter2 = " ";
            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text,new string[]{"E.ItCode", "E.ItName"});
            Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCtCode), "E.ItCtCode", true);


            SQL.AppendLine("Select CtName, Amt, AmtAfterTax ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select C.CtName, Sum(D.Qty*D.UPrice) As Amt, Sum(D.Qty*D.UPriceAfterTax) As AmtAfterTax ");
            SQL.AppendLine("    From TblSalesContract A  ");
            SQL.AppendLine("    Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo  ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status In ('O', 'A') ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("    Inner Join TblCustomer C ON B.CtCode=C.CtCode  ");
            SQL.AppendLine("    Inner Join TblSalesMemoDtl D ON B.DocNo=D.Docno ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By C.CtName ");
            SQL.AppendLine(" ) T Where Amt<>0.00 ");
            SQL.AppendLine("Order By Amt Desc; ");
            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "CtName", "Amt", "AmtAfterTax" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Grd01()
                        {
                            CtName = Sm.DrStr(dr, c[0]),
                            TotalPrice = Sm.DrDec(dr,c[1]),
                            TotalPriceAfterTax = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepData2(ref List<Grd02> l2)
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT CtCode, CtName, SUM(UPrice) AS TotalPrice, SUM(Qty) Qty FROM ");
            //SQL.AppendLine("(");
            //SQL.AppendLine("     SELECT C.CtCode,C.CtName,F.Qty, (F.Qty * F.UPrice) Uprice, A.DocDt, G.ItCode, G.ItName");
            //SQL.AppendLine("     From TblSalesContract A  ");
            //SQL.AppendLine("     Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo  ");
            //SQL.AppendLine("     Left Join TblCustomer C ON B.CtCode=C.CtCode  ");
            //SQL.AppendLine("     Left Join TblSite D ON B.SiteCode=D.SiteCode  ");
            //SQL.AppendLine("     Left Join TblCustomerCategory E ON C.CtCtCode=E.CtCtCode  ");
            //SQL.AppendLine("     INNER Join TblSalesMemoDtl F ON F.DocNo=B.Docno ");
            //SQL.AppendLine("     left Join TblItem G On F.ItCode=G.ItCode ");
            //SQL.AppendLine("     left JOIN tblpaymentterm H ON H.PtCode=A.PtCode  ");
            //SQL.AppendLine(" )T1 ");
            //SQL.AppendLine("Where 1=1 ");

            //mSQL = SQL.ToString();

            string Filter1 = " ", Filter2 = " ";
            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });
            Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);

            SQL.AppendLine("Select CtName, Amt, Qty ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select E.CtName, Sum(C.Qty*C.UPrice) As Amt, Sum(C.Qty) As Qty ");
            SQL.AppendLine("    From TblSalesContract A  ");
            SQL.AppendLine("    Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status In ('O', 'A') ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("    Inner Join TblSalesMemoDtl C ON B.DocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Inner Join TblCustomer E ON B.CtCode=E.CtCode  ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By E.CtName ");
            SQL.AppendLine(") T Where Amt<>0.00 Or Qty<>0.00 Order By Qty Desc; ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "CtName", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Grd02()
                        {
                            CtName = Sm.DrStr(dr, c[0]),
                            Qty = Sm.DrDec(dr,c[1]),
                            TotalPrice = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepData3(ref List<Grd03> l3)
        {
            var SQL = new StringBuilder();
            string Filter1 = " ", Filter2 = " ";
            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });
            Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCtCode), "D.ItCtCode", true);

            SQL.AppendLine("Select ItName, ItCtName, UPrice, Qty, UPriceAfterTax ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select D.ItName, E.ItCtName, C.UPrice, Sum(C.Qty) As Qty, C.UPriceAfterTax ");
            SQL.AppendLine("    From TblSalesContract A  ");
            SQL.AppendLine("    Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status In ('O', 'A') ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("    Inner Join TblSalesMemoDtl C ON B.DocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By D.ItName, E.ItCtName, C.UPrice, C.UPriceAfterTax ");
            SQL.AppendLine(") T Where Qty<>0.00 Order By Qty, ItName Desc; ");

            //SQL.AppendLine("SELECT ItCode, ItName, SUM(Totalprice) AS TotalPrice, SUM(Qty) Qty, UPrice FROM ");
            //SQL.AppendLine("(");
            //SQL.AppendLine("     Select G.ItCode, G.ItName, H.ItCtName, F.UPrice, Sum(F.Qty) Qty, Sum(F.Qty*F.UPrice) TotalPrice, ");
            //SQL.AppendLine("     From TblSalesContract A  ");
            //SQL.AppendLine("     Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo ");
            //SQL.AppendLine(Filter1);
            //SQL.AppendLine("     Left Join TblCustomer C ON B.CtCode=C.CtCode  ");
            //SQL.AppendLine("     Left Join TblSite D ON B.SiteCode=D.SiteCode  ");
            //SQL.AppendLine("     Left Join TblCustomerCategory E ON C.CtCtCode=E.CtCtCode  ");
            //SQL.AppendLine("     INNER Join TblSalesMemoDtl F ON F.DocNo=B.Docno ");
            //SQL.AppendLine("     left Join TblItem G On F.ItCode=G.ItCode ");
            //SQL.AppendLine(Filter2);
            //SQL.AppendLine("     Left join TblItemCategory H On G.ItCtCode=H.ItCtCode ");
            //SQL.AppendLine("     Where A.DocDt Between @DocDt1 AND @DocDt2 ");
            //SQL.AppendLine(" )T1 ");
            //SQL.AppendLine("Where 1=1 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "ItName",

                    //1-4
                    "ItCtName",
                    "Qty",
                    "UPrice",
                    "UPriceAfterTax"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new Grd03()
                        {
                            ItName = Sm.DrStr(dr, c[0]),
                            ItCtName = Sm.DrStr(dr, c[1]),
                            Qty = Sm.DrDec(dr, c[2]),
                            UPrice = Sm.DrDec(dr,c[3]),
                            Total = Sm.DrDec(dr, c[2])*Sm.DrDec(dr, c[3]),
                            UPriceAfterTax = Sm.DrDec(dr, c[4]),
                            TotalAfterTax = Sm.DrDec(dr, c[2]) * Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetData1(ref List<Grd01> l)
        {
            int Row = 0;
            foreach(var x in l)
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.CtName;
                Grd1.Cells[Row, 2].Value = x.TotalPrice;
                Grd1.Cells[Row, 3].Value = x.TotalPriceAfterTax;

                Row += 1;
            }
        }

        private void SetData2(ref List<Grd02> l2)
        {
            int Row = 0;
            foreach (var y in l2)
            {
                Grd2.Rows.Add();
                Grd2.Cells[Row, 0].Value = Row + 1;
                Grd2.Cells[Row, 1].Value = y.CtName;
                Grd2.Cells[Row, 2].Value = y.Qty;
                Grd2.Cells[Row, 3].Value = y.TotalPrice;

                Row++;
            }
        }

        private void SetData3(ref List<Grd03> l3)
        {
            int Row = 0;
            foreach (var z in l3)
            {
                Grd3.Rows.Add();
                Grd3.Cells[Row, 0].Value = Row + 1;
                Grd3.Cells[Row, 1].Value = z.ItName;
                Grd3.Cells[Row, 2].Value = z.ItCtName;
                Grd3.Cells[Row, 3].Value = z.Qty;
                Grd3.Cells[Row, 4].Value = z.UPrice;
                Grd3.Cells[Row, 5].Value = z.Total;
                Grd3.Cells[Row, 6].Value = z.UPriceAfterTax;
                Grd3.Cells[Row, 7].Value = z.TotalAfterTax;
                Row++;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeActive), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void PrintData()
        {

        }

        private void PrintToExcel()
        {

        }
      
       
        #endregion

        #endregion

        #region Class

        private class Grd01
        {
            public int No { get; set; }
            public string CtName { get; set; }
            public decimal TotalPrice { get; set; }

            public decimal TotalPriceAfterTax { get; set; }
        }

        private class Grd02
        {
            public int No { get; set; }
            public string CtName { get; set; }
            public decimal Qty { get; set; }
            public decimal TotalPrice { get; set; }
        }

        private class Grd03
        {
            public int No { get; set; }
            public string ItName { get; set; }
            public string ItCtName { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal Total { get; set; }
            public decimal UPriceAfterTax { get; set; }
            public decimal TotalAfterTax { get; set; }
        }

        #endregion 
    }
}
