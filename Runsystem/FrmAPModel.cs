﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAPModel : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmAPModelFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmAPModel(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtAPModelCode, TxtAPModelName, ChkActInd, LueBrCode }, true);
                    TxtAPModelCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAPModelCode, TxtAPModelName, LueBrCode }, false);
                    TxtAPModelCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAPModelName, ChkActInd, LueBrCode }, false);
                    TxtAPModelName.Focus();
                    break;
                default: break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtAPModelCode, TxtAPModelName, LueBrCode });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAPModelFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                Sl.SetLueBrCode(ref LueBrCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAPModelCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAPModelCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAPModel Where APModelCode=@APModelCode" };
                Sm.CmParam<String>(ref cm, "@APModelCode", TxtAPModelCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblAPModel(APModelCode, APModelName, BrCode, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@APModelCode, @APModelName, @BrCode, @ActInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update APModelName=@APModelName, BrCode=@BrCode, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@APModelCode", TxtAPModelCode.Text);
                Sm.CmParam<String>(ref cm, "@APModelName", TxtAPModelName.Text);
                Sm.CmParam<String>(ref cm, "@BrCode", Sm.GetLue(LueBrCode));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtAPModelCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string APModelCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@APModelCode", APModelCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select APModelCode, APModelName, BrCode, ActInd From TblAPModel Where APModelCode=@APModelCode;",
                        new string[]{ "APModelCode", "APModelName", "BrCode", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtAPModelCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtAPModelName.EditValue = Sm.DrStr(dr, c[1]);
                            Sl.SetLueBrCode(ref LueBrCode, Sm.DrStr(dr, c[2]));
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAPModelCode, "AP model code", false) ||
                Sm.IsTxtEmpty(TxtAPModelName, "AP model name", false) ||
                IsCodeAlreadyExisted();
        }

        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtAPModelCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select APModelCode From TblAPModel Where APModelCode=@Param;",
                    TxtAPModelCode.Text,
                    "AP model code ( " + TxtAPModelCode.Text + " ) already existed."
                    );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAPModelCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAPModelCode);
        }

        private void TxtAPModelName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAPModelName);
        }

        private void LueBrCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBrCode, new Sm.RefreshLue2(Sl.SetLueBrCode), string.Empty);
        }

        #endregion

        #endregion
    }
}
