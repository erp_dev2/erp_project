﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

#endregion

namespace RunSystem
{
    public partial class FrmBase12 : Form
    {
        #region Constructor

        public FrmBase12()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Form Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {

        }

        #endregion

        #region Button Method

        virtual protected void BtnProcessClick(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void FrmBase12_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #region Button Events

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            BtnProcessClick(sender, e);
        }

        #endregion

        #endregion

    }
}
