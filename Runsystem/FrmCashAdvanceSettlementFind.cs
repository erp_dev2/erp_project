﻿#region Update
/*
    08/10/2019 [TKG/YK] New application
    14/02/2020 [TKG/YK] Tambah voucher type
    13/05/2020 [HAR/IMS] tambah informasi document status
    17/03/2021 [WED/PHT] tambah informasi Completed berdasarkan parameter IsCASUseCompletedInd
    24/11/2021 [ARI/AMKA] Filter departemen mengacu pada group dengna parameter IsFilterByDept
    14/04/2023 [SET/PHT] Hide kolom Document Status berdasar Parameter IsCASUseDraft
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCashAdvanceSettlementFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmCashAdvanceSettlement mFrmParent;
        private string mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmCashAdvanceSettlementFind(FrmCashAdvanceSettlement FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, E.OptDesc As DocStatus, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' Else 'Unknown' End As StatusDesc, ");
            SQL.AppendLine("B.UserName, C.DeptName, A.CurCode, A.Amt1, A.Amt2, ");
            SQL.AppendLine("D.VoucherType, A.Remark, ");
            if (mFrmParent.mIsCASUseCompletedInd)
                SQL.AppendLine("A.CompletedInd, ");
            else
                SQL.AppendLine("'N' As CompletedInd, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr A ");
            SQL.AppendLine("Left Join TblUser B On A.PIC=B.UserCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T4.OptDesc Separator ', ') As VoucherType ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr T1 ");
            SQL.AppendLine("    Left Join TblCashAdvanceSettlementDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Left Join TblVoucherHdr T3 On T2.VoucherDocNo=T3.DocNo ");
            SQL.AppendLine("    Left Join TblOption T4 On T3.DocType=T4.OptCode And T4.OptCat='VoucherDocType' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblOption E On A.DocStatus = E.OptCode And E.Optcat = 'CASDocumentStatus' ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And Exists ( ");
                SQL.AppendLine("    Select 1 From tblgroupdepartment");
                SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where ");
                SQL.AppendLine("    UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Document"+Environment.NewLine+"Status",
                        "Approval"+Environment.NewLine+"Status",
                        
                        //6-10
                        "Completed",
                        "Department",
                        "Person In Charge", 
                        "Currency",
                        "Amount (Cost)",
                        
                        //11-15
                        "Amount (Cash Advance)",
                        "Voucher's Type",
                        "Remark",
                        "Created By",
                        "Created Date", 
                        
                        //16-18
                        "Created Time", 
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 60, 100, 180, 
                        
                        //6-10
                        70, 180, 200, 80, 130, 
                        
                        //11-15
                        130, 200, 200, 130, 130, 

                        //16-19
                        130, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, false);
            if (!mFrmParent.mIsCASUseCompletedInd)
                Sm.GrdColInvisible(Grd1, new int[] { 6 });
            if(!mFrmParent.mIsCASUseDraft)
                Sm.GrdColInvisible(Grd1, new int[] { 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "DocStatus", "StatusDesc", "CompletedInd", 
                        
                        //6-10
                        "DeptName", "UserName", "CurCode", "Amt1", "Amt2", 
                        
                        //11-15
                        "VoucherType", "Remark",  "CreateBy", "CreateDt", "LastUpBy", 
                        
                        //16
                        "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        if (mFrmParent.mIsCASUseCompletedInd)
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
