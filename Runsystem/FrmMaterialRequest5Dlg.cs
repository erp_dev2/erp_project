﻿#region Update
/*
    26/02/2021 [DITA/SIER] New Apps
    05/10/2022 [DITA/SIER] clear mljob, mljob2 ketika item berubah
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest5Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest5 mFrmParent;
        private string mSQL = string.Empty, nSQL = string.Empty, oSQL = string.Empty, mSQL2 = string.Empty, mDeptCode = string.Empty, mReqType = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequest5Dlg(FrmMaterialRequest5 FrmParent, string ReqType, string DeptCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mReqType = ReqType;
            mDeptCode = DeptCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mFrmParent.mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
                Sl.SetLueItGrpCode(ref LueItGrpCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "Local Code",
                        "", 
                        "Group",
                        
                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Item's Category",
                        "Item's Sub Category Code",
                        "Sub Category",
                        
                        //11-15
                        "UoM",
                        "Quotation#",
                        "D No",
                        "Quotation"+Environment.NewLine+"Date",
                        "Estimated"+Environment.NewLine+"Price",
                        
                        //16-20
                        "Minimum"+Environment.NewLine+"Stock",
                        "Reorder"+Environment.NewLine+"Point",
                        "Consumption"+Environment.NewLine+"last 1 Month",
                        "Consumption"+Environment.NewLine+"last 3 Month",
                        "Consumption"+Environment.NewLine+"last 6 Month",

                        //21-25
                        "Consumption"+Environment.NewLine+"last 9 Month",
                        "Consumption"+Environment.NewLine+"last 12 Month",
                        "Available Stock",
                        "",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 120, 20, 170,
                        
                        //6-10
                        200, 150, 150, 80, 150,
                        
                        //11-15
                        80, 150, 100, 100, 150,

                        //16-20
                        150, 150, 150, 150, 150,

                        //21-25 
                        150, 150, 150, 20, 300
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4, 24 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25});
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 18, 19, 20, 21, 22, 23 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 14 });
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 25 });
            Grd1.Cols[25].Move(8);
            if (mFrmParent.mIsShowForeignName)
            {
                if (Sm.GetParameter("IsUseItemConsumption") == "Y")
                {
                    if (Sm.GetParameter("ProcFormatDocNo") == "0")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 8, 9, 10, 13, 14, 15 }, false);
                    }
                    else
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 8, 9, 13, 14, 16, }, false);
                    }
                }
                else
                {
                    if (Sm.GetParameter("ProcFormatDocNo") == "0")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, false);
                    }
                    else
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 8, 9, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, false);
                    }
                }
            }
            else
            {
                if (Sm.GetParameter("IsUseItemConsumption") == "Y")
                {
                    if (Sm.GetParameter("ProcFormatDocNo") == "0")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7, 8, 9, 10, 13, 14, 15}, false);
                    }
                    else
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7, 8, 9, 13, 14, 15 }, false);
                        Grd1.Cols[20].Move(5);
                    }
                }
                else
                {
                    if (Sm.GetParameter("ProcFormatDocNo") == "0")
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22}, false);
                    }
                    else
                    {
                        Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7, 8, 9, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22}, false);
                    }
                }
            }
            if (mFrmParent.TxtDroppingRequestDocNo.Text.Length==0)
                Sm.GrdColInvisible(Grd1, new int[] { 24 }, false);
            else
                Grd1.Cols[24].Move(12);
            if (mFrmParent.mIsMRSPPJB) Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1 , false);
            if (mFrmParent.mIsMRItemGroupNotShow) Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            var SQL1 = new StringBuilder();
            var SQL2 = new StringBuilder();

                #region query without reorderpoint
                if (Sm.CompareStr(mReqType, "2") && !mFrmParent.mIsMaterialRequestForDroppingRequestEnabled)
                {
                    SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0.00 As UPrice, A.MinStock, A.ReorderStock, ");
                    SQL.AppendLine("A.ItName, A.ForeignName, ");
                    SQL.AppendLine("ifnull(C.Qty01, 0) As Mth01, ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, ");
                    SQL.AppendLine("A.ItScCode, D.ItScName, A.ItCodeInternal, A.ItGrpCode, E.ItGrpName, A.Specification  ");
                    SQL.AppendLine("From TblItem A ");
                    SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                    if (mFrmParent.mIsFilterByItCt)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                        SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    if (Sm.GetParameter("IsUseItemConsumption") == "Y")
                    {
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                        SQL.AppendLine("        From ( ");
                        SQL.AppendLine("        select Z1.itCode, ");
                        SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                        SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                        SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                        SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                        SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                        SQL.AppendLine("            From ");
                        SQL.AppendLine("            ( ");
                        SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                        SQL.AppendLine("                From ");
                        SQL.AppendLine("                ( ");
                        SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                        SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                        SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                        SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                        SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                        SQL.AppendLine("                )T1 ");
                        SQL.AppendLine("                Inner Join ");
                        SQL.AppendLine("                ( ");
                        SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                        SQL.AppendLine("	                Union ALL ");
                        SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL.AppendLine("	                Union All ");
                        SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL.AppendLine("	                Union All ");
                        SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL.AppendLine("	                Union All ");
                        SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                        SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                        SQL.AppendLine("        )Z1 ");
                        SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                        SQL.AppendLine(" ) C On C.ItCode = A.ItCode ");
                    }
                    else
                    {
                        SQL.AppendLine(" Left Join ( ");
                        SQL.AppendLine("        Select null As ItCode, 0 As Qty01, 0 As Qty03, 0 As Qty06, 0 As Qty09, 0 As Qty12 ");
                        SQL.AppendLine(" )C On C.ItCode = A.ItCode ");
                    }
                    SQL.AppendLine("Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
                    SQL.AppendLine("Left Join TblItemGroup E On A.ItGrpCode=E.ItGrpCode ");
                }
                else
                {
                    SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, ");
                    SQL.AppendLine("A.ItName, A.ForeignName, ");
                    SQL.AppendLine("C.UPrice*");
                    SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1.00 ");
                    SQL.AppendLine("    Else IfNull(E.Amt, 0.00) ");
                    SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                    SQL.AppendLine("ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, ");
                    SQL.AppendLine("A.ItScCode, G.ItScName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName, A.Specification ");
                    SQL.AppendLine("From TblItem A ");
                    SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                    if (mFrmParent.mIsFilterByItCt)
                    {
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                        SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                        SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                    SQL.AppendLine("    From TblQtHdr T1 ");
                    SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                    SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                    SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                    SQL.AppendLine("        Group By T3b.ItCode ");
                    SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                    SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                    SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                    SQL.AppendLine("    From TblCurrency T1 ");
                    SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                    SQL.AppendLine("    Left Join ( ");
                    SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                    SQL.AppendLine("        From TblCurrencyRate T3a  ");
                    SQL.AppendLine("        Inner Join  ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                    SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                    SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                    SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                    SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                    if (Sm.GetParameter("IsUseItemConsumption") == "Y")
                    {
                        SQL.AppendLine(" Left Join ( ");
                        SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                        SQL.AppendLine("        From ( ");
                        SQL.AppendLine("        select Z1.itCode, ");
                        SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                        SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                        SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                        SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                        SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                        SQL.AppendLine("            From ");
                        SQL.AppendLine("            ( ");
                        SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                        SQL.AppendLine("                From ");
                        SQL.AppendLine("                ( ");
                        SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                        SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                        SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                        SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                        SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                        SQL.AppendLine("                )T1 ");
                        SQL.AppendLine("                Inner Join ");
                        SQL.AppendLine("                ( ");
                        SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                        SQL.AppendLine("	                Union ALL ");
                        SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL.AppendLine("	                Union All ");
                        SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL.AppendLine("	                Union All ");
                        SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL.AppendLine("	                Union All ");
                        SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                        SQL.AppendLine("	                From TblDODeptHdr A ");
                        SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                        SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                        SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                        SQL.AppendLine("        )Z1 ");
                        SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                        SQL.AppendLine(" ) F On F.ItCode = A.ItCode ");
                    }
                    else
                    {
                        SQL.AppendLine(" Left Join ( ");
                        SQL.AppendLine("        Select null As ItCode, 0 As Qty01, 0 As Qty03, 0 As Qty06, 0 As Qty09, 0 As Qty12 ");
                        SQL.AppendLine(" )F On F.ItCode = A.ItCode ");
                    }
                    SQL.AppendLine("Left Join TblItemSubCategory G On A.ItScCode=G.ItScCode ");
                    SQL.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");

                }

                SQL.AppendLine("Where A.ActInd = 'Y' ");
                

                if (Sm.IsDataExist("Select 1 From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                    SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

                SQL.AppendLine("And Position(Concat('##', A.ItCode, '##') In @SelectedItem)<1 ");
                if (mFrmParent.mIsDORequestUseItemIssued) SQL.AppendLine("And A.ControlByDeptCode is Not null ");

                if (mFrmParent.TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("And A.ItCode In ( ");
                    SQL.AppendLine("    Select T3.ResourceItCode ");
                    SQL.AppendLine("    From TblDroppingRequestHdr T1 ");
                    SQL.AppendLine("    Inner Join TblDroppingRequestDtl T2 On T1.DocNo=T2.DocNo And T2.MRDocNo Is Null ");
                    SQL.AppendLine("    Inner Join TblProjectImplementationRBPHdr T3 On T2.PRBPDocNo=T3.DocNo ");
                    SQL.AppendLine("    Where T1.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("    And T1.PRJIDocNo Is Not Null ");
                    SQL.AppendLine("    And T1.PRJIDocNo=@PRJIDocNo ");
                    SQL.AppendLine(")  ");
                }

                if (mFrmParent.mDroppingRequestBCCode.Length > 0)
                {
                    SQL.AppendLine("And A.ItCode In ( ");
                    SQL.AppendLine("    Select T2.ItCode ");
                    SQL.AppendLine("    From TblDroppingRequestHdr T1 ");
                    SQL.AppendLine("    Inner Join TblDroppingRequestDtl2 T2 On T1.DocNo=T2.DocNo And T2.BCCode=@DroppingRequestBCCode And T2.MRDocNo Is Null And T2.Qty>0.00 ");
                    SQL.AppendLine("    Where T1.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("    And T1.PRJIDocNo Is Null ");
                    SQL.AppendLine(")  ");
                }

                mSQL = SQL.ToString();

                #endregion

                #region query with reorderpoint

                if (Sm.CompareStr(mReqType, "2") && !mFrmParent.mIsMaterialRequestForDroppingRequestEnabled)
                {
                    SQL1.AppendLine("Select A.ActInd, A.ControlByDeptCode, A.ItCode, A.ItCtCode, A.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, A.QtyStock, A.QtyOrder, A.Mth01, A.Mth03, A.Mth06, A.Mth09, A.Mth12, A.ItScCode, A.ItScName, ");
                    SQL1.AppendLine("A.ItName, A.ForeignName, A.ItCodeInternal, A.ItGrpCode, A.ItGrpName, A.Specification  ");
                    SQL1.AppendLine("From ");
                    SQL1.AppendLine("(Select A.ActInd,  A.ControlByDeptCode, A.ItCode, A.ItName, A.ItCtCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, ifnull(C.Qty, 0) AS QtyStock, ifnull(D.QtyOrder, 0) As QtyOrder, ");
                    SQL1.AppendLine("IfNull(E.Qty01, 0) As Mth01, ifnull(E.Qty03, 0) As Mth03, ifnull(E.Qty06, 0) As Mth06, ifnull(E.Qty09, 0) As Mth09, ifnull(E.Qty12, 0) As Mth12, ");
                    SQL1.AppendLine("A.ItScCode, F.ItScName, A.ForeignName, A.ItCodeInternal, A.ItGrpCode, G.ItGrpName, A.Specification  ");
                    SQL1.AppendLine("From TblItem A ");
                    SQL1.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                    if (mFrmParent.mIsFilterByItCt)
                    {
                        SQL1.AppendLine("And Exists( ");
                        SQL1.AppendLine("    Select 1 From TblGroupItemCategory ");
                        SQL1.AppendLine("    Where ItCtCode=B.ItCtCode ");
                        SQL1.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL1.AppendLine("    ) ");
                    }
                    SQL1.AppendLine("Left Join ");
                    SQL1.AppendLine(" (Select ItCode, SUM(QTY) As Qty from TblStockSummary where Qty>0 Group By ItCode) ");
                    SQL1.AppendLine(" C On A.ItCode = C.ItCode ");
                    SQL1.AppendLine("Left Join ");
                    SQL1.AppendLine("		(Select Sum(C.Qty)As QtyOrder, C.ItCode ");
                    SQL1.AppendLine("    From TblPODtl A ");
                    SQL1.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                    SQL1.AppendLine("    Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo ");
                    SQL1.AppendLine("    Where A.CancelInd='N' ");
                    SQL1.AppendLine("    And Concat(A.DocNo, A.DNo) Not In ( ");
                    SQL1.AppendLine("        Select Concat(PODocNo, PODNo) from TblRecvVdDtl Where CancelInd='N')");
                    SQL1.AppendLine("			 Group By C.ItCode ");
                    SQL1.AppendLine("    )D On A.ItCode = D.ItCode ");

                    if (Sm.GetParameter("IsUseItemConsumption") == "Y")
                    {
                        SQL1.AppendLine(" Left Join ( ");
                        SQL1.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                        SQL1.AppendLine("        From ( ");
                        SQL1.AppendLine("        select Z1.itCode, ");
                        SQL1.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                        SQL1.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                        SQL1.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                        SQL1.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                        SQL1.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                        SQL1.AppendLine("            From ");
                        SQL1.AppendLine("            ( ");
                        SQL1.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                        SQL1.AppendLine("                From ");
                        SQL1.AppendLine("                ( ");
                        SQL1.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                        SQL1.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                        SQL1.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                        SQL1.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                        SQL1.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                        SQL1.AppendLine("                )T1 ");
                        SQL1.AppendLine("                Inner Join ");
                        SQL1.AppendLine("                ( ");
                        SQL1.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                        SQL1.AppendLine("	                Union ALL ");
                        SQL1.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL1.AppendLine("	                Union All ");
                        SQL1.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL1.AppendLine("	                Union All ");
                        SQL1.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL1.AppendLine("	                Union All ");
                        SQL1.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL1.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                        SQL1.AppendLine("            Group By T1.mth, T2.ItCode ");
                        SQL1.AppendLine("        )Z1 ");
                        SQL1.AppendLine("   )Z2 Group By Z2.ItCode ");
                        SQL1.AppendLine(" )E On E.ItCode = A.ItCode ");
                    }
                    else
                    {
                        SQL1.AppendLine(" Left Join ( ");
                        SQL1.AppendLine("        Select null As ItCode, 0 As Qty01, 0 As Qty03, 0 As Qty06, 0 As Qty09, 0 As Qty12 ");
                        SQL1.AppendLine(" )E On E.ItCode = A.ItCode ");
                    }
                    SQL1.AppendLine("Left Join TblItemSubCategory F On A.ItScCode = F.ItScCode ");
                    SQL1.AppendLine("Left Join TblItemGroup G On A.ItGrpCode=G.ItGrpCode ");
                    SQL1.AppendLine("Where (ifnull(C.Qty, 0)+ ifnull(D.QtyOrder, 0)) <= A.ReorderStock ");
                    SQL1.AppendLine("And A.ReorderStock != 0 ) A ");
                }
                else
                {
                    SQL1.AppendLine("Select A.ActInd, A.ControlByDeptCode, A.ItCode, A.ItCtCode, A.ItCtName, A.PurchaseUomCode, A.DocNo, A.DNo, A.DocDt, ");
                    SQL1.AppendLine("A.ItName, A.ForeignName, A.ItgrpCode, A.ItGrpName, ");
                    SQL1.AppendLine("A.UPrice, A.MinStock, A.ReorderStock, A.QtyStock, A.QtyOrder, A.Mth01, A.Mth03, A.Mth06, A.Mth09, A.Mth12, A.ItScCode, A.ItScName, A.ForeignName, A.ItCodeInternal, A.Specification ");
                    SQL1.AppendLine("From ( ");
                    SQL1.AppendLine("   Select A.ActInd, A.ControlByDeptCode, A.ItCode, A.ItName, A.ItCtCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt,  ");
                    SQL1.AppendLine("   C.UPrice*");
                    SQL1.AppendLine("       Case When IfNull(C.CurCode, '')=D.ParValue Then 1.00 ");
                    SQL1.AppendLine("       Else IfNull(E.Amt, 0) ");
                    SQL1.AppendLine("       End As UPrice, A.MinStock, A.ReorderStock, ifnull(Z1.Qty, 0) AS QtyStock, ifnull(Z2.QtyOrder, 0) As QtyOrder, ");
                    SQL1.AppendLine("   IfNull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, ");
                    SQL1.AppendLine("   A.ItScCode, G.ItScName, A.ForeignName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName, A.Specification ");
                    SQL1.AppendLine("   From TblItem A ");
                    SQL1.AppendLine("   Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                    SQL1.AppendLine("   Left Join ( ");
                    SQL1.AppendLine("       Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                    SQL1.AppendLine("       From TblQtHdr T1 ");
                    SQL1.AppendLine("       Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                    SQL1.AppendLine("       Inner Join ( ");
                    SQL1.AppendLine("           Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                    SQL1.AppendLine("           From TblQtHdr T3a, TblQtDtl T3b ");
                    SQL1.AppendLine("           Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                    SQL1.AppendLine("           Group By T3b.ItCode ");
                    SQL1.AppendLine("       ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                    SQL1.AppendLine("   ) C On A.ItCode=C.ItCode ");
                    SQL1.AppendLine("   Left Join TblParameter D On D.ParCode='MainCurCode' ");
                    SQL1.AppendLine("   Left Join ( ");
                    SQL1.AppendLine("       Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                    SQL1.AppendLine("       From TblCurrency T1 ");
                    SQL1.AppendLine("       Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                    SQL1.AppendLine("       Left Join ( ");
                    SQL1.AppendLine("           Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                    SQL1.AppendLine("           From TblCurrencyRate T3a  ");
                    SQL1.AppendLine("           Inner Join  ");
                    SQL1.AppendLine("           ( ");
                    SQL1.AppendLine("               Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                    SQL1.AppendLine("               From TblCurrencyRate Group By CurCode1, CurCode2 ");
                    SQL1.AppendLine("           ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                    SQL1.AppendLine("       ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                    SQL1.AppendLine("   ) E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                    SQL1.AppendLine("   Left Join ");
                    SQL1.AppendLine("   (   Select ItCode, SUM(QTY) As Qty from TblStockSummary where Qty>0 Group By ItCode) ");
                    SQL1.AppendLine("   Z1 On A.ItCode = Z1.ItCode ");
                    SQL1.AppendLine("Left Join ");
                    SQL1.AppendLine("	(Select Sum(C.Qty)As QtyOrder, C.ItCode ");
                    SQL1.AppendLine("    From TblPODtl A ");
                    SQL1.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                    SQL1.AppendLine("    Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo ");
                    SQL1.AppendLine("    Where A.CancelInd='N' ");
                    SQL1.AppendLine("    And Concat(A.DocNo, A.DNo) Not In ( ");
                    SQL1.AppendLine("        Select Concat(PODocNo, PODNo) from TblRecvVdDtl Where CancelInd='N')");
                    SQL1.AppendLine("			 Group By C.ItCode ");
                    SQL1.AppendLine("    )Z2 On A.ItCode = Z2.ItCode ");

                    if (Sm.GetParameter("IsUseItemConsumption") == "Y")
                    {
                        SQL1.AppendLine(" Left Join ( ");
                        SQL1.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                        SQL1.AppendLine("        From ( ");
                        SQL1.AppendLine("        select Z1.itCode, ");
                        SQL1.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                        SQL1.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                        SQL1.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                        SQL1.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                        SQL1.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                        SQL1.AppendLine("            From ");
                        SQL1.AppendLine("            ( ");
                        SQL1.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                        SQL1.AppendLine("                From ");
                        SQL1.AppendLine("                ( ");
                        SQL1.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                        SQL1.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                        SQL1.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                        SQL1.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                        SQL1.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                        SQL1.AppendLine("                )T1 ");
                        SQL1.AppendLine("                Inner Join ");
                        SQL1.AppendLine("                ( ");
                        SQL1.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                        SQL1.AppendLine("                       And Position(Concat('##', B.ItCode, '##') In @SelectedItem)<1 ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                        SQL1.AppendLine("	                Union ALL ");
                        SQL1.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                        SQL1.AppendLine("                       And Position(Concat('##', B.ItCode, '##') In @SelectedItem)<1 ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL1.AppendLine("	                Union All ");
                        SQL1.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                        SQL1.AppendLine("                       And Position(Concat('##', B.ItCode, '##') In @SelectedItem)<1 ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL1.AppendLine("	                Union All ");
                        SQL1.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                        SQL1.AppendLine("                       And Position(Concat('##', B.ItCode, '##') In @SelectedItem)<1 ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL1.AppendLine("	                Union All ");
                        SQL1.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                        SQL1.AppendLine("	                From TblDODeptHdr A ");
                        SQL1.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                        SQL1.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                        SQL1.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                        SQL1.AppendLine("                       And Position(Concat('##', B.ItCode, '##') In @SelectedItem)<1 ");
                        SQL1.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                        SQL1.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                        SQL1.AppendLine("            Group By T1.mth, T2.ItCode ");
                        SQL1.AppendLine("        )Z1 ");
                        SQL1.AppendLine("   )Z2 Group By Z2.ItCode ");
                        SQL1.AppendLine(" )F On F.ItCode = A.ItCode ");
                    }
                    else
                    {
                        SQL1.AppendLine(" Left Join ( ");
                        SQL1.AppendLine("        Select null As ItCode, 0 As Qty01, 0 As Qty03, 0 As Qty06, 0 As Qty09, 0 As Qty12 ");
                        SQL1.AppendLine(" )F On F.ItCode = A.ItCode ");
                    }
                    SQL1.AppendLine("Left Join TblItemSubCategory G On A.ItScCode = G.ItScCode ");
                    SQL1.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");
                    SQL1.AppendLine("Where (ifnull(Z1.Qty, 0)+ ifnull(Z2.QtyOrder, 0)) <= A.ReorderStock ");
                    SQL1.AppendLine("And A.ReorderStock != 0 ) A ");
                }

                SQL1.AppendLine("Where A.ActInd = 'Y' ");
                if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                    SQL1.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

                SQL1.AppendLine("And Position(Concat('##', A.ItCode, '##') In @SelectedItem)<1 ");
                if (mFrmParent.mIsDORequestUseItemIssued)
                {
                    SQL1.AppendLine("And A.ControlByDeptCode is Not null ");
                }

                if (mFrmParent.TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("And A.ItCode In ( ");
                    SQL.AppendLine("    Select T3.ResourceItCode ");
                    SQL.AppendLine("    From TblDroppingRequestHdr T1 ");
                    SQL.AppendLine("    Inner Join TblDroppingRequestDtl T2 On T1.DocNo=T2.DocNo And T2.MRDocNo Is Null ");
                    SQL.AppendLine("    Inner Join TblProjectImplementationRBPHdr T3 On T2.PRBPDocNo=T3.DocNo ");
                    SQL.AppendLine("    Where T1.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("    And T1.PRJIDocNo Is Not Null ");
                    SQL.AppendLine("    And T1.PRJIDocNo=@PRJIDocNo ");
                    SQL.AppendLine(")  ");
                }

                if (mFrmParent.mDroppingRequestBCCode.Length > 0)
                {
                    SQL.AppendLine("And A.ItCode In ( ");
                    SQL.AppendLine("    Select T2.ItCode ");
                    SQL.AppendLine("    From TblDroppingRequestHdr T1 ");
                    SQL.AppendLine("    Inner Join TblDroppingRequestDtl2 T2 On T1.DocNo=T2.DocNo And T2.BCCode=@DroppingRequestBCCode And T2.MRDocNo Is Null And T2.Qty>0.00 ");
                    SQL.AppendLine("    Where T1.DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("    And T1.PRJIDocNo Is Null ");
                    SQL.AppendLine(")  ");
                }

                nSQL = SQL1.ToString();

                #endregion

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (ChkROP.Checked)
                {
                    oSQL = nSQL.ToString();
                }
                else
                {
                    oSQL = mSQL.ToString();
                }
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 "; string Filter2 = string.Empty;

                var cm = new MySqlCommand();

                DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItGrpCode), "A.ItGrpCode", true);

                if (mFrmParent.mIsMaterialRequestForDroppingRequestEnabled)
                {
                    Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", mFrmParent.TxtDroppingRequestDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@PRJIDocNo", mFrmParent.TxtDR_PRJIDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mFrmParent.mDroppingRequestBCCode);    
                }

                if (mFrmParent.mItGrpCodeNotShowOnMaterialRequest.Length > 0)
                {
                    Filter2 = " And Not Find_In_set(A.ItGrpCode, @ItGrpCodeNotShowOnMaterialRequest) ";
                    Sm.CmParam<String>(ref cm, "@ItGrpCodeNotShowOnMaterialRequest", mFrmParent.mItGrpCodeNotShowOnMaterialRequest);
                }

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        oSQL.ToString() + Filter + Filter2 + " Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "ItGrpName", "ItName", "ForeignName",  "ItCtName",   
                            
                            //6-7
                            "ItScCode", "ItScName", "PurchaseUomCode", "DocNo", "DNo", 

                            //11-15
                            "DocDt", "UPrice", "MinStock", "ReorderStock", "Mth01",   
  
                            //16-20
                            "Mth03", "Mth06", "Mth09", "Mth12", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = ChkAutoChoose.Checked;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            if (mFrmParent.mIsMRAutomaticGetLastVendorQuotation)
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 11);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            }
                            else
                            {
                                Grd.Cells[Row, 12].Value = string.Empty;
                                Grd.Cells[Row, 13].Value = string.Empty;
                                Grd.Cells[Row, 14].Value = string.Empty;
                                Grd.Cells[Row, 15].Value = 0m;
                            }
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                            Grd.Cells[Row, 23].Value = 0 ;
                            if (mFrmParent.mIsBudgetActive)
                                Grd1.Cells[Row, 6].ForeColor = Sm.GetGrdStr(Grd, Row, 12).Length>0?Color.Black:Color.Red;

                        }, true, false, false, false
                    );
                if (ChkShowStock.Checked) Process6();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ChooseDataMRSPPJB()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowPOQtyCancelInfo(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));

                int Row1 = 0, 
                    Row2 = Grd1.CurRow.Index;
                
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 2);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 6);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 7);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 11);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 12);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 13);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 14);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 15);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 17);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 9);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 10);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 3);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 25);

                if (mFrmParent.mIsBudgetActive)
                    mFrmParent.Grd1.Cells[Row1, 11].ForeColor = Sm.GetGrdStr(mFrmParent.Grd1, Row1, 20).Length > 0 ? Color.Black : Color.Red;

                mFrmParent.ComputeTotal(Row1);
                mFrmParent.mlJob.Clear();
                mFrmParent.mlJob2.Clear();
               

                this.Close();
            }
        }

        override protected void ChooseData()
        {
            if (mFrmParent.mIsMRSPPJB)
            {
                ChooseDataMRSPPJB();
                return;
            }
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 25);

                        if (mFrmParent.mIsBudgetActive)
                            mFrmParent.Grd1.Cells[Row1, 11].ForeColor = Sm.GetGrdStr(mFrmParent.Grd1, Row1, 20).Length > 0 ? Color.Black : Color.Red;

                        mFrmParent.ComputeTotal(Row1);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 17, 23, 24, 29, 35 });
                        
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            mFrmParent.SetSeqNo();
            mFrmParent.SetDroppingReqProjImplItQty();
            mFrmParent.SetDroppingReqBCItQty();
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 8), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (mFrmParent.mIsMRSPPJB && e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 24)
            {
                e.DoDefault = false;
                var f = new FrmQt(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.Tag;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 24)
            {
                var f = new FrmQt(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        //nentuin item
        private void Process1(ref List<P1> l)
        {
            l.Clear();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string ItCode = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length>0)
                {
                    l.Add(new P1()
                    {
                        ItCode = Sm.GetGrdStr(Grd1, Row, 2),
                        QtyStock = 0,
                    });
                }
            }
        }

        //nentuin stock summary
        private void Process2(ref List<P1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            string Filter = string.Empty;
            int Temp = 0;

            if (l.Count > 0)
            {
                for (var i = 0; i < l.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(ItCode = '" + l[i].ItCode + "')";
                }
            }
            
            SQL.AppendLine("Select ItCode, ifnull(Sum(Qty), 0) Qty ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where Qty<>0 ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Group By ItCode ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "Qty"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (var i = Temp; i < l.Count; i++)
                        {
                            if (string.Compare(l[i].ItCode, dr.GetString(0)) == 0)
                            {
                                l[i].QtyStock = dr.GetDecimal(1);
                            }
                        }
                    }

                }
                dr.Close();
            }
        }

        //nentuion ordered
        private void Process3(ref List<P1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            string Filter = string.Empty;
            int Temp = 0;
           
            if (l.Count > 0)
            {
                for (var i = 0; i < l.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T.ItCode = '" + l[i].ItCode + "')";
                }
            }
            SQL.AppendLine("Select T.ItCode, ifnull(Sum(T.Qty), 0) Qty From (");
            SQL.AppendLine("    Select C.ItCode, A.Qty-IfNull(D.Qty2, 0)-IfNull(E.Qty3, 0) As Qty  ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo  ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As Qty2 ");
            SQL.AppendLine("        From TblRecvVdDtl T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N'  ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
            SQL.AppendLine("        From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl T4 On T3.MaterialRequestDocNo=T4.DocNo And T3.MaterialRequestDNo=T4.DNo  ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) E On A.DocNo=E.DocNo And A.DNo=E.DNo ");
            SQL.AppendLine("    Where A.ProcessInd<>'F' And A.CancelInd='N' ");
            SQL.AppendLine(") T Where 0=0 ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Group By T.ItCode ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "Qty"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (var i = Temp; i < l.Count; i++)
                        {
                            if (string.Compare(l[i].ItCode, dr.GetString(0)) == 0)
                            {
                                decimal QtyStock = l[i].QtyStock; 
                                l[i].QtyStock =  QtyStock - Sm.DrDec(dr, c[1]);
                                //Temp = i;
                                //break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        //nentuin requested
        private void Process4(ref List<P1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            string Filter = string.Empty;
            int Temp = 0;

            if (l.Count > 0)
            {
                for (var i = 0; i < l.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T.ItCode = '" + l[i].ItCode + "')";
                }
            }

            SQL.AppendLine("Select T.ItCode, ifnull(Sum(T.Qty), 0) Qty From  (");
            SQL.AppendLine("    Select A.ItCode, A.Qty-IfNull(B.Qty, 0)-ifnull(C.Qty, 0) As Qty  ");
            SQL.AppendLine("    From TblMaterialRequestDtl A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.MaterialRequestDocNo As DocNo, T2.MaterialRequestDNo As DNo, Sum(T1.Qty) As Qty ");
            SQL.AppendLine("        From TblPODtl T1 ");
            SQL.AppendLine("        Inner Join TblPORequestDtl T2 On T1.PORequestDocNo=T2.DocNo And T1.PORequestDNo=T2.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl T3 On T2.MaterialRequestDocNo=T3.DocNo And T2.MaterialRequestDNo=T3.DNo And T3.ProcessInd<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T2.MaterialRequestDocNo, T2.MaterialRequestDNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join TblMRQtyCancel C On A.DocNo = C.MRDocNo And A.DNo = C.MRDNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    Where A.ProcessInd<>'F' And A.CancelInd='N' And A.Status<>'C'  ");
            SQL.AppendLine(") T Where 0=0 ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Group By T.ItCode ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "Qty"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (var i = Temp; i < l.Count; i++)
                        {
                            if (string.Compare(l[i].ItCode, dr.GetString(0)) == 0)
                            {
                                decimal QtyStock = l[i].QtyStock;
                                l[i].QtyStock = QtyStock - Sm.DrDec(dr, c[1]);
                                //Temp = i;
                                //break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        //nentuin committed
        private void Process5(ref List<P1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            string Filter = string.Empty;
            int Temp = 0;

            if (l.Count > 0)
            {
                for (var i = 0; i < l.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(ItCode = '" + l[i].ItCode + "')";
                }
            }

            SQL.AppendLine("Select T.ItCode, (-1*IfNull(T.Qty2, 0)) As QtyCommitted ");
            SQL.AppendLine("from ( ");
            SQL.AppendLine("    Select B.ItCode, Sum(B.Qty) As Qty2 ");
            SQL.AppendLine("    From TblDOWhsHdr A ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' "); 
            SQL.AppendLine("    Where A.CancelInd='N' And A.Status='O' ");
            SQL.AppendLine("    Group By B.ItCode ");
            SQL.AppendLine(")T Where 0=0 ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Group By ItCode ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "QtyCommitted"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (var i = Temp; i < l.Count; i++)
                        {
                            if (string.Compare(l[i].ItCode, dr.GetString(0)) == 0)
                            {
                                decimal QtyStock = l[i].QtyStock;
                                l[i].QtyStock = QtyStock - Sm.DrDec(dr, c[1]);
                                //Temp = i;
                                //break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        //nentuin Available
        private void Process6()
        {
            try
            {
                var lP1 = new List<P1>();

                Process1(ref lP1);
                if (lP1.Count > 0)
                {
                    Process2(ref lP1);
                    Process3(ref lP1);
                    Process4(ref lP1);
                    Process5(ref lP1);

                    if (lP1.Count > 0)
                    {
                        for (var i = 0; i < lP1.Count; i++)
                        {
                            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                            {
                                if (string.Compare(Sm.GetGrdStr(Grd1, Row, 2), lP1[i].ItCode) == 0)
                                {
                                    Grd1.Cells[Row, 23].Value = lP1[i].QtyStock;
                                    break;
                                }
                            }
                        }
                    }
                }
                lP1.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }

        }


        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }


        private void LueItGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItGrpCode, new Sm.RefreshLue2(Sl.SetLueItGrpCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's group");
        }

        #endregion

        #endregion

        #region Class

        private class P1
        {
            public string ItCode { set; get; }
            public decimal QtyStock { set; get; }
        }

        #endregion
     }
}
