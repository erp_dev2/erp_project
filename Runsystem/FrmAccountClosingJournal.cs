﻿#region Update
/*
    14/02/2018 [HAR] ubah Redaksional nama 
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAccountClosingJournal : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "";
        internal FrmAccountClosingJournalFind FrmFind;

        #endregion

        #region Constructor

        public FrmAccountClosingJournal(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Account Closing Journal";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Category",
                        "Account#",
                        "Description",
                        "Type"
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-4
                        20, 130, 120, 300, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 6;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Category",
                        "Account#",
                        "Description",
                        "Type"
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        20, 130, 120, 300, 80
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 6;
            Grd3.FrozenArea.ColCount = 1;
            Grd3.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "Dno",

                        //1-5
                        "",
                        "Category",
                        "Account#",
                        "Description",
                        "Type"
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-4
                        20, 130, 120, 300, 80
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtNumber,
                        DteDocDt 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5 });
                    ChkActiveInd.Properties.ReadOnly = true;
                    TxtNumber.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1 });
                    ChkActiveInd.Checked = true;
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1 });
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtNumber, DteDocDt,
            });
            ChkActiveInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.ClearGrd(Grd3, true);
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAccountClosingJournalFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            ChkActiveInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtNumber, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
               InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmAccountClosingJournalDlg(this, "1"));
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled) Sm.FormShowDialog(new FrmAccountClosingJournalDlg(this, "1"));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled) Sm.FormShowDialog(new FrmAccountClosingJournalDlg(this, "2"));
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmAccountClosingJournalDlg(this, "2"));
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd2, e, BtnSave);
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmAccountClosingJournalDlg(this, "3"));
                }
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled) Sm.FormShowDialog(new FrmAccountClosingJournalDlg(this, "3"));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string Number = string.Empty;
            if (TxtNumber.Text.Length > 0)
            {
                Number = TxtNumber.Text;
            }
            else
            {
                Number = Sm.GetValue("Select max(Number)+1 As Number From TblAccountClosingJournal ").Length > 0 ?
                    Sm.GetValue("Select max(Number)+1 As Number From TblAccountClosingJournal ") : "1";
            }

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateACJ(Number));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0) cml.Add(SaveACJl(Number, Row));

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0) cml.Add(SaveACJ2(Number, Row));

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 3).Length > 0) cml.Add(SaveACJ3(Number, Row));

           
            Sm.ExecCommands(cml);

            ShowData(Number);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date");
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list journal 1.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty2()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list journal 2.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty3()
        {
            if (Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list journal 3.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveACJl(string Number, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAccountClosingJournal(Number, JnGroup, DNo, DocDt, ActiveInd, AcNo, CreateBy, CreateDt) " +
                    "Values(@Number, @JnGroup, @DNo, @DocDt, 'Y', @AcNo, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@Number", Number);
            Sm.CmParam<String>(ref cm, "@JnGroup", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveACJ2(string Number, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAccountClosingJournal(Number, JnGroup, DNo, DocDt, ActiveInd, AcNo, CreateBy, CreateDt) " +
                    "Values(@Number, @JnGroup, @DNo, @DocDt, 'Y', @AcNo, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@Number", Number);
            Sm.CmParam<String>(ref cm, "@JnGroup", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveACJ3(string Number, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAccountClosingJournal(Number, JnGroup, DNo, DocDt, ActiveInd, AcNo, CreateBy, CreateDt) " +
                    "Values(@Number, @JnGroup, @DNo, @DocDt, 'Y', @AcNo, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@Number", Number);
            Sm.CmParam<String>(ref cm, "@JnGroup", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand UpdateACJ(string Number)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblAccountClosingJournal Where Number=@Number ; "+
                    "Update TblAccountClosingJournal  " +
                    "SET ActiveInd = 'N' Where Number != @number ; "
            };
            Sm.CmParam<String>(ref cm, "@Number", Number);
            return cm;
        }
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string Number)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowACJHdr(Number);
                ShowACJDtl(Number);
                ShowACJDtl2(Number);
                ShowACJDtl3(Number);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowACJHdr(string Number)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Number", Number);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.Number, A.DocDt, A.ActiveInd " +
                    "From TblAccountClosingJournal A " +
                    "Where A.Number=@Number;",
                    new string[] 
                    { 
                        //0
                        "Number", 

                        //1-5
                        "DocDt", "ActiveInd" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtNumber.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    }, true
                );
        }

        private void ShowACJDtl(string Number)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.JnGroup, A.AcNo, B.AcDesc, If(B.Actype = 'D', 'Debet', 'Credit') As AcType ");
            SQL.AppendLine("From TblAccountClosingJournal A  ");
            SQL.AppendLine("Inner Join TblCoa B On A.AcNo = B.AcNo  ");
            SQL.AppendLine("Where A.Number=@Number And A.JnGroup = '1' ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Number", Number);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-4
                    "JnGroup", "AcNo", "AcDesc", "Actype"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowACJDtl2(string Number)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.JnGroup, A.AcNo, B.AcDesc, If(B.Actype = 'D', 'Debet', 'Credit') As AcType ");
            SQL.AppendLine("From TblAccountClosingJournal A  ");
            SQL.AppendLine("Inner Join TblCoa B On A.AcNo = B.AcNo  ");
            SQL.AppendLine("Where A.Number=@Number And A.JnGroup = '2' ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Number", Number);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "JnGroup", "AcNo", "AcDesc", "Actype"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowACJDtl3(string Number)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.JnGroup, A.AcNo, B.AcDesc, If(B.Actype = 'D', 'Debet', 'Credit') As AcType ");
            SQL.AppendLine("From TblAccountClosingJournal A  ");
            SQL.AppendLine("Inner Join TblCoa B On A.AcNo = B.AcNo  ");
            SQL.AppendLine("Where A.Number=@Number And A.JnGroup = '3' ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Number", Number);

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "JnGroup", "AcNo", "AcDesc", "Actype"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd3, 0, 1);
        }


        #endregion

        #region Additional Method

        internal string GetSelectedCOA(iGrid GrdXXX)
        {
            var SQL = string.Empty;
            if (GrdXXX.Rows.Count != 1)
            {
                for (int Row = 0; Row < GrdXXX.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(GrdXXX, Row, 3).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(GrdXXX, Row, 3) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }
        #endregion

        #endregion
    }
}
