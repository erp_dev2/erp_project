﻿#region Update
/*
  10/06/2019 [WED] dialog baru untuk tambah informasi di detail POR
*/
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPORequestDlg5 : RunSystem.FrmBase7
    {
        #region Field

        internal string mMenuCode = string.Empty;
        private string 
            mSQL = string.Empty, 
            mMaterialRequestDocNo = string.Empty,
            mMaterialRequestDNo = string.Empty,
            mItCode = string.Empty,
            mItName = string.Empty;
        internal FrmPORequest mFrmParent;
        
        #endregion

        #region Constructor

        public FrmPORequestDlg5(FrmPORequest FrmParent, string MaterialRequestDocNo, string MaterialRequestDNo, string ItCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mItCode = ItCode;
            mItName = Sm.GetValue("Select IfNull(ItName, '') From TblItem Where ItCode = @Param Limit 1;", mItCode);
            mMaterialRequestDocNo = MaterialRequestDocNo;
            mMaterialRequestDNo = MaterialRequestDNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetFormControl(mState.View);

                TxtItCode.EditValue = mItCode;
                TxtItName.EditValue = mItName;

                if (mFrmParent.TxtDocNo.Text.Length > 0)
                {
                    ShowDataDtl(mFrmParent.TxtDocNo.Text);
                }
                else
                {
                    if (mFrmParent.lPOR2Dtl != null)
                        if (mFrmParent.lPOR2Dtl.Count > 0)
                            ShowDataDtlTemp(ref mFrmParent.lPOR2Dtl);
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtBrand
                    }, (mFrmParent.TxtDocNo.Text.Length > 0));
                    TxtBrand.Focus();
                    BtnRefresh.Visible = false;
                    BtnSave.Visible = (mFrmParent.TxtDocNo.Text.Length <= 0);
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItCode, TxtItName, TxtBrand
            });
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;

                SaveTempAdditionalDtl();
                this.Hide();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Show Data

        private void ShowDataDtlTemp(ref List<FrmPORequest.PORYKDtl> lPOR2Dtl)
        {
            for (int i = 0; i < lPOR2Dtl.Count; i++)
            {
                if (lPOR2Dtl[i].MaterialRequestDocNo == mMaterialRequestDocNo &&
                    lPOR2Dtl[i].MaterialRequestDNo == mMaterialRequestDNo &&
                    lPOR2Dtl[i].ItCode == mItCode)
                {
                    TxtItCode.EditValue = lPOR2Dtl[i].ItCode;
                    TxtItName.EditValue = Sm.GetValue("Select IfNull(ItName, '') From TblItem Where ItCode = @Param Limit 1; ", lPOR2Dtl[i].ItCode);
                    TxtBrand.EditValue = lPOR2Dtl[i].Brand;
                    break;
                }
            }
        }

        private void ShowDataDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, A.Brand ");
            SQL.AppendLine("From TblPORequest2Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.MaterialRequestDocNo = @MaterialRequestDocNo ");
            SQL.AppendLine("And A.MaterialRequestDNo = @MaterialRequestDNo ");
            SQL.AppendLine("And A.ItCode = @ItCode Limit 1; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", mMaterialRequestDocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDNo", mMaterialRequestDNo);
            Sm.CmParam<String>(ref cm, "@ItCode", mItCode);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    "ItCode", 
                    "ItName", "Brand"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtItCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[1]);
                    TxtBrand.EditValue = Sm.DrStr(dr, c[2]);
                }, false
            );
        }

        #endregion

        #region Save Data

        private void SaveTempAdditionalDtl()
        {
            if (mFrmParent.lPOR2Dtl == null) mFrmParent.lPOR2Dtl = new List<FrmPORequest.PORYKDtl>();

            if (mFrmParent.lPOR2Dtl.Count <= 0)
            {
                mFrmParent.lPOR2Dtl.Add(new FrmPORequest.PORYKDtl()
                {
                    MaterialRequestDocNo = mMaterialRequestDocNo,
                    MaterialRequestDNo = mMaterialRequestDNo,
                    ItCode = mItCode,
                    Brand = TxtBrand.Text
                });
            }
            else
            {
                bool mFlag = false;
                for (int i = 0; i < mFrmParent.lPOR2Dtl.Count; i++)
                {
                    if (mFrmParent.lPOR2Dtl[i].MaterialRequestDocNo == mMaterialRequestDocNo &&
                        mFrmParent.lPOR2Dtl[i].MaterialRequestDNo == mMaterialRequestDNo)
                    {
                        mFrmParent.lPOR2Dtl[i].Brand = TxtBrand.Text;
                        mFlag = true;
                        break;
                    }
                }

                if (!mFlag)
                {
                    mFrmParent.lPOR2Dtl.Add(new FrmPORequest.PORYKDtl()
                    {
                        MaterialRequestDocNo = mMaterialRequestDocNo,
                        MaterialRequestDNo = mMaterialRequestDNo,
                        ItCode = mItCode,
                        Brand = TxtBrand.Text
                    });
                }
            }
        }

        #endregion

        #endregion
    }
}
