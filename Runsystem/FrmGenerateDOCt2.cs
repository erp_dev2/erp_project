﻿#region Update
/*
    31/01/2020 [WED/SIER] new apps
    06/02/2020 [WED/SIER] whs bisa milih
    25/02/2020 [VIN/SIER] tambah kolom grand total dan round up
    16/06/2020 [IBL/SIER] menghilangkan year dan month
    17/06/2020 [VIN/SIER] tambah item di header 
    17/09/2021 [TKG/ALL] tambah validasi setting journal
    08/03/2023 [WED/KBN] ketika parameter GenerateDOCt2RentPrice, GenerateDOCt2UPrice, GenerateDOCt2RetributionPrice kosong, maka field grid untuk ketiga kolom tersebut editable
    04/04/2023 [ISN/KBN] ketika parameter GenerateDOCt2RentPrice, GenerateDOCt2UPrice, GenerateDOCt2RetributionPrice null, maka kolom (rent dan retribution) price berubah menjadi kolom (fixed dan maintenance) cost 
    24/04/2023 [WED/KBN] tambah logic print out berdasarkan parameter FormPrintOutGenerateDOCt2
    28/04/2023 [SET/KBN] printout baru
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGenerateDOCt2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmGenerateDOCt2Find FrmFind;
        private string
            mGenerateDOCt2WhsCode = string.Empty,
            mGenerateDOCt2ItCode = string.Empty,
            mAcNoForInitialStockOpeningBalance = string.Empty,
            mFormPrintOutGenerateDOCt2= string.Empty,
            mEntCode = string.Empty;
        private decimal
            mInventoryUomCodeConvert12 = 0m,
            mInventoryUomCodeConvert13 = 0m;
        internal decimal
            mGenerateDOCt2UPrice = 0m,
            mGenerateDOCt2RentPrice = 0m,
            mGenerateDOCt2RetributionPrice = 0m;
        private bool
            mIsCheckCOAJournalNotExists = false,
            mIsBatchNoUseDocDtIfEmpty = false,
            mIsAutoJournalActived = false,
            mIsMovingAvgEnabled = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtAmtRounded = false,
            mGenerateDOCt2UPriceIsNull = false,
            mGenerateDOCt2RentPriceIsNull = false,
            mGenerateDOCt2RetributionPriceIsNull = false;

        #endregion

        #region Constructor

        public FrmGenerateDOCt2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Generate DO To Customer (Water)";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueCtCtCode(ref LueCtCtCode);

                Sl.SetLueMth(LueMth);
                SetFormControl(mState.View);
                SetLueWhsCode(ref LueWhsCode, mGenerateDOCt2WhsCode);
                SetGrd();
                LblYr.Visible = LblMth.Visible = LueYr.Visible = LueMth.Visible = false;

                base.FrmLoad(sender, e);
                //if this application is called from other application

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Customer's Code", 
                    "DNo", 
                    "Customer's Name", 
                    "Shipping's Address", 
                    "This Period"+Environment.NewLine+"Meter", 

                    //6-10
                    "Last Period"+Environment.NewLine+"Meter", 
                    "Balance (m3)", 
                    "Unit Price", 
                    ((mGenerateDOCt2UPriceIsNull && mGenerateDOCt2RentPriceIsNull && mGenerateDOCt2RetributionPriceIsNull) ? "Fixed Cost" : "Rent Price"), 
                    ((mGenerateDOCt2UPriceIsNull && mGenerateDOCt2RentPriceIsNull && mGenerateDOCt2RetributionPriceIsNull) ? "Maintenance"+Environment.NewLine+"Cost" : "Retribution" + Environment.NewLine + "Price"), 
                    
                    //11-15
                    "Total Bills", 
                    "PPN (10%)", 
                    "Total After Tax", 
                    "Object", 
                    "Target", 
                    
                    //16-19
                    "Bill", 
                    "DO#", 
                    "Round Up", 
                    "GrandTotal"
                },
                new int[] 
                { 
                    //0
                    20, 
                    //1-5
                    120, 0, 200, 400, 120, 
                    //6-10
                    120, 120, 120, 120, 120,
                    //11-15
                    120, 120, 120, 120, 120, 
                    //16-19
                    120, 180, 120, 120
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 17 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueMth, LueYr, LueWhsCode, LueCtCtCode, TxtItCode, TxtItName
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnItCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueMth, LueYr, LueWhsCode, LueCtCtCode
                    }, false);
                    BtnItCode.Enabled = true;
                    Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 6, 18 });
                    if (Sm.GetParameter("GenerateDOCt2UPrice").Length == 0) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 8 });
                    if (Sm.GetParameter("GenerateDOCt2RentPrice").Length == 0) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 9 });
                    if (Sm.GetParameter("GenerateDOCt2RetributionPrice").Length == 0) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 10 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, LueMth, LueYr, LueWhsCode, LueCtCtCode
                    }, false);
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mEntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, MeeRemark, LueYr, LueMth, LueCtCtCode, TxtItCode, TxtItName
            });
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGenerateDOCt2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            try
            {
                
                SetFormControl(mState.Edit);
                Sm.SetDteCurrentDate(DteDocDt);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (mFormPrintOutGenerateDOCt2.Length > 0)
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                ParPrint(TxtDocNo.Text);
            }
        }

        #endregion

        #region Grid Methods

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 5, 6, 8, 9, 10, 18 }, e.ColIndex))
                {
                    ComputeDetail(e.RowIndex);
                }
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCtCode, "Customer Category"))
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmGenerateDOCt2Dlg(this));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCtCode, "Customer Category"))
                {
                    Sm.FormShowDialog(new FrmGenerateDOCt2Dlg(this));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "GenerateDOCt2", "TblGenerateDOCt2Hdr");
            string StockInitialDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "StockInitial", "TblStockInitialHdr");
            mEntCode = Sm.GetValue("Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode = '" + Sm.GetLue(LueWhsCode) + "' limit 1");

            var cml = new List<MySqlCommand>();
            var l = new List<GenerateDOCt2Hdr>();
            var ldtl = new List<GenerateDOCt2Dtl>();
            var l2 = new List<StockInitialHdr>();
            var l2dtl = new List<StockInitialDtl>();
            var l3 = new List<DOCtHdr>();
            var l3dtl = new List<DOCtDtl>();

            ProcessHdr(ref l, DocNo);
            ProcessDtl(ref ldtl, DocNo, StockInitialDocNo);
            ProcessDtl2(ref ldtl);
            ProcessStockInitial(ref l2, ref l2dtl, StockInitialDocNo, DocNo, ldtl.Count);
            ProcessDOCt(ref l3, ref l3dtl, ref ldtl);

            foreach (var i in l) { cml.Add(SaveGenerateDOCt2Hdr(i)); }
            foreach (var i in ldtl) { cml.Add(SaveGenerateDOCt2Dtl(i)); }

            //Save Stock Initial
            for (int i = 0; i < l2.Count; ++i)
            {
                cml.Add(SaveStockInitialHdr(ref l2, i));
                for (int j = 0; j < l2dtl.Count; ++j)
                {
                    if (l2[i].DocNo == l2dtl[j].DocNo)
                    {
                        cml.Add(SaveStockInitialDtl(ref l2dtl, j));
                    }
                }
                cml.Add(SaveStock(ref l2, i));
                if (mIsAutoJournalActived)
                {
                    if (IsJournalSettingInvalid(ref l2dtl))
                    {
                        cml.Clear();
                        return;
                    }
                    cml.Add(SaveJournal(ref l2, i));
                }
            }

            //Save DO To Customer
            for (int i = 0; i < l3.Count; ++i)
            {
                cml.Add(SaveDOCtHdr(ref l3, i));
                for (int j = 0; j < l3dtl.Count; ++j)
                {
                    if (l3[i].DocNo == l3dtl[j].DocNo)
                    {
                        cml.Add(SaveDOCtDtl(ref l3dtl, j));
                    }
                }
                cml.Add(SaveStock2(ref l3, i));
                if (mIsAutoJournalActived)
                {
                    if (IsJournalSettingInvalid(ref l3dtl))
                    {
                        cml.Clear();
                        return;
                    }
                    cml.Add(SaveJournal2(ref l3, i));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);

            l.Clear(); ldtl.Clear(); l2.Clear(); l2dtl.Clear(); l3.Clear(); l3dtl.Clear();
        }

        private bool IsJournalSettingInvalid(ref List<StockInitialDtl> l)
        {
            if (!mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (mAcNoForInitialStockOpeningBalance.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForInitialStockOpeningBalance is empty.");
                return true;
            }

            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg, ref l)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, ref List<StockInitialDtl> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCtName = string.Empty;
            int i = 0;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B.AcNo Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            foreach (var x in l)
            {
                if (x.ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + i.ToString(), x.ItCode);
                    i++;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid(ref List<DOCtDtl> l)
        {
            if (!mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter

            if (!mIsAcNoForSaleUseItemCategory)
            {
                if (Sm.GetParameter("AcNoForCOGS").Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForCOGS is empty.");
                    return true;
                }
                if (Sm.GetParameter("AcNoForSaleOfFinishedGoods").Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForSaleOfFinishedGoods is empty.");
                    return true;
                }
            }
            if (Sm.GetParameter("CustomerAcNoNonInvoice").Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                return true;
            }


            //Table
            if (IsJournalSettingInvalid_ItemCategory(Msg, ref l)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, ref List<DOCtDtl> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCtName = string.Empty;
            int i = 0;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("And (B.AcNo Is Null ");
            if (mIsAcNoForSaleUseItemCategory)
                SQL.AppendLine(" Or B.AcNo4 Is Null Or B.AcNo5 Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.ItCode In (");
            foreach (var x in l)
            {
                if (x.ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + i.ToString(), x.ItCode);
                    i++;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }


        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueCtCtCode, "Customer's category") ||

                //Sm.IsLueEmpty(LueYr, "Year") ||
                //Sm.IsLueEmpty(LueMth, "Month") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecord() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdValueNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.IsGrdValueEmpty(Grd1, i, 11, true, "Total Bills is zero.")) return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to insert at least 1 customer data.");
                Sm.FocusGrd(Grd1, 0, 1);
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecord()
        {
            if (Grd1.Rows.Count > 999)
            {
                Sm.StdMsg(mMsgType.Warning, "Data exceeds maximum amount of records (1000).");
                Sm.FocusGrd(Grd1, 0, 1);
                return true;
            }

            return false;
        }

        private MySqlCommand SaveGenerateDOCt2Hdr(GenerateDOCt2Hdr l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGenerateDOCt2Hdr ");
            SQL.AppendLine("(DocNo, DocDt, WhsCode, Yr, Mth, ");
            SQL.AppendLine("Remark, CtCtCode, ItCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, '0000', '00', ");
            SQL.AppendLine("@Remark, @CtCtCode, @ItCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", l.DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", l.DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", l.WhsCode);
            Sm.CmParam<String>(ref cm, "@Yr", l.Yr);
            Sm.CmParam<String>(ref cm, "@Mth", l.Mth);
            Sm.CmParam<String>(ref cm, "@Remark", l.Remark);
            Sm.CmParam<String>(ref cm, "@CtCtCode", l.CtCtCode);
            Sm.CmParam<String>(ref cm, "@ItCode", l.ItCode);
            
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveGenerateDOCt2Dtl(GenerateDOCt2Dtl l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGenerateDOCt2Dtl( ");
            SQL.AppendLine("DocNo, DNo, DOCtDocNo, DOCtDNo, CtCode, CtShipAddressDNo,  ");
            SQL.AppendLine("ThisPeriodMeter, LastPeriodMeter, Balance, UPrice, RentPrice, RetributionPrice, ");
            SQL.AppendLine("TotalBills, TaxAmt, TotalAfterTax, ");
            SQL.AppendLine("Object, Target, Bill, Roundup, GrandTotal,  ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values( ");
            SQL.AppendLine("@DocNo, @DNo, @DOCtDocNo, @DOCtDNo, @CtCode, @CtShipAddressDNo,  ");
            SQL.AppendLine("@ThisPeriodMeter, @LastPeriodMeter, @Balance, @UPrice, @RentPrice, @RetributionPrice, ");
            SQL.AppendLine("@TotalBills, @TaxAmt, @TotalAfterTax, ");
            SQL.AppendLine("@Object, @Target, @Bill, @Roundup, @GrandTotal, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", l.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l.DNo);
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", l.DOCtDocNo);
            Sm.CmParam<String>(ref cm, "@DOCtDNo", l.DOCtDNo);
            Sm.CmParam<String>(ref cm, "@CtCode", l.CtCode);
            Sm.CmParam<String>(ref cm, "@CtShipAddressDNo", l.CtShipAddressDNo);
            Sm.CmParam<Decimal>(ref cm, "@ThisPeriodMeter", l.ThisPeriodMeter);
            Sm.CmParam<Decimal>(ref cm, "@LastPeriodMeter", l.LastPeriodMeter);
            Sm.CmParam<Decimal>(ref cm, "@Balance", l.Balance);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", l.UPrice);
            Sm.CmParam<Decimal>(ref cm, "@RentPrice", l.RentPrice);
            Sm.CmParam<Decimal>(ref cm, "@RetributionPrice", l.RetributionPrice);
            Sm.CmParam<Decimal>(ref cm, "@TotalBills", l.TotalBills);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", l.TaxAmt);
            Sm.CmParam<Decimal>(ref cm, "@TotalAfterTax", l.TotalAfterTax);
            Sm.CmParam<String>(ref cm, "@Object", l.Object);
            Sm.CmParam<String>(ref cm, "@Target", l.Target);
            Sm.CmParam<Decimal>(ref cm, "@Bill", l.Bill);
            Sm.CmParam<Decimal>(ref cm, "@Roundup", l.Roundup);
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", l.GrandTotal);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #region Stock Initial

        private MySqlCommand SaveStockInitialHdr(ref List<StockInitialHdr> l, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblStockInitialHdr(DocNo, DocDt, WhsCode, CurCode, ExcRate, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @CurCode, @ExcRate, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", l[Row].DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", l[Row].WhsCode);
            Sm.CmParam<String>(ref cm, "@CurCode", l[Row].CurCode);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", l[Row].ExcRate);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveStockInitialDtl(ref List<StockInitialDtl> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockInitialDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @UPrice, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l[Row].DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", l[Row].ItCode);
            Sm.CmParam<String>(ref cm, "@PropCode", l[Row].PropCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", l[Row].BatchNo);
            Sm.CmParam<String>(ref cm, "@Lot", l[Row].Lot);
            Sm.CmParam<String>(ref cm, "@Bin", l[Row].Bin);
            Sm.CmParam<String>(ref cm, "@Source", l[Row].Source);
            Sm.CmParam<Decimal>(ref cm, "@Qty", l[Row].Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", l[Row].Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", l[Row].Qty3);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", l[Row].UPrice);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(ref List<StockInitialHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.BatchNo, B.Source, A.CurCode, B.UPrice, A.ExcRate, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "04");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(ref List<StockInitialHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblStockInitialHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Stock Initial : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, B.RemarkJournal, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt, Remark As RemarkJournal From (");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select D.AcNo, B.Qty*IfNull(E.MovingAvgPrice, 0.00) As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' And D.AcNo Is Not Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0.00 As DAmt, B.Qty*IfNull(E.MovingAvgPrice, 0.00) As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg E On C.ItCode=E.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForInitialStockOpeningBalance As AcNo, 0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt, B.Remark ");
                SQL.AppendLine("        From TblStockInitialHdr A ");
                SQL.AppendLine("        Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), (Row + 1)));
            Sm.CmParam<String>(ref cm, "@AcNoForInitialStockOpeningBalance", mAcNoForInitialStockOpeningBalance);
            Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmStockInitial' Limit 1;"));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].DocNo);
            return cm;
        }

        #endregion

        #region DO To Customer

        private MySqlCommand SaveDOCtHdr(ref List<DOCtHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCtHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, WhsCode, CtCode, CurCode, ");
            SQL.AppendLine("SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, ");
            SQL.AppendLine("SAPhone, SAFax, SAEmail, SAMobile, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @DocDt, 'A', @WhsCode, @CtCode, @CurCode, ");
            SQL.AppendLine("B.Name, IfNull(B.Address, A.Address), IfNull(B.CityCode, A.CityCode), IfNull(B.CntCode, A.CntCode), IfNull(B.PostalCd, A.PostalCd), ");
            SQL.AppendLine("IfNull(B.Phone, A.Phone), IfNull(B.Fax, A.Fax), IfNull(B.Email, A.Email), IfNull(B.Mobile, A.Mobile), @Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("Inner Join TblCustomerShipAddress B On A.CtCode = B.CtCode ");
            SQL.AppendLine("    And A.CtCode = @CtCode ");
            SQL.AppendLine("    And B.DNo = @CtShipAddressDNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", l[Row].DocDt);
            Sm.CmParam<String>(ref cm, "@WhsCode", l[Row].WhsCode);
            Sm.CmParam<String>(ref cm, "@CtCode", l[Row].CtCode);
            Sm.CmParam<String>(ref cm, "@CtShipAddressDNo", l[Row].CtShipAddressDNo);
            Sm.CmParam<String>(ref cm, "@CurCode", l[Row].CurCode);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDOCtDtl(ref List<DOCtDtl> l, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, 'O', @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @UPrice, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l[Row].DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", l[Row].ItCode);
            Sm.CmParam<String>(ref cm, "@PropCode", l[Row].PropCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", l[Row].BatchNo);
            Sm.CmParam<String>(ref cm, "@Lot", l[Row].Lot);
            Sm.CmParam<String>(ref cm, "@Bin", l[Row].Bin);
            Sm.CmParam<String>(ref cm, "@Source", l[Row].Source);
            Sm.CmParam<Decimal>(ref cm, "@Qty", l[Row].Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", l[Row].Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", l[Row].Qty3);
            Sm.CmParam<Decimal>(ref cm, "@UPrice", l[Row].UPrice);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock2(ref List<DOCtHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A'; ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where (A.Qty<>0) ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "07");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(ref List<DOCtHdr> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('DO To Customer : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A'; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty)) As DAmt, ");
                else
                    SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Group By E.AcNo5 ");
            }
            else
            {
                SQL.AppendLine("        Select D.ParValue As AcNo, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                else
                    SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            if (mIsDOCtAmtRounded)
                SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
            else
                SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCtHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
            if (mIsDOCtAmtRounded)
                SQL.AppendLine("        Sum(Floor(X1.Amt)) As DAmt, ");
            else
                SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
            SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
            SQL.AppendLine("            From TblDOCtHdr A ");
            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ) C On 0=0 ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) X1 ");
            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
            SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
            SQL.AppendLine("        Union All ");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select X.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
                SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
                SQL.AppendLine("            As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X ");
                SQL.AppendLine("        Group By X.AcNo ");
            }
            else
            {
                SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X1.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 1=1 ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X1 ");
                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                SQL.AppendLine("        Group By X2.ParValue ");
            }

            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblDOCtHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmDOCt' limit 1;"));
            Sm.CmParam<String>(ref cm, "@MainCurCode", "IDR");
            Sm.CmParamDt(ref cm, "@DocDt", l[Row].DocDt);
            Sm.CmParam<String>(ref cm, "@CurCode", l[Row].CurCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(l[Row].WhsCode));

            return cm;
        }

        #endregion

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowGenerateDOCt2Hdr(DocNo);
                ShowGenerateDOCt2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowGenerateDOCt2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.WhsCode, A.Yr, A.Mth, A.Remark, A.CtCtCode, A.ItCode, B.ItName ");
            SQL.AppendLine("FROM TblGenerateDOCt2Hdr A ");
            SQL.AppendLine("LEFT JOIN tblitem B ON A.ItCode=B.ItCode ");
            SQL.AppendLine("WHERE DocNo = @DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocDt", "WhsCode", "Yr", "Mth", "Remark",

                    //6-8
                    "CtCtCode", "ItCode", "ItName"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[2]));
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueMth, Sm.DrStr(dr, c[4]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetLue(LueCtCtCode, Sm.DrStr(dr, c[6]));
                    TxtItCode.EditValue = Sm.DrStr(dr, c[7]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[8]);
                }, true
            );
        }

        private void ShowGenerateDOCt2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT B.CtCode, B.CtShipAddressDNo, D.`Name`, IFNULL(D.Address, C.Address) SAAddress, B.ThisPeriodMeter, ");
            SQL.AppendLine("B.LastPeriodMeter, B.Balance, B.UPrice, B.RentPrice, B.RetributionPrice, ");
            SQL.AppendLine("B.TotalBills, B.TaxAmt, B.TotalAfterTax, B.Object, B.Target, B.Bill, B.Roundup, B.GrandTotal, B.DOCtDocNo ");
            SQL.AppendLine("FROM TblGenerateDOCt2Hdr A ");
            SQL.AppendLine("INNER JOIN TblGenerateDOCt2Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND A.DocNo = @DocNo ");
            SQL.AppendLine("INNER JOIN TblCustomer C ON B.CtCode = C.CtCode ");
            SQL.AppendLine("INNER JOIN TblCustomerShipAddress D ON C.CtCode = D.CtCode ");
            SQL.AppendLine("    AND B.CtShipAddressDNo = D.DNo ");
            SQL.AppendLine("ORDER BY B.DNo; ");


            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "CtCode",
                    //1-5
                    "CtShipAddressDNo", "Name", "SAAddress", "ThisPeriodMeter", "LastPeriodMeter", 
                    //6-10
                    "Balance", "UPrice", "RentPrice", "RetributionPrice", "TotalBills", 
                    //11-15
                    "TaxAmt", "TotalAfterTax", "Object", "Target", "Bill", 
                    //16-18
                    "DOCtDocNo", "Roundup", "GrandTotal"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                }, false, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Print Data

        private void ParPrint(string DocNo)
        {
            var SQL = new StringBuilder();
            var l = new List<PrintHdr>(); // print out di group header by ctcode, order by DNo
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
            string[] TableName = { "PrintHdr" };

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode = 'ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("F.SiteName, F.Address CompanyAddress, F.Remark As CompanyPhone, B.CtCode, G.CtName, H.Address As CtShpAddress, H.WaterMeter, ");
            SQL.AppendLine("B.ThisPeriodMeter, B.LastPeriodMeter, B.Balance, Date_Format(A.DocDt, '%d/%b/%Y') CreateDt, I.UserName As CreateBy, B.DNo ");
            SQL.AppendLine("From TblGenerateDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblGenerateDOCt2Dtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode = C.WhsCode ");
            SQL.AppendLine("Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
            SQL.AppendLine("Left Join TblDepartmentBudgetSite E On D.DeptCode = E.DeptCode ");
            SQL.AppendLine("Left Join TblSite F On E.SiteCode = F.SiteCode ");
            SQL.AppendLine("Inner Join TblCustomer G On B.CtCode = G.CtCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress H On G.CtCode = H.CtCode And B.CtShipAddressDNo = H.DNo ");
            SQL.AppendLine("Inner Join TblUser I On A.CreateBy = I.UserCode ");
            SQL.AppendLine("Order By B.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "SiteName",
                    "CompanyAddress",
                    "CompanyPhone",
                    "CtName",
                    
                    //6-10
                    "CtShpAddress",
                    "WaterMeter",
                    "ThisPeriodMeter",
                    "LastPeriodMeter",
                    "Balance",

                    //11-14
                    "CreateDt",
                    "CreateBy",
                    "CtCode",
                    "DNo"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PrintHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            CompanyAddress = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CtName = Sm.DrStr(dr, c[5]),

                            CtShpAddress = Sm.DrStr(dr, c[6]),
                            WaterMeter = Sm.DrStr(dr, c[7]),
                            ThisPeriodMeter = Sm.DrDec(dr, c[8]),
                            LastPeriodMeter = Sm.DrDec(dr, c[9]),
                            Balance = Sm.DrDec(dr, c[10]),

                            CreateDt = Sm.DrStr(dr, c[11]),
                            CreateBy = Sm.DrStr(dr, c[12]),
                            CtCode= Sm.DrStr(dr, c[13]),
                            DNo = Sm.DrStr(dr, c[14]),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            Sm.PrintReport(mFormPrintOutGenerateDOCt2, myLists, TableName, false);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {   
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsCheckCOAJournalNotExists', 'IsMovingAvgEnabled', 'GenerateDOCt2WhsCode', 'GenerateDOCt2ItCode', ");
            SQL.AppendLine("'GenerateDOCt2UPrice', 'GenerateDOCt2RentPrice', 'GenerateDOCt2RetributionPrice', 'AcNoForInitialStockOpeningBalance', 'IsDOCtAmtRounded', ");
            SQL.AppendLine("'IsBatchNoUseDocDtIfEmpty', 'IsAcNoForSaleUseItemCategory', 'FormPrintOutGenerateDOCt2' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsBatchNoUseDocDtIfEmpty": mIsBatchNoUseDocDtIfEmpty = ParValue == "Y"; break;
                            case "IsAcNoForSaleUseItemCategory": mIsAcNoForSaleUseItemCategory = ParValue == "Y"; break;

                            //string
                            case "GenerateDOCt2WhsCode": mGenerateDOCt2WhsCode = ParValue; break;
                            case "GenerateDOCt2ItCode": mGenerateDOCt2ItCode = ParValue; break;
                            case "AcNoForInitialStockOpeningBalance": mAcNoForInitialStockOpeningBalance = ParValue; break;
                            case "FormPrintOutGenerateDOCt2": mFormPrintOutGenerateDOCt2 = ParValue; break;

                            //decimal
                            case "GenerateDOCt2UPrice":
                                if (ParValue.Length == 0)
                                    mGenerateDOCt2UPrice = 1m;
                                else
                                    mGenerateDOCt2UPrice = decimal.Parse(ParValue);
                                break;
                            case "GenerateDOCt2RentPrice":
                                if (ParValue.Length == 0)
                                    mGenerateDOCt2RentPrice = 1m;
                                else
                                    mGenerateDOCt2RentPrice = decimal.Parse(ParValue);
                                break;
                            case "GenerateDOCt2RetributionPrice":
                                if (ParValue.Length == 0)
                                    mGenerateDOCt2RetributionPrice = 1m;
                                else
                                    mGenerateDOCt2RetributionPrice = decimal.Parse(ParValue);
                                break;
                        }
                    }

                    mGenerateDOCt2UPriceIsNull = (Sm.GetParameter("GenerateDOCt2UPrice").Length == 0) ? true : false;
                    mGenerateDOCt2RentPriceIsNull = (Sm.GetParameter("GenerateDOCt2RentPrice").Length == 0) ? true : false;
                    mGenerateDOCt2RetributionPriceIsNull = (Sm.GetParameter("GenerateDOCt2RetributionPrice").Length == 0) ? true : false;
                }
                dr.Close();
            }
            if (mGenerateDOCt2ItCode.Length > 0)
            {
                mInventoryUomCodeConvert12 = Decimal.Parse(Sm.GetValue("Select IfNull(InventoryUomCodeConvert12, 1) From TblItem Where ItCode=@Param ", mGenerateDOCt2ItCode));
                mInventoryUomCodeConvert13 = Decimal.Parse(Sm.GetValue("Select IfNull(InventoryUomCodeConvert13, 1) From TblItem Where ItCode=@Param ", mGenerateDOCt2ItCode));
            }
        }

        private void ComputeDetail(int Row)
        {
            decimal mThisPeriodMeter = 0m, mLastPeriodMeter = 0m, mBalance = 0m, mTotalBills = 0m, mRoundup=0m, mGrandTotal = 0m, 
                mPPN = 0m, mTotalAfterTax = 0m,
                mUPrice = 0m, mRentPrice = 0m, mRetributionPrice = 0m
                ;
            
            mThisPeriodMeter = Sm.GetGrdDec(Grd1, Row, 5);
            mLastPeriodMeter = Sm.GetGrdDec(Grd1, Row, 6);
            mRoundup = Sm.GetGrdDec(Grd1, Row, 18);
            mUPrice = Sm.GetGrdDec(Grd1, Row, 8);
            mRentPrice = Sm.GetGrdDec(Grd1, Row, 9);
            mRetributionPrice = Sm.GetGrdDec(Grd1, Row, 10);

            mBalance = mThisPeriodMeter - mLastPeriodMeter;
            //mTotalBills = (mBalance * mGenerateDOCt2UPrice) + mGenerateDOCt2RentPrice + mGenerateDOCt2RetributionPrice;
            mTotalBills = (mBalance * mUPrice) + mRentPrice + mRetributionPrice;
            mPPN = mTotalBills * 0.1m;
            mTotalAfterTax = mTotalBills + mPPN;
            mGrandTotal = mRoundup + mTotalBills ;

            Grd1.Cells[Row, 7].Value = Sm.FormatNum(mBalance, 0);
            Grd1.Cells[Row, 11].Value = Sm.FormatNum(mTotalBills, 0);
            Grd1.Cells[Row, 12].Value = Sm.FormatNum(mPPN, 0);
            Grd1.Cells[Row, 13].Value = Sm.FormatNum(mTotalAfterTax, 0);
            Grd1.Cells[Row, 16].Value = Sm.FormatNum(mTotalAfterTax, 0);
            Grd1.Cells[Row, 19].Value = Sm.FormatNum(mGrandTotal, 0);
        }

        private void ProcessHdr(ref List<GenerateDOCt2Hdr> l, string DocNo)
        {
            l.Add(new GenerateDOCt2Hdr()
            {
                DocNo = DocNo,
                DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                WhsCode = Sm.GetLue(LueWhsCode),
                Yr = Sm.GetLue(LueYr),
                Mth = Sm.GetLue(LueMth),
                Remark = MeeRemark.Text,
                CtCtCode = Sm.GetLue(LueCtCtCode),
                ItCode = TxtItCode.Text
            });
        }

        private void ProcessDtl(ref List<GenerateDOCt2Dtl> l, string DocNo, string StockInitialDocNo)
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                l.Add(new GenerateDOCt2Dtl()
                {
                    DocNo = DocNo,
                    DNo = Sm.Right(string.Concat("000", (i + 1)), 3),
                    DOCtDocNo = "1",
                    DOCtDNo = "1",
                    StockInitialDocNo = StockInitialDocNo,
                    CtCode = Sm.GetGrdStr(Grd1, i, 1),
                    CtShipAddressDNo = Sm.GetGrdStr(Grd1, i, 2),
                    ThisPeriodMeter = Sm.GetGrdDec(Grd1, i, 5),
                    LastPeriodMeter = Sm.GetGrdDec(Grd1, i, 6),
                    Balance = Sm.GetGrdDec(Grd1, i, 7),
                    UPrice = Sm.GetGrdDec(Grd1, i, 8),
                    RentPrice = Sm.GetGrdDec(Grd1, i, 9),
                    RetributionPrice = Sm.GetGrdDec(Grd1, i, 10),
                    TotalBills = Sm.GetGrdDec(Grd1, i, 11),
                    TaxAmt = Sm.GetGrdDec(Grd1, i, 12),
                    TotalAfterTax = Sm.GetGrdDec(Grd1, i, 13),
                    Object = Sm.GetGrdStr(Grd1, i, 14),
                    Target = Sm.GetGrdStr(Grd1, i, 15),
                    Bill = Sm.GetGrdDec(Grd1, i, 16),
                    Roundup = Sm.GetGrdDec(Grd1, i, 18),
                    GrandTotal = Sm.GetGrdDec(Grd1, i, 19),
                });
            }
        }

        private void ProcessDtl2(ref List<GenerateDOCt2Dtl> l)
        {
            string
                mDOCtDocNo = string.Empty,
                mDOCtDNo = string.Empty,
                mCtCode = string.Empty,
                mCtShipAddressDNo = string.Empty;

            int mDocNoIndex = 0, mDNoIndex = 1;

            foreach (var x in l.OrderBy(o => o.CtCode).ThenBy(p => p.CtShipAddressDNo))
            {
                if (mCtCode == x.CtCode && mCtShipAddressDNo == x.CtShipAddressDNo)
                {
                    mDNoIndex += 1;
                }
                else
                {
                    mCtCode = x.CtCode;
                    mCtShipAddressDNo = x.CtShipAddressDNo;
                    mDocNoIndex += 1;
                    mDNoIndex = 1;

                    mDOCtDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr", mDocNoIndex.ToString());
                }

                mDOCtDNo = Sm.Right(string.Concat("000", mDNoIndex.ToString()), 3);

                x.DOCtDocNo = mDOCtDocNo;
                x.DOCtDNo = mDOCtDNo;
            }
        }

        private void ProcessStockInitial(ref List<StockInitialHdr> l2, ref List<StockInitialDtl> l2dtl, string StockInitialDocNo, string DocNo, int Count)
        {
            l2.Add(new StockInitialHdr()
            {
                DocNo = StockInitialDocNo,
                DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                WhsCode = Sm.GetLue(LueWhsCode),
                CurCode = "IDR",
                ExcRate = 1m,
                Remark = string.Concat(MeeRemark.Text, " [Auto created by #", DocNo, "]")
            });

            l2dtl.Add(new StockInitialDtl()
            {
                DocNo = StockInitialDocNo,
                DNo = "001",
                ItCode = mGenerateDOCt2ItCode,
                PropCode = "-",
                BatchNo = mIsBatchNoUseDocDtIfEmpty ? Sm.Left(Sm.GetDte(DteDocDt), 8) : "-",
                Source = string.Concat("04*", StockInitialDocNo, "*001"),
                Lot = "-",
                Bin = "-",
                Qty = 1m * Count,
                Qty2 = 1m * Count * mInventoryUomCodeConvert12,
                Qty3 = 1m * Count * mInventoryUomCodeConvert13,
                UPrice = 0m,
                Remark = string.Concat("Auto created by #", DocNo)
            });
        }

        private void ProcessDOCt(ref List<DOCtHdr> l3, ref List<DOCtDtl> l3dtl, ref List<GenerateDOCt2Dtl> ldtl)
        {
            string mDocNo = string.Empty;

            foreach (var x in ldtl.OrderBy(o => o.DOCtDocNo).ThenBy(p => p.DOCtDNo))
            {
                l3dtl.Add(new DOCtDtl()
                {
                    DocNo = x.DOCtDocNo,
                    DNo = x.DOCtDNo,
                    ItCode = mGenerateDOCt2ItCode,
                    PropCode = "-",
                    BatchNo = mIsBatchNoUseDocDtIfEmpty ? Sm.Left(Sm.GetDte(DteDocDt), 8) : "-",
                    Source = string.Concat("04*", x.StockInitialDocNo, "*001"),
                    Lot = "-",
                    Bin = "-",
                    Qty = 1m,
                    Qty2 = 1m * mInventoryUomCodeConvert12,
                    Qty3 = 1m * mInventoryUomCodeConvert13,
                    UPrice = x.GrandTotal,
                    Remark = string.Concat("Auto created by #", x.DocNo)
                });

                if (x.DOCtDocNo != mDocNo)
                {
                    l3.Add(new DOCtHdr()
                    {
                        DocNo = x.DOCtDocNo,
                        DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                        WhsCode = Sm.GetLue(LueWhsCode),
                        CtCode = x.CtCode,
                        CtShipAddressDNo = x.CtShipAddressDNo,
                        CurCode = "IDR",
                        Remark = string.Concat(MeeRemark.Text, " [Auto created by #", x.DocNo, "]")
                    });

                    mDocNo = x.DOCtDocNo;
                }
            }
        }

        private void SetLueWhsCode(ref DXE.LookUpEdit Lue, string WhsCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WhsCode Col1, WhsName Col2 ");
            SQL.AppendLine("From TblWarehouse ");
            if (WhsCode.Length>0)
                SQL.AppendLine("Where Find_In_Set(WhsCode, @Param) ");
            else
                SQL.AppendLine("Where 1=0 ");
            SQL.AppendLine("Order By WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Param", WhsCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(SetLueWhsCode), mGenerateDOCt2WhsCode);
            }
        }
        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
            Sm.ClearGrd(Grd1, true);
            
        }

        #endregion

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if (BtnItCode.Enabled)
            {
                Sm.FormShowDialog(new FrmGenerateDOCt2Dlg2 (this));
            }
        }
        #endregion

        #region Class

        private class GenerateDOCt2Hdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string Remark { get; set; }
            public string CtCtCode { get; set; }
            public string ItCode { get; set; }
        }

        private class GenerateDOCt2Dtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DOCtDocNo { get; set; }
            public string DOCtDNo { get; set; }
            public string StockInitialDocNo { get; set; }
            public string CtCode { get; set; }
            public string CtShipAddressDNo { get; set; }
            public decimal ThisPeriodMeter { get; set; }
            public decimal LastPeriodMeter { get; set; }
            public decimal Balance { get; set; }
            public decimal UPrice { get; set; }
            public decimal RentPrice { get; set; }
            public decimal RetributionPrice { get; set; }
            public decimal TotalBills { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal TotalAfterTax { get; set; }
            public string Object { get; set; }
            public string Target { get; set; }
            public decimal Bill { get; set; }
            public decimal Roundup { get; set; }
            public decimal GrandTotal { get; set; }
        }

        private class StockInitialHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string Remark { get; set; }
        }

        private class StockInitialDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal UPrice { get; set; }
            public string Remark { get; set; }
        }

        public class DOCtHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string CtCode { get; set; }
            public string CtShipAddressDNo { get; set; }
            public string CurCode { get; set; }
            public string Remark { get; set; }
        }

        public class DOCtDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public decimal UPrice { get; set; }
            public string Remark { get; set; }
        }

        private class PrintHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string SiteName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CtCode { get; set; }
            public string CtName { get; set; }
            public string CtShpAddress { get; set; }
            public string WaterMeter { get; set; }
            public decimal ThisPeriodMeter { get; set; }
            public decimal LastPeriodMeter { get; set; }
            public decimal Balance { get; set; }
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
            public string DNo { get;set; }
        }

        #endregion
    }
}
