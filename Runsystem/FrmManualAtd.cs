﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmManualAtd : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "", mSQL = "";
        internal FrmManualAtdFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmManualAtd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Manual Attendance";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
            Sl.SetLueDeptCode(ref LueDeptCode);
            Sl.SetLueAGCode(ref LueAG);

            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = BtnRefresh.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "Employee",
                        "",
                        "Department",
                        "Position",
                        //6-10
                        "Group Attendance",
                        "IN",
                        "OUT",
                        "IN Actual",
                        "OUT Actual",
                        //11-15
                        "Rest Time"+Environment.NewLine+"IN",
                        "Rest Time"+Environment.NewLine+"Out",
                        "Rest Time IN"+Environment.NewLine+"Actual",
                        "Rest Time OUT"+Environment.NewLine+"Actual",
                        "Over Time"+Environment.NewLine+"IN",
                        //16-18
                        "Over Time"+Environment.NewLine+"Out",
                        "Over Time IN"+Environment.NewLine+"Actual",
                        "Over Time OUT"+Environment.NewLine+"Actual",
                    },
                     new int[] 
                    {
                        20, 
                        80, 200, 20, 150, 150,   
                        200, 80, 80, 80, 80,  
                        80, 80, 80, 80, 80,  
                        80, 80, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 3 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 10, 13, 14, 17, 18 });
            Sm.GrdColReadOnly(Grd1, new int[] { 6, 9, 10, 13, 14, 17, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 3, 11, 12, 13, 14, 15, 16, 17, 18 }, false);
            #endregion

        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, LueDeptCode, LueAG, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
                    BtnRefresh.Visible = false;
                    DteDocDt.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt, LueDeptCode, LueAG, MeeRemark
                    }, false);
                    BtnRefresh.Visible = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 7, 8 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    //Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9 });
                    BtnRefresh.Visible = true;
                    ChkCancelInd.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueDeptCode, LueAG, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, E.AGName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblAttendanceGrpDtl D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join TblAttendanceGrpHdr E On D.AGCode=E.AGCode ");

            mSQL = SQL.ToString();
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmManualAtdFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            string CurrentDateTime = Sm.ServerCurrentDateTime();
            
            Sm.SetDteCurrentDate(DteDocDt);
            SetSQL();           
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    UpdateData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

     
        #endregion

        #region Grid Method

        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(
                    new FrmManualAtdDlg(
                        this, Sm.GetLue(LueDeptCode))
                    );

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueDeptCode, "Department"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmManualAtdDlg(
                        this, Sm.GetLue(LueDeptCode)));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }           
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 7)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length == 4)
                {
                    Sm.FocusGrd(Grd1, e.RowIndex, 8);
                    SetTime(Grd1, e.RowIndex, 9, 7);
                }
            }

            if (e.ColIndex == 8)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length == 4)
                {
                    Sm.FocusGrd(Grd1, (e.RowIndex + 1), 7);
                    SetTime(Grd1, e.RowIndex, 10, 8);
                }
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = "";
            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ManualATD", "TblManualAtdHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveManualAtdHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                    cml.Add(SaveManualAtdDtl(DocNo, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date") ||
                IsGrdEmpty() ||
                IsGrdTimeNotValid()
                ;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input attendance.");
                return true;
            }
            return false;
        }

        private bool IsGrdTimeNotValid()
        {
            string Msg = "";

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                {
                    string InA;
                    string OutA;

                    Msg =
                       "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                       "Employee : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine;

                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                    {
                        InA = Sm.GetGrdStr(Grd1, Row, 7);
                        if (Decimal.Parse(InA) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Time In not Valid");
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                    {
                        InA = Sm.GetGrdStr(Grd1, Row, 11);
                        if (Decimal.Parse(InA) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Rest Time In not Valid");
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 13).Length != 0)
                    {
                        InA = Sm.GetGrdStr(Grd1, Row, 13);
                        if (Decimal.Parse(InA) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Over Time In not Valid");
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                    {
                        OutA = Sm.GetGrdStr(Grd1, Row, 8);
                        if (Decimal.Parse(OutA) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Time Out not Valid");
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 12).Length != 0)
                    {
                        OutA = Sm.GetGrdStr(Grd1, Row, 12);
                        if (Decimal.Parse(OutA) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Rest Time Out not Valid");
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 16).Length != 0)
                    {
                        OutA = Sm.GetGrdStr(Grd1, Row, 11);
                        if (Decimal.Parse(OutA) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Over Time Out not Valid");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveManualAtdHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblManualAtdHdr(DocNo, DocDt, CancelInd, DeptCode, AgCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, @DeptCode, @AgCode, @Remark, @UserCode, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@AgCode", Sm.GetLue(LueAG));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveManualAtdDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblManualAtdDtl(DocNo, EmpCode, InOut1, InOut2, InOut3, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @EmpCode, @InOut1, @InOut2, @InOut3, @UserCode, CurrentDateTime()) ");
            //SQL.AppendLine("On Duplicate key ");
            //SQL.AppendLine("  Update EmpCode=@EmpCode, Dt=@Dt, InOutL1=@InOutL1, InOutA1=@InOutA1, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            //SQL.AppendLine("  Update TblAttendanceLog Set ProcessInd = 'Y' Where EmpCode=@EmpCode And Dt=@Dt; ");  

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };


            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            //Sm.CmParam<String>(ref cm, "@InOut1", 
            Sm.CmParam<String>(ref cm, "@InOut1", String.Concat((Sm.GetGrdStr(Grd1, Row, 7)), Sm.GetGrdStr(Grd1, Row, 8)));
            Sm.CmParam<String>(ref cm, "@InOut2", String.Concat((Sm.GetGrdStr(Grd1, Row, 11)), Sm.GetGrdStr(Grd1, Row, 12)));
            Sm.CmParam<String>(ref cm, "@InOut3", String.Concat((Sm.GetGrdStr(Grd1, Row, 15)), Sm.GetGrdStr(Grd1, Row, 16)));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Update data
        private void UpdateData()
        {
            if (IsInsertedDataNotValid2() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = TxtDocNo.Text;

            var cml2 = new List<MySqlCommand>();

            cml2.Add(CancelManualAtdHdr(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
            //        cml2.Add(SaveAtdDtl(DocNo, Row));
            //}


            Sm.ExecCommands(cml2);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid2()
        {
            return
                IsGrdEmpty2() ||
                IsDocAlreadyCancel();
        }

        private bool IsDocAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblManualAtdHdr Where DocNo=@DocNo And CancelInd = 'Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "this document already cancelled .");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty2()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data.");
                return true;
            }
            return false;
        }


        private MySqlCommand CancelManualAtdHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblManualAtdHdr Set CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowManualAtdHdr(DocNo);
                ShowManualAtdDtl(DocNo);
            }

            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowManualAtdHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.CancelInd, " +
                    "A.DeptCode, A.AgCode, A.remark From TblManualAtdHdr A Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "DeptCode", "AgCode", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueAG, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    }, true
                );
        }

        public void ShowManualAtdDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.EmpCode, B.EmpName, C.DeptName, D.PosName, F.AgName, ");
            SQL.AppendLine("Left(InOut1, 4) As JIn, Right(InOut1, 4) As JOut, ");
            SQL.AppendLine("Left(InOut2, 4) As RIn, Right(InOut2, 4) As ROut, ");
            SQL.AppendLine("Left(InOut3, 4) As OIn, Right(InOut3, 4) As OOut ");
            SQL.AppendLine("From TblManualAtdDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("left Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("Left Join TblAttendanceGrpDtl E On B.EmpCode=E.EmpCode ");
            SQL.AppendLine("Left Join TblAttendanceGrpHdr F On E.AGCode=F.AGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",
                    //1-5
                    "EmpName", "DeptName", "PosName", "AGName", "JIn",  
                    //6-10
                    "JOut","RIn", "ROut", "OIn", "OOut",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 13, 7);
                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 14, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 17, 9);
                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 18, 10);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        //public void ShowEmpList(string DocNo, string MDeptCode, string MGrpCode)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName ");
        //    SQL.AppendLine("From TblEmployee A");
        //    SQL.AppendLine("Left Join TblPosition B on A.PosCode = B.PosCode");
        //    SQL.AppendLine("Left Join TblDepartment C on A.DeptCode = C.DeptCode");
        //    SQL.AppendLine("Where A.DeptCode = '" + MDeptCode + "' ");

        //    var cm = new MySqlCommand();
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

        //    Sm.ShowDataInGrid(
        //        ref Grd1, ref cm, SQL.ToString(),
        //        new string[] 
        //        { 
        //            //0
        //            "EmpCode",
        //            //1-5
        //            "EmpName", "DeptName", "PosName", "JIn", "JOut", 
        //            //6-9
        //            "RIn", "ROut", "OIn", "OOut",
        //        },
        //        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
        //        {
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
        //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
        //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 6, 4);
        //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 7, 5);
        //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 8, 6);
        //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 9, 7);
        //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 10, 8);
        //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 11, 9);
        //        }, false, false, true, false
        //        );
        //    Sm.FocusGrd(Grd1, 0, 1);
        //}

        #endregion

        #region Additional Method

        private void ShowEmployee()
        {

            string Filter = " Where 0=0 ";

            var cm = new MySqlCommand();

            Filter += " And A.ResignDt Is Null ";

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAG), "E.AGCode", true);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By C.DeptName, E.AGName, A.EmpName;",
                    new string[]
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "AGName"
                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    }, true, false, false, false
                );
            Grd1.Rows.Add();
        }


        internal string GetSelectedEmpCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        public void FormatGrid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 9).Length == 0)
                {
                    Grd1.Cells[Row, 9].Value = "00:00";
                }
                if (Sm.GetGrdStr(Grd1, Row, 10).Length == 0)
                {
                    Grd1.Cells[Row, 10].Value = "00:00";
                }
                if (Sm.GetGrdStr(Grd1, Row, 13).Length == 0)
                {
                    Grd1.Cells[Row, 13].Value = "00:00";
                }
                if (Sm.GetGrdStr(Grd1, Row, 14).Length == 0)
                {
                    Grd1.Cells[Row, 14].Value = "00:00";
                }
                if (Sm.GetGrdStr(Grd1, Row, 17).Length == 0)
                {
                    Grd1.Cells[Row, 17].Value = "00:00";
                }
                if (Sm.GetGrdStr(Grd1, Row, 18).Length == 0)
                {
                    Grd1.Cells[Row, 18].Value = "00:00";
                }
            }
        }

        public void SetTime(iGrid GrdXXX, int RowXXX, int ColActual, int ColEdit)
        {
            for (int Row = 0; Row < GrdXXX.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 4 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit))<2359)
                {
                    GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat(Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                }
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueAG_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAG, new Sm.RefreshLue1(Sl.SetLueAGCode));
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowEmployee();
        }

        #endregion    
    }
}
