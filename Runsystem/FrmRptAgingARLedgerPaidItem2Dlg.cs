﻿#region Update
/*
 *  25/08/2021 [DITA/KIM] New Application
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARLedgerPaidItem2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptAgingARLedgerPaidItem2 mFrmParent;
        private string mSQL = string.Empty, mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;


        #endregion

        #region Constructor

        public FrmRptAgingARLedgerPaidItem2Dlg(FrmRptAgingARLedgerPaidItem2 FrmParent, string DocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List of DO To Customer#";

            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-4
                    "Document#",
                    "Date",
                    "",
                    "Amount"

                },
                 new int[] 
                {
                    //0
                    25,

                    //1-5
                    150, 100, 20, 150
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 4 });
        }


        override protected void HideInfoInGrd()
        {
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T2.DocNo, T4.DocDt, ifnull(T2.Qty * T2.UPrice, 0.00) Amt ");
            SQL.AppendLine("From TblSalesInvoiceDtl T1 ");
            SQL.AppendLine("Inner Join TblDOCtDtl T2 On T1.DOCtDocNo = T2.DocNo ");
            SQL.AppendLine("   And T1.DOCtDNo = T2.DNo ");
            SQL.AppendLine("   And T1.DocType = '3' ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr T3 On T1.DocNo = T3.DocNo ");
            SQL.AppendLine("   And T3.CancelInd = 'N' ");
            SQL.AppendLine("   And T3.Status = 'A' ");
            SQL.AppendLine("Inner Join TblDOCtHdr T4 On T2.DocNo = T4.DocNo ");
            SQL.AppendLine("where find_in_set(T3.DocNo, '" + mDocNo + "' ) != 0 ");
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ", DocNo = string.Empty;
                var cm = new MySqlCommand();


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order by T2.DocNo;",
                        new string[] 
                        { 
                             "DocNo",
                             
                             "DocDt", "Amt"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

      

        #endregion 

    }
}
