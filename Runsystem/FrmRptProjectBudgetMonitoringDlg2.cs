﻿#region Update
/*
    06/12/2020 [DITA/IMS] New apps
    07/12/2020 [DITA/IMS] tampilan detail recvvd termasuk qty dan amount nya
    09/04/2021 [WED/IMS] bug query
    10/08/2021 [TKG/IMS] ubah query karena price tidak benar
    10/08/2021 [VIN/IMS] tambah mIsMovingAvgEnabled
    01/11/2021 [DITA/IMS] Price di receiving tidak menggunakan MAP
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectBudgetMonitoringDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptProjectBudgetMonitoring mFrmParent;
        internal string mRecvVdDocNo = string.Empty, mSelectedCOA = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectBudgetMonitoringDlg2(FrmRptProjectBudgetMonitoring FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "Receiving Vendor#",
                        "",
                        "POInd",
                        "PO#",
                        "Local Code",

                        //6-9
                        "Item Name",
                        "Quantity",
                        "Price",
                        "Total"
                    },
                   new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 20,0, 170, 130,

                        //6-9
                        250, 100, 130, 130
                    }
              );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] {7, 8, 9}, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);

            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.POInd, B.PODocNo, C.ItCodeInternal, C.ItName, B.Qty, ");
            #region ims tidak menggunakan MAP // dita 01/11/2021
                /*if (mFrmParent.mIsMovingAvgEnabled)
                {
                    SQL.AppendLine("Case When D.MovingAvgInd='Y' Then IfNull(I.MovingAvgPrice, 0.00) Else E.UPrice*E.ExcRate  End As UPrice,  ");
                    SQL.AppendLine("Case When D.MovingAvgInd='Y' Then (B.Qty*I.MovingAvgPrice) Else (B.Qty*E.UPrice*E.ExcRate) End As Total ");
                }*/
                #endregion

            SQL.AppendLine("E.UPrice*E.ExcRate As UPrice, (B.Qty*E.UPrice*E.ExcRate) As Total ");
           
            SQL.AppendLine("From TblRecvVdHdr A  ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B ON A.DocNo=B.DocNo And B.CancelInd='N' And B.Status='A' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo2 Is Not null And Find_In_Set(D.AcNo2, @SelectedCOA) ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source ");
            SQL.AppendLine("Inner Join TblPODtl F On B.PODocNo=F.DocNo And B.PODNo=F.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl G On F.PORequestDocNo=G.DocNo And F.PORequestDNo=G.DNo  ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr H ON G.MaterialRequestDocNo=H.DocNo And H.SOCDocNo Is Not Null And H.SOCDocNo=@SOContractDocNo ");
            SQL.AppendLine("Left Join TblItemMovingAvg I On C.ItCode=I.ItCode ");
            
            SQL.AppendLine("Where Find_In_Set(A.DocNo, @RecvVdDocNo); ");
            
            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = string.Empty;

                Sm.CmParam<string>(ref cm, "@RecvVdDocNo", mRecvVdDocNo);
                Sm.CmParam<string>(ref cm, "@SelectedCOA", mSelectedCOA);
                Sm.CmParam<string>(ref cm, "@SOContractDocNo", mFrmParent.TxtSOContractDocNo.Text);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL() + Filter,
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            //1-5
                            "POInd", "PODocNo", "ItCodeInternal", "ItName", "Qty",

                            //6-7
                            "UPrice", "Total"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] {7,8,9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Grid Control Events

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3) == "Y")
                {
                    var f = new FrmRecvVd(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmRecvVd2(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
               
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3) == "Y")
                {
                    var f = new FrmRecvVd(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmRecvVd2(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }

            }
        }

        #endregion

        #endregion

    }
}


