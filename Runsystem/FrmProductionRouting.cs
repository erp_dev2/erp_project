﻿#region Update
/*
    16/03/2022 [TKG/GSS] merubah proses save
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionRouting : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo= string.Empty;
        internal FrmProductionRoutingFind FrmFind;

        #endregion

        #region Constructor

        public FrmProductionRouting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Production Routing";
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            SetGrd();

            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Work Center"+Environment.NewLine+"Document Number",
                        "",
                        "Work Center"+Environment.NewLine+"Document Name",
                        "Location",

                        //6-8
                        "Capacity",
                        "UoM",
                        "Sequence"
                    },
                     new int[] 
                    {
                        100,
                        21,150,21,250,100,
                        100,100,100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 5, 6, 7}, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }  
 
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo,DteDocDt,TxtDocName,MeeRemark,ChkActInd, TxtSource
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnProductionRouting.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(TxtDocNo, true);
                    Sm.SetControlReadOnly(ChkActInd, true);
                    ChkActInd.Checked = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt,TxtDocName,MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnProductionRouting.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(ChkActInd, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo,DteDocDt,TxtDocName,MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo,DteDocDt,TxtDocName, MeeRemark, TxtSource, ChkActInd
            });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8 });
        }
        
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProductionRoutingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (IsDataNotValid()) return;
                    Cursor.Current = Cursors.WaitCursor;

                    string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProductionRouting", "TblProductionRoutingHdr");

                    var cml = new List<MySqlCommand>();

                    cml.Add(SaveProductionRoutingHdr(DocNo));
                     if (Grd1.Rows.Count > 1)
                     {
                        cml.Add(SaveProductionRoutingDtl(DocNo));
                        //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        //     if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveProductionRoutingDtl(DocNo, Row));
                     }
                    Sm.ExecCommands(cml);
                    
                    ShowData(DocNo);
                }
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmProductionRoutingDlg(this));
            }
            
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (BtnSave.Enabled &&
                TxtDocNo.Text.Length == 0 &&
                !IsWorkCenterEmpty(e) &&
                Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 1) Sm.FormShowDialog(new FrmProductionRoutingDlg(this));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmWorkCenter("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelProductionRoutingHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document number", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            string a = ChkActInd.Checked ? "Y" : "N";
            var SQL = new StringBuilder();
            if (a == "N")
            {
                SQL.AppendLine("Select DocNo From TblProductionRoutingHdr ");
                SQL.AppendLine("Where ActiveInd='N' And DocNo=@DocNo ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data is no actived already.");
                    return true;
                }

            }
            else if (a == "Y")
            {
                SQL.AppendLine("Select DocNo From TblProductionRoutingHdr ");
                SQL.AppendLine("Where ActiveInd='Y' And DocNo=@DocNo ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data is actived already.");
                    return true;
                }

            }

            return false;
        }

        private MySqlCommand CancelProductionRoutingHdr() 
        {   string a=ChkActInd.Checked ? "Y" : "N";
            var SQL = new StringBuilder();
            if (a == "N")
            {
                SQL.AppendLine("Update TblProductionRoutingHdr Set ");
                SQL.AppendLine("    ActiveInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And ActiveInd='Y' ");
            }
            else if (a == "Y")
            {
                SQL.AppendLine("Update TblProductionRoutingHdr Set ");
                SQL.AppendLine("    ActiveInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And ActiveInd='N' ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowProductionRoutingHdr(DocNo);
                ShowProductionRoutingDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        public void ShowData2(string DocNo)
        {
            try
            {
                Sm.ClearGrd(Grd1, true);
                ShowProductionRoutingDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowProductionRoutingHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                         ref cm,
                          "Select DocNo,DocDt,DocName,ActiveInd,Remark "+
                          "From tblProductionRoutinghdr " + 
                          "Where DocNo = @DocNo ",
                         new string[] 
                           {
                              //0
                              "DocNo",

                              //1-4
                              "DocDt","DocName","ActiveInd","Remark"
                           },
                         (MySqlDataReader dr, int[] c) =>
                         {
                             TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                             Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                             TxtDocName.EditValue = Sm.DrStr(dr, c[2]);
                             ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                             MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                         }, true
                     );
        }

        private void ShowProductionRoutingDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkCenterDocNo, B.DocName, B.Location, B.Capacity, C.UomName, A.Sequence ");
            SQL.AppendLine("From TblProductionRoutingDtl A ");
            SQL.AppendLine("Left Join TblWorkCenterHdr B On A.WorkCenterDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblUom C On B.UomCode=C.UomCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.Sequence");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "DNo",

                           //1-5
                           "WorkCenterDocNo","DocName","Location","Capacity","UomName",

                           //6
                           "Sequence"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }
      
        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date")||
                Sm.IsTxtEmpty(TxtDocName, "Document Name", false)||
                IsGrd1Empty()||
                IsGrd1ValueNotValid() 
                ;
        }
        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in production routing list.");
                return true;
            }
            return false;
        }

        private bool IsGrd1ValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Work center Name is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 8, true, "Sequance is empty.")) return true;

                }
            }
            return false;
        }

        private MySqlCommand SaveProductionRoutingHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into tblProductionRoutinghdr(DocNo,DocDt,DocName,ActiveInd,Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo,@DocDt,@DocName,@ActiveInd,@Remark, @UserCode, CurrentDateTime()) "

            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionRoutingDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Production Routing (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblProductionRoutingDtl(DocNo,DNo,WorkCenterDocNo, Sequence, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @WorkCenterDocNo_" + r.ToString() +
                        ", @Sequence_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@WorkCenterDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@Sequence_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveProductionRoutingDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblProductionRoutingDtl(DocNo,DNo,WorkCenterDocNo, Sequence, CreateBy, CreateDt) " +
        //            "Values (@DocNo,@DNo,@WorkCenterDocNo,@Sequence, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd1, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}
       
        #endregion

        #region Additional Method

        private bool IsWorkCenterEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnProductionRouting_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmProductionRoutingDlg2(this));
        }

        private void TxtDocName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocName);
        }

        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(MeeRemark);
        }

        #endregion

        private void TxtSource_EditValueChanged(object sender, EventArgs e)
        {

        }

     

        #endregion        
    }
}
