﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptLabour : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptLabour(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.WorkCenterDocNo, D.DocName, C.ProductionShiftCode, ");
            SQL.AppendLine("A.CurCode, SUM(B.Wages*B.Value) As Amount, A.Remark ");
            SQL.AppendLine("From TblProductionWagesHdr A ");
            SQL.AppendLine("Inner Join tblproductionwagesdtl2 B On A.DOcNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblShopFloorcontrolHdr C On A.ShopfloorControlDocNo = C.DocNo  ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr D On C.WorkCenterDocNo = D.DocNo");
            SQL.AppendLine("Where A.cancelInd = 'N' And C.CancelInd = 'N' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Production"+Environment.NewLine+"Wages", 
                        "",
                        "Date",
                        "Workcenter"+Environment.NewLine+"Code",
                        "",
                        
                        //6-10
                        "Workcenter",
                        "Shift",
                        "Currency",
                        "Amount",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 20, 100, 150, 20, 
                        
                        //6-10
                        200, 80, 80, 100, 250,
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5});
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtWCDocNo.Text, new string[] { "C.WorkcenterDocNo", "D.DocName" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By A.DocNo, A.DocDt, C.WorkCenterDocNo, D.DocName, C.ProductionShiftCode, A.Remark, A.CurCode ",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "WorkcenterDocNo", "Docname", "ProductionShiftCode", "CurCode",

                            //6-7
                            "Amount", "Remark"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9 });
        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPWG(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPWG(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }         
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void ChkWCDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Workcenter");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtWCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
