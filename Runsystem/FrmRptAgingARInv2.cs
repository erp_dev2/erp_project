﻿#region Update
/*
    07/02/2022 [SET/PRODUCT] Reporting baru
    10/02/2022 [WED/PHT] penyesuaian ke PHT
    16/02/2022 [SET] Feedback
    22/02/2022 [RDA/PHT] Bug : kolom ketika param AgingARDaysFormat bernilai "2" masih belum read-only
    07/06/2022 [SET/PHT] Feedback - penyesuain source data dari multi profit center 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARInv2 : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private string  mSQL = string.Empty, mDocTitle = string.Empty,
            mRptAgingARInvDlgFormat = string.Empty,
            mAgingARDaysFormat = string.Empty,
            mQueryProfitCenter = string.Empty,
            mQueryProfitCenter2 = string.Empty
            ;
        private bool 
            mMInd = false, 
            mIsRptAgingARInvShowDOCt = false,
            mIsRptAgingInvoiceARShowFulfilledDocument = false,
            mIsFilterByCtCt = false,
            mIsFilterByDept = false,
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsFilterByProfitCenter = false,
            mIsAllProfitCenterSelected = false;
        internal bool mIsUseMInd = false, mIsRptAgingARInvUseDocDtFilter = false, mIsRptShowVoucherLoop = false;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mMainCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAgingARInv2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (!mIsRptAgingARInvUseDocDtFilter)
                {
                    label2.Visible = DteDocumentDt1.Visible = label6.Visible = DteDocumentDt2.Visible = ChkDocumentDt.Visible = false;
                }
                mlProfitCenter = new List<String>();
                //if (!mIsFicoUseMultiProfitCenterFilter)
                //{
                //    LblMultiProfitCenterCode.Visible = false;
                //    CcbProfitCenterCode.Visible = false;
                //    ChkProfitCenterCode.Visible = false;
                //}
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                SetLueStatus();
                Sm.SetLue(LueStatus, "OP");
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                LblStatus.Visible = LueStatus.Visible = ChkStatus.Visible = mIsRptAgingInvoiceARShowFulfilledDocument;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(ref MySqlCommand cm)
        {
            var SQL = new StringBuilder();
            string Filter = " ", Filter2 = " ", Filter3 = " ", Filter4 = " ", Filter5 = " ";

            SetProfitCenter();

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
            
            Filter2 = Filter;

            Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DueDt");
            Sm.FilterDt(ref Filter2, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
            Sm.FilterDt(ref Filter3, ref cm, Sm.GetDte(DteDocumentDt1), Sm.GetDte(DteDocumentDt2), "A.DocDt");
            Sm.FilterStr(ref Filter4, ref cm, Sm.GetLue(LueCtCtCode), "T2.CtCtCode", true);
            if (Sm.GetLue(LueStatus) == "OP")
                Filter5 += " And T1.Status In ('O', 'P') ";
            else
                Sm.FilterStr(ref Filter5, ref cm, Sm.GetLue(LueStatus), "T1.Status", true);

            Sm.CmParam<String>(ref cm, "@MInd", mMInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

            if (ChkProfitCenterCode.Checked)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_ = string.Empty;
                    var Filter2_ = string.Empty;
                    mQueryProfitCenter = null;
                    mQueryProfitCenter2 = null;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_.Length > 0) Filter_ += " Or ";
                        Filter_ += " (M6.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter2_.Length > 0) Filter2_ += " Or ";
                        Filter2_ += " (K.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_.Length == 0 || Filter2_.Length == 0)
                    {
                        mQueryProfitCenter += "    And 1=0 ";
                        mQueryProfitCenter2 += "    And 1=0 ";
                    }
                    else
                    {
                        mQueryProfitCenter += "    And (" + Filter_ + ") ";
                        mQueryProfitCenter2 += "    And (" + Filter2_ + ") ";
                    }
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter += "    And Find_In_Set(M6.ProfitCenterCode, @ProfitCenterCode) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        mQueryProfitCenter2 += "    And Find_In_Set(K.ProfitCenterCode, @ProfitCenterCode2) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        mQueryProfitCenter += "    And M6.ProfitCenterCode In ( ";
                        mQueryProfitCenter += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "    ) ";
                        mQueryProfitCenter2 += "    And K.ProfitCenterCode In ( ";
                        mQueryProfitCenter2 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter2 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter2 += "    ) ";
                    }
                }
            }

            SQL.AppendLine("Select T1.Period, T1.DueDt, T1.AgingDays, T1.CtCode, T1.DocNo, T1.DocDt, T2.CtName, T4.CtCtName, T1.CurCode, T1.LocalDocNo, T1.TaxInvDocument, ");
            SQL.AppendLine("T1.TotalAmt, T1.TotalTax, T1.Downpayment, T1.Amt, T1.PaidAmt, T1.VoucherDocNo,T1.Balance, ");
            SQL.AppendLine("T1.DueDt, T1.AgingDays, ");
            SQL.AppendLine("Case T1.Status When 'O' Then 'Outstanding' When 'F' Then 'Fulfilled' When 'P' Then 'Partial' End As StatusDesc, ");
            SQL.AppendLine("If (AgingDays < 0, Balance, 0) As AgingCurrent, ");
            SQL.AppendLine("If (AgingDays > 0 And AgingDays <= 30, Balance, 0) As Aging1To30, ");
            SQL.AppendLine("If (AgingDays >= 31 And AgingDays <= 60, Balance, 0) As Aging31To60, ");
            SQL.AppendLine("If (AgingDays >= 61 And AgingDays <= 90, Balance, 0) As Aging61To90, ");
            SQL.AppendLine("If (AgingDays >= 91 And AgingDays <= 180, Balance, 0) As Aging91To180, ");
            SQL.AppendLine("If (AgingDays >= 181 And AgingDays <= 365, Balance, 0) As Aging181To365, ");
            if (mAgingARDaysFormat == "1")
            {
                SQL.AppendLine("If (AgingDays >= 366, Balance, 0) As AgingOver365, ");
                SQL.AppendLine("0.00 As Aging731To1095, ");
                SQL.AppendLine("0.00 As Aging1096To1460, ");
                SQL.AppendLine("0.00 As AgingOver1461, ");
            }
            else
            {
                SQL.AppendLine("If (AgingDays >= 366 And AgingDays <= 730, Balance, 0) As AgingOver365, ");
                SQL.AppendLine("If (AgingDays >= 731 And AgingDays <= 1095, Balance, 0) As Aging731To1095, ");
                SQL.AppendLine("If (AgingDays >= 1096 And AgingDays <= 1460, Balance, 0) As Aging1096To1460, ");
                SQL.AppendLine("If (AgingDays >= 1461, Balance, 0) As AgingOver1461, ");
            }
            SQL.AppendLine("T1.Remark, T1.WhsName, T1.EntName, T1.DOCtDocNo, T1.taxCode1, ifnull(T1.taxAmt1, 0) As TaxAmt1, T1.taxCode2, ifnull(T1.taxAmt2, 0)  As TaxAmt2, T1.taxCode3, ifnull(T1.taxAmt3, 0) As TaxAmt3, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT B.Percentage ");
            SQL.AppendLine("    FROM tblallowanceforimpairmenthdr A ");
            SQL.AppendLine("    INNER JOIN tblallowanceforimpairmentdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    WHERE A.CancelInd = 'N' AND B.DNo = ");
            SQL.AppendLine("        case ");
            SQL.AppendLine("            when AgingDays > 0 AND AgingDays <= 30 then '001' ");
            SQL.AppendLine("            when AgingDays >= 31 AND AgingDays <= 60 then '002' ");
            SQL.AppendLine("            when AgingDays >= 61 AND AgingDays <= 90 then '003' ");
            SQL.AppendLine("            when AgingDays >= 91 AND AgingDays <= 180 then '004' ");
            SQL.AppendLine("            when AgingDays >= 181 AND AgingDays <= 365 then '005' ");
            if (mAgingARDaysFormat == "1")
                SQL.AppendLine("            when AgingDays >= 366 then '006' ");
            else
            {
                SQL.AppendLine("            when AgingDays >= 366 And AgingDays <= 730 then '006' ");
                SQL.AppendLine("            when AgingDays >= 731 And AgingDays <= 1095 then '007' ");
                SQL.AppendLine("            when AgingDays >= 1096 And AgingDays <= 1460 then '008' ");
                SQL.AppendLine("            when AgingDays >= 1461 then '009' ");
            }
            SQL.AppendLine("            ELSE '' ");
            SQL.AppendLine("        END ");
            SQL.AppendLine(") AS AllowancePercentage, ");
            if (mIsRptAgingARInvShowDOCt)
                SQL.AppendLine("T3.DOCtDocNo, ");
            else
                SQL.AppendLine("Null As DOCtDocNo, ");
            SQL.AppendLine(" T5.DeptName As DeptName, T1.ProfitCenterName, T1.Balance AS AllowanceAmt, T1.CurRate ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period,  ");
            SQL.AppendLine("    A.DueDt, DateDiff(Left(currentdatetime(), 8), A.DueDt) As AgingDays,  ");
            SQL.AppendLine("    A.CtCode, A.DocNo, A.Docdt, A.LocalDocNo, A.TaxInvDocument, A.CurCode,  ");
            SQL.AppendLine("    A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, IfNull(B.PaidAmt, 0)+IfNull(C.ARSAmt, 0) As PaidAmt, B.VoucherDocNo,  ");
            SQL.AppendLine("    A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("    g.WhsName, G.EntName, G.DOCtDocNo, L.ProfitCenterName, IFNULL(M.Amt, 1.00) AS CurRate, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) = A.Amt then 'O' ");
            SQL.AppendLine("        When A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0) = 0 then 'F' ");
            SQL.AppendLine("        else 'P' ");
            SQL.AppendLine("    End Status, ");
		    SQL.AppendLine("    A.TaxCode1, ((D.Taxrate/100) * A.TotalAmt) taxAmt1, A.TaxCode2, ((E.Taxrate/100) * A.TotalAmt) taxAmt2, A.TaxCode3 , ((F.Taxrate/100) * A.TotalAmt) As taxAmt3 ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr B1  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='1'  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("        Where B1.CancelInd='N' " + Filter.Replace("A.", "B1."));
            SQL.AppendLine("        Group By B1.DocNo  ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select T1.DocNo, Sum(T2.Amt) As ARSAmt  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr T1  ");
            SQL.AppendLine("        Inner Join TblARSHdr T2 On T1.DocNo=T2.SalesInvoiceDocNo And T2.CancelInd='N'  ");
            SQL.AppendLine("        Where T1.CancelInd='N' " + Filter.Replace("A.", "T1."));
            SQL.AppendLine("        Group By T1.DocNo  ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("    Left Join Tbltax D On A.TaxCode1 = D.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax E On A.TaxCode2 = E.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax F On A.TaxCode3 = F.TaxCode ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("	        Select A.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct D.WhsName Separator ', ') As WhsName, ");
            SQL.AppendLine("            Group_Concat(Distinct G.EntName Separator ', ') As EntName, ");
            SQL.AppendLine("            Group_Concat(Distinct C.DocNo Separator ', ') As DOCtDocNo ");
            SQL.AppendLine("	        From tblSalesInvoiceHdr A ");
            SQL.AppendLine("	        Inner Join TblsalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("	        Inner Join TblDOCthdr C On B.DOCtDocNo = C.DocNo ");
            SQL.AppendLine("	        Inner Join TblWarehouse D On C.WhSCode = D.WhsCode ");
            SQL.AppendLine("	        Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("	        left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("	        left Join TblEntity G On F.EntCode = G.EntCode ");
            SQL.AppendLine("	        Group By A.DocNo ");
            SQL.AppendLine("    )G On A.DocNo = G.DocNo ");
            //if (mIsFicoUseMultiProfitCenterFilter)
            //{
                SQL.AppendLine("Inner Join TblsalesInvoiceDtl H On A.DocNo = H.DocNo ");
                SQL.AppendLine("Inner Join TblDOCthdr I On H.DOCtDocNo = I.DocNo  ");
                SQL.AppendLine("Inner Join TblWarehouse J ON I.WhSCode = J.WhsCode ");
                SQL.AppendLine("Left Join TblCostcenter K On J.CCCode = K.CCCode ");
                SQL.AppendLine("Left Join TblProfitcenter L On K.ProfitCenterCode = L.ProfitcenterCode ");
            //}
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT M1.DocNo, M1.CurCode, M2.CurCode1, M2.CurCode2, M2.Amt ");
            SQL.AppendLine("    FROM tblsalesinvoicehdr M1 ");
            SQL.AppendLine("    INNER JOIN tblcurrencyrate M2 ON M1.CurCode = M2.CurCode1 AND M2.CurCode2 = @MainCurCode ");
            SQL.AppendLine("    INNER JOIN tblsalesinvoicedtl M3 ON M1.DocNo = M3.DocNo ");
            SQL.AppendLine("    INNER JOIN tbldocthdr M4 ON M3.DOCtDocNo = M4.DocNo ");
            SQL.AppendLine("    INNER JOIN tblwarehouse M5 ON M4.WhsCode = M5.WhsCode ");
            SQL.AppendLine("    LEFT JOIN tblcostcenter M6 ON M5.CCCode = M6.CCCode ");
            SQL.AppendLine("    WHERE M1.CancelInd='N' -- AND FIND_IN_SET(M6.ProfitCenterCode, @ProfitCenterCode) ");
            SQL.AppendLine(     mQueryProfitCenter);
            SQL.AppendLine("    AND M2.RateDt = ( ");
            SQL.AppendLine("        SELECT MAX(RateDt)FROM tblcurrencyrate WHERE RateDt <= M1.DocDt) ");
            SQL.AppendLine("    GROUP BY M1.DocNo ");
            SQL.AppendLine(") M ON A.DocNo = M.DocNo ");
            if (!mIsRptAgingARInvUseDocDtFilter)
                SQL.AppendLine("    Where A.CancelInd='N'  ");
            else
                SQL.AppendLine("    Where A.CancelInd='N' " + Filter3);

            if (mIsUseMInd)
            {
                if (!mMInd)
                    SQL.AppendLine("        And A.MInd='N' ");
            }
            //if (mIsFicoUseMultiProfitCenterFilter)
            //{
                SQL.AppendLine("-- AND FIND_IN_SET(K.ProfitCenterCode, @ProfitCenterCode) ");
                SQL.AppendLine(mQueryProfitCenter2);
            //}
            SQL.AppendLine(Filter);

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period,  ");
			SQL.AppendLine("        A.DocDt As DueDt,  ");
			SQL.AppendLine("        DateDiff(Left(currentdatetime(), 8), A.DocDt) As AgingDays,  ");
			SQL.AppendLine("        A.CtCode, A.DocNo, A.DocDt,  A.LocalDocNo, A.TaxInvDocument, A.CurCode,  ");
			SQL.AppendLine("        -1*A.TotalAmt As TotalAmt,  ");
			SQL.AppendLine("        0 As TotalTax,  ");
			SQL.AppendLine("        0 As Downpayment,  ");
			SQL.AppendLine("        -1*A.TotalAmt As Amt,  ");
			SQL.AppendLine("        IfNull(B.PaidAmt, 0) As PaidAmt, B.VoucherDocNo,   ");
			SQL.AppendLine("        (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("        C.WhsName, C.EntName, C.DOCtDocNo, L.ProfitCenterName, IFNULL(M.Amt, 1.00) AS CurRate, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0) = (-1*A.TotalAmt) then 'O' ");
            SQL.AppendLine("        When (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0) = 0 then 'F' ");
            SQL.AppendLine("        else 'P' ");
            SQL.AppendLine("    End Status, ");
			SQL.AppendLine("        null, 0 taxAmt1, null, 0 taxAmt2, null, 0 As taxAmt3 ");
			SQL.AppendLine("        From TblSalesReturnInvoiceHdr A  ");
			SQL.AppendLine("        Left Join (  ");
            SQL.AppendLine("            Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
			SQL.AppendLine("            From TblSalesReturnInvoiceHdr B1  ");
			SQL.AppendLine("            Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='2'  ");
			SQL.AppendLine("            Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
			SQL.AppendLine("            Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
			SQL.AppendLine("            Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("            Where B1.CancelInd='N' " + Filter2.Replace("A.", "B1."));
            SQL.AppendLine("            Group By B1.DocNo  ");
			SQL.AppendLine("        ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("	        Select A.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct D.WhsName Separator ', ') As WhsName, ");
            SQL.AppendLine("            Group_Concat(Distinct G.EntName Separator ', ') As EntName, ");
            SQL.AppendLine("            Group_Concat(Distinct C.DocNo Separator ', ') As DOCtDocNo ");
            SQL.AppendLine("	        From tblSalesReturnInvoiceDtl A  ");
            SQL.AppendLine("	        Inner Join tblRecvCtHdr B On A.RecvCtDocno = B.DocNo ");
            SQL.AppendLine("	        Inner Join TblDOCthdr C On B.DOCtDocNo = C.DocNo ");
            SQL.AppendLine("	        Inner Join TblWarehouse D On C.WhSCode = D.WhsCode ");
            SQL.AppendLine("	        Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("	        Left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("	        Left Join TblEntity G On F.EntCode = G.EntCode ");
            SQL.AppendLine("	        Group By A.DocNo ");
            SQL.AppendLine("        )C On A.DocNo = C.Docno ");
            //if (mIsFicoUseMultiProfitCenterFilter)
            //{
                SQL.AppendLine("Inner Join TblsalesInvoiceDtl H On A.DocNo = H.DocNo ");
                SQL.AppendLine("Inner Join TblDOCthdr I On H.DOCtDocNo = I.DocNo  ");
                SQL.AppendLine("Inner Join TblWarehouse J ON I.WhSCode = J.WhsCode ");
                SQL.AppendLine("Left Join TblCostcenter K On J.CCCode = K.CCCode ");
                SQL.AppendLine("Left Join TblProfitcenter L On K.ProfitCenterCode = L.ProfitcenterCode ");
            //}
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT M1.DocNo, M1.CurCode, M2.CurCode1, M2.CurCode2, M2.Amt ");
            SQL.AppendLine("    FROM tblsalesinvoicehdr M1 ");
            SQL.AppendLine("    INNER JOIN tblcurrencyrate M2 ON M1.CurCode = M2.CurCode1 AND M2.CurCode2 = @MainCurCode ");
            SQL.AppendLine("    INNER JOIN tblsalesinvoicedtl M3 ON M1.DocNo = M3.DocNo ");
            SQL.AppendLine("    INNER JOIN tbldocthdr M4 ON M3.DOCtDocNo = M4.DocNo ");
            SQL.AppendLine("    INNER JOIN tblwarehouse M5 ON M4.WhsCode = M5.WhsCode ");
            SQL.AppendLine("    LEFT JOIN tblcostcenter M6 ON M5.CCCode = M6.CCCode ");
            SQL.AppendLine("    WHERE M1.CancelInd='N' -- AND FIND_IN_SET(M6.ProfitCenterCode, @ProfitCenterCode) ");
            SQL.AppendLine(     mQueryProfitCenter);
            SQL.AppendLine("    AND M2.RateDt = ( ");
            SQL.AppendLine("        SELECT MAX(RateDt)FROM tblcurrencyrate WHERE RateDt <= M1.DocDt) ");
            SQL.AppendLine("    GROUP BY M1.DocNo ");
            SQL.AppendLine(") M ON A.DocNo = M.DocNo ");
            SQL.AppendLine("        Where A.CancelInd='N'  ");
            //if (mIsFicoUseMultiProfitCenterFilter)
            //{
                SQL.AppendLine("-- AND FIND_IN_SET(K.ProfitCenterCode, @ProfitCenterCode) ");
                SQL.AppendLine(mQueryProfitCenter2);
            //}
            SQL.AppendLine(Filter2);

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
            SQL.AppendLine(Filter4);
            if (mIsRptAgingARInvShowDOCt)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.DocNo, ");
                SQL.AppendLine("    Group_Concat(Distinct B.DOCtDocNo Order By B.DOCtDocNo Separator ', ') As DOCtDocNo ");
                SQL.AppendLine("    From TblSalesInvoiceHdr A, TblSalesInvoiceDtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.DOCtDocNo Is Not Null ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    Group By A.DocNo ");
                SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");
            }
                SQL.AppendLine("Left Join TblCustomerCategory T4 On T2.CtCtCode=T4.CtCtCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("       Select A.DocNo, ");
                SQL.AppendLine("       Group_Concat(Distinct IfNull(E.DeptName, '') SEPARATOR ', ') AS DeptName,  E.DeptCode ");
                SQL.AppendLine("       FROM tblSalesInvoiceDtl A ");
                SQL.AppendLine("       Inner Join tblDoctHdr B ON B.DocNo = A.DOCtDocNo ");
                SQL.AppendLine("       Inner Join tblWarehouse C ON C.WhsCode = B.WhsCode ");
                SQL.AppendLine("       Inner Join tblCostCenter D ON D.CCCode = C.CCCode ");
                SQL.AppendLine("       Inner Join tblDepartment E ON E.DeptCode = D.DeptCode ");
                SQL.AppendLine("       Group By A.DocNo ");
                SQL.AppendLine(") T5 ON T5.DocNo = T1.DocNo ");
            if (!mIsRptAgingInvoiceARShowFulfilledDocument)
                SQL.AppendLine("Where T1.Balance<>0.00 ");
            else
            {
                SQL.AppendLine("Where 0 = 0 ");
                SQL.AppendLine(Filter5);
            }
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T5.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByCtCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=T4.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T1.Period, T1.CurCode, T2.CtName, T1.DocNo; ");

            return SQL.ToString();  
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 49;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 7;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No", 
                        //1-5
                        "Period",
                        "Currency",
                        "Customer", 
                        "Invoice#",
                        "",
                        //6-10
                        "Local"+Environment.NewLine+"Document#",
                        "Document"+Environment.NewLine+"Date",
                        "Remark",
                        "Entity",
                        "DO",
                        //11-15
                        "Warehouse",
                        "Tax"+Environment.NewLine+"Invoice#",
                        "Amount"+Environment.NewLine+"Before Tax",
                        "Tax"+Environment.NewLine+"Amount",
                        "Downpayment",
                        //16-20
                        "Invoice"+Environment.NewLine+"Amount",
                        "Paid/"+Environment.NewLine+"Settled",
                        "Balance",
                        "Due"+Environment.NewLine+"Date",
                        "Aging"+Environment.NewLine+"(Days)",
                        //21-25
                        "Non"+Environment.NewLine+"Over Due",
                        "Over Due"+Environment.NewLine+"1-30 Days",
                        "Over Due"+Environment.NewLine+"31-60 Days",
                        "Over Due"+Environment.NewLine+"61-90 Days",
                        "Over Due"+Environment.NewLine+"91-180 Days",

                        //26-30
                        "Over Due"+Environment.NewLine+"181-365 Days",
                        (mAgingARDaysFormat == "1") ? "Over 366 Days" : "Over Due"+Environment.NewLine+"366-730 Days",
                        "Over Due"+Environment.NewLine+"731-1095 Days",
                        "Over Due"+Environment.NewLine+"1096-1460 Days",
                        "Over 1460 Days",

                        //31-35
                        "Tax 1",
                        "Tax Amount 1",
                        "Tax 2",
                        "Tax Amount 2",
                        "Tax 3",

                        //36-40
                        "Tax Amount 3",
                        "DO To Customer",
                        "Voucher#",
                        "",
                        "Customer's Category",

                        //41-45 task SET
                        "Status",
                        "Department Name",
                        "Profit Center",
                        "Allowance for Impairment"+Environment.NewLine+"of Receivable Percentage (%)",
                        "Allowance for Impairment"+Environment.NewLine+"of Receivable Amount",

                        //46-48
                        "Balance After Allowance"+Environment.NewLine+"(Original Amount)",
                        "Currency Rate",
                        "Balance After Allowance"+Environment.NewLine+"(Local Currency)",
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        100, 60, 250, 145, 20,
                        //6-10
                        130, 130, 200, 180, 130, 
                        //11-15
                        130, 130, 130, 80, 130, 
                        //16-20
                        130, 130, 130, 130, 130,
                        //21-25
                        130, 130, 130, 130, 130,
                        //26-30
                        130, 130, 130, 130, 130, 
                        //31-35
                        100, 120, 100, 120, 100, 
                        //36-40
                        120, 150, 0, 20, 200, 
                        //41-45
                        100, 150, 200, 200, 200, 
                        //46-48
                        200, 100, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5, 39 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 34, 36, 44, 45, 46, 47, 48 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 31, 32, 33, 34, 35, 36, 38, 43 }, false);
            if (!mIsRptAgingARInvShowDOCt)
                Sm.GrdColInvisible(Grd1, new int[] { 37 }, false);
            if (!mIsRptShowVoucherLoop) Sm.GrdColInvisible(Grd1, new int[] { 39 }, false);
            if (mRptAgingARInvDlgFormat == "1")
                Grd1.Cols[42].Visible = false;
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48 });

            //task SET
                Grd1.Cols[48].Move(21);
                Grd1.Cols[47].Move(21);
                Grd1.Cols[46].Move(21);
                Grd1.Cols[45].Move(21);
                Grd1.Cols[44].Move(21);
            if (mAgingARDaysFormat == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 28, 29, 30 });
            }

            Grd1.Cols[39].Move(18);
            Grd1.Cols[40].Move(4);
            Grd1.Cols[41].Move(21);
            Grd1.Cols[42].Move(5);
            Grd1.Cols[43].Move(13);
            
            Sm.GrdColInvisible(Grd1, new int[] { 41 }, mIsRptAgingInvoiceARShowFulfilledDocument);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 31, 32, 33, 34, 35, 36, 42, 43 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            //if (mIsFicoUseMultiProfitCenterFilter)
            if (IsProfitCenterInvalid()) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(ref cm),
                    new string[]
                    { 
                        //0
                        "Period", 
                        //1-5
                        "CurCode", "CtName", "DocNo", "LocalDocNo", "DocDt", 
                        //6-10
                        "Remark", "EntName", "DOCtDocNo", "WhsName", "TaxInvDocument", 
                        //11-15
                        "TotalAmt", "TotalTax", "Downpayment", "Amt", "PaidAmt",   
                        //16-20
                        "Balance", "DueDt", "AgingDays", "AgingCurrent", "Aging1To30",                 
                        //21-25
                        "Aging31To60", "Aging61To90", "Aging91To180", "Aging181To365", "AgingOver365", 
                        //26-30
                        "TaxCode1", "TaxAmt1", "TaxCode2", "TaxAmt2", "TaxCode3", 
                        //31-35
                        "TaxAmt3", "DOCtDocNo", "VoucherDocNo", "CtCtName", "StatusDesc", 
                        //36-40
                        "DeptName", "ProfitCenterName", "AllowanceAmt", "CurRate", "AllowancePercentage",
                        //41-43
                        "Aging731To1095", "Aging1096To1460", "AgingOver1461"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                        if (mAgingARDaysFormat == "2")
                        {
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 41);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 42);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 43);
                        }
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 28);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 30);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 31);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 32);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 33);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 34);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 35);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 36);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 37);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 40);
                        Grd1.Cells[Row, 45].Value = Sm.GetGrdDec(Grd, Row, 18) * (Sm.GetGrdDec(Grd, Row, 44)/100);
                        Grd1.Cells[Row, 46].Value = Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 39);
                        Grd1.Cells[Row, 48].Value = Sm.GetGrdDec(Grd, Row, 46) * Sm.GetGrdDec(Grd, Row, 47);
                        //1-30
                        if (Sm.GetGrdDec(Grd, Row, 22) == 0)
                            Grd1.Cells[Row, 22].Value = 0;
                        else
                            Grd1.Cells[Row, 22].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);
                        //31-60
                        if (Sm.GetGrdDec(Grd, Row, 23) == 0)
                            Grd1.Cells[Row, 23].Value = 0;
                        else
                            Grd1.Cells[Row, 23].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);
                        //61-90
                        if (Sm.GetGrdDec(Grd, Row, 24) == 0)
                            Grd1.Cells[Row, 24].Value = 0;
                        else
                            Grd1.Cells[Row, 24].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);
                        //91-180
                        if (Sm.GetGrdDec(Grd, Row, 25) == 0)
                            Grd1.Cells[Row, 25].Value = 0;
                        else
                            Grd1.Cells[Row, 25].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);
                        //181-365
                        if (Sm.GetGrdDec(Grd, Row, 26) == 0)
                            Grd1.Cells[Row, 26].Value = 0;
                        else
                            Grd1.Cells[Row, 26].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);
                        //(mAgingARDaysFormat == 1) > 365 : 366-730
                        if (Sm.GetGrdDec(Grd, Row, 27) == 0)
                            Grd1.Cells[Row, 27].Value = 0;
                        else
                            Grd1.Cells[Row, 27].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);
                        //731-1095
                        if (Sm.GetGrdDec(Grd, Row, 28) == 0)
                            Grd1.Cells[Row, 28].Value = 0;
                        else
                            Grd1.Cells[Row, 28].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);
                        //1096-1460
                        if (Sm.GetGrdDec(Grd, Row, 29) == 0)
                            Grd1.Cells[Row, 29].Value = 0;
                        else
                            Grd1.Cells[Row, 29].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);
                        //>1460
                        if (Sm.GetGrdDec(Grd, Row, 30) == 0)
                            Grd1.Cells[Row, 30].Value = 0;
                        else
                            Grd1.Cells[Row, 30].Value = (Sm.GetGrdDec(Grd, Row, 18) - Sm.GetGrdDec(Grd, Row, 45)) * Sm.GetGrdDec(Grd, Row, 47);

                    }, true, false, false, false
                );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                switch (mDocTitle)
                {
                    case "KMI":
                    case "KIM":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    case "AMKA":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    default:
                        {
                            var f1 = new FrmSalesInvoice(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                }
            }

            if (e.ColIndex == 38 && Sm.GetGrdStr(Grd1, e.RowIndex, 37).Length != 0)
            {
                var f = new FrmRptAgingARInv2Dlg(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 34);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                switch (mDocTitle)
                {
                    case "KMI":
                    case "KIM":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    case "AMKA":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    case "SIER":
                        {
                            if (Sm.GetValue("Select DocType From TblSalesInvoiceDtl where DocNo=@Param", Sm.GetGrdStr(Grd1, e.RowIndex, 4)) == "1")
                            {
                                var f1 = new FrmSalesInvoice(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            else if (Sm.GetValue("Select DocType From TblSalesInvoiceDtl where DocNo=@Param", Sm.GetGrdStr(Grd1, e.RowIndex, 4)) == "3")
                            {
                                var f1 = new FrmSalesInvoice3(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            break;
                        }
                    default:
                        {
                            var f1 = new FrmSalesInvoice(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                }
            }
                if (mRptAgingARInvDlgFormat == "1" && (e.ColIndex == 39 && Sm.GetGrdStr(Grd1, e.RowIndex, 38).Length != 0))
                {
                    var f = new FrmRptAgingARInv2Dlg(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 38);
                    f.ShowDialog();
                }
                if (mRptAgingARInvDlgFormat == "2" && (e.ColIndex == 39 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length != 0))
                {
                    var f = new FrmRptAgingARInv2Dlg2(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.SalesInvoiceDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param)) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private void GetParameter()
        {
            //string MenuCodeForDocWithMInd = Sm.GetParameter("MenuCodeForDocWithMInd");
            //if (MenuCodeForDocWithMInd.Length > 0)
            //    mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1;
            //mIsUseMInd = Sm.GetParameterBoo("IsUseMInd");
            //mIsRptAgingARInvShowDOCt = Sm.GetParameterBoo("IsRptAgingARInvShowDOCt");
            //mIsRptAgingARInvUseDocDtFilter = Sm.GetParameterBoo("IsRptAgingARInvUseDocDtFilter");
            //mDocTitle = Sm.GetParameter("DocTitle");
            //mIsRptShowVoucherLoop = Sm.GetParameterBoo("IsRptShowVoucherLoop");
           

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'DocTitle', 'IsUseMInd', 'IsRptAgingARInvShowDOCt', 'IsRptAgingARInvUseDocDtFilter', 'IsRptShowVoucherLoop', ");
            SQL.AppendLine("'MenuCodeForDocWithMInd', 'IsRptAgingInvoiceARShowFulfilledDocument', 'RptAgingARInvDlgFormat', 'IsFilterByCtCt', 'IsFilterByDept', ");
            SQL.AppendLine("'IsFicoUseMultiProfitCenterFilter', 'IsFilterByProfitCenter', 'MainCurcode', 'AgingARDaysFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsUseMInd": mIsUseMInd = ParValue == "Y"; break;
                            case "IsRptAgingARInvShowDOCt": mIsRptAgingARInvShowDOCt = ParValue == "Y"; break;
                            case "IsRptAgingARInvUseDocDtFilter": mIsRptAgingARInvUseDocDtFilter = ParValue == "Y"; break;
                            case "IsRptShowVoucherLoop": mIsRptShowVoucherLoop = ParValue == "Y"; break;
                            case "IsRptAgingInvoiceARShowFulfilledDocument": mIsRptAgingInvoiceARShowFulfilledDocument = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsFicoUseMultiProfitCenterFilter": mIsFicoUseMultiProfitCenterFilter = ParValue == "Y"; break;
                            case "IsFilterByProfitCenter": mIsFilterByProfitCenter = ParValue == "Y"; break;

                            //string
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "AgingARDaysFormat": mAgingARDaysFormat = ParValue;
                                if (mAgingARDaysFormat.Length == 0) mAgingARDaysFormat = "1";
                                break;
                            case "MenuCodeForDocWithMInd":
                                string MenuCodeForDocWithMInd = ParValue;
                                if (MenuCodeForDocWithMInd.Length > 0)
                                    mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1;
                                break;
                            case "RptAgingARInvDlgFormat": mRptAgingARInvDlgFormat = ParValue;
                                if (mRptAgingARInvDlgFormat.Length == 0) mRptAgingARInvDlgFormat = "1";
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 27, 32, 34, 37 });
        }

        private void SetLueStatus()
        {
            Sm.SetLue2(
                ref LueStatus,
                "Select 'O' As Col1, 'Outstanding' As Col2 " +
                "Union All Select 'P' As Col1, 'Partial' As Col2 " +
                "Union All Select 'F' As Col1, 'Fulfilled' As Col2 " +
                "Union All Select 'OP' As Col1, 'Outstanding+Partial' As Col2;",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit Center");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocumentDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocumentDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocumentDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document's date");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue3(Sl.SetLueCtCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer's category");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

        #endregion
    }
}
