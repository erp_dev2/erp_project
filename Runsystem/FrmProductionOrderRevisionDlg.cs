﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionOrderRevisionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmProductionOrderRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProductionOrderRevisionDlg(FrmProductionOrderRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

            #region Form Method

            override protected void FrmLoad(object sender, EventArgs e)
            {
                if (this.Text.Length == 0) this.Text = "List of Production Order";
                try
                {
                    base.FrmLoad(sender, e);
                    Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                    Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);

                    SetGrd();
                    SetSQL();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }

            #endregion

            #region Standard Method

            private void SetGrd()
            {
                Grd1.Cols.Count = 9;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                        Grd1, new string[] 
                        {
                            //0
                            "No.",

                            //1-5
                            "Document#",
                            "",
                            "Date",
                            "Item's Code",
                            "Item's Name",
                            
                            //6-8
                            "Quantity",
                            "UOM",
                            "Remark"
                        },
                        new int[]
                        {
                            //0
                            40,
 
                            //1-5
                            160, 20, 80, 100, 150,

                            //6-8
                            100, 100, 200
                        }
                    );
                Sm.GrdColButton(Grd1, new int[] { 2 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8 });
                Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
                Sm.GrdFormatDate(Grd1, new int[] { 3 });
                Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, false);
                Sm.SetGrdProperty(Grd1, false);
            }

            override protected void HideInfoInGrd()
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
                Sm.SetGrdAutoSize(Grd1);
            }

            override protected void SetSQL()
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT a.DocNo, a.DocDt, a.ItCode, b.ItName, a.Qty, b.PlanningUomCode, a.Remark ");
                SQL.AppendLine("FROM TblProductionOrderHdr a ");
                SQL.AppendLine("INNER JOIN TblItem b ON a.ItCode = b.ItCode ");
                SQL.AppendLine("WHERE a.CancelInd = 'N' ");

                mSQL = SQL.ToString();
            }

            override protected void ShowData()
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;

                    string Filter = " AND 0=0 ";

                    var cm = new MySqlCommand();

                    Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "a.DocNo", false);
                    Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "a.DocDt");

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " ORDER BY a.DocDt, a.DocNo;",
                            new string[] 
                            { 
                                //0
                                "DocNo",

                                //1-5
                                "DocDt", 
                                "ItCode",
                                "ItName", 
                                "Qty",
                                "PlanningUomCode",

                                //6
                                "Remark"
                             },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            }, true, false, false, false
                        );
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }

            override protected void ChooseData()
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    int Row = Grd1.CurRow.Index;

                    mFrmParent.TxtProductionOrderDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                    mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                    mFrmParent.TxtItName.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                    mFrmParent.TxtQtyOld.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Row, 6), 0);
                    mFrmParent.TxtPlanningUomCode.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                    mFrmParent.TxtQty.EditValue = Sm.FormatNum(0, 0);
                    mFrmParent.TxtQty.Focus();

                    this.Close();
                }
            }

            #endregion

            #region Grid Method

            override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
            {
                e.DoDefault = false;

                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }

            override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
            {
                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }

            override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
            {
                if (e.RowIndex >= 0) ChooseData();
            }

            #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
            {
                Sm.FilterTxtSetCheckEdit(this, sender);
            }

            private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
            {
                Sm.FilterSetTextEdit(this, sender, "Document#");
            }

            private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
            {
                Sm.FilterDteSetCheckEdit(this, sender);
            }

            private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
            {
                Sm.FilterDteSetCheckEdit(this, sender);
            }

            private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
            {
                Sm.FilterSetDateEdit(this, sender, "Date");
            }

            #endregion

        #endregion

    }
}
