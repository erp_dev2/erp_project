﻿#region Update
/*
    21/02/2022 [RIS/PHT] New Apps
    16/04/2022 [TKG/PHT] mempercepat proses save
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCOAInterOffice : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmCOAInterOfficeFind FrmFind;

        #endregion

        #region Constructor

        public FrmCOAInterOffice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length <= 0) this.Text = Sm.GetMenuDesc("FrmCOAInterOffice");
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetGrd();
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account", "Description"
                    },
                     new int[]
                    {
                        //0
                        20, 
                        
                        //1-2
                        220, 300
                    }
                );

            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtDocNo, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason }, false);
                    Grd1.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            //mStateInd = 0;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCOAInterOfficeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            Sm.SetDteCurrentDate(DteDocDt);
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;

            if (Sm.IsDataExist("Select DocNo From TblCOAInterOfficeHdr WHERE DocNo = @Param And CancelInd = 'N'; ", TxtDocNo.Text))
                SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsDataNotValid()) return;

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "COAInterOffice", "TblCOAInterOfficeHdr");

                var cml = new List<MySqlCommand>();

                if (TxtDocNo.Text.Length == 0)
                {
                    cml.Add(SaveCOAInterOfficeHdr(DocNo));
                    cml.Add(SaveCOAInterOfficeDtl(DocNo));
                    //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    //{
                    //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    //    {
                    //        cml.Add(SaveCOAInterOfficeDtl(DocNo, Row));
                    //    }
                    //}

                    Sm.ExecCommands(cml);

                    ShowData(DocNo);
                }
                else                
                {
                    
                    cml.Add(UpdateCOAInterOffice(TxtDocNo.Text));

                    Sm.ExecCommands(cml);

                    ShowData(TxtDocNo.Text);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;

            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDataCOAInterOfficeHdr(DocNo);
                ShowDataCOAInterOfficeDtl(DocNo);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDataCOAInterOfficeHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, CancelReason From TblCOAInterOfficeHdr Where DocNo=@DocNo",
                    new string[]
                        {
                            "DocNo", "DocDt", "CancelInd", "CancelReason"
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y") ? true : false;
                    }, true
                );
        }

        private void ShowDataCOAInterOfficeDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "SELECT A.AcNo, B.AcDesc FROM TblCOAInterOfficeDtl A INNER JOIN tblcoa B ON A.AcNo = B.AcNo WHERE DocNo = @DocNo ORDER BY AcNo ",
                    new string[]
                    { 
                        //0-1
                        "AcNo", "AcDesc"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record item .");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveCOAInterOfficeHdr(String DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCOAInterOfficeHdr(DocNo, DocDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("UPDATE TblCOAInterOfficeHdr SET CancelInd = 'Y', CancelReason = 'Cancelled By System' WHERE Docno <> @DocNo and CancelInd != 'Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCOAInterOfficeDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* COA Inter Office (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblCOAInterOfficeDtl (DocNo, DNo, AcNo, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveCOAInterOfficeDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("INSERT INTO TblCOAInterOfficeDtl (DocNo, DNo, AcNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("VALUES(@DocNo, @DNo, @AcNo, @UserCode, CurrentDateTime()) ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateCOAInterOffice(String DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCOAInterOfficeHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo And CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region Events

        #region Grid Event

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmCOAInterOfficeDlg(this));
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmCOAInterOfficeDlg(this));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Misc Control events

        private void MeeCancelReason_Validated_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion


        #endregion
    }
}
