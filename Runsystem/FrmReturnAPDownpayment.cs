﻿#region Update
/*
    15/02/2018 [WED] update DepositSummary dan DepositSummary2
    15/02/2018 [WED] Deposit Amt saat sebelum save juga di save di tabel RetunAPDownpayment
    08/03/2018 [WED] tambah print out
    21/03/2018 [WED] remark function ke deposit summary
    01/08/2018 [TKG] tambah filter entity berdasarkan group\
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
    10/06/2021 [TRI/IMS] Pilihan departemen dibuat hanya menampilkan departemen yang aktif
    15/10/2021 [BRI/AMKA] menambah kolom Type mandatory berdasarkan param mIsAPARUseType
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#
 *  29/10/2021 [BRI/AMKA] bug fix
    24/01/2023 [SET/BBT] Penambahan Local Document
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmReturnAPDownpayment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mVdCode = string.Empty, mEntCode = string.Empty;
        private bool 
            mIsReturnAPDownpaymentApprovalBasedOnDept = false,
            mIsReturnAPDownpaymentUseEntity = false,
            mIsOutstandingReturnAPDownpaymentPrintEnabled = false,
            mIsAPARUseType = false;
        internal bool mIsFilterByEnt = false;
        internal FrmReturnAPDownpaymentFind FrmFind;

        #endregion

        #region Constructor

        public FrmReturnAPDownpayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Return AP Downpayment";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                Sl.SetLueUserCode(ref LuePIC);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sl.SetLueOption(ref LueAcNoType, "AcNoTypeForAPDP");
                if (!mIsAPARUseType)
                {
                    label12.Visible = false;
                    LueAcNoType.Visible = false;
                }

                if (mIsReturnAPDownpaymentApprovalBasedOnDept)
                    LblDeptCode.ForeColor = Color.Red;

                SetGrd();
                base.FrmLoad(sender, e);

               // if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsReturnAPDownpaymentApprovalBasedOnDept = Sm.GetParameterBoo("IsReturnAPDownpaymentApprovalBasedOnDept");
            mIsReturnAPDownpaymentUseEntity = Sm.GetParameterBoo("IsReturnAPDownpaymentUseEntity");
            mIsOutstandingReturnAPDownpaymentPrintEnabled = Sm.GetParameterBoo("IsOutstandingReturnAPDownpaymentPrintEnabled");
            mIsFilterByEnt = Sm.GetParameterBoo("IsFilterByEnt");
            mIsAPARUseType = Sm.GetParameterBoo("IsAPARUseType");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "Checked By", "Status", "Date", "Remark" },
                    new int[]{ 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, MeeCancelReason, ChkCancelInd, TxtLocalDocNo,  TxtReturnAmt, LueBankAcCode, 
                        LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, LuePIC, 
                        LueDeptCode, MeeRemark, LueAcNoType
                    }, true);
                    BtnVdCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, TxtReturnAmt, LueBankAcCode, LuePaymentType, LuePIC, 
                        LueDeptCode, MeeRemark, LueAcNoType
                    }, false);
                    BtnVdCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mVdCode = string.Empty;
            mEntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtLocalDocNo, TxtStatus, 
                TxtVoucherRequestDocNo, TxtVoucherDocNo, TxtVdCode, TxtEntCode, TxtCurCode, 
                LueBankAcCode, LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, 
                LuePIC, LueDeptCode, MeeRemark, LueAcNoType
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtReturnAmt, TxtSummary }, 0);
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, false);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmReturnAPDownpaymentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No ||
                    Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                    IsDataAlreadyCancelled() ||
                    (!mIsOutstandingReturnAPDownpaymentPrintEnabled && IsDataNotApproved())
                    ) return;

                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ReturnAPDownpayment", "TblReturnAPDownpayment");

            var cml = new List<MySqlCommand>();
            cml.Add(SaveReturnAPDownpayment(DocNo, GenerateVoucherRequestDocNo()));
            //cml.Add(UpdateDepositSummary());
            //cml.Add(UpdateDepositSummary2(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");


            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtVdCode, "Vendor", false) ||
                Sm.IsTxtEmpty(TxtReturnAmt, "Amount", true) ||
                Sm.IsLueEmpty(LueBankAcCode, "Bank account") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                IsPaymentTypeNotValid() ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                (mIsAPARUseType && Sm.IsLueEmpty(LueAcNoType, "Type")) ||
                IsBankAcCodeNotValid() ||
                IsAmtNotValid();
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsAmtNotValid()
        {
            decimal SummaryAmt = 0m, ReturnAmt = 0m;
            var Amt = string.Empty;
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.Amt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblVendorDepositSummary A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.VdCode, IfNull(T1.EntCode, '') As EntCode, T1.CurCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblReturnAPDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr T2 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status<>'C' ");
            SQL.AppendLine("        And T2.VoucherDocNo Is Null ");
            SQL.AppendLine("    Where T1.Status<>'C' And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.VdCode, IfNull(T1.EntCode, ''), T1.CurCode ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.VdCode=B.VdCode ");
            if (mIsReturnAPDownpaymentUseEntity)
            {
                SQL.AppendLine("    And IfNull(A.EntCode, '')=IfNull(B.EntCode, '') ");
            }
            SQL.AppendLine("    And A.CurCode=B.CurCode ");
            SQL.AppendLine("Where A.VdCode=@VdCode ");
            if (mIsReturnAPDownpaymentUseEntity)
            {
                SQL.AppendLine("And IfNull(A.EntCode, '')=@EntCode ");
            }
            SQL.AppendLine("And A.CurCode=@CurCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);

            Amt = Sm.GetValue(cm);
            if (Amt.Length > 0) SummaryAmt = decimal.Parse(Amt);
            ReturnAmt = decimal.Parse(TxtReturnAmt.Text);

            if (ReturnAmt > SummaryAmt)
            {
                Sm.StdMsg(mMsgType.Warning, "Return amount (" + ReturnAmt + ")  is bigger than existing deposit amount (" + SummaryAmt + ") ");
                return true;
            }
            return false;
        }

        private bool IsBankAcCodeNotValid()
        { 
            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    "Select BankAcCode From TblBankAccount " +
                    "Where BankAcCode=@BankAcCode And CurCode=@CurCode;"
            };
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Bank Account : " + LueBankAcCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Currency : " + TxtCurCode.Text + Environment.NewLine + Environment.NewLine +
                    "Invalid Currency."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveReturnAPDownpayment(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblReturnAPDownpayment ");
            if (mIsAPARUseType)
                SQL.AppendLine("(DocNo, DocDt, CancelInd, LocalDocNo, Status, VdCode, EntCode, CurCode, VoucherRequestDocNo,  BankAcCode, PaymentType, BankCode, GiroNo, DueDt, DepositAmt, Amt, PIC, DeptCode, Remark, AcNoType, CreateBy, CreateDt) ");
            else
                SQL.AppendLine("(DocNo, DocDt, CancelInd, LocalDocNo, Status, VdCode, EntCode, CurCode, VoucherRequestDocNo,  BankAcCode, PaymentType, BankCode, GiroNo, DueDt, DepositAmt, Amt, PIC, DeptCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            if (mIsAPARUseType)
                SQL.AppendLine("(@DocNo, @DocDt, 'N', @LocalDocNo, 'O', @VdCode, @EntCode, @CurCode, @VoucherRequestDocNo, @BankAcCode, @PaymentType, @BankCode, @GiroNo, @DueDt, @DepositAmt, @Amt, @PIC, @DeptCode, @Remark, @AcNoType, @CreateBy, CurrentDateTime()); ");
            else
                SQL.AppendLine("(@DocNo, @DocDt, 'N', @LocalDocNo, 'O', @VdCode, @EntCode, @CurCode, @VoucherRequestDocNo, @BankAcCode, @PaymentType, @BankCode, @GiroNo, @DueDt, @DepositAmt, @Amt, @PIC, @DeptCode, @Remark, @CreateBy, CurrentDateTime()); ");
            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', @DeptCode, ");
            SQL.AppendLine("'14', Null, 'D', @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, @PIC, 0, @CurCode, @Amt, Null, Concat('Return AP Downpayment (', @VdName, '). ', @Remark), @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', @Remark, @Amt, Null, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ReturnAPDownpayment' ");
            if (mIsReturnAPDownpaymentApprovalBasedOnDept)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblReturnAPDownpayment Where DocNo=@DocNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ReturnAPDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblReturnAPDownpayment Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ReturnAPDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@VdName", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@DepositAmt", Decimal.Parse(TxtSummary.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtReturnAmt.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@AcNoType", Sm.GetLue(LueAcNoType));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand UpdateDepositSummary()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblVendorDepositSummary ");
        //    SQL.AppendLine("    Set Amt = Amt - @Amt, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
        //    SQL.AppendLine("Where VdCode = @VdCode ");
        //    if(mIsReturnAPDownpaymentUseEntity)
        //        SQL.AppendLine("And EntCode = @EntCode ");
        //    SQL.AppendLine("And CurCode = @CurCode; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
        //    if (mIsReturnAPDownpaymentUseEntity)
        //        Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
        //    Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtReturnAmt.Text));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand UpdateDepositSummary2(string DocNo)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    var SQLD = new StringBuilder();
        //    var cmD = new MySqlCommand();
        //    var l = new List<DS>();
        //    l.Clear();

        //    SQL.AppendLine("Select VdCode, EntCode, CurCode, ExcRate, Amt ");
        //    SQL.AppendLine("From TblVendorDepositSummary2 ");
        //    SQL.AppendLine("Where VdCode = @VdCode ");
        //    if (mIsReturnAPDownpaymentUseEntity)
        //        SQL.AppendLine("And EntCode = @EntCode ");
        //    SQL.AppendLine("And CurCode = @CurCode ");
        //    SQL.AppendLine("And Amt > 0 ");
        //    SQL.AppendLine("And CurCode <> 'IDR' ");
        //    SQL.AppendLine("Order By CreateDt Desc;");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();
        //        Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
        //        if (mIsReturnAPDownpaymentUseEntity)
        //            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
        //        Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        {
        //             //0
        //             "VdCode",

        //             //1-4
        //             "EntCode",
        //             "CurCode",
        //             "ExcRate",
        //             "Amt"
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new DS()
        //                {
        //                    VdCode = Sm.DrStr(dr, c[0]),
        //                    EntCode = Sm.DrStr(dr, c[1]),
        //                    CurCode = Sm.DrStr(dr, c[2]),
        //                    ExcRate = Sm.DrDec(dr, c[3]),
        //                    Amt = Sm.DrDec(dr, c[4])
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }

        //    if (l.Count > 0)
        //    {
        //        decimal ReturnedAmt = Decimal.Parse(TxtReturnAmt.Text);
        //        for (int i = 0; i < l.Count; i++)
        //        {
        //            if (ReturnedAmt <= l[i].Amt)
        //            {
        //                SQLD.AppendLine("Update TblVendorDepositSummary2 ");
        //                SQLD.AppendLine("    Set Amt = Amt - " + ReturnedAmt + ", LastUpBy = @UserCode0" + i + ", LastUpDt = CurrentDateTime() ");
        //                SQLD.AppendLine("Where VdCode = @VdCode0" + i + " ");
        //                if (mIsReturnAPDownpaymentUseEntity)
        //                    SQLD.AppendLine("And EntCode = @EntCode0" + i + " ");
        //                SQLD.AppendLine("And CurCode = @CurCode0" + i + " ");
        //                SQLD.AppendLine("And ExcRate = @ExcRate0" + i + " ");
        //                //SQLD.AppendLine("And Amt > 0 ");
        //                SQLD.AppendLine("; ");

        //                SQLD.AppendLine("Insert Into TblReturnAPDownpaymentDtl(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
        //                SQLD.AppendLine("Values(@DocNo0" + i + ", @CurCode0" + i + ", @ExcRate0" + i + ", " + ReturnedAmt + ", @UserCode0" + i + ", CurrentDateTime()); ");

        //                Sm.CmParam<String>(ref cmD, "@VdCode0" + i + "", l[i].VdCode);
        //                if (mIsReturnAPDownpaymentUseEntity)
        //                    Sm.CmParam<String>(ref cmD, "@EntCode0" + i + "", l[i].EntCode);
        //                Sm.CmParam<String>(ref cmD, "@CurCode0" + i + "", l[i].CurCode);
        //                Sm.CmParam<Decimal>(ref cmD, "@ExcRate0" + i + "", l[i].ExcRate);
        //                Sm.CmParam<String>(ref cmD, "@UserCode0" + i + "", Gv.CurrentUserCode);
        //                Sm.CmParam<String>(ref cmD, "@DocNo0" + i + "", DocNo);

        //                break;
        //            }
        //            else
        //            {
        //                if (i == (l.Count - 1)) // kalau dia record terakhir dari List Deposit Summary 2, Amount nya dikurangin sejumlah ReturnedAmt yang tersisa (berpotensi nilai Amt di VendorDepositSummary2 itu minus)
        //                {
        //                    SQLD.AppendLine("Update TblVendorDepositSummary2 ");
        //                    SQLD.AppendLine("    Set Amt = Amt - " + ReturnedAmt + ", LastUpBy = @UserCode0" + i + ", LastUpDt = CurrentDateTime() ");
        //                    SQLD.AppendLine("Where VdCode = @VdCode0" + i + " ");
        //                    if (mIsReturnAPDownpaymentUseEntity)
        //                        SQLD.AppendLine("And EntCode = @EntCode0" + i + " ");
        //                    SQLD.AppendLine("And CurCode = @CurCode0" + i + " ");
        //                    SQLD.AppendLine("And ExcRate = @ExcRate0" + i + " ");
        //                    //SQLD.AppendLine("And Amt > 0 ");
        //                    SQLD.AppendLine("; ");

        //                    SQLD.AppendLine("Insert Into TblReturnAPDownpaymentDtl(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
        //                    SQLD.AppendLine("Values(@DocNo0" + i + ", @CurCode0" + i + ", @ExcRate0" + i + ", " + ReturnedAmt + ", @UserCode0" + i + ", CurrentDateTime()); ");

        //                    Sm.CmParam<String>(ref cmD, "@VdCode0" + i + "", l[i].VdCode);
        //                    if (mIsReturnAPDownpaymentUseEntity)
        //                        Sm.CmParam<String>(ref cmD, "@EntCode0" + i + "", l[i].EntCode);
        //                    Sm.CmParam<String>(ref cmD, "@CurCode0" + i + "", l[i].CurCode);
        //                    Sm.CmParam<Decimal>(ref cmD, "@ExcRate0" + i + "", l[i].ExcRate);
        //                    Sm.CmParam<String>(ref cmD, "@UserCode0" + i + "", Gv.CurrentUserCode);
        //                    Sm.CmParam<String>(ref cmD, "@DocNo0" + i + "", DocNo);

        //                    ReturnedAmt = 0;
        //                }
        //                else
        //                {
        //                    SQLD.AppendLine("Update TblVendorDepositSummary2 ");
        //                    SQLD.AppendLine("    Set Amt = Amt - @Amt0" + i + ", LastUpBy = @UserCode0" + i + ", LastUpDt = CurrentDateTime() ");
        //                    SQLD.AppendLine("Where VdCode = @VdCode0" + i + " ");
        //                    if (mIsReturnAPDownpaymentUseEntity)
        //                        SQLD.AppendLine("And EntCode = @EntCode0" + i + " ");
        //                    SQLD.AppendLine("And CurCode = @CurCode0" + i + " ");
        //                    SQLD.AppendLine("And ExcRate = @ExcRate0" + i + " ");
        //                    //SQLD.AppendLine("And Amt > 0 ");
        //                    SQLD.AppendLine("; ");

        //                    SQLD.AppendLine("Insert Into TblReturnAPDownpaymentDtl(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
        //                    SQLD.AppendLine("Values(@DocNo0" + i + ", @CurCode0" + i + ", @ExcRate0" + i + ", @Amt0" + i + ", @UserCode0" + i + ", CurrentDateTime()); ");

        //                    Sm.CmParam<String>(ref cmD, "@VdCode0" + i + "", l[i].VdCode);
        //                    if (mIsReturnAPDownpaymentUseEntity)
        //                        Sm.CmParam<String>(ref cmD, "@EntCode0" + i + "", l[i].EntCode);
        //                    Sm.CmParam<String>(ref cmD, "@CurCode0" + i + "", l[i].CurCode);
        //                    Sm.CmParam<Decimal>(ref cmD, "@ExcRate0" + i + "", l[i].ExcRate);
        //                    Sm.CmParam<Decimal>(ref cmD, "@Amt0" + i + "", l[i].Amt);
        //                    Sm.CmParam<String>(ref cmD, "@UserCode0" + i + "", Gv.CurrentUserCode);
        //                    Sm.CmParam<String>(ref cmD, "@DocNo0" + i + "", DocNo);

        //                    ReturnedAmt = ReturnedAmt - l[i].Amt;
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        SQLD.AppendLine("Select 1; ");
        //    }

        //    cmD.CommandText = SQLD.ToString();

        //    return cmD;
        //}

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditReturnAPDownpayment());
            //cml.Add(EditDepositSummary());
            //cml.Add(EditDepositSummary2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblReturnAPDownpayment Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text, 
                "This document is already cancelled.");
        }

        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherRequestHdr " +
                "Where CancelInd='N' And Status<>'C' And VoucherDocNo Is Not Null And DocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "This document is already process to voucher.");
        }

        private MySqlCommand EditReturnAPDownpayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblReturnAPDownpayment Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand EditDepositSummary()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblVendorDepositSummary A ");
        //    SQL.AppendLine("Inner Join TblReturnAPDownpayment B On A.VdCode = B.VdCode And A.CurCode = B.CurCode ");
        //    if (mIsReturnAPDownpaymentUseEntity)
        //        SQL.AppendLine("And A.EntCode = B.EntCode ");
        //    SQL.AppendLine("    Set A.Amt = A.Amt + B.Amt, A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
        //    SQL.AppendLine("Where B.DocNo = @DocNo And B.Status <> 'C'; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand EditDepositSummary2()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblVendorDepositSummary2 A ");
        //    SQL.AppendLine("Inner Join ");
        //    SQL.AppendLine("( ");
        //    SQL.AppendLine("    Select T1.DocNo, T1.Status, T1.CancelInd, T1.VdCode, T1.EntCode, T1.CurCode, T2.ExcRate, T2.Amt ");
        //    SQL.AppendLine("    From TblReturnAPDownpayment T1 ");
        //    SQL.AppendLine("    Inner Join TblReturnAPDownpaymentDtl T2 On T1.DocNo = T2.DocNo ");
        //    SQL.AppendLine("    Where T1.DocNo = @DocNo ");
        //    SQL.AppendLine(")B On A.VdCode = B.VdCode And A.CurCode = B.CurCode And A.ExcRate = B.ExcRate ");
        //    if (mIsReturnAPDownpaymentUseEntity)
        //        SQL.AppendLine("And A.EntCode = B.EntCode ");
        //    SQL.AppendLine("Set A.Amt = A.Amt + B.Amt, A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
        //    SQL.AppendLine("Where B.Status <> 'C'; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

        //    return cm;
        //}

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, ");
                SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
                SQL.AppendLine("A.CancelReason, A.CancelInd, A.LocalDocNo, A.VoucherRequestDocNo, E.VoucherDocNo, A.VdCode, B.VdName, A.EntCode, C.EntName, ");
                SQL.AppendLine("A.CurCode, A.DepositAmt As Summary, A.Amt, A.BankAcCode, ");
                SQL.AppendLine("A.PaymentType, A.BankCode, A.GiroNo, A.DueDt, A.PIC, A.DeptCode, A.Remark ");
                if (mIsAPARUseType)
                    SQL.AppendLine(", A.AcNoType ");
                else
                    SQL.AppendLine(", Null As AcNoType ");
                SQL.AppendLine("From TblReturnAPDownpayment A ");
                SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
                SQL.AppendLine("Left Join TblEntity C On A.EntCode = C.EntCode ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr E On A.VoucherRequestDocNo=E.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                           //0
                           "DocNo", 

                           // 1-5
                           "DocDt", "StatusDesc", "CancelReason", "CancelInd", "LocalDocNo", 
                           
                           //6-10
                           "VoucherRequestDocNo", "VoucherDocNo", "VdCode", "VdName", "EntCode", 
                           
                           //11-15
                           "EntName", "CurCode", "Summary", "Amt", "BankAcCode", 
                           
                            //16-20
                           "PaymentType", "BankCode", "GiroNo", "DueDt", "PIC", 
                           
                           //21-22
                           "DeptCode", "Remark", "AcNoType"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                            MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                            TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[5]);
                            TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[6]);
                            TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[7]);
                            mVdCode = Sm.DrStr(dr, c[8]);
                            TxtVdCode.EditValue = Sm.DrStr(dr, c[9]);
                            mEntCode = Sm.DrStr(dr, c[10]);
                            TxtEntCode.EditValue = Sm.DrStr(dr, c[11]);
                            TxtCurCode.EditValue = Sm.DrStr(dr, c[12]);
                            TxtSummary.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[13]), 0);
                            TxtReturnAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[14]), 0);
                            Sl.SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[15]));
                            Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[16]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[17]));
                            TxtGiroNo.EditValue = Sm.DrStr(dr, c[18]);
                            Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[19]));
                            Sm.SetLue(LuePIC, Sm.DrStr(dr, c[20]));
                            Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[21]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[22]);
                            Sm.SetLue(LueAcNoType, Sm.DrStr(dr, c[23]));
                        }, true
                    );
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='ReturnAPDownpayment' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",

                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Methods

        private bool IsDataNotApproved()
        {
            bool IsApproved =
                Sm.IsDataExist(
                "Select 1 from TblReturnAPDownpayment Where DocNo=@Param And CancelInd='N' And Status='A';",
                TxtDocNo.Text);

            if (!IsApproved)
            {
                Sm.StdMsg(mMsgType.Warning, "This document has not been approved yet.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblReturnAPDownpayment Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private void ParPrint()
        {
            var l = new List<ReturnAPDP>();

            string[] TableName = { "ReturnAPDP" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',");
            SQL.AppendLine("A.PaymentType ");
            SQL.AppendLine("From TblReturnAPDownpayment A ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N' And A.Status<>'C' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                     //0
                     "CompanyLogo",

                     //1-5
                     "CompanyName",
                     "CompanyAddress",
                     "CompanyAddressCity",
                     "CompanyPhone",
                     "CompanyEmail",

                     //6
                     "PaymentType"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ReturnAPDP()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            Company = Sm.DrStr(dr, c[1]),
                            Address = Sm.DrStr(dr, c[2]),
                            Phone = Sm.DrStr(dr, c[4]),
                            DocNo = TxtDocNo.Text,
                            DocDt = DteDocDt.Text,
                            
                            PIC = LuePIC.Text,
                            DeptName = LueDeptCode.Text,
                            EntName = TxtEntCode.Text,
                            VoucherDocNo = TxtVoucherDocNo.Text,
                            VdName = TxtVdCode.Text,

                            ReturnedAmt = Decimal.Parse(TxtReturnAmt.Text),
                            BankAccount = LueBankAcCode.Text,
                            PaymentTypeCode = Sm.DrStr(dr, c[6]),
                            PaymentType = LuePaymentType.Text,
                            BankName = LueBankCode.Text,

                            GiroCheque = TxtGiroNo.Text,
                            DueDt = DteDueDt.Text,
                            CurCode = TxtCurCode.Text,
                            Remark = MeeRemark.Text,
                            Printby = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            Sm.PrintReport("ReturnAPDownPayment", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void TxtReturnAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtReturnAmt, 0);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueAcNoType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcNoType, new Sm.RefreshLue2(Sl.SetLueOption), "AcNoTypeForAPDP");
        }

        #endregion

        #region Button Event

        private void BtnVdCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmReturnAPDownpaymentDlg(this));
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        class DS
        {
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string VdCode { get; set; }
            public string EntCode { get; set; }
        }

        class ReturnAPDP
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string PIC { get; set; }
            public string DeptName { get; set; }
            public string EntName { get; set; }
            public string VoucherDocNo { get; set; }
            public string Printby { get; set; }
            public string CompanyLogo { get; set; }
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string CurCode { get; set; }
            public decimal ReturnedAmt { get; set; }
            public string BankAccount { get; set; }
            public string PaymentType { get; set; }
            public string PaymentTypeCode { get; set; }
            public string BankName { get; set; }
            public string GiroCheque { get; set; }
            public string DueDt { get; set; }
            public string Remark { get; set; }
        }

        #endregion
    }
}
