﻿#region Update
/*
    03/08/2017 [TKG] tambah resign date, Karyawan yg tgl resignnya lebih lama dari tgl skrg tetap bisa minta leave
    23/08/2017 [HAR] bug fixing jumlah cuti besar tidak reload
    05/09/2017 [TKG] menggunakan validasi Employee's Leave Start Date untuk cuti tahunan/besar.
    28/10/2017 [TKG] perhitungan cuti besar utk HIN
    19/12/2017 [TKG] tambah filter dept+site berdasarkan group
    22/01/2019 [TKG] Data rincian tanggal cuti di database dikurangi jumlah hari libur sesuai dengan working schedule.
    11/02/2019 [TKG] approval menggunakan validasi approval group
    11/11/2019 [WED/SRN] TblDocumentApprovalGroupHdr --> TblDocApprovalGroupHdr; TblDocumentApprovalGroupDtl --> TblDocApprovalGroupDtl
    12/12/2019 [VIN+DITA/IMS] Printout Leave Request
    20/12/2019 [WED/SRN] Duration day saat show masih salah
    10/01/2020 [DITA/IMS] parameter untuk print out
    28/07/2022 [IBL/SIER] memunculkan tombol print ketika dipanggil dari docapproval & menu lain
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;
#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mAnnualLeaveCode = string.Empty,
            mLongServiceLeaveCode = string.Empty,
            mLongServiceLeaveCode2 = string.Empty;
        internal bool
            mIsApprovalBySiteMandatory = false,
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false;
        private string mNeglectLeaveCode = string.Empty,
            mEmpLeaveReqAutoApprovedByEmpCode = string.Empty,
            mFormPrintOutLeaveRequest = string.Empty,
            mDocTitle = string.Empty;
        internal FrmEmpLeaveFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpLeave(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Leave";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueLeaveType, "LeaveType");
                SetGrd();
                base.FrmLoad(sender, e);
                //if this application is called from other application

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    if (mDocTitle != "SIER") BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "Checked By", "Status", "Date", "Remark" },
                    new int[]{ 200, 80, 80, 400 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueLeaveCode, LueLeaveType, DteStartDt, 
                        DteEndDt, TmeStartTm, TmeEndTm, TxtLeaved, TxtDurationHour, 
                        ChkBreakInd, MeeRemark
                    }, true);
                    BtnEmpCode.Enabled = false;
                    BtnDurationHrDefault.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueLeaveCode, LueLeaveType, DteStartDt, 
                        MeeRemark
                    }, false);
                    BtnEmpCode.Enabled = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueLeaveCode, TxtEmpCode, 
                TxtEmpName, TxtEmpCodeOld, TxtPosCode, TxtDeptCode, TxtSiteCode, 
                DteJoinDt, LueLeaveType, DteStartDt, DteEndDt, TmeStartTm, 
                TmeEndTm, MeeRemark 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtDurationDay, TxtDurationHour, TxtLeaved }, 0);
            ChkCancelInd.Checked = false;
            ChkBreakInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtPosCode, TxtDeptCode, 
                TxtSiteCode, DteJoinDt, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay, TxtDurationHour, TxtLeaved }, 0);
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpLeaveFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueLeaveCode(ref LueLeaveCode, string.Empty);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteEndDt, TmeStartTm, TmeEndTm, TxtDurationHour, ChkBreakInd }, true);
                BtnDurationHrDefault.Enabled = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if(mFormPrintOutLeaveRequest.Length != 0)ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpLeave", "TblEmpLeaveHdr");
            
            var cml = new List<MySqlCommand>();
            var l = new List<EmpLeaveDtl>();
            
            SetEmpLeaveDtl(ref l);

            cml.Add(SaveEmpLeaveHdr(DocNo));
            l.ForEach(i => { cml.Add(SaveEmpLeaveDtl(DocNo, i)); });
            
            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueLeaveCode, "Leave name") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false) ||
                Sm.IsLueEmpty(LueLeaveType, "Leave type") ||
                Sm.IsDteEmpty(DteStartDt, "Leave's started date") ||
                (!DteEndDt.Properties.ReadOnly && Sm.IsDteEmpty(DteEndDt, "Leave's end date")) ||
                (!TmeStartTm.Properties.ReadOnly && Sm.IsTmeEmpty(TmeStartTm, "Leave's start time")) ||
                (!TmeEndTm.Properties.ReadOnly && Sm.IsTmeEmpty(TmeEndTm, "Leave's end time")) ||
                (!TmeStartTm.Properties.ReadOnly && Sm.IsTxtEmpty(TxtDurationHour, "Duration (hour)", true)) ||
                IsLeaveDateNotValid() ||
                IsLeaveDateAlreadyUsed() ||
                IsNeglectLeaveTypeNotValid() ||
                LeaveValidated()||
                NumbOfDayAnnualNotValid()||
                IsEmpHaveHoliday() ||
                IsDocDtForResingeeInvalid() ||
                IsLeaveStartDtForResingeeInvalid() ||
                IsLeaveEndDtForResingeeInvalid()
                ;
        }

        private bool IsDocDtForResingeeInvalid()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@DocDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Document Date :" + DteDocDt.Text + Environment.NewLine + Environment.NewLine +
                    "Document date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveStartDtForResingeeInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@StartDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Leave's Start Date :" + DteStartDt.Text + Environment.NewLine + Environment.NewLine +
                    "Leave's start date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveEndDtForResingeeInvalid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@EndDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Employee's Name : " + TxtEmpName.Text + Environment.NewLine +
                    "Resign Date : " + DteResignDt.Text + Environment.NewLine +
                    "Leave's End Date :" + DteEndDt.Text + Environment.NewLine + Environment.NewLine +
                    "Leave's end date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsNeglectLeaveTypeNotValid()
        {
            if (mNeglectLeaveCode.Length != 0 && 
                Sm.GetLue(LueLeaveCode) == mNeglectLeaveCode && 
                Sm.GetLue(LueLeaveType) != "F")
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Leave : " + LueLeaveCode.GetColumnValue("Col2") + Environment.NewLine + 
                    "Invalid leave type." + Environment.NewLine +
                    "It should be full day leave."
                    );
                return true;
            }
            return false;
        }

        private bool IsEmpHaveHoliday()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WsCode = B.WsCode ");
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL.AppendLine("Where B.ActInd = 'Y' And HolidayInd = 'Y' And A.EmpCode =@EmpCode  ");
            SQL.AppendLine("And A.Dt between @DocDt1 And @DocDt2 ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteStartDt).Substring(0, 8));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteEndDt).Substring(0, 8));

            if (Sm.IsDataExist(cm))
            {
                if (MessageBox.Show(
                    "Employee Code : " + TxtEmpCode.Text + Environment.NewLine +
                    "Name : " + TxtEmpName.Text + Environment.NewLine +
                    "have holiday schedule on " + DteStartDt.Text + " until " + DteEndDt.Text + " .", "", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                {
                    return true;
                }
            }
            return false;
          }

        private bool IsLeaveDateNotValid()
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            if (StartDt.Length != 0 && EndDt.Length != 0)
            {
                if (decimal.Parse(StartDt) > decimal.Parse(EndDt))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid leave date.");
                    return true;
                }
            }
            return false;
        }

        private bool IsLeaveDateAlreadyUsed()
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            if (StartDt.Length != 0 && EndDt.Length != 0)
            {
                //if (!DteEndDt.Properties.ReadOnly)
                //{
                    string Dt = string.Empty;
                    DateTime Dt1 = new DateTime(
                        Int32.Parse(StartDt.Substring(0, 4)),
                        Int32.Parse(StartDt.Substring(4, 2)),
                        Int32.Parse(StartDt.Substring(6, 2)),
                        0, 0, 0
                        );
                    DateTime Dt2 = new DateTime(
                        Int32.Parse(EndDt.Substring(0, 4)),
                        Int32.Parse(EndDt.Substring(4, 2)),
                        Int32.Parse(EndDt.Substring(6, 2)),
                        0, 0, 0
                        );

                    if (IsLeaveDateAlreadyUsed2(
                            Dt1.Year.ToString() +
                            ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                            ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                        ))
                        return true;

                    var TotalDays = (Dt2 - Dt1).Days;
                    for (int i = 1; i <= TotalDays; i++)
                    {
                        Dt1 = Dt1.AddDays(1);
                        
                        if (IsLeaveDateAlreadyUsed2(
                                Dt1.Year.ToString() +
                                ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                                ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                            ))
                            return true;
                    }
                //}
            }
            return false;
        }

        private bool IsLeaveDateAlreadyUsed2(string Dt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode From (");
            SQL.AppendLine("    Select A.EmpCode ");
            SQL.AppendLine("    From TblEmpLeaveHdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo And B.LeaveDt=@Dt ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.Status, 'O')<>'C' ");
            SQL.AppendLine("    And A.EmpCode=@EmpCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select B.EmpCode ");
            SQL.AppendLine("    From TblEmpLeave2Hdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo And C.LeaveDt=@Dt ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.Status, 'O')<>'C' ");
            SQL.AppendLine(") T Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@Dt", Dt);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Date : " + Sm.Right(Dt, 2) + "/" + Dt.Substring(4, 2) + "/" + Sm.Left(Dt, 4) + Environment.NewLine + 
                    "Invalid leave date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveTimeNotValid()
        {
            string
                StartTm = Sm.GetDte(DteStartDt) + Sm.GetTme(TmeStartTm),
                EndTm = Sm.GetDte(DteStartDt) + Sm.GetTme(TmeEndTm);

            if (StartTm.Length != 0 && EndTm.Length != 0)
            {
                if (decimal.Parse(StartTm) > decimal.Parse(EndTm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid leave time.");
                    return true;
                }
            }
            return false;
        }

        private bool LeaveValidated()
        {
            if (Sm.GetValue("Select PaidInd From TblLeave Where LeaveCode = '" + Sm.GetLue(LueLeaveCode) + "' ") == "Y")
            {
                if (decimal.Parse(TxtDurationDay.Text) > decimal.Parse(TxtLeaved.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid leave's duration (day).");
                    return true;
                }
            }

            return false;
        }

        private bool NumbOfDayAnnualNotValid()
        {
            var Leaved = decimal.Parse(TxtLeaved.Text);
            var LeaveCode = Sm.GetLue(LueLeaveCode);

            if (Leaved<=0m)
            {
                if (Sm.CompareStr(LeaveCode, mAnnualLeaveCode))
                {
                    Sm.StdMsg(mMsgType.Warning, "Remaining annual leave : 0 day.");
                    return true;
                }
                if (Sm.CompareStr(LeaveCode, mLongServiceLeaveCode))
                {
                    Sm.StdMsg(mMsgType.Warning, "Remaining long service leave : 0 day.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveEmpLeaveHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblEmpLeaveHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, EmpCode, LeaveCode, ");
            SQL.AppendLine("LeaveType, LeaveStartDt, StartDt, EndDt, DurationDay, StartTm, EndTm, DurationHour, BreakInd, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @EmpCode, @LeaveCode, ");
            SQL.AppendLine("@LeaveType, @LeaveStartDt, @StartDt, @EndDt, @DurationDay, @StartTm, @EndTm, @DurationHour, @BreakInd, @Remark, "); 
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='EmpLeave' ");
            if (mIsApprovalBySiteMandatory)
                SQL.AppendLine("And SiteCode In (Select SiteCode From TblEmployee Where EmpCode=@EmpCode) ");
            SQL.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode) ");
            if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpLeave' And DAGCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("And (DAGCode Is Null Or ");
                SQL.AppendLine("(DAGCode Is Not Null ");
                SQL.AppendLine("And DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@EmpCode ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("; ");


            SQL.AppendLine("Update TblEmpLeaveHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpLeave' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));
            Sm.CmParam<String>(ref cm, "@LeaveType", Sm.GetLue(LueLeaveType));
            Sm.CmParamDt(ref cm, "@LeaveStartDt", Sm.GetDte(DteJoinDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<Decimal>(ref cm, "@DurationDay", Decimal.Parse(TxtDurationDay.Text));
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
            Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEndTm));
            Sm.CmParam<Decimal>(ref cm, "@DurationHour", Decimal.Parse(TxtDurationHour.Text));
            Sm.CmParam<String>(ref cm, "@BreakInd", ChkBreakInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpLeaveDtl(string DocNo, EmpLeaveDtl i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpLeaveDtl(DocNo, DNo, LeaveDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @LeaveDt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", i.DNo);
            Sm.CmParam<String>(ref cm, "@LeaveDt", i.LeaveDt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void SetEmpLeaveDtl(ref List<EmpLeaveDtl> l)
        {
            SetEmpLeaveDtla(ref l); // menghitung tanggal leave
            if (l.Count > 0)
            {
                SetEmpLeaveDtlb(ref l); // dikurangi holiday
                l.RemoveAll(w => w.IsHoliday);
                if (l.Count > 0)
                {
                    int i = 1;
                    foreach (var x in l.OrderBy(o => o.LeaveDt))
                    {
                        x.DNo = Sm.Right(string.Concat("00", i.ToString()), 3);
                        i++;
                    }
                }
            }
        }

        private void SetEmpLeaveDtla(ref List<EmpLeaveDtl> l)
        {
            string StartDt = Sm.GetDte(DteStartDt).Substring(0, 8);

            l.Add(new EmpLeaveDtl() { DNo = "001", LeaveDt = StartDt });

            if (!DteEndDt.Properties.ReadOnly)
            {
                string Dt = string.Empty;
                string EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);
                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                var TotalDays = (Dt2 - Dt1).Days;
                for (int i = 1; i <= TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    Dt =
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2);

                    l.Add(new EmpLeaveDtl()
                    {
                        DNo = Sm.Right("00" + (i + 1).ToString(), 3),
                        LeaveDt = Dt,
                        IsHoliday = false
                    });
                }
            }
        }

        private void SetEmpLeaveDtlb(ref List<EmpLeaveDtl> l)
        {
            var Dt = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.Dt ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode = B.WSCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And B.HolidayInd='Y' ");
            SQL.AppendLine("And A.Dt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("Order By A.Dt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString(),
                    CommandTimeout = 600
                };
                Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
                Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteEndDt));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Dt = Sm.DrStr(dr, 0);
                        foreach (var x in l.Where(w => Sm.CompareStr(w.LeaveDt, Dt)))
                            x.IsHoliday = true;
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelEmpLeaveHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblEmpLeaveHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelEmpLeaveHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpLeaveHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpLeaveHdr(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpLeaveHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DocNo, DocDt, CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' Else 'Unknown' End As StatusDesc, ");
            SQL.AppendLine("A.LeaveCode, A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName, ");
            SQL.AppendLine("Case When IfNull(F.ParValue, '')='Y' Then ");
            SQL.AppendLine("    IfNull(A.LeaveStartDt, B.JoinDt) ");
            SQL.AppendLine("Else B.JoinDt ");
            SQL.AppendLine("End As JoinDt, ");
            SQL.AppendLine("B.ResignDt, A.LeaveType, A.StartDt, A.EndDt, A.DurationDay, A.StartTm, A.EndTm, A.DurationHour, A.BreakInd, A.Remark ");
            SQL.AppendLine("From TblEmpLeaveHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblParameter F On F.ParCode='IsAnnualLeaveUseStartDt' ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "LeaveCode", "EmpCode",  
                        
                        //6-10
                        "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName", 
                        
                        //11-15
                        "JoinDt", "LeaveType", "StartDt", "EndDt", "DurationDay", 
                        
                        //16-20
                        "StartTm", "EndTm", "DurationHour", "BreakInd", "Remark",

                        //21
                        "ResignDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                        SetLueLeaveCode(ref LueLeaveCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueLeaveCode, Sm.DrStr(dr, c[4]));
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[6]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[7]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[8]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LueLeaveType, Sm.DrStr(dr, c[12]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[13]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[14]));
                        TxtDurationDay.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                        Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[16]));
                        Sm.SetTme(TmeEndTm, Sm.DrStr(dr, c[17]));
                        TxtDurationHour.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        ChkBreakInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                        Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[21]));
                    }, true
                );

            var LeaveCode = Sm.GetLue(LueLeaveCode);
            if (Sm.CompareStr(LeaveCode, mAnnualLeaveCode) ||
                Sm.CompareStr(LeaveCode, mLongServiceLeaveCode) ||
                Sm.CompareStr(LeaveCode, mLongServiceLeaveCode2)
                )
                ComputeLeaveRemainingDay();
            else
                TxtLeaved.EditValue = Sm.FormatNum(decimal.Parse(
                    Sm.GetValue(
                        "Select IfNull(NoOfDay, 0) As NoOfDay " +
                        "From TblLeave Where LeaveCode=@Param;", 
                        LeaveCode)), 0);            
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='EmpLeave' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }


        #endregion

        #region Additional Method

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            string[] TableName = { "EmpLeave", "EmpLeaveSign", "EmpLeaveSign2" };

            var l = new List<EmpLeave>();
            var lDtlS = new List<EmpLeaveSign>();
            var lDtlS2 = new List<EmpLeaveSign2>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt, '%d-%b-%Y') DocDt, A.EmpCode, B.EmpName, C.DeptName, B.Address, B.Mobile, ");
            SQL.AppendLine("IfNull(A.Remark, D.LeaveName) Remark, ");
            SQL.AppendLine("D.LeaveName, A.DurationDay, IFNULL(DATE_FORMAT(A.StartDt, '%d-%b-%Y'), '') StartDt, ");
            SQL.AppendLine("IFNULL(DATE_FORMAT(A.EndDt, '%d-%b-%Y'), '') EndDt, A.DurationHour, ");
            SQL.AppendLine("IF(A.StartTm IS NULL, '', DATE_FORMAT(CONCAT('20101010', A.StartTm, '00'), '%H:%i')) StartTm, ");
            SQL.AppendLine("IF(A.EndTm IS NULL, '', DATE_FORMAT(CONCAT('20101010', A.EndTm, '00'), '%H:%i')) EndTm ");
            SQL.AppendLine("FROM TblEmpLeaveHdr A ");
            SQL.AppendLine("INNER JOIN TblEmployee B ON A.EmpCode = B.EmpCode ");
            SQL.AppendLine("AND A.DocNo = @DocNo ");
            SQL.AppendLine("INNER JOIN TblDepartment C ON B.DeptCode = C.DeptCode ");
            SQL.AppendLine("INNER JOIN TblLeave D ON A.LeaveCode = D.LeaveCode ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "DocNo", 

                         //6-10
                         "DocDt",
                         "EmpName",
                         "DeptName",
                         "Remark",
                         "StartDt",

                         //11-13
                         "StartTm",
                         "EndTm",
                         "LeaveName"



                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpLeave()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),
                            DocDt = Sm.DrStr(dr, c[6]),
                            EmpName = Sm.DrStr(dr, c[7]),
                            DeptName = Sm.DrStr(dr, c[8]),
                            Remark = Sm.DrStr(dr, c[9]),
                            StartDt = Sm.DrStr(dr, c[10]),
                            StartTm = Sm.DrStr(dr, c[11]),
                            EndTm = Sm.DrStr(dr, c[12]),
                            LeaveName = Sm.DrStr(dr, c[13]),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region Detail Signature IMS

            //Dibuat Oleh
            var cmDtlS = new MySqlCommand();

            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                SQLDtlS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtlS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtlS.AppendLine("From ( ");
                SQLDtlS.AppendLine("    Select Distinct ");
                SQLDtlS.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, '1' As DNo, 0 As Level, 'Prepared By,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                SQLDtlS.AppendLine("    From TblEmpLeaveHdr A ");
                SQLDtlS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtlS.AppendLine("    Where  A.DocNo=@DocNo ");
                SQLDtlS.AppendLine(") T1 ");
                SQLDtlS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtlS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtlS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtlS.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtlS.AppendLine("Order By T1.Level; ");

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6
                         "LastupDt"
                        });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {
                        lDtlS.Add(new EmpLeaveSign()
                        {
                            Signature = Sm.DrStr(drDtlS, cDtlS[0]),
                            UserName = Sm.DrStr(drDtlS, cDtlS[1]),
                            PosName = Sm.DrStr(drDtlS, cDtlS[2]),
                            Space = Sm.DrStr(drDtlS, cDtlS[3]),
                            DNo = Sm.DrStr(drDtlS, cDtlS[4]),
                            Title = Sm.DrStr(drDtlS, cDtlS[5]),
                            LastUpDt = Sm.DrStr(drDtlS, cDtlS[6])
                        });
                    }
                }
                drDtlS.Close();
            }
            myLists.Add(lDtlS);

            //Disetujui Oleh
            var cmDtlS2 = new MySqlCommand();

            var SQLDtlS2 = new StringBuilder();
            using (var cnDtlS2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS2.Open();
                cmDtlS2.Connection = cnDtlS2;

                SQLDtlS2.AppendLine("Select A.EmpName, B.PosName  ");
                SQLDtlS2.AppendLine("From TblEmployee A  ");
                SQLDtlS2.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode  ");
                SQLDtlS2.AppendLine("Where A.EmpCode = @EmpCode  ");

                cmDtlS2.CommandText = SQLDtlS2.ToString();
                Sm.CmParam<String>(ref cmDtlS2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtlS2, "@EmpCode", mEmpLeaveReqAutoApprovedByEmpCode);
                var drDtlS2 = cmDtlS2.ExecuteReader();
                var cDtlS2 = Sm.GetOrdinal(drDtlS2, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1
                         "PosName" 
                        });
                if (drDtlS2.HasRows)
                {
                    while (drDtlS2.Read())
                    {

                        lDtlS2.Add(new EmpLeaveSign2()
                        {
                            EmpName = Sm.DrStr(drDtlS2, cDtlS2[0]),
                            PosName = Sm.DrStr(drDtlS2, cDtlS2[1]),
                        });
                    }
                }
                drDtlS2.Close();
            }
            myLists.Add(lDtlS2);

            #endregion

            Sm.PrintReport(mFormPrintOutLeaveRequest, myLists, TableName, false);

        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            mNeglectLeaveCode = Sm.GetParameter("NeglectLeaveCode");
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mLongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            mLongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");
            mEmpLeaveReqAutoApprovedByEmpCode = Sm.GetParameter("EmpLeaveReqAutoApprovedByEmpCode");
            mFormPrintOutLeaveRequest = Sm.GetParameter("FormPrintOutLeaveRequest");
            mDocTitle = Sm.GetParameter("DocTitle");
        }

        public void ComputeDurationDay() 
        {
            if (BtnSave.Enabled)
            {
                TxtDurationDay.EditValue = 0;

                if (Sm.GetDte(DteStartDt).ToString().Length > 0 && Sm.GetDte(DteEndDt).ToString().Length > 0)
                {
                    string
                        StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                        EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

                    DateTime Dt1 = new DateTime(
                        Int32.Parse(StartDt.Substring(0, 4)),
                        Int32.Parse(StartDt.Substring(4, 2)),
                        Int32.Parse(StartDt.Substring(6, 2)),
                        0, 0, 0
                        );
                    DateTime Dt2 = new DateTime(
                        Int32.Parse(EndDt.Substring(0, 4)),
                        Int32.Parse(EndDt.Substring(4, 2)),
                        Int32.Parse(EndDt.Substring(6, 2)),
                        0, 0, 0
                        );

                    decimal holiday = ComputeHoliday(TxtEmpCode.Text, StartDt, EndDt);

                    TxtDurationDay.EditValue = (((Dt2 - Dt1).Days + 1) - holiday);

                }
            }
        }

        public void ComputeDurationHour()
        {
            if (BtnSave.Enabled)
            {
                TxtDurationHour.EditValue = 0;

                if (Sm.GetDte(DteStartDt).ToString().Length > 0 &&
                    Sm.GetTme(TmeStartTm).ToString().Length > 0 &&
                    Sm.GetTme(TmeEndTm).ToString().Length > 0)
                {
                    string
                    StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                    StartTm = Sm.GetTme(TmeStartTm),
                    EndTm = Sm.GetTme(TmeEndTm);

                    DateTime Dt1 = new DateTime(
                        Int32.Parse(StartDt.Substring(0, 4)),
                        Int32.Parse(StartDt.Substring(4, 2)),
                        Int32.Parse(StartDt.Substring(6, 2)),
                        Int32.Parse(StartTm.Substring(0, 2)),
                        Int32.Parse(StartTm.Substring(2, 2)),
                        0
                        );
                    DateTime Dt2 = new DateTime(
                        Int32.Parse(StartDt.Substring(0, 4)),
                        Int32.Parse(StartDt.Substring(4, 2)),
                        Int32.Parse(StartDt.Substring(6, 2)),
                        Int32.Parse(EndTm.Substring(0, 2)),
                        Int32.Parse(EndTm.Substring(2, 2)),
                        0
                        );

                    if (Dt2 < Dt1) Dt2 = Dt2.AddDays(1);
                    double TotalHours = (Dt2 - Dt1).TotalHours;

                    if (ChkBreakInd.Checked) TotalHours -= 1;
                    if (TotalHours < 0) TotalHours = 0;

                    if (TotalHours - (int)(TotalHours) > 0.5)
                        TxtDurationHour.EditValue = (int)(TotalHours) + 1;
                    else
                    {
                        if (TotalHours - (int)(TotalHours) == 0)
                            TxtDurationHour.EditValue = TotalHours;
                        else
                        {
                            if (TotalHours - (int)(TotalHours) > 0 &&
                                TotalHours - (int)(TotalHours) <= 0.5)
                                TxtDurationHour.EditValue = (int)(TotalHours) + 0.5;
                        }
                    }


                }
            }
        }

        private void SetLueLeaveCode(ref DXE.LookUpEdit Lue, string LeaveCode)
        {
            try
            {
                var SQL = new StringBuilder();

                if (LeaveCode.Length == 0)
                {
                    SQL.AppendLine("Select LeaveCode As Col1, LeaveName As Col2 ");
                    SQL.AppendLine("From TblLeave Where ActInd='Y' Order By LeaveName; ");
                }
                else
                    SQL.AppendLine(
                        "Select LeaveCode As Col1, LeaveName As Col2 " +
                        "From TblLeave Where LeaveCode='" + LeaveCode + "';"
                        );

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private static decimal ComputeHoliday(string EmpCode, string Dt1, string Dt2)
        {
            var HolidayAmt = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            try
            {
                SQL.AppendLine("Select Count(1) ");
                SQL.AppendLine("From TblEmpWorkSchedule A ");
                SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode = B.WSCode ");
                SQL.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL.AppendLine("And B.HolidayInd='Y' ");
                SQL.AppendLine("And A.Dt Between @Dt1 And @Dt2;");

                Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
                Sm.CmParam<String>(ref cm, "@Dt2", Dt2);

                cm.CommandText = SQL.ToString();

                HolidayAmt = Sm.GetValue(cm);

                if (HolidayAmt.Length > 0) return decimal.Parse(HolidayAmt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return 0m;
        }

        private string GetBreakSchedule()
        {
            if (TxtEmpCode.Text.Length == 0 || Sm.GetDte(DteStartDt).Length == 0) return string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(In2, Out2) As Break ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkschedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt=@Dt And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And In2 Is Not Null And Out2 Is Not Null ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));

            var b = Sm.GetValue(cm);
            if (b.Length == 0) 
                return string.Empty;
            else
                return
                    " (" +
                    Sm.Left(b, 2) + ":" + b.Substring(2, 2) + " - " +
                    b.Substring(4, 2) + ":" + Sm.Right(b, 2) +
                    ")";
        }

        private void ComputeLeaveRemainingDay()
        {
            var LeaveCode = Sm.GetLue(LueLeaveCode);
           
            var l = new List<EmployeeLeave>();
            string
                Dt1 = Sm.GetDte(DteStartDt),
                Dt2 = Sm.GetDte(DteEndDt);

            if(TxtEmpCode.Text.Length>0)
            {
                l.Add(new EmployeeLeave
                {
                    EmpCode = TxtEmpCode.Text,
                    EmpName = TxtEmpName.Text,
                    LeaveStartDt = Sm.Left(Dt1, 8),
                    LeaveEndDt = Sm.Left(Dt2, 8),
                    JoinDt = Sm.GetDte(DteJoinDt).Substring(0, 8),
                    Period = string.Empty,
                    IsValid = false
                });
            }
            
            if (l.Count <= 0) return;

            var RLPType = Sm.GetParameter("RLPType");

            if (RLPType == "1" || RLPType == "3")
            {
                if (Dt2.Length > 0 && !Sm.CompareStr(Sm.Left(Dt1, 4), Sm.Left(Dt2, 4)))
                    return;
                for (int i = 0; i < l.Count; i++)
                    l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
            }

            if (RLPType == "2")
            {
                if (Dt2.Length > 0)
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (Sm.CompareDtTm(Sm.Right(l[i].LeaveStartDt, 4), Sm.Right(l[i].JoinDt, 4)) < 0)
                            l[i].Period = (int.Parse(Sm.Left(l[i].LeaveStartDt, 4)) - 1).ToString();
                        else
                            l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
                        var v = (int.Parse(l[i].Period) + 1).ToString() + Sm.Right(l[i].JoinDt, 4);
                        if (Sm.CompareDtTm(l[i].LeaveEndDt, v) < 0)
                            l[i].IsValid = true;
                        else
                            return;
                    }
                }
            }

            string Period2 = Sm.Left(Dt1, 4);
            string Period1 = (int.Parse(Period2) - 1).ToString();

            var l2 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Period == Period2)
                {
                    l2.Add(new EmployeeResidualLeave
                    {
                        EmpCode = l[i].EmpCode,
                        EmpName = l[i].EmpName,
                        JoinDt = string.Empty,
                        ActiveDt = string.Empty,
                        StartDt = string.Empty,
                        EndDt = string.Empty,
                        LeaveDay = 0m,
                        UsedDay = 0m,
                        RemainingDay = 0m
                    });
                }
            }
            if (Sm.CompareStr(LeaveCode, mAnnualLeaveCode))
                Sm.GetEmpAnnualLeave(ref l2, Period2);
            
            if (Sm.CompareStr(LeaveCode, mLongServiceLeaveCode))
                Sm.GetEmpLongServiceLeave(ref l2, Period2);

            if (Sm.CompareStr(LeaveCode, mLongServiceLeaveCode2))
                Sm.GetEmpLongServiceLeave2(ref l2, Period2);

            var l3 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Period == Period1)
                {
                    l3.Add(new EmployeeResidualLeave
                    {
                        EmpCode = l[i].EmpCode,
                        EmpName = l[i].EmpName,
                        JoinDt = string.Empty,
                        ActiveDt = string.Empty,
                        StartDt = string.Empty,
                        EndDt = string.Empty,
                        LeaveDay = 0m,
                        UsedDay = 0m,
                        RemainingDay = 0m
                    });
                }
            }

            if (Sm.CompareStr(LeaveCode, mAnnualLeaveCode))
                Sm.GetEmpAnnualLeave(ref l3, Period1);
            
            if (Sm.CompareStr(LeaveCode, mLongServiceLeaveCode))
                Sm.GetEmpLongServiceLeave(ref l3, Period1);

            if (Sm.CompareStr(LeaveCode, mLongServiceLeaveCode2))
                Sm.GetEmpLongServiceLeave2(ref l3, Period1);
            
            var LeaveDuration = decimal.Parse(TxtDurationDay.Text);

            if (l2.Count > 0)
            {
                for (int i = 0; i < l2.Count; i++)
                {
                    TxtLeaved.EditValue = l2[i].RemainingDay;
                    break;
                }
            }

            if (l3.Count > 0)
            {
                for (int i = 0; i < l3.Count; i++)
                {
                    TxtLeaved.EditValue = l3[i].RemainingDay;
                    break;
                }
            }
        }

        #endregion

        #region Leave

        //private void ProcessLeaveSummary(ref bool IsInvalid)
        //{
        //    IsInvalid = true;

        //    var l = new List<LeaveSummary1>();
        //    var LeaveCodeTemp = Sm.GetLue(LueLeaveCode);
        //    var Yr1Temp = Sm.Left(Sm.GetDte(DteStartDt), 4);
        //    var Yr2Temp = (int.Parse(Yr1Temp) + 1).ToString();

        //    var AnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
        //    var LongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
        //    var LongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");

        //    bool
        //        IsUseAnnual = AnnualLeaveCode.Length > 0,
        //        IsUseLongService = LongServiceLeaveCode.Length > 0,
        //        IsUseLongService2 = LongServiceLeaveCode2.Length > 0;

        //    var AnnualLeaveNoOfYrToActive = 100m;
        //    var LongServiceLeaveNoOfYrToActive = 100m;
        //    var LongServiceLeaveNoOfYrToActive2 = 100m;
        //    var LongServiceLeaveNoOfYr = 100m;

        //    var Value = string.Empty;

        //    if (IsUseAnnual)
        //    {
        //        Value = Sm.GetParameter("AnnualLeaveNoOfYrToActive");
        //        if (Value.Length > 0) AnnualLeaveNoOfYrToActive = decimal.Parse(Value);
        //    }

        //    if (IsUseLongService)
        //    {
        //        Value = Sm.GetParameter("LongServiceLeaveNoOfYrToActive");
        //        if (Value.Length > 0) LongServiceLeaveNoOfYrToActive = decimal.Parse(Value);
        //    }

        //    if (IsUseLongService2)
        //    {
        //        Value = Sm.GetParameter("LongServiceLeaveNoOfYrToActive2");
        //        if (Value.Length > 0) LongServiceLeaveNoOfYrToActive2 = decimal.Parse(Value);
        //    }

        //    if (IsUseLongService || IsUseLongService2)
        //    {
        //        Value = Sm.GetParameter("LongServiceLeaveNoOfYr");
        //        if (Value.Length > 0) LongServiceLeaveNoOfYr = decimal.Parse(Value);
        //    }

        //    //bool IsHutangCuti = false;
        //    //bool IsInvalid = false;
        //    //decimal DurationDay = decimal.Parse(TxtDurationDay.Text);
        //    //var EmpCodeTemp = TxtEmpCode.Text;
        //    //var EndDt = Sm.GetDte(DteEndDt);
        //    //if (EndDt.Length >= 8) EndDt = Sm.Left(EndDt, 8);

        //    if (IsLeaveCodesNeedToProcessed(LeaveCodeTemp))
        //    {
        //        IsInvalid = false;
        //        return;
        //    }

        //    l.Add(new LeaveSummary1()
        //    {
        //        EmpCode = TxtEmpCode.Text,
        //        EmpName = TxtEmpName.Text,
        //        InitialDt = string.Empty,
        //        IsSummary1Existed = false,
        //        IsSummary2Existed = false,
        //        LeaveCode1 = string.Empty,
        //        LeaveCode2 = string.Empty
        //    });

        //    ProcessLeaveSummary1(ref l, Yr1Temp, Yr2Temp);

        //    for (int i = 0; i < l.Count; i++)
        //    {
        //        if (l[i].LeaveCode1.Length > 0)
        //        {
        //            if (!Sm.CompareStr(LeaveCodeTemp, l[i].LeaveCode1))
        //            {
        //                Sm.StdMsg(mMsgType.Warning, "Invalid leave type.");
        //                return;
        //            }
        //        }
        //        else
        //        { 
                
        //        }

        //        if (l[i].LeaveCode2.Length == 0)
        //        {

        //        }                
        //    }

        //    IsInvalid = false;


        //    if (IsLeaveSummaryExisted(YrTemp, EmpCodeTemp))
        //    {
        //        if (IsLeaveSummaryInvalid(YrTemp, EmpCodeTemp, LeaveCodeTemp)) return;
        //        GetLeaveSummary(ref l, EmpCodeTemp, TxtEmpName.Text, YrTemp);
        //    }
        //    else
        //    {
        //        GetLeaveSummary2(ref l, LeaveCodeTemp, EmpCodeTemp, TxtEmpName.Text, YrTemp);

        //        //if (Sm.CompareStr(LeaveCodeTemp, mAnnualLeaveCode))
        //        //Sm.SetLeaveSummaryForAnnualLeave(ref l, YrTemp);

        //        //if (Sm.CompareStr(LeaveCodeTemp, mLongServiceLeaveCode))
        //        //    Sm.SetLeaveSummaryForLongServiceLeave(ref l, YrTemp, string.Empty);

        //        //if (Sm.CompareStr(LeaveCodeTemp, mLongServiceLeaveCode2))
        //        //    Sm.SetLeaveSummaryForLongServiceLeave(ref l, YrTemp, "2");
        //    }

        //    if (IsLeaveSummaryExisted(Yr2Temp, EmpCodeTemp))
        //    {
        //        GetLeaveSummary(ref l, EmpCodeTemp, TxtEmpName.Text, Yr2Temp);
        //    }
        //    else
        //    {
        //        LeaveCode2Temp = GetLeaveCodeNextYr(
        //            Yr2Temp,
        //            l[0].EmpCode,
        //            l[0].InitialDt,
        //            l[0].StartDt,
        //            l[0].EndDt
        //        );

                
        //    }

            


        //    if (l.Count > 0)
        //    {
        //        for (int i = 0; i < l.Count; i++)
        //        {
        //            IsHutangCuti = false;
        //            if (Sm.CompareDtTm(EndDt, l[i].EndDt) > 0)
        //            {
        //                l[i].IsInvalid = true;
        //                IsInvalid = true;
        //                break;
        //            }
        //            if (l[i].Balance == 0m)
        //            {
        //                IsHutangCuti = true;
        //            }
        //            else
        //            {
        //                if (l[i].Balance < DurationDay)
        //                {
        //                    l[i].NoOfDays2 = l[i].NoOfDays1 - l[i].NoOfDays3 - l[i].NoOfDays4;
        //                    l[i].Balance = l[i].NoOfDays1 - l[i].NoOfDays2 - l[i].NoOfDays3 - l[i].NoOfDays4; 
        //                }
        //                else
        //                {

        //                }
        //            }

        //        }
        //    }
        //}

        //private bool IsLeaveCodesNeedToProcessed(string LeaveCode)
        //{
        //    return Sm.IsDataExist(
        //        "Select 1 From TblLeave " +
        //        "Where LeaveCode=@Param " +
        //        "And Find_In_Set(LeaveCode, " +
        //        "(Select ParValue From TblParameter Where ParCode='ProcessedLeaveCodes' And ParValue is Not Null)); ",
        //        LeaveCode);        
        //}

        //private bool IsLeaveSummaryExisted(string Yr, string EmpCode)
        //{
        //    return Sm.IsDataExist(
        //        "Select 1 From TblLeaveSummary " +
        //        "Where Yr=@Param1 " +
        //        "And EmpCode=@Param2;",
        //        Yr, EmpCode, string.Empty);
        //}

        //private bool IsLeaveSummaryInvalid(string Yr, string EmpCode, string LeaveCode)
        //{
        //    return !Sm.IsDataExist(
        //        "Select 1 From TblLeaveSummary " +
        //        "Where Yr=@Param1 " +
        //        "And EmpCode=@Param2 " +
        //        "And LeaveCode=@Param3;",
        //        Yr, EmpCode, LeaveCode);
        //}

        //private void ProcessLeaveSummary1(ref List<LeaveSummary1> l, string pYr1, string pYr2)
        //{
        //    var cm = new MySqlCommand();
        //    string
        //        Filter = string.Empty,
        //        EmpCode = string.Empty,
        //        LeaveStartDt = string.Empty,
        //        LeaveCode1 = string.Empty,
        //        LeaveCode2 = string.Empty;

        //    Sm.CmParam<string>(ref cm, "@Yr1", pYr1);
        //    Sm.CmParam<string>(ref cm, "@Yr2", pYr2);
            
        //    for (int i = 0;i<l.Count;i++)
        //    {
        //        if (Filter.Length > 0) Filter += " Or ";
        //        Filter += "(T.EmpCode=@EmpCode0" + i.ToString() + ") ";
        //        Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), l[i].EmpCode);
        //    }

        //    if (Filter.Length!=0)
        //        Filter += " Where (" + Filter + ") ";
        //    else
        //        Filter += " Where 0=1 ";
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select T.EmpCode, T.LeaveStartDt, ");
        //    SQL.AppendLine("(Select LeaveCode From TblLeaveSummary Where EmpCode=T.EmpCode And Yr=@Yr1) As LeaveCode1, ");
        //    SQL.AppendLine("(Select LeaveCode From TblLeaveSummary Where EmpCode=T.EmpCode And Yr=@Yr2) As LeaveCode2 ");
        //    SQL.AppendLine("From TblEmployee T ");
        //    SQL.AppendLine(Filter);
        //    SQL.AppendLine(";");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
                
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr,
        //            new string[] { 
        //                "EmpCode", 
        //                "LeaveStartDt", "LeaveCode1", "LeaveCode2" 
        //            });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                EmpCode = Sm.DrStr(dr, c[0]);
        //                LeaveStartDt = Sm.DrStr(dr, c[1]);
        //                LeaveCode1 = Sm.DrStr(dr, c[2]);
        //                LeaveCode2 = Sm.DrStr(dr, c[3]);
        //                for (int i = 0; i < l.Count; i++)
        //                {
        //                    if (Sm.CompareStr(l[i].EmpCode, EmpCode))
        //                    {
        //                        l[i].InitialDt = LeaveStartDt;
        //                        l[i].IsSummary1Existed = LeaveCode1.Length > 0;
        //                        l[i].IsSummary2Existed = LeaveCode2.Length > 0;
        //                        l[i].LeaveCode1 = LeaveCode1;
        //                        l[i].LeaveCode2 = LeaveCode2;
        //                    }
        //                }
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        //private void GetLeaveSummary(ref List<LeaveSummary> l, string pEmpCode, string pEmpName, string pYr)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select LeaveCode, InitialDt, StartDt, EndDt, ");
        //    SQL.AppendLine("NoOfDays1, NoOfDays2, NoOfDays3, NoOfDays4, Balance ");
        //    SQL.AppendLine("From TblLeaveSummary Where EmpCode=@EmpCode And Yr=@Yr; ");
            
        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        var cm = new MySqlCommand()
        //        {
        //            Connection = cn,
        //            CommandTimeout = 600,
        //            CommandText = SQL.ToString()
        //        };
        //        Sm.CmParam<string>(ref cm, "@EmpCode", pEmpCode);
        //        Sm.CmParam<string>(ref cm, "@Yr", pYr);
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //        { 
        //            //0
        //            "LeaveCode", 

        //            //1-5
        //            "InitialDt", "StartDt", "EndDt", "NoOfDays1", "NoOfDays2", 

        //            //6-8
        //            "NoOfDays3", "NoOfDays4", "Balance" 
        //        });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new LeaveSummary()
        //                {
        //                    EmpCode = pEmpCode,
        //                    EmpName = pEmpName,
        //                    Yr = pYr,
        //                    LeaveCode= Sm.DrStr(dr, c[0]),
        //                    InitialDt = Sm.DrStr(dr, c[1]),
        //                    StartDt = Sm.DrStr(dr, c[2]),
        //                    EndDt = Sm.DrStr(dr, c[3]),
        //                    NoOfDays1 = Sm.DrDec(dr, c[4]),
        //                    NoOfDays2 = Sm.DrDec(dr, c[5]),
        //                    NoOfDays3 = Sm.DrDec(dr, c[6]),
        //                    NoOfDays4 = Sm.DrDec(dr, c[7]),
        //                    Balance = Sm.DrDec(dr, c[8]),
        //                    IsInvalid = false
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        //private void GetLeaveSummary2(ref List<LeaveSummary> l, string pLeaveCode, string pEmpCode, string pEmpName, string pYr)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select LeaveStartDt, ");
        //    SQL.AppendLine("Case When Left(A.LeaveStartDt, 4)=@Yr Then A.LeaveStartDt Else Concat(@Yr, '0101') End As StartDt, ");
        //    SQL.AppendLine("Concat(@Yr, '1231') End As EndDt, ");
        //    SQL.AppendLine("B.NoOfDay ");
        //    SQL.AppendLine("From TblEmployee A ");
        //    SQL.AppendLine("Inner Join TblLeave B On B.LeaveCode=@LeaveCode ");
        //    SQL.AppendLine("Where A.EmpCode=@EmpCode;");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        var cm = new MySqlCommand()
        //        {
        //            Connection = cn,
        //            CommandTimeout = 600,
        //            CommandText = SQL.ToString()
        //        };
        //        Sm.CmParam<string>(ref cm, "@Yr", pYr);
        //        Sm.CmParam<string>(ref cm, "@LeaveCode", pLeaveCode);
        //        Sm.CmParam<string>(ref cm, "@EmpCode", pEmpCode);
                
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, 
        //            new string[] { "LeaveStartDt", "StartDt", "EndDt", "NoOfDay" }
        //            );
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new LeaveSummary()
        //                {
        //                    EmpCode = pEmpCode,
        //                    EmpName = pEmpName,
        //                    Yr = pYr,
        //                    LeaveCode = pLeaveCode,
        //                    InitialDt = Sm.DrStr(dr, c[0]),
        //                    StartDt = Sm.DrStr(dr, c[1]),
        //                    EndDt = Sm.DrStr(dr, c[2]),
        //                    NoOfDays1 = Sm.DrDec(dr, c[3]),
        //                    NoOfDays2 = 0m,
        //                    NoOfDays3 = 0m,
        //                    NoOfDays4 = 0m,
        //                    Balance = Sm.DrDec(dr, c[3]),
        //                    IsInvalid = false
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueLeaveCode, "Leave"))
            {
                if (!Sm.IsDteEmpty(DteStartDt, "Start Leave"))
                {
                    Sm.FormShowDialog(new FrmEmpLeaveDlg(this, Sm.GetLue(LueLeaveCode), Sm.GetDte(DteStartDt).Substring(0, 4)));
                }
            }
        }

        private void BtnDurationHrDefault_Click(object sender, EventArgs e)
        {
            ComputeDurationHour();
        }

        #endregion

        #region Misc Control Event

        private void LueLeaveCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearData2();
                Sm.RefreshLookUpEdit(LueLeaveCode, new Sm.RefreshLue2(SetLueLeaveCode), string.Empty);
            }
        }

        private void LueLeaveType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLeaveType, new Sm.RefreshLue2(Sl.SetLueOption), "LeaveType");

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { DteStartDt, DteEndDt, TmeStartTm, TmeEndTm });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtDurationDay, TxtDurationHour }, 0);

            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteEndDt, TmeStartTm, TmeEndTm, TxtDurationHour, ChkBreakInd }, true);
            BtnDurationHrDefault.Enabled = false;

            if (Sm.GetLue(LueLeaveType) == "F")
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteEndDt }, false);
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TmeStartTm, TmeEndTm, TxtDurationHour, ChkBreakInd }, false);
                BtnDurationHrDefault.Enabled = true;
            }
        }

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {

            if (BtnSave.Enabled)
            {
                DteEndDt.EditValue = DteStartDt.EditValue;
                if (TxtDocNo.Text.Length == 0)
                    ClearData2();
                ComputeDurationDay();
            }
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                    ClearData2();
                ComputeDurationDay();
            }
        }

        private void TmeStartHr_EditValueChanged(object sender, EventArgs e)
        {
            TmeEndTm.EditValue = TmeStartTm.EditValue;
            ComputeDurationHour();
        }

        private void TmeEndTm_EditValueChanged(object sender, EventArgs e)
        {
            ComputeDurationHour();
        }

        //private void DteStartDt_Validated(object sender, EventArgs e)
        //{
        //    DteEndDt.EditValue = DteStartDt.EditValue;
        //    ComputeDurationDay();
        //}

        //private void DteEndDt_Validated(object sender, EventArgs e)
        //{
        //    ComputeDurationDay();
        //}

        //private void TmeStartTm_Validated(object sender, EventArgs e)
        //{
        //    TmeEndTm.EditValue = TmeStartTm.EditValue;
        //    ComputeDurationHour();
        //}

        //private void TmeEndTm_Validated(object sender, EventArgs e)
        //{
        //    ComputeDurationHour();
        //}

        private void TxtLeaved_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FormatNum(TxtLeaved.Text, 1);
        }

        private void ChkBreakInd_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ChkBreakInd.Text = "Include Break Time";
                if (ChkBreakInd.Checked)
                    ChkBreakInd.Text += GetBreakSchedule(); 
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void TxtDurationHour_Validated(object sender, EventArgs e)
        {
            Sm.FormatNum(TxtDurationHour.Text, 0);
        }

        #endregion

        #endregion

        #region Class

        private class EmpLeave
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string Remark { get; set; }
            public string StartDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public string LeaveName { get; set; }

        }

        internal class EmpLeaveDtl
        {
            public string DNo { get; set; }
            public string LeaveDt { get; set; }
            public bool IsHoliday { get; set; }
        }

        private class EmployeeLeave
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string LeaveStartDt { get; set; }
            public string LeaveEndDt { get; set; }
            public string JoinDt { get; set; }
            public string Period { get; set; }
            public bool IsValid { get; set; }
        }

        private class LeaveSummary1
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string InitialDt { get; set; }
            public bool IsSummary1Existed { get; set; }
            public bool IsSummary2Existed { get; set; }
            public string LeaveCode1 { get; set; }
            public string LeaveCode2 { get; set; }
        }

        private class EmpLeaveSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class EmpLeaveSign2
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }


        #endregion
    }
}
