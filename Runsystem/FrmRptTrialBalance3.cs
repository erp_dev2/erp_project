#region Update

/*
    20/03/2023 [WED/PHT] new apps, kalkulasi selalu debit kurangi kredit tanpa melihat natura. ada balance OB
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptTrialBalance3 : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;
        //private List<String> mlCostCenter = null;

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNoForCurrentEarning = "3.3",
            mAcNoForCurrentEarning2 = "9",
            mAcNoForIncome = "4",
            mAcNoForCost = "5",
            mAcNoForActiva = "1",
            mAcNoForPassiva = "2",
            mAcNoForCapital = "3",
            mAcNoForIncome2 = string.Empty,
            mAcNoForCurrentEarning3 = string.Empty,
            mCOAAssetStartYr = string.Empty,
            mCOAAssetAcNo = string.Empty,
            mAccountingRptStartFrom = string.Empty,
            mCurrentEarningFormulaType = "1",
            mMaxAccountCategory = "9",
            mFormulaForComputeProfitLoss = "0";

        private bool
            mIsCOAAssetUseStartYr = false,
            mIsEntityMandatory = false,
            mIsReportingFilterByEntity = false,
            mIsAccountingRptUseJournalPeriod = false,
            mIsFicoUseMultiEntityFilter = false,
            mIsFicoUseCOALevelFilter = false,
            //mIsFicoUseMultiProfitCenterFilter = false,
            mIsRptTrialBalanceUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsRptAccountingShowJournalMemorial = false,
            mIsRptFinancialNotUseStartFromFilter = false,
            mIsPLComputeFromFicoSetting = false,
            mPrintFicoSettingLastYearBasedOnFilterMonth = false,
            mIsRptTBCalculateFicoNetIncome = false;

        #endregion

        #region Constructor

        public FrmRptTrialBalance3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();

                if (mIsAccountingRptUseJournalPeriod) LblPeriod.ForeColor = Color.Red;

                if (!mIsFicoUseCOALevelFilter) LblLevel.Visible = LblLevel2.Visible = LueLevelFrom.Visible = LueLevelTo.Visible = false;
                SetLueLevelFromTo(ref LueLevelFrom);
                SetLueLevelFromTo(ref LueLevelTo);

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLuePeriod(LuePeriod);
                if (mIsRptFinancialNotUseStartFromFilter)
                    Sm.SetControlReadOnly(LueStartFrom, true);
                else
                {
                    if (mAccountingRptStartFrom.Length > 0)
                    {
                        Sl.SetLueYr(LueStartFrom, mAccountingRptStartFrom);
                        Sm.SetLue(LueStartFrom, mAccountingRptStartFrom);
                    }
                    else
                    {
                        Sl.SetLueYr(LueStartFrom, string.Empty);
                        Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                    }
                }
                if (mIsReportingFilterByEntity) SetLueEntCode(ref LueEntCode);
                if (mIsRptTrialBalanceUseProfitCenter)
                {
                    mlProfitCenter = new List<String>();
                    //mlCostCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                SetGrd();
                LueYr.Focus();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAccountingShowJournalMemorial', 'IsRptFinancialNotUseStartFromFilter', 'IsPLComputeFromFicoSetting','IsRptTBCalculateFicoNetIncome', 'FormulaForComputeProfitLoss', ");
            SQL.AppendLine("'AcNoForCurrentEarning', 'AcNoForCurrentEarning2', 'AcNoForCurrentEarning3','AcNoForIncome', 'AcNoForIncome2', ");
            SQL.AppendLine("'IsFicoUseMultiEntityFilter', 'IsFicoUseCOALevelFilter', 'IsAccountingRptUseJournalPeriod','IsRptTrialBalanceUseProfitCenter', 'PrintFicoSettingLastYearBasedOnFilterMonth', ");
            SQL.AppendLine("'IsReportingFilterByEntity', 'IsEntityMandatory', 'IsCOAAssetUseStartYr','CurrentEarningFormulaType', 'MaxAccountCategory', ");
            SQL.AppendLine("'COAAssetAcNo', 'AccountingRptStartFrom', 'AcNoForActiva','AcNoForPassiva', 'AcNoForCapital', ");
            SQL.AppendLine("'AcNoForCost', 'COAAssetStartYr' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFicoUseMultiEntityFilter": mIsFicoUseMultiEntityFilter = ParValue == "Y"; break;
                            case "IsFicoUseCOALevelFilter": mIsFicoUseCOALevelFilter = ParValue == "Y"; break;
                            case "IsAccountingRptUseJournalPeriod": mIsAccountingRptUseJournalPeriod = ParValue == "Y"; break;
                            case "IsRptTrialBalanceUseProfitCenter": mIsRptTrialBalanceUseProfitCenter = ParValue == "Y"; break;
                            case "PrintFicoSettingLastYearBasedOnFilterMonth": mPrintFicoSettingLastYearBasedOnFilterMonth = ParValue == "Y"; break;
                            case "IsReportingFilterByEntity": mIsReportingFilterByEntity = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsCOAAssetUseStartYr": mIsCOAAssetUseStartYr = ParValue == "Y"; break;
                            case "IsRptAccountingShowJournalMemorial": mIsRptAccountingShowJournalMemorial = ParValue == "Y"; break;
                            case "IsRptFinancialNotUseStartFromFilter": mIsRptFinancialNotUseStartFromFilter = ParValue == "Y"; break;
                            case "IsPLComputeFromFicoSetting": mIsPLComputeFromFicoSetting = ParValue == "Y"; break;
                            case "IsRptTBCalculateFicoNetIncome": mIsRptTBCalculateFicoNetIncome = ParValue == "Y"; break;

                            //string
                            case "COAAssetAcNo": mCOAAssetAcNo = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "AcNoForActiva": mAcNoForActiva = ParValue; break;
                            case "AcNoForPassiva": mAcNoForPassiva = ParValue; break;
                            case "AcNoForCapital": mAcNoForCapital = ParValue; break;
                            case "COAAssetStartYr": mCOAAssetStartYr = ParValue; break;
                            case "AcNoForCost": mAcNoForCost = ParValue; break;
                            case "FormulaForComputeProfitLoss": mFormulaForComputeProfitLoss = ParValue; break;
                            case "CurrentEarningFormulaType": mCurrentEarningFormulaType = ParValue; break;
                            case "MaxAccountCategory": mMaxAccountCategory = ParValue; break;
                            case "AcNoForIncome2": mAcNoForIncome2 = ParValue; break;
                            case "AcNoForIncome": mAcNoForIncome = ParValue; break;
                            case "AcNoForCurrentEarning": mAcNoForCurrentEarning = ParValue; break;
                            case "AcNoForCurrentEarning2": mAcNoForCurrentEarning2 = ParValue; break;
                            case "AcNoForCurrentEarning3": mAcNoForCurrentEarning3 = ParValue; break;


                        }
                    }
                }
                dr.Close();
            }
            if (mMaxAccountCategory.Length == 0) mMaxAccountCategory = "5";
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 250;
            Grd1.Header.Cells[0, 0].Value = "Account#";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 250;
            Grd1.Header.Cells[0, 1].Value = "Alias";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Account Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[1, 3].Value = "Opening Balance";
            Grd1.Header.Cells[1, 3].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 3].SpanCols = 3;
            Grd1.Header.Cells[0, 3].Value = "Debit";
            Grd1.Header.Cells[0, 4].Value = "Credit";
            Grd1.Header.Cells[0, 5].Value = "Balance";
            Grd1.Cols[3].Width = 130;
            Grd1.Cols[4].Width = 130;
            Grd1.Cols[5].Width = 130;

            Grd1.Header.Cells[1, 6].Value = "Month To Date";
            Grd1.Header.Cells[1, 6].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 6].SpanCols = 3;
            Grd1.Header.Cells[0, 6].Value = "Debit";
            Grd1.Header.Cells[0, 7].Value = "Credit";
            Grd1.Header.Cells[0, 8].Value = "Balance";
            Grd1.Cols[6].Width = 130;
            Grd1.Cols[7].Width = 130;
            Grd1.Cols[8].Width = 130;

            Grd1.Header.Cells[1, 9].Value = "Current Month";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 3;
            Grd1.Header.Cells[0, 9].Value = "Debit";
            Grd1.Header.Cells[0, 10].Value = "Credit";
            Grd1.Header.Cells[0, 11].Value = "Balance";
            Grd1.Cols[9].Width = 130;
            Grd1.Cols[10].Width = 130;
            Grd1.Cols[11].Width = 130;

            Grd1.Header.Cells[1, 12].Value = "Year To Date";
            Grd1.Header.Cells[1, 12].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 12].SpanCols = 3;
            Grd1.Header.Cells[0, 12].Value = "Debit";
            Grd1.Header.Cells[0, 13].Value = "Credit";
            Grd1.Header.Cells[0, 14].Value = "Balance";
            Grd1.Cols[12].Width = 130;
            Grd1.Cols[13].Width = 130;
            Grd1.Cols[14].Width = 130;

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 }, 2);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        private bool IsProfitCenterInvalid()
        {
            if (!mIsRptTrialBalanceUseProfitCenter) return false;

            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Multi profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            var v = GetCcbProfitCenterCode();
            if (v.Contains("Consolidate"))
            {
                var split = v.Split(',');
                var mFlag = false;

                if (split.Length > 1)
                {
                    foreach (var x in split)
                    {
                        if (x.Length == 0)
                        {
                            mFlag = true;
                            break;
                        }
                    }

                    if (!mFlag)
                    {
                        Sm.StdMsg(mMsgType.Warning, "if consolidate already choosen, You can't choose another profit center.");
                        CcbProfitCenterCode.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsLevelFromToInvalid()
        {
            if (!mIsFicoUseCOALevelFilter) return false;

            string mLevelFrom = string.Empty, mLevelTo = string.Empty;

            mLevelFrom = Sm.GetLue(LueLevelFrom);
            mLevelTo = Sm.GetLue(LueLevelTo);

            if (mLevelTo.Length > 0 || mLevelFrom.Length > 0)
            {
                if (mLevelFrom.Length > 0 && mLevelTo.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Level to is empty.");
                    LueLevelTo.Focus();
                    return true;
                }

                if (mLevelFrom.Length == 0 && mLevelTo.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Level from is empty.");
                    LueLevelFrom.Focus();
                    return true;
                }

                if (mLevelTo.Length > 0 && mLevelFrom.Length > 0)
                {
                    if (Int32.Parse(mLevelFrom) > Int32.Parse(mLevelTo))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Level from should be smaller than level to.");
                        LueLevelFrom.Focus();
                        return true;
                    }
                }
            }

            return false;
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptTrialBalanceUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();
            //mlCostCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);

            //if (mlProfitCenter.Count > 0)
            //{
            //    IsCompleted = false;
            //    IsFirst = true;
            //    while (!IsCompleted) 
            //        SetCostCenter(ref IsFirst, ref IsCompleted);
            //}
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    //if (Filter2.Length > 0) Filter2 += " And ";
                    //Filter2 += " (ProfitCenterCode<>@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                    //if (Filter2.Length != 0) SQL.AppendLine("    And (" + Filter2 + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                (mIsAccountingRptUseJournalPeriod && Sm.IsLueEmpty(LuePeriod, "Period")) ||
                IsLevelFromToInvalid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var Yr = Sm.GetLue(LueYr);
            var Mth = (mIsAccountingRptUseJournalPeriod) ? Sm.GetLue(LuePeriod) : Sm.GetLue(LueMth);
            var StartFrom = Sm.GetLue(LueStartFrom);
            var IsFilterByEntity = ChkEntCode.Checked;

            try
            {
                var lCOA = new List<COA>();
                var lEntityCOA = new List<EntityCOA>();
                var lCOAPLSettingHdr = new List<COAProfitLossSettingHdr>();
                var lCOAPLSettingDtl = new List<COAProfitLossSettingDtl>();
                string AcNoPL = string.Empty;

                Sm.ClearGrd(Grd1, false);

                SetProfitCenter();

                Process1(ref lCOA);
                if (mFormulaForComputeProfitLoss == "2")
                {
                    GetCOAProfitLossSettingHdr(ref lCOAPLSettingHdr);
                    GetCOAProfitLossSettingDtl(ref lCOAPLSettingDtl);
                    AcNoPL = GetSelectedCOAPLSetting(ref lCOAPLSettingHdr);
                }
                if (IsFilterByEntity)
                    Process8(ref lEntityCOA);
                if (lCOA.Count > 0)
                {
                    if (StartFrom.Length > 0)
                    {
                        Process2(ref lCOA, StartFrom);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, StartFrom);
                        else
                            Process3(ref lCOA, Yr, Mth, StartFrom);
                    }
                    else
                    {
                        Process2(ref lCOA, Yr);
                        if (mIsRptAccountingShowJournalMemorial)
                            Process3_JournalMemorial(ref lCOA, Yr, Mth, string.Empty);
                        else
                            Process3(ref lCOA, Yr, Mth, string.Empty);
                    }
                    Process4(ref lCOA, Yr, Mth);
                    if (mFormulaForComputeProfitLoss != "2") Process5(ref lCOA);
                    if (mIsRptTBCalculateFicoNetIncome) Process5e(ref lCOA);
                    Process6(ref lCOA);
                    if (mFormulaForComputeProfitLoss == "2")
                    {
                        ComputeProfitLoss(ref lCOAPLSettingHdr, ref lCOAPLSettingDtl, ref lCOA);
                        //ComputeProfitLoss2(ref lCOAPLSettingHdr, ref lCOA);
                        Process12(ref lCOA, ref lCOAPLSettingHdr, AcNoPL);
                    }

                    #region Filtered By Entity

                    if (IsFilterByEntity)
                    {
                        if (lEntityCOA.Count > 0)
                        {
                            if (lCOA.Count > 0)
                            {
                                Grd1.BeginUpdate();
                                Grd1.Rows.Count = 0;

                                iGRow r;

                                r = Grd1.Rows.Add();
                                r.Level = 0;
                                r.TreeButton = iGTreeButtonState.Visible;
                                r.Cells[0].Value = "COA";
                                for (var j = 0; j < lEntityCOA.Count; j++)
                                {
                                    for (var i = 0; i < lCOA.Count; i++)
                                    {
                                        if (lCOA[i].AcNo == lEntityCOA[j].AcNo)
                                        {
                                            if (Sm.GetLue(LueLevelFrom).Length > 0 && Sm.GetLue(LueLevelTo).Length > 0)
                                            {
                                                if (lCOA[i].Level >= Int32.Parse(Sm.GetLue(LueLevelFrom)) &&
                                                    lCOA[i].Level <= Int32.Parse(Sm.GetLue(LueLevelTo)))
                                                {
                                                    r = Grd1.Rows.Add();
                                                    r.Level = lCOA[i].Level;
                                                    r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                    r.Cells[0].Value = lCOA[i].AcNo;
                                                    r.Cells[1].Value = lCOA[i].Alias;
                                                    r.Cells[2].Value = lCOA[i].AcDesc;
                                                    for (var c = 3; c < 15; c++)
                                                        r.Cells[c].Value = 0m;
                                                    r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                                    r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                                    r.Cells[5].Value = lCOA[i].OBBalance;
                                                    r.Cells[6].Value = lCOA[i].MonthToDateDAmt;
                                                    r.Cells[7].Value = lCOA[i].MonthToDateCAmt;
                                                    r.Cells[8].Value = lCOA[i].MonthToDateBalance;
                                                    r.Cells[9].Value = lCOA[i].CurrentMonthDAmt;
                                                    r.Cells[10].Value = lCOA[i].CurrentMonthCAmt;
                                                    r.Cells[11].Value = lCOA[i].CurrentMonthBalance;
                                                    r.Cells[12].Value = lCOA[i].YearToDateDAmt;
                                                    r.Cells[13].Value = lCOA[i].YearToDateCAmt;
                                                    r.Cells[14].Value = lCOA[i].YearToDateBalance;
                                                }
                                            }
                                            else
                                            {
                                                r = Grd1.Rows.Add();
                                                r.Level = lCOA[i].Level;
                                                r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                                r.Cells[0].Value = lCOA[i].AcNo;
                                                r.Cells[1].Value = lCOA[i].Alias;
                                                r.Cells[2].Value = lCOA[i].AcDesc;
                                                for (var c = 3; c < 15; c++)
                                                    r.Cells[c].Value = 0m;
                                                r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                                r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                                r.Cells[5].Value = lCOA[i].OBBalance;
                                                r.Cells[6].Value = lCOA[i].MonthToDateDAmt;
                                                r.Cells[7].Value = lCOA[i].MonthToDateCAmt;
                                                r.Cells[8].Value = lCOA[i].MonthToDateBalance;
                                                r.Cells[9].Value = lCOA[i].CurrentMonthDAmt;
                                                r.Cells[10].Value = lCOA[i].CurrentMonthCAmt;
                                                r.Cells[11].Value = lCOA[i].CurrentMonthBalance;
                                                r.Cells[12].Value = lCOA[i].YearToDateDAmt;
                                                r.Cells[13].Value = lCOA[i].YearToDateCAmt;
                                                r.Cells[14].Value = lCOA[i].YearToDateBalance;
                                            }
                                        }
                                    }
                                }
                                Grd1.TreeLines.Visible = true;
                                Grd1.Rows.CollapseAll();

                                decimal
                                    OpeningBalanceDAmt = 0m,
                                    OpeningBalanceCAmt = 0m,
                                    OBBalance = 0m,
                                    MonthToDateDAmt = 0m,
                                    MonthToDateCAmt = 0m,
                                    MonthToDateBalance = 0m,
                                    CurrentMonthDAmt = 0m,
                                    CurrentMonthCAmt = 0m,
                                    CurrentMonthBalance = 0m,
                                    YearToDateDAmt = 0m,
                                    YearToDateCAmt = 0m,
                                    YearToDateBalance = 0m;

                                if (!ChkEntCode.Checked)
                                {
                                    for (var i = 0; i < lCOA.Count; i++)
                                    {
                                        if (lCOA[i].Parent.Length == 0)
                                        {
                                            OpeningBalanceDAmt += lCOA[i].OpeningBalanceDAmt;
                                            OpeningBalanceCAmt += lCOA[i].OpeningBalanceCAmt;
                                            OBBalance += lCOA[i].OBBalance;
                                            MonthToDateDAmt += lCOA[i].MonthToDateDAmt;
                                            MonthToDateCAmt += lCOA[i].MonthToDateCAmt;
                                            MonthToDateBalance += lCOA[i].MonthToDateBalance;
                                            CurrentMonthDAmt += lCOA[i].CurrentMonthDAmt;
                                            CurrentMonthCAmt += lCOA[i].CurrentMonthCAmt;
                                            CurrentMonthBalance += lCOA[i].CurrentMonthBalance;
                                            YearToDateDAmt += lCOA[i].YearToDateDAmt;
                                            YearToDateCAmt += lCOA[i].YearToDateCAmt;
                                            YearToDateBalance += lCOA[i].YearToDateBalance;
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                                    {
                                        foreach (var x in lCOA.Where(w => w.AcNo == Sm.GetGrdStr(Grd1, i, 0)))
                                        {
                                            if (x.Parent.Length == 0)
                                            {
                                                OpeningBalanceDAmt += x.OpeningBalanceDAmt;
                                                OpeningBalanceCAmt += x.OpeningBalanceCAmt;
                                                OBBalance += x.OBBalance;
                                                MonthToDateDAmt += x.MonthToDateDAmt;
                                                MonthToDateCAmt += x.MonthToDateCAmt;
                                                MonthToDateBalance += x.MonthToDateBalance;
                                                CurrentMonthDAmt += x.CurrentMonthDAmt;
                                                CurrentMonthCAmt += x.CurrentMonthCAmt;
                                                CurrentMonthBalance += x.CurrentMonthBalance;
                                                YearToDateDAmt += x.YearToDateDAmt;
                                                YearToDateCAmt += x.YearToDateCAmt;
                                                YearToDateBalance += x.YearToDateBalance;
                                            }
                                        }
                                    }
                                }

                                r = Grd1.Rows.Add();
                                r.Cells[0].Value = "Total";
                                for (var c = 3; c < 15; c++)
                                    r.Cells[c].Value = 0m;

                                r.Cells[3].Value = OpeningBalanceDAmt;
                                r.Cells[4].Value = OpeningBalanceCAmt;
                                r.Cells[5].Value = OBBalance;
                                r.Cells[6].Value = MonthToDateDAmt;
                                r.Cells[7].Value = MonthToDateCAmt;
                                r.Cells[8].Value = MonthToDateBalance;
                                r.Cells[9].Value = CurrentMonthDAmt;
                                r.Cells[10].Value = CurrentMonthCAmt;
                                r.Cells[11].Value = CurrentMonthBalance;
                                r.Cells[12].Value = YearToDateDAmt;
                                r.Cells[13].Value = YearToDateCAmt;
                                r.Cells[14].Value = YearToDateBalance;
                                r.BackColor = Color.LightSalmon;

                                Grd1.EndUpdate();
                            }
                        }
                    }

                    #endregion

                    #region Not Filtered By Entity

                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            Grd1.BeginUpdate();
                            Grd1.Rows.Count = 0;

                            iGRow r;

                            r = Grd1.Rows.Add();
                            r.Level = 0;
                            r.TreeButton = iGTreeButtonState.Visible;
                            r.Cells[0].Value = "COA";
                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (Sm.GetLue(LueLevelFrom).Length > 0 && Sm.GetLue(LueLevelTo).Length > 0)
                                {
                                    if (lCOA[i].Level >= Int32.Parse(Sm.GetLue(LueLevelFrom)) &&
                                                    lCOA[i].Level <= Int32.Parse(Sm.GetLue(LueLevelTo)))
                                    {
                                        r = Grd1.Rows.Add();
                                        r.Level = lCOA[i].Level;
                                        r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                        r.Cells[0].Value = lCOA[i].AcNo;
                                        r.Cells[1].Value = lCOA[i].Alias;
                                        r.Cells[2].Value = lCOA[i].AcDesc;
                                        for (var c = 3; c < 15; c++)
                                            r.Cells[c].Value = 0m;
                                        r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                        r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                        r.Cells[5].Value = lCOA[i].OBBalance;
                                        r.Cells[6].Value = lCOA[i].MonthToDateDAmt;
                                        r.Cells[7].Value = lCOA[i].MonthToDateCAmt;
                                        r.Cells[8].Value = lCOA[i].MonthToDateBalance;
                                        r.Cells[9].Value = lCOA[i].CurrentMonthDAmt;
                                        r.Cells[10].Value = lCOA[i].CurrentMonthCAmt;
                                        r.Cells[11].Value = lCOA[i].CurrentMonthBalance;
                                        r.Cells[12].Value = lCOA[i].YearToDateDAmt;
                                        r.Cells[13].Value = lCOA[i].YearToDateCAmt;
                                        r.Cells[14].Value = lCOA[i].YearToDateBalance;
                                    }
                                }
                                else
                                {
                                    r = Grd1.Rows.Add();
                                    r.Level = lCOA[i].Level;
                                    r.TreeButton = lCOA[i].HasChild ? iGTreeButtonState.Visible : iGTreeButtonState.Hidden;
                                    r.Cells[0].Value = lCOA[i].AcNo;
                                    r.Cells[1].Value = lCOA[i].Alias;
                                    r.Cells[2].Value = lCOA[i].AcDesc;
                                    for (var c = 3; c < 15; c++)
                                        r.Cells[c].Value = 0m;
                                    r.Cells[3].Value = lCOA[i].OpeningBalanceDAmt;
                                    r.Cells[4].Value = lCOA[i].OpeningBalanceCAmt;
                                    r.Cells[5].Value = lCOA[i].OBBalance;
                                    r.Cells[6].Value = lCOA[i].MonthToDateDAmt;
                                    r.Cells[7].Value = lCOA[i].MonthToDateCAmt;
                                    r.Cells[8].Value = lCOA[i].MonthToDateBalance;
                                    r.Cells[9].Value = lCOA[i].CurrentMonthDAmt;
                                    r.Cells[10].Value = lCOA[i].CurrentMonthCAmt;
                                    r.Cells[11].Value = lCOA[i].CurrentMonthBalance;
                                    r.Cells[12].Value = lCOA[i].YearToDateDAmt;
                                    r.Cells[13].Value = lCOA[i].YearToDateCAmt;
                                    r.Cells[14].Value = lCOA[i].YearToDateBalance;
                                }
                            }
                            Grd1.TreeLines.Visible = true;
                            Grd1.Rows.CollapseAll();

                            decimal
                                OpeningBalanceDAmt = 0m,
                                OpeningBalanceCAmt = 0m,
                                OBBalance = 0m,
                                MonthToDateDAmt = 0m,
                                MonthToDateCAmt = 0m,
                                MonthToDateBalance = 0m,
                                CurrentMonthDAmt = 0m,
                                CurrentMonthCAmt = 0m,
                                CurrentMonthBalance = 0m,
                                YearToDateDAmt = 0m,
                                YearToDateCAmt = 0m,
                                YearToDateBalance = 0m;

                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                if (lCOA[i].Parent.Length == 0)
                                {
                                    OpeningBalanceDAmt += lCOA[i].OpeningBalanceDAmt;
                                    OpeningBalanceCAmt += lCOA[i].OpeningBalanceCAmt;
                                    OBBalance += lCOA[i].OBBalance;
                                    MonthToDateDAmt += lCOA[i].MonthToDateDAmt;
                                    MonthToDateCAmt += lCOA[i].MonthToDateCAmt;
                                    MonthToDateBalance += lCOA[i].MonthToDateBalance;
                                    CurrentMonthDAmt += lCOA[i].CurrentMonthDAmt;
                                    CurrentMonthCAmt += lCOA[i].CurrentMonthCAmt;
                                    CurrentMonthBalance += lCOA[i].CurrentMonthBalance;
                                    YearToDateDAmt += lCOA[i].YearToDateDAmt;
                                    YearToDateCAmt += lCOA[i].YearToDateCAmt;
                                    YearToDateBalance += lCOA[i].YearToDateBalance;
                                }
                            }

                            r = Grd1.Rows.Add();
                            r.Cells[0].Value = "Total";
                            for (var c = 3; c < 15; c++)
                                r.Cells[c].Value = 0m;

                            r.Cells[3].Value = OpeningBalanceDAmt;
                            r.Cells[4].Value = OpeningBalanceCAmt;
                            r.Cells[5].Value = OBBalance;
                            r.Cells[6].Value = MonthToDateDAmt;
                            r.Cells[7].Value = MonthToDateCAmt;
                            r.Cells[8].Value = MonthToDateBalance;
                            r.Cells[9].Value = CurrentMonthDAmt;
                            r.Cells[10].Value = CurrentMonthCAmt;
                            r.Cells[11].Value = CurrentMonthBalance;
                            r.Cells[12].Value = YearToDateDAmt;
                            r.Cells[13].Value = YearToDateCAmt;
                            r.Cells[14].Value = YearToDateBalance;
                            r.BackColor = Color.LightSalmon;

                            Grd1.EndUpdate();
                        }
                    }

                    #endregion

                    lCOA.Clear();
                    lEntityCOA.Clear();
                    lCOAPLSettingHdr.Clear();
                    lCOAPLSettingDtl.Clear();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            //SQL.AppendLine("    Select 'Consolidate' Col, '01' Col2  ");
            //SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select ProfitCenterName AS Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void SetLueEntCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            if (mIsReportingFilterByEntity)
            {
                SQL.AppendLine("    Select A.EntCode As Col1, B.EntName As Col2 ");
                SQL.AppendLine("    From TblGroupEntity A ");
                SQL.AppendLine("    Inner Join TblEntity B On A.EntCode = B.EntCode And B.ActInd='Y' ");
                SQL.AppendLine("    Where A.GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T Where T.ActInd='Y'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueLevelFromTo(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Level Col1, Level Col2 ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Order By Level; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void Process1(ref List<COA> lCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var entCode = string.Empty;
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.Alias, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                if (ChkEntCode.Checked) SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo And B.EntCode=@EntCode ");
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Alias", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Alias = Sm.DrStr(dr, c[1]),
                            AcDesc = Sm.DrStr(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrStr(dr, c[3]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[4]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            OBBalance = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<COA> lCOA, string Yr)
        {
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then SUM(B.Amt) Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then SUM(B.Amt) Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.Amt != 0.00 ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y'  ");
            SQL.AppendLine("Where 1=1 ");
            if (ChkEntCode.Checked)
            {
                SQL.AppendLine("    And A.EntCode Is Not Null And A.EntCode=@EntCode ");
            }
            if (mIsRptTrialBalanceUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.ProfitCenterCode=@ProfitCenter" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); // multiEntCode.Contains("Consolidate") ? "" : multiEntCode);
                    }
                    else
                    {
                        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                        SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                }
            }
            SQL.AppendLine("Group By C.AcNo");
            SQL.AppendLine("Order By B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.DAmt - B.CAmt != 0.00 ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            if (mIsRptTrialBalanceUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Order By B.AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process3_JournalMemorial(ref List<COA> lCOA, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo, Sum(DAmt) As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("From ( ");

            #region Journal

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.DAmt - B.CAmt != 0.00 ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            if (mIsRptTrialBalanceUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine("Union All ");

            #region Journal Memorial

            SQL.AppendLine("Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Inner Join TblJournalMemorialDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.DAmt - B.CAmt != 0.00 ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.CancelInd='N' And A.Status='O' ");
            if (StartFrom.Length == 0)
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            else
            {
                if (mIsAccountingRptUseJournalPeriod)
                {
                    SQL.AppendLine("    And A.Period Is Not Null ");
                    SQL.AppendLine("    And Left(A.Period, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.Period, 6) < @YrMth ");
                }
                else
                {
                    SQL.AppendLine("    And Left(A.DocDt, 6) >= @StartFrom ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) < @YrMth ");
                }
            }
            if (mIsRptTrialBalanceUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        //Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }

            #endregion

            SQL.AppendLine(") Tbl Group By AcNo Order By AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@COAAssetAcNo", mCOAAssetAcNo);
            Sm.CmParam<String>(ref cm, "@COAAssetStartYr", mCOAAssetStartYr);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process4(ref List<COA> lCOA, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.DAmt - B.CAmt != 0.00 ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("    And A.Period Is Not Null ");
                SQL.AppendLine("    And Left(A.Period, 6) = Concat(@Yr, @Mth) ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 4) = @Yr ");
                SQL.AppendLine("    And Substring(A.DocDt, 5, 2) = @Mth ");
            }
            if (ChkEntCode.Checked) SQL.AppendLine("    And B.EntCode Is Not Null And B.EntCode=@EntCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                SQL.AppendLine("And A.Period Is Not Null And A.Period=@Period ");
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LuePeriod)));
            }
            if (mIsRptTrialBalanceUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, IfNull(@ProfitCenterCode, '')) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select ProfitCenterCode ");
                        SQL.AppendLine("            From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine("Group By B.AcNo;");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOA[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5(ref List<COA> lCOA)
        {
            if (mCurrentEarningFormulaType == "2")
            {
                if (
                    mAcNoForCurrentEarning.Length == 0 ||
                    mAcNoForCurrentEarning2.Length == 0 ||
                    mAcNoForActiva.Length == 0 ||
                    mAcNoForPassiva.Length == 0 ||
                    mAcNoForCapital.Length == 0
                    ) return;

                Process5c(ref lCOA);
            }
            else
            {
                if (mCurrentEarningFormulaType == "3")
                {
                    if (
                        mAcNoForCurrentEarning.Length == 0 ||
                        mAcNoForActiva.Length == 0 ||
                        mAcNoForPassiva.Length == 0 ||
                        mAcNoForCapital.Length == 0
                        ) return;

                    Process5d(ref lCOA);
                }
                else
                {
                    if (
                        mAcNoForCurrentEarning.Length == 0 ||
                        mAcNoForCurrentEarning2.Length == 0 ||
                        mAcNoForIncome.Length == 0 ||
                        mAcNoForCost.Length == 0
                        ) return;

                    Process5a(ref lCOA);
                    Process5b(ref lCOA);
                }
            }
        }

        private void Process5a(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }


            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            if (CurrentProfiLossIndex != 0)
            {
                lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
                lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;
            }

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            if (CurrentProfiLossIndex != 0)
            {
                lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
                lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;
            }

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5b(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning2) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCost) == 0)
                {
                    CostIndex = i;
                    Completed += 1;
                    if (Completed == 3) break;
                }
            }

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Amt =
                (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt) -
                (lCOA[CostIndex].MonthToDateDAmt - lCOA[CostIndex].MonthToDateCAmt);

            if (CurrentProfiLossIndex != 0)
            {
                lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt;
                lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0m;
            }

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    if (CurrentProfiLossParentAcNo.Length == 0) break;
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += 0m;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            Amt =
                (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt) -
                (lCOA[CostIndex].CurrentMonthDAmt - lCOA[CostIndex].CurrentMonthCAmt);

            if (CurrentProfiLossIndex != 0)
            {
                lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt;
                lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;
            }

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    if (CurrentProfiLossParentAcNo.Length == 0) break;
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += 0m;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5c(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                CurrentProfiLossIndex2 = 0,
                IncomeIndex = 0,
                CostIndex1 = 0,
                CostIndex2 = 0,
                CostIndex3 = 0,
                CostIndex4 = 0
                ;
            decimal Income = 0m, Cost1 = 0m, Cost2 = 0m, Cost3 = 0m, Cost4 = 0m;
            int
                Completed = 0,
                MaxAccountCategory = int.Parse(mMaxAccountCategory);
            int Completed2 = MaxAccountCategory - 2;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning2) == 0)
                {
                    CurrentProfiLossIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "4") == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "5") == 0)
                {
                    CostIndex1 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "6") == 0 && MaxAccountCategory > 6)
                {
                    CostIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "7") == 0 && MaxAccountCategory > 7)
                {
                    CostIndex3 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "8") == 0 && MaxAccountCategory > 8)
                {
                    CostIndex4 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }
            }


            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            var CurrentProfiLossParentIndex2 = -1;
            var CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            var f2 = true;

            //Month To Date
            //if (lCOA[IncomeIndex].AcType == "C")
            Income = (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt);
            //else
            //    Income = (lCOA[IncomeIndex].MonthToDateDAmt - lCOA[IncomeIndex].MonthToDateCAmt);

            //if (lCOA[CostIndex1].AcType == "D")
            Cost1 = (lCOA[CostIndex1].MonthToDateDAmt - lCOA[CostIndex1].MonthToDateCAmt);
            //else
            //    Cost1 = (lCOA[CostIndex1].MonthToDateCAmt - lCOA[CostIndex1].MonthToDateDAmt);

            //if (lCOA[CostIndex2].AcType == "D")
            if (CostIndex2 != 0)
                Cost2 = (lCOA[CostIndex2].MonthToDateDAmt - lCOA[CostIndex2].MonthToDateCAmt);
            //else
            //    Cost2 = (lCOA[CostIndex2].MonthToDateCAmt - lCOA[CostIndex2].MonthToDateDAmt);

            //if (lCOA[CostIndex3].AcType == "D")
            if (CostIndex3 != 0)
                Cost3 = (lCOA[CostIndex3].MonthToDateDAmt - lCOA[CostIndex3].MonthToDateCAmt);
            //else
            //    Cost3 = (lCOA[CostIndex3].MonthToDateCAmt - lCOA[CostIndex3].MonthToDateDAmt);

            //if (lCOA[CostIndex4].AcType == "D")
            if (CostIndex4 != 0)
                Cost4 = (lCOA[CostIndex4].MonthToDateDAmt - lCOA[CostIndex4].MonthToDateCAmt);
            //else
            //    Cost4 = (lCOA[CostIndex4].MonthToDateCAmt - lCOA[CostIndex4].MonthToDateDAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            lCOA[CurrentProfiLossIndex2].MonthToDateDAmt = Amt;
            lCOA[CurrentProfiLossIndex2].MonthToDateCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            CurrentProfiLossParentIndex2 = -1;
            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            f2 = true;
            if (CurrentProfiLossParentAcNo2.Length > 0)
            {
                while (f2)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo2) == 0)
                        {
                            CurrentProfiLossParentIndex2 = i;
                            lCOA[CurrentProfiLossParentIndex2].MonthToDateDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex2].MonthToDateCAmt += 0m;

                            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossParentIndex2].Parent;
                            f2 = CurrentProfiLossParentAcNo2.Length > 0;
                            break;
                        }
                    }
                }
            }

            //Current Month
            //if (lCOA[IncomeIndex].AcType == "C")
            Income = (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt);
            //else
            //    Income = (lCOA[IncomeIndex].CurrentMonthDAmt - lCOA[IncomeIndex].CurrentMonthCAmt);

            //if (lCOA[CostIndex1].AcType == "D")
            Cost1 = (lCOA[CostIndex1].CurrentMonthDAmt - lCOA[CostIndex1].CurrentMonthCAmt);
            //else
            //    Cost1 = (lCOA[CostIndex1].CurrentMonthCAmt - lCOA[CostIndex1].CurrentMonthDAmt);

            //if (lCOA[CostIndex2].AcType == "D")
            if (CostIndex2 != 0)
                Cost2 = (lCOA[CostIndex2].CurrentMonthDAmt - lCOA[CostIndex2].CurrentMonthCAmt);
            //else
            //    Cost2 = (lCOA[CostIndex2].CurrentMonthCAmt - lCOA[CostIndex2].CurrentMonthDAmt);

            //if (lCOA[CostIndex3].AcType == "D")
            if (CostIndex3 != 0)
                Cost3 = (lCOA[CostIndex3].CurrentMonthDAmt - lCOA[CostIndex3].CurrentMonthCAmt);
            //else
            //    Cost3 = (lCOA[CostIndex3].CurrentMonthCAmt - lCOA[CostIndex3].CurrentMonthDAmt);

            //if (lCOA[CostIndex4].AcType == "D")
            if (CostIndex4 != 0)
                Cost4 = (lCOA[CostIndex4].CurrentMonthDAmt - lCOA[CostIndex4].CurrentMonthCAmt);
            //else
            //    Cost4 = (lCOA[CostIndex4].CurrentMonthCAmt - lCOA[CostIndex4].CurrentMonthDAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            lCOA[CurrentProfiLossIndex2].CurrentMonthDAmt = Amt;
            lCOA[CurrentProfiLossIndex2].CurrentMonthCAmt = 0m;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }

            CurrentProfiLossParentIndex2 = -1;
            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossIndex2].Parent;
            f2 = true;
            if (CurrentProfiLossParentAcNo2.Length > 0)
            {
                while (f2)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo2) == 0)
                        {
                            CurrentProfiLossParentIndex2 = i;
                            lCOA[CurrentProfiLossParentIndex2].CurrentMonthDAmt += Amt;
                            lCOA[CurrentProfiLossParentIndex2].CurrentMonthCAmt += 0m;

                            CurrentProfiLossParentAcNo2 = lCOA[CurrentProfiLossParentIndex2].Parent;
                            f2 = CurrentProfiLossParentAcNo2.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5d(ref List<COA> lCOA)
        {
            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0,
                CostIndex1 = 0,
                CostIndex2 = 0,
                CostIndex3 = 0,
                CostIndex4 = 0
                ;
            decimal Income = 0m, Cost1 = 0m, Cost2 = 0m, Cost3 = 0m, Cost4 = 0m;
            int
                Completed = 0,
                MaxAccountCategory = int.Parse(mMaxAccountCategory);
            int Completed2 = MaxAccountCategory - 2;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "4") == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "5") == 0)
                {
                    CostIndex1 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "6") == 0 && MaxAccountCategory >= 6)
                {
                    CostIndex2 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }


                if (string.Compare(lCOA[i].AcNo, "7") == 0 && MaxAccountCategory >= 7)
                {
                    CostIndex3 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }

                if (string.Compare(lCOA[i].AcNo, "8") == 0 && MaxAccountCategory >= 8)
                {
                    CostIndex4 = i;
                    Completed += 1;
                    if (Completed == Completed2) break;
                }
            }

            decimal Amt = 0m;

            //Opening Balance
            Amt = lCOA[CurrentProfiLossIndex].OpeningBalanceCAmt - lCOA[CurrentProfiLossIndex].OpeningBalanceDAmt;

            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            //Month To Date
            Income = (lCOA[IncomeIndex].MonthToDateCAmt - lCOA[IncomeIndex].MonthToDateDAmt);
            Cost1 = (lCOA[CostIndex1].MonthToDateDAmt - lCOA[CostIndex1].MonthToDateCAmt);
            if (CostIndex2 != 0) Cost2 = (lCOA[CostIndex2].MonthToDateDAmt - lCOA[CostIndex2].MonthToDateCAmt);
            if (CostIndex3 != 0) Cost3 = (lCOA[CostIndex3].MonthToDateDAmt - lCOA[CostIndex3].MonthToDateCAmt);
            if (CostIndex4 != 0) Cost4 = (lCOA[CostIndex4].MonthToDateDAmt - lCOA[CostIndex4].MonthToDateCAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
            lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }


            //Current Month
            Income = (lCOA[IncomeIndex].CurrentMonthCAmt - lCOA[IncomeIndex].CurrentMonthDAmt);
            Cost1 = (lCOA[CostIndex1].CurrentMonthDAmt - lCOA[CostIndex1].CurrentMonthCAmt);
            if (CostIndex2 != 0) Cost2 = (lCOA[CostIndex2].CurrentMonthDAmt - lCOA[CostIndex2].CurrentMonthCAmt);
            if (CostIndex3 != 0) Cost3 = (lCOA[CostIndex3].CurrentMonthDAmt - lCOA[CostIndex3].CurrentMonthCAmt);
            if (CostIndex4 != 0) Cost4 = (lCOA[CostIndex4].CurrentMonthDAmt - lCOA[CostIndex4].CurrentMonthCAmt);

            Amt = Income - (Cost1 + Cost2 + Cost3 + Cost4);

            lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
            lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;

            CurrentProfiLossParentIndex = -1;
            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            f = true;
            if (CurrentProfiLossParentAcNo.Length > 0)
            {
                while (f)
                {
                    for (var i = 0; i < lCOA.Count; i++)
                    {
                        if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                        {
                            CurrentProfiLossParentIndex = i;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt += 0m;
                            lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt += Amt;

                            CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                            f = CurrentProfiLossParentAcNo.Length > 0;
                            break;
                        }
                    }
                }
            }
        }

        private void Process5e(ref List<COA> lCOA)
        {
            var lCOATemp2 = new List<COATemp>();
            var lCOATemp = new List<COA2>();
            var lJournalTemp = new List<Journal2>();
            var lJournalCurrentMonthTemp = new List<Journal3>();
            var lCOAOpeningBalanceTemp = new List<COAOpeningBalance2>();

            //SetProfitCenter();
            string mListAcNo = "4,5,6,7,8,9.04.01.01.01.01.01.01.01.01.03,9.02,9.03";
            string mUsedAcNo = string.Empty;
            decimal mYearToDateBalance = 0m;

            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var StartFrom = Sm.GetLue(LueYr);

            GetAllCOA(ref lCOATemp, mListAcNo);
            if (lCOATemp.Count > 0)
            {
                foreach (var i in lCOATemp)
                {
                    if (mUsedAcNo.Length > 0) mUsedAcNo += ",";
                    mUsedAcNo += i.AcNo;
                }
            }

            if (StartFrom.Length > 0)
            {
                GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, mUsedAcNo, StartFrom);
                GetAllJournal(ref lJournalTemp, mUsedAcNo, Yr, Mth, StartFrom);
            }
            else
            {
                GetAllCOAOpeningBalance(ref lCOAOpeningBalanceTemp, mUsedAcNo, Yr);
                GetAllJournal(ref lJournalTemp, mUsedAcNo, Yr, Mth, string.Empty);
            }

            GetAllJournalCurrentMonth(ref lJournalCurrentMonthTemp, mUsedAcNo, Yr, Mth);

            Process5f(ref lCOATemp, ref lCOATemp2);
            Process5g(ref lCOAOpeningBalanceTemp, ref lCOATemp2);
            Process5h(ref lJournalTemp, ref lCOATemp2);
            Process5i(ref lJournalCurrentMonthTemp, ref lCOATemp2);
            Process5j(ref lCOATemp2);
            //Process5k(ref lCOATemp2);
            Process5l(ref lCOATemp2, ref mYearToDateBalance);
            Process5m(ref lCOA, ref mYearToDateBalance);
        }

        private void Process5f(ref List<COA2> lCOATemp, ref List<COATemp> lCOATemp2)
        {
            foreach (var y in lCOATemp)
            {
                lCOATemp2.Add(new COATemp()
                {
                    AcNo = y.AcNo,
                    AcDesc = y.AcDesc,
                    AcType = y.AcType,
                    HasChild = y.HasChild,
                    Level = y.Level,
                    Parent = y.Parent,
                    CurrentMonthBalance = y.CurrentMonthBalance,
                    CurrentMonthCAmt = y.CurrentMonthCAmt,
                    CurrentMonthDAmt = y.CurrentMonthDAmt,
                    MonthToDateBalance = y.MonthToDateBalance,
                    MonthToDateCAmt = y.MonthToDateCAmt,
                    MonthToDateDAmt = y.MonthToDateDAmt,
                    OpeningBalanceCAmt = y.OpeningBalanceCAmt,
                    OpeningBalanceDAmt = y.OpeningBalanceDAmt,
                    OBBalance = y.OBBalance,
                    YearToDateBalance = y.YearToDateBalance,
                    YearToDateCAmt = y.YearToDateCAmt,
                    YearToDateDAmt = y.YearToDateDAmt
                });
            }
        }

        private void Process5g(ref List<COAOpeningBalance2> l, ref List<COATemp> lCOATemp2) //Opening Balance
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) &&
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.')
                            ||
                            lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.')) &&
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.')
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5h(ref List<Journal2> l, ref List<COATemp> lCOATemp2) //Journal Month To Date
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5i(ref List<Journal3> l, ref List<COATemp> lCOATemp2) //Journal Current Month
        {
            var lJournal = new List<Journal>();

            foreach (var x in l.OrderBy(o => o.AcNo))
            {
                lJournal.Add(new Journal()
                {
                    AcNo = x.AcNo,
                    DAmt = x.DAmt,
                    CAmt = x.CAmt
                });
            }

            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);

                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOATemp2.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOATemp2[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOATemp2[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOATemp2[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOATemp2[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOATemp2[j].CurrentMonthDAmt += lJournal[i].DAmt;
                            lCOATemp2[j].CurrentMonthCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOATemp2[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        private void Process5j(ref List<COATemp> lCOATemp2)
        {
            for (var i = 0; i < lCOATemp2.Count; i++)
            {
                //hitung OB Balance
                lCOATemp2[i].OBBalance = lCOATemp2[i].OpeningBalanceDAmt - lCOATemp2[i].OpeningBalanceCAmt;

                //hitung MTD Balance
                lCOATemp2[i].MonthToDateBalance = lCOATemp2[i].MonthToDateDAmt - lCOATemp2[i].MonthToDateCAmt;

                //hitung Current Month Balance
                lCOATemp2[i].CurrentMonthBalance = lCOATemp2[i].MonthToDateBalance + lCOATemp2[i].CurrentMonthDAmt - lCOATemp2[i].CurrentMonthCAmt;

                //hitung YTD
                lCOATemp2[i].YearToDateDAmt = lCOATemp2[i].OpeningBalanceDAmt + lCOATemp2[i].MonthToDateDAmt + lCOATemp2[i].CurrentMonthDAmt;
                lCOATemp2[i].YearToDateCAmt = lCOATemp2[i].OpeningBalanceCAmt + lCOATemp2[i].MonthToDateCAmt + lCOATemp2[i].CurrentMonthCAmt;
                lCOATemp2[i].YearToDateBalance = lCOATemp2[i].YearToDateDAmt - lCOATemp2[i].YearToDateCAmt;
            }
        }

        private void Process5k(ref List<COATemp> lCOATemp2)
        {
            for (var i = 0; i < lCOATemp2.Count; i++)
            {
                if (lCOATemp2[i].AcType == "D")
                {
                    lCOATemp2[i].MonthToDateBalance =
                        lCOATemp2[i].OpeningBalanceDAmt +
                        lCOATemp2[i].MonthToDateDAmt -
                        lCOATemp2[i].MonthToDateCAmt;

                    lCOATemp2[i].CurrentMonthBalance =
                        lCOATemp2[i].MonthToDateBalance +
                        lCOATemp2[i].CurrentMonthDAmt -
                        lCOATemp2[i].CurrentMonthCAmt;

                    lCOATemp2[i].YearToDateBalance = lCOATemp2[i].YearToDateDAmt - lCOATemp2[i].YearToDateCAmt;
                }
                if (lCOATemp2[i].AcType == "C")
                {
                    lCOATemp2[i].MonthToDateBalance =
                        lCOATemp2[i].OpeningBalanceCAmt +
                        lCOATemp2[i].MonthToDateCAmt -
                        lCOATemp2[i].MonthToDateDAmt;

                    lCOATemp2[i].CurrentMonthBalance =
                        lCOATemp2[i].MonthToDateBalance +
                        lCOATemp2[i].CurrentMonthCAmt -
                        lCOATemp2[i].CurrentMonthDAmt;

                    lCOATemp2[i].YearToDateBalance = lCOATemp2[i].YearToDateCAmt - lCOATemp2[i].YearToDateDAmt;
                }
            }
        }

        private void Process5l(ref List<COATemp> lCOATemp2, ref decimal YearToDateBalance)
        {
            char[] delimiters = { '+', '-', '(', ')' };
            string SQLFormula = "#4#-#5#-#6#-#7#+#8#-#9.04.01.01.01.01.01.01.01.01.03#-#9.02#-#9.03#";
            string[] ArraySQLFormula = SQLFormula.Split(delimiters);
            var lFormula = new List<Formula>();

            ArraySQLFormula = ArraySQLFormula.Where(val => val != "").ToArray();
            for (int i = 0; i < ArraySQLFormula.Count(); i++)
                lFormula.Add(new Formula() { AcNo = ArraySQLFormula[i], Amt = 0m });

            for (int ind = 0; ind < lFormula.Count; ind++)
                for (var h = 0; h < lCOATemp2.Count; h++)
                    if (lFormula[ind].AcNo == string.Concat("#", lCOATemp2[h].AcNo, "#"))
                        lFormula[ind].Amt = lCOATemp2[h].YearToDateBalance;

            for (int j = 0; j < ArraySQLFormula.Count(); j++)
            {
                for (int k = 0; k < lFormula.Count; k++)
                {
                    if (ArraySQLFormula[j].ToString() == lFormula[k].AcNo)
                    {
                        string oldS = ArraySQLFormula[j].ToString();
                        string newS = lFormula[k].Amt.ToString();
                        SQLFormula = SQLFormula.Replace(oldS, newS);
                    }
                }
            }
            decimal baltemp = Decimal.Parse(Sm.GetValue("Select " + SQLFormula + " "));
            YearToDateBalance = baltemp;
        }

        private void Process5m(ref List<COA> lCOA, ref decimal YearToDateBalance)
        {
            if (mAcNoForCurrentEarning3.Length == 0 || mAcNoForIncome2.Length == 0) return;

            int
                CurrentProfiLossIndex = 0,
                IncomeIndex = 0
                ;

            byte Completed = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (string.Compare(lCOA[i].AcNo, mAcNoForCurrentEarning3) == 0)
                {
                    CurrentProfiLossIndex = i;
                    Completed += 1;
                    if (Completed == 2) break;
                }

                if (string.Compare(lCOA[i].AcNo, mAcNoForIncome2) == 0)
                {
                    IncomeIndex = i;
                    Completed += 1;
                    if (Completed == 2) break;
                }
            }
            decimal Amt = 0m;
            Amt = YearToDateBalance;

            //Laba rugi tahun berjalan
            var CurrentProfiLossParentIndex = -1;
            var CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossIndex].Parent;
            var f = true;

            if (lCOA[CurrentProfiLossIndex].AcType == "D")
            {
                if (Amt < 0)
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt * -1;
                }
                else
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;
                }
            }
            else
            {
                if (Amt < 0)
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = 0;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = Amt * -1;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = 0m;
                }
                else
                {
                    lCOA[CurrentProfiLossIndex].MonthToDateDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].MonthToDateCAmt = Amt;
                    lCOA[CurrentProfiLossIndex].CurrentMonthDAmt = 0m;
                    lCOA[CurrentProfiLossIndex].CurrentMonthCAmt = Amt;
                }
            }

            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, CurrentProfiLossParentAcNo) == 0)
                    {
                        CurrentProfiLossParentIndex = i;
                        if (lCOA[CurrentProfiLossIndex].AcType == "D")
                        {
                            if (Amt < 0)
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = Amt * -1;
                            }
                            else
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = 0m;
                            }
                        }
                        else
                        {
                            if (Amt < 0)
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = 0;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = Amt * -1;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = 0m;
                            }
                            else
                            {
                                lCOA[CurrentProfiLossParentIndex].MonthToDateDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].MonthToDateCAmt = Amt;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[CurrentProfiLossParentIndex].CurrentMonthCAmt = Amt;
                            }
                        }

                        CurrentProfiLossParentAcNo = lCOA[CurrentProfiLossParentIndex].Parent;
                        f = CurrentProfiLossParentAcNo.Length > 0;
                        break;
                    }
                }
            }

            //Laba rugi - penghasilan
            var IncomeParentIndex = -1;
            var IncomeParentAcNo = lCOA[IncomeIndex].Parent;
            f = true;

            if (lCOA[IncomeIndex].AcType == "D")
            {
                if (Amt < 0)
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = 0m;
                    lCOA[IncomeIndex].MonthToDateCAmt = Amt * -1;
                    lCOA[IncomeIndex].CurrentMonthDAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthCAmt = Amt * -1;
                }
                else
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = Amt;
                    lCOA[IncomeIndex].MonthToDateCAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthDAmt = Amt;
                    lCOA[IncomeIndex].CurrentMonthCAmt = 0m;
                }
            }
            else
            {
                if (Amt < 0)
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = Amt * -1;
                    lCOA[IncomeIndex].MonthToDateCAmt = 0;
                    lCOA[IncomeIndex].CurrentMonthDAmt = Amt * -1;
                    lCOA[IncomeIndex].CurrentMonthCAmt = 0m;
                }
                else
                {
                    lCOA[IncomeIndex].MonthToDateDAmt = 0m;
                    lCOA[IncomeIndex].MonthToDateCAmt = Amt;
                    lCOA[IncomeIndex].CurrentMonthDAmt = 0m;
                    lCOA[IncomeIndex].CurrentMonthCAmt = Amt;
                }
            }

            while (f)
            {
                for (var i = 0; i < lCOA.Count; i++)
                {
                    if (string.Compare(lCOA[i].AcNo, IncomeParentAcNo) == 0)
                    {
                        IncomeParentIndex = i;
                        if (lCOA[IncomeIndex].AcType == "D")
                        {
                            if (Amt < 0)
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = 0m;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = Amt * -1;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = Amt * -1;
                            }
                            else
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = Amt;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = Amt;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = 0m;
                            }
                        }
                        else
                        {
                            if (Amt < 0)
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = Amt * -1;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = 0;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = Amt * -1;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = 0m;
                            }
                            else
                            {
                                lCOA[IncomeParentIndex].MonthToDateDAmt = 0m;
                                lCOA[IncomeParentIndex].MonthToDateCAmt = Amt;
                                lCOA[IncomeParentIndex].CurrentMonthDAmt = 0m;
                                lCOA[IncomeParentIndex].CurrentMonthCAmt = Amt;
                            }
                        }

                        IncomeParentAcNo = lCOA[IncomeParentIndex].Parent;
                        f = IncomeParentAcNo.Length > 0;
                        break;
                    }
                }
            }
        }

        private void GetAllCOA(ref List<COA2> l, string ListAcNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;
            string[] AcNo = ListAcNo.Split(',');

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select Distinct A.AcNo, A.AcDesc, A.Parent, A.AcType ");
                SQL.AppendLine("From TblCOA A ");
                if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                {
                    SQL.AppendLine("Inner Join TblCOADtl B On A.AcNo=B.AcNo ");
                    SQL.AppendLine("And B.EntCode=@EntCode ");

                    if (mIsReportingFilterByEntity)
                    {
                        SQL.AppendLine("And B.EntCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupEntity ");
                        SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                }
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("And ( ");
                for (int x = 0; x < AcNo.Count(); ++x)
                {
                    SQL.AppendLine("(A.AcNo Like '" + AcNo[x] + "%') ");
                    if (x != AcNo.Count() - 1)
                        SQL.AppendLine(" Or ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By A.AcNo; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length > 0 ? Sm.GetLue(LueEntCode) == "Consolidate" ? "" : Sm.GetLue(LueEntCode) : Sm.GetLue(LueEntCode));

                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA2()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            OpeningBalanceDAmt = 0m,
                            OpeningBalanceCAmt = 0m,
                            OBBalance = 0m,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m,
                            CurrentMonthDAmt = 0m,
                            CurrentMonthCAmt = 0m,
                            CurrentMonthBalance = 0m,
                            YearToDateDAmt = 0m,
                            YearToDateCAmt = 0m,
                            YearToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournal(ref List<Journal2> l, string ListAcNo, string Yr, string Mth, string StartFrom)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("    Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("    From TblJournalHdr A ");
            SQL.AppendLine("    Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("        And Find_In_Set(B.AcNo, @AcNo) ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("    And B.EntCode=@EntCode ");

                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where 1=1 ");
            if (StartFrom.Length == 0)
            {
                SQL.AppendLine("    And Left(A.DocDt, 4)=@Yr ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            }
            else
            {
                SQL.AppendLine("    And Left(A.DocDt, 6)>=@StartFrom ");
                SQL.AppendLine("    And Left(A.DocDt, 6)<@YrMth ");
            }

            if (mIsRptTrialBalanceUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            if (StartFrom.Length > 0) Sm.CmParam<String>(ref cm, "@StartFrom", string.Concat(StartFrom, "01"));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllCOAOpeningBalance(ref List<COAOpeningBalance2> l, string ListAcNo, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo And C.ActInd = 'Y' ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.CancelInd='N' ");

            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("    And A.EntCode=@EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And A.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(A.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            if (mIsRptTrialBalanceUseProfitCenter)
            {
                SQL.AppendLine("    And A.ProfitCenterCode Is Not Null ");

                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                        SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("    ) ");
                    }
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COAOpeningBalance2()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetAllJournalCurrentMonth(ref List<Journal3> l, string ListAcNo, string Yr, string Mth)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            int i = 0;

            SQL.AppendLine("Select T.DocType, T.AcNo, SUm(T.DAmt) DAmt, Sum(T.CAmt) CAmt From ( ");
            SQL.AppendLine("Select 'New' As DocType, B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And Find_In_Set(B.AcNo, @AcNo) ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("    And B.EntCode=@EntCode ");

                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(B.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where Left(A.DocDt, 6) = @YrMth ");
            if (mIsRptTrialBalanceUseProfitCenter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    Filter = string.Empty;
                    i = 0;

                    SQL.AppendLine("    And A.CCCode Is Not Null ");
                    SQL.AppendLine("    And A.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And A.CCCode Is Not Null ");
                        SQL.AppendLine("    And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.DocType, T.AcNo ");
            SQL.AppendLine("Order By T.DocType, T.AcNo; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@AcNo", ListAcNo);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Journal3()
                        {
                            Year = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            DAmt = Sm.DrDec(dr, c[2]),
                            CAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }


        private void Process6(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                //hitung OB Balance
                lCOA[i].OBBalance = lCOA[i].OpeningBalanceDAmt - lCOA[i].OpeningBalanceCAmt;

                //hitung MTD Balance
                lCOA[i].MonthToDateBalance = lCOA[i].MonthToDateDAmt - lCOA[i].MonthToDateCAmt;

                //hitung Current Month Balance
                lCOA[i].CurrentMonthBalance = lCOA[i].MonthToDateBalance + lCOA[i].CurrentMonthDAmt - lCOA[i].CurrentMonthCAmt;

                //hitung YTD
                lCOA[i].YearToDateDAmt = lCOA[i].OpeningBalanceDAmt + lCOA[i].MonthToDateDAmt + lCOA[i].CurrentMonthDAmt;
                lCOA[i].YearToDateCAmt = lCOA[i].OpeningBalanceCAmt + lCOA[i].MonthToDateCAmt + lCOA[i].CurrentMonthCAmt;
                lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                //hitung level
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                    }
                    else
                    {
                        string currParent = lCOA[i].Parent;
                        foreach (var j in lCOA.Where(w => currParent == w.AcNo))
                        {
                            Parent = lCOA[i].Parent;
                            ParentLevel = j.Level;
                            lCOA[i].Level = j.Level + 1;

                            break;
                        }
                    }
                }

                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process8(ref List<EntityCOA> lEntityCOA)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName ");
            SQL.AppendLine("From TblCOA T ");
            if (ChkEntCode.Checked)
                SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo And U.EntCode Is Not Null And U.EntCode=@EntCode ");
            else
                SQL.AppendLine("Left Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("Order By T.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                if (ChkEntCode.Checked) Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process9(ref List<COA> lCOA)
        {
            int MaxAccountCategory = int.Parse(mMaxAccountCategory);
            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].AcType == "D")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceDAmt -
                        //tambahan berdasarkan OpeningBalanceCAmt bit.ly/31WjiQS
                        lCOA[i].OpeningBalanceCAmt +
                        lCOA[i].MonthToDateDAmt -
                        lCOA[i].MonthToDateCAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthDAmt -
                        lCOA[i].CurrentMonthCAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
                if (lCOA[i].AcType == "C")
                {
                    lCOA[i].MonthToDateBalance =
                        lCOA[i].OpeningBalanceCAmt -
                        //tambahan MonthToDateCAmt berdasarkan bit.ly/31WjiQS

                        lCOA[i].OpeningBalanceDAmt +
                        lCOA[i].MonthToDateCAmt -
                        lCOA[i].MonthToDateDAmt;

                    lCOA[i].CurrentMonthBalance =
                        lCOA[i].MonthToDateBalance +
                        lCOA[i].CurrentMonthCAmt -
                        lCOA[i].CurrentMonthDAmt;

                    lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;
                }

                if (mCurrentEarningFormulaType == "2" || mCurrentEarningFormulaType == "3")
                {
                    if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "6" && MaxAccountCategory > 6)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "7" && MaxAccountCategory > 7)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                    if (Sm.Left(lCOA[i].AcNo, 1) == "8" && MaxAccountCategory > 8)
                        lCOA[i].YearToDateBalance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;
                }
            }
        }

        private string GetSelectedCOAPLSetting(ref List<COAProfitLossSettingHdr> lCOAProfitLossSettingHdr)
        {
            string AcNo = string.Empty;

            foreach (var x in lCOAProfitLossSettingHdr)
            {
                if (AcNo.Length > 0)
                    AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        private void GetCOAProfitLossSettingDtl(ref List<COAProfitLossSettingDtl> lCOAProfitLossSettingDtl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("    Select Tb1.SettingCode, Tb1.AcNo, Tb1.ProfitLossSetting, tb2.Level, tb2.parent, tb1.ProcessInd From ( ");
            SQL.AppendLine("    SELECT A.SettingCode, B.AcNo, 'plus' AS ProfitLossSetting, B.ProcessInd ");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    INNER JOIN TblProfitLossSettingDtl B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' AND processind = 'Y' ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    SELECT A.SettingCode, B.AcNo, 'minus' AS ProfitLossSetting, B.ProcessInd ");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    INNER JOIN tblprofitlosssettingdtl2 B ON A.SettingCode = B.SettingCode ");
            SQL.AppendLine("    WHERE Yr = @Yr AND ActInd = 'Y' AND processind = 'Y'  ");
            SQL.AppendLine("    )tb1 ");
            SQL.AppendLine("    Inner Join TblCoa tb2 On Tb1.Acno = tb2.acNo ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SettingCode", "AcNo", "ProfitLossSetting", "ProcessInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAProfitLossSettingDtl.Add(new COAProfitLossSettingDtl()
                        {
                            SettingCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            ProfitLossSetting = Sm.DrStr(dr, c[2]),
                            ProcessInd = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetCOAProfitLossSettingHdr(ref List<COAProfitLossSettingHdr> lCOAProfitLossSettingHdr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("    SELECT A.SettingCode, A.CurrentEarningAcno As AcNo, B.AcType");
            SQL.AppendLine("    FROM tblProfitlossSettingHdr A ");
            SQL.AppendLine("    Inner Join TblCoa B On A.CurrentEarningAcno = B.Acno");
            SQL.AppendLine("    WHERE A.Yr = @Yr AND A.ActInd = 'Y'  ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SettingCode", "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOAProfitLossSettingHdr.Add(new COAProfitLossSettingHdr()
                        {
                            SettingCode = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            AcType = Sm.DrStr(dr, c[2]),
                            ProfitLossSetting = "Y",
                            ProcessInd = "Y",
                            OpeningBalanceCAmt = 0,
                            OpeningBalanceDAmt = 0,
                            MonthToDateDAmt = 0,
                            MonthToDateCAmt = 0,
                            MonthToDateBalance = 0,
                            CurrentMonthDAmt = 0,
                            CurrentMonthCAmt = 0,
                            CurrentMonthBalance = 0,
                            YearToDateDAmt = 0,
                            YearToDateCAmt = 0,
                            YearToDateBalance = 0,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ComputeProfitLoss(ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, ref List<COAProfitLossSettingDtl> lCOAPLSettingDtl, ref List<COA> lCOA)
        {

            foreach (var h in lCOAPLSettingHdr)
            {
                decimal penambahMTD = 0;
                decimal pengurangMTD = 0;
                decimal penambahCMD = 0;
                decimal pengurangCMD = 0;
                decimal penambahYTD = 0;
                decimal pengurangYTD = 0;

                foreach (var d in lCOAPLSettingDtl
                    .Where(w => w.SettingCode == h.SettingCode)
                    .OrderBy(d2 => d2.SettingCode)
                    .ThenBy(d3 => d3.ProfitLossSetting))
                {
                    foreach (var c in lCOA.Where(w => w.AcNo == d.AcNo))
                    {
                        if (d.ProfitLossSetting == "plus")
                        {
                            penambahMTD += c.MonthToDateBalance;
                            penambahCMD += c.CurrentMonthBalance;
                            penambahYTD += c.YearToDateBalance;
                        }
                        else
                        {
                            pengurangMTD += c.MonthToDateBalance;
                            pengurangCMD += c.CurrentMonthBalance;
                            pengurangYTD += c.YearToDateBalance;
                        }
                    }
                }

                h.OpeningBalanceCAmt = 0m;
                h.OpeningBalanceDAmt = 0m;
                h.MonthToDateBalance = penambahMTD - pengurangMTD;
                h.CurrentMonthBalance = penambahCMD - pengurangCMD;
                h.YearToDateBalance = penambahYTD - pengurangYTD;

                h.CurrentMonthDAmt = h.CurrentMonthBalance - h.MonthToDateBalance > 0m ? h.CurrentMonthBalance - h.MonthToDateBalance : 0m;
                h.CurrentMonthCAmt = h.CurrentMonthBalance - h.MonthToDateBalance < 0m ? (h.CurrentMonthBalance - h.MonthToDateBalance) * -1 : 0m;
                h.MonthToDateDAmt = h.MonthToDateBalance > 0m ? h.MonthToDateBalance : 0m;
                h.MonthToDateCAmt = h.MonthToDateBalance < 0m ? h.MonthToDateBalance * -1 : 0m;
                h.YearToDateDAmt = h.YearToDateBalance > 0m ? h.YearToDateBalance : 0m;
                h.YearToDateCAmt = h.YearToDateBalance < 0m ? h.YearToDateBalance * -1 : 0m;

                //if (h.AcType == "D")
                //{
                //    h.CurrentMonthDAmt = h.CurrentMonthBalance - h.MonthToDateBalance > 0m ? h.CurrentMonthBalance - h.MonthToDateBalance : 0m;
                //    h.CurrentMonthCAmt = h.CurrentMonthBalance - h.MonthToDateBalance < 0m ? (h.CurrentMonthBalance - h.MonthToDateBalance) * -1 : 0m;
                //    h.MonthToDateDAmt = h.MonthToDateBalance > 0m ? h.MonthToDateBalance : 0m;
                //    h.MonthToDateCAmt = h.MonthToDateBalance < 0m ? h.MonthToDateBalance * -1 : 0m;
                //    h.YearToDateDAmt = h.YearToDateBalance > 0m ? h.YearToDateBalance : 0m;
                //    h.YearToDateCAmt = h.YearToDateBalance < 0m ? h.YearToDateBalance * -1 : 0m;
                //}
                //else
                //{
                //    h.CurrentMonthCAmt = h.CurrentMonthBalance - h.MonthToDateBalance > 0m ? h.CurrentMonthBalance - h.MonthToDateBalance : 0m;
                //    h.CurrentMonthDAmt = h.CurrentMonthBalance - h.MonthToDateBalance < 0m ? (h.CurrentMonthBalance - h.MonthToDateBalance) * -1 : 0m;
                //    h.MonthToDateCAmt = h.MonthToDateBalance > 0m ? h.MonthToDateBalance : 0m;
                //    h.MonthToDateDAmt = h.MonthToDateBalance < 0m ? h.MonthToDateBalance * -1 : 0m;
                //    h.YearToDateCAmt = h.YearToDateBalance > 0m ? h.YearToDateBalance : 0m;
                //    h.YearToDateDAmt = h.YearToDateBalance < 0m ? h.YearToDateBalance * -1 : 0m;
                //}

                foreach (var c in lCOA.Where(w => w.AcNo == h.AcNo))
                {
                    c.MonthToDateDAmt = h.MonthToDateDAmt;
                    c.MonthToDateCAmt = h.MonthToDateCAmt;
                    c.MonthToDateBalance = h.MonthToDateBalance;
                    c.CurrentMonthDAmt = h.CurrentMonthDAmt;
                    c.CurrentMonthCAmt = h.CurrentMonthCAmt;
                    c.CurrentMonthBalance = h.CurrentMonthBalance;
                    c.YearToDateDAmt = h.YearToDateDAmt;
                    c.YearToDateCAmt = h.YearToDateCAmt;
                    c.YearToDateBalance = h.YearToDateBalance;
                }
            }
        }

        private void ComputeProfitLoss2(ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, ref List<COA> lCOA)
        {

            foreach (var h in lCOAPLSettingHdr
                .OrderBy(h2 => h2.SettingCode)
                .ThenBy(h3 => h3.AcNo))
            {
                foreach (var c in lCOA
                    .Where(w => w.AcNo == h.AcNo)
                    .OrderBy(c2 => c2.AcNo))
                {
                    //yg ditarik hanya year to date saja
                    //c.OpeningBalanceCAmt = h.OpeningBalanceCAmt;
                    //c.OpeningBalanceDAmt = h.OpeningBalanceDAmt;
                    c.MonthToDateDAmt = h.MonthToDateDAmt;
                    c.MonthToDateCAmt = h.MonthToDateCAmt;
                    c.MonthToDateBalance = h.MonthToDateBalance;
                    c.CurrentMonthDAmt = h.CurrentMonthDAmt;
                    c.CurrentMonthCAmt = h.CurrentMonthCAmt;
                    c.CurrentMonthBalance = h.CurrentMonthBalance;
                    c.YearToDateDAmt = h.YearToDateDAmt;
                    c.YearToDateCAmt = h.YearToDateCAmt;
                    c.YearToDateBalance = h.YearToDateBalance;
                }
            }

        }

        private void Process11(ref List<COA> lCOA, ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr)
        {
            if (lCOAPLSettingHdr.Count > 0)
            {
                lCOAPLSettingHdr.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lCOAPLSettingHdr.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lCOAPLSettingHdr[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lCOAPLSettingHdr[i].AcNo, lCOA[j].AcNo) ||
                            lCOAPLSettingHdr[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lCOAPLSettingHdr[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            //lCOA[j].OpeningBalanceDAmt += lCOAPLSettingHdr[i].OpeningBalanceDAmt;
                            //lCOA[j].OpeningBalanceCAmt += lCOAPLSettingHdr[i].OpeningBalanceCAmt;


                            //lCOA[j].MonthToDateDAmt += lCOAPLSettingHdr[i].MonthToDateDAmt;
                            //lCOA[j].MonthToDateCAmt += lCOAPLSettingHdr[i].MonthToDateCAmt;
                            //lCOA[j].MonthToDateBalance += lCOAPLSettingHdr[i].MonthToDateBalance;

                            //lCOA[j].CurrentMonthDAmt += lCOAPLSettingHdr[i].CurrentMonthDAmt;
                            //lCOA[j].CurrentMonthCAmt += lCOAPLSettingHdr[i].CurrentMonthCAmt;
                            //lCOA[j].CurrentMonthBalance += lCOAPLSettingHdr[i].CurrentMonthBalance;


                            lCOA[j].YearToDateDAmt += lCOAPLSettingHdr[i].YearToDateDAmt;
                            lCOA[j].YearToDateCAmt += lCOAPLSettingHdr[i].YearToDateCAmt;
                            lCOA[j].YearToDateBalance += lCOAPLSettingHdr[i].YearToDateBalance;

                            if (string.Compare(lCOA[j].AcNo, lCOAPLSettingHdr[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
        }

        private void Process12(ref List<COA> lCOA, ref List<COAProfitLossSettingHdr> lCOAPLSettingHdr, string GetSelectedCOAPLSetting)
        {
            foreach (var x in lCOAPLSettingHdr.OrderBy(w => w.AcNo))
            {
                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, GetSelectedCOAPLSetting))))
                {
                    if (x.AcNo.Count(y => y == '.') != i.AcNo.Count(y => y == '.') && x.AcNo.StartsWith(i.AcNo))
                    {
                        //i.OpeningBalanceDAmt = 0m;
                        //i.OpeningBalanceCAmt = 0m;

                        i.MonthToDateDAmt = 0m;
                        i.MonthToDateCAmt = 0m;
                        i.MonthToDateBalance = 0m;

                        i.CurrentMonthDAmt = 0m;
                        i.CurrentMonthCAmt = 0m;
                        i.CurrentMonthBalance = 0m;


                        i.YearToDateDAmt = 0m;
                        i.YearToDateCAmt = 0m;
                        i.YearToDateBalance = 0m;
                    }
                }

                foreach (var i in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && !w.HasChild))
                {
                    foreach (var j in lCOA.Where(w => Sm.Left(w.AcNo, 1) == Sm.Left(x.AcNo, 1) && w.HasChild && (!Sm.Find_In_Set(w.AcNo, GetSelectedCOAPLSetting))))
                    {
                        if (x.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && x.AcNo.StartsWith(j.AcNo))
                        {
                            if (
                                i.AcNo.Count(y => y == '.') == j.AcNo.Count(y => y == '.') && Sm.CompareStr(i.AcNo, j.AcNo) ||
                                i.AcNo.Count(y => y == '.') != j.AcNo.Count(y => y == '.') && i.AcNo.StartsWith(j.AcNo)
                            )
                            {
                                //j.OpeningBalanceDAmt += i.OpeningBalanceDAmt; 
                                //j.OpeningBalanceCAmt += i.OpeningBalanceCAmt;

                                j.MonthToDateDAmt += i.MonthToDateDAmt;
                                j.MonthToDateCAmt += i.MonthToDateCAmt;
                                j.MonthToDateBalance += i.MonthToDateBalance;

                                j.CurrentMonthDAmt += i.CurrentMonthDAmt;
                                j.CurrentMonthCAmt += i.CurrentMonthCAmt;
                                j.CurrentMonthBalance += i.CurrentMonthBalance;

                                j.YearToDateDAmt += i.YearToDateDAmt;
                                j.YearToDateCAmt += i.YearToDateCAmt;

                                if (j.AcType == i.AcType) j.YearToDateBalance += i.YearToDateBalance;
                                else j.YearToDateBalance -= i.YearToDateBalance;

                                if (string.Compare(j.AcNo, i.AcNo) == 0)
                                    break;
                            }
                        }
                    }
                }


            }
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        private void LueLevelFrom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelFrom, new Sm.RefreshLue1(SetLueLevelFromTo));
        }

        private void LueLevelTo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelTo, new Sm.RefreshLue1(SetLueLevelFromTo));
        }

        private void TxtPeriod_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LuePeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Period");
        }

        #endregion

        #endregion

        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string Alias { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal OBBalance { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }

            public string ProcessInd { get; set; }

            public string ProfitLossSetting { get; set; }
        }

        private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class EntityCOA
        {
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

        private class COATemp
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal OBBalance { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class COA2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal OBBalance { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }
        }

        private class Journal2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Journal3
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COAOpeningBalance2
        {
            public string Year { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Formula
        {
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
        }

        private class COAProfitLossSettingDtl
        {
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string ProfitLossSetting { get; set; }
            public string ProcessInd { get; set; }

        }

        private class COAProfitLossSettingHdr
        {
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string ProfitLossSetting { get; set; }
            public string ProcessInd { get; set; }
            public string AcType { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal OBBalance { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal MonthToDateBalance { get; set; }
            public decimal CurrentMonthDAmt { get; set; }
            public decimal CurrentMonthCAmt { get; set; }
            public decimal CurrentMonthBalance { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal YearToDateBalance { get; set; }

        }

        #endregion
    }
}
