﻿#region Update
/*
    04/18/2017 (HAR) merubah querynya 
    05/31/2017 (HAR) rubah query untuk split transfer date dan paid date
    03/08/2017 (HAR) revisi query yang AR downpayment
    07/08/2017 (HAR) Tambah total group
    13/09/2017 (HAR) bug fixing balance
    19/09/2017 (HAR) Tambah filter customer / buyer
    26/09/2017 (HAR) down payment ambil dari sales invoice
    30/09/2017 (HAR) bug fixing rumus balance
    28/06/2018 [TKG] tambah rincian AR downpayment
    09/07/2018 [TKG] Digit di belakang koma di kolom BALANCE Diubah menjadi 4 digit seperti di Total Value 
    07/02/2019 [HAR] DP date muncul padahal tidak ada AR downpayment
    12/04/2019 [WED] rumus FOB disamakan dengan FrmRptExportRealization & FrmRptExportRealization2
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptARExportCollection : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptARExportCollection(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X1.DocNo, X1.LocalDocNo, X1.PortCode, X1.PortName, X1.CtCode, X1.Buyer, X1.PEB, X1.PEBDt, ");
            SQL.AppendLine("Round(X1.FOB, 2) FOB, Truncate(X1.Freight, 2) Freight, Left(X2.ARDownpaymentRemark, 10) As DocDt, X1.VoucherDt, Truncate(X1.Amt, 2) Amt, X1.CurCode, Truncate(X1.AmtARDP, 2) AmtARDP, ");
            SQL.AppendLine("Truncate(X1.Amtpart1, 2) Amtpart1, Truncate(X1.Amtpart2, 2) Amtpart2, Truncate(X1.Amtpart3, 2) Amtpart3, Truncate(X1.Amtpart4, 2) Amtpart4, ");
            SQL.AppendLine("X1.VoucherDt1, X1.VoucherDt2, X1.VoucherDt3, X1.VoucherDt4, ");
            SQL.AppendLine("truncate((X1.FOB+X1.Freight-X1.AmtARDP-X1.Amt), 1) As Balance, ");
            SQL.AppendLine("X2.ARDownpaymentRemark ");
            SQL.AppendLine("From(  ");
            SQL.AppendLine("    Select X.DocNo, X.LocalDocNo, X.PortCode, X.PortName, X.CtCode, X.Buyer, X.PEB, X.PEBDt,   ");
            SQL.AppendLine("    X.Freight As Freight,  ");
            //SQL.AppendLine("    (SUM(X.Amount))-(X.Freight) As FOB, ");
            SQL.AppendLine("    If(X.FreightTHC > 0, ");
            SQL.AppendLine("        SUM((X.Amount)-(X.Qty/X.TotalQty*(X.FreightTHC))), ");
            SQL.AppendLine("        SUM(X.Amount) ");
            SQL.AppendLine("    ) As FOB, ");
            SQL.AppendLine("    DATE_FORMAT(X.DocDt,'%d/%m/%Y') As DocDt, X.VoucherDt, X.Amt Amt, X.CurCode, X.AmtARDP As AmtARDP,  ");
            SQL.AppendLine("    X.Amtpart1 As Amtpart1, X.Amtpart2 As Amtpart2, X.Amtpart3 As Amtpart3, X.Amtpart4 As Amtpart4, ");
            SQL.AppendLine("    X.VoucherDtpart1 As VoucherDt1, X.VoucherDtpart2 As VoucherDt2, X.VoucherDtpart3 As VoucherDt3, X.VoucherDtpart4 As VoucherDt4, ");
            SQL.AppendLine("    X.SODocNo ");
            SQL.AppendLine("    From (   ");
            SQL.AppendLine("        Select N.InvoiceDocNo As DocNo, ");
            SQL.AppendLine("        A.DocDt AS DocDtSP, A.LocalDocNo, K.PortCode, K.PortName, ");
            SQL.AppendLine("        A.CtCode, J.CtName As Buyer, A.PEB, A.PEBDt, H.CurCode, ");
            //SQL.AppendLine("        ifnull((Round(I.UPrice, 4) * Round(E.Qty, 4)), 0.00) As Amount, ");
            SQL.AppendLine("        IfNull((I.UPrice * E.Qty), 0.00) As Amount, ");
            SQL.AppendLine("        Round(E.Qty, 4) As Qty, P.TotalQty, ");
            SQL.AppendLine("        If(D.Freight1>0,D.Freight1-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight2>0,D.Freight2-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight3>0,D.Freight3-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight4>0,D.Freight4-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight5>0,D.Freight5-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight6>0,D.Freight6-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight7>0,D.Freight7-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight8>0,D.Freight8-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight9>0,D.Freight9-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight10>0,D.Freight10-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight11>0,D.Freight11-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight12>0,D.Freight12-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight13>0,D.Freight13-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight14>0,D.Freight14-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight15>0,D.Freight15-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight16>0,D.Freight16-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight17>0,D.Freight17-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight18>0,D.Freight18-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight19>0,D.Freight19-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight20>0,D.Freight20-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight21>0,D.Freight21-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight22>0,D.Freight22-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight23>0,D.Freight23-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight24>0,D.Freight24-O.Parvalue,0.00)+ ");
            SQL.AppendLine("        If(D.Freight25>0,D.Freight25-O.Parvalue,0.00) ");
            SQL.AppendLine("        As Freight, ");
            SQL.AppendLine("        Case E.SectionNo ");
            SQL.AppendLine("            When '1' Then D.Freight1 - O.ParValue ");
            SQL.AppendLine("            When '2' Then D.Freight2 - O.ParValue ");
            SQL.AppendLine("            When '3' Then D.Freight3 - O.ParValue ");
            SQL.AppendLine("            When '4' Then D.Freight4 - O.ParValue ");
            SQL.AppendLine("            When '5' Then D.Freight5 - O.ParValue ");
            SQL.AppendLine("            When '6' Then D.Freight6 - O.ParValue ");
            SQL.AppendLine("            When '7' Then D.Freight7 - O.ParValue ");
            SQL.AppendLine("            When '8' Then D.Freight8 - O.ParValue ");
            SQL.AppendLine("            When '9' Then D.Freight9 - O.ParValue ");
            SQL.AppendLine("            When '10' Then D.Freight10 - O.ParValue ");
            SQL.AppendLine("            When '11' Then D.Freight11 - O.ParValue ");
            SQL.AppendLine("            When '12' Then D.Freight12 - O.ParValue ");
            SQL.AppendLine("            When '13' Then D.Freight13 - O.ParValue ");
            SQL.AppendLine("            When '14' Then D.Freight14 - O.ParValue ");
            SQL.AppendLine("            When '15' Then D.Freight15 - O.ParValue ");
            SQL.AppendLine("            When '16' Then D.Freight16 - O.ParValue ");
            SQL.AppendLine("            When '17' Then D.Freight17 - O.ParValue ");
            SQL.AppendLine("            When '18' Then D.Freight18 - O.ParValue ");
            SQL.AppendLine("            When '19' Then D.Freight19 - O.ParValue ");
            SQL.AppendLine("            When '20' Then D.Freight20 - O.ParValue ");
            SQL.AppendLine("            When '21' Then D.Freight21 - O.ParValue ");
            SQL.AppendLine("            When '22' Then D.Freight22 - O.ParValue ");
            SQL.AppendLine("            When '23' Then D.Freight23 - O.ParValue ");
            SQL.AppendLine("            When '24' Then D.Freight24 - O.ParValue ");
            SQL.AppendLine("            When '25' Then D.Freight25 - O.ParValue ");
            SQL.AppendLine("        end as FreightTHC,  ");
            SQL.AppendLine("        N.DocDt, ");
            SQL.AppendLine("        ifnull(N.Amt, 0) As Amt, ");
            SQL.AppendLine("        N.VoucherDt, ");
            SQL.AppendLine("        ifnull(M2.Downpayment, 0) As AmtARDP, ");
            SQL.AppendLine("        SUBSTRING_INDEX(N.Amt2,'#',1) AS Amtpart1, ");
			SQL.AppendLine("        case ");
			SQL.AppendLine("        When (length(N.Amt2)-length(replace(N.Amt2, '#', '')) >= 1)    ");
			SQL.AppendLine("        then SUBSTRING_INDEX(SUBSTRING_INDEX(N.Amt2,'#',2),'#',-1)     ");
			SQL.AppendLine("        else null end AS Amtpart2,     ");
			SQL.AppendLine("        case ");
			SQL.AppendLine("        When (length(N.Amt2)-length(replace(N.Amt2, '#', '')) >= 2)    ");
			SQL.AppendLine("        then SUBSTRING_INDEX(SUBSTRING_INDEX(N.Amt2,'#',3),'#',-1)     ");
			SQL.AppendLine("        else null end AS Amtpart3,     ");
			SQL.AppendLine("        case     ");
			SQL.AppendLine("        When (length(N.Amt2)-length(replace(N.Amt2, '#', '')) >= 3)    ");
			SQL.AppendLine("        then SUBSTRING_INDEX(N.Amt2, '#',(length(N.Amt2)-length(replace(N.Amt2, '#', ''))-2)*-1)    ");
            SQL.AppendLine("        else null end AS Amtpart4,  ");
            SQL.AppendLine("        SUBSTRING_INDEX(N.VoucherDt,',',1) AS VoucherDtpart1,   ");
            SQL.AppendLine("        case   ");
            SQL.AppendLine("        When (length(N.VoucherDt)-length(replace(N.VoucherDt, ',', '')) >= 1)  ");
            SQL.AppendLine("        then SUBSTRING_INDEX(SUBSTRING_INDEX(N.VoucherDt,',',2),',',-1)   ");
            SQL.AppendLine("        else null end AS VoucherDtpart2,   ");
            SQL.AppendLine("        case   ");
            SQL.AppendLine("        When (length(N.VoucherDt)-length(replace(N.VoucherDt, ',', '')) >= 2)  ");
            SQL.AppendLine("        then SUBSTRING_INDEX(SUBSTRING_INDEX(N.VoucherDt,',',3),',',-1)   ");
            SQL.AppendLine("        else null end AS VoucherDtpart3,   ");
            SQL.AppendLine("        case   ");
            SQL.AppendLine("        When (length(N.VoucherDt)-length(replace(N.VoucherDt, ',', '')) >= 3)  ");
            SQL.AppendLine("        then SUBSTRING_INDEX(N.VoucherDt, ',',(length(N.VoucherDt)-length(replace(N.VoucherDt, ',', ''))-2)*-1)  ");
            SQL.AppendLine("        else null end AS VoucherDtpart4, ");
            SQL.AppendLine("        H.DocNo As SODocNo ");
            SQL.AppendLine("        From TblSP A   ");
            SQL.AppendLine("        Inner Join TblSiHdr B On A.DocNo=B.SpDocNo   ");
            SQL.AppendLine("        Inner Join TblPLHdr D On B.DocNo=D.SiDocNo   ");
            SQL.AppendLine("        Inner Join TblPLDtl E On D.DocNo=E.DocNo   ");
            SQL.AppendLine("        Inner Join TblSinv F On D.DocNo=F.PlDocNo   ");
            SQL.AppendLine("        Inner Join TblSODtl G On E.SoDocNo = G.DocNo And E.SoDno = G.DNo   ");
            SQL.AppendLine("        Inner Join TblSOHdr H On G.DocNo = H.DocNo And E.SODocNo = H.DocNo And H.Status<>'C' ");
            SQL.AppendLine("        Inner Join TblCtQtDtl I On H.CtQtDocNo = I.DocNo And G.CtQtDNo = I.Dno   ");
            SQL.AppendLine("        Inner Join TblCustomer J On A.CtCode=J.CtCode   ");
            SQL.AppendLine("        Inner Join TblPort K On A.PortCode2=K.PortCode   ");
            SQL.AppendLine("        Left Join TblDoct2Hdr L On E.DocNo=L.PLDocNo And E.SectionNo=L.SectionNo ");
            SQL.AppendLine("        Left Join TblDoct2Dtl3 L2 On L.DocNo=L2.DocNo And E.DNo=L2.PLDNo And L2.Qty>0 ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceDtl M On L.DocNo=M.DOCtDocNo And L2.DNo=M.DoctDNo   ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceHdr M2 On M.DocNo=M2.DocNo And M2.cancelInd = 'N'  ");
            SQL.AppendLine("        Left Join (   ");
		    SQL.AppendLine("                    Select P.InvoiceDocNo, ");
            SQL.AppendLine("					Max(O.DocDt) As DocDt,   ");
			SQL.AppendLine("					group_concat(distinct DATE_FORMAT(R.DocDt,'%d/%m/%Y')) As VoucherDt, ");
            SQL.AppendLine("					SUM(P.Amt) As Amt,  ");
			SQL.AppendLine("					group_concat(P.Amt separator '#') As Amt2 ");
			SQL.AppendLine("					From TblIncomingpaymentHdr O ");
			SQL.AppendLine("					Inner Join TblIncomingpaymentDtl P On O.DocNo=P.DocNo ");
			SQL.AppendLine("					Inner Join TblVoucherRequestHdr Q On O.VoucherRequestDocNo=Q.DocNo And O.CancelInd='N' "); 
			SQL.AppendLine("					Left Join TblVoucherHdr R On Q.VoucherDocNo=R.DocNo ");
			SQL.AppendLine("					Where O.CancelInd <> 'C' ");
            SQL.AppendLine("					Group by P.Invoicedocno ");
            SQL.AppendLine("        )N On M.DocNo=N.InvoiceDocNo ");
            SQL.AppendLine("        Inner Join TblParameter O On 0=0 And O.Parcode='THC' ");
            SQL.AppendLine("        Inner Join   ");
            SQL.AppendLine("        (  ");
            SQL.AppendLine("             Select A.DocNo, A2.SectionNo, SUM(Round(A2.Qty, 4)) As TotalQty, ");
            SQL.AppendLine("	            SUM(Round(A2.QtyInventory, 4)) As TotalKubik ");
            SQL.AppendLine("	            From TblSInv X   ");
            SQL.AppendLine("	            Inner Join TblPlhdr A On A.DocNo = X.PlDocNo   ");
            SQL.AppendLine("	            Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo   ");
            SQL.AppendLine("	            Inner Join TblItem B On A2.ItCode = B.ItCode    ");
            SQL.AppendLine("	            Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
            SQL.AppendLine("	            Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo And D.Status<>'C' ");
            SQL.AppendLine("	            Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno  ");
            SQL.AppendLine("	            Group By A.DocNo, A2.SectionNo ");
            SQL.AppendLine("        )P On D.DocNo = P.DocNo And E.SectionNo = P.SectionNo  ");
            SQL.AppendLine("        Where A.Status<>'C' ");
            SQL.AppendLine("    ) X  ");
            SQL.AppendLine("    Group By X.DocNo, X.LocalDocNo, X.PortCode, X.PortName, ");
            SQL.AppendLine("    X.CtCode, X.Buyer, X.PEB, X.PEBDt, X.Freight, X.DocDt, X.VoucherDt, X.Amt, X.CurCode, X.AmtARDP, ");
            SQL.AppendLine("    X.Amtpart1, X.Amtpart2, X.Amtpart3, X.Amtpart4, ");
            SQL.AppendLine("    X.VoucherDtpart1, X.VoucherDtpart2, X.VoucherDtpart3, X.VoucherDtpart4, ");
            SQL.AppendLine("    X.SODocNo ");
            SQL.AppendLine(") X1  ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select SODocNo As DocNo, ");
            SQL.AppendLine("    Group_Concat(Concat(Date_Format(DocDt,'%d/%m/%Y'), ' (', Convert(Format(Amt, 2) Using utf8),')') Order By DocDt Separator ', ') As ARDownpaymentRemark ");
            SQL.AppendLine("    From TblARDownpayment ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Status='A' ");
            SQL.AppendLine("    And SODocNo Is Not Null ");
            SQL.AppendLine("    Group By SODocNo ");
            SQL.AppendLine(") X2 On X1.SODocNo=X2.DocNo ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Document",
                        "Invoice#", 
                        "Country",
                        "Buyer",
                        "PEB",
                        //6-10
                        "PEB Date",
                        "FOB Value",
                        "Freight",
                        "DP"+Environment.NewLine+"Date",
                        "AR Downpayment",
                        //11-15
                        "Transfer"+Environment.NewLine+"Date",
                        "Amount 1",
                        "Transfer"+Environment.NewLine+"Date 2",
                        "Amount 2",
                        "Transfer"+Environment.NewLine+"Date 3",
                        //16-20
                        "Amount 3",
                        "Transfer"+Environment.NewLine+"Date 4",
                        "Amount 4",
                        "Total Amount"+Environment.NewLine+"Voucher",
                        "Balance",

                        //21
                        "SO's Downpayment Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 120, 200, 200, 120,  

                        //6-10
                        100, 120, 100, 100, 150, 

                        //11-15
                        100, 150, 100, 150, 100, 
                        
                        //16-20
                        150, 100, 150, 150, 150,

                        //21
                        300
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 10, 12, 14, 16, 18, 19 }, 8);
            Sm.GrdFormatDec(Grd1, new int[] { 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 21 }, false);
            Grd1.Cols[21].Move(11);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

               Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "X1.PEBDt");
               Sm.FilterStr(ref Filter, ref cm, TxtPortCode.Text, "X1.PortName", false);
               Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "X1.LocalDocNo", false);
               Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "X1.CtCode", true);

               Sm.ShowDataInGrid(
                         ref Grd1, ref cm,
                         mSQL + Filter + "Order by X1.PEBDt, X1.LocalDocNo;",
                         new string[]
                        {
                            //0
                            "DocNo",
                            //1-5
                            "LocalDocNo", "PortName", "Buyer", "PEB", "PEBDt",                             
                            //6-10
                            "FOB", "Freight", "DocDt", "AmtARDP",  "VoucherDt1",   
                            //11-15
                            "Amtpart1", "VoucherDt2", "Amtpart2", "VoucherDt3", "Amtpart3", 
                            //16-20
                            "VoucherDt4",  "Amtpart4", "Amt", "Balance", "ARDownpaymentRemark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            if (Sm.GetGrdDec(Grd, Row, 10) > 0m)
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            }
                        }, true, false, false, false
                    );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 10, 12, 14, 16, 18, 19, 20 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "PEB date");
        }

        private void TxtPortCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPortCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Country");
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Document");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Buyer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
     
    }
}
