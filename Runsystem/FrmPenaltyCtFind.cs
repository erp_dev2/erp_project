﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPenaltyCtFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPenaltyCt mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPenaltyCtFind(FrmPenaltyCt FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PenaltyCtCode, A.PenaltyCtName, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From tblpenaltycategory A ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Penalty"+Environment.NewLine+"Category Code", 
                        "Penalty"+Environment.NewLine+" Category Name",
                        "Remark",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        
                        //6-9
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                        
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 150, 100,  100, 100,  
                        
                        //6-9
                        100, 100, 100, 100

                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 5, 8 });
            Sm.GrdFormatTime(Grd1, new int[] { 6, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPenaltyCtCode.Text, new string[] { "A.PenaltyCtCode", "A.PenaltyCtName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[]
                        {
                              //0
                             "PenaltyCtCode", 

                              //1-5
                             "PenaltyCtName",
                             "Remark",
                             "CreateBy",
                             "CreateDt", 
                             "LastUpBy",

                             //6
                             "LastUpDt"
                             
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 9, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event


        private void TxtPenaltyCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPenaltyCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Penalty Category");
        }
       
        #endregion

       
    }
}
