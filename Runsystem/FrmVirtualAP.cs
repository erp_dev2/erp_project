﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVirtualAP : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmVirtualAPFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmVirtualAP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtVirtualAPCode, TxtVirtualAPName, TxtSSID, ChkActInd }, true);
                    TxtVirtualAPCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtVirtualAPCode, TxtVirtualAPName, TxtSSID }, false);
                    TxtVirtualAPCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtVirtualAPName, TxtSSID, ChkActInd }, false);
                    TxtVirtualAPName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtVirtualAPCode, TxtVirtualAPName });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVirtualAPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtVirtualAPCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtVirtualAPCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblVirtualAP Where VirtualAPCode=@VirtualAPCode" };
                Sm.CmParam<String>(ref cm, "@VirtualAPCode", TxtVirtualAPCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblVirtualAP(VirtualAPCode, VirtualAPName, SSID, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@VirtualAPCode, @VirtualAPName, @SSID, @ActInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update VirtualAPName=@VirtualAPName, SSID=@SSID, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@VirtualAPCode", TxtVirtualAPCode.Text);
                Sm.CmParam<String>(ref cm, "@VirtualAPName", TxtVirtualAPName.Text);
                Sm.CmParam<String>(ref cm, "@SSID", TxtSSID.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtVirtualAPCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string VirtualAPCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@VirtualAPCode", VirtualAPCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select VirtualAPCode, VirtualAPName, SSID, ActInd From TblVirtualAP Where VirtualAPCode=@VirtualAPCode;",
                        new string[]{ "VirtualAPCode", "VirtualAPName", "SSID", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtVirtualAPCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtVirtualAPName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtSSID.EditValue = Sm.DrStr(dr, c[2]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtVirtualAPCode, "Virtual AP code", false) ||
                Sm.IsTxtEmpty(TxtVirtualAPName, "Virtual AP name", false) ||
                Sm.IsTxtEmpty(TxtSSID, "SSID", false) ||
                IsCodeAlreadyExisted();
        }

        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtVirtualAPCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select VirtualAPCode From TblVirtualAP Where VirtualAPCode=@Param;",
                    TxtVirtualAPCode.Text,
                    "Virtual AP code ( " + TxtVirtualAPCode.Text + " ) already existed."
                    );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtVirtualAPCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVirtualAPCode);
        }

        private void TxtVirtualAPName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVirtualAPName);
        }

        private void TxtSSID_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSSID);
        }

        #endregion

        #endregion
    }
}
