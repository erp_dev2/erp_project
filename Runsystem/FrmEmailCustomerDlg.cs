﻿#region Update

/*
 *  [ICA/KIM] New Apps 
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmailCustomerDlg : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty;
        private FrmEmailCustomer mFrmParent;

        #endregion

        #region Constructor

        public FrmEmailCustomerDlg(FrmEmailCustomer FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From (");
            SQL.AppendLine("select A.CtCode, B.CtName, A.DocNo InvoiceDocNo, A.Amt, A.Remark, Group_concat(Distinct(C.Email)) Email ");
            SQL.AppendLine("from tblsalesinvoicehdr A ");
            SQL.AppendLine("inner join tblcustomer B on A.CtCode = B.CtCode ");
            SQL.AppendLine("inner join tblcustomercontactperson C On B.CtCode = C.CtCode And C.Email is not null ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    select 1 ");
            SQL.AppendLine("    from tblincomingpaymenthdr T ");
            SQL.AppendLine("    Inner join tblincomingpaymentdtl T2 ");
            SQL.AppendLine("    Where T2.InvoiceDocNo = A.DocNo ");
            SQL.AppendLine("    And T.CancelInd = 'N' ");
            SQL.AppendLine("    And T.Status = 'A' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Group by A.CtCode, B.CtName, A.DocNo, A.Amt, A.Remark ");
            SQL.AppendLine(") T");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Customer's Code", 
                        "Customer's Name",
                        "Customer's Email",
                        "Sales Invoice#",

                        //6
                        "Amount",
                        "Remark"
                    }, new int[]
                    {
                        //0
                        50,

                        //1-5
                        20, 130, 300, 200, 150, 

                        //6
                        130, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 0=0";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtInvoiceDocNo.Text, new string[] { "InvoiceDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), new string[] { "CtCode" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By CtName;",
                        new string[] 
                        { 
                            //0
                            "CtCode", 
                            
                            //1-5
                            "CtName", "Email", "InvoiceDocNo", "Amt", "Remark", 
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);

                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");

        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = String.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 5));
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (Sm.CompareStr(key, String.Concat(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(mFrmParent.Grd1, Index, 4))))
                    return true;
            }
            return false;
        }

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkInvoiceDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Sales Invoice#");
        }

        private void TxtInvoiceDocNo_Validated(object sender, EventArgs e)
        {

        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

    }
}
