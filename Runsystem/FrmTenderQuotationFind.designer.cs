﻿namespace RunSystem
{
    partial class FrmTenderQuotationFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtTenderDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkTenderDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtQtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkQtDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPORequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkPORequestDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtTQRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkTQRDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenderDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTenderDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkQtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPORequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPORequestDocNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTQRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTQRDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(861, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtTQRDocNo);
            this.panel2.Controls.Add(this.ChkTQRDocNo);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtPORequestDocNo);
            this.panel2.Controls.Add(this.ChkPORequestDocNo);
            this.panel2.Size = new System.Drawing.Size(861, 54);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(861, 419);
            this.Grd1.TabIndex = 22;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "Tender#";
            // 
            // TxtTenderDocNo
            // 
            this.TxtTenderDocNo.EnterMoveNextControl = true;
            this.TxtTenderDocNo.Location = new System.Drawing.Point(81, 5);
            this.TxtTenderDocNo.Name = "TxtTenderDocNo";
            this.TxtTenderDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTenderDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtTenderDocNo.Properties.MaxLength = 30;
            this.TxtTenderDocNo.Size = new System.Drawing.Size(233, 20);
            this.TxtTenderDocNo.TabIndex = 17;
            this.TxtTenderDocNo.Validated += new System.EventHandler(this.TxtTenderDocNo_Validated);
            // 
            // ChkTenderDocNo
            // 
            this.ChkTenderDocNo.Location = new System.Drawing.Point(318, 4);
            this.ChkTenderDocNo.Name = "ChkTenderDocNo";
            this.ChkTenderDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTenderDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkTenderDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkTenderDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkTenderDocNo.Properties.Caption = " ";
            this.ChkTenderDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTenderDocNo.Size = new System.Drawing.Size(21, 22);
            this.ChkTenderDocNo.TabIndex = 18;
            this.ChkTenderDocNo.ToolTip = "Remove filter";
            this.ChkTenderDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkTenderDocNo.ToolTipTitle = "Run System";
            this.ChkTenderDocNo.CheckedChanged += new System.EventHandler(this.ChkTenderDocNo_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 14);
            this.label1.TabIndex = 19;
            this.label1.Text = "Quotation#";
            // 
            // TxtQtDocNo
            // 
            this.TxtQtDocNo.EnterMoveNextControl = true;
            this.TxtQtDocNo.Location = new System.Drawing.Point(81, 26);
            this.TxtQtDocNo.Name = "TxtQtDocNo";
            this.TxtQtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQtDocNo.Properties.MaxLength = 30;
            this.TxtQtDocNo.Size = new System.Drawing.Size(233, 20);
            this.TxtQtDocNo.TabIndex = 20;
            this.TxtQtDocNo.Validated += new System.EventHandler(this.TxtQtDocNo_Validated);
            // 
            // ChkQtDocNo
            // 
            this.ChkQtDocNo.Location = new System.Drawing.Point(318, 25);
            this.ChkQtDocNo.Name = "ChkQtDocNo";
            this.ChkQtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkQtDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkQtDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkQtDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkQtDocNo.Properties.Caption = " ";
            this.ChkQtDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkQtDocNo.Size = new System.Drawing.Size(21, 22);
            this.ChkQtDocNo.TabIndex = 21;
            this.ChkQtDocNo.ToolTip = "Remove filter";
            this.ChkQtDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkQtDocNo.ToolTipTitle = "Run System";
            this.ChkQtDocNo.CheckedChanged += new System.EventHandler(this.ChkQtDocNo_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "PO Request#";
            // 
            // TxtPORequestDocNo
            // 
            this.TxtPORequestDocNo.EnterMoveNextControl = true;
            this.TxtPORequestDocNo.Location = new System.Drawing.Point(92, 26);
            this.TxtPORequestDocNo.Name = "TxtPORequestDocNo";
            this.TxtPORequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPORequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPORequestDocNo.Properties.MaxLength = 30;
            this.TxtPORequestDocNo.Size = new System.Drawing.Size(233, 20);
            this.TxtPORequestDocNo.TabIndex = 13;
            this.TxtPORequestDocNo.Validated += new System.EventHandler(this.TxtPORequestDocNo_Validated);
            // 
            // ChkPORequestDocNo
            // 
            this.ChkPORequestDocNo.Location = new System.Drawing.Point(329, 25);
            this.ChkPORequestDocNo.Name = "ChkPORequestDocNo";
            this.ChkPORequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPORequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkPORequestDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPORequestDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPORequestDocNo.Properties.Caption = " ";
            this.ChkPORequestDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPORequestDocNo.Size = new System.Drawing.Size(21, 22);
            this.ChkPORequestDocNo.TabIndex = 14;
            this.ChkPORequestDocNo.ToolTip = "Remove filter";
            this.ChkPORequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPORequestDocNo.ToolTipTitle = "Run System";
            this.ChkPORequestDocNo.CheckedChanged += new System.EventHandler(this.ChkPORequestDocNo_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtTenderDocNo);
            this.panel4.Controls.Add(this.ChkTenderDocNo);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.ChkQtDocNo);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.TxtQtDocNo);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(516, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(341, 50);
            this.panel4.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(47, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 14);
            this.label3.TabIndex = 9;
            this.label3.Text = "TQR#";
            // 
            // TxtTQRDocNo
            // 
            this.TxtTQRDocNo.EnterMoveNextControl = true;
            this.TxtTQRDocNo.Location = new System.Drawing.Point(92, 5);
            this.TxtTQRDocNo.Name = "TxtTQRDocNo";
            this.TxtTQRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTQRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtTQRDocNo.Properties.MaxLength = 30;
            this.TxtTQRDocNo.Size = new System.Drawing.Size(233, 20);
            this.TxtTQRDocNo.TabIndex = 10;
            this.TxtTQRDocNo.Validated += new System.EventHandler(this.TxtTQRDocNo_Validated);
            // 
            // ChkTQRDocNo
            // 
            this.ChkTQRDocNo.Location = new System.Drawing.Point(329, 4);
            this.ChkTQRDocNo.Name = "ChkTQRDocNo";
            this.ChkTQRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTQRDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkTQRDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkTQRDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkTQRDocNo.Properties.Caption = " ";
            this.ChkTQRDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTQRDocNo.Size = new System.Drawing.Size(21, 22);
            this.ChkTQRDocNo.TabIndex = 11;
            this.ChkTQRDocNo.ToolTip = "Remove filter";
            this.ChkTQRDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkTQRDocNo.ToolTipTitle = "Run System";
            this.ChkTQRDocNo.CheckedChanged += new System.EventHandler(this.ChkTQRDocNo_CheckedChanged);
            // 
            // FrmTenderQuotationFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 473);
            this.Name = "FrmTenderQuotationFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTenderDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTenderDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkQtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPORequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPORequestDocNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTQRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTQRDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtTenderDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkTenderDocNo;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtPORequestDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkPORequestDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtQtDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkQtDocNo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtTQRDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkTQRDocNo;
    }
}