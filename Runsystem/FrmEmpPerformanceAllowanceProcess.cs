﻿#region Update
/* 
    21/02/2020 [HAR/IMS] : bisa multi cuti penting
    22/02/2020 [TKG/IMS] : ganti position dengan grade level group
 *  24/02/2020 [HAR/IMS] : perhitungan tetap
 *  25/02/2020 [HAR/IMS] : bisa ribuan row
 *  09/06/2020 [HAR/IMS] : BUG : nilai effective day masih ke double dengan holiday 
 *  09/06/2020 [HAR/IMS] : BUG : waktu show data tidak muncul detailnya 
 *  24/06/2020 [HAR/IMS] : BUG  employee yang resign masih muncul, validasi resign berdasarkan tanggal pemnbuatan dokumen
 *  17/06/2021 [HAR/IMS] : tambah parameter employement status untuk mengambil nilai(effective day & performance value) di bulan sebeumnya
 *  24/06/2021 [HAR/IMS] : BUG : nilai perhitungan dibedakan berdasarkan employment status tetap dan lepas
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpPerformanceAllowanceProcess : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = string.Empty;
        internal decimal WDay  = 0m;
        internal decimal ToTalJumlah = 0m;
        internal decimal TotalBrutto = 0m;
        internal decimal totalkonversi = 0m;
        internal string mEmploymentStatusTetap = string.Empty;
        internal string mEmploymentStatusSpesial = string.Empty;
        internal string mEmploymentStatusLepas = string.Empty;
        internal string mADCodeActingFunctional= string.Empty;
        internal string mADCodeFunctionalAllowance = string.Empty;
        internal string mADCodeFunctionalAllowanceEmpPerformances = string.Empty;
        internal string mGroupOfEmploymentStatusPerformance = string.Empty;

        internal FrmEmpPerformanceAllowanceProcessFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpPerformanceAllowanceProcess(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee Performance Allowance";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo }, true);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                Sl.SetLueOption(ref LueEmpStat, "EmploymentStatus");
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Employee"+Environment.NewLine+"Code",
                    "Employee Code Old",
                    "Employee Name", 
                    "Departement",
                    "Position", 
                    
                    //6-10
                    "Grade",
                    "CT",
                    "CP",
                    "CD",
                    "CH",

                    //11-15
                    "Time Wasted",
                    "Work Permit",
                    "Working Day",
                    "Salary",
                    "Functional Allowance",

                    //16-20
                    "Functional",
                    "Total Functional",
                    "Total",
                    "Performance Allowance's" + Environment.NewLine + "Budget",
                    "Performance Allowance's" + Environment.NewLine + "Value",

                    //21-23
                    "Brutto",
                    "Conversion",
                    "Performance Allowance",
                    "Received" + Environment.NewLine + "Performance Allowance"
                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 150, 150, 150, 150,  
                    
                    //6-10
                    100, 120, 120, 120, 120, 

                    //11-15
                    120, 150, 150, 150, 150, 

                    //16-20
                    150, 150, 150, 150, 150, 

                    //21-23
                    150,  150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       LueGrdLvlGrpCode, LueEmpStat, LueYr, LueMth, TxtEffectiveDay, DteDocDt, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueGrdLvlGrpCode, LueEmpStat, LueYr, LueMth,  MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 3, 4 });
                    Grd1.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueYr, LueMth, LueGrdLvlGrpCode, LueEmpStat,
                MeeRemark, 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
              TxtEffectiveDay
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpPerformanceAllowanceProcessFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueGrdLvlGrpCode(ref LueGrdLvlGrpCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 2)
                    {
                    }
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.IsGrdColSelected(new int[] { 3, 4 }, e.ColIndex))
            {
                //var Amt = Sm.GetGrdStr(Grd1, 0, e.ColIndex);
                //for (int Row = 1; Row < Grd1.Rows.Count - 1; Row++)
                //    Grd1.Cells[Row, e.ColIndex].Value = Amt;
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {

        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpPerformanceAllowance", "TblEmpPerformanceAllowanceHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpPerformanceAllowanceHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveEmpPerformanceAllowanceDtl(DocNo, Row));
                }
            }
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueGrdLvlGrpCode, "Grade level's group") ||
                IsGrdEmpty() ||
                //IsGrdExceedMaxRecords()||
                IPosYrMthExisted();
        }

        private bool IPosYrMthExisted()
        {
             var cm = new MySqlCommand()
                {
                    CommandText = "Select DocNo From TblEmpPerformanceAllowanceHdr Where Yr=@Yr And Mth=@Mth And GrdLvlGrpCode=@GrdLvlGrpCode And EmpStat=@EmpStat And CancelInd = 'N' Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@GrdLvlGrpCode", Sm.GetLue(LueGrdLvlGrpCode));
                Sm.CmParam<String>(ref cm, "@EmpStat", Sm.GetLue(LueEmpStat));

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Document for this Grade Level Group, year and month already existed.");
                    return true;
                }
            return false;
        }



        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data on grid.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpPerformanceAllowanceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpPerformanceAllowanceHdr(DocNo, DocDt, Yr, Mth, CancelInd, GrdLvlGrpCode, EmpStat, EffectiveDay, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, @Mth, 'N', @GrdLvlGrpCode, @EmpStat, @EffectiveDay, @Remark, @CreateBy, CurrentDateTime()); ");           

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@GrdLvlGrpCode", Sm.GetLue(LueGrdLvlGrpCode));
            Sm.CmParam<String>(ref cm, "@EmpStat", Sm.GetLue(LueEmpStat));
            Sm.CmParam<Decimal>(ref cm, "@EffectiveDay", Decimal.Parse(TxtEffectiveDay.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveEmpPerformanceAllowanceDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpPerformanceAllowanceDtl(DocNo, DNo, EmpCode, CT, CP, CD, CH, TimeWasted, WorkPermit, WorkDay, "+
                    "Salary, FunctionalAllowance, Functional, Total, PerformanceBudget, PerformanceValue, "+
                    "Brutto, Conversion, PerformanceAllowance, Receive, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @CT, @CP, @CD, @CH, @TimeWasted, @WorkPermit, @WorkDay, "+
                    "@Salary, @FunctionalAllowance, @Functional, @Total, @PerformanceBudget, @PerformanceValue, "+
                    "@Brutto, @Conversion, @PerformanceAllowance, @Receive, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 6));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@CT", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@CP", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@CD", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@CH", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@TimeWasted", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@WorkPermit", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@WorkDay", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Salary", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@FunctionalAllowance", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Functional", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Total", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@PerformanceBudget", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@PerformanceValue", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@Brutto", Sm.GetGrdDec(Grd1, Row, 21)); 
            Sm.CmParam<Decimal>(ref cm, "@Conversion", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@PerformanceAllowance", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@Receive", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

      
        #endregion

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpPerformanceAllowance());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                CancelledNonActive() ||
                IsDataCancelledAlready();
        }

        private bool CancelledNonActive()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Info, "You must cancel this document");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblEmpPerformanceAllowanceHdr Where CancelInd='Y'  And DocNo=@Param;",
                TxtDocNo.Text,
                "This document is already cancelled.");
        }

        private MySqlCommand EditEmpPerformanceAllowance()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpPerformanceAllowanceHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

   
        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpPerformanceAllowanceHdr(DocNo);
                ShowEmpPerformanceAllowanceDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowEmpPerformanceAllowanceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CancelInd, ");
            SQL.AppendLine("GrdLvlGrpCode, Yr, Mth, EmpStat, EffectiveDay, Remark ");
            SQL.AppendLine("From TblEmpPerformanceAllowanceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                       "DocNo", 
                       
                       //1-5
                       "DocDt", "CancelInd", "GrdLvlGrpCode", "Yr", "Mth",   
                       
                       //6-8
                       "EmpStat", "EffectiveDay","Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y" ? true : false;
                        Sl.SetLueGrdLvlGrpCode(ref LueGrdLvlGrpCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueEmpStat, Sm.DrStr(dr, c[6]));
                        TxtEffectiveDay.EditValue = Sm.FormatNum((Sm.DrStr(dr, c[7])), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
            );
        }

        private void ShowEmpPerformanceAllowanceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpCodeOld, B.EmpName, C.DeptName, D.PosName, E.GrdLvlName, ");
            SQL.AppendLine("A.CT, A.CP, A.CD, A.CH, A.TimeWasted, A.WorkPermit, A.WorkDay, A.Salary, ");
            SQL.AppendLine("A.FunctionalAllowance, A.Functional, A.Total, A.performanceBudget, A.PerformanceValue, ");
            SQL.AppendLine("A.Brutto, A.Conversion, A.PerformanceAllowance, A.Receive ");
            SQL.AppendLine("From TblEmpPerformanceAllowanceDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode  ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode  ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode = E.GrdLvlCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DocNo ;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "EmpCode", "EmpCodeOld", "EmpName", "DeptName", "PosName", 
                    //6-10
                    "GrdLvlName", "CT", "CP", "CD", "CH", 
                    //11-15
                    "TimeWasted", "WorkPermit", "WorkDay", "Salary", "FunctionalAllowance",  
                    //16-20
                    "Functional", "Total", "performanceBudget", "PerformanceValue", "Brutto",
                    //21-23
                     "Conversion", "PerformanceAllowance", "Receive" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 16);
                    Grd1.Cells[Row, 17].Value = Sm.DrDec(dr, 15) + Sm.DrDec(dr, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                }, false, false, true, false
            );
            //Grd1.Rows.RemoveAt(Grd1.Rows.Count-1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });

            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mEmploymentStatusTetap = Sm.GetParameter("EmploymentStatusTetap");
            mEmploymentStatusSpesial = Sm.GetParameter("EmploymentStatusSpesial");
            mEmploymentStatusLepas = Sm.GetParameter("EmploymentStatusLepas");
            mADCodeActingFunctional = Sm.GetParameter("ADCodeActingFunctional");
            mADCodeFunctionalAllowance = Sm.GetParameter("ADCodeFunctionalAllowance");
            mADCodeFunctionalAllowanceEmpPerformances = Sm.GetParameter("ADCodeFunctionalAllowanceEmpPerformances");
            mGroupOfEmploymentStatusPerformance = Sm.GetParameter("GroupOfEmploymentStatusPerformance");
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month") || Sm.IsLueEmpty(LueGrdLvlGrpCode, "Position") || Sm.IsLueEmpty(LueEmpStat, "Employee Status")) return;

            try
            {
                Sm.ClearGrd(Grd1, false);
            
                Cursor.Current = Cursors.WaitCursor;
                var l = new List<EmployeePerformance>();

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                    Process4();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }


        private void Process1(ref List<EmployeePerformance> l)
        {
            l.Clear();
             ToTalJumlah = 0m;
             TotalBrutto = 0m;
             totalkonversi = 0m;
            WDay = ShowDate();
            
            ShowDate();
            string EmpStat = Sm.GetValue("SELECT 1 Where EXISTS (SELECT 1 WHERE FIND_IN_SET('"+Sm.GetLue(LueEmpStat)+"', ('"+mGroupOfEmploymentStatusPerformance+"'))  )");

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, B.DeptName, C.PosName, D.IKP, ");
            SQL.AppendLine("A.GrdLvlCode As GradeLevel, ifnull(E.BasicSalary, 0) BasicSalary, ifnull(F.Total, 0) FunctionalAllowance, ");
            SQL.AppendLine("ifnull(G.Functional, 0) Functional, ifnull(H.Total, 0) Total  ");
            SQL.AppendLine("From tblEmpLoyee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblPosition C On A.PosCode = C.PosCode ");
            SQL.AppendLine("Left Join TblEmpPerformanceAllowance D on A.EmpCode = D.EmpCode And D.Yr=@Yr And D.Mth=@Mth ");
            SQL.AppendLine("Inner Join TblGradelevelHdr E on A.GrdLvlCode = E.GrdLvlCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X.EmpCode, SUM(X.TotalFixAllowance) Total  ");
            SQL.AppendLine("    From (  ");
            SQL.AppendLine("        Select A.EmpCode, SUM(B.Amt) As TotalFixAllowance   ");
            SQL.AppendLine("        From TblEmployee A  ");
            SQL.AppendLine("        Inner Join TblGradeLevelDtl B On A.grdlvlCode = B.GrdlvlCode    ");
            SQL.AppendLine("        Where FIND_IN_SET(B.AdCode, @ADCodeFunctionalAllowanceEmpPerformances)  ");
            SQL.AppendLine("        Group By A.EmpCode   ");
            SQL.AppendLine("        UNION ALL  ");
            SQL.AppendLine("        Select A.EmpCode, SUM(A.Amt) TotalFixAllowance  ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction A  ");
            SQL.AppendLine("        Where FIND_IN_SET(A.AdCode, @ADCodeFunctionalAllowanceEmpPerformances)  ");
            SQL.AppendLine("        And Concat(@Yr,@Mth,'01') between StartDt And EndDt   ");
            SQL.AppendLine("        Group By A.EmpCode  ");
            SQL.AppendLine("    )X Group By X.EmpCode  ");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, SUM(B.Amt) As Functional  ");
            SQL.AppendLine("    From TblEmployee A  ");
            SQL.AppendLine("    Inner Join TblGradeLevelDtl B On A.grdlvlCode = B.GrdlvlCode   "); 
            SQL.AppendLine("    Where FIND_IN_SET(B.AdCode, @ADCodeFunctionalAllowance)  ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(")G On A.EmpCode = G.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X.EmpCode, SUM(X.TotalFixAllowance) Total  ");
            SQL.AppendLine("    From (  ");
            SQL.AppendLine("        Select A.EmpCode, SUM(B.Amt) As TotalFixAllowance   ");
            SQL.AppendLine("        From TblEmployee A  ");
            SQL.AppendLine("        Inner Join TblGradeLevelDtl B On A.grdlvlCode = B.GrdlvlCode    ");
            SQL.AppendLine("        Where FIND_IN_SET(B.AdCode, @ADCodeFunctionalAllowanceEmpPerformances)  ");
            SQL.AppendLine("        Group By A.EmpCode   ");
            SQL.AppendLine("        UNION ALL  ");
            SQL.AppendLine("        Select A.EmpCode, SUM(A.Amt)   ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction A  ");
            SQL.AppendLine("        Where FIND_IN_SET(A.AdCode, @ADCodeFunctionalAllowanceEmpPerformances)  ");
            SQL.AppendLine("        And Concat(@Yr,@Mth,'01') between StartDt And EndDt   ");
            SQL.AppendLine("        Group By A.EmpCode  ");
            SQL.AppendLine("        UNION ALL  ");
            SQL.AppendLine("        Select A.EmpCode, SUM(B.Amt) As Functional  ");
            SQL.AppendLine("        From TblEmployee A  ");
            SQL.AppendLine("        Inner Join TblGradeLevelDtl B On A.grdlvlCode = B.GrdlvlCode   ");
            SQL.AppendLine("        Where FIND_IN_SET(B.AdCode, @ADCodeFunctionalAllowance)  ");
            SQL.AppendLine("        Group By A.EmpCode ");
            SQL.AppendLine("    )X Group By X.EmpCode  ");
            SQL.AppendLine(")H On A.EmpCode = H.EmpCode ");
            SQL.AppendLine("Inner Join TblGradeLevelHdr I On A.GrdLvlCode=I.GrdLvlCode And I.GrdLvlGrpCode Is Not Null And I.GrdLvlGrpCode=@GrdLvlGrpCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And ResignDt>Concat(@Yr,@Mth))) ");
            SQL.AppendLine("And A.GrdLvlCode Is Not Null And A.EmploymentStatus = @EmpStat ");

            Sm.CmParam<String>(ref cm, "@GrdLvlGrpCode", Sm.GetLue(LueGrdLvlGrpCode));
            if (EmpStat == "1")
            {
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueMth) == "01" ? Convert.ToString(decimal.Parse(Sm.GetLue(LueYr)) - 1) : Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth) == "01" ? "12" : Sm.Right(string.Concat("00", Convert.ToString(Decimal.Parse(Sm.GetLue(LueMth)) - 1)), 2));
            }
            Sm.CmParam<String>(ref cm, "@EmpStat", Sm.GetLue(LueEmpStat));
            Sm.CmParam<String>(ref cm, "@ADCodeActingFunctional", mADCodeActingFunctional);
            Sm.CmParam<String>(ref cm, "@ADCodeFunctionalAllowance", mADCodeFunctionalAllowance);
            Sm.CmParam<String>(ref cm, "@ADCodeFunctionalAllowanceEmpPerformances", mADCodeFunctionalAllowanceEmpPerformances);


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL + " Order By A.EmpName; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    //1-5
                    "EmpCodeOld", "EmpName", "DeptName", "PosName", "IKP", 
                    //6
                    "GradeLevel", "BasicSalary", "FunctionalAllowance", "Functional", "Total"

                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ToTalJumlah += (Sm.DrDec(dr, c[7]) + Sm.DrDec(dr, c[8]) + Sm.DrDec(dr, c[9]));
                        l.Add(new EmployeePerformance()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]).Trim(),
                            EmpCodeOld = Sm.DrStr(dr, c[1]),
                            EmpName = Sm.DrStr(dr, c[2]),
                            DeptName = Sm.DrStr(dr, c[3]),
                            PosName = Sm.DrStr(dr, c[4]),
                            GradeLevel = Sm.DrStr(dr, c[6]),
                            CT = 0m,
                            CP = 0m,
                            CD = 0m,
                            CH = 0m,
                            TimeWasted = 0m,
                            WorkPermit = 0m,
                            WorkingDay = 0m,
                            Salary = Sm.DrDec(dr, c[7]),
                            FunctionalAllowance = Sm.DrDec(dr, c[8]),
                            Functional = Sm.DrDec(dr, c[9]),
                            TotalFunctional = Sm.DrDec(dr, c[10]),
                            //total jika tetap maka akumulasi dari tun jabatan + functional, jika pkwt sum fix allowance
                            Total = Sm.DrDec(dr, c[7])+Sm.DrDec(dr, c[10]),
                            PerformanceAllowanceBudget = 0m,
                            PerformanceAllowanceValue = Sm.DrDec(dr, c[5]),
                            Brutto = 0m,
                            Conversion = 0m,
                            PerformanceAllowance = 0m,
                            ReceivedPerformanceAllowance = 0m,
                            NoWorkDayPerMth = 0m,
                            BruttoPerformanceAllowanceMultiplier = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<EmployeePerformance> l)
        {
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, ifnull(B.Ct, 0) CT, ifnull(C.Cp, 0) CP, ifnull(D.CD, 0) CD, ifnull(E.CH, 0) CH, ifnull(F.JAHIL, 0) JAHIL ");
            SQL.AppendLine("From tblEmployee A ");
            SQL.AppendLine("left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode, Count(B.DocNo) As CT From tblEmpleavehdr A ");
	        SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO ");
            SQL.AppendLine("    Where A.LeaveCode in  (Select parvalue From tblparameter Where parCode = 'AnnualLeaveCode') And Left(B.LeaveDt, 6) =@YrMth  ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(")B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select EmpCode, Count(B.DocNo) As CP From tblEmpleavehdr A ");
	        SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO ");
            SQL.AppendLine("    Where FIND_in_SET(A.LeaveCode, (Select parvalue From tblparameter Where parCode = 'LeaveCodeUrgentPerformances')) And Left(B.LeaveDt, 6) =@YrMth ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(")C On A.EmpCode = C.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select EmpCode, Count(B.DocNo) As CD From tblEmpleavehdr A ");
	        SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO ");
            SQL.AppendLine("    Where FIND_in_SET(A.LeaveCode, (Select parvalue From tblparameter Where parCode = 'LeaveCodeHospitalPerformances')) And Left(B.LeaveDt, 6) =@YrMth  ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(")D On A.EmpCode = D.EmpCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select EmpCode, Count(B.DocNo) As CH From tblEmpleavehdr A ");
	        SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO ");
            SQL.AppendLine("    Where FIND_in_SET(A.LeaveCode, (Select parvalue From tblparameter Where parCode = 'LeaveCodePregnants')) And Left(B.LeaveDt, 6) =@YrMth  ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(")E On A.EmpCode = E.EmpCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select EmpCode, Count(B.DocNo) As JAHIL From tblEmpleavehdr A ");
            SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO ");
            SQL.AppendLine("    Where A.LeaveCode = 'LEAVE099' And Left(B.LeaveDt, 6) =@YrMth  ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A' ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode ");
            SQL.AppendLine("Inner Join TblGradeLevelHdr G On A.GrdLvlCode=G.GrdLvlCode And G.GrdLvlGrpCode Is Not Null And G.GrdLvlGrpCode=@GrdLvlGrpCode ");
            SQL.AppendLine("Where A.GrdLvlCode Is Not Null And A.EmploymentStatus = @EmpStat ");
            
           
            string Yr = Sm.GetLue(LueMth) == "01" ? Convert.ToString(Decimal.Parse(Sm.GetLue(LueYr)) - 1) : Sm.GetLue(LueYr);
            string Mth = Sm.GetLue(LueMth) == "01" ? "12" : Sm.Right(string.Concat("00", Convert.ToString(Decimal.Parse(Sm.GetLue(LueMth))-1)) ,2);

            Sm.CmParam<String>(ref cm, "@EmpStat", Sm.GetLue(LueEmpStat));
            Sm.CmParam<String>(ref cm, "@GrdLvlGrpCode", Sm.GetLue(LueGrdLvlGrpCode));
            Sm.CmParam<String>(ref cm, "@YrMth", String.Concat(Yr, Mth));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL + " Order By A.EmpCode; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    //1-5
                    "CT", "CP", "CD", "CH", "JAHIL"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        for (int i = 0; i < l.Count; ++i)
                        {
                            if (l[i].EmpCode == Sm.DrStr(dr, c[0]))
                            {
                                l[i].CT = Sm.DrDec(dr, c[1]);
                                l[i].CP = Sm.DrDec(dr, c[2]);
                                l[i].CD = Sm.DrDec(dr, c[3]);
                                l[i].CH = Sm.DrDec(dr, c[4]);
                                l[i].TimeWasted = Sm.DrDec(dr, c[5]);
                                l[i].WorkPermit = (Sm.DrDec(dr, c[1]) + Sm.DrDec(dr, c[2]) + Sm.DrDec(dr, c[3]) + Sm.DrDec(dr, c[4]) + Sm.DrDec(dr, c[5]));
                                l[i].WorkingDay = WDay - l[i].WorkPermit;
                                l[i].PerformanceAllowanceBudget = ToTalJumlah;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<EmployeePerformance> l)
        {
            totalkonversi = 0m;
            TotalBrutto = 0m;
            int No = 0;
            for (int i = 0; i < l.Count; ++i)
            {
                Grd1.Rows.Add();
                Grd1.Cells[No, 0].Value = No + 1;
                Grd1.Cells[No, 1].Value = l[i].EmpCode;
                Grd1.Cells[No, 2].Value = l[i].EmpCodeOld;
                Grd1.Cells[No, 3].Value = l[i].EmpName;
                Grd1.Cells[No, 4].Value = l[i].DeptName;
                Grd1.Cells[No, 5].Value = l[i].PosName;
                Grd1.Cells[No, 6].Value = l[i].GradeLevel;
                Grd1.Cells[No, 7].Value = l[i].CT;
                Grd1.Cells[No, 8].Value = l[i].CP;
                Grd1.Cells[No, 9].Value = l[i].CD;
                Grd1.Cells[No, 10].Value = l[i].CH;
                Grd1.Cells[No, 11].Value = l[i].TimeWasted;
                Grd1.Cells[No, 12].Value = l[i].WorkPermit;
                Grd1.Cells[No, 13].Value = l[i].WorkingDay;
                Grd1.Cells[No, 14].Value = l[i].Salary;
                Grd1.Cells[No, 15].Value = l[i].FunctionalAllowance;
                Grd1.Cells[No, 16].Value = l[i].Functional;
                Grd1.Cells[No, 17].Value = l[i].TotalFunctional;
                Grd1.Cells[No, 18].Value = l[i].Total;
                Grd1.Cells[No, 19].Value = l[i].PerformanceAllowanceBudget;
                Grd1.Cells[No, 20].Value = l[i].PerformanceAllowanceValue;

                //brutto
                if (Sm.GetLue(LueEmpStat) == mEmploymentStatusTetap && l[i].PerformanceAllowanceBudget > 0)
                    Grd1.Cells[No, 21].Value = Sm.FormatNum((l[i].Total / l[i].PerformanceAllowanceBudget) * (l[i].PerformanceAllowanceBudget / 12) * 1.5m, 0);
                if (Sm.GetLue(LueEmpStat) == mEmploymentStatusLepas)
                    Grd1.Cells[No, 21].Value = Sm.FormatNum((l[i].Total / 12), 0);
                else
                    Grd1.Cells[No, 21].Value = 0m;

                //conversion
                if (Sm.GetLue(LueEmpStat) == mEmploymentStatusTetap)
                    Grd1.Cells[No, 22].Value = Sm.FormatNum((l[i].Total * l[i].PerformanceAllowanceValue) / 100, 0);
                if (Sm.GetLue(LueEmpStat) == mEmploymentStatusLepas)
                    Grd1.Cells[No, 22].Value = Sm.FormatNum(l[i].PerformanceAllowanceValue * (l[i].Total / 12), 0);
            
               
                No += 1;
                totalkonversi += (l[i].Total * l[i].PerformanceAllowanceValue) / 100;
                if (l[i].PerformanceAllowanceBudget > 0)
                TotalBrutto += (l[i].Total / l[i].PerformanceAllowanceBudget) * (l[i].PerformanceAllowanceBudget / 12) * 1.5m;
            }

            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
        }

        private void Process4()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetLue(LueEmpStat) == mEmploymentStatusTetap && totalkonversi > 0)
                    Grd1.Cells[Row, 23].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row, 22) / totalkonversi) * TotalBrutto, 0);
                else if (Sm.GetLue(LueEmpStat) == mEmploymentStatusLepas)
                    Grd1.Cells[Row, 23].Value = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 20) * (Sm.GetGrdDec(Grd1, Row, 18) / 12), 0);

                if (Decimal.Parse(TxtEffectiveDay.Text) > 0)
                    Grd1.Cells[Row, 24].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row, 23) / Decimal.Parse(TxtEffectiveDay.Text)) * Sm.GetGrdDec(Grd1, Row,13), 0);
                //else if (Sm.GetLue(LueEmpStat) == mEmploymentStatusLepas)
                //    Grd1.Cells[Row, 24].Value = ((l[i].PerformanceAllowanceValue * (l[i].Total / 12)) * l[i].WorkingDay) / Decimal.Parse(TxtEffectiveDay.Text) > 0 ? Decimal.Parse(TxtEffectiveDay.Text) : 0m; ;


                //if(totalkonversi>0)
                //    Grd1.Cells[Row, 23].Value = (Sm.GetGrdDec(Grd1, Row, 22) / totalkonversi) * TotalBrutto;
                //if(WDay>0)
                //    Grd1.Cells[Row, 24].Value = (Sm.GetGrdDec(Grd1, Row, 22) / WDay) * Sm.GetGrdDec(Grd1, Row, 13);
            }

            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
  
            Grd1.EndUpdate();
        }

        private decimal ShowDate()
        {
            var lHoliday = new List<DateHoliday>();
            ProcessHoliday(ref lHoliday);

            string Yr = string.Empty;
            string Mth = String.Empty;
            string EmpStat = Sm.GetValue("SELECT 1 Where EXISTS (SELECT 1 WHERE FIND_IN_SET('" + Sm.GetLue(LueEmpStat) + "', ('" + mGroupOfEmploymentStatusPerformance + "'))  )");

            if (EmpStat == "1")
            {
                Yr = Sm.GetLue(LueYr);
                Mth = Sm.GetLue(LueMth);
            }
            else
            {
                Yr = Sm.GetLue(LueMth) == "01" ? Convert.ToString(decimal.Parse(Sm.GetLue(LueYr)) - 1) : Sm.GetLue(LueYr);
                Mth = Sm.GetLue(LueMth) == "01" ? "12" : Sm.Right(string.Concat("00", Convert.ToString(Decimal.Parse(Sm.GetLue(LueMth)) - 1)), 2);
            }

            var DtMin = String.Concat(Yr, Mth, "01"); 
            var DtMax = String.Concat(Yr, Mth, 
                Sm.Right(String.Concat("00", DateTime.DaysInMonth(Convert.ToInt32(Yr), Convert.ToInt32(Mth))), 2)); 
            decimal DayWeek = 0m;
            decimal EffectiveDay = 0m;
   

            if (DtMin.Length >= 8 &&
                DtMax.Length >= 8 &&
                decimal.Parse(DtMin) <= decimal.Parse(DtMax))
            {
                DateTime Dt1 = new DateTime(
                       Int32.Parse(DtMin.Substring(0, 4)),
                       Int32.Parse(DtMin.Substring(4, 2)),
                       Int32.Parse(DtMin.Substring(6, 2)),
                       0, 0, 0);

                DateTime Dt2 = new DateTime(
                    Int32.Parse(DtMax.Substring(0, 4)),
                    Int32.Parse(DtMax.Substring(4, 2)),
                    Int32.Parse(DtMax.Substring(6, 2)),
                    0, 0, 0);

                DateTime TempDt = Dt1;

                var TotalDays = (Dt2 - Dt1).Days + 1;
                EffectiveDay = TotalDays;

                var s = new List<string>();

                s.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );

                for (int i = 1; i < TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    s.Add(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                    );
                }

                s.ForEach(i =>
                {
                    if (lHoliday.Count() >= 1)
                    {
                        lHoliday.ForEach(x =>
                            {
                                if (TempDt.ToString("yyyyMMdd") == x.HolDt )
                                {
                                    if ((int)TempDt.DayOfWeek != 0 && (int)TempDt.DayOfWeek != 6)
                                    {
                                        DayWeek += 1;
                                    }
                                }
                            });
                    }

                    if ((int)TempDt.DayOfWeek == 0 || (int)TempDt.DayOfWeek == 6)
                    {
                        DayWeek += 1;
                    }

                    TempDt = TempDt.AddDays(1);
                });

            }

            EffectiveDay = EffectiveDay - DayWeek;
            TxtEffectiveDay.Text = Sm.FormatNum(EffectiveDay, 0); ;
            return EffectiveDay;
        }

        private void ProcessHoliday(ref List<DateHoliday> l)
        {
            l.Clear();
            
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string Yr = string.Empty;
            string Mth = String.Empty;
            string EmpStat = Sm.GetValue("SELECT 1 Where EXISTS (SELECT 1 WHERE FIND_IN_SET('" + Sm.GetLue(LueEmpStat) + "', ('" + mGroupOfEmploymentStatusPerformance + "'))  )");

            if (EmpStat == "1")
            {
                Yr = Sm.GetLue(LueYr);
                Mth = Sm.GetLue(LueMth);
            }
            else
            {
                Yr = Sm.GetLue(LueMth) == "01" ? Convert.ToString(decimal.Parse(Sm.GetLue(LueYr)) - 1) : Sm.GetLue(LueYr);
                Mth = Sm.GetLue(LueMth) == "01" ? "12" : Sm.Right(string.Concat("00", Convert.ToString(Decimal.Parse(Sm.GetLue(LueMth)) - 1)), 2);
            }

            var DtMin = String.Concat(Yr, Mth, "01");
            var DtMax = String.Concat(Yr, Mth,
                Sm.Right(String.Concat("00", DateTime.DaysInMonth(Convert.ToInt32(Yr), Convert.ToInt32(Mth))), 2));


            int Yr1 = int.Parse(Sm.Left(DtMin, 4));
            int Yr2 = int.Parse(Sm.Left(DtMax, 4));

            SQL.AppendLine("Select Distinct HolDt From ( ");
            SQL.AppendLine("Select HolDt From TblHoliday ");
            SQL.AppendLine("Where HolDt Between @StartDt And @EndDt ");
            for (int i = Yr1; i <= Yr2; i++)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Concat(@Yr" + i.ToString() + ", Right(HolDt, 4)) As HolDt From TblHoliday ");
                SQL.AppendLine("Where RoutineInd='Y' ");
                SQL.AppendLine("And HolDt<=@EndDt ");
                SQL.AppendLine("And Concat(@Yr" + i.ToString() + ", Right(HolDt, 4)) Between @StartDt And @EndDt  ");

                Sm.CmParam<string>(ref cm, "@Yr" + i.ToString(), i.ToString());
            }
            SQL.AppendLine(") T Order By HolDt;");
            Sm.CmParamDt(ref cm, "@StartDt", DtMin);
            Sm.CmParamDt(ref cm, "@EndDt", DtMax);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "HolDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DateHoliday()
                        {
                            HolDt = Sm.DrStr(dr, c[0]) 
                        });
                        //DayWeek += 1;
                    }
                }
                dr.Close();
            }

            //return totalWday = TotalDays - DayWeek;
        }

        #endregion

        #region Event

        private void LueGrdLvlGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGrdLvlGrpCode, new Sm.RefreshLue2(Sl.SetLueGrdLvlGrpCode), string.Empty);
        }

        private void LueEmpStat_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEmpStat, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
        }

        #endregion

        #region Class

        private class DateHoliday
        {
            public string HolDt { get; set; }
        }


        private class EmployeePerformance
        {
            public string EmpCodeOld { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string GradeLevel { get; set; }
            public decimal CT { get; set; }
            public decimal CP { get; set; }
            public decimal CD { get; set; }
            public decimal CH { get; set; }
            public decimal TimeWasted { get; set; }
            public decimal WorkPermit { get; set; }
            public decimal WorkingDay { get; set; }
            public decimal Salary { get; set; }
            public decimal FunctionalAllowance { get; set; }
            public decimal Functional { get; set; }
            public decimal TotalFunctional { get; set; }
            public decimal Total { get; set; }
            public decimal PerformanceAllowanceBudget { get; set; }
            public decimal PerformanceAllowanceValue { get; set; }
            public decimal Brutto { get; set; }
            public decimal Conversion { get; set; }
            public decimal PerformanceAllowance { get; set; }
            public decimal ReceivedPerformanceAllowance { get; set; }
            public decimal NoWorkDayPerMth { get; set; }
            public decimal BruttoPerformanceAllowanceMultiplier { get; set; }
        }


        #endregion
    }

 
}
