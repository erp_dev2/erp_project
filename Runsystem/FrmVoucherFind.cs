﻿#region Update
/* 
    09/05/2017 [WED] Tambah informasi Vendor untuk tipe APDP
    21/05/2017 [TKG] bug fixing filter vendor untuk downpayment
    13/07/2017 [TKG] entity diambil dari data voucher
    03/10/2018 [DITA] Update Filter berdasarkan Departmen Group\
    19/12/2019 [HAR/AWG] BUG saat refresh di find
    09/03/2020 [IBL/KBN] Tambah Filter #Local di FIND Voucher
    17/03/2020 [VIN/KBN] fambah filter bank account type
    13/04/2020 [VIN/SRN] Tambah Filter Department
    10/12/2020 [TKG/PHT] tambah budget category
    30/03/2021 [IBL/SRN] tambah kolom Debit/Credit
    12/04/2021 [TKG/SRN] tambah filter account type
    12/04/2021 [TKG/SRN] 
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
    20/06/2021 [TKG/PHT]
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
    13/07/2021 [WED/IMS] tambah informasi kolom Cash Type dan Debit/Credit 2
    24/11/2021 [YOG/AMKA] Membuat menu voucher ketika find terfilter berdasarkan departement group
    27/07/2022 [TYO/SIER] Group filter doc Type berdasarkan parameter IsGroupFilterByType
    05/08/2022 [TYO/SIER] Penyesuaian filter by group
    09/08/2022 [BRI/PHT] Penyesuaian filter by group bank acc berdasarkan param IsVoucherSwitchingNotFilterByDept
    23/03/2023 [MAU/PHT] penyesuaian filter Type berdasarkan group user
    27/03/2023 [RDA/MNET] tambah kolom remark (source : voucher header)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmVoucher mFrmParent;
        private string mSQL = string.Empty, mMInd = "N";

        #endregion

        #region Constructor

        public FrmVoucherFind(FrmVoucher FrmParent, string MInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMInd = MInd;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueVoucherDocType(ref LueDocType, mFrmParent.mIsGroupFilterByType ? "Y" : "N");
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsVoucherFilteredByDept ? "Y" : "N");
                Sl.SetLueAcType(ref LueAcType);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.LocalDocNo, B.OptDesc As DocTypeDesc, F.BankAcTp, F.BankAcNm, ");
            SQL.AppendLine("A.VoucherRequestDocNo, A.Amt, ");
            SQL.AppendLine("Case A.DocType When '04' Then J.VdName Else D.VdName End As VdName, ");
            SQL.AppendLine("E.VdInvNo, L.EntName, ");
            SQL.AppendLine("Case A.AcType ");
            SQL.AppendLine("    When 'D' Then 'Debit' ");
            SQL.AppendLine("    When 'C' Then 'Credit' ");
            SQL.AppendLine("    Else '' ");
            SQL.AppendLine("End As AcTypeDesc, ");
            SQL.AppendLine("Case A.AcType2 ");
            SQL.AppendLine("    When 'D' Then 'Debit' ");
            SQL.AppendLine("    When 'C' Then 'Credit' ");
            SQL.AppendLine("    Else '' ");
            SQL.AppendLine("End As AcTypeDesc2, ");
            SQL.AppendLine(" (SELECT UCASE(UserName) FROM TblUser WHERE UserCode=A.PIC) AS PIC, ");
            SQL.AppendLine("  Concat( ");
            SQL.AppendLine("    Case When G.BankName Is Not Null Then Concat(G.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("    Case When F.BankAcNo Is Not Null  ");
            SQL.AppendLine("    Then Concat(F.BankAcNo, ' [', IfNull(F.BankAcNm, ''), ']') ");
            SQL.AppendLine("    Else IfNull(F.BankAcNm, '') End ");
            SQL.AppendLine(") As BankAcDesc, ");
            SQL.AppendLine("  Concat( ");
            SQL.AppendLine("    Case When S.BankName Is Not Null Then Concat(S.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("    Case When R.BankAcNo Is Not Null  ");
            SQL.AppendLine("    Then Concat(R.BankAcNo, ' [', IfNull(R.BankAcNm, ''), ']') ");
            SQL.AppendLine("    Else IfNull(R.BankAcNm, '') End ");
            SQL.AppendLine(") As BankAcDesc2, K.DeptCode, M.DeptName, N.BCName, ");
            if (mFrmParent.mIsVoucherUseCashType)
                SQL.AppendLine("T.CashTypeName, ");
            else
                SQL.AppendLine("NULL as CashTypeName, ");
            SQL.AppendLine("A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode and B.OptCat='VoucherDocType' ");
            SQL.AppendLine("Left Join TblOutgoingPaymentHdr C On A.VoucherRequestDocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("Left Join TblVendor D On C.VdCode=D.VdCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T3.VdInvNo Separator ', ') As VdInvNo ");
            SQL.AppendLine("    From TblOutgoingPaymentHdr T1");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.VdInvNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblVoucherHdr T4 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T4.DocNo ");
            SQL.AppendLine("        And T4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") E On C.DocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblBankAccount F On A.BankAcCode=F.BankAcCode ");
            SQL.AppendLine("Left Join TblBank G On F.BankCode=G.BankCode ");
            SQL.AppendLine("Left Join TblAPDownpayment H On A.VoucherRequestDocNo=H.VoucherRequestDocNo ");
            SQL.AppendLine("Left Join TblPOHdr I On H.PODocNo=I.DocNo ");
            SQL.AppendLine("Left Join (Select VdCode As VdCode2, VdName From TblVendor) J On I.VdCode=J.VdCode2 ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr K On A.VoucherRequestDocNo=K.DocNo ");
            SQL.AppendLine("Left Join TblEntity L ON A.EntCode=L.EntCode ");
            SQL.AppendLine("Left Join TblDepartment M On K.DeptCode=M.DeptCode ");
            if (mFrmParent.mIsVoucherBudgetCategoryEnabled)
            {
                SQL.AppendLine("Inner Join TblBudgetCategory N On A.BCCode=N.BCCode ");
                SQL.AppendLine("Inner Join TblCostCenter O On N.CCCode=O.CCCode ");
                SQL.AppendLine("Inner Join TblGroupProfitCenter P On O.ProfitCenterCode=P.ProfitCenterCode ");
                SQL.AppendLine("Inner Join TblUser Q On P.GrpCode=Q.GrpCode And Q.UserCode=@UserCode ");
            }
            else
                SQL.AppendLine("Left Join TblBudgetCategory N On A.BCCode=N.BCCode ");

            SQL.AppendLine("Left Join TblBankAccount R On A.BankAcCode2 = R.BankAcCode ");
            SQL.AppendLine("Left Join TblBank S On R.BankCode = S.BankCode ");

            if (mFrmParent.mIsVoucherUseCashType)
                SQL.AppendLine("Left Join TblCashType T On A.CashTypeCode = T.CashTypeCode ");

            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mFrmParent.mIsVoucherFilteredByDept && !mFrmParent.mIsVoucherSwitchingNotFilterByDept)
            {
                SQL.AppendLine("And (K.DeptCode Is Null ");
                SQL.AppendLine("    Or (K.DeptCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=K.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");

            }
            if (mFrmParent.mIsVoucherSwitchingNotFilterByDept)
            {
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    (A.BankAcCode Is Null ");
                SQL.AppendLine("        Or(A.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("        Where BankAcCode = A.BankAcCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");
                SQL.AppendLine("    Or ");
                SQL.AppendLine("    (A.BankAcCode2 Is Null ");
                SQL.AppendLine("        Or (A.BankAcCode2 Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("        Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");
                SQL.AppendLine("    And ");
                SQL.AppendLine("    Find_In_Set(A.DocType,'16,73') ");
                SQL.AppendLine("    ) ");

                SQL.AppendLine("And (A.BankAcCode Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) Or Find_In_Set(A.DocType,'16,73') ");
                SQL.AppendLine("    ) ");

                SQL.AppendLine("And (A.BankAcCode2 Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode2 Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) Or Find_In_Set(A.DocType,'16,73') ");
                SQL.AppendLine("    ) ");

                if (mFrmParent.mIsGroupFilterByType)
                {
                    SQL.AppendLine("And EXISTS");
                    SQL.AppendLine("(  ");
                    SQL.AppendLine("	SELECT 1   ");
                    SQL.AppendLine("	FROM TblGroupType  ");
                    SQL.AppendLine("	WHERE TypeCode =A.DocType  ");
                    SQL.AppendLine("	AND GrpCode IN   ");
                    SQL.AppendLine("	(  ");
                    SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                    SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                    SQL.AppendLine("	)  ");
                    SQL.AppendLine(")  ");
                }
            }
            else if (mFrmParent.mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (A.BankAcCode Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");

                SQL.AppendLine("And (A.BankAcCode2 Is Null ");
                SQL.AppendLine("    Or (A.BankAcCode2 Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");

                if(mFrmParent.mIsGroupFilterByType)
                {
                    SQL.AppendLine("And EXISTS");
                    SQL.AppendLine("(  ");
                    SQL.AppendLine("	SELECT 1   ");
                    SQL.AppendLine("	FROM TblGroupType  ");
                    SQL.AppendLine("	WHERE TypeCode =A.DocType  ");
                    SQL.AppendLine("	AND GrpCode IN   ");
                    SQL.AppendLine("	(  ");
                    SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                    SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                    SQL.AppendLine("	)  ");
                    SQL.AppendLine(")  ");
                }
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel", 
                        "Local"+Environment.NewLine+"Document#", 
                        "Type",
                        
                        //6-10
                        "Voucher Request#",
                        "Person In Charge",
                        "Entity",
                        "Vendor",
                        "Vendor's"+Environment.NewLine+"Invoice#",
                        
                        //11-15
                        "Debit/Credit",
                        "Debit/Credit To",
                        "Bank Account",
                        "Bank's Account Type",
                        "Debit/Credit"+Environment.NewLine+"2",

                        //16-20
                        "Debit/Credit To"+Environment.NewLine+"2",
                        "Cash Type",
                        "Amount",
                        "Department",
                        "Remark", // x >= 20 naik 1 angka
                        "Budget Category",

                        //21-25
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date", 

                        //26
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 50, 130, 150, 
                        
                        //6-10
                        130, 200, 200, 200, 130, 

                        //11-15
                        80, 250, 200, 200, 80, 

                        //16-20
                        250, 250, 130, 200, 200, 200, 

                        //21-25
                        130, 130, 130, 130, 130, 
                        
                        //26
                        130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 23, 26 });
            Sm.GrdFormatTime(Grd1, new int[] { 24, 27 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 22, 23, 24, 25, 26, 27 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 21 }, mFrmParent.mIsVoucherBudgetCategoryEnabled);
            Sm.GrdColInvisible(Grd1, new int[] { 17 }, mFrmParent.mIsVoucherUseCashType);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 22, 23, 24, 25, 26, 27 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = mFrmParent.mIsUseMInd?" And A.MInd=@MInd ":" ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MInd", mMInd);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                if (mFrmParent.mIsVoucherFilteredByDept)
                    Sm.CmParam<String>(ref cm, "@VoucherDocTypeManual", mFrmParent.mVoucherDocTypeManual);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVoucherRequestDocNo.Text, "A.VoucherRequestDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "A.DocType", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), new string[] { "D.VdCode", "J.VdCode2" });
                Sm.FilterStr(ref Filter, ref cm, TxtVdInvNo.Text, "E.VdInvNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "K.DeptCode", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcType), "A.AcType", true);
                Sm.FilterStr(ref Filter, ref cm, TxtBankAcTp.Text, "F.BankAcTp", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "LocalDocNo", "DocTypeDesc", "VoucherRequestDocNo", 

                        //6-10
                        "PIC", "EntName", "VdName", "VdInvNo", "AcTypeDesc",
                        
                        //11-15
                        "BankAcDesc", "BankAcNm", "BankAcTp", "AcTypeDesc2", "BankAcDesc2", 
                        
                        //16-20
                        "CashTypeName", "Amt", "DeptName", "Remark", "BCName", 

                        //21-24
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 24);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
        
        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue2(Sl.SetLueVoucherDocType), mFrmParent.mIsGroupFilterByType ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsVoucherFilteredByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtVoucherRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVoucherRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher request document#");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtVdInvNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdInvNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor's invoice#");
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAcType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Account's Type");
        }

        private void TxtBankAcTp_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBankAcTp_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cash/Bank Account's type");
        }

        #endregion

        #endregion
    }
}
