﻿#region Update
/*
    05/11/2017 [TKG] Aplikasi baru
    20/11/2017 [TKG] tambah bbrp fasilitas untuk copy data (SFC, Formula)
    08/12/2017 [TKG] perhitungan otomatis produksi dan efisiensi data
    06/06/2018 [TKG] running machine menjadi 2 decimal point
    06/06/2018 [TKG] validasi apabila ada production date dan work center yg pernah diproses sebelumnya.
    20/06/2018 [HAR] validasi saat kalkulasi jika ada pembagi 0.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSpinningProduction2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmSpinningProduction2Find FrmFind;
        internal string mProdFormula = string.Empty;

        #endregion

        #region Constructor

        public FrmSpinningProduction2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 16;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",   

                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Quantity",
                        "Total" + Environment.NewLine + "Running" + Environment.NewLine + "Machine",

                        //Ring Spinning
                        "RPM",
                        
                        //6-10
                        "TPI",
                        "NE",
                        "Avg Spindle",
                        "Kg/Hr",

                        //Winder
                        "Speed",
                        
                        //11-15
                        "NE",
                        "Del MC",
                        "Del Run",
                        "100% Kg/Hr",
                        "100% Del"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        80, 200, 120, 100, 120, 
                        
                        //6-10
                        100, 100, 120, 150, 120,  
                        
                        //11-15
                        120, 120, 120, 120, 120
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15 }, 2);
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 6);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 9, 14, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Code",

                        //1-2
                        "Efficiency Variable",
                        "Value"
                    },
                     new int[] 
                    {
                        0,
                        200, 150
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 2 }, 3);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, DteProdDt, LueWCCode, 
                        MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13 });
                    panel4.Visible = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteProdDt, LueWCCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13 });
                    panel4.Visible = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mProdFormula = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, MeeCancelReason, DteProdDt, LueWCCode, 
                MeeRemark 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtRunningMachine }, 2);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, false);

            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSpinningProduction2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteProdDt);
                SetLueWCCode(ref LueWCCode, string.Empty);
                ChkAutoSaveFormula.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSpinningProduction2Hdr(DocNo);
                ShowSpinningProduction2Dtl(DocNo);
                ShowSpinningProduction2Dtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowSpinningProduction2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.ProdDt, ");
            SQL.AppendLine("A.WCCode, A.RunningMachine, A.Remark, B.ProdFormula ");
            SQL.AppendLine("From TblSpinningProduction2Hdr A ");
            SQL.AppendLine("Left Join TblWCSpinning B On A.WCCode=B.WCCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                   {
                        //0
                        "DocNo", 
                      
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "ProdDt", "WCCode", 
                        
                        //6-8
                        "RunningMachine", "Remark", "ProdFormula"
                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                     Sm.SetDte(DteProdDt, Sm.DrStr(dr, c[4]));
                     SetLueWCCode(ref LueWCCode, Sm.DrStr(dr, c[5]));
                     TxtRunningMachine.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 2);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                     mProdFormula = Sm.DrStr(dr, c[8]);
                     if (Sm.CompareStr(mProdFormula, "02"))
                         Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9 }, true);
                     if (Sm.CompareStr(mProdFormula, "03"))
                         Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, true);
                 }, true
             );
        }

        private void ShowSpinningProduction2Dtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.*, B.ItName " +
                    "From TblSpinningProduction2Dtl A " +
                    "Left Join TblItemSpinning B On A.ItCode=B.ItCode " +
                    "Where A.DocNo=@DocNo Order By A.DNo;",

                    new string[] 
                       { 
                           //0
                           "ItCode",

                           //1-5
                           "ItName", "Qty", "RunningMachine", "RSRPM", "RSTPI", 
                           
                           //6-10
                           "RSNE", "RSAvgSPL", "RSMFR", "WSpeed", "WNE",  
                           
                           //11-14
                           "WDelMC", "WDelRun", "WMFR", "WDel" 
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowSpinningProduction2Dtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select A.ProdEfficiencyVar, B.OptDesc, A.Value " +
                    "From TblSpinningProduction2Dtl2 A " +
                    "Left Join TblOption B On B.OptCat='ProdEfficiencyVar' And A.ProdEfficiencyVar=B.OptCode " +
                    "Where A.DocNo=@DocNo Order By A.DNo;",

                    new string[] 
                       { 
                           //0
                           "ProdEfficiencyVar",

                           //1-2
                           "OptDesc", "Value"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SpinningProduction2", "TblSpinningProduction2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSpinningProduction2Hdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveSpinningProduction2Dtl(DocNo, r));

            for (int r = 0; r < Grd2.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0) cml.Add(SaveSpinningProduction2Dtl2(DocNo, r));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteProdDt, "Production date") ||
                Sm.IsLueEmpty(LueWCCode, "Work center") ||
                IsDataAlreadyExisted() ||
                IsGrdEmpty(Grd1) ||
                IsGrdValueNotValid();
        }

        private bool IsDataAlreadyExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblSpinningProduction2Hdr " +
                "Where CancelInd='N' And ProdDt=@Param1 And WCCode=@Param2 Limit 1;",
                Sm.GetDte(DteProdDt), Sm.GetLue(LueWCCode), string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Data with the same production date and work center already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty(iGrid Grd)
        {
            if (Grd.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 3, true, "Quantity should not be 0."))
                        return true;
                }
            }

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, r, 2, true, "Value should not be 0."))
                        return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveSpinningProduction2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSpinningProduction2Hdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, ProdDt, WCCode, RunningMachine, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @ProdDt, @WCCode, @RunningMachine, @Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ProdDt", Sm.GetDte(DteProdDt));
            Sm.CmParam<String>(ref cm, "@WCCode", Sm.GetLue(LueWCCode));
            Sm.CmParam<Decimal>(ref cm, "@RunningMachine", decimal.Parse(TxtRunningMachine.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSpinningProduction2Dtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSpinningProduction2Dtl ");
            SQL.AppendLine("(DocNo, DNo, ItCode, Qty, RunningMachine, ");
            SQL.AppendLine("RSRPM, RSTPI, RSNE, RSAvgSPL, RSMFR, ");
            SQL.AppendLine("WSpeed, WNE, WDelMC, WDelRun, WMFR, WDel, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @ItCode, @Qty, @RunningMachine, ");
            SQL.AppendLine("@RSRPM, @RSTPI, @RSNE, @RSAvgSPL, @RSMFR, ");
            SQL.AppendLine("@WSpeed, @WNE, @WDelMC, @WDelRun, @WMFR, @WDel, ");
            SQL.AppendLine("@UserCode, CurrentDateTime());");

            if (ChkAutoSaveFormula.Checked)
            {
                if (Sm.CompareStr(mProdFormula, "02"))
                {
                    SQL.AppendLine("Update TblFormulaRingSpinningDtl T1 ");
                    SQL.AppendLine("Inner Join TblFormulaRingSpinningHdr T2 On T1.FRSCode=T2.FRSCode And T2.ActInd='Y' ");
                    SQL.AppendLine("Set ");
                    SQL.AppendLine("    T1.RPM=@RSRPM, T1.TPI=@RSTPI, T1.NE=@RSNE, T1.AvgSPL=@RSAvgSPL, T1.MFR=@RSMFR, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where T1.ItCode=@ItCode; ");
                    
                    SQL.AppendLine("Insert Into TblFormulaRingSpinningDtl ");
                    SQL.AppendLine("(FRSCode, ItCode, RPM, TPI, NE, AvgSPL, MFR, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select T.FRSCode, @ItCode, @RSRPM, @RSTPI, @RSNE, @RSAvgSPL, @RSMFR, @UserCode, CurrentDateTime() ");
                    SQL.AppendLine("From TblFormulaRingSpinningHdr T ");
                    SQL.AppendLine("Where T.ActInd='Y' ");
                    SQL.AppendLine("And Not Exists( ");
                    SQL.AppendLine("    Select 1 ");
                    SQL.AppendLine("    From TblFormulaRingSpinningDtl ");
                    SQL.AppendLine("    Where FRSCode=T.FRSCode ");
                    SQL.AppendLine("    And ItCode=@ItCode ");
                    SQL.AppendLine("    ); ");                    
                }

                if (Sm.CompareStr(mProdFormula, "03"))
                {
                    SQL.AppendLine("Update TblFormulaWinderDtl T1 ");
                    SQL.AppendLine("Inner Join TblFormulaWinderHdr T2 On T1.FWCode=T2.FWCode And T2.ActInd='Y' ");
                    SQL.AppendLine("Set ");
                    SQL.AppendLine("    T1.Speed=@WSpeed, T1.NE=@WNE, T1.DelMC=@WDelMC, T1.DelRun=@WDelRun, T1.MFR=@WMFR, T1.Del=@WDel, T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where T1.ItCode=@ItCode; ");

                    SQL.AppendLine("Insert Into TblFormulaWinderDtl ");
                    SQL.AppendLine("(FWCode, ItCode, Speed, NE, DelMC, DelRun, MFR, Del, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select T.FWCode, @ItCode, @WSpeed, @WNE, @WDelMC, @WDelRun, @WMFR, @WDel, @UserCode, CurrentDateTime() ");
                    SQL.AppendLine("From TblFormulaWinderHdr T ");
                    SQL.AppendLine("Where T.ActInd='Y' ");
                    SQL.AppendLine("And Not Exists( ");
                    SQL.AppendLine("    Select 1 ");
                    SQL.AppendLine("    From TblFormulaWinderDtl ");
                    SQL.AppendLine("    Where FWCode=T.FWCode ");
                    SQL.AppendLine("    And ItCode=@ItCode ");
                    SQL.AppendLine("    ); ");     
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 3));
            Sm.CmParam<Decimal>(ref cm, "@RunningMachine", Sm.GetGrdDec(Grd1, r, 4));

            //Ring Spinning

            Sm.CmParam<Decimal>(ref cm, "@RSRPM", Sm.GetGrdDec(Grd1, r, 5));
            Sm.CmParam<Decimal>(ref cm, "@RSTPI", Sm.GetGrdDec(Grd1, r, 6));
            Sm.CmParam<Decimal>(ref cm, "@RSNE", Sm.GetGrdDec(Grd1, r, 7));
            Sm.CmParam<Decimal>(ref cm, "@RSAvgSPL", Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<Decimal>(ref cm, "@RSMFR", Sm.GetGrdDec(Grd1, r, 9));

            //Winder
            Sm.CmParam<Decimal>(ref cm, "@WSpeed", Sm.GetGrdDec(Grd1, r, 10));
            Sm.CmParam<Decimal>(ref cm, "@WNE", Sm.GetGrdDec(Grd1, r, 11));
            Sm.CmParam<Decimal>(ref cm, "@WDelMC", Sm.GetGrdDec(Grd1, r, 12));
            Sm.CmParam<Decimal>(ref cm, "@WDelRun", Sm.GetGrdDec(Grd1, r, 13));
            Sm.CmParam<Decimal>(ref cm, "@WMFR", Sm.GetGrdDec(Grd1, r, 14));
            Sm.CmParam<Decimal>(ref cm, "@WDel", Sm.GetGrdDec(Grd1, r, 15));

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSpinningProduction2Dtl2(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSpinningProduction2Dtl2 ");
            SQL.AppendLine("(DocNo, DNo, ProdEfficiencyVar, Value, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @ProdEfficiencyVar, @Value, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProdEfficiencyVar", Sm.GetGrdStr(Grd2, r, 0));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd2, r, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSpinningProduction2Hdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataAlreadyCancelled();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist("Select DocNo From TblSpinningProduction2Hdr Where CancelInd='Y' And DocNo=@Param;", TxtDocNo.Text);
        }

        private MySqlCommand EditSpinningProduction2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSpinningProduction2Hdr Set CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        internal void ComputeTotalRunningMachine()
        {
            decimal RunningMachine = 0m;
            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                        RunningMachine += Sm.GetGrdDec(Grd1, r, 4);
            }
            TxtRunningMachine.EditValue = Sm.FormatNum(RunningMachine, 0);
        }

        private void SetLueWCCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode As Col1, WCName As Col2 ");
            SQL.AppendLine("From TblWCSpinning ");
            if (Code.Length > 0)
                SQL.AppendLine("Where WCCode=@Code;");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                SQL.AppendLine("And ProdDataPerItemInd='Y' ");
                SQL.AppendLine("Order By WCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void ShowRingSpinningFormula()
        {
            string ItCode = string.Empty, Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.ItCode=@ItCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    }
                }
            }

            if (Filter.Length == 0)
                Filter = " And 0=1 ";
            else
                Filter = " And ("+Filter+") ";

            SQL.AppendLine("Select B.ItCode, B.RPM, B.TPI, B.NE, B.AvgSPL, B.MFR ");
            SQL.AppendLine("From TblFormulaRingSpinningHdr A ");
            SQL.AppendLine("Inner Join TblFormulaRingSpinningDtl B On A.FRSCode=B.FRSCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("Order By B.ItCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "ItCode", 

                        //1-5
                        "RPM", "TPI", "NE", "AvgSPL", "MFR" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        for (int r= 0; r< Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), ItCode))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 5, 1);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 6, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 7, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 8, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 9, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ShowWinderFormula()
        {
            string ItCode = string.Empty, Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (ItCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.ItCode=@ItCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    }
                }
            }

            if (Filter.Length == 0)
                Filter = " And 0=1 ";
            else
                Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select B.ItCode, B.Speed, B.NE, B.DelMC, B.DelRun, B.MFR, B.Del ");
            SQL.AppendLine("From TblFormulaWinderHdr A ");
            SQL.AppendLine("Inner Join TblFormulaWinderDtl B On A.FWCode=B.FWCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("Order By B.ItCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "ItCode", 

                        //1-5
                        "Speed", "NE", "DelMC", "DelRun", "MFR", 
                        
                        //6
                        "Del" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 1), ItCode))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 10, 1);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 11, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 12, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 13, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 14, 5);
                                Sm.SetGrdValue("N", Grd1, dr, c, r, 15, 6);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ShowGrdData(string WCCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WCCode", WCCode);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                "Select A.ProdEfficiencyVar, B.OptDesc, A.DefaultValue " +
                "From TblWCSpinningEffVar A " +
                "Inner Join TblOption B On B.OptCat='ProdEfficiencyVar' and A.ProdEfficiencyVar=B.OptCode " +
                "Where A.UseInd='Y' And A.WCCode=@WCCode Order By B.OptDesc; ",
                new string[] { "ProdEfficiencyVar", "OptDesc", "DefaultValue" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ComputeRSMFR(int r)
        {
            decimal RSRPM = 0m, RSTPI = 0m, RSNE = 0m, RSAvgSPL = 0m, RSMFR = 0m;

            RSRPM = Sm.GetGrdDec(Grd1, r, 5);
            RSTPI = Sm.GetGrdDec(Grd1, r, 6);
            RSNE = Sm.GetGrdDec(Grd1, r, 7); 
            RSAvgSPL = Sm.GetGrdDec(Grd1, r, 8);

            decimal value1 = (RSRPM * 60m * 24m * RSAvgSPL * 1m);
            decimal value2 = (840m * 36m * RSTPI * RSNE * 400m);
            if(value2 !=0m)
            {
                RSMFR = (value1 / value2) * 181.44m;
            }

            Grd1.Cells[r, 9].Value = RSMFR;
        }

        private void ComputeWMFRandWDel(int r)
        {
            decimal WSpeed = 0m, WNE = 0m, WDelMC = 0m, WDelRun = 0m, WMFR = 0m, WDel = 0m;

            WSpeed = Sm.GetGrdDec(Grd1, r, 10);
            WNE = Sm.GetGrdDec(Grd1, r, 11);
            WDelMC = Sm.GetGrdDec(Grd1, r, 12);
            WDelRun = Sm.GetGrdDec(Grd1, r, 13);

            decimal pembagi = (768m * WNE * 400m);
            if (pembagi != 0m)
            {
                WMFR = ((60m * 24m * WDelMC * WSpeed) / pembagi) * 181.44m;
                WDel = ((WSpeed * 60m * 24m * WDelRun * 1m) / pembagi) * 181.44m;
            }

            Grd1.Cells[r, 14].Value = WMFR;
            Grd1.Cells[r, 15].Value = WDel;
        }

        private void ComputeRSRPM()
        {
            decimal RSRPM = 0m, RunningMachine = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0 && Sm.GetGrdStr(Grd1, r, 5).Length > 0)
                {
                    RSRPM += (Sm.GetGrdDec(Grd1, r, 4) * Sm.GetGrdDec(Grd1, r, 5));
                    RunningMachine += Sm.GetGrdDec(Grd1, r, 4);
                }
            }

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.CompareStr("02", Sm.GetGrdStr(Grd2, r, 0)) && RunningMachine != 0m)
                {
                    Grd2.Cells[r, 2].Value = RSRPM / RunningMachine;
                    break;
                }
            }
        }

        private void ComputeRSTPI()
        {
            decimal RSTPI = 0m, RunningMachine = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0 && Sm.GetGrdStr(Grd1, r, 6).Length > 0)
                {
                    RSTPI += (Sm.GetGrdDec(Grd1, r, 4) * Sm.GetGrdDec(Grd1, r, 6));
                    RunningMachine += Sm.GetGrdDec(Grd1, r, 4);
                }
            }

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.CompareStr("03", Sm.GetGrdStr(Grd2, r, 0)) && RunningMachine != 0m)
                {
                    Grd2.Cells[r, 2].Value = RSTPI / RunningMachine;
                    break;
                }
            }
        }

        private void ComputeRSAvgCountRiil()
        {
            decimal RSNE = 0m, RunningMachine = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0 && Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                {
                    RSNE += (Sm.GetGrdDec(Grd1, r, 4) * Sm.GetGrdDec(Grd1, r, 7));
                    RunningMachine += Sm.GetGrdDec(Grd1, r, 4);
                }
            }

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.CompareStr("06", Sm.GetGrdStr(Grd2, r, 0)) && RunningMachine != 0m)
                {
                    Grd2.Cells[r, 2].Value = RSNE / RunningMachine;
                    break;
                }
            }
        }

        private void ComputeWAvgCountRiil()
        {
            decimal WNE = 0m, Qty = 0m;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 3).Length > 0 && Sm.GetGrdStr(Grd1, r, 11).Length > 0)
                {
                    WNE += (Sm.GetGrdDec(Grd1, r, 3) * Sm.GetGrdDec(Grd1, r, 11));
                    Qty += Sm.GetGrdDec(Grd1, r, 3);
                }
            }

            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.CompareStr("06", Sm.GetGrdStr(Grd2, r, 0)) && Qty!=0m)
                {
                    Grd2.Cells[r, 2].Value = WNE/Qty;
                    break;
                }
            }
        }

        internal void RSComputeEfficiency()
        {
            if (Sm.CompareStr(mProdFormula, "02"))
            {
                ComputeRSRPM();
                ComputeRSTPI();
                ComputeRSAvgCountRiil();
            }
        }

        internal void WComputeEfficiency()
        {
            if (Sm.CompareStr(mProdFormula, "03"))
            {
                ComputeWAvgCountRiil();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueWCCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                mProdFormula = string.Empty;
                ClearGrd();
                Sm.RefreshLookUpEdit(LueWCCode, new Sm.RefreshLue2(SetLueWCCode), string.Empty);
                var WCCode = Sm.GetLue(LueWCCode);
                if (WCCode.Length > 0)
                {
                    mProdFormula = Sm.GetValue("Select ProdFormula From TblWCSpinning Where WCCode=@Param;", WCCode);
                    if (Sm.CompareStr(mProdFormula, "02"))
                        Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9 }, true);
                    if (Sm.CompareStr(mProdFormula, "03"))
                        Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, true);
                    ShowGrdData(WCCode);
                }
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtRunningMachine }, 2);
            }
        }

        private void TxtRunningMachine_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtRunningMachine, 2);
        }

        #endregion

        #region Grid Event

        #region Grid 1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0 )
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSpinningProduction2Dlg(this));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
                    }
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmSpinningProduction2Dlg(this));
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3) Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, e);
            if (e.ColIndex == 4) ComputeTotalRunningMachine();

            if (Sm.IsGrdColSelected(new int[] { 5, 6, 7, 8 }, e.ColIndex))
                ComputeRSMFR(e.RowIndex);

            if (Sm.IsGrdColSelected(new int[] { 10, 11, 12, 13 }, e.ColIndex))
                ComputeWMFRandWDel(e.RowIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6, 7, 8 }, e.ColIndex))
                RSComputeEfficiency();
            if (Sm.IsGrdColSelected(new int[] { 3, 4, 10, 11, 12, 13 }, e.ColIndex))
                WComputeEfficiency();
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalRunningMachine();
                if (Sm.CompareStr(mProdFormula, "02"))
                    RSComputeEfficiency();

                if (Sm.CompareStr(mProdFormula, "03"))
                    WComputeEfficiency();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 2

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 2) Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 2 }, e);
        }

        #endregion

        #endregion

        #region Button Event

        private void BtnCopySFC_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueWCCode, "Work center") ||
             Sm.IsDteEmpty(DteProdDt, "Production date")) return;

            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ItCode, C.ItName, Sum(B.Qty) As Qty ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItemSpinning C On B.ItCode=C.ItCode ");
            //SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode And D.ItGrpCode='002' ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt=@DocDt ");
            SQL.AppendLine("And A.WorkCenterDocNo=@WorkCenterDocNo ");
            SQL.AppendLine("Group By B.ItCode, C.ItName ");
            SQL.AppendLine("Having Sum(B.Qty)>0.00 ");
            SQL.AppendLine("Order By C.ItName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteProdDt));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetLue(LueWCCode));

            try
            {
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]{ "ItCode", "ItName", "Qty" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }); 
                        }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
                ComputeTotalRunningMachine();
                if (Sm.CompareStr(mProdFormula, "02"))
                    RSComputeEfficiency();

                if (Sm.CompareStr(mProdFormula, "03"))
                    WComputeEfficiency();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void BtnCopyFormula_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueWCCode, "Work center") || 
                IsGrdEmpty(Grd1)) return;

            try
            {
                if (Sm.CompareStr(mProdFormula, "02"))
                {
                    ShowRingSpinningFormula();
                    RSComputeEfficiency();
                }
                if (Sm.CompareStr(mProdFormula, "03")) 
                {
                    ShowWinderFormula();
                    WComputeEfficiency();
                };
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion
    }
}
