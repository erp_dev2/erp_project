﻿#region Update
/*
    21/12/2020 [IBL/SRN] New Apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestConsignmentDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestConsignment mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestConsignmentDlg2(FrmVoucherRequestConsignment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                SetLueWhsCode2(ref LueWhsCode);
                Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 } );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        #endregion

        #region Standar Method
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Bill_No BillNo, B.WhsCode, B.WhsName, A.Pay_Month PayMonth, A.Pay_Year PayYear, ");
            SQL.AppendLine("A.Total_Payable TotalPayable, C.VdName, ");
            SQL.AppendLine("tDeduction1, tDeduction2, tDeduction3, tDeduction4, tDeduction5, ");
            SQL.AppendLine("tDeduction6, tDeduction7, tDeduction8, tDeduction9 ");
            SQL.AppendLine("From " + mFrmParent.mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.Warehouse = B.WhsCode ");
            SQL.AppendLine("Inner Join TblVendor C On A.Vendor_Code = C.VdCodeInternal ");
            SQL.AppendLine("Where A.Status = 'AUTHORIZED 2' ");
            SQL.AppendLine("And A.Vendor_Code = @VdCodeInternal ");
            if(Sm.GetLue(mFrmParent.LueWhsCode).Length > 0 && Sm.GetLue(mFrmParent.LueWhsCode) != "all")
                SQL.AppendLine("And B.WhsCode = @WhsCode ");
            SQL.AppendLine("And Not Find_In_Set(Bill_No, @BillNo) ");
            SQL.AppendLine("And A.Bill_No Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.BillNo ");
            SQL.AppendLine("    From TblVoucherRequestConsignmentHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo = C.DocNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                Grd1, new String[]
                {
                    //0
                    "No. ",

                    //1-5
                    "",
                    "PPBK#",
                    "Warehouse",
                    "Vendor",
                    "Month Outs",

                    //6-8
                    "Year Outs",
                    "Payable",
                    "Warehouse Code",
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 }, true);
            Grd1.Cols[8].Move(2);
            Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@VdCodeInternal", mFrmParent.mVdCodeInternal);
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(mFrmParent.LueWhsCode));
                Sm.CmParam<String>(ref cm, "@BillNo", mFrmParent.GetBillNo());
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "B.WhsCode", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBillNo.Text, new string[] { "A.Bill_No" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.Pay_Year, A.Pay_Month",
                        new string[]
                        {
                            //0
                            "BillNo", 

                            //1-5
                            "WhsName", "VdName", "PayMonth", "PayYear", "TotalPayable",

                            //6
                            "WhsCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                int Row1 = 0, Row2 = 0;
                bool IsChoose = false;

                if (Grd1.Rows.Count != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !IsBillNoAlreadyChosen(Row))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;

                            mFrmParent.Grd1.Cells[Row1, 0].Value = Row1 + 1;
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);

                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 6 });
                        }
                    }
                    mFrmParent.ComputeVRAmt();
                    mFrmParent.ComputeDeductionAmt();
                }

                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            }
        }

        private bool IsBillNoAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #region Additional Method

        private void SetLueWhsCode2(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct B.WhsCode As Col1, B.WhsName As Col2 ");
            SQL.AppendLine("From " + mFrmParent.mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.Warehouse = B.WhsCode ");
            SQL.AppendLine(" Order By WhsName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBillNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBillNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PBBK#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(SetLueWhsCode2));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        #endregion

        #region Grid Control Method

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion
    }
}
