﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using MySql.Data.MySqlClient;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using Syncfusion.Windows;
using Syncfusion.Windows.Forms.Chart;
using SyXL = Syncfusion.XlsIO;
using SyDoc = Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmChrLeadTimePerformance : RunSystem.FrmBase10
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            exportFileName = string.Empty,
            file = string.Empty;

        private bool
            mIsNoOfDaysPurchasedItemComparisonReportShowDoc = false;

        #endregion

        #region Constructor

        public FrmChrLeadTimePerformance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnExcel.Visible = BtnPDF.Visible = BtnWord.Visible = false;
            mIsNoOfDaysPurchasedItemComparisonReportShowDoc = Sm.GetParameter("IsNoOfDaysPurchasedItemComparisonReportShowDoc") == "Y";
            string CurrentDate = Sm.ServerCurrentDateTime();
            DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-30);
            DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
            Sl.SetLueItCtCode(ref LueItCtCode);

            base.FrmLoad(sender, e);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                IsFilterByDateInvalid() ||
                IsFilterEmpty() ||
                IsBothFilterFilled()
            ) return;
            
            try
            {
                ChartAppearance.ApplyChartStyles(this.Chart);
                LoadData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Info, Exc.Message);
            }
        }

        private void LoadData()
        {
            var l = new List<PerItem>();
            var lAvg = new List<AverageAllItem>();

            #region Data Item

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter1 = "  ";

            SQL.AppendLine("Select X.ItCode, X.Performance, Round(Sum(X.NoD)/Count(X.Performance), 2) As NoD ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
            {
                SQL.AppendLine("Select T1.ItCode, 'POR v Recv' As Performance, IfNull(T5.Value3, 0) As NoD ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, 'Recv v ETA' As Performance, IfNull(T5.Value4, 0) As NoD ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, 'MR v Recv' As Performance, IfNull(T5.Value, 0) As NoD ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, 'Usage v Recv' As Performance, IfNull(T5.Value2, 0) As NoD ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

            }
            else
            {
                SQL.AppendLine("Select T1.ItCode, 'POR v Recv' As Performance, IfNull(T5.Value3, 0) As NoD ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DeptCode, B.ItCode ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, 'Recv v ETA' As Performance, IfNull(T5.Value4, 0) As NoD ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DeptCode, B.ItCode ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, 'MR v Recv' As Performance, IfNull(T5.Value, 0) As NoD ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DeptCode, B.ItCode ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T1.ItCode, 'Usage v Recv' As Performance, IfNull(T5.Value2, 0) As NoD ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Distinct A.DeptCode, B.ItCode ");
                SQL.AppendLine("    From TblMaterialRequestHdr A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(") T1 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select ItCode, ");
                SQL.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select B.ItCode, ");
                SQL.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL.AppendLine("        From TblMaterialRequestHdr A ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine("    ) T Group by ItCode ");
                SQL.AppendLine(") T5 On T1.ItCode=T5.ItCode ");
            }

            SQL.AppendLine(")X ");
            SQL.AppendLine("Inner Join TblItem Y On X.ItCode = Y.ItCode ");

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueItCtCode), "Y.ItCtCode", true);
            Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "Y.ItName", "Y.ForeignName" });

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString() + Filter1 + " Group By X.Performance; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]

                {
                    //0
                    "Performance",

                    //1
                    "NoD"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PerItem()
                        {
                            Performance = Sm.DrStr(dr, c[0]),
                            NoD = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                else
                    Sm.StdMsg(mMsgType.NoData, "");

                dr.Close();
            }

            #endregion

            #region Average All Item

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select X.ItCode, X.Performance As PerformanceAvg, Round(Sum(X.NoD)/Count(X.Performance), 2) As NoDAvg ");
            SQL2.AppendLine("From ");
            SQL2.AppendLine("( ");

            if (mIsNoOfDaysPurchasedItemComparisonReportShowDoc)
            {
                SQL2.AppendLine("Select T1.ItCode, 'POR v Recv' As Performance, IfNull(T5.Value3, 0) As NoD ");
                SQL2.AppendLine("From ( ");
                SQL2.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL2.AppendLine("    From TblMaterialRequestHdr A ");
                SQL2.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine(") T1 ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL2.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL2.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL2.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL2.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL2.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL2.AppendLine("Union All ");

                SQL2.AppendLine("Select T1.ItCode, 'Recv v ETA' As Performance, IfNull(T5.Value4, 0) As NoD ");
                SQL2.AppendLine("From ( ");
                SQL2.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL2.AppendLine("    From TblMaterialRequestHdr A ");
                SQL2.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine(") T1 ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL2.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL2.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL2.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL2.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL2.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL2.AppendLine("Union All ");

                SQL2.AppendLine("Select T1.ItCode, 'MR v Recv' As Performance, IfNull(T5.Value, 0) As NoD ");
                SQL2.AppendLine("From ( ");
                SQL2.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL2.AppendLine("    From TblMaterialRequestHdr A ");
                SQL2.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine(") T1 ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL2.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL2.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL2.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL2.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL2.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

                SQL2.AppendLine("Union All ");

                SQL2.AppendLine("Select T1.ItCode, 'Usage v Recv' As Performance, IfNull(T5.Value2, 0) As NoD ");
                SQL2.AppendLine("From ( ");
                SQL2.AppendLine("    Select Distinct A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, A.DeptCode, B.ItCode, C.SiteName ");
                SQL2.AppendLine("    From TblMaterialRequestHdr A ");
                SQL2.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("    Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine(") T1 ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff, C.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Left Join TblSite C On A.SiteCode=C.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, ItCode, SiteName ");
                SQL2.AppendLine(") T2 On T1.MaterialRequestDocNo=T2.MaterialRequestDocNo And T1.ItCode=T2.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2, E.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Left Join TblSite E On A.SiteCode=E.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T3 On T1.MaterialRequestDocNo=T3.MaterialRequestDocNo And T1.ItCode=T3.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, F.DocNo As PODocNo, F.DocDt As PODocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6, G.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Left Join TblSite G On A.SiteCode=G.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T4 On T3.MaterialRequestDocNo=T4.MaterialRequestDocNo And T3.PORequestDocNo=T4.PORequestDocNo And T3.ItCode=T4.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL2.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL2.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, SiteName ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select A.DocNo As MaterialRequestDocNo, A.DocDt As MaterialRequestDocDt, D.DocNo As PORequestDocNo, D.DocDt As PORequestDocDt, ");
                SQL2.AppendLine("        F.DocNo As PODocNo, F.DocDt As PODocDt, H.DocNo As RecvVdDocNo, H.DocDt As RecvVdDocDt, B.ItCode, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5, I.SiteName ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL2.AppendLine("        Left Join TblSite I On A.SiteCode=I.SiteCode ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by MaterialRequestDocNo, MaterialRequestDocDt, PORequestDocNo, PORequestDocDt, PODocNo, PODocDt, RecvVdDocNo, RecvVdDocDt, ItCode, SiteName ");
                SQL2.AppendLine(") T5 On T4.MaterialRequestDocNo=T5.MaterialRequestDocNo And T4.PORequestDocNo=T5.PORequestDocNo And T4.PODocNo=T5.PODocNo And T4.ItCode=T5.ItCode ");

            }
            else
            {
                SQL2.AppendLine("Select T1.ItCode, 'POR v Recv' As Performance, IfNull(T5.Value3, 0) As NoD ");
                SQL2.AppendLine("From ( ");
                SQL2.AppendLine("    Select Distinct A.DeptCode, B.ItCode ");
                SQL2.AppendLine("    From TblMaterialRequestHdr A ");
                SQL2.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine(") T1 ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL2.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL2.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL2.AppendLine("Union All ");

                SQL2.AppendLine("Select T1.ItCode, 'Recv v ETA' As Performance, IfNull(T5.Value4, 0) As NoD ");
                SQL2.AppendLine("From ( ");
                SQL2.AppendLine("    Select Distinct A.DeptCode, B.ItCode ");
                SQL2.AppendLine("    From TblMaterialRequestHdr A ");
                SQL2.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine(") T1 ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL2.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL2.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL2.AppendLine("Union All ");

                SQL2.AppendLine("Select T1.ItCode, 'MR v Recv' As Performance, IfNull(T5.Value, 0) As NoD ");
                SQL2.AppendLine("From ( ");
                SQL2.AppendLine("    Select Distinct A.DeptCode, B.ItCode ");
                SQL2.AppendLine("    From TblMaterialRequestHdr A ");
                SQL2.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine(") T1 ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL2.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL2.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T5 On T1.ItCode=T5.ItCode ");

                SQL2.AppendLine("Union All ");

                SQL2.AppendLine("Select T1.ItCode, 'Usage v Recv' As Performance, IfNull(T5.Value2, 0) As NoD ");
                SQL2.AppendLine("From ( ");
                SQL2.AppendLine("    Select Distinct A.DeptCode, B.ItCode ");
                SQL2.AppendLine("    From TblMaterialRequestHdr A ");
                SQL2.AppendLine("    Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine(") T1 ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, DateDiff(B.UsageDt, A.DocDt) As Diff ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T2 On T1.ItCode=T2.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, DateDiff(D.DocDt, A.DocDt) As Diff, DateDiff(D.DocDt, B.UsageDt) As Diff2 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T3 On T1.ItCode=T3.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, Sum(Diff2)/Count(ItCode) As Value2, ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5, Sum(Diff6)/Count(ItCode) As Value6 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, A.DocDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, B.UsageDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, B.UsageDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(F.DocDt, D.DocDt) As Diff5, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, D.DocDt) As Diff6 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T4 On T1.ItCode=T4.ItCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("    Select ItCode, ");
                SQL2.AppendLine("    Sum(Diff)/Count(ItCode) As Value, ");
                SQL2.AppendLine("    Sum(Diff2)/Count(ItCode) As Value2,  ");
                SQL2.AppendLine("    Sum(Diff3)/Count(ItCode) As Value3, ");
                SQL2.AppendLine("    Sum(Diff4)/Count(ItCode) As Value4, ");
                SQL2.AppendLine("    Sum(Diff5)/Count(ItCode) As Value5 ");
                SQL2.AppendLine("    From ( ");
                SQL2.AppendLine("        Select B.ItCode, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, A.DocDt) As Diff, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, B.UsageDt) As Diff2, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, D.DocDt) As Diff3, ");
                SQL2.AppendLine("        DateDiff(E.EstRecvDt, H.DocDt) As Diff4, ");
                SQL2.AppendLine("        DateDiff(H.DocDt, F.DocDt) As Diff5 ");
                SQL2.AppendLine("        From TblMaterialRequestHdr A ");
                SQL2.AppendLine("        Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.Cancelind='N' And IfNull(B.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.Cancelind='N' And IfNull(C.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblPORequestHdr D On C.DocNo=D.DocNo ");
                SQL2.AppendLine("        Inner Join TblPODtl E On C.DocNo=E.PORequestDocNo And C.DNo=E.PORequestDNo And E.Cancelind='N' ");
                SQL2.AppendLine("        Inner Join TblPOHdr F On E.DocNo=F.DocNo ");
                SQL2.AppendLine("        Inner Join TblRecvVdDtl G On e.DocNo=G.PODocNo And E.DNo=G.PODNo And G.Cancelind='N' And IfNull(G.Status, 'O')<>'C' ");
                SQL2.AppendLine("        Inner Join TblRecvVdHdr H On G.DocNo=H.DocNo And H.POInd='Y' ");
                SQL2.AppendLine("        Where (A.DocDt Between @DocDt1 And @DocDt2) ");
                SQL2.AppendLine("    ) T Group by ItCode ");
                SQL2.AppendLine(") T5 On T1.ItCode=T5.ItCode ");
            }

            SQL2.AppendLine(")X ");
            SQL2.AppendLine("Inner Join TblItem Y On X.ItCode = Y.ItCode ");

            Sm.CmParamDt(ref cm2, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm2, "@DocDt2", Sm.GetDte(DteDocDt2));

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString() + " Group By X.Performance; ";
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[]

                {
                    //0
                    "PerformanceAvg",

                    //1
                    "NoDAvg"
                });

                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        lAvg.Add(new AverageAllItem()
                        {
                            vs = Sm.DrStr(dr2, c2[0]),
                            AvgPoint = Sm.DrDec(dr2, c2[1])
                        });
                    }
                }
                else
                    Sm.StdMsg(mMsgType.NoData, "");

                dr2.Close();
            }

            #endregion

            BindChart(l, lAvg);
        }

        private void BindChart(object dataChart, object dataChartAvg)
        {
            this.Chart.Series.Clear();
            ChartSeries series = new ChartSeries("Per Item", ChartSeriesType.Line);
            ChartSeries seriesAvg = new ChartSeries("All Item", ChartSeriesType.Line);
            ChartDataBindModel dataSeriesModel = new ChartDataBindModel(dataChart);
            ChartDataBindModel dataSeriesModelAvg = new ChartDataBindModel(dataChartAvg);
            ChartLegend legends = new ChartLegend(Chart);

            // If ChartDataBindModel.XName is empty or null, X value is index of point.
            dataSeriesModel.XName = "Performance";
            dataSeriesModel.YNames = new string[] { "NoD" };
            series.Text = series.Name;

            dataSeriesModelAvg.XName = "vs";
            dataSeriesModelAvg.YNames = new string[] { "AvgPoint" };
            seriesAvg.Text = seriesAvg.Name;

            //display point on top of series
            series.Style.DisplayText = true;
            series.Style.TextOrientation = ChartTextOrientation.UpRight;

            seriesAvg.Style.DisplayText = true;
            seriesAvg.Style.TextOrientation = ChartTextOrientation.Right;

            //series.SeriesModel = dataSeriesModel;
            series.SeriesIndexedModelImpl = dataSeriesModel;
            seriesAvg.SeriesIndexedModelImpl = dataSeriesModelAvg;

            ChartDataBindAxisLabelModel dataLabelsModel = new ChartDataBindAxisLabelModel(dataChart);
            ChartDataBindAxisLabelModel dataLabelsModelAvg = new ChartDataBindAxisLabelModel(dataChartAvg);
            dataLabelsModel.LabelName = "Performance";
            dataLabelsModelAvg.LabelName = "vs";
            Chart.Series.Add(series);
            Chart.Series.Add(seriesAvg);
            Chart.Legends.Add(legends);

            // legend
            //Chart.Legend.RepresentationType = ChartLegendRepresentationType.SeriesType;
            //Chart.LegendPosition = ChartDock.Top;
            //Chart.LegendsPlacement = ChartPlacement.Outside;
            //Chart.LegendAlignment = ChartAlignment.Center;

            //Chart.Axes.Add(seriesAvg);

            Chart.PrimaryXAxis.LabelsImpl = dataLabelsModel;
            Chart.PrimaryXAxis.ValueType = ChartValueType.Custom;
            Chart.PrimaryXAxis.Title = "Supply of Process";
            Chart.PrimaryYAxis.Title = "Average Lead Time (Days)";
        }

        #endregion

        #region Additional Method

        private bool IsFilterByDateInvalid()
        {
            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        private bool IsFilterNotValid()
        {
            return                
                    IsFilterEmpty() ||
                    IsBothFilterFilled()
                ;
        }

        private bool IsFilterEmpty()
        {
            if (TxtItCode.Text.Length == 0 && LueItCtCode.EditValue == null)
            {
                Sm.StdMsg(mMsgType.Info, "Data should be filtered either by Item or Category.");
                TxtItCode.Focus();
                return true;
            }
            
            return false;
        }

        private bool IsBothFilterFilled()
        {
            if (TxtItCode.Text.Length > 0 && !(LueItCtCode.EditValue == null ||
                LueItCtCode.Text.Trim().Length == 0 ||
                LueItCtCode.EditValue.ToString().Trim().Length == 0))
            {
                Sm.StdMsg(mMsgType.Info, "Only one filter allowed.");
                return true;
            }

            return false;
        }

        protected void OpenFile(string filetype, string exportFileName)
        {
            try
            {
                //if (filetype == "Grid")
                //    gridForm.ShowDialog();
                //else
                System.Diagnostics.Process.Start(exportFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #region Button Click

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            PrintDialog pr = new PrintDialog();
            PrintPreviewDialog ppd = new PrintPreviewDialog();

            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.Info, "No Data");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Info, "The chart is empty");
                else
                {
                    pr.AllowSomePages = true;
                    pr.AllowSelection = true;
                    pr.PrinterSettings.Clone();
                    pr.Document = Chart.PrintDocument;
                    if (pr.ShowDialog() == DialogResult.OK)
                        pr.Document.Print();
                }
            }

            //if (pr.ShowDialog() == DialogResult.OK)
            //{
            //    pr.Document = Chart.PrintDocument;
            //    ppd.Document = Chart.PrintDocument;
            //    if (ppd.ShowDialog() == DialogResult.OK)
            //        //pr.Document.Print();
            //        ppd.Document.Print();
            //}
        }

        private void BtnWord_Click(object sender, EventArgs e)
        {
            try
            {
                if(Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".doc";
                        file = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".gif";

                        //Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + "_" + Sm.ConvertDate(Sm.GetDte(DteDocDt1)) + "_TO_" + Sm.ConvertDate(Sm.GetDte(DteDocDt2)) + ".gif";
                        //if (!System.IO.File.Exists(file))
                        Chart.SaveImage(file);

                        //Create a new document
                        WordDocument document = new WordDocument();
                        
                        //Adding a new section to the document.
                        IWSection section = document.AddSection();
                        //Adding a paragraph to the section
                        IWParagraph paragraph = section.AddParagraph();
                        //Writing text.
                        paragraph.AppendText("Top Destination Diagram");
                        //Adding a new paragraph		
                        paragraph = section.AddParagraph();
                        paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        //Inserting chart.
                        paragraph.AppendPicture(Image.FromFile(file));
                        //Save the Document to disk.
                        document.Save(exportFileName, Syncfusion.DocIO.FormatType.Doc);
                        System.Diagnostics.Process.Start(exportFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.NoData, "");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Info, "The chart is empty.");
                else
                {
                    exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".xls";

                    // A new workbook with a worksheet should be created. 
                    SyXL.IWorkbook chartBook = SyXL.ExcelUtils.CreateWorkbook(1);
                    SyXL.IWorksheet sheet = chartBook.Worksheets[0];

                    //if chart is not empty
                    // Fill the worksheet with chart data. 
                    for (int i = 1; i <= Chart.Series[0].Points.Count; i++)
                    {
                        sheet.Range[i, 1].Number = Chart.Series[0].Points[i - 1].X;
                        sheet.Range[i, 2].Number = Chart.Series[0].Points[i - 1].YValues[0];
                    }

                    // Create a chart worksheet. 
                    SyXL.IChart chart = chartBook.Charts.Add(Chart.Title.Text);

                    // Specify the title of the Chart.
                    chart.ChartTitle = Chart.Title.Text;

                    // Initialize a new series instance and add it to the series collection of the chart. 
                    SyXL.IChartSerie series = chart.Series.Add("Top Destination");

                    // Specify the chart type of the series. 
                    series.SerieType = SyXL.ExcelChartType.Column_Clustered;

                    // Specify the name of the series. This will be displayed as the text of the legend. 
                    //series.Name = Chart.Name;

                    // Specify the value ranges for the series.
                    series.Values = sheet.Range["B1:B10"];

                    // Specify the Category labels for the series. 
                    series.CategoryLabels = sheet.Range["A1:A10"];

                    // Make the chart as active sheet. 
                    chart.Activate();

                    // Save the Chart book. 
                    chartBook.SaveAs(exportFileName); chartBook.Close();
                    SyXL.ExcelUtils.Close();

                    // Launches the file. 
                    System.Diagnostics.Process.Start(exportFileName);
                }
            }
        }

        private void BtnPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".pdf";
                        file = Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + ".gif";

                        //if (!System.IO.File.Exists(file))
                        this.Chart.SaveImage(file);

                        //Create a new PDF Document. The pdfDoc object represents the PDF document.
                        //This document has one page by default and additional pages have to be added.
                        PdfDocument pdfDoc = new PdfDocument();

                        pdfDoc.Pages.Add();

                        pdfDoc.Pages[0].Graphics.DrawImage(PdfImage.FromFile(file), new PointF(10, 30));

                        //Save the PDF Document to disk.
                        pdfDoc.Save(exportFileName);
                        OpenFile("Pdf", exportFileName);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
        }

        #endregion

        #endregion

    }

    #region Class

    class PerItem
    {
        public string Performance { get; set; }
        public decimal NoD { get; set; }
    }

    class AverageAllItem
    {
        public string vs { get; set; }
        public decimal AvgPoint { get; set; }
    }

    #endregion
}
