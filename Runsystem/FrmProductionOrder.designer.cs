﻿namespace RunSystem
{
    partial class FrmProductionOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProductionOrder));
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSODocNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtQty = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtPlanningUomCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtProductionRoutingDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnSODocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnItCode = new DevExpress.XtraEditors.SimpleButton();
            this.BtnProductionRoutingDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSODocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnProductionRouting2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtItCodeInternal = new DevExpress.XtraEditors.TextEdit();
            this.TxtCtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtQty2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtPlanningUomCode2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtSpecification = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.LuePGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueProductionWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.WorkGroup = new System.Windows.Forms.Label();
            this.TxtBatchQuantity = new DevExpress.XtraEditors.TextEdit();
            this.LblBatchQty = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSODocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionRoutingDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningUomCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchQuantity.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(734, 0);
            this.panel1.Size = new System.Drawing.Size(70, 540);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtBatchQuantity);
            this.panel2.Controls.Add(this.LblBatchQty);
            this.panel2.Controls.Add(this.LueProductionWorkGroup);
            this.panel2.Controls.Add(this.WorkGroup);
            this.panel2.Controls.Add(this.TxtSpecification);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtQty2);
            this.panel2.Controls.Add(this.TxtPlanningUomCode2);
            this.panel2.Controls.Add(this.TxtCtItCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtItCodeInternal);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.BtnProductionRouting2);
            this.panel2.Controls.Add(this.BtnSODocNo2);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.BtnSODocNo);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtSODocNo);
            this.panel2.Controls.Add(this.BtnProductionRoutingDocNo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtQty);
            this.panel2.Controls.Add(this.DteUsageDt);
            this.panel2.Controls.Add(this.BtnItCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtPlanningUomCode);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.TxtProductionRoutingDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Size = new System.Drawing.Size(734, 306);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LuePGCode);
            this.panel3.Controls.Add(this.DteEndDt);
            this.panel3.Controls.Add(this.DteStartDt);
            this.panel3.Location = new System.Drawing.Point(0, 306);
            this.panel3.Size = new System.Drawing.Size(734, 234);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.DteStartDt, 0);
            this.panel3.Controls.SetChildIndex(this.DteEndDt, 0);
            this.panel3.Controls.SetChildIndex(this.LuePGCode, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 518);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(734, 234);
            this.Grd1.TabIndex = 44;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(137, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceFocused.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(101, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(137, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(61, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(137, 46);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(116, 20);
            this.DteUsageDt.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(64, 49);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Usage Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSODocNo
            // 
            this.TxtSODocNo.EnterMoveNextControl = true;
            this.TxtSODocNo.Location = new System.Drawing.Point(137, 67);
            this.TxtSODocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSODocNo.Name = "TxtSODocNo";
            this.TxtSODocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSODocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSODocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSODocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSODocNo.Properties.MaxLength = 16;
            this.TxtSODocNo.Properties.ReadOnly = true;
            this.TxtSODocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtSODocNo.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(56, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "SO# / MTS#";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(137, 88);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 16;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(166, 20);
            this.TxtItCode.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(101, 91);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Item";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(305, 89);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 16;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(372, 20);
            this.TxtItName.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(68, 112);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Local Code";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQty
            // 
            this.TxtQty.EnterMoveNextControl = true;
            this.TxtQty.Location = new System.Drawing.Point(137, 172);
            this.TxtQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty.Name = "TxtQty";
            this.TxtQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty.Properties.Appearance.Options.UseFont = true;
            this.TxtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty.Size = new System.Drawing.Size(81, 20);
            this.TxtQty.TabIndex = 32;
            this.TxtQty.Validated += new System.EventHandler(this.TxtQty_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(80, 175);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 14);
            this.label15.TabIndex = 31;
            this.label15.Text = "Quantity";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPlanningUomCode
            // 
            this.TxtPlanningUomCode.EnterMoveNextControl = true;
            this.TxtPlanningUomCode.Location = new System.Drawing.Point(220, 172);
            this.TxtPlanningUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlanningUomCode.Name = "TxtPlanningUomCode";
            this.TxtPlanningUomCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPlanningUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlanningUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlanningUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPlanningUomCode.Properties.MaxLength = 16;
            this.TxtPlanningUomCode.Properties.ReadOnly = true;
            this.TxtPlanningUomCode.Size = new System.Drawing.Size(83, 20);
            this.TxtPlanningUomCode.TabIndex = 33;
            // 
            // TxtProductionRoutingDocNo
            // 
            this.TxtProductionRoutingDocNo.EnterMoveNextControl = true;
            this.TxtProductionRoutingDocNo.Location = new System.Drawing.Point(137, 214);
            this.TxtProductionRoutingDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProductionRoutingDocNo.Name = "TxtProductionRoutingDocNo";
            this.TxtProductionRoutingDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProductionRoutingDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProductionRoutingDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProductionRoutingDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtProductionRoutingDocNo.Properties.MaxLength = 16;
            this.TxtProductionRoutingDocNo.Properties.ReadOnly = true;
            this.TxtProductionRoutingDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtProductionRoutingDocNo.TabIndex = 37;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(76, 217);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 14);
            this.label9.TabIndex = 36;
            this.label9.Text = "Routing#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(137, 236);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(565, 20);
            this.MeeRemark.TabIndex = 41;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(87, 239);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 40;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSODocNo
            // 
            this.BtnSODocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSODocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSODocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSODocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSODocNo.Appearance.Options.UseBackColor = true;
            this.BtnSODocNo.Appearance.Options.UseFont = true;
            this.BtnSODocNo.Appearance.Options.UseForeColor = true;
            this.BtnSODocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSODocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSODocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSODocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSODocNo.Image")));
            this.BtnSODocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSODocNo.Location = new System.Drawing.Point(303, 68);
            this.BtnSODocNo.Name = "BtnSODocNo";
            this.BtnSODocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnSODocNo.TabIndex = 19;
            this.BtnSODocNo.ToolTip = "Find Sales Order# / Make To Stock#";
            this.BtnSODocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSODocNo.ToolTipTitle = "Run System";
            this.BtnSODocNo.Click += new System.EventHandler(this.BtnSODocNo_Click);
            // 
            // BtnItCode
            // 
            this.BtnItCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItCode.Appearance.Options.UseBackColor = true;
            this.BtnItCode.Appearance.Options.UseFont = true;
            this.BtnItCode.Appearance.Options.UseForeColor = true;
            this.BtnItCode.Appearance.Options.UseTextOptions = true;
            this.BtnItCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnItCode.Image")));
            this.BtnItCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItCode.Location = new System.Drawing.Point(678, 88);
            this.BtnItCode.Name = "BtnItCode";
            this.BtnItCode.Size = new System.Drawing.Size(24, 21);
            this.BtnItCode.TabIndex = 24;
            this.BtnItCode.ToolTip = "Show Item Information";
            this.BtnItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItCode.ToolTipTitle = "Run System";
            this.BtnItCode.Click += new System.EventHandler(this.BtnItCode_Click);
            // 
            // BtnProductionRoutingDocNo
            // 
            this.BtnProductionRoutingDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnProductionRoutingDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProductionRoutingDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProductionRoutingDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProductionRoutingDocNo.Appearance.Options.UseBackColor = true;
            this.BtnProductionRoutingDocNo.Appearance.Options.UseFont = true;
            this.BtnProductionRoutingDocNo.Appearance.Options.UseForeColor = true;
            this.BtnProductionRoutingDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnProductionRoutingDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProductionRoutingDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnProductionRoutingDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnProductionRoutingDocNo.Image")));
            this.BtnProductionRoutingDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnProductionRoutingDocNo.Location = new System.Drawing.Point(305, 212);
            this.BtnProductionRoutingDocNo.Name = "BtnProductionRoutingDocNo";
            this.BtnProductionRoutingDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnProductionRoutingDocNo.TabIndex = 38;
            this.BtnProductionRoutingDocNo.ToolTip = "Find Production Routing\'s Document Number";
            this.BtnProductionRoutingDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnProductionRoutingDocNo.ToolTipTitle = "Run System";
            this.BtnProductionRoutingDocNo.Click += new System.EventHandler(this.BtnProductionRoutingDocNo_Click);
            // 
            // BtnSODocNo2
            // 
            this.BtnSODocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSODocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSODocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSODocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSODocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSODocNo2.Appearance.Options.UseFont = true;
            this.BtnSODocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSODocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSODocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSODocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSODocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSODocNo2.Image")));
            this.BtnSODocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSODocNo2.Location = new System.Drawing.Point(328, 68);
            this.BtnSODocNo2.Name = "BtnSODocNo2";
            this.BtnSODocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnSODocNo2.TabIndex = 20;
            this.BtnSODocNo2.ToolTip = "Show Sales Order# / Make to Stock#";
            this.BtnSODocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSODocNo2.ToolTipTitle = "Run System";
            this.BtnSODocNo2.Click += new System.EventHandler(this.BtnSODocNo2_Click);
            // 
            // BtnProductionRouting2
            // 
            this.BtnProductionRouting2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnProductionRouting2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProductionRouting2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProductionRouting2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProductionRouting2.Appearance.Options.UseBackColor = true;
            this.BtnProductionRouting2.Appearance.Options.UseFont = true;
            this.BtnProductionRouting2.Appearance.Options.UseForeColor = true;
            this.BtnProductionRouting2.Appearance.Options.UseTextOptions = true;
            this.BtnProductionRouting2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProductionRouting2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnProductionRouting2.Image = ((System.Drawing.Image)(resources.GetObject("BtnProductionRouting2.Image")));
            this.BtnProductionRouting2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnProductionRouting2.Location = new System.Drawing.Point(327, 212);
            this.BtnProductionRouting2.Name = "BtnProductionRouting2";
            this.BtnProductionRouting2.Size = new System.Drawing.Size(24, 21);
            this.BtnProductionRouting2.TabIndex = 39;
            this.BtnProductionRouting2.ToolTip = "Show Production Routing\'s Document  Number";
            this.BtnProductionRouting2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnProductionRouting2.ToolTipTitle = "Run System";
            this.BtnProductionRouting2.Click += new System.EventHandler(this.BtnProductionRouting2_Click);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(306, 4);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.ShowToolTips = false;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 12;
            // 
            // TxtItCodeInternal
            // 
            this.TxtItCodeInternal.EnterMoveNextControl = true;
            this.TxtItCodeInternal.Location = new System.Drawing.Point(137, 109);
            this.TxtItCodeInternal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCodeInternal.Name = "TxtItCodeInternal";
            this.TxtItCodeInternal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCodeInternal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCodeInternal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCodeInternal.Properties.Appearance.Options.UseFont = true;
            this.TxtItCodeInternal.Properties.MaxLength = 30;
            this.TxtItCodeInternal.Properties.ReadOnly = true;
            this.TxtItCodeInternal.Size = new System.Drawing.Size(166, 20);
            this.TxtItCodeInternal.TabIndex = 26;
            // 
            // TxtCtItCode
            // 
            this.TxtCtItCode.EnterMoveNextControl = true;
            this.TxtCtItCode.Location = new System.Drawing.Point(137, 151);
            this.TxtCtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtItCode.Name = "TxtCtItCode";
            this.TxtCtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtItCode.Properties.MaxLength = 40;
            this.TxtCtItCode.Properties.ReadOnly = true;
            this.TxtCtItCode.Size = new System.Drawing.Size(166, 20);
            this.TxtCtItCode.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(5, 154);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 14);
            this.label4.TabIndex = 29;
            this.label4.Text = "Customer\'s Item Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQty2
            // 
            this.TxtQty2.EnterMoveNextControl = true;
            this.TxtQty2.Location = new System.Drawing.Point(137, 193);
            this.TxtQty2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty2.Name = "TxtQty2";
            this.TxtQty2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQty2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty2.Properties.Appearance.Options.UseFont = true;
            this.TxtQty2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty2.Properties.ReadOnly = true;
            this.TxtQty2.Size = new System.Drawing.Size(81, 20);
            this.TxtQty2.TabIndex = 34;
            // 
            // TxtPlanningUomCode2
            // 
            this.TxtPlanningUomCode2.EnterMoveNextControl = true;
            this.TxtPlanningUomCode2.Location = new System.Drawing.Point(220, 193);
            this.TxtPlanningUomCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlanningUomCode2.Name = "TxtPlanningUomCode2";
            this.TxtPlanningUomCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPlanningUomCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlanningUomCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlanningUomCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtPlanningUomCode2.Properties.MaxLength = 16;
            this.TxtPlanningUomCode2.Properties.ReadOnly = true;
            this.TxtPlanningUomCode2.Size = new System.Drawing.Size(83, 20);
            this.TxtPlanningUomCode2.TabIndex = 35;
            // 
            // TxtSpecification
            // 
            this.TxtSpecification.EnterMoveNextControl = true;
            this.TxtSpecification.Location = new System.Drawing.Point(137, 130);
            this.TxtSpecification.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSpecification.Name = "TxtSpecification";
            this.TxtSpecification.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSpecification.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSpecification.Properties.Appearance.Options.UseFont = true;
            this.TxtSpecification.Properties.MaxLength = 30;
            this.TxtSpecification.Properties.ReadOnly = true;
            this.TxtSpecification.Size = new System.Drawing.Size(166, 20);
            this.TxtSpecification.TabIndex = 28;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(59, 133);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 14);
            this.label8.TabIndex = 27;
            this.label8.Text = "Specification";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(224, 21);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(129, 20);
            this.DteEndDt.TabIndex = 46;
            this.DteEndDt.Visible = false;
            this.DteEndDt.Leave += new System.EventHandler(this.DteEndDt_Leave);
            this.DteEndDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEndDt_KeyDown);
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(83, 21);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(129, 20);
            this.DteStartDt.TabIndex = 45;
            this.DteStartDt.Visible = false;
            this.DteStartDt.Leave += new System.EventHandler(this.DteStartDt_Leave);
            this.DteStartDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDt_KeyDown);
            // 
            // LuePGCode
            // 
            this.LuePGCode.EnterMoveNextControl = true;
            this.LuePGCode.Location = new System.Drawing.Point(372, 21);
            this.LuePGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePGCode.Name = "LuePGCode";
            this.LuePGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.Appearance.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePGCode.Properties.DropDownRows = 25;
            this.LuePGCode.Properties.NullText = "[Empty]";
            this.LuePGCode.Properties.PopupWidth = 220;
            this.LuePGCode.Size = new System.Drawing.Size(220, 20);
            this.LuePGCode.TabIndex = 47;
            this.LuePGCode.ToolTip = "F4 : Show/hide list";
            this.LuePGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePGCode.EditValueChanged += new System.EventHandler(this.LuePGCode_EditValueChanged);
            this.LuePGCode.Leave += new System.EventHandler(this.LuePGCode_Leave);
            this.LuePGCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePGCode_KeyDown);
            // 
            // LueProductionWorkGroup
            // 
            this.LueProductionWorkGroup.EnterMoveNextControl = true;
            this.LueProductionWorkGroup.Location = new System.Drawing.Point(137, 258);
            this.LueProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionWorkGroup.Name = "LueProductionWorkGroup";
            this.LueProductionWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionWorkGroup.Properties.DropDownRows = 25;
            this.LueProductionWorkGroup.Properties.NullText = "[Empty]";
            this.LueProductionWorkGroup.Properties.PopupWidth = 650;
            this.LueProductionWorkGroup.Size = new System.Drawing.Size(169, 20);
            this.LueProductionWorkGroup.TabIndex = 43;
            this.LueProductionWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueProductionWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // WorkGroup
            // 
            this.WorkGroup.AutoSize = true;
            this.WorkGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkGroup.ForeColor = System.Drawing.Color.Red;
            this.WorkGroup.Location = new System.Drawing.Point(94, 261);
            this.WorkGroup.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.WorkGroup.Name = "WorkGroup";
            this.WorkGroup.Size = new System.Drawing.Size(40, 14);
            this.WorkGroup.TabIndex = 42;
            this.WorkGroup.Text = "Group";
            this.WorkGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBatchQuantity
            // 
            this.TxtBatchQuantity.EnterMoveNextControl = true;
            this.TxtBatchQuantity.Location = new System.Drawing.Point(137, 280);
            this.TxtBatchQuantity.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBatchQuantity.Name = "TxtBatchQuantity";
            this.TxtBatchQuantity.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtBatchQuantity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBatchQuantity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBatchQuantity.Properties.Appearance.Options.UseFont = true;
            this.TxtBatchQuantity.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBatchQuantity.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBatchQuantity.Properties.ReadOnly = true;
            this.TxtBatchQuantity.Size = new System.Drawing.Size(169, 20);
            this.TxtBatchQuantity.TabIndex = 45;
            // 
            // LblBatchQty
            // 
            this.LblBatchQty.AutoSize = true;
            this.LblBatchQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBatchQty.ForeColor = System.Drawing.Color.Red;
            this.LblBatchQty.Location = new System.Drawing.Point(45, 283);
            this.LblBatchQty.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBatchQty.Name = "LblBatchQty";
            this.LblBatchQty.Size = new System.Drawing.Size(89, 14);
            this.LblBatchQty.TabIndex = 44;
            this.LblBatchQty.Text = "Batch Quantity";
            this.LblBatchQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmProductionOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 540);
            this.Name = "FrmProductionOrder";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSODocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProductionRoutingDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlanningUomCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchQuantity.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtSODocNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtProductionRoutingDocNo;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtPlanningUomCode;
        internal DevExpress.XtraEditors.TextEdit TxtQty;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.SimpleButton BtnSODocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnProductionRoutingDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnItCode;
        public DevExpress.XtraEditors.SimpleButton BtnSODocNo;
        public DevExpress.XtraEditors.SimpleButton BtnProductionRouting2;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.TextEdit TxtCtItCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtItCodeInternal;
        internal DevExpress.XtraEditors.TextEdit TxtQty2;
        internal DevExpress.XtraEditors.TextEdit TxtPlanningUomCode2;
        internal DevExpress.XtraEditors.TextEdit TxtSpecification;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        private DevExpress.XtraEditors.LookUpEdit LuePGCode;
        private DevExpress.XtraEditors.LookUpEdit LueProductionWorkGroup;
        private System.Windows.Forms.Label WorkGroup;
        internal DevExpress.XtraEditors.TextEdit TxtBatchQuantity;
        private System.Windows.Forms.Label LblBatchQty;
    }
}