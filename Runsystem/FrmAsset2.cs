﻿#region Update
/*
    28/12/2021 [TYO/PRODUCT] Menambah kolom residual value
    31/12/2021 [ISD/PRODUCT] Rename field Annual Depreciation
    11/01/2022 [RDA/PRODUCT] Hapus tab voucher manual
    11/01/2022 [RDA/PRODUCT] Replace field Location menjadi Site (mandatory) pada Master Asset
    11/01/2022 [RDA/PRODUCT] Opsi pilihan depreciation method otomatis berganti menyesuaikan asset category yang dipilih
    13/01/2022 [RDA/PRODUCT] mengubah tab recvvd menjadi do to department
    13/01/2022 [RDA/PRODUCT] validasi untuk data parsial yang dipilih ketika insert item pada tab do to department
    13/01/2022 [RDA/PRODUCT] asset value terkalkulasi otomatis berdasarkan total pada kolom capitalized amount
    13/01/2022 [RDA/PRODUCT] quantity used untuk tab do to department menjadi editable
    21/01/2022 [RDA/PRODUCT] journal untuk Master Asset ketika item active dan inactive
    07/02/2022 [ICA/PRODUCT] Pada field annual depreciation rate yg muncul bentuk persentase
    10/02/2022 [DITA/PRODUCT] Qty used belum tervalidasi saat proses save
    29/03/2022 [IBL/PRODUCT] BUG : saat nonaktifkan dokumen muncul warning validasi
                             Pada saat edit, item list di tab DO To Customer tidak bisa dihapus.
    30/03/2022 [ISD/PRODUCT] Asset Value dibuat non-editable
    04/07/2022 [RDA/PRODUCT] perubahan pada field ketika penarikan atau insert data item non depreciable
    04/07/2022 [RDA/PRODUCT] penambahan tab Recondition of Asset
    05/07/2022 [RDA/PRODUCT] initial asset sudah bisa di find dan show di master asset
    05/07/2022 [IBL/PRODUCT] Ubah get MaxAssetCode di GenerateAssetCode(string Tbl) sbg impact dari Initial Asset yg disimpan di tbl yg sama dg Asset. (DocAbbr = IAS)
    07/07/2022 [IBL/PRODUCT] Source kolom Economic life on recondition date di tab Recondition of Assetdiubah ke Economic Life After Recondition (sebelumnya Economic Life on Current Date)
    27/07/2022 [RDA/PRODUCT] pada tab Recondition of Asset, RoA yang muncul hanya yang cancel ind = N
    18/08/2022 [MYA/PRODUCT] Membuat validasi Departement, Cost Center, Item's Category dan COA berdasarkan apa yang telah terpasang di Group (system tools)
    13/09/2022 [IBL/PRODUCT] EcoLife Month di header tidak boleh decimal.
    18/11/2022 [IBL/PRODUCT] Site tidak muncul, saat frmasset2 dipanggil dari frm lain
    10/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
    12/01/2023 [DITA/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    02/02/2023 [WED/BBT] tambah informasi kolom Remark dari DODeptDtl
    16/02/2023 [MAU/BBT] BUG : tidak bisa delete DO Dept yg sudah di tarik
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmAsset2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mAssetCode = string.Empty, mFormulaAssetCodeBasedOnCategory = string.Empty;
        internal FrmAsset2Find FrmFind;
        private bool
            mIsAssetNameUnique = false,
            mIsMandatoryAssetFieldEditable = false,
            mIsAssetCodeBasedOnCategoryEnabled = false,
            mIsAssetCategoryMandatoryEnabled = false,
            mIsRecvForAssetShowRateInfo = false;
        internal bool
            mIsCOAUseAlias = false,
            mIsAssetShowAdditionalInformation = false,
            mIsAssetRecvQtyEditable = false,
            mIsRecvForAssetShowPOLocalDocNo = false,
            mIsFilterByCC = false,
            mIsFilterByItCt = false,
            mIsCOAFilteredByGroup = false,
            mIsFilterByDept = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false
            ;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty,
            mLengthAssetCode = string.Empty,
            mMaxPercentageOfResidualValue = string.Empty,
            mAssetCategoryCodeForNonDepreciable = string.Empty,
            mDepreciationMethodForNonDepreciable = string.Empty,
            mJournalDocNoFormat = string.Empty
        ;
        private byte[] downloadedData;

        internal decimal OutstandingQtyDlg = 0m;

        internal List<DoDeptItem> ListDoDeptItem = new List<DoDeptItem>();

        #endregion

        #region Constructor

        public FrmAsset2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Asset";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetGrd();
                SetFormControl(mState.View);
                GetParameter();
                if (!mIsAssetShowAdditionalInformation) TcAsset.TabPages.Remove(TpAdditional);
                TcAsset.SelectedTabPage = TpAdditional;
                Sl.SetLueOption(ref LueClassification, "AssetClassification");
                Sl.SetLueOption(ref LueSubClassification, "AssetSubClassification");
                Sl.SetLueOption(ref LueType, "AssetType");
                Sl.SetLueOption(ref LueSubType, "AssetSubType");
                Sl.SetLueOption(ref LueLocation, "AssetLocation");
                Sl.SetLueOption(ref LueSubLocation, "AssetSubLocation");
                Sl.SetLueOption(ref LueLocation2, "AssetLocation2");
                Sl.SetLueSiteCode(ref LueSiteCode);

                TcAsset.SelectedTabPage = TpGeneral;
                SetLueCCCode(ref LueCC);
                SetLueAssetCategoryCode(ref LueAssetCategory);
                SetLueDepreciationCode(ref LueDepreciationCode);
                Sl.SetLueSiteCode(ref LueSiteCode2);
                Sl.SetLueUomCode(new List<DevExpress.XtraEditors.LookUpEdit> {
                    LueLengthUomCode, LueHeightUomCode, LueWidthUomCode,
                    LueVolumeUomCode, LueDiameterUomCode, LueWideUomCode
                    });
                TcAsset.TabPages.Remove(TpVRManual);

                LblAssetCategoryCode.ForeColor = Color.Red;
                if (mAssetCode.Length != 0)
                {
                    ShowData(mAssetCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In ( ");
            SQL.AppendLine("'IsAssetNameUnique', 'IsMandatoryAssetFieldEditable', 'IsCOAUseAlias', 'IsAssetCodeBasedOnCategoryEnabled', ");
            SQL.AppendLine("'IsAssetCategoryMandatoryEnabled', 'FormulaAssetCodeBasedOnCategory', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', ");
            SQL.AppendLine("'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', 'FileSizeMaxUploadFTPClient', ");
            SQL.AppendLine("'IsAssetShowAdditionalInformation', 'LengthAssetCode', 'IsAssetRecvQtyEditable', 'IsRecvForAssetShowRateInfo', ");
            SQL.AppendLine("'IsRecvForAssetShowPOLocalDocNo', 'MaxPercentageOfResidualValue', 'AssetCategoryCodeForNonDepreciable', ");
            SQL.AppendLine("'DepreciationMethodForNonDepreciable', 'IsFilterByCC', 'IsFilterByItCt', 'IsCOAFilteredByGroup', 'IsFilterByDept', ");
            SQL.AppendLine("'JournalDocNoFormat', 'IsClosingJournalBasedOnMultiProfitCenter' ");
            SQL.AppendLine("); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAssetNameUnique": mIsAssetNameUnique = ParValue == "Y"; break;
                            case "IsMandatoryAssetFieldEditable": mIsMandatoryAssetFieldEditable = ParValue == "Y"; break;
                            case "IsCOAUseAlias": mIsCOAUseAlias = ParValue == "Y"; break;
                            case "IsAssetCodeBasedOnCategoryEnabled": mIsAssetCodeBasedOnCategoryEnabled = ParValue == "Y"; break;
                            case "IsAssetCategoryMandatoryEnabled": mIsAssetCategoryMandatoryEnabled = ParValue == "Y"; break;
                            case "IsAssetShowAdditionalInformation": mIsAssetShowAdditionalInformation = ParValue == "Y"; break;
                            case "IsAssetRecvQtyEditable": mIsAssetRecvQtyEditable = ParValue == "Y"; break;
                            case "IsRecvForAssetShowRateInfo": mIsRecvForAssetShowRateInfo = ParValue == "Y"; break;
                            case "IsRecvForAssetShowPOLocalDocNo": mIsRecvForAssetShowPOLocalDocNo = ParValue == "Y"; break;
                            case "IsFilterByCC": mIsFilterByCC = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsCOAFilteredByGroup": mIsCOAFilteredByGroup = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;

                            //string
                            case "FormulaAssetCodeBasedOnCategory": mFormulaAssetCodeBasedOnCategory = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "LengthAssetCode": mLengthAssetCode = ParValue; break;
                            case "MaxPercentageOfResidualValue": mMaxPercentageOfResidualValue = ParValue; break;
                            case "AssetCategoryCodeForNonDepreciable": mAssetCategoryCodeForNonDepreciable = ParValue; break;
                            case "DepreciationMethodForNonDepreciable": mDepreciationMethodForNonDepreciable = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mLengthAssetCode.Length == 0) mLengthAssetCode = "4";
            if (mMaxPercentageOfResidualValue.Length == 0) mMaxPercentageOfResidualValue = "0";
            if (mAssetCategoryCodeForNonDepreciable.Length == 0) mAssetCategoryCodeForNonDepreciable = "0";
            if (mDepreciationMethodForNonDepreciable.Length == 0) mDepreciationMethodForNonDepreciable = "0";
            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        private void SetGrd()
        {
            #region Grid 1 (DO to Department)

            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "DNo",

                    //1-5
                    "",
                    "Document#",
                    "",
                    "PO's Local",
                    "Date",

                    //6-10
                    "Item's Code",
                    "Item's Name",
                    "Quantity",
                    "UoM",
                    "Currency",

                    //11-15
                    "Amount",
                    "PO",
                    "Rate",
                    "Amount IDR",
                    "Unit Price",

                    //16-19
                    "Quantity Used",
                    "Capitalized Amount",
                    "Outstanding"+Environment.NewLine+"Quantity",
                    "Remark"
                },
                 new int[]
                {
                    //0
                    20,

                    //1-5
                    20, 180, 20, 180, 80, 
                    
                    //6-10
                    100, 200, 100, 90, 80, 

                    //11-15
                    120, 0, 120, 120, 120,

                    //16-19
                    120, 120, 120, 200
                }
            );

            if (mIsAssetRecvQtyEditable)
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 9, 10, 11, 12 });
            else
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 11, 13, 14, 15, 16, 17, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 6 }, false);
            //if (mIsRecvForAssetShowRateInfo)
            Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, mIsRecvForAssetShowRateInfo);
            //else
            //  Sm.GrdColInvisible(Grd1, new int[] { 12, 13 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 13, 14, 15, 17, 18, 19 });
            if (!mIsRecvForAssetShowPOLocalDocNo)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            }

            Grd1.Cols[9].Move(8);
            Grd1.Cols[15].Move(11);
            Grd1.Cols[18].Move(15);


            #endregion

            #region Grid 2 (VR Manual)

            Grd2.Cols.Count = 6;
            Grd2.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "", 
                    
                    //1-5
                    "Voucher Request#",
                    "",
                    "Date",
                    "Currency",
                    "Amount"
                },
                new int[]
                {
                    20,
                    180, 20, 80, 80, 120
                }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdFormatDec(Grd2, new int[] { 5 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0, 2 });
            Sm.GrdColInvisible(Grd2, new int[] { 2 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 3, 4, 5 });

            #endregion

            #region Grid 3 (Recondition of Asset Ref.)

            Grd3.Cols.Count = 10;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date of Recondition",
                    "Asset's Value"+Environment.NewLine+" on Acquisition Date",
                    "Asset's Acc. Depr"+Environment.NewLine+" on Recondition Date",

                    //6-9
                    "Recondition Value",
                    "Economic Life"+Environment.NewLine+" on Acquisition Date",
                    "Economic Life"+Environment.NewLine+" on Recondition Date",
                    "Recondition Reason",
                },
                 new int[]
                {
                    //0
                    30,

                    //1-5
                    150, 20, 130, 150, 150, 
                    
                    //6-9
                    150, 150, 150, 180
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 2 });
            Sm.GrdFormatDate(Grd3, new int[] { 3 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 5, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9 });
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtAssetCode, DteAssetDt, TxtAssetName, MeeDisabledReason, TxtItCode,
                        TxtItName, TxtEcoLifeMth, TxtAssetValue, TxtEcoLifeYr, LueCC,
                        LueDepreciationCode, TxtPercentageAnnualDepreciation, TxtDisplayName, LueAssetCategory, TxtLength,
                        TxtHeight, TxtWidth, TxtVolume, TxtDiameter, TxtWide,
                        LueLengthUomCode, LueHeightUomCode, LueWidthUomCode, LueVolumeUomCode, LueDiameterUomCode,
                        LueWideUomCode, TxtShortCode, LueParent, TxtFile, ChkFile,
                        LueClassification, LueSubClassification, LueType, LueSubType,
                        LueLocation, LueSubLocation, LueLocation2, LueSiteCode, TxtResidualValue, LueSiteCode2
                    }, true);
                    ChkAssetType.Properties.ReadOnly = true;
                    ChkActiveInd.Properties.ReadOnly = true;
                    ChkLeasingInd.Properties.ReadOnly = true;
                    ChkRentedInd.Properties.ReadOnly = true;
                    ChkSoldInd.Properties.ReadOnly = true;
                    ChkFiskalInd.Properties.ReadOnly = true;
                    BtnItCode.Enabled = false;
                    BtnAcNo.Enabled = false;
                    BtnAcNo2.Enabled = false;
                    TxtAssetCode.Focus();
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    if (TxtAssetCode.Text.Length > 0)
                        BtnDownload.Enabled = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteAssetDt, TxtAssetName, TxtEcoLifeYr, LueCC,
                        LueDepreciationCode, TxtDisplayName, LueAssetCategory,
                        TxtLength, TxtHeight, TxtWidth, TxtVolume, TxtDiameter,
                        TxtWide, LueLengthUomCode, LueHeightUomCode, LueWidthUomCode, LueVolumeUomCode,
                        LueDiameterUomCode, LueWideUomCode, TxtShortCode, LueParent, ChkFile,
                        LueClassification, LueSubClassification, LueType, LueSubType,
                        LueLocation, LueSubLocation, LueLocation2, LueSiteCode, TxtResidualValue, LueSiteCode2
                    }, false);
                    ChkAssetType.Properties.ReadOnly = false;
                    ChkLeasingInd.Properties.ReadOnly = false;
                    ChkActiveInd.Checked = true;
                    ChkRentedInd.Properties.ReadOnly = false;
                    ChkSoldInd.Properties.ReadOnly = false;
                    ChkFiskalInd.Properties.ReadOnly = false;
                    DteAssetDt.Focus();
                    BtnItCode.Enabled = true;
                    BtnAcNo.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    BtnFile.Enabled = true;
                    BtnDownload.Enabled = false;
                    break;
                case mState.Edit:
                    if (!Sm.Find_In_Set(Sm.GetLue(LueAssetCategory), mAssetCategoryCodeForNonDepreciable))
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            TxtDisplayName, LueAssetCategory, TxtLength, TxtHeight, TxtWidth,
                            TxtVolume, TxtDiameter, TxtWide, LueLengthUomCode, LueHeightUomCode,
                            LueWidthUomCode, LueVolumeUomCode, LueDiameterUomCode, LueWideUomCode, TxtShortCode,
                            LueParent, ChkFile,
                            LueClassification, LueSubClassification, LueType, LueSubType,
                            LueLocation, LueSubLocation, LueLocation2, LueSiteCode, LueSiteCode2
                        }, false);
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeDisabledReason }, ChkActiveInd.Checked);
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCC }, (Sm.GetLue(LueCC).Length > 0));
                        ChkActiveInd.Properties.ReadOnly = false;
                        ChkAssetType.Properties.ReadOnly = false;
                        ChkLeasingInd.Properties.ReadOnly = false;
                        ChkRentedInd.Properties.ReadOnly = false;
                        ChkSoldInd.Properties.ReadOnly = false;
                        ChkFiskalInd.Properties.ReadOnly = false;
                        DteAssetDt.Focus();
                        BtnAcNo.Enabled = true;
                        BtnAcNo2.Enabled = true;
                        Grd1.ReadOnly = true;
                        Grd2.ReadOnly = false;
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = false;

                        if (mIsMandatoryAssetFieldEditable)
                        {
                            if (!IsAssetDepreciated())
                            {
                                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                            {
                                TxtAssetName, DteAssetDt, LueDepreciationCode, LueCC
                            }, false);
                                BtnItCode.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            TxtAssetCode, DteAssetDt, TxtAssetName, MeeDisabledReason, TxtItCode,
                            TxtItName, TxtEcoLifeMth, TxtAssetValue, TxtEcoLifeYr, LueCC,
                            LueDepreciationCode, TxtPercentageAnnualDepreciation, TxtDisplayName, LueAssetCategory, TxtLength,
                            TxtHeight, TxtWidth, TxtVolume, TxtDiameter, TxtWide,
                            LueLengthUomCode, LueHeightUomCode, LueWidthUomCode, LueVolumeUomCode, LueDiameterUomCode,
                            LueWideUomCode, TxtShortCode, LueParent, TxtFile, ChkFile,
                            LueClassification, LueSubClassification, LueType, LueSubType,
                            LueLocation, LueSubLocation, LueLocation2, LueSiteCode, TxtResidualValue, LueSiteCode2
                        }, true);
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeDisabledReason }, ChkActiveInd.Checked);
                        ChkAssetType.Properties.ReadOnly = true;
                        ChkActiveInd.Properties.ReadOnly = false;
                        ChkLeasingInd.Properties.ReadOnly = true;
                        ChkRentedInd.Properties.ReadOnly = true;
                        ChkSoldInd.Properties.ReadOnly = true;
                        ChkFiskalInd.Properties.ReadOnly = true;
                        BtnItCode.Enabled = false;
                        BtnAcNo.Enabled = false;
                        BtnAcNo2.Enabled = false;
                        TxtAssetCode.Focus();
                        Grd1.ReadOnly = true;
                        Grd2.ReadOnly = true;
                        BtnFile.Enabled = false;
                        BtnDownload.Enabled = false;
                    }

                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
               TxtAssetCode, DteAssetDt, TxtAssetName, LueParent, MeeDisabledReason,
               TxtItCode, TxtItName, LueAssetCategory, TxtDisplayName, LueCC,
               LueDepreciationCode, TxtAcNo, TxtAcDesc, TxtAcNo2, TxtAcDesc2,
               LueLengthUomCode, LueHeightUomCode, LueDiameterUomCode, LueWidthUomCode, LueVolumeUomCode,
               LueWideUomCode, TxtShortCode, TxtFile,
               LueClassification, LueSubClassification, LueType, LueSubType,
               LueLocation, LueSubLocation, LueLocation2, LueSiteCode, LueSiteCode2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtAssetValue, TxtEcoLifeYr, TxtEcoLifeMth, TxtPercentageAnnualDepreciation, TxtLength,
                TxtHeight, TxtWidth, TxtVolume, TxtDiameter, TxtWide, TxtResidualValue
            }, 0);
            ChkActiveInd.Checked = false;
            ChkAssetType.Checked = false;
            ChkLeasingInd.Checked = false;
            ChkRentedInd.Checked = false;
            ChkSoldInd.Checked = false;
            ChkFiskalInd.Checked = false;
            PbUpload.Value = 0;
            ChkFile.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 11, 13, 14, 15, 16, 17, 18 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5 });
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 5, 6, 7, 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAsset2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteAssetDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sl.SetLueAssetCode(ref LueParent, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAssetCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtAssetCode, "", false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblAsset Where AssetCode=@AssetCode" };
                Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtAssetCode.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "Image Files(*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Grid Methods

        private void Grd1_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                InsertDataListDoDeptItem();
                Sm.FormShowDialog(new FrmAsset2Dlg5(this));
            }
 
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmDODept2(mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.Tag = mMenuCode;
                f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmDODept2' Limit 1;");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmAsset2Dlg5(this));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmDODept2(mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.Tag = mMenuCode;
                f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmDODept2' Limit 1;");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f1.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmAsset2Dlg4(this));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmVoucherRequest(mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f1.Tag = mMenuCode;
                f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVoucherRequest' Limit 1;");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }

        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmAsset2Dlg4(this));
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmVoucherRequest(mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f1.Tag = mMenuCode;
                f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVoucherRequest' Limit 1;");
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 16)
            {
                if (Sm.GetGrdDec(Grd1, e.RowIndex, 18) >= Sm.GetGrdDec(Grd1, e.RowIndex, 16))
                {
                    ComputeCapitalizedAmt(Grd1, e.RowIndex);
                }
                else
                {
                    Sm.StdMsg(mMsgType.Warning,
                        " Document# : " + Sm.GetGrdStr(Grd1, e.RowIndex, 2) +
                        "\n Item Name : " + Sm.GetGrdStr(Grd1, e.RowIndex, 7) +
                        "\n Quantity Used is more than Outstanding Quantity "

                        );
                }
            }

            decimal SumCapitalAmt = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    SumCapitalAmt += Sm.GetGrdDec(Grd1, Row, 17);
                }
            }

            TxtAssetValue.Text = Convert.ToString(SumCapitalAmt);
            Sm.FormatNumTxt(TxtAssetValue, 0);
        }

        private void Grd3_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmReconditionAsset(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string AssetCode = string.Empty;
                var code1 = Sm.GetCode1ForJournalDocNo("FrmAsset2", string.Empty, string.Empty, mJournalDocNoFormat);
                var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param", Sm.GetLue(LueCC));

                if (TxtAssetCode.Text.Length == 0)
                {
                    if (mIsAssetCodeBasedOnCategoryEnabled)
                        AssetCode = GenerateAssetCode();
                    else
                        AssetCode = GenerateAssetCode("TblAsset");
                }
                else
                    AssetCode = TxtAssetCode.Text;

                var cml = new List<MySqlCommand>();
                var cml2 = new List<MySqlCommand>();

                cml.Add(SaveAsset(AssetCode));

                if (Grd1.Rows.Count > 1)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                        cml.Add(SaveAssetDtl(AssetCode, i));
                }

                if (Grd2.Rows.Count > 1)
                {
                    for (int i = 0; i < Grd2.Rows.Count - 1; i++)
                        cml.Add(SaveAssetDtl2(AssetCode, i));
                }

                Sm.ExecCommands(cml);

                cml2.Add(SaveJournal(AssetCode, code1, profitCenterCode));
                Sm.ExecCommands(cml2);

                if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                    UploadFile(AssetCode);

                ShowData(AssetCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private MySqlCommand SaveAsset(string AssetCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAsset(AssetCode, ItCode, AssetName, Parent, DisplayName, ShortCode, AssetType, ActiveInd, LeasingInd, SoldInd, RentedInd, AssetDt, AssetValue, ResidualValue, EcoLifeYr, EcoLife, DepreciationCode, PercentageAnnualDepreciation, CCCode, AcNo, AcNo2, AssetCategoryCode, ");
            SQL.AppendLine("Length, Height, Width, Volume, Diameter, Wide, ");
            SQL.AppendLine("LengthUomCode, HeightUomCode, DiameterUomCode, WidthUomCode, VolumeUomCode, WideUomCode, FiskalInd, Location,  ");
            SQL.AppendLine("Classification, SubClassification, Type, SubType, Location2, SubLocation, Location3, SiteCode, SiteCode2, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AssetCode, @ItCode, @AssetName, @Parent, @DisplayName, @ShortCode, @AssetType, @ActiveInd, @LeasingInd, @SoldInd, @RentedInd, @AssetDt, @AssetValue, @ResidualValue, @EcoLifeYr, @EcoLife, @DepreciationCode, @PercentageAnnualDepreciation, @CCCode, @AcNo, @AcNo2, @AssetCategoryCode, ");
            SQL.AppendLine("@Length, @Height, @Width, @Volume, @Diameter, @Wide, ");
            SQL.AppendLine("@LengthUomCode, @HeightUomCode, @DiameterUomCode, @WidthUomCode, @VolumeUomCode, @WideUomCode, @FiskalInd , @Location,  ");
            SQL.AppendLine("@Classification, @SubClassification, @Type, @SubType, @Location2, @SubLocation, @Location3, @SiteCode, @SiteCode2, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblAssetSummary(AssetCode, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("    Values (@AssetCode, @CCCode, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update CCCode = @CCCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@AssetName", TxtAssetName.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@AssetType", ChkAssetType.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LeasingInd", ChkLeasingInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SoldInd", ChkSoldInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@RentedInd", ChkRentedInd.Checked ? "Y" : "N");
            Sm.CmParamDt(ref cm, "@AssetDt", Sm.GetDte(DteAssetDt));
            Sm.CmParam<Decimal>(ref cm, "@AssetValue", Decimal.Parse(TxtAssetValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@ResidualValue", Decimal.Parse(TxtResidualValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLifeYr", Decimal.Parse(TxtEcoLifeYr.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLife", Decimal.Parse(TxtEcoLifeMth.Text));
            Sm.CmParam<String>(ref cm, "@DepreciationCode", Sm.GetLue(LueDepreciationCode));
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", Decimal.Parse(TxtPercentageAnnualDepreciation.Text));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCC));
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
            Sm.CmParam<String>(ref cm, "@AssetCategoryCode", Sm.GetLue(LueAssetCategory));
            Sm.CmParam<Decimal>(ref cm, "@Length", Decimal.Parse(TxtLength.Text));
            Sm.CmParam<Decimal>(ref cm, "@Height", Decimal.Parse(TxtHeight.Text));
            Sm.CmParam<Decimal>(ref cm, "@Width", Decimal.Parse(TxtWidth.Text));
            Sm.CmParam<Decimal>(ref cm, "@Volume", Decimal.Parse(TxtVolume.Text));
            Sm.CmParam<Decimal>(ref cm, "@Diameter", Decimal.Parse(TxtDiameter.Text));
            Sm.CmParam<Decimal>(ref cm, "@Wide", Decimal.Parse(TxtWide.Text));
            Sm.CmParam<String>(ref cm, "@LengthUomCode", Sm.GetLue(LueLengthUomCode));
            Sm.CmParam<String>(ref cm, "@HeightUomCode", Sm.GetLue(LueHeightUomCode));
            Sm.CmParam<String>(ref cm, "@WidthUomCode", Sm.GetLue(LueWidthUomCode));
            Sm.CmParam<String>(ref cm, "@VolumeUomCode", Sm.GetLue(LueVolumeUomCode));
            Sm.CmParam<String>(ref cm, "@DiameterUomCode", Sm.GetLue(LueDiameterUomCode));
            Sm.CmParam<String>(ref cm, "@WideUomCode", Sm.GetLue(LueWideUomCode));
            //Sm.CmParam<String>(ref cm, "@Location", TxtLocation.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@FiskalInd", ChkFiskalInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Classification", Sm.GetLue(LueClassification));
            Sm.CmParam<String>(ref cm, "@SubClassification", Sm.GetLue(LueSubClassification));
            Sm.CmParam<String>(ref cm, "@Type", Sm.GetLue(LueType));
            Sm.CmParam<String>(ref cm, "@SubType", Sm.GetLue(LueSubType));
            Sm.CmParam<String>(ref cm, "@Location2", Sm.GetLue(LueLocation));
            Sm.CmParam<String>(ref cm, "@SubLocation", Sm.GetLue(LueSubLocation));
            Sm.CmParam<String>(ref cm, "@Location3", Sm.GetLue(LueLocation2));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@SiteCode2", Sm.GetLue(LueSiteCode2));
            string cek = Sm.GetLue(LueSiteCode2);

            return cm;
        }

        private MySqlCommand SaveAssetDtl(string AssetCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAssetDtl(AssetCode, AssetDNo, RecvVdDocNo, RecvVdDNo, ");
            SQL.AppendLine("OutstandingQty, Qty, ");
            SQL.AppendLine("CreateBy, CreateDt)");
            SQL.AppendLine("Values(@AssetCode, @AssetDNo, @RecvVdDocNo, @RecvVdDNo,  ");
            SQL.AppendLine("@OutstandingQty, @Qty, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.CmParam<String>(ref cm, "@AssetDNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@RecvVdDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@RecvVdDNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@OutstandingQty", Sm.GetGrdDec(Grd1, Row, 18) - Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 16));

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAssetDtl2(string AssetCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAssetDtl2(AssetCode, AssetDNo, VRDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AssetCode, @AssetDNo, @VRDocNo, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set AssetCode = @AssetCode, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @VRDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.CmParam<String>(ref cm, "@AssetDNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@VRDocNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
            SQL.AppendLine("CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DocDt, ");
            SQL.AppendLine("Concat('Master Asset (Product) : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CCCode, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo as DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, NULL As EntCode, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, DAmt, CAmt From (");

            //debit
            SQL.AppendLine("        SELECT A.AcNo AS AcNo, ");
            SQL.AppendLine("        A.AssetValue As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        FROM TblAsset A ");
            SQL.AppendLine("        WHERE A.AssetCode = @AssetCode ");

            SQL.AppendLine("        UNION ALL ");

            //credit
            SQL.AppendLine("        SELECT G.AcNo AS AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        B.Qty * E.UPrice As CAmt ");
            SQL.AppendLine("        FROM tblasset A ");
            SQL.AppendLine("        INNER JOIN tblassetdtl B ON A.AssetCode = B.AssetCode ");
            SQL.AppendLine("        INNER JOIN tbldodeptdtl C ON B.RecvVdDocNo = C.DocNo AND B.RecvVdDNo = C.DNo ");
            SQL.AppendLine("        INNER JOIN tbldodepthdr D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        INNER JOIN tblstockprice E ON C.Source = E.Source ");
            SQL.AppendLine("        INNER JOIN tblitemcostcategory F ON C.ItCode = F.ItCode AND D.CCCode = F.CCCode ");
            SQL.AppendLine("        INNER JOIN tblcostcategory G ON F.CCtCode = G.CCtCode AND F.CCCode = G.CCCode ");
            SQL.AppendLine("        INNER JOIN tblitem H ON C.ItCode = H.ItCode AND H.ActInd = 'Y' ");
            SQL.AppendLine("        WHERE A.AssetCode = @AssetCode ");

            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine(") T;  ");

            SQL.AppendLine("Update TblAsset Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where AssetCode=@DocNo ");
            SQL.AppendLine("And JournalDocNo is Null ");
            SQL.AppendLine("And ActiveInd='Y';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mJournalDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteAssetDt), 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteAssetDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteAssetDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@AssetCode", DocNo);
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCC));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo, string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();

            var IsClosingJournalUseCurrentDt = mIsClosingJournalBasedOnMultiProfitCenter ? Sm.IsClosingJournalUseCurrentDt(Sm.GetDte(DteAssetDt), GetProfitCenterCode()) : Sm.IsClosingJournalUseCurrentDt(Sm.GetDte(DteAssetDt));


            SQL.AppendLine("Update TblAsset Set ");
            SQL.AppendLine("    ActiveInd='N' ");
            SQL.AppendLine("Where AssetCode=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And ActiveInd='Y';");

            SQL.AppendLine("UPDATE tbljournalhdr SET ");
            SQL.AppendLine("    CancelInd = 'Y', ");
            SQL.AppendLine("    CancelReason = @CancelReason, ");
            SQL.AppendLine("    JournalDocNo = @JournalDocNo ");
            SQL.AppendLine("WHERE DocNo = @JournalDocNoAsset ");
            SQL.AppendLine("AND CancelInd = 'N' ");
            SQL.AppendLine("AND JournalDocNo IS NULL; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo,   ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblAsset ");
            SQL.AppendLine("    Where AssetCode=@DocNo ");
            SQL.AppendLine("    And ActiveInd='N' ");
            SQL.AppendLine("    And JournalDocNo is Not Null ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, ");
            SQL.AppendLine("EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblAsset ");
            SQL.AppendLine("    Where AssetCode=@DocNo ");
            SQL.AppendLine("    And ActiveInd='N' ");
            SQL.AppendLine("    And JournalDocNo is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mJournalDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteAssetDt), 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteAssetDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeDisabledReason.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNoAsset", Sm.GetValue("Select JournalDocNo From TblAsset Where AssetCode = @Param", DocNo));

            return cm;
        }

        private void EditData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

                string mDepreciationDocNo = GetDepreciationDocNo();
                var code1 = Sm.GetCode1ForJournalDocNo("FrmAsset2", string.Empty, string.Empty, mJournalDocNoFormat);
                var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param", Sm.GetLue(LueCC));
                
                if (mIsMandatoryAssetFieldEditable)
                {
                    if (mDepreciationDocNo.Length > 0)
                    {
                        var mMsgs = new StringBuilder();

                        mMsgs.AppendLine("This asset is depreciated in : " + mDepreciationDocNo);
                        mMsgs.AppendLine("Therefore, the mandatory information will not be updated.");
                        mMsgs.AppendLine("Do you want to proceed ? ");

                        if (Sm.StdMsgYN("Question", mMsgs.ToString()) == DialogResult.No) return;
                    }
                }

                Cursor.Current = Cursors.WaitCursor;

                var AssetCode = TxtAssetCode.Text;
                var cml = new List<MySqlCommand>();
                var cml2 = new List<MySqlCommand>();

                cml.Add(UpdateAsset(mDepreciationDocNo));
                cml.Add(UpdateRecvVdAndVR(AssetCode));
                cml.Add(DeleteAssetDtl(AssetCode));

                /*Comment By IBL
                //if (Grd1.Rows.Count > 1)
                //{
                //    for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                //    {
                //        cml.Add(SaveAssetDtl(AssetCode, i));
                //    }
                //}
                */

                if (Grd2.Rows.Count > 1)
                {
                    for (int i = 0; i < Grd2.Rows.Count - 1; i++)
                    {
                        cml.Add(SaveAssetDtl2(AssetCode, i));
                    }
                }

                Sm.ExecCommands(cml);
                if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                    UploadFile(AssetCode);

                if (ChkActiveInd.Checked == false)
                {
                    cml2.Add(SaveJournal2(AssetCode, code1, profitCenterCode));
                    Sm.ExecCommands(cml2);
                }

                ShowData(AssetCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsEditedDataNotValid()
        {
            return
                (!ChkActiveInd.Checked && Sm.IsMeeEmpty(MeeDisabledReason, "Disabled reason")) ||
                Sm.IsTxtEmpty(TxtItCode, "Item Code", false) ||
                Sm.IsTxtEmpty(TxtItName, "Item Name", false) ||
                Sm.IsDteEmpty(DteAssetDt, "Date Of Purchase") ||
                Sm.IsTxtEmpty(TxtAssetName, "Asset name", false) ||
                Sm.IsLueEmpty(LueDepreciationCode, "Depreciation method") ||
                Sm.IsLueEmpty(LueCC, "Initial cost center") ||
                Sm.IsLueEmpty(LueSiteCode2, "Site") ||
                Sm.IsLueEmpty(LueAssetCategory, "Asset's category") ||
                IsAssetNameNotUnique() ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, true, Sm.GetDte(DteAssetDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(true, true, Sm.GetDte(DteAssetDt))) ||
                IsUploadFileNotValid() ||
                IsResidualValueNotValid() ||
                IsGrdEmpty() ||
                Sm.IsTxtEmpty(TxtAcNo, "COA's Account#", false) ||
                IsAssetAlreadyCancelled() ||
                IsTOAvailable()
                ;
        }

        private bool IsAssetAlreadyProcessedToDBA()
        {
            return
                Sm.IsDataExist("Select 1 From TblDepreciationAssetHdr Where AssetCode = @Param And CancelInd = 'N' Limit 1;", TxtAssetCode.Text);
        }

        private bool IsTOAvailable()
        {
            if (ChkActiveInd.Checked) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblTOHdr ");
            SQL.AppendLine("Where AssetCode = @Param ");
            SQL.AppendLine("And ActInd = 'Y'; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtAssetCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Technical Object for this asset is available.");
                return true;
            }

            return false;
        }

        private MySqlCommand UpdateAsset(string DepreciationDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAssetSummary(AssetCode, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@AssetCode, @CCCode, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update CCCode=Case When CCCode Is Null Then @CCCode Else CCCode End, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (mIsMandatoryAssetFieldEditable)
            {
                if (DepreciationDocNo.Length > 0)
                {
                    SQL.AppendLine("Update TblAsset Set ");
                    //SQL.AppendLine("    AssetName=@AssetName, ");
                    //SQL.AppendLine("    ItCode=@ItCode, ");
                    SQL.AppendLine("    DisplayName=@DisplayName, ");
                    SQL.AppendLine("    Parent=@Parent, ");
                    SQL.AppendLine("    ShortCode=@ShortCode, ");
                    SQL.AppendLine("    DisabledReason=@DisabledReason, ");
                    SQL.AppendLine("    AssetType=@AssetType, ");
                    SQL.AppendLine("    ActiveInd=@ActiveInd, ");
                    SQL.AppendLine("    LeasingInd=@LeasingInd, ");
                    SQL.AppendLine("    SoldInd=@SoldInd, ");
                    SQL.AppendLine("    FiskalInd=@FiskalInd, ");
                    SQL.AppendLine("    RentedInd=@RentedInd, ");
                    //SQL.AppendLine("    AssetDt=@AssetDt, ");
                    SQL.AppendLine("    AssetValue = @AssetValue, ");
                    SQL.AppendLine("    EcoLifeYr=@EcoLifeYr, ");
                    SQL.AppendLine("    EcoLife=@EcoLife, ");
                    //SQL.AppendLine("    DepreciationCode=@DepreciationCode, ");
                    SQL.AppendLine("    PercentageAnnualDepreciation=@PercentageAnnualDepreciation, ");
                    //SQL.AppendLine("    CCCode=Case When CCCode Is Null Then @CCCode Else CCCode End, ");
                    SQL.AppendLine("    AcNo=@AcNo, ");
                    SQL.AppendLine("    AcNo2=@AcNo2, ");
                    SQL.AppendLine("    AssetCategoryCode=@AssetCategoryCode, ");
                    SQL.AppendLine("    Length=@Length, ");
                    SQL.AppendLine("    Height=@Height, ");
                    SQL.AppendLine("    Width=@Width, ");
                    SQL.AppendLine("    Volume=@Volume, ");
                    SQL.AppendLine("    Diameter=@Diameter, ");
                    SQL.AppendLine("    Wide=@Wide, ");
                    SQL.AppendLine("    LengthUomCode=@LengthUomCode, ");
                    SQL.AppendLine("    HeightUomCode=@HeightUomCode, ");
                    SQL.AppendLine("    DiameterUomCode=@DiameterUomCode, ");
                    SQL.AppendLine("    WidthUomCode=@WidthUomCode, ");
                    SQL.AppendLine("    VolumeUomCode=@VolumeUomCode, ");
                    SQL.AppendLine("    WideUomCode=@WideUomCode, ");
                    SQL.AppendLine("    Location=@Location, ");
                    SQL.AppendLine("    Classification=@Classification, ");
                    SQL.AppendLine("    SubClassification=@SubClassification, ");
                    SQL.AppendLine("    Type=@Type, ");
                    SQL.AppendLine("    SubType=@SubType, ");
                    SQL.AppendLine("    Location2=@Location2, ");
                    SQL.AppendLine("    SubLocation=@SubLocation, ");
                    SQL.AppendLine("    Location3=@Location3, ");
                    SQL.AppendLine("    SiteCode=@SiteCode, ");
                    SQL.AppendLine("    LastUpBy=@UserCode, ");
                    SQL.AppendLine("    LastUpDt=CurrentDateTime(), ");
                    SQL.AppendLine("    SiteCode2=@SiteCode2 ");
                    SQL.AppendLine("Where AssetCode=@AssetCode; ");
                }
                else
                {
                    SQL.AppendLine("Update TblAsset Set ");
                    SQL.AppendLine("    AssetName=@AssetName, ");
                    SQL.AppendLine("    ItCode=@ItCode, ");
                    SQL.AppendLine("    DisplayName=@DisplayName, ");
                    SQL.AppendLine("    ShortCode=@ShortCode, ");
                    SQL.AppendLine("    DisabledReason=@DisabledReason, ");
                    SQL.AppendLine("    AssetType=@AssetType, ");
                    SQL.AppendLine("    ActiveInd=@ActiveInd, ");
                    SQL.AppendLine("    FiskalInd=@FiskalInd, ");
                    SQL.AppendLine("    LeasingInd=@LeasingInd, ");
                    SQL.AppendLine("    SoldInd=@SoldInd, ");
                    SQL.AppendLine("    RentedInd=@RentedInd, ");
                    SQL.AppendLine("    AssetDt=@AssetDt, ");
                    SQL.AppendLine("    AssetValue = @AssetValue, ");
                    SQL.AppendLine("    EcoLifeYr=@EcoLifeYr, ");
                    SQL.AppendLine("    EcoLife=@EcoLife, ");
                    SQL.AppendLine("    DepreciationCode=@DepreciationCode, ");
                    SQL.AppendLine("    PercentageAnnualDepreciation=@PercentageAnnualDepreciation, ");
                    SQL.AppendLine("    CCCode=Case When CCCode Is Null or CCCode != @CCCode Then @CCCode Else CCCode End, ");
                    SQL.AppendLine("    AcNo=@AcNo, ");
                    SQL.AppendLine("    AcNo2=@AcNo2, ");
                    SQL.AppendLine("    AssetCategoryCode=@AssetCategoryCode, ");
                    SQL.AppendLine("    Length=@Length, ");
                    SQL.AppendLine("    Height=@Height, ");
                    SQL.AppendLine("    Width=@Width, ");
                    SQL.AppendLine("    Volume=@Volume, ");
                    SQL.AppendLine("    Diameter=@Diameter, ");
                    SQL.AppendLine("    Wide=@Wide, ");
                    SQL.AppendLine("    LengthUomCode=@LengthUomCode, ");
                    SQL.AppendLine("    HeightUomCode=@HeightUomCode, ");
                    SQL.AppendLine("    DiameterUomCode=@DiameterUomCode, ");
                    SQL.AppendLine("    WidthUomCode=@WidthUomCode, ");
                    SQL.AppendLine("    VolumeUomCode=@VolumeUomCode, ");
                    SQL.AppendLine("    WideUomCode=@WideUomCode, ");
                    SQL.AppendLine("    Location=@Location, ");
                    SQL.AppendLine("    Classification=@Classification, ");
                    SQL.AppendLine("    SubClassification=@SubClassification, ");
                    SQL.AppendLine("    Type=@Type, ");
                    SQL.AppendLine("    SubType=@SubType, ");
                    SQL.AppendLine("    Location2=@Location2, ");
                    SQL.AppendLine("    SubLocation=@SubLocation, ");
                    SQL.AppendLine("    Location3=@Location3, ");
                    SQL.AppendLine("    SiteCode=@SiteCode, ");
                    SQL.AppendLine("    LastUpBy=@UserCode, ");
                    SQL.AppendLine("    LastUpDt=CurrentDateTime(), ");
                    SQL.AppendLine("    SiteCode2=@SiteCode2 ");
                    SQL.AppendLine("Where AssetCode=@AssetCode; ");
                }
            }
            else
            {
                SQL.AppendLine("Update TblAsset Set ");
                SQL.AppendLine("    AssetName=@AssetName, ");
                //SQL.AppendLine("    ItCode=@ItCode, ");
                SQL.AppendLine("    Parent=@Parent, ");
                SQL.AppendLine("    DisplayName=@DisplayName, ");
                SQL.AppendLine("    ShortCode=@ShortCode, ");
                SQL.AppendLine("    DisabledReason=@DisabledReason, ");
                SQL.AppendLine("    AssetType=@AssetType, ");
                SQL.AppendLine("    ActiveInd=@ActiveInd, ");
                SQL.AppendLine("    FiskalInd=@FiskalInd, ");
                SQL.AppendLine("    LeasingInd=@LeasingInd, ");
                SQL.AppendLine("    SoldInd=@SoldInd, ");
                SQL.AppendLine("    RentedInd=@RentedInd, ");
                SQL.AppendLine("    AssetDt=@AssetDt, ");
                SQL.AppendLine("    AssetValue = @AssetValue, ");
                SQL.AppendLine("    EcoLifeYr=@EcoLifeYr, ");
                SQL.AppendLine("    EcoLife=@EcoLife, ");
                SQL.AppendLine("    DepreciationCode=@DepreciationCode, ");
                SQL.AppendLine("    PercentageAnnualDepreciation=@PercentageAnnualDepreciation, ");
                SQL.AppendLine("    CCCode=Case When CCCode Is Null Then @CCCode Else CCCode End, ");
                SQL.AppendLine("    AcNo=@AcNo, ");
                SQL.AppendLine("    AcNo2=@AcNo2, ");
                SQL.AppendLine("    AssetCategoryCode=@AssetCategoryCode, ");
                SQL.AppendLine("    Length=@Length, ");
                SQL.AppendLine("    Height=@Height, ");
                SQL.AppendLine("    Width=@Width, ");
                SQL.AppendLine("    Volume=@Volume, ");
                SQL.AppendLine("    Diameter=@Diameter, ");
                SQL.AppendLine("    Wide=@Wide, ");
                SQL.AppendLine("    LengthUomCode=@LengthUomCode, ");
                SQL.AppendLine("    HeightUomCode=@HeightUomCode, ");
                SQL.AppendLine("    DiameterUomCode=@DiameterUomCode, ");
                SQL.AppendLine("    WidthUomCode=@WidthUomCode, ");
                SQL.AppendLine("    VolumeUomCode=@VolumeUomCode, ");
                SQL.AppendLine("    WideUomCode=@WideUomCode, ");
                SQL.AppendLine("    Location=@Location, ");
                SQL.AppendLine("    Classification=@Classification, ");
                SQL.AppendLine("    SubClassification=@SubClassification, ");
                SQL.AppendLine("    Type=@Type, ");
                SQL.AppendLine("    SubType=@SubType, ");
                SQL.AppendLine("    Location2=@Location2, ");
                SQL.AppendLine("    SubLocation=@SubLocation, ");
                SQL.AppendLine("    Location3=@Location3, ");
                SQL.AppendLine("    SiteCode=@SiteCode, ");
                SQL.AppendLine("    LastUpBy=@UserCode, ");
                SQL.AppendLine("    LastUpDt=CurrentDateTime(), ");
                SQL.AppendLine("    SiteCode2=@SiteCode2 ");
                SQL.AppendLine("Where AssetCode=@AssetCode; ");
            }

            //SQL.AppendLine("Insert Into TblAssetSummary(AssetCode, CCCode, CreateBy, CreateDt) ");
            //SQL.AppendLine(" Values (@AssetCode, @CCCode, @CreateBy, CurrentDateTime()) ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("    Update CCCode=@CCCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            //SQL.AppendLine("Insert Into TblAssetMovement(DocType, DocNo, DNo, DocDt, AssetCode, CCCode, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("    Values('2', Select Concat('IA', 001 , Left(CurrentDateTime(), 8), @AssetCode, @CCCode, 'Dari Initial Asset', @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssetCode", TxtAssetCode.Text);
            Sm.CmParam<String>(ref cm, "@AssetName", TxtAssetName.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@DisplayName", TxtDisplayName.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@DisabledReason", MeeDisabledReason.Text);
            Sm.CmParam<String>(ref cm, "@AssetType", ChkAssetType.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LeasingInd", ChkLeasingInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SoldInd", ChkSoldInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@RentedInd", ChkRentedInd.Checked ? "Y" : "N");
            Sm.CmParamDt(ref cm, "@AssetDt", Sm.GetDte(DteAssetDt));
            Sm.CmParam<Decimal>(ref cm, "@AssetValue", Decimal.Parse(TxtAssetValue.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLifeYr", Decimal.Parse(TxtEcoLifeYr.Text));
            Sm.CmParam<Decimal>(ref cm, "@EcoLife", Decimal.Parse(TxtEcoLifeMth.Text));
            Sm.CmParam<String>(ref cm, "@DepreciationCode", Sm.GetLue(LueDepreciationCode));
            Sm.CmParam<Decimal>(ref cm, "@PercentageAnnualDepreciation", Decimal.Parse(TxtPercentageAnnualDepreciation.Text));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCC));
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
            Sm.CmParam<String>(ref cm, "@AssetCategoryCode", Sm.GetLue(LueAssetCategory));
            Sm.CmParam<Decimal>(ref cm, "@Length", Decimal.Parse(TxtLength.Text));
            Sm.CmParam<Decimal>(ref cm, "@Height", Decimal.Parse(TxtHeight.Text));
            Sm.CmParam<Decimal>(ref cm, "@Width", Decimal.Parse(TxtWidth.Text));
            Sm.CmParam<Decimal>(ref cm, "@Volume", Decimal.Parse(TxtVolume.Text));
            Sm.CmParam<Decimal>(ref cm, "@Diameter", Decimal.Parse(TxtDiameter.Text));
            Sm.CmParam<Decimal>(ref cm, "@Wide", Decimal.Parse(TxtWide.Text));
            Sm.CmParam<String>(ref cm, "@LengthUomCode", Sm.GetLue(LueLengthUomCode));
            Sm.CmParam<String>(ref cm, "@HeightUomCode", Sm.GetLue(LueHeightUomCode));
            Sm.CmParam<String>(ref cm, "@WidthUomCode", Sm.GetLue(LueWidthUomCode));
            Sm.CmParam<String>(ref cm, "@VolumeUomCode", Sm.GetLue(LueVolumeUomCode));
            Sm.CmParam<String>(ref cm, "@DiameterUomCode", Sm.GetLue(LueDiameterUomCode));
            Sm.CmParam<String>(ref cm, "@WideUomCode", Sm.GetLue(LueWideUomCode));
            //Sm.CmParam<String>(ref cm, "@Location", TxtLocation.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@FiskalInd", ChkFiskalInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Classification", Sm.GetLue(LueClassification));
            Sm.CmParam<String>(ref cm, "@SubClassification", Sm.GetLue(LueSubClassification));
            Sm.CmParam<String>(ref cm, "@Type", Sm.GetLue(LueType));
            Sm.CmParam<String>(ref cm, "@SubType", Sm.GetLue(LueSubType));
            Sm.CmParam<String>(ref cm, "@Location2", Sm.GetLue(LueLocation));
            Sm.CmParam<String>(ref cm, "@SubLocation", Sm.GetLue(LueSubLocation));
            Sm.CmParam<String>(ref cm, "@Location3", Sm.GetLue(LueLocation2));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@SiteCode2", Sm.GetLue(LueSiteCode2));

            return cm;
        }

        private MySqlCommand UpdateRecvVdAndVR(string AssetCode)
        {
            var SQL = new StringBuilder();
            if (!mIsAssetRecvQtyEditable)
            {
                SQL.AppendLine("Update TblRecvVdDtl Set AssetCode = null, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where AssetCode = @AssetCode; ");
            }

            SQL.AppendLine("Update TblVoucherRequestHdr Set AssetCode = null, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where AssetCode = @AssetCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteAssetDtl(string AssetCode)
        {
            var SQL = new StringBuilder();
            //SQL.AppendLine("Delete From TblAssetDtl Where AssetCode = @AssetCode; "); Comment By IBL
            SQL.AppendLine("Delete From TblAssetDtl2 Where AssetCode = @AssetCode; ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            return cm;
        }

        private MySqlCommand UpdateAssetFile(string AssetCode, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAsset Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where AssetCode=@AssetCode  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private bool IsAssetAlreadyCancelled()
        {
            string mMsg = (ChkActiveInd.Checked ? "You cannot activate asset that already not active." : "This asset already not active.");
            return Sm.IsDataExist(
                "Select AssetCode From TblAsset " +
                "Where AssetCode=@Param And ActiveInd='N';",
                TxtAssetCode.Text, mMsg);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                (!ChkActiveInd.Checked && Sm.IsMeeEmpty(MeeDisabledReason, "Disabled reason")) ||
                Sm.IsTxtEmpty(TxtItCode, "Item Code", false) ||
                Sm.IsTxtEmpty(TxtItName, "Item Name", false) ||
                Sm.IsDteEmpty(DteAssetDt, "Date Of Purchase") ||
                Sm.IsTxtEmpty(TxtAssetName, "Asset name", false) ||
                Sm.IsLueEmpty(LueDepreciationCode, "Depreciation method") ||
                Sm.IsLueEmpty(LueCC, "Initial cost center") ||
                Sm.IsLueEmpty(LueSiteCode2, "Site") ||
                Sm.IsLueEmpty(LueAssetCategory, "Asset's category") ||
                IsAssetNameNotUnique() ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteAssetDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteAssetDt))) ||
                IsUploadFileNotValid() ||
                //(mIsAssetRecvQtyEditable && !IsRecvQtyValid()) ||
                IsResidualValueNotValid() ||
                IsGrdEmpty() ||
                IsQtyUsedNotValid() ||
                Sm.IsTxtEmpty(TxtAcNo, "COA's Account#", false)
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in tab DO to Department");
                return true;
            }
            return false;
        }

        private bool IsGrdValueEmpty(iGrid Grd, int Row, int Col, bool IsNumeric)
        {
            bool Result = false;
            if (IsNumeric)
            {
                if ((Sm.GetGrdStr(Grd, Row, Col).Length == 0) ||
                        (Sm.GetGrdStr(Grd, Row, Col).Length != 0 &&
                            decimal.Parse(Sm.GetGrdStr(Grd, Row, Col)) == 0m))
                {
                    Result = true;
                }
            }
            else
            {
                if (Sm.GetGrdStr(Grd, Row, Col).Length == 0)
                {
                    Result = true;
                }
            }
            return Result;
        }

        private bool IsQtyUsedNotValid()
        {

            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdDec(Grd1, i, 18) < Sm.GetGrdDec(Grd1, i, 16))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                        " Document# : " + Sm.GetGrdStr(Grd1, i, 2) +
                        "\n Item Name : " + Sm.GetGrdStr(Grd1, i, 7) +
                        "\n Quantity Used is more than Outstanding Quantity "


                        );
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsRecvQtyValid() {
            bool isValid = true;
            string Dno,DocNo,ItCode,ItName = string.Empty;

            for (int i = 0; i < (Grd1.Rows.Count-1); i++) {

                Dno = Sm.GetGrdStr(Grd1, i, 0);
                DocNo = Sm.GetGrdStr(Grd1, i, 2);
                ItCode = Sm.GetGrdStr(Grd1, i, 6);
                ItName = Sm.GetGrdStr(Grd1, i, 7);

                Decimal recvQty = getSqlRecvQtyValid(Dno, DocNo, ItCode);
                if (Sm.GetGrdDec(Grd1, i, 8) > recvQty)
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        " Receiving# : "+DocNo+
                        "\n Item Name : "+ItName+
                        "\n Quantity Receiving From Vendor : "+ Sm.FormatNum(recvQty,0)+
                        "\n Quantity is more than Quantity Receiving From Vendor "
                        
                        );
                    return isValid = false;
                }
            }

            return isValid;
        }

        private Decimal getSqlRecvQtyValid(string Dno, string DocNo, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IFNULL((T.Qty - U.Qty),T.Qty) Qty From ( ");
            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, B.ItCode, C.ItName, C.ItCodeInternal, C.ForeignName, B.Qty, C.InventoryUomCode As Uom, D.CurCode, A.POInd ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo And B.DNo = @DNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblPOHdr D On B.PODocNo = D.DocNo ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, T3.UPrice, T1.Qty As POQty, T1.Discount, T1.DiscountAmt, T1.RoundingValue ");
	        SQL.AppendLine("    From TblPODtl T1 ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo = T2.DocNo And T1.PORequestDNo = T2.DNo ");
	        SQL.AppendLine("    Inner Join TblQtDtl T3 On T2.QtDocNo = T3.DocNo And T2.QtDNo = T3.DNo ");
            SQL.AppendLine(") E On D.DocNo = E.DocNo And B.PODNo = E.DNo ");
            SQL.AppendLine("Where B.CancelInd = 'N' ");
            SQL.AppendLine("And B.Status = 'A' ");
            SQL.AppendLine("And C.FixedItemInd = 'Y' ");
            SQL.AppendLine("And A.POInd = 'Y' ");
            // SQL.AppendLine("And B.AssetCode Is Null ");
            SQL.AppendLine("UNION ALL");
            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, B.ItCode, C.ItName, C.ItCodeInternal, C.ForeignName, B.Qty, C.InventoryUomCode As Uom, A.CurCode,");
			SQL.AppendLine("	 A.POInd ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo And B.DNo = @DNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Where B.CancelInd = 'N' ");
            SQL.AppendLine("And B.Status = 'A' ");
            SQL.AppendLine("And C.FixedItemInd = 'Y' ");
            SQL.AppendLine("And A.POInd = 'N' ");
            //SQL.AppendLine("And B.AssetCode Is Null ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    Select B.RecvVdDocno,B.RecvVdDno,SUM(B.Qty) Qty from tblAsset A ");
            SQL.AppendLine("    INNER JOIN tblAssetDtl B ON A.AssetCode = B.AssetCode ");
            SQL.AppendLine("    AND A.ActiveInd = 'Y' ");
            SQL.AppendLine("    AND B.RecvVdDocno = @DocNo ");
            SQL.AppendLine("    AND B.RecvVdDno = @Dno" );
            SQL.AppendLine("    Group By B.RecvVdDocno,B.RecvVdDno" );
            SQL.AppendLine(")U ON T.DocNo = U.RecvVdDocno AND T.DNo = U.RecvVdDno");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<string>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<string>(ref cm, "@DNo", Dno);
            Sm.CmParam<string>(ref cm, "@ItCode", ItCode);

            Decimal RecvQty = Sm.GetValueDec(cm);
            return RecvQty;
        }

        private bool IsAssetNameNotUnique()
        {
            if (!mIsAssetNameUnique) return false;

            if (TxtAssetCode.Text.Length <= 0)
            {
                if (Sm.IsDataExist("Select 1 from TblAsset Where ActiveInd='Y' And AssetName=@Param;", TxtAssetName.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "Asset name should be unique.");
                    return true;
                }
            }
            else
            {
                if (Sm.IsDataExist("Select 1 from TblAsset Where ActiveInd='Y' And AssetName=@Param1 And AssetCode<>@Param2;", TxtAssetName.Text, TxtAssetCode.Text, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "Asset name should be unique.");
                    return true;
                }
            }
            return false;
        }

        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted()
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select AssetCode From TblAsset ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private string GenerateAssetCode(string Tbl)
        {
            string
                Yr = Sm.GetDte(DteAssetDt).Substring(0, 4),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'");
            int lenAssetCode = Convert.ToInt32(mLengthAssetCode);

            var MaxAssetCode = Sm.GetValue("SELECT MAX(LENGTH(AssetCode)) FROM TblAsset Where AssetCode Like '%/AS/%' "); //Solve impact dari Initial Asset yg disimpan di tbl yg sama dg Asset. (DocAbbr = IAS)
            if (MaxAssetCode.Length > 0)
                lenAssetCode = Convert.ToInt32(MaxAssetCode);

            var SQL = new StringBuilder();
            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + mLengthAssetCode + "), Convert((AssetCode)+1, Char)), " + mLengthAssetCode + ") From ( ");
            SQL.Append("       Select Convert(Left(AssetCode, " + mLengthAssetCode + "), Decimal) As AssetCode From TblAsset ");
            SQL.Append("       Where Left(AssetDt, 4)=@Param And LENGTH(AssetCode)="+lenAssetCode+" Order By Left(AssetCode, " + mLengthAssetCode + ") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + mLengthAssetCode + "), '1'), " + mLengthAssetCode + ")) ");
            SQL.Append(", '/AS/', @Param");
            SQL.Append(") As AssetCode");

            return Sm.GetValue(SQL.ToString(), Yr);
        }

        private string GenerateAssetCode()
        {
            // 01/00001/INVENTARIS/IMS/05/20
            string 
                AssetDt = Sm.GetDte(DteAssetDt),
                AssetCategoryCode=Sm.GetLue(LueAssetCategory),
                AssetCategoryName = LueAssetCategory.GetColumnValue("Col2").ToString();
            string
                Mth = AssetDt.Substring(4, 2),
                Yr = AssetDt.Substring(2, 2),
                Yr2 = AssetDt.Substring(0, 4),
                YrMth = Sm.Left(AssetDt, 6),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'");

            var SQL = new StringBuilder();

            if (mFormulaAssetCodeBasedOnCategory == "1")
            {
                SQL.Append("Select Concat(");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('00000', Convert(AssetCode+1, Char)), 5) From ( ");
                SQL.Append("       Select Convert(Substring(AssetCode, 4, 5), Decimal) As AssetCode From TblAsset ");
                SQL.Append("       Where Left(AssetDt, 6)=@Param1 ");
                SQL.Append("       And AssetCategoryCode='" + AssetCategoryCode + "' ");
                SQL.Append("       Order By Left(AssetCode, 5) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '00001') ");
                SQL.Append(", '/" + AssetCategoryCode + "/" + DocTitle + "/', @Param3, '/', @Param2 ");
                SQL.Append(") As AssetCode");

                return Sm.GetValue(SQL.ToString(), YrMth, Yr, Mth);
            }
            else
            {
                SQL.Append("Select Concat(");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('000000', Convert(AssetCode+1, Char)), 6) From ( ");
                SQL.Append("       Select Convert(Left(AssetCode, 6), Decimal) As AssetCode From TblAsset ");
                SQL.Append("       Where Left(AssetDt, 6)=@Param1 ");
                SQL.Append("       And AssetCategoryCode='" + AssetCategoryCode + "' ");
                SQL.Append("       Order By Left(AssetCode, 6) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '000001') ");
                SQL.Append(", '/" + DocTitle + "/" + "AS" +"/" + AssetCategoryName + "/', @Param2");
                SQL.Append(") As AssetCode");

                return Sm.GetValue(SQL.ToString(), YrMth, Yr2, Mth);
            }

            
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string AssetCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ListDoDeptItem.Clear();
                ClearData();

                ShowAssetHdr(AssetCode);
                ShowAssetDtl(AssetCode);
                ShowAssetDtl2(AssetCode);
                ShowAssetDtl3(AssetCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAssetHdr(string AssetCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            SQL.AppendLine("Select A.AssetCode, A.ItCode, B.ItName, A.AssetName, A.DisplayName, A.ShortCode, A.AssetType, A.ActiveInd, A.AssetDt, ");
            SQL.AppendLine("A.AssetValue, A.ResidualValue, A.EcoLifeYr, A.EcoLife, A.DepreciationCode, A.PercentageAnnualDepreciation, A.CCCode, ");
            SQL.AppendLine("A.AcNo, A.AcNo2 As AcNo2, ");
            if (mIsCOAUseAlias)
            {
                SQL.AppendLine("Concat(C.AcDesc, Case When C.Alias Is Null Then '' Else Concat(' [', C.Alias, ']') End) As AcDesc, ");
                SQL.AppendLine("Concat(D.AcDesc, Case When D.Alias Is Null Then '' Else Concat(' [', D.Alias, ']') End) As AcDesc2, ");
            }
            else
                SQL.AppendLine("C.AcDesc, D.AcDesc As AcDesc2, ");
            SQL.AppendLine("A.AssetCategoryCode, A.LeasingInd, ");
            SQL.AppendLine("A.Length, A.Height, A.Width, A.Volume, A.Diameter, A.Wide, ");
            SQL.AppendLine("A.LengthUomCode, A.HeightUomCode, A.WidthUomCode, A.VolumeUomCode, A.DiameterUomCode, A.WideUomCode, ");
            SQL.AppendLine("A.SoldInd, A.RentedInd, A.DisabledReason, A.Parent, A.FiskalInd, A.Location, A.FileName, ");
            SQL.AppendLine("A.Classification, A.SubClassification, A.Type, A.SubType, A.Location2, A.SubLocation, A.Location3, A.SiteCode, A.SiteCode2 ");
            SQL.AppendLine("From TblAsset A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblCOA C On A.AcNo=C.AcNo ");
            SQL.AppendLine("Left Join TblCOA D On A.AcNo2 = D.AcNo ");
            SQL.AppendLine("Where A.AssetCode=@AssetCode; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "AssetCode", 
                    
                    //1-5
                    "ItCode", "ItName", "AssetName", "DisplayName", "AssetType",  
                    
                    //6-10
                    "ActiveInd", "AssetDt", "AssetValue", "EcoLifeYr", "EcoLife", 
                    
                    //11-15
                    "DepreciationCode", "PercentageAnnualDepreciation", "CCCode", "AcNo", "AcDesc",

                    //16-20
                    "AcNo2", "AcDesc2", "AssetCategoryCode", "LeasingInd", "Length", 
                    
                    //21-25
                    "Height", "Width", "Volume", "Diameter", "Wide", 

                    //26-30
                    "LengthUomCode", "HeightUomCode", "WidthUomCode", "VolumeUomCode", "DiameterUomCode", 
                    
                    //31-35
                    "WideUomCode", "SoldInd", "RentedInd", "DisabledReason", "ShortCode",
                    
                    //36-40
                    "Parent", "FiskalInd", "Location", "FileName", "Classification",

                    //41-45
                    "SubClassification", "Type", "SubType", "Location2", "SubLocation",

                    //46-49
                    "Location3", "SiteCode", "ResidualValue", "SiteCode2"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtAssetCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtItCode.EditValue = Sm.DrStr(dr, c[1]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[2]);
                    TxtAssetName.EditValue = Sm.DrStr(dr, c[3]);
                    TxtDisplayName.EditValue = Sm.DrStr(dr, c[4]);
                    ChkAssetType.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");
                    ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[6]), "Y");
                    Sm.SetDte(DteAssetDt, Sm.DrStr(dr, c[7]));
                    TxtAssetValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                    TxtEcoLifeYr.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[9]), 0);
                    TxtEcoLifeMth.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                    Sm.SetLue(LueAssetCategory, Sm.DrStr(dr, c[18]));
                    Sm.SetLue(LueDepreciationCode, Sm.DrStr(dr, c[11]));
                    TxtPercentageAnnualDepreciation.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[12]), 0);
                    Sm.SetLue(LueCC, Sm.DrStr(dr, c[13]));
                    TxtAcNo.EditValue = Sm.DrStr(dr, c[14]);
                    TxtAcDesc.EditValue = Sm.DrStr(dr, c[15]);
                    TxtAcNo2.EditValue = Sm.DrStr(dr, c[16]);
                    TxtAcDesc2.EditValue = Sm.DrStr(dr, c[17]);
                    ChkLeasingInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y");
                    TxtLength.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                    TxtHeight.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[21]), 0);
                    TxtWidth.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                    TxtVolume.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 0);
                    TxtDiameter.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                    TxtWide.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                    Sm.SetLue(LueLengthUomCode, Sm.DrStr(dr, c[26]));
                    Sm.SetLue(LueHeightUomCode, Sm.DrStr(dr, c[27]));
                    Sm.SetLue(LueWidthUomCode, Sm.DrStr(dr, c[28]));
                    Sm.SetLue(LueVolumeUomCode, Sm.DrStr(dr, c[29]));
                    Sm.SetLue(LueDiameterUomCode, Sm.DrStr(dr, c[30]));
                    Sm.SetLue(LueWideUomCode, Sm.DrStr(dr, c[31]));
                    ChkSoldInd.Checked = Sm.DrStr(dr, c[32])=="Y";
                    ChkRentedInd.Checked = Sm.DrStr(dr, c[33])=="Y";
                    MeeDisabledReason.EditValue = Sm.DrStr(dr, c[34]);
                    TxtShortCode.EditValue = Sm.DrStr(dr, c[35]);
                    Sl.SetLueAssetCode(ref LueParent, Sm.DrStr(dr, c[36]));
                    ChkFiskalInd.Checked = Sm.DrStr(dr, c[37]) == "Y";
                    //TxtLocation.EditValue = Sm.DrStr(dr, c[38]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[39]);
                    Sm.SetLue(LueClassification, Sm.DrStr(dr, c[40]));
                    Sm.SetLue(LueSubClassification, Sm.DrStr(dr, c[41]));
                    Sm.SetLue(LueType, Sm.DrStr(dr, c[42]));
                    Sm.SetLue(LueSubType, Sm.DrStr(dr, c[43]));
                    Sm.SetLue(LueLocation, Sm.DrStr(dr, c[44]));
                    Sm.SetLue(LueSubLocation, Sm.DrStr(dr, c[45]));
                    Sm.SetLue(LueLocation2, Sm.DrStr(dr, c[46]));
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[47]));
                    TxtResidualValue.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[48]), 0);
                    Sm.SetLue(LueSiteCode2, Sm.DrStr(dr, c[49]));
                }, true
            );
        }

        private void ShowAssetDtl(string AssetCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.DocNo, B.DNo, C.DocDt, B.ItCode, B.Qty, ");
            SQL.AppendLine("D.ItName, E.UomName as Uom, F.CurCode, F.UPrice, B.Qty * F.Uprice AS Amt, ");
            SQL.AppendLine("A.Qty as QtyUsed, A.OutstandingQty, B.Remark ");
            SQL.AppendLine("FROM tblassetdtl A ");
            SQL.AppendLine("INNER JOIN tbldodeptdtl B ON A.RecvVdDocNo = B.DocNo AND A.RecvVdDNo = B.DNo ");
            SQL.AppendLine("INNER JOIN tbldodepthdr C ON B.DocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN tblitem D ON B.ItCode = D.ItCode ");
            SQL.AppendLine("INNER JOIN tbluom E ON D.InventoryUOMCode = E.UomCode ");
            SQL.AppendLine("INNER JOIN tblstockprice F ON B.Source = F.Source ");
            SQL.AppendLine("Where A.AssetCode = @AssetCode; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DNo", "DocDt", "ItCode", "ItName", "Qty",
                    
                    //6-10
                     "Uom", "CurCode", "UPrice", "Amt", "QtyUsed", 
                    //11-12
                    "OutstandingQty", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 1); //Dno
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0); //DocNo
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 2); //date
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3); //itcode
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4); //itname
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5); //qty
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6); //uom
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7); //currency
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9); //amt
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 8); //uprice
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10); //qtyused
                    Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 15) * Sm.GetGrdDec(Grd1, Row, 16); //capitalizedamt
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 11); //outstandingqty
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 12); //remark
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 11 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowAssetDtl2(string AssetCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, B.* ");
            SQL.AppendLine("From TblAssetDtl2 A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DocNo, DocDt, CurCode, Amt ");
            SQL.AppendLine("    From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where DocType = '01' ");
            SQL.AppendLine(")B On A.VRDocNo = B.DocNo ");
            SQL.AppendLine("Where A.AssetCode = @AssetCode; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-3
                    "DocDt", "CurCode", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowAssetDtl3(string AssetCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo,  A.DocDt, A.ReconditionDt, A.AssetValue1, A.AssetValue2, ");
            SQL.AppendLine("A.ReconditionValue, A.EcoLife1, A.EcoLife2, A.EcoLife3, A.ReconditionReason");
            SQL.AppendLine("From TblReconditionAssetHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode ");
            SQL.AppendLine("Inner Join TblAssetCategory C On B.AssetCategoryCode = C.AssetCategoryCode ");
            SQL.AppendLine("Where A.AssetCode = @AssetCode and A.CancelInd = 'N' ");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "ReconditionDt", "AssetValue1", "AssetValue2", "ReconditionValue",

                    //6-9
                    "EcoLife1","EcoLife2","EcoLife3","ReconditionReason"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd3.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0); //docno
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2); //reconditiondt
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3); //assetvalue1
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4); //assetvalue2
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5); //reconditionvalue
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6); //ecolife1
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8); //ecolife3
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9); //reconditionreason
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion



        #region Additional Method

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueCC);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select ProfitCenterCode From TblCostCenter Where CCCode = @Param; " ,
                    Value);

        }

        private void ComputeCapitalizedAmt(iGrid Grd, int Row)
        {
                Grd.Cells[Row, 17].Value = Sm.FormatNum(Sm.GetGrdDec(Grd, Row, 15) * Sm.GetGrdDec(Grd, Row, 16), 0);
        }

        private void InsertDataListDoDeptItem()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                        ListDoDeptItem.Add(new DoDeptItem()
                        {
                            DocNo = Sm.GetGrdStr(Grd1, Row, 2),
                            DNo = Sm.GetGrdStr(Grd1, Row, 0),
                            ItCode = Sm.GetGrdStr(Grd1, Row, 6),
                            QtyUsed = IsGrdValueEmpty(Grd1, Row, 16, true) ? 0m : Sm.GetGrdDec(Grd1, Row, 16)
                        });
                }
            }
            
        }

        internal void ComputeRate()
        {
            var Amt = 0m;
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 14).Length != 0)
                {
                    Amt += Sm.GetGrdDec(Grd1, i, 14);
                }
            }

            if (mIsRecvForAssetShowRateInfo && Amt > 0)
            {
                TxtAssetValue.Text = Sm.FormatNum(Amt, 0);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAssetValue }, true);
            }
            //else
            //    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAssetValue }, false);
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }


        private void UploadFile(string AssetCode)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateAssetFile(AssetCode, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private string GetDepreciationDocNo()
        {
            var SQL = new StringBuilder();
            string mDepreciationAssetDocNo = string.Empty;

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblDepreciationAssetHdr ");
            SQL.AppendLine("Where AssetCode = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            mDepreciationAssetDocNo = Sm.GetValue(SQL.ToString(), TxtAssetCode.Text);

            return mDepreciationAssetDocNo;
        }

        private bool IsAssetDepreciated()
        {
            return (GetDepreciationDocNo().Length > 0);
        }

        internal string GetSelectedRecvVd()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 && Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "*" + Sm.GetGrdStr(Grd1, Row, 0) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedVRM()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void SetLueCCCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            string DeptCode = string.Empty;

            if (Grd1.Rows.Count > 1)
                DeptCode = Sm.GetGrdStr(Grd1, 0, 10);

            SQL.AppendLine("Select A.CCCode Col1, A.CCName Col2 ");
            SQL.AppendLine("From TblCostCenter A ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            if (mIsFilterByCC)
            {
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblGroupCostCenter T ");
                SQL.AppendLine("    Where T.CCCode = A.CCCode ");
                SQL.AppendLine("    And GrpCode In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select GrpCode ");
                SQL.AppendLine("        From TblUser ");
                SQL.AppendLine("        Where UserCode = @UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By A.CCName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public void SetLueAssetCategoryCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select AssetCategoryCode As Col1, AssetCategoryName As Col2 From TblAssetCategory where actind = 'Y' " +
                "Order By AssetCategoryName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsLueEmpty(LookUpEdit Lue)
        {
            bool Result = true;
            if (Lue.EditValue == null ||
                Lue.Text.Trim().Length == 0 ||
                Lue.EditValue.ToString().Trim().Length == 0)
            {
                Result = true;
            }
            else
                Result = false;
            return Result;
        }

        public void SetLueDepreciationCode(ref LookUpEdit Lue)
        {
            string Filter = string.Empty;
            string AssetCtCode = Sm.GetLue(LueAssetCategory);

            if (Sm.Find_In_Set(AssetCtCode, mAssetCategoryCodeForNonDepreciable))
                Filter = " And OptCode In ('" + mDepreciationMethodForNonDepreciable + "') ";
            else if (IsLueEmpty(LueAssetCategory))
                Filter = string.Empty;
            else
                Filter = " And OptCode Not In ('" + mDepreciationMethodForNonDepreciable + "') ";

            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'DepreciationMethod' "+ Filter +" Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private decimal ComputeMaxPercentageResidualValue()
        {
            decimal AssetValue = Convert.ToDecimal(TxtAssetValue.Text);
            decimal MaxPercentageFromParam = Convert.ToDecimal(mMaxPercentageOfResidualValue) * 0.01m;
            decimal MaxResidualValue;

            MaxResidualValue = MaxPercentageFromParam > 0 ? AssetValue * MaxPercentageFromParam : AssetValue;

            return MaxResidualValue;
            
        }

        private bool IsResidualValueNotValid()
        {
            decimal ResidualValue = Convert.ToDecimal(TxtResidualValue.Text);
            decimal MaxResidualValue = ComputeMaxPercentageResidualValue();

            string MaxResidualValueString = String.Format("{0:#,##0} !", MaxResidualValue);


            if (ResidualValue > MaxResidualValue)
            {
                Sm.StdMsg(mMsgType.Warning, "Percentage of Asset Value is " + mMaxPercentageOfResidualValue +
                 "% and Residual Value cannot more than " + MaxResidualValueString);
                return true;
            }

            else
                return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkActiveInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                bool IsNotActive = Sm.IsDataExist("Select 1 From TblAsset Where ActiveInd = 'N' And AssetCode = @Param Limit 1", TxtAssetCode.Text);
                string Reason = Sm.GetValue("Select DisabledReason From TblAsset Where ActiveInd = 'N' And AssetCode = @Param Limit 1;", TxtAssetCode.Text);

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeDisabledReason }, ChkActiveInd.Checked);
                if (ChkActiveInd.Checked) MeeDisabledReason.EditValue = null;

                if (!ChkActiveInd.Checked && IsNotActive) MeeDisabledReason.EditValue = Reason;
            }
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAssetCode);
        }

        private void TxtAssetName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAssetName);
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmAsset2Dlg(this));
        }

        private void LueDepreciationCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDepreciationCode, new Sm.RefreshLue1(SetLueDepreciationCode));
        }

        private void LueCC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCC, new Sm.RefreshLue1(SetLueCCCode));
        }

        private void TxtEcoLife_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtEcoLifeYr, 0);

            var year = Convert.ToDecimal(TxtEcoLifeYr.Text);

            if(year != 0)
            {
                if (year <= 30)
                {
                    var month = year * 12;
                    TxtEcoLifeMth.Text = Sm.FormatNum(Math.Floor(month), 0);
                }
                else
                {
                    Sm.StdMsg(mMsgType.Warning, "Economic Life cannot more than 30 years");
                    TxtEcoLifeYr.Focus();
                }

                var PercentageDepreciation =  (1 / year) * 100; // 1 is equal to 100% ( 100/100 = 1 )
                TxtPercentageAnnualDepreciation.Text = Convert.ToString(Math.Round(PercentageDepreciation, 2));
            }

            if (year == 0)
            {
                Sm.SetControlNumValueZero(new List<DXE.TextEdit>
                { TxtPercentageAnnualDepreciation, TxtEcoLifeMth }, 0);
            }

        }

        private void TxtAssetValue_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAssetValue, 0);
        }

        private void TxtPercentageAnnualDepreciation_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtPercentageAnnualDepreciation, 0);
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAsset2Dlg2(this, 1));
        }

        private void BtnAcNo2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAsset2Dlg2(this, 2));
        }

        private void LueAssetCategory_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueAssetCategory, new Sm.RefreshLue1(SetLueAssetCategoryCode));
                SetLueDepreciationCode(ref LueDepreciationCode);

                if (Sm.Find_In_Set(Sm.GetLue(LueAssetCategory), mAssetCategoryCodeForNonDepreciable))
                {
                    TxtEcoLifeYr.Properties.ReadOnly = true;
                    TxtEcoLifeYr.BackColor = Color.Silver;
                }
                else
                {
                    TxtEcoLifeYr.Properties.ReadOnly = false;
                    TxtEcoLifeYr.BackColor = Color.White;
                }
            }

            if(Sm.Find_In_Set(Sm.GetLue(LueAssetCategory),mAssetCategoryCodeForNonDepreciable)) Sm.SetLue(LueDepreciationCode, mDepreciationMethodForNonDepreciable);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );

                Grd2.Font = new Font(
                    Grd2.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void TxtLength_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtLength, 0);
        }

        private void LueLengthUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLengthUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtHeight_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtHeight, 0);
        }

        private void LueHeightUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueHeightUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtEcoLifeYr_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtEcoLifeYr, 0);
            
        }

        private void TxtResidualValue_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtResidualValue, 0);
        }

        private void TxtWidth_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtWidth, 0);
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue2(Sl.SetLueAssetCode), string.Empty);
        }

        private void LueWidthUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWidthUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtVolume_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtVolume, 0);
        }

        private void LueVolumeUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVolumeUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtDiameter_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtDiameter, 0);
        }

        private void LueDiameterUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDiameterUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtWide_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtWide, 0);
        }

        private void LueWideUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWideUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void LueClassification_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueClassification, new Sm.RefreshLue2(Sl.SetLueOption), "AssetClassification");
        }

        private void LueSubClassifiaction_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSubClassification, new Sm.RefreshLue2(Sl.SetLueOption), "AssetSubClassification");
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "AssetType");
        }

        private void LueSubType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSubType, new Sm.RefreshLue2(Sl.SetLueOption), "AssetSubType");
        }

        private void LueLocation_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocation, new Sm.RefreshLue2(Sl.SetLueOption), "AssetLocation");
        }

        private void LueSubLocation_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSubLocation, new Sm.RefreshLue2(Sl.SetLueOption), "AssetSubLocation");
        }

        private void LueLocation2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocation2, new Sm.RefreshLue2(Sl.SetLueOption), "AssetLocation2");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        private void LueSiteCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode2, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        #endregion

        #endregion

        #region Class

        internal class DoDeptItem
        {
            public string DocNo { get; set; }

            public string DNo { get; set; }

            public string ItCode { get; set; }

            public decimal QtyUsed { get; set; }
        }

        #endregion
    }
}
