﻿#region Update
/*
    08/01/2018 [TKG] Generate Multi Payrun
    13/04/2018 [TKG] Payrun code berdasarkan start date atau end date menggunakan parameter PayrunCodeFormatType
    20/09/2019 [TKG] menggunakan PPS untuk memproses multi payrun (sebelumnya master employee)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMultiPayrun : RunSystem.FrmBase12
    {
        #region Field, Property

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mPayrunCodeFormatType = "1";
        private bool mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmMultiPayrun(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR?"Y":"N");
                Sl.SetLueOption(ref LueSystemType, "EmpSystemType");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");   
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                if (mIsFilterBySiteHR) LblSiteCode.ForeColor = Color.Red;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mPayrunCodeFormatType = Sm.GetParameter("PayrunCodeFormatType");
        }


        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Process", string.Empty) == DialogResult.No ||
                (mIsFilterBySiteHR && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                Sm.IsLueEmpty(LueSystemType, "System type") ||
                Sm.IsLueEmpty(LuePayrunPeriod, "Payrun period") ||
                Sm.IsLueEmpty(LuePGCode, "Payroll group") ||
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsDteEmpty(DteEndDt, "End date")
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<SiteDept>();
            try
            {
                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Sm.StdMsg(mMsgType.Info, "Process is completed.");
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        LueSiteCode, LueSystemType, LuePayrunPeriod, LuePGCode, DteStartDt, 
                        DteEndDt
                    });
                    ChkSiteCode.Checked = false;
                    LueSiteCode.Focus();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<SiteDept> l)
        {
            var YrMth = string.Empty;

            if (mPayrunCodeFormatType == "2")
                YrMth = Sm.GetDte(DteEndDt).Substring(0, 6);
            else
                YrMth = Sm.GetDte(DteStartDt).Substring(0, 6);

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("   Select Convert(SeqNo+1, Char) From ( ");
            SQL.Append("       Select Convert(Right(PayrunCode, 4), Decimal) As SeqNo From TblPayrun ");
            SQL.Append("       Where Left(PayrunCode, 6)=@Param Order By Right(PayrunCode, 4) Desc Limit 1 ");
            SQL.Append("    ) As Temp), '1') As SeqNo;");

            var SeqNo = int.Parse(Sm.GetValue(SQL.ToString(), YrMth));

            SQL.Length = 0;
            SQL.Capacity = 0;

            var cm = new MySqlCommand();

            SQL.AppendLine("    Select Distinct T1.SiteCode, T1.DeptCode, T4.SiteName, T5.DeptName ");
            SQL.AppendLine("    From TblEmployeePPS T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select X.EmpCode, X.StartDt ");
            SQL.AppendLine("        From TblEmployeePPS X ");
            SQL.AppendLine("        Where Exists ( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblEmployee T ");
            SQL.AppendLine("            Where T.EmpCode=X.EmpCode ");
            SQL.AppendLine("            And T.JoinDt<=@EndDt ");
            SQL.AppendLine("            And ( ");
            SQL.AppendLine("                T.ResignDt Is Null Or ");
            SQL.AppendLine("                (T.ResignDt Is Not Null And T.ResignDt>@StartDt) Or ");
            SQL.AppendLine("                ( ");
            SQL.AppendLine("                    T.ResignDt Is Not Null ");
            SQL.AppendLine("                    And T.ResignDt<=@StartDt ");
            if (Sm.CompareStr(Sm.GetParameter("DocTitle"), "HIN"))
            {
                SQL.AppendLine("                    And ( ");
                SQL.AppendLine("                        T.EmpCode In ( ");
                SQL.AppendLine("                            Select Tbl2.EmpCode ");
                SQL.AppendLine("                            From TblEmpSCIHdr Tbl1, TblEmpSCIDtl Tbl2 ");
                SQL.AppendLine("                            Where Tbl1.DocNo=Tbl2.DocNo ");
                SQL.AppendLine("                            And Tbl1.DocDt Between @StartDt And @EndDt ");
                SQL.AppendLine("                            And (Tbl2.PayrunCode Is Null Or (Tbl2.PayrunCode Is Not Null And Tbl2.PayrunCode=@PayrunCode)) ");
                SQL.AppendLine("                            And Tbl1.CancelInd='N' ");
                SQL.AppendLine("                            ) Or ");
                SQL.AppendLine("                        T.EmpCode In ( ");
                SQL.AppendLine("                            Select EmpCode ");
                SQL.AppendLine("                            From TblSalaryAdjustmentHdr ");
                SQL.AppendLine("                            Where CancelInd='N' ");
                SQL.AppendLine("                            And PaidDt Between @StartDt And @EndDt ");
                SQL.AppendLine("                            And (PayrunCode Is Null Or (PayrunCode Is Not Null And PayrunCode=@PayrunCode)) ");
                SQL.AppendLine("                            ) Or ");
                SQL.AppendLine("                        T.EmpCode In ( ");
                SQL.AppendLine("                            Select Tbl2.EmpCode ");
                SQL.AppendLine("                            From TblSalaryAdjustment2Hdr Tbl1, TblSalaryAdjustment2Dtl Tbl2 ");
                SQL.AppendLine("                            Where Tbl1.DocNo=Tbl2.DocNo ");
                SQL.AppendLine("                            And Tbl1.PaidDt Between @StartDt And @EndDt ");
                SQL.AppendLine("                            And (Tbl2.PayrunCode Is Null Or (Tbl2.PayrunCode Is Not Null And Tbl2.PayrunCode=@PayrunCode)) ");
                SQL.AppendLine("                            And Tbl1.CancelInd='N' ");
                SQL.AppendLine("                            ) ");
                SQL.AppendLine("                        ) ");
            }
            SQL.AppendLine("                ) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) ");
            if (ChkSiteCode.Checked)
            {
                SQL.AppendLine("And X.SiteCode Is Not Null ");
                SQL.AppendLine("And X.SiteCode=@SiteCode ");
            }
            SQL.AppendLine("        And X.StartDt<=@EndDt ");
            SQL.AppendLine("        And (X.EndDt Is Null Or X.EndDt>=@StartDt)");
            SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode And T1.StartDt=T3.StartDt ");
            SQL.AppendLine("Inner Join TblSite T4 On T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("Inner Join TblDepartment T5 On T1.DeptCode=T5.DeptCode ");

            //SQL.AppendLine("Select Distinct A.SiteCode, A.DeptCode, B.SiteName, C.DeptName ");
            //SQL.AppendLine("From TblEmployee A ");
            //SQL.AppendLine("Inner Join TblSite B On A.SiteCode=B.SiteCode ");
            //SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            //SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@Dt)) ");
            //SQL.AppendLine("And A.DeptCode Is Not Null ");
            //SQL.AppendLine("And A.SiteCode Is Not Null ");
            //if (ChkSiteCode.Checked)
            //    SQL.AppendLine("And A.SiteCode=@SiteCode ");
            //SQL.AppendLine("Order By B.SiteName, C.DeptName;");

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            if (ChkSiteCode.Checked)
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "SiteCode", "DeptCode", "SiteName", "DeptName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SiteDept()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            DeptCode = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            DeptName = Sm.DrStr(dr, c[3]),
                            PayrunCode = string.Concat(YrMth, "-", Sm.Right(string.Concat("0000", SeqNo), 4))
                        });
                        SeqNo += 1;
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<SiteDept> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            for (int i = 0; i < l.Count; i++)
            {
                SQL.AppendLine("Insert Into TblPayrun(PayrunCode, PayrunName, CancelInd, EOYTaxYr, EOYTaxInd, DeptCode, SystemType, PayrunPeriod, PGCode, SiteCode, StartDt, EndDt, Status, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PayrunCode" + i.ToString() + ", @PayrunName" + i.ToString() + ", 'N', Null, 'N', @DeptCode" + i.ToString() + ", @SystemType, @PayrunPeriod, @PGCode, @SiteCode" + i.ToString() + ", @StartDt, @EndDt, 'O', Null, @UserCode, CurrentDateTime()); ");

                Sm.CmParam<String>(ref cm, "@SiteCode"+ i.ToString(), l[i].SiteCode);
                Sm.CmParam<String>(ref cm, "@DeptCode" + i.ToString(), l[i].DeptCode);
                Sm.CmParam<String>(ref cm, "@PayrunCode" + i.ToString(), l[i].PayrunCode);
                Sm.CmParam<String>(ref cm, "@PayrunName" + i.ToString(), 
                    string.Concat(
                    Sm.Left(l[i].PayrunCode, 4), "-",
                    l[i].PayrunCode.Substring(4, 2), " : ",
                    l[i].SiteName, " (", l[i].DeptName,")"
                    ));
            }

            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetLue(LueSystemType));
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cm.CommandText = SQL.ToString();

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);

        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSystemType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSystemType, new Sm.RefreshLue2(Sl.SetLueOption), "EmpSystemType");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
        }

        #endregion

        #endregion

        #region Class

        private class SiteDept
        {
            public string SiteCode { get; set; }
            public string DeptCode { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string PayrunCode { get; set; }
        }

        #endregion
    }
}
