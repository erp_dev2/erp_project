﻿#region Update
/*
    04/07/2018 [TKG] Tambah site, tambah amount before discount
    29/05/2019 [TKG] Tambah cost center
 *  20/04/2021 [HAR/PHT] tambah info pos name
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPosNoFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPosNo mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPosNoFind(FrmPosNo FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PosNo, B.CtName, C.WhsName, D.CCName, A.PosName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblPosNo A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("Left Join TblCostCenter D On A.CCCode=D.CCCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Pos Number", 
                        "Customer",
                        "Warehouse",
                        "Cost Center",
                        "Pos Name",  
                        
                        //6-10
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        //11
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 200, 200, 100, 
                        
                        //6-10
                        100, 100, 100, 100, 100, 
                        //11
                        100
                        
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 10 });
            Sm.GrdFormatTime(Grd1, new int[] { 8, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                
                Sm.FilterStr(ref Filter, ref cm, TxtPosNo.Text, "A.PosNo", false);

                Sm.ShowDataInGrid(
                       ref Grd1, ref cm, mSQL + Filter + "Order By A.PosNo;",
                        new string[]
                        {
                            //0
                            "PosNo", 
                                
                            //1-5
                            "CtName", "WhsName", "CCName", "PosName", "CreateBy",  
                            
                            //6-8
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPosNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPosNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Pos number");
        }

        #endregion

        #endregion
    }
}
