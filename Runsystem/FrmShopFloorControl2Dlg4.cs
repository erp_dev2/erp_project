﻿#region Update
/*
    25/06/2018 [TKG] tambah filter batch number
    09/01/2020 [WED/MMM] panggil function autogenerate batchno
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControl2Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmShopFloorControl2 mFrmParent;
        string mSQL = string.Empty, mWhsCode = string.Empty, mPPDocNo = string.Empty, mWorkCenterDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmShopFloorControl2Dlg4(
            FrmShopFloorControl2 FrmParent, 
            string WhsCode,
            string PPDocNo,
            string WorkCenterDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mPPDocNo = PPDocNo;
            mWorkCenterDocNo = WorkCenterDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
                SetCcbBatchNo(ref CcbBatchNo2);

                ShowData(GetSubQuery());
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion
 
        #region Standard Method

        private string GetSubQuery()
        { 
            var SQL = new StringBuilder();

            SQL.AppendLine(" And A.ItCode In ( ");
            SQL.AppendLine("    Select Distinct G.DocCode "); 
            SQL.AppendLine("    From TblPPHdr A "); 
            SQL.AppendLine("    Inner Join TblPPDtl B On A.DocNo=B.DocNo And B.DocType='1' "); 
            SQL.AppendLine("    Inner Join TblProductionOrderHdr C On B.ProductionOrderDocNo=C.DocNo "); 
            SQL.AppendLine("    Inner Join TblProductionOrderDtl D On C.DocNo=D.DocNo "); 
            SQL.AppendLine("    Inner Join TblProductionRoutingDtl E "); 
	        SQL.AppendLine("        On C.ProductionRoutingDocNo=E.DocNo "); 
	        SQL.AppendLine("        And D.ProductionRoutingDNo=E.DNo ");
	        SQL.AppendLine("        And E.WorkCenterDocNo=@WorkCenterDocNo ");
            if (!mFrmParent.mIsCodeForSFCStandard)
            {
                SQL.AppendLine("    Inner Join TblBOMHdr F On D.BOMDocNo=F.DocNo And F.ActiveInd='Y' ");
            }
            else
            {
                SQL.AppendLine("    Inner Join TblWorkcenterBOMFormulation E2 On E.WorkcenterDocno = E2.WorkcenterCode ");
                SQL.AppendLine("    Inner Join TblBOMHdr F On E2.BOMFormulationCode=F.DocNo And F.ActiveInd='Y'  ");
            }
            SQL.AppendLine("    Inner Join TblBOMDtl G On F.DocNo=G.DocNo And G.DocType<>'3' ");
            SQL.AppendLine("    Where A.DocNo=@PPDocNo ");
            SQL.AppendLine("    ) ");

            return SQL.ToString();
        }


        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, A.PropCode, C.PropName, A.BatchNo, A.Source, D.ItCtName, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3 ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Inner Join TblItemCategory D On B.ItCtCode=D.ItCtCode ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode ");
            SQL.AppendLine("And (A.Qty>0) ");
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Local Code", 
                        "Item's Name", 

                        //6-10
                        "Category",
                        "Property Code",
                        "Property",
                        "Batch#",
                        "Source",
                        
                        //11-15
                        "Lot",
                        "Bin", 
                        "Stock",
                        "UoM",
                        "Stock",

                        //16-19
                        "UoM",
                        "Stock",
                        "UoM",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,
                        //1-5
                        20, 80, 20, 60, 250, 
                        //6-10
                        150, 0, 100, 200, 180, 
                        //11-15
                        60, 60, 80, 80, 80, 
                        //16-19
                        80, 80, 80, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 8, 10, 11, 15, 16, 17, 18, 19 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsSFCShowRecvWhsWithTRRemark)
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, true);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 8, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            ShowData("");
        }

        private void ShowData(string SubQuery)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter2 = string.Empty;

                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter2, "A", ref mFrmParent.Grd3, 9, 10, 11);
                var Filter = (Filter2.Length > 0) ? " And (" + Filter2 + ") " : " And 1=1 ";
                
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@PPDocNo", mPPDocNo);
                Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", mWorkCenterDocNo);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItCodeInternal", "B.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, ProcessCcb(Sm.GetCcb(CcbBatchNo2)), "A.BatchNo", false);

                if (ChkBatchNo.Checked)
                    FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", "_1");
                if (ChkBatchNo3.Checked)
                    FilterStr(ref Filter, ref cm, TxtBatchNo3.Text, "A.BatchNo", "_2");
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + SubQuery + 
                        " Order By A.ItCode, A.BatchNo, A.Source, A.Bin;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "ItName", "ItCtName", "PropCode", "PropName", 
                            
                            //6-10
                            "BatchNo", "Source", "Lot", "Bin", "Qty", 
                            
                            //11-15
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        }, true, false, false, false
                    );
                if (mFrmParent.mIsSFCShowRecvWhsWithTRRemark && Grd1.Rows.Count > 0)
                    ShowRemark();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 13, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 14, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 15, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 16, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 17, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 18, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 19, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 20, Grd1, Row2, 18);
                        mFrmParent.Grd3.Cells[Row1, 22].Value = 0m;

                        //Sm.SetGrdNumValueZero(mFrmParent.Grd3, Row1, new int[] { 13, 16, 19 });
                        
                        mFrmParent.Grd3.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19, 22 });
                    }
                }
                mFrmParent.SetDNo(mFrmParent.Grd3);

                if (mFrmParent.mIsSFCUseOptionBatchNoFormula)
                {
                    for (int i = 0; i < mFrmParent.Grd1.Rows.Count - 1; ++i)
                    {
                        mFrmParent.GenerateBatchNo(i);
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string Key =
                Sm.GetGrdStr(Grd1, Row, 2) +
                Sm.GetGrdStr(Grd1, Row, 7) +
                Sm.GetGrdStr(Grd1, Row, 9) +
                Sm.GetGrdStr(Grd1, Row, 10) +
                Sm.GetGrdStr(Grd1, Row, 11) +
                Sm.GetGrdStr(Grd1, Row, 12);
            for (int Index = 0; Index < mFrmParent.Grd3.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Key,
                        Sm.GetGrdStr(mFrmParent.Grd3, Index, 2) +
                        Sm.GetGrdStr(mFrmParent.Grd3, Index, 6) +
                        Sm.GetGrdStr(mFrmParent.Grd3, Index, 8) +
                        Sm.GetGrdStr(mFrmParent.Grd3, Index, 9) +
                        Sm.GetGrdStr(mFrmParent.Grd3, Index, 10) +
                        Sm.GetGrdStr(mFrmParent.Grd3, Index, 11)
                    ))
                    return true;
            }
            return false;
        }

        private void SetCcbBatchNo(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct BatchNo As Col From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And (Qty>0 Or Qty2>0 Or Qty3>0) ");
            SQL.AppendLine("Order By BatchNo Asc;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private void ShowRemark()
        {
            var l = new List<SourceRemark>();

            ShowRemark1(ref l);
            ShowRemark2(ref l);
        }

        private void ShowRemark1(ref List<SourceRemark> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string SourceTemp = string.Empty;
    
            int No = 1;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                SourceTemp = Sm.GetGrdStr(Grd1, Row, 10);
                if (SourceTemp.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(C.Source=@Source" + No.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@Source" + No.ToString(), SourceTemp);
                    No += 1;
                    l.Add(new SourceRemark(){ Source = SourceTemp, Remark = string.Empty });
                }
            }
        
            SQL.AppendLine("Select Distinct C.Source, B.Remark ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            if (Filter.Length != 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Source", "Remark" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l.Where(x => Sm.CompareStr(x.Source, Sm.DrStr(dr, c[0]))))
                        {
                            if (x.Remark.Length!=0) x.Remark+=", ";
                            x.Remark += Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ShowRemark2(ref List<SourceRemark> l)
        {
            if (l.Count > 0)
            {
                l.ForEach(x =>
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), x.Source))
                            Grd1.Cells[Row, 19].Value = x.Remark;
                    }
                });
                l.Clear();
            }
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 +  Param + Index.ToString();
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void ChkBatchNo2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Batch#");
        }

        private void CcbBatchNo2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void TxtBatchNo3_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo3_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        #endregion

        #endregion

        #region Class

        private class SourceRemark
        {
            public string Source { get; set; }
            public string Remark { get; set; }
        }

        #endregion
    }
}
