﻿namespace RunSystem
{
    partial class FrmCtQt3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCtQt3));
            this.panel3 = new System.Windows.Forms.Panel();
            this.DteQtEndDt = new DevExpress.XtraEditors.DateEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnCtContactPersonName = new DevExpress.XtraEditors.SimpleButton();
            this.LueCtContactPersonName = new DevExpress.XtraEditors.LookUpEdit();
            this.DteQtStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BtnCtReplace = new DevExpress.XtraEditors.SimpleButton();
            this.BtnCtReplace2 = new DevExpress.XtraEditors.SimpleButton();
            this.LueContract = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtCtReplace = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LuePtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCreditLimit = new DevExpress.XtraEditors.TextEdit();
            this.ChkPrintSignatureInd = new DevExpress.XtraEditors.CheckEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LueSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.TpgQuotationItem = new System.Windows.Forms.TabPage();
            this.LueType = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LblSetDefault = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueContract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtReplace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPrintSignatureInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.TpgQuotationItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(836, 0);
            this.panel1.Size = new System.Drawing.Size(70, 523);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(836, 523);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.DteQtEndDt);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.TxtLocalDocNo);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.BtnCtContactPersonName);
            this.panel3.Controls.Add(this.LueCtContactPersonName);
            this.panel3.Controls.Add(this.DteQtStartDt);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.LueCtCode);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(836, 189);
            this.panel3.TabIndex = 9;
            // 
            // DteQtEndDt
            // 
            this.DteQtEndDt.EditValue = null;
            this.DteQtEndDt.EnterMoveNextControl = true;
            this.DteQtEndDt.Location = new System.Drawing.Point(154, 160);
            this.DteQtEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQtEndDt.Name = "DteQtEndDt";
            this.DteQtEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteQtEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQtEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQtEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQtEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQtEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQtEndDt.Properties.MaxLength = 8;
            this.DteQtEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQtEndDt.Size = new System.Drawing.Size(117, 20);
            this.DteQtEndDt.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(31, 162);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 14);
            this.label7.TabIndex = 24;
            this.label7.Text = "Quotation End Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(154, 73);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtLocalDocNo.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(44, 75);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 14);
            this.label13.TabIndex = 15;
            this.label13.Text = "Local Document#";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCtContactPersonName
            // 
            this.BtnCtContactPersonName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtContactPersonName.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtContactPersonName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtContactPersonName.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtContactPersonName.Appearance.Options.UseBackColor = true;
            this.BtnCtContactPersonName.Appearance.Options.UseFont = true;
            this.BtnCtContactPersonName.Appearance.Options.UseForeColor = true;
            this.BtnCtContactPersonName.Appearance.Options.UseTextOptions = true;
            this.BtnCtContactPersonName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtContactPersonName.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtContactPersonName.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtContactPersonName.Image")));
            this.BtnCtContactPersonName.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtContactPersonName.Location = new System.Drawing.Point(386, 116);
            this.BtnCtContactPersonName.Name = "BtnCtContactPersonName";
            this.BtnCtContactPersonName.Size = new System.Drawing.Size(24, 21);
            this.BtnCtContactPersonName.TabIndex = 21;
            this.BtnCtContactPersonName.ToolTip = "Add Contact Person";
            this.BtnCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtContactPersonName.ToolTipTitle = "Run System";
            this.BtnCtContactPersonName.Click += new System.EventHandler(this.BtnCtContactPersonName_Click_1);
            // 
            // LueCtContactPersonName
            // 
            this.LueCtContactPersonName.EnterMoveNextControl = true;
            this.LueCtContactPersonName.Location = new System.Drawing.Point(154, 116);
            this.LueCtContactPersonName.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtContactPersonName.Name = "LueCtContactPersonName";
            this.LueCtContactPersonName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.Appearance.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtContactPersonName.Properties.DropDownRows = 30;
            this.LueCtContactPersonName.Properties.NullText = "[Empty]";
            this.LueCtContactPersonName.Properties.PopupWidth = 300;
            this.LueCtContactPersonName.Size = new System.Drawing.Size(230, 20);
            this.LueCtContactPersonName.TabIndex = 20;
            this.LueCtContactPersonName.ToolTip = "F4 : Show/hide list";
            this.LueCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtContactPersonName.EditValueChanged += new System.EventHandler(this.LueCtContactPersonName_EditValueChanged_1);
            // 
            // DteQtStartDt
            // 
            this.DteQtStartDt.EditValue = null;
            this.DteQtStartDt.EnterMoveNextControl = true;
            this.DteQtStartDt.Location = new System.Drawing.Point(154, 138);
            this.DteQtStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQtStartDt.Name = "DteQtStartDt";
            this.DteQtStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteQtStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQtStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQtStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQtStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQtStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQtStartDt.Properties.MaxLength = 8;
            this.DteQtStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQtStartDt.Size = new System.Drawing.Size(117, 20);
            this.DteQtStartDt.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(25, 140);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 14);
            this.label8.TabIndex = 22;
            this.label8.Text = "Quotation Start Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(1, 118);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "Customer Contact Person";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(154, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(75, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(154, 27);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(67, 22);
            this.ChkActInd.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(89, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(154, 94);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(230, 20);
            this.LueCtCode.TabIndex = 18;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(154, 51);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(117, 20);
            this.DteDocDt.TabIndex = 14;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(115, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.BtnCtReplace);
            this.panel5.Controls.Add(this.BtnCtReplace2);
            this.panel5.Controls.Add(this.LueContract);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.TxtCtReplace);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.LuePtCode);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.TxtCreditLimit);
            this.panel5.Controls.Add(this.ChkPrintSignatureInd);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.LueSPCode);
            this.panel5.Controls.Add(this.LueCurCode);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(412, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(420, 185);
            this.panel5.TabIndex = 34;
            // 
            // BtnCtReplace
            // 
            this.BtnCtReplace.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtReplace.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtReplace.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtReplace.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtReplace.Appearance.Options.UseBackColor = true;
            this.BtnCtReplace.Appearance.Options.UseFont = true;
            this.BtnCtReplace.Appearance.Options.UseForeColor = true;
            this.BtnCtReplace.Appearance.Options.UseTextOptions = true;
            this.BtnCtReplace.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtReplace.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtReplace.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtReplace.Image")));
            this.BtnCtReplace.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtReplace.Location = new System.Drawing.Point(357, 115);
            this.BtnCtReplace.Name = "BtnCtReplace";
            this.BtnCtReplace.Size = new System.Drawing.Size(24, 21);
            this.BtnCtReplace.TabIndex = 46;
            this.BtnCtReplace.ToolTip = "Find Replacement Quotation ";
            this.BtnCtReplace.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtReplace.ToolTipTitle = "Run System";
            this.BtnCtReplace.Click += new System.EventHandler(this.BtnCtReplace_Click);
            // 
            // BtnCtReplace2
            // 
            this.BtnCtReplace2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtReplace2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtReplace2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtReplace2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtReplace2.Appearance.Options.UseBackColor = true;
            this.BtnCtReplace2.Appearance.Options.UseFont = true;
            this.BtnCtReplace2.Appearance.Options.UseForeColor = true;
            this.BtnCtReplace2.Appearance.Options.UseTextOptions = true;
            this.BtnCtReplace2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtReplace2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtReplace2.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtReplace2.Image")));
            this.BtnCtReplace2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtReplace2.Location = new System.Drawing.Point(386, 116);
            this.BtnCtReplace2.Name = "BtnCtReplace2";
            this.BtnCtReplace2.Size = new System.Drawing.Size(24, 21);
            this.BtnCtReplace2.TabIndex = 47;
            this.BtnCtReplace2.ToolTip = "Show Replacement Quotation";
            this.BtnCtReplace2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtReplace2.ToolTipTitle = "Run System";
            this.BtnCtReplace2.Click += new System.EventHandler(this.BtnCtReplace2_Click);
            // 
            // LueContract
            // 
            this.LueContract.EnterMoveNextControl = true;
            this.LueContract.Location = new System.Drawing.Point(151, 6);
            this.LueContract.Margin = new System.Windows.Forms.Padding(5);
            this.LueContract.Name = "LueContract";
            this.LueContract.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueContract.Properties.Appearance.Options.UseFont = true;
            this.LueContract.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueContract.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueContract.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueContract.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueContract.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueContract.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueContract.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueContract.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueContract.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueContract.Properties.DropDownRows = 30;
            this.LueContract.Properties.NullText = "[Empty]";
            this.LueContract.Properties.PopupWidth = 300;
            this.LueContract.Size = new System.Drawing.Size(262, 20);
            this.LueContract.TabIndex = 27;
            this.LueContract.ToolTip = "F4 : Show/hide list";
            this.LueContract.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueContract.EditValueChanged += new System.EventHandler(this.LueContract_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(59, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 14);
            this.label24.TabIndex = 26;
            this.label24.Text = "Contract Type";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtReplace
            // 
            this.TxtCtReplace.EnterMoveNextControl = true;
            this.TxtCtReplace.Location = new System.Drawing.Point(151, 115);
            this.TxtCtReplace.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtReplace.Name = "TxtCtReplace";
            this.TxtCtReplace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtReplace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtReplace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtReplace.Properties.Appearance.Options.UseFont = true;
            this.TxtCtReplace.Properties.MaxLength = 16;
            this.TxtCtReplace.Size = new System.Drawing.Size(198, 20);
            this.TxtCtReplace.TabIndex = 45;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 118);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 14);
            this.label4.TabIndex = 44;
            this.label4.Text = "Replacement\'s Quotation";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePtCode
            // 
            this.LuePtCode.EnterMoveNextControl = true;
            this.LuePtCode.Location = new System.Drawing.Point(151, 50);
            this.LuePtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePtCode.Name = "LuePtCode";
            this.LuePtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.Appearance.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePtCode.Properties.DropDownRows = 30;
            this.LuePtCode.Properties.NullText = "[Empty]";
            this.LuePtCode.Properties.PopupWidth = 300;
            this.LuePtCode.Size = new System.Drawing.Size(262, 20);
            this.LuePtCode.TabIndex = 39;
            this.LuePtCode.ToolTip = "F4 : Show/hide list";
            this.LuePtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePtCode.EditValueChanged += new System.EventHandler(this.LuePtCode_EditValueChanged_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(44, 53);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 14);
            this.label10.TabIndex = 37;
            this.label10.Text = "Term Of Payment";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCreditLimit
            // 
            this.TxtCreditLimit.EnterMoveNextControl = true;
            this.TxtCreditLimit.Location = new System.Drawing.Point(151, 93);
            this.TxtCreditLimit.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCreditLimit.Name = "TxtCreditLimit";
            this.TxtCreditLimit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCreditLimit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCreditLimit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseFont = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCreditLimit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCreditLimit.Properties.MaxLength = 18;
            this.TxtCreditLimit.Size = new System.Drawing.Size(164, 20);
            this.TxtCreditLimit.TabIndex = 43;
            this.TxtCreditLimit.Validated += new System.EventHandler(this.TxtCreditLimit_Validated);
            // 
            // ChkPrintSignatureInd
            // 
            this.ChkPrintSignatureInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPrintSignatureInd.Location = new System.Drawing.Point(151, 159);
            this.ChkPrintSignatureInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPrintSignatureInd.Name = "ChkPrintSignatureInd";
            this.ChkPrintSignatureInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPrintSignatureInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPrintSignatureInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPrintSignatureInd.Properties.Caption = "Print Signature";
            this.ChkPrintSignatureInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPrintSignatureInd.Size = new System.Drawing.Size(117, 22);
            this.ChkPrintSignatureInd.TabIndex = 50;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(81, 95);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 14);
            this.label19.TabIndex = 42;
            this.label19.Text = "Credit Limit";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(102, 140);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 14);
            this.label28.TabIndex = 48;
            this.label28.Text = "Remark";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(70, 31);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 14);
            this.label16.TabIndex = 28;
            this.label16.Text = "Sales Person";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(151, 137);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 700;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(262, 20);
            this.MeeRemark.TabIndex = 49;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(94, 74);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 14);
            this.label15.TabIndex = 40;
            this.label15.Text = "Currency";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSPCode
            // 
            this.LueSPCode.EnterMoveNextControl = true;
            this.LueSPCode.Location = new System.Drawing.Point(151, 28);
            this.LueSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSPCode.Name = "LueSPCode";
            this.LueSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSPCode.Properties.DropDownRows = 30;
            this.LueSPCode.Properties.NullText = "[Empty]";
            this.LueSPCode.Properties.PopupWidth = 300;
            this.LueSPCode.Size = new System.Drawing.Size(262, 20);
            this.LueSPCode.TabIndex = 29;
            this.LueSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSPCode.EditValueChanged += new System.EventHandler(this.LueSPCode_EditValueChanged_1);
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(151, 71);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 300;
            this.LueCurCode.Size = new System.Drawing.Size(164, 20);
            this.LueCurCode.TabIndex = 41;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged_1);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 481);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 10;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 501);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 11;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // TpgQuotationItem
            // 
            this.TpgQuotationItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgQuotationItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgQuotationItem.Controls.Add(this.LueType);
            this.TpgQuotationItem.Controls.Add(this.LueUomCode);
            this.TpgQuotationItem.Controls.Add(this.Grd2);
            this.TpgQuotationItem.Controls.Add(this.panel4);
            this.TpgQuotationItem.Location = new System.Drawing.Point(4, 26);
            this.TpgQuotationItem.Name = "TpgQuotationItem";
            this.TpgQuotationItem.Size = new System.Drawing.Size(828, 304);
            this.TpgQuotationItem.TabIndex = 1;
            this.TpgQuotationItem.Text = "Item";
            this.TpgQuotationItem.UseVisualStyleBackColor = true;
            // 
            // LueType
            // 
            this.LueType.EnterMoveNextControl = true;
            this.LueType.Location = new System.Drawing.Point(327, 140);
            this.LueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueType.Name = "LueType";
            this.LueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.Appearance.Options.UseFont = true;
            this.LueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueType.Properties.DropDownRows = 20;
            this.LueType.Properties.NullText = "[Empty]";
            this.LueType.Properties.PopupWidth = 500;
            this.LueType.Size = new System.Drawing.Size(171, 20);
            this.LueType.TabIndex = 60;
            this.LueType.ToolTip = "F4 : Show/hide list";
            this.LueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueType.Validated += new System.EventHandler(this.LueType_Validated);
            this.LueType.Leave += new System.EventHandler(this.LueType_Leave);
            // 
            // LueUomCode
            // 
            this.LueUomCode.EnterMoveNextControl = true;
            this.LueUomCode.Location = new System.Drawing.Point(192, 47);
            this.LueUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueUomCode.Name = "LueUomCode";
            this.LueUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUomCode.Properties.DropDownRows = 5;
            this.LueUomCode.Properties.NullText = "[Empty]";
            this.LueUomCode.Properties.PopupWidth = 150;
            this.LueUomCode.Size = new System.Drawing.Size(224, 20);
            this.LueUomCode.TabIndex = 59;
            this.LueUomCode.ToolTip = "F4 : Show/hide list";
            this.LueUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUomCode.Validated += new System.EventHandler(this.LueUomCode_Validated);
            this.LueUomCode.Leave += new System.EventHandler(this.LueUomCode_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 26);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(824, 274);
            this.Grd2.TabIndex = 58;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.ColHdrClick += new TenTec.Windows.iGridLib.iGColHdrClickEventHandler(this.Grd2_ColHdrClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd2_ColHdrDoubleClick);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LblSetDefault);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(824, 26);
            this.panel4.TabIndex = 56;
            // 
            // LblSetDefault
            // 
            this.LblSetDefault.AutoSize = true;
            this.LblSetDefault.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSetDefault.ForeColor = System.Drawing.Color.Green;
            this.LblSetDefault.Location = new System.Drawing.Point(5, 5);
            this.LblSetDefault.Name = "LblSetDefault";
            this.LblSetDefault.Size = new System.Drawing.Size(77, 14);
            this.LblSetDefault.TabIndex = 57;
            this.LblSetDefault.Text = "Set Default";
            this.LblSetDefault.Click += new System.EventHandler(this.LblSetDefault_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgQuotationItem);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 189);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(836, 334);
            this.tabControl1.TabIndex = 55;
            // 
            // FrmCtQt3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 523);
            this.Name = "FrmCtQt3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueContract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtReplace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPrintSignatureInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.TpgQuotationItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Panel panel5;
        public DevExpress.XtraEditors.SimpleButton BtnCtContactPersonName;
        private DevExpress.XtraEditors.LookUpEdit LueCtContactPersonName;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueSPCode;
        internal DevExpress.XtraEditors.DateEdit DteQtStartDt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LuePtCode;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtCreditLimit;
        private DevExpress.XtraEditors.CheckEdit ChkPrintSignatureInd;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        public DevExpress.XtraEditors.SimpleButton BtnCtReplace;
        public DevExpress.XtraEditors.SimpleButton BtnCtReplace2;
        internal DevExpress.XtraEditors.TextEdit TxtCtReplace;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueCtCode;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.DateEdit DteQtEndDt;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueContract;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgQuotationItem;
        private DevExpress.XtraEditors.LookUpEdit LueUomCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label LblSetDefault;
        private DevExpress.XtraEditors.LookUpEdit LueType;
    }
}