﻿#region
//23/08/2017 [HAR] tambah query ke itempackaging untuk dpt packaging unit
//29/08/2017 [HAR] CBD indicator, saat membuat salesinvoice, Otomatis membuat shipping addres, untuk  customer online, Sinkronisasi city JNE dengan City Runsystem
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGetDataWeb : RunSystem.FrmBase3
    {
        #region Field, Property

        private static string ConnectionString2 =
           @"Server=188.166.198.4;Database=molay_runsys_prod;Uid=runsys;Password=HB2rURCQrY9cgpe0*;Allow User Variables=True;Connection Timeout=1200;";
                 

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmGetDataWebFind FrmFind;
        internal int MaxDno = 0;

        #endregion

        #region Constructor

        public FrmGetDataWeb(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Get Data From Web";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetGrd();
                SetFormControl(mState.View);


                base.FrmLoad(sender, e);

                //if this application is called from other application
                //if (mDocNo.Length != 0)
                //{
                //    ShowData(mDocNo);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "ID",

                        //1-5
                        "Sales DocNo", "", "Sales Invoice DocNo", "", 
                    },
                     new int[] 
                    {
                        //0
                        80, 

                        //1-5
                        150, 20, 150, 20
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3 });
            Sm.GrdColButton(Grd1, new int[] { 2, 4 });
            Sm.GrdColInvisible(Grd1, new int[] {2, 4});
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3});
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeRemark
                    }, false);
                    MeeRemark.Focus();
                    break;
                case mState.Edit:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGetDataWebFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            ShowDataWebDtl();

        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {


        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f1 = new FrmSalesInvoice2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f1.ShowDialog();
            }          
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
          if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f1 = new FrmSalesInvoice2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f1.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            
        }

        #endregion

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid() ) return;

            Cursor.Current = Cursors.WaitCursor;

            
            SaveToWebMsi();

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "GetDataWeb", "TblGetDataWebHdr");           

            var cml = new List<MySqlCommand>();
            cml.Add(SaveGetDataWebHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveGetDataWebDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            SaveToLocalHdr();
            UpdateData(DocNo);
            
            ShowData(DocNo);
        }

        private void UpdateData(string DocNo)
        {
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateProcessInd(DocNo));
    
            Sm.ExecCommands(cml);
        }


        private bool IsInsertedDataNotValid()
        {
            return
                 Sm.IsDteEmpty(DteDocDt, "Date") ||
                 IsGrdEmpty() ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No new data order.");
                return true;
            }
            return false;
        }
        #endregion

        #region save data

        private MySqlCommand SaveGetDataWebHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblGetDataWebHdr(DocNo, DocDt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveGetDataWebDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGetDataWebDtl(DocNo, DNo, Id, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Id, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Id", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveGetDataWebSOHdr(string id_order)
        {
            string SODocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SO", "TblSOHdr");
            string SInvDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesInvoice", "TblSalesInvoiceHdr");
            
            var SQL = new StringBuilder();


            SQL.Append("Insert into tblcustomershipaddress(CtCode, Dno, Name, Address, CityCode, CntCode, Postalcd, Phone, Email, CreateBy, CreateDt) ");
            SQL.Append("Select (Select ParValue From tblParameter Where ParCode = 'OnlineCtCode'),  ");
            SQL.Append("(Select Max(cast(Dno as UNSIGNED))+1 From tblCustomerShipaddress Where CtCode='00003975') As Dno, ");
            SQL.Append("A.full_name, A.address, B.CityRun, 'INA', A.PostCode, A.Phone, A.Email, @UserCode, CurrentDateTime()  ");
            SQL.Append("From Msiweb.order_summaries A1 ");
            SQL.Append("Inner Join MsiWeb.order_addresses A  On A1.id = A.Order_id ");
            SQL.Append("Left Join MsiWeb.TblCityConnect B On A.regency_id=B.CityJNe  ");
            SQL.Append("Where A1.id = '" + id_order + "'; ");

            SQL.Append("Insert into TblSOHdr (DocNo, DocDt, Status, cancelInd, OverSeaInd, CtCode, CtContactpersonname, SCourier, ");
            SQL.Append("CtQtDocNo, LocalDocNo, CurCode, Amt, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, CreateBy, CreateDt) ");

            SQL.Append("Select @SODocNo, substring(A.invoice_no, 5, 8) As DocDt, 'O', 'N', 'N', (Select ParValue From tblParameter Where ParCode = 'OnlineCtCode'), 'Online', A.shipping_courier,  ");
            SQL.Append("B.qt_doc_no, A.id, 'IDR', Round(A.total_amount, 2), C.full_name, C.address, D.CityRun, 'INA', ");
            SQL.Append("C.postcode, @UserCode, CurrentDateTime() ");
            SQL.Append("from Msiweb.order_summaries A ");
            SQL.Append("Inner Join MsiWeb.order_details B On A.id = B.order_id ");
            SQL.Append("Inner Join MsiWeb.order_addresses C On A.id = C.order_id ");
            SQL.Append("Left Join MsiWeb.TblCityConnect D On C.regency_id=D.CityJNe  ");
            SQL.Append("Where A.process_indicator = 'N' And A.Status = 'O' And A.id = '" + id_order + "' ");
            SQL.Append("Group by A.id; ");

            SQL.Append("Insert Into Tblsalesinvoicehdr (DocNo, DocDt, CancelInd, ProcessInd, CtCode, LocalDocNo, SODocNo, DueDt, ");
            SQL.Append("CurCode, TotalAmt, TotalTax, Amt, MInd, SalesName, CBDInd, CreateBy, CreateDt) ");

            SQL.Append("Select distinct @SInvDocNo, substring(A.invoice_no, 5, 8) As DocDt, 'N', 'O', (Select ParValue From tblParameter Where ParCode = 'OnlineCtCode'), ");
            SQL.Append("A.id, @SODocNo, replace(Date_add(substring(A.invoice_no, 5, 8), interval 2 day), '-', ''), 'IDR', ");
            SQL.Append("Round(E.TotalAmt, 2), Round(E.TotalTax, 2) As TotalTax, Round((A.total_invoice-A.unique_code),2) As total_invoice, 'Y', (Select SpName From TblSalesPerson Order by CreateDt limit 1), 'Y', @UserCode, CurrentDateTime() ");
            SQL.Append("from Msiweb.order_summaries A ");
            SQL.Append("Inner Join MsiWeb.order_details B On A.id = B.order_id ");
            SQL.Append("Inner Join MsiWeb.order_addresses C On A.id = C.order_id ");
            SQL.Append("Inner Join  ");
            SQL.Append("    ( ");
            SQL.Append("    Select item_code, unit_price From msiWeb.item_info  ");
            SQL.Append("    )D On B.item_code = D.item_code ");
            SQL.Append("Inner Join  ");
            SQL.Append("( ");
            SQL.Append("    Select T.order_id, Sum(T.DiscAmt) As DiscAmt, Sum(T.AmtQty) As totalAmt, SUM(T.AmtTax) As TotalTax ");
            SQL.Append("         from  ");
            SQL.Append("         ( ");
            SQL.Append("            Select order_id, Qty*(Price*Discount/100) As DiscAmt, (Qty*Price) As AmtQty, (Qty*(Price*0.1)) As AmtTax  ");// (Qty*(Price/1.1)) As AmtQty, (Qty*((Price/1.1)*0.1)) As AmtTax ");
            SQL.Append("            from MsiWeb.order_details A ");
            SQL.Append("            Where order_id = '" + id_order + "' ");
            SQL.Append("         )T ");
            SQL.Append("    Group By T.order_id ");
            SQL.Append(")E On A.id = E.order_id ");
            SQL.Append("Where A.process_indicator = 'N' And A.Status = 'O'And A.id = '" + id_order + "' ; ");

            SQL.Append("Insert Into TblsalesInvoiceDtl2 (DocNo, Dno, AcNo, Damt, cAmt, CreateBy, CreateDt) ");
            SQL.Append("Select @SInvDocNo, '001', (Select ifnull(ParValue, '-') from TblParameter Where parCode = 'CBDFreightAcNo') As Acno, Round(shipping_charge, 0), 0, @UserCode, CurrentDateTime() ");
            SQL.Append("From Msiweb.order_summaries Where id='" + id_order + "' ");
            SQL.Append("Union All ");
            SQL.Append("Select @SInvDocNo, '002', (Select ifnull(ParValue, '-') from TblParameter Where parCode = 'CBDFreightAcNoX') As Acno, 0, Round(shipping_charge, 0), @UserCode, CurrentDateTime() ");
            SQL.Append("From Msiweb.order_summaries Where id='" + id_order + "' ");
            SQL.Append("Union All ");
            SQL.Append("Select @SInvDocNo, '003', (Select ifnull(ParValue, '-') from TblParameter Where parCode = 'CBDUniqueCodeAcNo') As Acno, unique_code, 0, @UserCode, CurrentDateTime() ");
            SQL.Append("From Msiweb.order_summaries Where id='" + id_order + "' ");
            SQL.Append("Union All ");
            SQL.Append("Select @SInvDocNo, '004', (Select ifnull(ParValue, '-') from TblParameter Where parCode = 'CBDUniqueCodeAcNoX') As Acno, 0, unique_code, @UserCode, CurrentDateTime() ");
            SQL.Append("From Msiweb.order_summaries Where id='" + id_order + "' ; ");

            SQL.Append("Update TblGetDataWebDtl ");
            SQL.Append("Set SODocNo=@SODocNo, SalesInvoiceDocNo=@SInvDocNo Where Id='" + id_order + "'; ");


            SaveToLocalDtl(id_order, SODocNo, SInvDocNo);

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SODocNo", SODocNo);
            Sm.CmParam<String>(ref cm, "@SInvDocNo", SInvDocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private static MySqlCommand SaveGetDataWebSODtl(string SODocNo, string SInvDocNo, string id_order, int CountData)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert into tblSODtl (DocNo, DNo, CtQtDNo,PackagingUnitUomCode,QtyPackagingUnit,Qty,TaxRate,CreateBy,CreateDt)");

            SQL.Append("Select @SODocNo, @DNo, B.qt_d_no, ifnull(E.UomCode, 'Pcs'), (E.Qty * B.qty) As QtyPackagingUnit, ");
            SQL.Append("B.Qty, B.tax, @UserCode, CurrentDateTime() ");
            SQL.Append("from Msiweb.order_summaries A ");
            SQL.Append("Inner Join MsiWeb.order_details B On A.id = B.order_id ");
            SQL.Append("Inner Join MsiWeb.order_addresses C On A.id = C.order_id ");
            SQL.Append("Inner Join MsiWeb.item_summaries D On B.item_code = D.item_code ");
            SQL.Append("Left Join TblItemPackagingunit E On B.item_code = E.ItCode ");
            SQL.Append("Where A.process_indicator = 'N' And Status = 'O' And B.id = '" + id_order + "'; ");


            SQL.Append("Insert Into TblSalesInvoiceDtl (DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, QtyPackagingUnit, Qty, ");
            SQL.Append("UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, CreateBy, CreateDt)  ");

            SQL.Append("Select @SinvDocNo, @DNo, '1', @SODocNo,  @Dno, 'N', B.item_code, ");
            SQL.Append("(F.Qty * B.qty) As QtyPackagingUnit, B.Qty, Round(E.unit_price, 2), B.tax, round((E.unit_price*0.1),2),   ");
            SQL.Append("Round(B.price,2) , @UserCode, CurrentDateTime() ");
            SQL.Append("from Msiweb.order_summaries A ");
            SQL.Append("Inner Join MsiWeb.order_details B On A.id = B.order_id ");
            SQL.Append("Inner Join MsiWeb.order_addresses C On A.id = C.order_id ");
            SQL.Append("Inner Join MsiWeb.item_summaries D On B.item_code = D.item_code ");
            SQL.Append("Inner Join  ");
            SQL.Append("    ( ");
            SQL.Append("    Select item_code, unit_price From msiWeb.item_info  ");
            SQL.Append("    )E On B.item_code = E.item_code ");
            SQL.Append("Left Join TblItemPackagingunit F On B.item_code = F.ItCode ");
            SQL.Append("Where A.process_indicator = 'N' And Status = 'O' And B.id = '" + id_order + "';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SODocNo", SODocNo);
            Sm.CmParam<String>(ref cm, "@SinvDocNo", SInvDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (CountData + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@userCode", Gv.CurrentUserCode);

            return cm;
        }

        private static MySqlCommand UpdateProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update MsiWeb.order_summaries Set process_indicator = 'Y' ");
            SQL.AppendLine("Where id In (Select Id From tblgetDataWebDtl Where DocNo = @DocNo) ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private static void SaveToWebMsi()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.Append("Select id From order_summaries ");
            SQL.Append("Where status = 'O' And process_indicator = 'N'");

            using (var cn = new MySqlConnection(ConnectionString2))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "id",  
                         
                        });
                if (dr.HasRows)
                {
                    var cml = new List<MySqlCommand>();
                    var cml2 = new List<MySqlCommand>();
                    while (dr.Read())
                    {
                        string Id_Order = Sm.DrStr(dr, c[0]);
                        var Data3 = new List<Order_addresses>();
                        SetOrderAddresses(ref Data3, Id_Order);
                        Data3.ForEach(i => { cml.Add(SaveOrderAddress(i)); });

                        var Data4 = new List<Order_Details>();
                        SetOrderDetails(ref Data4, Id_Order);
                        Data4.ForEach(i => { cml.Add(SaveOrderDetails(i)); });

                        var Data5 = new List<Order_Summaries>();
                        SetOrderSummaries(ref Data5, Id_Order);
                        Data5.ForEach(i => { cml.Add(SaveOrderSummaries(i)); });

                        cml2.Add(UpdateProcessIndWeb(Id_Order));
                        ExecCommands2(cml2);
                    }
                    Sm.ExecCommands(cml);
                }
                dr.Close();
            }
        }

        private static MySqlCommand SaveOrderAddress(Order_addresses i)
        {
            var SQL = new StringBuilder();

            SQL.Append("Insert Into msiweb.Order_addresses ");
            SQL.Append("(order_id, full_name, email, address, province_id, regency_id, district_id, village, postcode, phone) ");
            SQL.Append("Values (@order_id, @full_name, @email, @address, @province_id, @regency_id, @district_id, @village, @postcode, @phone) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@order_id", i.order_id);
            Sm.CmParam<String>(ref cm, "@full_name", i.full_name);
            Sm.CmParam<String>(ref cm, "@email", i.email);
            Sm.CmParam<String>(ref cm, "@address", i.address);
            Sm.CmParam<String>(ref cm, "@province_id", i.province_id);
            Sm.CmParam<String>(ref cm, "@regency_id", i.regency_id);
            Sm.CmParam<String>(ref cm, "@district_id", i.district_id);
            Sm.CmParam<String>(ref cm, "@village", i.village);
            Sm.CmParam<String>(ref cm, "@postcode", i.postcode);
            Sm.CmParam<String>(ref cm, "@phone", i.phone);
            return cm;
        }

        private static MySqlCommand SaveOrderDetails(Order_Details i)
        {
            var SQL = new StringBuilder();


            SQL.Append("Insert Into msiweb.order_details ");
            SQL.Append("(order_id, item_code, qty, qt_doc_no, qt_d_no, price, discount, tax ) ");
            SQL.Append("Values (@order_id, @item_code, @qty, @qt_doc_no, @qt_d_no, @price, @discount, @tax ) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            //Sm.CmParam<String>(ref cm, "@id", i.id);
            Sm.CmParam<String>(ref cm, "@order_id", i.order_id);
            Sm.CmParam<String>(ref cm, "@item_code", i.item_code);
            Sm.CmParam<Decimal>(ref cm, "@qty", i.qty);
            Sm.CmParam<String>(ref cm, "@qt_doc_no", i.qt_doc_no);
            Sm.CmParam<String>(ref cm, "@qt_d_no", i.qt_d_no);
            Sm.CmParam<Decimal>(ref cm, "@price", i.price);
            Sm.CmParam<Decimal>(ref cm, "@discount", i.discount);
            Sm.CmParam<Decimal>(ref cm, "@tax", i.tax);
            return cm;
        }

        private static MySqlCommand SaveOrderSummaries(Order_Summaries i)
        {
            var SQL = new StringBuilder();


            SQL.Append("Insert Into MsiWeb.order_summaries ");
            SQL.Append("(id, user_id, invoice_no, total_qty, total_weight, total_amount, shipping_courier, shipping_charge, ");
            SQL.Append("unique_code, total_invoice, payment_bank, status, process_indicator, created, updated) ");
            SQL.Append("Values ");
            SQL.Append("(@id, @user_id, @invoice_no, @total_qty, @total_weight, @total_amount, @shipping_courier, @shipping_charge, ");
            SQL.Append("@unique_code, @total_invoice, @payment_bank, @status, 'N', @created, @updated) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@id", i.id);
            //Sm.CmParam<String>(ref cm, "@user_id", i.user_id);
            Sm.CmParam<String>(ref cm, "@invoice_no", i.invoice_no);
            Sm.CmParam<decimal>(ref cm, "@total_qty", i.total_qty);
            Sm.CmParam<decimal>(ref cm, "@total_weight", i.total_weight);
            Sm.CmParam<decimal>(ref cm, "@total_amount", i.total_amount);
            Sm.CmParam<String>(ref cm, "@shipping_courier", i.shipping_courier);
            Sm.CmParam<decimal>(ref cm, "@shipping_charge", i.shipping_charge);
            Sm.CmParam<decimal>(ref cm, "@unique_code", i.unique_code);
            Sm.CmParam<decimal>(ref cm, "@total_invoice", i.total_invoice);
            Sm.CmParam<String>(ref cm, "@payment_bank", i.payment_bank);
            Sm.CmParam<String>(ref cm, "@status", i.status);
            //Sm.CmParam<String>(ref cm, "@process_indicator",i.process_indicator); 
            Sm.CmParam<String>(ref cm, "@created", i.created);
            //Sm.CmParam<String>(ref cm, "@updateD", i.updated);

            return cm;
        }

        private static MySqlCommand UpdateProcessIndWeb(string id_order)
        {
            var SQL = new StringBuilder();

            SQL.Append("UPDATE order_summaries set process_indicator = 'Y' Where id = '" + id_order + "' ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            return cm;
        }

        public void SaveToLocalHdr()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.Append("Select id From MsiWeb.order_summaries ");
            SQL.Append("Where status = 'O' And process_indicator = 'N'");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "id",  
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        var cml = new List<MySqlCommand>();
                        string Id_Order = Sm.DrStr(dr, c[0]);
                        cml.Add(SaveGetDataWebSOHdr(Id_Order));
                        Sm.ExecCommands(cml);
                    }
                }
                dr.Close();
            }
        }

        public void SaveToLocalDtl(string order_id, string SODocNo, string SInvDocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.Append("Select id From MsiWeb.order_details ");
            SQL.Append("Where order_id = '" + order_id + "'");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "id",  
                        });
                if (dr.HasRows)
                {
                    int CountData = -1;
                    while (dr.Read())
                    {
                        string Id_Order = Sm.DrStr(dr, c[0]);
                        CountData = CountData + 1;
                        var cml = new List<MySqlCommand>();
                        cml.Add(SaveGetDataWebSODtl(SODocNo, SInvDocNo, Id_Order, CountData));
                        Sm.ExecCommands(cml);
                    }
                }
                dr.Close();
            }
        }


        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowGetDataWebHdr(DocNo);
                ShowGetDataWebDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowGetDataWebHdr(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.Remark From TblGetDataWebHdr A " +
                    "Where A.DocNo=@DocNo ",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[2]);
                    }, true
                );
        }

        private void ShowGetDataWebDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Id, A.SODocNo, A.SalesInvoiceDocNo  ");
            SQL.AppendLine("From TblGetDataWebDtl A ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "Id", 

                        //1-5
                        "SODocNo", "SalesInvoiceDocNo"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                }, false, false, true, false );
           }

        private void ShowDataWebDtl()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.Append("Select Cast(ID As char) As id From order_summaries ");
            SQL.Append("Where status = 'O' And process_indicator = 'N'");

            using (var cn = new MySqlConnection(ConnectionString2))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                        //0
                        "id",
                    });
                if (dr.HasRows)
                {
                    int Row = 0;
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Row, 0].Value = dr.GetString(c[0]);
                        Row = Row + 1;
                    }
                }
                dr.Close();
            }
           }
        
        #endregion

        #region Additional Method

        private static void ExecCommands2(List<MySqlCommand> cml)
        {
            using (var cn = new MySqlConnection(ConnectionString2))
            {
                cn.Open();
                var tr = cn.BeginTransaction();
                try
                {
                    cml.ForEach
                    (cm =>
                    {
                        cm.Connection = cn;
                        cm.CommandTimeout = 600;
                        cm.Transaction = tr;
                        cm.ExecuteNonQuery();
                    }
                    );
                    //tr.Rollback();
                    tr.Commit();
                }
                catch (Exception Exc)
                {
                    tr.Rollback();
                    throw new Exception(Exc.Message);
                }
                finally
                {
                    tr = null;
                }
            }
        }

        #region Set list

        private static void SetOrderAddresses(ref List<Order_addresses> Data3, string id_order)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.Append("Select order_id, full_name, email, address, province_id, regency_id, district_id, village, postcode, phone From order_addresses Where order_id = '" + id_order + "' ");

            using (var cn = new MySqlConnection(ConnectionString2))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "order_id",  
                         //1-5
                         "full_name", "email", "address", "province_id", "regency_id",   
                         //6-9
                         "district_id", "village", "postcode", "phone"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Data3.Add(new Order_addresses()
                        {
                            order_id = Sm.DrStr(dr, c[0]),
                            full_name = Sm.DrStr(dr, c[1]),
                            email = Sm.DrStr(dr, c[2]),
                            address = Sm.DrStr(dr, c[3]),
                            province_id = Sm.DrStr(dr, c[4]),
                            regency_id = Sm.DrStr(dr, c[5]),
                            district_id = Sm.DrStr(dr, c[6]),
                            village = Sm.DrStr(dr, c[7]),
                            postcode = Sm.DrStr(dr, c[8]),
                            phone = Sm.DrStr(dr, c[9]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private static void SetOrderDetails(ref List<Order_Details> Data4, string order_id)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.Append("Select order_id, item_code, qty, qt_doc_no, qt_d_no, cast(price As decimal(12, 2)) As price,  cast(discount As decimal(12, 2)) As discount,  cast(tax As decimal(12, 2)) As tax From order_details Where order_id = '" + order_id + "' ");

            using (var cn = new MySqlConnection(ConnectionString2))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "order_id",  
                         //1-5
                         "item_code", "qty", "qt_doc_no", "qt_d_no", "price",   
                         //6
                         "discount", "tax"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Data4.Add(new Order_Details()
                        {
                            order_id = Sm.DrStr(dr, c[0]),
                            item_code = Sm.DrStr(dr, c[1]),
                            qty = Sm.DrDec(dr, c[2]),
                            qt_doc_no = Sm.DrStr(dr, c[3]),
                            qt_d_no = Sm.DrStr(dr, c[4]),
                            price = Sm.DrDec(dr, c[5]),
                            discount = Sm.DrDec(dr, c[6]),
                            tax = Sm.DrDec(dr, c[7]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private static void SetOrderSummaries(ref List<Order_Summaries> Data5, string id_order)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.Append("Select id, invoice_no, total_qty, total_weight, cast(total_amount As decimal(12, 2)) As total_amount, shipping_courier, shipping_charge, ");
            SQL.Append("unique_code, cast(total_invoice As decimal(12, 2)) As total_invoice, payment_bank, status, cast(created as Char) as created From order_summaries Where id = '" + id_order + "' ");

            using (var cn = new MySqlConnection(ConnectionString2))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                        "invoice_no", 
                        //1-5
                        "total_qty", "total_weight", "total_amount", "shipping_courier", "shipping_charge", 
                        //6-10
                        "unique_code", "total_invoice", "payment_bank", "status", "id",
                        //11
                        "created"  
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Data5.Add(new Order_Summaries()
                        {
                            invoice_no = Sm.DrStr(dr, c[0]),
                            total_qty = Sm.DrDec(dr, c[1]),
                            total_weight = Sm.DrDec(dr, c[2]),
                            total_amount = Sm.DrDec(dr, c[3]),
                            shipping_courier = Sm.DrStr(dr, c[4]),
                            shipping_charge = Sm.DrDec(dr, c[5]),
                            unique_code = Sm.DrDec(dr, c[6]),
                            total_invoice = Sm.DrDec(dr, c[7]),
                            payment_bank = Sm.DrStr(dr, c[8]),
                            status = Sm.DrStr(dr, c[9]),
                            id = Sm.DrStr(dr, c[10]),
                            created = Sm.DrStr(dr, c[11])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion   

        #endregion

     
        #endregion

    }

    #region Additional Class


    class Order_addresses
    {
        public string id { get; set; }
        public string order_id { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string province_id { get; set; }
        public string regency_id { get; set; }
        public string district_id { get; set; }
        public string village { get; set; }
        public string postcode { get; set; }
        public string phone { get; set; }


    }

    class Order_Details
    {
        public string id { get; set; }
        public string ct_code { get; set; }
        public string order_id { get; set; }
        public string item_code { get; set; }
        public decimal qty { get; set; }
        public string qt_doc_no { get; set; }
        public string qt_d_no { get; set; }
        public decimal price { get; set; }
        public decimal discount { get; set; }
        public decimal tax { get; set; }
        
    }

    class Order_Summaries
    {
        public string id { get; set; }
        public string user_id { get; set; }
        public string invoice_no { get; set; }
        public decimal total_qty { get; set; }
        public decimal total_weight { get; set; }
        public decimal total_amount { get; set; }
        public string shipping_courier { get; set; }
        public decimal shipping_charge { get; set; }
        public decimal unique_code { get; set; }
        public decimal total_invoice { get; set; }
        public string payment_bank { get; set; }
        public string status { get; set; }
        public string process_indicator { get; set; }
        public string created { get; set; }
        public string updated { get; set; }
    }

    class Id_Order
    {
        public string id_order { get; set; }
    }
    #endregion

}
