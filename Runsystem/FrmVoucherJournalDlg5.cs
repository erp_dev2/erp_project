﻿#region Update
/*
    23/02/2021 [TKG/PHT] saat membuka layar, tidak langsung menampilkan data.
    02/03/2021 [BRI/PHT] tambah parameter IsCostCenterShowChildOnly = Y tampil cost center paling bontot
    15/08/2022 [BRI/PHT] COA filter by group berdasarkan param IsJournalVoucherFilteredByGroup
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherJournalDlg5 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherJournal mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherJournalDlg5(FrmVoucherJournal FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode, A.CCName, B.DeptName ");
            SQL.AppendLine("From TblCostCenter A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            if (mFrmParent.mIsCostCenterShowChildOnly)
            {
                SQL.AppendLine("AND A.Parent Is Not Null ");
                SQL.AppendLine("AND A.CCCode Not In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("SELECT Parent From TblCostCenter Where Parent Is Not Null ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsCostCenterFilteredByGroup || mFrmParent.mIsJournalVoucherFilteredByGroup)
            {
                SQL.AppendLine("And (A.CCCode Is Null Or (A.CCCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=A.CCCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            if (mFrmParent.mIsVoucherJournalUseProfitCenter)
            {
                SQL.AppendLine("And A.NotParentInd='Y' ");
                SQL.AppendLine("And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                SQL.AppendLine("    Select CCCode ");
                SQL.AppendLine("    From TblCostCenter ");
                SQL.AppendLine("    Where ActInd='Y' ");
                SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And ProfitCenterCode In (");
                SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    )))) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-3
                    "Cost Center#",
                    "Cost Center Name",
                    "Department"
                },
                new int[]
                {
                    50, 
                    130, 200, 200
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, new string[] { "A.CCCode", "A.CCName" });
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By A.CCName; ",
                    new string[] 
                    { 
                        "CCCode", 
                        "CCName", "DeptName",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtCCCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtCCName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost Center#");
        }

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
