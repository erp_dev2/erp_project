﻿#region Update
/* 
    10/10/2017 [TKG] 
        menggunakan parameter IsFilterBySiteHR untuk filter site berdasarkan group
        menggunakan parameter IsFilterByDeptHR untuk filter department berdasarkan group
    20/10/2017 [TKG] bug fixing validasi data dengan periode yg sama tidak boleh disimpan lbh dari 1x.
    23/10/2017 [HAR] function relistemp tmbh process5 dan process6 untuk melist employee leave, dan row dikasih warna biru jika ada leavenya  
    04/12/2017 [TKG] tambah remark dtl
    04/01/2017 [HAR] validasi saat input in dan out jika sesuai WS maka row nya tidak merah begitu pula sebalikany
    09/02/2017 [HAR] bug fixing rounding time parameter jika param  = 0 maka actual defaultnya sama kayak ceklog
    09/05/2018 [WED] tambah set warna hijau untuk leave yang dapat uang makan & transport, warna biru untuk leave yang tidak dapat uang makan & transport
    11/05/2018 [WED] kalau pas klik refresh selain saat insert, nggak mengkalkulasi ulang jam attendance nya. cuma menampilkan warna aja
    20/08/2018 [HAR] tambah informasi log meskipun working schdulenya holiday 
    23/08/2018 [HAR] bug informasi log meskipun working schdulenya holiday : tambah pengecekan yang WS lengthnya kurang dari 8
    01/10/2018 [HAR] minta dimunculin leavenya waktu show data
    28/05/2019 [DITA] kolom employee di freeze
    24/10/2019 [RF/SIER] menambahkan informasi warna untuk leave berdasarkan parameter IsAtdVerificationLeaveColorActive (yang dirubah di proses5 dan proses6) 
    13/11/2019 [TKG/MMM] berdasarkan parameter IsAtdSystemTypeValidationEnabled, karyawan yg mau diproses perlu dicek system typenya atau tidak.
    10/12/2019 [DITA/KIM] tambah fasilitas export to excel
    27/03/2020 [HAR/MMM] tambah paraemter untuk bisa membuat periode beda bulan dalam 1 document IsAtdVerificationAllowToDifferentMonth
    08/05/2020 [WED/KIM] export excel disesuaikan dengan tampilan di grid depan
    03/06/2020 [IBL/SIER] menambahkan keterangan OT
    08/07/2020 [HAR/MMM] Parameter AtdVerificationActualInOutSource untuk setting mengambil default actual in out dari mana (1=ceklog, 2 dari=jadwal)
    02/10/2020 [VIN/PHT] Parameter IsAtdActualInputDisabled, jika Y actual in/out disabled
    03/05/2021 [BRI/IMS] Parameter IsAttendanceVerificationUseLeaveRequestGroup, if Y = menyambungkan attendance verification dengan group of leave
    11/01/2022 [TKG/IMS] ubah GetParameter
    12/01/2022 [TKG/IMS] tambah log vs ws (out dan akumulasi)
    19/01/2022 [TKG/IMS] ubah proses save
    31/01/2022 [TKG/IMS] ubah rumus perhitungan akumulasi
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;

using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

#endregion

namespace RunSystem
{
    public partial class FrmAtd : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmAtdFind FrmFind;
        internal bool
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsAtdVerificationAllowToDifferentMonth =false,
            mIsAtdShowOT = false;
        private bool
            mIsSiteMandatory = false,
            mIsAtdShowDataLogWhenHoliday = false,
            mIsAtdVerificationLeaveColorActive = false,
            mIsAtdSystemTypeValidationEnabled = false,
            mIsAtdActualInputDisabled = false,
            mIsAttendanceVerificationUseLeaveRequestGroup = false,
            mIsAtdShowLogVsWSAccumulation = false
            ;
        decimal Delay = 0m, RoundSch = 0m;
        private string mLeaveCodeGetMealTransport = string.Empty;
        private string mAtdActualInfoType = string.Empty;
        private int mStateInd = 0;
        private string mAtdVerificationActualInOutSource = string.Empty;

        private static Excel.Application objApp;
        private static Excel.Workbooks objBooks;
        private static Excel.Workbook objBook;
        private static Excel.Sheets objSheets;
        private static Excel.Worksheet objSheet;

        internal List<OTList> mlOT = new List<OTList>();
        
        #endregion

        #region Constructor

        public FrmAtd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Attendance";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueAGCode(ref LueAGCode);
                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
         
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsFilterByDeptHR', 'IsFilterBySiteHR', 'IsSiteMandatory', 'LeaveCodeGetMealTransport', 'IsAtdShowLogVsWSAccumulation', ");
            SQL.AppendLine("'IsAttendanceVerificationUseLeaveRequestGroup', 'IsAtdActualInputDisabled', 'IsAtdShowOT', 'AtdVerificationActualInOutSource', 'AtdActualInfoType', ");
            SQL.AppendLine("'DelayTolerance', 'IsAtdVerificationAllowToDifferentMonth', 'IsAtdSystemTypeValidationEnabled', 'IsAtdVerificationLeaveColorActive', 'IsAtdShowDataLogWhenHoliday', ");
            SQL.AppendLine("'RoundingValueSchedule' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();

                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsAtdShowLogVsWSAccumulation": mIsAtdShowLogVsWSAccumulation = ParValue == "Y"; break;
                            case "IsAttendanceVerificationUseLeaveRequestGroup": mIsAttendanceVerificationUseLeaveRequestGroup = ParValue == "Y"; break;
                            case "IsAtdActualInputDisabled": mIsAtdActualInputDisabled = ParValue == "Y"; break;
                            case "IsAtdShowOT": mIsAtdShowOT = ParValue == "Y"; break;
                            case "IsAtdVerificationAllowToDifferentMonth": mIsAtdVerificationAllowToDifferentMonth = ParValue == "Y"; break;
                            case "IsAtdSystemTypeValidationEnabled": mIsAtdSystemTypeValidationEnabled = ParValue == "Y"; break;
                            case "IsAtdVerificationLeaveColorActive": mIsAtdVerificationLeaveColorActive = ParValue == "Y"; break;
                            case "IsAtdShowDataLogWhenHoliday": mIsAtdShowDataLogWhenHoliday = ParValue == "Y"; break;

                            //string
                            case "LeaveCodeGetMealTransport": mLeaveCodeGetMealTransport = ParValue; break;
                            case "AtdActualInfoType": mAtdActualInfoType = ParValue; break;
                            case "AtdVerificationActualInOutSource": mAtdVerificationActualInOutSource = ParValue; break;

                            //decimal
                            case "DelayTolerance": 
                                if (ParValue.Length>0) Delay = decimal.Parse(ParValue); 
                                break;
                            case "RoundingValueSchedule":
                                if (ParValue.Length > 0) RoundSch = decimal.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }

        }

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 6;

            SetGrdHdr(ref Grd1, 0, 0, "Employee" + Environment.NewLine + "Code", 2, 80);
            SetGrdHdr(ref Grd1, 0, 1, "Employee Old" + Environment.NewLine + "Code", 2, 100);
            SetGrdHdr(ref Grd1, 0, 2, "Employee", 2, 150);
            SetGrdHdr(ref Grd1, 0, 3, "", 2, 20);
            SetGrdHdr(ref Grd1, 0, 4, "Department", 2, 150);
            SetGrdHdr(ref Grd1, 0, 5, "Date", 2, 80);
            SetGrdHdr2(ref Grd1, 1, 6, "Attendance" + Environment.NewLine + "Log", 2);
            SetGrdHdr2(ref Grd1, 1, 8, "Manual" + Environment.NewLine + "Attendance", 2);
            SetGrdHdr2(ref Grd1, 1, 10, "Working" + Environment.NewLine + "Schedule", 2);
            SetGrdHdr(ref Grd1, 0, 12, "Attendance" + Environment.NewLine + "VS" + Environment.NewLine + " Manual", 2, 100);
            SetGrdHdr(ref Grd1, 0, 13, "Attendance" + Environment.NewLine + "VS" + Environment.NewLine + "Working Schedule" + Environment.NewLine + "(In)", 2, 150);
            SetGrdHdr2(ref Grd1, 1, 14, "Actual (Input)", 2);
            SetGrdHdr3(ref Grd1, 1, 16, "Actual", 4);
            SetGrdHdr(ref Grd1, 0, 20, "Date", 2, 80);
            SetGrdHdr(ref Grd1, 0, 21, "Resign Date", 2, 80);
            SetGrdHdr(ref Grd1, 0, 22, "No", 2, 30);
            SetGrdHdr(ref Grd1, 0, 23, "Check", 2, 50);
            SetGrdHdr(ref Grd1, 0, 24, "OT Out", 2, 80);
            SetGrdHdr(ref Grd1, 0, 25, "Date Out", 2, 80);
            SetGrdHdr(ref Grd1, 0, 26, "Info", 2, 150);
            SetGrdHdr(ref Grd1, 0, 27, "Remark", 2, 200);
            SetGrdHdr(ref Grd1, 0, 28, "IN Old", 2, 80);
            SetGrdHdr(ref Grd1, 0, 29, "OUT Old", 2, 80);
            SetGrdHdr(ref Grd1, 0, 30, "OT#", 2, 150);
            SetGrdHdr(ref Grd1, 0, 31, "Attendance" + Environment.NewLine + "VS" + Environment.NewLine + "Working Schedule" + Environment.NewLine + "(Out)", 2, 150);
            SetGrdHdr(ref Grd1, 0, 32, "Attendance" + Environment.NewLine + "VS" + Environment.NewLine + "Working Schedule" + Environment.NewLine + "(Accumulation)", 2, 180);

            Sm.GrdFormatDate(Grd1, new int[] { 5, 16, 18, 20, 25 });
            Sm.GrdFormatTime(Grd1, new int[] { 6, 7, 8, 9, 10, 11});
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 30 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            if (!mIsAtdShowOT)
                Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 5, 8, 9, 12, 21, 24, 25, 28, 29, 30 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 5, 8, 9, 12, 21, 24, 25, 28, 29 }, false);

            if (!mIsAtdShowLogVsWSAccumulation)
                Sm.GrdColInvisible(Grd1, new int[] { 31, 32 }, false);

            Sm.GrdColCheck(Grd1, new int[] { 23 });
            Grd1.Cols[22].Move(0);
            Grd1.Cols[23].Move(1);
            Grd1.Cols[20].Move(2);
            if (mIsAtdShowLogVsWSAccumulation)
            {
                Grd1.Cols[32].Move(17);
                Grd1.Cols[31].Move(17);
            }
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "OUT", 1, 80);
        }

        private void SetGrdHdr3(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "Date IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 1, "Time IN", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 2, "Date OUT", 1, 80);
            SetGrdHdr(ref Grd1, 0, col + 3, "Time OUT", 1, 80);
        }

        private void ChkHideInfoInGrd_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 8, 9, 12 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueDeptCode, LueSiteCode, LueAGCode, DteStartDt, 
                        DteEndDt, LueYr, LueMth, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 26, 27, 28, 29, 31, 32 });
                    DteStartDt.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueSiteCode, LueAGCode, DteStartDt, DteEndDt, 
                        LueYr, LueMth, MeeRemark
                    }, false);
                    if(mIsAtdActualInputDisabled)
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 23, 27 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 14, 15, 23, 27 });

                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    if (mIsAtdActualInputDisabled)
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 27 });
                    else
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 14, 15, 27 });
                    ChkCancelInd.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueDeptCode, LueSiteCode, LueAGCode, DteStartDt, DteEndDt,
                LueYr, LueMth, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAtdFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                Sm.SetDteCurrentDate(DteDocDt);
                DteStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteEndDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());

                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR?"Y":"N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR?"Y":"N");
                mStateInd = 1;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateInd = 2;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    UpdateData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnExcelClick(object sender, EventArgs e)
        {
            ExportToExcel();
        }


        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                if (BtnSave.Enabled || TxtDocNo.Text.Length > 0)
                {
                    if (
                        (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                        Sm.IsLueEmpty(LueDeptCode, "Department") ||
                        IsGrdMonthNotValid() ||
                        IsGrdMonthNotValid2()
                        ) return;

                    Sm.ClearGrd(Grd1, false);
                    DateTime Dt1 = new DateTime(
                           Int32.Parse(Sm.GetDte(DteStartDt).Substring(0, 4)),
                           Int32.Parse(Sm.GetDte(DteStartDt).Substring(4, 2)),
                           Int32.Parse(Sm.GetDte(DteStartDt).Substring(6, 2)),
                           0, 0, 0
                           );

                    DateTime Dt2 = new DateTime(
                        Int32.Parse(Sm.GetDte(DteEndDt).Substring(0, 4)),
                        Int32.Parse(Sm.GetDte(DteEndDt).Substring(4, 2)),
                        Int32.Parse(Sm.GetDte(DteEndDt).Substring(6, 2)),
                        0, 0, 0
                        );

                    var range = (Dt2 - Dt1).Days;

                    ListOfEmp();

                    if (range >= 0)
                    {
                        //ReListEmp(range);
                        ReListEmp();
                    }

                    // kalau sudah ada DocNo nya
                    if (TxtDocNo.Text.Length > 0 && mStateInd != 1)
                    {
                        ReListDataDetail(TxtDocNo.Text);
                        ReListEmpManAtd();
                    }

                    ComputeVs(6, 8, 12);
                    ComputeVs(6, 10, 13);
                    if (mIsAtdShowLogVsWSAccumulation)
                    {
                        ComputeVs(11, 7, 31);
                        ComputeAccumulation(13, 31, 32);
                    }
                        

                    if (mIsAtdShowOT)
                        ComputeOT();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }           
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            var Value = string.Empty;
            if (e.ColIndex == 14)
            {
                Value = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                if (Value.Length == 4 || Value.Length == 3)
                {
                    SetTime(Grd1, e.RowIndex, 17, 14);
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 28, Grd1, e.RowIndex, 14);
                    UnRedRow(e.RowIndex);
                }
                else
                {
                    if (!(Value.Length == 5 && Value.Substring(2, 1) == ":"))
                    {
                        Grd1.Cells[e.RowIndex, 17].Value = Grd1.Cells[e.RowIndex, 28].Value = string.Empty;
                        UnRedRow(e.RowIndex);
                    }
                }
                Sm.FocusGrd(Grd1, e.RowIndex, 15);
            }

            if (e.ColIndex == 15)
            {
                Value = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                if (Value.Length == 4 || Value.Length == 3)
                {
                    Sm.FocusGrd(Grd1, (e.RowIndex + 1), 14);
                    SetTime(Grd1, e.RowIndex, 19, 15);
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 29, Grd1, e.RowIndex, 15);
                    UnRedRow(e.RowIndex);
                }
                else
                {
                    if (!(Value.Length == 5 && Value.Substring(2, 1) == ":"))
                    {
                        Grd1.Cells[e.RowIndex, 19].Value = Grd1.Cells[e.RowIndex, 29].Value = string.Empty;
                        UnRedRow(e.RowIndex);
                    }
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = "";
            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ATD", "TblAtdHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAtdHdr(DocNo));
            cml.Add(SaveAtdDtl(DocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd1, Row, 17).Length > 0 &&Sm.GetGrdStr(Grd1, Row, 19).Length > 0)
            //        cml.Add(SaveAtdDtl(DocNo, Row));
            //}

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                IsGrdEmpty() ||
                IsGrdTimeNotValid() ||
                IsGrdOneOfActualEmpty() ||
                IsGrdMonthNotValid() ||
                IsGrdAlreadyExist()
                ;
        }

  
        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input attendance.");
                return true;
            }
            return false;
        }

        private bool IsGrdAlreadyExist()
        {
            var SQL = new StringBuilder();

            int range = Convert.ToInt32(Sm.GetDte(DteEndDt).Substring(0, 8)) - Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(0, 8));
            string dayPlus = Sm.GetDte(DteStartDt).Substring(0, 8);

            if (Sm.GetLue(LueSiteCode).Length==0)
            {
                SQL.AppendLine("Select * from tblatdhdr where DeptCode ='" + Sm.GetLue(LueDeptCode) + "' And CancelInd = 'N' And TypeInd ='1' ");
                if(Sm.GetLue(LueAGCode).Length !=0)
                    SQL.AppendLine("And AgCode = '"+Sm.GetLue(LueAGCode)+"' ");
                SQL.AppendLine(" And Exists ");
            }
            else
            {
                SQL.AppendLine("Select * from tblatdhdr where DeptCode ='" + Sm.GetLue(LueDeptCode) + "' And SiteCode = '" + Sm.GetLue(LueSiteCode) + "' And CancelInd = 'N' And TypeInd ='1' ");
                if(Sm.GetLue(LueAGCode).Length !=0)
                    SQL.AppendLine("And AgCode = '"+Sm.GetLue(LueAGCode)+"' ");
                SQL.AppendLine(" And Exists ");
            }

            SQL.AppendLine("( ");
            SQL.AppendLine("    Select * from tblAtdHdr ");
            if (Sm.GetLue(LueSiteCode).Length==0)
            {
                SQL.AppendLine("    Where DeptCode='" + Sm.GetLue(LueDeptCode) + "'  And CancelInd = 'N' And TypeInd ='1' ) ");
                if (Sm.GetLue(LueAGCode).Length != 0)
                    SQL.AppendLine("And AgCode = '" + Sm.GetLue(LueAGCode) + "' ");
            }
            else
            {
                SQL.AppendLine("    Where DeptCode='" + Sm.GetLue(LueDeptCode) + "' And SiteCode='"+Sm.GetLue(LueSiteCode)+"'  And CancelInd = 'N' And TypeInd ='1' ) ");
                if (Sm.GetLue(LueAGCode).Length != 0)
                    SQL.AppendLine("And AgCode = '" + Sm.GetLue(LueAGCode) + "' ");
            }
            SQL.AppendLine("    And (StartDt in ( ");

            for (int Row = 0; Row <= range; Row++)
            {
                SQL.AppendLine("Select '" + Sm.GetValue("Select Replace(date_add('" + dayPlus + "', interval '" + Row + "' day), '-', '') As Day ") + "'  ");
                if (Row != range)
                {
                    SQL.AppendLine("Union ALL ");
                }
            }
            SQL.AppendLine("   ) Or EndDt in ( ");

            for (int Row = 0; Row <= range; Row++)
            {
                SQL.AppendLine("Select '" + Sm.GetValue("Select Replace(date_add('" + dayPlus + "', interval '" + Row + "' day), '-', '') As Day ") + "'  ");
                if (Row != range)
                {
                    SQL.AppendLine("Union ALL ");
                }
            }
            SQL.AppendLine(" )) ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Amendment for this periode has been created.");
                return true;
            }
            return false;
        }

        private bool IsGrdOneOfActualEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0 && Sm.GetGrdStr(Grd1, Row, 19).Length != 0 && Sm.GetGrdStr(Grd1, Row, 17).Length == 0)
                {
                    if (Decimal.Parse(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8)) <= Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 21)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "You need to input Actual In.");
                        Sm.FocusGrd(Grd1, Row, 14);
                        return true;
                    }
                }
                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0 && Sm.GetGrdStr(Grd1, Row, 17).Length != 0 && Sm.GetGrdStr(Grd1, Row, 19).Length == 0)
                {
                    if (Decimal.Parse(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8)) <= Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 21)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "You need to input Actual Out.");
                        Sm.FocusGrd(Grd1, Row, 15);
                        return true;
                    }
                }

                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0 && Sm.GetGrdStr(Grd1, Row, 17) == "" && Sm.GetGrdStr(Grd1, Row, 19) != "")
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Actual In.");
                    Sm.FocusGrd(Grd1, Row, 14);
                    return true;
                }

                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0 && Sm.GetGrdStr(Grd1, Row, 17) != "" && Sm.GetGrdStr(Grd1, Row, 19) == "")
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Actual Out.");
                    Sm.FocusGrd(Grd1, Row, 15);
                    return true;
                }

            }
          
            return false;
        }

        private bool IsGrdTimeNotValid()
        {
            string Msg = "";

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                {
                    string InA;
                    string OutA;

                    Msg =
                       "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                       "Employee : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                       "Date : " + Sm.GetGrdStr(Grd1, Row, 20) + Environment.NewLine;

                    if (Sm.GetGrdStr(Grd1, Row, 17).Length != 0)
                    {
                        InA = Sm.GetGrdStr(Grd1, Row, 17);
                        if (Decimal.Parse(RemoveQuotes(InA)) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Actual Time In not Valid");
                            Sm.FocusGrd(Grd1, Row, 14);
                            return true;
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 19).Length != 0)
                    {
                        OutA = Sm.GetGrdStr(Grd1, Row, 19);
                        if (Decimal.Parse(RemoveQuotes(OutA)) > 2359)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                Msg +
                                "Actual Time Out not Valid");
                            Sm.FocusGrd(Grd1, Row, 15);
                            return true;
                        }
                    }
                }
            }
            return false;
        }     

        private bool IsGrdMonthNotValid()
        {
            string Msg = "";
            if (!mIsAtdVerificationAllowToDifferentMonth)
            {
                if (Sm.GetDte(DteStartDt).Substring(4, 2) != Sm.GetDte(DteEndDt).Substring(4, 2))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Amandment have different month");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdMonthNotValid2()
        {
            string Msg = "";
            if (Decimal.Parse(Sm.GetDte(DteStartDt)) > Decimal.Parse(Sm.GetDte(DteEndDt)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Start date must be earlier than end date");
                    return true;
                }
            return false;
        }

        private MySqlCommand SaveAtdHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAtdHdr(DocNo, DocDt, TypeInd, CancelInd, Period, DeptCode, SiteCode, AgCode, StartDt, EndDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, '1', @CancelInd, @Period, @DeptCode, @SiteCode, @AgCode, @StartDt, @EndDt, @Remark, @UserCode, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Period", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@AgCode", Sm.GetLue(LueAGCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveAtdDtl(string DocNo)
        {
            string AIn, LIn, DtIn = string.Empty;
            string AOut, LOut, DtOut = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Attendance Verification */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r<Grd1.Rows.Count-1;r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 17).Length > 0 && Sm.GetGrdStr(Grd1, r, 19).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblAtdDtl(DocNo, EmpCode, Dt, InOutL1, InOutA1, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                
                    LIn = RemoveQuotes(Sm.GetGrdStr(Grd1, r, 6));
                    LOut = RemoveQuotes(Sm.GetGrdStr(Grd1, r, 7));
                    AIn = RemoveQuotes(Sm.GetGrdStr(Grd1, r, 17));
                    AOut = RemoveQuotes(Sm.GetGrdStr(Grd1, r, 19));
                    DtIn = Sm.GetGrdDate(Grd1, r, 20).Substring(0, 8);
                    DtOut = Sm.GetGrdDate(Grd1, r, 20).Substring(0, 8);

                    if (Sm.GetGrdStr(Grd1, r, 17).Length==0) AIn = "XXXX";
                    if (Sm.GetGrdStr(Grd1, r, 19).Length == 0) AOut = "XXXX";
                    if (Sm.GetGrdStr(Grd1, r, 6).Length == 0) LIn = "XXXX";
                    if (Sm.GetGrdStr(Grd1, r, 7).Length == 0) LOut = "XXXX";
                    
                    if (Sm.GetGrdStr(Grd1, r, 17).Length > 0 && Sm.GetGrdStr(Grd1, r, 19).Length > 0 && Decimal.Parse(AIn) < Decimal.Parse(AOut))
                    {
                        DtIn = Sm.GetGrdDate(Grd1, r, 20).Substring(0, 8);
                        DtOut = Sm.GetGrdDate(Grd1, r, 20).Substring(0, 8);
                    }

                    if (Sm.GetGrdStr(Grd1, r, 17).Length > 0 && Sm.GetGrdStr(Grd1, r, 19).Length > 0 && Decimal.Parse(AIn) > Decimal.Parse(AOut))
                    {
                        DtIn = Sm.GetGrdDate(Grd1, r, 20).Substring(0, 8);
                        DtOut = Sm.GetValue("Select Date_Format(Date_Add(STR_TO_DATE(" + Sm.GetGrdDate(Grd1, r, 20).Substring(0, 8) + ", '%Y%m%d'), Interval 1 Day), '%Y%m%d')");
                    }

                    SQL.AppendLine(
                        "(@DocNo, @EmpCode_" + r.ToString() +
                        ", @Dt_" + r.ToString() + 
                        ", @InOutL1_" + r.ToString() +
                        ", @InOutA1_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@EmpCode_"+r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                    Sm.CmParamDt(ref cm, "@Dt_" + r.ToString(), DtIn);
                    Sm.CmParam<String>(ref cm, "@InOutL1_" + r.ToString(), String.Concat(DtIn + LIn, DtOut + LOut));
                    Sm.CmParam<String>(ref cm, "@InOutA1_" + r.ToString(), String.Concat(DtIn + AIn, DtOut + AOut));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 27));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

    //private MySqlCommand SaveAtdDtl(string DocNo, int Row)
    //    {
    //        string AIn, LIn, DtIn = string.Empty;
    //        string AOut, LOut, DtOut = string.Empty;

    //        var SQL = new StringBuilder();

    //        SQL.AppendLine("Insert Into TblAtdDtl(DocNo, EmpCode, Dt, InOutL1, InOutA1, Remark, CreateBy, CreateDt) ");
    //        SQL.AppendLine("Values(@DocNo, @EmpCode, @Dt, @InOutL1, @InOutA1, @Remark, @UserCode, CurrentDateTime()) ");
            
    //        var cm = new MySqlCommand() { CommandText = SQL.ToString() };

    //        LIn = RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 6));
    //        LOut = RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 7));
    //        AIn = RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 17));
    //        AOut = RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 19));
    //        DtIn = Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8);
    //        DtOut = Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8);

    //        if (Sm.GetGrdStr(Grd1, Row, 17) == "")
    //        {
    //            AIn = "XXXX";
    //        }
    //        if (Sm.GetGrdStr(Grd1, Row, 19) == "")
    //        {
    //            AOut = "XXXX";
    //        }
    //        if (Sm.GetGrdStr(Grd1, Row, 6) == "")
    //        {
    //            LIn = "XXXX";
    //        }
    //        if (Sm.GetGrdStr(Grd1, Row, 7) == "")
    //        {
    //            LOut = "XXXX";
    //        }
    //        if (Sm.GetGrdStr(Grd1, Row, 17).Length > 0 && Sm.GetGrdStr(Grd1, Row, 19).Length > 0 && Decimal.Parse(AIn) < Decimal.Parse(AOut))
    //        {
    //            DtIn = Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8);
    //            DtOut = Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8);
    //        }

    //        if (Sm.GetGrdStr(Grd1, Row, 17).Length > 0 && Sm.GetGrdStr(Grd1, Row, 19).Length > 0 && Decimal.Parse(AIn) > Decimal.Parse(AOut))
    //        {
    //            DtIn = Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8);
    //            DtOut = Sm.GetValue("Select Date_Format(Date_Add(STR_TO_DATE(" + Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8) + ", '%Y%m%d'), Interval 1 Day), '%Y%m%d')");
    //        }

    //        Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
    //        Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 0));
    //        Sm.CmParamDt(ref cm, "@Dt", DtIn);
    //        Sm.CmParam<String>(ref cm, "@InOutL1", String.Concat(DtIn + LIn, DtOut + LOut));
    //        Sm.CmParam<String>(ref cm, "@InOutA1", String.Concat(DtIn+ AIn, DtOut + AOut));
    //        Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 27));
    //        Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

    //        return cm;
    //    }

        #endregion

        #region Update Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid2()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelAtdHdr(TxtDocNo.Text));
            cml.Add(SaveAtdDtl(TxtDocNo.Text));
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd1, Row, 17).Length > 0 && Sm.GetGrdStr(Grd1, Row, 19).Length > 0)
            //        cml2.Add(SaveAtdDtl(TxtDocNo.Text, Row));
            //}

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsInsertedDataNotValid2()
        {
            return
                IsGrdEmpty2() ||
                IsDocAlreadyCancel()||
                IsGrdOneOfActualEmpty()||
                IsGrdTimeNotValid()
                ;
        }

        private bool IsDocAlreadyCancel()
        {
            return Sm.IsDataExist(
                "Select 1 From TblAtdHdr Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsGrdEmpty2()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data.");
                return true;
            }
            return false;
        }

        
        private MySqlCommand CancelAtdHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAtdHdr Set CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete from TblAtdDtl Where DocNo = @DocNo; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand UpdateAtdLog(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAttendanceLog Set ProcessInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime()");
            SQL.AppendLine("Where EmpCode=@EmpCode And Dt=@Dt; ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };


            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd1, Row, 18));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                mStateInd = 0;
                ShowAtdHdr(DocNo);

                Sm.ClearGrd(Grd1, false);
                ListOfEmp();
                ReListDataDetail(DocNo);
                ReListEmpManAtd();
                var lWS = new List<WS>();
                var lEL = new List<EmpLeave>();
                Process1(ref lWS);
                if (lWS.Count > 0)
                {
                    Process5(ref lEL);
                    Process6(ref lEL);
                }
                ComputeVs(6, 8, 12);
                ComputeVs(6, 10, 13);
                if (mIsAtdShowLogVsWSAccumulation)
                {
                    ComputeVs(11, 7, 31);
                    ComputeAccumulation(13, 31, 32);
                }
                if (mIsAtdShowOT)
                    ComputeOT();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowAtdHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.CancelInd, " +
                    "Left(A.Period, 4) As PYr, Right(A.Period, 2) PMth, " +
                    "A.StartDt, A.EndDt, A.DeptCode, A.SiteCode, A.AGCode, A.Remark " +
                    "From TblAtdHdr A Where A.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "PYr", "PMth", "StartDt",  
                        //6
                        "EndDt", "DeptCode", "SiteCode", "AgCode", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sl.SetLueMth(LueMth);
                        Sl.SetLueYr(LueYr, "");
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[4]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[5]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[6]));
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[7]), string.Empty);
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[8]), string.Empty);
                        SetLueAGCode(ref LueAGCode, Sm.DrStr(dr, c[9]), string.Empty);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    }, true
                );
        }

        private void ReListDataDetail(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.Dt, ");
            SQL.AppendLine("Substring(InOutL1, 9, 4) As LIn, Right(InOutL1, 4) As LOut, ");
            SQL.AppendLine("Left(InOutA1, 8) As Dt1,  ");
            SQL.AppendLine("Substring(InOutA1, 9, 4) As AIn, ");
            SQL.AppendLine("Substring(InOutA1, 13, 8) As Dt2, ");
            SQL.AppendLine("Right(InOutA1, 4) As AOut, Remark ");
            SQL.AppendLine("From TblAtdDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("left Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Where A.DocNo='" + DocNo + "' Order By B.EmpName ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                       "EmpCode",
                        //1-5
                        "Dt", "LIn", "LOut", "AIn", "AOut", 
                        //6-8
                        "Dt1", "Dt2", "Remark" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 0)== Sm.DrStr(dr, 0) &&
                                Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8)== Sm.DrStr(dr, 1))
                            {
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 1);
                                if (Sm.DrStr(dr, 2) == "XXXX")
                                {
                                    Grd1.Cells[Row, 6].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 6, 2);
                                }
                                if (Sm.DrStr(dr, 3) == "XXXX")
                                {
                                    Grd1.Cells[Row, 7].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 7, 3);
                                }
                                if (Sm.DrStr(dr, 4) == "XXXX")
                                {
                                    Grd1.Cells[Row, 14].Value = "";
                                    Grd1.Cells[Row, 17].Value = "";
                                    Grd1.Cells[Row, 28].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 14, 4);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 17, 4);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 28, 4);
                                }
                                if (Sm.DrStr(dr, 5) == "XXXX")
                                {
                                    Grd1.Cells[Row, 15].Value = "";
                                    Grd1.Cells[Row, 19].Value = "";
                                    Grd1.Cells[Row, 29].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 15, 5);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 19, 5);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 29, 5);
                                }
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 6);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 7);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 8);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }
  
        #endregion

        #region Additional Method

        private void ExportToExcel()
        {
            if (IsGrdEmpty()) return;

            var l = new List<ExportedColumnHdr>();
            var l2 = new List<ExportedColumnDtl>();
            var l3 = new List<SumExportedColumnDtl>();

            GetColumnHdr(ref l);
            GetColumnDtl(ref l2);

            if (l2.Count > 0)
            {
                ExportToExcel2(ref l, ref l2, ref l3);
            }

            l.Clear(); l2.Clear(); l3.Clear();
        }

        private void GetColumnHdr(ref List<ExportedColumnHdr> l)
        {
            string[] ColName = new string[]
            {
                "No", "Check", "Date", "Employee", 
                "Attendance Log IN", "Attendance Log OUT", "Working Schedule (IN)", "Working Schedule (OUT)", 
                "Attendance VS Working Schedule", "Actual (Input) IN", "Actual (Input) OUT", 
                "Actual Date IN", "Actual Time IN", "Actual Date OUT", "Actual Time OUT", "Info", 
                "Remark"
            };

            foreach (var x in ColName)
            {
                l.Add(new ExportedColumnHdr()
                {
                    ColName = x
                });
            }
        }

        private void GetColumnDtl(ref List<ExportedColumnDtl> l2)
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
                {
                    l2.Add(new ExportedColumnDtl()
                    {
                        No = Sm.GetGrdStr(Grd1, i, 22),
                        Check = Sm.GetGrdBool(Grd1, i, 23),
                        Date = Sm.GetGrdStr(Grd1, i, 20),
                        Employee = Sm.GetGrdStr(Grd1, i, 2),
                        AttendanceLogIN = Sm.GetGrdStr(Grd1, i, 6),
                        AttendanceLogOUT = Sm.GetGrdStr(Grd1, i, 7),
                        WorkingScheduleIN = Sm.GetGrdStr(Grd1, i, 10),
                        WorkingScheduleOUT = Sm.GetGrdStr(Grd1, i, 11),
                        AttendanceVSWorkingSchedule = Sm.GetGrdStr(Grd1, i, 13),
                        ActualInputIN = Sm.GetGrdStr(Grd1, i, 14),
                        ActualInputOUT = Sm.GetGrdStr(Grd1, i, 15),
                        ActualDateIN = Sm.GetGrdStr(Grd1, i, 16),
                        ActualTimeIN = Sm.GetGrdStr(Grd1, i, 17),
                        ActualDateOUT = Sm.GetGrdStr(Grd1, i, 18),
                        ActualTimeOUT = Sm.GetGrdStr(Grd1, i, 19),
                        Info = Sm.GetGrdStr(Grd1, i, 26),
                        Remark = Sm.GetGrdStr(Grd1, i, 27)
                    });
                }
            }
        }

        private bool IsExcelExist()
        {
            bool f = true;
            if (Type.GetTypeFromProgID("Excel.Application") == null)
            {
                Sm.StdMsg(mMsgType.Warning, "No excel application in your PC");
                f = false;
            }
            return f;
        }

        private void ExportToExcel2(ref List<ExportedColumnHdr> l, ref List<ExportedColumnDtl> l2, ref List<SumExportedColumnDtl> l3)
        {
            System.Globalization.CultureInfo mySaveCI = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            if (!IsExcelExist()) return;

            Cursor.Current = Cursors.WaitCursor;

            // Instantiate Excel and start a new workbook.
            objApp = new Excel.Application();
            objBooks = objApp.Workbooks;
            objBook = objBooks.Add(Missing.Value);
            objSheets = objBook.Worksheets;
            objSheet = (Excel.Worksheet)objSheets.get_Item(1);

            int Col = 0;

            var AdditionalHeaderRowCount = 1;
            var HeaderColCount = l.Count;

            object[,] arr = new object[l2.Count + AdditionalHeaderRowCount + 2, l.Count + 1 + (AdditionalHeaderRowCount * HeaderColCount)];

            object[] arrformat = new object[l.Count + 1];

            for (int iCol = 0; iCol < l.Count; iCol++)
            {
                for (int iRow = 1; iRow >= 0; iRow--)
                {
                    arr[0, Col] = l[iCol].ColName.Replace(Environment.NewLine, " ");
                }
                Col++;
            }

            int Row = 1, RowTemp = 0;
            // Export the contents of iGrid
            foreach (ExportedColumnDtl row in l2)
            {
                string FormatType = string.Empty;

                arr[Row, 0] = row.No;
                arrformat[0] = FormatType;

                arr[Row, 1] = row.Check;
                arrformat[1] = FormatType;

                arr[Row, 2] = row.Date;
                arrformat[2] = "dd/MMM/yyyy";

                arr[Row, 3] = row.Employee;
                arrformat[3] = FormatType;

                arr[Row, 4] = row.AttendanceLogIN;
                arrformat[4] = FormatType;

                arr[Row, 5] = row.AttendanceLogOUT;
                arrformat[5] = FormatType;

                arr[Row, 6] = row.WorkingScheduleIN;
                arrformat[6] = FormatType;

                arr[Row, 7] = row.WorkingScheduleOUT;
                arrformat[7] = FormatType;

                arr[Row, 8] = row.AttendanceVSWorkingSchedule;
                arrformat[8] = FormatType;

                arr[Row, 9] = row.ActualInputIN;
                arrformat[9] = FormatType;

                arr[Row, 10] = row.ActualInputOUT;
                arrformat[10] = FormatType;

                arr[Row, 11] = row.ActualDateIN;
                arrformat[11] = "dd/MMM/yyyy";

                arr[Row, 12] = row.ActualTimeIN;
                arrformat[12] = FormatType;

                arr[Row, 13] = row.ActualDateOUT;
                arrformat[13] = "dd/MMM/yyyy";

                arr[Row, 14] = row.ActualTimeOUT;
                arrformat[14] = FormatType;

                arr[Row, 15] = row.Info;
                arrformat[15] = FormatType;

                arr[Row, 16] = row.Remark;
                arrformat[16] = FormatType;

                Row += 1;
            }

            Excel.Range c1 = (Excel.Range)objSheet.Cells[1, 1];
            Excel.Range c2 = (Excel.Range)objSheet.Cells[3 + l2.Count, l.Count];
            Excel.Range range = objSheet.get_Range(c1, c2);

            range.Value2 = arr;

            Excel.Range cHdr1 = (Excel.Range)objSheet.Cells[1, 1];
            Excel.Range cHdr2 = (Excel.Range)objSheet.Cells[1, l.Count];
            Excel.Range rangeHdr = objSheet.get_Range(cHdr1, cHdr2);

            rangeHdr.Font.Bold = true;

            for (int iCol = 0; iCol < arrformat.GetLength(0); iCol++)
            {
                if ((arrformat[iCol] != null) && (arrformat[iCol].ToString() != string.Empty))
                {
                    Excel.Range cDet1 = (Excel.Range)objSheet.Cells[2, iCol + 1];
                    //Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + Grd.Rows.Count - 1, iCol + 1];
                    Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + l2.Count, iCol + 1];
                    Excel.Range rangeDet = objSheet.get_Range(cDet1, cDet2);
                    rangeDet.NumberFormat = arrformat[iCol];
                }
            }

            //Auto Fit
            objSheet.Cells.Select();
            objSheet.Columns.AutoFit();
            objSheet.Rows.AutoFit();

            System.Threading.Thread.CurrentThread.CurrentCulture = mySaveCI;

            //Return control of Excel to the user.
            objApp.Visible = true;
            objApp.UserControl = true;
        }

        public void AddGrid(int Rowr)
        {
            SetGrd();
            int range = Convert.ToInt32(Sm.GetDte(DteEndDt).Substring(0, 8)) - Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(0, 8));
            //int Rowr = 0; 
            string dayPlus;
            string RowCol = Sm.GetDte(DteStartDt).Substring(6, 2);
            while (Rowr <= range)
            {
                Grd1.Rows.Add();
                int dayAdd = Convert.ToInt32(RowCol) + Rowr;
                if (dayAdd.ToString().Length != 2)
                {
                    dayPlus = "0" + dayAdd.ToString();
                }
                else
                {
                    dayPlus = dayAdd.ToString();
                }
                Grd1.Cells[Rowr, 15].Value = Sm.GetDte(DteStartDt).Substring(0, 6) + dayPlus;
                Rowr++;
            }
        }

        public void SetTime(iGrid GrdXXX, int RowXXX, int ColActual, int ColEdit)
        {
            try
            {
                //for (int Row = 0; Row < GrdXXX.Rows.Count; Row++)
                //{
                    if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 4 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                    {
                        GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat(Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                        GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                    }
                    if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 3 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                    {
                        GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat("0",Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 1), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                        GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                    }
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

          
        }

        static string RemoveQuotes(string input)
        {
            int index = 0;
            char[] result = new char[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] != ':')
                {
                    result[index++] = input[i];
                }
            }
            return new string(result, 0, index);
        }

        public void ListOfEmp()
        {
            try
            {
                string days = Sm.GetDte(DteStartDt).Substring(0, 6);

                var l = new List<Employee>();

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                DateTime Dt1 = new DateTime(
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(0, 4)),
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(4, 2)),
                    Int32.Parse(Sm.GetDte(DteStartDt).Substring(6, 2)),
                    0, 0, 0
                    );

                DateTime Dt2 = new DateTime(
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(0, 4)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(4, 2)),
                    Int32.Parse(Sm.GetDte(DteEndDt).Substring(6, 2)),
                    0, 0, 0
                    );

                var range = (Dt2 - Dt1).Days;
                DateTime TempDt = Dt1;

                //int range = Convert.ToInt32(Sm.GetDte(DteEndDt).Substring(0, 8)) - Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(0, 8));
                string RowCol = Sm.GetDte(DteStartDt).Substring(6, 2);
                string dayPlus;

                var SQL = new StringBuilder();
               
           
                SQL.AppendLine("Select A.EmpCode, ifnull(A.EmpCodeOld, '-') As EmpCodeOld, A.EmpName, B.DeptName, ifnull(A.ResignDt, 0) As ResignDt, ");
                SQL.AppendLine("C.Dt As DtList,  E.In1, E.Out1, D.Dt As DtWs, E.Out3 As OT  ");
                SQL.AppendLine("From tblEmployee A  ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
                SQL.AppendLine("Left Join ( ");
                for (int a = 0; a <= range; a++)
                {
                    dayPlus = Dt1.AddDays(Convert.ToDouble(a)).ToString("yyyyMMdd");
                    //int RowCols = Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(6, 2)) + a;
                    
                    //SQL.AppendLine("    select convert('" + days + RowCols.ToString("00") + "' using latin1) as dt ");

                    SQL.AppendLine("    select convert('" + dayPlus + "' using latin1) as dt ");

                    if (a != range)
                    {
                        SQL.AppendLine("    union all ");
                    }
                }
                SQL.AppendLine("    ) C on 0=0 ");
                SQL.AppendLine("Left Join TblEmpWorkSchedule D On A.EmpCode = D.EmpCode And C.Dt=D.Dt ");
                SQL.AppendLine("Left Join tblWorkSchedule E On D.WsCode = E.WsCode And E.ActInd = 'Y' And E.HolidayInd = 'N' ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("(   Select Y.EmpCode, Y.AGCode ");
                SQL.AppendLine("    From TblAttendanceGrpHdr X ");
                SQL.AppendLine("    Inner Join TblAttendanceGrpDtl Y On X.AgCode = Y.AGCode ");
                SQL.AppendLine("    Where X.ActInd = 'Y' ");
                SQL.AppendLine(" )F On A.EmpCode = F.EmpCode ");
                SQL.AppendLine("Where (A.ResignDt is null Or (A.ResignDt is not null And (@DocDt2 >A.resignDt>@DocDt1)) Or (A.ResignDt is not null And @DocDt1<A.ResignDt)) ");
                SQL.AppendLine("And A.JoinDt <= @DocDt2 ");
                if(Sm.GetLue(LueDeptCode).Length >0)
                    SQL.AppendLine("And A.DeptCode=@DeptCode ");
                //if (mIsFilterBySiteHR && Sm.GetLue(LueSiteCode).Length > 0 && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                if (Sm.GetLue(LueSiteCode).Length > 0)
                    SQL.AppendLine("And A.SiteCode=@SiteCode ");
                if (mIsAtdSystemTypeValidationEnabled)
                    SQL.AppendLine("And A.SystemType = '1' ");
                if (Sm.GetLue(LueAGCode).Length > 0)
                    SQL.AppendLine("And F.AGCode=@AGCode");
                //SQL.AppendLine("And D.Dt Between @DocDt1 And @DocDt2 ");    
                SQL.AppendLine("Order By A.EmpName, C.Dt;");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                    Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
                    Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                    Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteStartDt).Substring(0, 8));
                    Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteEndDt).Substring(0, 8));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         "EmpCode", 
                         "EmpCodeOld", "EmpName", "DeptName", "ResignDt", "DtList",
                         "In1", "Out1", "OT"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            
                                Grd1.Rows.Add();
                                int Row = Grd1.Rows.Count - 1;
                                Grd1.Cells[Row, 22].Value = Row + 1;
                                Grd1.Cells[Row, 0].Value = dr.GetString(c[0]);
                                Grd1.Cells[Row, 1].Value = dr.GetString(c[1]);
                                Grd1.Cells[Row, 2].Value = dr.GetString(c[2]);
                                Grd1.Cells[Row, 4].Value = dr.GetString(c[3]);

                                if (dr.GetString(c[4]) == "0")
                                    Grd1.Cells[Row, 21].Value = null;
                                else
                                    Grd1.Cells[Row, 21].Value = dr.GetString(c[4]);
                                if (dr.GetString(c[5]).Length == 0)
                                    Grd1.Cells[Row, 20].Value = null;
                                else
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 20, 5);

                                Grd1.Cells[Row, 21].Value = dr.GetString(c[4]);
                                Sm.SetGrdValue("T2", Grd1, dr, c, Row, 10, 6);
                                Sm.SetGrdValue("T2", Grd1, dr, c, Row, 11, 7);
                                Sm.SetGrdValue("T2", Grd1, dr, c, Row, 24, 8);

                                if (Sm.GetGrdStr(Grd1, Row, 24).Length > 0 && Sm.GetGrdStr(Grd1, Row, 11).Length > 0 &&
                                    Decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 24))) < Decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 11))))
                                {
                                    Grd1.Cells[Row, 25].Value = Sm.ConvertDate(dr.GetString(c[5])).AddDays(1);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 25].Value = Sm.ConvertDate(dr.GetString(c[5]));
                                }

                                CekResign(Row);
                            
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                Grd1.Rows.Add();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void ReListEmp_Old(int rangeDay)
        {
            var SQL = new StringBuilder();

            string day = Sm.GetDte(DteStartDt).Substring(0, 6);

            if (TxtDocNo.Text.Length != 0)
            {
                SQL.AppendLine("Select A.EmpCode, '1' As Tmind,  Left(InOutA1, 8) As Date, Substring(InOutA1, 13, 8) As Dt2, ");
                SQL.AppendLine("Substring(InOutA1, 9, 4) As AIn,  Right(InOutA1, 4) As AOut  ");
                SQL.AppendLine("From TblAtdHdr Az ");
                SQL.AppendLine("Inner Join TblAtdDtl A On Az.DocNo = A.DocNo ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("left Join TblDepartment C On B.DeptCode = C.DeptCode ");
                SQL.AppendLine("Where Az.DocNo = '" + TxtDocNo.Text + "'");
                SQL.AppendLine("UNION ALL ");
            }

            SQL.AppendLine("Select T4.EmpCode, ");
            SQL.AppendLine("Case When (T3.In1>T3.Out1) Then '2' Else '1' End As TmInd, T2.Dt As Date, ");
            SQL.AppendLine("Case When (T3.In1>T3.Out1) Then T2.Dt2 else T2.Dt end As Dt2, ");
            //SQL.AppendLine("Case When (T3.In1>T3.Out1) Then T4.MaxTm2 Else T4.MinTm1 End As AIn,  ");
            SQL.AppendLine("Case When (T3.In1>T3.Out1) Then T4.MinTm1 Else T4.MinTm1 End As AIn,  ");
            SQL.AppendLine("Case When (T3.In1>T3.Out1) Then T4.MaxTm1 Else T4.MaxTm1 End As AOut  ");
            SQL.AppendLine("From TblEmployee T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("    Date_Format(Date_Add(STR_TO_DATE(Dt, '%Y%m%d'), Interval 1 Day), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("    From TblEmpWorkSchedule  ");
            SQL.AppendLine("    Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(") T2 On T1.EmpCode=T2.EmpCode  ");
            SQL.AppendLine("Inner Join TblWorkSchedule T3 On T2.WsCode=T3.WsCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("        Select A1.EmpCode, A1.Dt, A.MinTm As MinTm1, B.MaxTm As MaxTm1, C.MinTm As MinTm2, D.MaxTm As MaxTm2 ");
            SQL.AppendLine("        From ( ");
            
            
		    SQL.AppendLine("            Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("            DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            SQL.AppendLine("            DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("            From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
		    SQL.AppendLine("        )A1");
		    SQL.AppendLine("            left Join ( ");
            SQL.AppendLine("                Select X1.EmpCode, X1.Dt, Min(Left(X3.Tm, 4)) As MinTm ");
            SQL.AppendLine("                From ( ");
            SQL.AppendLine("                    Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("                    DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1,"); 
            SQL.AppendLine("                    DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("                    From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("                ) X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode And X1.Dt = X3.Dt");
            SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            SQL.AppendLine("                Concat(Case When X2.bIn1<=X2.In1 Then X1.Dt Else Dt1 End, X2.bIn1) And ");
            SQL.AppendLine("                Concat(Case When X2.In1<=X2.aIn1 Then X1.Dt Else Dt2 End, X2.aIn1) ");
            SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            SQL.AppendLine("        ) A On A1.EmpCode = A.EmpCode And A1.Dt = A.Dt ");

            #region A old
            //SQL.AppendLine("            Select X1.EmpCode, X1.Dt, Min(Left(X3.Tm, 4)) As MinTm ");
            //SQL.AppendLine("            From ( ");
            //SQL.AppendLine("                Select EmpCode, Dt, WSCode, ");
            //SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            //SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            //SQL.AppendLine("                From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            //SQL.AppendLine("            ) X1 ");
            //SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            //SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode ");
            //SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            //SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt ");
            //SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            //SQL.AppendLine("                Concat(Case When X2.bIn1<=X2.In1 Then X1.Dt Else Dt1 End, X2.bIn1) And ");
            //SQL.AppendLine("                Concat(Case When X2.In1<=X2.aIn1 Then X1.Dt Else Dt2 End, X2.aIn1) ");
            //SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            //SQL.AppendLine("        ) A ");
            #endregion
            
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select X1.EmpCode, X1.Dt, Max(Left(X3.Tm, 4)) As MaxTm ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("                From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            ) X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode ");
            SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            SQL.AppendLine("                Concat(Case When X2.bOut1<=X2.Out1 Then X1.Dt Else Dt1 End, X2.bOut1) And ");
            SQL.AppendLine("                Concat(Case When X2.Out1<=X2.aOut1 Then X1.Dt Else Dt2 End, X2.aOut1) ");
            SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            SQL.AppendLine("        ) B On A1.EmpCode=B.EmpCode And A1.Dt=B.Dt ");

            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select X1.EmpCode, X1.Dt, Min(Left(X3.Tm, 4)) As MinTm ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("                From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt2 ");
            SQL.AppendLine("            ) X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode ");
            SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt2 ");
            SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            SQL.AppendLine("                Concat(Case When X2.bOut1<=X2.Out1 Then X1.Dt2 Else X1.Dt End, X2.bOut1) And ");
            SQL.AppendLine("                Concat(X1.Dt2, X2.aOut1) ");
            SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            SQL.AppendLine("        ) C  On A1.EmpCode=C.EmpCode And A1.Dt=C.Dt ");

            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select X1.EmpCode, X1.Dt, Max(Left(X3.Tm, 4)) As MaxTm ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select EmpCode, Dt, WSCode, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL -1 DAY), '%Y%m%d') As Dt1, ");
            SQL.AppendLine("                DATE_FORMAT(DATE_ADD(STR_TO_DATE(Dt, '%Y%m%d'),INTERVAL 1 DAY), '%Y%m%d') As Dt2 ");
            SQL.AppendLine("                From TblEmpWorkSchedule Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            ) X1 ");
            SQL.AppendLine("            Inner Join TblWorkSchedule X2 On X1.WSCode=X2.WSCode ");
            SQL.AppendLine("            Inner Join TblAttendanceLog X3 On X1.EmpCode=X3.EmpCode ");
            SQL.AppendLine("            Inner Join TblEmployee X4 On X1.EmpCode=X4.EmpCode And X4.DeptCode=@DeptCode ");
            SQL.AppendLine("            Where X1.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("            And Concat(X3.Dt, Left(X3.Tm, 4)) Between ");
            SQL.AppendLine("                Concat(Case When X2.bIn1<=X2.In1 Then X1.Dt Else Dt1 End, X2.bIn1) And ");
            SQL.AppendLine("                Concat(Case When X2.In1<=X2.aIn1 Then X1.Dt Else Dt2 End, X2.aIn1) ");
            SQL.AppendLine("            Group By X1.EmpCode, X1.Dt ");
            SQL.AppendLine("        ) D On A1.EmpCode=D.EmpCode And A1.Dt=D.Dt ");
            SQL.AppendLine(") T4  ");
            SQL.AppendLine("On T2.EmpCode=T4.EmpCode  ");
            SQL.AppendLine("And T2.Dt=T4.Dt  ");
            SQL.AppendLine("Where T1.JoinDt<=@EndDt And T1.DeptCode=@DeptCode ");
            SQL.AppendLine("And (T1.ResignDt is Null Or (T1.ResignDt is Not Null And T1.ResignDt>=@StartDt)) ");
            SQL.AppendLine("Group By T4.EmpCode, T4.Dt ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                Sm.CmParam<String>(ref cm, "@StartDt", Sm.GetDte(DteStartDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@EndDt", Sm.GetDte(DteEndDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@EndDt2", Sm.GetValue("Select Date_Format(Date_Add(STR_TO_DATE(" + Sm.GetDte(DteEndDt).Substring(0, 8) + ", '%Y%m%d'), Interval 1 Day), '%Y%m%d')"));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "EmpCode",
 
                        //1-5
                        "tmind", "date", "Ain", "Aout",  "Dt2", 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8), Sm.DrStr(dr, 2)))
                            {
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 2);
                                if (Sm.DrStr(dr, 4) != "XXXX")
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 6, 3);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 14, 3);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 17, 3);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 28, 3);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 6].Value =
                                    Grd1.Cells[Row, 14].Value =
                                    Grd1.Cells[Row, 17].Value =
                                    Grd1.Cells[Row, 28].Value = "";
                                }
                                if (Sm.DrStr(dr, 5) != "XXXX")
                                {
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 7, 4);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 15, 4);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 19, 4);
                                    Sm.SetGrdValue("T2", Grd1, dr, c, Row, 29, 4);
                                }
                                else
                                {
                                    Grd1.Cells[Row, 7].Value =
                                    Grd1.Cells[Row, 15].Value =
                                    Grd1.Cells[Row, 19].Value =
                                    Grd1.Cells[Row, 29].Value = "";
                                }
                                if (Sm.GetGrdStr(Grd1, Row, 6).Length == 0 && Sm.GetGrdStr(Grd1, Row, 7).Length == 0)
                                {
                                    Grd1.Cells[Row, 16].Value = "";
                                    Grd1.Cells[Row, 18].Value = "";
                                }
                                else
                                {
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 2);
                                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 2); 
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ReListEmpManAtd();
        }

        private void ReListEmpManAtd()
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Select B.EmpCode, A.DocDt, Left(B.InOut1, 4) As MAtdIn, Right(B.InOut1, 4) As MAtdOut  ");
            SQL.AppendLine("From TblManualAtdhdr A ");
            SQL.AppendLine("Inner Join TblManualAtdDtl B On A.DocNo = B.DocnO ");
            SQL.AppendLine("Where A.DocDt between '" + Sm.GetDte(DteStartDt).Substring(0, 8) + "' And '" + Sm.GetDte(DteEndDt).Substring(0, 8) + "' ");
            SQL.AppendLine("And A.DeptCode = '"+Sm.GetLue(LueDeptCode)+"' ");
            SQL.AppendLine("And A.cancelInd = 'N' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "EmpCode",
 
                        //1-5
                        "DocDt", "MAtdIn", "MAtdOut"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("T2", Grd1, dr, c, Row, 8, 2);
                                Sm.SetGrdValue("T2", Grd1, dr, c, Row, 9, 3);
                                break;
                            }

                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            if(TxtDocNo.Text.Length == 0)
            RoundingTime();
        }

        private void ComputeVs(int Col1, int Col2, int Col3)
        {
            TimeSpan TS = new TimeSpan();
            var Value = string.Empty;
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //CekOT(Row);
                if (Sm.GetGrdStr(Grd1, Row, Col1).Length != 0 && Sm.GetGrdStr(Grd1, Row, Col2).Length != 0
                   )
                {
                    TS = DateTime.Parse(Sm.GetGrdStr(Grd1, Row, Col2)).Subtract(DateTime.Parse(Sm.GetGrdStr(Grd1, Row, Col1)));
                    Value = TS.ToString();
                    if (Value.Substring(0, 1) == "-")
                    {
                        Grd1.Cells[Row, Col3].Value = Value.Substring(0, 6);
                    }
                    else
                    {
                        Grd1.Cells[Row, Col3].Value = Value.Substring(0, 5);
                    }
                }
            }
            Grd1.EndUpdate();
        }

        private void ComputeAccumulation(int Col1, int Col2, int Col3)
        {
            TimeSpan TS = new TimeSpan();
            string  Value1, Value2;
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Value1 = Sm.GetGrdStr(Grd1, Row, Col1);
                Value2 = Sm.GetGrdStr(Grd1, Row, Col2);
                if (Value1.Length != 0 && Value2.Length != 0)
                {
                    if (Sm.Left(Value1, 1)=="-" && Sm.Left(Value2, 1) != "-") 
                        Grd1.Cells[Row, Col3].Value = Value1.Replace("-", string.Empty);
                    if (Sm.Left(Value1, 1) != "-" && Sm.Left(Value2, 1) == "-")
                        Grd1.Cells[Row, Col3].Value = Value2.Replace("-", string.Empty);
                    if (Sm.Left(Value1, 1) != "-" && Sm.Left(Value2, 1) != "-")
                        Grd1.Cells[Row, Col3].Value = "00:00";
                    if (Sm.Left(Value1, 1) == "-" && Sm.Left(Value2, 1) == "-")
                    {
                        Value1 = Value1.Replace("-", string.Empty);
                        Value2 = Value2.Replace("-", string.Empty);
                        TS = (DateTime.Parse(Value2).AddTicks(DateTime.Parse(Value1).TimeOfDay.Ticks)).TimeOfDay;
                        Grd1.Cells[Row, Col3].Value = (TS.ToString()).Substring(0, 5);
                    }
                }
            }
            Grd1.EndUpdate();
        }

        #region method gak kepake
        //private void IntervalIsMin_Old()
        //{
        //    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, Row, 12).Length != 0)
        //        {
        //            if (decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 12))) < 0)
        //            {
        //                Grd1.Rows[Row].BackColor = Color.Red;
        //            }
        //        }
        //        if (Sm.GetGrdStr(Grd1, Row, 13).Length != 0)
        //        {
        //            if (decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 13))) < 0)
        //            {
        //                Grd1.Rows[Row].BackColor = Color.Red;
        //            }
        //        }
        //    }
        //}

        //private void CekOT_Old(int Row)
        //{
        //    if (Sm.GetGrdStr(Grd1, Row, 24).Length > 0 && Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
        //    {
        //        string LogOut = String.Concat(Sm.Left(Sm.GetGrdDate(Grd1, Row, 20), 8), RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 7)));
        //        string OTOut = String.Concat(Sm.Left(Sm.GetGrdDate(Grd1, Row, 25), 8), RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 24)));

        //        if (Decimal.Parse(LogOut) < Decimal.Parse(OTOut))
        //        {
        //            Grd1.Rows[Row].BackColor = Color.Red;
        //            Grd1.Cells[Row, 26].Value = "CheckOut Before OT Routine";

        //        }
        //    }
        //    else
        //    {
        //        if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
        //        {
        //            string LogOut = String.Concat(Sm.Left(Sm.GetGrdDate(Grd1, Row, 5), 8), RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 7)));
        //            string WSOut = String.Concat(Sm.Left(Sm.GetGrdDate(Grd1, Row, 20), 8), RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 11)));

        //            if (Decimal.Parse(LogOut) < Decimal.Parse(WSOut))
        //            {
        //                Grd1.Rows[Row].BackColor = Color.Red;
        //                Grd1.Cells[Row, 26].Value = "CheckOut Before WS";
        //            }
        //        }
        //    }
        //}

        #endregion


        private void UnRedRow(int Row)
        {
            if (Sm.GetGrdStr(Grd1, Row, 10).Length != 0 && Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, Row, 28).Length != 0 && Sm.GetGrdStr(Grd1, Row, 29).Length != 0)
                {
                    if (decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 28))) <= decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 10)))
                        &&
                        decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 29))) >= decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 11)))
                        )
                    {
                        Grd1.Rows[Row].BackColor = Color.Transparent;
                    }
                    else if (decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 28))) > decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 10)))
                        ||
                        decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 29))) < decimal.Parse(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 11)))
                        )
                    {
                        Grd1.Rows[Row].BackColor = Color.Red;
                    }
                }
            }
        }

      
        private void CekResign(int Row)
        {
            decimal DateRow = 0m;
            decimal DateResign = 0m;

            if (Sm.GetGrdDate(Grd1, Row, 20).Length > 1)
            {
                DateRow = Decimal.Parse(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8));
            }
            if (Sm.GetGrdStr(Grd1, Row, 21).Length > 1)
            {
                DateResign = Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 21));
            }
            Grd1.BeginUpdate();
            if (DateResign > 0 && DateRow >= DateResign)
            {
                Grd1.Rows[Row].ReadOnly = iGBool.True;
                Grd1.Rows[Row].CellStyle.BackColor = Color.Violet;
            }
            Grd1.EndUpdate();
        }

        private void RoundingTime()
        {
            if (RoundSch != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0)
                    {
                        string MMIn = Sm.Right(Sm.GetGrdStr(Grd1, Row, 14), 2);
                        if (Convert.ToInt32(MMIn) < 30)
                        {
                            TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 14)).Substring(0, 2)), 0, 0);
                            TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(RoundSch), 0);
                            string LogIn = Convert.ToString(TS.Add(TS2));
                            Grd1.Cells[Row, 14].Value = LogIn.Substring(0, 5);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 14);
                            Grd1.Cells[Row, 28].Value = Sm.GetGrdStr(Grd1, Row, 14);
                        }
                        else
                        {
                            TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 14)).Substring(0, 2)), 0, 0);
                            TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(2 * RoundSch), 0);
                            string LogIn = Convert.ToString(TS.Add(TS2));
                            Grd1.Cells[Row, 14].Value = LogIn.Substring(0, 5);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 14);
                            Grd1.Cells[Row, 28].Value = Sm.GetGrdStr(Grd1, Row, 14);
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 15).Length > 0)
                    {
                        string MMIn = Sm.Right(Sm.GetGrdStr(Grd1, Row, 15), 2);
                        if (Convert.ToInt32(MMIn) < 30)
                        {
                            TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 15)).Substring(0, 2)), 0, 0);
                            string LogOut = Convert.ToString(TS);
                            Grd1.Cells[Row, 15].Value = LogOut.Substring(0, 5);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 15);
                            Grd1.Cells[Row, 29].Value = Sm.GetGrdStr(Grd1, Row, 15);
                        }
                        else
                        {
                            TimeSpan TS = new TimeSpan(Convert.ToInt32(RemoveQuotes(Sm.GetGrdStr(Grd1, Row, 15)).Substring(0, 2)), 0, 0);
                            TimeSpan TS2 = new TimeSpan(0, Convert.ToInt32(RoundSch), 0);
                            string LogOut = Convert.ToString(TS.Add(TS2));
                            Grd1.Cells[Row, 15].Value = LogOut.Substring(0, 5);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 15);
                            Grd1.Cells[Row, 29].Value = Sm.GetGrdStr(Grd1, Row, 15);
                        }
                    }

                }
            }
            else if (RoundSch == 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                    {
                        
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdStr(Grd1, Row, 6);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 6);
                            Grd1.Cells[Row, 28].Value = Sm.GetGrdStr(Grd1, Row, 6);
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0 )
                    {
                        
                            Grd1.Cells[Row, 15].Value = Sm.GetGrdStr(Grd1, Row, 7);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 7);
                            Grd1.Cells[Row, 29].Value = Sm.GetGrdStr(Grd1, Row, 7);
                    }
                }
            }
            else
            {

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0 && Sm.GetGrdStr(Grd1, Row, 10).Length > 0)
                    {
                        if (Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 6).Replace(":", "")) < Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 10).Replace(":", "")))
                        {
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdStr(Grd1, Row, 10);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 10);
                            Grd1.Cells[Row, 28].Value = Sm.GetGrdStr(Grd1, Row, 10);
                        }
                        else
                        {
                            Grd1.Cells[Row, 14].Value = Sm.GetGrdStr(Grd1, Row, 6);
                            Grd1.Cells[Row, 17].Value = Sm.GetGrdStr(Grd1, Row, 6);
                            Grd1.Cells[Row, 28].Value = Sm.GetGrdStr(Grd1, Row, 6);
                        }
                    }

                    if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0 && Sm.GetGrdStr(Grd1, Row, 11).Length > 0)
                    {
                        if (Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 7).Replace(":", "")) > Decimal.Parse(Sm.GetGrdStr(Grd1, Row, 11).Replace(":", "")))
                        {
                            Grd1.Cells[Row, 15].Value = Sm.GetGrdStr(Grd1, Row, 11);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 11);
                            Grd1.Cells[Row, 29].Value = Sm.GetGrdStr(Grd1, Row, 11);
                        }
                        else
                        {
                            Grd1.Cells[Row, 15].Value = Sm.GetGrdStr(Grd1, Row, 7);
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdStr(Grd1, Row, 7);
                            Grd1.Cells[Row, 29].Value = Sm.GetGrdStr(Grd1, Row, 7);
                        }
                    }
                }
            }
        }

        private void SetLueAGCode(ref LookUpEdit Lue, string Code, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.AGCode As Col1, T.AGName As Col2 From TblAttendanceGrpHdr T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where AGCode=@Code;");
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (DeptCode.Length>0)
                    SQL.AppendLine("And IfNull(T.DeptCode, 'X')=@DeptCode ");
                SQL.AppendLine("Order By T.AGName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void ReListEmp()
        {
            var lWS = new List<WS>();
            var lLog = new List<Log>();
            var lEL = new List<EmpLeave>();
            Process1(ref lWS);
            if (lWS.Count > 0)
            {
                Process2(ref lLog);
                Process3(ref lWS, ref lLog);
                Process4(ref lWS);
                Process5(ref lEL);
                Process6(ref lEL);
            }
            lWS.Clear();
            lLog.Clear();
            lEL.Clear();
            if(mStateInd == 1 && mAtdVerificationActualInOutSource == "1") 
                RoundingTime();
        }

        private void Process1(ref List<WS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            
            SQL.AppendLine("Select A.Dt, A.EmpCode, ");
            SQL.AppendLine("B.In1 As LogIn, B.bIn1 As bLogIn, B.aIn1 As aLogIn, ");
            SQL.AppendLine("Case When B.Out3 Is Null Then B.Out1 Else B.Out3 End As LogOut, ");
            SQL.AppendLine("Case When B.bOut3 Is Null Then B.bOut1 Else B.bOut3 End As bLogOut, ");
            SQL.AppendLine("Case When B.aOut3 Is Null Then B.aOut1 Else B.aOut3 End As aLogOut ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WsCode=B.WsCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.EmpCode, A.Dt;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Dt",
 
                    //1-5
                    "EmpCode", "LogIn", "bLogIn", "aLogIn", "LogOut", 
                    
                    //6-7
                    "bLogOut", "aLogOut"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WS()
                        {
                            Dt = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            LogIn = Sm.DrStr(dr, c[2]),
                            bLogIn = Sm.DrStr(dr, c[3]),
                            aLogIn = Sm.DrStr(dr, c[4]),
                            LogOut = Sm.DrStr(dr, c[5]),
                            bLogOut = Sm.DrStr(dr, c[6]),
                            aLogOut = Sm.DrStr(dr, c[7]),
                            IsOneDay = true,
                            IsWarning = false,
                            Dt2 = string.Empty,
                            AGCode = string.Empty,
                            AGName = string.Empty,
                            InDtL = string.Empty,
                            InTmL = string.Empty,
                            OutDtL = string.Empty,
                            OutTmL = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Log> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteStartDt)).AddDays(-1)));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteEndDt)).AddDays(1)));
            
            SQL.AppendLine("Select A.EmpCode, A.Dt, A.Tm ");
            SQL.AppendLine("From TblAttendanceLog A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            if (Filter.Length != 0)
                SQL.AppendLine("And (" + Filter + ") ");
            SQL.AppendLine("Order By A.EmpCode, A.Dt, A.Tm;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Log()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DtTm = Sm.Left(Sm.DrStr(dr, c[1]) + Sm.DrStr(dr, c[2]), 12)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<WS> lWS, ref List<Log> lLog)
        {
            string
                EmpCode = string.Empty,
                Dt = string.Empty,
                Dt0 = string.Empty,
                Dt2 = string.Empty,
                LogIn = string.Empty,
                bLogIn = string.Empty,
                aLogIn = string.Empty,
                LogOut = string.Empty,
                bLogOut = string.Empty,
                aLogOut = string.Empty
                ;

            bool IsOneDay = true;

            string Now = Sm.ServerCurrentDateTime();

            if (Now.Length > 0) Now = Sm.Left(Now, 12);

            for (var i = 0; i < lWS.Count; i++)
            {
                EmpCode = lWS[i].EmpCode;
                Dt = lWS[i].Dt;
                Dt0 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(-1)), 8);
                Dt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(Dt).AddDays(1)), 8);

                IsOneDay = (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].LogOut) <= 0);

                lWS[i].Dt2 = Dt2;
                lWS[i].IsOneDay = IsOneDay;

                LogIn = Dt + lWS[i].LogIn;
                if (Sm.CompareDtTm(lWS[i].bLogIn, lWS[i].LogIn) <= 0)
                    bLogIn = Dt + lWS[i].bLogIn;
                else
                    bLogIn = Dt0 + lWS[i].bLogIn;

                if (Sm.CompareDtTm(lWS[i].LogIn, lWS[i].aLogIn) <= 0)
                    aLogIn = Dt + lWS[i].aLogIn;
                else
                    aLogIn = Dt2 + lWS[i].aLogIn;

                if (IsOneDay)
                    LogOut = Dt + lWS[i].LogOut;
                else
                    LogOut = Dt2 + lWS[i].LogOut;

                if (IsOneDay)
                    bLogOut = Dt + lWS[i].bLogOut;
                else
                {
                    if (Sm.CompareDtTm(lWS[i].bLogOut, lWS[i].LogOut) <= 0)
                        bLogOut = Dt2 + lWS[i].bLogOut;
                    else
                        bLogOut = Dt + lWS[i].bLogOut;
                }

                if (IsOneDay)
                {
                    if (Sm.CompareDtTm(lWS[i].LogOut, lWS[i].aLogOut) <= 0)
                        aLogOut = Dt + lWS[i].aLogOut;
                    else
                        aLogOut = Dt2 + lWS[i].aLogOut;
                }
                else
                    aLogOut = Dt2 + lWS[i].aLogOut;

                if (lLog.Count > 0)
                {
                    if (!mIsAtdShowDataLogWhenHoliday)
                    {
                        //IN
                        foreach (var index in lLog
                            .Where(x =>
                                    string.Compare(x.EmpCode, EmpCode) == 0 &&
                                    Sm.CompareDtTm(x.DtTm, bLogIn) >= 0 &&
                                    Sm.CompareDtTm(x.DtTm, aLogIn) <= 0
                                )
                            .OrderBy(o => o.DtTm)
                            .Take(1)
                            )
                        {
                            lWS[i].InDtL = Sm.Left(index.DtTm, 8);
                            lWS[i].InTmL = Sm.Right(index.DtTm, 4);
                        }
                        //OUT
                        foreach (var index in lLog
                            .Where(x =>
                                    string.Compare(x.EmpCode, EmpCode) == 0 &&
                                    Sm.CompareDtTm(x.DtTm, bLogOut) >= 0 &&
                                    Sm.CompareDtTm(x.DtTm, aLogOut) <= 0
                                )
                            .OrderByDescending(o => o.DtTm)
                            .Take(1)
                            )
                        {
                            lWS[i].OutDtL = Sm.Left(index.DtTm, 8);
                            lWS[i].OutTmL = Sm.Right(index.DtTm, 4);
                        }
                    }
                    else
                    {
                        for (int data = 0; data < lLog.Count; data++)
                        {
                            //region IN
                            if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, bLogIn) >= 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, aLogIn) <= 0)
                            {
                                lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                lWS[i].InTmL = Sm.Right(lLog[data].DtTm, 4);
                            }
                            else
                            {
                                if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(Sm.Left(lLog[data].DtTm, 8), Dt) == 0 &&
                                bLogIn.Length < 9)
                                {
                                    lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                    lWS[i].InTmL = Sm.GetValue("Select MIN(Left(Tm, 4)) From TblAttendanceLog Where EmpCode = '" + lLog[data].EmpCode + "' And Dt = '" + Dt + "' ");

                                }
                                else
                                {
                                    if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, bLogIn) >= 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, aLogIn) <= 0)
                                    {
                                        lWS[i].InDtL = Sm.Left(lLog[data].DtTm, 8);
                                        lWS[i].InTmL = Sm.Right(lLog[data].DtTm, 4);
                                    }
                                }
                            }
                            

                            // Region  OUT
                            if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, bLogOut) >= 0 &&
                                Sm.CompareDtTm(lLog[data].DtTm, aLogOut) <= 0)
                            {
                                lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                lWS[i].OutTmL = Sm.Right(lLog[data].DtTm, 4);
                            }
                            else
                            {
                                if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                Sm.CompareDtTm(Sm.Left(lLog[data].DtTm, 8), Dt) == 0 &&
                                bLogOut.Length < 9)
                                {
                                    lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                    lWS[i].OutTmL = Sm.GetValue("Select MAX(Left(Tm, 4)) From TblAttendanceLog Where EmpCode = '" + lLog[data].EmpCode + "' And Dt = '" + Dt + "' ");

                                }
                                else
                                {
                                    if (string.Compare(lLog[data].EmpCode, EmpCode) == 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, bLogOut) >= 0 &&
                                                                    Sm.CompareDtTm(lLog[data].DtTm, aLogOut) <= 0)
                                    {
                                        lWS[i].OutDtL = Sm.Left(lLog[data].DtTm, 8);
                                        lWS[i].OutTmL = Sm.Right(lLog[data].DtTm, 4);
                                    }
                                }
                            }
                            // OUT   
                        }
                    }    
                }

                if (LogIn.Length == 12 &&
                    Sm.CompareDtTm(LogIn, Now) < 0 &&
                    (lWS[i].InDtL.Length == 0 || lWS[i].InTmL.Length == 0))
                    lWS[i].IsWarning = true;

                if (LogOut.Length == 12 &&
                    Sm.CompareDtTm(LogOut, Now) < 0 &&
                    (lWS[i].OutDtL.Length == 0 || lWS[i].OutTmL.Length == 0))
                    lWS[i].IsWarning = true;

                if (LogIn.Length == 12 && lWS[i].InDtL.Length > 0 && lWS[i].InTmL.Length > 0 &&
                    Sm.CompareDtTm(LogIn, lWS[i].InDtL + lWS[i].InTmL) < 0)
                    lWS[i].IsWarning = true;

                if (LogOut.Length == 12 && lWS[i].OutDtL.Length > 0 && lWS[i].OutTmL.Length > 0 &&
                    Sm.CompareDtTm(lWS[i].OutDtL + lWS[i].OutTmL, LogOut) < 0)
                    lWS[i].IsWarning = true;
            }
        }

        private void Process4(ref List<WS> l)
        {
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), l[i].EmpCode) &&
                        Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8), l[i].Dt))
                    {
                        if (mStateInd == 1) // kalau insert, baru merubah nilai nya
                        {
                            if (mAtdVerificationActualInOutSource == "1")
                            {
                                SetDt(Row, 5, l[i].Dt);
                                SetTm(Row, 6, l[i].InTmL);
                                SetTm(Row, 7, l[i].OutTmL);
                                SetTm(Row, 14, l[i].InTmL);
                                SetTm(Row, 17, l[i].InTmL);
                                SetTm(Row, 15, l[i].OutTmL);
                                SetTm(Row, 19, l[i].OutTmL);
                                SetDt(Row, 16, l[i].InDtL);
                                SetDt(Row, 18, l[i].OutDtL);
                            }
                            else
                            {
                                SetDt(Row, 5, l[i].Dt);
                                SetTm(Row, 6, l[i].InTmL);
                                SetTm(Row, 7, l[i].OutTmL);
                                SetTm(Row, 14, l[i].LogIn);
                                SetTm(Row, 17, l[i].LogIn);
                                SetTm(Row, 15, l[i].LogOut);
                                SetTm(Row, 19, l[i].LogOut);
                                SetDt(Row, 16, l[i].InDtL);
                                SetDt(Row, 18, l[i].OutDtL);
                            }
                        }

                        if (l[i].IsWarning) Grd1.Rows[Row].BackColor = Color.FromName("Red");

                        break;
                    }
                }
            }
            Grd1.EndUpdate();
        }

        //list employee leave
        private void Process5(ref List<EmpLeave> lEL)
        {
            lEL.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty;
            string Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 0);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T.EmpCode=@EmpCode" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + No.ToString(), EmpCode);
                        No += 1;
                    }
                }
            }

            Sm.CmParamDt(ref cm, "@StartDt", Sm.Left(Sm.GetDte(DteStartDt), 8));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.Left(Sm.GetDte(DteEndDt), 8));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("Select A.EmpCode, B.LeaveDt, C.leavename, A.LeaveCode, E.OptDesc As LeaveColor ");
            SQL.AppendLine("From TblEmpLeavehdr A ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join Tblleave C On A.LeaveCode = C.leaveCode ");
            SQL.AppendLine("Inner Join TblEmployee D On A.Empcode=D.EmpCode ");
            SQL.AppendLine("Left Join tbloption E On C.LeaveColor = E.OptCode And E.OptCat = 'LeaveColor' ");
            SQL.AppendLine("Where A.cancelind = 'N' And A.status = 'A' And D.DeptCode=@DeptCode ");
            SQL.AppendLine("And B.LeaveDt Between @StartDt And @EndDt ");
            if (mIsAttendanceVerificationUseLeaveRequestGroup)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select G.EmpCode, H.LeaveDt, I.LeaveName, F.LeaveCode, K.OptDesc As LeaveColor ");
                SQL.AppendLine("From TblEmpLeave2Hdr F ");
                SQL.AppendLine("Inner Join tblempleave2dtl G On F.DocNo = G.DocNo ");
                SQL.AppendLine("Inner Join tblempleave2dtl2 H On F.DocNo = H.DocNo ");
                SQL.AppendLine("Inner Join Tblleave I On F.LeaveCode = I.leaveCode ");
                SQL.AppendLine("Inner Join TblEmployee J On G.Empcode = J.EmpCode ");
                SQL.AppendLine("Left Join tbloption K On I.LeaveColor = K.OptCode And K.OptCat = 'LeaveColor' ");
                SQL.AppendLine("Where F.cancelind = 'N' And F.status = 'A' And J.DeptCode=@DeptCode ");
                SQL.AppendLine("And H.LeaveDt Between @StartDt And @EndDt ");
            }
            SQL.AppendLine(")T ");
            if (Filter.Length != 0)
                SQL.AppendLine("Where (" + Filter + ") ");
            SQL.AppendLine("Order By T.EmpCode, T.LeaveDt, T.LeaveName; ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "LeaveDt", "LeaveName", "LeaveCode", "LeaveColor" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEL.Add(new EmpLeave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            LeaveDt = Sm.DrStr(dr, c[1]),
                            LeaveName = Sm.DrStr(dr, c[2]),
                            LeaveCode = Sm.DrStr(dr, c[3]),
                            LeaveColor = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        //bandingin list employee dgn list employee leave
        private void Process6(ref List<EmpLeave> lEL)
        {
            string[] mLeaves = {};

            if(mLeaveCodeGetMealTransport.Length > 0)
            {
                mLeaves = mLeaveCodeGetMealTransport.Split(',');
            }

            Grd1.BeginUpdate();
            for (var i = 0; i < lEL.Count; i++)
            {
                if (mIsAtdVerificationLeaveColorActive)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), lEL[i].EmpCode) &&
                            Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8), lEL[i].LeaveDt))
                        {
                            Grd1.Rows[Row].BackColor = Color.FromName(lEL[i].LeaveColor);
                            Grd1.Cells[Row, 26].Value = lEL[i].LeaveName;
                            break;
                        }
                    }
                }
                else
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), lEL[i].EmpCode) &&
                            Sm.CompareStr(Sm.GetGrdDate(Grd1, Row, 20).Substring(0, 8), lEL[i].LeaveDt))
                        {
                            Grd1.Rows[Row].BackColor = Color.DodgerBlue;
                            Grd1.Cells[Row, 26].Value = lEL[i].LeaveName;
                            if (mLeaveCodeGetMealTransport.Length > 0)
                            {
                                foreach (string mLeave in mLeaves)
                                {
                                    if (lEL[i].LeaveCode == mLeave)
                                    {
                                        Grd1.Rows[Row].BackColor = Color.SpringGreen;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }



            }
            Grd1.EndUpdate();
        }


        private void SetDt(int r, int c, string Dt)
        {
            if (Dt.Length == 0)
                Grd1.Cells[r, c].Value = string.Empty;
            else
                Grd1.Cells[r, c].Value = Sm.ConvertDate(Dt);
        }

        private void SetTm(int r, int c, string Tm)
        {
            if (Tm.Length == 0)
                Grd1.Cells[r, c].Value = string.Empty;
            else
                Grd1.Cells[r, c].Value = Sm.Left(Tm, 2) + ":" + Tm.Substring(2, 2);
        }

        internal List<OTList> ListOfOT()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mEmpCode = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (mEmpCode.Length > 0) mEmpCode += ",";
                    mEmpCode += Sm.GetGrdStr(Grd1, i, 0);
                }
            }

            mlOT.Clear();

            SQL.AppendLine("SELECT GROUP_CONCAT(A.DocNo) DocNo, A.OTDt, B.EmpCode FROM TblOTRequestHdr A ");
            SQL.AppendLine("INNER JOIN TblOTRequestDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE A.DeptCode = @DeptCode ");
            SQL.AppendLine("AND A.SiteCode = @SiteCode ");
            SQL.AppendLine("AND A.OTDt BETWEEN @DocDt1 AND @DocDt2 ");
            SQL.AppendLine("And Find_In_Set(B.EmpCode, @EmpCode) ");
            SQL.AppendLine("AND A.CancelInd = 'N' ");
            SQL.AppendLine("AND A.Status IN ('A','O') ");
            SQL.AppendLine("GROUP BY B.EmpCode, A.OTDt ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm.Connection = cn2;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteStartDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteEndDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);

                var dr2 = cm.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-2
                         "OTDt", "EmpCode"                         
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        mlOT.Add(new OTList()
                        {
                            DocNo = Sm.DrStr(dr2, c2[0]),
                            OTDt = Sm.DrStr(dr2, c2[1]),
                            EmpCode = Sm.DrStr(dr2, c2[2]),
                        });
                    }
                }
                dr2.Close();
            }

            return mlOT;
        }

        private void ComputeOT()
        {
            if (!mIsAtdShowOT) return;

            List<OTList> OTList = ListOfOT();

            if(Grd1.Rows.Count > 0 && OTList.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    foreach (var li in OTList
                        .Where(x => x.EmpCode == Sm.GetGrdStr(Grd1, i, 0) &&
                                    x.OTDt == Sm.Left(Sm.GetGrdDate(Grd1, i, 20), 8))
                        )
                    {
                        Grd1.Cells[i, 30].Value = li.DocNo;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                
                LueAGCode.EditValue = null;
                Sm.ClearGrd(Grd1, false);
                SetLueAGCode(ref LueAGCode, string.Empty, Sm.GetLue(LueDeptCode));
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
                Sm.ClearGrd(Grd1, false);
            }
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue3(SetLueAGCode), string.Empty, Sm.GetLue(LueDeptCode));
                Sm.ClearGrd(Grd1, false);
            }
        }

        #endregion

        #region Class

        private class Employee
        {
            public string EmpCode { set; get; }
        }

        private class Date
        {
            public string DayName { set; get; }
        }

        private class WS
        {
            public string Dt { get; set; }
            public string Dt2 { get; set; }
            public string EmpCode { get; set; }
            public bool IsOneDay { get; set; }
            public string LogIn { get; set; }
            public string bLogIn { get; set; }
            public string aLogIn { get; set; }
            public string LogOut { get; set; }
            public string bLogOut { get; set; }
            public string aLogOut { get; set; }
            public string AGCode { get; set; }
            public string AGName { get; set; }
            public string InDtL { get; set; }
            public string InTmL { get; set; }
            public string OutDtL { get; set; }
            public string OutTmL { get; set; }
            public bool IsWarning { get; set; }
        }

        private class Log
        {
            public string EmpCode { get; set; }
            public string DtTm { get; set; }
        }

        private class EmpLeave
        {
            public string EmpCode { get; set; }
            public string LeaveDt { get; set; }
            public string LeaveName { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveColor { get; set; }
        }

        private class ExportedColumnHdr
        {
            public string ColName { get; set; }
        }

        private class ExportedColumnDtl
        {
            public string No { get; set; }
            public bool Check { get; set; }
            public string Date { get; set; }
            public string Employee { get; set; }
            public string AttendanceLogIN { get; set; }
            public string AttendanceLogOUT { get; set; }
            public string WorkingScheduleIN { get; set; }
            public string WorkingScheduleOUT { get; set; }
            public string AttendanceVSWorkingSchedule { get; set; }
            public string ActualInputIN { get; set; }
            public string ActualInputOUT { get; set; }
            public string ActualDateIN { get; set; }
            public string ActualTimeIN { get; set; }
            public string ActualDateOUT { get; set; }
            public string ActualTimeOUT { get; set; }
            public string Info { get; set; }
            public string Remark { get; set; }
        }

        private class SumExportedColumnDtl
        {
            public decimal Salary { get; set; }
            public decimal PerformanceAllowance { get; set; }
            public decimal GeneralAllowanceAmt { get; set; }
            public decimal PerformanceAllowanceAmt { get; set; }
            public decimal YearOfServiceAllowanceAmt { get; set; }
            public decimal VariableAllowanceAmt { get; set; }
            public decimal TotalTHP { get; set; }
            public decimal OTAllowance { get; set; }
            public decimal Brutto { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal TotalDeduction { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal SalaryAfterDeduction { get; set; }
            public decimal THP { get; set; }
        }

        internal class OTList
        {
            public string DocNo { get; set; }
            public string OTDt { get; set; }
            public string EmpCode { get; set; }
        }

        #endregion
    }
}
