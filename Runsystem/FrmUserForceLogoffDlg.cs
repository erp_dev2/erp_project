﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUserForceLogoffDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmUserForceLogoff mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmUserForceLogoffDlg(FrmUserForceLogoff FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                base.FrmLoad(sender, e);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "User"+Environment.NewLine+"Code", 
                    "User"+Environment.NewLine+"Name",
                    "Group",
                    "Login"+Environment.NewLine+"Date",
                    "Login"+Environment.NewLine+"Time", 
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    100, 180, 200, 120, 100
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatTime(Grd1, new int[] { 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.UserCode, B.UserName, C.GrpName, A.Login ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select UserCode, Max(Login) Login ");
            SQL.AppendLine("    From TblLog ");
            SQL.AppendLine("    Where LogOut Is Null ");
            SQL.AppendLine("    And UserCode != @UserCode ");
            SQL.AppendLine("    Group By UserCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("        B.ExpDt Is Null ");
            SQL.AppendLine("        Or ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            B.ExpDt Is Not Null ");
            SQL.AppendLine("            And B.ExpDt > Replace(CurDate(), '-', '') ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblGroup C On B.GrpCode = C.GrpCode ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtUserName.Text, new string[] { "A.UserCode", "B.UserName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL() + Filter + " Order By A.UserCode; ",
                    new string[]
                    {
                        //0
                        "UserCode", 
                            
                        //1-3
                        "UserName", "GrpName", "Login"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("T", Grd1, dr, c, Row, 5, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtUserCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtUserName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtUserName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkUserName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "User");
        }

        #endregion

        #endregion

    }
}
