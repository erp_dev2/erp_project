﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Globalization;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetRequest : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string mBudgetBasedOn = string.Empty; // 1 = Department, 2 = Site
        internal FrmBudgetRequestFind FrmFind;
        private bool mIsFilterBySite = false;
        private bool mIsFilterByDept = false;

        #endregion

        #region Constructor

        public FrmBudgetRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, "");

                if (mBudgetBasedOn == "1")
                {
                    SetLueDeptCode(ref LueDeptCode);
                }
                if (mBudgetBasedOn == "2")
                {
                    LblDeptCode.Text = "Site";
                    SetLueSiteCode(ref LueDeptCode);
                }
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        "Month",
                        "Month",
                        "Amount",
                        "%"
                    },
                    new int[] 
                    {
                        0, 150, 120, 80
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueYr, LueDeptCode }, true);
                    Grd1.ReadOnly = true;
                    LueYr.Focus();
                    break;
                default:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueYr, LueDeptCode }, false);
                    Grd1.ReadOnly = false;
                    LueYr.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { LueYr, LueDeptCode });
            Sm.ClearGrd(Grd1, true);
            ShowMonth();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBudgetRequestFind(this, mBudgetBasedOn, mIsFilterBySite, mIsFilterByDept);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "")) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                for (int Row = 0; Row < 12; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveBudgetRequest(Row));

                Sm.ExecCommands(cml);

                ShowData(Sm.GetLue(LueYr), Sm.GetLue(LueDeptCode));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueDeptCode, (mBudgetBasedOn=="1")?"Department":"Site") ||
                IsBudgetRequestExisted();
        }

        private bool IsBudgetRequestExisted()
        {
            if (!LueYr.Properties.ReadOnly && 
                Sm.IsDataExist("Select Yr From TblBudgetRequest Where Concat(Yr, DeptCode) ='" + Sm.GetLue(LueYr) + Sm.GetLue(LueDeptCode) + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Year : " + Sm.GetLue(LueYr) + Environment.NewLine +
                    ((mBudgetBasedOn=="1")?"Department : ":"Site : " ) + LueDeptCode.GetColumnValue("Col2") + Environment.NewLine + Environment.NewLine +
                    "Budget request already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveBudgetRequest(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBudgetRequest(Yr, DeptCode, Mth, Amt, CreateBy, CreateDt) " +
                    "Values(@Yr, @DeptCode, @Mth, @Amt, @UserCode, CurrentDateTime()) " +
                    "On Duplicate Key " +
                    "   Update Amt=@amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); "
            };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        internal void ShowData(string Yr, string DeptCode)
        { 
            try
            {
                int Row = 0;
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = "Select Mth, Amt From TblBudgetRequest Where Yr=@Yr And DeptCode=@DeptCode Order By Mth;";
                    Sm.CmParam<String>(ref cm, "@Yr", Yr);
                    Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "Mth", "Amt" });
                    if (!dr.HasRows)
                    {
                        Sm.StdMsg(mMsgType.NoData, "");
                        ShowMonth();
                    }
                    else
                    {
                        Sm.SetLue(LueYr, Yr);
                        if (mBudgetBasedOn=="2") SetLueSiteCode(ref LueDeptCode);
                        Sm.SetLue(LueDeptCode, DeptCode);
                        Grd1.BeginUpdate();
                        Sm.ClearGrd(Grd1, true);
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Grd1.Cells[Row, 1].Value = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Int32.Parse(dr.GetString(c[0])));
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 1);
                            Row++;
                        }

                        Grd1.Cells[Row, 1].Value = "Total";
                        Grd1.Cells[Row, 2].Value = 0m;
                        Grd1.Cells[Row, 3].Value = 100m;

                        Grd1.Rows[12].BackColor = Color.Coral;
                    }


                    ComputeTotalAmount();
                    ComputePercentage();

                    Grd1.EndUpdate();
                    dr.Close();
                    Sm.FocusGrd(Grd1, 0, 1);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.RowIndex == 12 && e.ColIndex == 2) e.DoDefault = false;
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 2 && e.RowIndex != 12)
            {
                ComputeTotalAmount();
                ComputePercentage();
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsFilterByDept = Sm.GetParameter("IsFilterByDept") == "Y";
        }

        private void ShowMonth()
        {
            Sm.ClearGrd(Grd1, true);
            Grd1.Rows.Count = 13;
            for (int Mth = 1; Mth <= 12; Mth++)
            {
                Grd1.Cells[Mth - 1, 0].Value = Sm.Right("0" + Mth.ToString(), 2);
                Grd1.Cells[Mth - 1, 1].Value = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Mth);
                Grd1.Cells[Mth - 1, 2].Value = Grd1.Cells[Mth - 1, 3].Value = 0m;
            }
            Grd1.Cells[12, 1].Value = "Total";
            Grd1.Cells[12, 2].Value = 0m;
            Grd1.Cells[12, 3].Value = 100m;
            
            Grd1.Rows[12].BackColor = Color.Coral;
        }

        private void ComputeTotalAmount()
        {
            decimal TotalAmount = 0m;

            for (int Row=0;Row<12;Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0) TotalAmount += Sm.GetGrdDec(Grd1, Row, 2);

            Grd1.Cells[12, 2].Value = TotalAmount;    
        }

        private void ComputePercentage()
        {
            decimal Amount = 0m, TotalAmount = 0m;

            for (int Row = 0; Row < 12; Row++)
            {
                Amount = TotalAmount = 0m;
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0) Amount = Sm.GetGrdDec(Grd1, Row, 2);
                if (Sm.GetGrdStr(Grd1, 12, 2).Length != 0) TotalAmount = Sm.GetGrdDec(Grd1, 12, 2);

                if (TotalAmount == 0m)
                    Grd1.Cells[Row, 3].Value = 0m;
                else
                    Grd1.Cells[Row, 3].Value = (Amount / TotalAmount) * 100;
            }
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");
                
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (mIsFilterBySite) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mBudgetBasedOn=="1")
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
            if (mBudgetBasedOn == "2")
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueSiteCode));
        }

        #endregion

        #endregion
    }
}
