﻿#region Update
/*
    26/06/2019 [WED] nominal Qty dkk masuk sebagai default nominal nya
    27/06/2019 [WED] ambil data nya dari Project Implementation RBP
    28/06/2019 [WED] kalkulasi amount header main nya langsung
    01/07/2019 [WED] tambah vendor
    18/07/2019 [WED] tambah qty
    11/09/2019 [WED] ubah alur nya ke SO ContractRevision
    06/07/2020 [DITA/YK] RemunerationAmt dan DirectCostAmt ambil dari total yg sudah dijumlahkan dengan ssemployee, tax, dll
    07/07/2020 [DITA/YK] saat show data detail RBPDirectCostAmt masih bug ambil dari RemunerationAmt
    05/10/2022 [BRI/VIR] Bug RemunerationAmt & DirectCostAmt
    02/11/2022 [BRI/VIR] Bug : Dropping request partial
    03/11/2022 [BRI/VIR] Bug cancel data
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDroppingRequestDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDroppingRequest mFrmParent;
        private string 
            mSQL = string.Empty,
            mPRJIDocNo = string.Empty,
            mMth = string.Empty,
            mYr = string.Empty;

        #endregion

        #region Constructor

        public FrmDroppingRequestDlg2(FrmDroppingRequest FrmParent, string PRJIDocNo, string Mth, string Yr)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPRJIDocNo = PRJIDocNo;
            mMth = Mth;
            mYr = Yr;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ResourceItCode, D.ItName, D.ItGrpCode, E.ItGrpName, A.DocNo, B.DNo, B.Mth, B.Yr, ");
            if (mFrmParent.mIsDroppingRequestWithdraw)
            {
                SQL.AppendLine("If(B.RemunerationAmt > 0, B.RemunerationAmt+ B.TaxAmt + B.SSHealthAmt + B.SSEmploymentAmt + B.OthersAmt - IfNull(K.RemunerationAmt, 0.00), 0.00) RemunerationAmt, ");
                SQL.AppendLine("If(B.DirectCostAmt > 0, B.DirectCostAmt+ B.TaxAmt + B.SSHealthAmt + B.SSEmploymentAmt + B.OthersAmt - IfNull(K.DirectCostAmt, 0.00), 0.00) DirectCostAmt, ");
            }  
            else
            {
                SQL.AppendLine("If(B.RemunerationAmt > 0, B.RemunerationAmt+ B.TaxAmt + B.SSHealthAmt + B.SSEmploymentAmt + B.OthersAmt, 0.00) RemunerationAmt, ");
                SQL.AppendLine("If(B.DirectCostAmt > 0, B.DirectCostAmt+ B.TaxAmt + B.SSHealthAmt + B.SSEmploymentAmt + B.OthersAmt, 0.00) DirectCostAmt, ");
            }
            SQL.AppendLine("B.VdCode, F.VdName, IfNull(B.Remark, A.Remark) Remark ");
            SQL.AppendLine("From TblProjectImplementationRBPHdr A ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.PRJIDocNo = @PRJIDocNo ");
            SQL.AppendLine("    And B.Mth = @Mth ");
            SQL.AppendLine("    And B.Yr = @Yr ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl2 C On A.PRJIDocNo = C.DocNo And A.ResourceItCode = C.ResourceItCode ");
            SQL.AppendLine("Inner Join TblItem D On C.ResourceItCode = D.ItCode ");
            SQL.AppendLine("Left Join TblItemGroup E On D.ItGrpCode = E.ItGrpCode ");
            SQL.AppendLine("Left Join TblVendor F On B.VdCode = F.VdCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblProjectImplementationHdr G On A.PRJIDocNo=G.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr G1 On G.SOContractDocNo = G1.Docno ");
                SQL.AppendLine("Inner Join TblSOContractHdr H On G1.SOCDocNo=H.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr I On H.BOQDocNo=I.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr J On I.LOPDocNo=J.DocNo ");
                SQL.AppendLine("And (J.SiteCode Is Null Or ( ");
                SQL.AppendLine("    J.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(J.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsDroppingRequestWithdraw)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select B.PRBPDocNo, B.PRBPDNo, Sum(B.RemunerationAmt) RemunerationAmt, Sum(B.DirectCostAmt) DirectCostAmt ");
                SQL.AppendLine("    From TblDroppingRequestHdr A ");
                SQL.AppendLine("    Inner Join TblDroppingRequestDtl B On A.DocNo = B.DocNo And A.Status= 'A' And A.CancelInd= 'N' And A.ProcessInd= 'F' ");
                SQL.AppendLine("    Group By B.PRBPDocNo, B.PRBPDNo ");
                SQL.AppendLine(")K On B.DocNo = K.PRBPDocNo And B.DNo = K.PRBPDNo ");
                SQL.AppendLine("Where Locate(Concat('##', A.ResourceItCode, '##'), @SelectedItCode)<1 ");
                SQL.AppendLine("And( If(B.RemunerationAmt > 0, B.RemunerationAmt + B.TaxAmt + B.SSHealthAmt + B.SSEmploymentAmt + B.OthersAmt - IfNull(K.RemunerationAmt, 0.00), 0.00) > 0 ");
                SQL.AppendLine("Or If(B.DirectCostAmt > 0, B.DirectCostAmt + B.TaxAmt + B.SSHealthAmt + B.SSEmploymentAmt + B.OthersAmt - IfNull(K.DirectCostAmt, 0.00), 0.00) > 0) ");
            }
            else
                SQL.AppendLine("Where Locate(Concat('##', A.ResourceItCode, '##'), @SelectedItCode)<1 ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Resource Item Code",
                    "Resource Item Name",
                    "Item Group Code",
                    "Item Group",

                    //6-10
                    "RBPDocNo",
                    "RBPDNo",
                    "Month",
                    "Year",
                    "Remuneration",

                    //11-14
                    "Direct Cost",
                    "Vendor Code",
                    "Vendor",
                    "Remark"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 150, 220, 120, 200, 

                    //6-10
                    0, 0, 80, 80, 120, 
                    
                    //11-14
                    120, 100, 200, 200
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 6, 7, 12 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 12 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@PRJIDocNo", mPRJIDocNo);
                Sm.CmParam<String>(ref cm, "@Mth", mMth);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@SelectedItCode", mFrmParent.GetSelectedItCode());

                Sm.FilterStr(ref Filter, ref cm, TxtResourceItCode.Text, new string[] { "A.ResourceItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocNo; ",
                    new string[]
                    {
                        //0
                        "ResourceItCode", 

                        //1-5
                        "ItName", "ItGrpCode", "ItGrpName", "DocNo", "DNo",

                        //6-10
                        "Mth", "Yr", "RemunerationAmt", "DirectCostAmt", "VdCode",

                        //11-12
                        "VdName", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 14);
                        mFrmParent.Grd1.Cells[Row1, 18].Value = 0m;
                        mFrmParent.ComputeTotalAmt();
                        //mFrmParent.SetLueVdBankAccount(ref mFrmParent.LueVdBankAcDNo2, string.Empty, string.Empty);
                        if (mFrmParent.mStateInd == "I")
                        {
                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 9, 13, 14, 18 });
                        }
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 resource.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), ItCode)) return true;
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtResourceItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkResourceItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Resource");
        }

        #endregion

        #endregion

    }
}