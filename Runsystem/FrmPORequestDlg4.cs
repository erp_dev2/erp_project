﻿#region Update
/*
  10/06/2019 [WED] dialog baru untuk tambah informasi di header POR
  13/06/2019 [MEY] label1 dan dterepairdt di hide 
*/
#endregion 

#region Namespace
using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection; // Assembly
using System.IO; //Path

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;
#endregion

namespace RunSystem
{
    public partial class FrmPORequestDlg4 : RunSystem.FrmBase7
    {
        #region Field

        internal FrmPORequest mFrmParent;
        private string mSQL = string.Empty;
        internal string mMenuCode = string.Empty;
        #endregion

        #region Constructor

        public FrmPORequestDlg4(FrmPORequest FrmParent) // , ref List<FrmPORequest.PORYKHdr> lPOR2Hdr
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetFormControl(mState.View);

                if (mFrmParent.TxtDocNo.Text.Length > 0)
                {
                    ShowDataHdr(mFrmParent.TxtDocNo.Text);
                }
                else
                {
                    Sm.SetDteCurrentDate(DteRepairDt);
                    Sm.SetDteCurrentDate(DteRevisionDt);

                    if (mFrmParent.lPOR2Hdr != null)
                        if (mFrmParent.lPOR2Hdr.Count > 0)
                            ShowDataHdrTemp(ref mFrmParent.lPOR2Hdr);
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtConditionDesc, TxtTax, TxtTransferOfTitle, MeeBasisForProposal, TxtFormNo, TxtRevisionNo, DteRepairDt, DteRevisionDt, MeeRemarkTotal
                    }, (mFrmParent.TxtDocNo.Text.Length > 0));
                    DteRepairDt.Focus();
                    BtnRefresh.Visible = false;
                    BtnSave.Visible = (mFrmParent.TxtDocNo.Text.Length <= 0);
                    TxtTax.EditValue = Sm.FormatNum(0m, 0);
                    DteRepairDt.Visible = false;
                    label1.Visible = false;
                    
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtConditionDesc, TxtTransferOfTitle, MeeBasisForProposal, TxtFormNo, TxtRevisionNo, DteRepairDt, DteRevisionDt, MeeRemarkTotal
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtTax
            }, 0);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;

                SaveTempAdditionalHdr();
                this.Hide();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Show Data

        private void ShowDataHdrTemp(ref List<FrmPORequest.PORYKHdr> lPOR2Hdr) 
        {  
            for (int i = 0; i < lPOR2Hdr.Count; i++)
            {
                Sm.SetDte(DteRepairDt, mFrmParent.lPOR2Hdr[i].RepairDt);
                TxtConditionDesc.EditValue = mFrmParent.lPOR2Hdr[i].ConditionDesc;
                TxtTax.EditValue = Sm.FormatNum(mFrmParent.lPOR2Hdr[i].Tax, 0);
                TxtTransferOfTitle.EditValue = mFrmParent.lPOR2Hdr[i].TransferOfTitle;
                MeeBasisForProposal.EditValue = mFrmParent.lPOR2Hdr[i].BasisForProposal;
                TxtFormNo.EditValue = mFrmParent.lPOR2Hdr[i].FormNo;
                TxtRevisionNo.EditValue = mFrmParent.lPOR2Hdr[i].RevisionNo;
                Sm.SetDte(DteRevisionDt, mFrmParent.lPOR2Hdr[i].RevisionDt);
                MeeRemarkTotal.EditValue =mFrmParent.lPOR2Hdr[i].RemarkTotal;
            }
        }

        private void ShowDataHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select RepairDt, ConditionDesc, Tax, TransferOfTitle, BasisForProposal, FormNo, ");
            SQL.AppendLine("RevisionNo, RevisionDt, RemarkTotal ");
            SQL.AppendLine("From TblPORequest2Hdr ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    "RepairDt", 
                    "ConditionDesc", "Tax", "TransferOfTitle", "BasisForProposal", "FormNo",
                    "RevisionNo", "RevisionDt", "RemarkTotal",
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    Sm.SetDte(DteRepairDt, Sm.DrStr(dr, c[0]));
                    TxtConditionDesc.EditValue = Sm.DrStr(dr, c[1]);
                    TxtTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                    TxtTransferOfTitle.EditValue = Sm.DrStr(dr, c[3]);
                    MeeBasisForProposal.EditValue = Sm.DrStr(dr, c[4]);
                    TxtFormNo.EditValue = Sm.DrStr(dr, c[5]);
                    TxtRevisionNo.EditValue = Sm.DrStr(dr, c[6]);
                    Sm.SetDte(DteRevisionDt, Sm.DrStr(dr, c[7]));
                    MeeRemarkTotal.EditValue = Sm.DrStr(dr, c[8]);
                }, false
            );
        }

        #endregion

        #region Save Data

        private void SaveTempAdditionalHdr()
        {
            if (mFrmParent.lPOR2Hdr != null) mFrmParent.lPOR2Hdr.Clear();
            else mFrmParent.lPOR2Hdr = new List<FrmPORequest.PORYKHdr>();

            mFrmParent.lPOR2Hdr.Add(new FrmPORequest.PORYKHdr()
            {
                RepairDt = Sm.GetDte(DteRepairDt),
                ConditionDesc = TxtConditionDesc.Text,
                Tax = decimal.Parse(TxtTax.Text),
                TransferOfTitle = TxtTransferOfTitle.Text,
                BasisForProposal = MeeBasisForProposal.Text,
                FormNo = TxtFormNo.Text,
                RevisionNo = TxtRevisionNo.Text,
                RevisionDt = Sm.GetDte(DteRevisionDt),
                RemarkTotal = MeeRemarkTotal.Text,
            });
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtTax_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTax, 0);
        }

        #endregion

        #endregion

    }
}
