﻿#region Update
/*
    07/02/2018 [TKG] Reporting DO to customer with price
    28/02/2018 [TKG] tambah printout
    04/04/2018 [HAR] tambah informasi sales invoice
    08/04/2018 [HAR] tambah informasi sales invocie
    12/05/2018 [TKG] tambah informasi fico
    21/07/2018 [TKG] tambah informasi remark
    25/07/2018 [ARI] saat filter item, tampilan di printout juga ngikut
    09/08/2018 [TKG] tambah filter status do
    10/08/2018 [TKG] Berdasarkan parameter IsDOCtAmtRounded, amount di-rounding (0.01-05 dibulatkan ke bawah, di atas 0.5 dibulatkan ke atas). 
    30/09/2018 [TKG] menghilangkan angka di belakang koma
    02/10/2018 [MEY] tambah kolom Sales Invoice Date
    23/10/2021 [TKG] ubah query
    15/11/2021 [DEV/AMKA] Membuat kolom Warehouse pada Reporting Sales Reporting terotorisasi berdasarkan group login dan parameter IsFilterByWarehouse
    24/11/2021 [NJP/PHT] Menambahkan multi profit center di sales reporting Menggunakan Parameter IsFicoUseMultiProfitCenterFilter, IsFilterByProfitCenter
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDOCtWithPrice : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, 
            mMenuCodeForRptDOCtWithPriceForFICO = string.Empty;
        private bool 
            mIsFilterByItCt = false, 
            mIsDOCtAmtRounded = false, 
            mIsFilterByWarehouse = false,
            mIsFicoUseMultiProfitCenterFilter = false, 
            mIsFilterByProfitCenter = false,
            mIsAllProfitCenterSelected = false;
        private int mNumberOfInventoryUomCode = 1;
        private List<String> mlProfitCenter;

        #endregion

        #region Constructor

        public FrmRptDOCtWithPrice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueCtCode(ref LueCtCode);
                BtnPrint.Hide();
                mlProfitCenter = new List<String>();
                CcbProfitCenterCode.Visible = label4.Visible = ChkProfitCenterCode.Visible = mIsFicoUseMultiProfitCenterFilter;
                if (mIsFicoUseMultiProfitCenterFilter)
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                else
                    panel2.Height -= CcbProfitCenterCode.Height;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mMenuCodeForRptDOCtWithPriceForFICO = Sm.GetParameter("MenuCodeForRptDOCtWithPriceForFICO");
            mIsDOCtAmtRounded = Sm.GetParameterBoo("IsDOCtAmtRounded");
            mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
            mIsFilterByWarehouse = Sm.GetParameterBoo("IsFilterByWarehouse");
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("D.CtName, C.WhsName, B.Lot, B.Bin, ");
            SQL.AppendLine("B.ItCode, E.ItName, E.ForeignName, B.BatchNo, B.Source, ");
            SQL.AppendLine("B.Qty, E.InventoryUOMCode, B.Qty2, E.InventoryUOMCode2, B.Qty3, E.InventoryUOMCode3, ");
            SQL.AppendLine("A.CurCode, B.UPrice, B.Qty*B.UPrice As Amt, F.SalesInvoiceDocNo, F.SalesInvoiceDocDt, ");
            SQL.AppendLine("I.AcNo, I.AcDesc, G.ItCtName, H.ItScName, ");
            SQL.AppendLine("A.Remark ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblWarehouse C on A.WhsCode=C.WhsCode ");
            if (mIsFilterByWarehouse)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=C.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblCustomer D on A.CtCode=D.CtCode ");
            SQL.AppendLine("Inner Join TblItem E on B.ItCode=E.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X2.DocNo, X2.DNo, X4.DocNo As SalesInvoiceDocNo, X4.DocDt As SalesInvoiceDocDt ");
            SQL.AppendLine("    From TblDOCtHdr X1 ");
            SQL.AppendLine("    Inner Join TblDOCtDtl X2 On X1.DocNo=X2.DocNo And X2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl X3 On X2.DocNo=X3.DOCtDocNo And X2.DNo=X3.DOCtDNo ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr X4 On X3.DocNo=X4.DocNo And X4.CancelInd='N' ");
            SQL.AppendLine("    Where X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And X1.Status In ('O', 'A') ");
            SQL.AppendLine(") F On B.DocNo=F.DocNo And B.DNo=F.DNo ");
            SQL.AppendLine("Left Join TblItemCategory G On E.ItCtCode=G.ItCtCode ");
            SQL.AppendLine("Left Join TblItemSubCategory H On E.ItScCode=H.ItScCode ");
            SQL.AppendLine("Left Join TblCOA I On G.AcNo=I.AcNo ");
            SQL.AppendLine("INNER JOIN  tblcostcenter J On J.CCCode = C.CCCode ");
            SQL.AppendLine("INNER JOIN  tblprofitcenter K On J.ProfitCenterCode = K.ProfitCenterCode");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.Status In ('O', 'A') ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Document#",
                        "Date",
                        "Status",
                        "Customer",
                        "Warehouse",
                        
                        //6-10
                        "Lot",
                        "Bin",
                        "Item's Code",
                        "Item's Name",
                        "Foreign Name",
                        
                        //11-15
                        "Batch#",
                        "Source",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Currency",
                        "Price",
                        
                        //21-25
                        "Amount",
                        "Sales Invoice#",
                        "COA Account#",
                        "COA Account Description",
                        "Item's Category",
                        
                        //26-28
                        "Item's Sub Category",
                        "Remark",
                        "Sales Invoice's"+Environment.NewLine+"Date"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 100, 200, 200, 
                        
                        //6-10
                        60, 60, 100, 200, 150, 

                        //11-15
                        150, 180, 100, 80, 100, 

                        //16-20
                        80, 100, 80, 60, 120, 
                        
                        //21-25
                        120, 150, 100, 200, 200, 

                        //26-28
                        200, 400, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17, 20, 21 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 28 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 10, 12, 15, 16, 17, 18, 23, 24, 25, 26 }, false);
            if (Sm.CompareStr(mMenuCode, mMenuCodeForRptDOCtWithPriceForFICO))
                Sm.GrdColInvisible(Grd1, new int[] { 23, 24, 25, 26 }, true);
            ShowInventoryUomCode();
            Grd1.Cols[28].Move(27);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 10, 12, 23 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var SQL = new StringBuilder();
                string Filter = " ";

                var cm = new MySqlCommand();

                
            SetProfitCenter();

            if (mIsFicoUseMultiProfitCenterFilter)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter2 = string.Empty;
                    int i = 0;

                    SQL.AppendLine("    And C.CCCode Is Not Null ");
                    SQL.AppendLine("    And C.CCCode In ( ");
                    SQL.AppendLine("        Select Distinct CCCode ");
                    SQL.AppendLine("        From TblCostCenter ");
                    SQL.AppendLine("        Where ActInd='Y' ");
                    SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("        And ProfitCenterCode In ( ");
                    SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter2.Length > 0) Filter2 += " Or ";
                        Filter2 += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                        i++;
                    }
                    if (Filter2.Length == 0)
                        SQL.AppendLine("    And 1=0 ");
                    else
                        SQL.AppendLine("    And (" + Filter2 + ") ");
                    SQL.AppendLine("    ) ");
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("    And C.CCCode Is Not Null ");
                        SQL.AppendLine("    And C.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                        SQL.AppendLine("    ) ");
                        if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        SQL.AppendLine("    And (C.CCCode Is Null Or (C.CCCode Is Not Null And C.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In (");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        )))) ");
                    }
                }
            }
                

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "E.ItName", "E.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + SQL.ToString() + Filter + "Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[]
                        {
                            //0
                            "DocNo",  

                            //1-5
                            "DocDt", "StatusDesc", "CtName", "WhsName", "Lot", 

                            //6-10
                            "Bin", "ItCode", "ItName", "ForeignName", "BatchNo", 

                            //11-15
                            "Source", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                            
                            //16-20
                            "Qty3", "InventoryUomCode3", "CurCode", "UPrice", "Amt", 
                            
                            //21-25
                            "SalesInvoiceDocNo", "AcNo", "AcDesc", "ItCtName", "ItScName", 

                            //26-27
                            "Remark", "SalesInvoiceDocDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            if (mIsDOCtAmtRounded)
                                Grd.Cells[Row, 21].Value = decimal.Truncate(dr.GetDecimal(c[20]));
                            else
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 28, 27);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 15, 17, 21 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional

        private void BtnPrint2_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            {
                var l = new List<DoToCustomerWP>();

                string[] TableName = { "DoToCustomerWP" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("A.DocNo, Date_Format(A.Docdt,'%d %b %Y')As DocDt, D.CtName, A.SAAddress, E.ItName, B.Qty, E.InventoryUOMCode, ");
                SQL.AppendLine("B.Qty2, E.InventoryUOMCode2, A.CurCode, B.UPrice, B.Qty*B.UPrice As Amt, A.Remark, ");
                SQL.AppendLine("Date_Format(@DocDt1,'%d %M %Y')As DocDt1, Date_Format(@DocDt2,'%d %M %Y')As DocDt2 ");
                SQL.AppendLine("From TblDOCtHdr A ");
                SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQL.AppendLine("Inner Join TblWarehouse C on A.WhsCode=C.WhsCode ");
                SQL.AppendLine("Inner Join TblCustomer D on A.CtCode=D.CtCode ");
                SQL.AppendLine("Inner Join TblItem E on B.ItCode=E.ItCode ");
                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
                if(Sm.GetLue(LueCtCode).Length>0)
                    SQL.AppendLine("And A.CtCode=@CtCode");

                if (TxtItCode.Text.Length>0)
                    SQL.AppendLine("And (B.ItCode Like @ItCode Or E.ItName Like @ItName) ");

                SQL.AppendLine("Order By A.DocDt ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                    Sm.CmParam<String>(ref cm, "@ItCode", string.Concat("%", (TxtItCode.Text), "%"));
                    Sm.CmParam<String>(ref cm, "@ItName", string.Concat("%",(TxtItCode.Text),"%"));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "DocNo",
                         "DocDt",
                         "CtName",
                         "SAAddress",
                         "ItName",

                         //11-15
                         "Qty",
                         "InventoryUOMCode",
                         "Qty2",
                         "InventoryUOMCode2",
                         "CurCode",

                         //16-20
                         "UPrice",
                         "Amt",
                         "Remark",
                         "DocDt1",
                         "DocDt2"

                        });
                    if (dr.HasRows)
                    {
                        int nomor = 0;
                        while (dr.Read())
                        {
                            nomor = nomor + 1;
                            l.Add(new DoToCustomerWP()
                            {
                                nomor = nomor,
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressFull = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                DocNo = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7]),
                                CtName = Sm.DrStr(dr, c[8]),
                                SAAddress = Sm.DrStr(dr, c[9]),
                                ItName = Sm.DrStr(dr, c[10]),

                                Qty = Sm.DrDec(dr, c[11]),
                                InventoryUOMCode = Sm.DrStr(dr, c[12]),
                                Qty2 = Sm.DrDec(dr, c[13]),
                                InventoryUOMCode2 = Sm.DrStr(dr, c[14]),

                                CurCode = Sm.DrStr(dr, c[15]),
                                UPrice = Sm.DrDec(dr, c[16]),
                                Amt = Sm.DrDec(dr, c[17]),
                                Remark = Sm.DrStr(dr, c[18]),
                                DocDt1 = Sm.DrStr(dr, c[19]),
                                DocDt2 = Sm.DrStr(dr, c[20]),
                                Customer = LueCtCode.Text,

                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                Sm.PrintReport("DoToCustomerWP", myLists, TableName, false);

            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("    Where ProfitCenterCode In ( ");
                SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

      
        #endregion

        #region Class

        private class DoToCustomerWP
        {
            public int nomor { get; set; }
            public string CompanyLogo { set; get; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }

            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string SAAddress { get; set; }
            public string ItName { get; set; }

            public decimal Qty { get; set; }
            public string InventoryUOMCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUOMCode2 { get; set; }
            public string CurCode { get; set; }

            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; } 
            public string DocDt1 { get; set; }
            public string DocDt2 { get; set; }
            public string Customer { get; set; } 
        }

        #endregion

    }
}
