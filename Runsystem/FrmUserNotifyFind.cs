﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmUserNotifyFind : RunSystem.FrmBase2
    {
        #region Field
        private FrmUserNotify mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmUserNotifyFind(FrmUserNotify FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocCode, Case When A.DocCode='01' Then 'Employee Contrat Document' End As DocName, A.UserCode, B.UserName,  A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblUserNotify A ");
            SQL.AppendLine("Inner Join Tbluser B On A.UserCode = B.UserCode ");

            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document"+Environment.NewLine+"Code",
                        "Document",
                        "User Code",
                        "User Name",
                        "Created"+Environment.NewLine+"By",
                        
                        //6-10
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6, 9 });
            Sm.GrdFormatTime(Grd1, new int[] { 7, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 6, 7, 8, 9, 10 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtUserCode.Text, new string[] { "A.UserCode", "B.UserName" });
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.UserName;",
                        new string[]
                        {
                             //0
                             "DocCode",
                             
                             //1-5
                             "Docname","UserCode", "UserName", "CreateBy", "CreateDt", 
                             
                             //6
                              "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 7);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkUserCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "User");
        }

        #endregion

        #endregion
    }
}
