﻿namespace RunSystem
{
    partial class FrmDOVd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDOVd));
            this.TcRecvVd = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.ChkReplacedItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.memoExEdit1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.DteKBContractDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.DteKBPLDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtKBPackagingQty = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.DteKBRegistrationDt = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnPEB = new DevExpress.XtraEditors.SimpleButton();
            this.LueCustomsDocCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtKBSubmissionNo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtKBPackaging = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnKBContractNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtKBRegistrationNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtKBPLNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtKBContractNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcRecvVd)).BeginInit();
            this.TcRecvVd.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkReplacedItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackagingQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCustomsDocCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBSubmissionNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackaging.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBRegistrationNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPLNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBContractNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(730, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcRecvVd);
            this.panel2.Size = new System.Drawing.Size(730, 175);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 175);
            this.panel3.Size = new System.Drawing.Size(730, 298);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(730, 298);
            this.Grd1.TabIndex = 23;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TcRecvVd
            // 
            this.TcRecvVd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcRecvVd.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcRecvVd.Location = new System.Drawing.Point(0, 0);
            this.TcRecvVd.Name = "TcRecvVd";
            this.TcRecvVd.SelectedTabPage = this.Tp1;
            this.TcRecvVd.Size = new System.Drawing.Size(730, 175);
            this.TcRecvVd.TabIndex = 12;
            this.TcRecvVd.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(724, 147);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtLocalDocNo);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.ChkReplacedItemInd);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.LueWhsCode);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.LueVdCode);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(724, 147);
            this.panel5.TabIndex = 22;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(81, 47);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 16;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtLocalDocNo.TabIndex = 42;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(34, 50);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 14);
            this.label11.TabIndex = 41;
            this.label11.Text = "Local#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkReplacedItemInd
            // 
            this.ChkReplacedItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkReplacedItemInd.Location = new System.Drawing.Point(250, 5);
            this.ChkReplacedItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkReplacedItemInd.Name = "ChkReplacedItemInd";
            this.ChkReplacedItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkReplacedItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkReplacedItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkReplacedItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkReplacedItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkReplacedItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkReplacedItemInd.Properties.Caption = "Replaced Items";
            this.ChkReplacedItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkReplacedItemInd.Size = new System.Drawing.Size(119, 22);
            this.ChkReplacedItemInd.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(8, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 14);
            this.label4.TabIndex = 43;
            this.label4.Text = "Warehouse";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(81, 68);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 25;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 500;
            this.LueWhsCode.Size = new System.Drawing.Size(366, 20);
            this.LueWhsCode.TabIndex = 44;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(81, 110);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(592, 20);
            this.MeeRemark.TabIndex = 48;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 113);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 14);
            this.label1.TabIndex = 47;
            this.label1.Text = "Remark";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(30, 92);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 14);
            this.label3.TabIndex = 45;
            this.label3.Text = "Vendor";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(81, 89);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 25;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 500;
            this.LueVdCode.Size = new System.Drawing.Size(366, 20);
            this.LueVdCode.TabIndex = 46;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(81, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(121, 20);
            this.DteDocDt.TabIndex = 40;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(44, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 39;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(81, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 37;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(4, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 14);
            this.label12.TabIndex = 36;
            this.label12.Text = "Document#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.PageVisible = false;
            this.Tp2.Size = new System.Drawing.Size(766, 147);
            this.Tp2.Text = "Kawasan Berikat";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Controls.Add(this.BtnPEB);
            this.panel6.Controls.Add(this.LueCustomsDocCode);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.TxtKBSubmissionNo);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.TxtKBPackaging);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.BtnKBContractNo);
            this.panel6.Controls.Add(this.TxtKBRegistrationNo);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.TxtKBPLNo);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.TxtKBContractNo);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 147);
            this.panel6.TabIndex = 22;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.TxtAmt);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.LueCurCode);
            this.panel4.Controls.Add(this.memoExEdit1);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.DteKBContractDt);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.DteKBPLDt);
            this.panel4.Controls.Add(this.TxtKBPackagingQty);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.DteKBRegistrationDt);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(435, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(331, 147);
            this.panel4.TabIndex = 37;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(224, 152);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(129, 20);
            this.TxtAmt.TabIndex = 51;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(5, 155);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(83, 14);
            this.label22.TabIndex = 49;
            this.label22.Text = "Total Amount";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(94, 152);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 150;
            this.LueCurCode.Size = new System.Drawing.Size(127, 20);
            this.LueCurCode.TabIndex = 50;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // memoExEdit1
            // 
            this.memoExEdit1.EnterMoveNextControl = true;
            this.memoExEdit1.Location = new System.Drawing.Point(94, 174);
            this.memoExEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit1.Name = "memoExEdit1";
            this.memoExEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit1.Properties.MaxLength = 400;
            this.memoExEdit1.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.memoExEdit1.Properties.ShowIcon = false;
            this.memoExEdit1.Size = new System.Drawing.Size(259, 20);
            this.memoExEdit1.TabIndex = 53;
            this.memoExEdit1.ToolTip = "F4 : Show/hide text";
            this.memoExEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit1.ToolTipTitle = "Run System";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(41, 177);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 14);
            this.label25.TabIndex = 52;
            this.label25.Text = "Remark";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBContractDt
            // 
            this.DteKBContractDt.EditValue = null;
            this.DteKBContractDt.EnterMoveNextControl = true;
            this.DteKBContractDt.Location = new System.Drawing.Point(131, 32);
            this.DteKBContractDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBContractDt.Name = "DteKBContractDt";
            this.DteKBContractDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBContractDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBContractDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBContractDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBContractDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBContractDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBContractDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBContractDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBContractDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBContractDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBContractDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBContractDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBContractDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBContractDt.Size = new System.Drawing.Size(185, 20);
            this.DteKBContractDt.TabIndex = 39;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(44, 35);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 14);
            this.label8.TabIndex = 38;
            this.label8.Text = "Contract Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBPLDt
            // 
            this.DteKBPLDt.EditValue = null;
            this.DteKBPLDt.EnterMoveNextControl = true;
            this.DteKBPLDt.Location = new System.Drawing.Point(131, 53);
            this.DteKBPLDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBPLDt.Name = "DteKBPLDt";
            this.DteKBPLDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBPLDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBPLDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBPLDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBPLDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBPLDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBPLDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBPLDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBPLDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBPLDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBPLDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBPLDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBPLDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBPLDt.Size = new System.Drawing.Size(185, 20);
            this.DteKBPLDt.TabIndex = 41;
            // 
            // TxtKBPackagingQty
            // 
            this.TxtKBPackagingQty.EnterMoveNextControl = true;
            this.TxtKBPackagingQty.Location = new System.Drawing.Point(131, 116);
            this.TxtKBPackagingQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPackagingQty.Name = "TxtKBPackagingQty";
            this.TxtKBPackagingQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPackagingQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtKBPackagingQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtKBPackagingQty.Size = new System.Drawing.Size(185, 20);
            this.TxtKBPackagingQty.TabIndex = 45;
            this.TxtKBPackagingQty.Validated += new System.EventHandler(this.TxtKBPackagingQty_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(28, 57);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 14);
            this.label9.TabIndex = 40;
            this.label9.Text = "Packing List Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(16, 118);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 14);
            this.label15.TabIndex = 44;
            this.label15.Text = "Packaging Quantity";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBRegistrationDt
            // 
            this.DteKBRegistrationDt.EditValue = null;
            this.DteKBRegistrationDt.EnterMoveNextControl = true;
            this.DteKBRegistrationDt.Location = new System.Drawing.Point(131, 74);
            this.DteKBRegistrationDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBRegistrationDt.Name = "DteKBRegistrationDt";
            this.DteKBRegistrationDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBRegistrationDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBRegistrationDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBRegistrationDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBRegistrationDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBRegistrationDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBRegistrationDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBRegistrationDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBRegistrationDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBRegistrationDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBRegistrationDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBRegistrationDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBRegistrationDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBRegistrationDt.Size = new System.Drawing.Size(185, 20);
            this.DteKBRegistrationDt.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(27, 78);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 14);
            this.label10.TabIndex = 42;
            this.label10.Text = "Registration Date";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPEB
            // 
            this.BtnPEB.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPEB.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPEB.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPEB.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPEB.Appearance.Options.UseBackColor = true;
            this.BtnPEB.Appearance.Options.UseFont = true;
            this.BtnPEB.Appearance.Options.UseForeColor = true;
            this.BtnPEB.Appearance.Options.UseTextOptions = true;
            this.BtnPEB.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPEB.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPEB.Image = ((System.Drawing.Image)(resources.GetObject("BtnPEB.Image")));
            this.BtnPEB.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPEB.Location = new System.Drawing.Point(361, 4);
            this.BtnPEB.Name = "BtnPEB";
            this.BtnPEB.Size = new System.Drawing.Size(24, 21);
            this.BtnPEB.TabIndex = 26;
            this.BtnPEB.ToolTip = "Show PEB Document";
            this.BtnPEB.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPEB.ToolTipTitle = "Run System";
            this.BtnPEB.Click += new System.EventHandler(this.BtnPEB_Click);
            // 
            // LueCustomsDocCode
            // 
            this.LueCustomsDocCode.EnterMoveNextControl = true;
            this.LueCustomsDocCode.Location = new System.Drawing.Point(160, 4);
            this.LueCustomsDocCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCustomsDocCode.Name = "LueCustomsDocCode";
            this.LueCustomsDocCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.Appearance.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCustomsDocCode.Properties.DropDownRows = 30;
            this.LueCustomsDocCode.Properties.NullText = "[Empty]";
            this.LueCustomsDocCode.Properties.PopupWidth = 400;
            this.LueCustomsDocCode.Size = new System.Drawing.Size(177, 20);
            this.LueCustomsDocCode.TabIndex = 24;
            this.LueCustomsDocCode.ToolTip = "F4 : Show/hide list";
            this.LueCustomsDocCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCustomsDocCode.Validated += new System.EventHandler(this.LueCustomsDocCode_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(7, 8);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(146, 14);
            this.label16.TabIndex = 23;
            this.label16.Text = "Customs Document Code";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBSubmissionNo
            // 
            this.TxtKBSubmissionNo.EnterMoveNextControl = true;
            this.TxtKBSubmissionNo.Location = new System.Drawing.Point(160, 96);
            this.TxtKBSubmissionNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBSubmissionNo.Name = "TxtKBSubmissionNo";
            this.TxtKBSubmissionNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBSubmissionNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBSubmissionNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBSubmissionNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBSubmissionNo.Properties.MaxLength = 30;
            this.TxtKBSubmissionNo.Size = new System.Drawing.Size(225, 20);
            this.TxtKBSubmissionNo.TabIndex = 34;
            this.TxtKBSubmissionNo.Validated += new System.EventHandler(this.TxtKBSubmissionNo_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(78, 100);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 14);
            this.label14.TabIndex = 33;
            this.label14.Text = "Submission#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBPackaging
            // 
            this.TxtKBPackaging.EnterMoveNextControl = true;
            this.TxtKBPackaging.Location = new System.Drawing.Point(160, 117);
            this.TxtKBPackaging.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPackaging.Name = "TxtKBPackaging";
            this.TxtKBPackaging.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPackaging.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPackaging.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPackaging.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPackaging.Properties.MaxLength = 80;
            this.TxtKBPackaging.Size = new System.Drawing.Size(225, 20);
            this.TxtKBPackaging.TabIndex = 36;
            this.TxtKBPackaging.Validated += new System.EventHandler(this.TxtKBPackaging_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(92, 119);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 14);
            this.label13.TabIndex = 35;
            this.label13.Text = "Packaging";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnKBContractNo
            // 
            this.BtnKBContractNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnKBContractNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnKBContractNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnKBContractNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnKBContractNo.Appearance.Options.UseBackColor = true;
            this.BtnKBContractNo.Appearance.Options.UseFont = true;
            this.BtnKBContractNo.Appearance.Options.UseForeColor = true;
            this.BtnKBContractNo.Appearance.Options.UseTextOptions = true;
            this.BtnKBContractNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnKBContractNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnKBContractNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnKBContractNo.Image")));
            this.BtnKBContractNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnKBContractNo.Location = new System.Drawing.Point(342, 4);
            this.BtnKBContractNo.Name = "BtnKBContractNo";
            this.BtnKBContractNo.Size = new System.Drawing.Size(24, 21);
            this.BtnKBContractNo.TabIndex = 25;
            this.BtnKBContractNo.ToolTip = "Show Kawasan Berikat\'s Document";
            this.BtnKBContractNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnKBContractNo.ToolTipTitle = "Run System";
            this.BtnKBContractNo.Click += new System.EventHandler(this.BtnKBContractNo_Click);
            // 
            // TxtKBRegistrationNo
            // 
            this.TxtKBRegistrationNo.EnterMoveNextControl = true;
            this.TxtKBRegistrationNo.Location = new System.Drawing.Point(160, 75);
            this.TxtKBRegistrationNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBRegistrationNo.Name = "TxtKBRegistrationNo";
            this.TxtKBRegistrationNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBRegistrationNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBRegistrationNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBRegistrationNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBRegistrationNo.Properties.MaxLength = 30;
            this.TxtKBRegistrationNo.Size = new System.Drawing.Size(225, 20);
            this.TxtKBRegistrationNo.TabIndex = 32;
            this.TxtKBRegistrationNo.Validated += new System.EventHandler(this.TxtKBRegistrationNo_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(73, 79);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 14);
            this.label7.TabIndex = 31;
            this.label7.Text = "Registration#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBPLNo
            // 
            this.TxtKBPLNo.EnterMoveNextControl = true;
            this.TxtKBPLNo.Location = new System.Drawing.Point(160, 54);
            this.TxtKBPLNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPLNo.Name = "TxtKBPLNo";
            this.TxtKBPLNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPLNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPLNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPLNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPLNo.Properties.MaxLength = 30;
            this.TxtKBPLNo.Size = new System.Drawing.Size(225, 20);
            this.TxtKBPLNo.TabIndex = 30;
            this.TxtKBPLNo.Validated += new System.EventHandler(this.TxtKBPLNo_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(74, 58);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Packing List#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBContractNo
            // 
            this.TxtKBContractNo.EnterMoveNextControl = true;
            this.TxtKBContractNo.Location = new System.Drawing.Point(160, 33);
            this.TxtKBContractNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBContractNo.Name = "TxtKBContractNo";
            this.TxtKBContractNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBContractNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBContractNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBContractNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBContractNo.Properties.MaxLength = 30;
            this.TxtKBContractNo.Size = new System.Drawing.Size(225, 20);
            this.TxtKBContractNo.TabIndex = 28;
            this.TxtKBContractNo.Validated += new System.EventHandler(this.TxtKBContractNo_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(90, 36);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Contract#";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmDOVd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 473);
            this.Name = "FrmDOVd";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcRecvVd)).EndInit();
            this.TcRecvVd.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkReplacedItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackagingQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCustomsDocCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBSubmissionNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackaging.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBRegistrationNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPLNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBContractNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl TcRecvVd;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.LookUpEdit LueCustomsDocCode;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtKBSubmissionNo;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtKBPackaging;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtKBPackagingQty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.DateEdit DteKBRegistrationDt;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DteKBPLDt;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteKBContractDt;
        public DevExpress.XtraEditors.SimpleButton BtnKBContractNo;
        internal DevExpress.XtraEditors.TextEdit TxtKBRegistrationNo;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtKBPLNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtKBContractNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.CheckEdit ChkReplacedItemInd;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueVdCode;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.SimpleButton BtnPEB;
        protected System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit1;
        private System.Windows.Forms.Label label25;
        protected internal DevExpress.XtraEditors.LookUpEdit LueWhsCode;

    }
}