﻿#region Update
/*
    16/07/2019 [WED] bug saat filter tanggal
    23/07/2019 [DITA] Grouping per Divisi/Wilayah (Dropping Request Find)
    24/07/2019 [WED] Tambah kolom informasi ProcessInd dan MR#
    22/08/2019 [WED] ubah alur ke SO Contract Revision
    20/09/2022 [RDA/VIR] penarikan dropping request menjadi partial berdasarkan param IsDroppingRequestUseMRReceivingPartial (RCV)
    28/09/2022 [BRI/VIR] penarikan dropping request menjadi partial berdasarkan param IsDroppingRequestUseMRReceivingPartial (MR)
    04/01/2023 [BRI/MNET] tambah type berdasarkan param IsDroppingRequestUseType
    06/02/2023 [RDA/MNET] tambah kolom realization yg isi dan judul kolomnya melihat param IsFindMRinformationDroppingRequestActive dan IsDroppingRequestUseType
    24/02/2023 [WED/MNET] kolom realization dan source nya dipagerin parameter IsDroppingRequestUseType
    27/02/2023 [RDA/MNET] tambah kolom usage date dan purpose berdasarkan parameter IsDroppingRequestUseUsageDate dan IsDroppingRequestUsePurpose
    14/03/2023 [RDA/MNET] tambah kolom status
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDroppingRequestFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDroppingRequest mFrmParent;
        private string mSQL = string.Empty;
        private bool 
            mIsFilterBySite = false,
            mIsFilterByDept = false;

        #endregion

        #region Constructor

        public FrmDroppingRequestFind(FrmDroppingRequest FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueMth(ref LueMth);
                SetLueYr(ref LueYr);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT X1.*, ");
            if (mFrmParent.mIsDroppingRequestUseType) SQL.AppendLine("X2.Realization ");
            else SQL.AppendLine("Null As Realization ");
            SQL.AppendLine("FROM( ");
            SQL.AppendLine("Select A.*, B.ItName, C.BCName, D.CtName From( ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.PRJIDocNo, E.ProjectName, E.CtCode, I.OptDesc AS ProjectType, A.DeptCode, NULL As DeptName, A.Mth, A.Yr, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, E.SiteCode, F.SiteName, ");
            SQL.AppendLine("Case A.ProcessInd When 'D' Then 'Draft' When 'F' Then 'Final' Else '' End As ProcessInd, ");
            if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial)
                SQL.AppendLine("J.MRDocNo, ");
            else
                SQL.AppendLine("G.MRDocNo, ");
            if (mFrmParent.mIsDroppingRequestUseType)
                SQL.AppendLine("K.OptDesc DocType, ");
            else
                SQL.AppendLine("NULL as DocType, ");
            SQL.AppendLine("H.ResourceItCode As ItCode, NULL As BCCode ");
            SQL.AppendLine(", A.UsageDt, A.Purpose, Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else '' End As Status ");
            SQL.AppendLine("From TblDroppingRequestHdr A ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.PRJIDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B1 On B.SOContractDocno = B1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B1.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (E.SiteCode Is Null Or ( ");
                SQL.AppendLine("    E.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblSite F On E.SiteCode = F.SiteCode ");
            SQL.AppendLine("Inner Join TblDroppingRequestDtl G On A.DocNo = G.DocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr H On G.PRBPDocNo = H.DocNo ");
            SQL.AppendLine("Inner Join TblOption I On E.ProjectType = I.OptCode And I.OptCat = 'ProjectType' ");
            if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial)
            {
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("	SELECT GROUP_CONCAT(A.DocNo) MRDocNo, I.DocNo AS DroppingReqDocNo, ");
                SQL.AppendLine("	M.ItCode, M.ItName ");
                SQL.AppendLine("	FROM tblmaterialrequesthdr A ");
                SQL.AppendLine("	INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblporequestdtl C ON C.MaterialRequestDocNo = B.DocNo AND C.MaterialRequestDNo = B.DNo ");
                SQL.AppendLine("	LEFT JOIN tblporequesthdr D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblpodtl E ON C.DocNo = E.PORequestDocNo AND C.DNo = E.PORequestDNo ");
                SQL.AppendLine("	LEFT JOIN tblpohdr F ON E.DocNo = F.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblrecvvddtl G ON G.PODocNo = E.DocNo AND G.PODNo = E.DNo AND G.CancelInd = 'N' ");
                SQL.AppendLine("	LEFT JOIN tblrecvvdhdr H ON G.DocNo = H.DocNo ");
                SQL.AppendLine("	INNER JOIN tbldroppingrequesthdr I ON A.DroppingRequestDocNo = I.DocNo ");
                SQL.AppendLine("	INNER JOIN tbldroppingrequestdtl J ON I.DocNo = J.DocNo ");
                SQL.AppendLine("	INNER JOIN TblProjectImplementationRBPHdr K On J.PRBPDocNo = K.DocNo AND B.ItCode = K.ResourceItCode ");
                SQL.AppendLine("	INNER JOIN TblProjectImplementationRBPDtl L On K.DocNo = L.DocNo And J.PRBPDNo = L.DNo  ");
                SQL.AppendLine("	LEFT JOIN tblitem M ON K.ResourceItCode = M.ItCode ");
                SQL.AppendLine("	WHERE A.Status = 'A' AND A.CancelInd = 'N' ");
                SQL.AppendLine("	AND B.CancelInd = 'N' AND B.Status = 'A' ");
                SQL.AppendLine("	GROUP BY I.DocNo, M.ItCode, M.ItName ");
                SQL.AppendLine(")J ON A.DocNo = J.DroppingReqDocNo AND H.ResourceItCode = J.ItCode ");
            }
            if (mFrmParent.mIsDroppingRequestUseType)
                SQL.AppendLine("Left Join TblOption K On A.DocType = K.OptCode And K.OptCat = 'DroppingRequestDocType' ");

            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.PRJIDocNo, NULL AS ProjectName, NULL As CtCode, Null As ProjectType, A.DeptCode, B.DeptName, A.Mth, A.Yr, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, NULL As SiteCode, NULL As SiteName, ");
            SQL.AppendLine("Case A.ProcessInd When 'D' Then 'Draft' When 'F' Then 'Final' Else '' End As ProcessInd, ");
            if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial)
                SQL.AppendLine("D.MRDocNo, ");
            else
                SQL.AppendLine("C.MRDocNo, ");
            if (mFrmParent.mIsDroppingRequestUseType)
                SQL.AppendLine("E.OptDesc DocType, ");
            else
                SQL.AppendLine("NULL as DocType, ");
            SQL.AppendLine("C.ItCode, C.BCCode ");
            SQL.AppendLine(", A.UsageDt, A.Purpose, Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else '' End As Status ");
            SQL.AppendLine("From TblDroppingRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblDroppingRequestDtl2 C On A.DocNo = C.DocNo ");
            if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial)
            {
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("    SELECT GROUP_CONCAT(A.DocNo) MRDocNo, I.DocNo AS DroppingReqDocNo, ");
                SQL.AppendLine("    K.ItCode, K.ItName ");
                SQL.AppendLine("    FROM tblmaterialrequesthdr A ");
                SQL.AppendLine("    INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblporequestdtl C ON C.MaterialRequestDocNo = B.DocNo AND C.MaterialRequestDNo = B.DNo ");
                SQL.AppendLine("    LEFT JOIN tblporequesthdr D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblpodtl E ON C.DocNo = E.PORequestDocNo AND C.DNo = E.PORequestDNo ");
                SQL.AppendLine("    LEFT JOIN tblpohdr F ON E.DocNo = F.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblrecvvddtl G ON G.PODocNo = E.DocNo AND G.PODNo = E.DNo AND G.CancelInd = 'N' ");
                SQL.AppendLine("    LEFT JOIN tblrecvvdhdr H ON G.DocNo = H.DocNo ");
                SQL.AppendLine("    INNER JOIN tbldroppingrequesthdr I ON A.DroppingRequestDocNo = I.DocNo ");
                SQL.AppendLine("    INNER JOIN tbldroppingrequestdtl2 J ON I.DocNo = J.DocNo AND A.DroppingRequestBCCode = J.BCCode AND B.ItCode = J.ItCode ");
                SQL.AppendLine("    LEFT JOIN tblitem K ON J.ItCode = K.ItCode ");
                SQL.AppendLine("    WHERE A.Status = 'A' AND A.CancelInd = 'N' ");
                SQL.AppendLine("    AND B.CancelInd = 'N' AND B.Status = 'A' ");
                SQL.AppendLine("    GROUP BY I.DocNo, K.ItCode, K.ItName ");
                SQL.AppendLine(")D ON C.DocNo = D.DroppingReqDocNo AND C.ItCode = D.ItCode ");
            }
            if (mFrmParent.mIsDroppingRequestUseType)
                SQL.AppendLine("Left Join TblOption E On A.DocType = E.OptCode And E.OptCat = 'DroppingRequestDocType' ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or ( ");
                SQL.AppendLine("    A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=A.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine(")A ");
            SQL.AppendLine("Left Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Left Join TblBudgetCategory C On A.BCCode = C.BCCode ");
            SQL.AppendLine("Left Join TblCustomer D On A.CtCode = D.CtCode ");
            SQL.AppendLine(")X1 ");
            if (mFrmParent.mIsDroppingRequestUseType)
            {
                SQL.AppendLine("LEFT JOIN( ");
                SQL.AppendLine("-- Procurement ");
                SQL.AppendLine("SELECT A.DocNo, GROUP_CONCAT(I.DocNo) AS Realization, A.DocType ");
                SQL.AppendLine("FROM tbldroppingrequesthdr A ");
                SQL.AppendLine("INNER JOIN tblmaterialrequesthdr B ON A.DocNo=B.DroppingRequestDocNo ");
                SQL.AppendLine("INNER JOIN tblmaterialrequestdtl C ON B.DocNo=C.DocNo ");
                SQL.AppendLine("INNER JOIN tblporequestdtl D ON D.MaterialRequestDocNo=C.DocNo AND D.MaterialRequestDNo=C.DNo ");
                SQL.AppendLine("INNER JOIN tblporequesthdr E ON D.DocNo=E.DocNo ");
                SQL.AppendLine("INNER JOIN tblpodtl F ON D.DocNo=F.PORequestDocNo AND D.DNo=F.PORequestDNo ");
                SQL.AppendLine("INNER JOIN tblpohdr G ON F.DocNo=G.DocNo ");
                SQL.AppendLine("INNER JOIN tblrecvvddtl H ON H.PODocNo=F.DocNo AND H.PODNo=F.DNo ");
                SQL.AppendLine("INNER JOIN tblrecvvdhdr I ON H.DocNo=I.DocNo AND I.POInd='Y' ");
                SQL.AppendLine("WHERE A.DocType='1' ");
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("-- Pengadaan Langsung ");
                SQL.AppendLine("SELECT A.DocNo, GROUP_CONCAT(I.DocNo) AS Realization, A.DocType ");
                SQL.AppendLine("FROM tbldroppingrequesthdr A ");
                SQL.AppendLine("INNER JOIN tblmaterialrequesthdr B ON A.DocNo=B.DroppingRequestDocNo ");
                SQL.AppendLine("INNER JOIN tblmaterialrequestdtl C ON B.DocNo=C.DocNo ");
                SQL.AppendLine("INNER JOIN tblporequestdtl D ON D.MaterialRequestDocNo=C.DocNo AND D.MaterialRequestDNo=C.DNo ");
                SQL.AppendLine("INNER JOIN tblporequesthdr E ON D.DocNo=E.DocNo ");
                SQL.AppendLine("INNER JOIN tblpodtl F ON D.DocNo=F.PORequestDocNo AND D.DNo=F.PORequestDNo ");
                SQL.AppendLine("INNER JOIN tblpohdr G ON F.DocNo=G.DocNo ");
                SQL.AppendLine("INNER JOIN tblrecvvddtl H ON H.PODocNo=F.DocNo AND H.PODNo=F.DNo ");
                SQL.AppendLine("INNER JOIN tblrecvvdhdr I ON H.DocNo=I.DocNo AND I.POInd='N' ");
                SQL.AppendLine("WHERE A.DocType='2' ");
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("-- Cash Advance ");
                SQL.AppendLine("SELECT A.DocNo, GROUP_CONCAT(D.DocNo) AS Realization, A.DocType ");
                SQL.AppendLine("FROM tbldroppingrequesthdr A ");
                SQL.AppendLine("INNER JOIN tbldroppingpaymenthdr B ON A.DocNo=B.DRQDocNo ");
                SQL.AppendLine("INNER JOIN tblvoucherhdr C ON B.VoucherRequestDocNo=C.VoucherRequestDocNo ");
                SQL.AppendLine("INNER JOIN tblcashadvancesettlementdtl D ON C.DocNo=D.VoucherDocNo ");
                SQL.AppendLine("WHERE A.DocType='3' ");
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("-- Reimbursement ");
                SQL.AppendLine("SELECT A.DocNo, GROUP_CONCAT(B.DocNo) AS Realization, A.DocType ");
                SQL.AppendLine("FROM tbldroppingrequesthdr A ");
                SQL.AppendLine("INNER JOIN tblpettycashdisbursementhdr B ON B.DroppingRequestDocNo=A.DocNo ");
                SQL.AppendLine("WHERE A.DocType='4' ");
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("-- Payroll ");
                SQL.AppendLine("SELECT A.DocNo, GROUP_CONCAT(E.DocNo) AS Realization, A.DocType ");
                SQL.AppendLine("FROM tbldroppingrequesthdr A ");
                SQL.AppendLine("INNER JOIN tbldroppingrequestdtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("INNER JOIN tblvoucherrequestexternalpayrolldtl C ON C.DroppingRequestDocNo=B.DocNo AND C.DroppingRequestDNo=B.DNo ");
                SQL.AppendLine("INNER JOIN tblvoucherrequestexternalpayrollhdr D ON C.DocNo=D.DocNo ");
                SQL.AppendLine("INNER JOIN tblvoucherhdr E ON D.VoucherRequestDocNo=E.VoucherRequestDocNo ");
                SQL.AppendLine("WHERE A.DocType='5' ");
                SQL.AppendLine(")X2 ON X1.DocNo=X2.DocNo ");
            }

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Process",
                        "Status",

                        //6-10
                        "Month",
                        "Year",
                        "Project Impl.#",
                        "Project Name",
                        "Customer",

                        //11-15
                        "Type",
                        "Department Code",
                        "Department",
                        "Budget Category",
                        "Dropping Request Type",

                        //16-20
                        "Item",
                        "Remark",
                        "Usage Date",
                        "Purpose",
                        !mFrmParent.mIsFindMRinformationDroppingRequestActive && mFrmParent.mIsDroppingRequestUseType ? "Realization" : "MR#",

                        //21-25
                        "",
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",

                        //26-27
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 60, 80, 80,  
                        
                        //6-10
                        80, 80, 150, 200, 180,  
                        
                        //11-15
                        100, 180, 100, 180, 130,  

                        //16-20
                        120, 200, 80, 200, 120,   
                        
                        //21-25
                        20, 100, 100, 100, 100,

                        //26-27
                        100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 21 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 23, 26 });
            Sm.GrdFormatTime(Grd1, new int[] { 24, 27 });
            Sm.GrdColInvisible(Grd1, new int[] { 12, 22, 23, 24, 25, 26, 27 }, false);
            Sm.SetGrdProperty(Grd1, false);
            if (!mFrmParent.mIsDroppingRequestUseType)
                Sm.GrdColInvisible(Grd1, new int[] { 15 }, false);
            if (mFrmParent.mIsFindMRinformationDroppingRequestActive)
                Sm.GrdColInvisible(Grd1, new int[] { 21 }, false);
            if (!mFrmParent.mIsDroppingRequestUseUsageDate)
                Sm.GrdColInvisible(Grd1, new int[] { 18 }, false);
            if (!mFrmParent.mIsDroppingRequestUsePurpose)
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 22, 23, 24, 25, 26, 27 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "X1.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X1.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPRJIDocNo.Text, "X1.PRJIDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "X1.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMth), "X1.Mth", true);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, "X1.ProjectName", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "X1.CtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Order By X1.DocNo, X1.DocDt; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "ProcessInd", "Status", "Mth",

                        //6-10
                        "Yr", "PRJIDocNo", "ProjectName", "CtName", "ProjectType", 
                        
                        //11-15
                        "DeptCode", "DeptName", "BCName", "DocType", "ItName", 

                        //16-20
                        "Remark", "UsageDt", "Purpose",  "MRDocNo", "Realization", 
                        
                        //21-24
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, !mFrmParent.mIsFindMRinformationDroppingRequestActive && mFrmParent.mIsDroppingRequestUseType ? 20 : 19); //source Realization / MR#
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 24);
                    }, true, true, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        private void SetLueMth(ref DXE.LookUpEdit Lue)
        {
            //Sl.SetLookUpEdit(Lue, new string[] { null, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" });

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Mth As Col1, Mth As Col2 ");
            SQL.AppendLine("From TblDroppingRequestHdr ");
            SQL.AppendLine("Order By Mth; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void SetLueYr(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Yr As Col1, Yr As Col2 ");
            SQL.AppendLine("From TblDroppingRequestHdr ");
            SQL.AppendLine("Order By Yr; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length > 0)
            {
                var f = new FrmDroppingRequestFindDlg(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.Realization = Sm.GetGrdStr(Grd1, e.RowIndex, 20);
                f.DocType = Sm.GetValue("Select DocType From TblDroppingRequestHdr Where DocNo=@Param", Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 20).Length > 0)
            {
                var f = new FrmDroppingRequestFindDlg(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.Realization = Sm.GetGrdStr(Grd1, e.RowIndex, 20);
                f.DocType = Sm.GetValue("Select DocType From TblDroppingRequestHdr Where DocNo=@Param", Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
          
        }

        #endregion 

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtPRJIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPRJIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Impl.#");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMth, new Sm.RefreshLue1(SetLueMth));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueYr, new Sm.RefreshLue1(SetLueYr));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkMth_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Month");
        }

        private void ChkYr_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Year");
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion
    }
}
