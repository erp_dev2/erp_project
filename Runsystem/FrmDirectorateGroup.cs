﻿#region Update
/*
    08/04/2022 [TYO/PHT] New Apps
    27/05/2022 [DITA/PHT] tambah panjang karakter directorate group name

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDirectorateGroup : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmDirectorateGroupFind FrmFind;

        #endregion

        #region Constructor

        public FrmDirectorateGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            

            try
            {
                if (this.Text.Length == 0) this.Text = "Master Directorate Group";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                BtnPrint.Visible = false;
                BtnDelete.Visible = false;

                SetLueParent(ref LueParentCode);
                base.FrmLoad(sender, e);
            }

            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDirectorateGroupCode, TxtDirectorateGroupName, LueParentCode, ChkActive, ChkParent
                    }, true);
                    break;


                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                         TxtDirectorateGroupName, LueParentCode, ChkParent
                    }, false);
                    

                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDirectorateGroupName, LueParentCode, ChkActive, ChkParent
                    }, false);
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDirectorateGroupCode, TxtDirectorateGroupName, LueParentCode
            });

            ChkActive.Checked = ChkParent.Checked = false;
        }
        #endregion

        #region Button Method
        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDirectorateGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDirectorateGroupCode.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDirectorateGroupCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDirectorateGroupName, "Directorate Group Name", false);
        }

        private void InsertData()
        {
            try 
            {
                if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;

                string DirectorateGroupCode = string.Empty;

                if (TxtDirectorateGroupCode.Text.Length == 0)
                    DirectorateGroupCode = GenerateGroupCode();

                var cml = new List<MySqlCommand>();
                cml.Add(SaveDirectorateGroup(DirectorateGroupCode));

                Sm.ExecCommands(cml);
                ShowData(DirectorateGroupCode);
            }

            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private string GenerateGroupCode()
        {
            var SQL = new StringBuilder();
            int lenGroupCode = 5;

            var MaxGroupCode = Sm.GetValue("SELECT MAX(LENGTH(DirectorateGroupCode)) FROM TblDirectorateGroup");
            if (MaxGroupCode.Length > 0)
                lenGroupCode = Convert.ToInt32(MaxGroupCode);

            SQL.AppendLine("Select Right(Concat('0000', IfNull(( ");
            SQL.AppendLine("    Select Cast(IfNull(Max(DirectorateGroupCode), '00000') As UNSIGNED)+1 As Value ");
            SQL.AppendLine("    From TblDirectorateGroup ");
            SQL.AppendLine("    WHERE LENGTH(DirectorateGroupCode) = " + lenGroupCode + " ");
            SQL.AppendLine("    Group By DirectorateGroupCode ");
            SQL.AppendLine("    Order By DirectorateGroupCode Desc Limit 1 ");
            SQL.AppendLine("), 1)), " + lenGroupCode + ") As DirectorateGroupCode");

            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveDirectorateGroup(string DirectorateGroupCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblDirectorateGroup(DirectorateGroupCode, DirectorateGroupName, ParentCode, ActInd, NotParentInd, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@DirectorateGroupCode, @DirectorateGroupName, @ParentCode, @ActInd, @ParentInd, @UserCode, CurrentDateTime()) ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DirectorateGroupCode", DirectorateGroupCode);
            Sm.CmParam<String>(ref cm, "@DirectorateGroupName", TxtDirectorateGroupName.Text);
            Sm.CmParam<String>(ref cm, "@ParentCode", Sm.GetLue(LueParentCode));
            Sm.CmParam<String>(ref cm, "@ActInd", "Y");
            Sm.CmParam<String>(ref cm, "@ParentInd", ChkParent.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            string DirectorateGroupCode = TxtDirectorateGroupCode.Text;
            cml.Add(EditDirectorateGroup(DirectorateGroupCode));

            Sm.ExecCommands(cml);
            ShowData(DirectorateGroupCode);
        }

        private MySqlCommand EditDirectorateGroup(string DirectorateGroupCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblDirectorateGroup ");
            SQL.AppendLine("Set DirectorateGroupName = @DirectorateGroupName,ParentCode =@ParentCode, ActInd = @ActInd, NotParentInd = @ParentInd, ");
            SQL.AppendLine("LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DirectorateGroupCode = @DirectorateGroupCode ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DirectorateGroupCode", DirectorateGroupCode);
            Sm.CmParam<String>(ref cm, "@DirectorateGroupName", TxtDirectorateGroupName.Text);
            Sm.CmParam<String>(ref cm, "@ParentCode", Sm.GetLue(LueParentCode));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActive.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ParentInd", ChkParent.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DirectorateGroupCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();

                ShowDirectorateGroup(DirectorateGroupCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDirectorateGroup(string DirectorateGroupCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DirectorateGroupCode", DirectorateGroupCode);

            SQL.AppendLine("SELECT DirectorateGroupCode, DirectorateGroupName, ParentCode, ActInd, NotParentInd ");
            SQL.AppendLine("FROM tbldirectorategroup ");
            SQL.AppendLine("WHERE DirectorateGroupCode = @DirectorateGroupCode ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "DirectorateGroupCode",

                    //1-4
                    "DirectorateGroupName", "ParentCode", "ActInd", "NotParentInd"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDirectorateGroupCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtDirectorateGroupName.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetLue(LueParentCode, Sm.DrStr(dr, c[2]));
                    ChkActive.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    ChkParent.Checked = Sm.DrStr(dr, c[4]) == "Y";
                }, true
            );
        }

        

        #endregion

        #region Additional Method

        internal void SetLueParent(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DirectorateGroupCode As Col1, DirectorateGroupName As Col2 ");
            SQL.AppendLine("From TblDirectorateGroup ");
            SQL.AppendLine("Where NotParentInd = 'N' ");
            SQL.AppendLine("Order By DirectorateGroupName ;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        

        #endregion

        #region Event

        #region Misc Control Event

        private void LueParentCode_EditValueChanged(object sender, EventArgs e)
        {
           
           if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueParentCode, new Sm.RefreshLue1(SetLueParent));

        }

        private void ChkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkActive.Checked == true)
                {
                    string ActiveInd = Sm.GetValue("SELECT A.ActInd FROM tbldirectorategroup A WHERE A.DirectorateGroupCode = " + TxtDirectorateGroupCode.Text + " ");
                    if (ActiveInd == "N")
                    {
                        Sm.StdMsg(mMsgType.Warning, "Directorate Group Can't Set To Active Because It's Already Not Active");
                        ChkActive.Checked = false;
                    }
                }
                    
            }
          

        }

        //private void ChkActive_EditValueChanged(object sender, EventArgs e)
        //{
            
        //    //if (ChkActive.Checked == false)
        //    //{
        //    //    if (ChkActive.Checked == true)
        //    //    {
        //    //        Sm.StdMsg(mMsgType.Warning, "Directorate Group Can't Set To Active Because It's Already Not Active");
        //    //        ChkActive.Checked = false;
        //    //    }
        //    //}
        //}



        #endregion

        #endregion


        #endregion
    }
}
