﻿namespace RunSystem
{
    partial class FrmVoucherRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVoucherRequest));
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.BtnCopy = new DevExpress.XtraEditors.SimpleButton();
            this.LblCopyGeneral = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TcVoucherRequest = new DevExpress.XtraTab.XtraTabControl();
            this.TpGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.TxtBankAcTp = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtBankAcTp2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtBillingID = new DevExpress.XtraEditors.TextEdit();
            this.LblBillingID = new System.Windows.Forms.Label();
            this.TxtAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.LblManualExcRate = new System.Windows.Forms.Label();
            this.LueManualCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtManualExcRate = new DevExpress.XtraEditors.TextEdit();
            this.LblManualCurCode = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.BtnDocType = new DevExpress.XtraEditors.SimpleButton();
            this.DteRefundDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtPaymentUser = new DevExpress.XtraEditors.TextEdit();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.LueAcType = new DevExpress.XtraEditors.LookUpEdit();
            this.LblBankAcCode = new System.Windows.Forms.Label();
            this.LblPaymentUser = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LuePIC = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueAcType2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.LueBankAcCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtExcRate = new DevExpress.XtraEditors.TextEdit();
            this.LblBankAcCode2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtCurCode2 = new DevExpress.XtraEditors.TextEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.LueDocType = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TpAdditional = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BtnEmpCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.LblEmpName = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.LblCashAdvanceTypeCode = new System.Windows.Forms.Label();
            this.LueCashAdvanceTypeCode = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnSOContractDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.LblSOContractDocNo = new System.Windows.Forms.Label();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteOpeningDt = new DevExpress.XtraEditors.DateEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtDocEnclosure = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtBankAcName = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtBankBranch = new DevExpress.XtraEditors.TextEdit();
            this.LblDueDt = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtBankCode = new DevExpress.XtraEditors.TextEdit();
            this.LblGiroNo = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.LblBankCode = new System.Windows.Forms.Label();
            this.TxtBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgProject = new DevExpress.XtraTab.XtraTabPage();
            this.label41 = new System.Windows.Forms.Label();
            this.LueProjectDocNo3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.LueProjectDocNo2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.LueProjectDocNo1 = new DevExpress.XtraEditors.LookUpEdit();
            this.TpCOA = new DevExpress.XtraTab.XtraTabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.TxtDbt = new DevExpress.XtraEditors.TextEdit();
            this.TxtCdt = new DevExpress.XtraEditors.TextEdit();
            this.TxtBalanced = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpApproval = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TpBudget = new DevExpress.XtraTab.XtraTabPage();
            this.BtnBCCode = new DevExpress.XtraEditors.SimpleButton();
            this.LueDeptCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LblDeptCode2 = new System.Windows.Forms.Label();
            this.LueBIOPProject = new DevExpress.XtraEditors.LookUpEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.BtnCOA2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnItCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOCDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSOCDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnItCode = new DevExpress.XtraEditors.SimpleButton();
            this.BtnCOA = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtSOCDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueReqType = new DevExpress.XtraEditors.LookUpEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtRemainingBudget = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpUpload = new DevExpress.XtraTab.XtraTabPage();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.label40 = new System.Windows.Forms.Label();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.TpUpload2 = new DevExpress.XtraTab.XtraTabPage();
            this.LueVRCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpUpload3 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnCSV = new DevExpress.XtraEditors.SimpleButton();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkAutoWidth = new DevExpress.XtraEditors.CheckEdit();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcVoucherRequest)).BeginInit();
            this.TcVoucherRequest.SuspendLayout();
            this.TpGeneral.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcTp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcTp2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBillingID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueManualCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtManualExcRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRefundDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRefundDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaymentUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExcRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            this.TpAdditional.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashAdvanceTypeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOpeningDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOpeningDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocEnclosure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            this.TpgProject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo1.Properties)).BeginInit();
            this.TpCOA.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDbt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCdt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalanced.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            this.TpBudget.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBIOPProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            this.TpUpload.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            this.TpUpload2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueVRCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpUpload3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkAutoWidth);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Controls.Add(this.BtnCSV);
            this.panel1.Location = new System.Drawing.Point(1285, 0);
            this.panel1.Size = new System.Drawing.Size(70, 515);
            this.panel1.Controls.SetChildIndex(this.BtnCSV, 0);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.ChkAutoWidth, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Location = new System.Drawing.Point(0, 149);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnCancel.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Location = new System.Drawing.Point(0, 125);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnSave.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Location = new System.Drawing.Point(0, 101);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnDelete.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Location = new System.Drawing.Point(0, 77);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnEdit.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Location = new System.Drawing.Point(0, 53);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnInsert.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Location = new System.Drawing.Point(0, 29);
            this.BtnFind.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnFind.Size = new System.Drawing.Size(70, 24);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Location = new System.Drawing.Point(0, 173);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnPrint.Size = new System.Drawing.Size(70, 24);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Grd1);
            this.panel2.Controls.Add(this.TcVoucherRequest);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Size = new System.Drawing.Size(1285, 515);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.TxtDocNo);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.DteDocDt);
            this.panel6.Controls.Add(this.TxtLocalDocNo);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1285, 68);
            this.panel6.TabIndex = 8;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.BtnCopy);
            this.panel7.Controls.Add(this.LblCopyGeneral);
            this.panel7.Controls.Add(this.MeeCancelReason);
            this.panel7.Controls.Add(this.ChkCancelInd);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.TxtVoucherDocNo);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(791, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(494, 68);
            this.panel7.TabIndex = 15;
            // 
            // BtnCopy
            // 
            this.BtnCopy.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopy.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopy.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopy.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCopy.Appearance.Options.UseBackColor = true;
            this.BtnCopy.Appearance.Options.UseFont = true;
            this.BtnCopy.Appearance.Options.UseForeColor = true;
            this.BtnCopy.Appearance.Options.UseTextOptions = true;
            this.BtnCopy.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopy.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopy.Image = ((System.Drawing.Image)(resources.GetObject("BtnCopy.Image")));
            this.BtnCopy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopy.Location = new System.Drawing.Point(460, 47);
            this.BtnCopy.Name = "BtnCopy";
            this.BtnCopy.Size = new System.Drawing.Size(24, 18);
            this.BtnCopy.TabIndex = 24;
            this.BtnCopy.ToolTip = "Show Employee List";
            this.BtnCopy.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopy.ToolTipTitle = "Run System";
            this.BtnCopy.Click += new System.EventHandler(this.BtnCopy_Click);
            // 
            // LblCopyGeneral
            // 
            this.LblCopyGeneral.AutoSize = true;
            this.LblCopyGeneral.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCopyGeneral.ForeColor = System.Drawing.Color.Green;
            this.LblCopyGeneral.Location = new System.Drawing.Point(386, 47);
            this.LblCopyGeneral.Name = "LblCopyGeneral";
            this.LblCopyGeneral.Size = new System.Drawing.Size(71, 14);
            this.LblCopyGeneral.TabIndex = 23;
            this.LblCopyGeneral.Tag = "Copy Data From Existing Employee";
            this.LblCopyGeneral.Text = "Copy Data";
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(150, 26);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(335, 20);
            this.MeeCancelReason.TabIndex = 19;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(150, 47);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 20;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 30);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 18;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(150, 5);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 30;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(175, 20);
            this.TxtVoucherDocNo.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(79, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Voucher#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(85, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(188, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(7, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(47, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(85, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(130, 20);
            this.DteDocDt.TabIndex = 12;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(85, 47);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 300;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(188, 20);
            this.TxtLocalDocNo.TabIndex = 14;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(37, 50);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 14);
            this.label26.TabIndex = 13;
            this.label26.Text = "Local#";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(84, 282);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 14);
            this.label12.TabIndex = 59;
            this.label12.Text = "Remark";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(135, 279);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 5000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(700, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(615, 20);
            this.MeeRemark.TabIndex = 60;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TcVoucherRequest
            // 
            this.TcVoucherRequest.Dock = System.Windows.Forms.DockStyle.Top;
            this.TcVoucherRequest.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcVoucherRequest.Location = new System.Drawing.Point(0, 68);
            this.TcVoucherRequest.Name = "TcVoucherRequest";
            this.TcVoucherRequest.SelectedTabPage = this.TpGeneral;
            this.TcVoucherRequest.Size = new System.Drawing.Size(1285, 357);
            this.TcVoucherRequest.TabIndex = 21;
            this.TcVoucherRequest.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpGeneral,
            this.TpAdditional,
            this.TpgProject,
            this.TpCOA,
            this.TpApproval,
            this.TpBudget,
            this.TpUpload,
            this.TpUpload2,
            this.TpUpload3});
            // 
            // TpGeneral
            // 
            this.TpGeneral.Appearance.Header.Options.UseTextOptions = true;
            this.TpGeneral.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpGeneral.Controls.Add(this.panel4);
            this.TpGeneral.Name = "TpGeneral";
            this.TpGeneral.Size = new System.Drawing.Size(1279, 329);
            this.TpGeneral.Text = "General";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.TxtBillingID);
            this.panel4.Controls.Add(this.LblBillingID);
            this.panel4.Controls.Add(this.TxtAmt2);
            this.panel4.Controls.Add(this.LblManualExcRate);
            this.panel4.Controls.Add(this.LueManualCurCode);
            this.panel4.Controls.Add(this.TxtManualExcRate);
            this.panel4.Controls.Add(this.LblManualCurCode);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.BtnDocType);
            this.panel4.Controls.Add(this.DteRefundDt);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtPaymentUser);
            this.panel4.Controls.Add(this.LuePaymentType);
            this.panel4.Controls.Add(this.LueAcType);
            this.panel4.Controls.Add(this.LblBankAcCode);
            this.panel4.Controls.Add(this.LblPaymentUser);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.LueBankAcCode);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.LuePIC);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.LueAcType2);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.LueBankAcCode2);
            this.panel4.Controls.Add(this.TxtExcRate);
            this.panel4.Controls.Add(this.LblBankAcCode2);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.LueCurCode);
            this.panel4.Controls.Add(this.TxtCurCode2);
            this.panel4.Controls.Add(this.LueDeptCode);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.LueDocType);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.TxtAmt);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1279, 329);
            this.panel4.TabIndex = 22;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.TxtBankAcTp);
            this.panel11.Controls.Add(this.label32);
            this.panel11.Controls.Add(this.label28);
            this.panel11.Controls.Add(this.TxtBankAcTp2);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(1115, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(164, 329);
            this.panel11.TabIndex = 63;
            // 
            // TxtBankAcTp
            // 
            this.TxtBankAcTp.EnterMoveNextControl = true;
            this.TxtBankAcTp.Location = new System.Drawing.Point(36, 111);
            this.TxtBankAcTp.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcTp.Name = "TxtBankAcTp";
            this.TxtBankAcTp.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBankAcTp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcTp.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcTp.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcTp.Properties.MaxLength = 80;
            this.TxtBankAcTp.Properties.ReadOnly = true;
            this.TxtBankAcTp.Size = new System.Drawing.Size(125, 20);
            this.TxtBankAcTp.TabIndex = 38;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(0, 114);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(35, 14);
            this.label32.TabIndex = 37;
            this.label32.Text = "Type";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(0, 156);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 14);
            this.label28.TabIndex = 43;
            this.label28.Text = "Type";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcTp2
            // 
            this.TxtBankAcTp2.EnterMoveNextControl = true;
            this.TxtBankAcTp2.Location = new System.Drawing.Point(36, 153);
            this.TxtBankAcTp2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcTp2.Name = "TxtBankAcTp2";
            this.TxtBankAcTp2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBankAcTp2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcTp2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcTp2.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcTp2.Properties.MaxLength = 80;
            this.TxtBankAcTp2.Properties.ReadOnly = true;
            this.TxtBankAcTp2.Size = new System.Drawing.Size(125, 20);
            this.TxtBankAcTp2.TabIndex = 44;
            // 
            // TxtBillingID
            // 
            this.TxtBillingID.EnterMoveNextControl = true;
            this.TxtBillingID.Location = new System.Drawing.Point(135, 301);
            this.TxtBillingID.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBillingID.Name = "TxtBillingID";
            this.TxtBillingID.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBillingID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBillingID.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBillingID.Properties.Appearance.Options.UseFont = true;
            this.TxtBillingID.Properties.MaxLength = 100;
            this.TxtBillingID.Size = new System.Drawing.Size(298, 20);
            this.TxtBillingID.TabIndex = 62;
            // 
            // LblBillingID
            // 
            this.LblBillingID.AutoSize = true;
            this.LblBillingID.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBillingID.ForeColor = System.Drawing.Color.Black;
            this.LblBillingID.Location = new System.Drawing.Point(79, 304);
            this.LblBillingID.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBillingID.Name = "LblBillingID";
            this.LblBillingID.Size = new System.Drawing.Size(52, 14);
            this.LblBillingID.TabIndex = 61;
            this.LblBillingID.Text = "Billing ID";
            this.LblBillingID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt2
            // 
            this.TxtAmt2.EnterMoveNextControl = true;
            this.TxtAmt2.Location = new System.Drawing.Point(315, 195);
            this.TxtAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt2.Name = "TxtAmt2";
            this.TxtAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt2.Properties.ReadOnly = true;
            this.TxtAmt2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtAmt2.Size = new System.Drawing.Size(177, 20);
            this.TxtAmt2.TabIndex = 61;
            // 
            // LblManualExcRate
            // 
            this.LblManualExcRate.AutoSize = true;
            this.LblManualExcRate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblManualExcRate.ForeColor = System.Drawing.Color.Black;
            this.LblManualExcRate.Location = new System.Drawing.Point(549, 198);
            this.LblManualExcRate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblManualExcRate.Name = "LblManualExcRate";
            this.LblManualExcRate.Size = new System.Drawing.Size(73, 14);
            this.LblManualExcRate.TabIndex = 51;
            this.LblManualExcRate.Text = "Manual Rate";
            this.LblManualExcRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueManualCurCode
            // 
            this.LueManualCurCode.EnterMoveNextControl = true;
            this.LueManualCurCode.Location = new System.Drawing.Point(625, 175);
            this.LueManualCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueManualCurCode.Name = "LueManualCurCode";
            this.LueManualCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueManualCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueManualCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueManualCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueManualCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueManualCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueManualCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueManualCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueManualCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueManualCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueManualCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueManualCurCode.Properties.DropDownRows = 25;
            this.LueManualCurCode.Properties.NullText = "[Empty]";
            this.LueManualCurCode.Properties.PopupWidth = 500;
            this.LueManualCurCode.Size = new System.Drawing.Size(177, 20);
            this.LueManualCurCode.TabIndex = 48;
            this.LueManualCurCode.ToolTip = "F4 : Show/hide list";
            this.LueManualCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueManualCurCode.EditValueChanged += new System.EventHandler(this.LueManualCurCode_EditValueChanged);
            // 
            // TxtManualExcRate
            // 
            this.TxtManualExcRate.EnterMoveNextControl = true;
            this.TxtManualExcRate.Location = new System.Drawing.Point(625, 197);
            this.TxtManualExcRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtManualExcRate.Name = "TxtManualExcRate";
            this.TxtManualExcRate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtManualExcRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtManualExcRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtManualExcRate.Properties.Appearance.Options.UseFont = true;
            this.TxtManualExcRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtManualExcRate.Size = new System.Drawing.Size(177, 20);
            this.TxtManualExcRate.TabIndex = 52;
            this.TxtManualExcRate.Validated += new System.EventHandler(this.TxtManualExcRate_Validated);
            // 
            // LblManualCurCode
            // 
            this.LblManualCurCode.AutoSize = true;
            this.LblManualCurCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblManualCurCode.ForeColor = System.Drawing.Color.Black;
            this.LblManualCurCode.Location = new System.Drawing.Point(526, 176);
            this.LblManualCurCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblManualCurCode.Name = "LblManualCurCode";
            this.LblManualCurCode.Size = new System.Drawing.Size(96, 14);
            this.LblManualCurCode.TabIndex = 47;
            this.LblManualCurCode.Text = "Manual Currency";
            this.LblManualCurCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(55, 262);
            this.label14.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 14);
            this.label14.TabIndex = 57;
            this.label14.Text = "Refund Date";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDocType
            // 
            this.BtnDocType.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDocType.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDocType.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDocType.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDocType.Appearance.Options.UseBackColor = true;
            this.BtnDocType.Appearance.Options.UseFont = true;
            this.BtnDocType.Appearance.Options.UseForeColor = true;
            this.BtnDocType.Appearance.Options.UseTextOptions = true;
            this.BtnDocType.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDocType.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDocType.Image = ((System.Drawing.Image)(resources.GetObject("BtnDocType.Image")));
            this.BtnDocType.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDocType.Location = new System.Drawing.Point(495, 46);
            this.BtnDocType.Name = "BtnDocType";
            this.BtnDocType.Size = new System.Drawing.Size(24, 21);
            this.BtnDocType.TabIndex = 29;
            this.BtnDocType.ToolTip = "Show Transaction Document Information";
            this.BtnDocType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDocType.ToolTipTitle = "Run System";
            this.BtnDocType.Click += new System.EventHandler(this.BtnDocType_Click);
            // 
            // DteRefundDt
            // 
            this.DteRefundDt.EditValue = null;
            this.DteRefundDt.EnterMoveNextControl = true;
            this.DteRefundDt.Location = new System.Drawing.Point(135, 258);
            this.DteRefundDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteRefundDt.Name = "DteRefundDt";
            this.DteRefundDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRefundDt.Properties.Appearance.Options.UseFont = true;
            this.DteRefundDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRefundDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteRefundDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteRefundDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteRefundDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRefundDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteRefundDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRefundDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteRefundDt.Properties.MaxLength = 8;
            this.DteRefundDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteRefundDt.Size = new System.Drawing.Size(130, 20);
            this.DteRefundDt.TabIndex = 58;
            // 
            // TxtPaymentUser
            // 
            this.TxtPaymentUser.EnterMoveNextControl = true;
            this.TxtPaymentUser.Location = new System.Drawing.Point(135, 237);
            this.TxtPaymentUser.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaymentUser.Name = "TxtPaymentUser";
            this.TxtPaymentUser.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaymentUser.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaymentUser.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaymentUser.Properties.Appearance.Options.UseFont = true;
            this.TxtPaymentUser.Properties.MaxLength = 100;
            this.TxtPaymentUser.Size = new System.Drawing.Size(298, 20);
            this.TxtPaymentUser.TabIndex = 56;
            this.TxtPaymentUser.Validated += new System.EventHandler(this.TxtPaymentUser_Validated);
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(135, 216);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 8;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 500;
            this.LuePaymentType.Size = new System.Drawing.Size(298, 20);
            this.LuePaymentType.TabIndex = 54;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // LueAcType
            // 
            this.LueAcType.EnterMoveNextControl = true;
            this.LueAcType.Location = new System.Drawing.Point(135, 90);
            this.LueAcType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcType.Name = "LueAcType";
            this.LueAcType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.Appearance.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcType.Properties.DropDownRows = 4;
            this.LueAcType.Properties.NullText = "[Empty]";
            this.LueAcType.Properties.PopupWidth = 130;
            this.LueAcType.Size = new System.Drawing.Size(130, 20);
            this.LueAcType.TabIndex = 34;
            this.LueAcType.ToolTip = "F4 : Show/hide list";
            this.LueAcType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcType.EditValueChanged += new System.EventHandler(this.LueAcType_EditValueChanged);
            // 
            // LblBankAcCode
            // 
            this.LblBankAcCode.AutoSize = true;
            this.LblBankAcCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBankAcCode.ForeColor = System.Drawing.Color.Red;
            this.LblBankAcCode.Location = new System.Drawing.Point(31, 114);
            this.LblBankAcCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBankAcCode.Name = "LblBankAcCode";
            this.LblBankAcCode.Size = new System.Drawing.Size(100, 14);
            this.LblBankAcCode.TabIndex = 35;
            this.LblBankAcCode.Text = "Debit / Credit To";
            this.LblBankAcCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPaymentUser
            // 
            this.LblPaymentUser.AutoSize = true;
            this.LblPaymentUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPaymentUser.ForeColor = System.Drawing.Color.Black;
            this.LblPaymentUser.Location = new System.Drawing.Point(4, 240);
            this.LblPaymentUser.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPaymentUser.Name = "LblPaymentUser";
            this.LblPaymentUser.Size = new System.Drawing.Size(127, 14);
            this.LblPaymentUser.TabIndex = 55;
            this.LblPaymentUser.Text = "Paid To / Received By";
            this.LblPaymentUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(135, 111);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 25;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 650;
            this.LueBankAcCode.Size = new System.Drawing.Size(978, 20);
            this.LueBankAcCode.TabIndex = 36;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(30, 9);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Person In Charge";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePIC
            // 
            this.LuePIC.EnterMoveNextControl = true;
            this.LuePIC.Location = new System.Drawing.Point(135, 6);
            this.LuePIC.Margin = new System.Windows.Forms.Padding(5);
            this.LuePIC.Name = "LuePIC";
            this.LuePIC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.Appearance.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePIC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePIC.Properties.DropDownRows = 25;
            this.LuePIC.Properties.NullText = "[Empty]";
            this.LuePIC.Properties.PopupWidth = 500;
            this.LuePIC.Size = new System.Drawing.Size(356, 20);
            this.LuePIC.TabIndex = 24;
            this.LuePIC.ToolTip = "F4 : Show/hide list";
            this.LuePIC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePIC.EditValueChanged += new System.EventHandler(this.LuePIC_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(44, 219);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 14);
            this.label6.TabIndex = 53;
            this.label6.Text = "Payment Type";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(58, 94);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 14);
            this.label5.TabIndex = 33;
            this.label5.Text = "Debit/Credit";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAcType2
            // 
            this.LueAcType2.EnterMoveNextControl = true;
            this.LueAcType2.Location = new System.Drawing.Point(135, 132);
            this.LueAcType2.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcType2.Name = "LueAcType2";
            this.LueAcType2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.LueAcType2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.Appearance.Options.UseBackColor = true;
            this.LueAcType2.Properties.Appearance.Options.UseFont = true;
            this.LueAcType2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcType2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcType2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcType2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcType2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcType2.Properties.DropDownRows = 4;
            this.LueAcType2.Properties.NullText = "[Empty]";
            this.LueAcType2.Properties.PopupWidth = 130;
            this.LueAcType2.Size = new System.Drawing.Size(130, 20);
            this.LueAcType2.TabIndex = 40;
            this.LueAcType2.ToolTip = "F4 : Show/hide list";
            this.LueAcType2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcType2.EditValueChanged += new System.EventHandler(this.LueAcType2_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(58, 135);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 14);
            this.label24.TabIndex = 39;
            this.label24.Text = "Debit/Credit";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode2
            // 
            this.LueBankAcCode2.EnterMoveNextControl = true;
            this.LueBankAcCode2.Location = new System.Drawing.Point(134, 153);
            this.LueBankAcCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode2.Name = "LueBankAcCode2";
            this.LueBankAcCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode2.Properties.DropDownRows = 25;
            this.LueBankAcCode2.Properties.NullText = "[Empty]";
            this.LueBankAcCode2.Properties.PopupWidth = 650;
            this.LueBankAcCode2.Size = new System.Drawing.Size(979, 20);
            this.LueBankAcCode2.TabIndex = 42;
            this.LueBankAcCode2.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode2.EditValueChanged += new System.EventHandler(this.LueBankAcCode2_EditValueChanged);
            // 
            // TxtExcRate
            // 
            this.TxtExcRate.EnterMoveNextControl = true;
            this.TxtExcRate.Location = new System.Drawing.Point(135, 195);
            this.TxtExcRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExcRate.Name = "TxtExcRate";
            this.TxtExcRate.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtExcRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExcRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExcRate.Properties.Appearance.Options.UseFont = true;
            this.TxtExcRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtExcRate.Size = new System.Drawing.Size(177, 20);
            this.TxtExcRate.TabIndex = 50;
            this.TxtExcRate.Validated += new System.EventHandler(this.TxtExcRate_Validated);
            // 
            // LblBankAcCode2
            // 
            this.LblBankAcCode2.AutoSize = true;
            this.LblBankAcCode2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBankAcCode2.ForeColor = System.Drawing.Color.Black;
            this.LblBankAcCode2.Location = new System.Drawing.Point(31, 156);
            this.LblBankAcCode2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBankAcCode2.Name = "LblBankAcCode2";
            this.LblBankAcCode2.Size = new System.Drawing.Size(100, 14);
            this.LblBankAcCode2.TabIndex = 41;
            this.LblBankAcCode2.Text = "Debit / Credit To";
            this.LblBankAcCode2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(99, 198);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 14);
            this.label16.TabIndex = 49;
            this.label16.Text = "Rate";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(76, 177);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 14);
            this.label15.TabIndex = 45;
            this.label15.Text = "Currency";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(135, 69);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 25;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 500;
            this.LueCurCode.Size = new System.Drawing.Size(177, 20);
            this.LueCurCode.TabIndex = 30;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // TxtCurCode2
            // 
            this.TxtCurCode2.EnterMoveNextControl = true;
            this.TxtCurCode2.Location = new System.Drawing.Point(135, 174);
            this.TxtCurCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode2.Name = "TxtCurCode2";
            this.TxtCurCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode2.Properties.MaxLength = 30;
            this.TxtCurCode2.Properties.ReadOnly = true;
            this.TxtCurCode2.Size = new System.Drawing.Size(177, 20);
            this.TxtCurCode2.TabIndex = 46;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(135, 27);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 25;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 500;
            this.LueDeptCode.Size = new System.Drawing.Size(980, 20);
            this.LueDeptCode.TabIndex = 26;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            this.LueDeptCode.Validated += new System.EventHandler(this.LueDeptCode_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(58, 30);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 14);
            this.label18.TabIndex = 25;
            this.label18.Text = "Department";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDocType
            // 
            this.LueDocType.EnterMoveNextControl = true;
            this.LueDocType.Location = new System.Drawing.Point(135, 48);
            this.LueDocType.Margin = new System.Windows.Forms.Padding(5);
            this.LueDocType.Name = "LueDocType";
            this.LueDocType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.Appearance.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDocType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDocType.Properties.DropDownRows = 25;
            this.LueDocType.Properties.NullText = "[Empty]";
            this.LueDocType.Properties.PopupWidth = 500;
            this.LueDocType.Size = new System.Drawing.Size(356, 20);
            this.LueDocType.TabIndex = 28;
            this.LueDocType.ToolTip = "F4 : Show/hide list";
            this.LueDocType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDocType.EditValueChanged += new System.EventHandler(this.LueDocType_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(96, 51);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 27;
            this.label3.Text = "Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(314, 69);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtAmt.Size = new System.Drawing.Size(177, 20);
            this.TxtAmt.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(80, 72);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 14);
            this.label11.TabIndex = 31;
            this.label11.Text = "Amount";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpAdditional
            // 
            this.TpAdditional.Appearance.Header.Options.UseTextOptions = true;
            this.TpAdditional.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpAdditional.Controls.Add(this.panel5);
            this.TpAdditional.Name = "TpAdditional";
            this.TpAdditional.Size = new System.Drawing.Size(766, 329);
            this.TpAdditional.Text = "Additional Information";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.BtnEmpCode2);
            this.panel5.Controls.Add(this.BtnEmpCode);
            this.panel5.Controls.Add(this.LblEmpName);
            this.panel5.Controls.Add(this.TxtEmpName);
            this.panel5.Controls.Add(this.LblCashAdvanceTypeCode);
            this.panel5.Controls.Add(this.LueCashAdvanceTypeCode);
            this.panel5.Controls.Add(this.BtnSOContractDocNo2);
            this.panel5.Controls.Add(this.BtnSOContractDocNo);
            this.panel5.Controls.Add(this.LblSOContractDocNo);
            this.panel5.Controls.Add(this.TxtSOContractDocNo);
            this.panel5.Controls.Add(this.LblEntCode);
            this.panel5.Controls.Add(this.LueEntCode);
            this.panel5.Controls.Add(this.DteOpeningDt);
            this.panel5.Controls.Add(this.label25);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.TxtDocEnclosure);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.TxtBankAcName);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.DteDueDt);
            this.panel5.Controls.Add(this.TxtBankBranch);
            this.panel5.Controls.Add(this.LblDueDt);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.TxtGiroNo);
            this.panel5.Controls.Add(this.TxtBankCode);
            this.panel5.Controls.Add(this.LblGiroNo);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.LblBankCode);
            this.panel5.Controls.Add(this.TxtBankAcNo);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.LueBankCode);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 329);
            this.panel5.TabIndex = 22;
            // 
            // BtnEmpCode2
            // 
            this.BtnEmpCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode2.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode2.Appearance.Options.UseFont = true;
            this.BtnEmpCode2.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode2.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode2.Image")));
            this.BtnEmpCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode2.Location = new System.Drawing.Point(502, 275);
            this.BtnEmpCode2.Name = "BtnEmpCode2";
            this.BtnEmpCode2.Size = new System.Drawing.Size(16, 16);
            this.BtnEmpCode2.TabIndex = 56;
            this.BtnEmpCode2.ToolTip = "Show Employee";
            this.BtnEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode2.ToolTipTitle = "Run System";
            this.BtnEmpCode2.Click += new System.EventHandler(this.BtnEmpCode2_Click);
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(480, 275);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(16, 16);
            this.BtnEmpCode.TabIndex = 55;
            this.BtnEmpCode.ToolTip = "Find Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click);
            // 
            // LblEmpName
            // 
            this.LblEmpName.AutoSize = true;
            this.LblEmpName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEmpName.ForeColor = System.Drawing.Color.Black;
            this.LblEmpName.Location = new System.Drawing.Point(7, 277);
            this.LblEmpName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEmpName.Name = "LblEmpName";
            this.LblEmpName.Size = new System.Drawing.Size(140, 14);
            this.LblEmpName.TabIndex = 53;
            this.LblEmpName.Text = "Employee Cash Adv. PIC";
            this.LblEmpName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(153, 274);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 30;
            this.TxtEmpName.Properties.ReadOnly = true;
            this.TxtEmpName.Size = new System.Drawing.Size(320, 20);
            this.TxtEmpName.TabIndex = 54;
            // 
            // LblCashAdvanceTypeCode
            // 
            this.LblCashAdvanceTypeCode.AutoSize = true;
            this.LblCashAdvanceTypeCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCashAdvanceTypeCode.ForeColor = System.Drawing.Color.Black;
            this.LblCashAdvanceTypeCode.Location = new System.Drawing.Point(32, 255);
            this.LblCashAdvanceTypeCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCashAdvanceTypeCode.Name = "LblCashAdvanceTypeCode";
            this.LblCashAdvanceTypeCode.Size = new System.Drawing.Size(115, 14);
            this.LblCashAdvanceTypeCode.TabIndex = 51;
            this.LblCashAdvanceTypeCode.Text = "Cash Advance Type";
            this.LblCashAdvanceTypeCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCashAdvanceTypeCode
            // 
            this.LueCashAdvanceTypeCode.EnterMoveNextControl = true;
            this.LueCashAdvanceTypeCode.Location = new System.Drawing.Point(153, 253);
            this.LueCashAdvanceTypeCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCashAdvanceTypeCode.Name = "LueCashAdvanceTypeCode";
            this.LueCashAdvanceTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashAdvanceTypeCode.Properties.Appearance.Options.UseFont = true;
            this.LueCashAdvanceTypeCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashAdvanceTypeCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCashAdvanceTypeCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashAdvanceTypeCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCashAdvanceTypeCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashAdvanceTypeCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCashAdvanceTypeCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashAdvanceTypeCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCashAdvanceTypeCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCashAdvanceTypeCode.Properties.DropDownRows = 25;
            this.LueCashAdvanceTypeCode.Properties.NullText = "[Empty]";
            this.LueCashAdvanceTypeCode.Properties.PopupWidth = 500;
            this.LueCashAdvanceTypeCode.Size = new System.Drawing.Size(365, 20);
            this.LueCashAdvanceTypeCode.TabIndex = 52;
            this.LueCashAdvanceTypeCode.ToolTip = "F4 : Show/hide list";
            this.LueCashAdvanceTypeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCashAdvanceTypeCode.EditValueChanged += new System.EventHandler(this.LueCashAdvanceTypeCode_EditValueChanged);
            // 
            // BtnSOContractDocNo2
            // 
            this.BtnSOContractDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo2.Image")));
            this.BtnSOContractDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo2.Location = new System.Drawing.Point(355, 232);
            this.BtnSOContractDocNo2.Name = "BtnSOContractDocNo2";
            this.BtnSOContractDocNo2.Size = new System.Drawing.Size(16, 16);
            this.BtnSOContractDocNo2.TabIndex = 50;
            this.BtnSOContractDocNo2.ToolTip = "Show SO Contract Document";
            this.BtnSOContractDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo2.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo2.Click += new System.EventHandler(this.BtnSOContractDocNo2_Click);
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(333, 232);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(16, 16);
            this.BtnSOContractDocNo.TabIndex = 49;
            this.BtnSOContractDocNo.ToolTip = "Find SO Contract Document";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo.Click += new System.EventHandler(this.BtnSOContractDocNo_Click);
            // 
            // LblSOContractDocNo
            // 
            this.LblSOContractDocNo.AutoSize = true;
            this.LblSOContractDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSOContractDocNo.ForeColor = System.Drawing.Color.Black;
            this.LblSOContractDocNo.Location = new System.Drawing.Point(64, 233);
            this.LblSOContractDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSOContractDocNo.Name = "LblSOContractDocNo";
            this.LblSOContractDocNo.Size = new System.Drawing.Size(83, 14);
            this.LblSOContractDocNo.TabIndex = 47;
            this.LblSOContractDocNo.Text = "SO Contract#";
            this.LblSOContractDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(153, 230);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 30;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(177, 20);
            this.TxtSOContractDocNo.TabIndex = 48;
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(108, 211);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(39, 14);
            this.LblEntCode.TabIndex = 42;
            this.LblEntCode.Text = "Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(153, 208);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.MaxLength = 40;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 400;
            this.LueEntCode.Size = new System.Drawing.Size(365, 20);
            this.LueEntCode.TabIndex = 43;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // DteOpeningDt
            // 
            this.DteOpeningDt.EditValue = null;
            this.DteOpeningDt.EnterMoveNextControl = true;
            this.DteOpeningDt.Location = new System.Drawing.Point(153, 54);
            this.DteOpeningDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteOpeningDt.Name = "DteOpeningDt";
            this.DteOpeningDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteOpeningDt.Properties.Appearance.Options.UseFont = true;
            this.DteOpeningDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteOpeningDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteOpeningDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteOpeningDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteOpeningDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteOpeningDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteOpeningDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteOpeningDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteOpeningDt.Properties.MaxLength = 8;
            this.DteOpeningDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteOpeningDt.Size = new System.Drawing.Size(109, 20);
            this.DteOpeningDt.TabIndex = 28;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(64, 57);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 14);
            this.label25.TabIndex = 27;
            this.label25.Text = "Opening Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(268, 189);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 14);
            this.label23.TabIndex = 41;
            this.label23.Text = "Pages";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocEnclosure
            // 
            this.TxtDocEnclosure.EnterMoveNextControl = true;
            this.TxtDocEnclosure.Location = new System.Drawing.Point(153, 186);
            this.TxtDocEnclosure.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocEnclosure.Name = "TxtDocEnclosure";
            this.TxtDocEnclosure.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocEnclosure.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocEnclosure.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocEnclosure.Properties.Appearance.Options.UseFont = true;
            this.TxtDocEnclosure.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtDocEnclosure.Size = new System.Drawing.Size(109, 20);
            this.TxtDocEnclosure.TabIndex = 40;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(27, 189);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 14);
            this.label17.TabIndex = 39;
            this.label17.Text = "Document Enclosure";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcName
            // 
            this.TxtBankAcName.EnterMoveNextControl = true;
            this.TxtBankAcName.Location = new System.Drawing.Point(153, 164);
            this.TxtBankAcName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcName.Name = "TxtBankAcName";
            this.TxtBankAcName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcName.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcName.Properties.MaxLength = 100;
            this.TxtBankAcName.Size = new System.Drawing.Size(365, 20);
            this.TxtBankAcName.TabIndex = 38;
            this.TxtBankAcName.Validated += new System.EventHandler(this.TxtBankAcName_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(59, 167);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 14);
            this.label22.TabIndex = 37;
            this.label22.Text = "Account Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(153, 76);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.MaxLength = 8;
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(109, 20);
            this.DteDueDt.TabIndex = 30;
            // 
            // TxtBankBranch
            // 
            this.TxtBankBranch.EnterMoveNextControl = true;
            this.TxtBankBranch.Location = new System.Drawing.Point(153, 120);
            this.TxtBankBranch.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankBranch.Name = "TxtBankBranch";
            this.TxtBankBranch.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankBranch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankBranch.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankBranch.Properties.Appearance.Options.UseFont = true;
            this.TxtBankBranch.Properties.MaxLength = 100;
            this.TxtBankBranch.Size = new System.Drawing.Size(365, 20);
            this.TxtBankBranch.TabIndex = 34;
            this.TxtBankBranch.Validated += new System.EventHandler(this.TxtBankBranch_Validated);
            // 
            // LblDueDt
            // 
            this.LblDueDt.AutoSize = true;
            this.LblDueDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDueDt.ForeColor = System.Drawing.Color.Black;
            this.LblDueDt.Location = new System.Drawing.Point(88, 79);
            this.LblDueDt.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblDueDt.Name = "LblDueDt";
            this.LblDueDt.Size = new System.Drawing.Size(59, 14);
            this.LblDueDt.TabIndex = 29;
            this.LblDueDt.Text = "Due Date";
            this.LblDueDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(68, 123);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 14);
            this.label21.TabIndex = 33;
            this.label21.Text = "Branch Name";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(153, 32);
            this.TxtGiroNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 80;
            this.TxtGiroNo.Size = new System.Drawing.Size(365, 20);
            this.TxtGiroNo.TabIndex = 26;
            this.TxtGiroNo.Validated += new System.EventHandler(this.TxtGiroNo_Validated);
            // 
            // TxtBankCode
            // 
            this.TxtBankCode.EnterMoveNextControl = true;
            this.TxtBankCode.Location = new System.Drawing.Point(153, 98);
            this.TxtBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankCode.Name = "TxtBankCode";
            this.TxtBankCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBankCode.Properties.MaxLength = 100;
            this.TxtBankCode.Size = new System.Drawing.Size(365, 20);
            this.TxtBankCode.TabIndex = 32;
            this.TxtBankCode.Validated += new System.EventHandler(this.TxtBankCode_Validated);
            // 
            // LblGiroNo
            // 
            this.LblGiroNo.AutoSize = true;
            this.LblGiroNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGiroNo.ForeColor = System.Drawing.Color.Black;
            this.LblGiroNo.Location = new System.Drawing.Point(46, 34);
            this.LblGiroNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblGiroNo.Name = "LblGiroNo";
            this.LblGiroNo.Size = new System.Drawing.Size(101, 14);
            this.LblGiroNo.TabIndex = 25;
            this.LblGiroNo.Text = "Bilyet#/Cheque#";
            this.LblGiroNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(69, 101);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 14);
            this.label20.TabIndex = 31;
            this.label20.Text = "Paid To Bank";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblBankCode
            // 
            this.LblBankCode.AutoSize = true;
            this.LblBankCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBankCode.ForeColor = System.Drawing.Color.Black;
            this.LblBankCode.Location = new System.Drawing.Point(79, 12);
            this.LblBankCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBankCode.Name = "LblBankCode";
            this.LblBankCode.Size = new System.Drawing.Size(68, 14);
            this.LblBankCode.TabIndex = 23;
            this.LblBankCode.Text = "Bank Name";
            this.LblBankCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcNo
            // 
            this.TxtBankAcNo.EnterMoveNextControl = true;
            this.TxtBankAcNo.Location = new System.Drawing.Point(153, 142);
            this.TxtBankAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcNo.Name = "TxtBankAcNo";
            this.TxtBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNo.Properties.MaxLength = 100;
            this.TxtBankAcNo.Size = new System.Drawing.Size(365, 20);
            this.TxtBankAcNo.TabIndex = 36;
            this.TxtBankAcNo.Validated += new System.EventHandler(this.TxtBankAcNo_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(85, 145);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 14);
            this.label19.TabIndex = 35;
            this.label19.Text = "Account#";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(153, 10);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 25;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 500;
            this.LueBankCode.Size = new System.Drawing.Size(365, 20);
            this.LueBankCode.TabIndex = 24;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // TpgProject
            // 
            this.TpgProject.Appearance.Header.Options.UseTextOptions = true;
            this.TpgProject.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpgProject.Controls.Add(this.label41);
            this.TpgProject.Controls.Add(this.LueProjectDocNo3);
            this.TpgProject.Controls.Add(this.label42);
            this.TpgProject.Controls.Add(this.LueProjectDocNo2);
            this.TpgProject.Controls.Add(this.label43);
            this.TpgProject.Controls.Add(this.LueProjectDocNo1);
            this.TpgProject.Name = "TpgProject";
            this.TpgProject.Size = new System.Drawing.Size(766, 329);
            this.TpgProject.Text = "Project";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(44, 54);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(75, 14);
            this.label41.TabIndex = 52;
            this.label41.Text = "Project Child";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProjectDocNo3
            // 
            this.LueProjectDocNo3.EnterMoveNextControl = true;
            this.LueProjectDocNo3.Location = new System.Drawing.Point(125, 51);
            this.LueProjectDocNo3.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectDocNo3.Name = "LueProjectDocNo3";
            this.LueProjectDocNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.Appearance.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectDocNo3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectDocNo3.Properties.DropDownRows = 30;
            this.LueProjectDocNo3.Properties.NullText = "[Empty]";
            this.LueProjectDocNo3.Properties.PopupWidth = 300;
            this.LueProjectDocNo3.Size = new System.Drawing.Size(223, 20);
            this.LueProjectDocNo3.TabIndex = 53;
            this.LueProjectDocNo3.ToolTip = "F4 : Show/hide list";
            this.LueProjectDocNo3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectDocNo3.EditValueChanged += new System.EventHandler(this.LueProjectDocNo3_EditValueChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(9, 32);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(110, 14);
            this.label42.TabIndex = 50;
            this.label42.Text = "Project Costcenter";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProjectDocNo2
            // 
            this.LueProjectDocNo2.EnterMoveNextControl = true;
            this.LueProjectDocNo2.Location = new System.Drawing.Point(125, 29);
            this.LueProjectDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectDocNo2.Name = "LueProjectDocNo2";
            this.LueProjectDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.Appearance.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectDocNo2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectDocNo2.Properties.DropDownRows = 30;
            this.LueProjectDocNo2.Properties.NullText = "[Empty]";
            this.LueProjectDocNo2.Properties.PopupWidth = 300;
            this.LueProjectDocNo2.Size = new System.Drawing.Size(223, 20);
            this.LueProjectDocNo2.TabIndex = 51;
            this.LueProjectDocNo2.ToolTip = "F4 : Show/hide list";
            this.LueProjectDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectDocNo2.EditValueChanged += new System.EventHandler(this.LueProjectDocNo2_EditValueChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(73, 10);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(46, 14);
            this.label43.TabIndex = 48;
            this.label43.Text = "Project";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProjectDocNo1
            // 
            this.LueProjectDocNo1.EnterMoveNextControl = true;
            this.LueProjectDocNo1.Location = new System.Drawing.Point(125, 7);
            this.LueProjectDocNo1.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectDocNo1.Name = "LueProjectDocNo1";
            this.LueProjectDocNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.Appearance.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectDocNo1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectDocNo1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectDocNo1.Properties.DropDownRows = 30;
            this.LueProjectDocNo1.Properties.NullText = "[Empty]";
            this.LueProjectDocNo1.Properties.PopupWidth = 300;
            this.LueProjectDocNo1.Size = new System.Drawing.Size(223, 20);
            this.LueProjectDocNo1.TabIndex = 49;
            this.LueProjectDocNo1.ToolTip = "F4 : Show/hide list";
            this.LueProjectDocNo1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectDocNo1.EditValueChanged += new System.EventHandler(this.LueProjectDocNo1_EditValueChanged);
            // 
            // TpCOA
            // 
            this.TpCOA.Appearance.Header.Options.UseTextOptions = true;
            this.TpCOA.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCOA.Controls.Add(this.panel9);
            this.TpCOA.Controls.Add(this.panel8);
            this.TpCOA.Name = "TpCOA";
            this.TpCOA.Size = new System.Drawing.Size(766, 329);
            this.TpCOA.Text = "List of COA\'s Account";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 258);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(766, 71);
            this.panel9.TabIndex = 27;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.TxtDbt);
            this.panel10.Controls.Add(this.TxtCdt);
            this.panel10.Controls.Add(this.TxtBalanced);
            this.panel10.Controls.Add(this.label29);
            this.panel10.Controls.Add(this.label30);
            this.panel10.Controls.Add(this.label31);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(493, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(273, 71);
            this.panel10.TabIndex = 44;
            // 
            // TxtDbt
            // 
            this.TxtDbt.EnterMoveNextControl = true;
            this.TxtDbt.Location = new System.Drawing.Point(83, 5);
            this.TxtDbt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDbt.Name = "TxtDbt";
            this.TxtDbt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDbt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDbt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDbt.Properties.Appearance.Options.UseFont = true;
            this.TxtDbt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDbt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDbt.Properties.ReadOnly = true;
            this.TxtDbt.Size = new System.Drawing.Size(184, 20);
            this.TxtDbt.TabIndex = 45;
            // 
            // TxtCdt
            // 
            this.TxtCdt.EnterMoveNextControl = true;
            this.TxtCdt.Location = new System.Drawing.Point(83, 26);
            this.TxtCdt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCdt.Name = "TxtCdt";
            this.TxtCdt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCdt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCdt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCdt.Properties.Appearance.Options.UseFont = true;
            this.TxtCdt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCdt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCdt.Properties.ReadOnly = true;
            this.TxtCdt.Size = new System.Drawing.Size(184, 20);
            this.TxtCdt.TabIndex = 47;
            // 
            // TxtBalanced
            // 
            this.TxtBalanced.EnterMoveNextControl = true;
            this.TxtBalanced.Location = new System.Drawing.Point(83, 47);
            this.TxtBalanced.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBalanced.Name = "TxtBalanced";
            this.TxtBalanced.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBalanced.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalanced.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBalanced.Properties.Appearance.Options.UseFont = true;
            this.TxtBalanced.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBalanced.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBalanced.Properties.ReadOnly = true;
            this.TxtBalanced.Size = new System.Drawing.Size(184, 20);
            this.TxtBalanced.TabIndex = 49;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(30, 50);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(48, 14);
            this.label29.TabIndex = 48;
            this.label29.Text = "Balance";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(7, 29);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 14);
            this.label30.TabIndex = 46;
            this.label30.Text = "Total Credit";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(10, 8);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(68, 14);
            this.label31.TabIndex = 44;
            this.label31.Text = "Total Debit";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.SkyBlue;
            this.panel8.Controls.Add(this.Grd3);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(766, 329);
            this.panel8.TabIndex = 26;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 329);
            this.Grd3.TabIndex = 25;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpApproval
            // 
            this.TpApproval.Appearance.Header.Options.UseTextOptions = true;
            this.TpApproval.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpApproval.Controls.Add(this.Grd2);
            this.TpApproval.Controls.Add(this.panel3);
            this.TpApproval.Name = "TpApproval";
            this.TpApproval.Size = new System.Drawing.Size(766, 329);
            this.TpApproval.Text = "Approval Information";
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 36);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 293);
            this.Grd2.TabIndex = 25;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(766, 36);
            this.panel3.TabIndex = 22;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(54, 8);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(106, 20);
            this.TxtStatus.TabIndex = 24;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(8, 11);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 14);
            this.label27.TabIndex = 23;
            this.label27.Text = "Status";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpBudget
            // 
            this.TpBudget.Appearance.Header.Options.UseTextOptions = true;
            this.TpBudget.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpBudget.Controls.Add(this.BtnBCCode);
            this.TpBudget.Controls.Add(this.LueDeptCode2);
            this.TpBudget.Controls.Add(this.LblDeptCode2);
            this.TpBudget.Controls.Add(this.LueBIOPProject);
            this.TpBudget.Controls.Add(this.label39);
            this.TpBudget.Controls.Add(this.BtnCOA2);
            this.TpBudget.Controls.Add(this.BtnItCode2);
            this.TpBudget.Controls.Add(this.BtnSOCDocNo2);
            this.TpBudget.Controls.Add(this.BtnSOCDocNo);
            this.TpBudget.Controls.Add(this.BtnItCode);
            this.TpBudget.Controls.Add(this.BtnCOA);
            this.TpBudget.Controls.Add(this.TxtAcDesc);
            this.TpBudget.Controls.Add(this.TxtItName);
            this.TpBudget.Controls.Add(this.TxtAcNo);
            this.TpBudget.Controls.Add(this.TxtItCode);
            this.TpBudget.Controls.Add(this.TxtSOCDocNo);
            this.TpBudget.Controls.Add(this.label38);
            this.TpBudget.Controls.Add(this.label37);
            this.TpBudget.Controls.Add(this.label36);
            this.TpBudget.Controls.Add(this.LblSiteCode);
            this.TpBudget.Controls.Add(this.LueSiteCode);
            this.TpBudget.Controls.Add(this.LueReqType);
            this.TpBudget.Controls.Add(this.label33);
            this.TpBudget.Controls.Add(this.label34);
            this.TpBudget.Controls.Add(this.TxtRemainingBudget);
            this.TpBudget.Controls.Add(this.label35);
            this.TpBudget.Controls.Add(this.LueBCCode);
            this.TpBudget.Name = "TpBudget";
            this.TpBudget.Size = new System.Drawing.Size(766, 329);
            this.TpBudget.Text = "Budget";
            // 
            // BtnBCCode
            // 
            this.BtnBCCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBCCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBCCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBCCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBCCode.Appearance.Options.UseBackColor = true;
            this.BtnBCCode.Appearance.Options.UseFont = true;
            this.BtnBCCode.Appearance.Options.UseForeColor = true;
            this.BtnBCCode.Appearance.Options.UseTextOptions = true;
            this.BtnBCCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBCCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBCCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnBCCode.Image")));
            this.BtnBCCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBCCode.Location = new System.Drawing.Point(456, 96);
            this.BtnBCCode.Name = "BtnBCCode";
            this.BtnBCCode.Size = new System.Drawing.Size(24, 21);
            this.BtnBCCode.TabIndex = 48;
            this.BtnBCCode.ToolTip = "Show Budget Category List";
            this.BtnBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBCCode.ToolTipTitle = "Run System";
            this.BtnBCCode.Click += new System.EventHandler(this.BtnBCCode_Click);
            // 
            // LueDeptCode2
            // 
            this.LueDeptCode2.EnterMoveNextControl = true;
            this.LueDeptCode2.Location = new System.Drawing.Point(141, 76);
            this.LueDeptCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode2.Name = "LueDeptCode2";
            this.LueDeptCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode2.Properties.DropDownRows = 25;
            this.LueDeptCode2.Properties.NullText = "[Empty]";
            this.LueDeptCode2.Properties.PopupWidth = 500;
            this.LueDeptCode2.Size = new System.Drawing.Size(311, 20);
            this.LueDeptCode2.TabIndex = 45;
            this.LueDeptCode2.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode2.EditValueChanged += new System.EventHandler(this.LueDeptCode2_EditValueChanged);
            // 
            // LblDeptCode2
            // 
            this.LblDeptCode2.AutoSize = true;
            this.LblDeptCode2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeptCode2.ForeColor = System.Drawing.Color.Red;
            this.LblDeptCode2.Location = new System.Drawing.Point(2, 79);
            this.LblDeptCode2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeptCode2.Name = "LblDeptCode2";
            this.LblDeptCode2.Size = new System.Drawing.Size(138, 14);
            this.LblDeptCode2.TabIndex = 44;
            this.LblDeptCode2.Text = "Department For Budget";
            this.LblDeptCode2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBIOPProject
            // 
            this.LueBIOPProject.EnterMoveNextControl = true;
            this.LueBIOPProject.Location = new System.Drawing.Point(141, 11);
            this.LueBIOPProject.Margin = new System.Windows.Forms.Padding(5);
            this.LueBIOPProject.Name = "LueBIOPProject";
            this.LueBIOPProject.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBIOPProject.Properties.Appearance.Options.UseFont = true;
            this.LueBIOPProject.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBIOPProject.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBIOPProject.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBIOPProject.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBIOPProject.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBIOPProject.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBIOPProject.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBIOPProject.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBIOPProject.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBIOPProject.Properties.DropDownRows = 30;
            this.LueBIOPProject.Properties.NullText = "[Empty]";
            this.LueBIOPProject.Properties.PopupWidth = 300;
            this.LueBIOPProject.Size = new System.Drawing.Size(311, 20);
            this.LueBIOPProject.TabIndex = 39;
            this.LueBIOPProject.ToolTip = "F4 : Show/hide list";
            this.LueBIOPProject.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBIOPProject.EditValueChanged += new System.EventHandler(this.LueBIOPProject_EditValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(62, 14);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(78, 14);
            this.label39.TabIndex = 38;
            this.label39.Text = "BIOP/Project";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCOA2
            // 
            this.BtnCOA2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCOA2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCOA2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCOA2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCOA2.Appearance.Options.UseBackColor = true;
            this.BtnCOA2.Appearance.Options.UseFont = true;
            this.BtnCOA2.Appearance.Options.UseForeColor = true;
            this.BtnCOA2.Appearance.Options.UseTextOptions = true;
            this.BtnCOA2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCOA2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCOA2.Image = ((System.Drawing.Image)(resources.GetObject("BtnCOA2.Image")));
            this.BtnCOA2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCOA2.Location = new System.Drawing.Point(477, 185);
            this.BtnCOA2.Name = "BtnCOA2";
            this.BtnCOA2.Size = new System.Drawing.Size(20, 21);
            this.BtnCOA2.TabIndex = 64;
            this.BtnCOA2.ToolTip = "Show BOQ";
            this.BtnCOA2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCOA2.ToolTipTitle = "Run System";
            this.BtnCOA2.Click += new System.EventHandler(this.BtnCOA2_Click);
            // 
            // BtnItCode2
            // 
            this.BtnItCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItCode2.Appearance.Options.UseBackColor = true;
            this.BtnItCode2.Appearance.Options.UseFont = true;
            this.BtnItCode2.Appearance.Options.UseForeColor = true;
            this.BtnItCode2.Appearance.Options.UseTextOptions = true;
            this.BtnItCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnItCode2.Image")));
            this.BtnItCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItCode2.Location = new System.Drawing.Point(477, 163);
            this.BtnItCode2.Name = "BtnItCode2";
            this.BtnItCode2.Size = new System.Drawing.Size(20, 21);
            this.BtnItCode2.TabIndex = 59;
            this.BtnItCode2.ToolTip = "Show BOQ";
            this.BtnItCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItCode2.ToolTipTitle = "Run System";
            this.BtnItCode2.Click += new System.EventHandler(this.BtnItCode2_Click);
            // 
            // BtnSOCDocNo2
            // 
            this.BtnSOCDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOCDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOCDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOCDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOCDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOCDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOCDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOCDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOCDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOCDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOCDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOCDocNo2.Image")));
            this.BtnSOCDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOCDocNo2.Location = new System.Drawing.Point(477, 141);
            this.BtnSOCDocNo2.Name = "BtnSOCDocNo2";
            this.BtnSOCDocNo2.Size = new System.Drawing.Size(20, 21);
            this.BtnSOCDocNo2.TabIndex = 54;
            this.BtnSOCDocNo2.ToolTip = "Show BOQ";
            this.BtnSOCDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOCDocNo2.ToolTipTitle = "Run System";
            this.BtnSOCDocNo2.Click += new System.EventHandler(this.BtnSOCDocNo2_Click);
            // 
            // BtnSOCDocNo
            // 
            this.BtnSOCDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOCDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOCDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOCDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOCDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOCDocNo.Appearance.Options.UseFont = true;
            this.BtnSOCDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOCDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOCDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOCDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOCDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOCDocNo.Image")));
            this.BtnSOCDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOCDocNo.Location = new System.Drawing.Point(456, 141);
            this.BtnSOCDocNo.Name = "BtnSOCDocNo";
            this.BtnSOCDocNo.Size = new System.Drawing.Size(20, 21);
            this.BtnSOCDocNo.TabIndex = 53;
            this.BtnSOCDocNo.ToolTip = "Show BOQ";
            this.BtnSOCDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOCDocNo.ToolTipTitle = "Run System";
            this.BtnSOCDocNo.Click += new System.EventHandler(this.BtnSOCDocNo_Click);
            // 
            // BtnItCode
            // 
            this.BtnItCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItCode.Appearance.Options.UseBackColor = true;
            this.BtnItCode.Appearance.Options.UseFont = true;
            this.BtnItCode.Appearance.Options.UseForeColor = true;
            this.BtnItCode.Appearance.Options.UseTextOptions = true;
            this.BtnItCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnItCode.Image")));
            this.BtnItCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItCode.Location = new System.Drawing.Point(456, 163);
            this.BtnItCode.Name = "BtnItCode";
            this.BtnItCode.Size = new System.Drawing.Size(20, 21);
            this.BtnItCode.TabIndex = 58;
            this.BtnItCode.ToolTip = "Show BOQ";
            this.BtnItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItCode.ToolTipTitle = "Run System";
            this.BtnItCode.Click += new System.EventHandler(this.BtnItCode_Click);
            // 
            // BtnCOA
            // 
            this.BtnCOA.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCOA.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCOA.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCOA.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCOA.Appearance.Options.UseBackColor = true;
            this.BtnCOA.Appearance.Options.UseFont = true;
            this.BtnCOA.Appearance.Options.UseForeColor = true;
            this.BtnCOA.Appearance.Options.UseTextOptions = true;
            this.BtnCOA.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCOA.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCOA.Image = ((System.Drawing.Image)(resources.GetObject("BtnCOA.Image")));
            this.BtnCOA.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCOA.Location = new System.Drawing.Point(456, 185);
            this.BtnCOA.Name = "BtnCOA";
            this.BtnCOA.Size = new System.Drawing.Size(20, 21);
            this.BtnCOA.TabIndex = 63;
            this.BtnCOA.ToolTip = "Show BOQ";
            this.BtnCOA.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCOA.ToolTipTitle = "Run System";
            this.BtnCOA.Click += new System.EventHandler(this.BtnCOA_Click);
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(249, 185);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 30;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(203, 20);
            this.TxtAcDesc.TabIndex = 62;
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(249, 163);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 30;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(203, 20);
            this.TxtItName.TabIndex = 57;
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(141, 185);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 30;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(106, 20);
            this.TxtAcNo.TabIndex = 61;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(141, 163);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 30;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(106, 20);
            this.TxtItCode.TabIndex = 56;
            // 
            // TxtSOCDocNo
            // 
            this.TxtSOCDocNo.EnterMoveNextControl = true;
            this.TxtSOCDocNo.Location = new System.Drawing.Point(141, 141);
            this.TxtSOCDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOCDocNo.Name = "TxtSOCDocNo";
            this.TxtSOCDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOCDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOCDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOCDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOCDocNo.Properties.MaxLength = 30;
            this.TxtSOCDocNo.Properties.ReadOnly = true;
            this.TxtSOCDocNo.Size = new System.Drawing.Size(311, 20);
            this.TxtSOCDocNo.TabIndex = 52;
            this.TxtSOCDocNo.Validated += new System.EventHandler(this.TxtSOCDocNo_Validated);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(100, 188);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(40, 14);
            this.label38.TabIndex = 60;
            this.label38.Text = "COA#";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(71, 166);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(69, 14);
            this.label37.TabIndex = 55;
            this.label37.Text = "Finish Good";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(57, 144);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(83, 14);
            this.label36.TabIndex = 51;
            this.label36.Text = "SO Contract#";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(112, 58);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 42;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(141, 55);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(311, 20);
            this.LueSiteCode.TabIndex = 43;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LueReqType
            // 
            this.LueReqType.EnterMoveNextControl = true;
            this.LueReqType.Location = new System.Drawing.Point(141, 33);
            this.LueReqType.Margin = new System.Windows.Forms.Padding(5);
            this.LueReqType.Name = "LueReqType";
            this.LueReqType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.Appearance.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReqType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReqType.Properties.DropDownRows = 30;
            this.LueReqType.Properties.NullText = "[Empty]";
            this.LueReqType.Properties.PopupWidth = 300;
            this.LueReqType.Size = new System.Drawing.Size(311, 20);
            this.LueReqType.TabIndex = 41;
            this.LueReqType.ToolTip = "F4 : Show/hide list";
            this.LueReqType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReqType.EditValueChanged += new System.EventHandler(this.LueReqType_EditValueChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(56, 36);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(84, 14);
            this.label33.TabIndex = 40;
            this.label33.Text = "Request Type";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(43, 122);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(97, 14);
            this.label34.TabIndex = 49;
            this.label34.Text = "Available Budget";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRemainingBudget
            // 
            this.TxtRemainingBudget.EnterMoveNextControl = true;
            this.TxtRemainingBudget.Location = new System.Drawing.Point(141, 119);
            this.TxtRemainingBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemainingBudget.Name = "TxtRemainingBudget";
            this.TxtRemainingBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemainingBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemainingBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemainingBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemainingBudget.Properties.ReadOnly = true;
            this.TxtRemainingBudget.Size = new System.Drawing.Size(229, 20);
            this.TxtRemainingBudget.TabIndex = 50;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(40, 100);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 14);
            this.label35.TabIndex = 46;
            this.label35.Text = "Budget Category";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(141, 97);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 300;
            this.LueBCCode.Size = new System.Drawing.Size(311, 20);
            this.LueBCCode.TabIndex = 47;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // TpUpload
            // 
            this.TpUpload.Appearance.Header.Options.UseTextOptions = true;
            this.TpUpload.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpUpload.Controls.Add(this.BtnFile);
            this.TpUpload.Controls.Add(this.label40);
            this.TpUpload.Controls.Add(this.PbUpload3);
            this.TpUpload.Controls.Add(this.PbUpload2);
            this.TpUpload.Controls.Add(this.ChkFile3);
            this.TpUpload.Controls.Add(this.BtnDownload3);
            this.TpUpload.Controls.Add(this.TxtFile3);
            this.TpUpload.Controls.Add(this.BtnFile3);
            this.TpUpload.Controls.Add(this.ChkFile2);
            this.TpUpload.Controls.Add(this.BtnDownload2);
            this.TpUpload.Controls.Add(this.TxtFile2);
            this.TpUpload.Controls.Add(this.label44);
            this.TpUpload.Controls.Add(this.BtnFile2);
            this.TpUpload.Controls.Add(this.ChkFile);
            this.TpUpload.Controls.Add(this.BtnDownload);
            this.TpUpload.Controls.Add(this.PbUpload);
            this.TpUpload.Controls.Add(this.TxtFile);
            this.TpUpload.Controls.Add(this.label45);
            this.TpUpload.Name = "TpUpload";
            this.TpUpload.Size = new System.Drawing.Size(766, 329);
            this.TpUpload.Text = "Upload File";
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(494, 5);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 89;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(7, 92);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 14);
            this.label40.TabIndex = 98;
            this.label40.Text = "File 3";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(49, 110);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(500, 17);
            this.PbUpload3.TabIndex = 103;
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(49, 69);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(500, 17);
            this.PbUpload2.TabIndex = 97;
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(466, 88);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 100;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(525, 88);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 102;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(49, 88);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(410, 20);
            this.TxtFile3.TabIndex = 99;
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile3.Image")));
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(494, 88);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 101;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(466, 46);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 94;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(525, 46);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 96;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(49, 46);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(410, 20);
            this.TxtFile2.TabIndex = 93;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(7, 50);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 14);
            this.label44.TabIndex = 92;
            this.label44.Text = "File 2";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(494, 46);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 95;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(466, 5);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 88;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(525, 5);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 90;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(49, 27);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(500, 17);
            this.PbUpload.TabIndex = 91;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(49, 5);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(410, 20);
            this.TxtFile.TabIndex = 87;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(8, 9);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(35, 14);
            this.label45.TabIndex = 86;
            this.label45.Text = "File 1";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpUpload2
            // 
            this.TpUpload2.Appearance.Header.Options.UseTextOptions = true;
            this.TpUpload2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpUpload2.Controls.Add(this.LueVRCtCode);
            this.TpUpload2.Controls.Add(this.Grd4);
            this.TpUpload2.Name = "TpUpload2";
            this.TpUpload2.Size = new System.Drawing.Size(766, 329);
            this.TpUpload2.Text = "Upload File";
            // 
            // LueVRCtCode
            // 
            this.LueVRCtCode.EnterMoveNextControl = true;
            this.LueVRCtCode.Location = new System.Drawing.Point(70, 30);
            this.LueVRCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVRCtCode.Name = "LueVRCtCode";
            this.LueVRCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueVRCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVRCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVRCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVRCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVRCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVRCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVRCtCode.Properties.DropDownRows = 12;
            this.LueVRCtCode.Properties.NullText = "[Empty]";
            this.LueVRCtCode.Properties.PopupWidth = 500;
            this.LueVRCtCode.Size = new System.Drawing.Size(171, 20);
            this.LueVRCtCode.TabIndex = 67;
            this.LueVRCtCode.ToolTip = "F4 : Show/hide list";
            this.LueVRCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVRCtCode.EditValueChanged += new System.EventHandler(this.LueVRCtCode_EditValueChanged);
            this.LueVRCtCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueVRCtCode_KeyDown);
            this.LueVRCtCode.Leave += new System.EventHandler(this.LueVRCtCode_Leave);
            this.LueVRCtCode.Validated += new System.EventHandler(this.LueVRCtCode_Validated);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 329);
            this.Grd4.TabIndex = 104;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            this.Grd4.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd4_RequestCellToolTipText);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpUpload3
            // 
            this.TpUpload3.Controls.Add(this.Grd5);
            this.TpUpload3.Name = "TpUpload3";
            this.TpUpload3.Size = new System.Drawing.Size(766, 329);
            this.TpUpload3.Text = "Upload File";
            // 
            // Grd5
            // 
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.UseXPStyles = false;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 329);
            this.Grd5.TabIndex = 26;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd5_EllipsisButtonClick);
            this.Grd5.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd5_RequestCellToolTipText);
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 425);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(1285, 90);
            this.Grd1.TabIndex = 63;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // BtnCSV
            // 
            this.BtnCSV.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCSV.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCSV.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCSV.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCSV.Appearance.Options.UseBackColor = true;
            this.BtnCSV.Appearance.Options.UseFont = true;
            this.BtnCSV.Appearance.Options.UseForeColor = true;
            this.BtnCSV.Appearance.Options.UseTextOptions = true;
            this.BtnCSV.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCSV.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCSV.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnCSV.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnCSV.Location = new System.Drawing.Point(0, 0);
            this.BtnCSV.Name = "BtnCSV";
            this.BtnCSV.Size = new System.Drawing.Size(70, 29);
            this.BtnCSV.TabIndex = 14;
            this.BtnCSV.Text = "  Cs&v";
            this.BtnCSV.ToolTip = "Print Document";
            this.BtnCSV.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCSV.ToolTipTitle = "Run System";
            this.BtnCSV.Click += new System.EventHandler(this.BtnCSV_Click);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 493);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 15;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // ChkAutoWidth
            // 
            this.ChkAutoWidth.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkAutoWidth.Location = new System.Drawing.Point(0, 471);
            this.ChkAutoWidth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkAutoWidth.Name = "ChkAutoWidth";
            this.ChkAutoWidth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAutoWidth.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkAutoWidth.Properties.Appearance.Options.UseFont = true;
            this.ChkAutoWidth.Properties.Appearance.Options.UseForeColor = true;
            this.ChkAutoWidth.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAutoWidth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkAutoWidth.Properties.Caption = "Resize";
            this.ChkAutoWidth.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAutoWidth.Size = new System.Drawing.Size(70, 22);
            this.ChkAutoWidth.TabIndex = 16;
            this.ChkAutoWidth.ToolTip = "Set Automatic Report\'s Column Width";
            this.ChkAutoWidth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAutoWidth.ToolTipTitle = "Run System";
            this.ChkAutoWidth.CheckedChanged += new System.EventHandler(this.ChkAutoWidth_CheckedChanged);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 451);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 17;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // FrmVoucherRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1355, 515);
            this.Name = "FrmVoucherRequest";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcVoucherRequest)).EndInit();
            this.TcVoucherRequest.ResumeLayout(false);
            this.TpGeneral.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcTp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcTp2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBillingID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueManualCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtManualExcRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRefundDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRefundDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaymentUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExcRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            this.TpAdditional.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashAdvanceTypeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOpeningDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteOpeningDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocEnclosure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            this.TpgProject.ResumeLayout(false);
            this.TpgProject.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectDocNo1.Properties)).EndInit();
            this.TpCOA.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDbt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCdt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalanced.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            this.TpBudget.ResumeLayout(false);
            this.TpBudget.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBIOPProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            this.TpUpload.ResumeLayout(false);
            this.TpUpload.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            this.TpUpload2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueVRCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpUpload3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label26;
        private DevExpress.XtraTab.XtraTabControl TcVoucherRequest;
        private DevExpress.XtraTab.XtraTabPage TpGeneral;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtPaymentUser;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        private DevExpress.XtraEditors.LookUpEdit LueAcType;
        private System.Windows.Forms.Label LblBankAcCode;
        private System.Windows.Forms.Label LblPaymentUser;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LuePIC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueAcType2;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode2;
        internal DevExpress.XtraEditors.TextEdit TxtExcRate;
        private System.Windows.Forms.Label LblBankAcCode2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode2;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.LookUpEdit LueDocType;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraTab.XtraTabPage TpAdditional;
        private System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcName;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        internal DevExpress.XtraEditors.TextEdit TxtBankBranch;
        private System.Windows.Forms.Label LblDueDt;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtGiroNo;
        internal DevExpress.XtraEditors.TextEdit TxtBankCode;
        private System.Windows.Forms.Label LblGiroNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label LblBankCode;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNo;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private DevExpress.XtraTab.XtraTabPage TpApproval;
        protected TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtDocEnclosure;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.DateEdit DteOpeningDt;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label LblEntCode;
        internal DevExpress.XtraEditors.LookUpEdit LueEntCode;
        private System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcTp2;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcTp;
        private System.Windows.Forms.Label label32;
        private DevExpress.XtraTab.XtraTabPage TpgProject;
        private System.Windows.Forms.Label label41;
        private DevExpress.XtraEditors.LookUpEdit LueProjectDocNo3;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.LookUpEdit LueProjectDocNo2;
        private System.Windows.Forms.Label label43;
        private DevExpress.XtraEditors.LookUpEdit LueProjectDocNo1;
        private DevExpress.XtraTab.XtraTabPage TpCOA;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtDbt;
        internal DevExpress.XtraEditors.TextEdit TxtCdt;
        internal DevExpress.XtraEditors.TextEdit TxtBalanced;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        public DevExpress.XtraEditors.SimpleButton BtnDocType;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.DateEdit DteRefundDt;
        private DevExpress.XtraTab.XtraTabPage TpBudget;
        internal DevExpress.XtraEditors.LookUpEdit LueReqType;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtRemainingBudget;
        private System.Windows.Forms.Label label35;
        public DevExpress.XtraEditors.LookUpEdit LueBCCode;
        private System.Windows.Forms.Label LblSiteCode;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueManualCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtManualExcRate;
        private System.Windows.Forms.Label LblManualCurCode;
        private System.Windows.Forms.Label LblManualExcRate;
        internal DevExpress.XtraEditors.TextEdit TxtAmt2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        internal DevExpress.XtraEditors.TextEdit TxtSOCDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnItCode;
        public DevExpress.XtraEditors.SimpleButton BtnCOA;
        public DevExpress.XtraEditors.SimpleButton BtnSOCDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnCOA2;
        public DevExpress.XtraEditors.SimpleButton BtnItCode2;
        public DevExpress.XtraEditors.SimpleButton BtnSOCDocNo2;
        internal DevExpress.XtraEditors.LookUpEdit LueBIOPProject;
        private System.Windows.Forms.Label label39;
        public DevExpress.XtraEditors.SimpleButton BtnCSV;
        private System.Windows.Forms.Label LblSOContractDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtBillingID;
        private System.Windows.Forms.Label LblBillingID;
        private DevExpress.XtraTab.XtraTabPage TpUpload;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ProgressBar PbUpload3;
        private System.Windows.Forms.ProgressBar PbUpload2;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label44;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.ProgressBar PbUpload;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private DevExpress.XtraTab.XtraTabPage TpUpload2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraEditors.LookUpEdit LueVRCtCode;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode2;
        private System.Windows.Forms.Label LblDeptCode2;
        public DevExpress.XtraEditors.SimpleButton BtnBCCode;
        public DevExpress.XtraEditors.SimpleButton BtnCopy;
        private System.Windows.Forms.Label LblCopyGeneral;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private DevExpress.XtraEditors.CheckEdit ChkAutoWidth;
        internal DevExpress.XtraEditors.LookUpEdit LueFontSize;
        private DevExpress.XtraTab.XtraTabPage TpUpload3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.Label LblCashAdvanceTypeCode;
        private DevExpress.XtraEditors.LookUpEdit LueCashAdvanceTypeCode;
        private System.Windows.Forms.Panel panel11;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode2;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        private System.Windows.Forms.Label LblEmpName;
        internal DevExpress.XtraEditors.TextEdit TxtEmpName;
    }
}