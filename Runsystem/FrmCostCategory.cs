﻿#region Update
/*
    12/02/2018 [TKG] cost category group tidak mandatory
    12/02/2018 [TKG] account# berdasarkan otorisasi group thd site/entity
    07/06/2018 [HAR] Cost Category Account COA yang sdh dipake tdk bisa dipilih lagi brdasarkan parameter
    19/01/2020 [TKG/IMS] tambah alias nomor rekening coa
    14/04/2020 [TKG/IMS] tambah Cost Of Group
    08/10/2020 [ICA/PHT] Menambahkan checkbox JoinCost berdasarkan parameter IsCostCategoryUseJoinCost
    06/11/2020 [TKG/PHT] tambah cost category product
    18/11/2020 [VIN/PHT] tambah checkbox revenue (IsCostCategoryUseRevenue)
    17/06/2021 [TKG/PHT] Berdasarkan parameter IsCostCategoryUseProfitCenter, cost center difilter berdasarkan profit center dan cost center yg muncul adalah cost center yg tidak menjadi parent di cost center yg lain
 *  02/09/2021 [ICA/AMKA] membuat bisa dibuka di aplikasi lain
 *  28/12/2022 [VIN/VIR] saat edit tidak bisa edit cc 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCostCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mCCtCode = string.Empty; //if this application is called from other application
        internal FrmCostCategoryFind FrmFind;
        internal bool 
            mIsCostCategoryFilterByEntity = false, 
            mIsOneAccountForOneCostCategory = false, 
            mIsCOAUseAlias = false,
            mIsCostCategoryUseJoinCost=false,
            mIsCostCategoryUseRevenue=false,
            mIsCostCategoryUseProfitCenter = false;

        #endregion

        #region Constructor

        public FrmCostCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Cost Category";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);
                if (!mIsCostCategoryUseProfitCenter) Sl.SetLueCCCode(ref LueCC);
                Sl.SetLueOption(ref LueCCGrpCode, "CostCenterGroup");
                Sl.SetLueOption(ref LueCostOfGroupCode, "CostOfGroup");
                Sl.SetLueOption(ref LueCCtProductCode, "CCtProduct");
                ChkJoinCostInd.Visible = mIsCostCategoryUseJoinCost;
                ChkRevenueInd.Visible = mIsCostCategoryUseRevenue;

                //if this application is called from other application
                if (mCCtCode.Length != 0)
                {
                    ShowData(mCCtCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsCostCategoryFilterByEntity = Sm.GetParameterBoo("IsCostCategoryFilterByEntity");
            mIsOneAccountForOneCostCategory = Sm.GetParameterBoo("IsOneAccountForOneCostCategory");
            mIsCOAUseAlias = Sm.GetParameterBoo("IsCOAUseAlias");
            mIsCostCategoryUseJoinCost = Sm.GetParameterBoo("IsCostCategoryUseJoinCost");
            mIsCostCategoryUseRevenue = Sm.GetParameterBoo("IsCostCategoryUseRevenue");
            mIsCostCategoryUseProfitCenter = Sm.GetParameterBoo("IsCostCategoryUseProfitCenter");
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCCtCode, TxtCCtName, TxtAcNo, TxtAcDesc, LueCC, 
                        LueCCGrpCode, LueCostOfGroupCode, ChkJoinCostInd, LueCCtProductCode, ChkRevenueInd
                    }, true);
                    TxtCCtCode.Focus();
                    BtnAcNo.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCCtName, LueCC, LueCCGrpCode, LueCostOfGroupCode, ChkJoinCostInd, 
                        LueCCtProductCode, ChkRevenueInd
                    }, false);
                    BtnAcNo.Enabled = true;
                    TxtCCtName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCCtName, LueCCGrpCode, LueCostOfGroupCode, ChkJoinCostInd, 
                        LueCCtProductCode, ChkRevenueInd
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtCCtCode }, true);
                    BtnAcNo.Enabled = true;
                    TxtCCtName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { 
                TxtCCtCode, TxtCCtName, TxtAcNo, TxtAcDesc, LueCC, 
                LueCCGrpCode, LueCostOfGroupCode, LueCCtProductCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCostCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                if (mIsCostCategoryUseProfitCenter) 
                    Sl.SetLueCCCodeFilterByProfitCenter(ref LueCC, string.Empty, "Y");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCCtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblCostCategory Where CCtCode=@CCtCode;" };
                Sm.CmParam<String>(ref cm, "@CCtCode", TxtCCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            string DocNo = string.Empty;
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string CCCodeOld = Sm.GetValue("Select CCCode From TblCostCategory Where CCtCode = '"+TxtCCtCode.Text+"' ");

                if (TxtCCtCode.Text.Length == 0)
                {
                    DocNo = GenerateCCtCode();
                }
                else
                {
                    if (CCCodeOld == Sm.GetLue(LueCC))
                    {
                        DocNo = TxtCCtCode.Text;
                    }
                    else
                    {
                        DocNo = GenerateCCtCode();
                    }
                }

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblCostCategory(CCtCode, CCtName, CCCode, AcNo, CCGrpCode, CostOfGroupCode, CCtProductCode, JoinCostInd, RevenueInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@CCtCode, @CCtName, @CCCode, @AcNo, @CCGrpCode, @CostOfGroupCode, @CCtProductCode, @JoinCostInd, @RevenueInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update CCtName=@CCtName, CCCode=@CCCode, AcNo=@AcNo, CCGrpCode=@CCGrpCode, CostOfGroupCode=@CostOfGroupCode, CCtProductCode=@CCtProductCode, ");
                SQL.AppendLine("   JoinCostInd=@JoinCostInd, RevenueInd=@RevenueInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@CCtCode", DocNo);
                Sm.CmParam<String>(ref cm, "@CCtName", TxtCCtName.Text);
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCC));
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@CCGrpCode", Sm.GetLue(LueCCGrpCode));
                Sm.CmParam<String>(ref cm, "@CostOfGroupCode", Sm.GetLue(LueCostOfGroupCode));
                Sm.CmParam<String>(ref cm, "@CCtProductCode", Sm.GetLue(LueCCtProductCode));
                Sm.CmParam<String>(ref cm, "@JoinCostInd", ChkJoinCostInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@RevenueInd", ChkRevenueInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.CCtCode, A.CCtName, A.CCCode, A.AcNo, A.CCGrpCode, A.CostOfGroupCode, A.CCtProductCode, A.JoinCostInd, A.RevenueInd, ");
                if (mIsCOAUseAlias)
                    SQL.AppendLine("Concat(B.AcDesc, Case When B.Alias Is Null Then '' Else Concat(' [', B.Alias, ']') End) As AcDesc ");
                else
                    SQL.AppendLine("B.AcDesc ");
                SQL.AppendLine("From TblCostCategory A ");
                SQL.AppendLine("Left Join TblCoa B On A.AcNo = B.AcNo ");
                SQL.AppendLine("Where A.CCtCode=@CCtCode;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CCtCode", CCtCode);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            "CCtCode", 
                            "CCtName", "CCCode", "AcNo", "CCGrpCode", "CostOfGroupCode",
                            "CCtProductCode", "JoinCostInd", "AcDesc" ,"RevenueInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCCtName.EditValue = Sm.DrStr(dr, c[1]);
                            if (mIsCostCategoryUseProfitCenter)
                                Sl.SetLueCCCodeFilterByProfitCenter(ref LueCC, Sm.DrStr(dr, c[2]), "Y");
                            else
                                Sm.SetLue(LueCC, Sm.DrStr(dr, c[2]));
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[3]);
                            Sm.SetLue(LueCCGrpCode, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueCostOfGroupCode, Sm.DrStr(dr, c[5]));
                            Sm.SetLue(LueCCtProductCode, Sm.DrStr(dr, c[6]));
                            ChkJoinCostInd.Checked = Sm.DrStr(dr, c[7]) == "Y";
                            TxtAcDesc.EditValue = Sm.DrStr(dr, c[8]);
                            ChkRevenueInd.Checked = Sm.DrStr(dr, c[9]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCCtName, "Cost category name", false) ||
                Sm.IsLueEmpty(LueCC, "Cost center") ||
                Sm.IsTxtEmpty(TxtAcNo, "Account#", false) ||
                IsCCtCodeExisted()||
                IsJoinCostIndAndRevenueIndTick()||
                IsAccountCCtCodeExisted();
        }

        private bool IsCCtCodeExisted()
        {
            if (!TxtCCtCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select CCtCode From TblCostCategory Where CCtCode='" + TxtCCtCode.Text + "' And CCCode = '"+Sm.GetLue(LueCC)+"' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Cost category code ( " + TxtCCtCode.Text + " ) for Cost Center ("+Sm.GetLue(LueCC)+") already existed.");
                return true;
            }
            return false;
        }
        private bool IsJoinCostIndAndRevenueIndTick()
        {
            if (ChkRevenueInd.Checked && ChkJoinCostInd.Checked )
            {
                Sm.StdMsg(mMsgType.Warning, "Choose only one, between Join Cost or Revenue.");
                return true;
            }
            return false;
        }

        private bool IsAccountCCtCodeExisted()
        {
            if (Sm.GetParameterBoo("IsOneAccountForOneCostCategory") == true)
            {
                if(Sm.IsDataExist("Select AcNo From TblCostCategory Where AcNo = '"+TxtAcNo.Text+"' " ))
                {
                    Sm.StdMsg(mMsgType.Warning, "Account number already use for other cost category.");
                    return true;
                }
            }
            return false;
        }

        private string GenerateCCtCode()
        {
            return Sm.GetValue(
                "Select Right(Concat('00000', CCtCodeMax) , 5) As CCtCodeTemp "+
                "From (Select IfNull(Max(Cast(Trim(CCtCode) As Decimal)), 0)+1 CCtCodeMax From TblCostCategory) T;"
                );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmCostCategoryDlg(this));
        }

        private void LueCC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {                
                if (mIsCostCategoryUseProfitCenter)
                    Sm.RefreshLookUpEdit(LueCC, new Sm.RefreshLue3(Sl.SetLueCCCodeFilterByProfitCenter), string.Empty, "Y");
                else
                    Sm.RefreshLookUpEdit(LueCC, new Sm.RefreshLue1(Sl.SetLueCCCode));  
            }
        }

        private void LueCCGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCCGrpCode, new Sm.RefreshLue2(Sl.SetLueOption), "CostCenterGroup");
        }

        private void LueCostOfGroupCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCostOfGroupCode, new Sm.RefreshLue2(Sl.SetLueOption), "CostOfGroupCode");
        }

        private void LueCCtProductCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCCtProductCode, new Sm.RefreshLue2(Sl.SetLueOption), "CCtProduct");
        }

        #endregion

        #endregion
    }
}
