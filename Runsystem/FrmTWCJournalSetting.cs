﻿#region Update
/*
    03/02/2020 [HAR/TWC] POS TWC ticketing
 *  19/02/2020 [HAR/TWC] BUG ID no
 *  21/11/2020 [HAR/TWC] tambah informasi bankaccount
 *  18/10/2021 [HAR/TWC] tambah informasi costcenter dan active indicator
 *  09/02/2023 [HAR/TWC] tambah validasi untuk terusan akan mabil dari itcode dahulu baru ke groupname
    */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmTWCJournalSetting : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmTWCJournalSettingFind FrmFind;
        internal string IDNo = string.Empty;

        #endregion

        #region Constructor

        public FrmTWCJournalSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                SetLueCCCode(ref LueCCCode, string.Empty);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProcessCode, TxtProcessName, TxtIDNo, LueBankAcCode, 
                        TxtVar1, TxtVar2, TxtAcNo, TxtAcType, TxtAcDesc, LueCCCode, ChkActInd
                    }, true);
                    TxtProcessCode.Focus();
                    BtnAcNo.Enabled = false;
                    break;
                case mState.Insert:
                case mState.Edit:
                    BtnAcNo.Enabled = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueBankAcCode, LueCCCode, ChkActInd }, false);
                    BtnAcNo.Focus();
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtProcessCode, TxtProcessName, TxtIDNo, TxtVar1, TxtVar2, 
               TxtAcNo, TxtAcType, TxtAcDesc, LueBankAcCode, LueCCCode,
            });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTWCJournalSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            //SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtProcessCode, "", false)) return;
            //Sl.SetLueBankAcCode(ref LueBankAcCode);
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                var No = Sm.GetValue("Select MAX(CAST(IDno As INT))+1 As No From TblTWCJournalSetting ");

                
                if(TxtIDNo.Text.Length >0 )
                {
                    SQL.AppendLine("Update TblTWCJournalSetting ");
                    SQL.AppendLine("SET AcNo=@AcNo, BankAcCode=@BankAcCode, CCCode=@CCCode, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where IDNo=@IDNo; ");
                }
                else
                {
                    SQL.AppendLine("Insert Into TblTWCJournalSetting(IDNo, ProcessType, AcType, Var1, Var2, AcNo, BankAcCode, CCCode, ActInd, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@IDNo, @ProcessType, @AcType, @Var1, @Var2, @AcNo, @BankAcCode, @CCode, @ActInd, @UserCode, CurrentDateTime()) ");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@IDNo", (TxtIDNo.Text.Length == 0 ? No : TxtIDNo.Text));
                Sm.CmParam<String>(ref cm, "@ProcessType", TxtProcessCode.Text); 
                Sm.CmParam<String>(ref cm, "@AcType", TxtAcType.Text);
                Sm.CmParam<String>(ref cm, "@Var1", TxtVar1.Text);
                Sm.CmParam<String>(ref cm, "@Var2", TxtVar2.Text);
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked? "Y" : "N");
                Sm.ExecCommand(cm);

                ShowData(TxtIDNo.Text.Length == 0 ? No : TxtIDNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTWCJournalSettingDlg(this));
        }


        #endregion

        #region Show Data

        public void ShowData(string IDNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@IDNo", IDNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.IDNo, A.ProcessType As ProcessCode, "+
                        "Case When A.ProcessType = '1' THEN 'FLAT' " +
                        "When A.ProcessType = '2' Then 'TERUSAN' " +
                        "When A.ProcessType = '3' Then 'CASH' " +
                        "When A.ProcessType = '4' Then 'NON CASH' " +
                        "End As ProcessType, "+
                        "if(A.AcType='D', 'DEBET', 'CREDIT') As AcType, A.Var1, A.Var2, A.AcNo, "+
                        "B.AcDesc, A.BankAcCode, A.CCCode, A.ActInd "+
                        "From TblTWCJournalSetting A "+
                        "Left Join TblCOA B On A.Acno = B.Acno "+
                        "Where IDNo=@IDNo ",
                        new string[] 
                        {
                            "IDNo",  
                            "ProcessCode", "ProcessType", "AcType", "Var1", "Var2", 
                            "AcNo","AcDesc", "BankAcCode", "CCCode", "ActInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtIDNo.EditValue = Sm.DrStr(dr, c[0]);
                            TxtProcessCode.EditValue = Sm.DrStr(dr, c[1]);
                            TxtProcessName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtAcType.EditValue = Sm.DrStr(dr, c[3]);
                            TxtVar1.EditValue = Sm.DrStr(dr, c[4]);
                            TxtVar2.EditValue = Sm.DrStr(dr, c[5]);
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[6]);
                            TxtAcDesc.EditValue = Sm.DrStr(dr, c[7]);
                            Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[8]));
                            Sm.SetLue(LueCCCode, string.Empty);
                            SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[9]));
                            ChkActInd.Checked = Sm.DrStr(dr, c[10]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAcNo, "Account", false)
                ;
        }


        private void SetLueCCCode (ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 ");
            SQL.AppendLine("From TblCostCenter ");
            if (Code.Length == 0)
                SQL.AppendLine("Where ActInd='Y' ");
            else
                SQL.AppendLine("Where CCCode=@Code ");
            SQL.AppendLine("Order By CCName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }


        #endregion

        #region event
        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueCCCode_EditValueChanged (object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                 Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), string.Empty);
            }
        }
        #endregion

        
  
        #endregion
    }
}
