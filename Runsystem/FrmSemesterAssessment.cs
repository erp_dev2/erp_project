﻿#region Update
/*
  04/12/2019 [DITA/IMS] new apps 
  09/12/2019 [DITA/IMS] Printout Semester Assessment 
 * */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;
using FastReport;
using FastReport.Data;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmSemesterAssessment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = String.Empty;
        iGCell fCell;
        bool fAccept;
        internal FrmSemesterAssessmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmSemesterAssessment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Semester Assessment";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",
                    //1-5
                    "Type",
                    "Component",
                    "Value",
                    "Remark",
                    "AssessmentDNo"
                },
                new int[] 
                {
                    //0
                    100,

                    //1-5
                    150, 200, 100, 200, 100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 ,5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 5 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtAssessmentCode, TxtEmpCode,
                        MeeCancelReason, MeeRemark, TxtEmpName, TxtAssessmentName
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnAssessmentCode.Enabled = BtnEmpCode.Enabled =false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnAssessmentCode.Enabled = BtnEmpCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason
                    }, false);
                    Grd1.ReadOnly = true;
                    BtnAssessmentCode.Enabled  = BtnEmpCode.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtAssessmentCode, TxtEmpCode, MeeCancelReason, MeeRemark,
                TxtEmpName, TxtAssessmentName
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSemesterAssessmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);

        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if(ChkCancelInd.Checked == false) ParPrint();
            else Sm.StdMsg(mMsgType.Warning, "Data has been cancelled");

        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SemesterAssessment", "TblSemesterAssessmentHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSemesterAssessmentHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveSemesterAssessmentDtl(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtAssessmentCode, "Assessment Code", false) ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee Code", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string StartDt = string.Empty, EndDt = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                //if (Sm.IsGrdValueEmpty(Grd1, Row, 3, true, "Value is empty")) return true;
                if (Sm.GetGrdStr(Grd1, Row, 3).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value is empty");
                    return true;
                }
                if (Sm.GetGrdDec(Grd1, Row, 3) > 100)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value shouldn't be greater than 100 ");
                    return true;
                }

            }
            return false;
        }

        private MySqlCommand SaveSemesterAssessmentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSemesterAssessmentHdr(DocNo, DocDt, CancelInd, CancelReason, Status, AssessmentCode, EmpCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @CancelReason, 'A', @AssessmentCode, @EmpCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@AssessmentCode", TxtAssessmentCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSemesterAssessmentDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSemesterAssessmentDtl(DocNo, DNo, AssessmentCode, AssessmentDNo, Value, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AssessmentCode, @AssessmentDNo, @Value, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@AssessmentCode", TxtAssessmentCode.Text);
            Sm.CmParam<String>(ref cm, "@AssessmentDNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            
            cml.Add(EditSemesterAssessmentHdr());
           

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(EditSemesterAssessmentDtl(Row));
                }
            }
            
          
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Semester Assessment#", false) ||
                IsGrdValueNotValid() ||
                IsDocumentAlreadyCancel();
        }

        private bool IsDocumentAlreadyCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select 1 From TblSemesterAssessmentHdr Where DocNo=@DocNo And CancelInd='Y' "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancel");
                return true;
            }
            return false;
        }

  

        private MySqlCommand EditSemesterAssessmentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSemesterAssessmentHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason = @CancelReason,  ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand EditSemesterAssessmentDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSemesterAssessmentDtl Set ");
            SQL.AppendLine("    Value=@Value, Remark = @Remark,LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo and DNo=@Dno; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowSemesterAssessmentHdr(DocNo);
                ShowSemesterAssessmentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        private void ShowSemesterAssessmentHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();


            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.Cancelind, A.AssessmentCode, A.EmpCode, ");
            SQL.AppendLine("A.Remark, B.EmpName, C.AssessmentName ");
            SQL.AppendLine("From TblSemesterAssessmentHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblComponentSemesterAssessmentHdr C On A.AssessmentCode = C.AssessmentCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "Cancelind", "AssessmentCode", "AssessmentName", 

                        //6-8
                        "EmpCode", "EmpName", "Remark"
                        
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtAssessmentCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtAssessmentName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[7]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowSemesterAssessmentDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.AssessmentDNo, A.Value, A.Remark, C.OptDesc Type, B.Component ");
            SQL.AppendLine("From TblSemesterAssessmentDtl A ");
            SQL.AppendLine("Inner Join TblComponentSemesterAssessmentDtl B ON A.AssessmentCode = B.AssessmentCode And A.AssessmentDNo = B.AssessmentDNo ");
            SQL.AppendLine("Inner Join TblOption C On C.OptCode = B.Type And C.OptCat = 'ComponentSemesterAssessmentType'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    //1-5
                    "Type", "Component","Value", "Remark", "AssessmentDNo",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {

                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 1);
        }


        #endregion

        #region Additional Method

        internal void ShowSemesterAssessmentData(string AssessmentCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssessmentCode, B.AssessmentDNo, C.OptDesc Type, B.Component ");
            SQL.AppendLine("From  TblComponentSemesterAssessmentHdr A ");
            SQL.AppendLine("Inner Join TblComponentSemesterAssessmentDtl B On A.AssessmentCode = B.AssessmentCode  ");
            SQL.AppendLine("Inner Join TblOption C On C.OptCode = B.Type And C.OptCat = 'ComponentSemesterAssessmentType'  ");
            SQL.AppendLine("Where A.AssessmentCode = @AssessmentCode ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AssessmentCode", AssessmentCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "AssessmentDNo", 
                    //1-2
                    "Type", "Component"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ParPrint()
        {
            var l = new List<SemesterAssessmentHdr>();
            var ldtl = new List<SemesterAssessmentDtl>();

            string[] TableName = { "SemesterAssessmentHdr", "SemesterAssessmentDtl" };
            List<IList> myLists= new List<IList>();
            

            #region SemesterAssessmentHdr
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

             SQL.AppendLine("Select @CompanyLogo As CompanyLogo, ");
             SQL.AppendLine("B.AssessmentName, C.EmpCode, C.EmpName, D.PosName, E.DeptName, ");
             SQL.AppendLine("If(FIND_IN_SET(C.PosCode, @StructuralPosCodeSemesterAssessment), F.AvgValueStructural, F.AvgValueNonStructural) AS TotalAvg ");
             SQL.AppendLine("FROM TblSemesterAssessmentHdr A ");
             SQL.AppendLine("INNER JOIN TblComponentSemesterAssessmentHdr B ON A.AssessmentCode = B.AssessmentCode  ");
             SQL.AppendLine("INNER JOIN TblEmployee C ON A.Empcode = C.EmpCode ");
             SQL.AppendLine("LEFT Join TblPosition D On C.PosCode=D.PosCode  ");
             SQL.AppendLine("LEFT Join TblDepartment E On C.DeptCode=E.DeptCode  ");
             SQL.AppendLine("INNER JOIN  ");
             SQL.AppendLine("(  ");
             SQL.AppendLine("	SELECT T.DocNo, Sum(T.AvgValueStructural) AvgValueStructural, Sum(T.AvgValueNonStructural) AvgValueNonStructural  ");
             SQL.AppendLine("	FROM   ");
             SQL.AppendLine("	(  ");
             SQL.AppendLine("		SELECT T1.DocNo, Avg(T2.Value) AvgValueStructural, 0 AS AvgValueNonStructural  ");
             SQL.AppendLine("		FROM TblSemesterAssessmentHdr T1  ");
             SQL.AppendLine("		INNER JOIN TblSemesterAssessmentDtl T2 ON T1.DocNo = T2.DocNo AND T1.DocNo = @DocNo  ");
             SQL.AppendLine("		INNER JOIN TblEmployee T3 ON T1.EmpCode = T3.EmpCode  ");
             SQL.AppendLine("		INNER JOIN TblComponentSemesterAssessmentDtl T4 ON T1.AssessmentCode = T4.AssessmentCode And T2.AssessmentDNo = T4.AssessmentDNo  ");
             SQL.AppendLine("		WHERE FIND_IN_SET(T3.PosCode, @StructuralPosCodeSemesterAssessment)  ");
             SQL.AppendLine("		AND FIND_IN_SET(T4.Type, @StructuralComponentSemesterAssessmentTypeAvg)  ");
             SQL.AppendLine("		GROUP BY T1.DocNo  ");
             SQL.AppendLine("		UNION ALL   ");
             SQL.AppendLine("		SELECT T1.DocNo, 0 As AvgValueStructural, Avg(T2.Value) AS AvgValueNonStructural  ");
             SQL.AppendLine("		FROM TblSemesterAssessmentHdr T1  ");
             SQL.AppendLine("		INNER JOIN TblSemesterAssessmentDtl T2 ON T1.DocNo = T2.DocNo AND T1.DocNo = @DocNo  ");
             SQL.AppendLine("		INNER JOIN TblEmployee T3 ON T1.EmpCode = T3.EmpCode  ");
             SQL.AppendLine("		INNER JOIN TblComponentSemesterAssessmentDtl T4 ON T1.AssessmentCode = T4.AssessmentCode And T2.AssessmentDNo = T4.AssessmentDNo  ");
             SQL.AppendLine("		WHERE FIND_IN_SET(T3.PosCode, @NonStructuralPosCodeSemesterAssessment)  ");
             SQL.AppendLine("		AND FIND_IN_SET(T4.Type, @NonStructuralComponentSemesterAssessmentTypeAvg)  ");
             SQL.AppendLine("		GROUP BY T1.DocNo  ");
             SQL.AppendLine("	) T  ");
             SQL.AppendLine("	GROUP BY T.DocNo  ");
             SQL.AppendLine(") F ON A.DocNo = F.DocNo  ");
             SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@StructuralPosCodeSemesterAssessment", Sm.GetParameter("StructuralPosCodeSemesterAssessment"));
                Sm.CmParam<String>(ref cm, "@StructuralComponentSemesterAssessmentTypeAvg", Sm.GetParameter("StructuralComponentSemesterAssessmentTypeAvg"));
                Sm.CmParam<String>(ref cm, "@NonStructuralPosCodeSemesterAssessment", Sm.GetParameter("NonStructuralPosCodeSemesterAssessment"));
                Sm.CmParam<String>(ref cm, "@NonStructuralComponentSemesterAssessmentTypeAvg", Sm.GetParameter("NonStructuralComponentSemesterAssessmentTypeAvg"));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    {
                        //0
                        "CompanyLogo",

                        //1-5
                        "AssessmentName",
                        "EmpCode",
                        "EmpName",
                        "PosName",
                        "DeptName",

                        //6
                        "TotalAvg"


                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SemesterAssessmentHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            AssessmentName = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            EmpName = Sm.DrStr(dr, c[3]),
                            PosName = Sm.DrStr(dr, c[4]),
                            DeptName = Sm.DrStr(dr, c[5]),

                            TotalAvg = Sm.DrDec(dr, c[6])
                        });

                    }
                }

                dr.Close();
            }
            myLists.Add(l);
            #endregion


            #region SemesterAssessmentDtl
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("SELECT T.* ");
            SQL2.AppendLine("FROM  ");
            SQL2.AppendLine("( ");
            SQL2.AppendLine("	Select A.DocNo, D.Type, E.OptDesc TypeDesc, D.Component, B.Value, F.AvgValue ");
            SQL2.AppendLine("	FROM TblSemesterAssessmentHdr A ");
            SQL2.AppendLine("	INNER JOIN TblSemesterAssessmentDtl B ON A.DocNo=B.DocNo  ");
            SQL2.AppendLine("	INNER JOIN TblComponentSemesterAssessmentHdr C ON A.AssessmentCode = C.AssessmentCode ");
            SQL2.AppendLine("	INNER JOIN TblComponentSemesterAssessmentDtl D ON B.AssessmentCode = D.AssessmentCode AND D.AssessmentDNo = B.AssessmentDNo ");
            SQL2.AppendLine("	INNER JOIN TblOption E ON E.OptCode = D.Type AND E.OptCat = 'ComponentSemesterAssessmentType' ");
            SQL2.AppendLine("	INNER Join ");
            SQL2.AppendLine("	( ");
            SQL2.AppendLine("		SELECT X1.DocNo, X2.Type, AVG(X1.Value) AvgValue ");
            SQL2.AppendLine("		FROM TblSemesterAssessmentDtl X1 ");
            SQL2.AppendLine("		INNER JOIN TblComponentSemesterAssessmentDtl X2 ON X1.AssessmentCode = X2.AssessmentCode AND X1.AssessmentDNo = X2.AssessmentDNo ");
            SQL2.AppendLine("		WHERE X1.DocNo = @DocNo ");
            SQL2.AppendLine("		GROUP BY X1.DocNo, X2.Type ");
            SQL2.AppendLine("	) F ON A.DocNo = F.DocNo AND D.Type = F.Type ");
            SQL2.AppendLine(") T ");
            SQL2.AppendLine("WHERE T.DocNo = @DocNo ");
            SQL2.AppendLine("ORDER BY T.TypeDesc ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "Type",
                        "TypeDesc",
                        "Component",
                        "Value",
                        "AvgValue"


                    });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        ldtl.Add(new SemesterAssessmentDtl()
                        {
                            DocNo = Sm.DrStr(dr2, c2[0]),

                            TypeCode = Sm.DrStr(dr2, c2[1]),
                            TypeDesc = Sm.DrStr(dr2, c2[2]),
                            Component = Sm.DrStr(dr2, c2[3]),
                            Value = Sm.DrDec(dr2, c2[4]),
                            AvgValue = Sm.DrDec(dr2, c2[5]),
                        });
                       
                    }
                }

                dr2.Close();
            }
            myLists.Add(ldtl);

            #endregion
            Sm.PrintReport("SemesterAssessment", myLists, TableName, false);



        }

       
        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnAssessmentCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmSemesterAssessmentDlg(this));
            }
        }

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmSemesterAssessmentDlg2(this));
            }
        }
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }
        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);

        }
        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

       

        

        #endregion

        #region Class
        private class SemesterAssessmentHdr
        {
            public string CompanyLogo { get; set; }
            public string AssessmentName { get; set; }
            public string EmpName { get; set; }
            public string EmpCode { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public decimal TotalAvg { get; set; }

        }

        private class SemesterAssessmentDtl
        {
            public string DocNo { get; set; }
            public string TypeCode { get; set; }
            public string TypeDesc { get; set; }
            public string Component { get; set; }
            public decimal Value { get; set; }
            public decimal AvgValue { get; set; }

        }

        #endregion


    }
}
