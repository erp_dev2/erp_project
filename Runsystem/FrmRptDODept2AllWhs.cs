﻿#region Update
/*
    16/10/2017 [WED] revisi query, tambah group by DocDt, having, menghilangkan Abs()
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDODept2AllWhs : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<Warehouse> lWhs = null;
        private int mFirstColumnForWhs = -1;
        internal int ColCounts = 0;

        #endregion

        #region Constructor

        public FrmRptDODept2AllWhs(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetLueDocType(ref LueDocType);
                Sl.SetLueItCtCode(ref LueItCatCode);
                Sm.SetLue(LueDocType, "14");
                Sm.SetControlReadOnly(new List<BaseEdit> 
                { 
                    LueDocType
                }, true);

                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-29);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL(string DocType)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType, A.ItCode, C.ItName, C.ItCtCode, D.ItCtName, A.WhsCode, ");
            SQL.AppendLine("E.WhsCode2, E.WhsName As WhsTo, ");
            SQL.AppendLine("Sum(A.Qty * B.UPrice * B.ExcRate * -1) As Price ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocType, T1.DocDt, T1.DocNo, T1.Source, T1.WhsCode, T1.ItCode, T1.BatchNo, Sum(T1.Qty) As Qty ");
            SQL.AppendLine("    From TblStockMovement T1 ");
            //SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine("    Where 0 = 0 ");
            //SQL.AppendLine("    And T1.Qty < 0 ");
            SQL.AppendLine("    And T1.DocType = @DocType ");
            SQL.AppendLine("    And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Group By T1.DocType, T1.DocNo, T1.DocDt, T1.Source, T1.WhsCode, T1.ItCode ");
            SQL.AppendLine("    Having Sum(T1.Qty <> 0) ");
            SQL.AppendLine(")A ");
            SQL.AppendLine("Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("Inner Join TblItem C on A.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblItemCategory D On C.ItCtCode = D.ItCtCode ");            
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select T1.DocNo, T1.WhsCode As WhsCode2, T2.WhsName ");
            SQL.AppendLine("  From TblDODeptHdr T1 ");
            SQL.AppendLine("  Inner Join TblWarehouse T2 On T1.WhsCode = T2.WhsCode ");
            SQL.AppendLine("  Where (T1.DORequestDeptDocNo Is Null Or Length(T1.DORequestDeptDocNo) <= 0) ");
            SQL.AppendLine("  And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            SQL.AppendLine("Where A.DocType = @DocType ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (Sm.GetLue(LueItCatCode).Length > 0)
            {
                SQL.AppendLine("And C.ItCtCode = '" + Sm.GetLue(LueItCatCode) + "' ");
            }

            if (TxtItCode.Text.Length > 0)
            {
                SQL.AppendLine("And (A.ItCode Like '%" + TxtItCode.Text + "%' Or C.ItName Like '%" + TxtItCode.Text + "%') ");
            }

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            ColCounts = Grd1.Cols.Count - 1;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Item Code",
                        "Item Name",
                        "Item Category" + Environment.NewLine + "Code",
                        "Item Category",
                        "Total"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 180, 100, 180, 200
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                Sm.IsLueEmpty(LueDocType, "Type")
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SetSQL(Sm.GetLue(LueDocType)) + Filter + " Group By A.DocType, A.ItCode;",
                    new string[]
                    { 
                        //0
                        "ItCode",
                        
                        //1-4
                        "ItName", "ItCtCode", "ItCtName", "Price"
                    },

                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        //Grd.Cells[Row, 6].Value = 0m;
                    }, true, false, false, false
                );

                lWhs = new List<Warehouse>();
                SetWhsToColumn(SetSQL(Sm.GetLue(LueDocType)) + Filter + " Group By A.DocType, A.DocNo, A.WhsCode, E.WhsCode2, A.ItCode");
                ProcessHarga(SetSQL(Sm.GetLue(LueDocType)) + Filter + " Group By A.DocType, A.WhsCode, E.WhsCode2, A.ItCode Order By A.ItCode");

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                int a = ColCounts;
                for (int r = 0; r <= lWhs.Count; r++)
                {
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                    {  
                        (a + r)
                    });
                }
                Sm.SetGrdAlwaysShowSubTotal(Grd1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Methods

        private void SetLueDocType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.OptCode As Col1, T.OptDesc As Col2 ");
            SQL.AppendLine("From TblOption T Where T.OptCat = 'InventoryTransType' ");
            SQL.AppendLine("And T.OptCode = '14'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        private void SetWhsToColumn(string SQL)
        {
            lWhs.Clear();
            Grd1.Cols.Count = 6;
            var LatestColumn = Grd1.Cols.Count - 1;
            var cm1 = new MySqlCommand();
            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                var WhsColumn = new StringBuilder();

                WhsColumn.AppendLine("Select Distinct T.WhsCode2, T.WhsTo, T.WhsCode ");
                WhsColumn.AppendLine("From ");
                WhsColumn.AppendLine("( ");
                WhsColumn.AppendLine(SQL);
                WhsColumn.AppendLine(")T ");

                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandTimeout = 600;
                cm1.CommandText = WhsColumn.ToString();
                Sm.CmParamDt(ref cm1, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm1, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm1, "@DocType", Sm.GetLue(LueDocType));
                //Sm.FilterDt(ref Filter, ref cm1, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] { "WhsCode2", "WhsTo", "WhsCode" });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        LatestColumn += 1;
                        if (mFirstColumnForWhs == -1) mFirstColumnForWhs = LatestColumn;
                        lWhs.Add(new Warehouse()
                        {
                            WhsCode2 = Sm.DrStr(dr1, c1[0]),
                            WhsName2 = Sm.DrStr(dr1, c1[1]),
                            WhsCode = Sm.DrStr(dr1, c1[2]),
                            Column = LatestColumn
                        });
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = Sm.DrStr(dr1, c1[1]);
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 250;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);
                    }
                }
                dr1.Close();
            }
        }

        private void ProcessHarga(string SQL)
        {
            if (lWhs.Count > 0)
            {
                for (int Col = mFirstColumnForWhs; Col < Grd1.Cols.Count; Col++)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, Col].Value = 0m;
                }
                var l = new List<PriceWhsTo>();
                ProcessHarga1(ref l, SQL);
                if (l.Count > 0) ProcessHarga2(ref l, ref lWhs);
            }
        }

        private void ProcessHarga1(ref List<PriceWhsTo> l, string SQL)
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL;
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WhsCode2", "Price", "ItCtCode", "ItCode", "WhsCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PriceWhsTo()
                        {
                            WhsToColumn = GetWhsColumn(Sm.DrStr(dr, c[0])),
                            Price = Sm.DrDec(dr, c[1]),
                            ItCtCode = Sm.DrStr(dr, c[2]),
                            ItCode = Sm.DrStr(dr, c[3]),
                            WhsCode = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private int GetWhsColumn(string WhsCode2)
        {
            foreach (var x in lWhs.Where(x => string.Compare(x.WhsCode2, WhsCode2) == 0))
                return x.Column;
            return -1;
        }

        private void ProcessHarga2(ref List<PriceWhsTo> l, ref List<Warehouse> lWhs)
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                for (var i = 0; i < l.Count; i++)
                {                
                    var ItCodes = Sm.GetGrdStr(Grd1, Row, 1);
                    var ItCtCodes = Sm.GetGrdStr(Grd1, Row, 3);
                    
                    if (
                        string.Compare(l[i].ItCode, ItCodes) == 0 &&
                        string.Compare(l[i].ItCtCode, ItCtCodes) == 0 &&
                        string.Compare(l[i].WhsCode, l[i].WhsCode) == 0
                        )
                    {
                        if (l[i].WhsToColumn >= 0)
                        {
                            //Grd1.Cells[Row, 6].Value =+ l[i].Price;
                            Grd1.Cells[Row, l[i].WhsToColumn].Value =+ l[i].Price;
                        }
                        //break;
                    }
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            var ColCount = new int[Grd1.Cols.Count - 5];
            var i = 0;
            for (int c = 5; c < Grd1.Cols.Count; c++)
            {
                ColCount[i] = c;
                i++;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, ColCount);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #region Button Events

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 3].Value = "'" + Sm.GetGrdStr(Grd1, Row, 3);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 3].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 3).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Class

        private class Warehouse
        {
            public string WhsCode2 { get; set; }
            public string WhsName2 { get; set; }
            public string WhsCode { get; set; }
            public int Column { get; set; }

        }

        private class PriceWhsTo
        {
            public int WhsToColumn { get; set; }
            public decimal Price { get; set; }
            public string ItCtCode { get; set; }
            public string WhsCode { get; set; }
            public string ItCode { get; set; }
        }

        #endregion

    }
}
