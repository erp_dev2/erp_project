﻿#region Update
/*
    04/09/2019 [TKG] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMSoldOfflineDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmIMMSoldOffline mFrmParent;
        private string mWhsCode = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmIMMSoldOfflineDlg(FrmIMMSoldOffline FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueBin(ref LueBin);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "",
                        "Bin",
                        "Location Code",
                        "Location",
                        "Source",
                        
                        //6-9
                        "Product's Code",
                        "Product's Description",
                        "Color",
                        "Seller"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 130, 0, 200, 180, 
                        
                        //6-9
                        120, 200, 100, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Bin, A.LocCode, D.LocName, A.Source, B.ProdCode, B.ProdDesc, B.Color, C.SellerName ");
            SQL.AppendLine("From TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblIMMProduct B On A.ProdCode=B.ProdCode ");
            SQL.AppendLine("Left Join TblIMMSeller C On A.SellerCode=C.SellerCode ");
            SQL.AppendLine("Left Join TblIMMLocation D On A.LocCode=D.LocCode ");
            SQL.AppendLine("Inner Join TblBin E On A.Bin=E.Bin And E.QuarantineInd='Y' ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode ");
            SQL.AppendLine("And A.Qty>0.00 ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ", Filter2 = string.Empty, Source = string.Empty;
                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(mFrmParent.Grd1, r, 4);
                        if (Source.Length != 0)
                        {
                            if (Filter2.Length > 0) Filter2 += " And ";
                            Filter2 += "(A.Source<>@Source0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }

                if (Filter2.Length != 0) Filter2 = " And (" + Filter2 + ")";

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.FilterStr(ref Filter, ref cm, TxtProdCode.Text, new string[] { "B.ProdCode", "B.ProdDesc" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBin), "A.Bin", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "A.Source", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        string.Concat(mSQL, Filter, Filter2, " Order By B.ProdDesc, B.ProdCode, A.Source;"),
                        new string[] 
                        { 
                            //0
                           "Bin",

                           //1-5
                           "LocCode", "LocName", "Source", "ProdCode", "ProdDesc", 

                           //6-7
                           "Color", "SellerName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        mFrmParent.Grd1.Rows.Add();
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 product.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string Source = Sm.GetGrdStr(Grd1, Row, 5);
            for (int r = 0; r <= mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Source, Sm.GetGrdStr(mFrmParent.Grd1, r, 4)))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(Sl.SetLueBin));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bin");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        private void TxtProdCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Product");
        }

        #endregion

        #endregion
    }
}
