﻿#region Update
/*
    10/05/2020 [TKG] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHA12Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRHA12 mFrmParent;
        private string
            mHolidayDt = string.Empty,
            mSiteCode = string.Empty;
        private decimal
            mFunctionalExpenses = 0m,
            mFunctionalExpensesMaxAmt = 0m;
        private bool mIsRHATaxDeductionEnabled = false;
        private List<TI> mlTI = null;
        private List<NTI> mlNTI = null;

        #endregion

        #region Constructor

        public FrmRHA12Dlg(FrmRHA12 FrmParent, string HolidayDt, string SiteCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mHolidayDt = HolidayDt;
            mSiteCode = SiteCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                GetData();
                SetGrd();
                Sl.SetLueOption(ref LueReligion, "Religion");
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Employee's Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        
                        //6-10
                        "Department",
                        "Religion",
                        "Join",
                        "Resign",
                        "Bank",

                        //11-15  
                        "Bank's Account#",
                        "Salary (A)",
                        "Allowance (B)",
                        "A+B",
                        "Prorated",
                        
                        //16-20
                        "Tax",
                        "Amount",
                        "NPWP",
                        "PTKP",
                        "SS BPJS (Tax)"
                      },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 130, 200, 150, 150,   
                        
                        //6-10
                        150, 100, 100, 100, 150, 

                        //11-15
                        150, 120, 120, 120, 80,

                        //16-20
                        130, 120, 120, 120, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 15 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 16, 17, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            var subSQL = new StringBuilder();

            subSQL.AppendLine(" (");
            subSQL.AppendLine("     Select EmpCode From TblEmployee ");
            subSQL.AppendLine("     Where Religion Is Not Null ");
            subSQL.AppendLine("     And SiteCode=@SiteCode ");
            subSQL.AppendLine(Filter);
            subSQL.AppendLine("     And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@HolidayDt)) ");
            subSQL.AppendLine("     And TimeStampDiff(MONTH, Concat(Left(JoinDt, 4), '-', Substring(JoinDt, 5, 2), '-', Substring(JoinDt, 7, 2)), @HolidayDate)>0 ");
            subSQL.AppendLine("     And EmpCode Not In (");
            subSQL.AppendLine("         Select T2.EmpCode ");
            subSQL.AppendLine("         From TblRHAHdr T1 ");
            subSQL.AppendLine("         Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo ");
            subSQL.AppendLine("         Where T1.CancelInd='N' ");
            subSQL.AppendLine("         And T1.Status In ('O', 'A') ");
            subSQL.AppendLine("         And Left(HolidayDt, 4)=Left(@HolidayDt, 4) ");
            subSQL.AppendLine("         ) ");
            subSQL.AppendLine(" ) ");

            var Query = subSQL.ToString();

            SQL.AppendLine("Select T.EmpCode, T.EmpName, T.EmpCodeOld, T.PosName, T.DeptName, T.Religion, T.JoinDt, T.ResignDt, T.BankName, T.BankAcNo, ");
            SQL.AppendLine("T.Value, T.Value2, T.Value3, ");
            SQL.AppendLine("Case When T.WorkingTime<12.00 Then 'Y' Else 'N' End As ProrateInd, ");
            SQL.AppendLine("Case When T.WorkingTime<12.00 Then (T.WorkingTime/12.00)*T.Value3 Else T.Value3 End As Amt, ");
            SQL.AppendLine("T.NPWP, T.PTKP, IfNull(T.SSTax, 0.00) As SSTax  ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, D.OptDesc As Religion, ");
            SQL.AppendLine("    A.JoinDt, A.ResignDt, E.BankName, A.BankAcNo, ");
            SQL.AppendLine("    IfNull(F.BasicSalary, 0.00) As Value, ");
            SQL.AppendLine("    IfNull(G.Value2a, 0.00)+IfNull(H.Value2b, 0.00) As Value2, ");
            SQL.AppendLine("    IfNull(F.BasicSalary, 0.00)+IfNull(G.Value2a, 0.00)+IfNull(H.Value2b, 0.00) As Value3, ");
            SQL.AppendLine("    TimeStampDiff(MONTH, Concat(Left(A.JoinDt, 4), '-', Substring(A.JoinDt, 5, 2), '-', Substring(A.JoinDt, 7, 2)), @HolidayDate) As WorkingTime, ");
            SQL.AppendLine("    A.NPWP, A.PTKP, IfNull(I.SSTax, 0.00) As SSTax ");
            SQL.AppendLine("    From TblEmployee A "); 
            SQL.AppendLine("    Left Join TblPosition B On A.PosCode=B.PosCode "); 
            SQL.AppendLine("    Left Join TblDepartment C On A.DeptCode=C.DeptCode "); 
            SQL.AppendLine("    Left Join TblOption D On A.Religion=D.OptCode And D.OptCat='Religion' "); 
            SQL.AppendLine("    Left Join TblBank E On A.BankCode=E.BankCode ");
            SQL.AppendLine("    Left Join TblGradeLevelHdr F On A.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("    Left Join (");
            SQL.AppendLine("        Select T1.GrdLvlCode, Sum(T1.Amt) Value2a ");
            SQL.AppendLine("        From TblGradeLevelDtl T1, TblAllowanceDeduction T2 ");
            SQL.AppendLine("        Where T1.ADCode=T2.ADCode ");
            SQL.AppendLine("        And T2.ADType='A' ");
            SQL.AppendLine("        And T2.AmtType='1' ");
            SQL.AppendLine("        Group By T1.GrdLvlCode ");
            SQL.AppendLine("    ) G On A.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.EmpCode, Sum(T1.Amt) As Value2b ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("        Inner Join TblAllowanceDeduction T2 On T1.ADCode=T2.ADCode And T2.ADType='A' And T2.AmtType='1' ");
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("        (T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("        (T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt<=@HolidayDt) Or ");
            SQL.AppendLine("        (T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt<=T1.EndDt) Or ");
            SQL.AppendLine("        (T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt<=@HolidayDt And @HolidayDt<=T1.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And T1.EmpCode In ");
            SQL.AppendLine(Query);
            SQL.AppendLine("        Group By T1.EmpCode ");
            SQL.AppendLine("    ) H On A.EmpCode=H.EmpCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.EmpCode, Sum(T2.EmployeePerc) As SSTax ");
            SQL.AppendLine("        From TblEmployeeSS T1 ");
            SQL.AppendLine("        Inner Join TblSS T2 On T1.SSCode=T2.SSCode ");
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("        (T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("        (T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt<=@HolidayDt) Or ");
            SQL.AppendLine("        (T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt<=T1.EndDt) Or ");
            SQL.AppendLine("        (T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt<=@HolidayDt And @HolidayDt<=T1.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And Find_In_Set(T1.SSCode, ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='SSCodeRHA'), '')) ");
            SQL.AppendLine("        And T1.EmpCode In ");
            SQL.AppendLine(Query);
            SQL.AppendLine("        Group By T1.EmpCode ");
            SQL.AppendLine("    ) I On A.EmpCode=I.EmpCode ");
            SQL.AppendLine("    Where A.EmpCode In ");
            SQL.AppendLine(Query);
            SQL.AppendLine(") T ");
            SQL.AppendLine("Where Case When T.WorkingTime<12.00 Then (T.WorkingTime/12.00)*T.Value3 Else T.Value3 End>0.00 ");
            SQL.AppendLine("Order By T.DeptName, T.EmpName;");
            
            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, EmpCode = string.Empty;

                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        if (EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " EmpCode=@EmpCode0" + r.ToString();
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And Not (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@HolidayDt", mHolidayDt);
                Sm.CmParam<String>(ref cm, "@HolidayDate",
                    string.Concat(
                        Sm.Left(mHolidayDt, 4), "-",
                        mHolidayDt.Substring(4, 2), "-",
                        mHolidayDt.Substring(6, 2)
                        ));

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "EmpCode", "EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueReligion), "Religion", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "EmpCode",
                            
                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "Religion",
                            
                            //6-10
                            "JoinDt", "ResignDt", "BankName", "BankAcNo", "Value",
                            
                            //11-15
                            "Value2", "Value3", "ProrateInd", "Amt", "NPWP",

                            //16-17
                            "PTKP", "SSTax"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 15, 13);
                            Grd.Cells[Row, 16].Value = 0m;
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                        }, true, false, false, false
                    );
                ComputeTax();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 17);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 11, 12, 13, 15, 16 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            mFrmParent.ComputeAmt();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 1), EmpCode)) return true;
            return false;
        }

        private void ComputeTax()
        {
            var l = new List<RHA>();
            try
            {
                string EmpCodeTemp = string.Empty;

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCodeTemp.Length > 0)
                    {
                        l.Add(new RHA()
                        {
                            EmpCode = EmpCodeTemp,
                            Amt = Sm.GetGrdDec(Grd1, r, 17),
                            Tax = 0m,
                            NPWP = Sm.GetGrdStr(Grd1, r, 18),
                            PTKP = Sm.GetGrdStr(Grd1, r, 19),
                            SSTax = Sm.GetGrdDec(Grd1, r, 20)
                        });
                    }
                }

                if (l.Count>0)
                {
                    for(int i =0;i<l.Count;i++)
                    {
                        if (l[i].NPWP.Length != 0 && l[i].PTKP.Length != 0)
                        {
                            ProcessTax(ref l, i);
                            for (int r = 0; r < Grd1.Rows.Count; r++)
                            {
                                if (Sm.CompareStr(l[i].EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                                {
                                    Grd1.Cells[r, 16].Value = l[i].Tax;
                                    if (mIsRHATaxDeductionEnabled)
                                        Grd1.Cells[r, 17].Value = Sm.GetGrdDec(Grd1, r, 17)-l[i].Tax;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                l.Clear();
            }
        }

        private void ProcessTax(ref List<RHA> l, int i)
        {
            string NTI = l[i].PTKP;
            decimal
                Value = 0m,
                Value2 = 0m,
                Pengurang = 0m,
                Pengurang2 = 0m,
                NTIAmt = 0m,
                NTIAmt2 = 0m,
                TaxTemp = 0m,
                Amt2Temp = 0m,
                TaxTemp2 = 0m,
                Amt2Temp2 = 0m;

            Value = (l[i].Amt * 12m) + l[i].Amt;

            Pengurang = (l[i].SSTax*0.01m*l[i].Amt*12m);
            if (mFunctionalExpenses * 0.01m * Value > mFunctionalExpensesMaxAmt * 12m)
                Pengurang += (mFunctionalExpensesMaxAmt*12m);
            else
                Pengurang += (mFunctionalExpenses * 0.01m * Value);


            Value = Value - Pengurang;
            
            foreach (var x in mlNTI.Where(x => x.Status==NTI))
                NTIAmt = x.Amt;

            if (Value - NTIAmt < 0m)
                Value = 0m;
            else
                Value = Value - NTIAmt;

            //Value = decimal.Truncate(Value);
            //Value = Value - (Value % 1000);
            
            if (Value > 0m && mlTI.Count > 0)
            {
                TaxTemp = Value;
                Amt2Temp = 0m;
                Value = 0m;

                foreach (TI x in mlTI.OrderBy(o =>o.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (x.Amt2 - Amt2Temp))
                        {
                            Value += (TaxTemp * x.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            Value += ((x.Amt2 - Amt2Temp) * x.TaxRate * 0.01m);
                            TaxTemp -= (x.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = x.Amt2;
                }
            }

            Value2 = (l[i].Amt * 12m);

            Pengurang2 = (l[i].SSTax * 0.01m * l[i].Amt*12m);
            if (mFunctionalExpenses * 0.01m * Value2 > mFunctionalExpensesMaxAmt * 12m)
                Pengurang2 += (mFunctionalExpensesMaxAmt * 12m);
            else
                Pengurang2 += (mFunctionalExpenses * 0.01m * Value2);

            Value2 = Value2 - Pengurang2;

            foreach (var x in mlNTI.Where(x => x.Status == NTI))
                NTIAmt2 = x.Amt;

            if (Value2 - NTIAmt2 < 0m)
                Value2 = 0m;
            else
                Value2 = Value2 - NTIAmt2;

            //Value2 = decimal.Truncate(Value2);
            //Value2 = Value2 - (Value2 % 1000);

            if (Value2 > 0m && mlTI.Count > 0)
            {
                TaxTemp2 = Value2;
                Amt2Temp2 = 0m;
                Value2 = 0m;

                foreach (TI x in mlTI.OrderBy(o => o.SeqNo))
                {
                    if (TaxTemp2 > 0m)
                    {
                        if (TaxTemp2 <= (x.Amt2 - Amt2Temp2))
                        {
                            Value2 += (TaxTemp2 * x.TaxRate * 0.01m);
                            TaxTemp2 = 0m;
                        }
                        else
                        {
                            Value2 += ((x.Amt2 - Amt2Temp2) * x.TaxRate * 0.01m);
                            TaxTemp2 -= (x.Amt2 - Amt2Temp2);
                        }
                    }
                    Amt2Temp2 = x.Amt2;
                }
            }
            l[i].Tax = Value - Value2;
            if (l[i].Tax <= 0) l[i].Tax = 0m;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsRHATaxDeductionEnabled = Sm.GetParameterBoo("IsRHATaxDeductionEnabled");
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
        }

        private void GetData()
        {
            mlTI = new List<TI>();
            mlNTI = new List<NTI>();

            ProcessTI(ref mlTI);
            ProcessNTI(ref mlNTI);
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueReligion_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueReligion, new Sm.RefreshLue2(Sl.SetLueOption), "Religion");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkReligion_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Religion");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion
       
        #endregion

        #region Class

        private class RHA
        {
            public string EmpCode { get; set; }
            public decimal Tax { get; set; }
            public decimal Amt { get; set; }
            public decimal SSTax { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
        }

        private class NTI
        {
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        #endregion

    }
}
