﻿    #region Update
/*
    16/06/2021 [RDH+RDA/KSM] menu baru Monthly Outstanding DO to Customer - Rekap Sisa Order
    06/09/2021 [TKG/KSM] rombak aplikasi, masih banyak bolongnya.
    09/09/2021 [RDA/KSM] penyesuaian print out menu Monthly Outstanding DO to Customer - Rekap Sisa Order
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using System.Collections;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyOutstandingDOCt3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string mItemCtCodeForMonthlyOutstadingDoct3= string.Empty;

        #endregion

        #region Constructor

        public FrmRptMonthlyOutstandingDOCt3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                Sl.SetLueVdCode(ref LueVendor);
                Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
                GetParameter();
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter1, string Filter2, string Filter3)
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Select X1.ItName, X1.ItCtName, X1.Qty, X1.PurchaseUomCode, X1.VdName, ");
            SQL.AppendLine("X1.RecvVdQty, X1.InventoryUomCode, ");
            SQL.AppendLine("X1.RecvVdQty2, X1.InventoryUomCode2, ");
            SQL.AppendLine("X1.Balance, X1.CurCode, ");
            SQL.AppendLine("IfNull(X1.UPrice, 0.00)+");
            SQL.AppendLine("(IfNull(X1.UPrice, 0.00)*IfNull(X2.TaxRate, 0.00)*0.01)+ ");
            SQL.AppendLine("(IfNull(X1.UPrice, 0.00)*IfNull(X3.TaxRate, 0.00)*0.01)+ ");
            SQL.AppendLine("(IfNull(X1.UPrice, 0.00)*IfNull(X4.TaxRate, 0.00)*0.01) As UPrice ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select G.ItName, I.ItCtName, B.Qty, G.PurchaseUomCode, H.VdName, ");
            SQL.AppendLine("    IfNull(J.Qty, 0.00) As RecvVdQty, G.InventoryUomCode, ");
            SQL.AppendLine("    IfNull(J.Qty2, 0.00) As RecvVdQty2, G.InventoryUomCode2, ");
            SQL.AppendLine("    B.Qty-IfNull(J.QtyPurchase, 0.00) As Balance, ");
            SQL.AppendLine("    E.CurCode, ");
            SQL.AppendLine("    F.UPrice-(F.UPrice*B.Discount*0.01)-(B.DiscountAmt/B.Qty)+(B.RoundingValue/B.Qty) As UPrice, ");
            SQL.AppendLine("    A.TaxCode1, A.TaxCode2, A.TaxCode3 ");
            SQL.AppendLine("    From TblPOHdr A");
            SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo");
            SQL.AppendLine("    Inner Join TblQtHdr E On C.QtDocNo=E.DocNo ");
            if (Filter2.Length > 0) SQL.AppendLine(Filter2.Replace("X.", "E."));
            SQL.AppendLine("    Inner Join TblQtDtl F On C.QtDocNo=F.DocNo And C.QtDNo=F.DNo ");
            SQL.AppendLine("    Inner Join TblItem G On D.ItCode=G.ItCode ");
            if (mItemCtCodeForMonthlyOutstadingDoct3.Length > 1)
                SQL.AppendLine("    And Find_In_Set(G.ItCtCode, @Param) ");
            if (Filter1.Length > 0) SQL.AppendLine(Filter1.Replace("X.", "G."));
            if (Filter3.Length > 0) SQL.AppendLine(Filter3.Replace("X.", "G."));
            SQL.AppendLine("    Inner Join TblVendor H On E.VdCode=H.VdCode ");
            SQL.AppendLine("    Inner Join TblItemCategory I On G.ItCtCode=I.ItCtCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.DocNo, T2.DNo, Sum(T3.QtyPurchase) As QtyPurchase, Sum(T3.Qty) As Qty, Sum(T3.Qty2) As Qty2 ");
            SQL.AppendLine("        From TblPOHdr T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T3 On T2.DocNo=T3.PODocNo And T2.DNo=T3.PODNo And T3.CancelInd='N' And T3.Status='A' ");
            SQL.AppendLine("        Inner Join TblRecvVdHdr T4 On T3.DocNo=T4.DocNo ");
            if (Filter2.Length > 0) SQL.AppendLine(Filter2.Replace("X.", "T4."));
            SQL.AppendLine("    Inner Join TblItem T5 On T3.ItCode=T5.ItCode ");
            if (mItemCtCodeForMonthlyOutstadingDoct3.Length > 1)
                SQL.AppendLine("    And Find_In_Set(T5.ItCtCode, @Param) ");
            if (Filter1.Length > 0) SQL.AppendLine(Filter1.Replace("X.", "T5."));
            if (Filter3.Length > 0) SQL.AppendLine(Filter3.Replace("X.", "T5."));
            SQL.AppendLine("        Where T1.Status='A' ");
            SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine("    ) J On B.DocNo=J.DocNo And B.DNo=J.DNo ");
            SQL.AppendLine("    Where A.Status='A' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") X1 ");
            SQL.AppendLine("Left Join TblTax X2 On X1.TaxCode1=X2.TaxCode ");
            SQL.AppendLine("Left Join TblTax X3 On X1.TaxCode2=X3.TaxCode ");
            SQL.AppendLine("Left Join TblTax X4 On X1.TaxCode3=X4.TaxCode ");
            SQL.AppendLine("Order By X1.ItName; ");


            //SQL.AppendLine("SELECT T1.ItName,T1.Qty,T1.UoM,T1.VdName");
            //SQL.AppendLine(",IFNULL(T2.Qty1,0)Qty1,T2.InventoryUOMCode,IFNULL(T2.Qty2,0)Qty2,T2.InventoryUOMCode2");
            //SQL.AppendLine(",(T1.Qty - IFNULL(T2.Qty1,0)) OutstandingQty");
            //SQL.AppendLine(",(((100-T1.Discount)/100)*(T1.Qty*T1.UPrice)) - (T1.DiscountAmt+ T1.RoundingValue) AS total");
            //SQL.AppendLine("FROM (");
            //SQL.AppendLine("       SELECT A.DocNo,A.DNo,H.ItCode,H.ItName,A1.DocDt, SUM(ifnull(A.Qty,0)) Qty,A.Discount,A.DiscountAmt,A.RoundingValue,H.PurchaseUomCode UoM");
            //SQL.AppendLine("       ,I.VdCode,I.VdName,G.UPrice,J.ItCtCode");
            //SQL.AppendLine("       FROM tblpohdr A1");
            //SQL.AppendLine("       INNER JOIN TblPODtl A ON A1.DocNo = A.DocNo");
            //SQL.AppendLine("       INNER JOIN TblPORequestHdr B ON A.PORequestDocNo=B.DocNo");
            //SQL.AppendLine("       INNER JOIN TblPORequestDtl C ON A.PORequestDocNo=C.DocNo AND A.PORequestDNo=C.DNo");
            //SQL.AppendLine("       INNER JOIN TblMaterialRequestHdr D ON C.MaterialRequestDocNo=D.DocNo");
            //SQL.AppendLine("       INNER JOIN TblMaterialRequestDtl E ON C.MaterialRequestDocNo=E.DocNo AND C.MaterialRequestDNo=E.DNo");
            //SQL.AppendLine("       INNER JOIN TblQtHdr F ON C.QtDocNo=F.DocNo ");
            //SQL.AppendLine("       INNER JOIN TblQtDtl G ON C.QtDocNo=G.DocNo AND C.QtDNo=G.DNo ");
            //SQL.AppendLine("       INNER JOIN TblItem H ON E.ItCode=H.ItCode ");
            //SQL.AppendLine("       INNER JOIN TblVendor I ON F.VdCode=I.VdCode ");
            //SQL.AppendLine("       INNER JOIN tblitemcategory J ON H.ItCtCode = J.ItCtCode ");
            //SQL.AppendLine("       GROUP BY E.ItCode ");
            //SQL.AppendLine("    )T1");
            //SQL.AppendLine("LEFT JOIN (");
            //SQL.AppendLine("       SELECT L.itCode,K.WhsCode,SUM(IFNULL(L.Qty,0))Qty1,P.InventoryUOMCode,SUM(IFNULL(L.Qty2,0))qty2, P.InventoryUOMCode2 FROM tblrecvvdhdr K");
            //SQL.AppendLine("       INNER JOIN tblrecvvddtl L ON K.DocNo = L.DocNo AND L.CancelInd = 'N'");
            //SQL.AppendLine("       INNER JOIN tblpodtl M ON L.PODocNo = M.DocNo AND L.PODNo = M.DNo");
            //SQL.AppendLine("       Inner Join tblporequestdtl N ON M.PORequestDocNo = N.DocNo AND M.PORequestDNo = N.DNo");
            //SQL.AppendLine("       Inner Join TblMaterialRequestDtl O ON N.MaterialRequestDocNo=O.DocNo AND N.MaterialRequestDNo=O.DNo");
            //SQL.AppendLine("       Inner Join TblItem P On O.ItCode=P.ItCode");
            //SQL.AppendLine("       GROUP BY L.ItCode");
            //SQL.AppendLine(")T2 ON T1.ItCode = T2.ItCode");
            //SQL.AppendLine("WHERE 1=1 ");

            //if (mItemCtCodeForMonthlyOutstadingDoct3.Length >1)
            //    SQL.AppendLine("AND FIND_IN_SET(T1.itCtCode,@Param)");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 13;

            SetGrdHdr(ref Grd1, 0, 0, "No",2,40);
            SetGrdHdr(ref Grd1, 0, 1, "Contruction", 2, 200);
            SetGrdHdr(ref Grd1, 0, 2, "PO's Quantity", 2, 120);
            SetGrdHdr(ref Grd1, 0, 3, "UoM", 2, 80);
            SetGrdHdr(ref Grd1, 0, 4, "Supplier", 2, 200);
            SetGrdHdr2(ref Grd1, 1, 5, "Received", 4, 100);
            SetGrdHdr(ref Grd1, 0, 5, "Quantity", 1, 100);
            SetGrdHdr(ref Grd1, 0, 6, "UoM",1, 80);
            SetGrdHdr(ref Grd1, 0, 7, "Quantity", 1, 100);
            SetGrdHdr(ref Grd1, 0, 8, "UoM", 1, 80);
            SetGrdHdr(ref Grd1, 0, 9, "Rest Order" + Environment.NewLine + "(Purchase)", 2, 100);
            SetGrdHdr(ref Grd1, 0, 10, "Currency", 2, 80);
            SetGrdHdr(ref Grd1, 0, 11, "Price After Tax", 2, 120);
            SetGrdHdr(ref Grd1, 0, 12, "Item's Category", 2, 200);

            Sm.GrdFormatDec(Grd1, new int[] { 2, 5, 7, 9, 11 }, 0);
            Grd1.Cols[12].Move(3);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Grd1.Rows.Count > 0)
                Grd1.Rows[0].BackColor = Color.White;
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter1 = " ", Filter2 = " ", Filter3 = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Param", mItemCtCodeForMonthlyOutstadingDoct3);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter1, ref cm, TxtItem.Text, new string[] { "X.ItCode", "X.ItName" });
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueVendor), "X.VdCode", true);
                Sm.FilterStr(ref Filter3, ref cm, Sm.GetLue(LueItCtCode), "X.ItCtCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                GetSQL(Filter1, Filter2, Filter3),
                new string[]
                    {
                        //0
                        "ItName",

                        //1-5
                        "Qty",
                        "PurchaseUomCode", 
                        "VdName",
                        "RecvVdQty",
                        "InventoryUomCode",

                        //6-10
                        "RecvVdQty2",
                        "InventoryUomCode2",
                        "Balance",
                        "CurCode",
                        "UPrice",

                        //11
                        "ItCtName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void PrintData()
        {
            try
            {
                if (Grd1.Rows.Count == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }

                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Additional Method

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<Header>();
            var ldtl = new List<Detail>();

            string[] TableName = { "Header", "Detail" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            l.Add(new Header()
            {
                CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                CompanyName = Gv.CompanyName,
            });
            myLists.Add(l);

            #endregion

            #region Detail

            int nomor = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                nomor = nomor + 1;
                ldtl.Add(new Detail()
                {
                    No = nomor,
                    ItName = Sm.GetGrdStr(Grd1, i, 1),
                    Qty = Sm.GetGrdDec(Grd1, i, 2),
                    UoM = Sm.GetGrdStr(Grd1, i, 3),
                    VdName = Sm.GetGrdStr(Grd1, i, 4),
                    Qty1 = Sm.GetGrdDec(Grd1, i, 5),
                    InventoryUOMCode = Sm.GetGrdStr(Grd1, i, 6),
                    Qty2 = Sm.GetGrdDec(Grd1, i, 7),
                    InventoryUOMCode2 = Sm.GetGrdStr(Grd1, i, 8),
                    OutstandingQty = Sm.GetGrdDec(Grd1, i, 9),
                    CurCode = Sm.GetGrdStr(Grd1, i, 10),
                    total = Sm.GetGrdDec(Grd1, i, 11),
                });
            }
            myLists.Add(ldtl);

            #endregion

            Sm.PrintReport("OutstandingRSOKSM", myLists, TableName, false);

        }

        private void GetParameter()
        {
            mItemCtCodeForMonthlyOutstadingDoct3 = Sm.GetParameter("ItemCtCodeForMonthlyOutstadingDoct3");
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = ColWidth;
        }

        #endregion

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVendor_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVendor, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVendor_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItem_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeActive), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

        #region Class

        private class Header
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
        }

        private class Detail
        {
            public int No { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string UoM { get; set; }
            public string VdName { get; set; }
            public decimal Qty1 { get; set; }
            public string InventoryUOMCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUOMCode2 { get; set; }
            public decimal OutstandingQty { get; set; }
            public string CurCode { get; set; }
            public decimal total { get; set; }
        }
        #endregion
    }
}
