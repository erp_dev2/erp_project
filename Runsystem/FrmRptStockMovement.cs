﻿#region Update
/*
    08/04/2018 [TKG] tambah remark
    21/11/2018 [WED] tambah created by
    23/03/2020 [TKG/IMS] berdasarkan parameter IsInvTrnShowItSpec, menampilkan specifikasi item
    30/04/2020 [IBL/IMS] Penambahan field Nama Project dan Nomer PO Customer berdasarkan parameter IsRptStockMovementShowProjectInfo
    28/12/2020 [VIN/SRN] mengganti label kolom, qty --> qty1, uom --> uom1
    11/03/2021 [TKG/IMS] menambah heat number
    23/07/2021 [TRI/IMS] menambah parameter RptStockMovementDocNoSource, default = 1, jika 2 maka  menggunakan nomor dokumen DO TO OTH WHS PROJECT
    29/07/2021 [ICA/IMS] menambah filter multi warehouse
    02/11/2021 [DITA/IMS] masih ada bug,  ketika menampilkan data receiving yg sudah ada di stock movement juga menampilkan data receiving dari do sehingga data kedobel
    17/11/2021 [TRI/IMS] bug ketika refresh data doctype : DO to warehouse (Transfer Request)
    23/11/2021 [TRI/IMS] BUG ketika RptStockMovementDocNoSource = 2 harusnya ambil docno dari DO to Other Warehouse for Project
    25/11/2021 [TKG/IMS] untuk data yg diambil dari DOWhs4 (type 26), menggunakan function position untuk mencek posisi ':' , sample : 'abcde:12345'
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
using System.Text.RegularExpressions;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockMovement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string mRptStockMovementDocNoSource = string.Empty;
        private int 
            mNumberOfInventoryUomCode = 1;
        private bool 
            mIsInventoryShowTotalQty = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsInvTrnShowItSpec = false,
            mIsRptStockMovementShowProjectInfo = false,
            mIsStockMovementHeatNumberEnabled = false;
        
            
        #endregion

        #region Constructor

        public FrmRptStockMovement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCatCode, mIsFilterByItCt ? "Y" : "N");
                if (mIsInventoryRptFilterByGrpWhs)
                    Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                else
                    Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueTransType(ref LueDocType);
                SetCcbWhsName(ref CcbWhsName, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
            mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsInventoryRptFilterByGrpWhs = Sm.GetParameterBoo("IsInventoryRptFilterByGrpWhs");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            mIsRptStockMovementShowProjectInfo = Sm.GetParameterBoo("IsRptStockMovementShowProjectInfo");
            mIsStockMovementHeatNumberEnabled = Sm.GetParameterBoo("IsStockMovementHeatNumberEnabled");
            mRptStockMovementDocNoSource = Sm.GetParameter("RptStockMovementDocNoSource");
            if (mRptStockMovementDocNoSource.Length == 0) mRptStockMovementDocNoSource = "1"; 
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type",   
                        "Document#",
                        "Date",
                        "Warehouse",
                        "Lot",

                        //6-10
                        "Bin",
                        "Item's Code",
                        "Item's Name",
                        "Foreign Name",
                        "Property",
                        
                        //11-15
                        "Batch#",
                        "Source",
                        "Quantity 1", 
                        "UoM 1",                     
                        "Quantity 2", 
                        
                        //16-20
                        "UoM 2",
                        "Quantity 3", 
                        "UoM 3",
                        "Item Group"+Environment.NewLine+"Code",
                        "Item Group"+Environment.NewLine+"Name",

                        //21-25
                        "Sub-Category",
                        "Remark",
                        "Created By",
                        "Specification",
                        "Project Name",

                        //26-27
                        "Customer's PO#",
                        "Heat Number"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        250, 150, 80, 200, 60, 
                        
                        //6-10
                        60, 80, 250, 230, 80,   
                        
                        //11-15
                        200, 180, 120, 100, 120,

                        //16-20
                        100, 120, 100, 100, 150,
                        
                        //21-25
                        150, 300, 200, 200, 200,

                        //26-27
                        130, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 23 });
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 10, 12, 15, 16, 17, 18, 21 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9, 10, 12, 15, 16, 17, 18, 21 }, false);
            if (mIsItGrpCode) Sm.GrdColInvisible(Grd1, new int[] { 19, 20 }, false);
            Grd1.Cols[19].Move(10);
            Grd1.Cols[20].Move(11);
            Grd1.Cols[21].Move(12);
            if (mIsInvTrnShowItSpec)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 24 }, mIsInvTrnShowItSpec);
                Grd1.Cols[24].Move(9);
            }
            if (!mIsRptStockMovementShowProjectInfo) Sm.GrdColInvisible(Grd1, new int[] { 25, 26 });
            if (mIsStockMovementHeatNumberEnabled)
                Grd1.Cols[27].Move(17);
            else
                Grd1.Cols[27].Visible = false;
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 10, 12, 21, 23 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (ChkWhsName.Checked && ChkWhsCode.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "Please just use Multi Warehouse or Warehouse filter, instead of using both of them.");
                    CcbWhsName.Focus();
                    return;
                }

                var lsm = PrepareDataStockMovement();
                var li = PrepareDataItem();
                var lw = PrepareDataWarehouse();
                var lo = PrepareDataOption();
                var lp = PrepareDataProperty();

                if (lsm.Count > 0)
                {
                    var Rpt = (from A in lsm
                               join B in li on A.ItCode equals B.ItCode
                               join C in lw on A.WhsCode equals C.WhsCode
                               join D in lo on A.DocType equals D.OptCode
                               join E in lp on A.PropCode equals E.PropCode
                               select new { A, B, C, D, E }
                    ).Select(result =>
                    {
                        result.A.ItName = result.B.ItName;
                        result.A.ForeignName = result.B.ForeignName;
                        result.A.InventoryUomCode = result.B.InventoryUomCode;
                        result.A.InventoryUomCode2 = result.B.InventoryUomCode2;
                        result.A.InventoryUomCode3 = result.B.InventoryUomCode3;
                        result.A.WhsName = result.C.WhsName;
                        result.A.OptDesc = result.D.OptDesc;
                        result.A.PropName = result.E.PropName;
                        result.A.ItGrpCode = result.B.ItGrpCode;
                        result.A.ItGrpName = result.B.ItGrpName;
                        result.A.ItScName = result.B.ItScName;
                        result.A.Specification = result.B.Specification;
                        return result;
                    }).ToList();


                    li.Clear();
                    lw.Clear();
                    lo.Clear();
                    lp.Clear();

                    int Row = 0;
                    Grd1.BeginUpdate();
                    Grd1.Rows.Count = 0;
                    foreach (StockMovement i in lsm.OrderBy(x => x.DocDt).ThenBy(x => x.DocNo))
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = i.OptDesc;
                        Grd1.Cells[Row, 2].Value = i.DocNo;
                        Grd1.Cells[Row, 3].Value = Sm.ConvertDate(i.DocDt);
                        Grd1.Cells[Row, 4].Value = i.WhsName;
                        Grd1.Cells[Row, 5].Value = i.Lot;
                        Grd1.Cells[Row, 6].Value = i.Bin;
                        Grd1.Cells[Row, 7].Value = i.ItCode;
                        Grd1.Cells[Row, 8].Value = i.ItName;
                        Grd1.Cells[Row, 9].Value = i.ForeignName;
                        Grd1.Cells[Row, 10].Value = i.PropName;
                        Grd1.Cells[Row, 11].Value = i.BatchNo;
                        Grd1.Cells[Row, 12].Value = i.Source;
                        Grd1.Cells[Row, 13].Value = i.Qty;
                        Grd1.Cells[Row, 14].Value = i.InventoryUomCode;
                        Grd1.Cells[Row, 15].Value = i.Qty2;
                        Grd1.Cells[Row, 16].Value = i.InventoryUomCode2;
                        Grd1.Cells[Row, 17].Value = i.Qty3;
                        Grd1.Cells[Row, 18].Value = i.InventoryUomCode3;
                        Grd1.Cells[Row, 19].Value = i.ItGrpCode;
                        Grd1.Cells[Row, 20].Value = i.ItGrpName;
                        Grd1.Cells[Row, 21].Value = i.ItScName;
                        Grd1.Cells[Row, 22].Value = i.Remark;
                        Grd1.Cells[Row, 23].Value = i.CreatedBy;
                        Grd1.Cells[Row, 24].Value = i.Specification;
                        Grd1.Cells[Row, 25].Value = i.ProjectName;
                        Grd1.Cells[Row, 26].Value = i.PONo;
                        Grd1.Cells[Row, 27].Value = i.HeatNumber;
                        Row++;
                    }
                    if (ChkShowTotal.Checked)
                    {
                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 15, 17 });
                    }
                    Grd1.EndUpdate();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                
                lsm.Clear();
                li.Clear();
                lw.Clear();
                lo.Clear();
                lp.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private List<StockMovement> PrepareDataStockMovement()
        {
            string
                Filter = " ",
                DocTypeTemp = string.Empty, DocNoTemp = string.Empty, DNoTemp = string.Empty;
            decimal QtyTemp = 0m, Qty2Temp = 0m, Qty3Temp = 0m;
            bool IsCancelled = false, IsFilteredByItCtCode = Sm.GetLue(LueItCatCode).Length>0;
            
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            
            if (ChkItCatCode.Checked) Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCatCode));
            if (ChkItName.Checked) Sm.CmParam<String>(ref cm, "@ItName", "%" + TxtItName.Text + "%");
            //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "WhsCode", true);
            Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, "ItCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "T.DocType", true);
            Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "BatchNo", false);
            Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "Lot", false);
            Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "Bin", false);
            if (ChkWhsName.Checked)
                FilterStr(ref Filter, ref cm, ProcessCcb(Sm.GetCcb(CcbWhsName)), "WhsCode", "_2");
            else
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "WhsCode", true);


            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CancelInd, T.DocType, ");
            if (mRptStockMovementDocNoSource == "2")
            {
                SQL.AppendLine("case when t.doctype In ('26', '27') then T4.docno  ");
                SQL.AppendLine("ELSE t.docno END DocNo, ");
            }
            else
            SQL.AppendLine("T.DocNo, ");
            SQL.AppendLine("T.DNo, T.Qty, T.Qty2, T.Qty3, T.DocDt, ");
            SQL.AppendLine("T.WhsCode, T.Lot, T.Bin, T.ItCode, T.PropCode, T.BatchNo, T.Source, T.Remark, T.CreateBy, ");
            if (mIsRptStockMovementShowProjectInfo)
                SQL.AppendLine("T2.ProjectName, T3.Value1 as PONo ");
            else
                SQL.AppendLine("Null As ProjectName, Null As PONo ");
            if (mIsStockMovementHeatNumberEnabled)
                SQL.AppendLine(", T3.Value2 as HeatNumber ");
            else
                SQL.AppendLine(", Null As HeatNumber ");
            SQL.AppendLine("From TblStockMovement T ");

            //whs project
            if (mRptStockMovementDocNoSource == "2")
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct X1.DocType, X3.DocNo, X2.DocNo StockDocNo, X2.DNo StockDNo ");
                SQL.AppendLine("    From TblStockMovement X1 ");
                SQL.AppendLine("    Inner Join TblDOWhsDtl2 X2 On X2.DNo = X1.DNo ");
                SQL.AppendLine("        And X1.DocType = '26' ");
                SQL.AppendLine("        And Case When Position(':' In X1.DocNo)<1 Then X1.DocNo Else Left(X1.DocNo, Position(':' In X1.DocNo)-1) End= X2.DocNo ");
                SQL.AppendLine("    Inner Join TblDOWhs4Dtl X3 On X2.DocNo = X3.DOWhsDocNo And X2.DNo = X3.DOWhsDNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("     Select Distinct X1.DocType, X3.DocNo, X2.DocNo StockDocNo, X2.DNo StockDNo ");
                SQL.AppendLine("    From TblStockMovement X1 ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl X2 On X1.DocNo = X2.DocNo And X1.DNo = X2.DNo ");
                SQL.AppendLine("        And X1.DocType = '27' ");
                SQL.AppendLine("        And X1.CancelInd = X2.CancelInd ");
                SQL.AppendLine("    Inner Join TblDOWhs4Dtl X3 On X2.DOWhsDocNo = X3.DOWhsDocNo And X2.DOWhsDNo = X3.DOWhsDNo ");
                SQL.AppendLine(" ) T4 On T.DocType = T4.DocType And Case When Position(':' In T.DocNo)<1 Then T.DocNo Else Left(T.DocNo, Position(':' In T.DocNo)-1) End= T4.StockDocNo And T.DNo = T4.StockDNo ");
            }
            if (mIsRptStockMovementShowProjectInfo)
                SQL.AppendLine("Left Join TblProjectGroup T2 On T.BatchNo = T2.ProjectCode ");
            if (mIsRptStockMovementShowProjectInfo || mIsStockMovementHeatNumberEnabled)
                SQL.AppendLine("Left Join TblSourceInfo T3 On T.Source = T3.Source");
            SQL.AppendLine("Where (T.DocDt>=@DocDt1 And T.DocDt<=@DocDt2) ");
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=T.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByItCt || ChkItCatCode.Checked || ChkItName.Checked)
            {
                SQL.AppendLine("And Exists(Select 1 From TblItem Tbl Where Tbl.ItCode=T.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=Tbl.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                if (ChkItCatCode.Checked) SQL.AppendLine("And Tbl.ItCtCode=@ItCtCode ");
                if (ChkItName.Checked) SQL.AppendLine("And Tbl.ItName Like @ItName ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By T.DocType, T.DocNo, T.DNo, T.CancelInd Desc;");

            var l = new List<StockMovement>();

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[]
                {
                    //0
                    "CancelInd", 

                    //1-5
                    "DocType", "DocNo", "DNo", "Qty", "Qty2", 

                    //6-10
                    "Qty3", "DocDt", "WhsCode", "Lot", "Bin", 

                    //11-15
                    "ItCode", "PropCode", "BatchNo", "Source", "Remark",

                    //16-19
                    "CreateBy", "ProjectName", "PONo", "HeatNumber"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   if (Sm.DrStr(dr, 0) == "Y")
                   {
                       IsCancelled = true;
                       DocTypeTemp = Sm.DrStr(dr, 1);
                       DocNoTemp = Sm.DrStr(dr, 2);
                       DNoTemp = Sm.DrStr(dr, 3);
                   }
                   else
                   {
                       QtyTemp = Sm.DrDec(dr, 4);
                       Qty2Temp = Sm.DrDec(dr, 5);
                       Qty3Temp = Sm.DrDec(dr, 6);
                       if (
                               (
                                   !IsCancelled ||
                                   (
                                       IsCancelled &&
                                           !(
                                               Sm.CompareStr(DocTypeTemp, Sm.DrStr(dr, 1)) &&
                                               Sm.CompareStr(DocNoTemp, Sm.DrStr(dr, 2)) &&
                                               Sm.CompareStr(DNoTemp, Sm.DrStr(dr, 3))
                                           )
                                   )
                               )
                               &&
                               (QtyTemp != 0 || Qty2Temp != 0 || Qty3Temp != 0)
                           )
                       {
                           DocTypeTemp = Sm.DrStr(dr, 1);
                           DocNoTemp = Sm.DrStr(dr, 2);
                           DNoTemp = Sm.DrStr(dr, 3);
                           
                           l.Add(new StockMovement()
                           {
                                DocType = DocTypeTemp,
                                DocNo = DocNoTemp,
                                DNo = DNoTemp,
                                DocDt=Sm.DrStr(dr, 7),
                                WhsCode=Sm.DrStr(dr, 8),
                                Lot=Sm.DrStr(dr, 9),
                                Bin=Sm.DrStr(dr, 10),
                                ItCode=Sm.DrStr(dr, 11),
                                PropCode=Sm.DrStr(dr, 12),
                                BatchNo=Sm.DrStr(dr, 13),
                                Source=Sm.DrStr(dr, 14),
                                Qty = QtyTemp,
                                Qty2 = Qty2Temp,
                                Qty3 = Qty3Temp,
                                Remark = Sm.DrStr(dr, 15),
                                CreatedBy = Sm.DrStr(dr, 16),
                                ProjectName = Sm.DrStr(dr, 17),
                                PONo = Sm.DrStr(dr, 18),
                                HeatNumber = Sm.DrStr(dr, 19)
                           });
                       }
                       DocTypeTemp = string.Empty;
                       DocNoTemp = string.Empty;
                       DNoTemp = string.Empty;
                       QtyTemp = 0m;
                       Qty2Temp = 0m;
                       Qty3Temp = 0m;
                       IsCancelled = false;
                   }
               }, false
            );
            return l;
        }

        private List<Warehouse> PrepareDataWarehouse()
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            var SQL = new StringBuilder();
            var l = new List<Warehouse>();

            SQL.AppendLine("Select T.WhsCode, T.WhsName From TblWarehouse T ");
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    Where Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=T.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(";");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(), new string[]{ "WhsCode", "WhsName" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Warehouse()
                        {
                            WhsCode = Sm.DrStr(dr, 0),
                            WhsName = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Property> PrepareDataProperty()
        {
            var cm = new MySqlCommand();
            var l = new List<Property>();
            Sm.ShowDataInCtrl(
                ref cm,
                "Select PropCode, PropName From TblProperty Union All Select '-' As PropCode, '-' As PropName;",
                new string[] { "PropCode", "PropName" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Property()
                        {
                            PropCode = Sm.DrStr(dr, 0),
                            PropName = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Option> PrepareDataOption()
        {
            var cm = new MySqlCommand();
            var l = new List<Option>();
            Sm.ShowDataInCtrl(
                ref cm,
                "Select OptCode, OptDesc From TblOption Where OptCat='InventoryTransType';",
                new string[] { "OptCode", "OptDesc" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Option()
                        {
                            OptCode = Sm.DrStr(dr, 0),
                            OptDesc = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Item> PrepareDataItem()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<Item>();
            string Filter = string.Empty;

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkItCatCode.Checked) Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCatCode));
            if (ChkItName.Checked) Sm.CmParam<String>(ref cm, "@ItName", "%" + TxtItName.Text + "%");

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ForeignName, A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3, A.ItGrpCode, B.ItGrpName, C.ItScName, A.Specification ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemGroup B On A.ItGrpCode = B.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubCategory C On A.ItScCode = C.ItScCode ");
            SQL.AppendLine("Where 0=0 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (ChkItCatCode.Checked || ChkItName.Checked)
            {
                if (ChkItCatCode.Checked) SQL.AppendLine("And A.ItCtCode=@ItCtCode ");
                if (ChkItName.Checked) SQL.AppendLine("And A.ItName Like @ItName ");
            }

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "ItCode", 
                    "ItName", "ForeignName", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3", 
                    "ItGrpCode", "ItGrpName", "ItScName", "Specification" 
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Item()
                        {
                            ItCode = Sm.DrStr(dr, 0),
                            ItName = Sm.DrStr(dr, 1),
                            ForeignName = Sm.DrStr(dr, 2),
                            InventoryUomCode = Sm.DrStr(dr, 3),
                            InventoryUomCode2 = Sm.DrStr(dr, 4),
                            InventoryUomCode3 = Sm.DrStr(dr, 5),
                            ItGrpCode = Sm.DrStr(dr, 6),
                            ItGrpName = Sm.DrStr(dr, 7),
                            ItScName = Sm.DrStr(dr, 8),
                            Specification = Sm.DrStr(dr, 9),
                        }
                    );
               }, false
            );
            return l;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetCcbWhsName(ref CheckedComboBoxEdit Ccb, string WhsName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.WhsName As Col From TblWarehouse T ");
            SQL.AppendLine("Where 0 = 0 ");
            if (WhsName.Length > 0)
            {
                SQL.AppendLine("And T.WhsName = @WhsName ");
            }
            else
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.WhsName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsName", WhsName);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetCcb(ref Ccb, cm);
        }

        private string ProcessCcb(string Value)
        {
            if (Value.Length != 0)
            {
                Value = GetWhsCode(Value);
                Value = "#" + Value.Replace(", ", "# #") + "#";
                Value = Value.Replace("#", @"""");
            }
            return Value;
        }

        private string GetWhsCode(string Value)
        {
            if (Value.Length != 0)
            {
                Value = Value.Replace(", ", ",");

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(T.WhsCode Separator ', ') WhsCode ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select WhsCode ");
                SQL.AppendLine("    From TblWarehouse ");
                SQL.AppendLine("    Where Find_In_Set(WhsName, @Param) ");
                SQL.AppendLine(")T; ");

                Value = Sm.GetValue(SQL.ToString(), Value);
            }

            return Value;
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Param + Index.ToString();
                        //Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsInventoryRptFilterByGrpWhs)
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueTransType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item name");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void CcbWhsName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkWhsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Warehouse");
        }

        #endregion

        #region Class

        private class StockMovement
        {
            public string DocType { get; set; }
            public string OptDesc { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string WhsName { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string PropCode { get; set; }
            public string PropName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string ItGrpCode { get; set; }
            public string ItGrpName { get; set; }
            public string ItScName { get; set; }
            public string Remark { get; set; }
            public string CreatedBy { get; set; }
            public string Specification { get; set; }
            public string ProjectName { get; set; }
            public string PONo { get; set; }
            public string HeatNumber { get; set; }
        }

        private class Warehouse
        {
            public string WhsCode { get; set; }
            public string WhsName { get; set; }
        }

        private class Item
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string ItGrpCode { get; set; }
            public string ItGrpName { get; set; }
            public string ItScName { get; set; }
            public string Specification { get; set; }

        }

        private class Option
        {
            public string OptCode { get; set; }
            public string OptDesc { get; set; }
        }

        private class Property
        {
            public string PropCode { get; set; }
            public string PropName { get; set; }
        }

        #endregion
    }
}
