﻿#region Update
/*
    22/12/2017 [TKG] Filter department berdasarkan group
    28/12/2017 [HAR] tambah informasi currency dan estimated price dari MR
    17/01/2018 [TKG] saat save langsung tampilkan data yg telah disimpan.
    18/01/2018 [ARI] Tambah printout KIM
    24/01/2018 [TKG] tambah otorisasi group thd term of payment
    02/09/2018 [TKG] Menampilkan nomor urut berdasarkan parameter
    30/01/2019 [TKG] tambah property berdasarkan parameter PORequestPropCodeEnabled
    01/02/2019 [TKG] bug print out (status approvel cancel masih muncul)
    30/05/2019 [WED] ada Additional Information, berdasarkan parameter IsPORequestUseAdditionalInformation
    10/06/2019 [WED] parameter baru FormPrintOutPOR
    11/06/2019 [MEY] parameter baru GrpCodeForBOD
    11/06/2019 [MEY] layout baru untuk Print out YK
    25/07/2019 [TKG] adjustment ke MR Dropping Request YK
    04/11/2019 [DITA/IMS] tambah informasi Specifications
    05/11/2019 [DITA/IMS] tambah filter berdasarkan Item Category
    06/11/2019 [DITA/YK] tambah validasi nilai PO Request tidak boleh lebih dari dropping request
    13/12/2019 [WED/YK] Vendor harus sama dengan vendor di Dropping Request, berdasarkan parameter IsVendorPORequestEqualToDroppingRequest
    20/12/2019 [WED/YK] Vendor yg divalidasi, hanya untuk yg item nya Group tertentu, berdasarkan parameter ItGrpCodeToValidatePORequestDroppingRequest
    17/02/2020 [TKG/IMS] tambah project code, project name
    21/04/2020 [TKG/IMS] berdasarkan IsPORequestApprovalSettingValidationDisable, validasi pengecekan approval setting dinonaktifkan
    20/09/2020 [TKG/IMS] Berdasarkan parameter IsDocNoFormatUseFullYear, dokumen# menampilkan tahun secara lengkap (Misal : 2020).
    29/09/2020 [ICA/IMS] Menampilkan kolom PO Request's Estimate Price based on IsPORequestUseEstPrice
    07/10/2020 [TKG/IMS] GenerateDocNo reset nomor urut per tahun
    12/10/2020 [ICA/TWC] BUG saat save
    23/10/2020 [TKG/IMS] extimated price kalau kosong diisi 0.00
    27/10/2020 [ICA/IMS] mengubah Material Request menjadi Purchase Request
    03/12/2020 [IBL/SIER] menambah parameter PORequestMRItemOrderFormat untuk mengubah format order by query pada dialog list of Material Request
    09/12/2020 [IBL/SIER] parameter baru IsMRInPORShowApprovalInfo untuk menambah 3 kolom approval info pada List of Material Request
    11/02/2021 [TKG/SIER] tambah validasi approval endamt
    14/04/2021 [IBL/KIM] MR ambil yg TenderInd = Y, berdasarkan parameter MenuCodeForPORTender
    19/04/2021 [VIN/KIM] Printout: signature untuk approval POR Tender
    12/07/2021 [WED/PADI] parameter baru IsUseECatalog
    10/08/2021 [WED/PADI] validasi kalau qty yang diinput di PORequest tidak boleh melebihi qty capability milik vendor berdasarkan parameter IsUseECatalog
    09/09/2021 [WED/PADI] validasi qty terhadap qty capability, berdasarkan parameter IsUseECatalog
    29/09/2021 [ARI/PHT] Menambahkan com box procurement type di menu PO Request
    29/09/2021 [ARI/PHT] Parameter baru IsMRUseProcurementType untuk magerin informasi procurement type
    28/10/2021 [WED/RM] tambah input Expired Date berdasarkan parameter IsUseECatalog
    04/11/2021 [TRI/SIER] tambah group concat vendor di printout header, ketika vendor lebih dari 1
    12/11/2021 [NJP/RM] Menambahkan parameter IsQtyCapabilityEnabled untuk mengaktifkan/menonaktifkan Validasi quantity dg syarat param IsUseECatalog juka aktif
    12/11/2021 [NJP/RM] Menampilan Nilai Quotation Awal
    18/11/2021 [ICA/PHT] Menambah parameter IsPORQuotationOnlyShowData
    25/11/2021 [ICA/PHT] Menambahkan upload file bersifat mandatory berdasarkan parameter IsPORequestFileMandatory
    27/12/2021 [TYO/SIER] Set parameter IsPORShowDivisionInfo
    28/12/2021 [ISD/SIER] unhide status dan material request#, tambah kolom division berdasarkan param IsPORShowDivisionInfo 
    29/12/2021 [RIS/SIER] Membuat Printout SIER
    30/12/2021 [YOG/SIER] Penambahan field Copy Data pada Header PO Request
    06/01/2022 [TKG/GSS] ubah GetParameter dan proses save
    10/01/2022 [RIS/SIER] Menambah source img tanda tangan
    17/02/2022 [RIS/KIM] BUG : Source Printout
    20/04/2022 [WED/PHT] tambah parameter baru IsPORequestEstPriceMandatory setelah parameter IsPORequestUseEstPrice, agar kolom POR Estimated Price tidak boleh 0
    20/05/2022 [WED/PHT] BUG saat IsUseECatalog aktif, TakeOrderInd di PORequestDtl di update menjadi auto Y ketika di MR nya tidak dicentang RUNMarketInd nya
    31/08/2022 [VIN/KIM] BUG : Source Printout-distinct
    19/09/2022 [BRI/SIER] BUG : Source logo
    24/10/2022 [BRI/HEX] field Date & remark tidak editable ketika edit
    27/01/2023 [RDA/MNET] penyesuaian printout POR untuk MNET
    16/02/2023 [DITA/SIER] based on param IsUploadFileRenamed, upload file dapat di rename file nya --> ngerubah ambil method upload file dari stdmtd
    21/02/2023 [IBL/HEX] Approval berdasarkan item category. Berdasarkan parameter IsDocApprovalSettingUseItemCt
    01/03/2023 [MAU/HEX] menampilkan kolom MR Type berdasarkan parameter IsMRUseApprovalSheet
    10/03/2023 [MAU/HEX] BUG Sumber MRType
    17/03/2023 [RDA/MNET] button download pakai std mtd baru
    11/04/2023 [ISN/SIER] menambahkan parameter IsVendorQuotationValidatedByVendorActInd untuk filter vendor
*/
#endregion

#region Namespace

using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO; //Path
using System.Linq;
using System.Net;
using System.Reflection; // Assembly
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sl = RunSystem.SetLue;
using Sm = RunSystem.StdMtd;
#endregion

namespace RunSystem
{
    public partial class FrmPORequest : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mSiteCode = string.Empty,
            mHeaderDate = string.Empty,
            mDetailDate = string.Empty,
            compare1 = string.Empty,
            compare2 = string.Empty,
            Doctitle = Sm.GetParameter("Doctitle"),
            mPORequestMRItemOrderFormat = string.Empty,
            mECatalogWebURI = string.Empty
            ;
        private string 
            mFormPrintOutPOR = string.Empty, 
            mGrpCodeForBOD = string.Empty,
            mItGrpCodeToValidatePORequestDroppingRequest = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty
            ;
        internal FrmPORequestFind FrmFind;
        private bool
            mIsRemarkForApprovalMandatory = false,
            mIsCancelledDocumentCopyToRemark = false,
            mIsShowSeqNoInPORequest = false,
            mIsPORequestUseAdditionalInformation = false,
            mIsVendorPORequestEqualToDroppingRequest = false,
            mIsPORequestApprovalSettingValidationDisabled = false,
            mIsPORequestUseEstPrice = false,
            mIsPORequestFileMandatory = false,
            mIsPORequestEstPriceMandatory = false,
            mIsDocApprovalSettingUseItemCt = false
            ;
        internal bool
            mIsMRUseProcurementType  = false,
            mIsAutoGeneratePurchaseLocalDocNo = false,
            mIsShowForeignName = false,
            mIsSiteMandatory = false,
            mIsFilterBySite = false,
            mIsPORequestFilterByDept = false,
            mIsComparedToDetailDate = false,
            mIsMRShowEstimatedPrice = false,
            mIsGroupPaymentTermActived = false,
            mPORequestPropCodeEnabled = false,
            mIsMaterialRequestForDroppingRequestEnabled = false,
            mIsBOMShowSpecifications = false,
            mIsFilterByItCt = false,
            mIsAmtValidatedByDroppingRequest = false,
            mIsMRInPORShowApprovalInfo = false,
            mIsMRWithTenderEnabled = false,
            mMenuCodeForPORTender = false,
            mIsUseECatalog = false,
            IsQtyCapabilityEnabled = false,
            mIsPORQuotationOnlyShowData = false,
            mIsPORShowDivisionInfo = false,
            mIsPORAllowToCopyCancelledData = false,
            mIsUploadFileRenamed = false,
            mIsMRUseApprovalSheet = false,
            mIsVendorQuotationValidatedByVendorActInd = false
            ;
           
        private List<LocalDocument> mlLocalDocument = null;
        internal List<PORYKHdr> lPOR2Hdr = null;
        internal List<PORYKDtl> lPOR2Dtl = null;
        iGCell fCell;
        bool fAccept;

        public string optcodeselected = string.Empty;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmPORequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "PO Request";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (!mIsPORequestUseAdditionalInformation) LblAdditionalInformation.Visible = false;
                LblExpDt.Visible = DteExpDt.Visible = mIsUseECatalog;
                SetGrd();
                SetFormControl(mState.View);
                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                Sl.SetLuePropCode(ref LuePropCode);
                LuePropCode.Visible = false;
                base.FrmLoad(sender, e);
                Sl.SetLueOption(ref LueProcType, "ProcurementType");

                label3.Visible = LueProcType.Visible = mIsMRUseProcurementType;

                if (mIsPORequestFileMandatory) LblFile.ForeColor = Color.Red;


                //if this application is called from other application

                mlLocalDocument = new List<LocalDocument>();

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if(!mIsPORAllowToCopyCancelledData)
                {
                    TxtCopyData.Visible = false;
                    BtnCopyData.Visible = false;
                    label4.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'ProcFormatDocNo', 'MenuCodeForPORTender', 'PORequestMRItemOrderFormat', 'FormPrintOutPOR', 'GrpCodeForBOD', ");
            SQL.AppendLine("'ItGrpCodeToValidatePORequestDroppingRequest', 'ECatalogWebURI', 'FileSizeMaxUploadFTPClient', 'IsMRUseProcurementType', 'IsPORequestRemarkForApprovalMandatory', ");
            SQL.AppendLine("'HostAddrForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', 'SharedFolderForFTPClient', ");
            SQL.AppendLine("'IsPORequestUseEstPrice', 'IsPORequestApprovalSettingValidationDisabled', 'IsPORAllowToCopyCancelledData', 'IsQtyCapabilityEnabled', 'IsUseECatalog', ");
            SQL.AppendLine("'IsPORQuotationOnlyShowData', 'IsPORequestFileMandatory', 'IsPORShowDivisionInfo', 'IsFilterBySite', 'IsMRWithTenderEnabled', ");
            SQL.AppendLine("'IsShowForeignName', 'IsSiteMandatory', 'IsFilterByItCt', 'IsVendorPORequestEqualToDroppingRequest', 'IsMRInPORShowApprovalInfo', ");
            SQL.AppendLine("'IsAutoGeneratePurchaseLocalDocNo', 'IsPORequestUseAdditionalInformation', 'IsBOMShowSpecifications', 'IsMaterialRequestForDroppingRequestEnabled', 'IsAmtValidatedByDroppingRequest', ");
            SQL.AppendLine("'IsCancelledDocumentCopyToRemark', 'IsMRShowEstimatedPrice', 'IsGroupPaymentTermActived', 'IsShowSeqNoInPORequest', 'PORequestPropCodeEnabled', ");
            SQL.AppendLine("'IsComparedToDetailDate', 'IsPORequestFilterByDept', 'IsPORequestEstPriceMandatory', 'FormatFTPClient', 'IsUploadFileRenamed','IsDocApprovalSettingUseItemCt', ");
            SQL.AppendLine("'IsMRUseApprovalSheet', 'IsVendorQuotationValidatedByVendorActInd' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsPORequestFilterByDept": mIsPORequestFilterByDept = ParValue == "Y"; break;
                            case "IsComparedToDetailDate": mIsComparedToDetailDate = ParValue == "Y"; break;
                            case "IsCancelledDocumentCopyToRemark": mIsCancelledDocumentCopyToRemark = ParValue == "Y"; break;
                            case "IsMRShowEstimatedPrice": mIsMRShowEstimatedPrice = ParValue == "Y"; break;
                            case "IsGroupPaymentTermActived": mIsGroupPaymentTermActived = ParValue == "Y"; break;
                            case "IsShowSeqNoInPORequest": mIsShowSeqNoInPORequest = ParValue == "Y"; break;
                            case "PORequestPropCodeEnabled": mPORequestPropCodeEnabled = ParValue == "Y"; break;
                            case "IsAutoGeneratePurchaseLocalDocNo": mIsAutoGeneratePurchaseLocalDocNo = ParValue == "Y"; break;
                            case "IsPORequestUseAdditionalInformation": mIsPORequestUseAdditionalInformation = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsMaterialRequestForDroppingRequestEnabled": mIsMaterialRequestForDroppingRequestEnabled = ParValue == "Y"; break;
                            case "IsAmtValidatedByDroppingRequest": mIsAmtValidatedByDroppingRequest = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsVendorPORequestEqualToDroppingRequest": mIsVendorPORequestEqualToDroppingRequest = ParValue == "Y"; break;
                            case "IsMRInPORShowApprovalInfo": mIsMRInPORShowApprovalInfo = ParValue == "Y"; break;
                            case "IsMRWithTenderEnabled": mIsMRWithTenderEnabled = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsPORShowDivisionInfo": mIsPORShowDivisionInfo = ParValue == "Y"; break;
                            case "IsPORequestFileMandatory": mIsPORequestFileMandatory = ParValue == "Y"; break;
                            case "IsPORQuotationOnlyShowData": mIsPORQuotationOnlyShowData = ParValue == "Y"; break;
                            case "IsUseECatalog": mIsUseECatalog = ParValue == "Y"; break;
                            case "IsQtyCapabilityEnabled": IsQtyCapabilityEnabled = ParValue == "Y"; break;
                            case "IsPORAllowToCopyCancelledData": mIsPORAllowToCopyCancelledData = ParValue == "Y"; break;
                            case "IsPORequestApprovalSettingValidationDisabled": mIsPORequestApprovalSettingValidationDisabled = ParValue == "Y"; break;
                            case "IsPORequestUseEstPrice": mIsPORequestUseEstPrice = ParValue == "Y"; break;
                            case "IsPORequestRemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsMRUseProcurementType": mIsMRUseProcurementType = ParValue == "Y"; break;
                            case "IsPORequestEstPriceMandatory": mIsPORequestEstPriceMandatory = ParValue == "Y"; break;
                            case "IsUploadFileRenamed": mIsUploadFileRenamed = ParValue == "Y"; break;
                            case "IsDocApprovalSettingUseItemCt": mIsDocApprovalSettingUseItemCt = ParValue == "Y"; break;
                            case "IsMRUseApprovalSheet": mIsMRUseApprovalSheet = ParValue == "Y"; break;
                            case "IsVendorQuotationValidatedByVendorActInd": mIsVendorQuotationValidatedByVendorActInd = ParValue == "Y"; break;

                            //string
                            case "ProcFormatDocNo": IsProcFormat = ParValue; break;
                            case "MenuCodeForPORTender": mMenuCodeForPORTender = mMenuCode== ParValue; break;
                            case "PORequestMRItemOrderFormat":
                                mPORequestMRItemOrderFormat = ParValue;
                                if (ParValue.Length == 0) mPORequestMRItemOrderFormat = "1";
                                break;
                            case "FormPrintOutPOR": mFormPrintOutPOR = ParValue; break;
                            case "GrpCodeForBOD": mGrpCodeForBOD = ParValue; break;
                            case "ItGrpCodeToValidatePORequestDroppingRequest": mItGrpCodeToValidatePORequestDroppingRequest = ParValue; break;
                            case "ECatalogWebURI": mECatalogWebURI = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 56;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",
                    
                    //1-5
                    "Cancel",
                    "Old Cancel",
                    "Status",
                    "",
                    "",

                    //6-10
                    "Material Request#",
                    "Requested"+Environment.NewLine+"D#",
                    "", 
                    "Requested"+Environment.NewLine+"Date",
                    "Department",
                    
                    //11-15
                    "Item's Code",
                    "",
                    "Item's Name",
                    "Quantity",
                    "UoM",
                    
                    //16-20
                    "Usage Date",
                    "Requested Remark",
                    "",
                    "Quotation#",
                    "Quotation"+Environment.NewLine+"D Number",
                    
                    //21-25
                    "",
                    "Quotation"+Environment.NewLine+"Date",
                    "Vendor",
                    "Currency",
                    "Actual"+Environment.NewLine+"Unit Price",
                    
                    //26-30
                    "Total",
                    "Term of" + Environment.NewLine + "Payment",
                    "Delivery Type",
                    "Remark",
                    "ItScCode",

                    //31-35
                    "Sub-Category",
                    "Local#",
                    "Site Code",
                    "Site",
                    "Foreign Name",

                    //36-40
                    "Local Code",
                    "Currency",
                    "Estimated"+Environment.NewLine+"Price",
                    "No.",
                    "Property Code",

                    //41-45
                    "Property",
                    "#",
                    "Specification",
                    "Dropping Request's"+Environment.NewLine+"Amount",
                    "DroppingRequestDocNo",

                    //46-50
                    "VdCode",
                    "VdCode2",
                    "ItGrpCode",
                    "Project's Code",
                    "Project's Name",

                    //51-55
                    "PO Request's"+Environment.NewLine+"Estimated Price",
                    "Quantity"+Environment.NewLine+"Capability",
                    "Quotation Price",
                    "Division",
                    "MR Type"
                },
                 new int[] 
                {
                    //0
                    20, 

                    //1-5
                    50, 80, 130, 20, 20, 
                    
                    //6-10
                    150, 80, 20, 80, 130, 
                    
                    //11-15
                    80, 20, 200, 80, 80, 
                    
                    //16-20
                    80, 200, 20, 130, 80, 
                    
                    //21-25
                    20, 80, 200, 70, 80, 

                    //26-30
                    100, 150, 150, 300, 80, 
                    
                    //31-35
                    150, 180, 0, 200, 230,

                    //36-40
                    150, 80, 120, 50, 0,

                    //41-45
                    200, 20, 300, 100, 0, 

                    //46-50
                    0, 0, 0, 130, 200,

                    //51-55
                    120, 0, 120, 150, 130
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 14, 25, 26, 38, 51, 52, 53 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColButton(Grd1, new int[] { 4, 5, 8, 12, 18, 21, 42 });
            Sm.GrdFormatDate(Grd1, new int[] { 9, 16, 22 });

            if (!mIsVendorPORequestEqualToDroppingRequest) Sm.GrdColInvisible(Grd1, new int[] { 45, 46, 47, 48 });

            if (!mIsPORequestUseEstPrice)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 51 });
            }

            if (mIsShowForeignName)
            {
                Grd1.Cols[35].Move(14);
                Grd1.Cols[36].Move(15);
                if (IsProcFormat == "1")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 7, 8, 9, 11, 12, 19, 20, 21, 22, 30, 36}, false);
                    Grd1.Cols[31].Move(15);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 7, 8, 9, 11, 12, 19, 20, 21, 22, 30, 31, 36}, false);
                }
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 7, 8, 9, 11, 12, 19, 20, 21, 22, 30, 35, 36 }, false);
                    Grd1.Cols[31].Move(16);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 7, 8, 9, 11, 12, 19, 20, 21, 22, 30, 31, 35, 36 }, false);
                }
            }

            if (!mIsAutoGeneratePurchaseLocalDocNo) Sm.GrdColInvisible(Grd1, new int[] { 32 }, false);
            if (mIsSiteMandatory) Grd1.Cols[34].Move(7);
            if (!mIsPORShowDivisionInfo) Sm.GrdColInvisible(Grd1, new int[] { 54 });
            else Grd1.Cols[54].Move(8);

            if (mPORequestPropCodeEnabled) Grd1.Cols[41].Move(16);   
            Sm.GrdColInvisible(Grd1, new int[] { 40, 44, 52 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 41 }, mPORequestPropCodeEnabled);

            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 39, 40, 45, 46, 47, 48, 49, 50, 52, 53, 55 });
            Sm.GrdColInvisible(Grd1, new int[] { 39 }, mIsShowSeqNoInPORequest);
            if (!mIsPORequestUseAdditionalInformation) Sm.GrdColInvisible(Grd1, new int[] { 42 });
            Grd1.Cols[37].Move(32);
            Grd1.Cols[38].Move(33);
            Grd1.Cols[39].Move(0);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 43 });
            Grd1.Cols[43].Move(16);
            Grd1.Cols[51].Move(16);

            if (!mIsMRShowEstimatedPrice)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 37, 38 });
            }
            if (!mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 53 });
            else Grd1.Cols[53].Move(27);
            if (mIsMRUseApprovalSheet) Grd1.Cols[55].Move(8);
            Sm.GrdColInvisible(Grd1, new int[] { 55 }, mIsMRUseApprovalSheet);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 8, 9, 11, 12, 19, 21, 22, 36 }, !ChkHideInfoInGrd.Checked);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 32 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtDocNo, TxtLocalDocNo, DteDocDt, TxtSiteCode, TxtCopyData, MeeRemark, LueProcType, DteExpDt, ChkFile, TxtFile }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 41, 43, 45, 46, 47, 48, 51, 52, 54 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 42 });
                    LblAdditionalInformation.Enabled = (TxtDocNo.Text.Length > 0);
                    BtnFile.Enabled = false;
                    BtnCopyData.Enabled = false;
                    BtnDownload.Enabled = (TxtFile.Text.Length > 0);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark, LueProcType, DteExpDt, ChkFile }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 14, 18, 29, 41, 42, 51 });
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    LblAdditionalInformation.Enabled = mIsPORequestUseAdditionalInformation;
                    BtnFile.Enabled = true;
                    BtnCopyData.Enabled = true;
                    BtnDownload.Enabled = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    //Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark}, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mSiteCode = string.Empty;
            mDetailDate = string.Empty;
            mHeaderDate = string.Empty;
            compare1 = string.Empty;
            compare2 = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, TxtCopyData, TxtSiteCode, MeeRemark, LueProcType, DteExpDt, TxtFile
            }); 
            if (lPOR2Hdr != null) lPOR2Hdr.Clear();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 25, 26, 51, 52 });
            if (lPOR2Dtl != null) lPOR2Dtl.Clear();
            SetSeqNo();
        }

        public static string CompanyLogo2(string LogoName)
        {
            //Load Company Logo
            string FileName = string.Empty;
            try
            {
                FileName =
                    Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) +
                    @"\" + LogoName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return FileName;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && TxtDocNo.Text.Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length!=0)
            {
                e.DoDefault = mIsPORequestUseAdditionalInformation;
                ShowPORequestApprovalInfo(e.RowIndex);
            }

            if (e.ColIndex == 5 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = mIsPORequestUseAdditionalInformation;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPORequestDlg(this));
            }

            if (e.ColIndex == 18 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = mIsPORequestUseAdditionalInformation;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length==0)
                        Sm.StdMsg(mMsgType.Warning, (Doctitle == "IMS") ? "Purchase request document is empty." : "Material request document is empty.");
                    else
                        Sm.FormShowDialog(new FrmPORequestDlg2(this, e.RowIndex));
                }
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                e.DoDefault = mIsPORequestUseAdditionalInformation;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = mIsPORequestUseAdditionalInformation;
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                e.DoDefault = mIsPORequestUseAdditionalInformation;
                var f = new FrmQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }

            if (mIsPORequestUseAdditionalInformation)
            {
                e.DoDefault = mIsPORequestUseAdditionalInformation;
                if (e.ColIndex == 42 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 11, false, "Item is empty"))
                    Sm.FormShowDialog(new FrmPORequestDlg5(this, Sm.GetGrdStr(Grd1, e.RowIndex, 6), Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 11)));
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = mIsPORequestUseAdditionalInformation;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = mIsPORequestUseAdditionalInformation;

                if (TxtDocNo.Text.Length == 0 && Sm.IsGrdColSelected(new int[] { 5, 14, 18, 29, 41 }, e.ColIndex))
                {
                    if (e.ColIndex == 41) LueRequestEdit(Grd1, LuePropCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                SetSeqNo();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && TxtDocNo.Text.Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                ShowPORequestApprovalInfo(e.RowIndex);
            }

            if (e.ColIndex == 5 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmPORequestDlg(this));

            if (e.ColIndex == 18 && TxtDocNo.Text.Length == 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length == 0)
                    Sm.StdMsg(mMsgType.Warning, (Doctitle == "IMS") ? "Purchase request document is empty." : "Material request document is empty.");
                else
                    Sm.FormShowDialog(new FrmPORequestDlg2(this, e.RowIndex));
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                var f = new FrmQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }

            if (mIsPORequestUseAdditionalInformation)
            {
                if (e.ColIndex == 42 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 11, false, "Item is empty"))
                    Sm.FormShowDialog(new FrmPORequestDlg5(this, Sm.GetGrdStr(Grd1, e.RowIndex, 6), Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 11)));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 14) ComputeTotal(e.RowIndex);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPORequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                if(!mIsPORAllowToCopyCancelledData)
                    Sm.FormShowDialog(new FrmPORequestDlg(this));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || (Sm.GetParameter("DocTitle") == "KIM" && ShowPrintApproval()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            PrintData(TxtDocNo.Text);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            //DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            //SFD.FileName = TxtFile.Text;
            //SFD.DefaultExt = "";
            //SFD.AddExtension = true;

            //if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            //{

            //    if (SFD.ShowDialog() == DialogResult.OK)
            //    {
            //        Application.DoEvents();

            //        //Write the bytes to a file
            //        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
            //        newFile.Write(downloadedData, 0, downloadedData.Length);
            //        newFile.Close();
            //        MessageBox.Show("Saved Successfully");
            //    }
            //}
            //else
            //    MessageBox.Show("No File was Downloaded Yet!");
            bool isSuccessDownload = Sm.DownloadFileWithProgressBar(ref downloadedData, ref PbUpload, mFormatFTPClient, mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            if (isSuccessDownload) Sm.DownloadAction(ref SFD, ref downloadedData, TxtFile.Text);

        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (mIsVendorPORequestEqualToDroppingRequest) UpdateVendorDroppingRequest();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            #region Init
            bool PORequestApprovalForAllDept = IsPORequestApprovalForAllDept();
            string SubCategory = Sm.GetGrdStr(Grd1, 0, 30);
            string DocNo = string.Empty;
            if (mMenuCodeForPORTender)
                DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "PORequestTender", "TblPORequestHdr", SubCategory);
            else
                DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "PORequest", "TblPORequestHdr", SubCategory);

            string LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text;
            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            #endregion

            #region Auto Generate Purchase LocalDocNo
            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count == 0) return;
                if (IsLocalDocumentNotValid(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr
                    )) return;

                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length > 0)
                {
                    SetLocalDocNo(
                        "PORequest",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }

                mlLocalDocument.Clear();
            }
            #endregion
            
            string ProcurementType = string.Empty;
            string ReferenceDocNo = string.Empty;
            ProcurementType = Sm.GetLue(LueProcType);
            ReferenceDocNo = TxtCopyData.Text;

            //if (mIsMRUseProcurementType)
            //{
            //    if (Sm.IsLueEmpty(LueProcType, "Procurement Type")) return;
            //    else ProcurementType = Sm.GetLue(LueProcType);
            //}

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SavePORequest(
                DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ProcurementType, ReferenceDocNo,
                PORequestApprovalForAllDept));

            //cml.Add(SavePORequestHdr(DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ProcurementType, ReferenceDocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0) cml.Add(SavePORequestDtl(DocNo, Row, PORequestApprovalForAllDept));

            #region POR Use Additional Info
            if (mIsPORequestUseAdditionalInformation)
            {
                if (lPOR2Hdr != null)
                {
                    if (lPOR2Hdr.Count > 0)
                    {
                        cml.Add(SavePORequest2Hdr(DocNo, ref lPOR2Hdr));
                        //for (int x = 0; x < lPOR2Hdr.Count; x++)
                        //    cml.Add(SavePORequest2Hdr(DocNo, ref lPOR2Hdr, x));
                    }
                }

                if (lPOR2Dtl != null)
                {
                    if (lPOR2Dtl.Count > 0)
                    {
                        cml.Add(SavePORequest2Dtl(DocNo, ref lPOR2Dtl));
                        //int mDNo = 0;
                        //for (int x = 0; x < lPOR2Dtl.Count; x++)
                        //{
                        //    for (int y = 0; y < Grd1.Rows.Count; y++)
                        //    {
                        //        if (Sm.GetGrdStr(Grd1, y, 6).Length > 0)
                        //        {
                        //            if (lPOR2Dtl[x].MaterialRequestDocNo == Sm.GetGrdStr(Grd1, y, 6) &&
                        //                lPOR2Dtl[x].MaterialRequestDNo == Sm.GetGrdStr(Grd1, y, 7) &&
                        //                lPOR2Dtl[x].ItCode == Sm.GetGrdStr(Grd1, y, 11))
                        //            {
                        //                cml.Add(SavePORequest2Dtl(DocNo, mDNo, x, ref lPOR2Dtl));
                        //                mDNo++;
                        //                break;
                        //            }
                        //        }
                        //    }
                        //}
                    }
                }
            }
            #endregion

            Sm.ExecCommands(cml);

            if (mIsPORequestUseAdditionalInformation)
            {
                if (lPOR2Hdr != null) lPOR2Hdr.Clear();
                if (lPOR2Dtl != null) lPOR2Dtl.Clear();
            }

            ProcessUploadFile(DocNo);
            ShowData(DocNo);  //BtnInsertClick(sender, e);
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblPORequestHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }

        private bool IsLocalDocumentNotValid(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
            )
        {
            if (SeqNo.Length == 0) return false;

            foreach (var x in mlLocalDocument.Where(x => x.SeqNo.Length > 0))
            {
                if (!(
                  Sm.CompareStr(SeqNo, x.SeqNo) &&
                  Sm.CompareStr(DeptCode, x.DeptCode) &&
                  Sm.CompareStr(ItSCCode, x.ItSCCode) &&
                  Sm.CompareStr(Mth, x.Mth) &&
                  Sm.CompareStr(Yr, x.Yr)
                ))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Document# : " + x.DocNo + Environment.NewLine +
                        "Local# : " + x.LocalDocNo + Environment.NewLine + Environment.NewLine +
                        "Invalid data.");
                    return true;
                }
            }
            return false;
        }

        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            bool IsFirst = true;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 6));
                        No += 1;
                    }
                }
            }
            Filter = " Where (" + Filter + ")";


            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblMaterialRequestHdr " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (IsFirst && SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                            IsFirst = false;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsMRUseProcurementType && Sm.IsLueEmpty(LueProcType, "Procurement Type")) ||
                (mIsUseECatalog && Sm.IsDteEmpty(DteExpDt, "Expired Date")) ||
                (mIsPORequestFileMandatory && Sm.IsTxtEmpty(TxtFile, "File", false)) ||
                IsExpDtInvalid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsQtyNotValid() ||
                (mIsAmtValidatedByDroppingRequest && IsTotalAmtNotValid()) ||
                (mIsVendorPORequestEqualToDroppingRequest && IsVendorInvalid()) ||
                IsDocApprovalSettingNotExisted() ||
                IsMaterialRequestCancelled() ||
                IsSubcategoryDifferent() ||
                IsSiteNotValid() ||
                IsDateNotValid() ||
                IsTenderDataInvalid() ||
                TxtFile.Text.Length != 0 && IsUploadFileNotValid()
                ;
        }

        private bool IsExpDtInvalid()
        {
            if (!mIsUseECatalog) return false;

            string DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
            string ExpDt = Sm.Left(Sm.GetDte(DteExpDt), 8);

            if(Sm.CompareDtTm(DocDt, ExpDt) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Document date should not be bigger than Expired date.");
                DteExpDt.Focus();
                return true;
            }

            return false;
        }

        private bool IsTenderDataInvalid()
        {
            if (!mIsMRWithTenderEnabled) return false;

            string MRDocNo = string.Empty;
            string messageValidation = string.Empty;
            string DetectedDocNo = string.Empty;
            string warningMessage = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (MRDocNo.Length > 0) MRDocNo += ",";
                MRDocNo += Sm.GetGrdStr(Grd1, i, 6);
            }

            if (MRDocNo.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Group_Concat(DocNo Separator '\r\n') DocNo ");
                SQL.AppendLine("From TblMaterialRequestHdr ");
                SQL.AppendLine("Where Find_In_Set(DocNo, @Param) ");

                if (mMenuCodeForPORTender)
                {
                    SQL.AppendLine("And TenderInd = 'N' ");
                    messageValidation = "not";
                }
                else
                    SQL.AppendLine("And TenderInd = 'Y' ");

                SQL.AppendLine("; ");

                DetectedDocNo = Sm.GetValue(SQL.ToString(), MRDocNo);

                if (DetectedDocNo.Length > 0)
                {
                    warningMessage = string.Concat("This/these document(s) is/are ", messageValidation, " tender document : ", Environment.NewLine, DetectedDocNo);
                    Sm.StdMsg(mMsgType.Warning, warningMessage);
                    return true;
                }
                
            }

            return false;
        }

        private bool IsVendorInvalid()
        {
            if (mIsVendorPORequestEqualToDroppingRequest)
            {
                bool mFlag = false, mFlag2 = true;
                int mRow = 0;

                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 45).Length > 0)
                    {
                        mFlag2 = false;
                        if (Sm.GetGrdStr(Grd1, i, 47).Length == 0 && 
                            Sm.GetGrdStr(Grd1, i, 48) == mItGrpCodeToValidatePORequestDroppingRequest
                            ) // kalau belum ada vendor di dropping request nya, dan item nya group remunerasi
                        {
                            Sm.StdMsg(mMsgType.Warning, "Vendor isn't equal to Dropping Request's Vendor.");
                            Sm.FocusGrd(Grd1, i, 23);
                            return true;
                        }
                        else
                        {
                            if (Sm.GetGrdStr(Grd1, i, 47).Length == 0) mFlag2 = true;
                            else
                            {
                                string[] s = Sm.GetGrdStr(Grd1, i, 47).Split(',');
                                foreach (string d in s)
                                {
                                    if (d == Sm.GetGrdStr(Grd1, i, 46))
                                    {
                                        mFlag2 = true;
                                    }
                                }
                            }
                        }

                        if (!mFlag2 &&
                            Sm.GetGrdStr(Grd1, i, 48) == mItGrpCodeToValidatePORequestDroppingRequest)
                        {
                            mRow = i;
                            break;
                        }
                    }
                }

                mFlag = mFlag2;

                if (!mFlag)
                {
                    Sm.StdMsg(mMsgType.Warning, "Vendor isn't equal to Dropping Request's Vendor.");
                    Sm.FocusGrd(Grd1, mRow, 23);
                    return true;
                }
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            mHeaderDate = Sm.GetDte(DteDocDt);

            if (mIsComparedToDetailDate)
            {
                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk mendapatkan tanggal terbaru
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareDtTm(compare1, Sm.GetGrdDate(Grd1, Row, 9)) < 0)
                        {
                            compare1 = Sm.GetGrdDate(Grd1, Row, 9);
                            compare2 = compare1;
                        }
                        else
                            compare2 = compare1;
                    }
                    mDetailDate = compare2;

                    if (Sm.CompareDtTm(mHeaderDate, mDetailDate) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Date should be the same or greater than requested Date.");
                        DteDocDt.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsSiteNotValid()
        {
            mSiteCode = Sm.GetGrdStr(Grd1, 0, 33);
            TxtSiteCode.EditValue = Sm.GetGrdStr(Grd1, 0, 34);

            if (!mIsSiteMandatory) return false;

            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 33), mSiteCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            ((Doctitle == "IMS") ? "Purchase Request# : " : "Material Request# : ") + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + 
                            "Site Name : " + Sm.GetGrdStr(Grd1, Row, 34) + Environment.NewLine + Environment.NewLine +
                            "Invalid site."
                            );
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsMaterialRequestCancelled()
        {
            bool Cancelled = true;
            var SQL = new StringBuilder();
            var SelectedMaterialRequest = "##XXX##";
            
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                        SelectedMaterialRequest +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 6) +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            "##";
            }

            SQL.AppendLine("Select A.DocNo, A.ItCode, B.ItName ");
            SQL.AppendLine("From TblMaterialRequestDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where Locate(Concat('##', A.DocNo, A.DNo, '##'), @SelectedMaterialRequest)>0 ");
            SQL.AppendLine("And (A.CancelInd='Y' Or IfNull(A.Status, 'O')='C') ");
            SQL.AppendLine("Order By A.DocNo, B.ItName Limit 1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@SelectedMaterialRequest", SelectedMaterialRequest);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "DocNo", "ItCode", "ItName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            ((Doctitle == "IMS") ? "Purchase Request# : " : "Material Request# : ") + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Item Code : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Item Name : " + Sm.DrStr(dr, 2) + Environment.NewLine + Environment.NewLine +
                            "This request already cancelled."
                            );
                        break;
                    }
                }
                else
                    Cancelled = false;
                dr.Close();
            }
            return Cancelled;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false, (Doctitle == "IMS") ? "Purchase request# is empty." : "Material request# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 14, true, "Quantity is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 19, false, "Quotation# is empty.") ||
                    (mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 29, false, "Remark is empty.")) ||
                    (mIsUseECatalog && IsQtyInvalid(Row)) ||
                    (mIsPORequestUseEstPrice && mIsPORequestEstPriceMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 51, true, "PO Request's Estimated Price could not be zero."))
                    ) 
                    return true;
            }
            return false;
        }

        private bool IsQtyInvalid(int Row)
        {
            if (!mIsUseECatalog) return false;

            if (!IsQtyCapabilityEnabled) return false;

            ReComputeQtyCapability();

            decimal Qty = 0m, QtyCapable = 0m;

            Qty = Sm.GetGrdDec(Grd1, Row, 14);
            QtyCapable = Sm.GetGrdDec(Grd1, Row, 52);

            if (Qty > QtyCapable)
            {
                var msg = new StringBuilder();

                msg.AppendLine("Quantity : " + Sm.FormatNum(Qty, 0));
                msg.AppendLine("Vendor's capability : " + Sm.FormatNum(QtyCapable, 0) + Environment.NewLine);
                msg.AppendLine("Requested quantity exceeds vendor's quantity capability.");

                Sm.StdMsg(mMsgType.Warning, msg.ToString());
                Sm.FocusGrd(Grd1, Row, 14);
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Requested item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            StringBuilder SQL = new StringBuilder(), SQL2 = new StringBuilder();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                {
                    if (IsQtyNotValid2(Row)) return true;
                }
            }
            return false;
        }

        private bool IsTotalAmtNotValid()
        {
            //StringBuilder SQL = new StringBuilder(), SQL2 = new StringBuilder();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 26) > Sm.GetGrdDec(Grd1, Row, 44))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            ((Doctitle == "IMS") ? "Purchase Request# : " : "Material Request# : ") + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Item: " + Sm.GetGrdStr(Grd1, Row, 13)+ Environment.NewLine +
                            "Total Amount : " + Sm.FormatNum((Sm.GetGrdStr(Grd1, Row, 26)), 0) + Environment.NewLine +
                            "Droping Request's Amount : " + Sm.FormatNum((Sm.GetGrdStr(Grd1, Row, 44)), 0) + Environment.NewLine + Environment.NewLine +
                            "PO Request's Total is bigger than Droping Request's Amount.");
                             return true;
                        
                    }
                }
            }
            return false;
        }

        private bool IsQtyNotValid2(int Row)
        {
            StringBuilder SQL = new StringBuilder(), SQL2 = new StringBuilder();

            SQL.AppendLine("Select @DocNo As DocNo, @DNo As DNo, @Qty As Qty, ");
            SQL.AppendLine(" IfNull((Select Qty From TblMaterialRequestDtl Where DocNo=@DocNo And DNo=@DNo), 0) - ");
            SQL.AppendLine(" IfNull(( " );
            SQL.AppendLine("    Select Sum(Qty) From TblPORequestDtl ");
            SQL.AppendLine("    Where CancelInd='N'  And Status<>'C' And MaterialRequestDocNo=@DocNo And MaterialRequestDNo=@DNo ");
            SQL.AppendLine("    And Concat(DocNo, DNo) Not In ( ");
            SQL.AppendLine("        Select Concat(DocNo, DNo) From TblDocApproval ");
            SQL.AppendLine("        Where DocType='PORequest' And IfNull(Status, '')='C')), 0) As OutstandingQty ");

            SQL2.AppendLine("Select Concat('Material Request# : ', A.DocNo, '\n', 'Item : ', C.ItName, '\n', 'Outstanding Quantity : ', Convert(Format(A.OutstandingQty, 2) Using utf8), '\n', 'PO Request Quantity : ', Convert(Format(A.Qty, 2) Using utf8)) As GetValue ");
            SQL2.AppendLine("From (" + SQL.ToString() +") A ");
            SQL2.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL2.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL2.AppendLine("Where A.Qty>A.OutstandingQty ");

            var cm = new MySqlCommand()
            {
                CommandText =SQL2.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 14));

            string Msg = Sm.GetValue(cm);

            if (Msg.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + Environment.NewLine + "PO request quantity is bigger than outstanding quantity.");
                return true;
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            if (mIsPORequestApprovalSettingValidationDisabled) return false;

            var SQL = new StringBuilder();
            SQL.AppendLine("Select 1 From TblDocApprovalSetting Where ");
            if (mMenuCodeForPORTender)
                SQL.AppendLine("DocType='PORequestTender'");
            else
                SQL.AppendLine("DocType='PORequest'");
            SQL.AppendLine("Limit 1;");

            if (!Sm.IsDataExist(SQL.ToString()))
            {
                Sm.StdMsg(mMsgType.Warning, "No approval setting for this PO request.");
                return true;
            }
            return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 30).Length == 0)
                {
                    Grd1.Cells[Row, 30].Value = Grd1.Cells[Row, 31].Value = "XXX";
                }
            }
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 30);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 30))
                    {
                        Sm.StdMsg(mMsgType.Warning, (Doctitle=="IMS") ? "Purchase request have different subcategory " : "Material request have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsUploadFileNotValid()
        {

            string FileName = TxtFile.Text;


            FileInfo f = new FileInfo(FileName);

            if (Sm.IsFTPClientDataNotValid(true, FileName, mHostAddrForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPortForFTPClient) ||
                Sm.IsFileSizeNotValid(true, FileName, false, 0, ref f, mFileSizeMaxUploadFTPClient) ||
                Sm.IsFileNameAlreadyExisted(true, FileName, "TblPORequestHdr", "FileName"))
            {
                return true;
            }

            return false;
        }

      
        private string GenerateRemarkForCancelledDocument(string Remark)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, DocNo = string.Empty, DNo = string.Empty, RemarkTemp = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    DocNo = Sm.GetGrdStr(Grd1, r, 6);
                    DNo = Sm.GetGrdStr(Grd1, r, 7);
                    if (DocNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.MaterialRequestDocNo=@DocNo0" + r.ToString() + " And B.MaterialRequestDNo=@DNo0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                        Sm.CmParam<String>(ref cm, "@DNo0" + r.ToString(), DNo);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Group_Concat(Distinct ");
            SQL.AppendLine("Concat('- MR# : ', B.MaterialRequestDocNo, ', Item : ', D.ItName, ' (PO Request#:', B.DocNo, ') ') ");
            SQL.AppendLine("Order By B.MaterialRequestDocNo, D.ItName, B.DocNo Separator ',$$$') As Remarks ");
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (B.CancelInd='Y' Or B.Status='C') ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode;");

            cm.CommandText = SQL.ToString();

            RemarkTemp = Sm.GetValue(cm);

            if (Remark.Length > 0)
            {
                if (RemarkTemp.Length > 0)
                    Remark = string.Concat(Remark, Environment.NewLine, "Cancelled : ", Environment.NewLine, RemarkTemp.Replace("$$$", Environment.NewLine));
            }
            else
                Remark = string.Concat("Cancelled : ", Environment.NewLine, RemarkTemp.Replace("$$$", Environment.NewLine));

            return Remark;
        }

        private MySqlCommand SavePORequest(
            string DocNo, string LocalDocNo, string SeqNo, string DeptCode, string ItSCCode, string Mth, string Yr, string Revision, string ProcurementType, string ReferenceDocNo,
            bool PORequestApprovalForAllDept)
        {
            bool IsFirst = true;
            var cm = new MySqlCommand();
            var SQL1 = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var SQL4 = new StringBuilder();

            SQL1.AppendLine("/* PO Request */ ");
            SQL1.AppendLine("Set @Dt:=CurrentDateTime();");

            #region Hdr

            SQL1.AppendLine("Insert Into TblPORequestHdr ");
            SQL1.AppendLine("(DocNo, DocDt, LocalDocNo, SiteCode, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, Remark, ");
            if (mIsMRUseProcurementType) SQL1.AppendLine("ProcurementType, ");
            if (mIsPORAllowToCopyCancelledData) SQL1.AppendLine("ReferenceDocNo, ");
            if (mIsUseECatalog) SQL1.AppendLine("ExpDt, ");
            SQL1.AppendLine(" CreateBy, CreateDt) ");
            SQL1.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @SiteCode, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, @Remark,");
            if (mIsMRUseProcurementType) SQL1.AppendLine("@ProcurementType,");
            if (mIsPORAllowToCopyCancelledData) SQL1.AppendLine("@ReferenceDocNo, ");
            if (mIsUseECatalog) SQL1.AppendLine("@ExpDt, ");
            SQL1.AppendLine(" @CreateBy, @Dt);");

            #endregion

            #region Dtl

            SQL2.AppendLine("Insert Into TblPORequestDtl(DocNo, DNo, CancelInd, MaterialRequestDocNo, MaterialRequestDNo, Qty, QtDocNo, QtDNo, ");
            if (mIsPORequestUseEstPrice) SQL2.AppendLine("EstimatePrice, ");
            SQL2.AppendLine("CreditLimit, PropCode, Remark, CreateBy, CreateDt) ");
            SQL2.AppendLine("Values ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 6).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL2.AppendLine(", ");
                    SQL2.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", 'N', @MaterialRequestDocNo_" + r.ToString() + ", @MaterialRequestDNo_" + r.ToString() + ", @Qty_" + r.ToString() + ", @QtDocNo_" + r.ToString() + ", @QtDNo_" + r.ToString() + ", ");
                    if (mIsPORequestUseEstPrice) SQL2.AppendLine("@EstimatePrice_" + r.ToString() + ", ");
                    SQL2.AppendLine("0.00, @PropCode_" + r.ToString() + ", @Remark_" + r.ToString() + ", @CreateBy, @Dt) ");

                    SQL3.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                    SQL3.AppendLine("Select T.DocType, @DocNo, @DNo_" + r.ToString() + ", T.DNo, @CreateBy, @Dt ");
                    SQL3.AppendLine("From TblDocApprovalSetting T ");
                    if (mMenuCodeForPORTender)
                        SQL3.AppendLine("Where T.DocType='PORequestTender' ");
                    else
                        SQL3.AppendLine("Where T.DocType='PORequest' ");

                    if (!PORequestApprovalForAllDept)
                    {
                        SQL3.AppendLine("And T.DeptCode In ( ");
                        SQL3.AppendLine("    Select B.DeptCode ");
                        SQL3.AppendLine("    From TblPORequestDtl A ");
                        SQL3.AppendLine("    Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo=B.DocNo ");
                        SQL3.AppendLine("    Where A.DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ");
                        SQL3.AppendLine(") ");
                    }

                    SQL3.AppendLine("And (T.EndAmt=0.00 ");
                    SQL3.AppendLine("Or T.EndAmt>=IfNull(( ");
                    SQL3.AppendLine("    Select A.Qty*B.UPrice*IfNull(D.Amt, 1.00) ");
                    SQL3.AppendLine("    From TblPORequestDtl A ");
                    SQL3.AppendLine("    Inner Join TblQtDtl B On A.QtDocNo=B.DocNo And A.QtDNo=B.DNo ");
                    SQL3.AppendLine("    Inner Join TblQtHdr C On A.QtDocNo=C.DocNo ");
                    SQL3.AppendLine("    Left Join ( ");
                    SQL3.AppendLine("        Select D1.CurCode1, D1.Amt ");
                    SQL3.AppendLine("        From TblCurrencyRate D1 ");
                    SQL3.AppendLine("        Inner Join ( ");
                    SQL3.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                    SQL3.AppendLine("            From TblCurrencyRate ");
                    SQL3.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
                    SQL3.AppendLine("            Group By CurCode1 ");
                    SQL3.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
                    SQL3.AppendLine("    ) D On C.CurCode=D.CurCode1 ");
                    SQL3.AppendLine("    Where A.DocNo=@DocNo And A.DNo=@DNo_" + r.ToString() + " ");
                    SQL3.AppendLine("), 0.00)) ");

                    SQL3.AppendLine("And (T.StartAmt=0.00 ");
                    SQL3.AppendLine("Or T.StartAmt<=IfNull(( ");
                    SQL3.AppendLine("    Select A.Qty*B.UPrice*IfNull(D.Amt, 1.00) ");
                    SQL3.AppendLine("    From TblPORequestDtl A ");
                    SQL3.AppendLine("    Inner Join TblQtDtl B On A.QtDocNo=B.DocNo And A.QtDNo=B.DNo ");
                    SQL3.AppendLine("    Inner Join TblQtHdr C On A.QtDocNo=C.DocNo ");
                    SQL3.AppendLine("    Left Join ( ");
                    SQL3.AppendLine("        Select D1.CurCode1, D1.Amt ");
                    SQL3.AppendLine("        From TblCurrencyRate D1 ");
                    SQL3.AppendLine("        Inner Join ( ");
                    SQL3.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                    SQL3.AppendLine("            From TblCurrencyRate ");
                    SQL3.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
                    SQL3.AppendLine("            Group By CurCode1 ");
                    SQL3.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
                    SQL3.AppendLine("    ) D On C.CurCode=D.CurCode1 ");
                    SQL3.AppendLine("    Where A.DocNo=@DocNo And A.DNo=@DNo_" + r.ToString() + " ");
                    SQL3.AppendLine("), 0.00)) ");

                    if (mIsDocApprovalSettingUseItemCt)
                    {
                        SQL3.AppendLine("And T.ItCtCode In ( ");
                        SQL3.AppendLine("   Select C.ItCtCode ");
                        SQL3.AppendLine("   From TblPORequestDtl A ");
                        SQL3.AppendLine("   Inner Join TblMaterialRequestDtl B On A.MaterialRequestDocNo = B.DocNo And A.MaterialRequestDNo = B.DNo ");
                        SQL3.AppendLine("   Inner Join TblItem C On B.ItCode = C.ItCode ");
                        SQL3.AppendLine("   Where A.DocNo = @DocNo And A.DNo = @DNo_" + r.ToString());
                        SQL3.AppendLine(") ");
                    }
                    SQL3.AppendLine(";");

                    SQL3.AppendLine("Update TblPORequestDtl Set Status='A' ");
                    SQL3.AppendLine("Where DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ");
                    SQL3.AppendLine("And Not Exists(Select DocType From TblDocApproval ");
                    if (mMenuCodeForPORTender)
                        SQL3.AppendLine("Where DocType='PORequestTender' ");
                    else
                        SQL3.AppendLine("Where DocType='PORequest' ");
                    SQL3.AppendLine("And DocNo=@DocNo And DNo=@DNo_" + r.ToString() + "); ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<String>(ref cm, "@MaterialRequestDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 14));
                    Sm.CmParam<String>(ref cm, "@QtDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 19));
                    Sm.CmParam<String>(ref cm, "@QtDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 20));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 29));
                    Sm.CmParam<String>(ref cm, "@PropCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 40));
                    if (mIsPORequestUseEstPrice) Sm.CmParam<Decimal>(ref cm, "@EstimatePrice_"+r.ToString(), Sm.GetGrdDec(Grd1, r, 51));
                }
            }

            SQL2.AppendLine("; ");

            #endregion

            #region Update TakeOrderInd

            if (mIsUseECatalog)
            {
                SQL4.AppendLine("Update TblPORequestDtl ");
                SQL4.AppendLine("Set TakeOrderInd = 'Y' ");
                SQL4.AppendLine("Where DocNo = @DocNo ");
                SQL4.AppendLine("And MaterialRequestDocNo In ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("    Select Distinct DocNo ");
                SQL4.AppendLine("    From TblMaterialRequestHdr ");
                SQL4.AppendLine("    Where RMInd = 'N' ");
                SQL4.AppendLine("); ");
            }

            #endregion

            string commandText = SQL1.ToString() + SQL2.ToString() + SQL3.ToString();
            if (mIsUseECatalog) commandText += SQL4.ToString();

            cm.CommandText = commandText;

            #region Hdr

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            var Remark = MeeRemark.Text;
            if (mIsCancelledDocumentCopyToRemark)
                Remark = GenerateRemarkForCancelledDocument(Remark);
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            if (mIsMRUseProcurementType)
                Sm.CmParam<String>(ref cm, "@ProcurementType", ProcurementType);
            if (mIsPORAllowToCopyCancelledData)
                Sm.CmParam<String>(ref cm, "@ReferenceDocNo", ReferenceDocNo);
            if (mIsUseECatalog) Sm.CmParamDt(ref cm, "@ExpDt", Sm.GetDte(DteExpDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            #endregion

            return cm;
        }

        //private MySqlCommand SavePORequestHdr(
        //    string DocNo, string LocalDocNo, string SeqNo, string DeptCode, string ItSCCode, string Mth, string Yr, string Revision, string ProcurementType, string ReferenceDocNo
        //    )
        //{
        //    var SQL = new StringBuilder();
        //    SQL.AppendLine("Insert Into TblPORequestHdr ");
        //    SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SiteCode, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, Remark, ");
        //    if (mIsMRUseProcurementType)
        //        SQL.AppendLine("ProcurementType, ");
        //    if (mIsPORAllowToCopyCancelledData)
        //        SQL.AppendLine("ReferenceDocNo, ");

        //    if (mIsUseECatalog) SQL.AppendLine("ExpDt, ");

        //    SQL.AppendLine(" CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @SiteCode, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, @Remark,");
        //    if (mIsMRUseProcurementType)
        //        SQL.AppendLine("@ProcurementType,");
        //    if (mIsPORAllowToCopyCancelledData)
        //        SQL.AppendLine("@ReferenceDocNo, ");

        //    if (mIsUseECatalog) SQL.AppendLine("@ExpDt, ");

        //    SQL.AppendLine(" @CreateBy, CurrentDateTime());");

        //    var cm = new MySqlCommand()
        //    {
        //        CommandText = SQL.ToString()
        //    };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
        //    Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
        //    Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
        //    Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
        //    Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
        //    Sm.CmParam<String>(ref cm, "@Mth", Mth);
        //    Sm.CmParam<String>(ref cm, "@Yr", Yr);
        //    Sm.CmParam<String>(ref cm, "@Revision", Revision);
        //    var Remark = MeeRemark.Text;
        //    if (mIsCancelledDocumentCopyToRemark)
        //        Remark = GenerateRemarkForCancelledDocument(Remark);
        //    Sm.CmParam<String>(ref cm, "@Remark", Remark);
        //    if (mIsMRUseProcurementType) 
        //        Sm.CmParam<String>(ref cm, "@ProcurementType", ProcurementType);
        //    if (mIsPORAllowToCopyCancelledData)
        //        Sm.CmParam<String>(ref cm, "@ReferenceDocNo", ReferenceDocNo);
        //    if (mIsUseECatalog) Sm.CmParamDt(ref cm, "@ExpDt", Sm.GetDte(DteExpDt));

        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    return cm;
        //}

        //private MySqlCommand SavePORequestDtl(string DocNo, int Row, bool PORequestApprovalForAllDept)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPORequestDtl(DocNo, DNo, CancelInd, MaterialRequestDocNo, MaterialRequestDNo, Qty, QtDocNo, QtDNo, ");
        //    if (mIsPORequestUseEstPrice) SQL.AppendLine("EstimatePrice, ");
        //    SQL.AppendLine("CreditLimit, PropCode, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, 'N', @MaterialRequestDocNo, @MaterialRequestDNo, @Qty, @QtDocNo, @QtDNo, ");
        //    if (mIsPORequestUseEstPrice) SQL.AppendLine("@EstimatePrice, ");
        //    SQL.AppendLine("0.00, @PropCode, @Remark, @CreateBy, CurrentDateTime()); ");

        //    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDocApprovalSetting T ");
        //    if(mMenuCodeForPORTender)
        //        SQL.AppendLine("Where T.DocType='PORequestTender' ");
        //    else
        //        SQL.AppendLine("Where T.DocType='PORequest' ");

        //    if (!PORequestApprovalForAllDept)
        //    {
        //        SQL.AppendLine("And T.DeptCode In ( ");
        //        SQL.AppendLine("    Select B.DeptCode ");
        //        SQL.AppendLine("    From TblPORequestDtl A ");
        //        SQL.AppendLine("    Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo=B.DocNo ");
        //        SQL.AppendLine("    Where A.DocNo=@DocNo And DNo=@DNo ");
        //        SQL.AppendLine(") ");
        //    }

        //    SQL.AppendLine("And (T.EndAmt=0.00 ");
        //    SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
        //    SQL.AppendLine("    Select A.Qty*B.UPrice*IfNull(D.Amt, 1.00) ");
        //    SQL.AppendLine("    From TblPORequestDtl A ");
        //    SQL.AppendLine("    Inner Join TblQtDtl B On A.QtDocNo=B.DocNo And A.QtDNo=B.DNo ");
        //    SQL.AppendLine("    Inner Join TblQtHdr C On A.QtDocNo=C.DocNo ");
        //    SQL.AppendLine("    Left Join ( ");
        //    SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
        //    SQL.AppendLine("        From TblCurrencyRate D1 ");
        //    SQL.AppendLine("        Inner Join ( ");
        //    SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
        //    SQL.AppendLine("            From TblCurrencyRate ");
        //    SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
        //    SQL.AppendLine("            Group By CurCode1 ");
        //    SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
        //    SQL.AppendLine("    ) D On C.CurCode=D.CurCode1 ");
        //    SQL.AppendLine("    Where A.DocNo=@DocNo And A.DNo=@DNo ");
        //    SQL.AppendLine("), 0.00)) ");

        //    SQL.AppendLine("And (T.StartAmt=0.00 ");
        //    SQL.AppendLine("Or T.StartAmt<=IfNull(( " );
        //    SQL.AppendLine("    Select A.Qty*B.UPrice*IfNull(D.Amt, 1.00) ");
        //    SQL.AppendLine("    From TblPORequestDtl A ");
        //    SQL.AppendLine("    Inner Join TblQtDtl B On A.QtDocNo=B.DocNo And A.QtDNo=B.DNo ");
        //    SQL.AppendLine("    Inner Join TblQtHdr C On A.QtDocNo=C.DocNo ");
        //    SQL.AppendLine("    Left Join ( ");
        //    SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
        //    SQL.AppendLine("        From TblCurrencyRate D1 ");
        //    SQL.AppendLine("        Inner Join ( ");
        //    SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
        //    SQL.AppendLine("            From TblCurrencyRate ");
        //    SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
        //    SQL.AppendLine("            Group By CurCode1 ");
        //    SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
        //    SQL.AppendLine("    ) D On C.CurCode=D.CurCode1 ");
        //    SQL.AppendLine("    Where A.DocNo=@DocNo And A.DNo=@DNo ");
        //    SQL.AppendLine("), 0.00)); ");

        //    SQL.AppendLine("Update TblPORequestDtl Set Status='A' ");
        //    SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ");
        //    SQL.AppendLine("And Not Exists(Select DocType From TblDocApproval ");
        //    if (mMenuCodeForPORTender)
        //        SQL.AppendLine("Where DocType='PORequestTender' ");
        //    else
        //        SQL.AppendLine("Where DocType='PORequest' ");
        //    SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo); ");


        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", Sm.GetGrdStr(Grd1, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@MaterialRequestDNo", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 14));
        //    Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd1, Row, 19));
        //    Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd1, Row, 20));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 29));
        //    Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 40));
        //    if (mIsPORequestUseEstPrice) Sm.CmParam<Decimal>(ref cm, "@EstimatePrice", Sm.GetGrdDec(Grd1, Row, 51));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePORequest2Hdr(string DocNo, ref List<PORYKHdr> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("/* PO Request Hdr 2 */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblPORequest2Hdr(DocNo, RepairDt, ConditionDesc, Tax, TransferOfTitle, BasisForProposal, FormNo, RevisionNo, RevisionDt, RemarkTotal, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            for (int i = 0; i < l.Count; i++)
            {
                if (IsFirst)
                    IsFirst = false;
                else
                    SQL.AppendLine(", ");
                SQL.AppendLine(" (@DocNo, @RepairDt_" + i.ToString() + ", @ConditionDesc_" + i.ToString() + ",  @Tax_" + i.ToString() + ",  @TransferOfTitle_" + i.ToString() + ",  @BasisForProposal_" + i.ToString() + ",  @FormNo_" + i.ToString() + ",  @RevisionNo_" + i.ToString() + ",  @RevisionDt_" + i.ToString() + ",  @RemarkTotal_" + i.ToString() + ",  @CreateBy, @Dt) ");

                Sm.CmParamDt(ref cm, "@RepairDt_" + i.ToString(), l[i].RepairDt);
                Sm.CmParam<String>(ref cm, "@ConditionDesc_" + i.ToString(), l[i].ConditionDesc);
                Sm.CmParam<Decimal>(ref cm, "@Tax_" + i.ToString(), l[i].Tax);
                Sm.CmParam<String>(ref cm, "@TransferOfTitle_" + i.ToString(), l[i].TransferOfTitle);
                Sm.CmParam<String>(ref cm, "@BasisForProposal_" + i.ToString(), l[i].BasisForProposal);
                Sm.CmParam<String>(ref cm, "@FormNo_" + i.ToString(), l[i].FormNo);
                Sm.CmParam<String>(ref cm, "@RevisionNo_" + i.ToString(), l[i].RevisionNo);
                Sm.CmParamDt(ref cm, "@RevisionDt_" + i.ToString(), l[i].RevisionDt);
                Sm.CmParam<String>(ref cm, "@RemarkTotal_" + i.ToString(), l[i].RemarkTotal);                
            }
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            SQL.AppendLine("; ");
            
            cm.CommandText = SQL.ToString();

            return cm;
        }

        //private MySqlCommand SavePORequest2Hdr(string DocNo, ref List<PORYKHdr> l2, int i)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPORequest2Hdr(DocNo, RepairDt, ConditionDesc, Tax, TransferOfTitle, BasisForProposal, FormNo, RevisionNo, RevisionDt, RemarkTotal, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @RepairDt, @ConditionDesc, @Tax, @TransferOfTitle, @BasisForProposal, @FormNo, @RevisionNo, @RevisionDt, @RemarkTotal, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@RepairDt", l2[i].RepairDt);
        //    Sm.CmParam<String>(ref cm, "@ConditionDesc", l2[i].ConditionDesc);
        //    Sm.CmParam<Decimal>(ref cm, "@Tax", l2[i].Tax);
        //    Sm.CmParam<String>(ref cm, "@TransferOfTitle", l2[i].TransferOfTitle);
        //    Sm.CmParam<String>(ref cm, "@BasisForProposal", l2[i].BasisForProposal);
        //    Sm.CmParam<String>(ref cm, "@FormNo", l2[i].FormNo);
        //    Sm.CmParam<String>(ref cm, "@RevisionNo", l2[i].RevisionNo);
        //    Sm.CmParamDt(ref cm, "@RevisionDt", l2[i].RevisionDt);
        //    Sm.CmParam<String>(ref cm, "@RemarkTotal", l2[i].RemarkTotal);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePORequest2Dtl(string DocNo, ref List<PORYKDtl> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("/* PO Request Dtl 2 */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblPORequest2Dtl(DocNo, DNo, MaterialRequestDocNo, MaterialRequestDNo, ItCode, Brand, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            int DNo = 0;
            for (int x = 0; x < l.Count; x++)
            {
                for (int y = 0; y < Grd1.Rows.Count; y++)
                {
                    if (Sm.GetGrdStr(Grd1, y, 6).Length > 0)
                    {
                        if (l[x].MaterialRequestDocNo == Sm.GetGrdStr(Grd1, y, 6) &&
                            l[x].MaterialRequestDNo == Sm.GetGrdStr(Grd1, y, 7) &&
                            l[x].ItCode == Sm.GetGrdStr(Grd1, y, 11))
                        {
                            if (IsFirst)
                                IsFirst = false;
                            else
                                SQL.AppendLine(", ");
                            SQL.AppendLine("(@DocNo, @DNo_" + x.ToString() + ", @MaterialRequestDocNo_" + x.ToString() + ", @MaterialRequestDNo_" + x.ToString() + ",  @ItCode_" + x.ToString() + ",  @Brand_" + x.ToString() + ",  @CreateBy, @Dt) ");
                            //cml.Add(SavePORequest2Dtl(DocNo, mDNo, x, ref lPOR2Dtl));

                            Sm.CmParam<String>(ref cm, "@DNo_" + x.ToString(), Sm.Right("00" + (DNo + 1).ToString(), 3));
                            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo_" + x.ToString(), l[x].MaterialRequestDocNo);
                            Sm.CmParam<String>(ref cm, "@MaterialRequestDNo_" + x.ToString(), l[x].MaterialRequestDNo);
                            Sm.CmParam<String>(ref cm, "@ItCode_" + x.ToString(), l[x].ItCode);
                            Sm.CmParam<String>(ref cm, "@Brand_" + x.ToString(), l[x].Brand);

                            DNo++;
                            break;
                        }
                    }
                }
            }

            SQL.AppendLine("; ");
            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SavePORequest2Dtl(string DocNo, int DNo, int i, ref List<PORYKDtl> l2Dtl)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPORequest2Dtl(DocNo, DNo, MaterialRequestDocNo, MaterialRequestDNo, ItCode, Brand, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @MaterialRequestDocNo, @MaterialRequestDNo, @ItCode, @Brand, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (DNo + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", l2Dtl[i].MaterialRequestDocNo);
        //    Sm.CmParam<String>(ref cm, "@MaterialRequestDNo", l2Dtl[i].MaterialRequestDNo);
        //    Sm.CmParam<String>(ref cm, "@ItCode", l2Dtl[i].ItCode);
        //    Sm.CmParam<String>(ref cm, "@Brand", l2Dtl[i].Brand);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdatePORevisionFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPORequestHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        #endregion

        #region Edit data

        private void CancelData()
        {
            UpdateCancelledMaterialRequest();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 6).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;


            var cml = new List<MySqlCommand>();


            cml.Add(CancelPORequestDtl(TxtDocNo.Text, DNo));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledMaterialRequest()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd ");
            SQL.AppendLine("From TblPORequestDtl  ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Index, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Index, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                IsCancelledPORequestNotExisted(DNo) ||
                IsCancelledPORequestProcessedAlready(DNo);
        }

        private bool IsCancelledPORequestNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, (Doctitle == "IMS") ? "You need to cancel at least 1 purchase request." : "You need to cancel at least 1 material request.");
                return true;
            }
            return false;
        }

        private bool IsCancelledPORequestProcessedAlready(string DNo)
        {
            return Sm.IsDataExist(
                "Select 1 From TblPODtl Where CancelInd='N' And PORequestDocNo=@Param And PORequestDNo In (" + DNo + ") Limit 1;",
                TxtDocNo.Text,
                "This PO request already processed to PO.");
        }

        private MySqlCommand CancelPORequestDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPORequestDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C') And DNo In (" + DNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPORequestHdr(DocNo);
                ShowPORequestDtl(DocNo);
                SetSeqNo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPORequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.SiteCode, B.SiteName, A.Remark, A.FileName, ");

            if (mIsMRUseProcurementType) SQL.AppendLine("A.ProcurementType, ");
            else SQL.AppendLine("Null as ProcurementType, ");

            if (mIsPORAllowToCopyCancelledData) SQL.AppendLine(" A.ReferenceDocNo, ");
            else SQL.AppendLine("Null As ReferenceDocNo, ");

            if (mIsUseECatalog) SQL.AppendLine("A.ExpDt ");
            else SQL.AppendLine("Null As ExpDt ");
            
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    "DocNo", 
                    "LocalDocNo", "DocDt", "SiteCode", "SiteName", "Remark", 
                    "ProcurementType", "ExpDt", "FileName", "ReferenceDocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    mSiteCode = Sm.DrStr(dr, c[3]);
                    TxtSiteCode.EditValue = Sm.DrStr(dr, c[4]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    if (mIsMRUseProcurementType)
                        LueProcType.EditValue = Sm.DrStr(dr, c[6]);

                    if (mIsUseECatalog) Sm.SetDte(DteExpDt, Sm.DrStr(dr, c[7]));
                    TxtFile.EditValue = Sm.DrStr(dr, c[8]);
                    if (mIsPORAllowToCopyCancelledData) TxtCopyData.EditValue = Sm.DrStr(dr, c[9]);
                }, true
            );
        }

        private void ShowPORequestDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, A.CancelInd, Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
                SQL.AppendLine("A.MaterialRequestDocNo, A.MaterialRequestDNo, B.DocDt As MaterialRequestDocDt, ");
                SQL.AppendLine("D.DeptName, C.ItCode, A.Qty, E.PurchaseUomCode, ");
                SQL.AppendLine("E.ItName, E.ForeignName, ");
                SQL.AppendLine("C.UsageDt, C.Remark As MaterialRequestRemark, ");
                SQL.AppendLine("A.QtDocNo, A.QtDNo, F.DocDt, H.VdName, F.CurCode, G.UPrice, (A.Qty*G.UPrice) As Total, ");
                if (mIsUseECatalog)
                    SQL.AppendLine("G.UPriceInit, ");
                else
                    SQL.AppendLine("0.00 As UPriceInit, ");
                SQL.AppendLine("I.PtName, J.DTName, A.Remark, ifnull(E.ItScCode, 'XXX') As ItScCode, ifnull(K.ItScName, 'XXX') As ItScName, B.LocalDocNo, E.ItcodeInternal, C.CurCode, C.EstPrice, A.PropCode, L.PropName, E.Specification, ");
                SQL.AppendLine("M.ProjectCode, M.ProjectName, A.EstimatePrice,  ");
                if (mIsMRUseApprovalSheet)
                    SQL.AppendLine("case when B.EximInd='Y' then P.OptDesc when B.EximInd='N' then O.OptDesc ELSE '' END AS MRType, ");
                else
                    SQL.AppendLine("Null As MRType,");
                if(mIsPORShowDivisionInfo)
                    SQL.AppendLine("N.DivisionName ");
                else
                    SQL.AppendLine("Null As DivisionName ");
                SQL.AppendLine("From TblPORequestDtl A ");
                SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl C On A.MaterialRequestDocNo=C.DocNo And A.MaterialRequestDNo=C.DNo ");
                SQL.AppendLine("Inner Join TblDepartment D On B.DeptCode=D.DeptCode ");
                SQL.AppendLine("Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("Inner Join TblQtHdr F On A.QtDocNo=F.DocNo ");
                SQL.AppendLine("Inner Join TblQtDtl G On A.QtDocNo=G.DocNo And A.QtDNo=G.DNo ");
                SQL.AppendLine("Inner Join TblVendor H On F.VdCode=H.VdCode ");
                SQL.AppendLine("Inner Join TblPaymentTerm I On F.PtCode=I.PtCode ");
                SQL.AppendLine("Left Join TblDeliveryType J On F.DTCode=J.DTCode ");
                SQL.AppendLine("Left Join TblItemSubcategory K On E.ItScCode= K.ItScCode");
                SQL.AppendLine("Left Join TblProperty L On A.PropCode= L.PropCode ");
                SQL.AppendLine("Left Join TblProjectGroup M On B.PGCode=M.PGCode ");

                   
                    
                if (mIsPORShowDivisionInfo)
                    SQL.AppendLine("left Join TblDivision N On B.DivisionCode=N.DivisionCode ");
                if (mIsMRUseApprovalSheet)
                {
                    SQL.AppendLine("LEFT JOIN tbloption O ON B.MRType = O.OptCode and O.OptCat = 'MRType' ");
                    SQL.AppendLine("LEFT JOIN tbloption P ON B.MRType = P.OptCode AND P.OptCat = 'MRRoutineType' ");

                }
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "CancelInd", "StatusDesc", "MaterialRequestDocNo", "MaterialRequestDNo", "MaterialRequestDocDt",  
                        
                        //6-10
                        "DeptName", "ItCode", "ItName", "Qty", "PurchaseUomCode",  
                        
                        //11-15
                        "UsageDt", "MaterialRequestRemark", "QtDocNo", "QtDNo", "DocDt",  
                        
                        //16-20
                        "VdName", "CurCode", "UPrice", "Total", "PtName", 
                        
                        //21-25
                        "DTName", "Remark", "ItScCode", "ItScName", "LocalDocNo",

                        //26-30
                        "ForeignName", "ItcodeInternal", "CurCode", "Estprice", "PropCode",

                        //31-35
                        "PropName", "Specification", "ProjectCode", "ProjectName", "EstimatePrice",

                        //36-38
                        "UPriceInit", "DivisionName", "MRType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 25);
                        Grd.Cells[Row, 33].Value = mSiteCode;
                        Grd.Cells[Row, 34].Value = TxtSiteCode.EditValue;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 28);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 31);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 32);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 49, 33);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 50, 34);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 51, 35);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 36);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 54, 37);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 55, 38);
                    }, false, false, true, false
                );
                Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 25, 26, 38, 51 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (BtnSave.Enabled)
                    SetFormControl(mState.Insert);
                else
                    SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void ReComputeQtyCapability()
        {
            var l = new List<QtyCapability>();

            string QtDocNoDNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 19).Length > 0)
                {
                    if (QtDocNoDNo.Length > 0) QtDocNoDNo += ",";
                    QtDocNoDNo += string.Concat(
                        Sm.GetGrdStr(Grd1, i, 19),
                        Sm.GetGrdStr(Grd1, i, 20)
                        );
                }
            }

            if (QtDocNoDNo.Length > 0)
            {
                GetQtyCapability(ref l, QtDocNoDNo);
                if (l.Count > 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 19).Length > 0)
                        {
                            foreach (var x in l.Where(w => w.QtDocNo == Sm.GetGrdStr(Grd1, i, 19) && w.QtDNo == Sm.GetGrdStr(Grd1, i, 20)))
                            {
                                Grd1.Cells[i, 52].Value = x.Qty;
                            }
                        }
                    }
                }
            }

            l.Clear();
        }

        private void GetQtyCapability(ref List<QtyCapability> l, string QtDocNoDNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DNo, (A.Qty - IfNull(B.Qty, 0.00)) Qty ");
            SQL.AppendLine("From TblQtDtl A ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.QtDocNo, T1.QtDNo, Sum(T1.Qty) Qty ");
            SQL.AppendLine("    From TblPORequestDtl T1 ");
            SQL.AppendLine("    Where T1.CancelInd = 'N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By T1.QtDocNo, T1.QtDNo ");
            SQL.AppendLine(") B On A.DocNo = B.QtDocNo And A.DNo = B.QtDNo ");
            SQL.AppendLine("Where Find_In_Set(Concat(A.DocNo, A.DNo), @QtDocNoDNo); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@QtDocNoDNo", QtDocNoDNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "Qty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new QtyCapability()
                        {
                            QtDocNo = Sm.DrStr(dr, c[0]),
                            QtDNo = Sm.DrStr(dr, c[1]),
                            Qty = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void UpdateVendorDroppingRequest()
        {
            if(Grd1.Rows.Count > 1)
            {
                string mMRDocNo = string.Empty;
                string mItCode = string.Empty;
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (mMRDocNo.Length > 0) mMRDocNo += ",";
                    if (mItCode.Length > 0) mItCode += ",";
                    mMRDocNo += Sm.GetGrdStr(Grd1, i, 6);
                    mItCode += Sm.GetGrdStr(Grd1, i, 11);
                }

                if (mMRDocNo.Length > 0)
                {
                    var SQL = new StringBuilder();
                    var cm = new MySqlCommand();
                    var l = new List<DroppingVendor>();

                    SQL.AppendLine("Select A.DocNo, A1.ItCode, A.DroppingRequestDocNo, B.VdCode ");
                    SQL.AppendLine("From TblMaterialRequestHdr A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl A1 On A.DocNo = A1.DocNo ");
                    SQL.AppendLine("    And Find_In_Set(A.DocNo, @DocNo) ");
                    SQL.AppendLine("    And Find_In_Set(A1.ItCode, @ItCode) ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T1.DocNo, T4.ResourceItCode, Group_Concat(Distinct T2.VdCode) VdCode ");
                    SQL.AppendLine("    From TblDroppingRequestHdr T1 ");
                    SQL.AppendLine("    Inner Join TblDroppingRequestDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("        And T1.Status = 'A' ");
                    SQL.AppendLine("        And T1.CancelInd = 'N' ");
                    SQL.AppendLine("    Inner Join TblMaterialRequestHdr T3 On T1.DocNo = T3.DroppingRequestDocNo ");
                    SQL.AppendLine("        And Find_In_Set(T3.DocNo, @DocNo) ");
                    SQL.AppendLine("    Inner Join TblProjectImplementationRBPHdr T4 On T2.PRBPDocNo = T4.DocNo ");
                    SQL.AppendLine("        And Find_In_Set(T4.ResourceItCode, @ItCode) ");
                    SQL.AppendLine("    Group By T1.DocNo, T4.ResourceItCode ");
                    SQL.AppendLine(") B On A.DroppingRequestDocNo = B.DocNo And A1.ItCode = B.ResourceItCode; ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {
                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", mMRDocNo);
                        Sm.CmParam<String>(ref cm, "@ItCode", mItCode);
                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "ItCode", "DroppingRequestDocNo", "VdCode" });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new DroppingVendor()
                                {
                                    MaterialRequestDocNo = Sm.DrStr(dr, c[0]),
                                    ItCode = Sm.DrStr(dr, c[1]),
                                    DroppingRequestDocNo = Sm.DrStr(dr, c[2]),
                                    VdCode = Sm.DrStr(dr, c[3])
                                });
                            }
                        }
                        dr.Close();
                    }

                    if (l.Count > 0)
                    {
                        for (int i = 0; i < l.Count; ++i)
                        {
                            for (int j = 0; j < Grd1.Rows.Count - 1; ++j)
                            {
                                if (Sm.GetGrdStr(Grd1, j, 6) == l[i].MaterialRequestDocNo && 
                                    Sm.GetGrdStr(Grd1, j, 11) == l[i].ItCode)
                                {
                                    Grd1.Cells[j, 45].Value = l[i].DroppingRequestDocNo;
                                    Grd1.Cells[j, 47].Value = l[i].VdCode;
                                    break;
                                }
                            }
                        }
                    }

                    l.Clear();
                }
            }
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex-1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }
        
        internal void SetSeqNo()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Grd1.Cells[r, 39].Value = r + 1;
        }

        private static string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            var SQL = new StringBuilder();

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       And Substring(DocNo, Locate('" + DocAbbr + "', DocNo), Length('" + DocAbbr + "')) = '" + DocAbbr + "' ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    //SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Where Substring(DocNo, Locate('" + DocAbbr + "',DocNo))=Concat('" + DocAbbr + "','/','" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Substring(DocNo, Locate('" + DocAbbr + "',DocNo))=Concat('" + DocAbbr + "','/','" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        internal string GetSelectedMaterialRequest()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 6) +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedQt()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 19).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 20).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 19) +
                            Sm.GetGrdStr(Grd1, Row, 20) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 14).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 14);
            if (Sm.GetGrdStr(Grd1, Row, 25).Length != 0) UPrice = Sm.GetGrdDec(Grd1, Row, 25);

            Grd1.Cells[Row, 26].Value = Qty * UPrice;
        }

        private void ShowPORequestApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
	        SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            if (mMenuCodeForPORTender)
                SQL.AppendLine("Where DocType='PORequestTender' ");
            else
                SQL.AppendLine("Where DocType='PORequest' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 0;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                         new string[] 
                        { 
                            "UserCode", 
                            "StatusDesc", "LastUpDt", "Remark" 
                        }
                    );
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private bool IsPORequestApprovalForAllDept()
        {
            return Sm.GetParameter("PORequestApprovalForAllDept") == "Y";
        }

        private void PrintData(string DocNo)
        {
            var l = new List<POR>();
            var l2 = new List<PORYK>();
            var l3 = new List<PORYK2>();
            var l4 = new List<PORYKSign>();
            var l5 = new List<PORYKSign2>();
            var ldtl = new List<PORDtl>();
            var ldtl2 = new List<PORDtl2>();

            string mDocTitle = Sm.GetParameter("DocTitle");
            string[] TableName = { "POR", "PORDtl", "PORYK", "PORYK2", "PORYKSign","PORYKSign2", "PORDtl2" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select @CompanyLogo As CompanyLogo, H.CompanyName, H.CompanyPhone, H.CompanyFax, H.CompanyAddress, '' As CompanyAddressCity, H.SiteName, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', null as SiteName, ");
            }
            SQL.AppendLine("If((Select parvalue From TblParameter Where ParCode = 'IsFilterLocalDocNo')='Y',A.LocalDocNo,A.Docno) As DocNo,DATE_FORMAT(A.DocDt,'%d %M %Y')As DocDt, ");
            SQL.AppendLine("GROUP_CONCAT(DISTINCT D.VdName SEPARATOR ' ; ') VdName,GROUP_CONCAT(DISTINCT D.Phone SEPARATOR ' ; ') Phone,GROUP_CONCAT(DISTINCT D.Fax SEPARATOR ' ; ') Fax,GROUP_CONCAT(DISTINCT D.Address SEPARATOR ' ; ') Address, ");
            SQL.AppendLine("A.Remark, G.DeptName, Round(E.PtDay,0)As PtDay, A.LocalDocNo ");
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblQtHdr C On B.QtDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblVendor D On C.VdCode=D.VdCode ");
            SQL.AppendLine("Inner Join TblPaymentterm E On C.PtCode=E.PtCode ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr F On B.MaterialRequestDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblDepartment G On F.DeptCode=G.DeptCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress, B.SiteName ");
                SQL.AppendLine("    From TblPORequesthdr A  ");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(")H On A.DocNo = H.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select D.EntLogoName " +
                       "From TblPORequestHdr A  " +
                       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                       "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                       "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Companylogo",
                        //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                                                 
                         //6-10
                         "DocNo",
                         "DocDt",
                         "VdName",
                         "Phone" ,
                         "Fax" ,

                         //11-15
                         "Address",
                         "Remark",
                         "DeptName",
                         "PtDay",
                         "SiteName",

                         //16
                         "LocalDocNo"
                        
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new POR()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyLongAddress = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            VdName = Sm.DrStr(dr, c[8]),
                            Phone = Sm.DrStr(dr, c[9]),
                            Fax = Sm.DrStr(dr, c[10]),

                            Address = Sm.DrStr(dr, c[11]),
                            Remark = Sm.DrStr(dr, c[12]),
                            DeptName = Sm.DrStr(dr, c[13]),
                            PtDay = Sm.DrDec(dr, c[14]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                           
                            SiteName = Sm.DrStr(dr, c[15]),
                            LocalDocNo = Sm.DrStr(dr, c[16]),
                            FooterImage = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png",
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.DocNo, A.DNo, C.ItCode, E.ItName, A.Qty, E.PurchaseUomCode, F.CurCode, G.UPrice, ");
                SQLDtl.AppendLine("(A.Qty*G.UPrice) As Total, I.PtName, A.Remark, E.ForeignName, J.DivisionName, D.DeptName, Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc,  ");
                SQLDtl.AppendLine("H.VdName, E.Specification, A.MaterialRequestDocNo ");
                SQLDtl.AppendLine("From TblPORequestDtl A  ");
                SQLDtl.AppendLine("Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo=B.DocNo ");
                SQLDtl.AppendLine("Inner Join TblMaterialRequestDtl C On A.MaterialRequestDocNo=C.DocNo And A.MaterialRequestDNo=C.DNo ");
                SQLDtl.AppendLine("Inner Join TblDepartment D On B.DeptCode=D.DeptCode ");
                SQLDtl.AppendLine("Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQLDtl.AppendLine("Inner Join TblQtHdr F On A.QtDocNo=F.DocNo ");
                SQLDtl.AppendLine("Inner Join TblQtDtl G On A.QtDocNo=G.DocNo And A.QtDNo=G.DNo ");
                SQLDtl.AppendLine("Inner Join TblVendor H On F.VdCode=H.VdCode ");
                SQLDtl.AppendLine("Inner Join TblPaymentTerm I On F.PtCode=I.PtCode ");
                SQLDtl.AppendLine("Left Join TblDivision J On B.DivisionCode=J.DivisionCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine("And (A.CancelInd='N' And A.Status In ('O', 'A')) ");
                SQLDtl.AppendLine("Order By A.DNo; ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName" ,
                         "Qty",
                         "PurchaseUomCode" ,
                         "PtName",
                         "CurCode" ,

                         //6-10
                         "UPrice" ,
                         "Total" ,
                         "Remark",
                         "ForeignName",
                         "DivisionName",

                         //11-15
                         "DeptName",
                         "StatusDesc",
                         "VdName",
                         "Specification",
                         "MaterialRequestDocNo"
                        
                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new PORDtl()
                        {
                            nomor=nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),

                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            PtName = Sm.DrStr(drDtl, cDtl[4]),
                            CurCode = Sm.DrStr(drDtl, cDtl[5]),

                            UPrice = Sm.DrDec(drDtl, cDtl[6]),
                            Total = Sm.DrDec(drDtl, cDtl[7]),
                            Remark = Sm.DrStr(drDtl, cDtl[8]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[9]),
                            DivisionName = Sm.DrStr(drDtl, cDtl[10]),

                            DeptName = Sm.DrStr(drDtl, cDtl[11]),
                            StatusDesc = Sm.DrStr(drDtl, cDtl[12]),
                            VdName = Sm.DrStr(drDtl, cDtl[13]),
                            Specification = Sm.DrStr(drDtl, cDtl[14]),
                            MaterialRequestDocNo = Sm.DrStr(drDtl, cDtl[15]),

                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #region PORYK
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select  A.DocNo, SUM((A.Qty*G.UPrice)) As Total, Group_Concat(Distinct(K.DeptName)) as DeptName, Group_Concat(Distinct(E.ItName)) as ItName, ");
                SQLDtl3.AppendLine("O.PosName, M.UserName, DATE_FORMAT(Left(A.CreateDt,8),'%d %M %Y') as DocDt, P.RemarkTotal, ");
                SQLDtl3.AppendLine("Concat(IfNull(Q.ParValue, ''), M.UserCode, '.JPG') As Signature ");
                SQLDtl3.AppendLine("From TblPORequestDtl A ");
                SQLDtl3.AppendLine("Inner Join TblMaterialRequestDtl C On A.MaterialRequestDocNo=C.DocNo And A.MaterialRequestDNo=C.DNo ");
                SQLDtl3.AppendLine("Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQLDtl3.AppendLine("Inner Join TblQtHdr F On A.QtDocNo=F.DocNo ");
                SQLDtl3.AppendLine("Inner Join TblQtDtl G On A.QtDocNo=G.DocNo And A.QtDNo=G.DNo ");
                SQLDtl3.AppendLine("Inner Join TblVendor H On F.VdCode=H.VdCode ");
                SQLDtl3.AppendLine("Inner Join TblPaymentTerm I On F.PtCode=I.PtCode ");
                SQLDtl3.AppendLine("Inner Join TblMaterialRequestHdr J On A.MaterialRequestDocNo=J.DocNo ");
                SQLDtl3.AppendLine("Inner Join TblDepartment K On J.DeptCode=K.DeptCode ");
                SQLDtl3.AppendLine("Inner Join TblUser M On A.CreateBy=M.UserCode ");
                SQLDtl3.AppendLine("Left Join TblEmployee N On M.UserCode = N.UserCode ");
                SQLDtl3.AppendLine("Left Join TblPosition O On N.PosCode = O.PosCode ");
                SQLDtl3.AppendLine("Left Join TblPORequest2Hdr P On A.DocNo = P.DocNo ");
                SQLDtl3.AppendLine("Left Join TblParameter Q On Q.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl3.AppendLine("And (A.CancelInd='N' And A.Status In ('O', 'A')) ");
                SQLDtl3.AppendLine("Order By A.DNo; ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", DocNo);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0
                         "DocNo" ,

                         //1-5
                         "DocDt" ,
                         "DeptName",
                         "ItName",
                         "UserName",
                         "PosName",

                         //6-8
                         "Total",
                         "RemarkTotal",
                         "Signature",
                        });
                if (drDtl3.HasRows)
                {

                    while (drDtl3.Read())
                    {
                        l2.Add(new PORYK()
                        {
                            DocNo = Sm.DrStr(drDtl3, cDtl3[0]),
                            DocDt = Sm.DrStr(drDtl3, cDtl3[1]),
                            DeptName = Sm.DrStr(drDtl3, cDtl3[2]),
                            ItName = Sm.DrStr(drDtl3, cDtl3[3]),
                            UserName = Sm.DrStr(drDtl3, cDtl3[4]),
                            PosName = Sm.DrStr(drDtl3, cDtl3[5]),
                            Total = Sm.DrDec(drDtl3, cDtl3[6]),
                            RemarkTotal = Sm.DrStr(drDtl3, cDtl3[7]),
                            Signature = Sm.DrStr(drDtl3, cDtl3[8]),
                        });

                    }
                }
                drDtl3.Close();
            }
            myLists.Add(l2);

            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select distinct A.DocNo, E.ItName, A.Qty, E.PurchaseUomCode, G.UPrice, ");
                SQLDtl4.AppendLine("(A.Qty*G.UPrice) As Total, C.Remark, K.DeptName, A.Remark as Remark2,DATE_FORMAT(B.DocDt,'%d %M %Y') as RepairDt, ");
                SQLDtl4.AppendLine("O.ConditionDesc,O.Tax,O.TransferOfTitle,O.BasisForProposal, O.RemarkTotal, ");
                SQLDtl4.AppendLine("O.FormNo, O.RevisionNo,DATE_FORMAT(O.RevisionDt,'%d %M %Y') as RevisionDt, P.Brand ");
                SQLDtl4.AppendLine("From TblPORequestDtl A  ");
                SQLDtl4.AppendLine("Left Join  ");
				SQLDtl4.AppendLine("( ");
				SQLDtl4.AppendLine("Select A.DocDt, A.DocNo ");
                SQLDtl4.AppendLine("From TblPORequestHdr A ");
                SQLDtl4.AppendLine("Inner Join TblPORequestDtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo ");
                SQLDtl4.AppendLine("And Concat(B.MaterialRequestDocNo, B.MaterialRequestDNo) In ");
                SQLDtl4.AppendLine("( "); 
                SQLDtl4.AppendLine("Select Distinct Concat(X2.MaterialRequestDocNo, X2.MaterialRequestDNo) ");
                SQLDtl4.AppendLine("From TblPORequestHdr X1 ");
                SQLDtl4.AppendLine("Inner Join TblPORequestDtl X2 On X1.DocNo = X2.DocNo ");
                SQLDtl4.AppendLine("And X1.DocNo <> @DocNo ");
                SQLDtl4.AppendLine("And (X2.CancelInd = 'Y' Or X2.Status = 'C') ");
                SQLDtl4.AppendLine(")) B On A.DocNo= B.DocNo ");
                SQLDtl4.AppendLine("Inner Join TblMaterialRequestDtl C On A.MaterialRequestDocNo=C.DocNo And A.MaterialRequestDNo=C.DNo ");
                SQLDtl4.AppendLine("Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQLDtl4.AppendLine("Inner Join TblQtHdr F On A.QtDocNo=F.DocNo ");
                SQLDtl4.AppendLine("Inner Join TblQtDtl G On A.QtDocNo=G.DocNo And A.QtDNo=G.DNo ");
                SQLDtl4.AppendLine("Inner Join TblVendor H On F.VdCode=H.VdCode ");
                SQLDtl4.AppendLine("Inner Join TblPaymentTerm I On F.PtCode=I.PtCode ");
                SQLDtl4.AppendLine("Inner Join TblMaterialRequestHdr J On A.MaterialRequestDocNo=J.DocNo  ");
                SQLDtl4.AppendLine("Inner Join TblDepartment K On J.DeptCode=K.DeptCode  ");
                SQLDtl4.AppendLine("Left Join TblPORequest2Hdr O On A.DocNo=O.DocNo ");
                SQLDtl4.AppendLine("Left Join TblPORequest2Dtl P On A.DocNo=P.DocNo And P.MaterialRequestDocNo=C.DocNo And P.MaterialRequestDNo=C.DNo ");
                SQLDtl4.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl4.AppendLine("And (A.CancelInd='N' And A.Status In ('O', 'A')) ");
                SQLDtl4.AppendLine("Order By A.DNo; ");


                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", DocNo);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "DeptName" ,

                         //1-5
                         "ItName" ,
                         "Qty",
                         "PurchaseUomCode" ,
                         "UPrice" ,
                         "Total" ,

                         //6-10
                         "Remark",
                         "Remark2",
                         "RepairDt",
                         "ConditionDesc",
                         "Tax",

                         //11-15
                         "TransferOfTitle",
                         "BasisForProposal",
                         "FormNo",
                         "RevisionNo",
                         "RevisionDt",

                         //16-17
                         "RemarkTotal",
                         "Brand",
                        
                        });
                if (drDtl4.HasRows)
                {
                    int nomor = 0;
                    while (drDtl4.Read())
                    {
                        nomor = nomor + 1;
                        l3.Add(new PORYK2()
                        {
                            nomor = nomor,
                            DeptName = Sm.DrStr(drDtl4, cDtl4[0]),
                            ItName = Sm.DrStr(drDtl4, cDtl4[1]),
                            Qty = Sm.DrDec(drDtl4, cDtl4[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl4, cDtl4[3]),
                            UPrice = Sm.DrDec(drDtl4, cDtl4[4]),
                            Total = Sm.DrDec(drDtl4, cDtl4[5]),
                            Remark = Sm.DrStr(drDtl4, cDtl4[6]),
                            Remark2 = Sm.DrStr(drDtl4, cDtl4[7]),
                            RepairDt = Sm.DrStr(drDtl4, cDtl4[8]),
                            ConditionDesc = Sm.DrStr(drDtl4, cDtl4[9]),
                            Tax = Sm.DrDec(drDtl4, cDtl4[10]),
                            TransferOfTitle = Sm.DrStr(drDtl4, cDtl4[11]),
                            BasisForProposal = Sm.DrStr(drDtl4, cDtl4[12]),
                            FormNo = Sm.DrStr(drDtl4, cDtl4[13]),
                            RevisionNo = Sm.DrStr(drDtl4, cDtl4[14]),
                            RevisionDt = Sm.DrStr(drDtl4, cDtl4[15]),
                            Brand = Sm.DrStr(drDtl4, cDtl4[17])
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(l3);

            #region Detail Signature YK

            var cmDtl5 = new MySqlCommand();

            var SQLDtl5 = new StringBuilder();
            using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl5.Open();
                cmDtl5.Connection = cnDtl5;

                SQLDtl5.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl5.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt, T1.Remark ");
                SQLDtl5.AppendLine("From ( ");
                SQLDtl5.AppendLine("    Select Distinct ");
                SQLDtl5.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtl5.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt, 'Disetujui' As Remark ");
                SQLDtl5.AppendLine("    From TblPORequestDtl A ");
                SQLDtl5.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequest' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQLDtl5.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl5.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequest' ");
                SQLDtl5.AppendLine("    Left Join TblGroup E On C.GrpCode=E.GrpCode ");
                SQLDtl5.AppendLine("     Where A.CancelInd='N' And A.DocNo=@DocNo And E.GrpCode Not In ('"+ mGrpCodeForBOD +"') ");
                SQLDtl5.AppendLine(") T1 ");
                SQLDtl5.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl5.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl5.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl5.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                SQLDtl5.AppendLine("Order By T1.Level; ");

                cmDtl5.CommandText = SQLDtl5.ToString();
                Sm.CmParam<String>(ref cmDtl5, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl5, "@DocNo", DocNo);
                var drDtl5 = cmDtl5.ExecuteReader();
                var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt",

                         //6
                        "Remark"
                        });
                if (drDtl5.HasRows)
                {
                    while (drDtl5.Read())
                    {

                        l4.Add(new PORYKSign()
                        {
                            Signature = Sm.DrStr(drDtl5, cDtl5[0]),
                            UserName = Sm.DrStr(drDtl5, cDtl5[1]),
                            PosName = Sm.DrStr(drDtl5, cDtl5[2]),
                            Space = Sm.DrStr(drDtl5, cDtl5[3]),
                            DNo = Sm.DrStr(drDtl5, cDtl5[4]),
                            Title = Sm.DrStr(drDtl5, cDtl5[5]),
                            LastUpDt = Sm.DrStr(drDtl5, cDtl5[6]),
                            Remark = Sm.DrStr(drDtl5, cDtl5[7])
                        });
                    }
                }
                drDtl5.Close();
            }
            myLists.Add(l4);

             //Signature Direktur
            var cmDtl6 = new MySqlCommand();

            var SQLDtl6 = new StringBuilder();
            using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl6.Open();
                cmDtl6.Connection = cnDtl6;

                SQLDtl6.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl6.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt, T1.Remark ");
                SQLDtl6.AppendLine("From ( ");
                SQLDtl6.AppendLine("    Select Distinct ");
                SQLDtl6.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtl6.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt, 'Disetujui' as Remark ");
                SQLDtl6.AppendLine("    From TblPORequestDtl A ");
                SQLDtl6.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequest' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQLDtl6.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl6.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequest' ");
                SQLDtl6.AppendLine("    Left Join TblGroup E On C.GrpCode=E.GrpCode ");
                SQLDtl6.AppendLine("     Where A.CancelInd='N' And A.DocNo=@DocNo And E.GrpCode In ('" + mGrpCodeForBOD + "') ");
                SQLDtl6.AppendLine(") T1 ");
                SQLDtl6.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl6.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl6.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl6.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                SQLDtl6.AppendLine("Order By T1.DNo desc; ");

                cmDtl6.CommandText = SQLDtl6.ToString();
                Sm.CmParam<String>(ref cmDtl6, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl6, "@DocNo", DocNo);
                var drDtl6 = cmDtl6.ExecuteReader();
                var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt",

                         //6
                         "Remark"
                        });
                if (drDtl6.HasRows)
                {
                    while (drDtl6.Read())
                    {

                        l5.Add(new PORYKSign2()
                        {
                            Signature = Sm.DrStr(drDtl6, cDtl6[0]),
                            UserName = Sm.DrStr(drDtl6, cDtl6[1]),
                            PosName = Sm.DrStr(drDtl6, cDtl6[2]),
                            Space = Sm.DrStr(drDtl6, cDtl6[3]),
                            DNo = Sm.DrStr(drDtl6, cDtl6[4]),
                            Title = Sm.DrStr(drDtl6, cDtl6[5]),
                            LastUpDt = Sm.DrStr(drDtl6, cDtl6[6]),
                            Remark = Sm.DrStr(drDtl6, cDtl6[7])
                        });
                    }
                }
                drDtl6.Close();
            }
            myLists.Add(l5);
            #endregion
            #endregion


            #endregion

            #region Detail Signature

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                if (mDocTitle == "MNET")
                {
                    SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T1.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl2.AppendLine("From ( ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                    SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By ,' As Title, Left(B.LastUpDt, 8) As LastUpDt, C.Remark as PosName ");
                    SQLDtl2.AppendLine("    From TblPORequestDtl A ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequest' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequest' ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("    UNION ALL ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                    SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By ,' As Title, Left(B.LastUpDt, 8) As LastUpDt, C.Remark as PosName  ");
                    SQLDtl2.AppendLine("    From TblPORequestDtl A  ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequestTender' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequestTender' ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("    Union All ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                    SQLDtl2.AppendLine("    '00' As DNo, 0 As Level, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, B.Remark as PosName ");
                    SQLDtl2.AppendLine("    From TblPORequestDtl A ");
                    SQLDtl2.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine(") T1 ");
                    SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.Level ");
                    SQLDtl2.AppendLine("Order By T1.DNo asc; ");
                }
                else
                {
                    SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl2.AppendLine("From ( ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                    SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By ,' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("    From TblPORequestDtl A ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequest' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequest' ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("    UNION ALL ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                    SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By ,' As Title, Left(B.LastUpDt, 8) As LastUpDt  ");
                    SQLDtl2.AppendLine("    From TblPORequestDtl A  ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequestTender' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequestTender' ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("    Union All ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                    SQLDtl2.AppendLine("    '00' As DNo, 0 As Level, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("    From TblPORequestDtl A ");
                    SQLDtl2.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine(") T1 ");
                    SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.Level ");
                    SQLDtl2.AppendLine("Order By T1.DNo desc; ");
                }

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@Space", "                        ");
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {

                        ldtl2.Add(new PORDtl2()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[2]),
                            Space = Sm.DrStr(drDtl2, cDtl2[3]),
                            DNo = Sm.DrStr(drDtl2, cDtl2[4]),
                            Title = Sm.DrStr(drDtl2, cDtl2[5]),
                            LastUpDt = Sm.DrStr(drDtl2, cDtl2[6]),
                            LabelName = "", //Name       : 
                            LabelPos = "", //Position    : 
                            LabelDt = "Date : ",
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            if (mFormPrintOutPOR.Length <= 0) mFormPrintOutPOR = "PORequest";
            // KIM : PORKIM, YK : PORYK, else : PORequest

            Sm.PrintReport(mFormPrintOutPOR, myLists, TableName, false);

            //if (Sm.GetParameter("DocTitle") == "SIER")
                //Sm.PrintReport("PORequestSIER", myLists, TableName, false);
            //else
                //Sm.PrintReport("PORequest", myLists, TableName, false);
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblPORequestdtl " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePORevisionFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void ProcessUploadFile(string DocNo)
        {
                if (TxtFile.Text.Length == 0) return;

                var l = new List<Sm.UploadFileClass>();
                string RenameTo = string.Empty;
                bool isSuccessFile1 = true;
                FileInfo toUpload = null;
                #region init data FileName1
                if (TxtFile.Text.Length > 0)
                {
                    string FileName = TxtFile.Text;
                    toUpload = new FileInfo(string.Format(@"{0}", FileName));
                    string fExt = Path.GetExtension(toUpload.Name);
                    string fName = Path.GetFileNameWithoutExtension(toUpload.Name);
                    RenameTo = string.Concat(fName, "_", DocNo.Replace('/', '-'), fExt);
                    isSuccessFile1 = Sm.UploadFile(true, FileName, mFormatFTPClient, RenameTo, mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPasswordForFTPClient, mIsUploadFileRenamed);
                }
                #endregion

               
                if (isSuccessFile1)
                {
                    l.Add(new Sm.UploadFileClass()
                    {
                        DocNo = DocNo,
                        FileName = mIsUploadFileRenamed ? RenameTo : toUpload != null ? toUpload.Name : string.Empty
                    });
                    Sm.UpdateUploadedFile(true, ref l, "TblPORequestHdr");
                

                l.Clear();
                }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePropCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropCode, new Sm.RefreshLue1(Sl.SetLuePropCode));
        }

        private void LuePropCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }
        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPORequestDlg6(this));
        }
        private void LuePropCode_Leave(object sender, EventArgs e)
        {
            if (LuePropCode.Visible && fAccept && fCell.ColIndex == 41)
            {
                if (Sm.GetLue(LuePropCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 40].Value =
                    Grd1.Cells[fCell.RowIndex, 41].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 40].Value = Sm.GetLue(LuePropCode);
                    Grd1.Cells[fCell.RowIndex, 41].Value = LuePropCode.GetColumnValue("Col2");
                }
            }
        }

        private void LuePropCode_Validated(object sender, EventArgs e)
        {
            LuePropCode.Visible = false;
        }

        private void LblAdditionalInformation_Click(object sender, EventArgs e)
        {
           Sm.FormShowDialog(new FrmPORequestDlg4(this));            
        }

        private void LueProcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcType, new Sm.RefreshLue2(Sl.SetLueOption), "ProcurementType");
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        #endregion

        #endregion

        #region Class

        private class QtyCapability
        {
            public string QtDocNo { get; set; }
            public string QtDNo { get; set; }
            public decimal Qty { get; set; }
        }

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        private class POR
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Address { get; set; }
            public string Remark { get; set; }
            public string DeptName { get; set; }
            public decimal PtDay { get; set; }
            public string PrintBy { get; set; }
            public string SiteName { get; set; }
            public string LocalDocNo { get; set; }
            public string FooterImage { get; set; }
        }

        private class PORDtl
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string PtName { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Total { get; set; }
            public string Remark { get; set; }
            public string ForeignName { get; set; }
            public int nomor { get; set; }
            public string DivisionName { get; set; }
            public string DeptName { get; set; }
            public string StatusDesc { get; set; }
            public string VdName { get; set; }
            public string Specification { get; set; }
            public string MaterialRequestDocNo { get; set; }
        }

        private class PORDtl2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string LabelPos { get; set; }
            public string LabelName { get; set; }
            public string LabelDt { get; set; }
        }

        internal class PORYKHdr
        {
            public string RepairDt { get; set; }
            public string ConditionDesc { get; set; }
            public decimal Tax { get; set; }
            public string TransferOfTitle { get; set; }
            public string BasisForProposal { get; set; }
            public string FormNo { get; set; }
            public string RevisionNo { get; set; }
            public string RevisionDt { get; set; }
            public string RemarkTotal { get; set; }
        }

        internal class PORYKDtl
        {
            public string ItCode { get; set; }
            public string MaterialRequestDocNo { get; set; }
            public string MaterialRequestDNo { get; set; }
            public string Brand { get; set; }
        }

        private class PORYK
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DeptName { get; set; }
            public string ItName { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public decimal Total { get; set; }
            public string RemarkTotal { get; set; }
            public string Signature { get; set; }
        }

        private class PORYK2
        {
            public string DeptName { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Total { get; set; }
            public string Remark { get; set; }
            public string Remark2 { get; set; }
            public string RepairDt { get; set; }
            public string ConditionDesc { get; set; }
            public decimal Tax { get; set; }
            public string TransferOfTitle { get; set; }
            public string BasisForProposal { get; set; }
            public string FormNo { get; set; }
            public string RevisionNo { get; set; }
            public string RevisionDt { get; set; }
            public string Brand { get; set; }
            public int nomor { get; set; }
        }

        private class PORYKSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string Remark { get; set; }
        }

        private class PORYKSign2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string Remark { get; set; }
        }

        private class DroppingVendor
        {
            public string MaterialRequestDocNo { get; set; }
            public string ItCode { get; set; }
            public string DroppingRequestDocNo { get; set; }
            public string VdCode { get; set; }
        }

        #endregion

    }
}

