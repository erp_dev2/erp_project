﻿#region Update
/*
   13/09/2018 [TKG] tambah fasilitas untuk memilih bank account yg mau diproses.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmClosingBalanceInCashDaily : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmClosingBalanceInCashDailyFind FrmFind;
        internal bool mIsFilterByBankAccount = false, mIsEntityMandatory = false;

        #endregion

        #region Constructor

        public FrmClosingBalanceInCashDaily(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Daily Closing Balance in Cash";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterByBankAccount = Sm.GetParameterBoo("IsFilterByBankAccount");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0                        
                        "DNo",
                        
                        //1-5
                        "Bank Account Code",
                        "Bank Account Name",
                        "Amount",
                        "Entity",
                        "Description",

                        //6
                        ""
                    },
                     new int[] 
                    {
                        50, 
                        150, 450, 200, 150, 250,
                        20
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 5 }, false);
            if (!mIsEntityMandatory) Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
            Grd1.Cols[6].Move(0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, DteClosingDt, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, DteClosingDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, DteClosingDt, MeeRemark });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmClosingBalanceInCashDailyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteClosingDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string 
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ClosingBalanceInCashDaily", "TblClosingBalanceInCashDailyHdr"),
                ClosingBalanceInCashDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ClosingBalanceInCash", "TblClosingBalanceInCashHdr");
            
            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveClosingBalanceInCashDailyDtl(DocNo, Row));

            cml.Add(SaveClosingBalanceInCashDailyHdr(DocNo, ClosingBalanceInCashDocNo));
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteClosingDt, "Closing Date") ||
                IsGrdEmpty()||
                IsBankAccountAlreadyProcessed();
        }

        private bool IsBankAccountAlreadyProcessed()
        {
            string ClosingDt = Sm.GetDte(DteClosingDt), BankAcCode = string.Empty, Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    BankAcCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (BankAcCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.BankAcCode=@BankAcCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@BankAcCode0" + r.ToString(), BankAcCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            Sm.CmParamDt(ref cm, "@ClosingDt", ClosingDt);

            SQL.AppendLine("Select C.BankAcNm ");
            SQL.AppendLine("From TblClosingBalanceInCashDailyHdr A ");
            SQL.AppendLine("Inner Join TblClosingBalanceInCashDailyDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            SQL.AppendLine("Where A.ClosingDt=@ClosingDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();

            var BankAcName = Sm.GetValue(cm);

            if (BankAcName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank Account : " + BankAcName + Environment.NewLine +
                    "This bank account already processed.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return true;
            }
            return false;
        }

        private MySqlCommand SaveClosingBalanceInCashDailyHdr(string DocNo, string ClosingBalanceInCashDocNo)
        {
            var SQL = new StringBuilder();
            string ClosingDt = Sm.GetDte(DteClosingDt);

            SQL.AppendLine("Set @DNo:=0; ");

            SQL.AppendLine("Insert Into TblClosingBalanceInCashDtl(DocNo, DNo, BankAcCode, Amt, CreateBy, CreateDt)");
            SQL.AppendLine("Select @ClosingBalanceInCashDocNo, ");
            SQL.AppendLine("Right(Concat('00', Cast((@DNo:=@DNo+1) As Char(3))), 3), ");
            SQL.AppendLine("BankAcCode, Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblClosingBalanceInCashDailyDtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And BankAcCode Not In ( ");
            SQL.AppendLine("    Select BankAcCode ");
            SQL.AppendLine("    From TblClosingBalanceInCashHdr A, TblClosingBalanceInCashDtl B ");
            SQL.AppendLine("    Where A.Mth=@Mth ");
            SQL.AppendLine("    And A.Yr=@Yr ");
            SQL.AppendLine("    And A.DocNo=B.DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Insert Into TblClosingBalanceInCashHdr(DocNo, DocDt, Yr, Mth, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, @DocDt, @Yr, @Mth, @Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblClosingBalanceInCashDtl ");
            SQL.AppendLine("Where DocNo=@ClosingBalanceInCashDocNo Limit 1; ");

            SQL.AppendLine("Insert Into TblClosingBalanceInCashDailyHdr(DocNo, DocDt, ClosingDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ClosingDt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ClosingBalanceInCashDocNo", ClosingBalanceInCashDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ClosingDt", ClosingDt);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Mth", ClosingDt.Substring(4, 2));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(ClosingDt, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveClosingBalanceInCashDailyDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            string ClosingDt = Sm.GetDte(DteClosingDt);

            SQL.AppendLine("Insert Into TblClosingBalanceInCashDailyDtl(DocNo, DNo, BankAcCode, Amt, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @BankAcCode, @Amt, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblClosingBalanceInCashDtl Set ");
            SQL.AppendLine("    Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where BankAcCode=@BankAcCode ");
            SQL.AppendLine("And DocNo In ( ");
            SQL.AppendLine("    Select DocNo From TblClosingBalanceInCashHdr ");
            SQL.AppendLine("    Where Mth=@Mth ");
            SQL.AppendLine("    And Yr=@Yr ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Mth", ClosingDt.Substring(4, 2));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(ClosingDt, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowClosingBalanceInCashDailyHdr(DocNo);
                ShowClosingBalanceInCashDailyDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowClosingBalanceInCashDailyHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, ClosingDt, Remark From TblClosingBalanceInCashDailyHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "ClosingDt", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetDte(DteClosingDt, Sm.DrStr(dr, c[2]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowClosingBalanceInCashDailyDtl(string DocNo)
        {

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Dno, B.BankAcCode, Concat(C.BankAcNm, ' - ', C.BankAcNo) As BankAcName, B.Amt, ");
            SQL.AppendLine("E.EntName, ");
            SQL.AppendLine("Concat( ");
            SQL.AppendLine("    Case When D.BankName Is Not Null Then Concat(D.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("    Case When C.BankAcNo Is Not Null  ");
            SQL.AppendLine("    Then Concat(C.BankAcNo, ' [', IfNull(C.BankAcNm, ''), ']') ");
            SQL.AppendLine("    Else IfNull(C.BankAcNm, '') End ");
            SQL.AppendLine(") As BankAcDesc ");
            SQL.AppendLine("From TblClosingBalanceInCashDailyHdr A ");
            SQL.AppendLine("Inner Join TblClosingBalanceInCashDailyDtl B On A.DocNo = B.DOcNo  ");
            SQL.AppendLine("Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=C.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblBank D On C.BankCode=D.BankCode ");
            SQL.AppendLine("Left Join TblEntity E On C.EntCode=E.EntCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DocNo, B.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "BankAcCode", "BankAcName", "Amt", "EntName", "BankAcDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
           
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 6 && !Sm.IsDteEmpty(DteClosingDt, "Closing date"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmClosingBalanceInCashDailyDlg(this, Sm.GetDte(DteClosingDt)));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && !Sm.IsDteEmpty(DteClosingDt, "Closing date"))
                Sm.FormShowDialog(new FrmClosingBalanceInCashDailyDlg(this, Sm.GetDte(DteClosingDt)));
        }

        #endregion

        //#region Additional Method

        //internal void CalculateClosingBalance()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select T1.BankAcCode, T2.BankAcNm, T1.Amt, ");
        //    SQL.AppendLine("T4.EntName, ");
        //    SQL.AppendLine("Concat( ");
        //    SQL.AppendLine("    Case When T3.BankName Is Not Null Then Concat(T3.BankName, ' : ') Else '' End, ");
        //    SQL.AppendLine("    Case When T2.BankAcNo Is Not Null  ");
        //    SQL.AppendLine("    Then Concat(T2.BankAcNo, ' [', IfNull(T2.BankAcNm, ''), ']') ");
        //    SQL.AppendLine("    Else IfNull(T2.BankAcNm, '') End ");
        //    SQL.AppendLine(") As BankAcDesc ");
        //    SQL.AppendLine("From ( ");
        //    SQL.AppendLine("    Select T.BankAcCode, Sum(T.Amt) As Amt From (");
        //    SQL.AppendLine("        Select B.BankAcCode, B.Amt ");
        //    SQL.AppendLine("        From TblClosingBalanceInCashHdr A, TblClosingBalanceInCashDtl B ");
        //    SQL.AppendLine("        Where A.DocNo=B.DocNo And Concat(A.Yr, A.Mth)=@YrMth2 ");
        //    SQL.AppendLine("        Union All ");
        //    SQL.AppendLine("        Select A.BankAcCode, Case A.Actype When 'C' Then -1 Else 1 End * B.Amt As Amt ");
        //    SQL.AppendLine("        From TblVoucherHdr A, TblVoucherDtl B ");
        //    SQL.AppendLine("        Where A.DocNo = B.DocNo And A.CancelInd='N' And A.DocDt between Concat(@YrMth1, '01') And @ClosingDt ");
        //    SQL.AppendLine("        Union All ");
        //    SQL.AppendLine("        Select A.BankAcCode2, (Case A.Actype2 When 'C' Then -1 Else 1 End) * B.Amt * A.ExcRate As Amt ");
        //    SQL.AppendLine("        From TblVoucherHdr A, TblVoucherDtl B ");
        //    SQL.AppendLine("        Where A.DocNo=B.DocNo And A.CancelInd='N' And A.DocDt between Concat(@YrMth1, '01') And @ClosingDt And A.AcType2 Is Not Null ");
        //    SQL.AppendLine("    ) T Group By T.BankAcCode ");
        //    SQL.AppendLine(") T1 ");
        //    SQL.AppendLine("Inner Join TblBankAccount T2 On T1.BankAcCode=T2.BankAcCode ");
        //    if (mIsFilterByBankAccount)
        //    {
        //        SQL.AppendLine("And Exists( ");
        //        SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
        //        SQL.AppendLine("    Where BankAcCode=T2.BankAcCode ");
        //        SQL.AppendLine("    And GrpCode In ( ");
        //        SQL.AppendLine("        Select GrpCode From TblUser ");
        //        SQL.AppendLine("        Where UserCode=@UserCode ");
        //        SQL.AppendLine("    ) ");
        //        SQL.AppendLine(") ");
        //    }
        //    SQL.AppendLine("Left Join TblBank T3 On T2.BankCode=T3.BankCode ");
        //    SQL.AppendLine("Left Join TblEntity T4 On T2.EntCode=T4.EntCode ");
        //    SQL.AppendLine("Where T1.BankAcCode Not In ( ");
        //    SQL.AppendLine("    Select B.BankAcCode ");
        //    SQL.AppendLine("    From TblClosingBalanceInCashDailyHdr A ");
        //    SQL.AppendLine("    Inner Join TblClosingBalanceInCashDailyDtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("    Where A.ClosingDt=@ClosingDt ");
        //    SQL.AppendLine("    ); ");

        //    var cm = new MySqlCommand();

        //    string
        //        Mth = Sm.GetDte(DteClosingDt).Substring(4, 2),
        //        Yr = Sm.GetDte(DteClosingDt).Substring(0, 4),
        //        Mth2 = (Mth == "01") ? "12" : Sm.Right("0" + (int.Parse(Mth) - 1).ToString(), 2),
        //        Yr2 = (Mth == "01") ? ((int.Parse(Yr)) - 1).ToString() : Yr;

        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@YrMth1", Yr+Mth);
        //    Sm.CmParam<String>(ref cm, "@YrMth2", Yr2+Mth2);
        //    Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));

        //    Sm.ShowDataInGrid(
        //        ref Grd1, ref cm,
        //        SQL.ToString(),
        //        new string[] 
        //        { 
        //            "BankAcCode", 
        //            "BankAcNm", "Amt", "EntName", "BankAcDesc"
        //        },
        //        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
        //        {
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
        //            Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
        //            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
        //        }, true, false, true, false
        //    );
        //    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
        //    Sm.FocusGrd(Grd1, 0, 1);
        //}

        //#endregion

        #endregion

    }
}
