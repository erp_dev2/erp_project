﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControl2Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmShopFloorControl2 mFrmParent;
        private string mSQL = string.Empty, mDocDt = string.Empty;

        #endregion

        #region Constructor

        public FrmShopFloorControl2Dlg3(FrmShopFloorControl2 FrmParent, string DocDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            if (mFrmParent.mIsSFCShowLeaveRemark)
            {
                SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, A.ResignDt, ");
                SQL.AppendLine("Case When IfNull(D.NumberOfDay, 0)>0 Then  Concat(Convert(Format(D.NumberOfDay, 2) Using utf8), ' Day') Else ");
                SQL.AppendLine("    Case When IfNull(D.NumberOfHour, 0)>0 Then  Concat(Convert(Format(D.NumberOfHour, 2) Using utf8), ' Hour') Else Null End ");
                SQL.AppendLine("End As LeaveRemark ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("   Select EmpCode, Sum(NumberOfDay) NumberOfDay, Sum(NumberOfHour) NumberOfHour ");
                SQL.AppendLine("   From ( ");
                SQL.AppendLine("       Select T.EmpCode, ");
                SQL.AppendLine("       Case When T.LeaveType='F' Then 1 Else 0 End As NumberOfDay, ");
                SQL.AppendLine("       Case When T.LeaveType<>'F' Then T.DurationHour Else 0 End As NumberOfHour ");
                SQL.AppendLine("       From TblEmpLeaveHdr T ");
                SQL.AppendLine("       Where T.CancelInd='N' ");
                SQL.AppendLine("       And IfNull(T.Status, 'O')<>'C' ");
                SQL.AppendLine("       And Exists( ");
                SQL.AppendLine("           Select Distinct DocNo From TblEmpLeaveDtl ");
                SQL.AppendLine("           Where DocNo=T.DocNo And LeaveDt=@DocDt ");
                SQL.AppendLine("       ) ");
                SQL.AppendLine("   Union All ");
                SQL.AppendLine("       Select T2.EmpCode, ");
                SQL.AppendLine("       Case When T1.LeaveType='F' Then 1 Else 0 End As NumberOfDay, ");
                SQL.AppendLine("       Case When T1.LeaveType<>'F' Then T1.DurationHour Else 0 End As NumberOfHour ");
                SQL.AppendLine("       From TblEmpLeave2Hdr T1 ");
                SQL.AppendLine("       Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("       Where T1.CancelInd='N' ");
                SQL.AppendLine("       And IfNull(T1.Status, 'O')<>'C' ");
                SQL.AppendLine("       And Exists( ");
                SQL.AppendLine("           Select Distinct DocNo From TblEmpLeave2Dtl2 ");
                SQL.AppendLine("           Where DocNo=T1.DocNo And LeaveDt=@DocDt ");
                SQL.AppendLine("       ) ");
                SQL.AppendLine("   ) Tbl Group By EmpCode ");
                SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
                SQL.AppendLine("Where (A.ResignDt Is Null Or ");
                SQL.AppendLine("(A.ResignDt Is Not Null And A.ResignDt>Left(@DocDt, 8))) ");
            }
            else
            {
                SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, A.ResignDt, Null As LeaveRemark ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
                SQL.AppendLine("Where (A.ResignDt Is Null Or ");
                SQL.AppendLine("(A.ResignDt Is Not Null And A.ResignDt>Left(@DocDt, 8))) ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Code", 
                    "Direct Labor", 
                    "Old Code",
                    "Position",

                    //6-8
                    "Department",
                    "Resign Date",
                    "Leave" + Environment.NewLine + "Remark"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 80, 300, 80, 150,
                    
                    //6-8
                    150, 100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 8 }, mFrmParent.mIsSFCShowLeaveRemark);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And Position(Concat('##', A.EmpCode, '##') In @SelectedEmployee)<1 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedEmployee", mFrmParent.GetSelectedEmployee());
                Sm.CmParamDt(ref cm, "@DocDt", mDocDt);

                Sm.FilterStr(ref Filter, ref cm, TxtBomCode.Text, new string[] { "A.EmpCode", "A.EmpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By C.DeptName, A.EmpName;",
                        new string[] 
                        { 
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "ResignDt",

                            //6
                            "LeaveRemark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 9, Grd1, Row2, 8);
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd2, Row1, new int[] { 7 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, Row1, new int[] { 8 });

                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 7 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 8 });
                    }
                }
                mFrmParent.SetDNo(mFrmParent.Grd2);
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        private bool IsCodeAlreadyChosen(int Row)
        {
            string EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 2), EmpCode))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBomCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBomCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion

    }
}
