﻿namespace RunSystem
{
    partial class FrmRptAgingAPPaidItem2Dlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptAgingAPPaidItem2Dlg));
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtVdInvNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtInvoiceAmt = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDownpayment = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtPaidAmt = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtBalance = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtType = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnInvoice = new DevExpress.XtraEditors.SimpleButton();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdInvNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvoiceAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDownpayment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 315);
            this.panel3.Size = new System.Drawing.Size(842, 71);
            // 
            // Grd1
            // 
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(842, 71);
            this.Grd1.TabIndex = 31;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnInvoice);
            this.panel2.Controls.Add(this.TxtType);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtBalance);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtPaidAmt);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtDownpayment);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtCurCode);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.TxtInvoiceAmt);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtVdCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtVdInvNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.Grd2);
            this.panel2.Size = new System.Drawing.Size(842, 315);
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 196);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.RowMode = true;
            this.Grd2.RowModeHasCurCell = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(842, 119);
            this.Grd2.TabIndex = 30;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterRowStateChanged += new TenTec.Windows.iGridLib.iGAfterRowStateChangedEventHandler(this.Grd2_AfterRowStateChanged);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // TxtVdInvNo
            // 
            this.TxtVdInvNo.EnterMoveNextControl = true;
            this.TxtVdInvNo.Location = new System.Drawing.Point(118, 46);
            this.TxtVdInvNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdInvNo.Name = "TxtVdInvNo";
            this.TxtVdInvNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVdInvNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdInvNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdInvNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVdInvNo.Properties.MaxLength = 30;
            this.TxtVdInvNo.Properties.ReadOnly = true;
            this.TxtVdInvNo.Size = new System.Drawing.Size(155, 20);
            this.TxtVdInvNo.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(8, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Vendor\'s Invoice#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(118, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(155, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(42, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(118, 67);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 30;
            this.TxtVdCode.Properties.ReadOnly = true;
            this.TxtVdCode.Size = new System.Drawing.Size(441, 20);
            this.TxtVdCode.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(68, 70);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 14);
            this.label2.TabIndex = 18;
            this.label2.Text = "Vendor";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(118, 88);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 3;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(72, 20);
            this.TxtCurCode.TabIndex = 21;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(60, 91);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 14);
            this.label24.TabIndex = 20;
            this.label24.Text = "Currency";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvoiceAmt
            // 
            this.TxtInvoiceAmt.EnterMoveNextControl = true;
            this.TxtInvoiceAmt.Location = new System.Drawing.Point(118, 109);
            this.TxtInvoiceAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvoiceAmt.Name = "TxtInvoiceAmt";
            this.TxtInvoiceAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvoiceAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvoiceAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvoiceAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtInvoiceAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvoiceAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvoiceAmt.Properties.ReadOnly = true;
            this.TxtInvoiceAmt.Size = new System.Drawing.Size(202, 20);
            this.TxtInvoiceAmt.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(21, 112);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 14);
            this.label6.TabIndex = 22;
            this.label6.Text = "Invoice Amount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDownpayment
            // 
            this.TxtDownpayment.EnterMoveNextControl = true;
            this.TxtDownpayment.Location = new System.Drawing.Point(118, 130);
            this.TxtDownpayment.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDownpayment.Name = "TxtDownpayment";
            this.TxtDownpayment.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDownpayment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDownpayment.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDownpayment.Properties.Appearance.Options.UseFont = true;
            this.TxtDownpayment.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDownpayment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDownpayment.Properties.ReadOnly = true;
            this.TxtDownpayment.Size = new System.Drawing.Size(202, 20);
            this.TxtDownpayment.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(28, 133);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "Downpayment";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaidAmt
            // 
            this.TxtPaidAmt.EnterMoveNextControl = true;
            this.TxtPaidAmt.Location = new System.Drawing.Point(118, 151);
            this.TxtPaidAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidAmt.Name = "TxtPaidAmt";
            this.TxtPaidAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPaidAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPaidAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPaidAmt.Properties.ReadOnly = true;
            this.TxtPaidAmt.Size = new System.Drawing.Size(202, 20);
            this.TxtPaidAmt.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(38, 154);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 26;
            this.label5.Text = "Paid Amount";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBalance
            // 
            this.TxtBalance.EnterMoveNextControl = true;
            this.TxtBalance.Location = new System.Drawing.Point(118, 172);
            this.TxtBalance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBalance.Name = "TxtBalance";
            this.TxtBalance.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBalance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBalance.Properties.Appearance.Options.UseFont = true;
            this.TxtBalance.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBalance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBalance.Properties.ReadOnly = true;
            this.TxtBalance.Size = new System.Drawing.Size(202, 20);
            this.TxtBalance.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(67, 175);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 14);
            this.label7.TabIndex = 28;
            this.label7.Text = "Balance";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtType
            // 
            this.TxtType.EnterMoveNextControl = true;
            this.TxtType.Location = new System.Drawing.Point(118, 25);
            this.TxtType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtType.Name = "TxtType";
            this.TxtType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtType.Properties.Appearance.Options.UseFont = true;
            this.TxtType.Properties.MaxLength = 16;
            this.TxtType.Properties.ReadOnly = true;
            this.TxtType.Size = new System.Drawing.Size(155, 20);
            this.TxtType.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(80, 28);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 14;
            this.label8.Text = "Type";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnInvoice
            // 
            this.BtnInvoice.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnInvoice.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInvoice.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInvoice.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInvoice.Appearance.Options.UseBackColor = true;
            this.BtnInvoice.Appearance.Options.UseFont = true;
            this.BtnInvoice.Appearance.Options.UseForeColor = true;
            this.BtnInvoice.Appearance.Options.UseTextOptions = true;
            this.BtnInvoice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInvoice.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnInvoice.Image = ((System.Drawing.Image)(resources.GetObject("BtnInvoice.Image")));
            this.BtnInvoice.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnInvoice.Location = new System.Drawing.Point(276, 4);
            this.BtnInvoice.Name = "BtnInvoice";
            this.BtnInvoice.Size = new System.Drawing.Size(24, 21);
            this.BtnInvoice.TabIndex = 13;
            this.BtnInvoice.ToolTip = "Show Invoice Information";
            this.BtnInvoice.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnInvoice.ToolTipTitle = "Run System";
            this.BtnInvoice.Click += new System.EventHandler(this.BtnInvoice_Click);
            // 
            // FrmRptAgingAPPaidItem2Dlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 386);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmRptAgingAPPaidItem2Dlg";
            this.Text = "Invoice Information";
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdInvNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvoiceAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDownpayment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtType.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.TextEdit TxtVdInvNo;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtBalance;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtPaidAmt;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtDownpayment;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtInvoiceAmt;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtType;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnInvoice;
    }
}