﻿#region Update
/*
    31/07/2019 [TKG] Filter berdasarkan site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBOQDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBOQ mFrmParent;
        private string mSQL = string.Empty, mCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmBOQDlg(FrmBOQ FrmParent, string CurCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurCode = CurCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Bill of Quantity";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CtName, A.CtContactPersonName ");
            SQL.AppendLine("From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblLOPHdr C On A.LopDocNo=C.DocNo ");
                SQL.AppendLine("    And (C.SiteCode Is Null Or ( ");
                SQL.AppendLine("        C.SiteCode Is Not Null ");
                SQL.AppendLine("        And Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupSite ");
                SQL.AppendLine("            Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("            And GrpCode In ( ");
                SQL.AppendLine("                Select GrpCode From TblUser ");
                SQL.AppendLine("                Where UserCode=@UserCode ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    )) ");
            }
            SQL.AppendLine("Where A.CtCode=@CtCode ");
            SQL.AppendLine("And A.CurCode=@CurCode ");
            SQL.AppendLine("And A.ActInd='Y' ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Customer",
                    "Contact"+Environment.NewLine+"Person",
                },
                new int[]
                {
                    50,
                    150, 20, 80, 200, 200
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBOQ(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmBOQ");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmBOQ(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmBOQ");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(mFrmParent.LueCtCode));
                Sm.CmParam<String>(ref cm, "@CurCode", mCurCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocDt Desc, A.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CtName", "CtContactPersonName"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtCtReplace.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ClearGrd();
                mFrmParent.LoadAllItemBOQ();
                this.Hide();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
