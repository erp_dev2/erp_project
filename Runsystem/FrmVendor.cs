﻿#region Update
/*
    21/12/2017 [ARI] tambah tab item category
    09/03/2018 [WED] sinkron ke e-procurement
    09/03/2018 [WED] tambah send ke email calon vendor
    25/04/2018 [WED] tambah tab sector
    15/05/2018 [WED] tambah tab rating
    18/05/2018 [HAR] tambah tab vendor expertise dan vendor project historical
    26/04/2018 [HAR] tambah inputan NIB (Nomor Induk Berusaha)
    05/06/2018 [WED] tambah tab untuk view uploaded file
    06/06/2018 [WED] field dari vendor register di tambah
    11/06/2018 [ARI] tambah tab deed, tamabahan kolom CP, tambah di Expertise
    02/07/2018 [WED] Contact Person, tambah kolom ID Number (KTP)
    02/07/2018 [WED] Uploaded File, tambah kolom Local Document# dan Licenced By
    02/07/2018 [WED] header tambah field Website
    03/07/2018 [WED] tambah Head Office
    29/08/2018 [TKG] bug saat insert vendor code pertama kali
    03/09/2018 [WED] tambah combobox status vendor, berdasarkan optcat = VendorProcurementStatus
    21/09/2018 [WED] tambah establishment year, vendor project history, Meeremark modifier nya jadi internal
    01/04/2019 [HAR] tambah tab vehicle buat nampung licence number
    04/04/2019 [HAR] di tab vehicle tambah nomor lambung dan tranpor type
    10/05/2019 [DITA] pada lue Head Office hanya vendor yg aktif yg ditampilkan
    09/10/2019 [WED/SIER] param baru untuk nyimpen setting email vendor register
    15/01/2019 [DITA/YK] format vdcode baru untuk yk
    14/07/2020 [VIN/SIER] Hide tab deed dan vehicle
    13/11/2020 [WED/SIER] generate COA Vendor, untuk level dan parent nya berdasarkan parameter IsVdCodeUseVdCt
    12/01/2021 [IBL/SRN] tambah field localcode, berdasarkan parameter IsVendorUseLocalCode
    22/02/2021 [WED/IOK] tax otomatis ter tick saat insert vendor dengan category tertentu di parameter VendorCategoryCodeForTaxAutoTick
    12/07/2021 [WED/PADI] generate vendor code otomatis menggunakan short code berdasarkan parameter IsVdCodeUseShortCode
    01/09/2021 [WED/PADI] tambah informasi Project Experience di tab Vendor Expertise berdasarkan parameter IsUseECatalog
    02/09/2021 [WED/PADI] informasi di Vendor Project Historical disamakan dengan runmarket menu Project Experience, berdasarkan parameter IsUseECatalog
    07/09/2021 [WED/PADI] tambah RegisterID dari runmarket
    28/10/2021 [YOG/PHT] Menambahkan field 'Sub Sector' di detail menu master vendor untuk memasukan katagori dari masing-masing vendor sektor
    15/11/2021 [NJP/RM] Menambahkan Parameter VdRegisterEmailSignature unt Email signature untuk email registrasi vendor
    30/11/2021 [NJP/RM] Saat tarik data dari Vendor Register via lup, tidak perlu isi tab Item Category dan Tab Sector, serta gak perlu di readonly kedua grid tsb
    15/09/2022 [SET/PHT] add field Run Market's Vendor Code
    03/01/2022 [ICA/MNET] menambah field Entity Type berdasarkan parameter IsVendorUseEntityType dan IsVendorEntityTypeMandatory
    05/01/2022 [ICA/HEX] Bug : field di header tidak ikut bergerak alias freeze, ketika windows dinaikan
    11/01/2023 [VIN/ALL] event Lue hilang
    13/01/2023 [VIN/ALL] event Lue hilang
    13/01/2023 [TYO/VIR] dissable auto create vendor coa berdasarkan parameter IsMasterVendorAutocreateVendorCOA
    19/01/2023 [TYO/VIR] merubah parameter IsMasterVendorAutocreateVendorCOA menjadi IsMasterVendorNotAutocreateVendorCOA
    16/02/2023 [MYA/MNET] Menambahkan karakter name di master Vendor menjadi 100 karakter
    28/02/2023 [SET/HEX] add field Vendor External Code & Selection Score berdasar param IsMasterVendorUseExternalCode & IsMasterVendorUseSelectionScore
    28/03/2023 [HAR/HEX] bug RM vdCode tidak muncul
    17/04/2023 [RDA/HEX] tambah field department (non mandatory) berdasarkan param mIsVendorUseDepartment
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

// email
using System.Net;
using System.Net.Security;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

// download file
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmVendor : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mVdCode = string.Empty, 
            mVdRegisterEmail = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty
            ;
        private string 
            mVdPassword = string.Empty,
            mVdRegisterEmailFrom = string.Empty,
            mVdRegisterEmailPassword = string.Empty,
            mVdRegisterEmailPort = string.Empty,
            mVdRegisterEmailHost = string.Empty,
            mGenerateVendorCode = string.Empty,
            mVendorCategoryCodeForTaxAutoTick = string.Empty,
            mRegisterID = string.Empty
            ;
        internal FrmVendorFind FrmFind;
        iGCell fCell;
        bool fAccept;
        private bool
            mIsVdCodeAutoIncrement = true,
            mIsVdCodeUseVdCt = false,
            mIsVendorUseProcurementRegistration = false,
            mIsVdRegisterEmailUseSSL = false,
            mIsVendorDeedNotShow = true,
            mIsVendorVehicleNotShow = true,
            mIsVendorUseLocalCode = false,
            IsInsert = false,
            mIsVdCodeUseShortCode = false,
            mMasterVendorUseRunMarketCode = false,
            mIsVendorEntityTypeMandatory = false,
            mIsMasterVendorNotAutocreateVendorCOA = false,
            mIsVendorUseDepartment = false
            ;
        internal bool
            mIsVendorUseEntityType = false, 
            mIsUseECatalog = false,
            mIsMasterVendorUseExternalCode = false,
            mIsMasterVendorUseSelectionScore = false;
          
        internal byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmVendor(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);

                Sl.SetLueVdCtCode(ref LueVdCtCode);
                Sl.SetLueCityCode(ref LueCityCode);
                if (mIsVdCodeUseVdCt) LblVdCtCode.ForeColor = Color.Red;
                Sl.SetLueOption(ref LueProcurementStatus, "VendorProcurementStatus");
                Sl.SetLueOption(ref LueEntityType, "VendorEntityType");

                tabControl1.SelectTab("TpgVdDeed");
                SetGrd9();

                tabControl1.SelectTab("TpgUploadedFile");
                SetGrd8();

                tabControl1.SelectTab("TpgVdRating");
                SetLueRating(ref LueRating);
                SetLueVdRating(ref LueVdRating);
                LueRating.Visible = false;
                LueVdRating.Visible = false;
                SetGrd5();

                tabControl1.SelectTab("TpgVdSector");
                SetLueSector(ref LueSector);
                SetLueQualification(ref LueQualification);
                SetLueSubSectorCode(ref LueSubSectorCode, string.Empty);
                LueQualification.Visible = false;
                LueSector.Visible = false;
                LueSubSectorCode.Visible = false;
                SetGrd4();

                tabControl1.SelectTab("TpgBank");
                Sl.SetLueBankCode(ref LueBankCode);
                LueBankCode.Visible = false;
                SetGrd2();

                tabControl1.SelectTab("TpgItemCategory");
                SetGrd3();

                tabControl1.SelectTab("TpgProfile");
                Sl.SetLueCityCode(ref LueBirthPlace);
                LueBirthPlace.Visible = false;
                DteBirthDt.Visible = false;
                MeeEducation.Visible = false;
                MeeCertificate.Visible = false;
                MeeLanguage.Visible = false;
                MeeExperience.Visible = false;
                MeeAddress2.Visible = false;
                Sl.SetLueGender(ref LueGender);
                LueGender.Visible = false;
                Sl.SetLueCntCode(ref LueCountry);
                LueCountry.Visible = false;
                SetGrd6();

                tabControl1.SelectTab("TpgProject");
                SetLueProfile(ref LueProfile);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueOption(ref LueProgressCode, "VendorProgress");
                LueProfile.Visible = false;
                LueCurCode.Visible = false;
                LueProgressCode.Visible = false;
                MeeProject.Visible = false;
                DteContractDt.Visible = false;
                DteContractEndDt.Visible = false;
                SetGrd7();

                tabControl1.SelectTab("TpgVehicle");
                SetLueTranpsortType(ref LueTransportType);
                LueTransportType.Visible = false;
                SetGrd10();


                tabControl1.SelectTab("TpgContactPerson");
                SetLueType(ref LueType);
                LueType.Visible = false;
                SetGrd();

                SetLueParent(ref LueParent);
                if(mIsVendorVehicleNotShow)
                    tabControl1.TabPages.Remove(TpgVehicle);
                if(mIsVendorDeedNotShow)
                    tabControl1.TabPages.Remove(TpgVdDeed);

                if (!mIsVendorUseLocalCode)
                {
                    label21.Visible = TxtVdCodeInternal.Visible = false;
                    tabControl1.Size = new Size(0, 247);
                    if (mMasterVendorUseRunMarketCode)
                    {
                        MoveComponent(ref LblVdCtCode, 79, 90); MoveComponent(ref LueVdCtCode, 141, 88);
                        MoveComponent(ref label15, 85, 111); MoveComponent(ref MeeAddress, 141, 109);
                        MoveComponent(ref label4, 108, 132); MoveComponent(ref LueCityCode, 141, 130);
                        MoveComponent(ref label16, 66, 153); MoveComponent(ref LueSDCode, 141, 151);
                        MoveComponent(ref label14, 94, 174); MoveComponent(ref LueVilCode, 141, 172);
                        MoveComponent(ref label6, 64, 195); MoveComponent(ref TxtPostalCd, 141, 193);
                        MoveComponent(ref label18, 83, 216); MoveComponent(ref TxtWebsite, 141, 214);
                        MoveComponent(ref label19, 64, 237); MoveComponent(ref LueParent, 141, 235);
                        MoveComponent(ref label20, 24, 258); MoveComponent(ref TxtEstablishedYr, 141, 256);
                    }
                    else
                    {
                        label22.Visible = TxtRMVdCode.Visible = false;
                        //label21.Visible = TxtVdCodeInternal.Visible = false;
                        //tabControl1.Size = new Size(0, 259);
                        MoveComponent(ref LblVdCtCode, 79, 69); MoveComponent(ref LueVdCtCode, 141, 67);
                        MoveComponent(ref label15, 85, 88); MoveComponent(ref MeeAddress, 141, 88);
                        MoveComponent(ref label4, 108, 110); MoveComponent(ref LueCityCode, 141, 109);
                        MoveComponent(ref label16, 66, 131); MoveComponent(ref LueSDCode, 141, 130);
                        MoveComponent(ref label14, 94, 153); MoveComponent(ref LueVilCode, 141, 151);
                        MoveComponent(ref label6, 64, 174); MoveComponent(ref TxtPostalCd, 141, 172);
                        MoveComponent(ref label18, 83, 195); MoveComponent(ref TxtWebsite, 141, 193);
                        MoveComponent(ref label19, 64, 216); MoveComponent(ref LueParent, 141, 214);
                        MoveComponent(ref label20, 24, 236); MoveComponent(ref TxtEstablishedYr, 141, 235);
                    }
                }
                else
                {
                    if (!mMasterVendorUseRunMarketCode)
                    {
                        label22.Visible = TxtRMVdCode.Visible = false;
                        MoveComponent(ref label21, 25, 69); MoveComponent(ref TxtVdCodeInternal, 141, 67);
                        MoveComponent(ref LblVdCtCode, 79, 90); MoveComponent(ref LueVdCtCode, 141, 88);
                        MoveComponent(ref label15, 85, 111); MoveComponent(ref MeeAddress, 141, 109);
                        MoveComponent(ref label4, 108, 132); MoveComponent(ref LueCityCode, 141, 130);
                        MoveComponent(ref label16, 66, 153); MoveComponent(ref LueSDCode, 141, 151);
                        MoveComponent(ref label14, 94, 174); MoveComponent(ref LueVilCode, 141, 172);
                        MoveComponent(ref label6, 64, 195); MoveComponent(ref TxtPostalCd, 141, 193);
                        MoveComponent(ref label18, 83, 216); MoveComponent(ref TxtWebsite, 141, 214);
                        MoveComponent(ref label19, 64, 237); MoveComponent(ref LueParent, 141, 235);
                        MoveComponent(ref label20, 24, 258); MoveComponent(ref TxtEstablishedYr, 141, 256);
                    }
                }

                //if (!mMasterVendorUseRunMarketCode)
                //{
                //    label22.Visible = TxtRMVdCode.Visible = false;
                //}

                if (!mIsVendorUseEntityType) label23.Visible = LueEntityType.Visible = false;
                else
                {
                    if (mIsVendorEntityTypeMandatory) label23.ForeColor = Color.Red;
                    if(!mIsVendorUseProcurementRegistration)
                    {
                        label23.Top -= 49;
                        LueEntityType.Top -= 49;
                    }
                }

                if(!mIsMasterVendorUseExternalCode)
                {
                    LblVdExternalCode.Visible = TxtVdExternalCode.Visible = false;
                }
                if(!mIsMasterVendorUseSelectionScore)
                {
                    LblSelectionScore.Visible = TxtSelectionScore.Visible = false;
                }
                if (!mIsVendorUseEntityType && mIsMasterVendorUseExternalCode)
                {
                    LblVdExternalCode.Top -= 21; TxtVdExternalCode.Top -= 21;
                }
                if (!mIsVendorUseEntityType && mIsMasterVendorUseSelectionScore)
                {
                    LblSelectionScore.Top -= 21; TxtSelectionScore.Top -= 21;
                }

                Sl.SetLueDeptCode(ref LueDeptCode);
                if (!mIsVendorUseDepartment) label24.Visible = LueDeptCode.Visible = false;

                base.FrmLoad(sender, e);
               
                //if this application is called from other application
                if (mVdCode.Length != 0)
                {
                    ShowData(mVdCode);
                    if (mMenuCode.Length == 0)
                        BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                    else
                        SetFormControl(mState.Edit);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Contact Person Name",
                        //1-5
                        "ID Number",
                        "Position",
                        "Contact Number",
                        "Type Code",
                        "Type"
                    },
                     new int[] 
                    {
                        300, 180, 200, 200, 0, 150
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 4 });
        }

        private void SetGrd2()
        {
            Grd2.Cols.Count = 5;
            Grd2.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Code", "Bank Name", "Branch Name", "Account Name", "Account Number" },
                    new int[] { 0, 200, 150, 200, 180 }
                );
        }

        private void SetGrd3()
        {
            Grd3.Cols.Count = 3;
            Grd3.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd3, 
                new string[] 
                { 
                    "",
                    "Item's Category Code", 
                    "Item's Category Name" 
                }, new int[] 
                { 
                   20, 0, 300 
                }
             );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });
        }

        private void SetGrd4()
        {
            Grd4.Cols.Count = 6;
            Grd4.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[] 
                { 
                    "Sector Code", 
                    "Sector Name", 
                    "Sub Sector Code",
                    "Sub Sector Name",
                    "QualificationCode",
                    "Qualification"
                }, new int[] 
                { 
                   0, 300, 0, 200, 0, 200
                }
             );
            Sm.GrdColReadOnly(Grd4, new int[] { 0, 2, 4 });
        }

        private void SetGrd5()
        {
            Grd5.Cols.Count = 4;
            Grd5.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd5,
                new string[] 
                { 
                    "Indicator Code", 
                    "Rating Indicator", "Rating Assesment Code", "Value"
                }, new int[] 
                { 
                   0, 300, 0, 200
                }
             );
            Sm.GrdColReadOnly(Grd5, new int[] { 0, 2 });
            Sm.GrdColInvisible(Grd5, new int[] { 0, 2 });
        }

        private void SetGrd6() // Vendor Expertise
        {
            Grd6.Cols.Count = 24;
            Grd6.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd6,
                new string[] 
                { 
                    //0
                    "Dno",
                    //1-5
                    "Profil Code", "Profil Name", "Active", "Position", "Company",   
                    //6-10
                    "CityCode", "Birth Place", "Birth Date", "ID", "Address",  
                    //11-15
                    "NPWP", "Education", "Certificate", "Language"+Environment.NewLine+"Proficiency", "Working"+Environment.NewLine+"Experience",  
                    //16-20
                    "Original", "CountryCode", "Country", "Gendercode", "Gender", 
                    //21-23
                    "Email", "Project Experience", ""
                }, new int[] 
                { 
                   20, 
                   80, 200, 50, 150, 150, 
                   80, 150, 100, 150, 200,   
                   150, 250, 250, 120, 250,
                   80, 50, 150, 20, 100, 
                   150,200, 20
                }
             );
            Sm.GrdColReadOnly(Grd6, new int[] { 0 });
            Sm.GrdColInvisible(Grd6, new int[] { 0, 1, 6, 17, 19 });
            if (!mIsUseECatalog) Sm.GrdColInvisible(Grd6, new int[] { 22, 23 });
            Sm.GrdColButton(Grd6, new int[] { 23 });
            Sm.GrdFormatDate(Grd6, new int[] { 8 });
            Sm.GrdColCheck(Grd6, new int[] { 3, 16 });
        }

        private void SetGrd7() // Historical Project
        {
            Grd7.Cols.Count = 19;
            Grd7.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd7,
                new string[] 
                { 
                    //0
                    "Dno",
                    //1-5
                    "Profil Code", "Profile Name", "Project Experience", "Project Category", "Contract Number",
                    //6-10
                    "Contract Date", "Owner", "Owner Address", "Project Location", "Results",
                    //11-15
                    "Project Value", "CurCode", "Currency", "End of Contract", "Work Achievement",
                    //16-18
                    "ProgressCode", "Progress", ""
                }, new int[] 
                { 
                   40, 
                   80, 200, 400, 400, 120,
                   120, 120, 200, 120, 120,
                   120, 0, 100, 120, 120,
                   0, 120, 20
                }
             );
            Sm.GrdColReadOnly(Grd7, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd7, new int[] { 0, 1, 12, 16 });
            Sm.GrdFormatDec(Grd7, new int[] { 11 }, 0);
            Sm.GrdFormatDate(Grd7, new int[] { 6, 14 });
            Sm.GrdColButton(Grd7, new int[] { 18 });
            if (mIsUseECatalog) Sm.GrdColInvisible(Grd7, new int[] { 1, 2 });
            else Sm.GrdColInvisible(Grd7, new int[] { 18 });
        }

        private void SetGrd8()
        {
            Grd8.Cols.Count = 10;
            Grd8.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd8,
                new string[] 
                { 
                    //0
                    "CategoryCode",
                    //1-5
                    "Category", "File Name", "", "Valid", "Not Valid", 
                    //6-9
                    "Valid From", "Valid To", "Local Document#", "Licensed By"
                }, new int[] 
                { 
                   0, 
                   200, 200, 20, 80, 80, 
                   100, 100, 130, 180
                }
             );
            Sm.GrdColReadOnly(Grd8, new int[] { 0, 1, 2, 6, 7, 8, 9 });
            Sm.GrdFormatDate(Grd8, new int[] { 6, 7 });
            Sm.GrdColButton(Grd8, new int[] { 3 });
            Sm.GrdColCheck(Grd8, new int[] { 4, 5 });
            Sm.GrdColInvisible(Grd8, new int[] { 0 });
        }

        private void SetGrd9()
        {
            Grd9.Cols.Count = 8;
            Grd9.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd9,
                    new string[] 
                    {
                        "Deed Of Establishment",
                        //1-5
                        "Deed Number",
                        "Deed Date",
                        "Notary",
                        "Current Deed",
                        "Deed Number",
                        //6-7
                        "Deed Date",
                        "Notary"
                    },
                     new int[] 
                    {
                        200, 
                        150, 100, 150, 200, 150,
                        100, 150
                    }
                );
            Sm.GrdFormatDate(Grd9, new int[] { 2, 6 });
        }

        private void SetGrd10()
        {
            Grd10.Cols.Count = 5;
            Grd10.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd10,
                    new string[] 
                    {
                        "Dno",
                        //1-5
                        "TTcode",
                        "Transport",
                        "Licence No",
                        "Gastric number",
                    },
                     new int[] 
                    {
                        50, 
                        0, 120, 120, 100
                    }
                );
            Sm.GrdColInvisible(Grd10, new int[] { 0, 1 });
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVdCode, TxtVdName, ChkActInd, LueVdCtCode, MeeAddress, TxtShortName, 
                        LueVilCode, LueSDCode, LueCityCode, TxtPostalCd, TxtIdentityNo, 
                        TxtTIN, TxtPhone, TxtFax, TxtEmail, TxtMobile, 
                        TxtCreditLimit, MeeRemark, ChkTaxInd, ChkCreateUserVendor, LueSector, LueRating, LueVdRating,
                        LueCityCode, DteBirthDt, MeeAddress2, MeeEducation, MeeCertificate, MeeLanguage, MeeExperience, TxtNIB,
                        LueType, LueGender, LueCountry, TxtWebsite, LueQualification, LueParent, LueProcurementStatus, LueSubSectorCode,
                        TxtEstablishedYr, LueCurCode, DteContractDt, DteContractEndDt, LueProgressCode, TxtVdCodeInternal, TxtRMVdCode, 
                        LueEntityType,
                        TxtVdExternalCode, TxtSelectionScore, LueDeptCode
                    }, true);
                    BtnVendorCode.Enabled = BtnVendorRegister.Enabled = false;
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;
                    Grd5.ReadOnly = true;
                    Grd6.ReadOnly = true;
                    Grd7.ReadOnly = true;
                    Grd8.ReadOnly = true;
                    Grd9.ReadOnly = true;
                    Grd10.ReadOnly = true;
                    //tabControl1.SelectTab("TpgUploadedFile");
                    //Sm.GrdColReadOnly(true, true, Grd8, new int[] { 0, 1, 3, 4, 5 });
                    TxtVdCode.Focus();
                    //tabControl1.SelectTab("TpgContactPerson");

                    if (!mIsVendorUseProcurementRegistration)
                    {
                        BtnVendorRegister.Visible = false;
                        ChkCreateUserVendor.Visible = false;
                        LblVendorRegister.Visible = false;
                        LblProcurementStatus.Visible = false;
                        LueProcurementStatus.Visible = false;
                    }

                break;
                case mState.Insert:
                    Sm.SetControlReadOnly(TxtVdCode, mIsVdCodeAutoIncrement);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVdName, LueVdCtCode, MeeAddress, LueCityCode, TxtPostalCd, 
                        TxtIdentityNo, TxtTIN, TxtPhone, TxtFax, TxtEmail, TxtShortName,
                        TxtMobile, TxtCreditLimit, MeeRemark, ChkTaxInd, ChkCreateUserVendor, LueSector, LueRating, LueVdRating,
                        LueCityCode, DteBirthDt, MeeAddress2, MeeEducation, MeeCertificate, MeeLanguage, MeeExperience, TxtNIB, 
                        LueType, LueGender, LueCountry, TxtWebsite, LueQualification, LueParent, LueProcurementStatus, LueSubSectorCode,
                        TxtEstablishedYr, LueCurCode, DteContractDt, DteContractEndDt, LueProgressCode, TxtVdCodeInternal, 
                        LueEntityType, LueDeptCode
                    }, false);
                    BtnVendorCode.Enabled = !mIsVdCodeAutoIncrement;
                    BtnVendorRegister.Enabled = true;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    Grd5.ReadOnly = false;
                    Grd6.ReadOnly = false;
                    Grd7.ReadOnly = false;
                    Grd9.ReadOnly = false;
                    Grd10.ReadOnly = false;
                    //tabControl1.SelectTab("TpgUploadedFile");
                    //Sm.GrdColReadOnly(true, true, Grd8, new int[] { 0, 1, 2, 3, 4, 5 });
                    if (mIsVdCodeAutoIncrement)
                        TxtVdName.Focus();
                    else
                        TxtVdCode.Focus();
                    break;
                    //tabControl1.SelectTab("TpgContactPerson");
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtVdName, ChkActInd, LueVdCtCode, MeeAddress, LueCityCode, 
                        TxtPostalCd, TxtIdentityNo, TxtTIN, TxtPhone, TxtFax, TxtShortName, 
                        TxtEmail, TxtMobile, TxtCreditLimit, MeeRemark, ChkTaxInd, LueSector, 
                        LueRating, LueVdRating, DteBirthDt, MeeAddress2, MeeEducation, MeeCertificate, MeeLanguage, 
                        MeeExperience, TxtNIB, LueType, LueGender, LueCountry, TxtWebsite, LueQualification, LueParent, LueSubSectorCode,
                        LueProcurementStatus, TxtEstablishedYr, LueCurCode, DteContractDt, DteContractEndDt, LueProgressCode,
                        TxtVdCodeInternal, LueEntityType, LueDeptCode
                    }, false);

                    if (!ChkCreateUserVendor.Checked) 
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCreateUserVendor }, false);

                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    Grd5.ReadOnly = false;
                    Grd6.ReadOnly = false;
                    Grd7.ReadOnly = false;
                    Grd8.ReadOnly = false;
                    Grd9.ReadOnly = false;
                    Grd10.ReadOnly = false;
                    //tabControl1.SelectTab("TpgUploadedFile");
                    //Sm.GrdColReadOnly(false, true, Grd8, new int[] { 2, 3 });
                    TxtVdName.Focus();
                    //tabControl1.SelectTab("TpgContactPerson");
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mRegisterID = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtVdCode, TxtVdName, LueVdCtCode, MeeAddress, LueVilCode, TxtShortName, 
                LueSDCode, LueCityCode, TxtPostalCd, TxtIdentityNo, TxtTIN,
                TxtPhone, TxtFax, TxtEmail, TxtMobile, MeeRemark, LueSector, LueRating, LueVdRating,
                LueCityCode, DteBirthDt, MeeEducation, MeeCertificate, MeeLanguage, MeeExperience, MeeAddress2, TxtNIB,
                LueType, LueGender, LueCountry, TxtWebsite, LueQualification, LueParent, LueProcurementStatus, LueSubSectorCode, TxtEstablishedYr,
                LueCurCode, DteContractDt, DteContractEndDt, LueProgressCode, TxtVdCodeInternal, LueDeptCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtCreditLimit }, 0);
            ChkActInd.Checked = ChkTaxInd.Checked = ChkCreateUserVendor.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
            Sm.ClearGrd(Grd5, true);
            Sm.ClearGrd(Grd6, true);
            Sm.ClearGrd(Grd7, true);
            Sm.ClearGrd(Grd8, true);
            Sm.ClearGrd(Grd9, true); 
            Sm.ClearGrd(Grd10, true);
            
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.FocusGrd(Grd3, 0, 1);
            Sm.FocusGrd(Grd4, 0, 1);
            Sm.FocusGrd(Grd5, 0, 1);
            Sm.FocusGrd(Grd6, 0, 1);
            Sm.FocusGrd(Grd7, 0, 1);
            Sm.FocusGrd(Grd8, 0, 1);
            Sm.FocusGrd(Grd9, 0, 0);
            Sm.FocusGrd(Grd10, 0, 1);

            Sm.SetGrdNumValueZero(ref Grd7, 0, new int[] { 11 });
            
            mVdRegisterEmail = mVdPassword = string.Empty;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVendorFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
            IsInsert = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtVdCode, "", false)) return;
            SetFormControl(mState.Edit);

            if (Sm.GetLue(LueSDCode).Length != 0) Sm.SetControlReadOnly(LueSDCode, false);
            if (Sm.GetLue(LueVilCode).Length != 0) Sm.SetControlReadOnly(LueVilCode, false);

            SetLueProfile(ref LueProfile);
            IsInsert = false;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtVdCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            //Cursor.Current = Cursors.WaitCursor;
            //try
            //{
            //    var cm = new MySqlCommand() { CommandText = "Delete From TblVendor Where VdCode=@VdCode" };
            //    Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            //    Sm.ExecCommand(cm);
            //    BtnCancelClick(sender, e);
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

                var IsVdNameEdited = false;
                if (TxtVdCode.Text.Length > 0 && TxtVdCode.Properties.ReadOnly)
                    IsVdNameEdited = IsUserEditVdName();

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();
                bool IsVendorNew = !TxtVdCode.Properties.ReadOnly;

                if (mIsVdCodeAutoIncrement && TxtVdCode.Text.Length == 0)
                {
                    IsVendorNew = true;
                    TxtVdCode.EditValue = GenerateVdCode();
                }

                if (mMasterVendorUseRunMarketCode)
                    TxtRMVdCode.EditValue = GenerateRMVdCode();

                cml.Add(SaveVendor(IsVendorNew, IsVdNameEdited));
                if (!TxtEmail.Properties.ReadOnly) mVdRegisterEmail = (mVdRegisterEmail.Length > 0) ? mVdRegisterEmail : TxtEmail.Text;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveVendorContactPerson(Row));

                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveVendorBankAccount(Row));

                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveVendorItemCategory(Row));

                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 0).Length > 0) cml.Add(SaveVendorSector(Row));

                for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0) cml.Add(SaveVendorRating(Row));

                for (int Row = 0; Row < Grd6.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 2).Length > 0) cml.Add(SaveVendorProfile(Row));

                for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd7, Row, 2).Length > 0) cml.Add(SaveVendorProject(Row));

                if (IsInsert && mIsUseECatalog)
                {
                    for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd8, Row, 0).Length > 0) cml.Add(SaveVendorFileUpload(Row));
                }

                for (int Row = 0; Row < Grd10.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd10, Row, 1).Length > 0) cml.Add(SaveVendorVehicle(Row));


                //string mTimeNow = Sm.GetValue("Select Date_Format(Now(), '%Y%m%d%H%i%s') As mTimeNow; ");
                
                if(Grd8.Rows.Count >= 1)
                {
                    for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0)
                        {
                            if (Sm.GetGrdBool(Grd8, Row, 4) || Sm.GetGrdBool(Grd8, Row, 5)) // if document uploaded is valid or not valid
                            {
                                cml.Add(UpdateVendorUploadedFileStatus(Row, Sm.GetGrdBool(Grd8, Row, 4), Sm.GetGrdBool(Grd8, Row, 5)));
                            }
                        }
                    }
                }

                //if(Sm.GetParameter("DocTitle") == "TWC")
                if(mIsVendorUseProcurementRegistration)
                {
                    if (TxtEmail.Properties.ReadOnly == true || ChkCreateUserVendor.Properties.ReadOnly == false)
                    {
                        if (ChkCreateUserVendor.Checked)
                        {
                            if(mVdRegisterEmail.Length > 0)
                                ProcessSendingEmail(mVdRegisterEmail);
                        }
                    }
                }

                for (int Row = 0; Row < Grd9.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd9, Row, 2).Length > 0) cml.Add(SaveVendorDeed(Row));

                Sm.ExecCommands(cml);

                IsInsert = false;

                ShowData(TxtVdCode.Text);
            }
            catch (Exception Exc)
            {
                string mInnerException = Exc.InnerException != null ? Exc.InnerException.ToString() : string.Empty;
                Sm.ShowErrorMsg(Exc);
                if (mInnerException.Length > 0 && mIsVendorUseProcurementRegistration) Sm.StdMsg(mMsgType.Warning, mInnerException);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string VdCode)
        {
            try
            {
                ClearData();
                ShowVendor(VdCode);
                ShowVendorContactPerson(VdCode);
                ShowVendorBankAccount(VdCode);
                ShowVendorItemCategory(VdCode);
                ShowVendorSector(VdCode);
                ShowVendorRating(VdCode);
                ShowVendorProfile(VdCode);
                ShowVendorProject(VdCode);
                ShowVendorUploadedFile(VdCode);
                ShowVendorDeed(VdCode);
                ShowVendorVehicle(VdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVendor(string VdCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select VdCode, VdName, ActInd, VdCtCode, Address, CityCode, ");
            SQL.AppendLine("SDCode, VilCode, PostalCd, IdentityNo, TIN, ");
            SQL.AppendLine("Phone, Fax, Email, Mobile, CreditLimit,  ");
            SQL.AppendLine("TaxInd, Remark, ShortName, UserCode, VendorRegisterEmail, ");
            SQL.AppendLine("NIB, Website, Parent, ProcurementStatus, EstablishedYr, ");
            SQL.AppendLine("VdCodeInternal, EntityType, RMVdCode, ");
            if(mIsVendorUseDepartment)
                SQL.AppendLine("DeptCode ");
            else
                SQL.AppendLine("NULL as DeptCode ");
            SQL.AppendLine("From TblVendor ");
            SQL.AppendLine("Where VdCode = @VdCode; ");

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "VdCode", 

                        //1-5
                        "VdName", "ActInd", "VdCtCode", "Address", "CityCode",  

                        //6-10
                        "SDCode", "VilCode", "PostalCd", "IdentityNo", "TIN", 

                        //11-15
                        "Phone", "Fax", "Email", "Mobile", "CreditLimit", 

                        //16-20
                        "TaxInd", "Remark", "ShortName", "UserCode", "VendorRegisterEmail",

                        //21-25
                        "NIB", "Website", "Parent", "ProcurementStatus", "EstablishedYr",

                        //26-29
                        "VdCodeInternal", "EntityType", "RMVdCode", "DeptCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtVdName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueVdCtCode, Sm.DrStr(dr, c[3]));
                        MeeAddress.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[5]));
                        Sl.SetLueSDCode(ref LueSDCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueSDCode, Sm.DrStr(dr, c[6]));
                        Sl.SetLueVilCode(ref LueVilCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueVilCode, Sm.DrStr(dr, c[7]));
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[8]);
                        TxtIdentityNo.EditValue = Sm.DrStr(dr, c[9]);
                        TxtTIN.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[11]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[12]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[13]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[14]);
                        TxtCreditLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                        ChkTaxInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[16]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[17]);
                        TxtShortName.EditValue = Sm.DrStr(dr, c[18]);
                        ChkCreateUserVendor.Checked = Sm.DrStr(dr, c[19]).Length > 0 ? true : false;
                        mVdRegisterEmail = Sm.DrStr(dr, c[20]);
                        TxtNIB.EditValue = Sm.DrStr(dr, c[21]);
                        TxtWebsite.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetLue(LueParent, Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueProcurementStatus, Sm.DrStr(dr, c[24]));
                        TxtEstablishedYr.EditValue = Sm.DrStr(dr, c[25]);
                        TxtVdCodeInternal.EditValue = Sm.DrStr(dr, c[26]);
                        Sm.SetLue(LueEntityType, Sm.DrStr(dr, c[27]));
                        TxtRMVdCode.EditValue = Sm.DrStr(dr, c[28]);
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[29]));
                    }, true
                );
        }

        private void ShowVendorContactPerson(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ContactPersonName, A.IDNumber, A.Position, A.ContactNumber, A.Type, B.OptDesc As Type2 " +
                    "From TblVendorContactPerson A " +
                    "Left Join TblOption B On A.Type = B.OptCode And B.Optcat = 'VendorContactType'" +
                    "Where A.VdCode=@VdCode " +
                    "Order By A.ContactPersonName",
                    new string[] 
                    { 
                        "ContactPersonName", 
                        "IDNumber", "Position", "ContactNumber", "Type", "Type2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowVendorBankAccount(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select A.BankCode, B.BankName, A.BankBranch, A.BankAcName, A.BankAcNo " +
                    "From TblVendorBankAccount A, TblBank B " +
                    "Where A.BankCode=B.BankCode And A.VdCode=@VdCode " +
                    "Order By A.DNo",
                    new string[] 
                    { 
                        "BankCode", 
                        "BankName", "BankBranch", "BankAcName", "BankAcNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowVendorItemCategory(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                    "Select A.ItCtCode, B.ItCtName from tblvendoritemcategory A " +
                    "Inner Join Tblitemcategory B On A.ItCtCode=B.ItCtCode " +
                    "Where A.VdCode=@VdCode " +
                    "Order By B.ItCtName",
                    new string[] 
                    { 
                        "ItCtCode", 
                        "ItCtName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                      //  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowVendorSector(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                    "Select A.SectorCode, B.SectorName, A.QualificationCode, C.QualificationName, D.SubSectorCode, D.SubSectorName from tblvendorsector A " +
                    "Inner Join Tblsector B On A.SectorCode=B.SectorCode " +
                    "Left Join TblQualification C On A.QualificationCode = C.QualificationCode " +
                    "Left Join TblSubSector D ON A.SectorCode = D.SectorCode AND A.SubSectorCode = D.SubSectorCode " +
                    "Where A.VdCode=@VdCode " +
                    "Order By A.DNo ",
                    new string[] 
                    { 
                        "SectorCode", 
                        "SectorName", "SubSectorCode", "SubSectorName", "QualificationCode", "QualificationName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        private void ShowVendorRating(string VdCode)
        {
            var cm = new MySqlCommand();
            var SQL5 = new StringBuilder();

            SQL5.AppendLine("Select A.RatingCode, B.Description, A.RatingVendorAssesmentCode, C.OptDesc As RatingValue ");
            SQL5.AppendLine("From TblVendorRating A ");
            SQL5.AppendLine("Inner Join TblRating B On A.RatingCode = B.RatingCode ");
            SQL5.AppendLine("Inner Join TblOption C On A.RatingVendorAssesmentCode = C.OptCode And C.Optcat = 'RatingVendorAssesment' ");
            SQL5.AppendLine("Where A.VdCode = @VdCode ");
            SQL5.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL5.ToString(),
                    new string[] 
                    { 
                        "RatingCode", 
                        "Description", "RatingVendorAssesmentCode", "RatingValue"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowVendorProfile(string VdCode)
        {
            var cm = new MySqlCommand();
            var SQL6 = new StringBuilder();

            SQL6.AppendLine("Select A.Dno, A.ProfileCode, A.ProfileName, A.ActInd, A.Position, A.Company, A.Address, ");
            SQL6.AppendLine("A.CityCode, B.Cityname, A.BirthDt, A.ID, A.NPWP, A.Education, A.Certificate, ");
            SQL6.AppendLine("A.Language, A.Experience, A.OriInd, A.CntCode, C.CntName, A.Gender, D.OptDesc As Gender2, A.Email, A.Project ");
            SQL6.AppendLine("From TblVendorProfile A ");
            SQL6.AppendLine("Left Join TblCity B On A.CityCode = B.CityCode ");
            SQL6.AppendLine("Left Join TblCountry C On A.CntCode = C.CntCode ");
            SQL6.AppendLine("Left Join TblOption D On A.Gender = D.OptCode And D.Optcat = 'Gender'");
            SQL6.AppendLine("Where A.VdCode = @VdCode ");
            SQL6.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL6.ToString(),
                new string[] 
                { 
                    "Dno", 
                    "ProfileCode", "ProfileName", "ActInd", "Position", "Company", 
                    "CityCode", "Cityname", "BirthDt", "ID", "Address",
                    "NPWP", "Education", "Certificate", "Language", "Experience", 
                    "OriInd", "CntCode", "CntName", "Gender", "Gender2", 
                    "Email", "Project"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 22);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowVendorProject(string VdCode)
        {
            var cm = new MySqlCommand();
            var SQL7 = new StringBuilder();

            SQL7.AppendLine("Select A.*, ");

            if (!mIsUseECatalog) SQL7.AppendLine("B.ProfileName, ");
            else SQL7.AppendLine("Null As ProfileName, ");

            SQL7.AppendLine("C.CurName, D.OptDesc As ProgressName  ");
            SQL7.AppendLine("From TblVendorProject A ");
            
            if (!mIsUseECatalog) SQL7.AppendLine("Inner Join TblVendorProfile B On A.ProfileCode = B.ProfileCode And A.VdCode = B.VdCode ");
            
            SQL7.AppendLine("Left Join TblCurrency C On A.CurCode = C.CurCode ");
            SQL7.AppendLine("Left Join TblOption D On A.ProgressCode = D.OptCode And D.OptCat = 'VendorProgress' ");
            SQL7.AppendLine("Where A.VdCode = @VdCode ");
            SQL7.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                ref Grd7, ref cm, SQL7.ToString(),
                new string[] 
                { 
                    "Dno", 
                    "ProfileCode", "ProfileName", "Project", "ProjectCategory", "ContractNumber",
                    "ContractDt", "Owner", "OwnerAddress", "ProjectLocation", "Results", 
                    "ProjectValue", "CurCode", "CurName", "ContractEndDt", "WorkAchievement",
                    "ProgressCode", "ProgressName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 17);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 0);
            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 11 });
        }

        private void ShowVendorUploadedFile(string VdCode)
        {
            var cm = new MySqlCommand();
            var SQL8 = new StringBuilder();

            SQL8.AppendLine("Select A.CategoryCode, B.CategoryName, Replace(FileLocation, 'dist/pdf/', '') FileName,  ");
            SQL8.AppendLine("IfNull(If(A.ValidInd = 'Y', 'Y', 'N'), 'N') As ValidInd, IfNull(If(A.ValidInd = 'N', 'Y', 'N'), 'N') As InvalidInd,  ");
            SQL8.AppendLine("A.StartDt, A.EndDt, A.LocalDocNo, A.LicensedBy ");
            SQL8.AppendLine("From TblVendorFileUpload A ");
            SQL8.AppendLine("Inner Join TblVendorFileUploadCategory B On A.CategoryCode = B.CategoryCode And A.Status = 'A' And B.ActInd = 'Y' ");
            SQL8.AppendLine("Where VdCode = @VdCode ");
            SQL8.AppendLine("Order By CategoryCode; ");

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                ref Grd8, ref cm, SQL8.ToString(),
                new string[] 
                { 
                    "CategoryCode", 
                    "CategoryName", "FileName", "ValidInd", "InvalidInd", "StartDt", 
                    "EndDt", "LocalDocNo", "LicensedBy"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd8, 0, 0);
        }

        private void ShowVendorDeed(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd9, ref cm,
                    "Select DeedOfEstablishment, DeedNo, DeedDt, Notary, CurrentDeed, DeedNo2, DeedDt2, Notary2 " +
                    "From tblvendordeed " +
                    "Where VdCode=@VdCode ",
                    new string[] 
                    { 
                        "DeedOfEstablishment", 
                        //1-5
                        "DeedNo", 
                        "DeedDt", 
                        "Notary", 
                        "CurrentDeed", 
                        "DeedNo2", 
                        //6-7
                        "DeedDt2", 
                        "Notary2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd9, 0, 0);
        }

        private void ShowVendorVehicle(string VdCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            Sm.ShowDataInGrid(
                    ref Grd10, ref cm,
                    "Select A.Dno, A.TTCode, B.TTName, A.LicenceNo, A.GastricNo "+
                    "From tblvendorlicence A " +
                    "Inner Join TblTransportType B On A.TTCode = B.TTCode "+
                    "Where VdCode=@VdCode ",
                    new string[] 
                    { 
                        "Dno", 
                        //1-5
                        "TTCode", "TTName", "LicenceNo", "GastricNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd10, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                (!mIsVdCodeAutoIncrement && Sm.IsTxtEmpty(TxtVdCode, "Vendor code", false)) ||
                Sm.IsTxtEmpty(TxtVdName, "Vendor name", false) ||
                Sm.IsTxtEmpty(TxtShortName, "Short name", false) ||
                IsVdNameExisted() ||
                (mIsVdCodeUseVdCt && Sm.IsLueEmpty(LueVdCtCode, "Vendor's category")) ||
                IsTINExisted() ||
                Sm.IsLueEmpty(LueCityCode, "City") ||
                (mIsVendorUseEntityType && mIsVendorEntityTypeMandatory && Sm.IsLueEmpty(LueEntityType, "Entity Type")) ||
                IsVdCodeExisted() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                (ChkCreateUserVendor.Checked && Sm.IsTxtEmpty(TxtEmail, "Email", false)) ||
                (mIsVendorUseProcurementRegistration && IsParamEmpty());
        }

        private bool IsParamEmpty()
        {
            if (mIsVendorUseProcurementRegistration)
            {
                if (mVdRegisterEmailFrom.Length == 0 || mVdRegisterEmailHost.Length == 0 || mVdRegisterEmailPassword.Length == 0 || mVdRegisterEmailPort.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Make sure you fill all the parameter value for vendor register email.");
                    return true;
                }
            }

            return false;
        }

        private bool IsVdNameExisted()
        {
            var cm = new MySqlCommand() { CommandText = "Select VdCode From TblVendor Where VdName=@VdName And VdCode<>@VdCode Limit 1;" };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text.Length == 0 ? "XXX" : TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@VdName", TxtVdName.Text);

            string VdCode = Sm.GetValue(cm);
            if (VdCode.Length == 0)
                return false;
            else
            {
                return Sm.StdMsgYN("Question",
                    "Vendor Code : " + VdCode + Environment.NewLine +
                    "Vendor Name : " + TxtVdName.Text + Environment.NewLine + Environment.NewLine +
                    "Existing vendor code With the same name." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
        }

        private bool IsTINExisted()
        {
            if (TxtTIN.Text.Length == 0) return false;

            var cm = new MySqlCommand() { CommandText = "Select VdName From TblVendor Where TIN=@TIN And VdCode<>@VdCode Limit 1;" };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text.Length == 0 ? "XXX" : TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@TIN", TxtTIN.Text);

            string VdName = Sm.GetValue(cm);
            if (VdName.Length == 0)
                return false;
            else
            {
                return Sm.StdMsgYN("Question",
                    "Vendor Name : " + VdName + Environment.NewLine +
                    "Taxpayer Identification#  : " + TxtTIN.Text + Environment.NewLine + Environment.NewLine +
                    "Existing vendor code With the same taxpayer identification number." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
        }

        private bool IsVdCodeExisted()
        {
            if (!TxtVdCode.Properties.ReadOnly && Sm.IsDataExist("Select VdCode From TblVendor Where VdCode='" + TxtVdCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Vendor code ( " + TxtVdCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Name is empty.")) { tabControl1.SelectTab("TpgContactPerson"); return true; }
            }
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "Bank is empty.")) { tabControl1.SelectTab("TpgBank"); return true; }
            }
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "Item category is empty.")) { tabControl1.SelectTab("TpgItemCategory"); return true; }
            }
            if (Grd4.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd4, Row, 1, false, "Vendor sector is empty.")) { tabControl1.SelectTab("TpgVdSector"); return true; }

                for (int i = 0; i < Grd4.Rows.Count - 1; i++)
                {
                    for (int j = i; j < Grd4.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd4, i, 0) == Sm.GetGrdStr(Grd4, j + 1, 0))
                        {
                            tabControl1.SelectTab("TpgVdSector");
                            Sm.StdMsg(mMsgType.Warning, "Duplicate sector : " + Sm.GetGrdStr(Grd4, j + 1, 1));
                            Sm.FocusGrd(Grd4, j + 1, 1);
                            return true;
                        }
                    }
                }
            }
            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 1, false, "Rating indicator is empty.")) { tabControl1.SelectTab("TpgVdRating"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 3, false, "Value is empty.")) { tabControl1.SelectTab("TpgVdRating"); return true; }
                }

                for (int i = 0; i < Grd5.Rows.Count - 1; i++)
                {
                    for (int j = i; j < Grd5.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd5, i, 0) == Sm.GetGrdStr(Grd5, j + 1, 0))
                        {
                            tabControl1.SelectTab("TpgVdRating");
                            Sm.StdMsg(mMsgType.Warning, "Duplicate rating indicator : " + Sm.GetGrdStr(Grd5, j + 1, 1));
                            Sm.FocusGrd(Grd5, j + 1, 1);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Contact person information entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Contact person information entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveVendor(bool IsVendorNew, bool IsVdNameEdited)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVendor(VdCode, VdName, VdCodeInternal, RMVdCode, ActInd, VendorRegisterInd, UserCode, VendorRegisterEmail, ShortName, VdCtCode, Address, VilCode, SDCode, CityCode, PostalCd, IdentityNo, TIN, TaxInd, Phone, fax, Email, Mobile, CreditLimit, NIB, Website, Parent, ProcurementStatus, EstablishedYr, EntityType, Remark, ");
            if(mIsVendorUseDepartment) SQL.AppendLine("DeptCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@VdCode, @VdName, @VdCodeInternal, @RMVdCode, @ActInd, @VendorRegisterInd, @VdUserCode, @VendorRegisterEmail, @ShortName, @VdCtCode, @Address, @VilCode, @SDCode, @CityCode, @PostalCd, @IdentityNo, @TIN, @TaxInd, @Phone, @fax, @Email, @Mobile, @CreditLimit, @NIB, @Website, @Parent, @ProcurementStatus, @EstablishedYr, @EntityType, @Remark, ");
            if (mIsVendorUseDepartment) SQL.AppendLine("@DeptCode, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            
            if(ChkCreateUserVendor.Properties.ReadOnly == false && mIsVendorUseProcurementRegistration)
                SQL.AppendLine("        VendorRegisterInd=@VendorRegisterInd, UserCode=@VdUserCode, VendorRegisterEmail=@VendorRegisterEmail, ");

            SQL.AppendLine("        VdName=@VdName, VdCodeInternal=@VdCodeInternal, RMVdCode=@RMVdCode, ActInd=@ActInd, ShortName=@ShortName, VdCtCode=@VdCtCode, Address=@Address, ");
            SQL.AppendLine("        VilCode=@VilCode, SDCode=@SDCode, CityCode=@CityCode, PostalCd=@PostalCd, ");
            SQL.AppendLine("        IdentityNo=@IdentityNo, TIN=@TIN, TaxInd=@TaxInd, Phone=@Phone, Fax=@Fax, Email=@Email, ");
            SQL.AppendLine("        Mobile=@Mobile, CreditLimit=@CreditLimit, NIB=@NIB, Remark=@Remark, Website=@Website, Parent=@Parent, ProcurementStatus = @ProcurementStatus, EstablishedYr = @EstablishedYr, EntityType = @EntityType, ");
            if (mIsVendorUseDepartment)
                SQL.AppendLine("DeptCode=@DeptCode, ");
            SQL.AppendLine("        LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblVendorContactPerson Where VdCode=@VdCode; ");
            SQL.AppendLine("Delete From TblVendorBankAccount Where VdCode=@VdCode; ");
            SQL.AppendLine("Delete From TblVendorItemCategory Where VdCode=@VdCode; ");
            SQL.AppendLine("Delete From TblVendorSector Where VdCode=@VdCode; ");
            
            // only on desktop apps, not in e-procurement
            SQL.AppendLine("Delete From TblVendorRating Where VdCode=@VdCode; "); 
            SQL.AppendLine("Delete From TblVendorProfile Where VdCode=@VdCode; ");
            SQL.AppendLine("Delete From TblVendorProject Where VdCode=@VdCode; ");

            SQL.AppendLine("Delete From TblVendorDeed Where VdCode=@VdCode; ");
            SQL.AppendLine("Delete From TblVendorLicence Where VdCode=@VdCode; ");

            if (!mIsMasterVendorNotAutocreateVendorCOA)
            {
                if (IsVendorNew)
                {
                    COAVendor(ref SQL, "VendorAcNoDownPayment", "VendorAcDescDownPayment", "D");
                    COAVendor(ref SQL, "VendorAcNoAP", "VendorAcDescAP", "C");
                    COAVendor(ref SQL, "VendorAcNoUnInvoiceAP", "VendorAcDescUnInvoiceAP", "C");
                    COAVendor(ref SQL, "AcNoForGiroAP", "AcDescForGiroAP", "C");
                }
                else
                {
                    if (IsVdNameEdited)
                    {
                        UpdateCOAVendorDesc(ref SQL, "VendorAcNoDownPayment", "VendorAcDescDownPayment");
                        UpdateCOAVendorDesc(ref SQL, "VendorAcNoAP", "VendorAcDescAP");
                        UpdateCOAVendorDesc(ref SQL, "VendorAcNoUnInvoiceAP", "VendorAcDescUnInvoiceAP");
                        UpdateCOAVendorDesc(ref SQL, "AcNoForGiroAP", "AcDescForGiroAP");
                    }
                }
            }

            if (mIsVendorUseProcurementRegistration && ChkCreateUserVendor.Checked)
            {
                SQL.AppendLine("Insert Into TblUserVendor(UserCode, UserName, GrpCode, Pwd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@VdUserCode, @VdUserName, 'Vd', @Pwd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("    Update ");
                SQL.AppendLine("        LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                SQL.AppendLine("Update TblVendorRegister T Set ");
                SQL.AppendLine("    T.VerifiedInd = 'Y', ");
                SQL.AppendLine("    T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where ");
                if (mIsUseECatalog) SQL.AppendLine("T.RegisterID = @RegisterID ");
                else SQL.AppendLine("T.Email = @VendorRegisterEmail ");
                SQL.AppendLine("And T.VerifiedInd = 'N'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            mVdPassword = string.Concat(TxtVdCode.Text, Sm.Left(Sm.ServerCurrentDateTime(), 8));

            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@VdName", TxtVdName.Text);
            Sm.CmParam<String>(ref cm, "@VdCodeInternal", TxtVdCodeInternal.Text);
            Sm.CmParam<String>(ref cm, "@ShortName", TxtShortName.Text);
            Sm.CmParam<String>(ref cm, "@RmVdCode", TxtRMVdCode.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@VendorRegisterInd", ChkCreateUserVendor.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@VdUserCode", ChkCreateUserVendor.Checked ? TxtVdCode.Text : "");
            Sm.CmParam<String>(ref cm, "@VendorRegisterEmail", ChkCreateUserVendor.Checked ? TxtEmail.Text : "");
            Sm.CmParam<String>(ref cm, "@Pwd", mVdPassword);
            Sm.CmParam<String>(ref cm, "@VdUserName", TxtVdName.Text);
            Sm.CmParam<String>(ref cm, "@VdCtCode", Sm.GetLue(LueVdCtCode));
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@VilCode", Sm.GetLue(LueVilCode));
            Sm.CmParam<String>(ref cm, "@SDCode", Sm.GetLue(LueSDCode));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
            Sm.CmParam<String>(ref cm, "@PostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@IdentityNo", TxtIdentityNo.Text);
            Sm.CmParam<String>(ref cm, "@TIN", TxtTIN.Text);
            Sm.CmParam<String>(ref cm, "@TaxInd", ChkTaxInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Fax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@NIB", TxtNIB.Text);
            Sm.CmParam<String>(ref cm, "@Website", TxtWebsite.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@ProcurementStatus", Sm.GetLue(LueProcurementStatus));
            Sm.CmParam<String>(ref cm, "@EstablishedYr", TxtEstablishedYr.Text);
            Sm.CmParam<String>(ref cm, "@EntityType", Sm.GetLue(LueEntityType));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (mIsUseECatalog) Sm.CmParam<String>(ref cm, "@RegisterID", mRegisterID);
            if (mIsVendorUseDepartment) Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));

            return cm;
        }

        private void COAVendor(ref StringBuilder SQL, string AcNo, string AcDesc, string AcType)
        {
            SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
            SQL.AppendLine("Select ");
            SQL.AppendLine("(Select Concat(ParValue, @VdCode) From TblParameter Where ParCode='" + AcNo + "') As AcNo, ");
            SQL.AppendLine("(Select Concat(ParValue, ' ', @VdName) From TblParameter Where ParCode='" + AcDesc + "') As AcDesc, ");
            if (mIsVdCodeUseVdCt)
            {
                SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @VdCode), '.', Length(Concat(ParValue, @VdCode))-Length(Replace(Concat(ParValue, @VdCode), '.', ''))) From Tblparameter Where parcode = '" + AcNo + "') As Parent, ");
                SQL.AppendLine("(Select Length(Concat(ParValue, @VdCode))-Length(Replace(Concat(ParValue, @VdCode), '.', ''))+1 From TblParameter Where ParCode = '" + AcNo + "') As Level, ");
            }
            else
            {
                SQL.AppendLine("(Select Left(ParValue, Length(ParValue)-1) From TblParameter Where ParCode = '" + AcNo + "') As Parent, ");
                SQL.AppendLine("(Select Length(ParValue)-Length(Replace(ParValue, '.', ''))+1 From TblParameter Where ParCode = '" + AcNo + "') As Level, ");
            }
            
            SQL.AppendLine("'" + AcType + "', @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVendor ");
            SQL.AppendLine("Where VdCode=@VdCode  ");
            SQL.AppendLine("And Exists(Select ParCode From TblParameter Where ParCode='" + AcNo + "' And IfNull(ParValue, '')<>'') ");
            SQL.AppendLine("And Exists(Select ParCode From TblParameter Where ParCode='" + AcDesc + "' and IfNull(ParValue, '')<>''); ");
        }

        private void UpdateCOAVendorDesc(ref StringBuilder SQL, string AcNo, string AcDesc)
        {
            SQL.AppendLine("Update TblCOA A ");
            SQL.AppendLine("Inner Join TblParameter B On B.ParCode='" + AcNo + "' And B.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join TblParameter C On C.ParCode='" + AcDesc + "' And C.ParValue Is Not Null ");
            SQL.AppendLine("Set A.AcDesc=Concat(C.ParValue, ' ', @VdName), A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.AcNo=Concat(B.ParValue, @VdCode); ");
        }

        private MySqlCommand SaveVendorContactPerson(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVendorContactPerson(VdCode, DNo, ContactPersonName, IDNumber, Position, ContactNumber, Type, CreateBy, CreateDt) " +
                    "Values(@VdCode, @DNo, @ContactPersonName, @IDNumber, @Position, @ContactNumber, @Type, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ContactPersonName", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@IDNumber", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Position", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ContactNumber", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Type", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorBankAccount(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVendorBankAccount(VdCode, DNo, BankCode, BankBranch, BankAcName, BankAcNo, CreateBy, CreateDt) " +
                    "Values(@VdCode, @DNo, @BankCode, @BankBranch, @BankAcName, @BankAcNo, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@BankBranch", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@BankAcName", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@BankAcNo", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorItemCategory(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into tblvendoritemcategory(VdCode, DNo, ItCtCode, CreateBy, CreateDt) " +
                    "Values(@VdCode, @DNo, @ItCtCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorSector(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into tblvendorsector(VdCode, DNo, SectorCode, QualificationCode, SubSectorCode, CreateBy, CreateDt) " +
                    "Values(@VdCode, @DNo, @SectorCode, @QualificationCode, @SubSectorCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SectorCode", Sm.GetGrdStr(Grd4, Row, 0));
            Sm.CmParam<String>(ref cm, "@SubSectorCode", Sm.GetGrdStr(Grd4, Row, 2));
            Sm.CmParam<String>(ref cm, "@QualificationCode", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorRating(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVendorRating(VdCode, DNo, RatingCode, RatingVendorAssesmentCode, CreateBy, CreateDt) " +
                    "Values(@VdCode, @DNo, @RatingCode, @RatingVendorAssesmentCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@RatingCode", Sm.GetGrdStr(Grd5, Row, 0));
            Sm.CmParam<String>(ref cm, "@RatingVendorAssesmentCode", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorProfile(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVendorProfile(VdCode, DNo, ProfileCode, ProfileName,  " +
                    "ActInd, Position, Company, CityCode, BirthDt, ID, Address, NPWP, Education, Certificate, " +
                    "Language, Experience, OriInd, CntCode, Gender, Email, CreateBy, CreateDt) " +
                    "Values(@VdCode, @DNo, @ProfileCode, @ProfileName,    " +
                    "@ActInd, @Position, @Company, @CityCode, @BirthDt, @ID, @Address, @NPWP, @Education, @Certificate, " +
                    "@Language, @Experience,  @OriInd, @CntCode, @Gender, @Email, " +
                    "@CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProfileCode", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProfileName", Sm.GetGrdStr(Grd6, Row, 2));
            Sm.CmParam<String>(ref cm, "@ActInd", Sm.GetGrdBool(Grd6, Row, 3) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Position", Sm.GetGrdStr(Grd6, Row, 4));
            Sm.CmParam<String>(ref cm, "@Company", Sm.GetGrdStr(Grd6, Row, 5));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetGrdStr(Grd6, Row, 6));
            Sm.CmParamDt(ref cm, "@BirthDt", Sm.GetGrdDate(Grd6, Row, 8));
            Sm.CmParam<String>(ref cm, "@ID", Sm.GetGrdStr(Grd6, Row, 9));
            Sm.CmParam<String>(ref cm, "@Address", Sm.GetGrdStr(Grd6, Row, 10));
            Sm.CmParam<String>(ref cm, "@NPWP", Sm.GetGrdStr(Grd6, Row, 11)); 
            Sm.CmParam<String>(ref cm, "@Education", Sm.GetGrdStr(Grd6, Row, 12));
            Sm.CmParam<String>(ref cm, "@Certificate", Sm.GetGrdStr(Grd6, Row, 13));
            Sm.CmParam<String>(ref cm, "@Language", Sm.GetGrdStr(Grd6, Row, 14)); 
            Sm.CmParam<String>(ref cm, "@Experience", Sm.GetGrdStr(Grd6, Row, 15)); 
            Sm.CmParam<String>(ref cm, "@OriInd", Sm.GetGrdBool(Grd6, Row, 16) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CntCode", Sm.GetGrdStr(Grd6, Row, 17));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetGrdStr(Grd6, Row, 19));
            Sm.CmParam<String>(ref cm, "@Email", Sm.GetGrdStr(Grd6, Row, 21));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorProject(int Row)
        {
            var SQL7 = new StringBuilder();

            SQL7.AppendLine("Insert Into TblVendorProject(VdCode, DNo, ProfileCode, Project, ProjectCategory, ");
            SQL7.AppendLine("ContractNumber, ContractDt, Owner, OwnerAddress, ProjectLocation, Results, ");
            SQL7.AppendLine("ProjectValue, CurCode, ContractEndDt, WorkAchievement, ProgressCode, CreateBy, CreateDt) ");
            SQL7.AppendLine("Values(@VdCode, @DNo, @ProfileCode, @Project, @ProjectCategory, ");
            SQL7.AppendLine("@ContractNumber, @ContractDt, @Owner, @OwnerAddress, @ProjectLocation, @Results, ");
            SQL7.AppendLine("@ProjectValue, @CurCode, @ContractEndDt, @WorkAchievement, @ProgressCode, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQL7.ToString() };

            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ProfileCode", Sm.GetGrdStr(Grd7, Row, 1));
            Sm.CmParam<String>(ref cm, "@Project", Sm.GetGrdStr(Grd7, Row, 3));
            Sm.CmParam<String>(ref cm, "@ProjectCategory", Sm.GetGrdStr(Grd7, Row, 4));
            Sm.CmParam<String>(ref cm, "@ContractNumber", Sm.GetGrdStr(Grd7, Row, 5));
            Sm.CmParamDt(ref cm, "@ContractDt", Sm.GetGrdDate(Grd7, Row, 6));
            Sm.CmParam<String>(ref cm, "@Owner", Sm.GetGrdStr(Grd7, Row, 7));
            Sm.CmParam<String>(ref cm, "@OwnerAddress", Sm.GetGrdStr(Grd7, Row, 8));
            Sm.CmParam<String>(ref cm, "@ProjectLocation", Sm.GetGrdStr(Grd7, Row, 9));
            Sm.CmParam<String>(ref cm, "@Results", Sm.GetGrdStr(Grd7, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@ProjectValue", Sm.GetGrdDec(Grd7, Row, 11));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd7, Row, 12));
            Sm.CmParamDt(ref cm, "@ContractEndDt", Sm.GetGrdDate(Grd7, Row, 14));
            Sm.CmParam<String>(ref cm, "@WorkAchievement", Sm.GetGrdStr(Grd7, Row, 15));
            Sm.CmParam<String>(ref cm, "@ProgressCode", Sm.GetGrdStr(Grd7, Row, 16));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorFileUpload(int Row)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblVendorFileUpload(VdCode, CategoryCode, Status, FileLocation, ");
            SQL.AppendLine("LocalDocNo, LicensedBy, ValidInd, StartDt, EndDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@VdCode, @CategoryCode, @Status, @FileLocation, ");
            SQL.AppendLine("@LocalDocNo, @LicensedBy, NULL, @StartDt, @EndDt, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@CategoryCode", Sm.GetGrdStr(Grd8, Row, 0));
            Sm.CmParam<String>(ref cm, "@Status", "A");
            Sm.CmParam<String>(ref cm, "@FileLocation", Sm.GetGrdStr(Grd8, Row, 2));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", Sm.GetGrdStr(Grd8, Row, 8));
            Sm.CmParam<String>(ref cm, "@LicensedBy", Sm.GetGrdStr(Grd8, Row, 9));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd8, Row, 6));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd8, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateVendorUploadedFileStatus(int Row, bool Valid, bool Invalid)
        {
            var SQL8 = new StringBuilder();

            if (Valid)
            {
                SQL8.AppendLine("Update TblVendorFileUpload Set ");
                SQL8.AppendLine("    ValidInd = 'Y', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL8.AppendLine("Where VdCode = @VdCode ");
                SQL8.AppendLine("And CategoryCode = @CategoryCode ");
                SQL8.AppendLine("And IfNull(Status, 'A') = 'A'; ");
            }

            if (Invalid)
            {
                SQL8.AppendLine("Update TblVendorFileUpload Set ");
                SQL8.AppendLine("    Status = 'C', ValidInd = 'N', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL8.AppendLine("Where VdCode = @VdCode ");
                SQL8.AppendLine("And CategoryCode = @CategoryCode ");
                SQL8.AppendLine("And IfNull(Status, 'A') = 'A'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL8.ToString() };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@CategoryCode", Sm.GetGrdStr(Grd8, Row, 0));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorDeed(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into tblvendordeed(VdCode, DNo, DeedOfEstablishment, DeedNo, DeedDt, Notary, CurrentDeed, DeedNo2, DeedDt2, Notary2, CreateBy, CreateDt) " +
                                 "Values(@VdCode, @DNo, @DeedOfEstablishment, @DeedNo, @DeedDt, @Notary, @CurrentDeed, @DeedNo2, @DeedDt2, @Notary2, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DeedOfEstablishment", Sm.GetGrdStr(Grd9, Row, 0));
            Sm.CmParam<String>(ref cm, "@DeedNo", Sm.GetGrdStr(Grd9, Row, 1));
            Sm.CmParamDt(ref cm, "@DeedDt", Sm.GetGrdDate(Grd9, Row, 2));
            Sm.CmParam<String>(ref cm, "@Notary", Sm.GetGrdStr(Grd9, Row, 3));
            Sm.CmParam<String>(ref cm, "@CurrentDeed", Sm.GetGrdStr(Grd9, Row, 4));
            Sm.CmParam<String>(ref cm, "@DeedNo2", Sm.GetGrdStr(Grd9, Row, 5));
            Sm.CmParamDt(ref cm, "@DeedDt2", Sm.GetGrdDate(Grd9, Row, 6));
            Sm.CmParam<String>(ref cm, "@Notary2", Sm.GetGrdStr(Grd9, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVendorVehicle(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into tblvendorlicence(VdCode, DNo, TTCode, LicenceNo, GastricNo, CreateBy, CreateDt) " +
                                 "Values(@VdCode, @DNo, @TTCode, @LicenceNo, @GastricNo, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TTCode", Sm.GetGrdStr(Grd10, Row, 1));
            Sm.CmParam<String>(ref cm, "@LicenceNo", Sm.GetGrdStr(Grd10, Row, 3));
            Sm.CmParam<String>(ref cm, "@GastricNo", Sm.GetGrdStr(Grd10, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        //private void RenameUploadedFile(string TimeNow)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    var l = new List<FU>();

        //    SQL.AppendLine("Select VdCode, CategoryCode, FileLocation ");
        //    SQL.AppendLine("From TblVendorFileUpload ");
        //    SQL.AppendLine("Where VdCode = @VdCode ");
        //    SQL.AppendLine("And Status = 'C' ");
        //    SQL.AppendLine("And LastUpBy Is Null ");
        //    SQL.AppendLine("And LastUpDt Is Null; ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText = SQL.ToString();
        //        Sm.CmParam<String>(ref cm, "@VdCode", TxtVdCode.Text);
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "CategoryCode", "FileLocation" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new FU() 
        //                {
        //                    VdCode = Sm.DrStr(dr, c[0]),
        //                    CategoryCode = Sm.DrStr(dr, c[1]),
        //                    FileLocation = Sm.DrStr(dr, c[2]).Replace(string.Concat("_", TimeNow, ".pdf"), ".pdf"),
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }

        //    if(l.Count > 0)
        //    {
        //        for (int i = 0; i < l.Count; i++)
        //        {
        //            string mOldFileName = "ftp://" + string.Concat(mHostAddrForFTPClient, ":", mPortForFTPClient) + "/procurement/" + l[i].FileLocation;
        //            string mNewFileName = "ftp://" + string.Concat(mHostAddrForFTPClient, ":", mPortForFTPClient) + "/procurement/" + l[i].FileLocation.Replace(".pdf", string.Concat("_", TimeNow, ".pdf"));
        //            FtpWebRequest request = FtpWebRequest.Create(mOldFileName) as FtpWebRequest;
        //            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
        //            request.Method = WebRequestMethods.Ftp.Rename;
        //            //string mNewFileLocation = l[i].FileLocation.Replace("dist/pdf/", string.Empty).Replace(".pdf", string.Concat("_", TimeNow, ".pdf"));
        //            request.RenameTo = mNewFileName;
        //            request.GetResponse();
        //        }
        //    }
        //}

        internal void GetAdditionalRegisterInformation(string RegisterID)
        {
            mRegisterID = RegisterID;

            GetVendorContactPerson(RegisterID);
            GetVendorBankAccount(RegisterID);
            //GetVendorItemCategory(RegisterID);
            GetVendorSector(RegisterID);
            GetVendorCompliance(RegisterID);

            //Grd3.ReadOnly = true;
            //Grd4.ReadOnly = true;
            Grd8.ReadOnly = true;
        }

        private void GetVendorCompliance(string RegisterID)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.RegisterID, A.DNo, A.CategoryCode, B.CategoryName, ");
            SQL.AppendLine("A.FileLocation ");
            SQL.AppendLine("From TblVendorFileUploadRegister A ");
            SQL.AppendLine("Inner Join TblVendorFileUploadCategory B On A.CategoryCode = B.CategoryCode ");
            SQL.AppendLine("Where A.RegisterID = @RegisterID ");
            SQL.AppendLine("Order By A.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@RegisterID", RegisterID);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "RegisterID", 
                    "DNo", "CategoryCode", "CategoryName", "FileLocation"
                });
                if (dr.HasRows)
                {
                    Sm.ClearGrd(Grd8, true);
                    Grd8.BeginUpdate();
                    int row = 0;
                    while (dr.Read())
                    {
                        Grd8.Rows.Add();
                        Grd8.Cells[row, 0].Value = Sm.DrStr(dr, c[2]);
                        Grd8.Cells[row, 1].Value = Sm.DrStr(dr, c[3]);
                        Grd8.Cells[row, 2].Value = Sm.DrStr(dr, c[4]);

                        row += 1;
                    }
                    Grd8.EndUpdate();
                }
                dr.Close();
            }
        }

        private void GetVendorSector(string RegisterID)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.RegisterID, A.DNo, A.SectorCode, B.SectorName, ");
            SQL.AppendLine("A.QualificationCode, C.QualificationName, D.SubSectorCode, D.SubSectorname ");
            SQL.AppendLine("From TblVendorSectorRegister A ");
            SQL.AppendLine("Left Join TblSector B On A.SectorCode = B.SectorCode ");
            SQL.AppendLine("Left Join TblQualification C On A.QualificationCode = C.QualificationCode ");
            SQL.AppendLine("Left Join TblSubSector D ON A.SectorCode = D.SectorCode AND A.SubSectorCode = D.SubSectorCode ");
            SQL.AppendLine("Where A.RegisterID = @RegisterID ");
            SQL.AppendLine("Order By A.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@RegisterID", RegisterID);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "RegisterID", 
                    "DNo", "SectorCode", "SectorName", "QualificationCode", "QualificationName", 
                    "SubSectorCode", "SubSectorName"
                });
                if (dr.HasRows)
                {
                    Sm.ClearGrd(Grd4, true);
                    Grd4.BeginUpdate();
                    int row = 0;
                    while (dr.Read())
                    {
                        Grd4.Rows.Add();
                        Grd4.Cells[row, 0].Value = Sm.DrStr(dr, c[2]);
                        Grd4.Cells[row, 1].Value = Sm.DrStr(dr, c[3]);
                        Grd4.Cells[row, 2].Value = Sm.DrStr(dr, c[6]);
                        Grd4.Cells[row, 3].Value = Sm.DrStr(dr, c[7]);

                        row += 1;
                    }
                    Grd4.EndUpdate();
                }
                dr.Close();
            }
        }

        private void GetVendorItemCategory(string RegisterID)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.RegisterID, A.DNo, A.ItCtCode, B.ItCtName ");
            SQL.AppendLine("From TblVendorItemCategoryRegister A ");
            SQL.AppendLine("Left Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            SQL.AppendLine("Where A.RegisterID = @RegisterID ");
            SQL.AppendLine("Order By A.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@RegisterID", RegisterID);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "RegisterID", 
                    "DNo", "ItCtCode", "ItCtName"
                });
                if (dr.HasRows)
                {
                    Sm.ClearGrd(Grd3, true);
                    Grd3.BeginUpdate();
                    int row = 0;
                    while (dr.Read())
                    {
                        Grd3.Rows.Add();
                        Grd3.Cells[row, 1].Value = Sm.DrStr(dr, c[2]);
                        Grd3.Cells[row, 2].Value = Sm.DrStr(dr, c[3]);

                        row += 1;
                    }
                    Grd3.EndUpdate();
                }
                dr.Close();
            }
        }

        private void GetVendorBankAccount(string RegisterID)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.RegisterID, A.DNo, A.BankCode, B.BankName, A.BankBranch, ");
            SQL.AppendLine("A.BankAcName, A.BankAcNo ");
            SQL.AppendLine("From TblVendorBankAccountRegister A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode = B.BankCode ");
            SQL.AppendLine("Where A.RegisterID = @RegisterID ");
            SQL.AppendLine("Order By A.DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@RegisterID", RegisterID);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "RegisterID", 
                    "DNo", "BankCode", "BankName", "BankBranch", "BankAcName", 
                    "BankAcNo" 
                });
                if (dr.HasRows)
                {
                    Sm.ClearGrd(Grd2, true);
                    Grd2.BeginUpdate();
                    int row = 0;
                    while (dr.Read())
                    {
                        Grd2.Rows.Add();
                        Grd2.Cells[row, 0].Value = Sm.DrStr(dr, c[2]);
                        Grd2.Cells[row, 1].Value = Sm.DrStr(dr, c[3]);
                        Grd2.Cells[row, 2].Value = Sm.DrStr(dr, c[4]);
                        Grd2.Cells[row, 3].Value = Sm.DrStr(dr, c[5]);
                        Grd2.Cells[row, 4].Value = Sm.DrStr(dr, c[6]);

                        row += 1;
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
        }

        private void GetVendorContactPerson(string RegisterID)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select RegisterID, DNo, ContactPersonName, IDNumber, ");
            SQL.AppendLine("Position, ContactNumber ");
            SQL.AppendLine("From TblVendorContactPersonRegister ");
            SQL.AppendLine("Where RegisterID = @RegisterID ");
            SQL.AppendLine("Order By DNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@RegisterID", RegisterID);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "RegisterID", 
                    "DNo", "ContactPersonName", "IDNumber", "Position", "ContactNumber" 
                });
                if (dr.HasRows)
                {
                    Sm.ClearGrd(Grd1, true);
                    Grd1.BeginUpdate();
                    int row = 0;
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[row, 0].Value = Sm.DrStr(dr, c[2]);
                        Grd1.Cells[row, 1].Value = Sm.DrStr(dr, c[3]);
                        Grd1.Cells[row, 2].Value = Sm.DrStr(dr, c[4]);
                        Grd1.Cells[row, 3].Value = Sm.DrStr(dr, c[5]);

                        row += 1;
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }
        
        internal void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void ProcessSendingEmail(string emailTo)
        {
            var mBody = new StringBuilder();
            string emailFrom = mVdRegisterEmailFrom;//"eprocurement@borobudurpark.co.id";
            string mEmailPassword = mVdRegisterEmailPassword;//"procu54321";
            //string[] mCC = new string[] { "galihakmal2@gmail.com", "wedhatamacaraka@gmail.com" };
            string mDocTitle = string.Empty;
            if (mIsUseECatalog) mDocTitle = Sm.GetParameter("VdRegisterEmailSignature").ToUpper();
            else mDocTitle = Sm.GetParameter("DocTitle").ToUpper();

            string mVdRegisterEmailSignature = Sm.GetParameter("VdRegisterEmailSignature").ToUpper(); 

            mBody.AppendLine("Thank you for Registering your company to our system.");
            mBody.AppendLine(Environment.NewLine);
            mBody.AppendLine("You can now login to our e-procurement system using these credentials : ");
            mBody.AppendLine("- UserCode : " + TxtVdCode.Text);
            mBody.AppendLine("- Password : " + mVdPassword);
            mBody.AppendLine(Environment.NewLine);
            mBody.AppendLine("We recommend you to change the above password immediately for security purposes.");
            mBody.AppendLine("For the best experience, use at least 10 inch monitor for accessing our system.");
            mBody.AppendLine(Environment.NewLine);
            mBody.AppendLine(Environment.NewLine);
            mBody.AppendLine("Regards, ");
            mBody.AppendLine(Environment.NewLine);
            if (mIsUseECatalog) mBody.AppendLine(mVdRegisterEmailSignature);
            else mBody.AppendLine(mDocTitle);

            MailMessage mail = new MailMessage(emailFrom, emailTo);
            SmtpClient client = new SmtpClient();

            //foreach(string CC in mCC)
            //{
            //    mail.CC.Add(new MailAddress(CC));
            //}

            client.Port = Int32.Parse(mVdRegisterEmailPort); //587;
            client.Host = mVdRegisterEmailHost; //"smtp.gmail.com"; //"runsystem.id"; 
            //client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(emailFrom, mEmailPassword);
            client.EnableSsl = mIsVdRegisterEmailUseSSL;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;

            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                       X509Certificate certificate,
                       X509Chain chain,
                       SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            mail.Subject = mDocTitle + " E-Procurement : " + TxtVdName.Text;
            mail.Body = mBody.ToString();
            client.Send(mail);
            //object userState = mail;
            //try
            //{
            //    client.SendAsync(mail, userState);
            //}
            //catch(System.Net.Mail.SmtpException Ex)
            //{
            //    Sm.StdMsg(mMsgType.Warning, Ex.Message.ToString());
            //    return;
            //}
        }

        private bool IsUserEditVdName()
        {
            var VdNameNew = TxtVdName.Text;
            var VdNameOld = Sm.GetValue("Select VdName From TblVendor Where VdCode=@Param;", TxtVdCode.Text);
            return !Sm.CompareStr(VdNameNew, VdNameOld);
        }

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal string GetSelectedItemCategory()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd3, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private string GenerateVdCode()
        {
            var VdCode = string.Empty;
            var SQL = new StringBuilder();

            if (!mIsVdCodeUseShortCode)
            {
                if (mIsVdCodeUseVdCt)
                {
                    if (mGenerateVendorCode == "2")
                    {
                        SQL.AppendLine("Select Concat(@Param, '.', Right(Concat('00000', IfNull(VdCodeMax, '1')) , 5)) As VdCodeTemp ");
                        SQL.AppendLine("From ( ");
                        SQL.AppendLine("    Select Max(Cast(Right(Trim(VdCode), 5) As Decimal))+1 VdCodeMax ");
                        SQL.AppendLine("    From TblVendor ");
                        SQL.AppendLine("    Where VdCtCode=@Param ");
                        SQL.AppendLine(") T;");
                    }
                    else
                    {
                        SQL.AppendLine("Select Concat(@Param, Right(Concat('00000', IfNull(VdCodeMax, '1')) , 5)) As VdCodeTemp ");
                        SQL.AppendLine("From ( ");
                        SQL.AppendLine("    Select Max(Cast(Right(Trim(VdCode), 5) As Decimal))+1 VdCodeMax ");
                        SQL.AppendLine("    From TblVendor ");
                        SQL.AppendLine("    Where VdCtCode=@Param ");
                        SQL.AppendLine(") T;");
                    }

                    VdCode = Sm.GetValue(SQL.ToString(), Sm.GetLue(LueVdCtCode));
                }
                else
                    VdCode = Sm.GetValue(
                        "Select IfNull(Right(Concat('00000', VdCodeMax) , 5), '00001') As VdCodeTemp " +
                        "From ( " +
                        "   Select Max(Cast(Trim(VdCode) As Decimal))+1 VdCodeMax " +
                        "   From TblVendor " +
                        "   ) T;"
                    );
            }
            else
            {
                SQL.AppendLine("Select Concat(@Param, Right(Concat('00000', IfNull(VdCodeMax, '1')) , 5)) As VdCodeTemp ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select Max(Cast(Right(Trim(VdCode), 5) As Decimal))+1 VdCodeMax ");
                SQL.AppendLine("    From TblVendor ");
                SQL.AppendLine("    Where ShortName=@Param ");
                SQL.AppendLine(") T;");

                VdCode = Sm.GetValue(SQL.ToString(), TxtShortName.Text);
            }
            return VdCode;
        }

        private string GenerateRMVdCode()
        {
            var RMVdCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(@Param, Right(Concat('00000', IfNull(VdCodeMax, '1')) , 5)) As VdCodeTemp ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Max(Cast(Right(Trim(VdCode), 5) As Decimal))+1 VdCodeMax ");
            SQL.AppendLine("    From TblVendor ");
            SQL.AppendLine("    Where ShortName=@Param ");
            SQL.AppendLine(") T;");

            RMVdCode = Sm.GetValue(SQL.ToString(), TxtShortName.Text);

            return RMVdCode;
        }

        private void GetParameter()
        {
            mIsVdCodeUseVdCt = Sm.GetParameter("IsVdCodeUseVdCt") == "Y";
            mIsVdCodeAutoIncrement = Sm.GetParameter("IsVdCodeAutoIncrement") == "Y";
            mGenerateVendorCode = Sm.GetParameter("GenerateVendorCode");
            mVendorCategoryCodeForTaxAutoTick = Sm.GetParameter("VendorCategoryCodeForTaxAutoTick");

            // download file
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");

            if (mSharedFolderForFTPClient.Length == 0) mSharedFolderForFTPClient = "procurement/dist/pdf";

            // vendor register
            mIsVendorUseProcurementRegistration = Sm.GetParameterBoo("IsVendorUseProcurementRegistration");
            mVdRegisterEmailFrom = Sm.GetParameter("VdRegisterEmailFrom");
            mVdRegisterEmailPassword = Sm.GetParameter("VdRegisterEmailPassword");
            mVdRegisterEmailPort = Sm.GetParameter("VdRegisterEmailPort");
            mVdRegisterEmailHost = Sm.GetParameter("VdRegisterEmailHost");
            mIsVdRegisterEmailUseSSL = Sm.GetParameterBoo("IsVdRegisterEmailUseSSL");


            mIsVendorDeedNotShow = Sm.GetParameterBoo("IsVendorDeedNotShow");
            mIsVendorVehicleNotShow = Sm.GetParameterBoo("IsVendorVehicleNotShow");
            mIsVendorUseLocalCode = Sm.GetParameterBoo("IsVendorUseLocalCode");
            mIsVdCodeUseShortCode = Sm.GetParameterBoo("IsVdCodeUseShortCode");
            mMasterVendorUseRunMarketCode = Sm.GetParameterBoo("MasterVendorUseRunMarketCode");
            mIsVendorUseEntityType = Sm.GetParameterBoo("IsVendorUseEntityType");
            mIsVendorEntityTypeMandatory = Sm.GetParameterBoo("IsVendorEntityTypeMandatory");
            mIsMasterVendorUseExternalCode = Sm.GetParameterBoo("IsMasterVendorUseExternalCode");
            mIsMasterVendorUseSelectionScore = Sm.GetParameterBoo("IsMasterVendorUseSelectionScore");

            mIsUseECatalog = Sm.GetParameterBoo("IsUseECatalog");
            mIsMasterVendorNotAutocreateVendorCOA = Sm.GetParameterBoo("IsMasterVendorNotAutocreateVendorCOA");
            mIsVendorUseDepartment = Sm.GetParameterBoo("IsVendorUseDepartment");
        }

        private void SetLueSector(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SectorCode As Col1, SectorName As Col2 ");
            SQL.AppendLine("From TblSector ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By SectorName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueQualification(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select QualificationCode As Col1, QualificationName As Col2 ");
            SQL.AppendLine("From TblQualification ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By QualificationName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSubSectorCode(ref DXE.LookUpEdit Lue, string code)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select SubSectorCode As Col1, SubSectorName As Col2 ");
            SQL.AppendLine("From TblSubSector ");
            SQL.AppendLine("Where SectorCode = '"+code+"' ");
            SQL.AppendLine("Order By SubSectorName"); 

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueProfile(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ProfileCode As Col1, ProfileName As Col2 ");
            SQL.AppendLine("From TblVendorProfile ");
            SQL.AppendLine("Where ActInd = 'Y' And VdCode = '"+TxtVdCode.Text+"' ");
            SQL.AppendLine("Order By ProfileName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueRating(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select RatingCode As Col1, Description As Col2 ");
            SQL.AppendLine("From TblRating ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By RatingCode; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Description", "Col2", "Col1");
        }

        private void SetLueTranpsortType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select TTCode As Col1, TTName As Col2 ");
            SQL.AppendLine("From TblTransportType ");
            SQL.AppendLine("Order By TTCode; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Transport", "Col2", "Col1");
        }

        private void SetLueVdRating(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
            SQL.AppendLine("From TblOption ");
            SQL.AppendLine("Where OptCat = 'RatingVendorAssesment' ");
            SQL.AppendLine("Order By OptCode Desc; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Value", "Col2", "Col1");
        }

        private void MeeRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.MemoExEdit Mee,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Mee.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Mee.EditValue = null;
            else
                Mee.EditValue = Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex);

            Mee.Visible = true;
            Mee.Focus();

            fAccept = true;
        }

        private void SetLueType(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption ");
                SQL.AppendLine("Where OptCat = 'VendorContactType' ");
                SQL.AppendLine("Order By OptCode Desc; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueParent(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.VdCode As Col1, Concat(A.VdName, ' [ ', B.CityName, ' ]') As Col2 ");
                SQL.AppendLine("From TblVendor A ");
                SQL.AppendLine("Left Join TblCity B On A.CityCode = B.CityCode ");
                SQL.AppendLine("Where A.VdCode Not In (Select VdCode From TblVendor Where (Parent Is Not Null Or Length(Parent) > 0)) ");
                SQL.AppendLine("And ActInd = 'Y' ");
                if (TxtVdCode.Text.Length > 0)
                    SQL.AppendLine("And A.VdCode <> '" + TxtVdCode.Text + "' ");
                SQL.AppendLine("Order By A.VdName Desc; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void MoveComponent(ref Label Lbl, int x, int y)
        {
            Lbl.Location = new System.Drawing.Point(x, y);
        }

        private void MoveComponent(ref DXE.LookUpEdit Lue, int x, int y)
        {
            Lue.Location = new System.Drawing.Point(x, y);
        }

        private void MoveComponent(ref DXE.TextEdit Txt, int x, int y)
        {
            Txt.Location = new System.Drawing.Point(x, y);
        }

        private void MoveComponent(ref DXE.MemoExEdit Mee, int x, int y)
        {
            Mee.Location = new System.Drawing.Point(x, y);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtVdCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVdCode);
        }

        private void TxtVdName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVdName);
        }

        private void LueVdCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCtCode, new Sm.RefreshLue1(Sl.SetLueVdCtCode));
            if (BtnSave.Enabled && mVendorCategoryCodeForTaxAutoTick.Length > 0 && IsInsert)
            {
                string[] mVdCtCodes = mVendorCategoryCodeForTaxAutoTick.Split(',');
                string mSelectedVdCtCode = Sm.GetLue(LueVdCtCode);
                ChkTaxInd.Checked = false;

                if (mSelectedVdCtCode.Length > 0)
                {
                    foreach (var x in mVdCtCodes)
                    {
                        if (x == mSelectedVdCtCode)
                        {
                            ChkTaxInd.Checked = true;
                            break;
                        }
                    }
                }
            }
        }

        private void TxtPostalCd_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPostalCd);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPhone);
        }

        private void TxtFax_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFax);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEmail);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMobile);
        }

        private void TxtCreditLimit_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtCreditLimit, 0);
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));

            LueSDCode.EditValue = null;
            LueVilCode.EditValue = null;

            if (Sm.GetLue(LueCityCode).Length != 0)
            {
                LueSDCode.EditValue = "<Refresh>";
                Sm.SetControlReadOnly(LueSDCode, (BtnSave.Enabled ? false : true));
            }
            else
                Sm.SetControlReadOnly(LueSDCode, true);
        }

        private void LueSDCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSDCode, new Sm.RefreshLue2(Sl.SetLueSDCode), Sm.GetLue(LueCityCode));
            LueVilCode.EditValue = null;
            if (Sm.GetLue(LueSDCode).Length != 0)
            {
                LueVilCode.EditValue = "<Refresh>";
                Sm.SetControlReadOnly(LueVilCode, (BtnSave.Enabled ? false : true));
            }
            else
            {
                Sm.SetControlReadOnly(LueVilCode, true);
            }
        }

        private void LueVilCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVilCode, new Sm.RefreshLue2(Sl.SetLueVilCode), Sm.GetLue(LueSDCode));
        }

        private void TxtIdentityNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtIdentityNo);
        }

        private void TxtTIN_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTIN);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueBankCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueBankCode_Leave(object sender, EventArgs e)
        {
            if (LueBankCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueBankCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 0].Value =
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueBankCode);
                    Grd2.Cells[fCell.RowIndex, 1].Value = LueBankCode.GetColumnValue("Col2");
                }
                LueBankCode.Visible = false;
            }
        }

        private void LueSector_EditValueChanged(object sender, EventArgs e)
        {
            SetLueSubSectorCode(ref LueSubSectorCode, Sm.GetLue(LueSector));
            Sm.RefreshLookUpEdit(LueSector, new Sm.RefreshLue1(SetLueSector));
        }

        private void LueSector_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueSector_Leave(object sender, EventArgs e)
        {
            if (LueSector.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueSector).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 0].Value =
                    Grd4.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueSector);
                    Grd4.Cells[fCell.RowIndex, 1].Value = LueSector.GetColumnValue("Col2");
                }
                LueSector.Visible = false;
            }
        }

        private void LueSubSectorCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSubSectorCode, new Sm.RefreshLue2(SetLueSubSectorCode), Sm.GetLue(LueSector));
            //Sm.RefreshLookUpEdit(LueSector, new Sm.RefreshLue1(SetLueSector));
        }

        private void LueSubSectorCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueSubSectorCode_Leave(object sender, EventArgs e)
        {
            if (LueSubSectorCode.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueSubSectorCode).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 2].Value =
                    Grd4.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueSubSectorCode);
                    Grd4.Cells[fCell.RowIndex, 3].Value = LueSubSectorCode.GetColumnValue("Col2");
                }
                LueSubSectorCode.Visible = false;
            }
        }

        private void LueQualification_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueQualification, new Sm.RefreshLue1(SetLueQualification));
        }

        private void LueQualification_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueQualification_Leave(object sender, EventArgs e)
        {
            if (LueQualification.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueQualification).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 4].Value =
                    Grd4.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueQualification);
                    Grd4.Cells[fCell.RowIndex, 5].Value = LueQualification.GetColumnValue("Col2");
                }
                LueQualification.Visible = false;
            }
        }

        private void LueRating_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueRating, new Sm.RefreshLue1(SetLueRating));
        }

        private void LueRating_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueRating_Leave(object sender, EventArgs e)
        {
            if (LueRating.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueRating).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 0].Value =
                    Grd5.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueRating);
                    Grd5.Cells[fCell.RowIndex, 1].Value = LueRating.GetColumnValue("Col2");
                }
                LueRating.Visible = false;
            }
        }

        private void LueVdRating_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdRating, new Sm.RefreshLue1(SetLueVdRating));
        }

        private void LueVdRating_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueVdRating_Leave(object sender, EventArgs e)
        {
            if (LueVdRating.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueVdRating).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 2].Value =
                    Grd5.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueVdRating);
                    Grd5.Cells[fCell.RowIndex, 3].Value = LueVdRating.GetColumnValue("Col2");
                }
                LueVdRating.Visible = false;
            }
        }

        private void LueBirthPlace_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBirthPlace, new Sm.RefreshLue1(Sl.SetLueCityCode));

        }

        private void LueBirthPlace_Leave(object sender, EventArgs e)
        {
            if (LueBirthPlace.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (Sm.GetLue(LueBirthPlace).Length == 0)
                    Grd6.Cells[fCell.RowIndex, 6].Value =
                    Grd6.Cells[fCell.RowIndex, 7].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueBirthPlace);
                    Grd6.Cells[fCell.RowIndex, 7].Value = LueBirthPlace.GetColumnValue("Col2");
                }
                LueBirthPlace.Visible = false;
            }
        }

        private void LueBirthPlace_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeEducation_Leave(object sender, EventArgs e)
        {
            if (MeeEducation.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (MeeEducation.Text.Length == 0)
                    Grd6.Cells[fCell.RowIndex, 12].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 12].Value = MeeEducation.Text;
                }
                MeeEducation.Visible = false;
            }
        }

        private void MeeCertificate_Leave(object sender, EventArgs e)
        {
            if (MeeCertificate.Visible && fAccept && fCell.ColIndex == 13)
            {
                if (MeeCertificate.Text.Length == 0)
                    Grd6.Cells[fCell.RowIndex, 13].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 13].Value = MeeCertificate.Text;
                }
                MeeCertificate.Visible = false;
            }
        }

        private void MeeLanguage_Leave(object sender, EventArgs e)
        {
            if (MeeLanguage.Visible && fAccept && fCell.ColIndex == 14)
            {
                if (MeeLanguage.Text.Length == 0)
                    Grd6.Cells[fCell.RowIndex, 14].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 14].Value = MeeLanguage.Text;
                }
                MeeLanguage.Visible = false;
            }
        }

        private void MeeExperience_Leave(object sender, EventArgs e)
        {
            if (MeeExperience.Visible && fAccept && fCell.ColIndex == 15)
            {
                if (MeeExperience.Text.Length == 0)
                    Grd6.Cells[fCell.RowIndex, 15].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 15].Value = MeeExperience.Text;
                }
                MeeExperience.Visible = false;
            }
        }
      
       

        private void MeeEducation_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeCertificate_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeLanguage_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeExperience_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void DteBirthDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteBirthDt, ref fCell, ref fAccept);
        }

        private void DteBirthDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd6, ref fAccept, e);
        }

        private void MeeAddress2_Leave(object sender, EventArgs e)
        {
            if (MeeAddress2.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (MeeAddress2.Text.Length == 10)
                    Grd6.Cells[fCell.RowIndex, 10].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 10].Value = MeeAddress2.Text;
                }
                MeeAddress2.Visible = false;
            }
        }

        private void MeeAddress2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void LueGender_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue1(Sl.SetLueGender));
        }

        private void LueGender_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void LueGender_Leave(object sender, EventArgs e)
        {
            if (LueGender.Visible && fAccept && fCell.ColIndex == 20)
            {
                if (Sm.GetLue(LueGender).Length == 0)
                    Grd6.Cells[fCell.RowIndex, 19].Value =
                    Grd6.Cells[fCell.RowIndex, 20].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 19].Value = Sm.GetLue(LueGender);
                    Grd6.Cells[fCell.RowIndex, 20].Value = LueGender.GetColumnValue("Col2");
                }
                LueGender.Visible = false;
            }
        }

        private void LueCountry_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCountry, new Sm.RefreshLue1(Sl.SetLueCntCode));
        }

        private void LueCountry_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd6, ref fAccept, e);
        }

        private void LueCountry_Leave(object sender, EventArgs e)
        {
            if (LueCountry.Visible && fAccept && fCell.ColIndex == 18)
            {
                if (Sm.GetLue(LueCountry).Length == 0)
                    Grd6.Cells[fCell.RowIndex, 17].Value =
                    Grd6.Cells[fCell.RowIndex, 18].Value = null;
                else
                {
                    Grd6.Cells[fCell.RowIndex, 17].Value = Sm.GetLue(LueCountry);
                    Grd6.Cells[fCell.RowIndex, 18].Value = LueCountry.GetColumnValue("Col2");
                }
                LueCountry.Visible = false;
            }
        }

        private void MeeProject_Leave(object sender, EventArgs e)
        {
            if (MeeProject.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (MeeProject.Text.Length == 3)
                    Grd7.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 3].Value = MeeProject.Text;
                }
                MeeProject.Visible = false;
            }
        }

        private void MeeProject_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void LueProfile_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfile, new Sm.RefreshLue1(SetLueProfile));
        }

        private void LueProfile_Leave(object sender, EventArgs e)
        {
            if (LueProfile.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueProfile).Length == 0)
                    Grd7.Cells[fCell.RowIndex, 1].Value =
                    Grd7.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueProfile);
                    Grd7.Cells[fCell.RowIndex, 2].Value = LueProfile.GetColumnValue("Col2");
                }
                LueProfile.Visible = false;
            }
        }

        private void LueProfile_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(SetLueParent));
        }

        private void LueProcurementStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcurementStatus, new Sm.RefreshLue2(Sl.SetLueOption), "VendorProcurementStatus");
        }

        

        private void DteContractDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteContractDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteContractDt, ref fCell, ref fAccept);
        }

        private void DteContractEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteContractEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteContractEndDt, ref fCell, ref fAccept);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            }
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd7, ref fAccept, e);
            }
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueCurCode.Visible && fAccept && fCell.ColIndex == 13)
                {
                    if (Sm.GetLue(LueCurCode).Length == 0)
                        Grd7.Cells[fCell.RowIndex, 12].Value =
                        Grd7.Cells[fCell.RowIndex, 13].Value = null;
                    else
                    {
                        Grd7.Cells[fCell.RowIndex, 12].Value = Sm.GetLue(LueCurCode);
                        Grd7.Cells[fCell.RowIndex, 13].Value = LueCurCode.GetColumnValue("Col2");
                    }
                    LueCurCode.Visible = false;
                }
            }
        }

        private void TxtEstablishedYr_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtEstablishedYr, 11);
            }
        }

        private void LueProgressCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue2(Sl.SetLueOption), "VendorProgress");
            }
        }

        private void LueProgressCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd7, ref fAccept, e);
            }
        }

        private void LueProgressCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueProgressCode.Visible && fAccept && fCell.ColIndex == 17)
                {
                    if (Sm.GetLue(LueProgressCode).Length == 0)
                        Grd7.Cells[fCell.RowIndex, 16].Value =
                        Grd7.Cells[fCell.RowIndex, 17].Value = null;
                    else
                    {
                        Grd7.Cells[fCell.RowIndex, 16].Value = Sm.GetLue(LueProgressCode);
                        Grd7.Cells[fCell.RowIndex, 17].Value = LueProgressCode.GetColumnValue("Col2");
                    }
                    LueProgressCode.Visible = false;
                }
            }
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
        }

        private void LueType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueType_Leave(object sender, EventArgs e)
        {
            if (LueType.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueType).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueType);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueType.GetColumnValue("Col2");
                }
                LueType.Visible = false;
            }
        }

        #endregion

        #region Grid Event

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 2, 3, 4 }, e);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd2, LueBankCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5 }, e.ColIndex) && BtnSave.Enabled)
            if (e.ColIndex == 5) LueRequestEdit(Grd1, LueType, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmVendorDlg(this));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmVendorDlg(this));
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd4, LueSector, ref fCell, ref fAccept, e);
                if (e.ColIndex == 3) LueRequestEdit(Grd4, LueSubSectorCode, ref fCell, ref fAccept, e);
                if (e.ColIndex == 5) LueRequestEdit(Grd4, LueQualification, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd5, LueRating, ref fCell, ref fAccept, e);
                if (e.ColIndex == 3) LueRequestEdit(Grd5, LueVdRating, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
            Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 7) LueRequestEdit(Grd6, LueBirthPlace, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);

                if (e.ColIndex == 12)
                {
                    MeeRequestEdit(Grd6, MeeEducation, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                }

                if (e.ColIndex == 13)
                {
                    MeeRequestEdit(Grd6, MeeCertificate, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                }

                if (e.ColIndex == 14)
                {
                    MeeRequestEdit(Grd6, MeeLanguage, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                }

                if (e.ColIndex == 15)
                {
                    MeeRequestEdit(Grd6, MeeExperience, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                }

                if (e.ColIndex == 8)
                {
                    Sm.DteRequestEdit(Grd6, DteBirthDt, ref fCell, ref fAccept, e);
                }

                if (e.ColIndex == 10)
                {
                    MeeRequestEdit(Grd6, MeeAddress2, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                }

                if (e.ColIndex == 18) LueRequestEdit(Grd6, LueCountry, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);

                if (e.ColIndex == 20) LueRequestEdit(Grd6, LueGender, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }
        }

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (mIsUseECatalog && e.ColIndex == 23)
            {
                string VdCode = TxtVdCode.Text;
                string DNo = Sm.GetGrdStr(Grd6, e.RowIndex, 0);
                Sm.FormShowDialog(new FrmVendorDlg3(this, 1, VdCode, DNo));
            }
        }

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (mIsUseECatalog && e.ColIndex == 18)
            {
                string VdCode = TxtVdCode.Text;
                string DNo = Sm.GetGrdStr(Grd7, e.RowIndex, 0);
                Sm.FormShowDialog(new FrmVendorDlg3(this, 2, VdCode, DNo));
            }
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2) LueRequestEdit(Grd7, LueProfile, ref fCell, ref fAccept, e);
            if (e.ColIndex == 13) LueRequestEdit(Grd7, LueCurCode, ref fCell, ref fAccept, e);
            if (e.ColIndex == 17) LueRequestEdit(Grd7, LueProgressCode, ref fCell, ref fAccept, e);
            if (e.ColIndex == 6) Sm.DteRequestEdit(Grd7, DteContractDt, ref fCell, ref fAccept, e);
            if (e.ColIndex == 14) Sm.DteRequestEdit(Grd7, DteContractEndDt, ref fCell, ref fAccept, e);
            Sm.GrdRequestEdit(Grd7, e.RowIndex);

            if (e.ColIndex == 3)
            {
                MeeRequestEdit(Grd7, MeeProject, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }

            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 11 });
        }

        private void Grd8_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(e.ColIndex == 3)
                {
                    if (Sm.GetGrdStr(Grd8, e.RowIndex, 2).Length > 0)
                    {
                        DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd8, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                        SFD.FileName = Sm.GetGrdStr(Grd8, e.RowIndex, 2);
                        SFD.DefaultExt = "pdf";
                        SFD.AddExtension = true;

                        if (!Sm.IsGrdValueEmpty(Grd8, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                        {
                            if (SFD.ShowDialog() == DialogResult.OK)
                            {
                                Application.DoEvents();

                                //Write the bytes to a file
                                FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                                newFile.Write(downloadedData, 0, downloadedData.Length);
                                newFile.Close();
                                Sm.StdMsg(mMsgType.Info, "File Downloaded");
                            }
                        }
                        else
                            Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                    }
                }
            }
        }

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                e.DoDefault = false;
                if (e.ColIndex == 3)
                {
                    if (Sm.GetGrdStr(Grd8, e.RowIndex, 2).Length > 0)
                    {
                        DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd8, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                        SFD.FileName = Sm.GetGrdStr(Grd8, e.RowIndex, 2);
                        SFD.DefaultExt = "pdf";
                        SFD.AddExtension = true;

                        if (!Sm.IsGrdValueEmpty(Grd8, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                        {
                            if (SFD.ShowDialog() == DialogResult.OK)
                            {
                                Application.DoEvents();

                                //Write the bytes to a file
                                FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                                newFile.Write(downloadedData, 0, downloadedData.Length);
                                newFile.Close();
                                Sm.StdMsg(mMsgType.Info, "File Downloaded");
                            }
                        }
                        else
                            Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                    }
                }

                if (e.ColIndex == 4 || e.ColIndex == 5)
                {
                    //Grd8.Cells[e.RowIndex, 4].Value = !Sm.GetGrdBool(Grd8, e.RowIndex, 4);
                    Grd8.Cells[e.RowIndex, e.ColIndex].Value = !Sm.GetGrdBool(Grd8, e.RowIndex, e.ColIndex);

                    if (Sm.GetGrdBool(Grd8, e.RowIndex, e.ColIndex))
                        Grd8.Cells[e.RowIndex, (e.ColIndex == 4) ? 5 : 4].Value = false;
                }
            }
        }

        private void Grd9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd9, e, BtnSave);
            Sm.GrdKeyDown(Grd9, e, BtnFind, BtnSave);
        }

        private void Grd9_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.DteRequestEdit(Grd9, DteDeedDt, ref fCell, ref fAccept, e);
                }
                if (e.ColIndex == 6)
                {
                    Sm.DteRequestEdit(Grd9, DteDeedDt2, ref fCell, ref fAccept, e);
                }
            }
            Sm.GrdRequestEdit(Grd9, e.RowIndex);
            
        }

        private void Grd10_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2) LueRequestEdit(Grd10, LueTransportType, ref fCell, ref fAccept, e);
            Sm.GrdRequestEdit(Grd10, e.RowIndex);
        }

        private void Grd10_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtVdCode.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd10, e, BtnSave);
                Sm.GrdKeyDown(Grd10, e, BtnFind, BtnSave);
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "You can't remove");
            }
        }

        #endregion

        #region Button Events

        private void BtnVendorCode_Click(object sender, EventArgs e)
        {
            try
            {
                string VdCode = Sm.GetValue("Select MAX(Cast(VdCode As Decimal)) As VdCode From TblVendor");
                Sm.StdMsg(mMsgType.Info, "The Last Number of Vendor Code is " + VdCode + "");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnVendorRegister_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtVdCode.Text.Length <= 0)
            {
                Sm.FormShowDialog(new FrmVendorDlg2(this));
            }
        }

        private void DteDeedDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd9, ref fAccept, e);
        }

        private void DteDeedDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeedDt, ref fCell, ref fAccept);
        }

        private void DteDeedDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd9, ref fAccept, e);
        }

        private void DteDeedDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeedDt2, ref fCell, ref fAccept);
        }
        
        #endregion

        private void LueTransportType_Leave_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueTransportType.Visible && fAccept && fCell.ColIndex == 2)
                {
                    if (Sm.GetLue(LueTransportType).Length == 0)
                        Grd10.Cells[fCell.RowIndex, 1].Value =
                        Grd10.Cells[fCell.RowIndex, 2].Value = null;
                    else
                    {
                        Grd10.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueTransportType);
                        Grd10.Cells[fCell.RowIndex, 2].Value = LueTransportType.GetColumnValue("Col2");
                    }
                    LueTransportType.Visible = false;
                }
            }
        }

        private void LueTransportType_KeyDown_1(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd10, ref fAccept, e);
        }

        private void LueTransportType_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTransportType, new Sm.RefreshLue1(SetLueTranpsortType));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        #endregion

        #region Class

        //class FU
        //{
        //    public string VdCode { get; set; }
        //    public string CategoryCode { get; set; }
        //    public string FileLocation { get; set; }
        //}

        #endregion

    }
}
