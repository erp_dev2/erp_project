﻿#region Update
/*
    05/09/2019 [WED/IOK] new reporting IOK
    05/12/2019 [TKG/IOK] bug filter user 
    15/01/2020 [DITA/IOK] tambah kolom qty PO, remark POR, AmtBeforeTax, AmtAfterTax, dan Filter PI Date 
    25/02/2020 [TKG/IOK] bug quotation tidak perlu difilter status aktifnya.
 *  30/06/2021 [HAR/IOK] BUG AP downpayment yang dicancel masih ikut ketarik
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchaserPerformance2 : RunSystem.FrmBase6
    {
        #region Field

        private string mAccessInd = string.Empty, mSQL = string.Empty;
        internal string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPurchaserPerformance2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueVdCode(ref LueVdCode);
                SetGrd();
                //SetSQL();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Select B.PORequestDocNo, B.PORequestDNo, G1.VdName, C.CreateBy As PORequestCreateBy, A.CreateBy As POCreateBy, A.DocNo, B.DNo, ");
            //SQL.AppendLine("I.DocNo As PORevisionDocNo, D.ItCode, IfNull(I.UPrice, E.UPrice) UPriceBeforeTax, IfNull(I.QtDocNo, C.QtDocNo) QtDocNo, ");
            //SQL.AppendLine("0.00 As UPriceAfterTax, IfNull(I.DTName, H.DTName) DTName, IfNull(I.PtName, G.PtName) PtName, ");
            //SQL.AppendLine("IfNull(J.Amt, 0.00) As APDPAmt, K.PIDocNo ");
            //SQL.AppendLine("From TblPOHdr A ");
            //SQL.AppendLine("Inner Join TblPODtl B On A.DocNo = B.DocNo And (A.DocDt Between @DocDt1 And @DocDt2)  ");
            //SQL.AppendLine("    And B.CancelInd = 'N' ");
            //SQL.AppendLine("    And A.Status = 'A' ");
            //SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo And B.PORequestDNo = C.DNo ");
            //SQL.AppendLine("    And C.CancelInd = 'N' And C.Status = 'A' ");
            //SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo = D.DocNo And C.MaterialRequestDNo = C.DNo ");
            //SQL.AppendLine("    And D.CancelInd = 'N' And D.Status = 'A' ");
            //SQL.AppendLine("Inner Join TblQtDtl E On C.QtDocNo = E.DocNo And C.QtDNo = E.DNo And E.ActInd = 'Y' ");
            //SQL.AppendLine("Inner JOin TblQtHdr F On E.DocNo = F.DocNo And F.CancelInd = 'N' And F.Status = 'A' ");
            //SQL.AppendLine("Inner Join TblPaymentTerm G On F.PtCode = G.PtCode ");
            //SQL.AppendLine("Inner Join TblVendor G1 On A.VdCode = G1.VdCode ");
            //SQL.AppendLine("Left Join TblDeliveryType H On F.DTCode = H.DTCode ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select X1.DocNo, X1.PODocNo, X1.PODNo, X1.QtDocNo, X2.UPrice, X5.PtName, X6.DTName ");
            //SQL.AppendLine("    From ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select Max(Concat(Right(T1.DocNo, 2), Substr(T1.DocNo, 14, 2), Left(T1.DocNo, 4))) DocNo, T1.PODocNo, T1.PODNo ");
            //SQL.AppendLine("        From TblPORevision T1 ");
            //SQL.AppendLine("        Inner Join TblPOHDr T2 On T1.PODocNo = T2.DocNo And (T2.DocDt Between @DocDt1 And @DocDt2) And T2.Status = 'A' ");
            //SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            //SQL.AppendLine("    ) X0 ");
            //SQL.AppendLine("    Inner Join TblPORevision X1 On X1.DocNo = Concat(Right(X0.DocNo, 4), '/IOK/POR/', Substr(X0.DocNo, 3, 2), '/', Left(X0.DocNo, 2)) ");
            //SQL.AppendLine("    Inner Join TblPOHdr X4 On X1.PODocNo = X4.DocNo And (X4.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("    Inner Join TblQtDtl X2 On X1.QtDocNo = X2.DocNo And X1.QtDNo = X2.DNo And X2.ActInd = 'Y' ");
            //SQL.AppendLine("    Inner Join TblQtHdr X3 On X2.DocNo = X3.DocNo And X3.CancelInd = 'N' And X3.Status = 'A' ");
            //SQL.AppendLine("    Inner Join TblPaymentTerm X5 On X3.PtCode = X5.PtCode ");
            //SQL.AppendLine("    Left Join TblDeliveryType X6 On X3.DTCode = X6.DTCode	 ");
            //SQL.AppendLine(") I On B.DocNo = I.PODocNo And B.DNo = I.PODNo ");
            //SQL.AppendLine("Left Join TblAPDownpayment J On A.DocNo = J.PODocNo ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            ////SQL.AppendLine("    Select A1.DocNo As PIDocNo, A3.PODocNo, A3.PODNo  ");
            ////SQL.AppendLine("    From TblPurchaseInvoiceHdr A1 ");
            ////SQL.AppendLine("    Inner Join TblPurchaseInvoiceDtl A2 On A1.DocNo = A2.DocNo And A1.CancelInd = 'N' ");
            ////SQL.AppendLine("    Inner Join TblRecvVdDtl A3 On A2.RecvVdDocNo = A3.DocNo  ");
            ////SQL.AppendLine("        And A3.CancelInd = 'N' And A3.Status = 'A' ");
            ////SQL.AppendLine("    Inner Join TblPOHdr A4 On A3.PODocNo = A4.DocNo  ");
            ////SQL.AppendLine("        And (A4.DocDt Between @DocDt1 And @DocDt2) And A4.Status = 'A' ");
            ////SQL.AppendLine("    Inner Join TblPODtl A5 On A3.PODocNo = A5.DocNo And A3.PODNo = A5.DNo ");
            ////SQL.AppendLine("        And A5.CancelInd = 'N' ");
            //SQL.AppendLine("    SELECT A1.DocNo PODocNo, A2.DNo PODNo, A5.DocNo PIDocNo, A4.DNo PIDNo ");
            //SQL.AppendLine("    FROM TblPOHdr A1 ");
            //SQL.AppendLine("    INNER JOIN TblPODtl A2 ON A1.DocNo = A2.DocNo AND (A1.DocDt BETWEEN @DocDt1 AND @DocDt2)  ");
            //SQL.AppendLine("        AND A1.Status = 'A' AND A2.CancelInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblRecvVdDtl A3 ON A3.PODocNo = A2.DocNo AND A3.PODNo = A2.DNo ");
            //SQL.AppendLine("        AND A3.Status = 'A' AND A3.CancelInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblPurchaseInvoiceDtl A4 ON A4.RecvVdDocNo = A3.DocNo AND A4.RecvVdDNo = A3.DNo ");
            //SQL.AppendLine("    INNER JOIN TblPurchaseInvoiceHdr A5 ON A4.DocNo = A5.DocNo AND A5.CancelInd = 'N' ");
            //SQL.AppendLine(") K On A.DocNo = K.PODocNo And B.DNo = K.PODNo ");
            #endregion

            SQL.AppendLine("SELECT T1.PORequestDocNo, T1.PORequestCreateBy, T3.UserName AS PORequestUserName, T1.POCreateBy, ");
            SQL.AppendLine("T4.UserName AS POUserName, T1.DocNo, T1.DNo, T1.VdCode, T1.VdName, T1.PORevisionDocNo, T1.ItCode, ");
            SQL.AppendLine("T2.ItName, T1.UPriceBeforeTax, T1.QtDocNo, T1.UPriceAfterTax, T1.DTName, T1.PtName, T1.APDPAmt, ");
            SQL.AppendLine("T1.POQty, T1.PORRemark, (T1.POQty * T1.UPriceAfterTax) AS TotalUPriceAfterTax, (T1.POQty * T1.UPriceBeforeTax) AS TotalUPriceBeforeTax ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select B.PORequestDocNo, C.CreateBy As PORequestCreateBy, A.CreateBy As POCreateBy, A.DocNo, B.DNo, A.VdCode, G1.VdName,  ");
	        SQL.AppendLine("    I.DocNo As PORevisionDocNo, D.ItCode, IfNull(I.UPrice, E.UPrice) UPriceBeforeTax, IfNull(I.QtDocNo, C.QtDocNo) QtDocNo,  ");
            SQL.AppendLine("    ( IfNull(I.UPrice, E.UPrice) + ");
		    SQL.AppendLine("        IfNull(IFNULL((I.UPrice * K.TaxRate * 0.01), (E.UPrice * K.TaxRate * 0.01)), 0.00) + ");
		    SQL.AppendLine("        IfNull(IFNULL((I.UPrice * L.TaxRate * 0.01), (E.UPrice * L.TaxRate * 0.01)), 0.00) + ");
		    SQL.AppendLine("        ifNull(IFNULL((I.UPrice * M.TaxRate * 0.01), (E.UPrice * M.TaxRate * 0.01)), 0.00) ");
	        SQL.AppendLine("    ) As UPriceAfterTax, ");
            SQL.AppendLine("    IfNull(I.DTName, H.DTName) DTName, IfNull(I.PtName, G.PtName) PtName, ");
            SQL.AppendLine("    IfNull(J.Amt, 0.00) As APDPAmt,B.Qty POQty, C.Remark PORRemark ");
	        SQL.AppendLine("    From TblPOHdr A ");
	        SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo = B.DocNo And B.CancelInd = 'N' ");
            if (Sm.GetDte(DtePIDt1).Length > 0 && Sm.GetDte(DtePIDt2).Length > 0) 
             {
                SQL.AppendLine(" And Concat(B.DocNo, B.DNo) In ");
		        SQL.AppendLine("    ( ");
		        SQL.AppendLine("    Select Distinct Concat(D.DocNo, D.DNo) ");
		        SQL.AppendLine("    FROM TblPurchaseInvoiceDtl A ");
		        SQL.AppendLine("    INNER JOIN TblPurchaseInvoiceHdr B ON A.DocNo = B.DocNo AND B.CancelInd = 'N' ");
		        SQL.AppendLine("    And (B.DocDt Between @PIDocDt1 And @PIDocDt2) ");
		        SQL.AppendLine("    INNER JOIN TblRecvVdDtl C ON A.RecvVdDocNo = C.DocNo AND A.RecvVdDNo = C.DNo ");
		        SQL.AppendLine("    INNER JOIN TblPODtl D ON C.PODocNo = D.DocNo AND C.PODNo = D.DNo ");
                SQL.AppendLine("    ) ");
             }
	        SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo And B.PORequestDNo = C.DNo ");
	        SQL.AppendLine("        And C.CancelInd = 'N' And C.Status = 'A' ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo = D.DocNo And C.MaterialRequestDNo = D.DNo ");
	        SQL.AppendLine("        And D.CancelInd = 'N' And D.Status = 'A' ");
	        SQL.AppendLine("    Inner Join TblQtDtl E On C.QtDocNo = E.DocNo And C.QtDNo = E.DNo ");
	        SQL.AppendLine("    Inner JOin TblQtHdr F On E.DocNo = F.DocNo And F.CancelInd = 'N' And F.Status = 'A' ");
	        SQL.AppendLine("    Inner Join TblPaymentTerm G On F.PtCode = G.PtCode ");
	        SQL.AppendLine("    Inner Join TblVendor G1 On A.VdCode = G1.VdCode ");
	        SQL.AppendLine("    Left Join TblDeliveryType H On F.DTCode = H.DTCode ");
	        SQL.AppendLine("    Left Join ");
	        SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select X1.DocNo, X1.PODocNo, X1.PODNo, X1.QtDocNo, X2.UPrice, X5.PtName, X6.DTName ");
	        SQL.AppendLine("        From ");
	        SQL.AppendLine("        ( ");
	        SQL.AppendLine("            Select Max(Concat(Right(T1.DocNo, 2), Substr(T1.DocNo, 14, 2), Left(T1.DocNo, 4))) DocNo, T1.PODocNo, T1.PODNo ");
	        SQL.AppendLine("            From TblPORevision T1 ");
	        SQL.AppendLine("            Inner Join TblPOHDr T2 On T1.PODocNo = T2.DocNo And (T2.DocDt Between @DocDt1 And @DocDt2) And T2.Status = 'A' ");
	        SQL.AppendLine("            Group By T1.PODocNo, T1.PODNo ");
	        SQL.AppendLine("        ) X0 ");
	        SQL.AppendLine("        Inner Join TblPORevision X1 On X1.DocNo = Concat(Right(X0.DocNo, 4), '/IOK/POR/', Substr(X0.DocNo, 3, 2), '/', Left(X0.DocNo, 2)) ");
	        SQL.AppendLine("        Inner Join TblPOHdr X4 On X1.PODocNo = X4.DocNo And (X4.DocDt Between @DocDt1 And @DocDt2) ");
	        SQL.AppendLine("        Inner Join TblQtDtl X2 On X1.QtDocNo = X2.DocNo And X1.QtDNo = X2.DNo ");
	        SQL.AppendLine("        Inner Join TblQtHdr X3 On X2.DocNo = X3.DocNo And X3.CancelInd = 'N' And X3.Status = 'A' ");
	        SQL.AppendLine("        Inner Join TblPaymentTerm X5 On X3.PtCode = X5.PtCode ");
	        SQL.AppendLine("        Left Join TblDeliveryType X6 On X3.DTCode = X6.DTCode ");
	        SQL.AppendLine("    ) I On B.DocNo = I.PODocNo And B.DNo = I.PODNo ");
	        SQL.AppendLine("    Left Join TblAPDownpayment J On A.DocNo = J.PODocNo And J.CancelInd = 'N' And J.Status ='A' ");
            SQL.AppendLine("    LEFT JOIN TblTax K ON A.TaxCode1 = K.TaxCode ");
	        SQL.AppendLine("    LEFT JOIN TblTax L ON A.TaxCode2 = L.TaxCode ");
	        SQL.AppendLine("    LEFT JOIN TblTax M ON A.TaxCode3 = M.TaxCode ");
            SQL.AppendLine("    Where A.Status = 'A' ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("INNER JOIN TblItem T2 ON T1.ItCode = T2.ItCode ");
            SQL.AppendLine("INNER JOIN TblUser T3 ON T1.PORequestCreateBy = T3.UserCode ");
            SQL.AppendLine("INNER JOIN TblUser T4 ON T1.POCreateBy = T4.UserCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "PO Request#",
                    "PO Request's User", 
                    "Purchaser",
                    "PO#",
                    "PODNo",

                    //6-10
                    "PO Revision#",
                    "Vendor",
                    "Item's Code",
                    "Item's Name",
                    "PO Price"+Environment.NewLine+"Before Tax",

                    //11-15
                    "PO Price"+Environment.NewLine+"Before Tax Amount",
                    "QtDocNo",
                    "",
                    "PO Price"+Environment.NewLine+"After Tax",
                    "PO Price"+Environment.NewLine+"After Tax Amount",
                    
                    //16-20
                    "Delivery Type",
                    "TOP",
                    "Item's Quantity"+Environment.NewLine+"(based on PO) ",
                    "PO Request Remark",
                    "APDP's Amount",

                    //21
                    ""
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 200, 180, 120, 0, 

                    //6-10
                    180, 220, 100, 200, 180, 

                    //11-15
                    180, 0, 20, 180, 180, 

                    //16-20
                    180, 180, 150, 200, 180,

                    //21
                    20
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 14, 15, 18, 20  }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 12, 13, 21 }, false);
            Sm.GrdColButton(Grd1, new int[] { 13, 21 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 13, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.CmParamDt(ref cm, "@PIDocDt1", Sm.GetDte(DtePIDt1));
                Sm.CmParamDt(ref cm, "@PIDocDt2", Sm.GetDte(DtePIDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtUserCode.Text, new string[] { "T4.UserCode", "T4.UserName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T2.ItCode", "T2.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "T1.VdCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[] 
                    { 
                        //0
                        "PORequestDocNo", 
                        //1-5
                        "PORequestUserName", "POUserName", "DocNo", "DNo", "PORevisionDocNo",
                        //6-10
                        "VdName", "ItCode", "ItName", "UPriceBeforeTax", "TotalUPriceBeforeTax", 
                        //11-15
                        "QtDocNo", "UPriceAfterTax", "TotalUPriceAfterTax","DTName", "PtName", 
                        //16-18
                        "POQty","PORRemark","APDPAmt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    }, true, false, false, false
                );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
            
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 14, 15, 18, 20 });
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            e.DoDefault = false;
            
            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, r, 12).Length > 0)
            {
                var f1 = new FrmQt(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, r, 11);
                f1.ShowDialog();
            }

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, r, 4).Length > 0 && Sm.GetGrdStr(Grd1, r, 5).Length > 0)
            {
                string
                    mPODocNo = Sm.GetGrdStr(Grd1, r, 4),
                    mPODNo = Sm.GetGrdStr(Grd1, r, 5),
                    mItCode = Sm.GetGrdStr(Grd1, r, 8),
                    mItName = Sm.GetGrdStr(Grd1, r, 9);
                decimal
                    mUPriceBeforeTax = Sm.GetGrdDec(Grd1, r, 10),
                    mUPriceAfterTax = Sm.GetGrdDec(Grd1, r, 14);
                Sm.FormShowDialog(new FrmRptPurchaserPerformance2Dlg(this, mPODocNo, mPODNo, mItCode, mItName, mUPriceBeforeTax, mUPriceAfterTax));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;

            if (e.ColIndex == 13 && Sm.GetGrdStr(Grd1, r, 11).Length > 0)
            {
                var f1 = new FrmQt(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, r, 11);
                f1.ShowDialog();
            }

            if (e.ColIndex == 21 && Sm.GetGrdStr(Grd1, r, 4).Length > 0 && Sm.GetGrdStr(Grd1, r, 5).Length > 0)
            {
                string 
                    mPODocNo = Sm.GetGrdStr(Grd1, r, 4), 
                    mPODNo = Sm.GetGrdStr(Grd1, r, 5),
                    mItCode = Sm.GetGrdStr(Grd1, r, 8),
                    mItName = Sm.GetGrdStr(Grd1, r, 9);
                decimal
                    mUPriceBeforeTax = Sm.GetGrdDec(Grd1, r, 10),
                    mUPriceAfterTax = Sm.GetGrdDec(Grd1, r, 14);
                Sm.FormShowDialog(new FrmRptPurchaserPerformance2Dlg(this, mPODocNo, mPODNo, mItCode, mItName, mUPriceBeforeTax, mUPriceAfterTax));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkUserCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Purchaser");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void DtePIDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DtePIDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkPIDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "PI's date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
