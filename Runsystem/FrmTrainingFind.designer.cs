﻿namespace RunSystem
{
    partial class FrmTrainingFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtTrainingName = new DevExpress.XtraEditors.TextEdit();
            this.ChkTrainingName = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueTrainingType = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkTrainingType = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTrainingName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTrainingName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTrainingType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkTrainingType);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueTrainingType);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtTrainingName);
            this.panel2.Controls.Add(this.ChkTrainingName);
            this.panel2.Size = new System.Drawing.Size(672, 52);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 421);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Training";
            // 
            // TxtTrainingName
            // 
            this.TxtTrainingName.EnterMoveNextControl = true;
            this.TxtTrainingName.Location = new System.Drawing.Point(69, 3);
            this.TxtTrainingName.Name = "TxtTrainingName";
            this.TxtTrainingName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTrainingName.Properties.Appearance.Options.UseFont = true;
            this.TxtTrainingName.Properties.MaxLength = 40;
            this.TxtTrainingName.Size = new System.Drawing.Size(251, 20);
            this.TxtTrainingName.TabIndex = 10;
            this.TxtTrainingName.Validated += new System.EventHandler(this.TxtTrainingName_Validated);
            // 
            // ChkTrainingName
            // 
            this.ChkTrainingName.Location = new System.Drawing.Point(324, 1);
            this.ChkTrainingName.Name = "ChkTrainingName";
            this.ChkTrainingName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTrainingName.Properties.Appearance.Options.UseFont = true;
            this.ChkTrainingName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkTrainingName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkTrainingName.Properties.Caption = " ";
            this.ChkTrainingName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTrainingName.Size = new System.Drawing.Size(31, 22);
            this.ChkTrainingName.TabIndex = 11;
            this.ChkTrainingName.ToolTip = "Remove filter";
            this.ChkTrainingName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkTrainingName.ToolTipTitle = "Run System";
            this.ChkTrainingName.CheckedChanged += new System.EventHandler(this.ChkTrainingName_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(27, 27);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "Type";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTrainingType
            // 
            this.LueTrainingType.EnterMoveNextControl = true;
            this.LueTrainingType.Location = new System.Drawing.Point(69, 25);
            this.LueTrainingType.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingType.Name = "LueTrainingType";
            this.LueTrainingType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingType.Properties.DropDownRows = 4;
            this.LueTrainingType.Properties.NullText = "[Empty]";
            this.LueTrainingType.Properties.PopupWidth = 500;
            this.LueTrainingType.Size = new System.Drawing.Size(251, 20);
            this.LueTrainingType.TabIndex = 13;
            this.LueTrainingType.ToolTip = "F4 : Show/hide list";
            this.LueTrainingType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingType.EditValueChanged += new System.EventHandler(this.LueTrainingType_EditValueChanged);
            // 
            // ChkTrainingType
            // 
            this.ChkTrainingType.Location = new System.Drawing.Point(324, 24);
            this.ChkTrainingType.Name = "ChkTrainingType";
            this.ChkTrainingType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTrainingType.Properties.Appearance.Options.UseFont = true;
            this.ChkTrainingType.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkTrainingType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkTrainingType.Properties.Caption = " ";
            this.ChkTrainingType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTrainingType.Size = new System.Drawing.Size(31, 22);
            this.ChkTrainingType.TabIndex = 14;
            this.ChkTrainingType.ToolTip = "Remove filter";
            this.ChkTrainingType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkTrainingType.ToolTipTitle = "Run System";
            this.ChkTrainingType.CheckedChanged += new System.EventHandler(this.ChkTrainingType_CheckedChanged);
            // 
            // FrmTrainingFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmTrainingFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTrainingName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTrainingName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTrainingType.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtTrainingName;
        private DevExpress.XtraEditors.CheckEdit ChkTrainingName;
        private DevExpress.XtraEditors.CheckEdit ChkTrainingType;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingType;
    }
}