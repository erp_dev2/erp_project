﻿#region Update
/*
    22/01/2021 [IBL/PHT] New Apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeUpdateApprovalDlg : RunSystem.FrmBase1
    {
        #region Field, Property

        private FrmEmployeeUpdateApproval mFrmParent;
        private string mSQL = string.Empty, mEmpCode = string.Empty;
        private bool
            mIsFilterBySiteHR = false,
            mIsSiteDivisionEnabled = false,
            mIsDivisionDepartmentEnabled = false,
            mIsFilterByDeptHR = false,
            mIsDeptFilterByDivision = false,
            mIsDepartmentPositionEnabled = false,
            mIsPayrollActived = false,
            mIsEmployeeUseGradeSalary = false,
            mIsEmpContractDtMandatory = false,
            mIsSiteMandatory = false
            ;

        #endregion

        #region Constructor

        public FrmEmployeeUpdateApprovalDlg(FrmEmployeeUpdateApproval FrmParent, string EmpCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mEmpCode = EmpCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "Employee Update";
                base.FrmLoad(sender, e);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnDelete.Visible = BtnSave.Visible =
                    BtnCancel.Visible = BtnPrint.Visible = false;
                BtnFind.Enabled = BtnInsert.Enabled = BtnEdit.Enabled = BtnDelete.Enabled = BtnSave.Enabled =
                    BtnCancel.Enabled = BtnPrint.Enabled = false;
                GetParameter();
                SetFormControl(mState.View);

                tabControl1.SelectTab("TpgGeneral");
                SetGrd();

                Sl.SetLueEntCode(ref LueEntityCode);
                Sl.SetLuePosCode(ref LuePosCode);
                Sl.SetLuePositionStatusCode(ref LuePositionStatusCode);
                Sl.SetLueGender(ref LueGender);
                Sl.SetLueReligion(ref LueReligion);
                Sl.SetLueCityCode(ref LueCity);
                Sl.SetLueEmployeePayrollType(ref LuePayrollType);
                Sl.SetLuePTKP(ref LuePTKP);
                Sl.SetLueBankCode(ref LueBankCode);
                if (mIsEmployeeUseGradeSalary)
                    Sl.SetLueGradeSalary(ref LueGrdLvlCode);
                else
                    Sl.SetLueGrdLvlCode(ref LueGrdLvlCode);
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
                Sl.SetLueOption(ref LueMaritalStatus, "MaritalStatus");
                Sl.SetLueOption(ref LueSystemType, "EmpSystemType");
                Sl.SetLueOption(ref LueBloodType, "BloodType");
                Sl.SetLueOption(ref LueClothesSize, "EmployeeClothesSize");
                Sl.SetLueOption(ref LueShoeSize, "EmployeeShoeSize");
                Sl.SetLueOption(ref LueEducationType, "EmployeeEducationType");
                Sl.SetLueOption(ref LueCostGroup, "EmpCostGroup");
                SetLueSection(ref LueSection);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                SetLueWorkGroup(ref LueWorkGroup, Sm.GetLue(LueSection));
                if (!mIsSiteDivisionEnabled)
                    Sl.SetLueDivisionCode(ref LueDivisionCode);

                if (!mIsDeptFilterByDivision)
                    SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");

                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                if (mIsEmpContractDtMandatory) LblContractDt.ForeColor = Color.Red;

                tabControl1.SelectTab("TpgEmployeeFamily");
                //Sl.SetLueFamilyStatus(ref LueStatus);
                //LueStatus.Visible = false;
                //Sl.SetLueGender(ref LueFamGender);
                //LueFamGender.Visible = false;
                //Sl.SetLueOption(ref LueProfessionCode, "Profession");
                //LueProfessionCode.Visible = false;
                //Sl.SetLueOption(ref LueEducationLevelCode, "EmployeeEducationLevel");
                //LueEducationLevelCode.Visible = false;
                //DteFamBirthDt.Visible = false;
                


                tabControl1.SelectTab("TpgGeneral");
                if (mIsPayrollActived)
                {
                    LblDeptCode.ForeColor = Color.Red;
                    LblPosCode.ForeColor = Color.Red;
                    LblGrdLvlCode.ForeColor = Color.Red;
                    LblPGCode.ForeColor = Color.Red;
                    LblEmploymentStatus.ForeColor = Color.Red;
                    LblSystemType.ForeColor = Color.Red;
                    LblPayrunPeriod.ForeColor = Color.Red;
                }

                ShowData(mEmpCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Family Name",
                        "Status Code",
                        "Status",
                        "Gender Code",
                        "Gender",
                        
                        //6-10
                        "Birth Date",
                        "ID Card",
                        "National"+Environment.NewLine+"Identity#",
                        "Profession",
                        "Latest Education",

                        //11-13
                        "Remark",
                        "Profession Code",
                        "Education Level Code",
                        
                    },
                     new int[] 
                    {
                        0, 
                        200, 0, 100, 0, 80, 
                        80, 150, 150, 200, 200,
                        300, 0, 0
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 10, 12 }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpCode, TxtEmpName, TxtDisplayName, TxtUserCode, LueDeptCode,
                        LuePosCode, DteJoinDt, DteResignDt, DteTGDt, TxtIdNumber,
                        LueGender, LueReligion, TxtBirthPlace,DteBirthDt, MeeAddress,
                        LueCity, TxtPostalCode, LueEntityCode, LueSiteCode, MeeDomicile,
                        LueBloodType, LueSection, LueWorkGroup, TxtPhone, TxtMobile,
                        TxtEmail, LueGrdLvlCode, TxtNPWP, LuePayrollType, LuePTKP,
                        LueBankCode, TxtBankBranch, TxtBankAcName, TxtBankAcNo, TxtEmpCodeOld, 
                        TxtShortCode, TxtBarcodeCode, LuePayrunPeriod, LueEmploymentStatus, TxtSubDistrict, 
                        TxtVillage, TxtRTRW, TxtMother, LueMaritalStatus, DteWeddingDt,
                        LueSystemType, LueDivisionCode, LuePGCode, TxtPOH, DteLeaveStartDt,
                        LuePositionStatusCode, DteContractDt, LueClothesSize, LueShoeSize, LueEducationType,
                        LueCostGroup, ChkActingOfficial,
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtEmpCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtEmpCode, TxtEmpName, TxtDisplayName, TxtUserCode, LueDeptCode,
                LuePosCode, DteJoinDt, DteResignDt, DteTGDt, TxtIdNumber,
                LueGender, LueReligion, TxtBirthPlace,DteBirthDt, MeeAddress,
                LueCity, TxtPostalCode, LueEntityCode, LueSiteCode, MeeDomicile,
                LueBloodType, LueSection, LueWorkGroup, TxtPhone, TxtMobile,
                TxtEmail, LueGrdLvlCode, TxtNPWP, LuePayrollType, LuePTKP,
                LueBankCode, TxtBankBranch, TxtBankAcName, TxtBankAcNo, TxtEmpCodeOld, 
                TxtShortCode, TxtBarcodeCode, LuePayrunPeriod, LueEmploymentStatus, TxtSubDistrict, 
                TxtVillage, TxtRTRW, TxtMother, LueMaritalStatus, DteWeddingDt,
                LueSystemType, LueDivisionCode, LuePGCode, TxtPOH, DteLeaveStartDt,
                LuePositionStatusCode, DteContractDt, LueClothesSize, LueShoeSize, LueEducationType,
                LueCostGroup, ChkActingOfficial,
            });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
            ChkActingOfficial.Checked = false;
        }

        #endregion

        public void ShowData(string EmpCode)
        {
            try
            {
                ClearData();
                ShowEmployee(EmpCode);
                ShowEmployeeFamily(EmpCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmployee(string EmpCode)
        {
            string
                DivisionCode = string.Empty,
                DeptCode = string.Empty,
                PosCode = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, C.LevelName, D.EntName ");
            SQL.AppendLine("From TblEmployeeUpdate A ");
            SQL.AppendLine("Left Join TblGradeLevelHdr B On A.GrdLvlCode=B.GrdLvlCode ");
            SQL.AppendLine("Left Join TblLevelHdr C On B.LevelCode=C.LevelCode ");
            SQL.AppendLine("Left Join TblEntity D On A.RegEntCode=D.EntCode ");
            SQL.AppendLine("Where EmpCode=@EmpCode;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "EmpCode",

                        //1-5
                        "EmpName", "DisplayName", "UserCode", "DeptCode", "PosCode",

                        //6-10
                        "JoinDt","ResignDt","IdNumber","Gender","Religion",

                        //11-15
                        "BirthPlace","BirthDt","Address","CityCode","PostalCode",

                        //16-20
                        "Phone","Mobile","GrdLvlCode","Email","NPWP",

                        //21-25
                        "PayrollType","PTKP","BankCode","BankBranch","BankAcName",

                        //26-30
                        "BankAcNo","EmpCodeOld", "ShortCode", "BarcodeCode", "PayrunPeriod", 

                        //31-35
                        "EmploymentStatus","Mother", "SubDistrict", "Village", "RTRW", 

                        //36-40
                        "MaritalStatus","WeddingDt", "SystemType", "EntCode", "SiteCode", 
                        
                        //41-45
                         "Domicile", "BloodType", "SectionCode", "WorkGroupCode", "DivisionCode", 

                         //46-50
                         "PGCode", "POH", "TGDt", "LeaveStartDt", "LevelName",

                         //51-55
                         "EntName", "PositionStatusCode", "ContractDt", "ClothesSize", "ShoeSize",

                         //56-58
                         "EducationType", "CostGroup"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtDisplayName.EditValue = Sm.DrStr(dr, c[2]);
                        TxtUserCode.EditValue = Sm.DrStr(dr, c[3]);

                        DeptCode = Sm.DrStr(dr, c[4]);
                        PosCode = Sm.DrStr(dr, c[5]);
                        //Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[5]));

                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteResignDt, Sm.DrStr(dr, c[7]));
                        TxtIdNumber.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetLue(LueGender, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueReligion, Sm.DrStr(dr, c[10]));
                        TxtBirthPlace.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetDte(DteBirthDt, Sm.DrStr(dr, c[12]));
                        MeeAddress.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetLue(LueCity, Sm.DrStr(dr, c[14]));
                        TxtPostalCode.EditValue = Sm.DrStr(dr, c[15]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[16]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[17]);
                        Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[18]));
                        TxtEmail.EditValue = Sm.DrStr(dr, c[19]);
                        TxtNPWP.EditValue = Sm.DrStr(dr, c[20]);
                        Sm.SetLue(LuePayrollType, Sm.DrStr(dr, c[21]));
                        Sm.SetLue(LuePTKP, Sm.DrStr(dr, c[22]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[23]));
                        TxtBankBranch.EditValue = Sm.DrStr(dr, c[24]);
                        TxtBankAcName.EditValue = Sm.DrStr(dr, c[25]);
                        TxtBankAcNo.EditValue = Sm.DrStr(dr, c[26]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[27]);
                        TxtShortCode.EditValue = Sm.DrStr(dr, c[28]);
                        TxtBarcodeCode.EditValue = Sm.DrStr(dr, c[29]);
                        Sm.SetLue(LuePayrunPeriod, Sm.DrStr(dr, c[30]));
                        Sm.SetLue(LueEmploymentStatus, Sm.DrStr(dr, c[31]));
                        TxtMother.EditValue = Sm.DrStr(dr, c[32]);
                        TxtSubDistrict.EditValue = Sm.DrStr(dr, c[33]);
                        TxtVillage.EditValue = Sm.DrStr(dr, c[34]);
                        TxtRTRW.EditValue = Sm.DrStr(dr, c[35]);
                        Sm.SetLue(LueMaritalStatus, Sm.DrStr(dr, c[36]));
                        Sm.SetDte(DteWeddingDt, Sm.DrStr(dr, c[37]));
                        Sm.SetLue(LueSystemType, Sm.DrStr(dr, c[38]));
                        Sm.SetLue(LueEntityCode, Sm.DrStr(dr, c[39]));

                        SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[40]), mIsFilterBySiteHR ? "Y" : "N");

                        MeeDomicile.EditValue = Sm.DrStr(dr, c[41]);
                        Sm.SetLue(LueBloodType, Sm.DrStr(dr, c[42]));
                        Sm.SetLue(LueSection, Sm.DrStr(dr, c[43]));
                        SetLueWorkGroup(ref LueWorkGroup, Sm.DrStr(dr, c[43]));
                        Sm.SetLue(LueWorkGroup, Sm.DrStr(dr, c[44]));
                        DivisionCode = Sm.DrStr(dr, c[45]);

                        Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[46]));
                        TxtPOH.EditValue = Sm.DrStr(dr, c[47]);
                        Sm.SetDte(DteTGDt, Sm.DrStr(dr, c[48]));
                        Sm.SetDte(DteLeaveStartDt, Sm.DrStr(dr, c[49]));
                        TxtLevelCode.EditValue = Sm.DrStr(dr, c[50]);
                        TxtRegEntCode.EditValue = Sm.DrStr(dr, c[51]);
                        Sm.SetLue(LuePositionStatusCode, Sm.DrStr(dr, c[52]));
                        Sm.SetDte(DteContractDt, Sm.DrStr(dr, c[53]));
                        Sm.SetLue(LueClothesSize, Sm.DrStr(dr, c[54]));
                        Sm.SetLue(LueShoeSize, Sm.DrStr(dr, c[55]));
                        if (mIsSiteDivisionEnabled)
                            SetLueDivisionCodeBasedOnSite(ref LueDivisionCode, Sm.GetLue(LueSiteCode), DivisionCode);
                        else
                            Sm.SetLue(LueDivisionCode, DivisionCode);
                        if (mIsDivisionDepartmentEnabled)
                            SetLueDeptCodeBasedOnDivision(ref LueDeptCode, DivisionCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N");
                        else
                        {
                            if (mIsDeptFilterByDivision)
                                SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N", DivisionCode);
                            else
                                SetLueDeptCode(ref LueDeptCode, DeptCode, mIsFilterByDeptHR ? "Y" : "N");
                        }
                        if (mIsDepartmentPositionEnabled)
                            SetLuePosCodeBasedOnDepartment(ref LuePosCode, DeptCode, PosCode);
                        else
                            Sm.SetLue(LuePosCode, PosCode);
                        Sm.SetLue(LueEducationType, Sm.DrStr(dr, c[56]));
                        Sm.SetLue(LueCostGroup, Sm.DrStr(dr, c[57]));
                        ChkActingOfficial.Checked = Sm.CompareStr(Sm.GetValue("Select ActingOfficialInd From TblPosition " +
                                               "Where PosCode = @Param", Sm.DrStr(dr, c[5])), "Y");
                    }, true
                );
        }

        private void ShowEmployeeFamily(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.FamilyName, ");
            SQL.AppendLine("A.Status, B.OptDesc as StatusDesc, A.Gender, C.OptDesc As GenderDesc, ");
            SQL.AppendLine("A.BirthDt, A.IDNo, A.NIN, A.Remark, A.ProfessionCode, D.OptDesc As ProfessionDesc, A.EducationLevelCode, E.OptDesc As EducationLevelDesc  ");
            SQL.AppendLine("From TblEmployeeFamilyUpdate A ");
            SQL.AppendLine("Left Join TblOption B On A.Status=B.OptCode and B.OptCat='FamilyStatus' ");
            SQL.AppendLine("Left Join TblOption C On A.Gender=C.OptCode and C.OptCat='Gender' ");
            SQL.AppendLine("Left Join TblOption D On A.ProfessionCode=D.OptCode and D.OptCat='Profession' ");
            SQL.AppendLine("Left Join TblOption E On A.EducationLevelCode=E.OptCode and E.OptCat='EmployeeEducationLevel' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.FamilyName;");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "FamilyName", "Status", "StatusDesc", "Gender", "GenderDesc", 
                        
                        //6-10
                        "BirthDt", "IDNo", "NIN", "Remark", "ProfessionCode", 
                        
                        //11-13
                        "ProfessionDesc", "EducationLevelCode", "EducationLevelDesc"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsSiteDivisionEnabled = Sm.GetParameterBoo("IsSiteDivisionEnabled");
            mIsDivisionDepartmentEnabled = Sm.GetParameterBoo("IsDivisionDepartmentEnabled");
            mIsDeptFilterByDivision = Sm.GetParameterBoo("IsDeptFilterByDivision");
            mIsDepartmentPositionEnabled = Sm.GetParameterBoo("IsDepartmentPositionEnabled");
        }

        private void SetLueSection(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select SectionCode As Col1, SectionName As Col2 ");
                SQL.AppendLine("From TblSection Where ActInd='Y' Order By SectionName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueSiteCode(ref LookUpEdit Lue, string Code, string IsFilterBySite)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where SiteCode=@Code ");
                SQL.AppendLine("Or (T.ActInd='Y' ");
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.SiteName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueWorkGroup(ref DXE.LookUpEdit Lue, string SectionCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select WorkGroupCode As Col1, WorkGroupName As Col2 ");
                SQL.AppendLine("From TblWorkingGroup Where ActInd='Y' And SectionCode = '" + SectionCode + "' Order By WorkGroupName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueDivisionCodeBasedOnSite(ref LookUpEdit Lue, string SiteCode, string DivisionCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DivisionCode As Col1, DivisionName As Col2 From TblDivision ");
            if (DivisionCode.Length > 0)
            {
                SQL.AppendLine("Where (DivisionCode=@DivisionCode ");
                SQL.AppendLine("Or DivisionCode In (Select DivisionCode From TblSiteDivision Where SiteCode=@SiteCode)) ");
            }
            else
                SQL.AppendLine("Where ActInd='Y' And DivisionCode In (Select DivisionCode From TblSiteDivision Where SiteCode=@SiteCode) ");
            SQL.AppendLine("Order By DivisionName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.CmParam<String>(ref cm, "@DivisionCode", DivisionCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (DivisionCode.Length > 0) Sm.SetLue(Lue, DivisionCode);
        }

        private void SetLuePosCodeBasedOnDepartment(ref LookUpEdit Lue, string DeptCode, string PosCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PosCode As Col1, PosName As Col2 From TblPosition ");
            if (PosCode.Length > 0)
            {
                SQL.AppendLine("Where (PosCode=@PosCode ");
                SQL.AppendLine("Or PosCode In (Select PosCode From TblDepartmentPosition Where DeptCode=@DeptCode)) ");
            }
            else
                SQL.AppendLine("Where PosCode In (Select PosCode From TblDepartmentPosition Where DeptCode=@DeptCode) ");
            SQL.AppendLine("Order By PosName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (PosCode.Length > 0) Sm.SetLue(Lue, PosCode);
        }

        private void SetLueDeptCodeBasedOnDivision(ref LookUpEdit Lue, string DivisionCode, string DeptCode, string IsFilterByDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DeptCode As Col1, DeptName As Col2 From TblDepartment T ");
            if (DeptCode.Length > 0)
            {
                SQL.AppendLine("Where (DeptCode=@DeptCode ");
                SQL.AppendLine("Or DeptCode In (Select DeptCode From TblDivisionDepartment Where DivisionCode=@DivisionCode)) ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And  Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("Where DeptCode In (Select DeptCode From TblDivisionDepartment Where DivisionCode=@DivisionCode) ");
            }
            SQL.AppendLine("Order By DeptName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DivisionCode", DivisionCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (DeptCode.Length > 0) Sm.SetLue(Lue, DeptCode);
        }

        private void SetLueDeptCode(ref LookUpEdit Lue, string Code, string IsFilterByDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T Where 0 = 0 ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("And DeptCode=@Code  ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("Or Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                SQL.AppendLine("And ActInd = 'Y' ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueDeptCode(ref LookUpEdit Lue, string Code, string IsFilterByDept, string DivisionCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 From TblDepartment T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where DeptCode=@Code ");
                SQL.AppendLine("Or (DivisionCode=IfNull(@DivisionCode, 'XXX') ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And  Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where DivisionCode=IfNull(@DivisionCode, 'XXX') ");
                SQL.AppendLine("And T.ActInd = 'Y' ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And  Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
            {
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DivisionCode", DivisionCode);
            }
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion
    }
}
