﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTenderQuotationDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTenderQuotation mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTenderQuotationDlg(FrmTenderQuotation FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Active Tender";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Tender#",
                    "",
                    
                    //6-10
                    "Tender Name",
                    "Item Code",
                    "Item Name",
                    "Vendor",
                    "Quotation#",

                    //11-14
                    "", 
                    "Currency",
                    "Estimated"+Environment.NewLine+"Price",
                    "Quotation"+Environment.NewLine+"Price",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    160, 20, 80, 160, 20, 
                    
                    //6-10
                    200, 100, 180, 170, 150, 
                    
                    //11-14
                    20, 100, 100, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2, 5, 11 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 6, 7, 8, 9, 10, 12, 13, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 7, 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 7, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.TenderDocNo, B.TenderName, C.ItCode, D.ItName, G.VdName, ");
            SQL.AppendLine("A.QtDocNo, C.CurCode, C.EstPrice, F.UPrice ");
            SQL.AppendLine("From TblTenderQuotationRequest A ");
            SQL.AppendLine("Inner Join TblTender B On A.TenderDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("Inner Join TblQtHdr E On A.QtDocNo = E.DocNo And E.TenderDocNo = B.DocNo And E.Status = 'A' And E.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblQtDtl F On A.QtDocNo = F.DocNo And F.DNo = '001' And F.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblVendor G On E.VdCode = G.VdCode ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.DocNo Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.TQRDocNo ");
            SQL.AppendLine("    From TblTenderQuotation T ");
            SQL.AppendLine(") ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            if (Sm.IsDteEmpty(DteDocDt1, "Start Date") ||
                Sm.IsDteEmpty(DteDocDt2, "End Date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;            
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter +
                    " Order By A.CreateDt Desc;",
                    new string[] 
                    {                     
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "TenderDocNo", "TenderName", "ItCode", "ItName", 
                        
                        //6-10
                        "VdName", "QtDocNo", "CurCode", "EstPrice", "UPrice",
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ClearData2();
                int Row = Grd1.CurRow.Index;
                mFrmParent.ShowTenderQuotationDetail(Sm.GetGrdStr(Grd1, Row, 1));
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                ChooseData();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmTenderQuotationRequest(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmTenderQutationRequest");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmTender(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmTender");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f1.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmQt(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmQt");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f1.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmTenderQuotationRequest(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmTenderQutationRequest");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f1 = new FrmTender(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmTender");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f1.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f1 = new FrmQt(mFrmParent.mMenuCode);
                f1.Text = Sm.GetMenuDesc("FrmQt");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
