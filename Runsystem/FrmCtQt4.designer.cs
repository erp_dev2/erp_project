﻿namespace RunSystem
{
    partial class FrmCtQt4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCtQt4));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgQuotationItem = new System.Windows.Forms.TabPage();
            this.LueCtQtType = new DevExpress.XtraEditors.LookUpEdit();
            this.LueUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LblSetDefault = new System.Windows.Forms.Label();
            this.TpgQuotationCity = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgPort = new System.Windows.Forms.TabPage();
            this.LuePort = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgAgent = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.BtnCtContactPersonName = new DevExpress.XtraEditors.SimpleButton();
            this.label17 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSalesTarget = new DevExpress.XtraEditors.TextEdit();
            this.TxtQtMth = new DevExpress.XtraEditors.TextEdit();
            this.LueCtContactPersonName = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LueSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.DteQtStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueRingCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BtnCtReplace = new DevExpress.XtraEditors.SimpleButton();
            this.BtnCtReplace2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCtReplace = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.MeeWhsCapacity = new DevExpress.XtraEditors.MemoExEdit();
            this.LueShpMCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtCreditLimit = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.ChkPrintSignatureInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtGoodsReturnDay = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtMinDelivery = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LueAgingAP = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgQuotationItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtQtType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel4.SuspendLayout();
            this.TpgQuotationCity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpgPort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpgAgent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesTarget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRingCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtReplace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeWhsCapacity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPrintSignatureInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGoodsReturnDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinDelivery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAgingAP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(836, 0);
            this.panel1.Size = new System.Drawing.Size(70, 523);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            this.panel1.Controls.SetChildIndex(this.LueFontSize, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(836, 523);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgQuotationItem);
            this.tabControl1.Controls.Add(this.TpgQuotationCity);
            this.tabControl1.Controls.Add(this.TpgPort);
            this.tabControl1.Controls.Add(this.TpgAgent);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 259);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(836, 264);
            this.tabControl1.TabIndex = 55;
            // 
            // TpgQuotationItem
            // 
            this.TpgQuotationItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgQuotationItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgQuotationItem.Controls.Add(this.LueCtQtType);
            this.TpgQuotationItem.Controls.Add(this.LueUomCode);
            this.TpgQuotationItem.Controls.Add(this.Grd2);
            this.TpgQuotationItem.Controls.Add(this.panel4);
            this.TpgQuotationItem.Location = new System.Drawing.Point(4, 26);
            this.TpgQuotationItem.Name = "TpgQuotationItem";
            this.TpgQuotationItem.Size = new System.Drawing.Size(828, 234);
            this.TpgQuotationItem.TabIndex = 1;
            this.TpgQuotationItem.Text = "Item";
            this.TpgQuotationItem.UseVisualStyleBackColor = true;
            // 
            // LueCtQtType
            // 
            this.LueCtQtType.EnterMoveNextControl = true;
            this.LueCtQtType.Location = new System.Drawing.Point(300, 105);
            this.LueCtQtType.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtQtType.Name = "LueCtQtType";
            this.LueCtQtType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtQtType.Properties.Appearance.Options.UseFont = true;
            this.LueCtQtType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtQtType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtQtType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtQtType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtQtType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtQtType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtQtType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtQtType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtQtType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtQtType.Properties.DropDownRows = 5;
            this.LueCtQtType.Properties.NullText = "[Empty]";
            this.LueCtQtType.Properties.PopupWidth = 150;
            this.LueCtQtType.Size = new System.Drawing.Size(224, 20);
            this.LueCtQtType.TabIndex = 60;
            this.LueCtQtType.ToolTip = "F4 : Show/hide list";
            this.LueCtQtType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtQtType.Leave += new System.EventHandler(this.LueCtQtType_Leave);
            this.LueCtQtType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCtQtType_KeyDown);
            // 
            // LueUomCode
            // 
            this.LueUomCode.EnterMoveNextControl = true;
            this.LueUomCode.Location = new System.Drawing.Point(192, 47);
            this.LueUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueUomCode.Name = "LueUomCode";
            this.LueUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUomCode.Properties.DropDownRows = 5;
            this.LueUomCode.Properties.NullText = "[Empty]";
            this.LueUomCode.Properties.PopupWidth = 150;
            this.LueUomCode.Size = new System.Drawing.Size(224, 20);
            this.LueUomCode.TabIndex = 59;
            this.LueUomCode.ToolTip = "F4 : Show/hide list";
            this.LueUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUomCode.Validated += new System.EventHandler(this.LueUomCode_Validated);
            this.LueUomCode.Leave += new System.EventHandler(this.LueUomCode_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 26);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(824, 204);
            this.Grd2.TabIndex = 58;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.ColHdrClick += new TenTec.Windows.iGridLib.iGColHdrClickEventHandler(this.Grd2_ColHdrClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LblSetDefault);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(824, 26);
            this.panel4.TabIndex = 56;
            // 
            // LblSetDefault
            // 
            this.LblSetDefault.AutoSize = true;
            this.LblSetDefault.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSetDefault.ForeColor = System.Drawing.Color.Green;
            this.LblSetDefault.Location = new System.Drawing.Point(5, 5);
            this.LblSetDefault.Name = "LblSetDefault";
            this.LblSetDefault.Size = new System.Drawing.Size(77, 14);
            this.LblSetDefault.TabIndex = 57;
            this.LblSetDefault.Text = "Set Default";
            this.LblSetDefault.Click += new System.EventHandler(this.LblSetDefault_Click);
            // 
            // TpgQuotationCity
            // 
            this.TpgQuotationCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgQuotationCity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgQuotationCity.Controls.Add(this.Grd1);
            this.TpgQuotationCity.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgQuotationCity.Location = new System.Drawing.Point(4, 26);
            this.TpgQuotationCity.Name = "TpgQuotationCity";
            this.TpgQuotationCity.Size = new System.Drawing.Size(764, 0);
            this.TpgQuotationCity.TabIndex = 0;
            this.TpgQuotationCity.Text = "City";
            this.TpgQuotationCity.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(764, 0);
            this.Grd1.TabIndex = 56;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown_1);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // TpgPort
            // 
            this.TpgPort.Controls.Add(this.LuePort);
            this.TpgPort.Controls.Add(this.Grd4);
            this.TpgPort.Location = new System.Drawing.Point(4, 26);
            this.TpgPort.Name = "TpgPort";
            this.TpgPort.Padding = new System.Windows.Forms.Padding(3);
            this.TpgPort.Size = new System.Drawing.Size(828, 234);
            this.TpgPort.TabIndex = 3;
            this.TpgPort.Text = "Port";
            this.TpgPort.UseVisualStyleBackColor = true;
            // 
            // LuePort
            // 
            this.LuePort.EnterMoveNextControl = true;
            this.LuePort.Location = new System.Drawing.Point(73, 28);
            this.LuePort.Margin = new System.Windows.Forms.Padding(5);
            this.LuePort.Name = "LuePort";
            this.LuePort.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePort.Properties.Appearance.Options.UseFont = true;
            this.LuePort.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePort.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePort.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePort.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePort.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePort.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePort.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePort.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePort.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePort.Properties.DropDownRows = 5;
            this.LuePort.Properties.NullText = "[Empty]";
            this.LuePort.Properties.PopupWidth = 150;
            this.LuePort.Size = new System.Drawing.Size(152, 20);
            this.LuePort.TabIndex = 59;
            this.LuePort.ToolTip = "F4 : Show/hide list";
            this.LuePort.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePort.EditValueChanged += new System.EventHandler(this.LuePort_EditValueChanged);
            this.LuePort.Validated += new System.EventHandler(this.LuePort_Validated);
            this.LuePort.Leave += new System.EventHandler(this.LuePort_Leave);
            this.LuePort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePort_KeyDown);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(3, 3);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(822, 228);
            this.Grd4.TabIndex = 58;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpgAgent
            // 
            this.TpgAgent.Controls.Add(this.Grd3);
            this.TpgAgent.Location = new System.Drawing.Point(4, 26);
            this.TpgAgent.Name = "TpgAgent";
            this.TpgAgent.Padding = new System.Windows.Forms.Padding(3);
            this.TpgAgent.Size = new System.Drawing.Size(764, 0);
            this.TpgAgent.TabIndex = 2;
            this.TpgAgent.Text = "Agent";
            this.TpgAgent.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(3, 3);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(758, 0);
            this.Grd3.TabIndex = 55;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.BtnCtContactPersonName);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.TxtSalesTarget);
            this.panel3.Controls.Add(this.TxtQtMth);
            this.panel3.Controls.Add(this.LueCtContactPersonName);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.LueSPCode);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.DteQtStartDt);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.LueRingCode);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.LueCtCode);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(836, 259);
            this.panel3.TabIndex = 9;
            // 
            // BtnCtContactPersonName
            // 
            this.BtnCtContactPersonName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtContactPersonName.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtContactPersonName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtContactPersonName.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtContactPersonName.Appearance.Options.UseBackColor = true;
            this.BtnCtContactPersonName.Appearance.Options.UseFont = true;
            this.BtnCtContactPersonName.Appearance.Options.UseForeColor = true;
            this.BtnCtContactPersonName.Appearance.Options.UseTextOptions = true;
            this.BtnCtContactPersonName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtContactPersonName.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtContactPersonName.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtContactPersonName.Image")));
            this.BtnCtContactPersonName.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtContactPersonName.Location = new System.Drawing.Point(386, 95);
            this.BtnCtContactPersonName.Name = "BtnCtContactPersonName";
            this.BtnCtContactPersonName.Size = new System.Drawing.Size(24, 21);
            this.BtnCtContactPersonName.TabIndex = 19;
            this.BtnCtContactPersonName.ToolTip = "Add Contact Person";
            this.BtnCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtContactPersonName.ToolTipTitle = "Run System";
            this.BtnCtContactPersonName.Click += new System.EventHandler(this.BtnCtContactPersonName_Click_1);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(274, 209);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 14);
            this.label17.TabIndex = 31;
            this.label17.Text = "/month";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(274, 186);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 14);
            this.label11.TabIndex = 28;
            this.label11.Text = "month(s)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(125, 186);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 14);
            this.label7.TabIndex = 26;
            this.label7.Text = "For";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSalesTarget
            // 
            this.TxtSalesTarget.EnterMoveNextControl = true;
            this.TxtSalesTarget.Location = new System.Drawing.Point(155, 205);
            this.TxtSalesTarget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesTarget.Name = "TxtSalesTarget";
            this.TxtSalesTarget.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalesTarget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesTarget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesTarget.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesTarget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalesTarget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalesTarget.Properties.MaxLength = 18;
            this.TxtSalesTarget.Size = new System.Drawing.Size(117, 20);
            this.TxtSalesTarget.TabIndex = 30;
            this.TxtSalesTarget.Validated += new System.EventHandler(this.TxtSalesTarget_Validated);
            // 
            // TxtQtMth
            // 
            this.TxtQtMth.EnterMoveNextControl = true;
            this.TxtQtMth.Location = new System.Drawing.Point(155, 183);
            this.TxtQtMth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtMth.Name = "TxtQtMth";
            this.TxtQtMth.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQtMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtMth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtMth.Properties.Appearance.Options.UseFont = true;
            this.TxtQtMth.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQtMth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQtMth.Properties.MaxLength = 6;
            this.TxtQtMth.Size = new System.Drawing.Size(117, 20);
            this.TxtQtMth.TabIndex = 27;
            // 
            // LueCtContactPersonName
            // 
            this.LueCtContactPersonName.EnterMoveNextControl = true;
            this.LueCtContactPersonName.Location = new System.Drawing.Point(155, 95);
            this.LueCtContactPersonName.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtContactPersonName.Name = "LueCtContactPersonName";
            this.LueCtContactPersonName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.Appearance.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtContactPersonName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtContactPersonName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtContactPersonName.Properties.DropDownRows = 30;
            this.LueCtContactPersonName.Properties.NullText = "[Empty]";
            this.LueCtContactPersonName.Properties.PopupWidth = 300;
            this.LueCtContactPersonName.Size = new System.Drawing.Size(230, 20);
            this.LueCtContactPersonName.TabIndex = 18;
            this.LueCtContactPersonName.ToolTip = "F4 : Show/hide list";
            this.LueCtContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtContactPersonName.EditValueChanged += new System.EventHandler(this.LueCtContactPersonName_EditValueChanged_1);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(77, 142);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 14);
            this.label16.TabIndex = 22;
            this.label16.Text = "Sales Person";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSPCode
            // 
            this.LueSPCode.EnterMoveNextControl = true;
            this.LueSPCode.Location = new System.Drawing.Point(155, 139);
            this.LueSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSPCode.Name = "LueSPCode";
            this.LueSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSPCode.Properties.DropDownRows = 30;
            this.LueSPCode.Properties.NullText = "[Empty]";
            this.LueSPCode.Properties.PopupWidth = 300;
            this.LueSPCode.Size = new System.Drawing.Size(230, 20);
            this.LueSPCode.TabIndex = 23;
            this.LueSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSPCode.EditValueChanged += new System.EventHandler(this.LueSPCode_EditValueChanged_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(77, 207);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 29;
            this.label9.Text = "Sales Target";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteQtStartDt
            // 
            this.DteQtStartDt.EditValue = null;
            this.DteQtStartDt.EnterMoveNextControl = true;
            this.DteQtStartDt.Location = new System.Drawing.Point(155, 161);
            this.DteQtStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQtStartDt.Name = "DteQtStartDt";
            this.DteQtStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteQtStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQtStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQtStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQtStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQtStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQtStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQtStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQtStartDt.Properties.MaxLength = 8;
            this.DteQtStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQtStartDt.Size = new System.Drawing.Size(117, 20);
            this.DteQtStartDt.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(29, 163);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 14);
            this.label8.TabIndex = 24;
            this.label8.Text = "Quotation Start Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(37, 119);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 14);
            this.label6.TabIndex = 20;
            this.label6.Text = "Customer Ring Area";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueRingCode
            // 
            this.LueRingCode.EnterMoveNextControl = true;
            this.LueRingCode.Location = new System.Drawing.Point(155, 117);
            this.LueRingCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueRingCode.Name = "LueRingCode";
            this.LueRingCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.Appearance.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueRingCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRingCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueRingCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueRingCode.Properties.DropDownRows = 30;
            this.LueRingCode.Properties.NullText = "[Empty]";
            this.LueRingCode.Properties.PopupWidth = 300;
            this.LueRingCode.Size = new System.Drawing.Size(230, 20);
            this.LueRingCode.TabIndex = 21;
            this.LueRingCode.ToolTip = "F4 : Show/hide list";
            this.LueRingCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueRingCode.EditValueChanged += new System.EventHandler(this.LueRingCode_EditValueChanged_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(5, 97);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Customer Contact Person";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(155, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(79, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(155, 27);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(67, 22);
            this.ChkActInd.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(93, 76);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(155, 73);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(230, 20);
            this.LueCtCode.TabIndex = 16;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(155, 51);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(117, 20);
            this.DteDocDt.TabIndex = 14;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(119, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.BtnCtReplace);
            this.panel5.Controls.Add(this.BtnCtReplace2);
            this.panel5.Controls.Add(this.TxtCtReplace);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.MeeWhsCapacity);
            this.panel5.Controls.Add(this.LueShpMCode);
            this.panel5.Controls.Add(this.LuePtCode);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.TxtCreditLimit);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.ChkPrintSignatureInd);
            this.panel5.Controls.Add(this.TxtGoodsReturnDay);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.TxtMinDelivery);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.LueAgingAP);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.LueCurCode);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(412, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(420, 255);
            this.panel5.TabIndex = 34;
            // 
            // BtnCtReplace
            // 
            this.BtnCtReplace.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtReplace.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtReplace.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtReplace.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtReplace.Appearance.Options.UseBackColor = true;
            this.BtnCtReplace.Appearance.Options.UseFont = true;
            this.BtnCtReplace.Appearance.Options.UseForeColor = true;
            this.BtnCtReplace.Appearance.Options.UseTextOptions = true;
            this.BtnCtReplace.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtReplace.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtReplace.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtReplace.Image")));
            this.BtnCtReplace.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtReplace.Location = new System.Drawing.Point(349, 183);
            this.BtnCtReplace.Name = "BtnCtReplace";
            this.BtnCtReplace.Size = new System.Drawing.Size(24, 21);
            this.BtnCtReplace.TabIndex = 50;
            this.BtnCtReplace.ToolTip = "Find Replacement Quotation ";
            this.BtnCtReplace.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtReplace.ToolTipTitle = "Run System";
            this.BtnCtReplace.Click += new System.EventHandler(this.BtnCtReplace_Click);
            // 
            // BtnCtReplace2
            // 
            this.BtnCtReplace2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtReplace2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtReplace2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtReplace2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtReplace2.Appearance.Options.UseBackColor = true;
            this.BtnCtReplace2.Appearance.Options.UseFont = true;
            this.BtnCtReplace2.Appearance.Options.UseForeColor = true;
            this.BtnCtReplace2.Appearance.Options.UseTextOptions = true;
            this.BtnCtReplace2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtReplace2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtReplace2.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtReplace2.Image")));
            this.BtnCtReplace2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtReplace2.Location = new System.Drawing.Point(373, 183);
            this.BtnCtReplace2.Name = "BtnCtReplace2";
            this.BtnCtReplace2.Size = new System.Drawing.Size(24, 21);
            this.BtnCtReplace2.TabIndex = 51;
            this.BtnCtReplace2.ToolTip = "Show Replacement Quotation";
            this.BtnCtReplace2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtReplace2.ToolTipTitle = "Run System";
            this.BtnCtReplace2.Click += new System.EventHandler(this.BtnCtReplace2_Click);
            // 
            // TxtCtReplace
            // 
            this.TxtCtReplace.EnterMoveNextControl = true;
            this.TxtCtReplace.Location = new System.Drawing.Point(150, 182);
            this.TxtCtReplace.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtReplace.Name = "TxtCtReplace";
            this.TxtCtReplace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtReplace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtReplace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtReplace.Properties.Appearance.Options.UseFont = true;
            this.TxtCtReplace.Properties.MaxLength = 16;
            this.TxtCtReplace.Size = new System.Drawing.Size(198, 20);
            this.TxtCtReplace.TabIndex = 73;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(1, 185);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 14);
            this.label4.TabIndex = 49;
            this.label4.Text = "Replacement\'s Quotation";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeWhsCapacity
            // 
            this.MeeWhsCapacity.EnterMoveNextControl = true;
            this.MeeWhsCapacity.Location = new System.Drawing.Point(150, 160);
            this.MeeWhsCapacity.Margin = new System.Windows.Forms.Padding(5);
            this.MeeWhsCapacity.Name = "MeeWhsCapacity";
            this.MeeWhsCapacity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.Appearance.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeWhsCapacity.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeWhsCapacity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeWhsCapacity.Properties.MaxLength = 700;
            this.MeeWhsCapacity.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeWhsCapacity.Properties.ShowIcon = false;
            this.MeeWhsCapacity.Size = new System.Drawing.Size(262, 20);
            this.MeeWhsCapacity.TabIndex = 48;
            this.MeeWhsCapacity.ToolTip = "F4 : Show/hide text";
            this.MeeWhsCapacity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeWhsCapacity.ToolTipTitle = "Run System";
            // 
            // LueShpMCode
            // 
            this.LueShpMCode.EnterMoveNextControl = true;
            this.LueShpMCode.Location = new System.Drawing.Point(150, 94);
            this.LueShpMCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueShpMCode.Name = "LueShpMCode";
            this.LueShpMCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.Appearance.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShpMCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShpMCode.Properties.DropDownRows = 30;
            this.LueShpMCode.Properties.NullText = "[Empty]";
            this.LueShpMCode.Properties.PopupWidth = 300;
            this.LueShpMCode.Size = new System.Drawing.Size(262, 20);
            this.LueShpMCode.TabIndex = 40;
            this.LueShpMCode.ToolTip = "F4 : Show/hide list";
            this.LueShpMCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShpMCode.EditValueChanged += new System.EventHandler(this.LueShpMCode_EditValueChanged_1);
            // 
            // LuePtCode
            // 
            this.LuePtCode.EnterMoveNextControl = true;
            this.LuePtCode.Location = new System.Drawing.Point(150, 6);
            this.LuePtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePtCode.Name = "LuePtCode";
            this.LuePtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.Appearance.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePtCode.Properties.DropDownRows = 30;
            this.LuePtCode.Properties.NullText = "[Empty]";
            this.LuePtCode.Properties.PopupWidth = 300;
            this.LuePtCode.Size = new System.Drawing.Size(262, 20);
            this.LuePtCode.TabIndex = 33;
            this.LuePtCode.ToolTip = "F4 : Show/hide list";
            this.LuePtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePtCode.EditValueChanged += new System.EventHandler(this.LuePtCode_EditValueChanged_1);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(323, 119);
            this.label23.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 14);
            this.label23.TabIndex = 43;
            this.label23.Text = "Kg ";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(41, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 14);
            this.label10.TabIndex = 32;
            this.label10.Text = "Term Of Payment";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(323, 141);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 14);
            this.label22.TabIndex = 46;
            this.label22.Text = "Day(s)";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCreditLimit
            // 
            this.TxtCreditLimit.EnterMoveNextControl = true;
            this.TxtCreditLimit.Location = new System.Drawing.Point(150, 72);
            this.TxtCreditLimit.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCreditLimit.Name = "TxtCreditLimit";
            this.TxtCreditLimit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCreditLimit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCreditLimit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseFont = true;
            this.TxtCreditLimit.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCreditLimit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCreditLimit.Properties.MaxLength = 18;
            this.TxtCreditLimit.Size = new System.Drawing.Size(164, 20);
            this.TxtCreditLimit.TabIndex = 38;
            this.TxtCreditLimit.Validated += new System.EventHandler(this.TxtCreditLimit_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(47, 97);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 14);
            this.label14.TabIndex = 39;
            this.label14.Text = "Shipping Method";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkPrintSignatureInd
            // 
            this.ChkPrintSignatureInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkPrintSignatureInd.Location = new System.Drawing.Point(150, 226);
            this.ChkPrintSignatureInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkPrintSignatureInd.Name = "ChkPrintSignatureInd";
            this.ChkPrintSignatureInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkPrintSignatureInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPrintSignatureInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseFont = true;
            this.ChkPrintSignatureInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkPrintSignatureInd.Properties.Caption = "Print Signature";
            this.ChkPrintSignatureInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPrintSignatureInd.Size = new System.Drawing.Size(117, 22);
            this.ChkPrintSignatureInd.TabIndex = 54;
            // 
            // TxtGoodsReturnDay
            // 
            this.TxtGoodsReturnDay.EnterMoveNextControl = true;
            this.TxtGoodsReturnDay.Location = new System.Drawing.Point(150, 138);
            this.TxtGoodsReturnDay.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGoodsReturnDay.Name = "TxtGoodsReturnDay";
            this.TxtGoodsReturnDay.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGoodsReturnDay.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGoodsReturnDay.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGoodsReturnDay.Properties.Appearance.Options.UseFont = true;
            this.TxtGoodsReturnDay.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGoodsReturnDay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGoodsReturnDay.Properties.MaxLength = 6;
            this.TxtGoodsReturnDay.Size = new System.Drawing.Size(164, 20);
            this.TxtGoodsReturnDay.TabIndex = 45;
            this.TxtGoodsReturnDay.Validated += new System.EventHandler(this.TxtGoodsReturnDay_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(78, 74);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 14);
            this.label19.TabIndex = 37;
            this.label19.Text = "Credit Limit";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(28, 163);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(118, 14);
            this.label12.TabIndex = 47;
            this.label12.Text = "Warehouse Capacity";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMinDelivery
            // 
            this.TxtMinDelivery.EnterMoveNextControl = true;
            this.TxtMinDelivery.Location = new System.Drawing.Point(150, 116);
            this.TxtMinDelivery.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMinDelivery.Name = "TxtMinDelivery";
            this.TxtMinDelivery.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMinDelivery.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMinDelivery.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMinDelivery.Properties.Appearance.Options.UseFont = true;
            this.TxtMinDelivery.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMinDelivery.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMinDelivery.Properties.MaxLength = 18;
            this.TxtMinDelivery.Size = new System.Drawing.Size(164, 20);
            this.TxtMinDelivery.TabIndex = 42;
            this.TxtMinDelivery.Validated += new System.EventHandler(this.TxtMinDelivery_Validated);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(64, 140);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 14);
            this.label20.TabIndex = 44;
            this.label20.Text = "Goods Return";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(33, 31);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 14);
            this.label18.TabIndex = 34;
            this.label18.Text = "Aging AR Based On";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(46, 118);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 14);
            this.label21.TabIndex = 41;
            this.label21.Text = "Minimum Delivery";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(99, 207);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 14);
            this.label28.TabIndex = 52;
            this.label28.Text = "Remark";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(150, 204);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 700;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(262, 20);
            this.MeeRemark.TabIndex = 53;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LueAgingAP
            // 
            this.LueAgingAP.EnterMoveNextControl = true;
            this.LueAgingAP.Location = new System.Drawing.Point(150, 28);
            this.LueAgingAP.Margin = new System.Windows.Forms.Padding(5);
            this.LueAgingAP.Name = "LueAgingAP";
            this.LueAgingAP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.Appearance.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAgingAP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAgingAP.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAgingAP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAgingAP.Properties.DropDownRows = 30;
            this.LueAgingAP.Properties.NullText = "[Empty]";
            this.LueAgingAP.Properties.PopupWidth = 300;
            this.LueAgingAP.Size = new System.Drawing.Size(262, 20);
            this.LueAgingAP.TabIndex = 35;
            this.LueAgingAP.ToolTip = "F4 : Show/hide list";
            this.LueAgingAP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAgingAP.EditValueChanged += new System.EventHandler(this.LueAgingAP_EditValueChanged_1);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(91, 53);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 14);
            this.label15.TabIndex = 36;
            this.label15.Text = "Currency";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(150, 50);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 300;
            this.LueCurCode.Size = new System.Drawing.Size(164, 20);
            this.LueCurCode.TabIndex = 37;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged_1);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 481);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 10;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            this.LueFontSize.EditValueChanged += new System.EventHandler(this.LueFontSize_EditValueChanged);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 501);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 11;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmCtQt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 523);
            this.Name = "FrmCtQt";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgQuotationItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCtQtType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.TpgQuotationCity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpgPort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LuePort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpgAgent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesTarget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtContactPersonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQtStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueRingCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtReplace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeWhsCapacity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreditLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPrintSignatureInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGoodsReturnDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMinDelivery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAgingAP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgQuotationCity;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgQuotationItem;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected System.Windows.Forms.Panel panel3;
        protected System.Windows.Forms.Panel panel5;
        public DevExpress.XtraEditors.SimpleButton BtnCtContactPersonName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtSalesTarget;
        internal DevExpress.XtraEditors.TextEdit TxtQtMth;
        private DevExpress.XtraEditors.LookUpEdit LueCtContactPersonName;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueSPCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DteQtStartDt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueRingCode;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.MemoExEdit MeeWhsCapacity;
        private DevExpress.XtraEditors.LookUpEdit LueShpMCode;
        private DevExpress.XtraEditors.LookUpEdit LuePtCode;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtCreditLimit;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.CheckEdit ChkPrintSignatureInd;
        internal DevExpress.XtraEditors.TextEdit TxtGoodsReturnDay;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtMinDelivery;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraEditors.LookUpEdit LueAgingAP;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private DevExpress.XtraEditors.LookUpEdit LueUomCode;
        private DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private System.Windows.Forms.TabPage TpgAgent;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        public DevExpress.XtraEditors.SimpleButton BtnCtReplace;
        public DevExpress.XtraEditors.SimpleButton BtnCtReplace2;
        internal DevExpress.XtraEditors.TextEdit TxtCtReplace;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.TabPage TpgPort;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraEditors.LookUpEdit LuePort;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label LblSetDefault;
        public DevExpress.XtraEditors.LookUpEdit LueCtQtType;
    }
}