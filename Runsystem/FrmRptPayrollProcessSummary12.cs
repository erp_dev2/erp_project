﻿#region Update
/*
    28/08/2019 [TKG] SIER
    06/09/2019 [HAR] tambah inromasi jiwasraya ambil dari kolom life insurance
    20/09/2019 [HAR] kolom SSemployeepension masuk ke SSemployeemployment, kolom SSemployerpension masuk ke SSemployermployment   
    24/09/2019 [HAR/SIER] THP = brutto - tax
    20/03/2020 [WED/SIER] tambah beberapa kolom + print out
    06/05/2020 [WED/SIER] penyesuaian kolom
    13/07/2020 [HAR/SIER] Feedback : Fixed Allowance masih kecampur sama  tunjangan lain
    01/09/2020 [TKG/SIER] memunculkan meal, transport
    01/09/2020 [TKG/SIER] tambah transfer, ubah perhitungan fix allowance, brutto, thp
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

#endregion

namespace RunSystem
{
    public partial class FrmRptPayrollProcessSummary12 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal string mSalaryInd = "1", mRptPayrollProcessSummaryVersion ="1";
        private bool
             mIsNotFilterByAuthorization = false,
             mIsFilterBySiteHR = false,
             mIsFilterByDeptHR = false;

        private List<RptPayrollProcessSummary12> lr = null;

        private static Excel.Application objApp;
        private static Excel.Workbooks objBooks;
        private static Excel.Workbook objBook;
        private static Excel.Sheets objSheets;
        private static Excel.Worksheet objSheet;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummary12(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd(); 
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");

                lr = new List<RptPayrollProcessSummary12>();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mRptPayrollProcessSummaryVersion = Sm.GetParameter("RptPayrollProcessSummaryVersion");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, A.EmpCode, C.EmpName, C.EmpCodeOld, E.PosName, D.DeptName, C.JoinDt, ");
            SQL.AppendLine("C.ResignDt, F.OptDesc As SystemTypeDesc, G.OptDesc As PayrunPeriodDesc, H.PGName, J.SiteName, A.NPWP, ");
            SQL.AppendLine("I.OptDesc As NonTaxableIncomeDesc, A.Salary, A.WorkingDay, A.PLDay, A.PLHr, A.PLAmt, A.ProcessPLAmt, ");
            SQL.AppendLine("A.UPLDay, A.UPLHr, A.UPLAmt, A.ProcessUPLAmt, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, A.OT1Amt, A.OT2Amt, ");
            SQL.AppendLine("A.OTHolidayAmt, A.TaxableFixAllowance, A.NonTaxableFixAllowance, A.FixAllowance, ");
            //SQL.AppendLine("(A.FixAllowance -ifnull(L.GeneralAllowanceAmt, 0) - ifnull(L.PerformanceAllowanceAmt, 0) - ifnull(L.YearOfServiceAllowanceAmt, 0) - ifnull(L.VariableAllowanceAmt, 0)  ) FixAllowance, ");
            SQL.AppendLine("ifnull(K.Amt001, 0) Al001, ifnull(K.Amt004, 0) Al004, A.PerformanceValue, ");
            SQL.AppendLine("A.ADOT, A.Meal, A.Transport, A.TaxAllowance, A.SSEmployerPension, ");
            SQL.AppendLine("A.SSEmployerHealth, A.SSEmployeeHealth, (A.SSEmployerEmployment+A.SSErPension) As SSEmployerEmployment,  (A.SSEmployeeEmployment+A.SSEePension) As SSEmployeeEmployment, 0 As SSErPension, 0 As SSEePension, ");
            SQL.AppendLine("A.SSEmployeePension, A.SSErLifeInsurance, A.SSEeLifeInsurance, A.NonTaxableFixDeduction, A.TaxableFixDeduction, A.FixDeduction, A.DedEmployee, ");
            SQL.AppendLine("A.DedProduction, A.DedProdLeave, A.EmpAdvancePayment, A.SalaryAdjustment, A.Brutto, ");
            SQL.AppendLine("A.Tax, A.EOYTax, A.Amt, A.VoucherRequestPayrollDocNo, E1.DivisionName, ");
            SQL.AppendLine("IfNull(L.GeneralAllowanceAmt, 0.00) GeneralAllowanceAmt, IfNull(L.PerformanceAllowanceAmt, 0.00) PerformanceAllowanceAmt, ");
            SQL.AppendLine("IfNull(L.YearOfServiceAllowanceAmt, 0.00) YearOfServiceAllowanceAmt, IfNull(L.VariableAllowanceAmt, 0.00) VariableAllowanceAmt, ");
            SQL.AppendLine("A.Transferred ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblDivision E1 On C.DivisionCode = E1.DivisionCode ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption G On B.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr H On B.PGCode=H.PGCode ");
            SQL.AppendLine("Left Join TblOption I On A.PTKP=I.OptCode And I.OptCat='NonTaxableIncome' ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("        Select EmpCode, PayrunCode, Sum(Amt001) As Amt001, Sum(Amt004) As Amt004 ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select EmpCode, PayrunCode, Amt As Amt001, 0.00 As Amt004  ");
            SQL.AppendLine("            From TblPayrollProcessAD Where ADCode ='001' ");
            SQL.AppendLine("            Union All ");
            SQL.AppendLine("            Select EmpCode, PayrunCode, 0.00 As Amt001, Amt As Amt004  ");
            SQL.AppendLine("            From TblPayrollProcessAD Where ADCode ='004' ");
            SQL.AppendLine("        ) T ");
            SQL.AppendLine("        Group By EmpCode, PayrunCode ");
	        SQL.AppendLine(") K On A.PayrunCode=K.PayrunCode And A.EmpCode=K.EmpCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.PayrunCode, T.EmpCode, Sum(T.GeneralAllowanceAmt) GeneralAllowanceAmt, Sum(T.PerformanceAllowanceAmt) PerformanceAllowanceAmt, ");
            SQL.AppendLine("    Sum(T.YearOfServiceAllowanceAmt) YearOfServiceAllowanceAmt, Sum(T.VariableAllowanceAmt) VariableAllowanceAmt ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T1.PayrunCode, T1.EmpCode, IfNull(T1.Amt, 0.00) GeneralAllowanceAmt, 0.00 PerformanceAllowanceAmt, 0.00 YearOfServiceAllowanceAmt, 0.00 VariableAllowanceAmt ");
            SQL.AppendLine("        From TblPayrollProcessAD T1 ");
            SQL.AppendLine("        Inner Join TblParameter T2 On T2.ParCode = 'ADCodeGeneralAllowance' And T1.ADCode = T2.ParValue ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T1.PayrunCode, T1.EmpCode, 0.00 GeneralAllowanceAmt, IfNull(T1.Amt, 0.00) PerformanceAllowanceAmt, 0.00 YearOfServiceAllowanceAmt, 0.00 VariableAllowanceAmt ");
            SQL.AppendLine("        From TblPayrollProcessAD T1 ");
            SQL.AppendLine("        Inner Join TblParameter T2 On T2.ParCode = 'ADCodePerformance' And T1.ADCode = T2.ParValue ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T1.PayrunCode, T1.EmpCode, 0.00 GeneralAllowanceAmt, 0.00 PerformanceAllowanceAmt, IfNull(T1.Amt, 0.00) YearOfServiceAllowanceAmt, 0.00 VariableAllowanceAmt ");
            SQL.AppendLine("        From TblPayrollProcessAD T1 ");
            SQL.AppendLine("        Inner Join TblParameter T2 On T2.ParCode = 'ADCodeYearOfService' And T1.ADCode = T2.ParValue ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T1.PayrunCode, T1.EmpCode, 0.00 GeneralAllowanceAmt, 0.00 PerformanceAllowanceAmt, 0.00 YearOfServiceAllowanceAmt, IfNull(T1.Amt, 0.00) VariableAllowanceAmt ");
            SQL.AppendLine("        From TblPayrollProcessAD T1 ");
            SQL.AppendLine("        Inner Join TblParameter T2 On T2.ParCode = 'ADCodeVariableAllowance' And T1.ADCode = T2.ParValue ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.PayrunCode, T.EmpCode ");
            SQL.AppendLine(") L On A.PayrunCode = L.PayrunCode And A.EmpCode = L.EmpCode ");
            SQL.AppendLine("Where 1=1 ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 72;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Payrun"+Environment.NewLine+"Code",
                    "Payrun Name",
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name",

                    //6-10
                    "Old Code",
                    "Position",
                    "Department",
                    "Division",
                    "Join"+Environment.NewLine+"Date",
                    
                    //11-15
                    "Resign"+Environment.NewLine+"Date",
                    "Type",
                    "Period",
                    "Group",
                    "Site",

                    //16-20
                    "NPWP",
                    "PTKP",
                    "Salary", 
                    "Working Day", 
                    "Paid Leave"+Environment.NewLine+"(Day)", 
                    
                    //21-25
                    "Paid Leave"+Environment.NewLine+"(Hour)",  
                    "Paid Leave"+Environment.NewLine+"(Amount)", 
                    "Processed Paid"+Environment.NewLine+"Leave (Amount)", 
                    "Unpaid Leave"+Environment.NewLine+"(Day)", 
                    "Unpaid Leave"+Environment.NewLine+"(Hour)",  
                    
                    //26-30
                    "Unpaid Leave"+Environment.NewLine+"(Amount)", 
                    "Processed Unpaid"+Environment.NewLine+"Leave (Amount)", 
                    "Total OT"+Environment.NewLine+"(Hour)", 
                    "OT 2"+Environment.NewLine+"(Hour)", 
                    "OT Holiday"+Environment.NewLine+"(Hour)", 
                    
                    //31-35
                    "Total OT"+Environment.NewLine+"(Amount)", 
                    "OT 2"+Environment.NewLine+"(Amount)", 
                    "OT Holiday"+Environment.NewLine+"(Amount)", 
                    "Taxable Fixed"+Environment.NewLine+"Allowance", 
                    "Non Taxable Fixed"+Environment.NewLine+"Allowance",
                    
                    //36-40
                    Sm.GetValue("Select ADName From TblAllowanceDeduction Where AdCode = '001';").Replace(" ", Environment.NewLine),
                    Sm.GetValue("Select ADName From TblAllowanceDeduction Where AdCode = '004';").Replace(" ", Environment.NewLine),
                    "Fixed"+Environment.NewLine+"Allowance", 
                    "Performance"+Environment.NewLine+"Allowance", 
                    "General"+Environment.NewLine+"Allowance", 

                    //41-45
                    "Work"+Environment.NewLine+"Allowance", 
                    "Year of Service"+Environment.NewLine+"Allowance", 
                    "Variable"+Environment.NewLine+"Allowance", 
                    "Total THP",
                    "OT"+Environment.NewLine+"Allowance",

                    //46-50
                    "",
                    "Meal", 
                    "Transport", 
                    "Tax"+Environment.NewLine+"Allowance",
                    "SS Employer"+Environment.NewLine+"Health", 

                    //51-55
                    "SS Employee"+Environment.NewLine+"Health", 
                    "SS Employer"+Environment.NewLine+"Employment",
                    "SS Employee"+Environment.NewLine+"Employment", 
                    "SS Employer"+Environment.NewLine+"Pension",
                    "SS Employee"+Environment.NewLine+"Pension",

                    //56-60 
                    "SS Employer"+Environment.NewLine+"DPLK",
                    "SS Employee"+Environment.NewLine+"DPLK",
                    "SS Employee"+Environment.NewLine+"Jiwasraya",
                    "SS Employeer"+Environment.NewLine+"Jiwasraya",
                    "Non Taxable Employee's"+Environment.NewLine+"Deduction",

                    //61-65
                    "Taxable Employee's"+Environment.NewLine+"Deduction",
                    "Fixed"+Environment.NewLine+"Deduction", 
                    "Employee's"+Environment.NewLine+"Advance Payment",
                    "Salary"+Environment.NewLine+"Adjustment",
                    "Salary Before"+Environment.NewLine+"Adjustment",

                    //66-70
                    "Brutto",
                    "Tax", 
                    "End of Year"+Environment.NewLine+"Tax", 
                    "Take Home Pay", 
                    "Voucher Request#"+Environment.NewLine+"(Payroll)",

                    //71
                    "Transferred"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 200, 80, 200, 
                    
                    //6-10
                    80, 200, 200, 200, 100, 
                    
                    //11-15
                    100, 100, 100, 100, 150, 
                    
                    //16-20
                    130, 100, 100, 100, 100, 
                    
                    //21-25
                    100, 100, 100, 100, 100, 
                    
                    //26-30
                    100, 100, 100, 100, 100, 

                    //31-35
                    100, 100, 100, 130, 130, 

                    //36-40
                    100, 100, 100, 100, 100, 

                    //41-50
                    100, 100, 100, 100, 100, 

                    //46-50
                    20, 100, 100, 130, 100, 
                    
                    //51-55
                    100, 100, 100, 100, 100, 
                    
                    //56-60
                    100, 100, 120, 120, 120, 
                    
                    //61-65
                    120, 100, 130, 100, 150, 

                    //66-70
                    150, 130, 100, 100, 130,

                    //71
                    130
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 1, 46 });
            Sm.GrdFormatDec(Grd1, new int[] { 
                18, 19, 20, 21, 
                22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
                32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 
                42, 43, 44, 45, 47, 48, 49, 50, 51, 52, 
                53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 
                63, 64, 65, 66, 67, 68, 69,
                71
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10, 11 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] 
            { 
                0, 
                2, 3, 4, 5, 6, 7, 8, 9, 10, 
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 36, 37, 39, 40, 41, 
                42, 42, 44, 45, 47, 48, 49, 50, 51, 52, 
                53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 
                63, 64, 65, 66, 67, 68, 69, 70,
                71
            });
            Sm.GrdColInvisible(Grd1, new int[] 
            { 
                4, 8, 10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 
                22, 23, 24, 25, 26, 27, 29, 30, 32, 33, 36, 37, 
                45, 46, 50, 52, 54, 56, 59, 60, 61,
                63, 64, 65, 68
            }, false);
            Grd1.Cols[49].Move(67);
            Grd1.Cols[71].Move(70);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 50, 52, 54, 56, 59 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowData()
        {
            if (Sm.IsTxtEmpty(TxtPayCod, "Payrun Code", false) || IsPayrunCodeNotValid()) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                lr.Clear();
                PrepData(ref lr);
                if (lr.Count > 0)
                {
                    ShowDataInGrd(ref lr);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    Sm.FormShowDialog(
                        new FrmRptPayrollProcessSummary12Dlg(
                            this,
                            Sm.GetGrdStr(Grd1, r, 2),
                            Sm.GetGrdStr(Grd1, r, 4)
                            ));
                }
            }
            if (e.ColIndex == 46 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 45) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessADOT(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                Sm.FormShowDialog(
                    new FrmRptPayrollProcessSummary12Dlg(
                        this,
                        Sm.GetGrdStr(Grd1, r, 2),
                        Sm.GetGrdStr(Grd1, r, 4)
                        ));
            }
            if (e.ColIndex == 46 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 45) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessADOT(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void PrepData(ref List<RptPayrollProcessSummary12> l)
        {
            string Filter = " ";

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "C.EmpName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = mSQL + Filter + " Order By A.PayrunCode, C.EmpName; ";

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {                
                    //0
                    "PayrunCode",
                    //1-5
                    "PayrunName", "EmpCode", "EmpName", "EmpCodeOld", "PosName",
                    //6-10
                    "DeptName", "JoinDt", "ResignDt", "SystemTypeDesc", "PayrunPeriodDesc",
                    //11-15
                    "PGName", "SiteName", "NPWP", "NonTaxableIncomeDesc", "Salary", 
                    //16-20
                    "WorkingDay", "PLDay", "PLHr",  "PLAmt", "ProcessPLAmt", 
                    //21-25
                    "UPLDay", "UPLHr", "UPLAmt", "ProcessUPLAmt", "OT1Hr", 
                    //26-30
                    "OT2Hr", "OTHolidayHr", "OT1Amt", "OT2Amt", "OTHolidayAmt", 
                    //31-35
                    "TaxableFixAllowance", "NonTaxableFixAllowance", "Al001", "Al004", "FixAllowance", 
                    //36-40
                    "PerformanceValue",  "ADOT", "Meal", "Transport", "TaxAllowance",
                    //41-45
                    "SSEmployerHealth", "SSEmployeeHealth", "SSEmployerEmployment", "SSEmployeeEmployment", "SSErPension", 
                    //46-50
                    "SSEePension", "SSEmployerPension", "SSEmployeePension", "SSErLifeInsurance", "SSEeLifeInsurance", 
                    //51-55
                    "NonTaxableFixDeduction", "TaxableFixDeduction", "FixDeduction", "EmpAdvancePayment", "SalaryAdjustment", 
                    //56-60
                    "Brutto", "Tax", "EOYTax", "Amt", "VoucherRequestPayrollDocNo",
                    //61-65
                    "DivisionName", "GeneralAllowanceAmt", "PerformanceAllowanceAmt", "YearOfServiceAllowanceAmt", "VariableAllowanceAmt",
                    //66
                    "Transferred"
                });

                if (dr.HasRows)
                {
                    int mNo = 1;
                    while (dr.Read())
                    {
                        l.Add(new RptPayrollProcessSummary12()
                        {
                            No = mNo, 
                            PayrunCode = Sm.DrStr(dr, c[0]),
                            PayrunName = Sm.DrStr(dr, c[1]),
                            EmployeeCode = Sm.DrStr(dr, c[2]),
                            EmployeeName = Sm.DrStr(dr, c[3]),
                            OldCode = Sm.DrStr(dr, c[4]),
                            Position = Sm.DrStr(dr, c[5]),
                            Department = Sm.DrStr(dr, c[6]),
                            Division = Sm.DrStr(dr, c[61]),
                            JoinDate = Sm.DrStr(dr, c[7]),
                            ResignDate = Sm.DrStr(dr, c[8]),
                            Type = Sm.DrStr(dr, c[9]),
                            Period = Sm.DrStr(dr, c[10]),
                            Group = Sm.DrStr(dr, c[11]),
                            Site = Sm.DrStr(dr, c[12]),
                            NPWP = Sm.DrStr(dr, c[13]),
                            PTKP = Sm.DrStr(dr, c[14]),
                            Salary = Sm.DrDec(dr, c[15]),
                            WorkingDay = Sm.DrDec(dr, c[16]),
                            PaidLeaveDay = Sm.DrDec(dr, c[17]),
                            PaidLeaveHour = Sm.DrDec(dr, c[18]),
                            PaidLeaveAmount = Sm.DrDec(dr, c[19]),
                            ProcessedPaidLeaveAmount = Sm.DrDec(dr, c[20]),
                            UnpaidLeaveDay = Sm.DrDec(dr, c[21]),
                            UnpaidLeaveHour = Sm.DrDec(dr, c[22]),
                            UnpaidLeaveAmount = Sm.DrDec(dr, c[23]),
                            ProcessedUnpaidLeaveAmount = Sm.DrDec(dr, c[24]),
                            OT1Hour = Sm.DrDec(dr, c[25]),
                            OT2Hour = Sm.DrDec(dr, c[26]),
                            OTHolidayHour = Sm.DrDec(dr, c[27]),
                            OT1Amount = Sm.DrDec(dr, c[28]),
                            OT2Amount = Sm.DrDec(dr, c[29]),
                            OTHolidayAmount = Sm.DrDec(dr, c[30]),
                            TaxableFixedAllowance = Sm.DrDec(dr, c[31]),
                            NonTaxableFixedAllowance = Sm.DrDec(dr, c[32]),
                            ADName1 = Sm.DrDec(dr, c[33]),
                            ADName2 = Sm.DrDec(dr, c[34]),
                            FixedAllowance = Sm.DrDec(dr, c[35]),
                            PerformanceAllowance = Sm.DrDec(dr, c[36]),
                            GeneralAllowance = Sm.DrDec(dr, c[62]),
                            WorkAllowance = Sm.DrDec(dr, c[63]),
                            YearofServiceAllowance = Sm.DrDec(dr, c[64]),
                            VariableAllowance = Sm.DrDec(dr, c[65]),
                            TotalTHP = Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[36]) + Sm.DrDec(dr, c[62]) + Sm.DrDec(dr, c[63]) + Sm.DrDec(dr, c[64]) + Sm.DrDec(dr, c[65]),
                            OTAllowance = Sm.DrDec(dr, c[37]),
                            Meal = Sm.DrDec(dr, c[38]),
                            Transport = Sm.DrDec(dr, c[39]),
                            TaxAllowance = Sm.DrDec(dr, c[40]),
                            SSEmployerHealth = Sm.DrDec(dr, c[41]),
                            SSEmployeeHealth = Sm.DrDec(dr, c[42]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[43]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[44]),
                            SSEmployerPension = Sm.DrDec(dr, c[45]),
                            SSEmployeePension = Sm.DrDec(dr, c[46]),
                            SSEmployerDPLK = Sm.DrDec(dr, c[47]),
                            SSEmployeeDPLK = Sm.DrDec(dr, c[48]),
                            SSEmployeeJiwasraya = Sm.DrDec(dr, c[49]),
                            SSEmployeerJiwasraya = Sm.DrDec(dr, c[50]),
                            NonTaxableEmployeeDeduction = Sm.DrDec(dr, c[51]),
                            TaxableEmployeeDeduction = Sm.DrDec(dr, c[52]),
                            FixedDeduction = Sm.DrDec(dr, c[53]),
                            EmployeeAdvancePayment = Sm.DrDec(dr, c[54]),
                            SalaryAdjustment = Sm.DrDec(dr, c[55]),
                            SalaryBeforeAdjustment = 0m,
                            Brutto = Sm.DrDec(dr, c[56]),
                            Tax = Sm.DrDec(dr, c[57]),
                            EndofYearTax = Sm.DrDec(dr, c[58]),
                            TakeHomePay = Sm.DrDec(dr, c[59]),
                            VoucherRequestDocNoPayroll = Sm.DrStr(dr, c[60]),
                            Transferred = Sm.DrDec(dr, c[66])
                        });

                        mNo += 1;
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach(var x in l)
                {
                    if (x.SalaryAdjustment < 0m) x.SalaryBeforeAdjustment = x.Brutto + x.SalaryAdjustment;
                    else if (x.SalaryAdjustment > 0m) x.SalaryBeforeAdjustment = x.Brutto - x.SalaryAdjustment;

                    x.OT1Amount = x.OT1Amount + x.OT2Amount + x.OTHolidayAmount;
                    x.OT1Hour = x.OT1Hour + x.OT2Hour + x.OTHolidayHour;
                }
            }
        }

        private void ShowDataInGrd(ref List<RptPayrollProcessSummary12> l)
        {
            Sm.ClearGrd(Grd1, false);
            int No = 0;
            foreach(var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[No, 0].Value = No + 1;
                Grd1.Cells[No, 2].Value = x.PayrunCode;
                Grd1.Cells[No, 3].Value = x.PayrunName;
                Grd1.Cells[No, 4].Value = x.EmployeeCode;
                Grd1.Cells[No, 5].Value = x.EmployeeName;
                Grd1.Cells[No, 6].Value = x.OldCode;
                Grd1.Cells[No, 7].Value = x.Position;
                Grd1.Cells[No, 8].Value = x.Department;
                Grd1.Cells[No, 9].Value = x.Division;
                Grd1.Cells[No, 10].Value = x.JoinDate;
                Grd1.Cells[No, 11].Value = x.ResignDate;
                Grd1.Cells[No, 12].Value = x.Type;
                Grd1.Cells[No, 13].Value = x.Period;
                Grd1.Cells[No, 14].Value = x.Group;
                Grd1.Cells[No, 15].Value = x.Site;
                Grd1.Cells[No, 16].Value = x.NPWP;
                Grd1.Cells[No, 17].Value = x.PTKP;
                Grd1.Cells[No, 18].Value = x.Salary;
                Grd1.Cells[No, 19].Value = x.WorkingDay;
                Grd1.Cells[No, 20].Value = x.PaidLeaveDay;
                Grd1.Cells[No, 21].Value = x.PaidLeaveHour;
                Grd1.Cells[No, 22].Value = x.PaidLeaveAmount;
                Grd1.Cells[No, 23].Value = x.ProcessedPaidLeaveAmount;
                Grd1.Cells[No, 24].Value = x.UnpaidLeaveDay;
                Grd1.Cells[No, 25].Value = x.UnpaidLeaveHour;
                Grd1.Cells[No, 26].Value = x.UnpaidLeaveAmount;
                Grd1.Cells[No, 27].Value = x.ProcessedUnpaidLeaveAmount;
                Grd1.Cells[No, 28].Value = x.OT1Hour;
                Grd1.Cells[No, 29].Value = x.OT2Hour;
                Grd1.Cells[No, 30].Value = x.OTHolidayHour;
                Grd1.Cells[No, 31].Value = x.OT1Amount;
                Grd1.Cells[No, 32].Value = x.OT2Amount;
                Grd1.Cells[No, 33].Value = x.OTHolidayAmount;
                Grd1.Cells[No, 34].Value = x.TaxableFixedAllowance;
                Grd1.Cells[No, 35].Value = x.NonTaxableFixedAllowance;
                Grd1.Cells[No, 36].Value = x.ADName1;
                Grd1.Cells[No, 37].Value = x.ADName2;
                Grd1.Cells[No, 38].Value = x.FixedAllowance;
                Grd1.Cells[No, 39].Value = x.PerformanceAllowance;
                Grd1.Cells[No, 40].Value = x.GeneralAllowance;
                Grd1.Cells[No, 41].Value = x.WorkAllowance;
                Grd1.Cells[No, 42].Value = x.YearofServiceAllowance;
                Grd1.Cells[No, 43].Value = x.VariableAllowance;
                Grd1.Cells[No, 44].Value = x.TotalTHP;
                Grd1.Cells[No, 45].Value = x.OTAllowance;
                Grd1.Cells[No, 47].Value = x.Meal;
                Grd1.Cells[No, 48].Value = x.Transport;
                Grd1.Cells[No, 49].Value = x.TaxAllowance;
                Grd1.Cells[No, 50].Value = x.SSEmployerHealth;
                Grd1.Cells[No, 51].Value = x.SSEmployeeHealth;
                Grd1.Cells[No, 52].Value = x.SSEmployerEmployment;
                Grd1.Cells[No, 53].Value = x.SSEmployeeEmployment;
                Grd1.Cells[No, 54].Value = x.SSEmployerPension;
                Grd1.Cells[No, 55].Value = x.SSEmployeePension;
                Grd1.Cells[No, 56].Value = x.SSEmployerDPLK;
                Grd1.Cells[No, 57].Value = x.SSEmployeeDPLK;
                Grd1.Cells[No, 58].Value = x.SSEmployeeJiwasraya;
                Grd1.Cells[No, 59].Value = x.SSEmployeerJiwasraya;
                Grd1.Cells[No, 60].Value = x.NonTaxableEmployeeDeduction;
                Grd1.Cells[No, 61].Value = x.TaxableEmployeeDeduction;
                Grd1.Cells[No, 62].Value = x.FixedDeduction;
                Grd1.Cells[No, 63].Value = x.EmployeeAdvancePayment;
                Grd1.Cells[No, 64].Value = x.SalaryAdjustment;
                Grd1.Cells[No, 65].Value = x.SalaryBeforeAdjustment;
                Grd1.Cells[No, 66].Value = x.Brutto;
                Grd1.Cells[No, 67].Value = x.Tax;
                Grd1.Cells[No, 68].Value = x.EndofYearTax;
                Grd1.Cells[No, 69].Value = x.TakeHomePay;
                Grd1.Cells[No, 70].Value = x.VoucherRequestDocNoPayroll;
                Grd1.Cells[No, 71].Value = x.Transferred;

                No += 1;
            }

            Grd1.BeginUpdate();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
            { 
                18, 19, 20, 21, 
                22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
                32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 
                42, 43, 44, 45, 47, 48, 49, 50, 51, 52, 
                53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 
                63, 64, 65, 66, 67, 68, 69,
                71
            });
            Grd1.EndUpdate();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return true;
            }
            return false;
        }

        private bool IsPayrunCodeNotValid()
        {
            if (TxtPayCod.Text.Length < 6)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input a 6 digit minimum of payrun code (ex : 202003)");
                TxtPayCod.Focus();
                return true;
            }

            return false;
        }

        override protected void PrintData()
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            if (IsGrdEmpty() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            
            var l = new List<ExportedColumnHdr>();
            var l0 = new List<PayrollProcess>();
            var l2 = new List<ExportedColumnDtl>();
            var l3 = new List<SumExportedColumnDtl>();
            var l4 = new List<Sign>();

            GetColumnHdr(ref l);
            GetColumnDtl(ref l2);

            //if (l2.Count > 0)
            //{
            //    SumDtl(ref l2, ref l3);
            //}

            string[] TableName = { "PayrollProcess", "ExportedColumnHdr", "ExportedColumnDtl", "SumExportedColumnDtl", "Sign" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where Menucode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("(Select Date_Format(Concat(@PayrunCode, '01'), '%M %Y')) As Period ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                string mPayrunCode = string.Empty;

                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    {
                        mPayrunCode = Sm.Left(Sm.GetGrdStr(Grd1, i, 2), 6);
                        break;
                    }
                }
                Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                        "CompanyLogo",

                         //1-5
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyPhone",
                        "MenuDesc",
                        "Period"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l0.Add(new PayrollProcess()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            MenuDesc = Sm.DrStr(dr, c[4]),
                            Period = Sm.DrStr(dr, c[5]).ToUpper(),
                            UserName = Gv.CurrentUserCode,
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            #endregion

            #region Sign

            l4.Add(new Sign()
            {
                PayrollProcessPrintOutSignName1 = Sm.GetParameter("PayrollProcessPrintOutSignName1"),
                PayrollProcessPrintOutSignName2 = Sm.GetParameter("PayrollProcessPrintOutSignName2"),
                PayrollProcessPrintOutSignName3 = Sm.GetParameter("PayrollProcessPrintOutSignName3"),
                PayrollProcessPrintOutSignName4 = Sm.GetParameter("PayrollProcessPrintOutSignName4"),
                PayrollProcessPrintOutPosName1 = Sm.GetParameter("PayrollProcessPrintOutPosName1"),
                PayrollProcessPrintOutPosName2 = Sm.GetParameter("PayrollProcessPrintOutPosName2"),
                PayrollProcessPrintOutPosName3 = Sm.GetParameter("PayrollProcessPrintOutPosName3"),
                PayrollProcessPrintOutPosName4 = Sm.GetParameter("PayrollProcessPrintOutPosName4"),
                DocDt = string.Concat(Sm.GetParameter("CompanyLocation1"), ", ", String.Format("{0:dd MMM yyyy}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())))
            });

            #endregion

            myLists.Add(l0);
            myLists.Add(l);
            myLists.Add(l2);
            myLists.Add(l3);
            myLists.Add(l4);

            Sm.PrintReport("PayrollProcess12", myLists, TableName, false);

            l0.Clear(); l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
        }

        private void ShowPayrollProcessADOT(string PayrunCode, string EmpCode)
        {
            StringBuilder
                SQL = new StringBuilder(),
                Msg = new StringBuilder();

            SQL.AppendLine("Select B.ADName, Sum(A.Amt) As Amt, Sum(A.Duration) As Duration ");
            SQL.AppendLine("From TblPayrollProcessADOT A ");
            SQL.AppendLine("Left Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Group By B.ADName ");
            SQL.AppendLine("Order By B.ADName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt", "Duration" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append("Allowance : "); 
                            Msg.AppendLine(Sm.DrStr(dr, c[0]));
                            Msg.Append("Amount : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                            Msg.Append("Duration : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[2]), 2));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private bool IsExcelExist()
        {
            bool f = true;
            if (Type.GetTypeFromProgID("Excel.Application") == null)
            {
                Sm.StdMsg(mMsgType.Warning, "No excel application in your PC");
                f = false;
            }
            return f;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        #endregion

        #region Button Events

        //override protected void ExportToExcel()
        //{
        //    if (IsGrdEmpty()) return;

        //    var l = new List<ExportedColumnHdr>();
        //    var l2 = new List<ExportedColumnDtl>();
        //    var l3 = new List<SumExportedColumnDtl>();

        //    GetColumnHdr(ref l);
        //    GetColumnDtl(ref l2);

        //    if (l2.Count > 0)
        //    {
        //        SumDtl(ref l2, ref l3);
        //        ExportToExcel2(ref l, ref l2, ref l3);
        //    }

        //    l.Clear(); l2.Clear(); l3.Clear();
        //}

        private void ExportToExcel2(ref List<ExportedColumnHdr> l, ref List<ExportedColumnDtl> l2, ref List<SumExportedColumnDtl> l3)
        {
            //System.Globalization.CultureInfo mySaveCI = System.Threading.Thread.CurrentThread.CurrentCulture;
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            //if (!IsExcelExist()) return;

            //Cursor.Current = Cursors.WaitCursor;

            //// Instantiate Excel and start a new workbook.
            //objApp = new Excel.Application();
            //objBooks = objApp.Workbooks;
            //objBook = objBooks.Add(Missing.Value);
            //objSheets = objBook.Worksheets;
            //objSheet = (Excel.Worksheet)objSheets.get_Item(1);

            //int Col = 0;

            //var AdditionalHeaderRowCount = 1;
            //var HeaderColCount = l.Count;

            //object[,] arr = new object[l2.Count + AdditionalHeaderRowCount + 2, l.Count + 1 + (AdditionalHeaderRowCount * HeaderColCount)];

            //object[] arrformat = new object[l.Count + 1];
            
            //for (int iCol = 0; iCol < l.Count; iCol++)
            //{                
            //    for (int iRow = 1; iRow >= 0; iRow--)
            //    {
            //        arr[0, Col] = l[iCol].ColName.Replace(Environment.NewLine, " ");
            //    }
            //    Col++;
            //}

            //int Row = 1, RowTemp = 0;
            //// Export the contents of iGrid
            //foreach (ExportedColumnDtl row in l2)
            //{
            //    string FormatType = string.Empty;

            //    arr[Row, 0] = row.PayrunCode;
            //    arrformat[0] = FormatType;

            //    arr[Row, 1] = row.EmpName;
            //    arrformat[1] = FormatType;

            //    arr[Row, 2] = row.EmploymentStatus;
            //    arrformat[2] = FormatType;

            //    arr[Row, 3] = row.SiteName;
            //    arrformat[3] = FormatType;

            //    arr[Row, 4] = row.DeptName;
            //    arrformat[4] = FormatType;

            //    arr[Row, 5] = row.DivisionName;
            //    arrformat[5] = FormatType;

            //    arr[Row, 6] = row.Salary;
            //    arrformat[6] = "#,##0.00";

            //    arr[Row, 7] = row.PerformanceAllowance;
            //    arrformat[7] = "#,##0.00";

            //    arr[Row, 8] = row.GeneralAllowanceAmt;
            //    arrformat[8] = "#,##0.00";

            //    arr[Row, 9] = row.PerformanceAllowanceAmt;
            //    arrformat[9] = "#,##0.00";

            //    arr[Row, 10] = row.YearOfServiceAllowanceAmt;
            //    arrformat[10] = "#,##0.00";

            //    arr[Row, 11] = row.VariableAllowanceAmt;
            //    arrformat[11] = "#,##0.00";

            //    arr[Row, 12] = row.TotalTHP;
            //    arrformat[12] = "#,##0.00";

            //    arr[Row, 13] = row.OTAllowance;
            //    arrformat[13] = "#,##0.00";

            //    arr[Row, 14] = row.Brutto;
            //    arrformat[14] = "#,##0.00";

            //    arr[Row, 15] = row.TaxAmt;
            //    arrformat[15] = "#,##0.00";

            //    arr[Row, 16] = row.TotalDeduction;
            //    arrformat[16] = "#,##0.00";

            //    arr[Row, 17] = row.SalaryAdjustment;
            //    arrformat[17] = "#,##0.00";

            //    arr[Row, 18] = row.SalaryAfterDeduction;
            //    arrformat[18] = "#,##0.00";

            //    arr[Row, 19] = row.THP;
            //    arrformat[19] = "#,##0.00";

            //    Row += 1;                
            //}

            //if (l3.Count > 0)
            //{
            //    foreach(var row in l3)
            //    {
            //        arr[Row, 6] = row.Salary;
            //        arrformat[6] = "#,##0.00";

            //        arr[Row, 7] = row.PerformanceAllowance;
            //        arrformat[7] = "#,##0.00";

            //        arr[Row, 8] = row.GeneralAllowanceAmt;
            //        arrformat[8] = "#,##0.00";

            //        arr[Row, 9] = row.PerformanceAllowanceAmt;
            //        arrformat[9] = "#,##0.00";

            //        arr[Row, 10] = row.YearOfServiceAllowanceAmt;
            //        arrformat[10] = "#,##0.00";

            //        arr[Row, 11] = row.VariableAllowanceAmt;
            //        arrformat[11] = "#,##0.00";

            //        arr[Row, 12] = row.TotalTHP;
            //        arrformat[12] = "#,##0.00";

            //        arr[Row, 13] = row.OTAllowance;
            //        arrformat[13] = "#,##0.00";

            //        arr[Row, 14] = row.Brutto;
            //        arrformat[14] = "#,##0.00";

            //        arr[Row, 15] = row.TaxAmt;
            //        arrformat[15] = "#,##0.00";

            //        arr[Row, 16] = row.TotalDeduction;
            //        arrformat[16] = "#,##0.00";

            //        arr[Row, 17] = row.SalaryAdjustment;
            //        arrformat[17] = "#,##0.00";

            //        arr[Row, 18] = row.SalaryAfterDeduction;
            //        arrformat[18] = "#,##0.00";

            //        arr[Row, 19] = row.THP;
            //        arrformat[19] = "#,##0.00";
            //    }
            //}

            //Excel.Range c1 = (Excel.Range)objSheet.Cells[1, 1];
            //Excel.Range c2 = (Excel.Range)objSheet.Cells[3 + l2.Count, l.Count];
            //Excel.Range range = objSheet.get_Range(c1, c2);

            //range.Value2 = arr;

            //Excel.Range cHdr1 = (Excel.Range)objSheet.Cells[1, 1];
            //Excel.Range cHdr2 = (Excel.Range)objSheet.Cells[1, l.Count];
            //Excel.Range rangeHdr = objSheet.get_Range(cHdr1, cHdr2);

            //rangeHdr.Font.Bold = true;

            //for (int iCol = 0; iCol < arrformat.GetLength(0); iCol++)
            //{
            //    if ((arrformat[iCol] != null) && (arrformat[iCol].ToString() != string.Empty))
            //    {
            //        Excel.Range cDet1 = (Excel.Range)objSheet.Cells[2, iCol + 1];
            //        //Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + Grd.Rows.Count - 1, iCol + 1];
            //        Excel.Range cDet2 = (Excel.Range)objSheet.Cells[1 + l2.Count, iCol + 1];
            //        Excel.Range rangeDet = objSheet.get_Range(cDet1, cDet2);
            //        rangeDet.NumberFormat = arrformat[iCol];
            //    }
            //}

            ////Auto Fit
            //objSheet.Cells.Select();
            //objSheet.Columns.AutoFit();
            //objSheet.Rows.AutoFit();

            //System.Threading.Thread.CurrentThread.CurrentCulture = mySaveCI;

            ////Return control of Excel to the user.
            //objApp.Visible = true;
            //objApp.UserControl = true;
        }

        private void SumDtl(ref List<ExportedColumnDtl> l2, ref List<SumExportedColumnDtl> l3)
        {
            decimal mSalary = 0m, mPerformanceAllowance = 0m, mGeneralAllowanceAmt = 0m,
                mPerformanceAllowanceAmt = 0m, mYearOfServiceAllowanceAmt = 0m,
                mVariableAllowanceAmt = 0m, mTotalTHP = 0m, mOTAllowance = 0m,
                mBrutto = 0m, mTaxAmt = 0m, mTotalDeduction = 0m,
                mSalaryAdjustment = 0m, mSalaryAfterDeduction = 0m, mTHP = 0m;

            foreach(var x in l2)
            {
                //mSalary += x.Salary;
                //mPerformanceAllowance += x.PerformanceAllowance;
                //mGeneralAllowanceAmt += x.GeneralAllowanceAmt;
                //mPerformanceAllowanceAmt += x.PerformanceAllowanceAmt;
                //mYearOfServiceAllowanceAmt += x.YearOfServiceAllowanceAmt;
                //mVariableAllowanceAmt += x.VariableAllowanceAmt;
                //mTotalTHP += x.TotalTHP;
                //mOTAllowance += x.OTAllowance;
                //mBrutto += x.Brutto;
                //mTaxAmt += x.TaxAmt;
                //mTotalDeduction += x.TotalDeduction;
                //mSalaryAdjustment += x.SalaryAdjustment;
                //mSalaryAfterDeduction += x.SalaryAfterDeduction;
                //mTHP += x.THP;
            }

            l3.Add(new SumExportedColumnDtl()
            {
                Salary = mSalary,
                PerformanceAllowance = mPerformanceAllowance,
                GeneralAllowanceAmt = mGeneralAllowanceAmt,
                PerformanceAllowanceAmt = mPerformanceAllowanceAmt,
                YearOfServiceAllowanceAmt = mYearOfServiceAllowanceAmt,
                VariableAllowanceAmt = mVariableAllowanceAmt,
                TotalTHP = mTotalTHP,
                OTAllowance = mOTAllowance,
                Brutto = mBrutto,
                TaxAmt = mTaxAmt,
                TotalDeduction = mTotalDeduction,
                SalaryAdjustment = mSalaryAdjustment,
                SalaryAfterDeduction = mSalaryAfterDeduction,
                THP = mTHP
            });
        }

        private void GetColumnHdr(ref List<ExportedColumnHdr> l)
        {
            string[] ColName = new string[]
            {
                "Payrun Code", "Employee", "Employment Status", "Site", 
                "Department", "Division", "Salary", "Performance Allowance", 
                "General Allowance", "Performance Allowance", "Year Of Service Allowance", 
                "Variable Allowance", "Total THP", "OT Allowance", "Brutto", "Tax", 
                "Total Deduction", "Salary Adjustment", "Salary"+Environment.NewLine+"After Deduction", "THP", 
            };

            foreach (var x in ColName)
            {
                l.Add(new ExportedColumnHdr()
                {
                    ColName = x
                });
            }
        }

        private void GetColumnDtl(ref List<ExportedColumnDtl> l2)
        {
            #region Old Code
            //for (int i = 0; i < Grd1.Rows.Count; ++i)
            //{
            //    if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
            //    {
            //        l2.Add(new ExportedColumnDtl()
            //        {
            //            PayrunCode = Sm.GetGrdStr(Grd1, i, 2),
            //            EmpName = Sm.GetGrdStr(Grd1, i, 5),
            //            EmploymentStatus = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'EmploymentStatus' And OptCode = (Select EmploymentStatus From TblEmployee Where EmpCode = @Param) Limit 1;", Sm.GetGrdStr(Grd1, i, 4)),
            //            SiteName = Sm.GetGrdStr(Grd1, i, 15),
            //            DeptName = Sm.GetGrdStr(Grd1, i, 8),
            //            DivisionName = Sm.GetGrdStr(Grd1, i, 9),
            //            Salary = Sm.GetGrdDec(Grd1, i, 18),
            //            PerformanceAllowance = Sm.GetGrdDec(Grd1, i, 39),
            //            GeneralAllowanceAmt = Sm.GetGrdDec(Grd1, i, 40),
            //            PerformanceAllowanceAmt = Sm.GetGrdDec(Grd1, i, 41),
            //            YearOfServiceAllowanceAmt = Sm.GetGrdDec(Grd1, i, 42),
            //            VariableAllowanceAmt = Sm.GetGrdDec(Grd1, i, 43),
            //            TotalTHP = Sm.GetGrdDec(Grd1, i, 44),
            //            OTAllowance = Sm.GetGrdDec(Grd1, i, 45),
            //            Brutto = Sm.GetGrdDec(Grd1, i, 66),
            //            TaxAmt = Sm.GetGrdDec(Grd1, i, 67),
            //            TotalDeduction = Sm.GetGrdDec(Grd1, i, 51) + Sm.GetGrdDec(Grd1, i, 53) + Sm.GetGrdDec(Grd1, i, 57) + Sm.GetGrdDec(Grd1, i, 58),
            //            SalaryAdjustment = Sm.GetGrdDec(Grd1, i, 64),
            //            SalaryAfterDeduction = Sm.GetGrdDec(Grd1, i, 66) - Sm.GetGrdDec(Grd1, i, 67) - (Sm.GetGrdDec(Grd1, i, 51) + Sm.GetGrdDec(Grd1, i, 53) + Sm.GetGrdDec(Grd1, i, 57) + Sm.GetGrdDec(Grd1, i, 58)),
            //            THP = Sm.GetGrdDec(Grd1, i, 69)
            //        });
            //    }
            //}
            #endregion

            foreach(var x in lr)
            {
                l2.Add(new ExportedColumnDtl()
                {
                    PayrunCode = x.PayrunCode,
                    PayrunName = x.PayrunName,
                    EmpName = x.EmployeeName,
                    OldCode = x.OldCode,
                    Position = x.Position,
                    DivisionName = x.Division,
                    PTKP = x.PTKP,
                    Salary = x.Salary,
                    FixedAllowance = x.FixedAllowance,
                    OT1Hour = x.OT1Hour,
                    OT1Amount = x.OT1Amount,
                    SSEmployeeHealth = x.SSEmployeeHealth,
                    SSEmployeeEmployment = x.SSEmployeeEmployment,
                    SSEmployeeDPLK = x.SSEmployeeDPLK,
                    SSEmployeeJiwasraya = x.SSEmployeeJiwasraya,
                    FixedDeduction = x.FixedDeduction,
                    Brutto = x.Brutto,
                    Tax = x.Tax,
                    TakeHomePay = x.TakeHomePay,
                    VoucherRequestDocNoPayroll = x.VoucherRequestDocNoPayroll,
                });
            }

        }

        #endregion

        #endregion

        #region Report Class

        class PayrollProcess
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string MenuDesc { get; set; }
            public string UserName { get; set; }
            public string PrintBy { get; set; }
            public string Period { get; set; }
        }

        class PayrollProcessDtl
        {
            public int nomor { get; set; }
            public string PayrunCode { get; set; }
            public string PayrunName { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public decimal TotalTHPBPJS { get; set; }
            public decimal TotBPJSKet { get; set; }
        }

        private class ExportedColumnHdr
        {
            public string ColName { get; set; }
        }

        private class ExportedColumnDtl
        {
            public string PayrunCode { get; set; }
            public string PayrunName { get; set; }
            public string EmpName { get; set; }
            public string OldCode { get; set; }
            public string Position { get; set; }
            public string DivisionName { get; set; }
            public string PTKP { get; set; }
            public decimal Salary { get; set; }
            public decimal FixedAllowance { get; set; }
            public decimal OT1Hour { get; set; }
            public decimal OT1Amount { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal SSEmployeeDPLK { get; set; }
            public decimal SSEmployeeJiwasraya { get; set; }
            public decimal FixedDeduction { get; set; }
            public decimal Brutto { get; set; }
            public decimal Tax { get; set; }
            public decimal TakeHomePay { get; set; }
            public string VoucherRequestDocNoPayroll { get; set; }

            //public string EmploymentStatus { get; set; }
            //public string SiteName { get; set; }
            //public string DeptName { get; set; }
            //public decimal PerformanceAllowance { get; set; }
            //public decimal GeneralAllowanceAmt { get; set; }
            //public decimal PerformanceAllowanceAmt { get; set; }
            //public decimal YearOfServiceAllowanceAmt { get; set; }
            //public decimal VariableAllowanceAmt { get; set; }
            //public decimal TotalTHP { get; set; }
            //public decimal OTAllowance { get; set; }            
            //public decimal TotalDeduction { get; set; }
            //public decimal SalaryAdjustment { get; set; }
            //public decimal SalaryAfterDeduction { get; set; }            
        }

        private class SumExportedColumnDtl
        {
            public decimal Salary { get; set; }
            public decimal PerformanceAllowance { get; set; }
            public decimal GeneralAllowanceAmt { get; set; }
            public decimal PerformanceAllowanceAmt { get; set; }
            public decimal YearOfServiceAllowanceAmt { get; set; }
            public decimal VariableAllowanceAmt { get; set; }
            public decimal TotalTHP { get; set; }
            public decimal OTAllowance { get; set; }
            public decimal Brutto { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal TotalDeduction { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal SalaryAfterDeduction { get; set; }
            public decimal THP { get; set; }
        }

        private class Sign
        {
            public string PayrollProcessPrintOutSignName1 { get; set; }
            public string PayrollProcessPrintOutSignName2 { get; set; }
            public string PayrollProcessPrintOutSignName3 { get; set; }
            public string PayrollProcessPrintOutSignName4 { get; set; }
            public string PayrollProcessPrintOutPosName1 { get; set; }
            public string PayrollProcessPrintOutPosName2 { get; set; }
            public string PayrollProcessPrintOutPosName3 { get; set; }
            public string PayrollProcessPrintOutPosName4 { get; set; }
            public string DocDt { get; set; }
        }

        private class RptPayrollProcessSummary12
        {
            public int No { get; set; }
            public string PayrunCode { get; set; }
            public string PayrunName { get; set; }
            public string EmployeeCode { get; set; }
            public string EmployeeName { get; set; }
            public string OldCode { get; set; }
            public string Position { get; set; }
            public string Department { get; set; }
            public string Division { get; set; }
            public string JoinDate { get; set; }
            public string ResignDate { get; set; }
            public string Type { get; set; }
            public string Period { get; set; }
            public string Group { get; set; }
            public string Site { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
            public decimal Salary { get; set; }
            public decimal WorkingDay { get; set; }
            public decimal PaidLeaveDay { get; set; }
            public decimal PaidLeaveHour { get; set; }
            public decimal PaidLeaveAmount { get; set; }
            public decimal ProcessedPaidLeaveAmount { get; set; }
            public decimal UnpaidLeaveDay { get; set; }
            public decimal UnpaidLeaveHour { get; set; }
            public decimal UnpaidLeaveAmount { get; set; }
            public decimal ProcessedUnpaidLeaveAmount { get; set; }
            public decimal OT1Hour { get; set; }
            public decimal OT2Hour { get; set; }
            public decimal OTHolidayHour { get; set; }
            public decimal OT1Amount { get; set; }
            public decimal OT2Amount { get; set; }
            public decimal OTHolidayAmount { get; set; }
            public decimal TaxableFixedAllowance { get; set; }
            public decimal NonTaxableFixedAllowance { get; set; }
            public decimal ADName1 { get; set; }
            public decimal ADName2 { get; set; }
            public decimal FixedAllowance { get; set; }
            public decimal PerformanceAllowance { get; set; }
            public decimal GeneralAllowance { get; set; }
            public decimal WorkAllowance { get; set; }
            public decimal YearofServiceAllowance { get; set; }
            public decimal VariableAllowance { get; set; }
            public decimal TotalTHP { get; set; }
            public decimal OTAllowance { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal TaxAllowance { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal SSEmployerDPLK { get; set; }
            public decimal SSEmployeeDPLK { get; set; }
            public decimal SSEmployeeJiwasraya { get; set; }
            public decimal SSEmployeerJiwasraya { get; set; }
            public decimal NonTaxableEmployeeDeduction { get; set; }
            public decimal TaxableEmployeeDeduction { get; set; }
            public decimal FixedDeduction { get; set; }
            public decimal EmployeeAdvancePayment { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal SalaryBeforeAdjustment { get; set; }
            public decimal Brutto { get; set; }
            public decimal Tax { get; set; }
            public decimal EndofYearTax { get; set; }
            public decimal TakeHomePay { get; set; }
            public string VoucherRequestDocNoPayroll { get; set; }
            public decimal Transferred { get; set; }
        }

        #endregion
    }
}
