﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOProductionDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOProduction mFrmParent;
        string mSQL = string.Empty, mProdoDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmDOProductionDlg3(FrmDOProduction FrmParent, string ProdoDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mProdoDocNo = ProdoDocNo;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X.DocNo, X.PDDNo, X.BomDNo, X.DocDt, X.DocName, X.DocCode, X.ItName, X.Qty, SUM(X.QtyDOP) As QtyDelivered From ( ");
            SQL.AppendLine("Select A.BomDocNo As DocNo, A.DNo As PDDNo, C.Dno As BomDNo, B.DocDt, B.DocName, C.DocCode, D.ItName, C.Qty, ifnull(E.QtyDOP, 0) As QtyDOP ");
            SQL.AppendLine("From TblProductionOrderDtl A ");
            SQL.AppendLine("Inner Join TblBomHdr B On A.BOMDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBomDtl C On B.DocNo = C.DocNo And DocType = '1' ");
            SQL.AppendLine("Inner join TblItem D On C.DocCode = D.ItCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Y.BomDocNo, Y.BomDno, Y.ProductionOrderDno, X.DocCode, Y.QtyDOP ");
            SQL.AppendLine("    From tblbomDtl X ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select A.ProductionOrderDocNo, B.ProductionOrderDno, ");
	        SQL.AppendLine("        C.BomDocNo, B.BomDno, SUM(B.Qty) As QtyDOP ");
	        SQL.AppendLine("        From TblDoProductionHdr A ");
	        SQL.AppendLine("        Inner Join TblDOProductionDtl2 B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("        left join ");
	        SQL.AppendLine("        ( ");
		    SQL.AppendLine("            Select A.DocNo, A.Dno, A.BomDocNo ");
		    SQL.AppendLine("            From TblProductionOrderDtl A ");
	        SQL.AppendLine("        )C On A.ProductionOrderDocNo = C.DocNo And  B.ProductionOrderDno = C.Dno ");
	        SQL.AppendLine("        Group By  A.ProductionOrderDocNo, B.ProductionOrderDno, C.BomDocNo, B.BomDno ");
            SQL.AppendLine("    )Y On X.DocNo = Y.BomDocNo And X.Dno = Y.BomDno  ");
            SQL.AppendLine("    Where X.DocType = '1' ");
            SQL.AppendLine(")E On E.BomDocNo = A.BomDocNo And E.BomDno = C.Dno And E.ProductionOrderDno = A.DNo And E.DocCode = C.DocCode ");
            SQL.AppendLine("Where B.ActiveInd = 'Y' And A.DocNo = @ProdoDocNo ");
            SQL.AppendLine("And Locate(Concat('##', A.BOMDocNo, C.DNo, '##'), @SelectedBom)<1 ");
            SQL.AppendLine(")X ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                          //0
                        "No.",

                        //1-5
                        "",
                        "BOM Number",
                        "Bom Dno",
                        "Prod DNo",
                        "",

                        //6-10
                        "Document"+Environment.NewLine+"Date",
                        "Document"+Environment.NewLine+"Name",
                        "Item Code",
                        "",
                        "Item Name",

                        //11
                        "Quantity",
                        "Delivered"+Environment.NewLine+"Quantity"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 5, 9 });
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2,3, 4, 6, 7, 8, 10,11,12 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 8 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@ProdoDocNo", mProdoDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "X.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.DocCode", "X.ItName" });
                Sm.CmParam<String>(ref cm, "@SelectedBom", mFrmParent.GetSelectedBOMNumber());

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group by X.DocNo, X.PDDNo, X.BomDNo, X.DocDt, X.DocName, X.DocCode, X.ItName Order by X.DocNo, X.PDDno ;",
                        new string[]
                        {
                            //0
                           "DocNo", 
                                
                            //1-5
                           "BomDno", "PDDno", "DocDt", "DocName",  "DocCode", 
                           
                           //6
                           "ItName", "Qty", "QtyDelivered"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 8, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 9, Grd1, Row2, 12);

                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 11, 12 });
                    }
                }
                mFrmParent.InsertItem();
                mFrmParent.ComputeBalanceBOM();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(mFrmParent.Grd2, Index, 1) + Sm.GetGrdStr(mFrmParent.Grd2, Index, 2))) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBom2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                    e.DoDefault = false;
                    var f = new FrmItem(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                    f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmBom2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }

        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
