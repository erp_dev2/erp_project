﻿#region Update
/*
    13/03/2020 [IBL/KBN] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmDailyCurrencyRateFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDailyCurrencyRate mFrmParent;
        private string mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmDailyCurrencyRateFind(FrmDailyCurrencyRate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetPeriod(ref DteRateDt1, ref DteRateDt2, ref ChkRateDt, -7);

        }

        override protected  void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select RateDt,CurCode1,CurCode2,Amt,CreateBy,CreateDt,LastUpBy,LastUpDt ");
            SQL.AppendLine("From TblDailyCurrencyRate ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date", 
                        "From",
                        "To",//hide
                        "Amt",//hide
                        "Created"+Environment.NewLine+"By",//hide

                        //6-10
                        "Created"+Environment.NewLine+"Date", //semua hide                       
                        "Created"+Environment.NewLine+"Time", //semua hide                       
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            //digunakan untuk render tabel 
            
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1, 6, 9 });
            Sm.GrdFormatTime(Grd1, new int[] { 7, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteRateDt1), Sm.GetDte(DteRateDt2), "RateDt");
                
                Sm.ShowDataInGrid(
                        ref Grd1, 
                        ref cm,
                        mSQL + Filter + " Order By RateDt",
                        new string[]
                        {
                            //0
                            "RateDt", 
                            
                            //1-5
                            "CurCode1", "CurCode2", "Amt", "CreateBy", "CreateDt",  
                            
                            //6-7
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 7);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(
                    Sm.Left(Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 1), 8), 
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2), 
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3));
                this.Hide();
            }
        }

        
        #endregion

       
        #region Event

        #region Misc Control Event


        private void DteRateDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteRateDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkRateDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

    
        #endregion

       

        #endregion

    }
}
