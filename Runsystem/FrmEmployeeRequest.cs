﻿#region Update
/*
    07/06/2017 [WED] tambahan validasi di Requisition source
    27/03/2018 [TKG] tambahan experience level
    29/03/2018 [WED] tambah Level dari master level
    31/03/2018 [TKG] tambah cancel reason, status
    06/03/2020 [IBL/SIER] Pilihan Group Mutation pada Employee Request akan di hide --> [ada di dokumen BRD ttraining 2, wed : lihat ke catatan
    27/07/2020 [ICA/SIER] Source of Requisition hanya bisa pilih satu dengan parameter IsEmployeeRequestRequisitionSourceOnlyOne
    27/07/2020 [ICA/SIER] Requisition Reason hanya bisa pilih satu dengan parameter IsEmployeeRequestRequisitionReasonOnlyOne
    16/02/2022 [ISD/GSS] bug departement tidak muncul saat show dan nambah field PIC dengan parameter IsEmployeeRequestUsePIC
    25/04/2022 [BRI/HIN] employee request menggunakan position berdasrkan param IsEmployeeRequestUsePositionForNewCandidate
    26/04/2022 [BRI/HIN] printout baru berdasrkan param IsEmployeeRequestUsePositionForNewCandidate
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeRequest : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = String.Empty;
        internal bool 
            mIsFilterBySiteHR, 
            mIsApprovalBySiteMandatory, 
            mIsFilterByDeptHR = false,
            mIsShowGroupMutation = false;
        private bool
            mIsDocApprovalByDept = false,
            mIsEmployeeRequestRequisitionSourceOnlyOne = false,
            mIsEmployeeRequestRequisitionReasonOnlyOne = false,
            mIsDocApprovalBySite = false,
            mIsEmployeeRequestUsePIC = false,
            mIsEmployeeRequestUsePositionForNewCandidate = false;
        internal FrmEmployeeRequestFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmployeeRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmEmployeeRequest");
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                GetValue();
                SetFormControl(mState.View);

                TcEmployeeRequest.SelectedTabPage = Tp2;
                Sl.SetLuePosCode(ref LuePosCode);
                Sl.SetLueGender(ref LueGender);
                Sl.SetLueEmployeeEducationLevel(ref LueLevel);
                Sl.SetLueOption(ref LueCompetenceLevel, "CompetenceLevelName");
                Sl.SetLueLevelCode(ref LueLevelCode);
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                SetLuePIC(ref LuePICName, string.Empty);

                if (!mIsEmployeeRequestUsePositionForNewCandidate)
                {
                    label23.Visible = false;
                    TxtExpectedSalaryAmt.Visible = false;
                    label24.Visible = false;
                    LueEmploymentStatus.Visible = false;
                    label7.Visible = false;
                    label9.Visible = false;
                    TxtHead.Visible = false;
                    label10.Visible = false;
                    TxtCandidate.Visible = false;
                    label22.Visible = false;
                    TxtSubOrdinat.Visible = false;
                }

                SetGrd();

                TcEmployeeRequest.SelectedTabPage = Tp1;
                if (mIsFilterBySiteHR) label61.ForeColor = Color.Red;
                if (!mIsEmployeeRequestUsePIC) 
                {
                    int ypoint = 21;
                    LblPICName.Visible = LuePICName.Visible = false;
                    label28.Top = label28.Top - ypoint; TxtProcess.Top = TxtProcess.Top - ypoint; 
                    label8.Top = label8.Top - ypoint; MeeRemark.Top = MeeRemark.Top - ypoint;
                }
                ChkGroupMutation.Visible = mIsShowGroupMutation;

                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 0, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, MeeJobDesk, LueCompetenceLevel, DteDocDt, ChkCancelInd,
                        LueSiteCode, LueDeptCode, MeeRemark, ChkResigned, ChkTerminate,
                        ChkMutated, ChkPromoted, ChkRetired, ChkNewPosition, ChkOther2,
                        MeeOther2, LuePosCode, TxtAmount, DteUseDt, LueGender, 
                        TxtAge, LueLevel, MeeWorkExperience, MeeOther, ChkDeptMutation, 
                        ChkGroupMutation, ChkNewEmp, TxtProcess, LueLevelCode, MeeCancelReason, LuePICName,
                        TxtExpectedSalaryAmt, LueEmploymentStatus, TxtHead, TxtCandidate, TxtSubOrdinat
                    }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, MeeRemark, MeeOther2, LuePosCode,
                        TxtAmount, DteUseDt, LueGender, TxtAge, LueLevel, 
                        MeeWorkExperience, LueCompetenceLevel, MeeOther, MeeJobDesk, ChkDeptMutation, 
                        ChkGroupMutation, ChkNewEmp, ChkResigned, ChkTerminate, ChkMutated,
                        ChkPromoted, ChkRetired, ChkNewPosition, ChkOther2, LueLevelCode,
                        LueSiteCode, LuePICName, TxtExpectedSalaryAmt, LueEmploymentStatus, TxtHead, TxtCandidate, TxtSubOrdinat
                    }, false);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, ChkCancelInd, 
                LueSiteCode, LueDeptCode, MeeRemark, MeeOther2, LuePosCode, 
                TxtAmount, DteUseDt, LueGender, TxtAge, LueLevel, 
                MeeWorkExperience, LueCompetenceLevel, MeeOther, MeeJobDesk, TxtProcess, 
                LueLevelCode, LuePICName, TxtExpectedSalaryAmt, LueEmploymentStatus, TxtHead, TxtCandidate, TxtSubOrdinat
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmount, TxtAge }, 1);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtExpectedSalaryAmt }, 0);
            ChkCancelInd.Checked = false;
            ChkResigned.Checked = ChkTerminate.Checked = ChkMutated.Checked = ChkPromoted.Checked = ChkRetired.Checked = ChkNewPosition.Checked = ChkOther2.Checked =false;
            ChkDeptMutation.Checked = ChkGroupMutation.Checked = ChkNewEmp.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void GetParameter()
        {
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            mIsShowGroupMutation = Sm.GetParameterBoo("IsShowGroupMutation");
            mIsEmployeeRequestRequisitionSourceOnlyOne = Sm.GetParameterBoo("IsEmployeeRequestRequisitionSourceOnlyOne");
            mIsEmployeeRequestRequisitionReasonOnlyOne = Sm.GetParameterBoo("IsEmployeeRequestRequisitionReasonOnlyOne");
            mIsEmployeeRequestUsePIC = Sm.GetParameterBoo("IsEmployeeRequestUsePIC");
            mIsEmployeeRequestUsePositionForNewCandidate = Sm.GetParameterBoo("IsEmployeeRequestUsePositionForNewCandidate");
        }

        private void GetValue()
        {
            mIsDocApprovalByDept = Sm.IsDocApprovalByDept("EmployeeRequest");
            mIsDocApprovalBySite = Sm.IsDocApprovalBySite("EmployeeRequest");
        }

        #endregion    

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmployeeRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                
                TcEmployeeRequest.SelectedTabPage = Tp1;
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                TxtProcess.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty, mMenuCode) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (!mIsEmployeeRequestUsePositionForNewCandidate) return;
            if (Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;

            string[] TableName = { "EmployeeRequest", "Approval" };
            List<IList> myLists = new List<IList>();

            var l = new List<EmployeeRequest>();
            var l2 = new List<Approval>();

            #region Employee Request

            l.Add(new EmployeeRequest()
            {
                CompanyLogo = Sm.CompanyLogo(),
                DocNo = TxtDocNo.Text,
                DocDt = DteDocDt.Text,
                Dept = LueDeptCode.Text,
                Pos = LuePosCode.Text,
                Site = LueSiteCode.Text,
                UsageDt = DteUseDt.Text,
                ExpectedSalary = Decimal.Parse(TxtExpectedSalaryAmt.Text),
                EmploymentStatus = LueEmploymentStatus.Text,
                JobDesc = MeeJobDesk.Text,
                Head = TxtHead.Text,
                Candidate = TxtCandidate.Text,
                SubOrdinat = TxtSubOrdinat.Text,
                Education = LueLevel.Text,
            });

            myLists.Add(l);
            #endregion

            #region Approval HIN

            var RequisitionReasonTemp = string.Empty;

            var cm2 = new MySqlCommand();

            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select ");
            SQL2.AppendLine("T1.UserName UserName1, Date_Format(T1.LastUpDt, '%d/%b/%Y') LastUpDt1, T1.PosName PosName1, ");
            SQL2.AppendLine("T2.UserName UserName2, Date_Format(T2.LastUpDt, '%d/%b/%Y') LastUpDt2, T2.PosName PosName2, ");
            SQL2.AppendLine("T3.UserName Username3, Date_Format(T2.LastUpDt, '%d/%b/%Y') LastUpDt3, T3.PosName Posname3 ");
            SQL2.AppendLine("From ");
            SQL2.AppendLine("( ");
            SQL2.AppendLine("   Select A.DocNo, B.EmpName UserName, A.DocDt LastUpDt, C.PosName ");
            SQL2.AppendLine("   From TblEmployeeRequest A ");
            SQL2.AppendLine("   Left Join TblEmployee B ON A.CreateBy = B.EmpCode ");
            SQL2.AppendLine("   Left Join TblPosition C On B.PosCode = C.PosCode ");
            SQL2.AppendLine("   Where A.DocNo = @DocNo ");
            SQL2.AppendLine(")T1 ");
            SQL2.AppendLine("Left Join ");
            SQL2.AppendLine("( ");
            SQL2.AppendLine("    Select A.DocNo, C.EmpName UserName, Left(A.LastUpDt, 8) LastUpDt, D.PosName ");
            SQL2.AppendLine("    From TblDocApproval A ");
            SQL2.AppendLine("    Inner Join TblDocApprovalSetting B On A.DocType = B.DocType ");
            SQL2.AppendLine("        And A.ApprovalDNo = B.DNo ");
            SQL2.AppendLine("        And B.`Level`= '1' ");
            SQL2.AppendLine("    Left Join TblEmployee C On A.UserCode = C.EmpCode ");
            SQL2.AppendLine("    Left Join TblPosition D On C.PosCode = D.PosCode ");
            SQL2.AppendLine("    Where A.DocType = 'EmployeeRequest' ");
            SQL2.AppendLine("    And A.Status In('A', 'C') ");
            SQL2.AppendLine("    And A.DocNo = @DocNo ");
            SQL2.AppendLine("    Order By A.ApprovalDNo ");
            SQL2.AppendLine(")T2 On T1.DocNo = T2.DocNo ");
            SQL2.AppendLine("Left Join ");
            SQL2.AppendLine("( ");
            SQL2.AppendLine("    Select A.DocNo, C.EmpName UserName, Left(A.LastUpDt, 8) LastUpDt, D.PosName ");
            SQL2.AppendLine("    From TblDocApproval A ");
            SQL2.AppendLine("    Inner Join TblDocApprovalSetting B On A.DocType = B.DocType ");
            SQL2.AppendLine("        And A.ApprovalDNo = B.DNo ");
            SQL2.AppendLine("        And B.`Level`= '2' ");
            SQL2.AppendLine("    Left Join TblEmployee C On A.UserCode = C.EmpCode ");
            SQL2.AppendLine("    Left Join TblPosition D On C.PosCode = D.PosCode ");
            SQL2.AppendLine("    Where A.DocType = 'EmployeeRequest' ");
            SQL2.AppendLine("    And A.Status In('A', 'C') ");
            SQL2.AppendLine("    And A.DocNo = @DocNo ");
            SQL2.AppendLine("    Order By A.ApprovalDNo ");
            SQL2.AppendLine(")T3 On T1.DocNo = T3.DocNo ");
            SQL2.AppendLine("; ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[]
                    {
                         //0-2
                         "UserName1",
                         "LastUpDt1",
                         "PosName1",

                         //3-5
                         "UserName2",
                         "LastUpDt2",
                         "PosName2",

                         //6-8
                         "UserName3",
                         "LastUpDt3",
                         "PosName3",



                    });

                if (ChkResigned.Checked) RequisitionReasonTemp += LblResigned.Text;
                if (ChkTerminate.Checked) RequisitionReasonTemp += Environment.NewLine + LblTerminate.Text;
                if (ChkMutated.Checked) RequisitionReasonTemp += Environment.NewLine + LblMutated.Text;
                if (ChkPromoted.Checked) RequisitionReasonTemp += Environment.NewLine + LblPromoted.Text;
                if (ChkRetired.Checked) RequisitionReasonTemp += Environment.NewLine + LblRetired.Text;
                if (ChkNewPosition.Checked) RequisitionReasonTemp += Environment.NewLine + LblNewPosition.Text;
                if (ChkOther2.Checked) RequisitionReasonTemp += Environment.NewLine + MeeOther2.Text;

                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new Approval()
                        {
                            UserName1 = Sm.DrStr(dr2, c2[0]),
                            LastUpDt1 = Sm.DrStr(dr2, c2[1]),
                            PosName1 = Sm.DrStr(dr2, c2[2]),

                            UserName2 = Sm.DrStr(dr2, c2[3]),
                            LastUpDt2 = Sm.DrStr(dr2, c2[4]),
                            PosName2 = Sm.DrStr(dr2, c2[5]),

                            Username3 = Sm.DrStr(dr2, c2[6]),
                            LastUpDt3 = Sm.DrStr(dr2, c2[7]),
                            PosName3 = Sm.DrStr(dr2, c2[8]),

                            CurYr = Sm.Left(Sm.ServerCurrentDateTime(), 4),
                            Other = MeeOther.Text,
                            RequisitionReason = RequisitionReasonTemp
                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);
            #endregion

            Sm.PrintReport("EmployeeRequest", myLists, TableName, false);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmployeeRequest", "TblEmployeeRequest");

            var cml = new List<MySqlCommand>();
          
            cml.Add(SaveEmployeeRequest(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                FilterSite() ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsEmployeeRequestUsePIC && Sm.IsLueEmpty(LuePICName, "PIC")) ||
                Sm.IsLueEmpty(LuePosCode, "Position") ||
                Sm.IsLueEmpty(LueLevelCode, "Level") ||
                Sm.IsTxtEmpty(TxtAmount, "Requisition Candidate", true) ||
                Sm.IsDteEmpty(DteUseDt, "Use Date") ||
                Sm.IsLueEmpty(LueGender, "Gender") ||
                (mIsEmployeeRequestUsePositionForNewCandidate && Sm.IsLueEmpty(LueEmploymentStatus, "Employment Status")) ||
                IsCheckBox() ||
                IsRequisitionSourceUnChecked() ||
                IsRequisitionSourceInvalid() ||
                (mIsEmployeeRequestRequisitionReasonOnlyOne && IsRequisitionReasonNotOnlyOne());
        }

        private bool IsRequisitionSourceUnChecked()
        {
            if (!ChkGroupMutation.Checked && !ChkDeptMutation.Checked && !ChkNewEmp.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Please select at least one Source of Requisition.");
                ChkDeptMutation.Focus();
                return true;
            }
            
            return false;
        }

        private bool IsRequisitionSourceInvalid()
        {
           if (mIsEmployeeRequestRequisitionReasonOnlyOne)
           {
               return IsRequisitionSourceNotOnlyOne();
           }
           else
           {
                if (ChkDeptMutation.Checked && ChkNewEmp.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "Choose only one, between Department Mutation or New Employee");
                    return true;
                }
                else if (ChkNewEmp.Checked && ChkGroupMutation.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, "Choose only one, between Group Mutation or New Employee");
                    return true;
                }           
           }

            return false;
        }
        
        private bool IsCheckBox()
        {
            var hasChecked = false;
           
            {
                if (ChkPromoted.Checked)
                {
                    hasChecked = true;
                }
                if (ChkResigned.Checked)
                {
                    hasChecked = true;
                }
                if (ChkTerminate.Checked)
                {
                    hasChecked = true;
                }
                if (ChkMutated.Checked)
                {
                    hasChecked = true;
                }
                if (ChkRetired.Checked)
                {
                    hasChecked = true;
                }
                if (ChkNewPosition.Checked)
                {
                    hasChecked = true;
                }
                if (ChkOther2.Checked)
                {
                    hasChecked = true;
                }
            }
            if (hasChecked == false)
            
                {
                    Sm.StdMsg(mMsgType.Warning, "Please select at least one requisition reason.");
                    return true;
                }
            
                return false;
        }

        private bool IsRequisitionSourceNotOnlyOne()
        {
            int sourceCount = 0;
            bool[] lc = { ChkDeptMutation.Checked, ChkNewEmp.Checked, ChkGroupMutation.Checked };

            for (int i = 0; i < lc.Length; i++)
            {
                if (lc[i] == true)
                {
                    sourceCount += 1;
                }
            }

            if (sourceCount > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Choose only one Requisition Source");
                return true;
            }

            return false;
        }

        private bool IsRequisitionReasonNotOnlyOne()
        {
            int reasonCount = 0;

            bool[] lc2 = {ChkResigned.Checked, ChkTerminate.Checked, ChkMutated.Checked, ChkPromoted.Checked, 
                            ChkRetired.Checked, ChkNewPosition.Checked, ChkOther2.Checked};
            
            for (int i = 0; i < lc2.Length; i++ )
            {
                if (lc2[i] == true)
                {
                    reasonCount += 1;
                }
            }

            if (reasonCount > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Choose only one Requisition Reason");
                return true;
            }
                
            return false;
        }

        private bool FilterSite()
        {
            return mIsFilterBySiteHR && Sm.IsLueEmpty(LueSiteCode, "Site");
        }

        private MySqlCommand SaveEmployeeRequest(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeRequest ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, ProcessInd, SiteCode, DeptCode, Remark, PosCode, LevelCode, Amount, UseDt, Gender, Age, Level, WorkExperience, CompetenceLevel, ");
            SQL.AppendLine("Other, Resigned, Terminate, Mutated, Promoted, Retired, NewPosition, Other2, JobDesk, DeptMutation, GroupMutation, NewEmp, ");
            if (mIsEmployeeRequestUsePIC) SQL.AppendLine("PICCode, ");
            if (mIsEmployeeRequestUsePositionForNewCandidate) SQL.AppendLine("ExpectedSalary, EmploymentStatus, Head, Candidate, SubOrdinat, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, Null, 'N', 'O', 'O', @SiteCode, @DeptCode, @Remark, @PosCode, @LevelCode, @Amount, @UseDt, @Gender, @Age, @Level, @WorkExperience, @CompetenceLevel, ");
            SQL.AppendLine("@Other, @Resigned, @Terminate, @Mutated, @Promoted, @Retired, @NewPosition, @Other2, @JobDesk, @DeptMutation, @GroupMutation, @NewEmp, ");
            if (mIsEmployeeRequestUsePIC) SQL.AppendLine("@PICCode, ");
            if (mIsEmployeeRequestUsePositionForNewCandidate) SQL.AppendLine("@ExpectedSalary, @EmploymentStatus, @Head, @Candidate, @SubOrdinat, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='EmployeeRequest' ");
            if (mIsDocApprovalByDept) SQL.AppendLine("And DeptCode Is Not Null And DeptCode=@DeptCode ");
            if (mIsDocApprovalBySite) SQL.AppendLine("And SiteCode Is Not Null And SiteCode=@SiteCode ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblEmployeeRequest Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmployeeRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PICCode", Sm.GetLue(LuePICName));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetLue(LueLevelCode));
            Sm.CmParam<Decimal>(ref cm, "@Amount", Decimal.Parse(TxtAmount.Text));
            Sm.CmParamDt(ref cm, "@UseDt", Sm.GetDte(DteUseDt));
            Sm.CmParam<String>(ref cm, "@Gender", Sm.GetLue(LueGender));
            Sm.CmParam<Decimal>(ref cm, "@Age", Decimal.Parse(TxtAge.Text));
            Sm.CmParam<String>(ref cm, "@Level", Sm.GetLue(LueLevel));
            Sm.CmParam<String>(ref cm, "@WorkExperience", MeeWorkExperience.Text);
            Sm.CmParam<String>(ref cm, "@CompetenceLevel", Sm.GetLue(LueCompetenceLevel));
            Sm.CmParam<String>(ref cm, "@Other", MeeOther.Text);
            Sm.CmParam<String>(ref cm, "@Resigned", ChkResigned.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Terminate", ChkTerminate.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Mutated", ChkMutated.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Promoted", ChkPromoted.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Retired", ChkRetired.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@NewPosition", ChkNewPosition.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Other2", MeeOther2.Text);
            Sm.CmParam<String>(ref cm, "@JobDesk", MeeJobDesk.Text);
            Sm.CmParam<String>(ref cm, "@DeptMutation", ChkDeptMutation.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@GroupMutation", ChkGroupMutation.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@NewEmp", ChkNewEmp.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@ExpectedSalary", Decimal.Parse(TxtExpectedSalaryAmt.Text));
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<String>(ref cm, "@Head", TxtHead.Text);
            Sm.CmParam<String>(ref cm, "@Candidate", TxtCandidate.Text);
            Sm.CmParam<String>(ref cm, "@SubOrdinat", TxtSubOrdinat.Text);

            return cm;
        }


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmployeeRequest());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this employee request.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblEmployeeRequest " +
                "Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private MySqlCommand EditEmployeeRequest()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployeeRequest Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmployeeRequest(DocNo);
                Sm.ShowDocApproval(DocNo, "EmployeeRequest", ref Grd1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmployeeRequest(string DocNo) 
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("A.SiteCode, A.DeptCode, A.Remark, A.PosCode, A.Amount, A.UseDt, A.Gender, A.Age, A.Level, ");
            SQL.AppendLine("A.WorkExperience, A.Other, A.Resigned, A.Terminate, A.Mutated, A.Promoted, A.Retired, A.NewPosition, A.Other2, A.JobDesk, ");
            SQL.AppendLine("A.DeptMutation, A.GroupMutation, A.NewEmp, ");
            if (mIsEmployeeRequestUsePIC) SQL.AppendLine("A.PICCode As PIC, ");
            else SQL.AppendLine("Null As PIC, ");
            if (mIsEmployeeRequestUsePositionForNewCandidate) SQL.AppendLine("A.ExpectedSalary, A.EmploymentStatus, A.Head, A.Candidate, A.SubOrdinat, ");
            else SQL.AppendLine("Null As ExpectedSalary, Null As EmploymentStatus, Null As Head, Null As Candidate, Null As SubOrdinat, ");
            SQL.AppendLine("Case When A.ProcessInd = 'O' Then 'Outstanding' When A.ProcessInd = 'F' Then 'Fulfilled' Else 'Partial' End As Processind, ");
            SQL.AppendLine("A.CompetenceLevel, A.LevelCode, A.CancelReason, A.CancelInd ");
            SQL.AppendLine("From TblEmployeeRequest A ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "StatusDesc", "SiteCode", "DeptCode", "Remark",  

                        //6-10
                        "PosCode", "Amount", "UseDt", "Gender", "Age",  

                        //11-15
                        "Level", "WorkExperience", "Other", "Resigned", "Terminate", 
                         
                        //16-20
                        "Mutated",  "Promoted", "Retired", "NewPosition", "Other2", 

                        //21-25
                        "JobDesk", "DeptMutation", "GroupMutation", "NewEmp", "ProcessInd",

                        //26-30
                        "CompetenceLevel", "LevelCode", "CancelReason", "CancelInd", "PIC",

                        //31-35
                        "ExpectedSalary", "EmploymentStatus", "Head", "Candidate", "SubOrdinat"

                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[3]), mIsFilterBySiteHR?"Y":"N");
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[4]), mIsFilterByDeptHR?"Y":"N");
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[6]));
                    TxtAmount.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]),1);
                    Sm.SetDte(DteUseDt, Sm.DrStr(dr, c[8]));
                    Sm.SetLue(LueGender, Sm.DrStr(dr, c[9]));
                    TxtAge.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]),1);
                    Sm.SetLue(LueLevel, Sm.DrStr(dr, c[11]));
                    MeeWorkExperience.EditValue = Sm.DrStr(dr, c[12]);
                    MeeOther.EditValue = Sm.DrStr(dr, c[13]);
                    ChkResigned.Checked = Sm.CompareStr(Sm.DrStr(dr, c[14]), "Y");
                    ChkTerminate.Checked = Sm.CompareStr(Sm.DrStr(dr, c[15]), "Y");
                    ChkMutated.Checked = Sm.CompareStr(Sm.DrStr(dr, c[16]), "Y");
                    ChkPromoted.Checked = Sm.CompareStr(Sm.DrStr(dr, c[17]), "Y");
                    ChkRetired.Checked = Sm.CompareStr(Sm.DrStr(dr, c[18]), "Y");
                    ChkNewPosition.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y");
                    MeeOther2.EditValue = Sm.DrStr(dr, c[20]);
                    MeeJobDesk.EditValue = Sm.DrStr(dr, c[21]);
                    ChkDeptMutation.Checked = Sm.CompareStr(Sm.DrStr(dr, c[22]), "Y");
                    ChkGroupMutation.Checked = Sm.CompareStr(Sm.DrStr(dr, c[23]), "Y");
                    ChkNewEmp.Checked = Sm.CompareStr(Sm.DrStr(dr, c[24]), "Y");
                    if (MeeOther2.Text.Length !=0) ChkOther2.Checked = true;
                    TxtProcess.EditValue = Sm.DrStr(dr, c[25]);
                    Sm.SetLue(LueCompetenceLevel, Sm.DrStr(dr, c[26]));
                    Sm.SetLue(LueLevelCode, Sm.DrStr(dr, c[27]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[28]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[29]), "Y");
                    SetLuePIC(ref LuePICName, Sm.DrStr(dr, c[30]));
                    TxtExpectedSalaryAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 0);
                    Sm.SetLue(LueEmploymentStatus, Sm.DrStr(dr, c[32]));
                    TxtHead.EditValue = Sm.DrStr(dr, c[33]);
                    TxtCandidate.EditValue = Sm.DrStr(dr, c[34]);
                    TxtSubOrdinat.EditValue = Sm.DrStr(dr, c[35]);
                }, true
            );
        }

        #endregion

        #region Additional Methods

        private static void ChkOther2CheckedChanged(SimpleButton Btn, MemoExEdit Mee, CheckEdit Chk)
        {
            if (Btn.Enabled)
            {
                if (Chk.Checked)
                {
                    if (Mee.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Other reason for requisition is empty.");
                        Chk.Checked = false;
                    }
                }
                else
                    Mee.EditValue = null;
            }
        }

        private static void MeeOther2Validated(MemoExEdit Mee, CheckEdit Chk)
        {
            Sm.MeeTrim(Mee);
            Chk.Checked = (Mee.Text.Length > 0);
        }

        public static void SetLuePIC(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode As Col1, EmpName As Col2 From TblEmployee T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where EmpCode=@Code;");
            else
                SQL.AppendLine("Order By EmpName;");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR?"Y":"N");
        }

        private void LuePICName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePICName, new Sm.RefreshLue2(SetLuePIC), string.Empty);
        }

        private void LueGender_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGender, new Sm.RefreshLue1(Sl.SetLueGender));
        }

        private void LueEducation_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(Sl.SetLueEmployeeEducationLevel));
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void ChkOther2_CheckedChanged(object sender, EventArgs e)
        {
            ChkOther2CheckedChanged(BtnSave, MeeOther2, ChkOther2);
        }

        private void MeeOther2_Validated(object sender, EventArgs e)
        {
            MeeOther2Validated(MeeOther2, ChkOther2);
        }

        private void LueCompetenceLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceLevel, new Sm.RefreshLue2(Sl.SetLueOption), "CompetenceLevelName");
        }

        private void LueLevelCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelCode, new Sm.RefreshLue1(Sl.SetLueLevelCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
        }

        private void TxtExpectedSalaryAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtExpectedSalaryAmt, 0);
        }

        #endregion

        #endregion

        #region Class

        class EmployeeRequest
        {
            public string CompanyLogo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Dept { get; set; }
            public string Pos { get; set; }
            public string Site { get; set; }
            public string UsageDt { get; set; }
            public decimal ExpectedSalary { get; set; }
            public string EmploymentStatus { get; set; }
            public string JobDesc { get; set; }
            public string Head { get; set; }
            public string Candidate { get; set; }
            public string SubOrdinat { get; set; }
            public string Education { get; set; }
        }

        class Approval
        {
            public string UserName1 { get; set; }
            public string LastUpDt1 { get; set; }
            public string PosName1 { get; set; }
            public string UserName2 { get; set; }
            public string LastUpDt2 { get; set; }
            public string PosName2 { get; set; }
            public string Username3 { get; set; }
            public string LastUpDt3 { get; set; }
            public string PosName3 { get; set; }
            public string CurYr { get; set; }
            public string Other { get; set; }
            public string RequisitionReason { get; set; }
        }

        #endregion

    }
}
