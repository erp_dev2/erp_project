﻿#region Update
    // 26/04/2017 [WED] tambah approval header Make To Stock
    // 25/08/2020 [IBL/MGI] menambah field group kerja
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMakeToStock : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mItCtRMPActual = string.Empty,
            mItCtRMPActual2 = string.Empty
            ;
        internal FrmMakeToStockFind FrmFind;
        internal bool mIsMTSNeedBom = false, mIsUseProductionWorkGroup = false;
        internal decimal mQty = 0m;
        
        
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmMakeToStock(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Make To Stock";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtDocNo, TxtBomDocNo, TxtDocName, TxtItCode, TxtItName, 
                    TxtUomCode, TxtStatus
                }, true);
                SetGrd();
                Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                SetFormControl(mState.View);
                if (!mIsMTSNeedBom)
                {
                    panelB.Visible = false;
                    panel2.Height = 117;
                }

                if (!mIsUseProductionWorkGroup)
                {
                    panel4.Visible = false;
                }

                base.FrmLoad(sender, e);
                
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsMTSNeedBom = !Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForMakeToStockStandard"));
            mItCtRMPActual = Sm.GetParameter("ItCtRMPActual");
            mItCtRMPActual2 = Sm.GetParameter("ItCtRMPActual2");
            mIsUseProductionWorkGroup = Sm.GetParameterBoo("IsUseProductionWorkGroup");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                       //0
                        "",
                        
                        //1-5
                        "Item's Code",
                        "Local Code",
                        "Item's Name",
                        "Quantity",
                        "UoM",
                        
                        //6-10
                        "Quantity",
                        "UoM",
                        "Usage Date",
                        "Remark",
                        "Bom Qty",

                        //11-13
                        "Convert12",
                        "Specification",
                        "Customer's"+Environment.NewLine+"Item Code"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        100, 100, 250, 100, 80,
                        
                        //6-10
                        100, 80, 100, 300, 0,

                        //11-13
                        0, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 6, 10, 11 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 8 });
            Grd1.Cols[12].Move(4);
            Grd1.Cols[13].Move(5);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 10, 11, 12 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 5, 6, 7, 8, 10, 11, 12, 13 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, ChkCancelInd, TxtQty, DteUsageDt, MeeRemark, LueProductionWorkGroup }, true);
                    BtnBomDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 4, 8, 9 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, TxtQty, DteUsageDt, MeeRemark, LueProductionWorkGroup }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 4, 8, 9 });
                    if (mIsMTSNeedBom) Sm.GrdColReadOnly(true, true, Grd1, new int[] { 4 });
                    BtnBomDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mQty = 0m;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtBomDocNo, TxtDocName, TxtItCode, 
                TxtItName, TxtUomCode, DteUsageDt, MeeRemark, TxtStatus,
                LueProductionWorkGroup
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtQty }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 6, 10, 11 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMakeToStockFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                   InsertData();
                else
                   EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "MakeToStock", "TblMakeToStockHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveMakeToStockHdr(DocNo, IsDocApprovalSettingNotExisted()));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    cml.Add(SaveMakeToStockDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocType From TblDocApprovalSetting ");
            SQL.AppendLine("Where UserCode Is Not Null ");
            SQL.AppendLine("And DocType='MakeToStock' ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (!Sm.IsDataExist(cm))
                //Sm.StdMsg(mMsgType.Warning, "Nobody will approve this request.");
                return true;
            else
                return false;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsMTSNeedBom && Sm.IsTxtEmpty(TxtBomDocNo, "Bill of material#", false)) ||
                (mIsMTSNeedBom && Sm.IsTxtEmpty(TxtQty, "Quantity", false)) ||
                Sm.IsDteEmpty(DteUsageDt, "Usage Date") ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group")) ||
                IsGrdEmpty()||
                IsGrdValueNotValid() ||
                IsUsageDtNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 4, true, "Quantity is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "Usage date is empty.")
                    ) return true;
            }
            return false;
        }

        private bool IsUsageDtNotValid()
        {
            var UsageDt = decimal.Parse(Sm.GetDte(DteUsageDt).Substring(0, 8));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (decimal.Parse(Sm.GetGrdDate(Grd1, Row, 8).Substring(0, 8)) > UsageDt)
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine + 
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "Usage Date : " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, Row, 8)) + Environment.NewLine + Environment.NewLine +
                        "Usage date should not be later than " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetDte(DteUsageDt).Substring(0, 8) + ".")
                        );
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveMakeToStockHdr(string DocNo, bool NoNeedApproval)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMakeToStockHdr(DocNo, DocDt, CancelInd, Status, BomDocNo, Qty, UsageDt, ProductionWorkGroup, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, @Status, @BomDocNo, @Qty, @UsageDt, @ProductionWorkGroup, @Remark, @UserCode, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='MakeToStock' ");
                SQL.AppendLine("; ");
            }
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@BomDocNo", TxtBomDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Qty", decimal.Parse(TxtQty.Text));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetDte(DteUsageDt));
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveMakeToStockDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMakeToStockDtl(DocNo, DNo, ItCode, Qty, UsageDt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @Qty, @UsageDt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetGrdDate(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditMakeToStock(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsDataAlreadyCancelled() ||
                IsDataAlreadyProcessed()
                ;
        }

        private bool IsDataAlreadyCancelled()
        {
            return 
                Sm.IsDataExist(
                    "Select DocNo From TblMakeToStockHdr " +
                    "Where CancelInd='Y' And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already cancelled.");
        }

        private bool IsDataAlreadyProcessed()
        {
            return
                Sm.IsDataExist(
                    "Select DocNo From TblMakeToStockDtl " +
                    "Where (ProcessInd<>'O' Or ProcessInd2<>'O') And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already processed.");
        } 

        private MySqlCommand EditMakeToStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMakeToStockHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowMakeToStockHdr(DocNo);
                ShowMakeToStockDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowMakeToStockHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("  When 'O' Then 'Outstanding' ");
            SQL.AppendLine("  When 'C' Then 'Cancelled' ");
            SQL.AppendLine("  Else 'Approved' ");
            SQL.AppendLine("End As Status, ");
            SQL.AppendLine("A.BomDocNo, B.DocName, C.ItCode, D.ItName, A.Qty, D.PlanningUomCode, ");
            SQL.AppendLine("A.UsageDt, A.ProductionWorkGroup, A.Remark ");
            SQL.AppendLine("From TblMakeToStockHdr A ");
            SQL.AppendLine("Left Join TblBomHdr B On A.BomDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblBomDtl2 C On A.BomDocNo=C.DocNo And C.ItType='1' ");
            SQL.AppendLine("Left Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "BomDocNo", "DocName", "ItCode", 
                        
                        //6-10
                        "ItName", "Qty", "PlanningUomCode", "UsageDt", "Remark",
 
                        //11-12
                        "Status", "ProductionWorkGroup"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y") ? true : false;
                        TxtBomDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtDocName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[6]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetDte(DteUsageDt, Sm.DrStr(dr, c[9]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[12]));
                    }, true
                );
        }

        private void ShowMakeToStockDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                if (mItCtRMPActual.Length > 0) Sm.CmParam<String>(ref cm, "@ItCtRMPActual", mItCtRMPActual);
                if (mItCtRMPActual2.Length > 0) Sm.CmParam<String>(ref cm, "@ItCtRMPActual2", mItCtRMPActual2);

                var SQL = new StringBuilder();

                if (mIsMTSNeedBom)
                {
                    SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, B.Specification, E.CtItCode, ");
                    SQL.AppendLine("A.Qty, B.PlanningUomCode2 As PlanningUomCode, 0 As Qty2, Null As PlanningUomCode2, ");
                    SQL.AppendLine("A.UsageDt, A.Remark, B.PlanningUomCodeConvert12 ");
                    SQL.AppendLine("From TblMakeToStockDtl A ");
                    SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                    SQL.AppendLine("Inner Join TblMakeToStockHdr C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("Inner Join TblBOMDtl D On C.BomDocNo=D.DocNo And D.DocType<>'3' And B.ItCode=D.DocCode ");
                    SQL.AppendLine("LEFT JOIN TblCustomerItem E ON B.ItCode = E.ItCode ");
                    SQL.AppendLine("Where A.DocNo=@DocNo Order By D.DNo;");
                }
                else
                {
                    SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.Specification, C.CtItCode, ");
                    SQL.AppendLine("A.Qty, B.PlanningUomCode, ");
                    SQL.AppendLine("(A.Qty*B.PlanningUomCodeConvert12) As Qty2, B.PlanningUomCode2, ");
                    SQL.AppendLine("A.UsageDt, ");
                    if (mItCtRMPActual.Length > 0 || mItCtRMPActual2.Length > 0)
                    {
                        SQL.AppendLine("ifnull(Concat(B.ItName, ' ( ', ");
                        SQL.AppendLine("        Case B.ItCtCode  ");
                        SQL.AppendLine("            When @ItCtRMPActual Then ");
                        SQL.AppendLine("                Concat( ");
                        SQL.AppendLine("                'L: ', B.Length, ");
                        SQL.AppendLine("                ', D: ', B.Diameter ");
                        SQL.AppendLine("                ) ");
                        SQL.AppendLine("            When @ItCtRMPActual2 Then  ");
                        SQL.AppendLine("                Concat(  ");
                        SQL.AppendLine("                'L: ', B.Length, ");
                        SQL.AppendLine("                ', W: ', B.Width,  ");
                        SQL.AppendLine("                ', H: ', B.Height ");
                        SQL.AppendLine("                ) ");
                        SQL.AppendLine("        End, ");
                        SQL.AppendLine("    ' )'), B.ItName) As ItName, ");
                    }
                    else
                        SQL.AppendLine("B.ItName, ");
                    SQL.AppendLine("A.Remark, B.PlanningUomCodeConvert12 ");
                    SQL.AppendLine("From TblMakeToStockDtl A ");
                    SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
                    SQL.AppendLine("LEFT JOIN TblCustomerItem C ON B.ItCode = C.ItCode ");
                    SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");
                }
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "ItCode",
                        
                        //1-5
                        "ItCodeInternal", "ItName", "Qty", "PlanningUomCode", "Qty2", 
                        
                        //6-10
                        "PlanningUomCode2", "UsageDt", "Remark", "PlanningUomCodeConvert12", "Specification",

                        //11
                        "CtItCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Grd.Cells[Row, 10].Value = 0m;
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 6, 10, 11 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                        {
                            Sm.FormShowDialog(new FrmMakeToStockDlg(this, mIsMTSNeedBom ? TxtBomDocNo.Text : string.Empty));
                        }
                    }

                    if (Sm.IsGrdColSelected(new int[] { 0, 4, 8, 9 }, e.ColIndex))
                    {
                        if (e.ColIndex == 8) Sm.DteRequestEdit(Grd1, DteDt, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 6, 10, 11 });
                    }
                }
            }
        }

       
        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmMakeToStockDlg(this, mIsMTSNeedBom?TxtBomDocNo.Text:string.Empty));
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 8)
            {
                if (Sm.GetGrdDate(Grd1, 0, 8).Length != 0)
                {
                    var UsageDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 8));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) Grd1.Cells[Row, 8].Value = UsageDt;
                }
            }
        }

        override protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 8)
                e.Text = "Double click title to copy data based on the first row's value.";
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 4 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 9 }, e);
            if (e.ColIndex == 4) ComputeQty2(e.RowIndex);
        }

        #endregion

        #endregion

        #region Additional Method

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void RecomputeQty()
        {
            if (Grd1.Rows.Count != 1 && mIsMTSNeedBom)
            {
                decimal Index = 1m;
                if (mQty!=0) Index = decimal.Parse(TxtQty.Text) / mQty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        Grd1.Cells[Row, 4].Value = Index*Sm.GetGrdDec(Grd1, Row, 10);
            }
        }

        private void ComputeQty2(int Row)
        {
            decimal PlanningUomCodeConvert12 = 0m, Qty = 0m;
            
            if (Sm.GetGrdStr(Grd1, Row, 11).Length > 0) PlanningUomCodeConvert12 = Sm.GetGrdDec(Grd1, Row, 11);
            if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) Qty = Sm.GetGrdDec(Grd1, Row, 4);

            Grd1.Cells[Row, 6].Value = PlanningUomCodeConvert12 * Qty;
        }

        #endregion

        #region Event

        #region Button Event

        private void BtnBomDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMakeToStockDlg2(this));
        }

        #endregion

        #region Misc Control Event

        private void DteDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDt, ref fCell, ref fAccept);
        }

        private void TxtQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQty, 0);
            RecomputeQty();
        }

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
            }
        }

        #endregion

        #endregion
    }
}
