﻿#region Update
/*
    20/11/2017 [TKG] nilai berupa angka menggunakan format 2 angka di belakang koma.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialQCInspection : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMaterialQCInspection(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.QCPlanningDocNo, A.DocDt, H.QCPDesc, H.QCPIndicator, I.ItName, D.BatchNo, ");
            SQL.AppendLine("Round(D.Qty2,2) As PlanningQty, Round(B.Qty,2) As QCPassed, Round((D.Qty2-B.Qty),2)As Residual, ");
            SQL.AppendLine("Round((B.Qty/D.Qty2)*100,0) As Passed, J.WhsName "); 
            SQL.AppendLine("From TblMaterialQCInspection2Hdr A ");
            SQL.AppendLine("Inner Join TblMaterialQCInspection2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblQCPlanningHdr C On A.QCPlanningDocNo=C.DocNo ");
            SQL.AppendLine("Inner JOIN TblQCPlanningDtl D ON A.QCPlanningDocNo = D.DocNo And D.DNo = B.QCPlanningDNo ");
            SQL.AppendLine("Inner Join TblQCParameter H On B.QCPCode = H.QCPCode ");
            SQL.AppendLine("Inner Join TblItem I On D.ItCode=I.ItCode ");
            SQL.AppendLine("Inner Join TblWarehouse J On C.WhsCode=J.WhsCode ");
            SQL.AppendLine("Where B.status='1'");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "",
                        "Planning"+Environment.NewLine+"Document",
                        "",
                        "Location",
                        
                        //6-10 
                        "Inspection Date",
                        "Parameter",
                        "Indicator#",
                        "Item's Name",
                        "Batch#",
                       
                        //11-14
                        "Planning Qty",
                        "QC Passed",
                        "Residual",
                        "Passed %"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 20, 140, 20, 150,
                        
                        //6-10
                        100, 120, 120, 200, 130,
                        
                        //11-14
                        100, 100, 120, 120,

                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 14 }, 2);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, false);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

 
        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPlanningDocNo.Text, "A.QCPlanningDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "C.WhsCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "QCPlanningDocNo", "WhsName", "DocDt",  "QCPDesc", "QCPIndicator",
                            
                            //6-10
                            "ItName", "BatchNo", "PlanningQty", "QCPassed", "Residual", 
                            
                            //11
                            "Passed", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmMaterialQCInspection2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmQCPlanning(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmMaterialQCInspection2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmQCPlanning(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void textEdit1_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        private void TxtPlanningDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPlanningDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Planning#");
        }

        #endregion

    }
}
