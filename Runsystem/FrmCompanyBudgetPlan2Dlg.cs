﻿#region Update
/*
    03/12/2020 [TKG/IMS] New application
    12/01/2021 [WED/IMS] rate per bulan bisa beda
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyBudgetPlan2Dlg : RunSystem.FrmBase15
    {
        #region Field

        private FrmCompanyBudgetPlan2 mFrmParent;
        private int mR;
        private bool mEditable = false;
        private int[] mColsAmt = { 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39 };

        #endregion

        #region Constructor

        public FrmCompanyBudgetPlan2Dlg(FrmCompanyBudgetPlan2 FrmParent, int r, bool Editable)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mR = r;
            mEditable = Editable;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetGrd();
                ShowData();
                if (!mEditable)
                {
                    Grd1.ReadOnly = true;
                    BtnSave.Enabled = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 40;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Item's Code",
                    "Item's Name",
                    "UoM",
                    "Rate 01",
                    "Quantity 01",

                    //6-10
                    "Amount 01",
                    "Rate 02",
                    "Quantity 02",
                    "Amount 02",
                    "Rate 03",

                    //11-15
                    "Quantity 03",
                    "Amount 03",
                    "Rate 04",
                    "Quantity 04",
                    "Amount 04",

                    //16-20
                    "Rate 05",
                    "Quantity 05",
                    "Amount 05",
                    "Rate 06",
                    "Quantity 06",

                    //21-25
                    "Amount 06",
                    "Rate 07",
                    "Quantity 07",
                    "Amount 07",
                    "Rate 08",

                    //26-30
                    "Quantity 08",
                    "Amount 08",
                    "Rate 09",
                    "Quantity 09",
                    "Amount 09",

                    //31-35
                    "Rate 10",
                    "Quantity 10",
                    "Amount 10",
                    "Rate 11",
                    "Quantity 11",

                    //36-39
                    "Amount 11",
                    "Rate 12",
                    "Quantity 12",
                    "Amount 12"
                },
                new int[] 
                {
                    //0
                    20,

                    //1-5
                    100, 200, 100, 130, 130,
                    
                    //6-10
                    130, 130, 130, 130, 130,

                    //11-15
                    130, 130, 130, 130, 130,

                    //16-20
                    130, 130, 130, 130, 130,

                    //21-25
                    130, 130, 130, 130, 130,

                    //26-30
                    130, 130, 130, 130, 130, 

                    //31-35
                    130, 130, 130, 130, 130, 

                    //36-39
                    130, 130, 130, 130
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39 });
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowData()
        {
            TxtAcNo.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mR, 1);
            TxtAcDesc.EditValue = Sm.GetValue("Select AcDesc From TblCOA Where AcNo=@Param Limit 1;", TxtAcNo.Text);
            if (mFrmParent.ml.Count > 0)
            {
                int r = 0;
                Grd1.BeginUpdate();
                foreach (var x in mFrmParent.ml.Where(w => Sm.CompareStr(w.AcNo, TxtAcNo.Text)))
                {
                    Grd1.Rows.Add();
                    Grd1.Cells[r, 1].Value = x.ItCode;
                    Grd1.Cells[r, 2].Value = x.ItName;
                    Grd1.Cells[r, 3].Value = x.PurchaseUomCode;
                    Grd1.Cells[r, 4].Value = x.Rate01;
                    Grd1.Cells[r, 5].Value = x.Qty01;
                    Grd1.Cells[r, 6].Value = x.Rate01*x.Qty01;
                    Grd1.Cells[r, 7].Value = x.Rate02;
                    Grd1.Cells[r, 8].Value = x.Qty02;
                    Grd1.Cells[r, 9].Value = x.Rate02 * x.Qty02;
                    Grd1.Cells[r, 10].Value = x.Rate03;
                    Grd1.Cells[r, 11].Value = x.Qty03;
                    Grd1.Cells[r, 12].Value = x.Rate03 * x.Qty03;
                    Grd1.Cells[r, 13].Value = x.Rate04;
                    Grd1.Cells[r, 14].Value = x.Qty04;
                    Grd1.Cells[r, 15].Value = x.Rate04 * x.Qty04;
                    Grd1.Cells[r, 16].Value = x.Rate05;
                    Grd1.Cells[r, 17].Value = x.Qty05;
                    Grd1.Cells[r, 18].Value = x.Rate05 * x.Qty05;
                    Grd1.Cells[r, 19].Value = x.Rate06;
                    Grd1.Cells[r, 20].Value = x.Qty06;
                    Grd1.Cells[r, 21].Value = x.Rate06 * x.Qty06;
                    Grd1.Cells[r, 22].Value = x.Rate07;
                    Grd1.Cells[r, 23].Value = x.Qty07;
                    Grd1.Cells[r, 24].Value = x.Rate07 * x.Qty07;
                    Grd1.Cells[r, 25].Value = x.Rate08;
                    Grd1.Cells[r, 26].Value = x.Qty08;
                    Grd1.Cells[r, 27].Value = x.Rate08 * x.Qty08;
                    Grd1.Cells[r, 28].Value = x.Rate09;
                    Grd1.Cells[r, 29].Value = x.Qty09;
                    Grd1.Cells[r, 30].Value = x.Rate09 * x.Qty09;
                    Grd1.Cells[r, 31].Value = x.Rate10;
                    Grd1.Cells[r, 32].Value = x.Qty10;
                    Grd1.Cells[r, 33].Value = x.Rate10 * x.Qty10;
                    Grd1.Cells[r, 34].Value = x.Rate11;
                    Grd1.Cells[r, 35].Value = x.Qty11;
                    Grd1.Cells[r, 36].Value = x.Rate11 * x.Qty11;
                    Grd1.Cells[r, 37].Value = x.Rate12;
                    Grd1.Cells[r, 38].Value = x.Qty12;
                    Grd1.Cells[r, 39].Value = x.Rate12 * x.Qty12;
                    r++;
                }
            }
            Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, 
                new int[] { 
                    4, 5, 6, 7, 8, 9, 10, 
                    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                    31, 32, 33, 34, 35, 36, 37, 38, 39
                });
            Grd1.EndUpdate();
        }

        #endregion

        #region Button Methods

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (IsGrdValueNotValid()) return;
            try
            {
                if (mFrmParent.ml.Count > 0) mFrmParent.ml.RemoveAll(x => Sm.CompareStr(x.AcNo, TxtAcNo.Text));
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    {
                        mFrmParent.ml.Add(new FrmCompanyBudgetPlan2.AcItem()
                        {
                            AcNo = TxtAcNo.Text,
                            ItCode = Sm.GetGrdStr(Grd1, r, 1),
                            ItName = Sm.GetGrdStr(Grd1, r, 2),
                            PurchaseUomCode = Sm.GetGrdStr(Grd1, r, 3),
                            Rate01 = Sm.GetGrdDec(Grd1, r, 4),
                            Qty01 = Sm.GetGrdDec(Grd1, r, 5),
                            Rate02 = Sm.GetGrdDec(Grd1, r, 7),
                            Qty02 = Sm.GetGrdDec(Grd1, r, 8),
                            Rate03 = Sm.GetGrdDec(Grd1, r, 10),
                            Qty03 = Sm.GetGrdDec(Grd1, r, 11),
                            Rate04 = Sm.GetGrdDec(Grd1, r, 13),
                            Qty04 = Sm.GetGrdDec(Grd1, r, 14),
                            Rate05 = Sm.GetGrdDec(Grd1, r, 16),
                            Qty05 = Sm.GetGrdDec(Grd1, r, 17),
                            Rate06 = Sm.GetGrdDec(Grd1, r, 19),
                            Qty06 = Sm.GetGrdDec(Grd1, r, 20),
                            Rate07 = Sm.GetGrdDec(Grd1, r, 22),
                            Qty07 = Sm.GetGrdDec(Grd1, r, 23),
                            Rate08 = Sm.GetGrdDec(Grd1, r, 25),
                            Qty08 = Sm.GetGrdDec(Grd1, r, 26),
                            Rate09 = Sm.GetGrdDec(Grd1, r, 28),
                            Qty09 = Sm.GetGrdDec(Grd1, r, 29),
                            Rate10 = Sm.GetGrdDec(Grd1, r, 31),
                            Qty10 = Sm.GetGrdDec(Grd1, r, 32),
                            Rate11 = Sm.GetGrdDec(Grd1, r, 34),
                            Qty11 = Sm.GetGrdDec(Grd1, r, 35),
                            Rate12 = Sm.GetGrdDec(Grd1, r, 37),
                            Qty12 = Sm.GetGrdDec(Grd1, r, 38)
                        });
                    }
                }
                decimal v = 0m;
                for (int i = 1; i <= 12; i++)
                {
                    v = 0m;
                    for (int j= 0; j < Grd1.Rows.Count; j++)
                        v+=Sm.GetGrdDec(Grd1, j, mColsAmt[i-1]);
                    mFrmParent.Grd1.Cells[mR, (i * 2) + 4].Value = v;
                    mFrmParent.ComputeAmt(mR, (i * 2) + 4, mFrmParent.mInputAmt);
                }
                this.Close();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Item's code is empty.")) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmCompanyBudgetPlan2Dlg2(this));
            }

            if (Sm.IsGrdColSelected(new int[] { 0, 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26, 28, 29, 31, 32, 34, 35, 37, 38 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26, 28, 29, 31, 32, 34, 35, 37, 38 });
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmCompanyBudgetPlan2Dlg2(this));
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26, 28, 29, 31, 32, 34, 35, 37, 38 }, e);
            
            //if (e.ColIndex == 4)
            //{ 
            //    var Rate = Sm.GetGrdDec(Grd1, e.RowIndex, 4);
            //    for(int i=1;i<=12;i++)
            //        Grd1.Cells[e.RowIndex, (2 * i) + 4].Value = Rate * Sm.GetGrdDec(Grd1, e.RowIndex, (2 * i) + 3);
            //}

            if (Sm.IsGrdColSelected(new int[] { 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26, 28, 29, 31, 32, 34, 35, 37, 38 }, e.ColIndex))
            {
                ComputeAmount(e.RowIndex, e.ColIndex);
            }
        }

        #endregion

        #region Additional Methods

        private void ComputeAmount(int Row, int SelectedCols)
        {
            if (SelectedCols == 4 || SelectedCols == 5)
                ComputeAmountPerMonth(Row, 4, 5, mColsAmt[0]);

            if (SelectedCols == 7 || SelectedCols == 8)
                ComputeAmountPerMonth(Row, 7, 8, mColsAmt[1]);

            if (SelectedCols == 10 || SelectedCols == 11)
                ComputeAmountPerMonth(Row, 10, 11, mColsAmt[2]);

            if (SelectedCols == 13 || SelectedCols == 14)
                ComputeAmountPerMonth(Row, 13, 14, mColsAmt[3]);

            if (SelectedCols == 16 || SelectedCols == 17)
                ComputeAmountPerMonth(Row, 16, 17, mColsAmt[4]);

            if (SelectedCols == 19 || SelectedCols == 20)
                ComputeAmountPerMonth(Row, 19, 20, mColsAmt[5]);

            if (SelectedCols == 22 || SelectedCols == 23)
                ComputeAmountPerMonth(Row, 22, 23, mColsAmt[6]);

            if (SelectedCols == 25 || SelectedCols == 26)
                ComputeAmountPerMonth(Row, 25, 26, mColsAmt[7]);

            if (SelectedCols == 28 || SelectedCols == 29)
                ComputeAmountPerMonth(Row, 28, 29, mColsAmt[8]);

            if (SelectedCols == 31 || SelectedCols == 32)
                ComputeAmountPerMonth(Row, 31, 32, mColsAmt[9]);

            if (SelectedCols == 34 || SelectedCols == 35)
                ComputeAmountPerMonth(Row, 34, 35, mColsAmt[10]);

            if (SelectedCols == 37 || SelectedCols == 38)
                ComputeAmountPerMonth(Row, 37, 38, mColsAmt[11]);
            
        }

        private void ComputeAmountPerMonth(int Row, int RateCols, int QtyCols, int AmtCols)
        {
            Grd1.Cells[Row, AmtCols].Value = Sm.GetGrdDec(Grd1, Row, RateCols) * Sm.GetGrdDec(Grd1, Row, QtyCols);
        }

        #endregion

        #endregion
    }
}

