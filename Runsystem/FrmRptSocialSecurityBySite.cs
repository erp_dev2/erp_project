﻿#region
/*
 * 03/08/2021 [ICA/PHT] new Apps 
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSocialSecurityBySite : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool
            mIsNotFilterByAuthorization = false,
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptSocialSecurityBySite(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From( ");
            SQL.AppendLine("Select B.SiteCode, D.SiteName, Sum(A.SSEmployerHealth+A.SSEmployeeHealth) as HealthAmt, Sum(A.SSEmployerEmployment+A.SSEmployeeEmployment) as EmploymentAmt, ");
            SQL.AppendLine("Sum(A.SSEmployerPension+A.SSEmployeePension) as DPLKAmt, Sum(E.DPNPHTAmt) DPNPHTAmt, Sum(E.DPNPNSAmt) DPNPNSAmt, Sum(E.TSPAmt) TSPAmt, Sum(A.SSEmployerBNILife+A.SSEmployeeBNILife) as EmpDapenAmt, ");
            SQL.AppendLine("Sum(E.PRMHAmt) PRMHAmt, Sum(E.PKESAmt) PKESAmt, Sum(E.WNRAmt) WNRAmt ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
            SQL.AppendLine("Inner Join TblSite D On B.SiteCode = D.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.PayrunCode, T.EmpCode,  Sum(IfNull(T.DPNPHT, 0.00)) as DPNPHTAmt, Sum(IfNull(T.DPNPNS, 0.00)) as DPNPNSAmt, Sum(IfNull(T.TSP, 0.00)) as TSPAmt, ");
            SQL.AppendLine("    Sum(IfNull(T.PRMH, 0.00)) as PRMHAmt, Sum(IfNull(T.PKES, 0.00)) as PKESAmt, Sum(IfNull(T.WNR, 0.00)) as WNRAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.PayrunCode, A.EmpCode, B.SSPName, ");
            SQL.AppendLine("        Case A.SSPCode When 'DPN-PHT' then A.EeAmt End DPNPHT, ");
            SQL.AppendLine("    	Case A.SSPCode When 'DPN-PNS' then A.EeAmt End DPNPNS, ");
            SQL.AppendLine("        Case A.SSPCode When 'TSP' then A.EeAmt End TSP, ");
            SQL.AppendLine("        Case A.SSPCode When 'P-RMH' then A.ErAmt End PRMH, ");
            SQL.AppendLine("        Case A.SSPCode When 'P-KES' then A.ErAmt End PKES, ");
            SQL.AppendLine("        Case A.SSPCode When 'WNR' then A.ErAmt End WNR ");
            SQL.AppendLine("        From TblPayrollProcessSSProgram A ");
            SQL.AppendLine("        Inner Join TblSSProgram B On A.SSPCode = B.SSPCode ");
            SQL.AppendLine("        Where A.BPJSInd = 'N' ");
            SQL.AppendLine("    )T ");
            SQL.AppendLine("    Group By T.PayrunCode, T.EMpCode ");
            SQL.AppendLine(")E On E.PayrunCode = A.PayrunCode And E.EmpCode = A.EmpCode ");
            SQL.AppendLine("Where Left(B.StartDt, 6) <= CONCAT(@Yr, @Mth) ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Group By B.SiteCode )T2 ");
            SQL.AppendLine("Where 1=1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Site",
                    "Amount"+Environment.NewLine+"BPJS Kesehatan",
                    "Amount"+Environment.NewLine+"BPJS Ketenagakerjaan",
                    "Amount"+Environment.NewLine+"BPJS DPLK",
                    "Amount"+Environment.NewLine+"Dana Pensiun PHT",
                     
                    //6-10
                    "Amount"+Environment.NewLine+"Dana Pensiun PNS",
                    "Amount"+Environment.NewLine+"Taspen",
                    "Amount"+Environment.NewLine+"Dapen Pemberi Kerja",
                    "Amount"+Environment.NewLine+"Kepemilikan Rumah",
                    "Amount"+Environment.NewLine+"Premi Kesehatan",

                    //11
                    "Amount"+Environment.NewLine+"Wanaarta",

                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    350, 150, 150, 150, 150,  
                    
                    //6-10
                    150, 150, 150, 150, 150,
  
                    //11
                    150,
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, 0);
            Sm.SetGrdProperty(Grd1, false);

        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T2.SiteCode", true);


                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T2.SiteName; ",
                    new string[]
                    {
                        //0
                        "SiteName",
                        
 
                        //1-5
                        "HealthAmt", "EmploymentAmt", "DPLKAmt", "DPNPHTAmt", "DPNPNSAmt", 

                        //6-10
                        "TSPAmt", "EmpDapenAmt", "PRMHAmt", "PKESAmt", "WNRAmt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);

                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion


        #endregion

        #region events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");

        }

        #endregion events
    }
}
