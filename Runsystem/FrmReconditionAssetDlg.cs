﻿#region Update
/*
    24/01/2022 [IBL/Product] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmReconditionAssetDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmReconditionAsset mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmReconditionAssetDlg(FrmReconditionAsset FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List of Asset";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLueAssetCategoryCode(ref LueAssetCtCode);
            Sl.SetLueSiteCode(ref LueSiteCode);
            ShowData();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.AssetDt, B.AssetCode, C.AssetCategoryName, ");
            SQL.AppendLine("B.AssetName, D.SiteName, B.AssetValue, E.AcNo, E.AcDesc ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode And A.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblAssetCategory C On B.AssetCategoryCode = C.AssetCategoryCode ");
            SQL.AppendLine("Left Join TblSite D On B.SiteCode2 = D.SiteCode ");
            SQL.AppendLine("Left Join TblCOA E On B.AcNo = E.AcNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
            Grd1, new string[]
            {
                //0
                "No.",

                //1-5
                "Date of" + Environment.NewLine + "Purchase",
                "Asset's Code",
                "Asset's"+Environment.NewLine+"Category",
                "Asset's Name",
                "Site",

                //6-8
                "Asset Value",
                "COA#",
                "COA Description",
                "Depreciation#"
            },
            new int[]
            {
                //0
                50,

                //1-5
                120, 200, 250, 250, 200,

                //6-9
                200, 200, 250, 0
            }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
        }
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAssetCtCode), "B.AssetCategoryCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetName.Text, new string[] { "B.AssetName", "B.AssetCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.AssetCode",
                new string[]
                {
                    //0
                    "AssetDt", 

                    //1-5
                    "AssetCode", "AssetCategoryName", "AssetName", "SiteName", "AssetValue",

                    //6-8
                    "AcNo", "AcDesc", "DocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 0))
                {
                    mFrmParent.ShowDepreciationAssetData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9));
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void LueAssetCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAssetCtCode, new Sm.RefreshLue1(Sl.SetLueAssetCategoryCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkAssetCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset's Category");
        }
        private void TxtAssetName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset's Name");
        }
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #region Grid Event
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
