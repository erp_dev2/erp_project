﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalAPDownpayment : RunSystem.FrmBase9
    {
        #region Field

        internal string mDocNo = string.Empty;
 
        #endregion

        #region Constructor

        public FrmDocApprovalAPDownpayment(string MenuCode) 
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                TxtDocNo.EditValue = mDocNo;
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr( 
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Requested"+Environment.NewLine+"Date",
                        "Requested#", 
                        "Item",
                        "Requested"+Environment.NewLine+"Qty",
                        "PO Request"+Environment.NewLine+"Date",

                        //6-10
                        "PO Request#",
                        "Vendor", 
                        "PO Date",
                        "PO#",
                        "PO"+Environment.NewLine+"Price",
                        
                        //11
                        "PO"+Environment.NewLine+"Tax %",
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1, 5, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 10, 11 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select ");
                SQL.AppendLine("E.DocDt As MaterialRequestDocDt, E.LocalDocNo As MaterialRequestLocalDocNo, I.ItName, F.Qty, ");
                SQL.AppendLine("C.DocDt As PORequestDocDt, C.LocalDocNo As PORequestLocalDocNo, J.VdName, ");
                SQL.AppendLine("A.DocDt As PODocDt, A.LocalDocNo As POLocalDocNo, H.UPrice, IfNull(K.TaxRate, 0) As TaxRate ");
                SQL.AppendLine("From TblPOHdr A ");
                SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
                SQL.AppendLine("Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                SQL.AppendLine("Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                SQL.AppendLine("Inner Join TblItem I On F.ItCode=I.ItCode ");
                SQL.AppendLine("Inner Join TblVendor J On G.VdCode=J.VdCode ");
                SQL.AppendLine("Left Join TblTax K On A.TaxCode1=K.TaxCode ");
                SQL.AppendLine("Where A.DocNo In (Select PODocNo From TblAPDownpayment Where DocNo=@DocNo) ");
                SQL.AppendLine("Order By I.ItName, J.VdName;");

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "MaterialRequestDocDt",
                            
                            //1-5
                            "MaterialRequestLocalDocNo", 
                            "ItName", 
                            "Qty", 
                            "PORequestDocDt", 
                            "PORequestLocalDocNo", 

                            //6-10
                            "VdName", 
                            "PODocDt", 
                            "POLocalDocNo", 
                            "UPrice", 
                            "TaxRate"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion
    }
}
