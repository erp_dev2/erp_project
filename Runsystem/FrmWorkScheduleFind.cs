﻿#region Update
/*
    06/11/2017 [TKG] tambah site
    02/12/2017 [TKG] tambah indikator double-shift
    08/02/2020 [TKG/IMS] tambah break 2
    14/02/2020 [HAR/IOK] Bug jumlah kolom tidak sesuai
    04/12/2020 [TKG/IMS] tambah deduction rounding
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWorkScheduleFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmWorkSchedule mFrmParent;

        #endregion

        #region Constructor

        public FrmWorkScheduleFind(FrmWorkSchedule FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 38;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "Holiday",
                        "Double-Shift",
                        
                        //6-10
                        "Check In",
                        "Before"+Environment.NewLine+"Check In",
                        "After"+Environment.NewLine+"Check In",
                        "Check Out",
                        "Before"+Environment.NewLine+"Check Out",
                        
                        //11-15
                        "After"+Environment.NewLine+"Check Out",
                        "Break In",
                        "Before"+Environment.NewLine+"Break In",
                        "After"+Environment.NewLine+"Break In",
                        "Break Out",
                        
                        //16-20
                        "Before"+Environment.NewLine+"Break Out",
                        "After"+Environment.NewLine+"Break Out",
                        "Routin OT In",
                        "Before"+Environment.NewLine+"Routin OT In",
                        "After"+Environment.NewLine+"Routin OT In",
                        
                        //21-25
                        "Routin OT Out",
                        "Before"+Environment.NewLine+"Routin OT Out",
                        "After"+Environment.NewLine+"Routin OT Out",
                        "Break In (2)",
                        "Before"+Environment.NewLine+"Break In (2)",

                        //26-30
                        "After"+Environment.NewLine+"Break In (2)",
                        "Break Out (2)",
                        "Before"+Environment.NewLine+"Break Out (2)",
                        "After"+Environment.NewLine+"Break Out (2)",
                        "Site",

                        //31-35
                        "Deduction"+Environment.NewLine+"Rounding",
                        "Created By",
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By", 

                        //36-37
                        "Last Updated Date", 
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 80, 80, 100, 
                        
                        //6-10
                        80, 90, 90, 90, 90, 

                        //11-15
                        90, 90, 90, 90, 90,

                        //16-20
                        90, 80, 100, 100, 100, 
                        
                        //21-25
                        100, 100, 100, 100, 100, 
                        
                        //26-30
                        100, 100, 100, 100, 200, 
                        
                        //31-35
                        130, 130, 130, 130, 130, 
                        
                        //36-37
                        130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4, 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 33, 36});
            Sm.GrdFormatTime(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 34, 37 });
            Sm.GrdColInvisible(Grd1, new int[] { 31, 32, 33, 34, 35, 36, 37 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 31, 32, 33, 34, 35, 36, 37 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var cm = new MySqlCommand();

            Sm.FilterStr(ref Filter, ref cm, TxtWSCode.Text, new string[] { "A.WSCode", "A.WSName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

            SQL.AppendLine("Select A.*, B.SiteName, C.OptDesc As DeductionRoundDesc ");
            SQL.AppendLine("From TblWorkSchedule A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblOption C On A.DeductionRoundCode=C.OptCode And C.OptCat='DeductionRound' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By A.WSName;");

            try
            {
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "WSCode", 
                                
                            //1-5
                            "WSName", "ActInd", "HolidayInd", "DoubleShiftInd", "In1", 

                            //6-10
                            "bIn1", "aIn1", "Out1", "bOut1", "aOut1", 
                            
                            //10-15
                            "In2", "bIn2", "aIn2", "Out2", "bOut2", 

                            //16-20
                            "aOut2", "In3", "bIn3", "aIn3", "Out3", 
                            
                            //21-25
                            "bOut3", "aOut3", "In4", "bIn4", "aIn4", 
                            
                            //26-30
                            "Out4", "bOut4", "aOut4", "SiteName", "DeductionRoundDesc", 
                            
                            //31-34
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 33, 32);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 33);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 36, 34);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 37, 34);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Schedule Event

        private void TxtWSCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWSCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work schedule");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
