﻿#region Update
/*
    26/11/2020 [DITA/PHT] new apps --> RKAP (Rencana Kerja Anggaran Perusahaan)
    03/12/2020 [TKG/PHT] tambah proses per item per coa
    11/12/2020 [TKG/PHT] ubah proses pembuatan budget request
    23/12/2020 [ICA/PHT] Set Lue Cost Center berdasarkan CBPInd
    12/01/2021 [WED/PHT] bisa input per bulan
    28/01/2021 [WED/PHT] ketika di CompletedInd, maka akan terbentuk dokumen BudgetRequestYearly
    18/05/2021 [VIN/PHT] seqno 8 digit 
    18/06/2021 [TKG/PHT] cost center divalidasi berdasarkan group thd profit center, cost center yg ditampilkan adalah cost center yg tidak menjadi parent cost center yg lain
    08/07/2021 [DITA/PHT] buat format csv untuk profit loss
    08/07/2021 [DITA/PHT] buat format csv untuk balance sheet
    12/07/2021 [DITA/PHT] buat validasi -> data dengan cost center yang sama tidak bisa save
    21/01/2022 [DITA/PHT] hasil import data, item name dan uom di dialog belum muncul
    02/05/2023 [TKG/PHT] mempercepat proses save
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmCompanyBudgetPlan2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty;
        internal bool
            mIsCompanyBudgetPlanProfitLoss = false,
            mIsCompanyBudgetPlanBalanceSheet = false,
            mIsFilterByCC = false;
        internal int[] mInputAmt = { 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28 };
        private int[] mPrevAmt = { 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29 };
        private int[] mActualAmt = { 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 };
        internal FrmCompanyBudgetPlan2Find FrmFind;
        internal List<AcItem> ml;

        #endregion

        #region Constructor

        public FrmCompanyBudgetPlan2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                ml = new List<AcItem>();
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueYr(LueYr, string.Empty);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 45;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "Account",
                    //1-5
                    "Account#", "Alias", "Allow", "Total", "Prev Total",
                    //6-10
                    "01", "Prev 01", "02", "Prev 02", "03",
                    //11-15
                    "Prev 03", "04", "Prev 04", "05", "Prev 05",
                    //16-20
                    "06", "Prev 06", "07", "Prev 07", "08", 
                    //21-25
                    "Prev 08", "09", "Prev 09", "10", "Prev 10",
                    //26-30
                    "11", "Prev 11", "12", "Prev 12", "Saved Total",
                    //31-35
                    "", "Actual Total", "Actual 01", "Actual 02", "Actual 03", 
                    //36-40
                    "Actual 04", "Actual 05", "Actual 06", "Actual 07", "Actual 08", 
                    //41-45
                    "Actual 09", "Actual 10", "Actual 11", "Actual 12"
                },
                new int[] 
                {
                    600,
                    200, 180, 20, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150, 
                    150, 150, 150, 150, 150,
                    20, 150, 150, 150, 150,
                    150, 150, 150, 150, 150,
                    150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 30 }, 0);
            Sm.GrdFormatDec(Grd1, mInputAmt, 0);
            Sm.GrdFormatDec(Grd1, mPrevAmt, 0);
            Sm.GrdFormatDec(Grd1, mActualAmt, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 30 });
            Sm.GrdColReadOnly(Grd1, mPrevAmt);
            Sm.GrdColReadOnly(Grd1, mActualAmt);
            Sm.GrdColReadOnly(Grd1, mInputAmt);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 30 });
            Sm.GrdColInvisible(Grd1, mPrevAmt);
            Sm.GrdColButton(Grd1, new int[] { 31 });
            Grd1.Cols[31].Move(1);
            Grd1.Cols[32].Move(7);
            Grd1.Cols[33].Move(10);
            Grd1.Cols[34].Move(13);
            Grd1.Cols[35].Move(16);
            Grd1.Cols[36].Move(19);
            Grd1.Cols[37].Move(21);
            Grd1.Cols[38].Move(24);
            Grd1.Cols[39].Move(27);
            Grd1.Cols[40].Move(30);
            Grd1.Cols[41].Move(33);
            Grd1.Cols[42].Move(36);
            Grd1.Cols[43].Move(39);
            Grd1.Cols[44].Move(42);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueYr, LueCCCode, MeeRemark, ChkCompletedInd
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnShowAc.Enabled = BtnImport.Enabled = false;
                    Sm.GrdColInvisible(Grd1, mActualAmt, true);
                    LueYr.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueYr, MeeRemark, LueCCCode, ChkCompletedInd }, false);
                    BtnShowAc.Enabled = BtnImport.Enabled = true;
                    Sm.GrdColInvisible(Grd1, mActualAmt, false);
                    LueYr.Focus();
                    break;
                case mState.Edit:
                    if (!ChkCancelInd.Checked) ChkCompletedInd.Properties.ReadOnly = false;
                    Sm.GrdColInvisible(Grd1, mActualAmt, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            ml.Clear();
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, LueYr, LueCCCode, MeeRemark,
               TxtBudgetRequestDocNo
            });
            ChkCancelInd.Checked = false;
            ChkCompletedInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5, 30 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, mInputAmt);
            Sm.SetGrdNumValueZero(ref Grd1, 0, mPrevAmt);
            Sm.SetGrdNumValueZero(ref Grd1, 0, mActualAmt);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(mInputAmt, e.ColIndex))
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 3) == "Y")
                    ComputeAmt(e.RowIndex, e.ColIndex, mInputAmt);
                else
                    Sm.StdMsg(mMsgType.Info, "You can't input amount for this account");
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(mInputAmt, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                if (e.ColIndex == 31 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "COA account# is empty"))
                {
                    e.DoDefault = false;
                    Sm.FormShowDialog(new FrmCompanyBudgetPlan2Dlg(this, e.RowIndex,
                        BtnSave.Enabled && (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length>0 && !IsDocAlreadyCompleted()))
                        ));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 31 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "COA account# is empty"))
                Sm.FormShowDialog(new FrmCompanyBudgetPlan2Dlg(this, e.RowIndex, BtnSave.Enabled && (TxtDocNo.Text.Length == 0 || (TxtDocNo.Text.Length > 0 && !IsDocAlreadyCompleted()))));
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCompanyBudgetPlan2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
                SetLueCCCode(ref LueCCCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if (ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "You can't edit this data." + Environment.NewLine +
                    "This data already cancelled.");
                return;
            }
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    UpdateData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Insert Data

        #region Old Code
        //private void InsertData()
        //{
        //    if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
        //        IsDataNotValid())
        //        return;
        //    string DocNo = string.Empty;

        //    Cursor.Current = Cursors.WaitCursor;

        //    if (mIsCompanyBudgetPlanBalanceSheet)
        //        DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CompanyBudgetPlanBalanceSheet", "TblCompanyBudgetPlanHdr");
        //    else
        //        DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "CompanyBudgetPlanProfitLoss", "TblCompanyBudgetPlanHdr");

        //    var cml = new List<MySqlCommand>();

        //    cml.Add(SaveCompanyBudgetPlanHdr(DocNo));

        //    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
        //        if (
        //            Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
        //            Sm.GetGrdDec(Grd1, Row, 4) != 0m
        //            )
        //            cml.Add(SaveCompanyBudgetPlanDtl(DocNo, Row));

        //    if (ChkCompletedInd.Checked)
        //    {
        //        string BudgetRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetRequest", "TblBudgetRequestYearlyHdr");
        //        cml.Add(SaveBudgetRequestYearly(DocNo, BudgetRequestDocNo));
        //    }

        //    Sm.ExecCommands(cml);

        //    ShowData(DocNo);
        //}

        //private MySqlCommand SaveCompanyBudgetPlanHdr(string DocNo)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    string CurrentDateTime = Sm.ServerCurrentDateTime();

        //    //doctype 2 = Profit Loss, 3= Blance Sheet
        //    SQL.AppendLine("Insert Into TblCompanyBudgetPlanHdr(DocNo, DocDt, Yr, CancelInd, CCCode, ");
        //    SQL.AppendLine("DocType, CompletedInd, BudgetRequestDocNo, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DocDt, @Yr, 'N', @CCCode, ");
        //    SQL.AppendLine("@DocType, @CompletedInd, @BudgetRequestDocNo, @Remark, @CreateBy, @CreateDt); ");

        //    SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl(DocNo, AcNo, Amt, Amt01, Amt02, Amt03, Amt04, Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, Amt11, Amt12, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocNo, AcNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @CreateBy, @CreateDt ");
        //    SQL.AppendLine("From TblCOA ");
        //    SQL.AppendLine("Where Find_In_Set(AcNo, @CCAcNo) ");
        //    if (mIsCompanyBudgetPlanProfitLoss)
        //        SQL.AppendLine("And Left(AcNo, 1) > 3 ");
        //    else
        //        SQL.AppendLine("And Left(AcNo, 1) Between 1 AND 3 ");

        //    SQL.AppendLine("; ");

        //    int i = 0;
        //    foreach (var x in ml)
        //    {
        //        SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl2(DocNo, AcNo, ItCode, Rate01, Rate02, Rate03, Rate04, Rate05, Rate06, Rate07, Rate08, Rate09, Rate10, Rate11, Rate12, Qty01, Qty02, Qty03, Qty04, Qty05, Qty06, Qty07, Qty08, Qty09, Qty10, Qty11, Qty12, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
        //        SQL.AppendLine("Select DocNo, @AcNo_" + i.ToString() +
        //            ", @ItCode_" + i.ToString() +
        //            ", @Rate01_" + i.ToString() +
        //            ", @Rate02_" + i.ToString() +
        //            ", @Rate03_" + i.ToString() +
        //            ", @Rate04_" + i.ToString() +
        //            ", @Rate05_" + i.ToString() +
        //            ", @Rate06_" + i.ToString() +
        //            ", @Rate07_" + i.ToString() +
        //            ", @Rate08_" + i.ToString() +
        //            ", @Rate09_" + i.ToString() +
        //            ", @Rate10_" + i.ToString() +
        //            ", @Rate11_" + i.ToString() +
        //            ", @Rate12_" + i.ToString() +
        //            ", @Qty01_" + i.ToString() +
        //            ", @Qty02_" + i.ToString() +
        //            ", @Qty03_" + i.ToString() +
        //            ", @Qty04_" + i.ToString() +
        //            ", @Qty05_" + i.ToString() +
        //            ", @Qty06_" + i.ToString() +
        //            ", @Qty07_" + i.ToString() +
        //            ", @Qty08_" + i.ToString() +
        //            ", @Qty09_" + i.ToString() +
        //            ", @Qty10_" + i.ToString() +
        //            ", @Qty11_" + i.ToString() +
        //            ", @Qty12_" + i.ToString() +
        //            ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
        //        SQL.AppendLine("From TblCompanyBudgetPlanHdr ");
        //        SQL.AppendLine("Where DocNo=@DocNo; ");

        //        Sm.CmParam<String>(ref cm, "@AcNo_" + i, x.AcNo);
        //        Sm.CmParam<String>(ref cm, "@ItCode_" + i, x.ItCode);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate01_" + i, x.Rate01);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate02_" + i, x.Rate02);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate03_" + i, x.Rate03);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate04_" + i, x.Rate04);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate05_" + i, x.Rate05);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate06_" + i, x.Rate06);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate07_" + i, x.Rate07);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate08_" + i, x.Rate08);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate09_" + i, x.Rate09);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate10_" + i, x.Rate10);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate11_" + i, x.Rate11);
        //        Sm.CmParam<Decimal>(ref cm, "@Rate12_" + i, x.Rate12);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty01_" + i, x.Qty01);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty02_" + i, x.Qty02);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty03_" + i, x.Qty03);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty04_" + i, x.Qty04);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty05_" + i, x.Qty05);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty06_" + i, x.Qty06);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty07_" + i, x.Qty07);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty08_" + i, x.Qty08);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty09_" + i, x.Qty09);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty10_" + i, x.Qty10);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty11_" + i, x.Qty11);
        //        Sm.CmParam<Decimal>(ref cm, "@Qty12_" + i, x.Qty12);

        //        ++i;
        //    }

        //    #region Old Code
        //    //if (ChkCompletedInd.Checked)
        //    //{
        //    //    SQL.AppendLine("Insert Into TblBudgetRequestHdr ");
        //    //    SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Yr, Mth, DeptCode, Amt, BudgetDocNo, Remark, Status, EntCode, CreateBy, CreateDt) ");
        //    //    SQL.AppendLine("Values(@BudgetRequestDocNo, @DocDt, Null, 'N', @Yr, '00', ");
        //    //    SQL.AppendLine("(Select DeptCode From TblCostCenter Where CCCode=@CCCode), ");
        //    //    SQL.AppendLine("(Select Sum(B.Amt) ");
        //    //    SQL.AppendLine(" From TblCompanyBudgetPlanHdr A ");
        //    //    SQL.AppendLine(" Inner Join TblCompanyBudgetPlanDtl B On A.DocNo=B.DocNo And B.AcNo Not In (Select Parent from TblCOA Where Parent Is Not Null) ");
        //    //    SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd='N'), ");
        //    //    SQL.AppendLine("Null, Concat('RKAP : ', @DocNo), 'O', ");
        //    //    SQL.AppendLine("(Select EntCode From TblCostCenter A, TblProfitCenter B Where A.ProfitCenterCode=B.ProfitCenterCode And A.CCCode=@CCCode), ");
        //    //    SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

        //    //    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //    //    SQL.AppendLine("Select T.DocType, @BudgetRequestDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
        //    //    SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BudgetRequest2' ");
        //    //    SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode=@DeptCode ");
        //    //    SQL.AppendLine("And (T.StartAmt=0 ");
        //    //    SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
        //    //    SQL.AppendLine("    Select A.Amt*1 ");
        //    //    SQL.AppendLine("    From TblBudgetRequestHdr A ");
        //    //    SQL.AppendLine("    Where A.DocNo=@BudgetRequestDocNo ");
        //    //    SQL.AppendLine("), 0)); ");

        //    //    SQL.AppendLine("Update TblBudgetRequestHdr Set Status = 'A' ");
        //    //    SQL.AppendLine("Where DocNo=@BudgetRequestDocNo ");
        //    //    SQL.AppendLine("And Not Exists( ");
        //    //    SQL.AppendLine("    Select 1 From TblDocApproval ");
        //    //    SQL.AppendLine("    Where DocType='BudgetRequest2' ");
        //    //    SQL.AppendLine("    And DocNo=@BudgetRequestDocNo ");
        //    //    SQL.AppendLine("); ");

        //    //    SQL.AppendLine("Set @Row:=0; ");

        //    //    SQL.AppendLine("Insert Into TblBudgetRequestDtl ");
        //    //    SQL.AppendLine("(DocNo, DNo, BCCode, Amt, ItCode, Qty, Remark, CreateBy, CreateDt) ");
        //    //    SQL.AppendLine("Select @BudgetRequestDocNo, ");
        //    //    SQL.AppendLine("Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //    //    SQL.AppendLine("BCCode, Amt, Null, 0.00, Null, @CreateBy, CurrentDateTime() ");
        //    //    SQL.AppendLine("From ( ");
        //    //    SQL.AppendLine("    Select E.BCCode, Sum(B.Amt) As Amt ");
        //    //    SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
        //    //    SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo=B.DocNo And B.AcNo Not In (Select Parent from TblCOA Where Parent Is Not Null) ");
        //    //    //SQL.AppendLine("    Inner Join TblCostCategory C On A.CCCode=C.CCCode And B.AcNo=C.AcNo And C.AcNo Is Not Null ");
        //    //    //SQL.AppendLine("    Inner Join TblCostCenter D On A.CCCode=D.CCCode And D.DeptCode Is Not Null ");
        //    //    //SQL.AppendLine("    Inner Join TblBudgetCategory E On C.CCtCode=E.CCtCode And D.DeptCode=E.DeptCode And E.ActInd='Y' ");
        //    //    SQL.AppendLine("    Inner Join TblBudgetCategory E On A.CCCode=E.CCCode And B.AcNo=E.AcNo And E.ActInd='Y' ");
        //    //    SQL.AppendLine("    Where A.CancelInd='N' ");
        //    //    SQL.AppendLine("    And A.DocNo=@DocNo ");
        //    //    SQL.AppendLine("    Group By E.BCCode ");
        //    //    SQL.AppendLine(") T Order By BCCode; ");
        //    //}
        //    #endregion

        //    cm.CommandText = SQL.ToString();
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
        //    if (mIsCompanyBudgetPlanBalanceSheet) Sm.CmParam<String>(ref cm, "@DocType", "3");
        //    else Sm.CmParam<String>(ref cm, "@DocType", "2");
        //    Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
        //    Sm.CmParam<String>(ref cm, "@CCAcNo", GetCCAcNo());
        //    Sm.CmParam<String>(ref cm, "@CompletedInd", ChkCompletedInd.Checked ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
        //    //if (ChkCompletedInd.Checked)
        //    //    Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetRequest", "TblBudgetRequestHdr"));
        //    //else
        //    //    Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", string.Empty);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@CreateDt", CurrentDateTime);

        //    return cm;
        //}

        //private MySqlCommand SaveCompanyBudgetPlanDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblCompanyBudgetPlanDtl Set ");
        //    SQL.AppendLine("    Amt=@Amt, ");
        //    SQL.AppendLine("    Amt01=@Amt01, ");
        //    SQL.AppendLine("    Amt02=@Amt02, ");
        //    SQL.AppendLine("    Amt03=@Amt03, ");
        //    SQL.AppendLine("    Amt04=@Amt04, ");
        //    SQL.AppendLine("    Amt05=@Amt05, ");
        //    SQL.AppendLine("    Amt06=@Amt06, ");
        //    SQL.AppendLine("    Amt07=@Amt07, ");
        //    SQL.AppendLine("    Amt08=@Amt08, ");
        //    SQL.AppendLine("    Amt09=@Amt09, ");
        //    SQL.AppendLine("    Amt10=@Amt10, ");
        //    SQL.AppendLine("    Amt11=@Amt11, ");
        //    SQL.AppendLine("    Amt12=@Amt12 ");
        //    SQL.AppendLine("Where DocNo=@DocNo And AcNo=@AcNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt01", Sm.GetGrdDec(Grd1, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt02", Sm.GetGrdDec(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt03", Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt04", Sm.GetGrdDec(Grd1, Row, 12));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt05", Sm.GetGrdDec(Grd1, Row, 14));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt06", Sm.GetGrdDec(Grd1, Row, 16));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt07", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt08", Sm.GetGrdDec(Grd1, Row, 20));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt09", Sm.GetGrdDec(Grd1, Row, 22));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt10", Sm.GetGrdDec(Grd1, Row, 24));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt11", Sm.GetGrdDec(Grd1, Row, 26));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt12", Sm.GetGrdDec(Grd1, Row, 28));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}
        #endregion
        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid())
                return;

            Cursor.Current = Cursors.WaitCursor;

            string 
                DocNoTemp = string.Empty, 
                DocNo = string.Empty,
                DocDt = Sm.GetDte(DteDocDt);
            var SQL = new StringBuilder();
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
            if (mIsCompanyBudgetPlanBalanceSheet)
                SQL.AppendLine("Set @DocNo=" + Sm.GetNewDocNo(DocDt, "CompanyBudgetPlanBalanceSheet", "TblCompanyBudgetPlanHdr", "1") + "; ");
            else
                SQL.AppendLine("Set @DocNo=" + Sm.GetNewDocNo(DocDt, "CompanyBudgetPlanProfitLoss", "TblCompanyBudgetPlanHdr", "1") + "; ");

            SaveCompanyBudgetPlanHdr(ref cm, ref SQL);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0 &&
                    Sm.GetGrdDec(Grd1, r, 4) != 0m)
                    SaveCompanyBudgetPlanDtl(ref cm, ref SQL, r);
            }


            if (ChkCompletedInd.Checked)
            {
                SQL.AppendLine("Set @BudgetRequestDocNo=" + Sm.GetNewDocNo(DocDt, "BudgetRequest", "TblBudgetRequestYearlyHdr", "1") + "; ");
                SaveBudgetRequestYearly(ref cm, ref SQL);
            }

            cm.CommandText = SQL.ToString();

            if (Gv.CurrentUserCode.Length <= 5)
                DocNoTemp = Gv.CurrentUserCode;
            else
                DocNoTemp = Sm.Right(Gv.CurrentUserCode, 5);

            DocNoTemp += Sm.ServerCurrentDateTime();

            Sm.CmParam<String>(ref cm, "@DocNoTemp", DocNoTemp);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            cml.Add(cm);
            Sm.ExecCommands(cml);

            DocNo = Sm.GetValue("Select DocNo From TblCompanyBudgetPlanHdr Where DocNoTemp Is Not Null And DocNoTemp=@Param;", DocNoTemp);

            if (DocNo.Length > 0)
                ShowData(DocNo);
            else
                BtnCancelClick(sender, e);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueCCCode, "Profit Center") ||
                IsGrdEmpty() ||
                IsDocAlreadyCreated()
                ;
        }

        private bool IsDocAlreadyCreated()
        {
            var CCCode = Sm.GetLue(LueCCCode);
            var SQL = new StringBuilder();
            SQL.AppendLine("Select 1 From TblCompanyBudgetPlanHdr ");
            SQL.AppendLine("Where Yr=@Yr ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And CCCode Is Not Null And CCCode=@CCCode ");
            SQL.AppendLine("And Doctype=@DocType");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            if (mIsCompanyBudgetPlanBalanceSheet) Sm.CmParam<String>(ref cm, "@DocType", "3");
            else Sm.CmParam<String>(ref cm, "@DocType", "2");

            if (Sm.IsDataExist(cm))
            {
                var Msg = string.Empty;

                Msg = ("Year : " + Sm.GetLue(LueYr) + Environment.NewLine);
                if (CCCode.Length > 0) Msg += ("Cost Center : " + LueCCCode.GetColumnValue("Col2") + Environment.NewLine);
                Msg += (Environment.NewLine + "Data has been created.");

                Sm.StdMsg(mMsgType.Warning, Msg);
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private void SaveCompanyBudgetPlanHdr(ref MySqlCommand cm, ref StringBuilder SQL)
        {
            var subSQL = new StringBuilder();

            GetCCAcNo(ref cm, ref subSQL);

            //doctype 2 = Profit Loss, 3= Blance Sheet
            SQL.AppendLine("Insert Into TblCompanyBudgetPlanHdr(DocNo, DocNoTemp, DocDt, Yr, CancelInd, CCCode, ");
            SQL.AppendLine("DocType, CompletedInd, BudgetRequestDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocNoTemp, @1_DocDt, @1_Yr, 'N', @1_CCCode, ");
            SQL.AppendLine("@1_DocType, @1_CompletedInd, @1_BudgetRequestDocNo, @1_Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl(DocNo, AcNo, Amt, Amt01, Amt02, Amt03, Amt04, Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, Amt11, Amt12, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, AcNo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @UserCode, @Dt ");
            SQL.AppendLine("From TblCOA ");
            if (mIsCompanyBudgetPlanProfitLoss)
                SQL.AppendLine("Where Left(AcNo, 1) > 3 ");
            else
                SQL.AppendLine("Where Left(AcNo, 1) Between 1 AND 3 ");
            SQL.AppendLine(subSQL.ToString());
            SQL.AppendLine("; ");

            int i = 0;
            foreach (var x in ml)
            {
                SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl2(DocNo, AcNo, ItCode, Rate01, Rate02, Rate03, Rate04, Rate05, Rate06, Rate07, Rate08, Rate09, Rate10, Rate11, Rate12, Qty01, Qty02, Qty03, Qty04, Qty05, Qty06, Qty07, Qty08, Qty09, Qty10, Qty11, Qty12, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                SQL.AppendLine("Select DocNo, @1_AcNo_" + i.ToString() +
                    ", @1_ItCode_" + i.ToString() +
                    ", @1_Rate01_" + i.ToString() +
                    ", @1_Rate02_" + i.ToString() +
                    ", @1_Rate03_" + i.ToString() +
                    ", @1_Rate04_" + i.ToString() +
                    ", @1_Rate05_" + i.ToString() +
                    ", @1_Rate06_" + i.ToString() +
                    ", @1_Rate07_" + i.ToString() +
                    ", @1_Rate08_" + i.ToString() +
                    ", @1_Rate09_" + i.ToString() +
                    ", @1_Rate10_" + i.ToString() +
                    ", @1_Rate11_" + i.ToString() +
                    ", @1_Rate12_" + i.ToString() +
                    ", @1_Qty01_" + i.ToString() +
                    ", @1_Qty02_" + i.ToString() +
                    ", @1_Qty03_" + i.ToString() +
                    ", @1_Qty04_" + i.ToString() +
                    ", @1_Qty05_" + i.ToString() +
                    ", @1_Qty06_" + i.ToString() +
                    ", @1_Qty07_" + i.ToString() +
                    ", @1_Qty08_" + i.ToString() +
                    ", @1_Qty09_" + i.ToString() +
                    ", @1_Qty10_" + i.ToString() +
                    ", @1_Qty11_" + i.ToString() +
                    ", @1_Qty12_" + i.ToString() +
                    ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
                SQL.AppendLine("From TblCompanyBudgetPlanHdr ");
                SQL.AppendLine("Where DocNo=@DocNo; ");

                Sm.CmParam<String>(ref cm, "@1_AcNo_" + i, x.AcNo);
                Sm.CmParam<String>(ref cm, "@1_ItCode_" + i, x.ItCode);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate01_" + i, x.Rate01);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate02_" + i, x.Rate02);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate03_" + i, x.Rate03);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate04_" + i, x.Rate04);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate05_" + i, x.Rate05);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate06_" + i, x.Rate06);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate07_" + i, x.Rate07);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate08_" + i, x.Rate08);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate09_" + i, x.Rate09);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate10_" + i, x.Rate10);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate11_" + i, x.Rate11);
                Sm.CmParam<Decimal>(ref cm, "@1_Rate12_" + i, x.Rate12);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty01_" + i, x.Qty01);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty02_" + i, x.Qty02);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty03_" + i, x.Qty03);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty04_" + i, x.Qty04);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty05_" + i, x.Qty05);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty06_" + i, x.Qty06);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty07_" + i, x.Qty07);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty08_" + i, x.Qty08);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty09_" + i, x.Qty09);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty10_" + i, x.Qty10);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty11_" + i, x.Qty11);
                Sm.CmParam<Decimal>(ref cm, "@1_Qty12_" + i, x.Qty12);

                i++;
            }

            Sm.CmParamDt(ref cm, "@1_DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@1_Yr", Sm.GetLue(LueYr));
            if (mIsCompanyBudgetPlanBalanceSheet) 
                Sm.CmParam<String>(ref cm, "@1_DocType", "3");
            else 
                Sm.CmParam<String>(ref cm, "@1_DocType", "2");
            Sm.CmParam<String>(ref cm, "@1_Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@1_CompletedInd", ChkCompletedInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@1_CCCode", Sm.GetLue(LueCCCode));
        }

        private void SaveCompanyBudgetPlanDtl(ref MySqlCommand cm, ref StringBuilder SQL, int r)
        {
            SQL.AppendLine("Update TblCompanyBudgetPlanDtl Set ");
            SQL.AppendLine("    Amt=@2_Amt_" + r.ToString());
            SQL.AppendLine("    ,Amt01=@2_Amt01_" + r.ToString());
            SQL.AppendLine("    ,Amt02=@2_Amt02_" + r.ToString());
            SQL.AppendLine("    ,Amt03=@2_Amt03_" + r.ToString());
            SQL.AppendLine("    ,Amt04=@2_Amt04_" + r.ToString());
            SQL.AppendLine("    ,Amt05=@2_Amt05_" + r.ToString());
            SQL.AppendLine("    ,Amt06=@2_Amt06_" + r.ToString());
            SQL.AppendLine("    ,Amt07=@2_Amt07_" + r.ToString());
            SQL.AppendLine("    ,Amt08=@2_Amt08_" + r.ToString());
            SQL.AppendLine("    ,Amt09=@2_Amt09_" + r.ToString());
            SQL.AppendLine("    ,Amt10=@2_Amt10_" + r.ToString());
            SQL.AppendLine("    ,Amt11=@2_Amt11_" + r.ToString());
            SQL.AppendLine("    ,Amt12=@2_Amt12_" + r.ToString());
            SQL.AppendLine("Where DocNo=@DocNo And AcNo=@2_AcNo_" + r.ToString() + "; ");

            Sm.CmParam<String>(ref cm, "@2_AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt01_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt02_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt03_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt04_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 12));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt05_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 14));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt06_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 16));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt07_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt08_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt09_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 22));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt10_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 24));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt11_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 26));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt12_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 28));
        }

        private void SaveBudgetRequestYearly(ref MySqlCommand cm, ref StringBuilder SQL)
        {
            SQL.AppendLine("Update TblCompanyBudgetPlanHdr Set ");
            SQL.AppendLine("    BudgetRequestDocNo = @BudgetRequestDocNo ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            SQL.AppendLine("Insert Into TblBudgetRequestYearlyHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Yr, CCCode, Amt, BudgetDocNo, Remark, Status, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @BudgetRequestDocNo, DocDt, 'N', Yr, CCCode, ");
            SQL.AppendLine("(Select Sum(T2.Amt) ");
            SQL.AppendLine(" From TblCompanyBudgetPlanHdr T1 ");
            SQL.AppendLine(" Inner Join TblCompanyBudgetPlanDtl T2 On T1.DocNo=T2.DocNo And T2.AcNo Not In (Select Parent from TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine(" Where T1.DocNo=@DocNo And T1.CancelInd='N'), ");
            SQL.AppendLine("Null, Concat('RKAP : ', @DocNo), 'O', @UserCode, @Dt ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @BudgetRequestDocNo, '001', T.DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='BudgetRequestYearly' ");
            SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode In (Select DeptCode From TblCostCenter Where CCCode= @CCCode) ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*1 ");
            SQL.AppendLine("    From TblBudgetRequestYearlyHdr A ");
            SQL.AppendLine("    Where A.DocNo=@BudgetRequestDocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblBudgetRequestYearlyHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo=@BudgetRequestDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='BudgetRequestYearly' ");
            SQL.AppendLine("    And DocNo=@BudgetRequestDocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Set @Row := 0;");

            SQL.AppendLine("Insert Into TblBudgetRequestYearlyDtl ");
            SQL.AppendLine("(DocNo, BCCode, SeqNo, Amt01, Amt02, Amt03, Amt04, Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, Amt11, Amt12, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @BudgetRequestDocNo,  ");
            SQL.AppendLine("C.BCCode, ");
            SQL.AppendLine("Right(Concat('0000000', Cast((@row:=@row+1) As Char(8))), 8) As SeqNo, ");
            SQL.AppendLine("Sum(A.Amt01) Amt01, Sum(A.Amt02) Amt02, Sum(A.Amt03) Amt03, Sum(A.Amt04) Amt04, Sum(A.Amt05) Amt05, Sum(A.Amt06) Amt06, Sum(A.Amt07) Amt07, Sum(A.Amt08) Amt08, Sum(A.Amt09) Amt09, Sum(A.Amt10) Amt10, Sum(A.Amt11) Amt11, Sum(A.Amt12) Amt12, Concat('RKAP : ', @DocNo) Remark, @UserCde, @Dt ");
            SQL.AppendLine("From TblCompanyBudgetPlanDtl A ");
            SQL.AppendLine("Inner Join TblCompanyBudgetPlanHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBudgetCategory C On B.CCCode = C.CCCode ");
            SQL.AppendLine("Inner Join TblCostCategory D On C.CCtCode = D.CCtCode And A.AcNo = D.AcNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Group By B.DocNo, C.BCCode; ");

            SQL.AppendLine("Insert Into TblBudgetRequestYearlyDtl2 ");
            SQL.AppendLine("(DocNo, BCCode, ItCode, Rate01, Rate02, Rate03, Rate04, Rate05, Rate06, Rate07, Rate08, Rate09, Rate10, Rate11, Rate12, Qty01, Qty02, Qty03, Qty04, Qty05, Qty06, Qty07, Qty08, Qty09, Qty10, Qty11, Qty12, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @BudgetRequestDocNo, C.BCCode, ");
            SQL.AppendLine("A.ItCode, Sum(A.Rate01) Rate01, Sum(A.Rate02) Rate02, Sum(A.Rate03) Rate03, Sum(A.Rate04) Rate04, Sum(A.Rate05) Rate05, Sum(A.Rate06) Rate06, Sum(A.Rate07) Rate07, Sum(A.Rate08) Rate08, Sum(A.Rate09) Rate09, Sum(A.Rate10) Rate10, Sum(A.Rate11) Rate11, Sum(A.Rate12) Rate12, ");
            SQL.AppendLine("Sum(A.Qty01) Qty01, Sum(A.Qty02) Qty02, Sum(A.Qty03) Qty03, Sum(A.Qty04) Qty04, Sum(A.Qty05) Qty05, Sum(A.Qty06) Qty06, Sum(A.Qty07) Qty07, Sum(A.Qty08) Qty08, Sum(A.Qty09) Qty09, Sum(A.Qty10) Qty10, Sum(A.Qty11) Qty11, Sum(A.Qty12) Qty12, @UserCode, @Dt ");
            SQL.AppendLine("From TblCompanyBudgetPlanDtl2 A ");
            SQL.AppendLine("Inner Join TblCompanyBudgetPlanHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBudgetCategory C On B.CCCode = C.CCCode ");
            SQL.AppendLine("Inner JOin TblCostCategory D On C.CCtCode = D.CCtCode And A.AcNo = D.AcNo ");
            SQL.AppendLine("Where A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("Group By B.DocNo, C.BCCode, A.ItCode; ");

            SQL.AppendLine("Update TblBudgetRequestYearlyHdr Set CancelInd='Y', CancelReason='Auto cancelled by system.' ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And Yr=@3_Yr ");
            SQL.AppendLine("And CCCode=@3_CCCode ");
            SQL.AppendLine("And DocNo <> @BudgetRequestDocNo ");
            SQL.AppendLine("And BudgetDocNo Is Null ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='BudgetRequestYearly' ");
            SQL.AppendLine("    And DocNo=@BudgetRequestDocNo ");
            SQL.AppendLine("); ");

            Sm.CmParam<String>(ref cm, "@3_Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@3_CCCode", Sm.GetLue(LueCCCode));
        }

        #endregion

        #region Edit Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();


            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
            if (!ChkCancelInd.Checked && ChkCompletedInd.Checked)
                SQL.AppendLine("Set @BudgetRequestDocNo=" + Sm.GetNewDocNo(Sm.GetDte(DteDocDt), "BudgetRequest", "TblBudgetRequestYearlyHdr", "1") + "; ");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            EditCompanyBudgetPlanHdr(ref cm, ref SQL);
            if (!ChkCancelInd.Checked)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (
                        Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
                        Sm.GetGrdDec(Grd1, Row, 4) != Sm.GetGrdDec(Grd1, Row, 30)
                        )
                        EditCompanyBudgetPlanDtl(ref cm, ref SQL, Row);

                if (ChkCompletedInd.Checked)
                    SaveBudgetRequestYearly(ref cm, ref SQL);
            }

            cm.CommandText = SQL.ToString();
            
            cml.Add(cm);
            Sm.ExecCommands(cml);


            ShowData(TxtDocNo.Text);
        }

        //private void UpdateData()
        //{
        //    if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

        //    Cursor.Current = Cursors.WaitCursor;

        //    var cml = new List<MySqlCommand>();

        //    cml.Add(EditCompanyBudgetPlanHdr());
        //    if (!ChkCancelInd.Checked)
        //    {
        //        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //            if (
        //                Sm.GetGrdStr(Grd1, Row, 0).Length > 0 &&
        //                Sm.GetGrdDec(Grd1, Row, 4) != Sm.GetGrdDec(Grd1, Row, 30)
        //                )
        //                cml.Add(EditCompanyBudgetPlanDtl(Row));

        //        if (ChkCompletedInd.Checked)
        //        {
        //            string BudgetRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetRequest", "TblBudgetRequestYearlyHdr");
        //            cml.Add(SaveBudgetRequestYearly(TxtDocNo.Text, BudgetRequestDocNo));
        //        }
        //    }

        //    Sm.ExecCommands(cml);

        //    ShowData(TxtDocNo.Text);
        //}

        private void EditCompanyBudgetPlanHdr(ref MySqlCommand cm, ref StringBuilder SQL)
        {
            SQL.AppendLine("Update TblCompanyBudgetPlanHdr Set ");
            SQL.AppendLine("   CancelInd=@1_CancelInd, ");
            if (!ChkCancelInd.Checked && ChkCompletedInd.Checked)
            {
                SQL.AppendLine("   CompletedInd=@1_CompletedInd, ");
                SQL.AppendLine("   BudgetRequestDocNo=@BudgetRequestDocNo, ");
            }
            SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=@Dt ");
            SQL.AppendLine("Where DocNo=@DocNo; ");


            if (!ChkCancelInd.Checked)
            {
                SQL.AppendLine("Delete From TblCompanyBudgetPlanDtl2 Where DocNo=@DocNo; ");
                int i = 0;
                foreach (var x in ml)
                {
                    SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl2(DocNo, AcNo, ItCode, Rate01, Rate02, Rate03, Rate04, Rate05, Rate06, Rate07, Rate08, Rate09, Rate10, Rate11, Rate12, Qty01, Qty02, Qty03, Qty04, Qty05, Qty06, Qty07, Qty08, Qty09, Qty10, Qty11, Qty12, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                    SQL.AppendLine(
                        "Select DocNo, @1_AcNo_" + i.ToString() +
                        ", @1_ItCode_" + i.ToString() +
                        ", @1_Rate01_" + i.ToString() +
                        ", @1_Rate02_" + i.ToString() +
                        ", @1_Rate03_" + i.ToString() +
                        ", @1_Rate04_" + i.ToString() +
                        ", @1_Rate05_" + i.ToString() +
                        ", @1_Rate06_" + i.ToString() +
                        ", @1_Rate07_" + i.ToString() +
                        ", @1_Rate08_" + i.ToString() +
                        ", @1_Rate09_" + i.ToString() +
                        ", @1_Rate10_" + i.ToString() +
                        ", @1_Rate11_" + i.ToString() +
                        ", @1_Rate12_" + i.ToString() +
                        ", @1_Qty01_" + i.ToString() +
                        ", @1_Qty02_" + i.ToString() +
                        ", @1_Qty03_" + i.ToString() +
                        ", @1_Qty04_" + i.ToString() +
                        ", @1_Qty05_" + i.ToString() +
                        ", @1_Qty06_" + i.ToString() +
                        ", @1_Qty07_" + i.ToString() +
                        ", @1_Qty08_" + i.ToString() +
                        ", @1_Qty09_" + i.ToString() +
                        ", @1_Qty10_" + i.ToString() +
                        ", @1_Qty11_" + i.ToString() +
                        ", @1_Qty12_" + i.ToString() +
                        ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
                    SQL.AppendLine("From TblCompanyBudgetPlanHdr ");
                    SQL.AppendLine("Where DocNo=@DocNo; ");

                    Sm.CmParam<String>(ref cm, "@1_AcNo_" + i, x.AcNo);
                    Sm.CmParam<String>(ref cm, "@1_ItCode_" + i, x.ItCode);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate01_" + i, x.Rate01);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate02_" + i, x.Rate02);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate03_" + i, x.Rate03);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate04_" + i, x.Rate04);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate05_" + i, x.Rate05);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate06_" + i, x.Rate06);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate07_" + i, x.Rate07);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate08_" + i, x.Rate08);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate09_" + i, x.Rate09);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate10_" + i, x.Rate10);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate11_" + i, x.Rate11);
                    Sm.CmParam<Decimal>(ref cm, "@1_Rate12_" + i, x.Rate12);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty01_" + i, x.Qty01);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty02_" + i, x.Qty02);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty03_" + i, x.Qty03);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty04_" + i, x.Qty04);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty05_" + i, x.Qty05);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty06_" + i, x.Qty06);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty07_" + i, x.Qty07);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty08_" + i, x.Qty08);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty09_" + i, x.Qty09);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty10_" + i, x.Qty10);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty11_" + i, x.Qty11);
                    Sm.CmParam<Decimal>(ref cm, "@1_Qty12_" + i, x.Qty12);

                    ++i;
                }
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@1_CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@1_CompletedInd", ChkCompletedInd.Checked ? "Y" : "N");
        }

        private void EditCompanyBudgetPlanDtl(ref MySqlCommand cm, ref StringBuilder SQL, int r)
        {
            SQL.AppendLine("Update TblCompanyBudgetPlanDtl Set ");
            SQL.AppendLine("    Amt=@2_Amt_" + r.ToString());
            SQL.AppendLine("    ,Amt01=@2_Amt01_" + r.ToString());
            SQL.AppendLine("    ,Amt02=@2_Amt02_" + r.ToString());
            SQL.AppendLine("    ,Amt03=@2_Amt03_" + r.ToString());
            SQL.AppendLine("    ,Amt04=@2_Amt04_" + r.ToString());
            SQL.AppendLine("    ,Amt05=@2_Amt05_" + r.ToString());
            SQL.AppendLine("    ,Amt06=@2_Amt06_" + r.ToString());
            SQL.AppendLine("    ,Amt07=@2_Amt07_" + r.ToString());
            SQL.AppendLine("    ,Amt08=@2_Amt08_" + r.ToString());
            SQL.AppendLine("    ,Amt09=@2_Amt09_" + r.ToString());
            SQL.AppendLine("    ,Amt10=@2_Amt10_" + r.ToString());
            SQL.AppendLine("    ,Amt11=@2_Amt11_" + r.ToString());
            SQL.AppendLine("    ,Amt12=@2_Amt12_" + r.ToString());
            SQL.AppendLine("    ,LastUpBy = @UserCode, LastUpDt=@Dt ");
            SQL.AppendLine("Where DocNo=@DocNo And AcNo=@2_AcNo_" + r.ToString() + "; ");

            Sm.CmParam<String>(ref cm, "@2_AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt01_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt02_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt03_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt04_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 12));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt05_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 14));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt06_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 16));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt07_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt08_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt09_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 22));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt10_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 24));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt11_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 26));
            Sm.CmParam<Decimal>(ref cm, "@2_Amt12_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 28));
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocAlreadyCancelled() ||
                IsBudgetRequestAlreadyExisted()
                ;
        }

        private bool IsBudgetRequestAlreadyExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("Inner JOin TblBudgetRequestYearlyHdr B On A.BudgetRequestDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @Param ");
            SQL.AppendLine("    And A.BudgetRequestDocNo Is Not Null ");
            SQL.AppendLine("    And B.CancelInd = 'N' And B.Status In ('O', 'A') ");
            SQL.AppendLine("; ");

            return Sm.IsDataExist(
                SQL.ToString(),
                TxtDocNo.Text,
                "This document already processed to budget request.");
        }
     
        private bool IsDocAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblCompanyBudgetPlanHdr Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text, 
                "This document already cancelled.");
        }

        #region Old Code

        //private MySqlCommand EditCompanyBudgetPlanHdr()
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Update TblCompanyBudgetPlanHdr Set ");
        //    SQL.AppendLine("   CancelInd=@CancelInd, ");
        //    if (!ChkCancelInd.Checked && ChkCompletedInd.Checked)
        //    {
        //        SQL.AppendLine("   CompletedInd=@CompletedInd, ");
        //        SQL.AppendLine("   BudgetRequestDocNo=@BudgetRequestDocNo, ");
        //    }
        //    SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
        //    SQL.AppendLine("Where DocNo=@DocNo; ");


        //    if (!ChkCancelInd.Checked)
        //    {
        //        SQL.AppendLine("Delete From TblCompanyBudgetPlanDtl2 Where DocNo=@DocNo; ");
        //        int i = 0;
        //        foreach (var x in ml)
        //        {
        //            SQL.AppendLine("Insert Into TblCompanyBudgetPlanDtl2(DocNo, AcNo, ItCode, Rate01, Rate02, Rate03, Rate04, Rate05, Rate06, Rate07, Rate08, Rate09, Rate10, Rate11, Rate12, Qty01, Qty02, Qty03, Qty04, Qty05, Qty06, Qty07, Qty08, Qty09, Qty10, Qty11, Qty12, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
        //            SQL.AppendLine("Select DocNo, @AcNo_" + i.ToString() +
        //                ", @ItCode_" + i.ToString() +
        //                ", @Rate01_" + i.ToString() +
        //                ", @Rate02_" + i.ToString() +
        //                ", @Rate03_" + i.ToString() +
        //                ", @Rate04_" + i.ToString() +
        //                ", @Rate05_" + i.ToString() +
        //                ", @Rate06_" + i.ToString() +
        //                ", @Rate07_" + i.ToString() +
        //                ", @Rate08_" + i.ToString() +
        //                ", @Rate09_" + i.ToString() +
        //                ", @Rate10_" + i.ToString() +
        //                ", @Rate11_" + i.ToString() +
        //                ", @Rate12_" + i.ToString() +
        //                ", @Qty01_" + i.ToString() +
        //                ", @Qty02_" + i.ToString() +
        //                ", @Qty03_" + i.ToString() +
        //                ", @Qty04_" + i.ToString() +
        //                ", @Qty05_" + i.ToString() +
        //                ", @Qty06_" + i.ToString() +
        //                ", @Qty07_" + i.ToString() +
        //                ", @Qty08_" + i.ToString() +
        //                ", @Qty09_" + i.ToString() +
        //                ", @Qty10_" + i.ToString() +
        //                ", @Qty11_" + i.ToString() +
        //                ", @Qty12_" + i.ToString() +
        //                ", CreateBy, CreateDt, LastUpBy, LastUpDt ");
        //            SQL.AppendLine("From TblCompanyBudgetPlanHdr ");
        //            SQL.AppendLine("Where DocNo=@DocNo; ");

        //            Sm.CmParam<String>(ref cm, "@AcNo_" + i, x.AcNo);
        //            Sm.CmParam<String>(ref cm, "@ItCode_" + i, x.ItCode);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate01_" + i, x.Rate01);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate02_" + i, x.Rate02);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate03_" + i, x.Rate03);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate04_" + i, x.Rate04);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate05_" + i, x.Rate05);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate06_" + i, x.Rate06);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate07_" + i, x.Rate07);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate08_" + i, x.Rate08);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate09_" + i, x.Rate09);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate10_" + i, x.Rate10);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate11_" + i, x.Rate11);
        //            Sm.CmParam<Decimal>(ref cm, "@Rate12_" + i, x.Rate12);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty01_" + i, x.Qty01);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty02_" + i, x.Qty02);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty03_" + i, x.Qty03);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty04_" + i, x.Qty04);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty05_" + i, x.Qty05);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty06_" + i, x.Qty06);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty07_" + i, x.Qty07);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty08_" + i, x.Qty08);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty09_" + i, x.Qty09);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty10_" + i, x.Qty10);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty11_" + i, x.Qty11);
        //            Sm.CmParam<Decimal>(ref cm, "@Qty12_" + i, x.Qty12);

        //            ++i;
        //        }
        //    }

        //    #region Old Code
        //    //if (!ChkCancelInd.Checked && ChkCompletedInd.Checked)
        //    //{
        //    //    SQL.AppendLine("Insert Into TblBudgetRequestHdr ");
        //    //    SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Yr, Mth, DeptCode, Amt, BudgetDocNo, Remark, Status, EntCode, CreateBy, CreateDt) ");
        //    //    SQL.AppendLine("Values(@BudgetRequestDocNo, @DocDt, Null, 'N', @Yr, '00', ");
        //    //    SQL.AppendLine("(Select DeptCode From TblCostCenter Where CCCode=@CCCode), ");
        //    //    SQL.AppendLine("(Select Sum(B.Amt) ");
        //    //    SQL.AppendLine(" From TblCompanyBudgetPlanHdr A ");
        //    //    SQL.AppendLine(" Inner Join TblCompanyBudgetPlanDtl B On A.DocNo=B.DocNo And B.AcNo Not In (Select Parent from TblCOA Where Parent Is Not Null) ");
        //    //    SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd='N'), ");
        //    //    SQL.AppendLine("Null, Concat('RKAP : ', @DocNo), 'O', ");
        //    //    SQL.AppendLine("(Select EntCode From TblCostCenter A, TblProfitCenter B Where A.ProfitCenterCode=B.ProfitCenterCode And A.CCCode=@CCCode), ");
        //    //    SQL.AppendLine("@UserCode, CurrentDateTime()); ");

        //    //    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //    //    SQL.AppendLine("Select T.DocType, @BudgetRequestDocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
        //    //    SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BudgetRequest2' ");
        //    //    SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode=@DeptCode ");
        //    //    SQL.AppendLine("And (T.StartAmt=0 ");
        //    //    SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
        //    //    SQL.AppendLine("    Select A.Amt*1 ");
        //    //    SQL.AppendLine("    From TblBudgetRequestHdr A ");
        //    //    SQL.AppendLine("    Where A.DocNo=@BudgetRequestDocNo ");
        //    //    SQL.AppendLine("), 0)); ");

        //    //    SQL.AppendLine("Update TblBudgetRequestHdr Set Status = 'A' ");
        //    //    SQL.AppendLine("Where DocNo=@BudgetRequestDocNo ");
        //    //    SQL.AppendLine("And Not Exists( ");
        //    //    SQL.AppendLine("    Select 1 From TblDocApproval ");
        //    //    SQL.AppendLine("    Where DocType='BudgetRequest2' ");
        //    //    SQL.AppendLine("    And DocNo=@BudgetRequestDocNo ");
        //    //    SQL.AppendLine("); ");

        //    //    SQL.AppendLine("Set @Row:=0; ");

        //    //    SQL.AppendLine("Insert Into TblBudgetRequestDtl ");
        //    //    SQL.AppendLine("(DocNo, DNo, BCCode, Amt, ItCode, Qty, Remark, CreateBy, CreateDt) ");
        //    //    SQL.AppendLine("Select @BudgetRequestDocNo, ");
        //    //    SQL.AppendLine("Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //    //    SQL.AppendLine("BCCode, Amt, Null, 0.00, Null, @UserCode, CurrentDateTime() ");
        //    //    SQL.AppendLine("From ( ");
        //    //    SQL.AppendLine("    Select E.BCCode, Sum(B.Amt) As Amt ");
        //    //    SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
        //    //    SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo=B.DocNo And B.AcNo Not In (Select Parent from TblCOA Where Parent Is Not Null) ");
        //    //    //SQL.AppendLine("    Inner Join TblCostCategory C On A.CCCode=C.CCCode And B.AcNo=C.AcNo And C.AcNo Is Not Null ");
        //    //    //SQL.AppendLine("    Inner Join TblCostCenter D On A.CCCode=D.CCCode And D.DeptCode Is Not Null ");
        //    //    //SQL.AppendLine("    Inner Join TblBudgetCategory E On C.CCtCode=E.CCtCode And D.DeptCode=E.DeptCode And E.ActInd='Y' ");
        //    //    SQL.AppendLine("    Inner Join TblBudgetCategory E On A.CCCode=E.CCCode And B.AcNo=E.AcNo And E.ActInd='Y' ");
        //    //    SQL.AppendLine("    Where A.CancelInd='N' ");
        //    //    SQL.AppendLine("    And A.DocNo=@DocNo ");
        //    //    SQL.AppendLine("    Group By E.BCCode ");
        //    //    SQL.AppendLine(") T Order By BCCode; ");
        //    //}
        //    #endregion

        //    cm.CommandText = SQL.ToString();

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
        //    Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CompletedInd", ChkCompletedInd.Checked ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
        //    if (!ChkCancelInd.Checked && ChkCompletedInd.Checked)
        //        Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BudgetRequest", "TblBudgetRequestHdr"));
        //    else
        //        Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", string.Empty);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
        //    return cm;
        //}

        //private MySqlCommand SaveBudgetRequestYearly(string DocNo, string BudgetRequestDocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblCompanyBudgetPlanHdr ");
        //    SQL.AppendLine("Set BudgetRequestDocNo = @BudgetRequestDocNo ");
        //    SQL.AppendLine("Where DocNo = @DocNo; ");

        //    SQL.AppendLine("Insert Into TblBudgetRequestYearlyHdr ");
        //    SQL.AppendLine("(DocNo, DocDt, CancelInd, Yr, CCCode, Amt, BudgetDocNo, Remark, Status, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @BudgetRequestDocNo, DocDt, 'N', Yr, CCCode, ");
        //    SQL.AppendLine("(Select Sum(T2.Amt) ");
        //    SQL.AppendLine(" From TblCompanyBudgetPlanHdr T1 ");
        //    SQL.AppendLine(" Inner Join TblCompanyBudgetPlanDtl T2 On T1.DocNo=T2.DocNo And T2.AcNo Not In (Select Parent from TblCOA Where Parent Is Not Null) ");
        //    SQL.AppendLine(" Where T1.DocNo=@DocNo And T1.CancelInd='N'), ");
        //    SQL.AppendLine("Null, Concat('RKAP : ', @DocNo), 'O', @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblCompanyBudgetPlanHdr ");
        //    SQL.AppendLine("Where DocNo = @DocNo; ");

        //    SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select T.DocType, @BudgetRequestDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BudgetRequestYearly' ");
        //    SQL.AppendLine("And T.DeptCode Is Not Null And T.DeptCode In (Select DeptCode From TblCostCenter Where CCCode= @CCCode) ");
        //    SQL.AppendLine("And (T.StartAmt=0 ");
        //    SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
        //    SQL.AppendLine("    Select A.Amt*1 ");
        //    SQL.AppendLine("    From TblBudgetRequestYearlyHdr A ");
        //    SQL.AppendLine("    Where A.DocNo=@BudgetRequestDocNo ");
        //    SQL.AppendLine("), 0)); ");

        //    SQL.AppendLine("Update TblBudgetRequestYearlyHdr Set Status = 'A' ");
        //    SQL.AppendLine("Where DocNo=@BudgetRequestDocNo ");
        //    SQL.AppendLine("And Not Exists( ");
        //    SQL.AppendLine("    Select 1 From TblDocApproval ");
        //    SQL.AppendLine("    Where DocType='BudgetRequestYearly' ");
        //    SQL.AppendLine("    And DocNo=@BudgetRequestDocNo ");
        //    SQL.AppendLine("); ");

        //    SQL.AppendLine("Set @Row := 0;");

        //    SQL.AppendLine("Insert Into TblBudgetRequestYearlyDtl ");
        //    SQL.AppendLine("(DocNo, BCCode, SeqNo, Amt01, Amt02, Amt03, Amt04, Amt05, Amt06, Amt07, Amt08, Amt09, Amt10, Amt11, Amt12, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @BudgetRequestDocNo,  ");
        //    SQL.AppendLine("C.BCCode, ");
        //    SQL.AppendLine("Right(Concat('0000000', Cast((@row:=@row+1) As Char(8))), 8) As SeqNo, ");
        //    SQL.AppendLine("Sum(A.Amt01) Amt01, Sum(A.Amt02) Amt02, Sum(A.Amt03) Amt03, Sum(A.Amt04) Amt04, Sum(A.Amt05) Amt05, Sum(A.Amt06) Amt06, Sum(A.Amt07) Amt07, Sum(A.Amt08) Amt08, Sum(A.Amt09) Amt09, Sum(A.Amt10) Amt10, Sum(A.Amt11) Amt11, Sum(A.Amt12) Amt12, COncat('RKAP : ', @DocNo) Remark, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblCompanyBudgetPlanDtl A ");
        //    SQL.AppendLine("Inner Join TblCompanyBudgetPlanHdr B On A.DocNo = B.DocNo ");
        //    SQL.AppendLine("    And A.DocNo = @DocNo ");
        //    SQL.AppendLine("    And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
        //    SQL.AppendLine("Inner Join TblBudgetCategory C On B.CCCode = C.CCCode ");
        //    SQL.AppendLine("Inner Join TblCostCategory D On C.CCtCode = D.CCtCode And A.AcNo = D.AcNo ");
        //    SQL.AppendLine("Group By B.DocNo, C.BCCode; ");

        //    SQL.AppendLine("Insert Into TblBudgetRequestYearlyDtl2 ");
        //    SQL.AppendLine("(DocNo, BCCode, ItCode, Rate01, Rate02, Rate03, Rate04, Rate05, Rate06, Rate07, Rate08, Rate09, Rate10, Rate11, Rate12, Qty01, Qty02, Qty03, Qty04, Qty05, Qty06, Qty07, Qty08, Qty09, Qty10, Qty11, Qty12, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @BudgetRequestDocNo, C.BCCode, ");
        //    SQL.AppendLine("A.ItCode, Sum(A.Rate01) Rate01, Sum(A.Rate02) Rate02, Sum(A.Rate03) Rate03, Sum(A.Rate04) Rate04, Sum(A.Rate05) Rate05, Sum(A.Rate06) Rate06, Sum(A.Rate07) Rate07, Sum(A.Rate08) Rate08, Sum(A.Rate09) Rate09, Sum(A.Rate10) Rate10, Sum(A.Rate11) Rate11, Sum(A.Rate12) Rate12, ");
        //    SQL.AppendLine("Sum(A.Qty01) Qty01, Sum(A.Qty02) Qty02, Sum(A.Qty03) Qty03, Sum(A.Qty04) Qty04, Sum(A.Qty05) Qty05, Sum(A.Qty06) Qty06, Sum(A.Qty07) Qty07, Sum(A.Qty08) Qty08, Sum(A.Qty09) Qty09, Sum(A.Qty10) Qty10, Sum(A.Qty11) Qty11, Sum(A.Qty12) Qty12, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblCompanyBudgetPlanDtl2 A ");
        //    SQL.AppendLine("Inner Join TblCompanyBudgetPlanHdr B On A.DocNo = B.DocNo ");
        //    SQL.AppendLine("    And B.DocNo = @DocNo ");
        //    SQL.AppendLine("    And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
        //    SQL.AppendLine("Inner Join TblBudgetCategory C On B.CCCode = C.CCCode ");
        //    SQL.AppendLine("Inner JOin TblCostCategory D On C.CCtCode = D.CCtCode And A.AcNo = D.AcNo ");
        //    SQL.AppendLine("Group By B.DocNo, C.BCCode, A.ItCode; ");

        //    SQL.AppendLine("Update TblBudgetRequestYearlyHdr Set CancelInd='Y', CancelReason='Auto cancelled by system.' ");
        //    SQL.AppendLine("Where CancelInd='N' ");
        //    SQL.AppendLine("And Yr=@Yr ");
        //    SQL.AppendLine("And CCCode=@CCCode ");
        //    SQL.AppendLine("And DocNo <> @BudgetRequestDocNo ");
        //    SQL.AppendLine("And BudgetDocNo Is Null ");
        //    SQL.AppendLine("And Not Exists ( ");
        //    SQL.AppendLine("    Select 1 From TblDocApproval ");
        //    SQL.AppendLine("    Where DocType='BudgetRequestYearly' ");
        //    SQL.AppendLine("    And DocNo=@BudgetRequestDocNo ");
        //    SQL.AppendLine("); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
        //    Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
        //    Sm.CmParam<String>(ref cm, "@BudgetRequestDocNo", BudgetRequestDocNo);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;

        //}

        //private MySqlCommand EditCompanyBudgetPlanDtl(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblCompanyBudgetPlanDtl Set ");
        //    SQL.AppendLine("    Amt=@Amt, ");
        //    SQL.AppendLine("    Amt01=@Amt01, ");
        //    SQL.AppendLine("    Amt02=@Amt02, ");
        //    SQL.AppendLine("    Amt03=@Amt03, ");
        //    SQL.AppendLine("    Amt04=@Amt04, ");
        //    SQL.AppendLine("    Amt05=@Amt05, ");
        //    SQL.AppendLine("    Amt06=@Amt06, ");
        //    SQL.AppendLine("    Amt07=@Amt07, ");
        //    SQL.AppendLine("    Amt08=@Amt08, ");
        //    SQL.AppendLine("    Amt09=@Amt09, ");
        //    SQL.AppendLine("    Amt10=@Amt10, ");
        //    SQL.AppendLine("    Amt11=@Amt11, ");
        //    SQL.AppendLine("    Amt12=@Amt12, ");
        //    SQL.AppendLine("    LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
        //    SQL.AppendLine("Where DocNo=@DocNo And AcNo=@AcNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt01", Sm.GetGrdDec(Grd1, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt02", Sm.GetGrdDec(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt03", Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt04", Sm.GetGrdDec(Grd1, Row, 12));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt05", Sm.GetGrdDec(Grd1, Row, 14));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt06", Sm.GetGrdDec(Grd1, Row, 16));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt07", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt08", Sm.GetGrdDec(Grd1, Row, 20));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt09", Sm.GetGrdDec(Grd1, Row, 22));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt10", Sm.GetGrdDec(Grd1, Row, 24));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt11", Sm.GetGrdDec(Grd1, Row, 26));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt12", Sm.GetGrdDec(Grd1, Row, 28));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowCompanyBudgetPlanHdr(DocNo);
                ShowCompanyBudgetPlanDtl2(DocNo);
                ShowAccount();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCompanyBudgetPlanHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, Yr, CancelInd, CCCode, CompletedInd, BudgetRequestDocNo, Remark " +
                    "From TblCompanyBudgetPlanHdr " +
                    "Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Yr", "CancelInd", "CCCode", "CompletedInd", 
                        
                        //6-7
                        "BudgetRequestDocNo", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[4]));
                        ChkCompletedInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");
                        TxtBudgetRequestDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowCompanyBudgetPlanDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.ItCode, B.ItName, B.PurchaseUomCode, ");
            SQL.AppendLine("A.Rate01, A.Rate02, A.Rate03, A.Rate04, A.Rate05, A.Rate06, ");
            SQL.AppendLine("A.Rate07, A.Rate08, A.Rate09, A.Rate10, A.Rate11, A.Rate12, ");
            SQL.AppendLine("A.Qty01, A.Qty02, A.Qty03, A.Qty04, A.Qty05, A.Qty06, ");
            SQL.AppendLine("A.Qty07, A.Qty08, A.Qty09, A.Qty10, A.Qty11, A.Qty12 ");
            SQL.AppendLine("From TblCompanyBudgetPlanDtl2 A, TblItem B ");
            SQL.AppendLine("Where A.ItCode=B.ItCode ");
            SQL.AppendLine("And A.DocNo=@DocNo; ");
            
            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "AcNo", 
                    //1-5
                    "ItCode", "ItName", "PurchaseUomCode", "Rate01", "Rate02",
                    //6-10
                    "Rate03", "Rate04", "Rate05", "Rate06", "Rate07", 
                    //11-15
                    "Rate08", "Rate09", "Rate10", "Rate11", "Rate12", 
                    //16-20
                    "Qty01", "Qty02", "Qty03", "Qty04", "Qty05", 
                    //21-25
                    "Qty06", "Qty07", "Qty08", "Qty09", "Qty10", 
                    //26-27
                    "Qty11", "Qty12"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ml.Add(new AcItem()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            ItCode = Sm.DrStr(dr, c[1]),
                            ItName = Sm.DrStr(dr, c[2]),
                            PurchaseUomCode = Sm.DrStr(dr, c[3]),
                            Rate01 = Sm.DrDec(dr, c[4]),
                            Rate02 = Sm.DrDec(dr, c[5]),
                            Rate03 = Sm.DrDec(dr, c[6]),
                            Rate04 = Sm.DrDec(dr, c[7]),
                            Rate05 = Sm.DrDec(dr, c[8]),
                            Rate06 = Sm.DrDec(dr, c[9]),
                            Rate07 = Sm.DrDec(dr, c[10]),
                            Rate08 = Sm.DrDec(dr, c[11]),
                            Rate09 = Sm.DrDec(dr, c[12]),
                            Rate10 = Sm.DrDec(dr, c[13]),
                            Rate11 = Sm.DrDec(dr, c[14]),
                            Rate12 = Sm.DrDec(dr, c[15]),
                            Qty01 = Sm.DrDec(dr, c[16]),
                            Qty02 = Sm.DrDec(dr, c[17]),
                            Qty03 = Sm.DrDec(dr, c[18]),
                            Qty04 = Sm.DrDec(dr, c[19]),
                            Qty05 = Sm.DrDec(dr, c[20]),
                            Qty06 = Sm.DrDec(dr, c[21]),
                            Qty07 = Sm.DrDec(dr, c[22]),
                            Qty08 = Sm.DrDec(dr, c[23]),
                            Qty09 = Sm.DrDec(dr, c[24]),
                            Qty10 = Sm.DrDec(dr, c[25]),
                            Qty11 = Sm.DrDec(dr, c[26]),
                            Qty12 = Sm.DrDec(dr, c[27])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private bool IsDocAlreadyCompleted()
        {
            return Sm.IsDataExist("Select 1 From TblCompanyBudgetPlanHdr Where DocNo=@Param And CompletedInd='Y';", TxtDocNo.Text);
        }

        //private decimal GetTotalAmt()
        //{
        //    var v = 0m;
        //    for (int r = 0; r < Grd1.Rows.Count; r++)
        //        if (Sm.GetGrdStr(Grd1, r, 0).Length > 0 && Sm.GetGrdDec(Grd1, r, 4) != 0m)
        //            v += Sm.GetGrdDec(Grd1, r, 4);
        //    return v;
        //}

        private void GetParameter()
        {
            mIsCompanyBudgetPlanBalanceSheet = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForCompanyBudgetPlanBalanceSheet"));
            mIsCompanyBudgetPlanProfitLoss = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForCompanyBudgetPlanProfitLoss"));
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
        }

        internal void ComputeAmt(int Row, int Col, int[] Cols)
        {
            int r = Row;
            var AcNo = Sm.GetGrdStr(Grd1, r, 1);
            var Amt = Sm.GetGrdDec(Grd1, r, Col);
            var AmtPrev = Sm.GetGrdDec(Grd1, r, (Col + 1));
            Grd1.Cells[r, (Col + 1)].Value = Amt;
            var l = new List<string>();
            var Pos = 1;

            while (Pos > 0)
            {
                Pos = AcNo.LastIndexOf(".");
                if (Pos > 0)
                {
                    AcNo = AcNo.Substring(0, Pos);
                    l.Add(AcNo);
                }
            }

            //calculate row's total
            decimal mTotalR = 0m;
            for (int i = 0; i < Cols.Length; ++i)
            {
                mTotalR += Sm.GetGrdDec(Grd1, Row, Cols[i]);
            }

            Grd1.Cells[Row, 4].Value = mTotalR;
            Grd1.Cells[Row, 5].Value = Sm.GetGrdDec(Grd1, Row, 5);

            // calculate parent
            foreach (var x in l)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, i, 1), x))
                    {
                        decimal mTotalP = 0m;

                        Grd1.Cells[i, Col].Value = Sm.GetGrdDec(Grd1, i, Col) - AmtPrev + Amt;
                        Grd1.Cells[i, (Col + 1)].Value = Sm.GetGrdDec(Grd1, i, (Col + 1));

                        for (int k = 0; k < Cols.Length; ++k)
                        {
                            mTotalP += Sm.GetGrdDec(Grd1, i, Cols[k]);
                        }

                        Grd1.Cells[i, 4].Value = mTotalP;
                        Grd1.Cells[i, 5].Value = Sm.GetGrdDec(Grd1, i, 5);
                        break;
                    }
                }
            }
        }

        internal void ComputeActual(int r, int c, ref List<AcRow> l)
        {
            var Amt = Sm.GetGrdDec(Grd1, r, c);
            if (Amt == 0m) return;
            var AcNo = Sm.GetGrdStr(Grd1, r, 1);
            var l2 = new List<string>();
            var Pos = 1;
            while (Pos > 0)
            {
                Pos = AcNo.LastIndexOf(".");
                if (Pos > 0)
                {
                    AcNo = AcNo.Substring(0, Pos);
                    l2.Add(AcNo);
                }
            }

            // calculate parent
            foreach (var i in l2.OrderBy(o=>o))
            {
                foreach (var j in l.Where(w=>Sm.CompareStr(w.AcNo, i)))
                    Grd1.Cells[j.Row, c].Value = Sm.GetGrdDec(Grd1, j.Row, c) + Amt;
            }
        }

        private void ShowAccount()
        {
            int Level = 0;
            int PrevLevel = -1;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var subSQL = new StringBuilder();

            GetCCAcNo(ref cm, ref subSQL);

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    if (TxtDocNo.Text.Length == 0)
                    {
                        SQL.AppendLine("Select A.Level, Concat(A.AcNo, ' ', A.AcDesc) As Account, ");
                        SQL.AppendLine("A.AcNo, A.Alias, 0.00 As Amt, 0.00 As Amt01, ");
                        SQL.AppendLine("0.00 As Amt02, 0.00 As Amt03, ");
                        SQL.AppendLine("0.00 As Amt04, 0.00 As Amt05, ");
                        SQL.AppendLine("0.00 As Amt06, 0.00 As Amt07, ");
                        SQL.AppendLine("0.00 As Amt08, 0.00 As Amt09, ");
                        SQL.AppendLine("0.00 As Amt10, 0.00 As Amt11, ");
                        SQL.AppendLine("0.00 As Amt12, ");
                        SQL.AppendLine("If(B.X Is Null, 'N', 'Y') As Allow, ");
                        SQL.AppendLine("0.00 As Actual, ");
                        SQL.AppendLine("0.00 As Actual01, 0.00 As Actual02, 0.00 As Actual03, ");
                        SQL.AppendLine("0.00 As Actual04, 0.00 As Actual05, 0.00 As Actual06, ");
                        SQL.AppendLine("0.00 As Actual07, 0.00 As Actual08, 0.00 As Actual09, ");
                        SQL.AppendLine("0.00 As Actual10, 0.00 As Actual11, 0.00 As Actual12 ");
                        SQL.AppendLine("From TblCOA A ");
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("    Select AcNo As X From TblCOA ");
                        SQL.AppendLine("    Where AcNo Not In (");
                        SQL.AppendLine("        Select Parent From TblCoa ");
                        SQL.AppendLine("        Where Parent Is Not Null ");
                        SQL.AppendLine("        ) ");
                        // SQL.AppendLine("    And Find_In_Set(AcNo, @CCAcNo) ");
                        SQL.AppendLine(subSQL.ToString());
                        SQL.AppendLine(") B On A.AcNo=B.X ");
                        SQL.AppendLine("Where A.ActInd='Y' ");
                        if(mIsCompanyBudgetPlanProfitLoss)
                            SQL.AppendLine("And Left(A.AcNo, 1) > 3 ");
                        else
                            SQL.AppendLine("And Left(A.AcNo, 1) Between 1 AND 3 ");
                        // SQL.AppendLine("And Find_In_Set(A.AcNo, @CCAcNo) ");
                        SQL.AppendLine(subSQL.ToString());
                        SQL.AppendLine("Order by A.AcNo;");

                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
                        // Sm.CmParam<String>(ref cm, "@CCAcNo", GetCCAcNo());
                    }
                    else
                    {
                        SQL.AppendLine("Select B.Level, Concat(A.AcNo, ' ', B.AcDesc) As Account, ");
                        SQL.AppendLine("A.AcNo, B.Alias, A.Amt, A.Amt01, ");
                        SQL.AppendLine("A.Amt02, A.Amt03, ");
                        SQL.AppendLine("A.Amt04, A.Amt05, ");
                        SQL.AppendLine("A.Amt06, A.Amt07, ");
                        SQL.AppendLine("A.Amt08, A.Amt09, ");
                        SQL.AppendLine("A.Amt10, A.Amt11, ");
                        SQL.AppendLine("A.Amt12, ");
                        SQL.AppendLine("If(C.AcNo Is Null, 'N', 'Y') As Allow, ");
                        if (!BtnSave.Enabled && ChkCompletedInd.Checked)
                        {
                            SQL.AppendLine("IfNull(D.Actual, 0.00) As Actual, ");
                            SQL.AppendLine("IfNull(D.Actual01, 0.00) As Actual01, IfNull(D.Actual02, 0.00) As Actual02, IfNull(D.Actual03, 0.00) As Actual03, ");
                            SQL.AppendLine("IfNull(D.Actual04, 0.00) As Actual04, IfNull(D.Actual05, 0.00) As Actual05, IfNull(D.Actual06, 0.00) As Actual06, ");
                            SQL.AppendLine("IfNull(D.Actual07, 0.00) As Actual07, IfNull(D.Actual08, 0.00) As Actual08, IfNull(D.Actual09, 0.00) As Actual09, ");
                            SQL.AppendLine("IfNull(D.Actual10, 0.00) As Actual10, IfNull(D.Actual11, 0.00) As Actual11, IfNull(D.Actual12, 0.00) As Actual12 ");
                        }
                        else
                        {
                            SQL.AppendLine("0.00 As Actual, ");
                            SQL.AppendLine("0.00 As Actual01, 0.00 As Actual02, 0.00 As Actual03, ");
                            SQL.AppendLine("0.00 As Actual04, 0.00 As Actual05, 0.00 As Actual06, ");
                            SQL.AppendLine("0.00 As Actual07, 0.00 As Actual08, 0.00 As Actual09, ");
                            SQL.AppendLine("0.00 As Actual10, 0.00 As Actual11, 0.00 As Actual12 ");
                        }

                        SQL.AppendLine("From TblCompanyBudgetPlanDtl A ");
                        SQL.AppendLine("Inner Join TblCOA B On A.AcNo=B.AcNo ");
                        SQL.AppendLine("Left Join ( ");
                        SQL.AppendLine("   Select AcNo From TblCoa ");
                        SQL.AppendLine("   Where Acno Not In (Select Parent From TblCoa Where Parent Is Not Null) ");
                        SQL.AppendLine(") C On B.Acno=C.AcNo ");
                        if (!BtnSave.Enabled && ChkCompletedInd.Checked)
                        {
                            SQL.AppendLine("Left Join (");
                            SQL.AppendLine("    Select AcNo, 0.00 ");
                            for (int i = 1; i <= 12; i++)
                                SQL.AppendLine("    +Sum(Actual" + Sm.Right("0" + i.ToString(), 2) + ") ");
                            SQL.AppendLine("    As Actual ");
                            for (int i = 1; i <= 12; i++)
                                SQL.AppendLine("    ,Sum(Actual" + Sm.Right("0" + i.ToString(), 2) + ") As Actual" + Sm.Right("0" + i.ToString(), 2));
                            SQL.AppendLine("    From ( ");
                            SQL.AppendLine("    Select B.AcNo ");
                            for (int i = 1; i <= 12; i++)
                                SQL.AppendLine("    ,Case Substring(D.DocDt, 5, 2) When '" + Sm.Right("0" + i.ToString(), 2) + "' Then D.Amt Else 0.00 End As Actual" + Sm.Right("0" + i.ToString(), 2));
                            SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
                            SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("    Inner Join TblBudgetCategory C On A.CCCode=C.CCCode And B.AcNo=C.AcNo ");
                            SQL.AppendLine("    Inner Join TblVoucherHdr D On C.BCCode=D.BCCode And D.CancelInd='N' And A.Yr=Left(D.DocDt, 4) And D.BCCode Is Not Null ");
                            SQL.AppendLine("    Where A.CancelInd='N' ");
                            SQL.AppendLine("    And A.CompletedInd='Y' ");
                            SQL.AppendLine("    And A.DocNo=@DocNo ");
                            SQL.AppendLine("    ) T Group By AcNo ");
                            SQL.AppendLine(") D On B.Acno=D.AcNo ");
                        }
                        SQL.AppendLine("Where DocNo=@DocNo ");
                        SQL.AppendLine("Order By AcNo;");

                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                        
                    }
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Level", 
                        //1-5
                        "Account", "AcNo", "Alias", "Allow", "Amt",
                        //6-10
                        "Amt01", "Amt02", "Amt03", "Amt04", "Amt05",
                        //11-15
                        "Amt06", "Amt07", "Amt08", "Amt09", "Amt10",
                        //16-20
                        "Amt11", "Amt12", "Actual", "Actual01", "Actual02", 
                        //21-25
                        "Actual03", "Actual04", "Actual05", "Actual06", "Actual07", 
                        //26-30
                        "Actual08", "Actual09", "Actual10", "Actual11", "Actual12"
                    });
                    if (!dr.HasRows)
                    {
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    }
                    else
                    {
                        int Row = 0;
                        Grd1.Rows.Count = 0;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Level = int.Parse(Sm.DrStr(dr, 0));
                            //Level = Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 4);
                            if (TxtDocNo.Text.Length == 0)
                            {
                                Grd1.Cells[Row, 4].Value = 0m;
                                Grd1.Cells[Row, 5].Value = 0m;
                                Grd1.Cells[Row, 6].Value = 0m;
                                Grd1.Cells[Row, 7].Value = 0m;
                                Grd1.Cells[Row, 8].Value = 0m;
                                Grd1.Cells[Row, 9].Value = 0m;
                                Grd1.Cells[Row, 10].Value = 0m;
                                Grd1.Cells[Row, 11].Value = 0m;
                                Grd1.Cells[Row, 12].Value = 0m;
                                Grd1.Cells[Row, 13].Value = 0m;
                                Grd1.Cells[Row, 14].Value = 0m;
                                Grd1.Cells[Row, 15].Value = 0m;
                                Grd1.Cells[Row, 16].Value = 0m;
                                Grd1.Cells[Row, 17].Value = 0m;
                                Grd1.Cells[Row, 18].Value = 0m;
                                Grd1.Cells[Row, 19].Value = 0m;
                                Grd1.Cells[Row, 20].Value = 0m;
                                Grd1.Cells[Row, 21].Value = 0m;
                                Grd1.Cells[Row, 22].Value = 0m;
                                Grd1.Cells[Row, 23].Value = 0m;
                                Grd1.Cells[Row, 24].Value = 0m;
                                Grd1.Cells[Row, 25].Value = 0m;
                                Grd1.Cells[Row, 26].Value = 0m;
                                Grd1.Cells[Row, 27].Value = 0m;
                                Grd1.Cells[Row, 28].Value = 0m;
                                Grd1.Cells[Row, 29].Value = 0m;
                                Grd1.Cells[Row, 30].Value = 0m;
                            }
                            else
                            {
                                Grd1.Cells[Row, 4].Value = Sm.DrDec(dr, c[5]);
                                Grd1.Cells[Row, 5].Value = Sm.DrDec(dr, c[5]);
                                Grd1.Cells[Row, 6].Value = Sm.DrDec(dr, c[6]);
                                Grd1.Cells[Row, 7].Value = Sm.DrDec(dr, c[6]);
                                Grd1.Cells[Row, 8].Value = Sm.DrDec(dr, c[7]);
                                Grd1.Cells[Row, 9].Value = Sm.DrDec(dr, c[7]);
                                Grd1.Cells[Row, 10].Value = Sm.DrDec(dr, c[8]);
                                Grd1.Cells[Row, 11].Value = Sm.DrDec(dr, c[8]);
                                Grd1.Cells[Row, 12].Value = Sm.DrDec(dr, c[9]);
                                Grd1.Cells[Row, 13].Value = Sm.DrDec(dr, c[9]);
                                Grd1.Cells[Row, 14].Value = Sm.DrDec(dr, c[10]);
                                Grd1.Cells[Row, 15].Value = Sm.DrDec(dr, c[10]);
                                Grd1.Cells[Row, 16].Value = Sm.DrDec(dr, c[11]);
                                Grd1.Cells[Row, 17].Value = Sm.DrDec(dr, c[11]);
                                Grd1.Cells[Row, 18].Value = Sm.DrDec(dr, c[12]);
                                Grd1.Cells[Row, 19].Value = Sm.DrDec(dr, c[12]);
                                Grd1.Cells[Row, 20].Value = Sm.DrDec(dr, c[13]);
                                Grd1.Cells[Row, 21].Value = Sm.DrDec(dr, c[13]);
                                Grd1.Cells[Row, 22].Value = Sm.DrDec(dr, c[14]);
                                Grd1.Cells[Row, 23].Value = Sm.DrDec(dr, c[14]);
                                Grd1.Cells[Row, 24].Value = Sm.DrDec(dr, c[15]);
                                Grd1.Cells[Row, 25].Value = Sm.DrDec(dr, c[15]);
                                Grd1.Cells[Row, 26].Value = Sm.DrDec(dr, c[16]);
                                Grd1.Cells[Row, 27].Value = Sm.DrDec(dr, c[16]);
                                Grd1.Cells[Row, 28].Value = Sm.DrDec(dr, c[17]);
                                Grd1.Cells[Row, 29].Value = Sm.DrDec(dr, c[17]);
                                Grd1.Cells[Row, 30].Value = Sm.DrDec(dr, c[5]);
                            }

                            Grd1.Cells[Row, 32].Value = Sm.DrDec(dr, c[18]);
                            Grd1.Cells[Row, 33].Value = Sm.DrDec(dr, c[19]);
                            Grd1.Cells[Row, 34].Value = Sm.DrDec(dr, c[20]);
                            Grd1.Cells[Row, 35].Value = Sm.DrDec(dr, c[21]);
                            Grd1.Cells[Row, 36].Value = Sm.DrDec(dr, c[22]);
                            Grd1.Cells[Row, 37].Value = Sm.DrDec(dr, c[23]);
                            Grd1.Cells[Row, 38].Value = Sm.DrDec(dr, c[24]);
                            Grd1.Cells[Row, 39].Value = Sm.DrDec(dr, c[25]);
                            Grd1.Cells[Row, 40].Value = Sm.DrDec(dr, c[26]);
                            Grd1.Cells[Row, 41].Value = Sm.DrDec(dr, c[27]);
                            Grd1.Cells[Row, 42].Value = Sm.DrDec(dr, c[28]);
                            Grd1.Cells[Row, 43].Value = Sm.DrDec(dr, c[29]);
                            Grd1.Cells[Row, 44].Value = Sm.DrDec(dr, c[30]);

                            if (Sm.GetGrdStr(Grd1, Row, 3) == "N")
                            {
                                Grd1.Rows[Row].ReadOnly = iGBool.True;
                                Grd1.Rows[Row].BackColor = Color.FromArgb(224, 224, 224);
                            }
                            Grd1.Rows[Row].Level = Level;
                            if (Row > 0)
                                Grd1.Rows[Row - 1].TreeButton =
                                    (PrevLevel >= Level) ? iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            PrevLevel = Level; 
                            Row++;
                        }
                    }
                    dr.Close();

                    //for (int r = 0; r < Grd1.Rows.Count; r++)
                    //{
                    //    // Level = int.Parse(Grd1.Cells[r, 45].Value.ToString());
                    //    // Grd1.Rows[r].Level = Level;
                    //    if (r > 0)
                    //        Grd1.Rows[r - 1].TreeButton =
                    //            (PrevLevel >= Level) ? //(PrevAcNo.Length >= Sm.DrStr(dr, 2).Length) ?
                    //                iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                    //    PrevLevel = Level; //Sm.DrStr(dr, 2);
                    //}

                    if (TxtDocNo.Text.Length != 0 && !BtnSave.Enabled && !ChkCancelInd.Checked && ChkCompletedInd.Checked && ChkParentActual.Checked)
                    {
                        var l = new List<AcRow>();
                        for (int i = 0; i < Grd1.Rows.Count; i++)
                            l.Add(new AcRow {AcNo = Sm.GetGrdStr(Grd1, i, 1), Row = i });

                        for (int i = 0; i < Grd1.Rows.Count; i++)
                        {
                            if (Sm.GetGrdStr(Grd1, i, 3) == "Y")
                            {
                                for (int j = 32; j <= 44; j++)
                                    ComputeActual(i, j, ref l);
                            }
                        }
                        l.Clear();
                    }

                    Grd1.TreeLines.Visible = true;
                    Grd1.EndUpdate();
                    
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Old Code
        //private string GetCCAcNo()
        //{
        //    var l1 = new List<string>();
        //    var l2 = new List<string>();
        //    var v = string.Empty;

        //    GetCCAcNo(ref l1);

        //    if (l1.Count() > 0)
        //    {
        //        int p = 1;

        //        foreach (var x in l1)
        //        {
        //            p = 1;
        //            v = x;
        //            l2.Add(v);
        //            while (p > 0)
        //            {
        //                p = v.LastIndexOf(".");
        //                if (p > 0)
        //                {
        //                    v = v.Substring(0, p);
        //                    l2.Add(v);
        //                }
        //            }
        //        }
        //        l1.Clear();
        //        v = string.Empty; //digunakan kembali utk proses lain

        //        foreach (var x in l2.Distinct())
        //        {
        //            if (v.Length > 0) v += ",";
        //            v += x;
        //        }
        //    }
        //    return v.Length == 0?"***":v;
        //}
        #endregion

        private void GetCCAcNo(ref MySqlCommand cm, ref StringBuilder subSQL)
        {
            var l1 = new List<string>();
            var l2 = new List<string>();
            var v = string.Empty;
            int i = 0;

            GetCCAcNo(ref l1);

            if (l1.Count() > 0)
            {
                int p = 1;

                foreach (var x in l1)
                {
                    p = 1;
                    v = x;
                    l2.Add(v);
                    while (p > 0)
                    {
                        p = v.LastIndexOf(".");
                        if (p > 0)
                        {
                            v = v.Substring(0, p);
                            l2.Add(v);
                        }
                    }
                }
                l1.Clear();
                v = string.Empty; //digunakan kembali utk proses lain

                subSQL.AppendLine(" And (AcNo='***' ");
                foreach (var x in l2.Distinct())
                {
                    subSQL.AppendLine(" Or AcNo=@AcNo__" + i.ToString());
                    Sm.CmParam<String>(ref cm, "@AcNo__" + i.ToString(), x);
                    i++;
                }
                subSQL.AppendLine(") ");
            }
        }

        private void GetCCAcNo(ref List<string> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.AcNo ");
            SQL.AppendLine("From TblCostCategory A ");
            SQL.AppendLine("Inner Join TblCoa B On A.AcNo=B.AcNo And B.ActInd='Y' ");
            SQL.AppendLine("Where A.AcNo Is Not Null ");
            SQL.AppendLine("And A.CCCode=@CCCode ");
            SQL.AppendLine("Order By A.AcNo; ");

            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read()) l.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        //internal void SetLueCCCode(ref LookUpEdit Lue)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter Where CBPInd = 'Y'  Order By CCName ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        internal void SetLueCCCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CCCode As Col1, T.CCName As Col2 From TblCostCenter T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.CCCode=@Code;");
            else
            {
                SQL.AppendLine("Where T.NotParentInd='Y' ");
                SQL.AppendLine("And T.ActInd='Y' ");
                SQL.AppendLine("And T.CBPInd='Y' ");
                SQL.AppendLine("And T.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("And T.ProfitCenterCode In (");
                SQL.AppendLine("    Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By T.CCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        //internal void SetLueCCCode(ref LookUpEdit Lue, string Code, string IsFilterByCC)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Select T.CCCode As Col1, T.CCName As Col2 From TblCostCenter T Where 0=0  ");
        //    if (Code.Length > 0)
        //    {
        //        SQL.AppendLine("And CCCode=@Code;");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("And T.CBPInd = 'Y' ");
        //        SQL.AppendLine("And T.CCCode Not In ( ");
        //        SQL.AppendLine("    Select Parent From TblCostCenter ");
        //        SQL.AppendLine("    Where Parent Is Not Null ");
        //        SQL.AppendLine(") ");
        //        SQL.AppendLine("And T.ActInd='Y' ");

        //        if (IsFilterByCC == "Y")
        //        {
        //            SQL.AppendLine("And Exists( ");
        //            SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
        //            SQL.AppendLine("    Where CCCode=T.CCCode ");
        //            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
        //            SQL.AppendLine("    ) ");
        //        }
        //        SQL.AppendLine("Order By T.CCName;");
        //    }

        //    cm.CommandText = SQL.ToString();
        //    if (Code.Length > 0)
        //        Sm.CmParam<String>(ref cm, "@Code", Code);
        //    else
        //        Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //    if (Code.Length > 0) Sm.SetLue(Lue, Code);
        //}

        #endregion

        #region Import CSV

        private void ProcessImport()
        {
            string FileName = string.Empty;
            var l = new List<ImportCSV>();
            var l2 = new List<GrdAccount>();

            FileName = OpenFileDialog();
            if (FileName.Length == 0 || FileName == "openFileDialog1") return;
            Sm.ClearGrd(Grd1, true);
            ShowAccount();
            ReadFileToList(ref l, FileName);
            if (IsCOAInvalid(ref l)) return;
            Process1(ref l); //move from list csv ti list acitem
            ProcessItem();
            
            if (l.Count > 0)
            {
               Process2(ref l, ref l2); //compute amount
               Process3(ref l2); // input amount to grid
            }

            l.Clear(); l2.Clear();
        }

        private bool IsCOAInvalid(ref List<ImportCSV> l)
        {
            string InvalidAcNo = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                foreach (var x in l
                    .OrderBy(y => y.AcNo)
                    .Where(z => z.AcNo == Sm.GetGrdStr(Grd1, i, 1))
                    )
                {
                    x.IsValid = true;
                    //if (InvalidAcNo.Length == 0)
                    //    InvalidAcNo = x.AcNo;
                    //else
                    //    InvalidAcNo += String.Concat(" ," ,x.AcNo);
                    //break;
                }
            }

            foreach (var x in l.Where(w => !w.IsValid))
            {
                if (InvalidAcNo.Length > 0) InvalidAcNo += ", ";
                InvalidAcNo += x.AcNo;
            }

            if (InvalidAcNo.Length != 0)
            {
                var Msg = string.Empty;
                Sm.ClearGrd(Grd1, false);
                l.Clear();
                Msg = ("One or more Account is Invalid : " + InvalidAcNo);

                Sm.StdMsg(mMsgType.Warning, Msg);
                return true;
            }
            return false;
        }

        private string OpenFileDialog()
        {
            OD.InitialDirectory = "c:";
            OD.Filter = "CSV files (*.csv)|*.CSV";
            OD.FilterIndex = 2;
            OD.ShowDialog();

            return OD.FileName;
        }

        private void ReadFileToList(ref List<ImportCSV> l, string FileName)
        {
            bool IsFirst = true;

            using (var rd = new StreamReader(FileName))
            {
                int mRow = 0;

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 26)
                    {
                        if (splits.Count() > 26)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Each field " + splits[0].Trim() + " could not have any comma(s) in its description.");
                        }
                        else if (splits.Count() < 26)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Make sure this data " + splits[0].Trim() + " have a correct column count data.");
                        }

                        l.Clear();
                        return;
                    }
                    else
                    {
                        if (IsFirst) IsFirst = false;
                        else
                        {
                            mRow += 1;

                            var arr = splits.ToArray();
                            if (arr[0].Trim().Length > 0)
                            {
                                l.Add(new ImportCSV()
                                {
                                    AcNo = splits[0].Trim(),
                                    ItCode = splits[1].Trim(),
                                    Rate01 = Decimal.Parse(splits[2].Trim()),
                                    Rate02 = Decimal.Parse(splits[3].Trim()),
                                    Rate03 = Decimal.Parse(splits[4].Trim()),
                                    Rate04 = Decimal.Parse(splits[5].Trim()),
                                    Rate05 = Decimal.Parse(splits[6].Trim()),
                                    Rate06 = Decimal.Parse(splits[7].Trim()),
                                    Rate07 = Decimal.Parse(splits[8].Trim()),
                                    Rate08 = Decimal.Parse(splits[9].Trim()),
                                    Rate09 = Decimal.Parse(splits[10].Trim()),
                                    Rate10 = Decimal.Parse(splits[11].Trim()),
                                    Rate11 = Decimal.Parse(splits[12].Trim()),
                                    Rate12 = Decimal.Parse(splits[13].Trim()),
                                    Qty01 = Decimal.Parse(splits[14].Trim()),
                                    Qty02 = Decimal.Parse(splits[15].Trim()),
                                    Qty03 = Decimal.Parse(splits[16].Trim()),
                                    Qty04 = Decimal.Parse(splits[17].Trim()),
                                    Qty05 = Decimal.Parse(splits[18].Trim()),
                                    Qty06 = Decimal.Parse(splits[19].Trim()),
                                    Qty07 = Decimal.Parse(splits[20].Trim()),
                                    Qty08 = Decimal.Parse(splits[21].Trim()),
                                    Qty09 = Decimal.Parse(splits[22].Trim()),
                                    Qty10 = Decimal.Parse(splits[23].Trim()),
                                    Qty11 = Decimal.Parse(splits[24].Trim()),
                                    Qty12 = Decimal.Parse(splits[25].Trim()),
                                    IsValid = false
                                });
                            }
                            else
                            {
                                Sm.StdMsg(mMsgType.Warning, "No data at row#" + mRow.ToString());
                            }
                        }
                    }
                }
            }
        }

        private void Process1(ref List<ImportCSV> l)
        {
            foreach(var x in l)
            {
                ml.Add(new AcItem()
                {
                    AcNo = x.AcNo,
                    ItCode = x.ItCode,
                    Rate01 = x.Rate01,
                    Rate02 = x.Rate02,
                    Rate03 = x.Rate03,
                    Rate04 = x.Rate04,
                    Rate05 = x.Rate05,
                    Rate06 = x.Rate06,
                    Rate07 = x.Rate07,
                    Rate08 = x.Rate08,
                    Rate09 = x.Rate09,
                    Rate10 = x.Rate10,
                    Rate11 = x.Rate11,
                    Rate12 = x.Rate12,
                    Qty01 = x.Qty01,
                    Qty02 = x.Qty02,
                    Qty03 = x.Qty03,
                    Qty04 = x.Qty04,
                    Qty05 = x.Qty05,
                    Qty06 = x.Qty06,
                    Qty07 = x.Qty07,
                    Qty08 = x.Qty08,
                    Qty09 = x.Qty09,
                    Qty10 = x.Qty10,
                    Qty11 = x.Qty11,
                    Qty12 = x.Qty12,


                });
            }
        }

        private void ProcessItem()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string
                ItCode = string.Empty;

            SQL.AppendLine("Select ItCode, ItName, PurchaseUomCode ");
            SQL.AppendLine("From TblItem ");
            SQL.AppendLine("Where ActInd = 'Y' And Find_In_Set(ItCode, @ItCode) ; ");

            foreach (var x in ml.Where(w => w.ItCode.Trim().Length > 0))
            {
                if (ItCode.Length > 0) ItCode += ",";
                ItCode += x.ItCode;
            }
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                { 
                    //0
                    "ItCode", 

                    //1-2 
                    "ItName",
                    "PurchaseUomCode",
                   
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in ml.Where(w => w.ItCode.Trim().Length > 0 &&
                            w.ItCode == Sm.DrStr(dr, c[0]) // ItCode
                         ))
                        {
                            x.ItName = Sm.DrStr(dr, c[1]);
                            x.PurchaseUomCode = Sm.DrStr(dr, c[2]);
                            
                        }
                    }
                }
                dr.Close();
            }

        }

        private void Process2(ref List<ImportCSV> l, ref List<GrdAccount>l2)
        {
            l2 = l.GroupBy(x => x.AcNo)
                .Select(t => new GrdAccount
                {
                    AcNo = t.Key,
                    Amt01 = t.Sum(s => s.Qty01 * s.Rate01),
                    Amt02 = t.Sum(s => s.Qty02 * s.Rate02),
                    Amt03 = t.Sum(s => s.Qty03 * s.Rate03),
                    Amt04 = t.Sum(s => s.Qty04 * s.Rate04),
                    Amt05 = t.Sum(s => s.Qty05 * s.Rate05),
                    Amt06 = t.Sum(s => s.Qty06 * s.Rate06),
                    Amt07 = t.Sum(s => s.Qty07 * s.Rate07),
                    Amt08 = t.Sum(s => s.Qty08 * s.Rate08),
                    Amt09 = t.Sum(s => s.Qty09 * s.Rate09),
                    Amt10 = t.Sum(s => s.Qty10 * s.Rate10),
                    Amt11 = t.Sum(s => s.Qty11 * s.Rate11),
                    Amt12 = t.Sum(s => s.Qty12 * s.Rate12),
                    
                }).ToList();

        }
        private void Process3(ref List<GrdAccount> l2)
        {
            for (int i = 0; i < Grd1.Rows.Count ; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 3) == "Y")
                {
                    foreach (var x in l2
                        .OrderBy(y => y.AcNo)
                        .Where(z => z.AcNo == Sm.GetGrdStr(Grd1, i, 1))
                        )
                    {
                        Grd1.Cells[i, 6].Value = x.Amt01;
                        Grd1.Cells[i, 8].Value = x.Amt02;
                        Grd1.Cells[i, 10].Value = x.Amt03;
                        Grd1.Cells[i, 12].Value = x.Amt04;
                        Grd1.Cells[i, 14].Value = x.Amt05;
                        Grd1.Cells[i, 16].Value = x.Amt06;
                        Grd1.Cells[i, 18].Value = x.Amt07;
                        Grd1.Cells[i, 20].Value = x.Amt08;
                        Grd1.Cells[i, 22].Value = x.Amt09;
                        Grd1.Cells[i, 24].Value = x.Amt10;
                        Grd1.Cells[i, 26].Value = x.Amt11;
                        Grd1.Cells[i, 28].Value = x.Amt12;

                        for (int j = 1; j <= 12; j++)
                        {
                            ComputeAmt(i, (j * 2) + 4, mInputAmt);
                        }

                        break;
                    }
                    
                }                
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), string.Empty);
            Sm.ClearGrd(Grd1, true);
        }
        
        #endregion

        #region Button Clicks

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ProcessImport();
            }
        }

        private void BtnShowAc_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueYr, "Year") && !(Sm.IsLueEmpty(LueCCCode, "Cost center")))
                ShowAccount();
        }

        private void BtnBudgetRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBudgetRequestDocNo, "Budget Request Yearly", false))
            {
                var f = new FrmBudgetRequestYearly("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtBudgetRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        internal class AcItem
        {
            public string AcNo { set; get; }
            public string ItCode { set; get; }
            public string ItName { get; set; }
            public string PurchaseUomCode { get; set; }
            public decimal Rate01 { get; set; }
            public decimal Rate02 { get; set; }
            public decimal Rate03 { get; set; }
            public decimal Rate04 { get; set; }
            public decimal Rate05 { get; set; }
            public decimal Rate06 { get; set; }
            public decimal Rate07 { get; set; }
            public decimal Rate08 { get; set; }
            public decimal Rate09 { get; set; }
            public decimal Rate10 { get; set; }
            public decimal Rate11 { get; set; }
            public decimal Rate12 { get; set; }
            public decimal Qty01 { get; set; }
            public decimal Qty02 { get; set; }
            public decimal Qty03 { get; set; }
            public decimal Qty04 { get; set; }
            public decimal Qty05 { get; set; }
            public decimal Qty06 { get; set; }
            public decimal Qty07 { get; set; }
            public decimal Qty08 { get; set; }
            public decimal Qty09 { get; set; }
            public decimal Qty10 { get; set; }
            public decimal Qty11 { get; set; }
            public decimal Qty12 { get; set; }
        }

        internal class AcRow
        {
            public string AcNo { set; get; }
            public int Row { set; get; }
        }

        internal class ImportCSV
        {
            public string AcNo { set; get; }
            public string ItCode { set; get; }
            public decimal Rate01 { get; set; }
            public decimal Rate02 { get; set; }
            public decimal Rate03 { get; set; }
            public decimal Rate04 { get; set; }
            public decimal Rate05 { get; set; }
            public decimal Rate06 { get; set; }
            public decimal Rate07 { get; set; }
            public decimal Rate08 { get; set; }
            public decimal Rate09 { get; set; }
            public decimal Rate10 { get; set; }
            public decimal Rate11 { get; set; }
            public decimal Rate12 { get; set; }
            public decimal Qty01 { get; set; }
            public decimal Qty02 { get; set; }
            public decimal Qty03 { get; set; }
            public decimal Qty04 { get; set; }
            public decimal Qty05 { get; set; }
            public decimal Qty06 { get; set; }
            public decimal Qty07 { get; set; }
            public decimal Qty08 { get; set; }
            public decimal Qty09 { get; set; }
            public decimal Qty10 { get; set; }
            public decimal Qty11 { get; set; }
            public decimal Qty12 { get; set; }
            public bool IsValid { get; set; }
        }

        internal class GrdAccount
        {
            public string AcNo { set; get; }
            public decimal Amt01 { get; set; }
            public decimal Amt02 { get; set; }
            public decimal Amt03 { get; set; }
            public decimal Amt04 { get; set; }
            public decimal Amt05 { get; set; }
            public decimal Amt06 { get; set; }
            public decimal Amt07 { get; set; }
            public decimal Amt08 { get; set; }
            public decimal Amt09 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
            
        }


        #endregion

    }
}
