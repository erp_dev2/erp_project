﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRingArea : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
       internal FrmRingAreaFind FrmFind;

        #endregion

        #region Constructor

        public FrmRingArea(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRingCode, TxtRingName
                    }, true);
                    TxtRingCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRingCode, TxtRingName
                    }, false);
                    TxtRingCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtRingCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRingName
                    }, false);
                    TxtRingName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtRingCode, TxtRingName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRingAreaFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRingCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRingCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblRingArea Where RingCode=@RingCode" };
                Sm.CmParam<String>(ref cm, "@RingCode", TxtRingCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblRingArea(RingCode, RingName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@RingCode, @RingName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update RingName=@RingName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@RingCode", TxtRingCode.Text);
                Sm.CmParam<String>(ref cm, "@RingName", TxtRingName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtRingCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string RingCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@RingCode", RingCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select RingCode, RingName From TblRingArea Where RingCode=@RingCode",
                        new string[] 
                        {
                            "RingCode", "RingName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtRingCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtRingName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtRingCode, "Ring area code", false) ||
                Sm.IsTxtEmpty(TxtRingName, "Ring area name", false) ||
                IsRingCodeExisted();
        }

        private bool IsRingCodeExisted()
        {
            if (!TxtRingCode.Properties.ReadOnly && Sm.IsDataExist("Select RingCode From TblRingArea Where RingCode='" + TxtRingCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Ring area code ( " + TxtRingCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtRingCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtRingCode);
        }

        private void TxtRingName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtRingName);
        }

        #endregion

        #endregion
    }
}
