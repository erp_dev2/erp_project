﻿#region Update
/*
    03/02/2022 [IBL/PRODUCT] new apps
*/
#endregion

#region Namespace

using System;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DevExpress.XtraTab;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using MySql.Data.MySqlClient;
using System.Linq;

#endregion

namespace RunSystem
{
    public partial class FrmDashboardUser : RunSystem.FrmBase18
    {
        #region Field

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        public FrmDashboardUser(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #region Method

        #region Form Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            DashboardInit();
        }

        #endregion

        #region Additional Method

        private void DashboardInit()
        {
            var l = new List<DashboardCategory>();
            GetDashboardCategory(ref l);

            if (l.Count > 0)
            {
                SetDashboard(l);
            }

            l.Clear();
        }

        private string QueryBuilder(string DashCtCodes)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("From TblDashboardGroupUser A ");
            SQL.AppendLine("Inner Join TblDashboardFormula B On A.DashCode = B.DashCode ");
            SQL.AppendLine("    And A.GrpCode = (Select GrpCode From TblUser Where UserCode = @UserCode Limit 1) ");
            SQL.AppendLine("    And A.ActInd = 'Y' ");
            SQL.AppendLine("    And B.ActInd = 'Y' ");
            if (DashCtCodes.Length > 0)
                SQL.AppendLine("    And Find_In_Set(B.DashCtCode, @DashCtCode) ");
            SQL.AppendLine("Inner Join TblDashboardCategory C On B.DashCtCode = C.DashCtCode ");
            SQL.AppendLine("    And C.ActInd = 'Y' ");
            SQL.AppendLine("Order By C.DashCtCode; ");

            return SQL.ToString();
        }

        private void GetDashboardCategory(ref List<DashboardCategory> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct C.DashCtCode, C.DashCtName ");


            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString() + QueryBuilder(string.Empty);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                { "DashCtCode", "DashCtName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DashboardCategory()
                        {
                            DashCtCode = Sm.DrStr(dr, c[0]),
                            DashCtName = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetDashboard(List<DashboardCategory> l)
        {
            var lm = new List<XtraTabPage>();
            var l2 = new List<UserDashboard>();
            string DashCtCodes = string.Empty;

            l.ForEach(t =>
            {
                lm.Add(new XtraTabPage()
                {
                    Name = t.DashCtName,
                    Text = t.DashCtName,
                    Tag = t.DashCtCode,
                });

                if (DashCtCodes.Length > 0) DashCtCodes += ",";
                DashCtCodes += t.DashCtCode;
            });

            GetUserDashboard(ref l2, DashCtCodes);

            lm.ForEach(t =>
            {
                // ref : https://www.youtube.com/watch?v=NxfYFAw0JDs&ab_channel=whitehorsehn
                FlowLayoutPanel flp = new FlowLayoutPanel()
                {
                    Dock = DockStyle.Fill,
                    BackColor = Color.White,
                    Name = string.Concat("FLP", t.Tag),
                    FlowDirection = FlowDirection.LeftToRight,
                    WrapContents = true,
                    AutoScroll = true,
                };
                Tc.TabPages.Add(t);
                SetDashboard2(l, l2, t, flp);
            });

            lm.Clear();
            l2.Clear();
        }

        private void GetUserDashboard(ref List<UserDashboard> l2, string DashCtCodes)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DashCode, B.DashName, B.DashCtCode, B.DashType, B.Formula, A.SeqNo ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DashCtCode", DashCtCodes);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString() + QueryBuilder(DashCtCodes);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                { "DashCode", "DashName", "DashCtCode", "DashType", "Formula", "SeqNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new UserDashboard()
                        {
                            DashCode = Sm.DrStr(dr, c[0]),
                            DashName = Sm.DrStr(dr, c[1]),
                            DashCtCode = Sm.DrStr(dr, c[2]),
                            DashType = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            SeqNo = Sm.DrInt(dr, c[5]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetDashboard2(List<DashboardCategory> l, List<UserDashboard> l2, XtraTabPage page, FlowLayoutPanel flp)
        {
            if (l2.Count > 0)
            {
                page.Controls.Add(flp);

                foreach (var x in l.Where(w => w.DashCtCode == (string)page.Tag))
                {
                    #region Counter
                    foreach (var y in l2
                    .Where(w => w.DashCtCode == x.DashCtCode && w.DashType.ToUpper() == "COUNTER")
                    .OrderBy(o => Sm.Right(string.Concat("000", o.SeqNo.ToString()), 3)))
                    {
                        var lx = new List<Panel>();
                        GetWidgetCounterFormula2(ref lx, y);

                        if (lx.Count > 0)
                        {
                            lx.ForEach(p =>
                            {
                                flp.Controls.Add(p);
                            });
                        }

                        lx.Clear();
                    }
                    #endregion
                };
            }
        }

        private void GetWidgetCounterFormula2(ref List<Panel> lx, UserDashboard ud)
        {
            var cm = new MySqlCommand();
            string prefixQuery = "Select Convert(Format(T.Counter, 0) Using utf8) Counter From ( ";
            string suffixQuery = " ) T;";

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = string.Concat(prefixQuery, ud.Formula.Replace(";", string.Empty), suffixQuery);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                { "Counter" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Label lbCounter = new Label()
                        {
                            Font = new Font("Lato", 28F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0))),
                            Size = new Size(200, 39),
                            Location = new Point(1, 10),
                            Text = Sm.DrStr(dr, c[0]),
                            Anchor = AnchorStyles.Top,
                            TextAlign = ContentAlignment.TopCenter,
                            AutoSize = false
                        };

                        Label lbDashName = new Label()
                        {
                            Font = new Font("Lato", 8F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))),
                            Size = new Size(200, 39),
                            Location = new Point(1, 60),
                            Text = ud.DashName,
                            ForeColor = Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51))))),
                            Anchor = AnchorStyles.Top,
                            TextAlign = ContentAlignment.TopCenter,
                            AutoSize = false
                        };

                        decimal counter = Decimal.Parse(Sm.DrStr(dr, c[0]).Replace(",", string.Empty));

                        if (counter > 20) lbCounter.ForeColor = Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(85)))), ((int)(((byte)(89)))));
                        else if (counter > 10 && counter <= 20) lbCounter.ForeColor = Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(117)))), ((int)(((byte)(0)))));
                        else lbCounter.ForeColor = Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(198)))), ((int)(((byte)(114)))));

                        lx.Add(new Panel()
                        {
                            BackColor = Color.White,
                            Name = ud.DashCode,
                            Dock = DockStyle.None,
                            BorderStyle = BorderStyle.FixedSingle,
                        });

                        foreach (var i in lx.Where(w => w.Name == ud.DashCode))
                        {
                            i.Controls.Add(lbCounter);
                            i.Controls.Add(lbDashName);
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmTemplate18_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #endregion

        #region Class
        private class DashboardCategory
        {
            public string DashCtCode { get; set; }
            public string DashCtName { get; set; }
        }

        private class UserDashboard
        {
            public string DashCode { get; set; }
            public string DashName { get; set; }
            public string DashCtCode { get; set; }
            public string DashType { get; set; }
            public string Formula { get; set; }
            public int SeqNo { get; set; }
        }
        #endregion
    }
}
