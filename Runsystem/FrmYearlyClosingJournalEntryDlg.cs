﻿#region Update
/*
    01/02/2023 [IBL/BBT] Menu baru
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmYearlyClosingJournalEntryDlg : RunSystem.FrmBase4
    {

        #region Field

        private FrmYearlyClosingJournalEntry mFrmParent;
        private string
            mSQL = string.Empty,
            mTypeDlg = string.Empty
            ;
        private Label label6;
        private DXE.TextEdit TxtAcNo;
        private DXE.CheckEdit ChkAcNo;

        #endregion

        #region Constructor

        public FrmYearlyClosingJournalEntryDlg(FrmYearlyClosingJournalEntry FrmParent, string TypeDlg)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mTypeDlg = TypeDlg;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List of COA's Account#";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueLevel(ref LueLevel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    "No.",

                    //1-5
                    "",
                    "Account#",
                    "Description",
                    "Type",
                    "Alias",

                    //6
                    "ProcessInd",
                    "Level",
                    "hasChild",
                    "Amt"
                },
                new int[] { 50,
                    20, 150, 350, 100, 150,
                    80, 80, 80, 120 }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, Case A.AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType, A.Alias, A.level, 'Y' haschild, ");
            SQL.AppendLine("IfNull(B.DAmt, 0.00) + IfNull(B.CAmt, 0.00) As Amt ");
            SQL.AppendLine("From TblCoa A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("	Select X2.AcNo, ");
            SQL.AppendLine("	Sum(If(X3.AcType = 'D', DAmt, DAmt*-1)) As DAmt, ");
            SQL.AppendLine("	Sum(If(X3.AcType = 'C', CAmt, CAmt*-1)) As CAmt ");
            SQL.AppendLine("	From TblJournalHdr X1 ");
            SQL.AppendLine("	Inner Join TblJournalDtl X2 On X2.DocNo = X1.DocNo ");
            SQL.AppendLine("	Inner Join TblCOA X3 On X2.AcNo = X3.AcNo ");
            SQL.AppendLine("	Where Left(X1.DocDt, 4) = @Yr ");
            SQL.AppendLine("    And X3.ActInd = 'Y' ");
            SQL.AppendLine("	Group By X2.AcNo ");
            SQL.AppendLine(") B On A.AcNo = B.AcNo ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            SQL.AppendLine("And A.Parent is Not Null ");
            SQL.AppendLine("And A.AcNo Not In (Select Parent From TblCoa Where Parent is Not Null) ");
            if (Sm.GetLue(LueLevel).Length > 0)
                SQL.AppendLine("And Left(A.Acno, 1) = '" + Sm.GetLue(LueLevel) + "'");


            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueLevel, "Account Group")) return;
                Cursor.Current = Cursors.WaitCursor;


                SetSQL();
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAccountNo.Text, new string[] { "A.AcNo", "A.AcDesc", "A.Alias" });
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(mFrmParent.LueYr));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By AcNo;",
                        new string[]
                       {
                            "AcNo",

                            "AcDesc",
                            "AcType",
                            "Alias",
                            "Level",
                            "haschild",
                            "Amt"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Grd1.Cells[Row, 8].Value = null;
                        }, true, false, false, false
                    );
                //var lCOA = new List<COA>();
                //Process1(ref lCOA);
                //Process2(ref lCOA);
                //Process3(ref lCOA);

                //lCOA.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                if (mTypeDlg == "1")
                {
                    int CountCheck = 0;
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1))
                        {
                            CountCheck++;
                        }
                    }
                    if (CountCheck > 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "You need to choose just 1 account#.");
                        return;
                    }
                    else
                    {
                        mFrmParent.TxtAcNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                        mFrmParent.TxtAcDesc.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                    }
                }
                if (mTypeDlg == "2")
                {
                    ChooseMultiData(mFrmParent.Grd1, mFrmParent.Grd2);
                }
                if (mTypeDlg == "3")
                {
                    ChooseMultiData(mFrmParent.Grd2, mFrmParent.Grd1);
                }
                this.Close();
            }
        }

        private void ChooseMultiData(iGrid mParentGrid, iGrid mParentGrid2)
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCOAAlreadyChosen(Row, mParentGrid) && !IsCOAAlreadyChosen(Row, mParentGrid2))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mParentGrid.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mParentGrid, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mParentGrid, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mParentGrid, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mParentGrid, Row1, 5, Grd1, Row2, 5);
                        //Sm.CopyGrdValue(mParentGrid, Row1, 6, Grd1, Row2, 9);

                        mParentGrid.Rows.Add();
                        Sm.SetGrdNumValueZero(ref mParentGrid, mParentGrid.Rows.Count - 1, new int[] { 6 });

                        mFrmParent.mProcessInd = false;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account#.");
        }

        private bool IsCOAAlreadyChosen(int Row, iGrid mParentGrd)
        {
            for (int Index = 0; Index < mParentGrd.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mParentGrd, Index, 2),
                    Sm.GetGrdStr(Grd1, Row, 2)
                    )) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }
        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, e.RowIndex, 1);
                Grd1.Cells[e.RowIndex, 6].Value = IsSelected;

                string mRow = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                if (ChkChildInd.Checked)
                {
                    for (int xrow = e.RowIndex + 1; xrow < Grd1.Rows.Count; xrow++)
                    {
                        if (Sm.GetGrdStr(Grd1, xrow, 2).Length >= mRow.Length)
                        {
                            string mRow2 = Sm.Left(Sm.GetGrdStr(Grd1, xrow, 2), mRow.Length);
                            if (mRow2 == mRow)
                            {
                                Grd1.Cells[xrow, 1].Value = IsSelected;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }


        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void SetLueLevel(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            for (int i = 1; i <= Convert.ToInt32(mFrmParent.mMaxAccountCategory); i++)
            {
                if (i == Convert.ToInt32(mFrmParent.mMaxAccountCategory))
                    SQL.AppendLine("Select " + i + " As Col1, " + i + " As Col2 ");
                else
                {
                    SQL.AppendLine("Select " + i + " As Col1, " + i + " As Col2 ");
                    SQL.AppendLine("UNION ALL ");
                }

            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void Process1(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.AcNo, A.Parent, A.Level ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("ORDER BY A.AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Parent", "Level" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            Parent = Sm.DrStr(dr, c[1]),
                            Level = Convert.ToInt32(Sm.DrDec(dr, c[2])),
                        });
                    }
                }
                dr.Close();
            }

        }

        private void Process2(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;
            //var ParentRow = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                        //lCOA[i].ParentRow = ParentRow;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                //ParentRow = j;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                //lCOA[i].ParentRow = ParentRow;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process3(ref List<COA> lCOA)
        {
           
            for (var i = 0; i<Grd1.Rows.Count; i++)
            {
                for (var j = 0; j < lCOA.Count; j++)
                {
                    if(Sm.GetGrdStr(Grd1, i, 2) == lCOA[j].AcNo)
                    {
                        Grd1.Cells[i, 8].Value = lCOA[j].HasChild;
                    }
                }
            
            }
        }


        #endregion

        #endregion

        #region Events

        private void ChkAccountNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(SetLueLevel));
        }

        private void TxtAccountNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }


        #endregion


        #region Class

        private class COA
        {
            public string AcNo { get; set; }
            public string Parent { get; set; }

            public int Level { get; set; }
            public bool HasChild { get; set; }
        }
        #endregion
    }
}
