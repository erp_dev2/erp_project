﻿namespace RunSystem
{
    partial class FrmSalaryAdjustment3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.DtePaidDt = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.MeeDesc = new DevExpress.XtraEditors.MemoExEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.BtnCsv = new DevExpress.XtraEditors.SimpleButton();
            this.BtnRemoveAllEmployee = new DevExpress.XtraEditors.SimpleButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ChkNontaxable = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaidDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaidDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNontaxable.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LueFontSize
            // 
            this.LueFontSize.EditValue = "9";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkNontaxable);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.DtePaidDt);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.MeeDesc);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Size = new System.Drawing.Size(672, 98);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 98);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(672, 375);
            this.Grd1.TabIndex = 16;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(78, 69);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(342, 20);
            this.MeeRemark.TabIndex = 12;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(28, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 14);
            this.label3.TabIndex = 11;
            this.label3.Text = "Remark";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DtePaidDt
            // 
            this.DtePaidDt.EditValue = null;
            this.DtePaidDt.EnterMoveNextControl = true;
            this.DtePaidDt.Location = new System.Drawing.Point(78, 48);
            this.DtePaidDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePaidDt.Name = "DtePaidDt";
            this.DtePaidDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaidDt.Properties.Appearance.Options.UseFont = true;
            this.DtePaidDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaidDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePaidDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePaidDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePaidDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePaidDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePaidDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePaidDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePaidDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePaidDt.Size = new System.Drawing.Size(104, 20);
            this.DtePaidDt.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(26, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Paid On";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDesc
            // 
            this.MeeDesc.EnterMoveNextControl = true;
            this.MeeDesc.Location = new System.Drawing.Point(78, 27);
            this.MeeDesc.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDesc.Name = "MeeDesc";
            this.MeeDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.Appearance.Options.UseFont = true;
            this.MeeDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDesc.Properties.MaxLength = 400;
            this.MeeDesc.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeDesc.Properties.ShowIcon = false;
            this.MeeDesc.Size = new System.Drawing.Size(342, 20);
            this.MeeDesc.TabIndex = 8;
            this.MeeDesc.ToolTip = "F4 : Show/hide text";
            this.MeeDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDesc.ToolTipTitle = "Run System";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(8, 30);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "Description";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(78, 6);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(104, 20);
            this.DteDocDt.TabIndex = 6;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(42, 9);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 14);
            this.label17.TabIndex = 5;
            this.label17.Text = "Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BtnCsv);
            this.panel3.Controls.Add(this.BtnRemoveAllEmployee);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(468, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 94);
            this.panel3.TabIndex = 13;
            // 
            // BtnCsv
            // 
            this.BtnCsv.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCsv.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCsv.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCsv.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.BtnCsv.Appearance.Options.UseBackColor = true;
            this.BtnCsv.Appearance.Options.UseFont = true;
            this.BtnCsv.Appearance.Options.UseForeColor = true;
            this.BtnCsv.Appearance.Options.UseTextOptions = true;
            this.BtnCsv.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCsv.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCsv.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BtnCsv.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCsv.Location = new System.Drawing.Point(0, 40);
            this.BtnCsv.Name = "BtnCsv";
            this.BtnCsv.Size = new System.Drawing.Size(200, 27);
            this.BtnCsv.TabIndex = 14;
            this.BtnCsv.Text = "Generate Employee From Csv File";
            this.BtnCsv.ToolTip = "Remove All Employee";
            this.BtnCsv.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCsv.ToolTipTitle = "Run System";
            this.BtnCsv.Click += new System.EventHandler(this.BtnCsv_Click);
            // 
            // BtnRemoveAllEmployee
            // 
            this.BtnRemoveAllEmployee.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRemoveAllEmployee.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRemoveAllEmployee.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRemoveAllEmployee.Appearance.ForeColor = System.Drawing.Color.Red;
            this.BtnRemoveAllEmployee.Appearance.Options.UseBackColor = true;
            this.BtnRemoveAllEmployee.Appearance.Options.UseFont = true;
            this.BtnRemoveAllEmployee.Appearance.Options.UseForeColor = true;
            this.BtnRemoveAllEmployee.Appearance.Options.UseTextOptions = true;
            this.BtnRemoveAllEmployee.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRemoveAllEmployee.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRemoveAllEmployee.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BtnRemoveAllEmployee.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRemoveAllEmployee.Location = new System.Drawing.Point(0, 67);
            this.BtnRemoveAllEmployee.Name = "BtnRemoveAllEmployee";
            this.BtnRemoveAllEmployee.Size = new System.Drawing.Size(200, 27);
            this.BtnRemoveAllEmployee.TabIndex = 15;
            this.BtnRemoveAllEmployee.Text = "Remove All Employee";
            this.BtnRemoveAllEmployee.ToolTip = "Remove All Employee";
            this.BtnRemoveAllEmployee.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRemoveAllEmployee.ToolTipTitle = "Run System";
            this.BtnRemoveAllEmployee.Click += new System.EventHandler(this.BtnRemoveAllEmployee_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ChkNontaxable
            // 
            this.ChkNontaxable.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkNontaxable.Location = new System.Drawing.Point(185, 5);
            this.ChkNontaxable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkNontaxable.Name = "ChkNontaxable";
            this.ChkNontaxable.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkNontaxable.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkNontaxable.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkNontaxable.Properties.Appearance.Options.UseBackColor = true;
            this.ChkNontaxable.Properties.Appearance.Options.UseFont = true;
            this.ChkNontaxable.Properties.Appearance.Options.UseForeColor = true;
            this.ChkNontaxable.Properties.Caption = "Nontaxable";
            this.ChkNontaxable.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkNontaxable.Size = new System.Drawing.Size(100, 22);
            this.ChkNontaxable.TabIndex = 7;
            // 
            // FrmSalaryAdjustment3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmSalaryAdjustment3";
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaidDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaidDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkNontaxable.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DtePaidDt;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.MemoExEdit MeeDesc;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel3;
        public DevExpress.XtraEditors.SimpleButton BtnRemoveAllEmployee;
        public DevExpress.XtraEditors.SimpleButton BtnCsv;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraEditors.CheckEdit ChkNontaxable;
    }
}