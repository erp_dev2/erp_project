﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWorkCenterWagesFormulation : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, position = "I";
        internal FrmWorkCenterWagesFormulationFind FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmWorkCenterWagesFormulation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Workcenter's Wages Formulation";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                LueFormulationCode.Visible = false;
                SetLueWagesFormulation(ref LueFormulationCode);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Workcenter",

                        //1-5
                        "",
                        "Workcenter",
                        "Formula",
                        "",
                        "Formula",
                        
                        //6-8
                        "Formula",
                        "",
                        "Previous"+Environment.NewLine+"Formula"
                    },
                    new int[] 
                    {
                        //0
                        130, 
                        
                        //1-5
                        20, 250, 130, 20, 250, 
                        
                        //6-8
                        130, 20, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 4, 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 5, 6, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6, 7 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    position = "I";
                    Sm.SetControlReadOnly(DteDocDt, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 8 });
                    Grd1.Cols[5].Text = "Formula";
                    Grd1.Cols[8].Text = "Previous" + Environment.NewLine + "Formula";
                    break;
                case mState.Insert:
                    position = "I";
                    Sm.SetControlReadOnly(DteDocDt, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5 });
                    Grd1.Cols[5].Text = "New" + Environment.NewLine + "Formula";
                    Grd1.Cols[8].Text = "Current" + Environment.NewLine + "Formula";
                    break;
                case mState.Edit:
                    position = "E";
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5 });
                    break;
            }
            DteDocDt.Focus(); 
        }

        private void ClearData()
        {
            DteDocDt.EditValue = null;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWorkCenterWagesFormulationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ShowDataWorkCenter();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteDocDt, "")) return;
            SetFormControl(mState.Edit);
            ShowWWFData(Sm.GetDte(DteDocDt).Substring(0, 8), true);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (position != "E")
                {
                    InsertData();
                }
                else
                {
                    EditData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWagesFormulation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWagesFormulation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueFormulationCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmWorkCenter(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmWagesFormulation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmWagesFormulation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

       
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocDt = Sm.GetDte(DteDocDt).Substring(0,8);

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveWWFDtl(DocDt, Row));

            Sm.ExecCommands(cml);

            ShowData(DocDt);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsDateAlready()||
                IsGrdEmpty()||
                IsFormulationEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need input at least one workcenter.");
                return true;
            }
            return false;
        }

        private bool IsDateAlready()
        {
            var Date = Sm.GetValue(
                "Select Date From TblWorkCenterWagesFormulation Where Date='" + Sm.GetDte(DteDocDt).Substring(0, 8) + "'"
                ) ;
            if (Date.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Workcenter's formulation for this date already exist.");
                return true;
            }
            return false;
        }

        private bool IsFormulationEmpty()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Workcenter Code : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                            "Workcenter Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Formulation still empty.");
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveWWFDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into tblWorkcenterWagesFormulation(Date, WorkCenterCode, WagesFormulationCode, CreateBy, CreateDt) " +
                    "Values(@Date, @WorkCenterCode, @WagesFormulationCode, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParamDt(ref cm, "@Date", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WorkCenterCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWWFDtlForEdit(string DocNo, int Row, string DocDt)
        {
            string userCode = Sm.GetValue("Select Distinct CreateBy From TblWorkcenterWagesFormulation Where Date = '" + DocDt + "' ");
            string createCode = Sm.GetValue("Select Distinct CreateDt From TblWorkcenterWagesFormulation Where Date = '" + DocDt + "' ");

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into tblWorkcenterWagesFormulation(Date, WorkCenterCode, WagesFormulationCode, CreateBy, CreateDt, LastUpBy, LastUpDt) " +
                    "Values(@Date, @WorkCenterCode, @WagesFormulationCode, @CreateBy, @CreateDt, @LastUpBy, CurrentDateTime()) "
            };
            Sm.CmParamDt(ref cm, "@Date", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WorkCenterCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", userCode);
            Sm.CmParam<String>(ref cm, "@CreateDt", createCode);
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            string DocDt = Sm.GetDte(DteDocDt).Substring(0,8);

            var cml = new List<MySqlCommand>();


            cml.Add(DeleteData(DocDt));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveWWFDtlForEdit(DocDt, Row, DocDt));

            Sm.ExecCommands(cml);

            ShowData(DocDt);
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsWCFormulaAlreadyOnProdWages()||
                IsWCFormulaAlreadyOnProdPenalty();
        }

        private MySqlCommand DeleteData(string DocDt)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblWorkcenterWagesFormulation " +
                    "Where Date=@Date"
            };
            Sm.CmParam<String>(ref cm, "@Date", DocDt);
            return cm;
        }

        private bool IsWCFormulaAlreadyOnProdWages()
        {
            string DocDt = Sm.GetDte(DteDocDt).Substring(0,8);
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                string keyCode = Sm.GetValue(
                    "Select DocKey From ( "+
                    "Select Distinct  Concat(A.WagesFormulationCode, C.WorkCenterDocNo) As DocKey " +
                    "From TblProductionWagesHdr A "+
                    "Inner Join TblShopfloorControlHdr C On A.ShopFloorControlDocNo = C.DocNo "+ 
                    "Where A.CancelInd = 'N' And C.Cancelind = 'N' "+
                    "And A.DocDt >= "+DocDt+" "+
                    ")T Where DocKey='"+Sm.GetGrdStr(Grd1, Row, 3)+Sm.GetGrdStr(Grd1, Row, 0)+"' ");
                if(keyCode.Length >0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                           "Workcenter Code : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                           "Workcenter Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                           "Formulation : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                           "already use on production wages. ");
                    return true;
                }
            }
            return false;
        }

        private bool IsWCFormulaAlreadyOnProdPenalty()
        {
            string DocDt = Sm.GetDte(DteDocDt).Substring(0, 8);
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                string keyCode = Sm.GetValue(
                    "Select DocKey From ( " +
                    "Select Distinct  Concat(A.WagesFormulationCode, C.WorkCenterDocNo) As DocKey " +
                    "From TblProductionPenaltyHdr A " +
                    "Inner Join TblShopfloorControlHdr C On A.ShopFloorControlDocNo = C.DocNo " +
                    "Where A.CancelInd = 'N' And C.Cancelind = 'N' " +
                    "And A.DocDt >= " + DocDt + " " +
                    ")T Where DocKey='" + Sm.GetGrdStr(Grd1, Row, 3) + Sm.GetGrdStr(Grd1, Row, 0) + "' ");
                if (keyCode.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                           "Workcenter Code : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine +
                           "Workcenter Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                           "Formulation : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                           "already use on production wages. ");
                    return true;
                }
            }
            return false;
        }

       
        #endregion

        #region  Show Data

        public void ShowData(string DocDt)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowWWF(DocDt);
                ShowWWFData(DocDt, false);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWWF(string DocDt)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocDt", DocDt);
                
                Sm.ShowDataInCtrl(
                        ref cm,"Select Date From TblWorkcenterWagesFormulation Where Date=@DocDt Limit 1;",
                        new string[]{ "Date" },
                        (MySqlDataReader dr, int[] c) =>
                        { Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0])); }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWWFData(string DocDt, bool EditInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.WorkcenterCode, T1.DocName, T1.WagesFormulationCode, T1.WagesFormulationName, ");
            SQL.AppendLine("T2.WagesFormulationCode As WagesFormulationCode2, T3.WagesFormulationName As WagesFormulationName2 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WorkcenterCode, B.DocName, A.WagesFormulationCode, C.WagesFormulationName ");
            SQL.AppendLine("    From TblWorkcenterWagesFormulation A ");
            SQL.AppendLine("    Inner Join TblWorkcenterHdr B On A.WorkCenterCode = B.DocNo ");
            SQL.AppendLine("    Inner Join TblWagesFormulationHdr C On A.WagesFormulationCode = C.WagesFormulationCode");
            SQL.AppendLine("    Where A.Date=@DocDt ");
            if (EditInd)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select DocNo As WorkcenterCode, DocName, Null As WagesFormulationCode, Null As WagesFormulationName ");
                SQL.AppendLine("    From TblWorkcenterHdr ");
                SQL.AppendLine("    Where ActiveInd='Y' ");
                SQL.AppendLine("    And DocDt<=@DocDt ");
                SQL.AppendLine("    And DocNo Not In ");
                SQL.AppendLine("        (Select WorkCenterCode From TblWorkcenterWagesFormulation Where Date=@DocDt) ");
            }
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.WorkCenterCode As DocNo, T1.WagesFormulationCode ");
            SQL.AppendLine("    From TblWorkcenterWagesFormulation T1 ");
            SQL.AppendLine("    Inner Join TblWorkcenterHdr T2 ");
            SQL.AppendLine("        On T1.WorkCenterCode=T2.DocNo ");
            SQL.AppendLine("        And T2.DocDt<=@DocDt ");
            SQL.AppendLine("        And T2.ActiveInd = 'Y'");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select T3a.WorkCenterCode, Max(T3a.Date) As Date ");
            SQL.AppendLine("        From TblWorkcenterWagesFormulation T3a ");
            SQL.AppendLine("        Inner Join TblWorkcenterHdr T3b ");
            SQL.AppendLine("            On T3a.WorkCenterCode=T3b.DocNo ");
            SQL.AppendLine("            And T3b.DocDt<=@DocDt ");
            SQL.AppendLine("            And T3b.ActiveInd = 'Y' ");
            SQL.AppendLine("        Where T3a.Date<@DocDt ");
            SQL.AppendLine("        Group By T3a.WorkCenterCode ");
            SQL.AppendLine("    ) T3 ");
            SQL.AppendLine("        On T1.WorkCenterCode=T3.WorkCenterCode ");
            SQL.AppendLine("        And T1.Date=T3.Date ");
            SQL.AppendLine(") T2 On T1.WorkcenterCode=T2.DocNo ");
            SQL.AppendLine("Left Join TblWagesFormulationHdr T3 On T2.WagesFormulationCode = T3.WagesFormulationCode ");
            SQL.AppendLine("Order By T1.DocName;");

            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "WorkCenterCode",  
                    //1-5
                    "DocName", "WagesFormulationCode", "WagesFormulationName", "WagesFormulationCode2", "WagesFormulationName2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDataWorkCenter()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocName, B.WagesFormulationCode, C.WagesFormulationName ");
            SQL.AppendLine("From TblWorkcenterHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.WorkCenterCode As DocNo, T1.WagesFormulationCode ");
            SQL.AppendLine("    From TblWorkcenterWagesFormulation T1 ");
            SQL.AppendLine("    Inner Join TblWorkcenterHdr T2 ");
            SQL.AppendLine("        On T1.WorkCenterCode=T2.DocNo ");
            SQL.AppendLine("        And T2.DocDt<=@DocDt ");
            SQL.AppendLine("        And T2.ActiveInd = 'Y'");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select T3a.WorkCenterCode, Max(T3a.Date) As Date ");
            SQL.AppendLine("        From TblWorkcenterWagesFormulation T3a ");
            SQL.AppendLine("        Inner Join TblWorkcenterHdr T3b ");
            SQL.AppendLine("            On T3a.WorkCenterCode=T3b.DocNo ");
            SQL.AppendLine("            And T3b.DocDt<=@DocDt ");
            SQL.AppendLine("            And T3b.ActiveInd = 'Y' ");
            SQL.AppendLine("        Where T3a.Date<@DocDt ");
            SQL.AppendLine("        Group By T3a.WorkCenterCode ");
            SQL.AppendLine("    ) T3 ");
            SQL.AppendLine("        On T1.WorkCenterCode=T3.WorkCenterCode ");
            SQL.AppendLine("        And T1.Date=T3.Date ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblWagesFormulationHdr C On B.WagesFormulationCode = C.WagesFormulationCode");
            SQL.AppendLine("Where A.DocDt<=@DocDt ");
            SQL.AppendLine("And A.ActiveInd = 'Y' ");
            SQL.AppendLine("Order By A.DocName, C.WagesFormulationName; "); 

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt)); 

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "DocNo", "DocName", "WagesFormulationCode", "WagesFormulationName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #endregion

        #region Additional method

        private void SetLueWagesFormulation(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WagesFormulationCode As Col1, Concat(WagesFormulationName, ifnull(Concat(' (', UomCode, ')'), '') ) As Col2 ");
            SQL.AppendLine("From TblWagesFormulationHdr Order By WagesFormulationCode  ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "FormulationCode", "Wages Formulation Name", "Col2", "Col1");
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 3));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #region Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
            if (Sm.GetDte(DteDocDt).Length>0 && BtnSave.Enabled) ShowDataWorkCenter();
        }

        private void LueFormulationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueFormulationCode, new Sm.RefreshLue1(SetLueWagesFormulation));
        }

        private void LueFormulationCode_Leave(object sender, EventArgs e)
        {
            if (LueFormulationCode.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueFormulationCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 3].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueFormulationCode);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueFormulationCode.GetColumnValue("Col2");
                }
                LueFormulationCode.Visible = false;
            }
        }
        #endregion
    }
}
