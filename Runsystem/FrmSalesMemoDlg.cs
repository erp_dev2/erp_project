﻿#region Update
/*
    13/03/2020 [DITA/KSM] new apps
    26/05/2020 [VIN/KSM] Tambah Product Name
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesMemoDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesMemo mFrmParent;
        private string mSQL = string.Empty;


        #endregion

        #region Constructor

        public FrmSalesMemoDlg(FrmSalesMemo FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
                Sl.SetLueItGrpCode(ref LueItGrpCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "Local Code",
                        "", 
                        "Group",
                        
                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Item's Category",
                        "Item's Sub Category Code",
                        "Sub Category",
                        
                        //11
                        "UoM"
                      
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 120, 20, 170,
                        
                        //6-10
                        200, 150, 150, 80, 150,
                        
                        //11
                        80
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4});
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 10, 11 });
          
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

                SQL.AppendLine("Select A.ItCode, D.ItCtName, A.SalesUomCode, A.ItName, A.ForeignName, ");
                SQL.AppendLine("A.ItScCode, B.ItScName, A.ItCodeInternal, A.ItGrpCode, C.ItGrpName  ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Left Join TblItemSubCategory B On A.ItScCode = B.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup C On A.ItGrpCode=C.ItGrpCode ");
                SQL.AppendLine("Inner Join TblItemCategory D On A.ItCtCode=D.ItCtCode ");
                SQL.AppendLine("Where A.ActInd = 'Y' ");
                SQL.AppendLine("And A.SalesItemInd = 'Y' ");

            mSQL = SQL.ToString();

        }


        override protected void ShowData()
        {
            try
            {
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItGrpCode), "A.ItGrpCode", true);

              
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "ItGrpName", "ItName", "ForeignName",  "ItCtName",   
                            
                            //6-8
                            "ItScCode", "ItScName", "SalesUomCode",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                         
                        }, true, false, false, false
                    );
               
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void ChooseData()
        {
            
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 11);
                       
                
                        mFrmParent.Grd1.Rows.Add();
                     }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 5), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

           
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }


        private void LueItGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItGrpCode, new Sm.RefreshLue2(Sl.SetLueItGrpCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's group");
        }

        #endregion

        #endregion


    }
}
