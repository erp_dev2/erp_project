﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest2Dlg3 : RunSystem.FrmBase4
    {
        //update [Ari] 04/05/17 1.tambah localdocno, 2.yang ditampilkan Ship Instruction dan DR
        #region Field

        private FrmMaterialRequest2 mFrmParent;
        private string mSQL = string.Empty;
        private string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequest2Dlg3(FrmMaterialRequest2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Local"+Environment.NewLine+"Document",
                        "Date",
                        "",
                        "Status",
                       
                        //6-10
                        "Customer Code",
                        "Customer",
                        "Stuffing Date",
                        "Remark",
                        "Type"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 100, 80, 20, 80, 
                        
                        //6-10
                        80, 200, 80, 300, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 });
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X.DocNo, X.LocalDocNo, X.DocDt, X.SpStatus, X.CtCode, X.CtName, X.SpStfDt, X.Remark, X.DocType From ( "); 
            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, Case When B.Status = 'R' Then 'Released' ");
            SQL.AppendLine("When B.Status = 'F' Then 'FulFilled' Else 'Planning' End As SpStatus, ");
            SQL.AppendLine("B.CtCode, C.CtName, A.SpStfDt, A.Remark2 As Remark, '1' AS DocType ");
            SQL.AppendLine("from TblSIhdr A ");
            SQL.AppendLine("Inner Join TblSP B On A.SpDocno = B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode = C.CtCode ");
            SQL.AppendLine("Where B.Status <> 'C' ");
            SQL.AppendLine("UNION ALL ");
            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, Case A.processInd When 'O' Then 'Outstanding' ");
            SQL.AppendLine("When 'F' Then 'FulFilled' When 'M' Then 'Manual FulFilled' When 'P' Then 'Partial FulFilled' ");
            SQL.AppendLine("When 'C' Then 'Cancelled' End As SpStatus, A.CtCode, B.CtName, Null As SpStfDt , A.Remark, '2' AS DocType ");
            SQL.AppendLine("From TblDRHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("Where A.processInd <> 'C' And A.cancelInd = 'N' ");
            SQL.AppendLine(" )X ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "X.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "X.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), new string[] { "X.CtCode" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                             
                            //1-5
                            "LocalDocNo",
                            "DocDt", 
                            "SPStatus", 
                            "CtCode",
                            "CtName", 
                           
                            //6-8
                            "SpStfDt",
                            "Remark",
                            "DocType"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtSIDocNo.EditValue =  Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.docType = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex,1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10) == "1")
                {
                    e.DoDefault = false;
                    var f = new FrmSI(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    e.DoDefault = false;
                    var f = new FrmDR(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }

            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 10) == "1")
                {
                    var f = new FrmSI(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDR(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

       

      
    }
}
