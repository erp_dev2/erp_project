﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptTrialBalance : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private List<TblCOA> ListData = new List<TblCOA>();
        private List<TblCOA> ListCOA = new List<TblCOA>();

        #endregion

        #region Constructor

        public FrmRptTrialBalance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                //InitControl();
                SetGrd();
                PrepareCOA();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        "COA's Account",
                        "", "", "", "", "Journal#", 
                        "", "Opening", "Debit","Credit", "Balance",
                    },
                    new int[] 
                    {
                        400,
                        150, 100, 150, 100, 140,
                        100, 130, 130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 4, 6 }, false);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || 
                Sm.IsLueEmpty(LueMth, "Month")) 
                return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ClearDataGrid();
                PopulateTree();
                DisplayToGrid();
                ListData.Clear();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void DisplayToGrid()
        {
            Grd1.Rows.Count = 0;
            AddRootNode("Report", "Root", "D");
            //Int32 Gradasi = 255;
            Grd1.BeginUpdate();
            ListCOA.ForEach(Index =>
            {
                if (Index.Parent.Length == 0)
                {
                    AddChildNode(
                        Index.AcDesc + " [" + Index.AcNo + "]",
                        "Root",
                        Index.AcNo,
                        "N",
                        0,
                        0,
                        0,
                        Index.AcType,
                        "",
                        "",
                        Index.CalculateIndicator
                        // , Color.FromArgb(Gradasi, Gradasi, Gradasi)
                        );
                    GetChildNote(
                        Index,
                        (Grd1.Rows.Count - 1).ToString()
                        ); //, Gradasi - 15);
                }

            });
            Grd1.EndUpdate();
        }

        private void ClearDataGrid()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            for (int iRow = 0; iRow < Grd1.Rows.Count; iRow++)
            {
                Grd1.Cells[iRow, 7].Value = 0;
                Grd1.Cells[iRow, 8].Value = 0;
                Grd1.Cells[iRow, 9].Value = 0;
                Grd1.Cells[iRow, 10].Value = 0;
            }
            ListData.Clear();
        }

        private void PopulateTree()
        {
            //bool IsFilterByDay = LueDay.Visible && Sm.GetLue(LueDay).Length != 0;
            //bool IsFilterByEntity = Sm.GetLue(LueEntity).Length != 0;
            bool IsPeriodClosed = Sm.IsDataExist("Select Yr From TblCOAOpeningBalance Where Yr='" + Sm.GetLue(LueYr) + "' Limit 1;");

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");

            SQL.AppendLine("    Select A.AcNo, A.AcDesc, A.Parent, 'Y' As CashInd, B.DocNo As JnNo, ");
            SQL.AppendLine("    Left(B.JnDesc, 50) As JnDesc, B.AcType, ");
            SQL.AppendLine("    B.Period, B.PeriodClose, B.IsRugiLaba, IfNull(B.Amt, 0) As Amount, ");
            SQL.AppendLine("    IfNull(C.Amt, 0)+IfNull(D.Amt, 0)+IfNull(E.Amt, 0) As Opening ");
            SQL.AppendLine("    From tblCOA A ");
            SQL.AppendLine("    Left Join ( ");

            SQL.AppendLine("        Select A.DocNo, A.DocDt, A.JnDesc, ");
            SQL.AppendLine("        Left(A.DocDt, 6)  As Period, ");
            SQL.AppendLine("        'Y' As PeriodClose, ");
            SQL.AppendLine("        'N' As IsRugiLaba, B.AcNo, ");
            SQL.AppendLine("        If(B.DAmt>0, 'D', 'C') As AcType, ");
            SQL.AppendLine("        If(B.DAmt>0, B.DAmt, B.CAmt) As Amt ");
            SQL.AppendLine("        From TblJournalHdr A ");
            SQL.AppendLine("        Inner Join TblJournalDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Where Left(A.DocDt, 4)=@Yr ");
            SQL.AppendLine("        And Substring(A.DocDt, 5, 2) Between @Mth1 And @Mth2 ");

            SQL.AppendLine("        Union All ");

            //Generate Rugi Laba
            SQL.AppendLine("        Select JnNo, '' As JnDt, JnDesc, Period, PeriodClose, 'Y' As IsRugiLaba, ");
            SQL.AppendLine("        AcNo, Case When Amt >= 0 Then 'D' Else 'C' End As AcType, Abs(Amt) As Amt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select '' As JnNo, ");
            SQL.AppendLine("            'Rugi/Laba berjalan' As JnDesc, ");
            SQL.AppendLine("            '3.3' As AcNo, ");
            SQL.AppendLine("            Sum(If(B.DAmt>0, B.DAmt, -1*B.CAmt)) As Amt, ");
            SQL.AppendLine("            Left(A.DocDt, 6) As Period,  ");
            SQL.AppendLine("            'Y' As PeriodClose ");
            SQL.AppendLine("            From TblJournalHdr A ");
            SQL.AppendLine("            Inner Join TblJournalDtl B On A.DocNo=B.DocNo And Left(B.AcNo, 1) In ('4', '5') ");
            SQL.AppendLine("            Where Left(A.DocDt, 4)=@Yr ");
            SQL.AppendLine("            And Substring(A.DocDt, 5, 2) Between @Mth1 And @Mth2 ");
            SQL.AppendLine("            Group By Left(A.DocDt, 6) ");
            SQL.AppendLine("        ) T ");
            SQL.AppendLine("    ) B On A.AcNo=B.AcNo ");

            SQL.AppendLine("    Left Join (");
            SQL.AppendLine("        Select T2.AcNo, T2.Amt As Amt ");
            SQL.AppendLine("        From TblCOAOpeningBalanceHdr T1, TblCOAOpeningBalanceDtl T2 ");
            SQL.AppendLine("        Where T1.Yr=Convert((Cast(@Yr As UNSIGNED)" + (IsPeriodClosed ? "" : "-1") + ") Using utf8) "); ;
            SQL.AppendLine("        And T1.DocNo=T2.DocNo And T1.CancelInd='N' ");
            SQL.AppendLine("    ) C On A.AcNo=C.AcNo ");

            SQL.AppendLine("    Left Join (");
            SQL.AppendLine("            Select T2.AcNo, Sum(T2.DAmt-T2.CAmt) As Amt ");
            SQL.AppendLine("            From TblJournalHdr T1 ");
            SQL.AppendLine("            Inner Join TblJournalDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And Left(T1.DocDt, 6)<@Period ");
            SQL.AppendLine("            And Left(T1.DocDt, 4)=@Yr ");
            SQL.AppendLine("            Group By T2.AcNo ");
            SQL.AppendLine("    ) D On A.AcNo=D.AcNo ");

            SQL.AppendLine("    Left Join (");
            SQL.AppendLine("        Select '3.3' As AcNo, ");
            SQL.AppendLine("        Sum(If(T2.DAmt>0, T2.DAmt, -1*T2.CAmt)) As Amt ");
            SQL.AppendLine("        From TblJournalHdr T1 ");
            SQL.AppendLine("        Inner Join TblJournalDtl T2 On T1.DocNo=T2.DocNo And Left(T2.AcNo, 1) In ('4', '5') ");
            SQL.AppendLine("        And Left(T1.DocDt, 6)<@Period ");
            SQL.AppendLine("        And Left(T1.DocDt, 4)=@Yr ");
            SQL.AppendLine("    ) E On A.AcNo=E.AcNo ");

            SQL.AppendLine("    ) Tbl Where Amount<>0 Or Opening<>0 Order By JnDesc;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Period", Sm.GetLue(LueYr) + "01");
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth1", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Mth2", Sm.GetLue(LueMth));
                //if (IsFilterByDay) Sm.CmParam<String>(ref cm, "@Day", Sm.GetLue(LueDay));

                var dr = cm.ExecuteReader();
                var c = new int[]
                {
                    //0
                    dr.GetOrdinal("AcNo"),

                    //1-5
                    dr.GetOrdinal("AcDesc"),
                    dr.GetOrdinal("Parent"),
                    dr.GetOrdinal("CashInd"),
                    dr.GetOrdinal("JnDesc"),
                    dr.GetOrdinal("Amount"),

                    //6-9
                    dr.GetOrdinal("AcType"),
                    dr.GetOrdinal("Opening"),
                    dr.GetOrdinal("JnNo"),
                    dr.GetOrdinal("IsRugiLaba")
                };
                while (dr.Read())
                {
                    ListData.Add(new TblCOA
                    {
                        AcNo = Sm.DrStr(dr, c[0]),
                        AcDesc = Sm.DrStr(dr, c[1]),
                        Parent = Sm.DrStr(dr, c[2]),
                        CashInd = Sm.DrStr(dr, c[3]),
                        Remark = Sm.DrStr(dr, c[4]),
                        Detail = "Y",
                        Opening = Sm.DrDec(dr, c[7]),
                        Debit = (Sm.CompareStr(Sm.DrStr(dr, c[6]), "D") ? Sm.DrDec(dr, c[5]) : 0),
                        Credit = (Sm.CompareStr(Sm.DrStr(dr, c[6]), "C") ? Sm.DrDec(dr, c[5]) : 0),
                        AcType = Sm.DrStr(dr, c[6]),
                        JnNo = Sm.DrStr(dr, c[8]),
                        IsRugiLaba = Sm.DrStr(dr, c[9])
                    });
                }

            }
        }

        private void PrepareCOA()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = "Select * From TblCOA Order By AcNo;"
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "AcNo", 
                    
                    //1-5    
                    "AcDesc", "Parent", "CashInd", "AcType", "CalcInd" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ListCOA.Add(new TblCOA
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            CashInd = Sm.DrStr(dr, c[3]),
                            AcType = Sm.DrStr(dr, c[4]),
                            CalculateIndicator = Sm.DrStr(dr, c[5])
                        });
                    }
                }
            }
        }

        private void AddRootNode(string text, string key, string AcType)
        {
            iGRow Row;
            Row = Grd1.Rows.Add();
            Row.Key = key;
            Row.TreeButton = iGTreeButtonState.Hidden;
            Row.Cells[0].Value = text;
            Row.Cells[1].Value = "";
            Row.Cells[2].Value = "Y";
            Row.Cells[3].Value = key;
            Row.Cells[4].Value = AcType;
            Row.Cells[7].Value = 0;
            Row.Cells[8].Value = 0;
            Row.Cells[9].Value = 0;
            Row.Cells[10].Value = 0;
        }

        private void UpdateParent(string ParentRow, decimal Opening, decimal Debit, decimal Credit, string AcType, string CalculateIndicator, string IsRugiLaba)
        {
            if (ParentRow.Length != 0)
            {
                int Row = int.Parse(ParentRow);
                Grd1.Cells[Row, 7].Value = Sm.GetGrdDec(Grd1, Row, 7) + Opening;
                Grd1.Cells[Row, 8].Value = Sm.GetGrdDec(Grd1, Row, 8) + Debit;
                Grd1.Cells[Row, 9].Value = Sm.GetGrdDec(Grd1, Row, 9) + Credit;
                Grd1.Cells[Row, 10].Value = Sm.GetGrdDec(Grd1, Row, 7) + (Sm.GetGrdDec(Grd1, Row, 8) - Sm.GetGrdDec(Grd1, Row, 9));
                UpdateParent(Sm.GetGrdStr(Grd1, Row, 3), Opening, Debit, Credit, Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 6), IsRugiLaba);
            }
            else
                if (Sm.CompareStr(IsRugiLaba, "N"))
                    Grd1.Cells[0, 10].Value = Sm.GetGrdDec(Grd1, 0, 6) + Sm.GetGrdDec(Grd1, 0, 7) + Sm.GetGrdDec(Grd1, 0, 8) - Sm.GetGrdDec(Grd1, 0, 9);
        }

        private void AddChildNode(string text, string ParentRow, string key, string IsDetail, decimal Opening, decimal Debit, decimal Credit, string AcType, string AcNo, string JnNo, string CalcInd)
            //, Color CurrentColor)
        {
            Grd1.Rows[ParentRow].TreeButton = iGTreeButtonState.Visible;
            iGRow Row;
            Row = Grd1.Rows.Add();
            Row.Key = key;
            Row.TreeButton = iGTreeButtonState.Hidden;
            Row.Level = Grd1.Rows[ParentRow].Level + 1;
            Row.VisibleUnderGrouping = Grd1.Rows[ParentRow].VisibleUnderGrouping && Grd1.Rows[ParentRow].Expanded;
            Row.Cells[0].Value = text;
            Row.Cells[1].Value = ParentRow;
            Row.Cells[2].Value = IsDetail;
            Row.Cells[3].Value = AcNo;
            Row.Cells[4].Value = AcType;
            Row.Cells[5].Value = JnNo;
            Row.Cells[6].Value = CalcInd;
            Row.Cells[7].Value = Opening;
            Row.Cells[8].Value = Debit;
            Row.Cells[9].Value = Credit;
            Row.Cells[10].Value = Opening + (Debit - Credit);
            //Row.BackColor = CurrentColor;
            //Row.ForeColor = (Sm.CompareStr(IsDetail, "Y")) ? Color.DarkRed : Color.Black;
            //Row.Expanded = false;
        }

        private void GetChildNote(TblCOA MyCOA, string ParentRow) //, Int32 Gradasi)
        {
            foreach (var Index in ListCOA.Where(x => x.Parent == MyCOA.AcNo))
            {
                //ListCOA.ForEach(Index =>
                //{
                //    if (Sm.CompareStr(Index.Parent, MyCOA.AcNo))
                //    {
                AddChildNode(
                    Index.AcDesc + " [" + Index.AcNo + "]",
                    Index.Parent,
                    Index.AcNo,
                    "N",
                    0,
                    0,
                    0,
                    Index.AcType,
                    ParentRow,
                    "",
                    Index.CalculateIndicator //,
                    //Color.FromArgb(Gradasi, Gradasi, Gradasi)
                    );
                string CurParent = (Grd1.Rows.Count - 1).ToString();
                int iCount = 0;
                bool FirstOpening = true;
                //ListData.ForEach(Index2 =>
                //{
                //    if ((Sm.CompareStr(Index2.AcNo, Index.AcNo)) && (Index2.Opening != 0 || Index2.Debit != 0 || Index2.Credit != 0))
                //    {
                foreach (var Index2 in ListData.Where(x => x.AcNo == Index.AcNo && (x.Opening != 0 || x.Debit != 0 || x.Credit != 0)))
                {
                    iCount++;
                    if ((Index2.Debit != 0) || (Index2.Credit != 0))
                        AddChildNode(
                            Index2.Remark + " [" + Index2.AcNo + "]",
                            Index2.AcNo,
                            Index2.AcNo + iCount.ToString(),
                            "Y",
                            0,
                            Index2.Debit,
                            Index2.Credit,
                            Index2.AcType,
                            CurParent,
                            Index2.JnNo,
                            "N"
                            //, Color.FromArgb(Gradasi, Gradasi, Gradasi)
                            );
                    UpdateParent(
                        CurParent,
                        (FirstOpening ? Index2.Opening : 0),
                        Index2.Debit,
                        Index2.Credit,
                        Index2.AcType,
                        Index.CalculateIndicator,
                        Index2.IsRugiLaba
                        );
                    FirstOpening = false;
                }
                //    }
                //});
                GetChildNote(Index, CurParent) ; //, Gradasi - 20);
                //    }
                //});
            };
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        //private void ShowJournal(string JnNo, string AcNo)
        //{
        //    Grd2.Rows.Count = 0;
        //    var cm = new SqlCommand();
        //    using (var cn = new SqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandText =
        //            "SELECT A.JnDesc, A.AcNo, B.AcDesc, A.AcType, A.Amt, CONVERT(varchar, CONVERT(datetime, A.JnDate, 102), 106) As JnDate " +
        //            "FROM vwJournal A LEFT JOIN TblCOA B ON A.AcNo=B.AcNo WHERE A.JnNo='" + JnNo + "' ORDER BY A.AcNo";
        //        var dr = cm.ExecuteReader();
        //        var c = new int[]
        //                {
        //                    dr.GetOrdinal("JnDesc"),

        //                    dr.GetOrdinal("AcNo"),
        //                    dr.GetOrdinal("AcDesc"),
        //                    dr.GetOrdinal("AcType"),
        //                    dr.GetOrdinal("Amt"),
        //                    dr.GetOrdinal("JnDate")
        //                };

        //        if (dr.HasRows)
        //        {
        //            decimal TotalDebit = 0;
        //            decimal TotalCredit = 0;
        //            while (dr.Read())
        //            {
        //                LblJnDesc.Text = Sm.DrStr(dr, c[0]) + " " + Sm.DrStr(dr, c[5]);
        //                Grd2.Rows.Count++;
        //                Grd2.Cells[Grd2.Rows.Count - 1, 0].Value = Sm.DrStr(dr, c[1]);
        //                Grd2.Cells[Grd2.Rows.Count - 1, 1].Value = Sm.DrStr(dr, c[2]);
        //                if (Sm.DrStr(dr, c[3]) == "D")
        //                {
        //                    TotalDebit = TotalDebit + Sm.DrDec(dr, c[4]);
        //                    Grd2.Cells[Grd2.Rows.Count - 1, 2].Value = Sm.DrDec(dr, c[4]);
        //                    Grd2.Cells[Grd2.Rows.Count - 1, 3].Value = "";
        //                }
        //                else
        //                {
        //                    TotalCredit = TotalCredit + Sm.DrDec(dr, c[4]);
        //                    Grd2.Cells[Grd2.Rows.Count - 1, 2].Value = "";
        //                    Grd2.Cells[Grd2.Rows.Count - 1, 3].Value = Sm.DrDec(dr, c[4]);
        //                }
        //                if (Sm.DrStr(dr, c[1]) == AcNo)
        //                    Grd2.Rows[Grd2.Rows.Count - 1].ForeColor = Color.DarkBlue;
        //                else
        //                    Grd2.Rows[Grd2.Rows.Count - 1].ForeColor = Color.Black;
        //            }
        //            Grd2.Rows.Count++;
        //            Grd2.Cells[Grd2.Rows.Count - 1, 1].Value = "Total";
        //            Grd2.Cells[Grd2.Rows.Count - 1, 2].Value = TotalDebit;
        //            Grd2.Cells[Grd2.Rows.Count - 1, 3].Value = TotalCredit;
        //            Grd2.Rows[Grd2.Rows.Count - 1].ForeColor = Color.Red;
        //            Grd2.Rows[Grd2.Rows.Count - 1].BackColor = Color.Silver;
        //        }
        //    }
        //}

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            //if (Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            //    ShowJournal(Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
        }

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private class TblCOA
        {
            public string AcNo { set; get; }
            public string Parent { set; get; }
            public string AcDesc { set; get; }
            public string AcType { set; get; }
            public string CashInd { set; get; }
            public string Remark { set; get; }
            public string Detail { set; get; }
            public string JnNo { set; get; }
            public decimal Opening { set; get; }
            public decimal Debit { set; get; }
            public decimal Credit { set; get; }
            public decimal Closing { set; get; }
            public string CalculateIndicator { set; get; }
            public string IsRugiLaba { set; get; }
        }
    }
}
