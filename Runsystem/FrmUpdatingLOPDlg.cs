﻿#region Update
/*
    26/07/2018 [HAR] tambah info target awal
    07/08/2018 [WED] filter DocDt belum masuk ke query
    14/08/2018 [WED] TblLOPHdr.Status --> TblLOPHdr.ProcessInd
    06/05/2020 [IBL/IMS] Tambah kolom Quotation Letter#
    16/02/2023 [MYA/MNET] Merubah kolom PIC Name menjadi PIC Sales pada saat loop LOP di menu Updating List Of Project
    24/02/2023 [WED/MNET] bug source kolom PIC masih ambil dari employee, padahal parameter IsLOPShowPICSales sudah Y
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUpdatingLOPDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmUpdatingLOP mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmUpdatingLOPDlg(FrmUpdatingLOP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                GetParameter();
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Status",
                        "Project Name",
                        //6-10
                        "Customer",
                        "Project Type",
                        "City",
                        "PIC Code",
                        (mFrmParent.mIsLOPShowPICSales ? "PIC Sales" : "PIC Name"),
                        //11-13
                        "Estimated Value",
                        "Quotation Letter#",
                        "Remark",
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        160, 100, 60, 80, 180, 
                        //6-10
                        150, 100, 100, 100, 180, 
                        //11-12
                        120, 120, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 11 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, Case A.Status When 'P' Then 'On Progress' When 'C' Then 'Cancelled' When 'L' Then 'Closed' End Status,  ");
            SQL.AppendLine("A.ProjectName, B.Ctname, C.OptDesc, E.Cityname, A.PICCode, A.EstValue, A.Remark, ");
            if (mFrmParent.mIsLOPShowPICSales) SQL.AppendLine("D.UserName PICName, ");
            else SQL.AppendLine("D.Empname As PICName,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, E.CityName, A.QtLetterNo  ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.* From TblLOPHdr T Where T.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (T.SiteCode Is Null Or ( ");
                SQL.AppendLine("    T.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(T.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblCustomer B  On A.CtCode = B.CtCode  ");
            SQL.AppendLine("Inner Join TblOption C  On A.ProjectType = C.OptCode And C.OptCat ='ProjectType' ");
            if (mFrmParent.mIsLOPShowPICSales) SQL.AppendLine("Inner Join TblUser D On A.PICCode = D.UserCode ");
            else SQL.AppendLine("Inner Join TblEmployee D  On A.picCode = D.EmpCode   ");
            SQL.AppendLine("Left Join TblCity E On A.CityCode = E.CityCode ");
            SQL.AppendLine("Where A.cancelInd = 'N' And A.ProcessInd = 'P' And A.Status = 'A' ");
          

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "A.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[] 
                    { 
                         //0
                          "DocNo",
                         //1-5
                         "DocDt", "CancelInd", "Status", "ProjectName", "Ctname", 
                         //6-10
                         "OptDesc", "CityName", "PICCode", "PICName", "EstValue", 
                         //11-12
                          "QtLetterNo","Remark"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                Sm.ClearGrd(mFrmParent.Grd2, true);
                
                int Row = Grd1.CurRow.Index;
                string LOPDocNo = Sm.GetGrdStr(Grd1, Row, 1);

                mFrmParent.TxtLOPDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtCtName.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.TxtType.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.TxtCityName.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                mFrmParent.TxtPIC.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                mFrmParent.TxtEstimated.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 11), 0);
                mFrmParent.TxtQtLetterNo.EditValue = Sm.GetGrdStr(Grd1, Row, 12);
                mFrmParent.ShowUpdatingLOPHistory(LOPDocNo);
                mFrmParent.ShowUpdatingLOPDtl2(LOPDocNo);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
           
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
           
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Additional Method

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

      
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");

        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

       
    }
}
