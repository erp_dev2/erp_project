﻿#region Update
/*
    11/11/2019 [TKG/SIER] Tambah site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJobDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmJob mFrmParent; private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmJobDlg(FrmJob FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-4
                        "",
                        "Site's Code",
                        "Site's Name"
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3 });
            Sm.SetGrdProperty(Grd1, true);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SiteCode, SiteName From TblSite Where 1=1 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (SiteCode Is Null Or ( ");
                SQL.AppendLine("    SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By SiteName;");

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();
                string SiteCode = string.Empty;

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        SiteCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        if (SiteCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += "(SiteCode<>@SiteCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@SiteCode0" + r.ToString(), SiteCode);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And (" + Filter + ")";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtSiteCode.Text, new string[] { "SiteCode", "SiteName" });
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]{ "SiteCode", "SiteName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);

                        mFrmParent.Grd1.Rows.Add();

                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 site.");
        }

        private bool IsCodeAlreadyChosen(int Row)
        {
            var SiteCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r< mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 1), SiteCode)) return true;
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSiteCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Site");
        }

        #endregion

        #endregion

    }
}
