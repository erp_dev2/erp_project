﻿#region Update
/* 
    09/11/2019 [TKG/SIER] New application
    07/12/2019 [TKG/SIER] feature import item
    06/01/2020 [TKG/SIER] tambah customer, panjang, lebar, tinggi, berat. tampilkan lot/bin. sembunyikan qty 2 dan qty 3.
    07/01/2020 [DITA/SIER] Bug Index grd out of range
    15/01/2020 [WED/SIER] tambah approval, type : StockInbound
    24/02/2020 [DITA/SIER] batch diisi customer name
    27/02/2020 [WED/SIER] print out
    02/03/2020 [DITA/SIER] Tambah parameter untuk generate batchno 
    28/02/2020 [VIN/SIER] print out
    05/03/2020 [WED/SIER] approval per warehouse berdasarkan parameter IsStockInboundApprovalBasedOnWhs
    18/03/2020 [IBL/SIER] tambah field inputan local document
    08/04/2020 [DITA/SIER] Lot pada Stock Inbound menyesuaikan warehouse
    27/04/2020 [DITA/SIER] Dapat memilih item yang sama beberapa kali berdasar parameter IsInboundItemDuplicateAllowed
    06/05/2020 [DITA/SIER] Bug penyesuain lue bin dan lue lot
    26/05/2020 [IBL/SRN] Copy menu inbond dari sier. Field customer di ganti jadi vendor dengan parameter StockInboundFormat
    25/06/2020 [DITA/SIER] Tambah inputan net weight, gross weight, length, width, height
    25/06/2020 [DITA/SIER] Tamabh kolom Total net weight dan net gross
    25/06/2020 [DITA/SIER] tambah field proses (Draft/Final)
    25/06/2020 [DITA/SIER] kolom untuk import disesuaikan dan dilengkapi
    07/07/2020 [IBL/SIER] penyesuaian printout
    13/07/2020 [DITA/SIER] Bug tombol delete tidak bisa di hapus item nya saat masih draft
    13/07/2020 [DITA/SIER] Bug : Lot dan Bin tidak bisa diedit saat masih draft
    14/07/2020 [VIN/SIER] Saat status dokumen "Draft", kolom: "Entry time", "Vechile reg number", "Transport type, masih dapat diedit
    28/07/2020 [WED/SIER] tambah inputan THCAL di detail
    30/07/2020 [DITA/SIER] Tambah event double click untuk cancel all item di Grid
    30/07/2020 [DITA/SIER] Bug : Remark tidak tersimpan saat edit data
    11/08/2020 [WED/SRN] print out tambah Price, ambil dari Item Price aktif
    12/08/2020 [ICA/SRN] menambahkan parameter FormPrintOutStockInbound
    31/05/2021 [SET/SIER] hide kolom gross weight, length, width, height, total gross weight, THCAL weight
 *  03/06/2021 [IQA/SIER] menambah kolom Customer 
 *  31/03/2022 [SET/PRODUCT] Merubah format import data dengan tambahan ExpiredDt format "yyyymmdd"
 *  21/12/2022 [VIN/SIER] BUG Import Data 
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmStockInbound : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mInitProcessInd= string.Empty;
        private bool 
            mIsStockInboundAutoGenerateBatchNo = false,
            mIsStockInboundApprovalBasedOnWhs = false,
            mIsInboundItemDuplicateAllowed = false;
        internal FrmStockInboundFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        string mDocType = "52";
        iGCell fCell;
        bool fAccept;
        internal string mStockInboundFormat = string.Empty;

        #endregion

        #region Constructor

        public FrmStockInbound(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();
            SetGrd();

            if (mStockInboundFormat == "1")
                Sl.SetLueCtCode(ref LueCtCode);
            else
                Sl.SetLueVdCode(ref LueCtCode);

            Sl.SetLueOption(ref LueStockStatus, "StockStatus");
            LueStockStatus.Visible = false;
            SetLueLot(ref LueLot, string.Empty);
            LueLot.Visible = false;
            SetLueProcess(ref LueProcessInd);
            //SetLueBin(ref LueBin, string.Empty);
            LueBin.Visible = false;
            DteExpiredDt.Visible = false;
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);
            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }

            if (mStockInboundFormat == "1")
                LblCtVd.Text = "Customer";
            else
                LblCtVd.Text = "Vendor";
 
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 38;
            Grd1.FrozenArea.ColCount = 7;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Item's Name",
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",

                        //11-15
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Stock Status", 
                        "Status",
                        "Expired",
                        "Remark",

                        //21-25
                        "Length",
                        "UoM",
                        "Width",
                        "UoM",
                        "Height",

                        //26-30
                        "UoM",
                        "Weight",
                        "UoM",
                        "Net Weight",
                        "Gross Weight",

                        //31-35
                        "Dimension" + Environment.NewLine + "(Length)",
                        "Dimension" + Environment.NewLine + "(Width)",
                        "Dimension" + Environment.NewLine + "(Height)",
                        "Total" + Environment.NewLine + "Net Weight",
                        "Total" + Environment.NewLine + "Gross Weight",

                        //36
                        "THCAL Weight",
                        "Customer's Item Code"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        50, 0, 20, 80, 20, 
                        
                        //6-10
                        300, 200, 180, 100, 100, 
                        
                        //11-15
                        80, 60, 0, 0, 0, 
                        
                        //16-20
                        0, 0, 100, 100, 300,

                        //21-25
                        80, 80, 80, 80, 80,

                        //26-30
                        80, 80, 80, 80, 90,

                        //31-35
                        80, 80, 80, 100, 100,

                        //36
                        150, 150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 19 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 21, 23, 25, 27, 29, 30, 31, 32, 33, 34, 35, 36 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 8, 13, 14, 15, 16, 17, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 35, 36 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 12, 14, 16, 17, 21, 22, 23, 24, 25, 26, 27, 28, 34, 35 });
            ShowInventoryUomCode();
            Grd1.Cols[20].Move(36);

            Grd1.Cols[37].Move(7);
            Grd2.Cols.Count = 5;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 35, 36 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2) Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);
            if (mNumberOfInventoryUomCode == 3) Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, TmeEntryTm, TxtVehicleRegNo, 
                        TxtTransportType, MeeRemark, TxtLocalDocNo, LueProcessInd
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 7, 9, 10, 11, 13, 15, 18, 19, 20, 29, 30, 31, 32, 33, 36, 37 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
                    BtnImport.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, TmeEntryTm,  TxtVehicleRegNo, 
                        TxtTransportType, MeeRemark, TxtLocalDocNo
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 9, 10, 11, 13, 15, 18, 19, 20, 29, 30, 31, 32, 33, 36, 37 });
                    if (!mIsStockInboundAutoGenerateBatchNo) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 7 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    BtnImport.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    mInitProcessInd = Sm.GetValue("Select ProcessInd From TblStockInboundHdr Where DocNo = @Param;", TxtDocNo.Text);
                    if (Sm.GetLue(LueProcessInd) == "D")
                    {
                        BtnImport.Enabled = true;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueProcessInd, TxtLocalDocNo, MeeRemark, TxtVehicleRegNo, TmeEntryTm, TxtTransportType
                        }, false);
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 9, 10, 11, 13, 15, 18, 19, 20, 29, 30, 31, 32, 33, 36, 37 });
                        if (!mIsStockInboundAutoGenerateBatchNo) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 7 });
                        Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    }
                    else
                    {
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    }
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mInitProcessInd = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueWhsCode, LueCtCode, 
                TmeEntryTm, TxtVehicleRegNo, TxtTransportType, MeeRemark,
                TxtLocalDocNo, LueProcessInd
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 13, 15, 21, 23, 25, 27, 29, 30, 31, 32, 33, 34, 35, 36 });
            Grd2.Rows.Clear();
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmStockInboundFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sm.SetLue(LueProcessInd, "D");
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();   
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmStockInboundDlg(this));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 7, 9, 10, 11, 13, 15, 18, 19, 20, 36 }, e.ColIndex))
                    {
                        if (Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex))
                        {
                            LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e, 9);
                            SetLueLot(ref LueLot, Sm.GetLue(LueWhsCode));
                        }

                        if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex))
                        {
                            LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e, 10);
                            SetLueBin(ref LueBin, Sm.GetGrdStr(Grd1, e.RowIndex, 9));
                        }

                        if (Sm.IsGrdColSelected(new int[] { 18 }, e.ColIndex))
                            LueRequestEdit(Grd1, LueStockStatus, ref fCell, ref fAccept, e, 17);
                        
                        if (e.ColIndex == 19) Sm.DteRequestEdit(Grd1, DteExpiredDt, ref fCell, ref fAccept, e);

                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 13, 15 });
                         }
                //else
                //{
                //    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2)||Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length==0)) 
                //        e.DoDefault = false;
                //}
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(Sm.GetLue(LueProcessInd) == "D" || (mInitProcessInd.Length > 0 && mInitProcessInd == "D"))
                {
                    Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                }
            }
          
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            
                if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && (Sm.GetLue(LueProcessInd) == "D" || (mInitProcessInd.Length > 0 && mInitProcessInd == "D")))
                {
                    Sm.FormShowDialog(new FrmStockInboundDlg(this));
                    if (mIsStockInboundAutoGenerateBatchNo) UpdateBatch();
                 }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11, 13, 15, 36 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 7, 9, 10, 20 }, e);

            if (e.ColIndex == 11)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 11, 13, 15, 12, 14, 16);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 11, 15, 13, 12, 16, 14);
            }

            if (e.ColIndex == 13)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 13, 11, 15, 14, 12, 16);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 13, 15, 11, 14, 16, 12);
            }

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 15, 11, 13, 16, 12, 14);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 15, 13, 11, 16, 14, 12);
            }

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 14)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 13, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 29 || e.ColIndex == 30 || e.ColIndex == 11)
                ComputeTotalWeight(e.RowIndex);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 11, 13, 15 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }

            if (e.ColIndex == 19)
            {
                if (Sm.GetGrdDate(Grd1, 0, 19).Length != 0)
                {
                    var ExpiredDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 19));
                    for (int r = 0; r< Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdStr(Grd1, r, 4).Length != 0) Grd1.Cells[r, 19].Value = ExpiredDt;
                }
            }

            if (e.ColIndex == 18)
            {
                var StockStatus = Sm.GetGrdStr(Grd1, 0, 17);
                var StockStatusDesc = Sm.GetGrdStr(Grd1, 0, 18);
                if (StockStatusDesc.Length != 0)
                {
                    for (int r = 0; r< Grd1.Rows.Count - 1; r++)
                        if (Sm.GetGrdStr(Grd1, r, 4).Length != 0)
                        {
                            Grd1.Cells[r, 17].Value = StockStatus;
                            Grd1.Cells[r, 18].Value = StockStatusDesc;
                        }
                }
            }

            if (e.ColIndex == 1 && BtnSave.Enabled && TxtDocNo.Text.Length > 0 && Sm.GetLue(LueProcessInd) == "F")
            {
                if (Grd1.Rows.Count > 0)
                {
                    bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, 1].Value = !IsSelected;
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "StockInbound", "TblStockInboundHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveStockInboundHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) 
                    cml.Add(SaveStockInboundDtl(DocNo, Row));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count >100000)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Item data entered (" + (Grd1.Rows.Count-1).ToString() + ") exceeds the maximum limit (99.999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsInventoryQtyMandatory = (Sm.GetParameter("IsInventoryQtyMandatory")=="Y");
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Item's name is empty.") ||
                    (!IsInventoryQtyMandatory && 
                    (
                        Sm.IsGrdValueEmpty(Grd1, Row, 11, true, "Quantity (1) is empty.") ||
                        (Sm.GetGrdStr(Grd1, Row, 14).Length > 0 && mNumberOfInventoryUomCode>=2 && Sm.IsGrdValueEmpty(Grd1, Row, 13, true, "Quantity (2) is empty.")) ||
                        (Sm.GetGrdStr(Grd1, Row, 16).Length > 0 && mNumberOfInventoryUomCode==3 && Sm.IsGrdValueEmpty(Grd1, Row, 15, true, "Quantity (3) is empty."))
                    ))
                    ) 
                    return true;

                if (!mIsInboundItemDuplicateAllowed)
                {
                    for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                    {
                        if (Row != Row2 &&
                            Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 4) + Sm.GetGrdStr(Grd1, Row, 7), Sm.GetGrdStr(Grd1, Row2, 4) + Sm.GetGrdStr(Grd1, Row2, 7)))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                                "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                                "Batch# : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                                "Duplicate item in the list."
                                );
                            Sm.FocusGrd(Grd1, Row, 1);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='StockInbound' ");
            if (mIsStockInboundApprovalBasedOnWhs)
                SQL.AppendLine("And T.WhsCode = @WhsCode ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblStockInboundHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            return cm;

        }

        private MySqlCommand SaveStockInboundHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblStockInboundHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, ProcessInd, WhsCode, CtCode, VdCode, EntryTm, VehicleRegNo, LocalDocNo, TransportType, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'O', @ProcessInd, @WhsCode, @CtCode, @VdCode, @EntryTm, @VehicleRegNo, @LocalDocNo, @TransportType, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@EntryTm", Sm.GetTme(TmeEntryTm));
            Sm.CmParam<String>(ref cm, "@VehicleRegNo", TxtVehicleRegNo.Text);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TransportType", TxtTransportType.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockInboundDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockInboundDtl(DocNo, DNo, CancelInd, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, StockStatus, ExpiredDt,  ");
            SQL.AppendLine("NetWeight, GrossWeight, Length, Width, Height, TotalNetWeight, TotalGrossWeight, THCALWeight, CtItCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCode, ");
            SQL.AppendLine("IfNull(@BatchNo, '-'), Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-') , ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @StockStatus, @ExpiredDt, ");
            SQL.AppendLine("@NetWeight, @GrossWeight, @Length, @Width, @Height, @TotalNetWeight, @TotalGrossWeight, @THCALWeight, @CtItCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@StockStatus", Sm.GetGrdStr(Grd1, Row, 17));
            Sm.CmParamDt(ref cm, "@ExpiredDt", Sm.GetGrdDate(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@NetWeight", Sm.GetGrdDec(Grd1, Row, 29));
            Sm.CmParam<Decimal>(ref cm, "@GrossWeight", Sm.GetGrdDec(Grd1, Row, 30));
            Sm.CmParam<Decimal>(ref cm, "@Length", Sm.GetGrdDec(Grd1, Row, 31));
            Sm.CmParam<Decimal>(ref cm, "@Width", Sm.GetGrdDec(Grd1, Row, 32));
            Sm.CmParam<Decimal>(ref cm, "@Height", Sm.GetGrdDec(Grd1, Row, 33));
            Sm.CmParam<Decimal>(ref cm, "@TotalNetWeight", Sm.GetGrdDec(Grd1, Row, 34));
            Sm.CmParam<Decimal>(ref cm, "@TotalGrossWeight", Sm.GetGrdDec(Grd1, Row, 35));
            Sm.CmParam<Decimal>(ref cm, "@THCALWeight", Sm.GetGrdDec(Grd1, Row, 36));
            Sm.CmParam<String>(ref cm, "@CtItCode", Sm.GetGrdStr(Grd1, Row, 37));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @MainCurCode:=(Select ParValue From TblParameter Where ParCode='MainCurCode'); ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, @UserCode, @Dt ");
            SQL.AppendLine("From TblStockInboundHdr A ");
            SQL.AppendLine("Inner Join TblStockInboundDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblStockInboundHdr A ");
            SQL.AppendLine("Inner Join TblStockInboundDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.BatchNo, B.Source, @MainCurCode, 0.00, 0.00, Null, @UserCode, @Dt ");
            SQL.AppendLine("From TblStockInboundHdr A ");
            SQL.AppendLine("Inner Join TblStockInboundDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            string DNo = "'XXXXX'";
            string DocNo = TxtDocNo.Text;
           
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
        
            if (Sm.GetLue(LueProcessInd) == "D")
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid2() || IsInsertedDataNotValid()) return;

                cml.Add(EditStockInboundHdr());
                cml.Add(DeleteStockInboundDtl());
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                        cml.Add(SaveStockInboundDtl(DocNo, Row));
            }
            if (Sm.GetLue(LueProcessInd) == "F")
            {
                if (mInitProcessInd == "D")
                {
                    if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid2() || IsInsertedDataNotValid()) return;

                    cml.Add(EditStockInboundHdr());
                    cml.Add(DeleteStockInboundDtl());
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                            cml.Add(SaveStockInboundDtl(DocNo, Row));

                    cml.Add(SaveDocApproval(DocNo));
                    cml.Add(SaveStock(DocNo));
                }
                else
                {
                    UpdateCancelledItem();
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                            DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

                    if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid(DNo)) return;

                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                            cml.Add(EditStockInboundDtl(Row));
                    }
                }
            }
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select DNo, CancelInd From TblStockInboundDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {
            return
                IsCancelledItemNotExisted(DNo) ||
                IsStockNotEnough();
        }

        private bool IsEditedDataNotValid2()
        {
            return
                IsDataAlreadyFinal();
        }

        private bool IsDataAlreadyFinal()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblStockInboundHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And ProcessInd = 'F' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already Final.");
                return true;
            }

            return false;
        }


        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXXXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false; 
        }

        private bool IsStockNotEnough()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                    if (IsStockNotEnough(r)) return true;
            }
            return false;
        }

        private bool IsStockNotEnough(int r)
        {
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;
            decimal Stock = 0m, Stock2 = 0m, Stock3 = 0m;

            if (Sm.GetGrdStr(Grd1, r, 11).Length > 0) Qty = Sm.GetGrdDec(Grd1, r, 11);
            if (Sm.GetGrdStr(Grd1, r, 13).Length > 0) Qty2 = Sm.GetGrdDec(Grd1, r, 13);
            if (Sm.GetGrdStr(Grd1, r, 15).Length > 0) Qty3 = Sm.GetGrdDec(Grd1, r, 15);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Qty, Qty2, Qty3 From TblStockSummary ");
            SQL.AppendLine("Where Source=@Source ");
            SQL.AppendLine("And WhsCode=@WhsCode ");
            SQL.AppendLine("And Source=@Lot ");
            SQL.AppendLine("And Bin=@Bin Limit 1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 8));
                Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 9));
                Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 10));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Qty", "Qty2", "Qty3" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Stock = Sm.DrDec(dr, 0);
                        Stock2 = Sm.DrDec(dr, 1);
                        Stock3 = Sm.DrDec(dr, 2);
                    }
                }
                dr.Close();
            }

            if (Qty < Stock)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine + 
                     "Lot : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + 
                     "Bin : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine + 
                     "Stock : " + Sm.FormatNum(Stock, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }

            if (mNumberOfInventoryUomCode>=2 && Qty2 < Stock2)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                     "Lot : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                     "Bin : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                     "Stock : " + Sm.FormatNum(Stock2, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty2, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }

            if (mNumberOfInventoryUomCode >= 3 && Qty3 < Stock3)
            {
                Sm.StdMsg(mMsgType.Warning,
                     "Item's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                     "Item's Name : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                     "Batch# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                     "Source : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                     "Lot : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine +
                     "Bin : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                     "Stock : " + Sm.FormatNum(Stock3, 0) + Environment.NewLine +
                     "Cancelled Quantity : " + Sm.FormatNum(Qty3, 0) + Environment.NewLine + Environment.NewLine +
                     "Not enough stock.");
                return true;
            }
            return false;
        }

        private MySqlCommand DeleteStockInboundDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblStockInboundDtl Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists (Select DocNo From TblStockInboundHdr Where DocNo = @DocNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private MySqlCommand EditStockInboundHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockInboundHdr Set ");
            SQL.AppendLine("ProcessInd=@ProcessInd,LocalDocNo=@LocalDocNo, EntryTm=@EntryTm, VehicleRegNo=@VehicleRegNo,  ");
            SQL.AppendLine("TransportType=@TransportType, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@EntryTm", Sm.GetTme(TmeEntryTm));
            Sm.CmParam<String>(ref cm, "@VehicleRegNo", TxtVehicleRegNo.Text);
            Sm.CmParam<String>(ref cm, "@TransportType", TxtTransportType.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
           

            return cm;
        }

        private MySqlCommand EditStockInboundDtl(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockInboundDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo=@DNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInboundHdr A ");
            SQL.AppendLine("Inner Join TblStockInboundDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            SQL.AppendLine("Update TblStockSummary T Set ");
            SQL.AppendLine("    T.Qty=0.00, T.Qty2=0.00, T.Qty3=0.00, T.LastUpBy=@UserCode, T.LastUpDt=CurrentDateTime()  ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And Source=@Source ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblStockInboundHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, r, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

       
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowStockInboundHdr(DocNo);
                ShowStockInboundDtl(DocNo);
                Sm.ShowDocApproval(DocNo, "StockInbound", ref Grd2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowStockInboundHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ");
            SQL.AppendLine("Case Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("WhsCode, CtCode, VdCode, EntryTm, VehicleRegNo, localDocNo,TransportType, Remark, ProcessInd ");
            SQL.AppendLine("From TblStockInboundHdr Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "StatusDesc", "WhsCode", "CtCode", "EntryTm", 

                        //6-9
                        "VehicleRegNo", "TransportType", "Remark", "LocalDocNo",
 
                        //10-11
                        "VdCode", "ProcessInd"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                        
                        if (mStockInboundFormat == "1")
                            Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[4]));
                        else
                            Sl.SetLueVdCode2(ref LueCtCode, Sm.DrStr(dr, c[10]));

                        Sm.SetTme(TmeEntryTm, Sm.DrStr(dr, c[5]));
                        TxtVehicleRegNo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtTransportType.EditValue = Sm.DrStr(dr, c[7]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[11]));
                    }, true
                );
        }

        private void ShowStockInboundDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.ItCode, B.ItName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, ");
            SQL.AppendLine("A.StockStatus, OptDesc, A.ExpiredDt, A.Remark, ");
            SQL.AppendLine("B.Length, B.LengthUomCode, B.Width, B.WidthUomCode, B.Height, B.HeightUomCode, B.Weight, B.WeightUomCode, ");
            SQL.AppendLine("A.NetWeight, A.GrossWeight, A.Length DimensionLength, A.Width DimensionWidth, A.Height DimensionHeight, ");
            SQL.AppendLine("A.TotalNetWeight, A.TotalGrossWeight, A.THCALWeight, A.CtItCode ");
            SQL.AppendLine("From TblStockInboundDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblOption C On C.OptCat='StockStatus' And A.StockStatus=C.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "ItCode", "ItName", "BatchNo", "Source", 
                    
                    //6-10
                    "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2", 

                    //11-15
                    "InventoryUomCode2", "Qty3", "InventoryUomCode3", "StockStatus", "OptDesc",
 
                    //16-20
                    "ExpiredDt", "Remark", "Length", "LengthUomCode", "Width", 
                    
                    //21-25
                    "WidthUomCode", "Height", "HeightUomCode", "Weight", "WeightUomCode",

                    //26-30
                    "NetWeight", "GrossWeight", "DimensionLength", "DimensionWidth", "DimensionHeight",
 
                    //31-33
                    "TotalNetWeight", "TotalGrossWeight", "THCALWeight", "CtItCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12); 
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 26);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 27);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 28);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 29);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 30);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 31);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 32);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 33);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 34);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 13, 15, 21, 23, 25, 27, 29, 31, 30, 32, 33, 34, 35, 36 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void SetLueProcess(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select 'D' As Col1, 'Draft' As Col2 Union All " +
                "Select 'F' As Col1, 'Final' As Col2 ; ", 
                0, 35, false, true, "Code", "Process", "Col2", "Col1");
        }


        private void ComputeTotalWeight(int Row)
        {
            decimal Qty = 0m, NetWeight = 0m, GrossWeight = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 11);
            if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0) NetWeight = Sm.GetGrdDec(Grd1, Row, 29);
            if (Sm.GetGrdStr(Grd1, Row, 30).Length != 0) GrossWeight = Sm.GetGrdDec(Grd1, Row, 30);

            Grd1.Cells[Row, 34].Value = Qty * NetWeight;
            Grd1.Cells[Row, 35].Value = Qty * GrossWeight;

        }

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<StockInboundHdr>();
            var ldtl = new List<StockInboundDtl>();
            var lsub = new List<SubTotal>();
            string[] TableName = { "StockInboundHdr", "StockInboundDtl", "SubTotal" };
            List<IList> myLists = new List<IList>();
            int Nomor = 1;

            #region Header

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "CompanyLogo",

                    //1-3
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyAddressCity"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new StockInboundHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CtName = LueCtCode.Text,
                            DocNo = TxtDocNo.Text,
                            DocDt = DteDocDt.Text,
                            Reference = string.Empty,
                            LocalDocNo = TxtLocalDocNo.Text,
                            VehicleNo = TxtVehicleRegNo.Text,
                            RecvDt = string.Empty,
                            DeliveryDt = string.Empty,
                            DeliveryFrom = string.Empty,
                            DeliveryTo = string.Empty,
                            Remark = MeeRemark.Text
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if(!Sm.GetGrdBool(Grd1, i, 1)) // tidak di cancel
                {
                    ldtl.Add(new StockInboundDtl()
                    {
                        No = Nomor,
                        ItCode = Sm.GetGrdStr(Grd1, i, 4),
                        ItName = Sm.GetGrdStr(Grd1, i, 6),
                        ForeignName = string.Empty,
                        BatchNo = Sm.GetGrdStr(Grd1, i, 7),
                        Lot = Sm.GetGrdStr(Grd1, i, 9),
                        Bin = Sm.GetGrdStr(Grd1, i, 10),
                        ExpDt = Sm.GetGrdStr(Grd1, i, 19).Length == 0 ? string.Empty : Convert.ToDateTime(Sm.GetGrdStr(Grd1, i, 19)).ToString("dd/MMM/yyyy"),
                        Qty = Sm.GetGrdDec(Grd1, i, 11),
                        UPrice = 0m,
                        Uom = Sm.GetGrdStr(Grd1, i, 12),
                        Length = Sm.GetGrdDec(Grd1, i, 21),
                        Width = Sm.GetGrdDec(Grd1, i, 23),
                        Height = Sm.GetGrdDec(Grd1, i, 25),
                        Weight = Sm.GetGrdDec(Grd1, i, 27),
                        Remark = Sm.GetGrdStr(Grd1, i, 20)
                    });
                    Nomor += 1;
                }
            }

            #endregion

            #region Get Unit Price & Foreign Name
            if (mStockInboundFormat != "1")
            {
                if (ldtl.Count > 0)
                {
                    string ItCodes = string.Empty;
                    string ItCode = string.Empty;

                    foreach(var x in ldtl)
                    {
                        if (ItCodes.Length > 0) ItCodes += ",";
                        if (ItCode.Length > 0) ItCode += ",";
                        ItCodes += string.Concat(x.ItCode, "#", x.Uom);
                        ItCode += x.ItCode;
                    }

                    if (ItCode.Length > 0)
                    {
                        GetForeignName(ref ldtl, ItCode);
                    }

                    if (ItCodes.Length > 0)
                    {
                        GetUPrice(ref ldtl, ItCodes);
                        GetSubTotal(ref ldtl, ref lsub);
                    }
                }
            }
            #endregion

            myLists.Add(ldtl); // dikeluarin dari region detail karena dipakai di region GetUnitPrice & GetForeignName
            myLists.Add(lsub);

            if (Sm.Left(TxtStatus.Text.ToUpper(), 1) == "A")
            {
                string RptName = Sm.GetParameter("FormPrintOutStockInbound");
                if (RptName.Length > 0)
                    Sm.PrintReport(RptName, myLists, TableName, false);
            }
            else
                Sm.PrintReport("StockPackingList", myLists, TableName, false);
        }

        private void GetForeignName(ref List<StockInboundDtl> ldtl, string ItCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select ItCode, ForeignName ");
            SQL.AppendLine("From TblItem ");
            SQL.AppendLine("Where Find_In_set(ItCode, @ItCode); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "ForeignName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in ldtl.Where(w => w.ItCode == Sm.DrStr(dr, c[0])))
                        {
                            x.ForeignName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void GetUPrice(ref List<StockInboundDtl> ldtl, string ItCodes)
        {
            var SQLU = new StringBuilder();
            var cmu = new MySqlCommand();

            SQLU.AppendLine("Select Distinct T2.ItCode, T2.UPrice ");
            SQLU.AppendLine("From ");
            SQLU.AppendLine("( ");
            SQLU.AppendLine("    Select Distinct Concat(A.DocNo, B.ItCode) Variances, A.DocNo, B.ItCode ");
            SQLU.AppendLine("    From TblItemPriceHdr A ");
            SQLU.AppendLine("    Inner JOin TblItemPriceDtl B On A.DocNo = B.DocNo ");
            SQLU.AppendLine("        And A.ActInd = 'Y' ");
            SQLU.AppendLine("        And A.CurCode = (Select ParValue From TblParameter Where Parcode = 'MainCurCode') ");
            SQLU.AppendLine("        And Find_In_set(Concat(B.ItCode, '#', A.PriceUomCode), @Param) ");
            SQLU.AppendLine(") T1 ");
            SQLU.AppendLine("Inner Join TblItemPriceDtl T2 On T1.DocNo = T2.DocNo And T2.ItCode = T2.ItCode; ");

            using (var cnu = new MySqlConnection(Gv.ConnectionString))
            {
                cnu.Open();
                cmu.Connection = cnu;
                cmu.CommandText = SQLU.ToString();
                Sm.CmParam<String>(ref cmu, "@Param", ItCodes);
                var dru = cmu.ExecuteReader();
                var cu = Sm.GetOrdinal(dru, new string[] { "ItCode", "UPrice" });
                if (dru.HasRows)
                {
                    while (dru.Read())
                    {
                        foreach (var x in ldtl.Where(w => w.ItCode == Sm.DrStr(dru, cu[0])))
                        {
                            x.UPrice = Sm.DrDec(dru, cu[1]);
                        }
                    }
                }
                dru.Close();
            }
        }

        private void GetSubTotal(ref List<StockInboundDtl> ldtl, ref List<SubTotal> lsub)
        {
            decimal TotalQty = 0m, TotalUPrice = 0m;

            foreach(var x in ldtl)
            {
                TotalQty += x.Qty;
                TotalUPrice += x.UPrice;
            }

            lsub.Add(new SubTotal() 
            {
                Qty = TotalQty,
                UPrice = TotalUPrice
            });
        }
        
        private void UpdateBatch()            
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if(Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                    Grd1.Rows[Row].Cells[7].Value = LueCtCode.Text;
            }

        }

        public static void SetLueLot(ref LookUpEdit Lue, string WhsCode)
        {
            if (WhsCode.Length > 0)
                 Sm.SetLue1(ref Lue, "Select Distinct A.Lot As Col1 From TblLotHdr A Inner Join TblWhsLotDtl B On A.Lot = B.Lot And B.WhsCode = '" + WhsCode + "' Where A.ActInd='Y' Order By A.Lot ; ", "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue, string Lot)
        {
            if (Lot.Length > 0)
                Sm.SetLue1(ref Lue, "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' And Bin In (Select Bin From TblLotDtl Where Lot='" + Lot + "') Order By Bin;", "Bin");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e,
          int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, Col).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
            
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += ((SQL.Length != 0?",":"") + "'" + Sm.GetGrdStr(Grd1, Row, 4) + "'");
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }


        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsStockInboundAutoGenerateBatchNo = Sm.GetParameterBoo("IsStockInboundAutoGenerateBatchNo");
            mIsStockInboundApprovalBasedOnWhs = Sm.GetParameterBoo("IsStockInboundApprovalBasedOnWhs");
            mIsInboundItemDuplicateAllowed = Sm.GetParameterBoo("IsInboundItemDuplicateAllowed");
            mStockInboundFormat = Sm.GetParameter("StockInboundFormat"); //LblCtVd
        }

        private void ProcessImport()
        {
            Cursor.Current = Cursors.WaitCursor;

            var l = new List<Item>();

            ClearGrd();
            try
            {
                ProcessImport1(ref l);
                if (!IsExpiredDtFormatInvalid(ref l))
                {
                    if (l.Count > 0)
                    {

                        ProcessImport2(ref l);
                        ProcessImport3(ref l);
                        ProcessImport4(ref l);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessImport1(ref List<Item> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            string ItCodeTemp = string.Empty, BatchTemp = string.Empty, LotTemp = string.Empty, BinTemp = string.Empty, StatusTemp = string.Empty, RemarkTemp = string.Empty, ExpiredDt = string.Empty;
            decimal QtyTemp = 0m, NetWeightTemp = 0m, GrossWeightTemp = 0m, LengthTemp = 0m, WidthTemp = 0m, HeightTemp = 0m ;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            ItCodeTemp = arr[0].Trim();
                            BatchTemp = arr[1].Trim();
                            LotTemp = arr[2].Trim();
                            BinTemp = arr[3].Trim();
                            if (arr[4].Trim().Length > 0)QtyTemp = decimal.Parse(arr[4].Trim());
                            else QtyTemp = 0m;
                            StatusTemp = arr[5].Trim();
                            //if (arr[6].Trim().Length > 0) NetWeightTemp = decimal.Parse(arr[6].Trim());
                            //else NetWeightTemp = 0m;
                            //if (arr[7].Trim().Length > 0) GrossWeightTemp = decimal.Parse(arr[7].Trim());
                            //else GrossWeightTemp = 0m;
                            //if (arr[8].Trim().Length > 0) LengthTemp = decimal.Parse(arr[8].Trim());
                            //else LengthTemp = 0m;
                            //if (arr[9].Trim().Length > 0) WidthTemp = decimal.Parse(arr[9].Trim());
                            //else WidthTemp = 0m;
                            //if (arr[10].Trim().Length > 0) HeightTemp = decimal.Parse(arr[10].Trim());
                            //else HeightTemp = 0m;
                            //RemarkTemp = arr[11].Trim();
                            //if (arr[6].Trim().Length > 0) 
                                ExpiredDt = arr[6].Trim();
                            //else ExpiredDt = string.Empty;
                            if (arr[7].Trim().Length > 0) NetWeightTemp = decimal.Parse(arr[7].Trim());
                            else NetWeightTemp = 0m;
                            if (arr[8].Trim().Length > 0) GrossWeightTemp = decimal.Parse(arr[8].Trim());
                            else GrossWeightTemp = 0m;
                            if (arr[9].Trim().Length > 0) LengthTemp = decimal.Parse(arr[9].Trim());
                            else LengthTemp = 0m;
                            if (arr[10].Trim().Length > 0) WidthTemp = decimal.Parse(arr[10].Trim());
                            else WidthTemp = 0m;
                            if (arr[11].Trim().Length > 0) HeightTemp = decimal.Parse(arr[11].Trim());
                            else HeightTemp = 0m;
                            RemarkTemp = arr[12].Trim();
                            l.Add(new Item()
                            {
                                ItCode = ItCodeTemp,
                                Qty = QtyTemp,
                                ItName = string.Empty,
                                InventoryUomCode = string.Empty,
                                BatchNumber = BatchTemp,
                                Lot = LotTemp,
                                Bin = BinTemp,
                                Status = StatusTemp,
                                ExpiredDt = ExpiredDt,
                                NetWeight = NetWeightTemp,
                                GrossWeight = GrossWeightTemp,
                                LengthDimension = LengthTemp,
                                WidthDimension = WidthTemp,
                                HeightDimension = HeightTemp,
                                Remark = RemarkTemp
                            });
                        }
                    }
                }
            }
        }

        private void ProcessImport2(ref List<Item> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string 
                Filter = string.Empty, ItCode = string.Empty, ItName = string.Empty, InventoryUomCode = string.Empty,
                LengthUomCode = string.Empty, 
                WidthUomCode = string.Empty, 
                HeightUomCode = string.Empty, 
                WeightUomCode = string.Empty;
            decimal Length = 0m, Width = 0m, Height = 0m, Weight = 0m;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(ItCode=@ItCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), l[i].ItCode);
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select ItCode, ItName, InventoryUomCode, ");
            SQL.AppendLine("Length, LengthUomCode, Width, WidthUomCode, Height, HeightUomCode, Weight, WeightUomCode ");
            SQL.AppendLine("From TblItem ");
            SQL.AppendLine("Where InventoryUomCode=InventoryUomCode2 ");
            SQL.AppendLine("And InventoryUomCode2=InventoryUomCode3 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItName", 
                    "InventoryUomCode",
                    "Length", 
                    "LengthUomCode", 
                    "Width", 
                    
                    //6-10
                    "WidthUomCode", 
                    "Height", 
                    "HeightUomCode", 
                    "Weight", 
                    "WeightUomCode" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, c[0]);
                        ItName = Sm.DrStr(dr, c[1]);
                        InventoryUomCode = Sm.DrStr(dr, c[2]);
                        Length = Sm.DrDec(dr, c[3]);
                        LengthUomCode = Sm.DrStr(dr, c[4]);
                        Width = Sm.DrDec(dr, c[5]);
                        WidthUomCode = Sm.DrStr(dr, c[6]);
                        Height = Sm.DrDec(dr, c[7]);
                        HeightUomCode = Sm.DrStr(dr, c[8]);
                        Weight = Sm.DrDec(dr, c[9]);
                        WeightUomCode = Sm.DrStr(dr, c[10]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].ItCode, ItCode))
                            {
                                l[i].ItName = ItName;
                                l[i].InventoryUomCode = InventoryUomCode;
                                l[i].Length = Length;
                                l[i].LengthUomCode = LengthUomCode;
                                l[i].Width = Width;
                                l[i].WidthUomCode = WidthUomCode;
                                l[i].Height = Height;
                                l[i].HeightUomCode = HeightUomCode;
                                l[i].Weight = Weight;
                                l[i].WeightUomCode = WeightUomCode;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessImport3(ref List<Item> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Status = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(OptCode=@OptCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@OptCode0" + i.ToString(), l[i].Status);
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select OptCode, OptDesc From TblOption Where OptCat = 'StockStatus' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Status = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].Status, Status))
                                l[i].StatusName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessImport4(ref List<Item> l)
        {
            //Sm.ConvertDate()
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[4].Value = l[i].ItCode;
                r.Cells[6].Value = l[i].ItName;
                r.Cells[7].Value = l[i].BatchNumber;
                r.Cells[9].Value = l[i].Lot;
                r.Cells[10].Value = l[i].Bin;
                r.Cells[11].Value = l[i].Qty;
                r.Cells[12].Value = l[i].InventoryUomCode;
                r.Cells[13].Value = l[i].Qty;
                r.Cells[14].Value = l[i].InventoryUomCode;
                r.Cells[15].Value = l[i].Qty;
                r.Cells[16].Value = l[i].InventoryUomCode;
                r.Cells[17].Value = l[i].Status;
                r.Cells[18].Value = l[i].StatusName;
                if (l[i].ExpiredDt.Length != 0)
                    r.Cells[19].Value = Sm.ConvertDate(l[i].ExpiredDt);
                else
                    r.Cells[19].Value = string.Empty;
                r.Cells[20].Value = l[i].Remark;
                r.Cells[21].Value = l[i].Length;
                r.Cells[22].Value = l[i].LengthUomCode;
                r.Cells[23].Value = l[i].Width;
                r.Cells[24].Value = l[i].WidthUomCode;
                r.Cells[25].Value = l[i].Height;
                r.Cells[26].Value = l[i].HeightUomCode;
                r.Cells[27].Value = l[i].Weight;
                r.Cells[28].Value = l[i].WeightUomCode;
                r.Cells[29].Value = l[i].NetWeight;
                r.Cells[30].Value = l[i].GrossWeight;
                r.Cells[31].Value = l[i].LengthDimension;
                r.Cells[32].Value = l[i].WidthDimension;
                r.Cells[33].Value = l[i].HeightDimension;
                r.Cells[36].Value = 0m;
                ComputeTotalWeight(i);

            }
            r = Grd1.Rows.Add();
            Grd1.EndUpdate();
        }

        private bool IsExpiredDtFormatInvalid(ref List<Item> l)
        {
            for (int i = 0; i < l.Count(); i++)
            {
                if (l[i].ExpiredDt.Contains("/") || l[i].ExpiredDt.Contains("-") || l[i].ExpiredDt.Contains(".") || l[i].ExpiredDt.Contains(" "))
                {
                    Sm.StdMsg(mMsgType.Info, "Format date should be 'yyyymmdd' at row #" + (i + 1));
                    return true;
                }
            }

            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                SetLueLot(ref LueLot, Sm.GetLue(LueWhsCode));
                ClearGrd();
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
                if(mIsStockInboundAutoGenerateBatchNo) UpdateBatch();
               
            }
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue2(SetLueBin), string.Empty);
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 10].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 10].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue2(SetLueLot), string.Empty);
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 9)
            {
                string mLot = string.Empty;
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 9].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LueLot);//LueLot.GetColumnValue("Col1");
                    mLot = Sm.GetLue(LueLot);
                }
                Grd1.Cells[fCell.RowIndex, 10].Value = null;
                //SetLueBin(ref LueBin, mLot);
                LueLot.Visible = false;
            }
        }

        private void TxtVehicleRegNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtVehicleRegNo);
        }

        private void TxtTransportType_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTransportType);
        }

        private void DteExpiredDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteExpiredDt, ref fCell, ref fAccept);
        }

        private void DteExpiredDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStockStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStockStatus, new Sm.RefreshLue2(Sl.SetLueOption), "StockStatus");
        }

        private void LueStockStatus_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStockStatus_Leave(object sender, EventArgs e)
        {
            if (LueStockStatus.Visible && fAccept && fCell.ColIndex == 18)
            {
                if (Sm.GetLue(LueStockStatus).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 17].Value = null;
                    Grd1.Cells[fCell.RowIndex, 18].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 17].Value = Sm.GetLue(LueStockStatus);
                    Grd1.Cells[fCell.RowIndex, 18].Value = LueStockStatus.GetColumnValue("Col2");
                }
            }
        }

        private void LueStockStatus_Validated(object sender, EventArgs e)
        {
            LueStockStatus.Visible = false;
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        #endregion

        private void BtnImport_Click(object sender, EventArgs e)
        {
            ProcessImport();
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue1(SetLueProcess));
        }

        #endregion

        #region Class

        private class Item
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty { get; set; }
            public decimal Length { get; set; }
            public string LengthUomCode { get; set; }
            public decimal Width { get; set; }
            public string WidthUomCode { get; set; }
            public decimal Height { get; set; }
            public string HeightUomCode { get; set; }
            public decimal Weight { get; set; }
            public string WeightUomCode { get; set; }
            public string BatchNumber { get; set;  }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string Status { get; set; }
            public string ExpiredDt { get; set; }
            public string StatusName { get; set; }
            public decimal NetWeight { get; set; }
            public decimal GrossWeight { get; set; }
            public decimal LengthDimension { get; set; }
            public decimal WidthDimension { get; set; }
            public decimal HeightDimension { get; set; }
            public string Remark { get; set; }
        }

        private class StockInboundHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string Reference { get; set; }
            public string LocalDocNo { get; set; }
            public string RecvDt { get; set; }
            public string DeliveryDt { get; set; }
            public string DeliveryFrom { get; set; }
            public string DeliveryTo { get; set; }
            public string VehicleNo { get; set; }
            public string Remark { get; set; }
        }

        public class StockInboundDtl
        {
            public int No { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string BatchNo { get; set; }
            public string ExpDt { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public string Uom { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Length { get; set; }
            public decimal Width { get; set; }
            public decimal Height { get; set; }
            public decimal Weight { get; set; }
            public string Remark { get; set; }
        }

        public class SubTotal
        {
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
        }

        #endregion

    }
}
