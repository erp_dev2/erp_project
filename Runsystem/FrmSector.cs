﻿#region Update
/*
 *  29/10/2021 [RIS/PHT] Menambah grid sub sector
    15/11/2021 [TRI/PHT] Menambah maksimal karakter di sector name
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSector : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mStateInd = 0;
        internal FrmSectorFind FrmFind;

        #endregion

        #region Constructor

        public FrmSector(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length <= 0) this.Text = Sm.GetMenuDesc("FrmVendorSector");
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetGrd();
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 1;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Sub Sector Name"
                    },
                     new int[] 
                    {
                        //0
                        150, 
                    }
                );
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSectorCode, TxtSectorName, ChkActInd
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtSectorCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSectorCode, TxtSectorName
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtSectorCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtSectorCode, true);
                    if(Sm.IsDataExist("Select SectorCode From TblSector Where SectorCode = @Param And ActInd = 'Y'; ", TxtSectorCode.Text))
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtSectorName, ChkActInd }, false);
                    Grd1.ReadOnly = false;
                    TxtSectorName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mStateInd = 0;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSectorCode, TxtSectorName
            });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSectorFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            mStateInd = 1;
            ChkActInd.Checked = true;
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSectorCode, "", false)) return;

            if (Sm.IsDataExist("Select SectorCode From TblSector Where SectorCode = @Param And ActInd = 'Y'; ", TxtSectorCode.Text))
            {
                mStateInd = 2;
                SetFormControl(mState.Edit);
            }
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtSectorCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            //Cursor.Current = Cursors.WaitCursor;
            //try
            //{
            //    var cm = new MySqlCommand() { CommandText = "Delete From TblSector Where SectorCode=@SectorCode" };
            //    Sm.CmParam<String>(ref cm, "@SectorCode", TxtSectorCode.Text);
            //    Sm.ExecCommand(cm);

            //    BtnCancelClick(sender, e);
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try 
            {
                var cml = new List<MySqlCommand>();

                if (mStateInd == 1)
                {
                    cml.Add(SaveSector());
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                        {
                            cml.Add(SaveSubSector(Row));
                        }
                    }

                    Sm.ExecCommands(cml);

                    ShowData(TxtSectorCode.Text);
                }
                else if (mStateInd == 2)
                {
                    cml.Add(UpdateSector());
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                        {
                            cml.Add(SaveSubSector(Row));
                        }
                    }

                    Sm.ExecCommands(cml);

                    ShowData(TxtSectorCode.Text);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            //InsertData();
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string SectorCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                ShowDataSector(SectorCode);
                ShowDataSubSector(SectorCode);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDataSector(string SectorCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SectorCode", SectorCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select SectorCode, SectorName, ActInd From TblSector Where SectorCode=@SectorCode",
                    new string[] 
                        {
                            "SectorCode", "SectorName", "ActInd"
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSectorCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtSectorName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                    }, true
                );
        }

        private void ShowDataSubSector(string SectorCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@SectorCode", SectorCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "SELECT SubSectorName FROM tblsubsector WHERE sectorcode = @SectorCode ORDER BY SubSectorName ",
                    new string[] 
                    { 
                        //0
                        "SubSectorName",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSectorCode, "Sector code", false) ||
                Sm.IsTxtEmpty(TxtSectorName, "Sector name", false) ||
                (mStateInd == 1 && IsSectorCodeExisted()) ||
                (mStateInd == 2 && IsDataAlreadyInactive());
        }

        private bool IsDataAlreadyInactive()
        {
            if(Sm.IsDataExist("Select SectorCode From TblSector Where SectorCode = @Param And ActInd = 'N'; ", TxtSectorCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already inactive.");
                TxtSectorCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsSectorCodeExisted()
        {
            if (!TxtSectorCode.Properties.ReadOnly && Sm.IsDataExist("Select SectorCode From TblSector Where SectorCode='" + TxtSectorCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Sector code ( " + TxtSectorCode.Text + " ) is exists.");
                return true;
            }
            return false;
        }

        //private void InsertData()
        //{
        //    try
        //    {
        //        if (IsDataNotValid()) return;

        //        Cursor.Current = Cursors.WaitCursor;

        //        var SQL = new StringBuilder();

        //        if (mStateInd == 1)
        //        {
        //            SQL.AppendLine("Insert Into TblSector(SectorCode, SectorName, ActInd, CreateBy, CreateDt) ");
        //            SQL.AppendLine("Values(@SectorCode, @SectorName, @ActInd, @UserCode2, CurrentDateTime()); ");
        //        }
        //        else if (mStateInd == 2)
        //        {
        //            SQL.AppendLine("Update TblSector Set ");
        //            SQL.AppendLine("    SectorName = @SectorName, ActInd = @ActInd, LastUpBy = @UserCode2, LastUpDt = CurrentDateTime() ");
        //            SQL.AppendLine("Where SectorCode = @SectorCode And ActInd = 'Y'; ");
        //        }

        //        var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //        Sm.CmParam<String>(ref cm, "@SectorCode", TxtSectorCode.Text);
        //        Sm.CmParam<String>(ref cm, "@SectorName", TxtSectorName.Text);
        //        Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

        //        Sm.ExecCommand(cm);

        //        ShowData(TxtSectorCode.Text);
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //    finally
        //    {
        //        Cursor.Current = Cursors.Default;
        //    }
        //}

        private MySqlCommand SaveSector()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSector(SectorCode, SectorName, ActInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SectorCode, @SectorName, @ActInd, @UserCode2, CurrentDateTime()); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SectorCode", TxtSectorCode.Text);
            Sm.CmParam<String>(ref cm, "@SectorName", TxtSectorName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSubSector(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO tblsubsector (SubSectorCode, SubSectorName, SectorCode, ActInd, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@SubSectorCode, @SubSectorName, @SectorCode, @ActInd, @UserCode2, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SectorCode", TxtSectorCode.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@SubSectorCode", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SubSectorName", Sm.GetGrdStr(Grd1, Row, 0));

            return cm;
        }

        private MySqlCommand UpdateSector()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSector Set ");
            SQL.AppendLine("    SectorName = @SectorName, ActInd = @ActInd, LastUpBy = @UserCode2, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where SectorCode = @SectorCode And ActInd = 'Y'; ");

            SQL.AppendLine("DELETE FROM tblsubsector WHERE SectorCode = @SectorCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SectorCode", TxtSectorCode.Text);
            Sm.CmParam<String>(ref cm, "@SectorName", TxtSectorName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode2", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region Events

        #region Grid Event

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        #endregion

        #region Misc Control events

        private void TxtSectorCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSectorCode);
        }

        private void TxtSectorName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSectorName);
        }

        private void ChkActInd_CheckedChanged(object sender, EventArgs e)
        {
            if (mStateInd != 0 && ChkActInd.Checked == true)
            {
                Grd1.ReadOnly = false;
            }
            else if (mStateInd != 0 && ChkActInd.Checked == false)
            {
                Grd1.ReadOnly = true;
                Sm.ClearGrd(Grd1, true);
                Sm.FocusGrd(Grd1, 0, 0);
            }
            
        }

        #endregion

        

        #endregion

    }
}
