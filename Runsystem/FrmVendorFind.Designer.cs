﻿namespace RunSystem
{
    partial class FrmVendorFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtVdName = new DevExpress.XtraEditors.TextEdit();
            this.ChkVdName = new DevExpress.XtraEditors.CheckEdit();
            this.ChkVdCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueVdCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblVendorSector = new System.Windows.Forms.Label();
            this.TxtSector = new DevExpress.XtraEditors.TextEdit();
            this.ChkSector = new DevExpress.XtraEditors.CheckEdit();
            this.LblVendorSubSector = new System.Windows.Forms.Label();
            this.TxtSubSector = new DevExpress.XtraEditors.TextEdit();
            this.ChkSubSector = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubSector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSubSector.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LblVendorSubSector);
            this.panel2.Controls.Add(this.TxtSubSector);
            this.panel2.Controls.Add(this.ChkSubSector);
            this.panel2.Controls.Add(this.LblVendorSector);
            this.panel2.Controls.Add(this.TxtSector);
            this.panel2.Controls.Add(this.ChkSector);
            this.panel2.Controls.Add(this.ChkVdCtCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueVdCtCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtVdName);
            this.panel2.Controls.Add(this.ChkVdName);
            this.panel2.Size = new System.Drawing.Size(672, 95);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 378);
            this.Grd1.TabIndex = 15;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(67, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Vendor";
            // 
            // TxtVdName
            // 
            this.TxtVdName.EnterMoveNextControl = true;
            this.TxtVdName.Location = new System.Drawing.Point(119, 3);
            this.TxtVdName.Name = "TxtVdName";
            this.TxtVdName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdName.Properties.Appearance.Options.UseFont = true;
            this.TxtVdName.Properties.MaxLength = 80;
            this.TxtVdName.Size = new System.Drawing.Size(258, 20);
            this.TxtVdName.TabIndex = 10;
            this.TxtVdName.Validated += new System.EventHandler(this.TxtVdName_Validated);
            // 
            // ChkVdName
            // 
            this.ChkVdName.Location = new System.Drawing.Point(381, 2);
            this.ChkVdName.Name = "ChkVdName";
            this.ChkVdName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkVdName.Properties.Appearance.Options.UseFont = true;
            this.ChkVdName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkVdName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkVdName.Properties.Caption = " ";
            this.ChkVdName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkVdName.Size = new System.Drawing.Size(31, 22);
            this.ChkVdName.TabIndex = 11;
            this.ChkVdName.ToolTip = "Remove filter";
            this.ChkVdName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkVdName.ToolTipTitle = "Run System";
            this.ChkVdName.CheckedChanged += new System.EventHandler(this.ChkVdName_CheckedChanged);
            // 
            // ChkVdCtCode
            // 
            this.ChkVdCtCode.Location = new System.Drawing.Point(381, 25);
            this.ChkVdCtCode.Name = "ChkVdCtCode";
            this.ChkVdCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkVdCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkVdCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkVdCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkVdCtCode.Properties.Caption = " ";
            this.ChkVdCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkVdCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkVdCtCode.TabIndex = 14;
            this.ChkVdCtCode.ToolTip = "Remove filter";
            this.ChkVdCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkVdCtCode.ToolTipTitle = "Run System";
            this.ChkVdCtCode.CheckedChanged += new System.EventHandler(this.ChkVdCtCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(14, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 14);
            this.label4.TabIndex = 12;
            this.label4.Text = "Vendor Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCtCode
            // 
            this.LueVdCtCode.EnterMoveNextControl = true;
            this.LueVdCtCode.Location = new System.Drawing.Point(119, 26);
            this.LueVdCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCtCode.Name = "LueVdCtCode";
            this.LueVdCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCtCode.Properties.DropDownRows = 25;
            this.LueVdCtCode.Properties.NullText = "[Empty]";
            this.LueVdCtCode.Properties.PopupWidth = 500;
            this.LueVdCtCode.Size = new System.Drawing.Size(258, 20);
            this.LueVdCtCode.TabIndex = 13;
            this.LueVdCtCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCtCode.EditValueChanged += new System.EventHandler(this.LueVdCtCode_EditValueChanged);
            // 
            // LblVendorSector
            // 
            this.LblVendorSector.AutoSize = true;
            this.LblVendorSector.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVendorSector.Location = new System.Drawing.Point(27, 51);
            this.LblVendorSector.Name = "LblVendorSector";
            this.LblVendorSector.Size = new System.Drawing.Size(87, 14);
            this.LblVendorSector.TabIndex = 15;
            this.LblVendorSector.Text = "Vendor Sector";
            // 
            // TxtSector
            // 
            this.TxtSector.EnterMoveNextControl = true;
            this.TxtSector.Location = new System.Drawing.Point(119, 48);
            this.TxtSector.Name = "TxtSector";
            this.TxtSector.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSector.Properties.Appearance.Options.UseFont = true;
            this.TxtSector.Properties.MaxLength = 80;
            this.TxtSector.Size = new System.Drawing.Size(258, 20);
            this.TxtSector.TabIndex = 16;
            this.TxtSector.Validated += new System.EventHandler(this.TxtSector_Validated);
            // 
            // ChkSector
            // 
            this.ChkSector.Location = new System.Drawing.Point(381, 47);
            this.ChkSector.Name = "ChkSector";
            this.ChkSector.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSector.Properties.Appearance.Options.UseFont = true;
            this.ChkSector.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSector.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSector.Properties.Caption = " ";
            this.ChkSector.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSector.Size = new System.Drawing.Size(31, 22);
            this.ChkSector.TabIndex = 17;
            this.ChkSector.ToolTip = "Remove filter";
            this.ChkSector.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSector.ToolTipTitle = "Run System";
            this.ChkSector.CheckedChanged += new System.EventHandler(this.ChkSector_CheckedChanged);
            // 
            // LblVendorSubSector
            // 
            this.LblVendorSubSector.AutoSize = true;
            this.LblVendorSubSector.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblVendorSubSector.Location = new System.Drawing.Point(2, 73);
            this.LblVendorSubSector.Name = "LblVendorSubSector";
            this.LblVendorSubSector.Size = new System.Drawing.Size(112, 14);
            this.LblVendorSubSector.TabIndex = 18;
            this.LblVendorSubSector.Text = "Vendor Sub Sector";
            // 
            // TxtSubSector
            // 
            this.TxtSubSector.EnterMoveNextControl = true;
            this.TxtSubSector.Location = new System.Drawing.Point(119, 70);
            this.TxtSubSector.Name = "TxtSubSector";
            this.TxtSubSector.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubSector.Properties.Appearance.Options.UseFont = true;
            this.TxtSubSector.Properties.MaxLength = 80;
            this.TxtSubSector.Size = new System.Drawing.Size(258, 20);
            this.TxtSubSector.TabIndex = 19;
            this.TxtSubSector.Validated += new System.EventHandler(this.TxtSubSector_Validated);
            // 
            // ChkSubSector
            // 
            this.ChkSubSector.Location = new System.Drawing.Point(381, 69);
            this.ChkSubSector.Name = "ChkSubSector";
            this.ChkSubSector.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSubSector.Properties.Appearance.Options.UseFont = true;
            this.ChkSubSector.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSubSector.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSubSector.Properties.Caption = " ";
            this.ChkSubSector.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSubSector.Size = new System.Drawing.Size(31, 22);
            this.ChkSubSector.TabIndex = 20;
            this.ChkSubSector.ToolTip = "Remove filter";
            this.ChkSubSector.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSubSector.ToolTipTitle = "Run System";
            this.ChkSubSector.CheckedChanged += new System.EventHandler(this.ChkSubSector_CheckedChanged);
            // 
            // FrmVendorFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmVendorFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkVdCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubSector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSubSector.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtVdName;
        private DevExpress.XtraEditors.CheckEdit ChkVdName;
        private DevExpress.XtraEditors.CheckEdit ChkVdCtCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueVdCtCode;
        private System.Windows.Forms.Label LblVendorSubSector;
        private DevExpress.XtraEditors.TextEdit TxtSubSector;
        private DevExpress.XtraEditors.CheckEdit ChkSubSector;
        private System.Windows.Forms.Label LblVendorSector;
        private DevExpress.XtraEditors.TextEdit TxtSector;
        private DevExpress.XtraEditors.CheckEdit ChkSector;
    }
}