﻿#region Update
/*
    30/11/2017 [WED] tambah informasi work center
    30/11/2017 [WED] tambah filter work center
    06/03/2018 [ARI] tambah department
    06/12/2021 [YOG/IOK] Pada menu QC Planning (01110103), bisa planning lebih dari 1 baris untuk 1 item/batch dalam satu dokumen. Karena parameter yang digunakan berbeda-beda untuk setiap 1 item dengan batch# yang sama tersebut. Menggunakan parameter IsQCParameterInDetail
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmQCPlanningFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmQCPlanning mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmQCPlanningFind(FrmQCPlanning FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            #region new code
            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.ActInd, E.WhsName, F.DocName, G.DeptName, ");
            SQL.AppendLine("A.QCPCode, C.QCPDesc, C.UomCode, D.UomName, B.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  "); 
            SQL.AppendLine("FROM TblQCPlanningHdr A  ");
            SQL.AppendLine("INNER JOIN TblQCPlanningDtl B On A.DocNo = B.DocNo ");
            if (mFrmParent.mIsQCParameterInDetail)
                SQL.AppendLine("INNER JOIN TblQCParameter C On B.QCPCode = C.QCPCode ");
            else
                SQL.AppendLine("INNER JOIN TblQCParameter C On A.QCPCode = C.QCPCode ");
            SQL.AppendLine("INNER JOIN TblQCParameterUom D On C.UomCode = D.UomCode ");
            SQL.AppendLine("LEFT JOIN TblWarehouse E On A.WhsCode = E.WhsCode ");
            SQL.AppendLine("Left Join TblWorkCenterHdr F On A.WorkCenterDocNo = F.DocNo ");
            SQL.AppendLine("Left Join TblDepartment G On A.DeptCode = G.DeptCode ");
            SQL.AppendLine("WHERE A.DocDt IN (SELECT DocDt FROM TblQCPlanningHdr WHERE DocDt BETWEEN @DocDt1 AND @DocDt2) ");
            #endregion

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Active",
                        "Location",
                        "Work Center",

                        //6-10
                        "Department",
                        "Parameter"+Environment.NewLine+"Code",
                        "Parameter Description",
                        "Uom Code",
                        "UoM",
                     
                        //11-15
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 

                        //16-17
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 80, 60, 100, 250, 
                        
                        //6-10
                        150, 80, 150, 80, 100, 

                        //11-15
                        200, 100, 100, 100, 100,  
                        
                        //16-17
                        100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13, 16 });
            Sm.GrdFormatTime(Grd1, new int[] { 14, 17 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 11, 12, 13, 14, 15, 16, 17 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 11, 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                    ) return;
                
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " AND 0=0 ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParam<String>(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenter.Text, new string[] { "A.WorkCenterDocNo", "F.DocName", "F.ShortCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "ActInd", "WhsName", "DocName", "DeptName",  
                            
                            //6-10
                            "QCPCode", "QCPDesc", "UomCode", "UomName", "Remark",  
                                         
                            //11-14
                            "CreateBy", "CreateDt", "LastUpBy",  "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 17, 14);
                        }, true, true, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Event

        #region Misc Control Methods

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Misc Control Events

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtWorkCenter_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work Center");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        
        #endregion

        #endregion

       
    }
}
