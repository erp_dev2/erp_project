﻿#region Update
/*
    20/04/2020 [WED/KBN] new apps, reporting laba rugi khusus KBN
    22/04/2020 [WED/KBN] label di kolom di rubah
    04/05/2020 [DITA/KBN] tambah filter entity
    05/05/2020 [WED/KBN] nilai bagi rata-rata dikurangi 1
    10/06/2020 [WED/KBN] bisa filter entity
    12/06/2020 [WED/KBN] belom akomodir COA berkoma
    17/06/2020 [WED/KBN] 
                    untuk januari : 
                    Up To Mth-1 ==> 0
                    Up To Avg ==> 0
                    Mth Real ==> OB Yr + Journal Mth
                    else :
                    Up To Mth-1 ==> Journal Mth-1
                    Up To Avg ==> Up To Mth-1 / (Mth - 1)
                    Mth Real ==> Journal Mth + OB Yr
    19/06/2020 [WED/KBN] tambah entity
    23/06/2020 [WED/KBN] consolidate --> tetap ngeliat entity nya
    24/06/2020 [WED/KBN] kalau entity nya kosong, semua entity yang di journal dan OB ikut diitung.
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPrintRptFicoSetting3 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mThisYear = string.Empty,
            mLastYear = string.Empty,
            mThisMonth = string.Empty,
            mLastMonth = string.Empty,
            mLastMonthNo = string.Empty,
            mSetSession = string.Empty;
        private string[] mMths = { "JAN", "FEB", "MAR", "APR", "MAY", "JUNE", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
        private bool mIsFilterBySite = false,
            mIsEntityMandatory = false,
            mIsFilterByEnt = false,
            mIsReportingFilterByEntity = false;

        #endregion

        #region Constructor

        public FrmPrintRptFicoSetting3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetLueFicoFormula(ref LueOptionCode);
                TxtStartYr.EditValue = Sm.GetLue(LueYr);
                SetTimeStampVariable();
                SetGrd();
                TxtStartYr.Enabled = false;
                SetLueEntCode(ref LueEntCode, string.Empty, mIsFilterByEnt ? "Y" : "N");
                mSetSession = SetSession();
                if (mIsEntityMandatory)
                    LblEntCode.ForeColor = Color.Red;
                else
                {
                    LblEntCode.Visible = false;
                    LueEntCode.Visible = false;
                    LblEntCode.ForeColor = Color.Black;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "Description",

                    //1-5
                    "RKAP"+Environment.NewLine+mThisYear+Environment.NewLine+"(a)",
                    "Up to "+mLastMonth+ " "+mLastYear+Environment.NewLine+"REAL"+Environment.NewLine+"(b)",
                    "Up to "+mLastMonth+ " "+mLastYear+Environment.NewLine+"AVERAGE"+Environment.NewLine+"(c)",
                    mThisMonth+ " "+mThisYear+Environment.NewLine+"RKAP"+Environment.NewLine+"(d)",
                    mThisMonth+ " "+mThisYear+Environment.NewLine+"REAL"+Environment.NewLine+"(e)",
                    
                    //6-10
                    "Up to "+mThisMonth+ " "+mThisYear+Environment.NewLine+"RKAP"+Environment.NewLine+"(f)",
                    "Up to "+mThisMonth+ " "+mThisYear+Environment.NewLine+"REAL"+Environment.NewLine+"(g)",
                    "(%)"+Environment.NewLine+Environment.NewLine+"(g:f)",
                    "(%)"+Environment.NewLine+Environment.NewLine+"(g:a)",
                    "(%)"+Environment.NewLine+Environment.NewLine+"(e:d)",

                    //11
                    "(%)"+Environment.NewLine+Environment.NewLine+"(e:c)",
                },
                new int[] 
                {
                    //0
                    300, 

                    //1-5
                    200, 200, 200, 200, 200,  
                    
                    //6-10
                    200, 200, 200, 200, 200, 

                    //11
                    200
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, 0);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueOptionCode, "Type")||
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity") ||
                IsAccountInvalid()) 
                )
                return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                SetTimeStampVariable();
                SetGrd();

                var l = new List<RptFicoSetting>();
                var l2 = new List<RptFicoSettingCOA>();

                ProcessFicoSetting(ref l);
                if (l.Count == 0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }
                ProcessFicoCOA(ref l, ref l2);
                ProcessFicoFormula(ref l);
                ProcessFicoSetting2(ref l);
                ProcessFicoSetting3(ref l);

                l.Clear();
                l2.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private bool IsAccountInvalid()
        {
            var SQL = new StringBuilder();
            string DocNos = string.Empty;

            SQL.AppendLine("Select Group_Concat(AcNo Separator ';') ");
            SQL.AppendLine("From TblRptFicoSettingDtl2 ");
            SQL.AppendLine("Where RptFicoCode = @Param ");
            SQL.AppendLine("And AcNo In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct Parent ");
            SQL.AppendLine("    From TblCOA ");
            SQL.AppendLine("    Where ActInd = 'Y' ");
            SQL.AppendLine("); ");

            DocNos = Sm.GetValue(SQL.ToString(), Sm.GetLue(LueOptionCode));

            if (DocNos.Length > 0)
            {
                var mMsgWarning = new StringBuilder();

                mMsgWarning.AppendLine("One or more account in this setting is assigned as a parent. ");
                mMsgWarning.AppendLine(DocNos.Replace(";", Environment.NewLine));

                Sm.StdMsg(mMsgType.Warning, mMsgWarning.ToString());
                return true;
            }

            return false;
        }

        private string SetSession()
        {
            return "Set Session group_concat_max_len = 1000000; ";
        }

        #region Process FICO Setting

        //dapetin semua data setting description dari master fico setting
        private void ProcessFicoSetting(ref List<RptFicoSetting> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            //var lRptFicoSetting = new List<RptFicoSetting>();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, B.SettingDesc, B.Sequence, B.Formula, ");
            SQL.AppendLine("B.AcNo ");
            //SQL.AppendLine("C.AcNo ");
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl B On A.RptFicoCode = B.RptFicoCode ");
            //SQL.AppendLine("INNER JOIN TblCOA C ON FIND_IN_SET(C.AcNo, B.AcNo) ");
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order by Cast(sequence As UNSIGNED), B.SettingCode; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = string.Concat(mSetSession, SQL.ToString());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "SettingDesc", "Sequence", "Formula", "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RptFicoSetting()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            SettingDesc = Sm.DrStr(dr, c[2]),
                            Sequence = Sm.DrStr(dr, c[3]),
                            Formula = Sm.DrStr(dr, c[4]),
                            AcNo = Sm.DrStr(dr, c[5]),
                            RKAP = 0m,
                            RealOpeningBalance = 0m,
                            AvgOpeningBalance = 0m,
                            RKAPCurrentMth = 0m,
                            RealCurrentMth = 0m,
                            RKAPToDate = 0m,
                            RealToDate = 0m,
                            Percentage1 = 0m,
                            Percentage2 = 0m,
                            Percentage3 = 0m,
                            Percentage4 = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        #region Fico COA

        //dapetin + proses semua data master fico setting yang ada COA nya
        private void ProcessFicoCOA(ref List<RptFicoSetting> l, ref List<RptFicoSettingCOA> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.RptFicoCode, B.SettingCode, C.AcNo ");
            SQL.AppendLine(", B.EntCode ");
            SQL.AppendLine("From TblRptFicoSettingHdr A ");
            SQL.AppendLine("Inner Join TblRptFicoSettingDtl2 B On A.RptFicoCode = B.RptFicoCode And A.RptFicoCode = @FicoCode ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("    And B.EntCode = @EntCode ");
            SQL.AppendLine("INNER JOIN TblCOA C ON B.AcNo = C.AcNo And C.ActInd = 'Y' ");            
            SQL.AppendLine("Where A.RptFicoCode = @FicoCode ");
            
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ");
                SQL.AppendLine("(A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order by B.SettingCode; ");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "AcNo", "EntCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new RptFicoSettingCOA()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            AcNo = Sm.DrStr(dr, c[2]),
                            EntCode = Sm.DrStr(dr, c[3]) == "-" ? string.Empty : Sm.DrStr(dr, c[3]),
                            RKAP = 0m,
                            RealOpeningBalance = 0m,
                            AvgOpeningBalance = 0m,
                            RKAPCurrentMth = 0m,
                            RealCurrentMth = 0m,
                            RKAPToDate = 0m,
                            RealToDate = 0m
                        });
                    }
                }
                dr.Close();
            }

            if (l2.Count > 0)
            {
                string EntCodeLists = GetAllEntCode();

                //RKAP
                GetRKAPAmt(ref l2); // RKAP & RKAPCurrentMonth
                GetRKAPAmt2(ref l2); // RKAP To Date

                //Journal
                var lOB = new List<RptFicoSettingOpeningBalance>();
                var lJOB = new List<RptFicoSettingJournalOpeningBalance>();
                var lJCM = new List<RptFicoSettingJournalCurrentMonth>();

                GetOB(ref lOB);
                GetJOB(ref lJOB, EntCodeLists);
                GetJCM(ref lJCM);

                if (lOB.Count > 0)
                {
                    ProcessOB(ref l2, ref lOB);
                }

                if (lJOB.Count > 0)
                {
                    //CalculateJOB(ref lJOB);
                    lJOB.RemoveAll(x => x.AcNoReal != x.AcNo);
                    ProcessJOB(ref l2, ref lOB, ref lJOB);
                }

                if (lJCM.Count > 0)
                {
                    //CalculateJCM(ref lJCM);
                    lJCM.RemoveAll(x => x.AcNoReal != x.AcNo);
                    ProcessJCM(ref l2, ref lJCM);
                }

                //var l3 = new List<RptFicoSettingCOASummary>();

                var l3 = l2.GroupBy(g => g.SettingCode)
                    .Select(s => new
                    {
                        FicoCode = Sm.GetLue(LueOptionCode),
                        SettingCode = s.Key,
                        RKAP = s.Sum(ts => ts.RKAP),
                        RealOpeningBalance = s.Sum(ts => ts.RealOpeningBalance),
                        AvgOpeningBalance = s.Sum(ts => ts.AvgOpeningBalance),
                        RKAPCurrentMth = s.Sum(ts => ts.RKAPCurrentMth),
                        RealCurrentMth = s.Sum(ts => ts.RealCurrentMth),
                        RKAPToDate = s.Sum(ts => ts.RKAPToDate),
                        RealToDate = s.Sum(ts => ts.RealToDate)
                    }).ToList();

                var l4 = new List<RptFicoSettingCOASummary>();
                foreach (var x in l3)
                {
                    l4.Add(new RptFicoSettingCOASummary()
                    {
                        FicoCode = x.FicoCode,
                        SettingCode = x.SettingCode,
                        RKAP = x.RKAP,
                        RealOpeningBalance = x.RealOpeningBalance,
                        AvgOpeningBalance = x.AvgOpeningBalance,
                        RKAPCurrentMth = x.RKAPCurrentMth,
                        RealCurrentMth = x.RealCurrentMth,
                        RKAPToDate = x.RKAPToDate,
                        RealToDate = x.RealToDate,
                    });
                }

                ProcessFicoCOA2(ref l4); // Process opening balance's average & real to date
                ProcessFicoCOA3(ref l, ref l4); // Process from FicoSettingCOA to FicoSetting

                lOB.Clear();
                lJOB.Clear();
                lJCM.Clear();

                l4.Clear();
                l3.Clear();
            }
        }

        private string GetAllEntCode()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Group_Concat(Distinct IfNull(EntCode, '')) ");
            SQL.AppendLine("From TblRptFicoSettingDtl2 ");
            SQL.AppendLine("Where RptFicoCode = @Param ");
            SQL.AppendLine("; ");

            return Sm.GetValue(SQL.ToString(), Sm.GetLue(LueOptionCode));
        }

        private void GetRKAPAmt(ref List<RptFicoSettingCOA> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT T.RptFicoCode, T.SettingCode, T.EntCode, SUM(T.RKAP) RKAP, SUM(T.RKAPCurrentMth) RKAPCurrentMth ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.RptFicoCode, A.SettingCode, C.EntCode, D.Amt RKAP, 0.00 RKAPCurrentMth ");
            SQL.AppendLine("    FROM TblRptFicoSettingDtl A ");
            SQL.AppendLine("    INNER JOIN TblCOA B ON B.ActInd = 'Y' ");
            SQL.AppendLine("        AND FIND_IN_SET(B.AcNo, A.AcNo)  ");
            SQL.AppendLine("        AND A.RptFicoCode = @FicoCode ");
            SQL.AppendLine("        AND A.Formula IS NULL ");
            SQL.AppendLine("        AND A.AcNo IS NOT NULL ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanHdr C ON C.Yr = @Yr ");
            SQL.AppendLine("        AND C.CancelInd = 'N' ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("        And C.EntCode = @EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And C.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(C.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                
                SQL.AppendLine("    Left Join TblRptFicoSettingDtl2 C1 On C1.RptFicoCode = @FicoCode ");
                SQL.AppendLine("        And A.SettingCode = C1.SettingCode ");
                SQL.AppendLine("        And B.AcNo = C1.AcNo ");
                SQL.AppendLine("        And C1.EntCode = @EntCode ");
            }
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        AND B.AcNo = D.AcNo ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    SELECT A.RptFicoCode, A.SettingCode, C.EntCode, 0.00 RKAP,  ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When @Mth = '01' Then D.Amt01 When @Mth = '02' Then D.Amt02 When @Mth = '03' Then D.Amt03 ");
            SQL.AppendLine("        When @Mth = '04' Then D.Amt04 When @Mth = '05' Then D.Amt05 When @Mth = '06' Then D.Amt06 ");
            SQL.AppendLine("        When @Mth = '07' Then D.Amt07 When @Mth = '08' Then D.Amt08 When @Mth = '09' Then D.Amt09 ");
            SQL.AppendLine("        When @Mth = '10' Then D.Amt10 When @Mth = '11' Then D.Amt11 When @Mth = '12' Then D.Amt12 ");
            SQL.AppendLine("    END AS RKAPCurrentMth ");
            SQL.AppendLine("    FROM TblRptFicoSettingDtl A ");
            SQL.AppendLine("    INNER JOIN TblCOA B ON B.ActInd = 'Y' ");
            SQL.AppendLine("        AND FIND_IN_SET(B.AcNo, A.AcNo)  ");
            SQL.AppendLine("        AND A.RptFicoCode = @FicoCode ");
            SQL.AppendLine("        AND A.Formula IS NULL ");
            SQL.AppendLine("        AND A.AcNo IS NOT NULL ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanHdr C ON C.Yr = @Yr ");
            SQL.AppendLine("        AND C.CancelInd = 'N' ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("        And C.EntCode = @EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And C.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(C.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("    Left Join TblRptFicoSettingDtl2 C1 On C1.RptFicoCode = @FicoCode ");
                SQL.AppendLine("        And A.SettingCode = C1.SettingCode ");
                SQL.AppendLine("        And B.AcNo = C1.AcNo ");
                SQL.AppendLine("        And C1.EntCode = @EntCode ");
            }
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        AND B.AcNo = D.AcNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.RptFicoCode, T.SettingCode, T.EntCode; ");

            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));
            Sm.CmParam<string>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<string>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<string>(ref cm, "@Mth", Sm.GetLue(LueMth));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "RKAP", "RKAPCurrentMth", "EntCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l2
                            .Where(a => 
                                a.FicoCode == Sm.DrStr(dr, c[0]) && 
                                a.SettingCode == Sm.DrStr(dr, c[1])
                                )
                            )
                        {
                            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                            {
                                if (x.EntCode.Length == 0)
                                {
                                    if (Sm.DrStr(dr, c[4]) == Sm.GetLue(LueEntCode))
                                    {
                                        x.RKAP = Sm.DrDec(dr, c[2]);
                                        x.RKAPCurrentMth = Sm.DrDec(dr, c[3]);
                                    }
                                }
                                else
                                {
                                    if (x.EntCode == Sm.DrStr(dr, c[4]))
                                    {
                                        x.RKAP = Sm.DrDec(dr, c[2]);
                                        x.RKAPCurrentMth = Sm.DrDec(dr, c[3]);
                                    }
                                }
                            }
                            else
                            {
                                if (x.EntCode.Length == 0)
                                {
                                    x.RKAP = Sm.DrDec(dr, c[2]);
                                    x.RKAPCurrentMth = Sm.DrDec(dr, c[3]);
                                }
                                else
                                {
                                    if (x.EntCode == Sm.DrStr(dr, c[4]))
                                    {
                                        x.RKAP = Sm.DrDec(dr, c[2]);
                                        x.RKAPCurrentMth = Sm.DrDec(dr, c[3]);
                                    }
                                }
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void GetRKAPAmt2(ref List<RptFicoSettingCOA> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT T.RptFicoCode, T.SettingCode, T.EntCode, (SUM(T.Amt) + SUM(T.Amt2)) RKAPToDate ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.RptFicoCode, A.SettingCode, C.EntCode, D.Amt, 0.00 Amt2 ");
            SQL.AppendLine("    FROM TblRptFicoSettingDtl A ");
            SQL.AppendLine("    INNER JOIN TblCOA B ON B.ActInd = 'Y' ");
            SQL.AppendLine("        AND FIND_IN_SET(B.AcNo, A.AcNo)  ");
            SQL.AppendLine("        AND A.RptFicoCode = @FicoCode ");
            SQL.AppendLine("        AND A.Formula IS NULL ");
            SQL.AppendLine("        AND A.AcNo IS NOT NULL ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanHdr C ON C.Yr < @Yr ");
            SQL.AppendLine("        AND C.Yr >= @StartYr ");
            SQL.AppendLine("        AND C.CancelInd = 'N' ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("        And C.EntCode = @EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And C.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(C.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("    Inner Join TblRptFicoSettingDtl2 C1 On C1.RptFicoCode = @FicoCode ");
                SQL.AppendLine("        And A.SettingCode = C1.SettingCode ");
                SQL.AppendLine("        And B.AcNo = C1.AcNo ");
                SQL.AppendLine("        And C1.EntCode = @EntCode ");
            }
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        AND B.AcNo = D.AcNo ");
                //ini di loop berdasarkan Mth nya
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    SELECT A.RptFicoCode, A.SettingCode, C.EntCode, 0.00 Amt, ");
            SQL.AppendLine("    ( 0.00 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 1)
                SQL.AppendLine("   + D.Amt01 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 2)
                SQL.AppendLine("   + D.Amt02 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 3)
                SQL.AppendLine("   + D.Amt03 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 4)
                SQL.AppendLine("   + D.Amt04 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 5)
                SQL.AppendLine("   + D.Amt05 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 6)
                SQL.AppendLine("   + D.Amt06 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 7)
                SQL.AppendLine("   + D.Amt07 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 8)
                SQL.AppendLine("   + D.Amt08 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 9)
                SQL.AppendLine("   + D.Amt09 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 10)
                SQL.AppendLine("   + D.Amt10 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 11)
                SQL.AppendLine("   + D.Amt11 ");
            if (Int32.Parse(Sm.GetLue(LueMth)) >= 12)
                SQL.AppendLine("   + D.Amt12 ");
            SQL.AppendLine("    ) Amt2 ");
            SQL.AppendLine("    FROM TblRptFicoSettingDtl A ");
            SQL.AppendLine("    INNER JOIN TblCOA B ON B.ActInd = 'Y' ");
            SQL.AppendLine("        AND FIND_IN_SET(B.AcNo, A.AcNo)  ");
            SQL.AppendLine("        AND A.RptFicoCode = @FicoCode ");
            SQL.AppendLine("        AND A.Formula IS NULL ");
            SQL.AppendLine("        AND A.AcNo IS NOT NULL ");
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanHdr C ON C.Yr = @Yr ");
            SQL.AppendLine("        AND C.CancelInd = 'N' ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("        And C.EntCode = @EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And C.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(C.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("    Inner Join TblRptFicoSettingDtl2 C1 On C1.RptFicoCode = @FicoCode ");
                SQL.AppendLine("        And A.SettingCode = C1.SettingCode ");
                SQL.AppendLine("        And B.AcNo = C1.AcNo ");
                SQL.AppendLine("        And C1.EntCode = @EntCode ");
            }
            SQL.AppendLine("    INNER JOIN TblCompanyBudgetPlanDtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        AND B.AcNo = D.AcNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.RptFicoCode, T.SettingCode, T.EntCode; ");

            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));
            Sm.CmParam<string>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<string>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<string>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<string>(ref cm, "@StartYr", TxtStartYr.Text);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "RKAPToDate", "EntCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        foreach (var x in l2
                            .Where(a =>
                                a.FicoCode == Sm.DrStr(dr, c[0]) &&
                                a.SettingCode == Sm.DrStr(dr, c[1])
                                )
                            )
                        {
                            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                            {
                                if (x.EntCode.Length == 0)
                                {
                                    if (Sm.DrStr(dr, c[4]) == Sm.GetLue(LueEntCode))
                                    {
                                        x.RKAPToDate = Sm.DrDec(dr, c[2]);
                                    }
                                }
                                else
                                {
                                    if (x.EntCode == Sm.DrStr(dr, c[4]))
                                    {
                                        x.RKAPToDate = Sm.DrDec(dr, c[2]);
                                    }
                                }
                            }
                            else
                            {
                                if (x.EntCode.Length == 0)
                                {
                                    x.RKAPToDate = Sm.DrDec(dr, c[2]);
                                }
                                else
                                {
                                    if (x.EntCode == Sm.DrStr(dr, c[4]))
                                    {
                                        x.RKAPToDate = Sm.DrDec(dr, c[2]);
                                    }
                                }
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void GetOB(ref List<RptFicoSettingOpeningBalance> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT T.RptFicoCode, T.SettingCode, T.AcNo, T.EntCode, (SUM(T.DAmt) + SUM(T.CAmt)) Amt ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.RptFicoCode, A.SettingCode, B.AcNo, C.EntCode, If(B.AcType = 'C', D.Amt, 0.00) CAmt, ");
            SQL.AppendLine("    If(B.AcType = 'D', D.Amt, 0.00) DAmt ");
            SQL.AppendLine("    FROM TblRptFicoSettingDtl2 A ");
            SQL.AppendLine("    INNER JOIN TblCOA B ON B.ActInd = 'Y' ");
            SQL.AppendLine("        AND A.AcNo = B.AcNo  ");
            SQL.AppendLine("        AND A.RptFicoCode = @FicoCode ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("        And A.EntCode = @EntCode ");
            SQL.AppendLine("    INNER JOIN TblCOAOpeningBalanceHdr C ON C.Yr = @Yr ");
            SQL.AppendLine("        AND C.CancelInd = 'N' ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("        And C.EntCode = @EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And C.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(C.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("    INNER JOIN TblCOAOpeningBalanceDtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("        AND B.AcNo = D.AcNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("GROUP BY T.RptFicoCode, T.SettingCode, T.AcNo, T.EntCode; ");

            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));
            Sm.CmParam<string>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<string>(ref cm, "@Yr", Sm.GetLue(LueYr));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "AcNo", "Amt", "EntCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RptFicoSettingOpeningBalance()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            AcNo = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            EntCode = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetJOB(ref List<RptFicoSettingJournalOpeningBalance> l, string EntCodeLists)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.RptFicoCode, A.SettingCode, B.AcNo, C.AcNo AcNoReal, C.Level, C.Parent, D.EntCode, IFNULL(SUM(D.Amt), 0.00) Amt ");
            SQL.AppendLine("FROM TblRptFicoSettingDtl2 A ");
            SQL.AppendLine("INNER JOIN TblCOA B ON A.AcNo = B.AcNo ");
            SQL.AppendLine("    AND A.RptFicoCode = @FicoCode ");
            SQL.AppendLine("    AND B.ActInd = 'Y' ");
            SQL.AppendLine("INNER JOIN TblCOA C ON C.AcNo LIKE CONCAT(B.AcNo, '%') ");
            SQL.AppendLine("    AND C.ActInd = 'Y' ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T2.AcNo, T2.EntCode, If(T3.AcType = 'D', (T2.DAmt - T2.CAmt), (T2.CAmt - T2.DAmt)) Amt ");
            SQL.AppendLine("    FROM TblJournalHdr T1 ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        AND LEFT(T1.DocDt, 6) BETWEEN @StartYrMth AND @LastMth ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T2.AcNo In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT DISTINCT X3.AcNo ");
            SQL.AppendLine("            FROM TblRptFicoSettingDtl2 X1 ");
            SQL.AppendLine("            INNER JOIN TblCOA X2 ON X1.RptFicoCode = @FicoCode ");
            SQL.AppendLine("                AND X2.AcNo = X1.AcNo ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("                AND X1.EntCode = @EntCode ");
            SQL.AppendLine("            INNER JOIN TblCOA X3 ON X3.AcNo LIKE CONCAT(X2.AcNo, '%') ");
            SQL.AppendLine("        ) ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("        And T2.EntCode=@EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("        And T2.EntCode Is Not Null ");
                    SQL.AppendLine("        And Exists( ");
                    SQL.AppendLine("            Select 1 From TblGroupEntity ");
                    SQL.AppendLine("            Where EntCode=IfNull(T2.EntCode, '') ");
                    SQL.AppendLine("            And GrpCode In ( ");
                    SQL.AppendLine("                Select GrpCode From TblUser ");
                    SQL.AppendLine("                Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                }
            }
            SQL.AppendLine("    INNER JOIN TblCOA T3 On T2.AcNo = T3.AcNo ");
            SQL.AppendLine(") D ON C.AcNo = D.AcNo ");
            //SQL.AppendLine("Inner JOIN TblRptFicoSettingDtl2 E ON A.RptFicoCode = E.RptFicoCode ");
            //SQL.AppendLine("    AND A.SettingCode = E.SettingCode ");
            //SQL.AppendLine("    AND D.AcNo = E.AcNo ");
            //SQL.AppendLine("    AND D.EntCode = E.EntCode ");
            SQL.AppendLine("GROUP BY A.RptFicoCode, A.SettingCode, A.AcNo, C.AcNo, C.Level, C.Parent, D.EntCode ");
            SQL.AppendLine("HAVING IFNULL(SUM(D.Amt), 0.00) != 0.00 ");
            SQL.AppendLine("ORDER BY A.RptFicoCode, A.SettingCode, A.AcNo, C.AcNo Desc, D.EntCode; ");

            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));
            Sm.CmParam<string>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<string>(ref cm, "@StartYrMth", string.Concat(TxtStartYr.Text, "01"));
            Sm.CmParam<string>(ref cm, "@LastMth", string.Concat(Sm.GetLue(LueYr), mLastMonthNo));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "AcNo", "AcNoReal", "Level", "Parent", "Amt", "EntCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RptFicoSettingJournalOpeningBalance()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            AcNo = Sm.DrStr(dr, c[2]),
                            AcNoReal = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrInt(dr, c[4]),
                            ParentReal = Sm.DrStr(dr, c[5]),
                            IsDone = false,
                            Amt = Sm.DrDec(dr, c[6]),
                            EntCode = Sm.DrStr(dr, c[7]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void CalculateJOB(ref List<RptFicoSettingJournalOpeningBalance> l)
        {
            int mMaxLvlCOA = 0;
            string sSQL = "Select Max(Level) From TblCOA where ActInd = 'Y';";
            string mParent = string.Empty, mAcNo = string.Empty;
            decimal mAmt = 0m;

            if (Sm.GetValue(sSQL).Length > 0) mMaxLvlCOA = Int32.Parse(Sm.GetValue(sSQL));

            for (int i = mMaxLvlCOA; i > 0; --i)
            {
                foreach(var x in l
                    .Where(a => 
                        a.Level == i &&
                        a.IsDone == false))
                {
                    if (mParent.Length == 0) mParent = x.ParentReal;
                    if (mAcNo.Length == 0) mAcNo = x.AcNo;

                    if (mParent == x.ParentReal && mAcNo == x.AcNo)
                    {
                        if (x.Amt != 0m) mAmt += x.Amt;
                        x.IsDone = true;
                    }
                    else
                    {
                        if (mAmt != 0m)
                        {
                            foreach(var y in l
                                .Where(a =>
                                    a.AcNo == mAcNo &&
                                    a.AcNoReal == mParent
                                    ))
                            {
                                y.Amt = mAmt;
                                break;
                            }
                        }
                        mAmt = x.Amt;
                        mParent = x.ParentReal;
                        mAcNo = x.AcNo;
                    }
                }
            }
        }

        private void ProcessOB(ref List<RptFicoSettingCOA> l2, ref List<RptFicoSettingOpeningBalance> lOB)
        {
            if (Sm.GetLue(LueMth) == "01")
            {
                foreach (var x in lOB.OrderBy(o => o.SettingCode).ThenBy(o => o.AcNo).ThenBy(o => o.EntCode))
                {
                    if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                    {
                        foreach (var y in l2
                        .Where(a =>
                            a.FicoCode == x.FicoCode &&
                            a.SettingCode == x.SettingCode &&
                            a.AcNo == x.AcNo
                        ))
                        {
                            if (y.EntCode.Length == 0 || y.EntCode == Sm.GetLue(LueEntCode)) 
                                y.RealCurrentMth += x.Amt;
                            //break;
                        }
                    }
                    else
                    {
                        foreach (var y in l2
                        .Where(a =>
                            a.FicoCode == x.FicoCode &&
                            a.SettingCode == x.SettingCode &&
                            a.AcNo == x.AcNo
                        ))
                        {
                            if (y.EntCode.Length == 0) y.RealCurrentMth += x.Amt;
                            else
                            {
                                if (y.EntCode == x.EntCode) y.RealCurrentMth += x.Amt;
                            }
                            //break;
                        }
                    }
                }
            }
        }

        private void ProcessJOB(ref List<RptFicoSettingCOA> l2, ref List<RptFicoSettingOpeningBalance> lOB, ref List<RptFicoSettingJournalOpeningBalance> lJOB)
        {
            if (Sm.GetLue(LueMth) != "01")
            {
                if (lOB.Count > 0)
                {
                    foreach (var x in lOB.OrderBy(o => o.SettingCode).ThenBy(o => o.AcNo).ThenBy(o => o.EntCode))
                    {
                        if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                        {
                            foreach (var y in l2
                            .Where(a =>
                                a.FicoCode == x.FicoCode &&
                                a.SettingCode == x.SettingCode &&
                                a.AcNo == x.AcNo
                            ))
                            {
                                if (y.EntCode.Length == 0 || y.EntCode == Sm.GetLue(LueEntCode))
                                    y.RealOpeningBalance += x.Amt;
                                //break;
                            }
                        }
                        else
                        {
                            foreach (var y in l2
                            .Where(a =>
                                a.FicoCode == x.FicoCode &&
                                a.SettingCode == x.SettingCode &&
                                a.AcNo == x.AcNo
                            ))
                            {
                                if (y.EntCode.Length == 0) y.RealOpeningBalance += x.Amt;
                                else
                                {
                                    if (y.EntCode == x.EntCode) y.RealOpeningBalance += x.Amt;
                                }                                
                                //break;
                            }
                        }
                    }
                }
            }

            if (Sm.GetLue(LueMth) != "01")
            {
                foreach (var x in lJOB.OrderBy(o => o.SettingCode).ThenBy(o => o.AcNo).ThenBy(o => o.EntCode))
                {
                    if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                    {
                        foreach (var y in l2
                        .Where(a =>
                            a.FicoCode == x.FicoCode &&
                            a.SettingCode == x.SettingCode &&
                            a.AcNo == x.AcNo &&
                            a.AcNo == x.AcNoReal
                        ))
                        {
                            if (y.EntCode.Length == 0 || y.EntCode == Sm.GetLue(LueEntCode))
                                y.RealOpeningBalance += x.Amt;
                            //break;
                        }
                    }
                    else
                    {
                        foreach (var y in l2
                        .Where(a =>
                            a.FicoCode == x.FicoCode &&
                            a.SettingCode == x.SettingCode &&
                            a.AcNo == x.AcNo &&
                            a.AcNo == x.AcNoReal
                        ))
                        {
                            if (y.EntCode.Length == 0) y.RealOpeningBalance += x.Amt;
                            else
                            {
                                if (y.EntCode == x.EntCode) y.RealOpeningBalance += x.Amt;
                            }
                            //break;
                        }
                    }
                }
            }
        }

        private void GetJCM(ref List<RptFicoSettingJournalCurrentMonth> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.RptFicoCode, A.SettingCode, B.AcNo, C.AcNo AcNoReal, C.Level, C.Parent, D.EntCode, IFNULL(SUM(D.Amt), 0.00) Amt ");
            SQL.AppendLine("FROM TblRptFicoSettingDtl2 A ");
            SQL.AppendLine("INNER JOIN TblCOA B ON A.AcNo = B.AcNo ");
            SQL.AppendLine("    AND A.RptFicoCode = @FicoCode ");
            SQL.AppendLine("    AND B.ActInd = 'Y' ");
            SQL.AppendLine("INNER JOIN TblCOA C ON C.AcNo LIKE CONCAT(B.AcNo, '%') ");
            SQL.AppendLine("    AND C.ActInd = 'Y' ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T2.AcNo, T2.EntCode, If(T3.AcType = 'D', (T2.DAmt - T2.CAmt), (T2.CAmt - T2.DAmt)) Amt ");
            SQL.AppendLine("    FROM TblJournalHdr T1 ");
            SQL.AppendLine("    INNER JOIN TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        AND LEFT(T1.DocDt, 6) = @YrMth ");
            SQL.AppendLine("        AND T1.CancelInd = 'N' ");
            SQL.AppendLine("        AND T2.AcNo In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            SELECT DISTINCT X3.AcNo ");
            SQL.AppendLine("            FROM TblRptFicoSettingDtl2 X1 ");
            SQL.AppendLine("            INNER JOIN TblCOA X2 ON X1.RptFicoCode = @FicoCode ");
            SQL.AppendLine("                AND X2.AcNo = X1.AcNo ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                SQL.AppendLine("                AND X1.EntCode = @EntCode ");
            SQL.AppendLine("            INNER JOIN TblCOA X3 ON X3.AcNo LIKE CONCAT(X2.AcNo, '%') ");
            SQL.AppendLine("        ) ");
            if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
            {
                SQL.AppendLine("    And T2.EntCode=@EntCode ");
                if (mIsReportingFilterByEntity)
                {
                    SQL.AppendLine("And T2.EntCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=IfNull(T2.EntCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("    INNER JOIN TblCOA T3 On T2.AcNo = T3.AcNo ");
            SQL.AppendLine(") D ON C.AcNo = D.AcNo ");
            //SQL.AppendLine("Inner JOIN TblRptFicoSettingDtl2 E ON A.RptFicoCode = E.RptFicoCode ");
            //SQL.AppendLine("    AND A.SettingCode = E.SettingCode ");
            //SQL.AppendLine("    AND D.AcNo = E.AcNo ");
            //SQL.AppendLine("    AND D.EntCode = E.EntCode ");
            SQL.AppendLine("GROUP BY A.RptFicoCode, A.SettingCode, A.AcNo, C.AcNo, C.Level, C.Parent, D.EntCode ");
            SQL.AppendLine("HAVING IFNULL(SUM(D.Amt), 0.00) != 0.00 ");
            SQL.AppendLine("ORDER BY A.RptFicoCode, A.SettingCode, A.AcNo, C.AcNo Desc, D.EntCode; ");

            Sm.CmParam<string>(ref cm, "@FicoCode", Sm.GetLue(LueOptionCode));
            Sm.CmParam<string>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<string>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptFicoCode", "SettingCode", "AcNo", "AcNoReal", "Level", "Parent", "Amt", "EntCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RptFicoSettingJournalCurrentMonth()
                        {
                            FicoCode = Sm.DrStr(dr, c[0]),
                            SettingCode = Sm.DrStr(dr, c[1]),
                            AcNo = Sm.DrStr(dr, c[2]),
                            AcNoReal = Sm.DrStr(dr, c[3]),
                            Level = Sm.DrInt(dr, c[4]),
                            ParentReal = Sm.DrStr(dr, c[5]),
                            IsDone = false,
                            Amt = Sm.DrDec(dr, c[6]),
                            EntCode = Sm.DrStr(dr, c[7]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void CalculateJCM(ref List<RptFicoSettingJournalCurrentMonth> l)
        {
            int mMaxLvlCOA = 0;
            string sSQL = "Select Max(Level) From TblCOA where ActInd = 'Y';";
            string mParent = string.Empty, mAcNo = string.Empty;
            decimal mAmt = 0m;

            if (Sm.GetValue(sSQL).Length > 0) mMaxLvlCOA = Int32.Parse(Sm.GetValue(sSQL));

            for (int i = mMaxLvlCOA; i > 0; --i)
            {
                foreach (var x in l
                    .Where(a =>
                        a.Level == i &&
                        a.IsDone == false))
                {
                    if (mParent.Length == 0) mParent = x.ParentReal;
                    if (mAcNo.Length == 0) mAcNo = x.AcNo;

                    if (mParent == x.ParentReal && mAcNo == x.AcNo)
                    {
                        if (x.Amt != 0m) mAmt += x.Amt;
                        x.IsDone = true;
                    }
                    else
                    {
                        if (mAmt != 0m)
                        {
                            foreach (var y in l
                                .Where(a =>
                                    a.AcNo == mAcNo &&
                                    a.AcNoReal == mParent
                                    ))
                            {
                                y.Amt = mAmt;
                                break;
                            }
                        }
                        mAmt = x.Amt;
                        mParent = x.ParentReal;
                        mAcNo = x.AcNo;
                    }
                }
            }
        }

        private void ProcessJCM(ref List<RptFicoSettingCOA> l2, ref List<RptFicoSettingJournalCurrentMonth> lJCM)
        {
            foreach (var x in lJCM.OrderBy(o => o.SettingCode).ThenBy(o => o.AcNo).ThenBy(o => o.EntCode))
            {
                if (Sm.GetLue(LueEntCode).Length > 0 && Sm.GetLue(LueEntCode) != "Consolidate")
                {
                    foreach (var y in l2
                    .Where(a =>
                        a.FicoCode == x.FicoCode &&
                        a.SettingCode == x.SettingCode &&
                        a.AcNo == x.AcNo &&
                        a.AcNo == x.AcNoReal
                    ))
                    {
                        if (y.EntCode.Length == 0 || y.EntCode == Sm.GetLue(LueEntCode))
                            y.RealCurrentMth += x.Amt;
                        //break;
                    }
                }
                else
                {
                    foreach (var y in l2
                    .Where(a =>
                        a.FicoCode == x.FicoCode &&
                        a.SettingCode == x.SettingCode &&
                        a.AcNo == x.AcNo &&
                        a.AcNo == x.AcNoReal
                    ))
                    {
                        if (y.EntCode.Length == 0) y.RealCurrentMth += x.Amt;
                        else
                        {
                            if (y.EntCode == x.EntCode) y.RealCurrentMth += x.Amt;
                        }                        
                        //break;
                    }
                }
            }
        }

        private void ProcessFicoCOA2(ref List<RptFicoSettingCOASummary> l2)
        {
            foreach(var x in l2)
            {
                decimal mDevider = Decimal.Parse(Sm.GetLue(LueMth));
                if(TxtStartYr.Text != Sm.GetLue(LueYr)) mDevider += (((Decimal.Parse(Sm.GetLue(LueYr)) - Decimal.Parse(TxtStartYr.Text)) * 12m));

                if (x.RealOpeningBalance != 0m && (mDevider - 1m) != 0m) x.AvgOpeningBalance = x.RealOpeningBalance / (mDevider - 1m);
                x.RealToDate = x.RealOpeningBalance + x.RealCurrentMth;
            }
        }

        private void ProcessFicoCOA3(ref List<RptFicoSetting> l, ref List<RptFicoSettingCOASummary> l2)
        {
            foreach(var x in l)
            {
                foreach(var y in l2
                    .Where(a =>
                        a.FicoCode == x.FicoCode &&
                        a.SettingCode == x.SettingCode
                    ))
                {
                    x.RKAP = y.RKAP;
                    x.RealOpeningBalance = y.RealOpeningBalance;
                    x.AvgOpeningBalance = y.AvgOpeningBalance;
                    x.RKAPCurrentMth = y.RKAPCurrentMth;
                    x.RealCurrentMth = y.RealCurrentMth;
                    x.RKAPToDate = y.RKAPToDate;
                    x.RealToDate = y.RealToDate;

                    break;
                }
            }
        }

        #endregion

        #region Fico Formula

        private void ProcessFicoFormula(ref List<RptFicoSetting> l)
        {
            foreach(var x in l
                .Where(a =>
                    a.Formula.Length > 0 &&
                    a.AcNo.Length <= 0
                )
                .OrderBy(o => o.SettingCode))
            {
                char[] delimiter = { '+', '-' };
                string SQLFormula = x.Formula;
                string SQLFormula1 = SQLFormula, // RKAP
                    SQLFormula2 = SQLFormula, // RealOpeningBalance
                    SQLFormula3 = SQLFormula, // AvgOpeningBalance
                    SQLFormula4 = SQLFormula, // RKAPCurrentMonth
                    SQLFormula5 = SQLFormula, // RealCurrentMonth
                    SQLFormula6 = SQLFormula, // RKAPToDate
                    SQLFormula7 = SQLFormula; // RealToDate
                string[] ArraySQLFormula = x.Formula.Split(delimiter);
                foreach(var y in ArraySQLFormula)
                {
                    foreach (var z in l
                        .Where(a =>
                            a.SettingCode == y
                        )
                        .OrderBy(o => o.SettingCode))
                    {
                        string
                            oldS = y,
                            newS1 = z.RKAP.ToString(),
                            newS2 = z.RealOpeningBalance.ToString(),
                            newS3 = z.AvgOpeningBalance.ToString(),
                            newS4 = z.RKAPCurrentMth.ToString(),
                            newS5 = z.RealCurrentMth.ToString(),
                            newS6 = z.RKAPToDate.ToString(),
                            newS7 = z.RealToDate.ToString();

                        SQLFormula1 = SQLFormula1.Replace(oldS, newS1);
                        SQLFormula2 = SQLFormula2.Replace(oldS, newS2);
                        SQLFormula3 = SQLFormula3.Replace(oldS, newS3);
                        SQLFormula4 = SQLFormula4.Replace(oldS, newS4);
                        SQLFormula5 = SQLFormula5.Replace(oldS, newS5);
                        SQLFormula6 = SQLFormula6.Replace(oldS, newS6);
                        SQLFormula7 = SQLFormula7.Replace(oldS, newS7);

                        break;
                    }
                }
                
                x.RKAP = Decimal.Parse(Sm.GetValue(string.Concat("Select ", SQLFormula1)));
                x.RealOpeningBalance = Decimal.Parse(Sm.GetValue(string.Concat("Select ", SQLFormula2)));
                x.AvgOpeningBalance = Decimal.Parse(Sm.GetValue(string.Concat("Select ", SQLFormula3)));
                x.RKAPCurrentMth = Decimal.Parse(Sm.GetValue(string.Concat("Select ", SQLFormula4)));
                x.RealCurrentMth = Decimal.Parse(Sm.GetValue(string.Concat("Select ", SQLFormula5)));
                x.RKAPToDate = Decimal.Parse(Sm.GetValue(string.Concat("Select ", SQLFormula6)));
                x.RealToDate = Decimal.Parse(Sm.GetValue(string.Concat("Select ", SQLFormula7)));
            }
        }

        #endregion

        // dapetin semua persentase
        private void ProcessFicoSetting2(ref List<RptFicoSetting> l)
        {
            foreach(var x in l)
            {
                if (x.RKAPToDate != 0m) x.Percentage1 = (x.RealToDate / x.RKAPToDate) * 100m;
                if (x.RKAP != 0m) x.Percentage2 = (x.RealToDate / x.RKAP) * 100m;
                if (x.RKAPCurrentMth != 0m) x.Percentage3 = (x.RealCurrentMth / x.RKAPCurrentMth) * 100m;
                if (x.AvgOpeningBalance != 0m) x.Percentage4 = (x.RealCurrentMth / x.AvgOpeningBalance) * 100m;
            }
        }

        //input data ke grid
        private void ProcessFicoSetting3(ref List<RptFicoSetting> l)
        {
            int Row = 0;
            foreach(var x in l)
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 0].Value = x.SettingDesc;
                Grd1.Cells[Row, 1].Value = x.RKAP;
                Grd1.Cells[Row, 2].Value = x.RealOpeningBalance;
                Grd1.Cells[Row, 3].Value = x.AvgOpeningBalance;
                Grd1.Cells[Row, 4].Value = x.RKAPCurrentMth;
                Grd1.Cells[Row, 5].Value = x.RealCurrentMth;
                Grd1.Cells[Row, 6].Value = x.RKAPToDate;
                Grd1.Cells[Row, 7].Value = x.RealToDate;
                Grd1.Cells[Row, 8].Value = x.Percentage1;
                Grd1.Cells[Row, 9].Value = x.Percentage2;
                Grd1.Cells[Row, 10].Value = x.Percentage3;
                Grd1.Cells[Row, 11].Value = x.Percentage4;

                Row += 1;
            }
        }

        #endregion

        private void SetTimeStampVariable()
        {
            mThisYear = Sm.GetLue(LueYr);
            mLastYear = Sm.GetLue(LueYr);
            mThisMonth = string.Empty;
            mLastMonth = string.Empty;
            mLastMonthNo = string.Empty;

            if (Sm.GetLue(LueMth).Length > 0)
            {
                mThisMonth = mMths[Int32.Parse(Sm.GetLue(LueMth)) - 1];
                if (Sm.GetLue(LueMth) == "01")
                {
                    mLastMonthNo = "12";
                    mLastMonth = mMths[11];
                    mLastYear = (Int32.Parse(Sm.GetLue(LueYr)) - 1).ToString();
                }
                else
                {
                    mLastMonthNo = Sm.Right(string.Concat("00", (Int32.Parse(Sm.GetLue(LueMth)) - 1).ToString()), 2);
                    mLastMonth = mMths[Int32.Parse(Sm.GetLue(LueMth)) - 2];
                }
            }
        }

        private void SetLueFicoFormula(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.RptFicoCode As Col1, T.RptFicoName As Col2 ");
            SQL.AppendLine("From TblRptFicoSettingHdr T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where (T.SiteCode Is Null Or ");
                SQL.AppendLine("(T.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueEntCode(ref LookUpEdit Lue, string Code, string IsFilterByEnt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Consolidate' As Col1, 'Consolidate' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            if (IsFilterByEnt == "Y")
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupEntity ");
                SQL.AppendLine("    Where EntCode=T.EntCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By Col2;");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void GetParameter()
        {
            //mAcNoForCurrentEarning = Sm.GetParameter("AcNoForCurrentEarning");
            //mAcNoForIncome = Sm.GetParameter("AcNoForIncome");
            //mAcNoForCost = Sm.GetParameter("AcNoForCost");
            //mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            //mIsCOAAssetUseStartYr = Sm.GetParameterBoo("IsCOAAssetUseStartYr");
            //mCOAAssetStartYr = Sm.GetParameter("COAAssetStartYr");
            //mCOAAssetAcNo = Sm.GetParameter("COAAssetAcNo");
            //mAccountingRptStartFrom = Sm.GetParameter("AccountingRptStartFrom");
            //mIsFilterByEnt = Sm.GetParameterBoo("IsFilterByEnt");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsFilterByEnt = Sm.GetParameterBoo("IsFilterByEnt");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsReportingFilterByEntity = Sm.GetParameterBoo("IsReportingFilterByEntity");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue3(SetLueEntCode), string.Empty, mIsFilterByEnt ? "Y" : "N");
        }

        private void LueOptionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOptionCode, new Sm.RefreshLue1(SetLueFicoFormula));
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueYr).Length > 0)
            {
                TxtStartYr.EditValue = Sm.GetLue(LueYr);
            }
        }

        #endregion

        #endregion

        #region Class

        private class RptFicoSetting
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string SettingDesc { get; set; }
            public string Sequence { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string Formula { get; set; }
            public decimal RKAP { get; set; }
            public decimal RealOpeningBalance { get; set; }
            public decimal AvgOpeningBalance { get; set; }
            public decimal RKAPCurrentMth { get; set; }
            public decimal RealCurrentMth { get; set; }
            public decimal RKAPToDate { get; set; }
            public decimal RealToDate { get; set; }
            public decimal Percentage1 { get; set; }
            public decimal Percentage2 { get; set; }
            public decimal Percentage3 { get; set; }
            public decimal Percentage4 { get; set; }
        }

        private class RptFicoSettingCOA
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public decimal RKAP { get; set; }
            public decimal RealOpeningBalance { get; set; }
            public decimal AvgOpeningBalance { get; set; }
            public decimal RKAPCurrentMth { get; set; }
            public decimal RealCurrentMth { get; set; }
            public decimal RKAPToDate { get; set; }
            public decimal RealToDate { get; set; }
        }

        private class RptFicoSettingCOASummary
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public decimal RKAP { get; set; }
            public decimal RealOpeningBalance { get; set; }
            public decimal AvgOpeningBalance { get; set; }
            public decimal RKAPCurrentMth { get; set; }
            public decimal RealCurrentMth { get; set; }
            public decimal RKAPToDate { get; set; }
            public decimal RealToDate { get; set; }
        }

        private class RptFicoSettingOpeningBalance
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class RptFicoSettingJournalOpeningBalance
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string AcNoReal { get; set; }
            public string ParentReal { get; set; }
            public int Level { get; set; }
            public bool IsDone { get; set; }
            public decimal Amt { get; set; }
        }

        private class RptFicoSettingJournalCurrentMonth
        {
            public string FicoCode { get; set; }
            public string SettingCode { get; set; }
            public string AcNo { get; set; }
            public string EntCode { get; set; }
            public string AcNoReal { get; set; }
            public string ParentReal { get; set; }
            public int Level { get; set; }
            public bool IsDone { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
