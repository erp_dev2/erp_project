﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWOSettlement : RunSystem.FrmBase5
    {
        #region Field

        private string mMenuCode = string.Empty, mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmWOSettlement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            SetGrd();
            SetSQL();
            base.FrmLoad(sender, e);
        }

        #endregion

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WORDocNo, ");
            SQL.AppendLine("C.OptDesc As MtcStatusDesc, D.OptDesc As MtcTypeDesc, E.OptDesc As SymProblemDesc, ");
            SQL.AppendLine("B.Description ");
            SQL.AppendLine("From TblWOHdr A");
            SQL.AppendLine("Inner Join TblWORHdr B On A.WORDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblOption C On B.MtcStatus=C.OptCode And C.OptCat ='MaintenanceStatus' ");
            SQL.AppendLine("Left Join TblOption D On B.MtcType=D.OptCode And D.OptCat ='MaintenanceType' ");
            SQL.AppendLine("Left Join TblOption E On B.SymProblem=E.OptCode And E.OptCat ='SymptomProblem' ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.SettleInd='N' ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "WO#", 
                        "",
                        "Date",
                        "WO Request#",
                        "Status",

                        //6-8
                        "Type",
                        "Symptom"+Environment.NewLine+"Problem",
                        "Description"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 80, 130, 100, 
                        
                        //6-8
                        180, 180, 250
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 4, 5, 6, 7, 8 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt Desc, A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "WORDocNo", "MtcStatusDesc", "MtcTypeDesc", "SymProblemDesc", 

                            //6
                            "Description"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            try
            {
                if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdBool(Grd1, r, 0)) cml.Add(SaveWOHdr(Sm.GetGrdStr(Grd1, r, 1)));

                Sm.ExecCommands(cml);
                ShowData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return
                IsDataNotExisted() ||
                IsWOAlreadyCancelled() ||
                IsWOAlreadySettled();
        }

        private bool IsDataNotExisted()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdBool(Grd1, r, 0)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to settle minimum 1 WO.");
            return true;
        }

        private bool IsWOAlreadyCancelled()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0) && IsWOAlreadyCancelled(Sm.GetGrdStr(Grd1, r, 1)))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "WO# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine + 
                        "This WO already cancelled.");
                    return true;
                }
            }
            return false;
        }

        private bool IsWOAlreadyCancelled(string DocNo)
        {
            return Sm.IsDataExist("Select DocNo From TblWOHdr Where CancelInd='Y' And DocNo=@Param;", DocNo);
        }

        private bool IsWOAlreadySettled()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0) && IsWOAlreadySettled(Sm.GetGrdStr(Grd1, r, 1)))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "WO# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine + 
                        "This WO already settled.");
                    return true;
                }
            }
            return false;
        }

        private bool IsWOAlreadySettled(string DocNo)
        {
            return Sm.IsDataExist("Select DocNo From TblWOHdr Where SettleInd='Y' And DocNo=@Param;", DocNo);
        }

        private MySqlCommand SaveWOHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblWOHdr Set SettleInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And CancelInd='N' And SettleInd='N'; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
 

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
