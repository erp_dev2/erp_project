﻿namespace RunSystem
{
    partial class FrmSurvey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSurvey));
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.LueProposal = new DevExpress.XtraEditors.LookUpEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtWorkExp = new DevExpress.XtraEditors.TextEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TcSurvey = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtManPower = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtNoOfLoan = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.LueDevelopment = new DevExpress.XtraEditors.LookUpEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.LueFiles = new DevExpress.XtraEditors.LookUpEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.LuePosting = new DevExpress.XtraEditors.LookUpEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtAssurance = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtInstallment = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtAsset = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtOmzet = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LueBussinessDomicile = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LueLicence = new DevExpress.XtraEditors.LookUpEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.LueSIUP = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.LueIDNumber = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TxtDueDt = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtTotalAmt = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TxtInterestRateAmt = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtInterestRate = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.LueMonth = new DevExpress.XtraEditors.LookUpEdit();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.BtnVoucherDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnVoucherRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnRQLPDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtVoucherRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.DteEstimatedDt = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnRQLPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.TxtRQLPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.LueBSCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtIdentityNo = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtCompanyName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.MeeCompanyAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtPnName = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProposal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkExp.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TcSurvey)).BeginInit();
            this.TcSurvey.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtManPower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfLoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDevelopment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFiles.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssurance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInstallment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOmzet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBussinessDomicile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLicence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSIUP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueIDNumber.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestRateAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstimatedDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstimatedDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRQLPDocNo.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBSCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompanyAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPnName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(805, 0);
            this.panel1.Size = new System.Drawing.Size(70, 535);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(805, 535);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(102, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(111, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(24, 8);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(73, 14);
            this.label72.TabIndex = 9;
            this.label72.Text = "Document#";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(102, 5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(226, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 35);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 14);
            this.label4.TabIndex = 56;
            this.label4.Text = "Proposal Completeness";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(102, 173);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(289, 20);
            this.MeeRemark.TabIndex = 31;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(50, 176);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 30;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Red;
            this.label61.Location = new System.Drawing.Point(103, 7);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(52, 14);
            this.label61.TabIndex = 54;
            this.label61.Text = "Proposal";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProposal
            // 
            this.LueProposal.EnterMoveNextControl = true;
            this.LueProposal.Location = new System.Drawing.Point(162, 5);
            this.LueProposal.Margin = new System.Windows.Forms.Padding(5);
            this.LueProposal.Name = "LueProposal";
            this.LueProposal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProposal.Properties.Appearance.Options.UseFont = true;
            this.LueProposal.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProposal.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProposal.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProposal.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProposal.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProposal.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProposal.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProposal.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProposal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProposal.Properties.DropDownRows = 30;
            this.LueProposal.Properties.MaxLength = 16;
            this.LueProposal.Properties.NullText = "[Empty]";
            this.LueProposal.Properties.PopupWidth = 300;
            this.LueProposal.Size = new System.Drawing.Size(132, 20);
            this.LueProposal.TabIndex = 55;
            this.LueProposal.ToolTip = "F4 : Show/hide list";
            this.LueProposal.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProposal.EditValueChanged += new System.EventHandler(this.LueProposal_EditValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(64, 29);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 14);
            this.label25.TabIndex = 11;
            this.label25.Text = "Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(331, 46);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 15;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(79, 28);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 14);
            this.label28.TabIndex = 66;
            this.label28.Text = "Work Experience";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWorkExp
            // 
            this.TxtWorkExp.EnterMoveNextControl = true;
            this.TxtWorkExp.Location = new System.Drawing.Point(183, 25);
            this.TxtWorkExp.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkExp.Name = "TxtWorkExp";
            this.TxtWorkExp.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWorkExp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkExp.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkExp.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkExp.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWorkExp.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWorkExp.Properties.MaxLength = 13;
            this.TxtWorkExp.Size = new System.Drawing.Size(175, 20);
            this.TxtWorkExp.TabIndex = 67;
            this.TxtWorkExp.Validated += new System.EventHandler(this.TxtWorkExp_Validated);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TcSurvey);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 218);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(805, 317);
            this.panel3.TabIndex = 54;
            // 
            // TcSurvey
            // 
            this.TcSurvey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcSurvey.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcSurvey.Location = new System.Drawing.Point(0, 0);
            this.TcSurvey.Name = "TcSurvey";
            this.TcSurvey.SelectedTabPage = this.Tp1;
            this.TcSurvey.Size = new System.Drawing.Size(805, 317);
            this.TcSurvey.TabIndex = 53;
            this.TcSurvey.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(799, 289);
            this.Tp1.Text = "Survey Result";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.LueLicence);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.LueSIUP);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.LueIDNumber);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.LueProposal);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label61);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(799, 289);
            this.panel5.TabIndex = 22;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label36);
            this.panel6.Controls.Add(this.TxtManPower);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Controls.Add(this.label35);
            this.panel6.Controls.Add(this.label33);
            this.panel6.Controls.Add(this.TxtAmt);
            this.panel6.Controls.Add(this.label32);
            this.panel6.Controls.Add(this.TxtNoOfLoan);
            this.panel6.Controls.Add(this.label31);
            this.panel6.Controls.Add(this.LueDevelopment);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.LueFiles);
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.LuePosting);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.TxtAssurance);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.TxtInstallment);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.TxtAsset);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.TxtOmzet);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.LueBussinessDomicile);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.TxtWorkExp);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(345, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(454, 289);
            this.panel6.TabIndex = 63;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(363, 50);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(44, 14);
            this.label36.TabIndex = 71;
            this.label36.Text = "People";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtManPower
            // 
            this.TxtManPower.EnterMoveNextControl = true;
            this.TxtManPower.Location = new System.Drawing.Point(183, 47);
            this.TxtManPower.Margin = new System.Windows.Forms.Padding(5);
            this.TxtManPower.Name = "TxtManPower";
            this.TxtManPower.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtManPower.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtManPower.Properties.Appearance.Options.UseBackColor = true;
            this.TxtManPower.Properties.Appearance.Options.UseFont = true;
            this.TxtManPower.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtManPower.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtManPower.Properties.MaxLength = 13;
            this.TxtManPower.Size = new System.Drawing.Size(175, 20);
            this.TxtManPower.TabIndex = 70;
            this.TxtManPower.Validated += new System.EventHandler(this.TxtManPower_Validated);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(115, 50);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 14);
            this.label37.TabIndex = 69;
            this.label37.Text = "Manpower";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(364, 116);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(42, 14);
            this.label35.TabIndex = 78;
            this.label35.Text = "Month";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(363, 28);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(32, 14);
            this.label33.TabIndex = 68;
            this.label33.Text = "Year";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(184, 265);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.MaxLength = 13;
            this.TxtAmt.Size = new System.Drawing.Size(175, 20);
            this.TxtAmt.TabIndex = 91;
            this.TxtAmt.Validated += new System.EventHandler(this.TxtAmt_Validated);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(98, 268);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(81, 14);
            this.label32.TabIndex = 90;
            this.label32.Text = "Loan Amount";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNoOfLoan
            // 
            this.TxtNoOfLoan.EnterMoveNextControl = true;
            this.TxtNoOfLoan.Location = new System.Drawing.Point(183, 243);
            this.TxtNoOfLoan.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNoOfLoan.Name = "TxtNoOfLoan";
            this.TxtNoOfLoan.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNoOfLoan.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNoOfLoan.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNoOfLoan.Properties.Appearance.Options.UseFont = true;
            this.TxtNoOfLoan.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNoOfLoan.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNoOfLoan.Properties.MaxLength = 13;
            this.TxtNoOfLoan.Size = new System.Drawing.Size(175, 20);
            this.TxtNoOfLoan.TabIndex = 89;
            this.TxtNoOfLoan.Validated += new System.EventHandler(this.TxtNoOfLoan_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(108, 246);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(71, 14);
            this.label31.TabIndex = 88;
            this.label31.Text = "No. of Loan";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDevelopment
            // 
            this.LueDevelopment.EnterMoveNextControl = true;
            this.LueDevelopment.Location = new System.Drawing.Point(183, 222);
            this.LueDevelopment.Margin = new System.Windows.Forms.Padding(5);
            this.LueDevelopment.Name = "LueDevelopment";
            this.LueDevelopment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDevelopment.Properties.Appearance.Options.UseFont = true;
            this.LueDevelopment.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDevelopment.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDevelopment.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDevelopment.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDevelopment.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDevelopment.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDevelopment.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDevelopment.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDevelopment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDevelopment.Properties.DropDownRows = 30;
            this.LueDevelopment.Properties.MaxLength = 16;
            this.LueDevelopment.Properties.NullText = "[Empty]";
            this.LueDevelopment.Properties.PopupWidth = 300;
            this.LueDevelopment.Size = new System.Drawing.Size(175, 20);
            this.LueDevelopment.TabIndex = 87;
            this.LueDevelopment.ToolTip = "F4 : Show/hide list";
            this.LueDevelopment.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDevelopment.EditValueChanged += new System.EventHandler(this.LueDevelopment_EditValueChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(40, 225);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(139, 14);
            this.label30.TabIndex = 86;
            this.label30.Text = "Development Probability";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFiles
            // 
            this.LueFiles.EnterMoveNextControl = true;
            this.LueFiles.Location = new System.Drawing.Point(183, 199);
            this.LueFiles.Margin = new System.Windows.Forms.Padding(5);
            this.LueFiles.Name = "LueFiles";
            this.LueFiles.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFiles.Properties.Appearance.Options.UseFont = true;
            this.LueFiles.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFiles.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFiles.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFiles.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFiles.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFiles.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFiles.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFiles.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFiles.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFiles.Properties.DropDownRows = 30;
            this.LueFiles.Properties.MaxLength = 16;
            this.LueFiles.Properties.NullText = "[Empty]";
            this.LueFiles.Properties.PopupWidth = 300;
            this.LueFiles.Size = new System.Drawing.Size(175, 20);
            this.LueFiles.TabIndex = 85;
            this.LueFiles.ToolTip = "F4 : Show/hide list";
            this.LueFiles.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFiles.EditValueChanged += new System.EventHandler(this.LueFiles_EditValueChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(114, 202);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 14);
            this.label29.TabIndex = 84;
            this.label29.Text = "Other Files";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosting
            // 
            this.LuePosting.EnterMoveNextControl = true;
            this.LuePosting.Location = new System.Drawing.Point(183, 177);
            this.LuePosting.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosting.Name = "LuePosting";
            this.LuePosting.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosting.Properties.Appearance.Options.UseFont = true;
            this.LuePosting.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosting.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosting.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosting.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosting.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosting.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosting.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosting.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosting.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosting.Properties.DropDownRows = 30;
            this.LuePosting.Properties.MaxLength = 16;
            this.LuePosting.Properties.NullText = "[Empty]";
            this.LuePosting.Properties.PopupWidth = 300;
            this.LuePosting.Size = new System.Drawing.Size(175, 20);
            this.LuePosting.TabIndex = 83;
            this.LuePosting.ToolTip = "F4 : Show/hide list";
            this.LuePosting.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosting.EditValueChanged += new System.EventHandler(this.LuePosting_EditValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(45, 180);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(134, 14);
            this.label27.TabIndex = 82;
            this.label27.Text = "Posting / Book keeping";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(81, 162);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 14);
            this.label9.TabIndex = 81;
            this.label9.Text = "Administration";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssurance
            // 
            this.TxtAssurance.EnterMoveNextControl = true;
            this.TxtAssurance.Location = new System.Drawing.Point(183, 135);
            this.TxtAssurance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssurance.Name = "TxtAssurance";
            this.TxtAssurance.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAssurance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssurance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssurance.Properties.Appearance.Options.UseFont = true;
            this.TxtAssurance.Properties.MaxLength = 500;
            this.TxtAssurance.Size = new System.Drawing.Size(175, 20);
            this.TxtAssurance.TabIndex = 80;
            this.TxtAssurance.Validated += new System.EventHandler(this.TxtAssurance_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(117, 138);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 14);
            this.label22.TabIndex = 79;
            this.label22.Text = "Assurance";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInstallment
            // 
            this.TxtInstallment.EnterMoveNextControl = true;
            this.TxtInstallment.Location = new System.Drawing.Point(183, 113);
            this.TxtInstallment.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInstallment.Name = "TxtInstallment";
            this.TxtInstallment.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInstallment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInstallment.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInstallment.Properties.Appearance.Options.UseFont = true;
            this.TxtInstallment.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInstallment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInstallment.Properties.MaxLength = 3;
            this.TxtInstallment.Size = new System.Drawing.Size(175, 20);
            this.TxtInstallment.TabIndex = 77;
            this.TxtInstallment.Validated += new System.EventHandler(this.TxtInstallment_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(6, 116);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(173, 14);
            this.label18.TabIndex = 76;
            this.label18.Text = "Installment Payment Capability";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAsset
            // 
            this.TxtAsset.EnterMoveNextControl = true;
            this.TxtAsset.Location = new System.Drawing.Point(183, 91);
            this.TxtAsset.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAsset.Name = "TxtAsset";
            this.TxtAsset.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAsset.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsset.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAsset.Properties.Appearance.Options.UseFont = true;
            this.TxtAsset.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAsset.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAsset.Properties.MaxLength = 13;
            this.TxtAsset.Size = new System.Drawing.Size(175, 20);
            this.TxtAsset.TabIndex = 75;
            this.TxtAsset.Validated += new System.EventHandler(this.TxtAsset_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(142, 94);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 14);
            this.label17.TabIndex = 74;
            this.label17.Text = "Asset";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOmzet
            // 
            this.TxtOmzet.EnterMoveNextControl = true;
            this.TxtOmzet.Location = new System.Drawing.Point(183, 69);
            this.TxtOmzet.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOmzet.Name = "TxtOmzet";
            this.TxtOmzet.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtOmzet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOmzet.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOmzet.Properties.Appearance.Options.UseFont = true;
            this.TxtOmzet.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOmzet.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOmzet.Properties.MaxLength = 13;
            this.TxtOmzet.Size = new System.Drawing.Size(175, 20);
            this.TxtOmzet.TabIndex = 73;
            this.TxtOmzet.Validated += new System.EventHandler(this.TxtOmzet_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(89, 72);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 14);
            this.label16.TabIndex = 72;
            this.label16.Text = "Monthly Omzet";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBussinessDomicile
            // 
            this.LueBussinessDomicile.EnterMoveNextControl = true;
            this.LueBussinessDomicile.Location = new System.Drawing.Point(183, 3);
            this.LueBussinessDomicile.Margin = new System.Windows.Forms.Padding(5);
            this.LueBussinessDomicile.Name = "LueBussinessDomicile";
            this.LueBussinessDomicile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBussinessDomicile.Properties.Appearance.Options.UseFont = true;
            this.LueBussinessDomicile.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBussinessDomicile.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBussinessDomicile.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBussinessDomicile.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBussinessDomicile.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBussinessDomicile.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBussinessDomicile.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBussinessDomicile.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBussinessDomicile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBussinessDomicile.Properties.DropDownRows = 30;
            this.LueBussinessDomicile.Properties.MaxLength = 16;
            this.LueBussinessDomicile.Properties.NullText = "[Empty]";
            this.LueBussinessDomicile.Properties.PopupWidth = 300;
            this.LueBussinessDomicile.Size = new System.Drawing.Size(175, 20);
            this.LueBussinessDomicile.TabIndex = 65;
            this.LueBussinessDomicile.ToolTip = "F4 : Show/hide list";
            this.LueBussinessDomicile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBussinessDomicile.EditValueChanged += new System.EventHandler(this.LueBussinessDomicile_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(74, 6);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 14);
            this.label15.TabIndex = 64;
            this.label15.Text = "Bussiness Domicile";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLicence
            // 
            this.LueLicence.EnterMoveNextControl = true;
            this.LueLicence.Location = new System.Drawing.Point(162, 98);
            this.LueLicence.Margin = new System.Windows.Forms.Padding(5);
            this.LueLicence.Name = "LueLicence";
            this.LueLicence.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLicence.Properties.Appearance.Options.UseFont = true;
            this.LueLicence.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLicence.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLicence.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLicence.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLicence.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLicence.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLicence.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLicence.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLicence.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLicence.Properties.DropDownRows = 30;
            this.LueLicence.Properties.MaxLength = 16;
            this.LueLicence.Properties.NullText = "[Empty]";
            this.LueLicence.Properties.PopupWidth = 300;
            this.LueLicence.Size = new System.Drawing.Size(132, 20);
            this.LueLicence.TabIndex = 62;
            this.LueLicence.ToolTip = "F4 : Show/hide list";
            this.LueLicence.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLicence.EditValueChanged += new System.EventHandler(this.LueLicence_EditValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(65, 101);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 14);
            this.label14.TabIndex = 61;
            this.label14.Text = "Licence / Deed";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSIUP
            // 
            this.LueSIUP.EnterMoveNextControl = true;
            this.LueSIUP.Location = new System.Drawing.Point(162, 76);
            this.LueSIUP.Margin = new System.Windows.Forms.Padding(5);
            this.LueSIUP.Name = "LueSIUP";
            this.LueSIUP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSIUP.Properties.Appearance.Options.UseFont = true;
            this.LueSIUP.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSIUP.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSIUP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSIUP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSIUP.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSIUP.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSIUP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSIUP.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSIUP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSIUP.Properties.DropDownRows = 30;
            this.LueSIUP.Properties.MaxLength = 16;
            this.LueSIUP.Properties.NullText = "[Empty]";
            this.LueSIUP.Properties.PopupWidth = 300;
            this.LueSIUP.Size = new System.Drawing.Size(132, 20);
            this.LueSIUP.TabIndex = 60;
            this.LueSIUP.ToolTip = "F4 : Show/hide list";
            this.LueSIUP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSIUP.EditValueChanged += new System.EventHandler(this.LueSIUP_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(122, 79);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 14);
            this.label13.TabIndex = 59;
            this.label13.Text = "SIUP";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueIDNumber
            // 
            this.LueIDNumber.EnterMoveNextControl = true;
            this.LueIDNumber.Location = new System.Drawing.Point(162, 54);
            this.LueIDNumber.Margin = new System.Windows.Forms.Padding(5);
            this.LueIDNumber.Name = "LueIDNumber";
            this.LueIDNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueIDNumber.Properties.Appearance.Options.UseFont = true;
            this.LueIDNumber.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueIDNumber.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueIDNumber.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueIDNumber.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueIDNumber.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueIDNumber.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueIDNumber.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueIDNumber.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueIDNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueIDNumber.Properties.DropDownRows = 30;
            this.LueIDNumber.Properties.MaxLength = 16;
            this.LueIDNumber.Properties.NullText = "[Empty]";
            this.LueIDNumber.Properties.PopupWidth = 300;
            this.LueIDNumber.Size = new System.Drawing.Size(132, 20);
            this.LueIDNumber.TabIndex = 58;
            this.LueIDNumber.ToolTip = "F4 : Show/hide list";
            this.LueIDNumber.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueIDNumber.EditValueChanged += new System.EventHandler(this.LueIDNumber_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(89, 57);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 14);
            this.label11.TabIndex = 57;
            this.label11.Text = "ID Number";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Controls.Add(this.splitContainer1);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 0);
            this.Tp2.Text = "Installment Result";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.splitContainer1.Panel1.Controls.Add(this.TxtDueDt);
            this.splitContainer1.Panel1.Controls.Add(this.label45);
            this.splitContainer1.Panel1.Controls.Add(this.label39);
            this.splitContainer1.Panel1.Controls.Add(this.TxtTotalAmt);
            this.splitContainer1.Panel1.Controls.Add(this.label40);
            this.splitContainer1.Panel1.Controls.Add(this.TxtInterestRateAmt);
            this.splitContainer1.Panel1.Controls.Add(this.label41);
            this.splitContainer1.Panel1.Controls.Add(this.TxtInterestRate);
            this.splitContainer1.Panel1.Controls.Add(this.label42);
            this.splitContainer1.Panel1.Controls.Add(this.label43);
            this.splitContainer1.Panel1.Controls.Add(this.LueMonth);
            this.splitContainer1.Panel1.Controls.Add(this.LueYr);
            this.splitContainer1.Panel1.Controls.Add(this.label44);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Grd1);
            this.splitContainer1.Size = new System.Drawing.Size(766, 0);
            this.splitContainer1.SplitterDistance = 308;
            this.splitContainer1.TabIndex = 0;
            // 
            // TxtDueDt
            // 
            this.TxtDueDt.EnterMoveNextControl = true;
            this.TxtDueDt.Location = new System.Drawing.Point(115, 112);
            this.TxtDueDt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDueDt.Name = "TxtDueDt";
            this.TxtDueDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDueDt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDueDt.Properties.Appearance.Options.UseFont = true;
            this.TxtDueDt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDueDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDueDt.Properties.MaxLength = 12;
            this.TxtDueDt.Size = new System.Drawing.Size(67, 20);
            this.TxtDueDt.TabIndex = 66;
            this.TxtDueDt.Validated += new System.EventHandler(this.TxtDueDt_Validated);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(51, 114);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(59, 14);
            this.label45.TabIndex = 65;
            this.label45.Text = "Due Date";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(183, 10);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 14);
            this.label39.TabIndex = 56;
            this.label39.Text = "%";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalAmt
            // 
            this.TxtTotalAmt.EnterMoveNextControl = true;
            this.TxtTotalAmt.Location = new System.Drawing.Point(115, 49);
            this.TxtTotalAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalAmt.Name = "TxtTotalAmt";
            this.TxtTotalAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalAmt.Properties.MaxLength = 12;
            this.TxtTotalAmt.Properties.ReadOnly = true;
            this.TxtTotalAmt.Size = new System.Drawing.Size(185, 20);
            this.TxtTotalAmt.TabIndex = 60;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(27, 51);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(83, 14);
            this.label40.TabIndex = 59;
            this.label40.Text = "Total Amount";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInterestRateAmt
            // 
            this.TxtInterestRateAmt.EnterMoveNextControl = true;
            this.TxtInterestRateAmt.Location = new System.Drawing.Point(115, 28);
            this.TxtInterestRateAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInterestRateAmt.Name = "TxtInterestRateAmt";
            this.TxtInterestRateAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInterestRateAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterestRateAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInterestRateAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtInterestRateAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInterestRateAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInterestRateAmt.Properties.MaxLength = 12;
            this.TxtInterestRateAmt.Properties.ReadOnly = true;
            this.TxtInterestRateAmt.Size = new System.Drawing.Size(185, 20);
            this.TxtInterestRateAmt.TabIndex = 58;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(44, 29);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(66, 14);
            this.label41.TabIndex = 57;
            this.label41.Text = "IR Amount";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInterestRate
            // 
            this.TxtInterestRate.EnterMoveNextControl = true;
            this.TxtInterestRate.Location = new System.Drawing.Point(115, 7);
            this.TxtInterestRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInterestRate.Name = "TxtInterestRate";
            this.TxtInterestRate.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInterestRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterestRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInterestRate.Properties.Appearance.Options.UseFont = true;
            this.TxtInterestRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInterestRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInterestRate.Properties.MaxLength = 12;
            this.TxtInterestRate.Properties.ReadOnly = true;
            this.TxtInterestRate.Size = new System.Drawing.Size(65, 20);
            this.TxtInterestRate.TabIndex = 55;
            this.TxtInterestRate.Validated += new System.EventHandler(this.TxtInterestRate_Validated);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(5, 8);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(105, 14);
            this.label42.TabIndex = 54;
            this.label42.Text = "Interest Rate (IR)";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Red;
            this.label43.Location = new System.Drawing.Point(37, 72);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(73, 14);
            this.label43.TabIndex = 61;
            this.label43.Text = "Start Month";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueMonth
            // 
            this.LueMonth.EnterMoveNextControl = true;
            this.LueMonth.Location = new System.Drawing.Point(115, 70);
            this.LueMonth.Margin = new System.Windows.Forms.Padding(5);
            this.LueMonth.Name = "LueMonth";
            this.LueMonth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMonth.Properties.Appearance.Options.UseFont = true;
            this.LueMonth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMonth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMonth.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMonth.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMonth.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMonth.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMonth.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMonth.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMonth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMonth.Properties.DropDownRows = 14;
            this.LueMonth.Properties.MaxLength = 30;
            this.LueMonth.Properties.NullText = "[Empty]";
            this.LueMonth.Properties.PopupWidth = 125;
            this.LueMonth.Size = new System.Drawing.Size(125, 20);
            this.LueMonth.TabIndex = 62;
            this.LueMonth.ToolTip = "F4 : Show/hide list";
            this.LueMonth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMonth.EditValueChanged += new System.EventHandler(this.LueMonth_EditValueChanged);
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(115, 91);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 15;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 80;
            this.LueYr.Size = new System.Drawing.Size(67, 20);
            this.LueYr.TabIndex = 64;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueYr.EditValueChanged += new System.EventHandler(this.LueYr_EditValueChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(78, 93);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(32, 14);
            this.label44.TabIndex = 63;
            this.label44.Text = "Year";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(487, 0);
            this.Grd1.TabIndex = 65;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.BtnVoucherDocNo);
            this.panel4.Controls.Add(this.BtnVoucherRequestDocNo);
            this.panel4.Controls.Add(this.BtnRQLPDocNo2);
            this.panel4.Controls.Add(this.TxtVoucherDocNo);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.TxtVoucherRequestDocNo);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.DteEstimatedDt);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.BtnRQLPDocNo);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.TxtStatus);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.TxtRQLPDocNo);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.TxtDocNo);
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.label72);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.DteDocDt);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(805, 218);
            this.panel4.TabIndex = 8;
            // 
            // BtnVoucherDocNo
            // 
            this.BtnVoucherDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucherDocNo.Image")));
            this.BtnVoucherDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucherDocNo.Location = new System.Drawing.Point(331, 150);
            this.BtnVoucherDocNo.Name = "BtnVoucherDocNo";
            this.BtnVoucherDocNo.Size = new System.Drawing.Size(14, 21);
            this.BtnVoucherDocNo.TabIndex = 29;
            this.BtnVoucherDocNo.ToolTip = "Show Voucher";
            this.BtnVoucherDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherDocNo.Click += new System.EventHandler(this.BtnVoucherDocNo_Click);
            // 
            // BtnVoucherRequestDocNo
            // 
            this.BtnVoucherRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherRequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucherRequestDocNo.Image")));
            this.BtnVoucherRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucherRequestDocNo.Location = new System.Drawing.Point(331, 130);
            this.BtnVoucherRequestDocNo.Name = "BtnVoucherRequestDocNo";
            this.BtnVoucherRequestDocNo.Size = new System.Drawing.Size(14, 21);
            this.BtnVoucherRequestDocNo.TabIndex = 26;
            this.BtnVoucherRequestDocNo.ToolTip = "Show Voucher Request";
            this.BtnVoucherRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherRequestDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherRequestDocNo.Click += new System.EventHandler(this.BtnVoucherRequestDocNo_Click);
            // 
            // BtnRQLPDocNo2
            // 
            this.BtnRQLPDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRQLPDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRQLPDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRQLPDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRQLPDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnRQLPDocNo2.Appearance.Options.UseFont = true;
            this.BtnRQLPDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnRQLPDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnRQLPDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRQLPDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRQLPDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnRQLPDocNo2.Image")));
            this.BtnRQLPDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRQLPDocNo2.Location = new System.Drawing.Point(351, 89);
            this.BtnRQLPDocNo2.Name = "BtnRQLPDocNo2";
            this.BtnRQLPDocNo2.Size = new System.Drawing.Size(14, 21);
            this.BtnRQLPDocNo2.TabIndex = 21;
            this.BtnRQLPDocNo2.ToolTip = "Show Request for Loan to Partner";
            this.BtnRQLPDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRQLPDocNo2.ToolTipTitle = "Run System";
            this.BtnRQLPDocNo2.Click += new System.EventHandler(this.BtnRQLPDocNo2_Click);
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(102, 152);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 30;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(226, 20);
            this.TxtVoucherDocNo.TabIndex = 28;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(35, 155);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(62, 14);
            this.label38.TabIndex = 27;
            this.label38.Text = "Voucher#";
            // 
            // TxtVoucherRequestDocNo
            // 
            this.TxtVoucherRequestDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo.Location = new System.Drawing.Point(102, 131);
            this.TxtVoucherRequestDocNo.Name = "TxtVoucherRequestDocNo";
            this.TxtVoucherRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo.Properties.MaxLength = 30;
            this.TxtVoucherRequestDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo.Size = new System.Drawing.Size(226, 20);
            this.TxtVoucherRequestDocNo.TabIndex = 25;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(66, 134);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(31, 14);
            this.label34.TabIndex = 24;
            this.label34.Text = "VR#";
            // 
            // DteEstimatedDt
            // 
            this.DteEstimatedDt.EditValue = null;
            this.DteEstimatedDt.EnterMoveNextControl = true;
            this.DteEstimatedDt.Location = new System.Drawing.Point(102, 110);
            this.DteEstimatedDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEstimatedDt.Name = "DteEstimatedDt";
            this.DteEstimatedDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstimatedDt.Properties.Appearance.Options.UseFont = true;
            this.DteEstimatedDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstimatedDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEstimatedDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEstimatedDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEstimatedDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstimatedDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEstimatedDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstimatedDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEstimatedDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEstimatedDt.Size = new System.Drawing.Size(111, 20);
            this.DteEstimatedDt.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(6, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "Estimated Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnRQLPDocNo
            // 
            this.BtnRQLPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRQLPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRQLPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRQLPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRQLPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnRQLPDocNo.Appearance.Options.UseFont = true;
            this.BtnRQLPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnRQLPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnRQLPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRQLPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRQLPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnRQLPDocNo.Image")));
            this.BtnRQLPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRQLPDocNo.Location = new System.Drawing.Point(331, 89);
            this.BtnRQLPDocNo.Name = "BtnRQLPDocNo";
            this.BtnRQLPDocNo.Size = new System.Drawing.Size(14, 21);
            this.BtnRQLPDocNo.TabIndex = 20;
            this.BtnRQLPDocNo.ToolTip = "Find Request for Loan to Partner";
            this.BtnRQLPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRQLPDocNo.ToolTipTitle = "Run System";
            this.BtnRQLPDocNo.Click += new System.EventHandler(this.BtnRQLPDocNo_Click);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(102, 68);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(106, 20);
            this.TxtStatus.TabIndex = 17;
            // 
            // TxtRQLPDocNo
            // 
            this.TxtRQLPDocNo.EnterMoveNextControl = true;
            this.TxtRQLPDocNo.Location = new System.Drawing.Point(102, 89);
            this.TxtRQLPDocNo.Name = "TxtRQLPDocNo";
            this.TxtRQLPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRQLPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRQLPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRQLPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRQLPDocNo.Properties.MaxLength = 30;
            this.TxtRQLPDocNo.Properties.ReadOnly = true;
            this.TxtRQLPDocNo.Size = new System.Drawing.Size(226, 20);
            this.TxtRQLPDocNo.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(36, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 14);
            this.label1.TabIndex = 18;
            this.label1.Text = "Request#";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(55, 71);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "Status";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label21);
            this.panel7.Controls.Add(this.LueBSCode);
            this.panel7.Controls.Add(this.label20);
            this.panel7.Controls.Add(this.LueBCCode);
            this.panel7.Controls.Add(this.TxtMobile);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.TxtPhone);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.TxtIdentityNo);
            this.panel7.Controls.Add(this.label19);
            this.panel7.Controls.Add(this.DteBirthDt);
            this.panel7.Controls.Add(this.TxtCompanyName);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.MeeCompanyAddress);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.MeeAddress);
            this.panel7.Controls.Add(this.label23);
            this.panel7.Controls.Add(this.TxtPnName);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(400, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(405, 218);
            this.panel7.TabIndex = 32;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(20, 199);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 14);
            this.label21.TabIndex = 51;
            this.label21.Text = "Business Sector";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBSCode
            // 
            this.LueBSCode.EnterMoveNextControl = true;
            this.LueBSCode.Location = new System.Drawing.Point(119, 194);
            this.LueBSCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBSCode.Name = "LueBSCode";
            this.LueBSCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueBSCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueBSCode.Properties.Appearance.Options.UseFont = true;
            this.LueBSCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBSCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBSCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBSCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBSCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBSCode.Properties.DropDownRows = 30;
            this.LueBSCode.Properties.NullText = "[Empty]";
            this.LueBSCode.Properties.PopupWidth = 350;
            this.LueBSCode.Properties.ReadOnly = true;
            this.LueBSCode.Size = new System.Drawing.Size(272, 20);
            this.LueBSCode.TabIndex = 52;
            this.LueBSCode.ToolTip = "F4 : Show/hide list";
            this.LueBSCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(7, 177);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 14);
            this.label20.TabIndex = 49;
            this.label20.Text = "Business Category";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(119, 173);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 350;
            this.LueBCCode.Properties.ReadOnly = true;
            this.LueBCCode.Size = new System.Drawing.Size(272, 20);
            this.LueBCCode.TabIndex = 50;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(119, 152);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 80;
            this.TxtMobile.Properties.ReadOnly = true;
            this.TxtMobile.Size = new System.Drawing.Size(272, 20);
            this.TxtMobile.TabIndex = 48;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(70, 134);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 14);
            this.label7.TabIndex = 45;
            this.label7.Text = "Phone";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(71, 155);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 14);
            this.label10.TabIndex = 47;
            this.label10.Text = "Mobile";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(119, 131);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 80;
            this.TxtPhone.Properties.ReadOnly = true;
            this.TxtPhone.Size = new System.Drawing.Size(272, 20);
            this.TxtPhone.TabIndex = 46;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(15, 112);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 14);
            this.label12.TabIndex = 43;
            this.label12.Text = "Identity Number";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIdentityNo
            // 
            this.TxtIdentityNo.EnterMoveNextControl = true;
            this.TxtIdentityNo.Location = new System.Drawing.Point(119, 110);
            this.TxtIdentityNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdentityNo.Name = "TxtIdentityNo";
            this.TxtIdentityNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIdentityNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdentityNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdentityNo.Properties.Appearance.Options.UseFont = true;
            this.TxtIdentityNo.Properties.MaxLength = 40;
            this.TxtIdentityNo.Properties.ReadOnly = true;
            this.TxtIdentityNo.Size = new System.Drawing.Size(272, 20);
            this.TxtIdentityNo.TabIndex = 44;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(50, 92);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 14);
            this.label19.TabIndex = 41;
            this.label19.Text = "Birth Date";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(119, 89);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.MaxLength = 8;
            this.DteBirthDt.Properties.ReadOnly = true;
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(143, 20);
            this.DteBirthDt.TabIndex = 42;
            // 
            // TxtCompanyName
            // 
            this.TxtCompanyName.EnterMoveNextControl = true;
            this.TxtCompanyName.Location = new System.Drawing.Point(119, 47);
            this.TxtCompanyName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCompanyName.Name = "TxtCompanyName";
            this.TxtCompanyName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCompanyName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCompanyName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCompanyName.Properties.Appearance.Options.UseFont = true;
            this.TxtCompanyName.Properties.MaxLength = 40;
            this.TxtCompanyName.Properties.ReadOnly = true;
            this.TxtCompanyName.Size = new System.Drawing.Size(272, 20);
            this.TxtCompanyName.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(20, 49);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 14);
            this.label2.TabIndex = 37;
            this.label2.Text = "Company Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCompanyAddress
            // 
            this.MeeCompanyAddress.EnterMoveNextControl = true;
            this.MeeCompanyAddress.Location = new System.Drawing.Point(119, 68);
            this.MeeCompanyAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompanyAddress.Name = "MeeCompanyAddress";
            this.MeeCompanyAddress.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeCompanyAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.Appearance.Options.UseBackColor = true;
            this.MeeCompanyAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompanyAddress.Properties.MaxLength = 400;
            this.MeeCompanyAddress.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCompanyAddress.Properties.ReadOnly = true;
            this.MeeCompanyAddress.Properties.ShowIcon = false;
            this.MeeCompanyAddress.Size = new System.Drawing.Size(272, 20);
            this.MeeCompanyAddress.TabIndex = 40;
            this.MeeCompanyAddress.ToolTip = "F4 : Show/hide text";
            this.MeeCompanyAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompanyAddress.ToolTipTitle = "Run System";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(8, 71);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(104, 14);
            this.label26.TabIndex = 39;
            this.label26.Text = "Company Address";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(119, 26);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 400;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeAddress.Properties.ReadOnly = true;
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(272, 20);
            this.MeeAddress.TabIndex = 36;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(62, 28);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 14);
            this.label23.TabIndex = 35;
            this.label23.Text = "Address";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPnName
            // 
            this.TxtPnName.EnterMoveNextControl = true;
            this.TxtPnName.Location = new System.Drawing.Point(119, 5);
            this.TxtPnName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPnName.Name = "TxtPnName";
            this.TxtPnName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPnName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPnName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPnName.Properties.Appearance.Options.UseFont = true;
            this.TxtPnName.Properties.MaxLength = 80;
            this.TxtPnName.Properties.ReadOnly = true;
            this.TxtPnName.Size = new System.Drawing.Size(272, 20);
            this.TxtPnName.TabIndex = 34;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(30, 7);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 14);
            this.label24.TabIndex = 33;
            this.label24.Text = "Partner Name";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(102, 47);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(226, 20);
            this.MeeCancelReason.TabIndex = 14;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 14);
            this.label5.TabIndex = 13;
            this.label5.Text = "Cancel Reason";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 513);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 10;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmSurvey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 535);
            this.Name = "FrmSurvey";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProposal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkExp.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TcSurvey)).EndInit();
            this.TcSurvey.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtManPower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNoOfLoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDevelopment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFiles.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssurance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInstallment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOmzet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBussinessDomicile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLicence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSIUP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueIDNumber.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestRateAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstimatedDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstimatedDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRQLPDocNo.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBSCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompanyAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPnName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label72;
        private DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label61;
        private DevExpress.XtraEditors.LookUpEdit LueProposal;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.TextEdit TxtWorkExp;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraTab.XtraTabControl TcSurvey;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtRQLPDocNo;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnRQLPDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtPnName;
        private System.Windows.Forms.Label label24;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label23;
        protected internal DevExpress.XtraEditors.TextEdit TxtCompanyName;
        private System.Windows.Forms.Label label2;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeCompanyAddress;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.DateEdit DteEstimatedDt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit TxtIdentityNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        protected internal DevExpress.XtraEditors.TextEdit TxtPhone;
        private System.Windows.Forms.Label label21;
        protected internal DevExpress.XtraEditors.LookUpEdit LueBSCode;
        private System.Windows.Forms.Label label20;
        protected internal DevExpress.XtraEditors.LookUpEdit LueBCCode;
        private DevExpress.XtraEditors.LookUpEdit LueLicence;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.LookUpEdit LueSIUP;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.LookUpEdit LueIDNumber;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit LueBussinessDomicile;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.TextEdit TxtAsset;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.TextEdit TxtOmzet;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.TextEdit TxtAssurance;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.TextEdit TxtInstallment;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueFiles;
        private System.Windows.Forms.Label label29;
        private DevExpress.XtraEditors.LookUpEdit LuePosting;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.LookUpEdit LueDevelopment;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label32;
        private DevExpress.XtraEditors.TextEdit TxtNoOfLoan;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label36;
        private DevExpress.XtraEditors.TextEdit TxtManPower;
        private System.Windows.Forms.Label label37;
        private DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo;
        private System.Windows.Forms.Label label34;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherRequestDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnRQLPDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherDocNo;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtTotalAmt;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtInterestRateAmt;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtInterestRate;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private DevExpress.XtraEditors.LookUpEdit LueMonth;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.TextEdit TxtDueDt;
        private System.Windows.Forms.Label label45;
        protected DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
    }
}