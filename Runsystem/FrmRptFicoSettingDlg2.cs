﻿#region Update
/*
    29/04/2020 [WED/KBN] new apps
    19/06/2020 [WED/KBN] bisa pilih entity
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptFicoSettingDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptFicoSetting mFrmParent;
        private int mRow = 0;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptFicoSettingDlg2(FrmRptFicoSetting FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List Of Selected COA";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnRefresh.Visible = BtnChoose.Visible = false;
                if (!mFrmParent.IsInsert) BtnDuplicateEntCode.Enabled = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Account#",
                    "Account Description",
                    "Alias",
                    "Type",
                    "",

                    //6-7
                    "Entity Code",
                    "Entity"
                }, new int[]
                {
                    50,
                    200, 300, 180, 100, 20,
                    100, 200
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, Concat(A.AcNo, ' - ', A.AcDesc) AcDesc, A.Alias, ");
            SQL.AppendLine("Case A.AcType When 'C' Then 'Credit' When 'D' Then 'Debit' End As AcType ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Where Find_In_Set(A.AcNo, @AcNo) ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            ShowDataHdr();
            ShowDataDtl();
        }

        override protected void CloseForm()
        {
            string[] Lists = GetLists();

            mFrmParent.GetCOAEntList(TxtSettingCode.Text, Lists);
            this.Close();
        }

        private void ShowDataHdr()
        {
            TxtSettingCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 0);
            TxtSettingDesc.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 1);
        }

        private void ShowDataDtl()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                int mListIndex = 0;
                for (int t = 0; t < mFrmParent.lCR.Count; ++t)
                {
                    if (mFrmParent.lCR[t].Row == mRow)
                    {
                        mListIndex = t;
                        break;
                    }
                }

                //Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(mFrmParent.Grd1, mRow, 7));
                Sm.CmParam<String>(ref cm, "@AcNo", mFrmParent.lCR[mListIndex].AcNo);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + " Order By A.AcNo; ",
                    new string[] 
                    { 
                        "AcNo", 
                        "AcDesc", "Alias", "AcType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    }, true, false, false, false
                );
                
                ShowCOAEnt(TxtSettingCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void ShowCOAEnt(string SettingCode)
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                foreach (var x in mFrmParent.lD2
                    .Where(w => 
                        w.SettingCode == SettingCode &&
                        w.AcNo == Sm.GetGrdStr(Grd1, i, 1)))
                {
                    Grd1.Cells[i, 6].Value = x.EntCode;
                    foreach (var y in mFrmParent.lEnt.Where(w => w.EntCode == x.EntCode))
                    {
                        Grd1.Cells[i, 7].Value = y.EntName;
                    }
                }
            }
        }

        internal void ShowSelectedEntity(string EntCode, string EntName, int Row)
        {
            if (EntCode.Length > 0)
            {
                Grd1.Cells[Row, 6].Value = EntCode;
                Grd1.Cells[Row, 7].Value = EntName;
            }
        }

        private string[] GetLists()
        {
            string[] arr = { };
            var x = new List<string>();

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        string AcNo = Sm.GetGrdStr(Grd1, i, 1);
                        string EntCode = Sm.GetGrdStr(Grd1, i, 6);

                        x.Add(string.Concat(AcNo, "#", EntCode));                        
                    }
                }
            }

            if (x.Count > 0)
            {
                arr = x.ToArray();
            }

            x.Clear();

            return arr;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                e.Text = "Choose Entity";
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (mFrmParent.IsInsert)
            {
                if (e.ColIndex == 5 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 1, false, "Account# is empty."))
                    Sm.FormShowDialog(new FrmRptFicoSettingDlg3(this, e.RowIndex));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnDuplicateEntCode_Click(object sender, EventArgs e)
        {
            if (mFrmParent.IsInsert)
            {
                string EntCode = Sm.GetGrdStr(Grd1, 0, 6);
                string EntName = Sm.GetGrdStr(Grd1, 0, 7);

                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        Grd1.Cells[i, 6].Value = EntCode;
                        Grd1.Cells[i, 7].Value = EntName;
                    }
                }
            }
        }

        #endregion

        #endregion

    }
}
