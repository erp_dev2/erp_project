﻿#region Update
/*
    30/10/2019 [WED/YK] benerin link nya ke SO Contract Revision
    18/12/2019 [WED/IMS] di group berdasarkan item group, berdasarkan parameter ProjectBudgetControlGroupBy
    08/07/2020 [TKG/IMS] Berdasarkan parameter ProjectBudgetControlGroupBy, ubah judul code dan nama
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectBudgetControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mProjectBudgetControlGroupBy = string.Empty;
        private bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmRptProjectBudgetControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetLueDocNo(ref LueDocNo);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            string Code = string.Empty, Name = string.Empty;

            if (mProjectBudgetControlGroupBy == "1")
            {
                Code = "Item's Code";
                Name = "Item's Name";
            }
            else
            {
                Code = "Item's Group"+Environment.NewLine+"Code";
                Name = "Item's Group"+Environment.NewLine+"Name";
            }

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        Code,
                        Name,
                        "Implementation",
                        "PO",
                        "Received From Vendor",

                        //6
                        "DO"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 250, 150, 150, 150,  
                        
                        //6
                        150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (Sm.IsLueEmpty(LueDocNo, "List of Project")) return;

            Cursor.Current = Cursors.WaitCursor;

            var l1 = new List<Result>();
            var l2 = new List<String>();
            try
            {
                Process1(ref l1);
                if (l1.Count > 0)
                {
                    Process2(ref l2);
                    Process3(ref l1, ref l2);
                    Process4(ref l1);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<Result> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");                
            if (mProjectBudgetControlGroupBy == "2")
                SQL.AppendLine("Tbl2.ItGrpCode ItCode, Tbl3.ItGrpName ItName, Sum(Tbl1.Amt1) Amt1, Sum(Tbl1.Amt2) Amt2, Sum(Tbl1.Amt3) Amt3, Sum(Tbl1.Amt4) Amt4 ");
            else
                SQL.AppendLine("Tbl1.ItCode, Tbl2.ItName, Tbl1.Amt1, Tbl1.Amt2, Tbl1.Amt3, Tbl1.Amt4 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.ItCode,  ");
            SQL.AppendLine("    Sum(T.Amt1) As Amt1, Sum(T.Amt2) As Amt2, Sum(T.Amt3) As Amt3, Sum(T.Amt4) As Amt4 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select E.ResourceItCode As ItCode, E.TotalWithoutTax As Amt1, 0.00 As Amt2, 0.00 As Amt3, 0.00 As Amt4 ");
            SQL.AppendLine("        From TblLOPHdr A ");
            SQL.AppendLine("        Inner Join TblBOQHdr B On A.DocNo=B.LOPDocNo And B.ActInd='Y' And B.Status='A' ");
            SQL.AppendLine("        Inner Join TblSOContractHdr C On B.DocNo=C.BOQDocNo And C.CancelInd='N' And B.Status='A' ");
            SQL.AppendLine("        Inner Join TblSOContractRevisionHdr C1 On C.DocNo = C1.SOCDocNo ");
            SQL.AppendLine("        Inner Join TblProjectImplementationHdr D On C1.DocNo = D.SOContractDocNo And D.CancelInd='N' And D.Status='A' ");
            SQL.AppendLine("        Inner Join TblProjectImplementationDtl2 E On D.DocNo=E.DocNo ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And A.Status='A' ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select D.ItCode, 0.00 As Amt1, B.Qty*E.UPrice As Amt2,  IfNull(F.Qty, 0.00)*E.UPrice As Amt3, 0.00 As Amt4 ");
            SQL.AppendLine("        From TblPOHdr A ");
            SQL.AppendLine("        Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.LOPDocNo Is Not Null And B.LOPDocNo=@DocNo ");
            SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo  ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo  ");
            SQL.AppendLine("        Inner Join TblQtDtl E On C.QtDocNo=E.DocNo And C.QtDNo=D.DNo  ");
            SQL.AppendLine("        Left Join ( ");
	        SQL.AppendLine("            Select T2.DocNo, T2.DNo, Sum(T3.QtyPurchase) As Qty ");
	        SQL.AppendLine("            From TblPOHdr T1 ");
            SQL.AppendLine("            Inner Join TblPODtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.LOPDocNo Is Not Null And T2.LOPDocNo=@DocNo ");
	        SQL.AppendLine("            Inner Join TblRecvVdDtl T3 On T2.DocNo=T3.PODocNo And T2.DNo=T3.DNo And T3.CancelInd='N' And T3.Status='A' ");
	        SQL.AppendLine("            Group By T2.DocNo, T2.DNo  ");
            SQL.AppendLine("        ) F On B.DocNo=F.DocNo And B.DNo=F.DNo ");
            SQL.AppendLine("        Where A.Status='A' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.ItCode, 0.00 As Amt1, 0.00 As Amt2, 0.00 As Amt3, B.Qty*C.UPrice*C.ExcRate As Amt4 ");
            SQL.AppendLine("        From TblDODeptHdr A ");
            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Where A.LOPDocNo Is Not Null And A.LOPDocNo=@DocNo ");
            SQL.AppendLine("    ) T  ");
            SQL.AppendLine("    Group By T.ItCode ");
            SQL.AppendLine(") Tbl1  ");
            SQL.AppendLine("Inner Join TblItem Tbl2 On Tbl1.ItCode=Tbl2.ItCode ");
            if (mProjectBudgetControlGroupBy == "2")
            {
                SQL.AppendLine("Left Join TblItemGroup Tbl3 On Tbl2.ItGrpCode = Tbl3.ItGrpCode ");
                SQL.AppendLine("Group By Tbl2.ItGrpCode, Tbl3.ItGrpName ");
            }
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetLue(LueDocNo));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItName", 
                    "Amt1", 
                    "Amt2",
                    "Amt3", 
                    "Amt4"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            ItName = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            Amt3 = Sm.DrDec(dr, c[4]),
                            Amt4 = Sm.DrDec(dr, c[5]),
                            MiscInd = false,
                            OrderByMiscInd = false
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<String> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            if (mProjectBudgetControlGroupBy == "2")
                SQL.AppendLine("Distinct T2.ItGrpCode ItCode ");
            else
                SQL.AppendLine("T1.ItCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct D.ItCode ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.LOPDocNo Is Not Null And B.LOPDocNo=@DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo  ");
            SQL.AppendLine("    Where A.Status='A' ");
            SQL.AppendLine("    And D.ItCode Not In ( ");
            SQL.AppendLine("        Select E.ResourceItCode ");
            SQL.AppendLine("        From TblLOPHdr A ");
            SQL.AppendLine("        Inner Join TblBOQHdr B On A.DocNo=B.LOPDocNo And B.ActInd='Y' And B.Status='A' ");
            SQL.AppendLine("        Inner Join TblSOContractHdr C On B.DocNo=C.BOQDocNo And C.CancelInd='N' And B.Status='A' ");
            SQL.AppendLine("        Inner Join TblSOContractRevisionHdr C1 On C.DocNo = C1.SOCDocNo ");
            SQL.AppendLine("        Inner Join TblProjectImplementationHdr D On C1.DocNo=D.SOContractDocNo And D.CancelInd='N' And D.Status='A' ");
            SQL.AppendLine("        Inner Join TblProjectImplementationDtl2 E On D.DocNo=E.DocNo ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And A.Status='A' ");
            SQL.AppendLine("        And A.DocNo=@DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") T1 ");
            if (mProjectBudgetControlGroupBy == "2")
            {
                SQL.AppendLine("Inner Join TblItem T2 On T1.ItCode = T2.ItCode ");
            }
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetLue(LueDocNo));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "ItCode" });
                if (dr.HasRows)
                {
                    while (dr.Read()) l.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Result> l1, ref List<string> l2)
        {
            if (l2.Count > 0)
            {
                decimal Amt1Temp = 0m, Amt2Temp = 0m, Amt3Temp = 0m, Amt4Temp = 0m; 
                foreach (var i in l2)
                {
                    foreach (var j in l1.Where(w => Sm.CompareStr(w.ItCode, i)))
                    {
                        j.MiscInd = true;
                        Amt1Temp += j.Amt1;
                        Amt2Temp += j.Amt2;
                        Amt3Temp += j.Amt3;
                        Amt4Temp += j.Amt4;
                    }
                }
                l1.Add(new Result()
                {
                    ItCode = "MISC",
                    ItName = "Miscellaneous Items",
                    Amt1 = Amt1Temp,
                    Amt2 = Amt2Temp,
                    Amt3 = Amt3Temp,
                    Amt4 = Amt4Temp,
                    MiscInd = false,
                    OrderByMiscInd = true
                });
            }
        }

        private void Process4(ref List<Result> l)
        {
            iGRow r;
            int i = 0;
            Grd1.BeginUpdate();

            foreach (var x in l.Where(w => !w.MiscInd).OrderBy(o => o.OrderByMiscInd).ThenBy(t => t.ItName))
            {
                r = Grd1.Rows.Add();
                i++;
                r.Cells[0].Value = i;
                r.Cells[1].Value = x.ItCode;
                r.Cells[2].Value = x.ItName;
                r.Cells[3].Value = x.Amt1;
                r.Cells[4].Value = x.Amt2;
                r.Cells[5].Value = x.Amt3;
                r.Cells[6].Value = x.Amt4;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6 });
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mProjectBudgetControlGroupBy = Sm.GetParameter("ProjectBudgetControlGroupBy");
            if (mProjectBudgetControlGroupBy.Length == 0) mProjectBudgetControlGroupBy = "1";
        }

        private void SetLueDocNo(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As Col1, ");
            SQL.AppendLine("Concat(A.ProjectName, ' (', A.DocNo, ')')  As Col2 ");
            SQL.AppendLine("From TblLOPHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNo = B.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B.DocNo = C.BOQDocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr D On C.DocNo = D.SOCDocNo ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr E On D.DocNo = E.SOContractDocNo ");
            SQL.AppendLine("    And E.Status = 'A' And E.CancelInd = 'N' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Status='A' ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode='" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Order By ProjectName; ");

            Sm.SetLue2(
                ref LueDocNo,
                "Select DocNo As Col1, Concat(ProjectName, ' (', DocNo, ')')  As Col2 From TblLOPHdr Where CancelInd='N' ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDocNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocNo, new Sm.RefreshLue1(SetLueDocNo));
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Amt4 { get; set; }
            public bool MiscInd { get; set; }
            public bool OrderByMiscInd { get; set; }
        }

        #endregion
    }
}
