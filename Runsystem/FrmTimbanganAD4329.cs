﻿#region Update 
/*
    18/11/2022 [VIN/IOK] DataReceivedHandler update krn ST nggak masuk
    
 */
#endregion 

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using FastReport;
using FastReport.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTimbanganAD4329 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mDocNo = string.Empty; //if this application is called from other application;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, WeighingUOM = string.Empty;
        internal FrmTimbanganAD4329Find FrmFind;
        SerialPort serialPort = new SerialPort();
        private string LoadingQueueNormalStatus = string.Empty;
        private string mPrinterName = string.Empty, mCompanyName = string.Empty;
        #endregion

        #region Constructor

        public FrmTimbanganAD4329(string MenuCode)
        {
            try
            {
                InitializeComponent();
                mMenuCode = MenuCode;
                serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Method

        #region Port Method

        public void openSerial()
        {
            try
            {
                if (!serialPort.IsOpen)
                {
                    WeighingUOM = Sm.GetValue("Select ParValue from tblparameter where ParCode='WeighingUOM'");
                    serialPort.PortName = Sm.GetValue("Select ParValue from tblparameter where ParCode='WeighingSerialPort'");
                    serialPort.BaudRate = Convert.ToInt32(Sm.GetValue("Select ParValue from tblparameter where ParCode='WeighingBaudRate'"));
                    serialPort.DataBits = Convert.ToInt16(Sm.GetValue("Select ParValue from tblparameter where ParCode='WeighingDataBits'"));
                    serialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), Sm.GetValue("Select ParValue from tblparameter where ParCode='WeighingStopBits'"));
                    serialPort.Parity = (Parity)Enum.Parse(typeof(Parity), Sm.GetValue("Select ParValue from tblparameter where ParCode='WeighingParity'"));
                    serialPort.Open();
                }
            }
            catch (Exception)
            {
                int ExcNumber = System.Runtime.InteropServices.Marshal.GetExceptionCode();
                if (ExcNumber == -532459699)
                    Sm.StdMsg(mMsgType.Info,
                        "Tidak terhubung dengan alat (Jembatan timbang, dll)." + Environment.NewLine +
                        "Silahkan lanjutkan.");
                //else
                //    Sm.ShowErrorMsg(Exc);
            }
        }

        public void closeSerial()
        {
            try
            {
                if (serialPort.IsOpen) serialPort.Close();
            }
            catch (Exception Exc)
            {
                //int code = System.Runtime.InteropServices.Marshal.GetExceptionCode();
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                SerialPort sp = (SerialPort)sender;
                string indata = sp.ReadExisting();
                //indata = indata.Replace("?", "");
                if (indata.Substring(indata.Length - WeighingUOM.Length, WeighingUOM.Length) == WeighingUOM)
                    if (indata.Substring(0, 6) == "ST,GS,")
                    {
                        if (indata.Length > 8)
                            lblWeight.Text = indata.Substring(6, indata.Length - 8);
                    }
                    else
                        if (indata.Length > 0)
                            lblWeight.Text = indata.Substring(5, indata.Length - 7); 
            //if ((lblWeight.Text != string.Empty) && (lblWeight.Text != "0"))
            //    try
            //    {
            //        decimal decWeight = decimal.Parse(lblWeight.Text);
            //        if (lblStatus.Text == "IN")
            //            txtWeightBefore.Text = Sm.FormatNum(decWeight, 0);
            //        if (lblStatus.Text == "OUT")
            //            txtWeightAfter.Text = Sm.FormatNum(decWeight, 0);
            //    }
            //    catch 
            //    {
            //    }
            }
            catch (Exception)
            {
                //Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetLueTTCode(ref LueTransportType);
                Sl.SetLueLoadingItemCat(ref LueItem);
                Sl.SetLueLoadingArea(ref LueAreaBongkar);
                ClearData();

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }    
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void StartComSerial()
        {
            try
            {
                openSerial();
                serialPort.Write("RW\r\n");
            }
            catch (Exception)
            {
                //int ExcNumber = System.Runtime.InteropServices.Marshal.GetExceptionCode();
                //if (ExcNumber != -532459699)
                //    Sm.ShowErrorMsg(Exc);
                closeSerial();
            }

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, DteDocDtAf, LueTransportType, txtLicenceNo, txtDriverName, LueItem, LueAreaBongkar,
                        txtWeightBefore, txtWeightAfter, txtWeightNetto, MeeRemark
                    }, true);
                    BtnStart.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, DteDocDtAf, txtWeightAfter, txtWeightBefore, txtWeightNetto
                    }, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueTransportType, txtLicenceNo, txtDriverName, LueItem, LueAreaBongkar, MeeRemark 
                        
                    }, false);
                    BtnStart.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, DteDocDtAf, LueTransportType, txtLicenceNo, txtDriverName, LueItem, LueAreaBongkar,
                        txtWeightBefore, txtWeightAfter, txtWeightNetto
                    }, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeRemark
                        
                    }, false);
                    BtnStart.Enabled = true;
                    txtWeightAfter.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, DteDocDtAf, LueTransportType, txtLicenceNo, txtDriverName, LueItem, LueAreaBongkar,
                txtWeightBefore, txtWeightAfter, txtWeightNetto, MeeRemark
            });
            lblWeight.Text = "0";
            TxtDocNo.Text = string.Empty;
            txtWeightAfter.Text = "0";
            txtWeightBefore.Text = "0";
            txtWeightNetto.Text = "0";
            lblStatus.Text = string.Empty;
            lblJenisTimbangan.Text = string.Empty;
            lblTimeAf.Text = string.Empty;
            lblUser.Text = string.Empty;

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTimbanganAD4329Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                lblStatus.Text = "IN";
                StartComSerial();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if ((lblStatus.Text == "Cancelled") || lblStatus.Text == "OUT")
            {
                Sm.StdMsg(mMsgType.Warning, "Unable to Edit Cancelled or Checked Out Record");
                return;
            }
            SetFormControl(mState.Edit);
            DteDocDtAf.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
            lblStatus.Text = "OUT";
            lblJenisTimbangan.Text = "Timbangan ke-2";
            StartComSerial();
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Update TblLoadingQueue Set Status='C' Where DocNo=@DocNo" };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void GetParameter()
        {
            mPrinterName = Sm.GetParameter("WeighingPrinterName");
            mCompanyName = Sm.GetParameter("ReportTitle1");
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {

            if (
                Sm.IsTxtEmpty(TxtDocNo, "Nomor Antrian", false) || 
                Sm.StdMsgYN("Print", "") == DialogResult.No
                ) return;

            //string PrinterName = mPrinterName;
            //string PrinterName = Sm.GetDefaultPrinter();

            var l = new List<LoadingQueue>();
            var ldtl = new List<LoadingQueue>();

            string[] TableName = { "LoadingQueue" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();


            var SQL = new StringBuilder();

            //SQL.AppendLine("Select Distinct A.DocNo, DATE_FORMAT(A.DocDateBf,'%d/%m/%Y') As DocDateBf, ");
            //SQL.AppendLine("Case When A.Status='I' Then 'Timbangan ke-1' When A.Status='O' then 'Timbangan ke-2' When A.Status='C' then 'Cancelled' end As QueueType,  ");
            //SQL.AppendLine("D.TTName, A.LicenceNo, A.DrvName, A.WeightBf, A.WeightAf, A.Netto, A.Status, ");
            //SQL.AppendLine("Concat(Left(A.DocTimeBf, 2), ':', Right(A.DocTimeBf, 2)) As DocTimeBf, ");
            //SQL.AppendLine("Concat(Left(A.DocTimeAf, 2), ':', Right(A.DocTimeAf, 2)) As DocTimeAf, ");
            //SQL.AppendLine("DATE_FORMAT(A.DocDateAf,'%d/%m/%Y') As DocDateAf, E.UserName As CreateBy, ");
            //SQL.AppendLine("C.ItName As ItCat, B.LoadAreaName As LoadArea, A.Remark, @CompanyName As 'CompanyName' ");
            //SQL.AppendLine("From TblLoadingQueue A ");
            //SQL.AppendLine("Left Join (Select OptCode As LoadArea, OptDesc As LoadAreaName from tblOption where OptCat='LoadingArea') B On A.LoadArea=B.LoadArea ");
            //SQL.AppendLine("Left Join (Select OptCode As ItCat, OptDesc As ItName from tblOption where OptCat='LoadingItemCat') C On A.ItCat=C.ItCat ");
            //SQL.AppendLine("Left Join TblTransportType D On A.TTCode=D.TTCode ");
            //SQL.AppendLine("Inner Join TblUser E On A.CreateBy = E.UserCode ");
            //SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Select Distinct A.DocNo, DATE_FORMAT(A.DocDateBf,'%d/%m/%Y') As DocDateBf, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'I' Then 'Timbangan ke-1' ");
            SQL.AppendLine("    When 'O' Then 'Timbangan ke-2' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As QueueType,  ");
            SQL.AppendLine("D.TTName, A.LicenceNo, A.DrvName, A.WeightBf, A.WeightAf, A.Netto, A.Status, ");
            SQL.AppendLine("Concat(Left(A.DocTimeBf, 2), ':', Right(A.DocTimeBf, 2)) As DocTimeBf, ");
            SQL.AppendLine("Concat(Left(A.DocTimeAf, 2), ':', Right(A.DocTimeAf, 2)) As DocTimeAf, ");
            SQL.AppendLine("DATE_FORMAT(A.DocDateAf,'%d/%m/%Y') As DocDateAf, E.UserName As CreateBy, ");
            SQL.AppendLine("C.OptDesc As ItCat, B.OptDesc As LoadArea, A.Remark, @CompanyName As 'CompanyName' ");
            SQL.AppendLine("From TblLoadingQueue A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat='LoadingArea' And A.LoadArea=B.OptCode ");
            SQL.AppendLine("Left Join TblOption C On C.OptCat='LoadingItemCat' And A.ItCat=C.OptCode ");
            SQL.AppendLine("Left Join TblTransportType D On A.TTCode=D.TTCode ");
            SQL.AppendLine("Inner Join TblUser E On A.CreateBy = E.UserCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyName", mCompanyName);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                            //0
                            "DocNo", 
                            //1-5
                            "DocDateBf", 
                            "QueueType", 
                            "TTName",
                            "LicenceNo", 
                            "DrvName", 
                            //6-10
                            "WeightBf", 
                            "WeightAf", 
                            "Netto", 
                            "Status", 
                            "DocTimeBf", 
                            //11-15
                            "DocTimeAf", 
                            "DocDateAf", 
                            "CreateBy",
                            "ItCat", 
                            "LoadArea",
                            "Remark",
                            "CompanyName"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LoadingQueue()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            //1-5
                            DocDateBf = Sm.DrStr(dr, c[1]),
                            QueueType = Sm.DrStr(dr, c[2]),
                            TTName = Sm.DrStr(dr, c[3]),
                            LicenceNo = Sm.DrStr(dr, c[4]),
                            DrvName = Sm.DrStr(dr, c[5]),
                            //6-10
                            WeightBf = Sm.DrDec(dr, c[6]),
                            WeightAf = Sm.DrDec(dr, c[7]),
                            Netto = Sm.DrDec(dr, c[8]),
                            Status = Sm.DrStr(dr, c[9]),
                            DocTimeBf = Sm.DrStr(dr, c[10]),
                            //11-15
                            DocTimeAf = Sm.DrStr(dr, c[11]),
                            DocDateAf = Sm.DrStr(dr, c[12]),
                            CreateBy = Sm.DrStr(dr, c[13]),
                            ItCat = Sm.DrStr(dr, c[14]),
                            LoadArea = Sm.DrStr(dr, c[15]),
                            Remark = Sm.DrStr(dr, c[16]),
                            CompanyName = Sm.DrStr(dr, c[17]),
                            PrintBy = Gv.CurrentUserCode
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            try
            {
                var report = new Report();
                if (lblStatus.Text == "IN")
                {
                    report.Load(Application.StartupPath + @"\\Report\\repTimbang1.frx");
                    report.RegisterData(myLists[0], TableName[0]);
                    report.PrintSettings.Printer = mPrinterName;
                    report.PrintSettings.ShowDialog = false;
                    report.Print();
                    //report.Show();
                    //report.Design();
                    report.Dispose();
                }
                if (lblStatus.Text == "OUT")
                {
                    report.Load(Application.StartupPath + @"\\Report\\repTimbang2.frx");
                    report.RegisterData(myLists[0], TableName[0]);
                    report.PrintSettings.Printer = mPrinterName;
                    report.PrintSettings.ShowDialog = false;
                    report.Print();
                    //report.Design();
                    report.Dispose();
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                TxtDocNo.Focus();
            }

            //string CR = Convert.ToString((char)13);
            //string LF = Convert.ToString((char)10);
            //string ESC = Convert.ToString((char)27);

            //string COMMAND = string.Empty;
            //COMMAND = ESC + "@";
            //COMMAND += "Nota Timbangan" + CR + LF;
            //COMMAND += Gv.CompanyName + CR + LF;
            //COMMAND += CR + LF;
            //COMMAND += "No. ";
            //COMMAND += ESC + "E";
            //COMMAND += TxtDocNo.Text + CR + LF;
            //COMMAND += ESC + "F";
            //COMMAND += CR + LF;
            //COMMAND += "Jenis Antrian : " + LueItem.Text + CR + LF;
            //COMMAND += CR + LF;
            //COMMAND += "Area Bongkar : " + LueAreaBongkar.Text + CR + LF;
            //COMMAND += CR + LF;
            //COMMAND += "Tgl Masuk : " + Sm.FormatDate(DteDocDt.DateTime) + " " + lblTimeBf.Text + CR + LF;
            //if (lblStatus.Text == "OUT")
            //    COMMAND += "Tgl Keluar : " + Sm.FormatDate(DteDocDtAf.DateTime) + " " + lblTimeAf.Text + CR + LF;
            //COMMAND += "No Pol : " + txtLicenceNo.Text + CR + LF;
            //COMMAND += "Sopir : " + txtDriverName.Text + CR + LF;
            //COMMAND += CR + LF;
            //if (lblStatus.Text == "OUT")
            //{
            //    COMMAND += "Berat 1 : " + txtWeightBefore.Text + CR + LF;
            //    COMMAND += "Berat 2 : " + txtWeightAfter.Text + CR + LF;
            //    COMMAND += "------------------------------------" + CR + LF;
            //    COMMAND += "Netto : " + txtWeightNetto.Text + CR + LF;
            //    COMMAND += CR + LF;
            //    COMMAND += CR + LF;
            //    COMMAND += CR + LF;
            //    COMMAND += "(" + lblUser.Text + ")    (" + txtDriverName.Text + ")" + CR + LF;
            //}
            //else
            //{
            //    COMMAND += "Berat 1 : " + txtWeightBefore.Text + CR + LF;
            //    COMMAND += CR + LF;
            //    COMMAND += CR + LF;
            //    COMMAND += CR + LF;
            //    COMMAND += "(" + lblUser.Text + ")" + CR + LF;
            //}
            //COMMAND += CR + LF;
            //COMMAND += CR + LF;
            //COMMAND += CR + LF;
            //COMMAND += CR + LF;
            //COMMAND += CR + LF;
            //COMMAND += CR + LF;

            //RawPrinterHelper.SendStringToPrinter(PrinterName, COMMAND);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
               if (TxtDocNo.Text == string.Empty) 
                    InsertData();
               else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select DocNo, DocDateBf, Case when Status='I' then "+
                        "'Timbangan ke-1' when Status='O' then 'Timbangan ke-2' else '' end As QueueType, TTCode, " +
                        "LicenceNo, DrvName, WeightBf, WeightAf, Netto, Status, "+
                        "Concat(Left(DocTimeBf, 2), ':', Right(DocTimeBf, 2)) As DocTimeBf, "+
                        "Concat(Left(DocTimeAf, 2), ':', Right(DocTimeAf, 2)) As DocTimeAf, " +
                        "DocDateAf, CreateBy, ItCat, LoadArea, Remark " +
                        "From TblLoadingQueue Where DocNo=@DocNo",
                        new string[] 
                        {
                            "DocNo", 
                            "DocDateBf", "QueueType", "TTCode", "LicenceNo", "DrvName", 
                            "WeightBf", "WeightAf", "Netto", "Status", "DocTimeBf", 
                            "DocTimeAf", "DocDateAf", "CreateBy", "ItCat", "LoadArea", 
                            "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            lblJenisTimbangan.Text = Sm.DrStr(dr, c[2]);
                            Sm.SetLue(LueTransportType, Sm.DrStr(dr, c[3]));
                            txtLicenceNo.EditValue = Sm.DrStr(dr, c[4]);
                            txtDriverName.EditValue = Sm.DrStr(dr, c[5]);
                            txtWeightBefore.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                            txtWeightAfter.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                            txtWeightNetto.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                            if (Sm.DrStr(dr, c[9]) == "I")
                            {
                                lblStatus.Text = "IN";
                                lblStatus.ForeColor = Color.SteelBlue;
                            }
                            else
                            {
                                if (Sm.DrStr(dr, c[9]) == "O")
                                {
                                    lblStatus.Text = "OUT";
                                    lblStatus.ForeColor = Color.Black;
                                }
                                else
                                {
                                    if (Sm.DrStr(dr, c[9]) == "C")
                                    {
                                        lblStatus.Text = "Cancelled";
                                        lblStatus.ForeColor = Color.Red;
                                    }
                                }
                            }
                            lblTimeBf.Text = Sm.DrStr(dr, c[10]);
                            lblTimeAf.Text = Sm.DrStr(dr, c[11]);
                            Sm.SetDte(DteDocDtAf, Sm.DrStr(dr, c[12]));
                            lblUser.Text = Sm.DrStr(dr, c[13]);
                            Sm.SetLue(LueItem, Sm.DrStr(dr, c[14]));
                            Sm.SetLue(LueAreaBongkar, Sm.DrStr(dr, c[15]));
                            MeeRemark.Text = Sm.DrStr(dr, c[16]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (IsDataNotValid()) return;
            if (!IsWeightValid()) return;

            closeSerial();
            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();

            Sm.SetDteCurrentDate(DteDocDt);

            TxtDocNo.Text = GenerateDocNo(Sm.GetLue(LueItem));
            string LegalDocVerifyDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "LegalDocVerify", "TblLegalDocVerifyHdr");

            SQL.AppendLine("Insert Into TblLoadingQueue(DocNo, Status, DocDateBf, DocTimeBf, ");
            SQL.AppendLine("TTCode, ItCat, LoadArea, LicenceNo, DrvName, ");
            SQL.AppendLine(" WeightBf, WeightAf, Netto, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, 'I', @DocDateBf, @DocTimeBf, ");
            SQL.AppendLine("@TTCode, @ItCat, @LoadArea, @LicenceNo, @DrvName, ");
            SQL.AppendLine("@WeightBf, @WeightAf, @Netto, @Remark, @UserCode, CurrentDateTime());");

            SQL.AppendLine("Insert Into TblLegalDocVerifyHdr(DocNo, DocDt, CancelInd, ProcessInd1, ProcessInd2, QueueNo, VehicleRegNo, VdCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@LegalDocVerifyDocNo, @DocDt, 'N', 'O', 'O', @DocNo, @LicenceNo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='GlobalVendorRMP'), ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocDateBf", Sm.ServerCurrentDateTime().Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@DocTimeBf", Sm.ServerCurrentDateTime().Substring(8, 4));
            Sm.CmParam<String>(ref cm, "@TTCode", Sm.GetLue(LueTransportType));
            Sm.CmParam<String>(ref cm, "@ItCat", Sm.GetLue(LueItem));
            Sm.CmParam<String>(ref cm, "@LoadArea", Sm.GetLue(LueAreaBongkar));
            Sm.CmParam<String>(ref cm, "@LicenceNo", txtLicenceNo.Text);
            Sm.CmParam<String>(ref cm, "@DrvName", txtDriverName.Text);
            Sm.CmParam<Decimal>(ref cm, "@WeightBf", Decimal.Parse(txtWeightBefore.Text));
            Sm.CmParam<Decimal>(ref cm, "@WeightAf", Decimal.Parse(txtWeightAfter.Text));
            Sm.CmParam<Decimal>(ref cm, "@Netto", Decimal.Parse(txtWeightNetto.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", LegalDocVerifyDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

            Sm.ExecCommand(cm);

            ShowData(TxtDocNo.Text);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(LueTransportType, "Jenis Kendaraan", false) ||
                Sm.IsTxtEmpty(txtLicenceNo, "No Polisi", false) ||
                Sm.IsTxtEmpty(txtDriverName, "Nama Sopir", false) ||
                Sm.IsTxtEmpty(LueItem, "Jenis Antrian", false) ||
                Sm.IsTxtEmpty(LueAreaBongkar, "Area Bongkar/Muat", false);
        }

        private void SetLoadingQueueNormalStatus()
        {
            try
            {
                LoadingQueueNormalStatus = Sm.GetParameter("LoadingQueueNormalStatus");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private bool IsWeightValid()
        {
            bool IsValid = true;
            decimal decWeight = 0;
            bool IsNormal = true;

            SetLoadingQueueNormalStatus();

            IsNormal = LoadingQueueNormalStatus == "Y";

            if (lblStatus.Text == "IN")
            {
                try
                {
                    decWeight = decimal.Parse(txtWeightBefore.Text);
                }
                catch
                {
                }
                if (IsNormal && decWeight == 0) IsValid = false;
            }
            if (lblStatus.Text == "OUT")
            {
                try
                {
                    decWeight = decimal.Parse(txtWeightAfter.Text);
                }
                catch
                {
                }
                if (IsNormal && decWeight == 0) IsValid = false;
            }
            if (!IsValid)
            {
                if (Sm.StdMsgYN("Question", 
                    "Angka di timbangan masih 0." + Environment.NewLine + 
                    "Apakah anda mau melanjutkan untuk menyimpan data ini ?"
                    ) == DialogResult.Yes) 
                    IsValid = true;
            }
            return IsValid;
        }

        #endregion

        #region Additional Method

        public static void SetLueTTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TTCode As Col1, TTName As Col2 From TblTransportType Order By CreateDt",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string GenerateDocNo(string ItCat)
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                Dt = Sm.GetDte(DteDocDt).Substring(6, 2);

            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("'" + ItCat + "',");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 3) From ( ");
            SQL.Append("       Select Convert(Substring(DocNo, 2, 3), Decimal) As DocNo From TblLoadingQueue ");
            SQL.Append("       Where Right(DocNo, 6)=Concat('" + Dt + "','" + Mth + "', '" + Yr + "') ");
            SQL.Append("       And Left(DocNo, 1)='" + ItCat + "' ");
            SQL.Append("       Order By Substring(DocNo, 2, 3) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '001'), ");
            SQL.Append("'" + Dt + "', '" + Mth + "', '" + Yr + "'");
            SQL.Append(") As DocNo");
            
            return Sm.GetValue(SQL.ToString());
        }

        private bool IsEditedDataNotValid()
        {
            if (
                !IsWeightValid() ||
                (IsLoadingQueueNeedVerify() && IsLoadingQueueVerificationNotValid()) 
                )
                return true;

            return false;
        }

        private bool IsLoadingQueueNeedVerify()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblLoadingQueue ");
            SQL.AppendLine("Where Locate( ");
            SQL.AppendLine("    Concat('##', ItCat, '##'), ");
            SQL.AppendLine("    (Select ParValue From TblParameter Where ParCode='LoadingQueueNeedVerifyItCat') ");
            SQL.AppendLine("    )>0 ");
            SQL.AppendLine("And Status='I' ");
            SQL.AppendLine("And DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
                return true;
            else
                return false;
        }

        private bool IsLoadingQueueVerificationNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblLegalDocVerifyHdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And QueueNo=@QueueNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@QueueNo", TxtDocNo.Text);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Nomor antrian ini masih belum diverifikasi.");
                return true;
            }
            return false;
        }

        private void EditData()
        {
            if (IsEditedDataNotValid()) return;
            closeSerial();

            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();

            SQL.AppendLine(" Update TblLoadingQueue ");
            SQL.AppendLine(" Set Status='O', DocDateAf=@DocDateAf, DocTimeAf=@DocTimeAf, ");
            SQL.AppendLine(" WeightAf=@WeightAf, Netto=@Netto, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine(" Where DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocDateAf", Sm.ServerCurrentDateTime().Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@DocTimeAf", Sm.ServerCurrentDateTime().Substring(8, 4));
            Sm.CmParam<Decimal>(ref cm, "@WeightAf", Decimal.Parse(txtWeightAfter.Text));
            Sm.CmParam<Decimal>(ref cm, "@Netto", Decimal.Parse(txtWeightNetto.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);

            ShowData(TxtDocNo.Text);
        }

        #endregion

        #endregion

        #region Event

        private void LueAreaBongkar_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAreaBongkar, new Sm.RefreshLue1(Sl.SetLueLoadingArea));
        }

        private void txtWeightBefore_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtWeightNetto.Text =
                    Sm.FormatNum(Decimal.Parse(txtWeightBefore.Text) - Decimal.Parse(txtWeightAfter.Text), 0);
            }
            catch
            {
            }
        }

        private void txtWeightAfter_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtWeightNetto.Text =
                    Sm.FormatNum(Decimal.Parse(txtWeightBefore.Text) - Decimal.Parse(txtWeightAfter.Text), 0);
            }
            catch
            {
            }
        }

        private void LueTransportType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTransportType, new Sm.RefreshLue1(Sl.SetLueTTCode));
        }

        private void LueItem_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItem, new Sm.RefreshLue1(Sl.SetLueLoadingItemCat));
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            if ((lblWeight.Text != string.Empty) && (lblWeight.Text != "0"))
            {
                try
                {
                    decimal decWeight = decimal.Parse(lblWeight.Text);
                    if (lblStatus.Text == "IN")
                        txtWeightBefore.Text = Sm.FormatNum(decWeight, 0);
                    if (lblStatus.Text == "OUT")
                        txtWeightAfter.Text = Sm.FormatNum(decWeight, 0);
                }
                catch
                {
                }
            }
        }

        #endregion

    }

    public class RawPrinterHelper
    {
        // Structure and API declarions:
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }

        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        // SendBytesToPrinter()
        // When the function is given a printer name and an unmanaged array
        // of bytes, the function sends those bytes to the print queue.
        // Returns true on success, false on failure.
        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
        {
            Int32 dwError = 0, dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFOA di = new DOCINFOA();
            bool bSuccess = false; // Assume failure unless you specifically succeed.

            di.pDocName = "My C#.NET RAW Document";
            di.pDataType = "RAW";

            // Open the printer.
            if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                // Start a document.
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    // Start a page.
                    if (StartPagePrinter(hPrinter))
                    {
                        // Write your bytes.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }
            // If you did not succeed, GetLastError may give more information
            // about why not.
            if (bSuccess == false)
            {
                dwError = Marshal.GetLastWin32Error();
            }
            return bSuccess;
        }

        public static bool SendFileToPrinter(string szPrinterName, string szFileName)
        {
            // Open the file.
            FileStream fs = new FileStream(szFileName, FileMode.Open);
            // Create a BinaryReader on the file.
            BinaryReader br = new BinaryReader(fs);
            // Dim an array of bytes big enough to hold the file's contents.
            Byte[] bytes = new Byte[fs.Length];
            bool bSuccess = false;
            // Your unmanaged pointer.
            IntPtr pUnmanagedBytes = new IntPtr(0);
            int nLength;

            nLength = Convert.ToInt32(fs.Length);
            // Read the contents of the file into the array.
            bytes = br.ReadBytes(nLength);
            // Allocate some unmanaged memory for those bytes.
            pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
            // Copy the managed byte array into the unmanaged array.
            Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
            // Send the unmanaged bytes to the printer.
            bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, nLength);
            // Free the unmanaged memory that you allocated earlier.
            Marshal.FreeCoTaskMem(pUnmanagedBytes);
            return bSuccess;
        }
        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
            IntPtr pBytes;
            Int32 dwCount;
            // How many characters are in the string?
            dwCount = szString.Length;
            // Assume that the printer is expecting ANSI text, and then convert
            // the string to ANSI text.
            pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            // Send the converted ANSI string to the printer.
            SendBytesToPrinter(szPrinterName, pBytes, dwCount);
            Marshal.FreeCoTaskMem(pBytes);
            return true;
        }
    }

    #region Report Class

    class LoadingQueue
    {
        public string DocNo { get; set; }
        public string DocDateBf { get; set; }
        public string QueueType { get; set; }
        public string TTName { get; set; }
        public string LicenceNo { get; set; }
        public string DrvName { get; set; }
        public decimal WeightBf { get; set; }
        public decimal WeightAf { get; set; }
        public decimal Netto { get; set; }
        public string Status { get; set; }
        public string DocTimeBf { get; set; }
        public string DocTimeAf { get; set; }
        public string DocDateAf { get; set; }
        public string CreateBy { get; set; }
        public string ItCat { get; set; }
        public string LoadArea { get; set; }
        public string Remark { get; set; }
        public string CompanyName { get; set; }
        public string PrintBy { get; set; }

    }

    #endregion
}
