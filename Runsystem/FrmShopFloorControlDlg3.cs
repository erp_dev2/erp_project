﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControlDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmShopFloorControl mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmShopFloorControlDlg3(FrmShopFloorControl FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sl.SetLueBomDocType(ref LueBomType);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.Code, T1.Name, T1.TypeCode, T2.OptDesc As TypeName  ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select ItCode As Code, ItName As Name, '1' As TypeCode ");
            SQL.AppendLine("    From TblItem Where PlanningItemInd='Y' And ActInd='Y' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo As Code, DocName As Name, '2' As TypeCode ");
            SQL.AppendLine("    From TblFormulaHdr Where ActiveInd='Y' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select EmpCode As Code, EmpName As Name, '3' As TypeCode ");
            SQL.AppendLine("    From TblEmployee ");
            SQL.AppendLine("    Where EmpCode In (Select Distinct EmpCode From TblWorkCenterDtlEmp) And ResignDt Is Null ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join TblOption T2 On T1.TypeCode=T2.OptCode And T2.OptCat='BomDocType' ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Code", 
                        "", 
                        "Name", 
                        "Type",

                        //6
                        "Type"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where Position(Concat('##', T1.Code, T1.TypeCode, '##') In @SelectedBom)<1 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedBom", mFrmParent.GetSelectedBom());

                Sm.FilterStr(ref Filter, ref cm, TxtBomCode.Text, new string[] { "T1.Code", "T1.Name" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBomType), "T1.TypeCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T1.TypeCode, T1.Name",
                        new string[] 
                        { 
                            //0
                            "Code", 

                            //1-3
                            "Name", "TypeCode", "TypeName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 6);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, Row1, new int[] { 7, 8 });

                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 7, 8 });
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        private bool IsCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 2), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 5), Sm.GetGrdStr(Grd1, Row, 5))
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBomCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBomCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Name");
        }

        private void LueBomType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBomType, new Sm.RefreshLue1(Sl.SetLueBomDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBomType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        #endregion

        #endregion

    }
}
