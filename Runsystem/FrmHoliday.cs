﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmHoliday : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmHolidayFind FrmFind;

        #endregion

        #region Constructor

        public FrmHoliday(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
          if (FrmFind != null) FrmFind.Close();
          
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtHolCode, TxtHolName, DteHolDt, ChkRoutineInd
                    }, true);
                    TxtHolCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtHolCode, TxtHolName, DteHolDt, ChkRoutineInd
                    }, false);
                    TxtHolCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtHolName, DteHolDt, ChkRoutineInd
                    }, false);
                    TxtHolName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtHolCode, TxtHolName, DteHolDt
            });
            ChkRoutineInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmHolidayFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtHolCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtHolCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblHoliday Where HolCode=@HolCode" };
                Sm.CmParam<String>(ref cm, "@HolCode", TxtHolCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblHoliday(HolCode, HolName, HolDt, RoutineInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@HolCode, @HolName, @HolDt, @RoutineInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update HolName=@HolName, HolDt=@HolDt, RoutineInd=@RoutineInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@HolCode", TxtHolCode.Text);
                Sm.CmParam<String>(ref cm, "@HolName", TxtHolName.Text);
                Sm.CmParamDt(ref cm, "@HolDt", Sm.GetDte(DteHolDt));
                Sm.CmParam<String>(ref cm, "@RoutineInd", ChkRoutineInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtHolCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string HolCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@HolCode", HolCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblHoliday Where HolCode = @HolCode",
                        new string[] 
                        {
                            "HolCode", "HolName", "HolDt", "RoutineInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtHolCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtHolName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetDte(DteHolDt, Sm.DrStr(dr, c[2]));
                            ChkRoutineInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]),"Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtHolCode, "Holiday code", false) ||
                Sm.IsTxtEmpty(TxtHolName, "Holiday name", false) ||
                Sm.IsDteEmpty(DteHolDt, "Holiday date") ||
                IsHolCodeExisted() ||
                IsHolDtExisted(); 
        }

        private bool IsHolCodeExisted()
        {
            if (!TxtHolCode.Properties.ReadOnly && Sm.IsDataExist("Select HolCode From TblHoliday Where HolCode ='" + TxtHolCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Holiday code ( " + TxtHolCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsHolDtExisted()
        {
            if (!TxtHolCode.Properties.ReadOnly && Sm.IsDataExist("Select HolDt From TblHoliday Where HolDt ='" + Sm.GetDte(DteHolDt).Substring(0, 8) + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Holiday date already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtHolCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtHolCode);
        }
        private void TxtHolName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtHolName);
        }

        #endregion

        #endregion
    }
}
