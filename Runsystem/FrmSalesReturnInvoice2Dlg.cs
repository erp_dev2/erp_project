﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesReturnInvoice2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesReturnInvoice2 mFrmParent;
        private string mCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesReturnInvoice2Dlg(FrmSalesReturnInvoice2 FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                //Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DNo, DocDt, ItCode, ItName, ");
            SQL.AppendLine("Qty, InventoryUomCode, ");
            SQL.AppendLine("CurCode, UPrice, PriceUomCode, ");
            SQL.AppendLine("TaxInvDocument, TaxInvoiceDt, TaxRate ");
            SQL.AppendLine("From ( ");

            //DR
            SQL.AppendLine("    Select Distinct A.DocNo, B.DNo, A.DocDt, E.ItCode, F.ItName, ");
            SQL.AppendLine("    Case E.PriceUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("        When F.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("        When F.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("        Else 0.00 End As Qty, ");
            SQL.AppendLine("    Case E.PriceUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode Then F.InventoryUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode2 Then F.InventoryUomCode2 ");
            SQL.AppendLine("        When F.InventoryUomCode3 Then F.InventoryUomCode3 ");
            SQL.AppendLine("        Else Null End As InventoryUomCode, ");
            SQL.AppendLine("    E.CurCode, E.UPrice, E.PriceUomCode, ");
            SQL.AppendLine("    I.TaxInvDocument, I.DocDt As TaxInvoiceDt, H.TaxRate ");
            SQL.AppendLine("    From TblRecvCtHdr A ");
            SQL.AppendLine("    Inner Join TblRecvCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DRDocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.DNo, T7.ItCode, T6.CurCode, T6.PriceUomCode, ");
            SQL.AppendLine("        (T5.UPrice-(T5.UPrice*0.01*IfNull(T8.DiscRate, 0.00))) As UPrice ");
            SQL.AppendLine("        From TblDRHdr T1 ");
            SQL.AppendLine("        Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo  ");
            SQL.AppendLine("        Inner Join TblSODtl T4 On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl T5 On T3.CtQTDocno=T5.DocNo And T4.CtQTDno=T5.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr T6 On T5.ItemPriceDocNo=T6.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl T7 On T5.ItemPriceDocNo=T7.DocNo And T5.ItemPriceDNo=T7.DNo ");
            SQL.AppendLine("        Left Join TblSOQuotPromoItem T8 On T3.SOQuotPromoDocNo=T8.DocNo And T7.ItCode=T8.ItCode  ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.CBDInd='N' ");
            SQL.AppendLine("        And Exists( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblRecvCtDtl X1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr X2 On X1.DOCtDocNo=X2.DocNo ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl X3 On X1.DOCtDocNo=X3.DocNo And X1.DOCtDNo=X3.DNo ");
            SQL.AppendLine("            Where X2.DRDocNo=T1.DocNo ");
            SQL.AppendLine("            And X3.ItCode=T7.ItCode ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) E On C.DrDocno=E.DocNo And D.ItCode=E.ItCode ");
            SQL.AppendLine("    Inner Join TblItem F On E.ItCode=F.ItCode ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 G On C.DocNo=G.DocNo And E.DNo=G.DRDNo And G.Qty>0.00 ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceDtl H On D.DocNo=H.DOCtDocNo And G.DNo=H.DOCtDNo ");
            SQL.AppendLine("    Inner Join TblSalesInvoiceHdr I On H.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CtCode=@CtCode ");
            SQL.AppendLine("    And A.SalesReturnInvoiceInd='O' ");

            //untuk PL
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct A.DocNo, B.DNo, A.DocDt, E.ItCode, F.ItName, ");
            SQL.AppendLine("    Case E.PriceUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("        When F.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("        When F.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("        Else 0.00 End As Qty, ");
            SQL.AppendLine("    Case E.PriceUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode Then F.InventoryUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode2 Then F.InventoryUomCode2 ");
            SQL.AppendLine("        When F.InventoryUomCode3 Then F.InventoryUomCode3 ");
            SQL.AppendLine("        Else Null End As InventoryUomCode, ");
            SQL.AppendLine("    E.CurCode, E.UPrice, E.PriceUomCode, ");
            SQL.AppendLine("    I.TaxInvDocument, I.DocDt As TaxInvoiceDt, H.TaxRate ");
            SQL.AppendLine("    From TblRecvCtHdr A ");
            SQL.AppendLine("    Inner Join TblRecvCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.PLDocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.DNo, T7.ItCode, T6.CurCode, T6.PriceUomCode, ");
            SQL.AppendLine("        (T5.UPrice-(T5.UPrice*0.01*IfNull(T8.DiscRate, 0))) As UPrice ");
            SQL.AppendLine("        From TblPLHdr T1 ");
            SQL.AppendLine("        Inner Join TblPLDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo  ");
            SQL.AppendLine("        Inner Join TblSODtl T4 On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl T5 On T3.CtQTDocno=T5.DocNo And T4.CtQTDno=T5.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr T6 On T5.ItemPriceDocNo=T6.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl T7 On T5.ItemPriceDocNo=T7.DocNo And T5.ItemPriceDNo=T7.DNo ");
            SQL.AppendLine("        Left Join TblSOQuotPromoItem T8 On T3.SOQuotPromoDocNo=T8.DocNo And T7.ItCode=T8.ItCode  ");
            SQL.AppendLine("        Where Exists( ");
            SQL.AppendLine("            Select X1.DocNo ");
            SQL.AppendLine("            From TblRecvCtDtl X1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr X2 On X1.DOCtDocNo=X2.DocNo ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl X3 On X1.DOCtDocNo=X3.DocNo And X1.DOCtDNo=X3.DNo ");
            SQL.AppendLine("            Where X2.PLDocNo=T1.DocNo ");
            SQL.AppendLine("            And X3.ItCode=T7.ItCode ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) E On C.PLDocno=E.DocNo And D.ItCode=E.ItCode ");
            SQL.AppendLine("    Inner Join TblItem F On E.ItCode=F.ItCode ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl3 G On C.DocNo=G.DocNo And E.DNo=G.PLDNo And G.Qty>0 ");
            SQL.AppendLine("    Left Join TblSalesInvoiceDtl H On D.DocNo=H.DOCtDocNo And G.DNo=H.DOCtDNo ");
            SQL.AppendLine("    Left Join TblSalesInvoiceHdr I On H.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CtCode=@CtCode ");
            SQL.AppendLine("    And A.SalesReturnInvoiceInd='O' ");

            //untuk DR CBD
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct  ");
            SQL.AppendLine("    A.DocNo, B.DNo, A.DocDt, E.ItCode, F.ItName, ");
            SQL.AppendLine("    Case E.PriceUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("        When F.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("        When F.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("        Else 0.00 End As Qty, ");
            SQL.AppendLine("    Case E.PriceUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode Then F.InventoryUomCode ");
            SQL.AppendLine("        When F.InventoryUomCode2 Then F.InventoryUomCode2 ");
            SQL.AppendLine("        When F.InventoryUomCode3 Then F.InventoryUomCode3 ");
            SQL.AppendLine("        Else Null End As InventoryUomCode, ");
            SQL.AppendLine("    E.CurCode, E.UPrice, E.PriceUomCode, ");
            SQL.AppendLine("    K.TaxInvDocument, K.DocDt As TaxInvoiceDt, J.TaxRate ");
            SQL.AppendLine("    From TblRecvCtHdr A ");
            SQL.AppendLine("    Inner Join TblRecvCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo And C.DRDocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDNo=D.DNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.DNo, T7.ItCode, T6.CurCode, T6.PriceUomCode, ");
            SQL.AppendLine("        (T5.UPrice-(T5.UPrice*0.01*IfNull(T8.DiscRate, 0))) As UPrice ");
            SQL.AppendLine("        From TblDRHdr T1 ");
            SQL.AppendLine("        Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo ");
            SQL.AppendLine("        Inner Join TblSODtl T4 On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl T5 On T3.CtQTDocno=T5.DocNo And T4.CtQTDno=T5.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr T6 On T5.ItemPriceDocNo=T6.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl T7 On T5.ItemPriceDocNo=T7.DocNo And T5.ItemPriceDNo=T7.DNo ");
            SQL.AppendLine("        Left Join TblSOQuotPromoItem T8 On T3.SOQuotPromoDocNo=T8.DocNo And T7.ItCode=T8.ItCode ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.CBDInd='Y' ");
            SQL.AppendLine("        And Exists( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblRecvCtDtl X1 ");
            SQL.AppendLine("            Inner Join TblDOCt2Hdr X2 On X1.DOCtDocNo=X2.DocNo ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl X3 On X1.DOCtDocNo=X3.DocNo And X1.DOCtDNo=X3.DNo ");
            SQL.AppendLine("            Where X2.DRDocNo=T1.DocNo ");
            SQL.AppendLine("            And X3.ItCode=T7.ItCode ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) E On C.DrDocno=E.DocNo And D.ItCode=E.ItCode ");
            SQL.AppendLine("    Inner Join TblItem F On E.ItCode=F.ItCode ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 G On C.DocNo=G.DocNo And E.DNo=G.DRDNo And G.Qty>0 ");
            SQL.AppendLine("    Inner Join TblDRDtl H On C.DRDocNo = H.DocNo ");
            SQL.AppendLine("    Inner JOin TBlSODtl I On H.SODocNo=I.DocNo And H.SODNo=I.DNo  ");
            SQL.AppendLine("    Left Join TblSalesInvoiceDtl J On I.DocNo=J.DOCtDocNo And I.DNo=J.DOCtDNo ");
            SQL.AppendLine("    Left Join TblSalesInvoiceHdr K On J.DocNo=K.DocNo And K.CancelInd='N' ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CtCode=@CtCode ");
            SQL.AppendLine("    And A.SalesReturnInvoiceInd='O' ");

            SQL.AppendLine(") Tbl ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "",
                        "Received#",
                        "DNo",
                        "Date",
                        "Item's"+Environment.NewLine+"Code",
                        
                        //6-10
                        "Item's Name",
                        "Quantity",
                        "UoM",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                        
                        //11-15
                        "UoM",
                        "Amount Before Tax",
                        "Tax Invoice#",
                        "Tax Invoice"+Environment.NewLine+"Date",
                        "Tax Rate",
                        
                        //17
                        "Tax",
                        "Amount After Tax"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 130, 0, 80, 100, 

                        //6-10
                        200, 80, 60, 60, 120, 
                        
                        //11-15
                        60, 130, 150, 100, 100, 
                        
                        //16-17
                        130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 10, 12, 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                string DocNo = string.Empty, DNo = string.Empty;

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        DNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 2);

                        if (DocNo.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (DocNo=@DocNo0" + r.ToString() + " And DNo=@DNo0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            Sm.CmParam<String>(ref cm, "@DNo0" + r.ToString(), DNo);
                        }
                    }
                }
                if (Filter.Length>0) Filter = " Where Not(" + Filter + ") ";


                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL() + Filter + " Order By DocNo, DNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DNo", "DocDt", "ItCode", "ItName", "Qty", 
                            
                            //6-10
                            "InventoryUomCode", "CurCode", "UPrice", "PriceUomCode", "TaxInvDocument", 
                            
                            //11-12
                            "TaxInvoiceDt", "TaxRate"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Grd.Cells[Row, 12].Value = Sm.GetGrdDec(Grd, Row, 7)*Sm.GetGrdDec(Grd, Row, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Grd.Cells[Row, 16].Value = Sm.GetGrdDec(Grd, Row, 12)*Sm.GetGrdDec(Grd, Row, 15)*0.01m;
                            Grd.Cells[Row, 17].Value = Sm.GetGrdDec(Grd, Row, 12)+Sm.GetGrdDec(Grd, Row, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 17);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 5, 8, 10, 13, 14, 15 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var key = string.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3));
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(key, 
                    string.Concat(Sm.GetGrdStr(mFrmParent.Grd1, r, 1), Sm.GetGrdStr(mFrmParent.Grd1, r, 2))
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion      

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion
    }
}
