﻿#region Update
/*
    22/01/2018 [TKG] tambah filter+info entity
    12/04/2021 [TKG/PHT] tambah validasi filter multi profit center, filter cost center (text box).
    02/07/2021 [HAR/IMS] BUG nilai bulanan muncul double karena penghubung terhadap amount di journal
                    hanya COA yg mana coa tersebut kepakai di banyak costcenter. tambh CCCode wktu select dan group by.  
    02/06/2022 [TKG/GSS] berdasarkan parameter IsRptMonthlyCostInclPayrollBrutto, ditambah data brutto dari payroll
    16/06/2022 [TKG/GSS] menambah info dari thr dan bonus 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyCost : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<String> mlProfitCenter = null;
        private bool 
            mIsRptMonthlyCostUseProfitCenter = false,
            mIsRptMonthlyCostFilterByCostCenter = false, 
            mIsAllProfitCenterSelected = false;
        internal bool mIsRptMonthlyCostInclPayrollBrutto = false;

        #endregion

        #region Constructor

        public FrmRptMonthlyCost(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueEntCode(ref LueEntCode);
                if (mIsRptMonthlyCostUseProfitCenter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                else
                    SetLueCCCode(ref LueCCCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptMonthlyCostUseProfitCenter', 'IsRptMonthlyCostFilterByCostCenter', 'IsRptMonthlyCostInclPayrollBrutto' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptMonthlyCostUseProfitCenter": mIsRptMonthlyCostUseProfitCenter = ParValue == "Y"; break;
                            case "IsRptMonthlyCostFilterByCostCenter": mIsRptMonthlyCostFilterByCostCenter = ParValue == "Y"; break;
                            case "IsRptMonthlyCostInclPayrollBrutto": mIsRptMonthlyCostInclPayrollBrutto = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.CodeParent, Tbl1.CodeChild, Tbl1.CCtCode, Tbl1.AcNo, ");
            SQL.AppendLine("Tbl1.Total, Tbl1.Mth01, Tbl1.Mth02, Tbl1.Mth03, Tbl1.Mth04, Tbl1.Mth05, Tbl1.Mth06, ");
            SQL.AppendLine("Tbl1.Mth07, Tbl1.Mth08, Tbl1.Mth09, Tbl1.Mth10, Tbl1.Mth11, Tbl1.Mth12, ");
            SQL.AppendLine("Tbl4.CCName As Parent, Tbl3.CCName As Child, Tbl8.AcDesc, Tbl2.CCtName, Tbl5.OptDesc As CCGrpName, Tbl7.EntName ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select CodeParent, CodeChild, CCtCode, AcNo, ");
            SQL.AppendLine("    (Sum(Mth01)+Sum(Mth02)+Sum(Mth03)+Sum(Mth04)+Sum(Mth05)+Sum(Mth06)+ ");
            SQL.AppendLine("    Sum(Mth07)+Sum(Mth08)+Sum(Mth09)+Sum(Mth10)+Sum(Mth11)+Sum(Mth12)) As Total,");
            SQL.AppendLine("    Sum(Mth01) As Mth01, Sum(Mth02) As Mth02, Sum(Mth03) As Mth03, Sum(Mth04) As Mth04, ");
            SQL.AppendLine("    Sum(Mth05) As Mth05, Sum(Mth06) As Mth06, Sum(Mth07) As Mth07, Sum(Mth08) As Mth08, ");
            SQL.AppendLine("    Sum(Mth09) As Mth09, Sum(Mth10) As Mth10, Sum(Mth11) As Mth11, Sum(Mth12) As Mth12 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select CodeParent, CodeChild, CCtCode, AcNo, ");
            SQL.AppendLine("            Case Mth When '01' Then Amt Else 0.0000 End As Mth01, ");
            SQL.AppendLine("            Case Mth When '02' Then Amt Else 0.0000 End As Mth02, ");
            SQL.AppendLine("            Case Mth When '03' Then Amt Else 0.0000 End As Mth03, ");
            SQL.AppendLine("            Case Mth When '04' Then Amt Else 0.0000 End As Mth04, ");
            SQL.AppendLine("            Case Mth When '05' Then Amt Else 0.0000 End As Mth05, ");
            SQL.AppendLine("            Case Mth When '06' Then Amt Else 0.0000 End As Mth06, ");
            SQL.AppendLine("            Case Mth When '07' Then Amt Else 0.0000 End As Mth07, ");
            SQL.AppendLine("            Case Mth When '08' Then Amt Else 0.0000 End As Mth08, ");
            SQL.AppendLine("            Case Mth When '09' Then Amt Else 0.0000 End As Mth09, ");
            SQL.AppendLine("            Case Mth When '10' Then Amt Else 0.0000 End As Mth10, ");
            SQL.AppendLine("            Case Mth When '11' Then Amt Else 0.0000 End As Mth11, ");
            SQL.AppendLine("            Case Mth When '12' Then Amt Else 0.0000 End As Mth12 ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select C.CCCode As CodeParent, B.CCCode As CodeChild, A.CCtCode, E.AcNo, E.Mth, "); 
            SQL.AppendLine("                Sum(IfNull(E.Amt, 0.0000)) As Amt ");
            SQL.AppendLine("                From TblCostCategory A  ");
            SQL.AppendLine("                Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            if (mIsRptMonthlyCostFilterByCostCenter)
            {
                SQL.AppendLine("                And B.CCCode In ( ");
                SQL.AppendLine("                    Select Distinct A.CCCode ");
                SQL.AppendLine("                    From TblGroupCostCenter A ");
                SQL.AppendLine("                    Inner Join TblUser B On A.GrpCode = B.GrpCode ");
                SQL.AppendLine("                    Where Upper(B.UserCode)= Upper(@UserCode) ");
                SQL.AppendLine("                ) ");
            }
            SQL.AppendLine("                Left Join TblCostCenter C On B.Parent = C.CCCode  ");
            SQL.AppendLine("                Inner Join ( ");
            SQL.AppendLine("                    Select convert('01' using latin1) As Mth Union All ");
            SQL.AppendLine("                    Select convert('02' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('03' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('04' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('05' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('06' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('07' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('08' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('09' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('10' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('11' using latin1) Union All ");
            SQL.AppendLine("                    Select convert('12' using latin1)  ");
            SQL.AppendLine("                ) D On 1=1  ");
            SQL.AppendLine("                Left Join ( ");
            SQL.AppendLine("                    Select Mth, AcNo, Sum(Amt) Amt ");
            SQL.AppendLine("                    From ( ");
            SQL.AppendLine("                        Select Substring(T1.DocDt, 5, 2) As Mth, T2.AcNo, ");
            SQL.AppendLine("                        Case T3.AcType  ");
            SQL.AppendLine("                            When 'D' Then IfNull(T2.DAmt, 0)- IfNull(T2.CAmt,0) ");
            SQL.AppendLine("                            When 'C' Then IfNull(T2.CAmt, 0)- IfNull(T2.DAmt,0) ");
            SQL.AppendLine("                        End As Amt ");
            SQL.AppendLine("                        From TblJournalHdr T1 ");
            SQL.AppendLine("                        Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("                        Inner Join TblCOA T3 On T2.AcNo=T3.AcNo ");
            SQL.AppendLine("                        Where Left(T1.DocDt, 4) = @Year ");
            if (mIsRptMonthlyCostInclPayrollBrutto)
            {
                SQL.AppendLine("                        Union All ");
                SQL.AppendLine("                        Select Substring(T2.EndDt, 5, 2) As Mth, T3.AcNo3 As AcNo, ");
                SQL.AppendLine("                        T1.Brutto As Amt ");
                SQL.AppendLine("                        From TblPayrollProcess1 T1 ");
                SQL.AppendLine("                        Inner Join TblPayrun T2 On T1.PayrunCode=T2.PayrunCode And T2.CancelInd='N' And Left(T2.EndDt, 4)=@Year  ");
                SQL.AppendLine("                        Inner Join TblDepartment T3 On T2.DeptCode=T3.DeptCode And T3.AcNo3 Is Not Null ");

                SQL.AppendLine("                        Union All ");
                SQL.AppendLine("                        Select T4.CCCode, Substring(T1.DocDt, 5, 2) As Mth, T3.AcNo2 As AcNo, ");
                SQL.AppendLine("                        T2.Amt As Amt ");
                SQL.AppendLine("                        From TblBonusHdr T1 ");
                SQL.AppendLine("                        Inner Join TblBonusDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                        Inner Join TblDepartment T3 On T2.DeptCode=T3.DeptCode And T3.AcNo2 Is Not Null ");
                SQL.AppendLine("                        Inner Join TblCostCenter T4 On T3.DeptCode=T4.DeptCode And T4.DeptCode Is Not Null And T4.IsCCPayrollJournalEnabled='Y' And T4.ActInd='Y' ");
                SQL.AppendLine("                        Where T1.CancelInd='N' And T1.Status='A' And Left(T1.DocDt, 4)=@Year ");

                SQL.AppendLine("                        Union All ");
                SQL.AppendLine("                        Select T4.CCCode, Substring(T1.HolidayDt, 5, 2) As Mth, T3.AcNo7 As AcNo, ");
                SQL.AppendLine("                        T2.Amt As Amt ");
                SQL.AppendLine("                        From TblRHAHdr T1 ");
                SQL.AppendLine("                        Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                        Inner Join TblDepartment T3 On T2.DeptCode=T3.DeptCode And T3.AcNo7 Is Not Null ");
                SQL.AppendLine("                        Inner Join TblCostCenter T4 On T3.DeptCode=T4.DeptCode And T4.DeptCode Is Not Null And T4.IsCCPayrollJournalEnabled='Y' And T4.ActInd='Y' ");
                SQL.AppendLine("                        Where T1.CancelInd='N' And T1.Status='A' And Left(T1.HolidayDt, 4)=@Year ");
            }
            SQL.AppendLine("                    ) T ");
            SQL.AppendLine("                    Group By Mth, AcNo ");
            SQL.AppendLine("                    Having Amt<>0 ");
            SQL.AppendLine("                ) E On A.AcNo=E.AcNo And D.Mth=Convert(E.Mth using latin1) ");
            SQL.AppendLine("                Group By C.CCCode, B.CCCode, A.CCtCode, E.AcNo, E.Mth "); 
            SQL.AppendLine("        ) Tbl ");
            SQL.AppendLine("    ) X ");
            SQL.AppendLine("    Group By CodeParent, CodeChild, CCtCode, AcNo ");
            SQL.AppendLine("    Having  ");
            SQL.AppendLine("    Sum(Mth01)<>0 Or Sum(Mth02)<>0 Or Sum(Mth03)<>0 Or Sum(Mth04)<>0 Or Sum(Mth05)<>0 Or Sum(Mth06)<>0 Or ");
            SQL.AppendLine("    Sum(Mth07)<>0 Or Sum(Mth08)<>0 Or Sum(Mth09)<>0 Or Sum(Mth10)<>0 Or Sum(Mth11)<>0 Or Sum(Mth12)<>0 ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Inner Join TblCostCategory Tbl2 On Tbl1.CCtCode=Tbl2.CCtCode ");
            SQL.AppendLine("Inner Join TblCostCenter Tbl3 On Tbl1.CodeChild=Tbl3.CCCode ");
            SQL.AppendLine("Left Join TblCostCenter Tbl4 On Tbl1.CodeParent=Tbl4.CCCode  ");
            SQL.AppendLine("Left Join TblOption Tbl5 On Tbl2.CCGrpCode=Tbl5.OptCode And Tbl5.OptCat='CostCenterGroup' ");
            SQL.AppendLine("Left Join TblProfitCenter Tbl6 On Tbl3.ProfitCenterCode=Tbl6.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity Tbl7 On Tbl6.EntCode=Tbl7.EntCode ");
            SQL.AppendLine("Inner Join TblCOA Tbl8 On Tbl1.AcNo=Tbl8.AcNo ");


            //SQL.AppendLine("Select * From ");
            //SQL.AppendLine("(Select CodeParent, Parent, CodeChild, Child, CCtCode, AcNo, AcDesc, CCtName, CCGrpName, EntCode, EntName, ");
            //SQL.AppendLine("(Sum(Mth01)+Sum(Mth02)+Sum(Mth03)+Sum(Mth04)+Sum(Mth05)+Sum(Mth06)+ ");
            //SQL.AppendLine("Sum(Mth07)+Sum(Mth08)+Sum(Mth09)+Sum(Mth10)+Sum(Mth11)+Sum(Mth12)) As Total,");
            //SQL.AppendLine("Sum(Mth01) As Mth01, Sum(Mth02) As Mth02, Sum(Mth03) As Mth03, Sum(Mth04) As Mth04, ");
            //SQL.AppendLine("Sum(Mth05) As Mth05, Sum(Mth06) As Mth06, Sum(Mth07) As Mth07, Sum(Mth08) As Mth08, ");
            //SQL.AppendLine("Sum(Mth09) As Mth09, Sum(Mth10) As Mth10, Sum(Mth11) As Mth11, Sum(Mth12) As Mth12 ");
            //SQL.AppendLine("From ( ");
            //SQL.AppendLine("    Select CodeParent, Parent, CodeChild, Child, CCtCode, AcNo, AcDesc, CCtName, CCGrpName, EntCode, EntName, ");
            //SQL.AppendLine("        Case Mth When '01' Then Amt Else 0.0000 End As Mth01, ");
            //SQL.AppendLine("        Case Mth When '02' Then Amt Else 0.0000 End As Mth02, ");
            //SQL.AppendLine("        Case Mth When '03' Then Amt Else 0.0000 End As Mth03, ");
            //SQL.AppendLine("        Case Mth When '04' Then Amt Else 0.0000 End As Mth04, ");
            //SQL.AppendLine("        Case Mth When '05' Then Amt Else 0.0000 End As Mth05, ");
            //SQL.AppendLine("        Case Mth When '06' Then Amt Else 0.0000 End As Mth06, ");
            //SQL.AppendLine("        Case Mth When '07' Then Amt Else 0.0000 End As Mth07, ");
            //SQL.AppendLine("        Case Mth When '08' Then Amt Else 0.0000 End As Mth08, ");
            //SQL.AppendLine("        Case Mth When '09' Then Amt Else 0.0000 End As Mth09, ");
            //SQL.AppendLine("        Case Mth When '10' Then Amt Else 0.0000 End As Mth10, ");
            //SQL.AppendLine("        Case Mth When '11' Then Amt Else 0.0000 End As Mth11, ");
            //SQL.AppendLine("        Case Mth When '12' Then Amt Else 0.0000 End As Mth12 ");
            //SQL.AppendLine("    From ( ");
            //SQL.AppendLine("        Select C.CCCode As CodeParent, IfNull(C.CCName, '') As Parent, ");
            //SQL.AppendLine("        B.CCCode As CodeChild, B.CCName As Child, ");
            //SQL.AppendLine("        A.CCtCode As CCtCode, A.CCtName, ");
            //SQL.AppendLine("        E.AcNo, E.AcDesc, F.OptDesc As CCGrpName, D.Mth, ");
            //SQL.AppendLine("        H.EntCode, H.EntName, ");
            //SQL.AppendLine("        Sum(IfNull(E.Amt, 0)) As Amt ");
            //SQL.AppendLine("        From TblCostCategory A  ");
            //SQL.AppendLine("        Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            //SQL.AppendLine("            And B.CCCode In ( ");
            //SQL.AppendLine("                Select Distinct A.CCCode ");
            //SQL.AppendLine("                From TblGroupCostCenter A ");
            //SQL.AppendLine("                Inner Join TblUser B On A.GrpCode = B.GrpCode ");
            //SQL.AppendLine("                Where Upper(B.UserCode)= Upper(@UserCode) ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("        Left Join TblCostCenter C On B.Parent = C.CCCode  ");
            //SQL.AppendLine("        Inner Join ( ");
            //SQL.AppendLine("            Select convert('01' using latin1) As Mth Union All ");
            //SQL.AppendLine("            Select convert('02' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('03' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('04' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('05' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('06' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('07' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('08' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('09' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('10' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('11' using latin1) Union All ");
            //SQL.AppendLine("            Select convert('12' using latin1)  ");
            //SQL.AppendLine("        ) D On 0=0  ");
            //SQL.AppendLine("        Left Join ( ");
            //SQL.AppendLine("            Select Mth, AcNo, Sum(Amt) Amt, AcDesc ");
            //SQL.AppendLine("            From ( ");
            //SQL.AppendLine("                Select Substring(T1.DocDt, 5, 2) As Mth, T2.AcNo, T3.AcDesc, ");
            //SQL.AppendLine("                Case T3.AcType  ");
            //SQL.AppendLine("                    When 'D' Then IfNull(T2.DAmt, 0)- IfNull(T2.CAmt,0) ");
            //SQL.AppendLine("                    When 'C' Then IfNull(T2.CAmt, 0)- IfNull(T2.DAmt,0) ");
            //SQL.AppendLine("                End As Amt ");
            //SQL.AppendLine("                From TblJournalHdr T1 ");
            //SQL.AppendLine("                Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("                Inner Join TblCOA T3 On T2.AcNo = T3.AcNo ");
            //SQL.AppendLine("                Where Left(T1.DocDt, 4) = @Year "); 
            //SQL.AppendLine("            ) T Group By Mth, AcNo, AcDesc ");
            //SQL.AppendLine("            Having Amt<>0 ");
            //SQL.AppendLine("        ) E On A.AcNo=E.AcNo And D.Mth=Convert(E.Mth using latin1) ");
            //SQL.AppendLine("        Inner Join TblOption F On A.CCGrpCode = F.OptCode And F.OptCat = 'CostCenterGroup' ");
            //SQL.AppendLine("        Left Join TblProfitCenter G On B.ProfitCenterCode=G.ProfitCenterCode ");
            //SQL.AppendLine("        Left Join TblEntity H On G.EntCode=H.EntCode ");
            //SQL.AppendLine("        Group By C.CCCode, C.CCName, B.CCCode, B.CCName, ");
            //SQL.AppendLine("        A.CCtCode, E.AcNo, E.AcDesc, A.CCtName, F.OptDesc , D.Mth, H.EntCode, H.EntName ");
            //SQL.AppendLine("    ) Tbl ");
            //SQL.AppendLine(") X ");

            //SQL.AppendLine("Group By CodeParent, Parent, CodeChild, Child, CCtCode, AcNo, AcDesc, CCtName, CCGrpName, EntCode, EntName ");
            //SQL.AppendLine("Having  ");
            //SQL.AppendLine("    Sum(Mth01)<>0 Or  Sum(Mth02)<>0 Or Sum(Mth03)<>0 Or Sum(Mth04)<>0 Or Sum(Mth05)<>0 Or Sum(Mth06)<>0 Or ");
            //SQL.AppendLine("    Sum(Mth07)<>0 Or Sum(Mth08)<>0 Or Sum(Mth09)<>0 Or Sum(Mth10)<>0 Or Sum(Mth11)<>0 Or Sum(Mth12)<>0 ");
            //SQL.AppendLine("Order By Parent, Child, CCtName ");
            //SQL.AppendLine(") Z1 ");
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Parent",

                        //1-5
                        "Cost Center Parent",
                        "Child",
                        "Cost Center Child", 
                        "Cost Category Code",
                        "Account#",

                        //6-10
                        "Description",
                        "Cost Category",
                        "Total",
                        "",
                        "January",

                        //11-15
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        
                        //16-20
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",

                        //21-23
                        "December",
                        "Cost Category's"+Environment.NewLine+"Group",
                        "Entity"
                    },
                    new int[] 
                    {
                        //0
                        80, 

                        //1-5
                        200, 80, 200, 80, 150, 
                        
                        //6-10
                        200, 200, 130, 20, 100,   

                        //11-15
                        100, 100, 100, 100, 100,   
                        
                        //16-20
                        100, 100, 100, 100, 100, 
                        
                        //21-23
                        100, 180, 180
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, false);
            if(mMenuCode == Sm.GetParameter("MenuCodeForMonthlyCost"))
            {
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 9});
            }
            else if (mMenuCode == Sm.GetParameter("MenuCodeForMonthlyCostWithDO"))
            {
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, });
            }
            Sm.GrdColButton(Grd1, new int[] { 9 });
            Grd1.Cols[22].Move(8);
            Grd1.Cols[23].Move(9);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);

            if (Year.Length==0)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, Filter2 = String.Empty, Filter3 = String.Empty, Filter4 = " ";
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SetProfitCenter();

                Sm.CmParam<String>(ref cm, "@Year", Year);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "Tbl1.CodeChild", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEntCode), "Tbl7.EntCode", true);
                Sm.FilterStr(ref Filter4, ref cm, TxtCCName.Text, new string[] { "B.CCCode", "B.CCName" });

                SQL.AppendLine("Select Tbl1.CodeParent, Tbl1.CodeChild, Tbl1.CCtCode, Tbl1.AcNo, ");
                SQL.AppendLine("Tbl1.Total, Tbl1.Mth01, Tbl1.Mth02, Tbl1.Mth03, Tbl1.Mth04, Tbl1.Mth05, Tbl1.Mth06, ");
                SQL.AppendLine("Tbl1.Mth07, Tbl1.Mth08, Tbl1.Mth09, Tbl1.Mth10, Tbl1.Mth11, Tbl1.Mth12, ");
                SQL.AppendLine("Tbl4.CCName As Parent, Tbl3.CCName As Child, Tbl8.AcDesc, Tbl2.CCtName, Tbl5.OptDesc As CCGrpName, Tbl7.EntName ");
                SQL.AppendLine("From (");
                SQL.AppendLine("    Select CodeParent, CodeChild, CCtCode, AcNo, ");
                SQL.AppendLine("    (Sum(Mth01)+Sum(Mth02)+Sum(Mth03)+Sum(Mth04)+Sum(Mth05)+Sum(Mth06)+ ");
                SQL.AppendLine("    Sum(Mth07)+Sum(Mth08)+Sum(Mth09)+Sum(Mth10)+Sum(Mth11)+Sum(Mth12)) As Total,");
                SQL.AppendLine("    Sum(Mth01) As Mth01, Sum(Mth02) As Mth02, Sum(Mth03) As Mth03, Sum(Mth04) As Mth04, ");
                SQL.AppendLine("    Sum(Mth05) As Mth05, Sum(Mth06) As Mth06, Sum(Mth07) As Mth07, Sum(Mth08) As Mth08, ");
                SQL.AppendLine("    Sum(Mth09) As Mth09, Sum(Mth10) As Mth10, Sum(Mth11) As Mth11, Sum(Mth12) As Mth12 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select CodeParent, CodeChild, CCtCode, AcNo, ");
                SQL.AppendLine("            Case Mth When '01' Then Amt Else 0.0000 End As Mth01, ");
                SQL.AppendLine("            Case Mth When '02' Then Amt Else 0.0000 End As Mth02, ");
                SQL.AppendLine("            Case Mth When '03' Then Amt Else 0.0000 End As Mth03, ");
                SQL.AppendLine("            Case Mth When '04' Then Amt Else 0.0000 End As Mth04, ");
                SQL.AppendLine("            Case Mth When '05' Then Amt Else 0.0000 End As Mth05, ");
                SQL.AppendLine("            Case Mth When '06' Then Amt Else 0.0000 End As Mth06, ");
                SQL.AppendLine("            Case Mth When '07' Then Amt Else 0.0000 End As Mth07, ");
                SQL.AppendLine("            Case Mth When '08' Then Amt Else 0.0000 End As Mth08, ");
                SQL.AppendLine("            Case Mth When '09' Then Amt Else 0.0000 End As Mth09, ");
                SQL.AppendLine("            Case Mth When '10' Then Amt Else 0.0000 End As Mth10, ");
                SQL.AppendLine("            Case Mth When '11' Then Amt Else 0.0000 End As Mth11, ");
                SQL.AppendLine("            Case Mth When '12' Then Amt Else 0.0000 End As Mth12 ");
                SQL.AppendLine("            From ( ");
                SQL.AppendLine("                Select C.CCCode As CodeParent, B.CCCode As CodeChild, A.CCtCode, E.AcNo, E.Mth, ");
                SQL.AppendLine("                Sum(IfNull(E.Amt, 0.0000)) As Amt ");
                SQL.AppendLine("                From TblCostCategory A  ");
                SQL.AppendLine("                Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
                SQL.AppendLine(Filter4);
                if (mIsRptMonthlyCostFilterByCostCenter)
                {
                    SQL.AppendLine("                And B.CCCode In ( ");
                    SQL.AppendLine("                    Select Distinct A.CCCode ");
                    SQL.AppendLine("                    From TblGroupCostCenter A ");
                    SQL.AppendLine("                    Inner Join TblUser B On A.GrpCode = B.GrpCode ");
                    SQL.AppendLine("                    Where Upper(B.UserCode)= Upper(@UserCode) ");
                    SQL.AppendLine("                ) ");
                }
                if (mIsRptMonthlyCostUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        int i = 0;

                        SQL.AppendLine("    And B.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        ) ");
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter3.Length > 0) Filter3 += " Or ";
                            Filter3 += " (ProfitCenterCode=@ProfitCenterCode_2_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode_2_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter3.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter3 + ") ");
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And B.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode_2) ");
                            SQL.AppendLine("    ) ");
                            if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode_2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And B.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("    )) ");
                        }
                    }
                }

                SQL.AppendLine("                Left Join TblCostCenter C On B.Parent = C.CCCode  ");
                SQL.AppendLine("                Inner Join ( ");
                SQL.AppendLine("                    Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("                    Select convert('02' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('03' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('04' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('05' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('06' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('07' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('08' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('09' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('10' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('11' using latin1) Union All ");
                SQL.AppendLine("                    Select convert('12' using latin1)  ");
                SQL.AppendLine("                ) D On 1=1  ");
                SQL.AppendLine("                Left Join ( ");
                SQL.AppendLine("                    Select CCCode, Mth, AcNo, Sum(Amt) Amt ");
                SQL.AppendLine("                    From ( ");
                SQL.AppendLine("                        Select T1.CCCode, Substring(T1.DocDt, 5, 2) As Mth, T2.AcNo, ");
                SQL.AppendLine("                        Case T3.AcType  ");
                SQL.AppendLine("                            When 'D' Then IfNull(T2.DAmt, 0)- IfNull(T2.CAmt,0) ");
                SQL.AppendLine("                            When 'C' Then IfNull(T2.CAmt, 0)- IfNull(T2.DAmt,0) ");
                SQL.AppendLine("                        End As Amt ");
                SQL.AppendLine("                        From TblJournalHdr T1 ");
                SQL.AppendLine("                        Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("                        Inner Join TblCOA T3 On T2.AcNo=T3.AcNo ");
                SQL.AppendLine("                        Inner Join TblCostCenter T4 On T1.CCCode=T4.CCCode ");
                SQL.AppendLine(Filter4.Replace("B.", "T4."));
                SQL.AppendLine("                        Where Left(T1.DocDt, 4) = @Year ");
                if (mIsRptMonthlyCostUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        int i = 0;

                        SQL.AppendLine("    And T1.CCCode Is Not Null ");
                        SQL.AppendLine("    And T1.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        ) ");
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenterCode_1_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode_1_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter2 + ") ");
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And T1.CCCode Is Not Null ");
                            SQL.AppendLine("    And T1.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode_1) ");
                            SQL.AppendLine("    ) ");
                            if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode_1", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And (T1.CCCode Is Null Or (T1.CCCode Is Not Null And T1.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And ProfitCenterCode In (");
                            SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        )))) ");
                        }
                    }
                }
                if (mIsRptMonthlyCostInclPayrollBrutto)
                {
                    SQL.AppendLine("                        Union All ");
                    SQL.AppendLine("                        Select T4.CCCode, Substring(T2.EndDt, 5, 2) As Mth, T3.AcNo3 As AcNo, ");
                    SQL.AppendLine("                        T1.Brutto As Amt ");
                    SQL.AppendLine("                        From TblPayrollProcess1 T1 ");
                    SQL.AppendLine("                        Inner Join TblPayrun T2 On T1.PayrunCode=T2.PayrunCode And T2.CancelInd='N' And Left(T2.EndDt, 4)=@Year ");
                    SQL.AppendLine("                        Inner Join TblDepartment T3 On T2.DeptCode=T3.DeptCode And T3.AcNo3 Is Not Null ");
                    SQL.AppendLine("                        Inner Join TblCostCenter T4 On T3.DeptCode=T4.DeptCode And T4.DeptCode Is Not Null And T4.IsCCPayrollJournalEnabled='Y' And T4.ActInd='Y' ");

                    SQL.AppendLine("                        Union All ");
                    SQL.AppendLine("                        Select T4.CCCode, Substring(T1.DocDt, 5, 2) As Mth, T3.AcNo2 As AcNo, ");
                    SQL.AppendLine("                        T2.Amt As Amt ");
                    SQL.AppendLine("                        From TblBonusHdr T1 ");
                    SQL.AppendLine("                        Inner Join TblBonusDtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("                        Inner Join TblDepartment T3 On T2.DeptCode=T3.DeptCode And T3.AcNo2 Is Not Null ");
                    SQL.AppendLine("                        Inner Join TblCostCenter T4 On T3.DeptCode=T4.DeptCode And T4.DeptCode Is Not Null And T4.IsCCPayrollJournalEnabled='Y' And T4.ActInd='Y' ");
                    SQL.AppendLine("                        Where T1.CancelInd='N' And T1.Status='A' And Left(T1.DocDt, 4)=@Year ");

                    SQL.AppendLine("                        Union All ");
                    SQL.AppendLine("                        Select T4.CCCode, Substring(T1.HolidayDt, 5, 2) As Mth, T3.AcNo7 As AcNo, ");
                    SQL.AppendLine("                        T2.Amt As Amt ");
                    SQL.AppendLine("                        From TblRHAHdr T1 ");
                    SQL.AppendLine("                        Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("                        Inner Join TblDepartment T3 On T2.DeptCode=T3.DeptCode And T3.AcNo7 Is Not Null ");
                    SQL.AppendLine("                        Inner Join TblCostCenter T4 On T3.DeptCode=T4.DeptCode And T4.DeptCode Is Not Null And T4.IsCCPayrollJournalEnabled='Y' And T4.ActInd='Y' ");
                    SQL.AppendLine("                        Where T1.CancelInd='N' And T1.Status='A' And Left(T1.HolidayDt, 4)=@Year ");
                }
                SQL.AppendLine("                    ) T ");
                SQL.AppendLine("                    Group By CCCode, Mth, AcNo ");
                SQL.AppendLine("                    Having Amt<>0 ");
                SQL.AppendLine("                ) E On A.AcNo=E.AcNo And D.Mth=Convert(E.Mth using latin1) And B.CCCode = E.CCCode  ");
                SQL.AppendLine("                Group By C.CCCode, B.CCCode, A.CCtCode, E.AcNo, E.Mth ");
                SQL.AppendLine("        ) Tbl ");
                SQL.AppendLine("    ) X ");
                SQL.AppendLine("    Group By CodeParent, CodeChild, CCtCode, AcNo ");
                SQL.AppendLine("    Having  ");
                SQL.AppendLine("    Sum(Mth01)<>0 Or Sum(Mth02)<>0 Or Sum(Mth03)<>0 Or Sum(Mth04)<>0 Or Sum(Mth05)<>0 Or Sum(Mth06)<>0 Or ");
                SQL.AppendLine("    Sum(Mth07)<>0 Or Sum(Mth08)<>0 Or Sum(Mth09)<>0 Or Sum(Mth10)<>0 Or Sum(Mth11)<>0 Or Sum(Mth12)<>0 ");
                SQL.AppendLine(") Tbl1 ");
                SQL.AppendLine("Inner Join TblCostCategory Tbl2 On Tbl1.CCtCode=Tbl2.CCtCode ");
                SQL.AppendLine("Inner Join TblCostCenter Tbl3 On Tbl1.CodeChild=Tbl3.CCCode ");
                SQL.AppendLine("Left Join TblCostCenter Tbl4 On Tbl1.CodeParent=Tbl4.CCCode  ");
                SQL.AppendLine("Left Join TblOption Tbl5 On Tbl2.CCGrpCode=Tbl5.OptCode And Tbl5.OptCat='CostCenterGroup' ");
                SQL.AppendLine("Left Join TblProfitCenter Tbl6 On Tbl3.ProfitCenterCode=Tbl6.ProfitCenterCode ");
                SQL.AppendLine("Left Join TblEntity Tbl7 On Tbl6.EntCode=Tbl7.EntCode ");
                SQL.AppendLine("Inner Join TblCOA Tbl8 On Tbl1.AcNo=Tbl8.AcNo ");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString() + Filter,
                        new string[]
                        { 
                            //0
                            "CodeParent",  
                            //1-5
                            "Parent", "CodeChild", "Child", "CCtCode", "AcNo",   
                            //6-10
                            "AcDesc", "CCtName", "Total", "Mth01", "Mth02",   
                            //11-15
                            "Mth03", "Mth04", "Mth05", "Mth06", "Mth07",  
                            //16-20
                            "Mth08", "Mth09", "Mth10", "Mth11", "Mth12", 
                            //21-22
                            "CCGrpName", "EntName"
                        },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (Grd1.Rows.Count > 1)
                    Sm.FocusGrd(Grd1, 1, ChkHideInfoInGrd.Checked ? 7 : 3);
                else
                    Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }

        }

        #region Additional Method

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
        }

        public static void SetLueCCCode(ref LookUpEdit Lue, string EntCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("select Distinct A.CCCode As Col1, C.CCName As Col2 From ");
            SQL.AppendLine("TblGroupCostCenter A ");
            SQL.AppendLine("Inner Join TblUser B On A.GrpCode = B.GrpCode ");
            SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode = C.CCCOde ");
            if (EntCode.Length > 0)
            {
                SQL.AppendLine("And C.CCCode In (");
                SQL.AppendLine("    Select CCCOde From TblCostCenter T1, TblProfitCenter T2 ");
                SQL.AppendLine("    Where T1.ProfitCenterCode=T2.ProfitCenterCode  ");
                SQL.AppendLine("    And T2.EntCode Is Not Null And T2.EntCode='" + EntCode+ "' ");
                SQL.AppendLine(") ");    
            }
            SQL.AppendLine("Where B.UserCode = '"+Gv.CurrentUserCode+"' ");
            SQL.AppendLine("Order By C.CCName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(), 
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptMonthlyCostUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName AS Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));

            //var IsIncludeConsolidate = Value.Contains("Consolidate");
            //return string.Concat(
            //    (IsIncludeConsolidate?"Consolidate, ":string.Empty), 
            //    Sm.GetValue(
            //        "Select Group_Concat(T.Code Separator ', ') As Code " +
            //        "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ", 
            //        Value.Replace(", ", ",")));
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptMonthlyCostDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetGrdStr(Grd1, e.RowIndex, 4), Sm.GetLue(LueYr));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtCCCodeParent.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtCCCodeChild.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.TxtCCtCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmRptMonthlyCostDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetGrdStr(Grd1, e.RowIndex, 4), Sm.GetLue(LueYr));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtCCCodeParent.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtCCCodeChild.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.TxtCCtCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
            var EntCode = Sm.GetLue(LueEntCode);
            LueCCCode.EditValue = null;
            ChkCCCode.Checked = false;
            if (EntCode.Length == 0)
                SetLueCCCode(ref LueCCCode, string.Empty);
            else
                SetLueCCCode(ref LueCCCode, EntCode);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            var EntCode = Sm.GetLue(LueEntCode);
            if (EntCode.Length == 0)
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), EntCode);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        private void TxtCCName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost center");
        }

        #endregion

        #endregion
    }
}
