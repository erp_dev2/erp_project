﻿#region Update
/*
    01/10/2018 [HAR] ubah rumus bruto
    18/01/2019 [HAR] show other allowance ambilnya dari payrollprocessAD (ShowPayrollProcessAD)
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RuniProbe.GlobalVar;
using Sm = RuniProbe.StdMtd;
using Sl = RuniProbe.SetLue;

#endregion

namespace RuniProbe
{
    public partial class FrmRptPayrollProcessSummary8 : RuniProbe.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal string mSalaryInd = "1", mRptPayrollProcessSummaryVersion ="1";
        private bool
             mIsNotFilterByAuthorization = false,
             mIsFilterBySiteHR = false,
             mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummary8(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd(); 
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mRptPayrollProcessSummaryVersion = Sm.GetParameter("RptPayrollProcessSummaryVersion");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, A.EmpCode, C.EmpName, C.EmpCodeOld, E.PosName, D.DeptName, C.JoinDt, ");
            SQL.AppendLine("C.ResignDt, F.OptDesc As SystemTypeDesc, G.OptDesc As PayrunPeriodDesc, H.PGName, J.SiteName, A.NPWP, ");
            SQL.AppendLine("I.OptDesc As NonTaxableIncomeDesc, A.Salary, A.WorkingDay, A.PLDay, A.PLHr, A.PLAmt, A.ProcessPLAmt, ");
            SQL.AppendLine("A.UPLDay, A.UPLHr, A.UPLAmt, A.ProcessUPLAmt, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, A.OT1Amt, A.OT2Amt, ");
            SQL.AppendLine("A.OTHolidayAmt, A.TaxableFixAllowance, A.NonTaxableFixAllowance, (ifnull(A.FixAllowance, 0)) FixAllowance, ");
            SQL.AppendLine("ifnull(K.Amt001, 0) Al001, ifnull(K.Amt004, 0) Al004, A.PerformanceValue, ");
            SQL.AppendLine("A.ADOT, A.Meal, A.Transport, A.TaxAllowance, A.SSEmployerPension, ");
            SQL.AppendLine("A.SSEmployerHealth, A.SSEmployeeHealth, A.SSEmployerEmployment,  A.SSEmployeeEmployment, A.SSErPension, A.SSEePension, ");
            SQL.AppendLine("A.SSEmployeePension, A.NonTaxableFixDeduction, A.TaxableFixDeduction, A.FixDeduction, A.DedEmployee, ");
            SQL.AppendLine("A.DedProduction, A.DedProdLeave, A.EmpAdvancePayment, A.SalaryAdjustment,  ");
            //SCU
            SQL.AppendLine("(A.Salary+A.FixAllowance+A.meal+A.transport+A.VariableAllowance+A.SSErPension+A.SSEmployerHealth+ ");
            SQL.AppendLine("A.SSEmployerEmployment+A.SSErDPLK+A.SeveranceReserve+A.taxAllowance+A.AddSeveranceReserve+A.IncEmployee+ADLeave+A.SalaryAdjustment) As Brutto, ");
            //AWG
            //SQL.AppendLine("(A.Salary+A.FixAllowance+A.SSEmployerhealth+A.SSEmployerEmployment+A.SSErPension+A.OverHrAmt+A.AtdPremi+A.ShiftPremi) As Brutto, "); 
            //Old
            // (A.Amt+A.Tax-A.TaxAllowance) As Brutto, ");
            SQL.AppendLine("A.Tax, A.EOYTax, A.Amt, A.VoucherRequestPayrollDocNo, A.OverHr, A.OverHrAmt, (A.AtdPremi+A.AtdPremi2+A.AtdPremi3) As AtdPremi, A.ShiftPremi, ");
            SQL.AppendLine("A.SeveranceReserve, A.AddSeveranceReserve, A.VariableAllowance, A.SSErDPLK, A.SSEeDPLK, C.DeptCode, L.GrdLvlname,  ");
            SQL.AppendLine("(A.Salary+A.FixAllowance+A.meal+A.transport+A.VariableAllowance+A.IncEmployee+ADLeave+A.SalaryAdjustment) As NetIncome, A.IncEmployee, ifnull(M.AmtInsPntSA, 0) AmtInsPntSA, ifnull(N.AmtInsPntUTD, 0) AmtInsPntUTD, A.ADleave ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption G On B.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr H On B.PGCode=H.PGCode ");
            SQL.AppendLine("Left Join TblOption I On A.PTKP=I.OptCode And I.OptCat='NonTaxableIncome' ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X.EmpCode, X.payruncode, X.Amt001, X.Amt004  ");
	        SQL.AppendLine("    from  ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select A.EmpCode, A.PayrunCode, A.Amt As Amt001, ifnull(B.Amt004, 0) Amt004  ");
		    SQL.AppendLine("        From TblPayrollProcessAd A ");
		    SQL.AppendLine("        Left Join  ");
		    SQL.AppendLine("        (  ");
			SQL.AppendLine("            Select A.EmpCode, A.PayrunCode, A.Amt As Amt004  ");
			SQL.AppendLine("            From TblPayrollProcessAd A ");
			SQL.AppendLine("            Where A.AdCode ='065' ");
		    SQL.AppendLine("        )B On A.PayrunCode = B.payrunCode And A.EmpCode = B.EmpCode ");
		    SQL.AppendLine("        Where A.AdCode ='001' ");
	        SQL.AppendLine("    )X ");
            SQL.AppendLine(")K On A.payruncode = K.PayrunCode And  A.EmpCode = K.EmpCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr L On C.GrdLvlCode = L.GrdLvlCode ");

            SQL.AppendLine(" Left Join ( ");
            SQL.AppendLine("     Select A.payrunCOde, A.EmpCode, D.insPntCode, D.InspntName, SUM(B.AmtInsPnt) As AmtInsPntSA ");
            SQL.AppendLine("     From tblpayrollProcess1 A ");
            SQL.AppendLine("     Inner Join tblEmpInsPntDtl B On A.PayrunCode = B.PayrunCode And A.EmpCode = B.EmpCode ");
            SQL.AppendLine("     Inner Join TblEmpInsPnthdr C On B.DocNo = C.DocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("     Inner Join tblinspnt D On C.InsPntCode = D.InspntCode ");
            SQL.AppendLine("     Where D.InsPntCode='01' ");
            SQL.AppendLine("     Group by A.payrunCOde, A.EmpCode, D.InspntName ");
            SQL.AppendLine("     Having Sum(B.AmtInsPnt)<>0.00 ");
            SQL.AppendLine("     Order By D.InspntName ");
            SQL.AppendLine(" )M On  A.PayrunCode=M.PayrunCode And A.EmpCode=M.EmpCode  ");

            SQL.AppendLine(" Left Join ( ");
            SQL.AppendLine("     Select A.payrunCOde, A.EmpCode, D.insPntCode, D.InspntName, SUM(B.AmtInsPnt) As AmtInsPntUTD ");
            SQL.AppendLine("     From tblpayrollProcess1 A ");
            SQL.AppendLine("     Inner Join tblEmpInsPntDtl B On A.PayrunCode = B.PayrunCode And A.EmpCode = B.EmpCode ");
            SQL.AppendLine("     Inner Join TblEmpInsPnthdr C On B.DocNo = C.DocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("     Inner Join tblinspnt D On C.InsPntCode = D.InspntCode ");
            SQL.AppendLine("     Where  D.InsPntCode='02' ");
            SQL.AppendLine("     Group by A.payrunCOde, A.EmpCode, D.InspntName ");
            SQL.AppendLine("     Having Sum(B.AmtInsPnt)<>0.00 ");
            SQL.AppendLine("     Order By D.InspntName ");
            SQL.AppendLine(" )N On  A.PayrunCode=N.PayrunCode And A.EmpCode=N.EmpCode  ");

            SQL.AppendLine("Where 1=1 ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 82;
            Grd1.FrozenArea.ColCount = 6;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "",
                        "Payrun"+Environment.NewLine+"Code",
                        "Payrun Name",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        //6-10
                        "Old Code",
                        "Position",
                        "Department"+Environment.NewLine+"Code",
                        "Department",
                        "Join"+Environment.NewLine+"Date",
                        //11-15
                        "Resign"+Environment.NewLine+"Date",
                        "Type",
                        "Period",
                        "Group",
                        "Site",
                        //16-20
                        "NPWP",
                        "PTKP",
                        "Grade"+Environment.NewLine+"Level",
                        "Salary", 
                        "Working Day", 
                        //21-25
                        "Paid Leave"+Environment.NewLine+"(Day)", 
                        "Paid Leave"+Environment.NewLine+"(Hour)",  
                        "Paid Leave"+Environment.NewLine+"(Amount)", 
                        "Processed Paid"+Environment.NewLine+"Leave (Amount)", 
                        "Unpaid Leave"+Environment.NewLine+"(Day)", 
                        //26-30
                        "Unpaid Leave"+Environment.NewLine+"(Hour)",  
                        "Unpaid Leave"+Environment.NewLine+"(Amount)", 
                        "Processed Unpaid"+Environment.NewLine+"Leave (Amount)", 
                        "OT 1"+Environment.NewLine+"(Hour)", 
                        "OT 2"+Environment.NewLine+"(Hour)", 
                        //31-35
                        "OT Holiday"+Environment.NewLine+"(Hour)", 
                        "OT 1"+Environment.NewLine+"(Amount)", 
                        "OT 2"+Environment.NewLine+"(Amount)", 
                        "OT Holiday"+Environment.NewLine+"(Amount)", 
                        "Taxable Fixed"+Environment.NewLine+"Allowance", 
                        //36-40
                        "Non Taxable Fixed"+Environment.NewLine+"Allowance",
                        Sm.GetValue("Select ADName From TblAllowanceDeduction Where AdCode = '001';").Replace(" ", Environment.NewLine),
                        Sm.GetValue("Select ADName From TblAllowanceDeduction Where AdCode = '004';").Replace(" ", Environment.NewLine),
                        "Other"+Environment.NewLine+"Allowance", 
                        "",
                        //41-45
                        "Performance"+Environment.NewLine+"Allowance", 
                        "OT"+Environment.NewLine+"Allowance",
                        "",
                        "Meal", 
                        "Transport", 
                        //46-50
                        "Variable",
                        "Tax"+Environment.NewLine+"Allowance",
                        "SS Employer"+Environment.NewLine+"Health", 
                        "SS Employee"+Environment.NewLine+"Health", 
                        "Severance"+Environment.NewLine+"Reserve",
                        //51-55
                        "Additional"+Environment.NewLine+"Severance Reserve",
                        "SS Employer"+Environment.NewLine+"Employment",
                        "SS Employee"+Environment.NewLine+"Employment", 
                        "SS Employer"+Environment.NewLine+"Pension",
                        "SS Employee"+Environment.NewLine+"Pension",
                        //56-60
                        "SS Employer"+Environment.NewLine+"Jiwasraya",
                        "SS Employee"+Environment.NewLine+"Jiwasraya",
                        "Non Taxable Employee's"+Environment.NewLine+"Deduction",
                        "Taxable Employee's"+Environment.NewLine+"Deduction", 
                        "SS Employer"+Environment.NewLine+"DPLK",
                        //61-65
                        "SS Employee"+Environment.NewLine+"DPLK",
                        "Fixed"+Environment.NewLine+"Deduction", 
                        "Employee's"+Environment.NewLine+"Loan",
                        "",
                        "Salary"+Environment.NewLine+"Adjustment",
                        //66- 70
                        "Salary Before"+Environment.NewLine+"Adjustment",
                        "Brutto",
                        "Tax", 
                        "End of Year"+Environment.NewLine+"Tax", 
                        "Transfer", 
                        //71-75
                        "Voucher Request#"+Environment.NewLine+"(Payroll)" ,
                        "Over"+Environment.NewLine+"Hour",
                        "Over Hour"+Environment.NewLine+"Amount",
                        "Attendance"+Environment.NewLine+"Allowance",
                        "Shift"+Environment.NewLine+"Allowance",
                        //76-80
                        "Net"+Environment.NewLine+"Income",
                        "Incentive"+Environment.NewLine+"Employee",
                        "",
                        "Shift"+Environment.NewLine+"Allowance",
                        "UTD",
                        //81
                        "Annual"+Environment.NewLine+"Leave"+Environment.NewLine+"Allowance",
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        20, 100, 150, 80, 180,                      
                        //6-10
                        80, 150, 80, 150, 80,                         
                        //11-15
                        80, 100, 100, 150, 130,                         
                        //16-20
                        100, 100, 100, 100, 80,                        
                        //21-25
                        100, 100, 100, 100, 100,                        
                        //26-30
                        100, 100, 100, 100, 100, 
                        //31-35
                        100, 100, 100, 100, 130, 
                        //36-40
                        130, 100, 100, 100, 20, 
                        //41-50
                        100, 100, 20, 100, 100, 
                        //46-50
                        100, 100, 100, 100, 100, 
                        //51-55
                        100, 120, 120, 100, 130,  
                        //56-60
                        100, 150, 150, 130, 100, 
                        //61-65
                        100, 130, 130, 20, 130,
                        //66-70
                        130, 130, 130, 130, 130, 
                        //71-75
                        130, 130, 130, 100, 100,
                        //76-80
                        100, 100, 20, 100, 100, 
                        //81
                        100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 40, 43, 64, 78 });
            Sm.GrdFormatDec(Grd1, new int[] { 
                19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34, 35, 36, 37, 38, 39, 41, 42, 44,
                45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
                57, 58, 59, 60, 61, 62, 63, 65, 66, 67, 68, 69,
                70, 72, 73, 74, 75, 76, 77, 79, 80, 81
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10, 11 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] 
            { 
                0, 
                2, 3, 4, 5, 6, 7, 8, 9, 10, 
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 36, 37, 39, 41, 42,  
                44, 45, 46, 47, 48, 49, 50, 51, 52, 
                53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 
                63, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 79, 80, 81
            });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 12, 14, 35, 36, 37, 38, 56, 57, 72, 73, 74, 75, 78  }, false);
            Grd1.Cols[76].Move(74);
            Grd1.Cols[64].Move(63);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.PayrunCode, C.EmpName;",
                        new string[]
                        {
                            //0
                            "PayrunCode",
                            //1-5
                            "PayrunName",
                            "EmpCode",
                            "EmpName",
                            "EmpCodeOld",
                            "PosName",
                            //6-10
                            "DeptCode",
                            "DeptName",
                            "JoinDt",
                            "ResignDt",
                            "SystemTypeDesc",
                            //11-15
                            "PayrunPeriodDesc",
                            "PGName",
                            "SiteName",
                            "NPWP",
                            "NonTaxableIncomeDesc",
                            //16-20
                            "GrdLvlname",
                            "Salary", 
                            "WorkingDay", 
                            "PLDay", 
                            "PLHr",
                            //21-25 
                            "PLAmt",
                            "ProcessPLAmt",
                            "UPLDay",
                            "UPLHr", 
                            "UPLAmt",
                            //26-30 
                            "ProcessUPLAmt",
                            "OT1Hr", 
                            "OT2Hr", 
                            "OTHolidayHr", 
                            "OT1Amt",
                            //31-35 
                            "OT2Amt",
                            "OTHolidayAmt", 
                            "TaxableFixAllowance",
                            "NonTaxableFixAllowance",
                            "Al001",
                            //36-40
                            "Al004",
                            "FixAllowance", 
                            "PerformanceValue", 
                            "ADOT",
                            "Meal",
                            //41-45
                            "Transport",
                            "VariableAllowance",
                            "TaxAllowance",
                            "SSEmployerHealth", 
                            "SSEmployeeHealth", 
                            //46-50
                            "SeveranceReserve",
                            "AddSeveranceReserve",
                            "SSEmployerEmployment", 
                            "SSEmployeeEmployment",
                            "SSErPension", 
                            //51-55
                            "SSEePension", 
                            "SSEmployerPension",
                            "SSEmployeePension", 
                            "NonTaxableFixDeduction",
                            "TaxableFixDeduction",
                            //56-60
                            "SSErDPLK",
                            "SSEeDPLK", 
                            "FixDeduction", 
                            "EmpAdvancePayment",
                            "SalaryAdjustment",
                            //61-65 
                            "Brutto",
                            "Tax", 
                            "EOYTax",
                            "Amt",
                            "VoucherRequestPayrollDocNo",
                            //66-70
                            "OverHr",
                            "OverHrAmt",
                            "AtdPremi",
                            "ShiftPremi",
                            "NetIncome",
                            //71
                            "IncEmployee", 
                            "AmtInsPntSA", 
                            "AmtInsPntUTD",
                            "ADLeave"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) => 
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 34);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 35);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 36);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 37);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 38);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 39);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 40);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 41);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 42);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 43);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 44);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 49, 45);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 50, 46);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 51, 47);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 52, 48);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 49);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 54, 50);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 55, 51);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 56, 52);//sseejs
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 57, 53);//SSerjs
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 58, 54);//NTD
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 59, 55);//TTD
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 60, 56);//SSerDPLK
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 61, 57);//SSeeDPLK
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 62, 58);//fixd deduc

                            Sm.SetGrdValue("N", Grd, dr, c, Row, 63, 59);//loan
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 65, 60);//sal adjust
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 67, 61);//bruto
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 68, 62);//tax
                            if (Sm.GetGrdDec(Grd1, Row, 65) < 0)
                            {
                                Grd1.Cells[Row, 66].Value = Sm.GetGrdDec(Grd1, Row, 65) + Sm.GetGrdDec(Grd1, Row, 67);
                            }
                            else if (Sm.GetGrdDec(Grd1, Row, 69) > 0)
                            {
                                Grd1.Cells[Row, 66].Value = Sm.GetGrdDec(Grd1, Row, 67) - Sm.GetGrdDec(Grd1, Row, 65);
                            }
                            else
                            {
                                Grd1.Cells[Row, 66].Value = 0m;
                            }
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 69, 63);//eoyt
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 70, 64);//thp
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 71, 65);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 72, 66);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 73, 67);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 74, 68);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 75, 69);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 76, 70);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 77, 71);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 79, 72);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 80, 73);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 81, 74);
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                    { 
                        19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34, 35, 36, 37, 38, 39, 41, 42, 44,
                45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
                57, 58, 59, 60, 61, 62, 63, 65, 66, 67, 68, 69,
                70, 72, 73, 74, 75, 76, 77, 79, 80, 81
                    });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    Sm.FormShowDialog(
                        new FrmRptPayrollProcessSummary8Dlg(
                            this,
                            Sm.GetGrdStr(Grd1, r, 2),
                            Sm.GetGrdStr(Grd1, r, 4)
                            ));
                }
            }
            if (e.ColIndex == 43 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r,42) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessADOT(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 40 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 39) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 64 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 62) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 78 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 77) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessIncentif(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                Sm.FormShowDialog(
                    new FrmRptPayrollProcessSummary8Dlg(
                        this,
                        Sm.GetGrdStr(Grd1, r, 2),
                        Sm.GetGrdStr(Grd1, r, 4)
                        ));
            }
            if (e.ColIndex == 43 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 42) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessADOT(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 40 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 39) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 64 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 62) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 78 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 77) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                   ShowPayrollProcessIncentif(Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void ShowPayrollProcessAD(string ADType, string PayrunCode, string EmpCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            // OLD
            //SQL.AppendLine("Select D.ADName, Sum(C.Amt) As Amt ");
            //SQL.AppendLine("From TblPayrollProcess1 A ");
            //SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode ");
            //SQL.AppendLine("Inner Join TblEmployeeAllowanceDeduction C ");
            //SQL.AppendLine("    On A.EmpCode=C.EmpCode ");
            //SQL.AppendLine("    And ( ");
            //SQL.AppendLine("    (C.StartDt Is Null And C.EndDt Is Null) Or ");
            //SQL.AppendLine("    (C.StartDt Is Not Null And C.EndDt Is Null And C.StartDt<=B.EndDt) Or ");
            //SQL.AppendLine("    (C.StartDt Is Null And C.EndDt Is Not Null And B.EndDt<=C.EndDt) Or ");
            //SQL.AppendLine("    (C.StartDt Is Not Null And C.EndDt Is Not Null And C.StartDt<=B.EndDt And B.EndDt<=C.EndDt) ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("Inner Join TblAllowanceDeduction D On C.ADCode=D.ADCode And D.ADType=@ADType And D.AmtType='1' ");
            //SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            //SQL.AppendLine("And A.EmpCode=@EmpCode ");
            //SQL.AppendLine("Group By D.ADName ");
            //SQL.AppendLine("Having Sum(C.Amt)<>0.00 ");
            //SQL.AppendLine("Order By D.ADName;");

            SQL.AppendLine("Select A.PayrunCode, A.EMpCode, A.Adcode, D.ADName, Sum(A.Amt) As Amt  ");
            SQL.AppendLine("From TblPayrollProcessAD A  ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction D On A.ADCode=D.ADCode And D.ADType='A' And D.AmtType='1'  ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode  ");
            SQL.AppendLine("And A.EmpCode=@EmpCode  ");
            SQL.AppendLine("Group By A.PayrunCode, A.EmpCode, A.Adcode, D.ADName  ");
            SQL.AppendLine("Having Sum(A.Amt)<>0.00  ");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                    Sm.CmParam<String>(ref cm, "@ADType", ADType);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowPayrollProcessIncentif(string PayrunCode, string EmpCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select A.payrunCOde, A.EmpCode, D.InspntName, SUM(B.AmtInsPnt) As AmtInsPnt ");
            SQL.AppendLine("From tblpayrollProcess1 A ");
            SQL.AppendLine("Inner Join tblEmpInsPntDtl B On A.PayrunCode = B.PayrunCode And A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblEmpInsPnthdr C On B.DocNo = C.DocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join tblinspnt D On C.InsPntCode = D.InspntCode ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Group by A.payrunCOde, A.EmpCode, D.InspntName ");
            SQL.AppendLine("Having Sum(B.AmtInsPnt)<>0.00 ");
            SQL.AppendLine("Order By D.InspntName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "InspntName", "AmtInsPnt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return true;
            }
            return false;
        }

        override protected void PrintData()
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            if (IsGrdEmpty() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<PayrollProcess>();
            var ldtl = new List<PayrollProcessDtl>();
            string[] TableName = { "PayrollProcess", "PayrollProcessDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where Menucode=@MenuCode) As MenuDesc ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                        "CompanyLogo",

                         //1-5
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyPhone",
                        "MenuDesc",

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PayrollProcess()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            MenuDesc = Sm.DrStr(dr, c[4]),
                            UserName = Gv.CurrentUserCode,
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.PayrunCode, B.PayrunName, C.EmpName, C.EmpCodeOld, D.DeptName, A.SSEmployerHealth, A.SSEmployerEmployment, A.SSEmployeeHealth, ");
                SQLDtl.AppendLine("A.SSEmployeeEmployment, A.Amt, (A.SSEmployerHealth+A.SSEmployerEmployment+A.SSEmployeeHealth+A.SSEmployeeEmployment+A.Amt)As TotalTHPBPJS, ");
                SQLDtl.AppendLine("(A.SSEmployerEmployment+A.SSEmployeeEmployment)As TotBPJSKet ");
                SQLDtl.AppendLine("From tblpayrollprocess1 A ");
                SQLDtl.AppendLine("Inner Join TblPayrun B On A.PayrunCode = B.PayrunCode ");
                SQLDtl.AppendLine("Inner Join TblEmployee C On A.EmpCode = C.EmpCode ");
                SQLDtl.AppendLine("Left Join TblDepartment D On B.DeptCode = D.DeptCode ");
                SQLDtl.AppendLine("Where A.PayrunCode in ( ");

                int x = Grd1.Rows.Count-1;
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (i == x)
                    {
                        SQLDtl.AppendLine("'" + Sm.GetGrdStr(Grd1, i, 2) + "'");

                    }
                    else
                    {
                        SQLDtl.AppendLine("'" + Sm.GetGrdStr(Grd1, i, 2) + "'");
                        SQLDtl.AppendLine(", ");
                    }
                }
                SQLDtl.AppendLine(" );");

                cmDtl.CommandText = SQLDtl.ToString();
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "PayrunCode",

                    //1-5
                    "PayrunName",
                    "EmpName",
                    "EmpCodeOld",
                    "DeptName",
                    "SSEmployerHealth",

                    //6-10
                    "SSEmployerEmployment",
                    "SSEmployeeHealth",
                    "SSEmployeeEmployment",
                    "Amt",
                    "TotalTHPBPJS",

                    //11
                    "TotBPJSKet",
                   
                });

                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new PayrollProcessDtl()
                        {
                            nomor = nomor,
                            PayrunCode = Sm.DrStr(drDtl, cDtl[0]),

                            PayrunName = Sm.DrStr(drDtl, cDtl[1]),
                            EmpName = Sm.DrStr(drDtl, cDtl[2]),
                            EmpCodeOld = Sm.DrStr(drDtl, cDtl[3]),
                            DeptName = Sm.DrStr(drDtl, cDtl[4]),
                            SSEmployerHealth = Sm.DrDec(drDtl, cDtl[5]),

                            SSEmployerEmployment = Sm.DrDec(drDtl, cDtl[6]),
                            SSEmployeeHealth = Sm.DrDec(drDtl, cDtl[7]),
                            SSEmployeeEmployment = Sm.DrDec(drDtl, cDtl[8]),
                            Amt = Sm.DrDec(drDtl, cDtl[9]),
                            TotalTHPBPJS = Sm.DrDec(drDtl, cDtl[10]),

                            TotBPJSKet = Sm.DrDec(drDtl, cDtl[11]),
                        });
                    }
                }

                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            Sm.PrintReport("PayrollProcess", myLists, TableName, true);
        }

        private void ShowPayrollProcessADOT(string PayrunCode, string EmpCode)
        {
            StringBuilder
                SQL = new StringBuilder(),
                Msg = new StringBuilder();

            SQL.AppendLine("Select B.ADName, Sum(A.Amt) As Amt, Sum(A.Duration) As Duration ");
            SQL.AppendLine("From TblPayrollProcessADOT A ");
            SQL.AppendLine("Left Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Group By B.ADName ");
            SQL.AppendLine("Order By B.ADName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt", "Duration" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append("Allowance : "); 
                            Msg.AppendLine(Sm.DrStr(dr, c[0]));
                            Msg.Append("Amount : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                            Msg.Append("Duration : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[2]), 2));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPayCod_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtPayCod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayCod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        #endregion

        #endregion

        #region Report Class

        class PayrollProcess
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string MenuDesc { get; set; }
            public string UserName { get; set; }
            public string PrintBy { get; set; }

        }

        class PayrollProcessDtl
        {
            public int nomor { get; set; }
            public string PayrunCode { get; set; }
            public string PayrunName { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public decimal TotalTHPBPJS { get; set; }
            public decimal TotBPJSKet { get; set; }
        }

        #endregion
    }
}
