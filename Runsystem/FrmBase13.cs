﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace

using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmBase13 : Form
    {
        #region Constructor

        public FrmBase13()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
            Sm.SetLue(LueFontSize, "9");
        }

        virtual protected void SetSQL()
        {
        }

        virtual protected string SetReportName()
        {
            return "";
        }

        virtual protected void ShowData()
        {

        }

        virtual protected void HideInfoInGrd()
        {

        }

        virtual protected void ExportToExcel()
        {

        }


        virtual protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
           
        }

        #endregion

        #region Event

        #region Form Event

        private void FrmBase13_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        #endregion

        #region Misc Control event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            LueFontSizeEditValueChanged(sender, e);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        #endregion

        #region Print Manager Event

        private void PM1_CustomDrawPageHeader(object sender, TenTec.Windows.iGridLib.Printing.iGCustomDrawPageBandEventArgs e)
        {
            try
            {
                e.Graphics.DrawImage(
                    Image.FromFile(@Sm.CompanyLogo()),
                    //new Rectangle() { Height = 50, Width = Gv.CompanyLogoWidth, X = 100, Y = 50 }
                     new Rectangle() { Height = 50, Width = Gv.CompanyLogoWidth, X = 78, Y = 78 }
                    );

                e.Graphics.DrawString(
                    Gv.CompanyName,
                    new Font("Times New Roman", 12, FontStyle.Regular),
                    SystemBrushes.WindowText,
                    //new Rectangle() { Height = 100, Width = 300, X = 350, Y = 50 });
                    new Rectangle() { Height = 100, Width = 300, X = Gv.CompanyLogoWidth + 85, Y = 90 });
            }
            catch (FileNotFoundException)
            {

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void PM1_CustomDrawPageHeaderGetBounds(object sender, TenTec.Windows.iGridLib.Printing.iGCustomDrawPageBandGetBoundsEventArgs e)
        {
            e.Bounds.Height = 20;
        }

        #endregion

       
        #endregion


    }
}
