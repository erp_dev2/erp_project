﻿#region Update
/*
    13/09/2019 [WED] tambah tarik data dari Dropping Request
    23/09/2019 [WED] tambah informasi Dropping Payment Amount
    28/01/2020 [WED/YK] item nya khusus service dan lihat ke Cost center, jika mMenuCodeForRecvVd2AutoDO true
    18/03/2020 [IBL/MMM] tambah keterangan vendor name pada kolom batch#
    20/03/2020 [HAR/KBN] tambah filter item category 
    06/07/2020 [TKG/IMS] tambah specification dan item local code
    13/07/2020 [TKG/IMS] filter by item's local code
 *  29/09/2020 [HAR/IMS] jika mMenuCodeForRecvVd2AutoDO false item nya yang non service
 *  07/12/2021 [VIN/VIR] BUG saat PRJI DRQ kosong harusnya ngga nyambung ke PRJI
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVd2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvVd2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvVd2Dlg(FrmRecvVd2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ItCodeInternal, A.Specification, B.ItCtName, A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3, A.ItScCode, C.ItScName, A.ItGrpCode, A.ForeignName ");
            
            if (mFrmParent.TxtDR_PRJIDocNo.Text.Length > 0)
                SQL.AppendLine(", T2.VdCode, T4.VdName, T2.Qty, (T2.RemunerationAmt + T2.DirectCostAmt) Amt, T5.Amt PaymentAmt ");
            else if (mFrmParent.mDroppingRequestBCCode.Length > 0)
                SQL.AppendLine(", T2.VdCode, T3.VdName, T2.Qty, T2.Amt, T4.Amt PaymentAmt ");
            else
                SQL.AppendLine(", null As VdCode, null AS VdName, 0.00 As Qty, 0.00 As Amt, 0.00 PaymentAmt ");

            if (mFrmParent.mMenuCodeForRecvVd2AutoCreateDO)
                SQL.AppendLine(", B1.CCtCode, B2.CCtName, B.AcNo ");
            else
                SQL.AppendLine(", null As CCtCode, null as CCtName, null As AcNo ");

            if (mFrmParent.TxtDR_PRJIDocNo.Text.Length > 0)//disambungin ke prji kalau prji nya keiisi aja
                SQL.AppendLine(", T9.LOPDocNo ");
            else
                SQL.AppendLine(", null as LOPDocNo ");
            
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            if (mFrmParent.mMenuCodeForRecvVd2AutoCreateDO)
            {
                SQL.AppendLine("    And A.ServiceItemInd = 'Y' ");
                SQL.AppendLine("Left Join TblItemCostCategory B1 On A.ItCode=B1.ItCode And B1.CCCode=@CCCode ");
                SQL.AppendLine("Left Join TblCostCategory B2 On B1.CCCode=B2.CCCode And B1.CCtCode=B2.CCtCode ");
            }
            else
            {
                SQL.AppendLine("    And A.ServiceItemInd = 'N' ");
            }
            SQL.AppendLine("Left Join TblItemSubcategory C On A.ItScCode = C.ItScCode ");
            if (mFrmParent.TxtDR_PRJIDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Inner Join TblDroppingRequestHdr T1 On T1.DocNo = @DroppingRequestDocNo ");
                SQL.AppendLine("    And T1.PRJIDocNo Is Not Null ");
                SQL.AppendLine("    And T1.PRJIDocNo=@PRJIDocNo ");
                SQL.AppendLine("Inner Join TblDroppingRequestDtl T2 On T1.DocNo=T2.DocNo And T2.MRDocNo Is Null ");
                SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr T3 On T2.PRBPDocNo=T3.DocNo And T3.ResourceItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblVendor T4 On T2.VdCode = T4.VdCode ");
                SQL.AppendLine("Left Join TblDroppingPaymentDtl T5 On T2.DocNo = T5.DRQDocNo And T2.DNo = T5.DRQDNo ");
                SQL.AppendLine("    And T5.DocNo In (Select DocNo From TblDroppingPaymentHdr Where CancelInd = 'N' And Status In ('O', 'A')) ");
                SQL.AppendLine("Inner Join TblProjectImplementationHdr T6 On T1.PRJIDocNo = T6.DocNo ");
                SQL.AppendLine("    And T6.CancelInd = 'N' ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr T7 On T6.SOContractDocNo = T7.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr T8 On T7.SOCDocNo = T8.DocNo ");
                SQL.AppendLine("    And T8.CancelInd = 'N' ");
                SQL.AppendLine("Inner JOin TblBOQHDr T9 On T8.BOQDocNo = T9.DocNo And T9.ActInd = 'Y' ");
            }

            if (mFrmParent.mDroppingRequestBCCode.Length > 0)
            {
                SQL.AppendLine("Inner Join TblDroppingRequestHdr T1 On T1.DocNo = @DroppingRequestDocNo ");
                SQL.AppendLine("    And T1.PRJIDocNo Is Null ");
                SQL.AppendLine("Inner Join TblDroppingRequestDtl2 T2 On T1.DocNo=T2.DocNo And T2.BCCode=@DroppingRequestBCCode And T2.MRDocNo Is Null And T2.Qty>0.00 ");
                SQL.AppendLine("    And T2.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblVendor T3 On T2.VdCode = T3.VdCode ");
                SQL.AppendLine("Left Join TblDroppingPaymentDtl T4 On T2.DocNo = T4.DRQDocNo And T2.DNo = T4.DRQDNo ");
                SQL.AppendLine("    And T4.DocNo In (Select DocNo From TblDroppingPaymentHdr Where CancelInd = 'N' And Status In ('O', 'A')) ");
                //SQL.AppendLine("Inner Join TblProjectImplementationHdr T6 On T1.PRJIDocNo = T6.DocNo ");
                //SQL.AppendLine("    And T6.CancelInd = 'N' ");
                //SQL.AppendLine("Inner Join TblSOContractRevisionHdr T7 On T6.SOContractDocNo = T7.DocNo ");
                //SQL.AppendLine("Inner Join TblSOContractHdr T8 On T7.SOCDocNo = T8.DocNo ");
                //SQL.AppendLine("    And T8.CancelInd = 'N' ");
                //SQL.AppendLine("Inner JOin TblBOQHDr T9 On T8.BOQDocNo = T9.DocNo And T9.ActInd = 'Y' ");
            }
            
            SQL.AppendLine("Where A.ActInd='Y' ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",
                    //1-5
                    "", 
                    "Item's"+Environment.NewLine+"Code", 
                    "", 
                    "Item's Name", 
                    "Group",
                    //6-10
                    "Foreign"+Environment.NewLine+"Name",
                    "ItScCode",
                    "Sub-Category",
                    "Item's"+Environment.NewLine+"Category",
                    "UoM ",
                    //11-15
                    "UoM",
                    "UoM",
                    "Dropping Request Vendor Code",
                    "Dropping Request"+Environment.NewLine+"Quantity",
                    "Dropping Request"+Environment.NewLine+"Amount",
                    //16-20
                    "Dropping Request"+Environment.NewLine+"Vendor",
                    "Dropping Payment"+Environment.NewLine+"Amount",
                    "Cost Category Code",
                    "Cost Category Name",
                    "LOP#",

                    //21-23
                    "Inventory COA#",
                    "Local Code",
                    "Specification"
                }
                ,new int[] 
                {
                    //0
                    50,
                    //1-5
                    20, 100, 20, 200, 180,
                    //6-10
                    200, 100, 200, 200, 100,
                    //11-15
                    100, 100, 0, 120, 120,
                    //16-20
                    180, 180, 200, 200, 200, 
                    //21-23
                    200, 130, 200
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            if(mFrmParent.TxtDroppingRequestDocNo.Text.Length == 0)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 17 });
            if (Sm.GetParameter("ProcFormatDocNo") == "0")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 7, 8, 11, 12 }, !ChkHideInfoInGrd.Checked);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 7, 11, 12  }, !ChkHideInfoInGrd.Checked);
            }
            if (!mFrmParent.mMenuCodeForRecvVd2AutoCreateDO) Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21 });
            if (mFrmParent.mIsItGrpCodeShow) Grd1.Cols[5].Visible = true;
            if (!mFrmParent.mIsShowForeignName) Grd1.Cols[6].Visible = true;
            if (!mFrmParent.mIsRecvVd2ShowSpecificationEnabled) Grd1.Cols[23].Visible = false;
            Grd1.Cols[22].Move(5);
            Grd1.Cols[23].Move(6);
            ShowInventoryUomCode();

            
            
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 10, 11 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                if (mFrmParent.mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", mFrmParent.TxtDroppingRequestDocNo.Text);
                Sm.CmParam<String>(ref cm, "@PRJIDocNo", mFrmParent.TxtDR_PRJIDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mFrmParent.mDroppingRequestBCCode);
                if (mFrmParent.mMenuCodeForRecvVd2AutoCreateDO) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(mFrmParent.LueCCCode));
                
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName", "A.ForeignName", "A.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By A.ItCode;",
                    new string[] 
                    { 
                        //0
                        "ItCode", 
                        //1-5
                        "ItName", "ItGrpCode", "ForeignName", "ItScCode", "ItScName",
                        //6-10
                        "ItCtName", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3", "VdCode",
                        //11-15
                        "Qty", "Amt", "VdName", "PaymentAmt", "CCtCode", 
                        //16-20
                        "CCtName", "LOPDocNo", "AcNo", "ItCodeInternal", "Specification"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                        if (mFrmParent.mMenuCodeForRecvVd2AutoCreateDO)
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        }
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 14);
                        if (mFrmParent.mMenuCodeForRecvVd2AutoCreateDO)
                        {
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 18);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 19);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 20);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 21);
                        }
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 39, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 40, Grd1, Row2, 23);
                        mFrmParent.Grd1.Cells[Row1, 27].Value = Sm.GetGrdDec(Grd1, Row2, 14) * Sm.GetGrdDec(Grd1, Row2, 15);

                        mFrmParent.Grd1.Cells[Row1, 0].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 1].Value = false;
                        mFrmParent.Grd1.Cells[Row1, 2].Value = false;
                        mFrmParent.Grd1.Cells[Row1, 14].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 15].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 16].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 17].Value = null;
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 21, 23, 25, 26 });
                        mFrmParent.Grd1.Cells[Row1, 28].Value = null;
                        mFrmParent.ProcessConvertUom(Row1);
                        mFrmParent.ComputeTotalQty();
                        mFrmParent.GenerateBatchNo(Row1);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 18, 19, 21, 23, 25, 26, 27, 29, 30, 33 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
