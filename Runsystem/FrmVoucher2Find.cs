﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVoucher2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmVoucher2 mFrmParent;
        private string mSQL = string.Empty, mMInd = "N";

        #endregion

        #region Constructor

        public FrmVoucher2Find(FrmVoucher2 FrmParent, string MInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMInd = MInd;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueVoucherDocType(ref LueDocType);
                Sl.SetLueVdCode(ref LueVdCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                ChkExcludeCancelledVoucher.Checked = true;
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.LocalDocNo, B.OptDesc As DocTypeDesc, A.VoucherRequestDocNo, ");
            SQL.AppendLine("IfNull(IfNull(J.VdCode, IfNull(N.VdCode, R.VdCode)), F.VdCode) As VdCode, ");
            SQL.AppendLine("IfNull(IfNull(J.VdName, IfNull(N.VdName, R.VdName)), F.VdName) As VdName, ");
            SQL.AppendLine("IfNull(IfNull(I.QueueNo, M.QueueNo), Q.QueueNo) As QueueNo, ");
            SQL.AppendLine("S.OptDesc As PaymentType, A.Amt, ");
            SQL.AppendLine("A.CreateBy, A.Createdt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode and B.OptCat='VoucherDocType' ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblOutgoingPaymentHdr D On C.DocNo=D.VoucherRequestDocNo ");
            SQL.AppendLine("Left Join TblVendor F On D.VdCode=F.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr G On C.DocNo=G.VoucherRequestDocNo ");
            SQL.AppendLine("Left Join TblRawMaterialVerify H On G.RawMaterialVerifyDocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr I On H.LegalDocVerifyDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblVendor J On I.VdCode=J.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr K On C.DocNo=K.VoucherRequestDocNo2 And K.VoucherRequestDocNo2 Is Not Null ");
            SQL.AppendLine("Left Join TblRawMaterialVerify L On K.RawMaterialVerifyDocNo=L.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr M On L.LegalDocVerifyDocNo=M.DocNo ");
            SQL.AppendLine("Left Join TblVendor N On M.VdCode=N.VdCode ");

            SQL.AppendLine("Left Join TblPurchaseInvoiceRawMaterialHdr O On C.DocNo=O.VoucherRequestDocNo3 And O.VoucherRequestDocNo3 Is Not Null ");
            SQL.AppendLine("Left Join TblRawMaterialVerify P On O.RawMaterialVerifyDocNo=P.DocNo ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr Q On P.LegalDocVerifyDocNo=Q.DocNo ");
            SQL.AppendLine("Left Join TblVendor R On Q.VdCode=R.VdCode ");

            SQL.AppendLine("Left Join TblOption S On A.PaymentType=S.OptCode And S.OptCat='VoucherPaymentType' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Voucher#", 
                        "Date",
                        "Cancel",
                        "Local#",
                        "Type",

                        //6-10
                        "Vendor",
                        "Queue#",
                        "Payment",
                        "Amount",
                        "Created"+Environment.NewLine+"By",
                        
                        //11-15
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 80, 130, 200, 
                        
                        //6-10
                        250, 130, 180, 130, 100, 
                        
                        //11-15
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string 
                    SQL = string.Empty,
                    Filter1 = mFrmParent.mIsUseMInd ? " Where A.MInd=@MInd " : " Where 0=0 ", 
                    Filter2 = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@MInd", mMInd);
                if (ChkExcludeCancelledVoucher.Checked) Filter1 += " And A.CancelInd='N' ";
                Sm.FilterStr(ref Filter1, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueDocType), "A.DocType", true);
                Sm.FilterStr(ref Filter1, ref cm, TxtQueueNo.Text, "I.QueueNo", false);
                Sm.FilterDt(ref Filter1, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                SQL = "Select * From ( " + mSQL + Filter1 + ") T ";

                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueVdCode), "T.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL + Filter2 + " Order By T.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "LocalDocNo", "DocTypeDesc", "VdName", 

                            //6-10
                            "QueueNo", "PaymentType", "Amt", "CreateBy", "CreateDt", 
                            
                            //11-12
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueVoucherDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtQueueNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkQueueNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Queue#");
        }

        #endregion

        #endregion
    }
}
