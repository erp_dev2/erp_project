﻿#region Update
/*
    03/06/2020 [WED/SRN] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSSDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSS mFrmParent;
        private byte mType = 0;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSSDlg(FrmSS FrmParent, byte Type)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mType = Type;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No.", 

                    //1-4
                    "COA#",
                    "Account",
                    "Alias",
                    "Type", 
                },
                new int[] 
                {
                    //0
                    40, 

                    //1-4
                    180, 200, 120, 100
                }
            );
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.Alias, A.AcDesc, B.OptDesc As AcType ");
            SQL.AppendLine("From TblCoa A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            SQL.AppendLine("where A.AcNo Not In (Select parent From TblCoa Where parent is not null) ");
            SQL.AppendLine("And A.ActInd='Y' ");
            if (mFrmParent.mIsCOAFilteredByGroup)
            {
                SQL.AppendLine("    And Exists ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblGroupCOA ");
                SQL.AppendLine("        Where GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
                //SQL.AppendLine("        And AcNo = A.AcNo ");
                SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);
                var cm = new MySqlCommand();

                string Filter = " And 0 = 0 ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "A.AcNo", "A.Alias", "A.AcDesc" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.AcNo;",
                    new string[] 
                    { 
                        //0
                        "AcNo",

                        //1-5
                        "AcDesc", "Alias", "AcType"            
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                if (mType == 1)
                {
                    mFrmParent.TxtAcNo1.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                    mFrmParent.TxtAcDesc1.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                }
                else
                {
                    mFrmParent.TxtAcNo2.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                    mFrmParent.TxtAcDesc2.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                }

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        #endregion

        #endregion

    }
}
