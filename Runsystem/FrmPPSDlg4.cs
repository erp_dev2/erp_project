﻿#region update 
   /* 19/02/2021 [RDH/PHT] tambah info download SK yang sudah diupload sebelumnya
    * 
    */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using System.Net;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmPPSDlg4 : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        internal string mDocNo = string.Empty;
        internal string 
            mHostAddrForFTPClient, 
            mPortForFTPClient, 
            mUsernameForFTPClient,
            mPasswordForFTPClient, 
            mSharedFolderForFTPClient = string.Empty;

        private byte[] downloadedData;


        #endregion

        #region Constructor

        public FrmPPSDlg4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method
        protected override void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Attachment SK";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.SetControlReadOnly(new List<BaseEdit> { TxtFile, TxtFile2, TxtFile3 },true);
                base.FrmLoad(sender, e);
                ShowData(mDocNo);
                BtnProcess.Visible = false;

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<BaseEdit>
            { TxtFile, TxtFile2, TxtFile3 });
        }
        
        #endregion

        #region ShowData
        
        private void ShowData(string mDocNo)
        {
            try
            {
                ClearData();
                ShowAttachmentSK();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowAttachmentSK()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@docNo", mDocNo);

            SQL.AppendLine("SELECT A.FileName, A.FileName2, A.FileName3 ");
            SQL.AppendLine("FROM tblpps A ");
            SQL.AppendLine("WHERE A.DocNo = @docNo");

            Sm.ShowDataInCtrl(ref cm, SQL.ToString(), new string[]{
                //0
                "FileName",
                //1-2
                "FileName2", "FileName3"
            }, (MySqlDataReader dr, int[] c) =>
            {
                TxtFile.EditValue = Sm.DrStr(dr, c[0]);
                TxtFile2.EditValue = Sm.DrStr(dr, c[1]);
                TxtFile3.EditValue = Sm.DrStr(dr, c[2]);

            },true);

            

        }

        
        #endregion

        #region button method
        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }
        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }
        #endregion

        #region method download
        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                //FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, mFrmParent.mSharedFolderForFTPClient));
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                //FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, mFrmParent.mSharedFolderForFTPClient));
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        #endregion
    }
}
