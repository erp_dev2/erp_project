﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmProductionOrderRevisionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProductionOrderRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProductionOrderRevisionFind(FrmProductionOrderRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT a.DocNo, a.DocDt,  ");
            SQL.AppendLine("CASE a.`Status`  ");
            SQL.AppendLine("  WHEN 'O' THEN 'Outstanding' ");
            SQL.AppendLine("  WHEN 'A' THEN 'Approved' ");
            SQL.AppendLine("  WHEN 'C' THEN 'Cancelled' ");
            SQL.AppendLine("END AS `Status`, ");
            SQL.AppendLine("a.ProductionOrderDocNo, b.ItCode, c.ItName, a.QtyOld, a.Qty, c.PlanningUomCode, a.Remark, ");
            SQL.AppendLine("a.CreateBy, a.CreateDt, a.LastUpBy, a.LastUpDt ");
            SQL.AppendLine("FROM TblProductionOrderRevision a ");
            SQL.AppendLine("INNER JOIN TblProductionOrderHdr b ON a.ProductionOrderDocNo = b.DocNo ");
            SQL.AppendLine("INNER JOIN TblItem c ON b.ItCode = c.ItCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Production Order#",
                        "Item's Code",
                        
                        //6-10
                        "Item's Name",
                        "Quantity (Old)", 
                        "Quantity (New)",
                        "UOM",
                        "Remark",

                        //11-15
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        
                        //16
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 100, 180, 100, 
                        
                        //6-10
                        150, 100, 100, 100, 200,  
                        
                        //11-15
                        100, 100, 100, 100, 100,

                        //16
                        100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 11, 12, 13, 14, 15, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " WHERE 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "a.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "a.DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ORDER BY a.DocDt, a.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "Status", "ProductionOrderDocNo", "ItCode", "ItName",  
                            
                            //6-10
                            "QtyOld", "Qty", "PlanningUomCode", "Remark", "CreateBy",   

                            //11-13
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Events

            #region Misc Control Events

            private void TxtDocNo_Validated(object sender, EventArgs e)
            {
                Sm.FilterTxtSetCheckEdit(this, sender);
            }

            private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
            {
                Sm.FilterSetTextEdit(this, sender, "Document#");
            }

            private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
            {
                Sm.FilterDteSetCheckEdit(this, sender);
            }

            private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
            {
                Sm.FilterDteSetCheckEdit(this, sender);
            }

            private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
            {
                Sm.FilterSetDateEdit(this, sender, "Date");
            }

            #endregion

        #endregion
    }
}
