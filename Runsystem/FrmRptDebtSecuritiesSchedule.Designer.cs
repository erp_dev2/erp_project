﻿namespace RunSystem
{
    partial class FrmRptDebtSecuritiesSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptDebtSecuritiesSchedule));
            this.panel4 = new System.Windows.Forms.Panel();
            this.Tc1 = new System.Windows.Forms.TabControl();
            this.Tp1 = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.BtnInvestmentCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAccruedInterest = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.DteNextCouponDt = new DevExpress.XtraEditors.DateEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.DteLastCouponDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.DteMaturityDt = new DevExpress.XtraEditors.DateEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.DteLastSettlementDt = new DevExpress.XtraEditors.DateEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.DteLastTradeDt = new DevExpress.XtraEditors.DateEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.DteListingDt = new DevExpress.XtraEditors.DateEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtInterestFrequency = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtAnnualDaysAssumption = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCouponInterestRate = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtInvestmentCost = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtMovingAvgPrice = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtNominalAmt = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtInvestmentCategory = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueInvestmentType = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtInvestmentName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtInvestmentCode = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.DteValuationDt = new DevExpress.XtraEditors.DateEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.LueFinancialInstitution = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkFinancialInstitution = new DevExpress.XtraEditors.CheckEdit();
            this.ChkBankAcCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccruedInterest.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastSettlementDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastSettlementDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastTradeDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastTradeDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAnnualDaysAssumption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCouponInterestRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMovingAvgPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNominalAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteValuationDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteValuationDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFinancialInstitution.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFinancialInstitution.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBankAcCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(988, 0);
            this.panel1.Size = new System.Drawing.Size(70, 509);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkBankAcCode);
            this.panel2.Controls.Add(this.ChkFinancialInstitution);
            this.panel2.Controls.Add(this.LueBankAcCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueFinancialInstitution);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.DteValuationDt);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Size = new System.Drawing.Size(988, 73);
            this.panel2.TabIndex = 16;
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(988, 436);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 487);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(0, 73);
            this.panel3.Size = new System.Drawing.Size(988, 436);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.panel4, 0);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.Tc1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(988, 436);
            this.panel4.TabIndex = 10;
            // 
            // Tc1
            // 
            this.Tc1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.Tc1.Controls.Add(this.Tp1);
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedIndex = 0;
            this.Tc1.Size = new System.Drawing.Size(988, 436);
            this.Tc1.TabIndex = 29;
            // 
            // Tp1
            // 
            this.Tp1.Controls.Add(this.Grd2);
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Location = new System.Drawing.Point(4, 28);
            this.Tp1.Name = "Tp1";
            this.Tp1.Padding = new System.Windows.Forms.Padding(3);
            this.Tp1.Size = new System.Drawing.Size(980, 404);
            this.Tp1.TabIndex = 0;
            this.Tp1.Text = "Investment Details";
            this.Tp1.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.ForeColorDisabled = System.Drawing.SystemColors.Control;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.GroupBox.Visible = true;
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 19;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(434, 3);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowMode = true;
            this.Grd2.RowModeHasCurCell = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(543, 398);
            this.Grd2.TabIndex = 55;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd2.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.BtnInvestmentCode);
            this.panel5.Controls.Add(this.TxtAccruedInterest);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.DteNextCouponDt);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.DteLastCouponDt);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.DteMaturityDt);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.DteLastSettlementDt);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.DteLastTradeDt);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.DteListingDt);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.TxtInterestFrequency);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.TxtAnnualDaysAssumption);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.TxtCouponInterestRate);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.TxtInvestmentCost);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.TxtMovingAvgPrice);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.TxtNominalAmt);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.TxtCurCode);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.TxtInvestmentCategory);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.LueInvestmentType);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.TxtInvestmentName);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.TxtInvestmentCode);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(431, 398);
            this.panel5.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(395, 177);
            this.label21.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 14);
            this.label21.TabIndex = 36;
            this.label21.Text = "%";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(395, 135);
            this.label20.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 14);
            this.label20.TabIndex = 31;
            this.label20.Text = "%";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnInvestmentCode
            // 
            this.BtnInvestmentCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnInvestmentCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInvestmentCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInvestmentCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInvestmentCode.Appearance.Options.UseBackColor = true;
            this.BtnInvestmentCode.Appearance.Options.UseFont = true;
            this.BtnInvestmentCode.Appearance.Options.UseForeColor = true;
            this.BtnInvestmentCode.Appearance.Options.UseTextOptions = true;
            this.BtnInvestmentCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInvestmentCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnInvestmentCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnInvestmentCode.Image")));
            this.BtnInvestmentCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnInvestmentCode.Location = new System.Drawing.Point(394, 5);
            this.BtnInvestmentCode.Name = "BtnInvestmentCode";
            this.BtnInvestmentCode.Size = new System.Drawing.Size(27, 21);
            this.BtnInvestmentCode.TabIndex = 18;
            this.BtnInvestmentCode.ToolTip = "Show Request for Loan to Partner";
            this.BtnInvestmentCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnInvestmentCode.ToolTipTitle = "Run System";
            this.BtnInvestmentCode.Click += new System.EventHandler(this.BtnInvestmentCode_Click);
            // 
            // TxtAccruedInterest
            // 
            this.TxtAccruedInterest.EnterMoveNextControl = true;
            this.TxtAccruedInterest.Location = new System.Drawing.Point(147, 363);
            this.TxtAccruedInterest.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAccruedInterest.Name = "TxtAccruedInterest";
            this.TxtAccruedInterest.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtAccruedInterest.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAccruedInterest.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAccruedInterest.Properties.Appearance.Options.UseFont = true;
            this.TxtAccruedInterest.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAccruedInterest.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAccruedInterest.Properties.MaxLength = 13;
            this.TxtAccruedInterest.Properties.ReadOnly = true;
            this.TxtAccruedInterest.Size = new System.Drawing.Size(246, 20);
            this.TxtAccruedInterest.TabIndex = 54;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(44, 366);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 14);
            this.label19.TabIndex = 53;
            this.label19.Text = "Accrued Interest";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteNextCouponDt
            // 
            this.DteNextCouponDt.EditValue = null;
            this.DteNextCouponDt.EnterMoveNextControl = true;
            this.DteNextCouponDt.Location = new System.Drawing.Point(147, 342);
            this.DteNextCouponDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteNextCouponDt.Name = "DteNextCouponDt";
            this.DteNextCouponDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteNextCouponDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteNextCouponDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteNextCouponDt.Properties.Appearance.Options.UseFont = true;
            this.DteNextCouponDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteNextCouponDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteNextCouponDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteNextCouponDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteNextCouponDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteNextCouponDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteNextCouponDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteNextCouponDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteNextCouponDt.Properties.ReadOnly = true;
            this.DteNextCouponDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteNextCouponDt.Size = new System.Drawing.Size(111, 20);
            this.DteNextCouponDt.TabIndex = 52;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(35, 345);
            this.label18.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 14);
            this.label18.TabIndex = 51;
            this.label18.Text = "Next Coupon Date";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteLastCouponDt
            // 
            this.DteLastCouponDt.EditValue = null;
            this.DteLastCouponDt.EnterMoveNextControl = true;
            this.DteLastCouponDt.Location = new System.Drawing.Point(147, 321);
            this.DteLastCouponDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLastCouponDt.Name = "DteLastCouponDt";
            this.DteLastCouponDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteLastCouponDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastCouponDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteLastCouponDt.Properties.Appearance.Options.UseFont = true;
            this.DteLastCouponDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastCouponDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLastCouponDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLastCouponDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLastCouponDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastCouponDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLastCouponDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastCouponDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLastCouponDt.Properties.ReadOnly = true;
            this.DteLastCouponDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLastCouponDt.Size = new System.Drawing.Size(111, 20);
            this.DteLastCouponDt.TabIndex = 50;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(39, 324);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 14);
            this.label17.TabIndex = 49;
            this.label17.Text = "Last Coupon Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteMaturityDt
            // 
            this.DteMaturityDt.EditValue = null;
            this.DteMaturityDt.EnterMoveNextControl = true;
            this.DteMaturityDt.Location = new System.Drawing.Point(147, 300);
            this.DteMaturityDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteMaturityDt.Name = "DteMaturityDt";
            this.DteMaturityDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteMaturityDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaturityDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteMaturityDt.Properties.Appearance.Options.UseFont = true;
            this.DteMaturityDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaturityDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteMaturityDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteMaturityDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteMaturityDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaturityDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteMaturityDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaturityDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteMaturityDt.Properties.ReadOnly = true;
            this.DteMaturityDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteMaturityDt.Size = new System.Drawing.Size(111, 20);
            this.DteMaturityDt.TabIndex = 48;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(63, 303);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 14);
            this.label16.TabIndex = 47;
            this.label16.Text = "Maturity Date";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteLastSettlementDt
            // 
            this.DteLastSettlementDt.EditValue = null;
            this.DteLastSettlementDt.EnterMoveNextControl = true;
            this.DteLastSettlementDt.Location = new System.Drawing.Point(147, 279);
            this.DteLastSettlementDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLastSettlementDt.Name = "DteLastSettlementDt";
            this.DteLastSettlementDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteLastSettlementDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastSettlementDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteLastSettlementDt.Properties.Appearance.Options.UseFont = true;
            this.DteLastSettlementDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastSettlementDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLastSettlementDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLastSettlementDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLastSettlementDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastSettlementDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLastSettlementDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastSettlementDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLastSettlementDt.Properties.ReadOnly = true;
            this.DteLastSettlementDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLastSettlementDt.Size = new System.Drawing.Size(111, 20);
            this.DteLastSettlementDt.TabIndex = 46;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(19, 282);
            this.label14.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(125, 14);
            this.label14.TabIndex = 45;
            this.label14.Text = "Last Settlement Date";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteLastTradeDt
            // 
            this.DteLastTradeDt.EditValue = null;
            this.DteLastTradeDt.EnterMoveNextControl = true;
            this.DteLastTradeDt.Location = new System.Drawing.Point(147, 258);
            this.DteLastTradeDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLastTradeDt.Name = "DteLastTradeDt";
            this.DteLastTradeDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteLastTradeDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastTradeDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteLastTradeDt.Properties.Appearance.Options.UseFont = true;
            this.DteLastTradeDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastTradeDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLastTradeDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLastTradeDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLastTradeDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastTradeDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLastTradeDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastTradeDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLastTradeDt.Properties.ReadOnly = true;
            this.DteLastTradeDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLastTradeDt.Size = new System.Drawing.Size(111, 20);
            this.DteLastTradeDt.TabIndex = 44;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(49, 261);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 14);
            this.label13.TabIndex = 43;
            this.label13.Text = "Last Trade Date";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteListingDt
            // 
            this.DteListingDt.EditValue = null;
            this.DteListingDt.EnterMoveNextControl = true;
            this.DteListingDt.Location = new System.Drawing.Point(147, 237);
            this.DteListingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteListingDt.Name = "DteListingDt";
            this.DteListingDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteListingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteListingDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteListingDt.Properties.Appearance.Options.UseFont = true;
            this.DteListingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteListingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteListingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteListingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteListingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteListingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteListingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteListingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteListingDt.Properties.ReadOnly = true;
            this.DteListingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteListingDt.Size = new System.Drawing.Size(111, 20);
            this.DteListingDt.TabIndex = 42;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(73, 240);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 14);
            this.label12.TabIndex = 41;
            this.label12.Text = "Listing Date";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInterestFrequency
            // 
            this.TxtInterestFrequency.EnterMoveNextControl = true;
            this.TxtInterestFrequency.Location = new System.Drawing.Point(147, 216);
            this.TxtInterestFrequency.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInterestFrequency.Name = "TxtInterestFrequency";
            this.TxtInterestFrequency.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtInterestFrequency.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterestFrequency.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInterestFrequency.Properties.Appearance.Options.UseFont = true;
            this.TxtInterestFrequency.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInterestFrequency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInterestFrequency.Properties.MaxLength = 13;
            this.TxtInterestFrequency.Properties.ReadOnly = true;
            this.TxtInterestFrequency.Size = new System.Drawing.Size(246, 20);
            this.TxtInterestFrequency.TabIndex = 40;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(32, 219);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 14);
            this.label11.TabIndex = 39;
            this.label11.Text = "Interest Frequency";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAnnualDaysAssumption
            // 
            this.TxtAnnualDaysAssumption.EnterMoveNextControl = true;
            this.TxtAnnualDaysAssumption.Location = new System.Drawing.Point(147, 195);
            this.TxtAnnualDaysAssumption.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAnnualDaysAssumption.Name = "TxtAnnualDaysAssumption";
            this.TxtAnnualDaysAssumption.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtAnnualDaysAssumption.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAnnualDaysAssumption.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAnnualDaysAssumption.Properties.Appearance.Options.UseFont = true;
            this.TxtAnnualDaysAssumption.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAnnualDaysAssumption.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAnnualDaysAssumption.Properties.MaxLength = 13;
            this.TxtAnnualDaysAssumption.Properties.ReadOnly = true;
            this.TxtAnnualDaysAssumption.Size = new System.Drawing.Size(246, 20);
            this.TxtAnnualDaysAssumption.TabIndex = 38;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(4, 198);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 14);
            this.label10.TabIndex = 37;
            this.label10.Text = "Annual Days Assumption";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCouponInterestRate
            // 
            this.TxtCouponInterestRate.EnterMoveNextControl = true;
            this.TxtCouponInterestRate.Location = new System.Drawing.Point(147, 174);
            this.TxtCouponInterestRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCouponInterestRate.Name = "TxtCouponInterestRate";
            this.TxtCouponInterestRate.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtCouponInterestRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCouponInterestRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCouponInterestRate.Properties.Appearance.Options.UseFont = true;
            this.TxtCouponInterestRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCouponInterestRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCouponInterestRate.Properties.MaxLength = 13;
            this.TxtCouponInterestRate.Properties.ReadOnly = true;
            this.TxtCouponInterestRate.Size = new System.Drawing.Size(246, 20);
            this.TxtCouponInterestRate.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(18, 177);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 14);
            this.label9.TabIndex = 34;
            this.label9.Text = "Coupon Interest Rate";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentCost
            // 
            this.TxtInvestmentCost.EnterMoveNextControl = true;
            this.TxtInvestmentCost.Location = new System.Drawing.Point(147, 153);
            this.TxtInvestmentCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentCost.Name = "TxtInvestmentCost";
            this.TxtInvestmentCost.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtInvestmentCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCost.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvestmentCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvestmentCost.Properties.MaxLength = 13;
            this.TxtInvestmentCost.Properties.ReadOnly = true;
            this.TxtInvestmentCost.Size = new System.Drawing.Size(246, 20);
            this.TxtInvestmentCost.TabIndex = 33;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(46, 156);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 14);
            this.label8.TabIndex = 32;
            this.label8.Text = "Investment Cost";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMovingAvgPrice
            // 
            this.TxtMovingAvgPrice.EnterMoveNextControl = true;
            this.TxtMovingAvgPrice.Location = new System.Drawing.Point(147, 132);
            this.TxtMovingAvgPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMovingAvgPrice.Name = "TxtMovingAvgPrice";
            this.TxtMovingAvgPrice.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtMovingAvgPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMovingAvgPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMovingAvgPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtMovingAvgPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMovingAvgPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMovingAvgPrice.Properties.MaxLength = 13;
            this.TxtMovingAvgPrice.Properties.ReadOnly = true;
            this.TxtMovingAvgPrice.Size = new System.Drawing.Size(246, 20);
            this.TxtMovingAvgPrice.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(20, 135);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 14);
            this.label7.TabIndex = 29;
            this.label7.Text = "Moving Average Price";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNominalAmt
            // 
            this.TxtNominalAmt.EnterMoveNextControl = true;
            this.TxtNominalAmt.Location = new System.Drawing.Point(147, 111);
            this.TxtNominalAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNominalAmt.Name = "TxtNominalAmt";
            this.TxtNominalAmt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtNominalAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNominalAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNominalAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtNominalAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNominalAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNominalAmt.Properties.MaxLength = 13;
            this.TxtNominalAmt.Properties.ReadOnly = true;
            this.TxtNominalAmt.Size = new System.Drawing.Size(246, 20);
            this.TxtNominalAmt.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(47, 114);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 14);
            this.label6.TabIndex = 27;
            this.label6.Text = "Nominal Amount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(147, 90);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCurCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TxtCurCode.Properties.MaxLength = 13;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(246, 20);
            this.TxtCurCode.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(89, 93);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 14);
            this.label5.TabIndex = 25;
            this.label5.Text = "Currency";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentCategory
            // 
            this.TxtInvestmentCategory.EnterMoveNextControl = true;
            this.TxtInvestmentCategory.Location = new System.Drawing.Point(147, 69);
            this.TxtInvestmentCategory.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentCategory.Name = "TxtInvestmentCategory";
            this.TxtInvestmentCategory.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtInvestmentCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCategory.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCategory.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCategory.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvestmentCategory.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TxtInvestmentCategory.Properties.MaxLength = 13;
            this.TxtInvestmentCategory.Properties.ReadOnly = true;
            this.TxtInvestmentCategory.Size = new System.Drawing.Size(246, 20);
            this.TxtInvestmentCategory.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(21, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 14);
            this.label4.TabIndex = 23;
            this.label4.Text = "Investment Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentType
            // 
            this.LueInvestmentType.EnterMoveNextControl = true;
            this.LueInvestmentType.Location = new System.Drawing.Point(147, 48);
            this.LueInvestmentType.Margin = new System.Windows.Forms.Padding(5);
            this.LueInvestmentType.Name = "LueInvestmentType";
            this.LueInvestmentType.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.LueInvestmentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.Appearance.Options.UseBackColor = true;
            this.LueInvestmentType.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentType.Properties.DropDownRows = 30;
            this.LueInvestmentType.Properties.MaxLength = 16;
            this.LueInvestmentType.Properties.NullText = "[Empty]";
            this.LueInvestmentType.Properties.PopupWidth = 300;
            this.LueInvestmentType.Properties.ReadOnly = true;
            this.LueInvestmentType.Size = new System.Drawing.Size(246, 20);
            this.LueInvestmentType.TabIndex = 22;
            this.LueInvestmentType.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(42, 51);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "Investment Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentName
            // 
            this.TxtInvestmentName.EnterMoveNextControl = true;
            this.TxtInvestmentName.Location = new System.Drawing.Point(147, 27);
            this.TxtInvestmentName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentName.Name = "TxtInvestmentName";
            this.TxtInvestmentName.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentName.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvestmentName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TxtInvestmentName.Properties.MaxLength = 13;
            this.TxtInvestmentName.Properties.ReadOnly = true;
            this.TxtInvestmentName.Size = new System.Drawing.Size(246, 20);
            this.TxtInvestmentName.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(39, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 14);
            this.label2.TabIndex = 19;
            this.label2.Text = "Investment Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentCode
            // 
            this.TxtInvestmentCode.EnterMoveNextControl = true;
            this.TxtInvestmentCode.Location = new System.Drawing.Point(147, 6);
            this.TxtInvestmentCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentCode.Name = "TxtInvestmentCode";
            this.TxtInvestmentCode.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtInvestmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCode.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvestmentCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TxtInvestmentCode.Properties.MaxLength = 13;
            this.TxtInvestmentCode.Properties.ReadOnly = true;
            this.TxtInvestmentCode.Size = new System.Drawing.Size(246, 20);
            this.TxtInvestmentCode.TabIndex = 17;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(42, 9);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(102, 14);
            this.label28.TabIndex = 16;
            this.label28.Text = "Investment Code";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteValuationDt
            // 
            this.DteValuationDt.EditValue = null;
            this.DteValuationDt.EnterMoveNextControl = true;
            this.DteValuationDt.Location = new System.Drawing.Point(194, 4);
            this.DteValuationDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteValuationDt.Name = "DteValuationDt";
            this.DteValuationDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteValuationDt.Properties.Appearance.Options.UseFont = true;
            this.DteValuationDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteValuationDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteValuationDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteValuationDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteValuationDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteValuationDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteValuationDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteValuationDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteValuationDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteValuationDt.Size = new System.Drawing.Size(111, 20);
            this.DteValuationDt.TabIndex = 9;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(104, 7);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 14);
            this.label25.TabIndex = 8;
            this.label25.Text = "Valuation Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFinancialInstitution
            // 
            this.LueFinancialInstitution.EnterMoveNextControl = true;
            this.LueFinancialInstitution.Location = new System.Drawing.Point(194, 25);
            this.LueFinancialInstitution.Margin = new System.Windows.Forms.Padding(5);
            this.LueFinancialInstitution.Name = "LueFinancialInstitution";
            this.LueFinancialInstitution.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.Appearance.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitution.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFinancialInstitution.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFinancialInstitution.Properties.DropDownRows = 30;
            this.LueFinancialInstitution.Properties.MaxLength = 16;
            this.LueFinancialInstitution.Properties.NullText = "[Empty]";
            this.LueFinancialInstitution.Properties.PopupWidth = 300;
            this.LueFinancialInstitution.Size = new System.Drawing.Size(233, 20);
            this.LueFinancialInstitution.TabIndex = 11;
            this.LueFinancialInstitution.ToolTip = "F4 : Show/hide list";
            this.LueFinancialInstitution.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFinancialInstitution.EditValueChanged += new System.EventHandler(this.LueFinancialInstitution_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(80, 28);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 14);
            this.label15.TabIndex = 10;
            this.label15.Text = "Financial Institution";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(194, 46);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 30;
            this.LueBankAcCode.Properties.MaxLength = 16;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 300;
            this.LueBankAcCode.Size = new System.Drawing.Size(233, 20);
            this.LueBankAcCode.TabIndex = 14;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Investment Bank Account (RDN)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFinancialInstitution
            // 
            this.ChkFinancialInstitution.Location = new System.Drawing.Point(432, 25);
            this.ChkFinancialInstitution.Name = "ChkFinancialInstitution";
            this.ChkFinancialInstitution.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFinancialInstitution.Properties.Appearance.Options.UseFont = true;
            this.ChkFinancialInstitution.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFinancialInstitution.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFinancialInstitution.Properties.Caption = " ";
            this.ChkFinancialInstitution.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFinancialInstitution.Size = new System.Drawing.Size(19, 22);
            this.ChkFinancialInstitution.TabIndex = 12;
            this.ChkFinancialInstitution.ToolTip = "Remove filter";
            this.ChkFinancialInstitution.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFinancialInstitution.ToolTipTitle = "Run System";
            this.ChkFinancialInstitution.CheckedChanged += new System.EventHandler(this.ChkFinancialInstitution_CheckedChanged);
            // 
            // ChkBankAcCode
            // 
            this.ChkBankAcCode.Location = new System.Drawing.Point(432, 45);
            this.ChkBankAcCode.Name = "ChkBankAcCode";
            this.ChkBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.ChkBankAcCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBankAcCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBankAcCode.Properties.Caption = " ";
            this.ChkBankAcCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBankAcCode.Size = new System.Drawing.Size(19, 22);
            this.ChkBankAcCode.TabIndex = 15;
            this.ChkBankAcCode.ToolTip = "Remove filter";
            this.ChkBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBankAcCode.ToolTipTitle = "Run System";
            this.ChkBankAcCode.CheckedChanged += new System.EventHandler(this.ChkBankAcCode_CheckedChanged);
            // 
            // FrmRptDebtSecuritiesSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 509);
            this.Name = "FrmRptDebtSecuritiesSchedule";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccruedInterest.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastSettlementDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastSettlementDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastTradeDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastTradeDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAnnualDaysAssumption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCouponInterestRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMovingAvgPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNominalAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteValuationDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteValuationDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFinancialInstitution.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFinancialInstitution.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBankAcCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl Tc1;
        private System.Windows.Forms.TabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.DateEdit DteValuationDt;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.DateEdit DteListingDt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.DateEdit DteNextCouponDt;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.DateEdit DteLastCouponDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.DateEdit DteMaturityDt;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.DateEdit DteLastSettlementDt;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.DateEdit DteLastTradeDt;
        private System.Windows.Forms.Label label13;
        public DevExpress.XtraEditors.SimpleButton BtnInvestmentCode;
        private DevExpress.XtraEditors.CheckEdit ChkFinancialInstitution;
        private DevExpress.XtraEditors.CheckEdit ChkBankAcCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        internal DevExpress.XtraEditors.LookUpEdit LueFinancialInstitution;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCode;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentName;
        internal DevExpress.XtraEditors.LookUpEdit LueInvestmentType;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCategory;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtNominalAmt;
        internal DevExpress.XtraEditors.TextEdit TxtMovingAvgPrice;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCost;
        internal DevExpress.XtraEditors.TextEdit TxtCouponInterestRate;
        internal DevExpress.XtraEditors.TextEdit TxtAnnualDaysAssumption;
        internal DevExpress.XtraEditors.TextEdit TxtInterestFrequency;
        internal DevExpress.XtraEditors.TextEdit TxtAccruedInterest;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
    }
}