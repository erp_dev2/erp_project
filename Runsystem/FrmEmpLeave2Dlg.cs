﻿#region Update
/*
    13/07/2017 [TKG] Tambah validasi hari libur (ambil Working Schedule) di duration (day)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpLeave2 mFrmParent;
        private string 
            mSQL = string.Empty, 
            mDeptCode = string.Empty,
            mSiteCode = string.Empty,
            mLeaveCode = string.Empty,
            mLeaveType = string.Empty,
            mLeaveStartDt = string.Empty,
            mLeaveEndDt = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpLeave2Dlg(FrmEmpLeave2 FrmParent, string DeptCode, string SiteCode, string LeaveCode, string LeaveType, string LeaveStartDt, string LeaveEndDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDeptCode = DeptCode;
            mSiteCode = SiteCode;
            mLeaveCode = LeaveCode;
            mLeaveType = LeaveType;
            mLeaveStartDt = LeaveStartDt;
            mLeaveEndDt = LeaveEndDt;
        }

        #endregion

        #region Methods

        #region Form Load
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            bool
                IsFilterByAGCode = Sm.GetLue(mFrmParent.LueAGCode).Length > 0,
                IsFilterBySiteCode = Sm.GetLue(mFrmParent.LueSiteCode).Length > 0;

            if (mLeaveCode == Sm.GetParameter("AnnualLeaveCode"))
            {
                SQL.AppendLine("Select T1.EmpCode, T1.EmpCodeOld, T1.EmpName, T1.PosName, T1.JoinDt, T1.ResignDt, 0 As NoOfDay ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select A.EmpCode, A.EmpName, A.EmpCodeOld, A.DeptCode, B.PosName, ");
                SQL.AppendLine("    Case When IfNull(G.ParValue, '')='Y' Then ");
                SQL.AppendLine("        IfNull(A.LeaveStartDt, A.JoinDt) ");
                SQL.AppendLine("    Else A.JoinDt ");
                SQL.AppendLine("    End As JoinDt, ");
                SQL.AppendLine("    A.ResignDt ");
                SQL.AppendLine("    From TblEmployee A  ");
                SQL.AppendLine("    Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("    Inner Join TblDepartment C On A.DeptCode=C.DeptCode And C.DeptCode=@DeptCode ");
                SQL.AppendLine("        And Locate(Concat('##', A.EmpCode, '##'), @SelectedEmpCode) < 1 ");
                if (IsFilterBySiteCode)
                    SQL.AppendLine("    Inner Join TblSite D On A.SiteCode=D.SiteCode And D.SiteCode=@SiteCode ");
                if (IsFilterByAGCode)
                {
                    SQL.AppendLine("    Inner Join TblAttendanceGrpHdr E On E.AGCode=@AGCode And E.ActInd='Y' ");
                    SQL.AppendLine("    Inner Join TblAttendanceGrpDtl F On A.EmpCode=F.EmpCode And E.AGCode=F.AGCode ");
                }
                SQL.AppendLine("Left Join TblParameter G On G.ParCode='IsAnnualLeaveUseStartDt' ");
                SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                SQL.AppendLine("    And A.EmpCode Not In ( ");
                SQL.AppendLine("        Select EmpCode From TblEmpPd Where JobTransfer = 'P' And PosCode=@TrainingPosCode  ");
                SQL.AppendLine("    )  ");
                if (mFrmParent.mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And A.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mFrmParent.mIsFilterByDeptHR)
                {
                    SQL.AppendLine("And A.DeptCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine(") T1  ");
                SQL.AppendLine("Where T1.DeptCode=@DeptCode ");
            }
            else
            {
                SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, A.JoinDt, A.ResignDt, If(@leaveCode = (Select ParValue From Tblparameter Where ParCode = 'LongServiceLeaveCode'),  0, G.NoOfDay) As NoOfDay, A.DeptCode, C.Deptname ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode And C.DeptCode=@DeptCode ");
                SQL.AppendLine("    And Locate(Concat('##', A.EmpCode, '##'), @SelectedEmpCode) < 1 ");
                if (IsFilterBySiteCode)
                    SQL.AppendLine("    Inner Join TblSite D On A.SiteCode=D.SiteCode And D.SiteCode=@SiteCode ");
                if (IsFilterByAGCode)
                {
                    SQL.AppendLine("    Inner Join TblAttendanceGrpHdr E On E.AGCode=@AGCode And E.ActInd='Y' ");
                    SQL.AppendLine("    Inner Join TblAttendanceGrpDtl F On A.EmpCode=F.EmpCode And E.AGCode=F.AGCode ");
                }
                SQL.AppendLine("Left Join TblLeave G On LeaveCode=@LeaveCode ");
                SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                if (mFrmParent.mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And A.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mFrmParent.mIsFilterByDeptHR)
                {
                    SQL.AppendLine("And A.DeptCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No.", 
                    
                    //1-5
                    "",
                    "Employee's"+Environment.NewLine+"Code",
                    "Employee's Name",
                    "Old Code",
                    "Position", 

                    //6-10
                    "Permanent"+Environment.NewLine+"Date", 
                    "Number of"+Environment.NewLine+"leave (day)",
                    "Duration" + Environment.NewLine + "(Hour)",
                    "Include"+Environment.NewLine+"Break Time",
                    "Break Schedule",

                    //11
                    "Resign Date"
                },
                 new int[] 
                {
                    //0
                    40, 

                    //1-5
                    20, 80, 200, 100, 200, 
                    
                    //6-10
                    100, 100, 70, 80, 150,

                    //11
                    80
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1, 9 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 11 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10 });
            Grd1.Cols[11].Move(7);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 1=1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(mFrmParent.LueAGCode));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@LeaveCode", mLeaveCode);
                Sm.CmParam<String>(ref cm, "@TrainingPosCode", mFrmParent.mTrainingPosCode);
                Sm.CmParam<String>(ref cm, "@AnnualLeaveCode", mFrmParent.mAnnualLeaveCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParam<String>(ref cm, "@SelectedEmpCode", mFrmParent.GetSelectedEmpCode());
                if (mLeaveCode == Sm.GetParameter("AnnualLeaveCode"))
                    Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T1.EmpCode", "T1.EmpName", "T1.EmpCodeOld" });
                else
                    Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By EmpName; ",
                    new string[]
                    {
                        //0
                        "EmpCode",
                        
                        //1-5
                        "EmpName", "EmpCodeOld", "PosName", "JoinDt", "ResignDt",

                        //6
                        "NoOfDay"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Grd.Cells[Row, 8].Value = 0m;
                        Grd.Cells[Row, 9].Value = false;
                        Grd.Cells[Row, 10].Value = string.Empty;
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsEmpCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 11);
                        mFrmParent.Grd1.Cells[Row1, 12].Value = 0m;
                        mFrmParent.ComputeDurationHour2(Row1);
                        mFrmParent.ComputeLeaveRemainingDay2(Row1);

                        mFrmParent.Grd1.Rows.Add();
                    }
                }

                Sm.SetGrdBoolValueFalse(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9 });
                Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 7, 8, 12 });
            }

            if (!IsChoose) 
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");

            mFrmParent.SetEmployeeNoOfHoliday();
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                mFrmParent.Grd1.Cells[r, 0].Value = r + 1;
        }

        private bool IsEmpCodeAlreadyChosen(int Row)
        {
            string EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), EmpCode)) return true;
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        #endregion

        #endregion

    }
}
