﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemGroup : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmItemGroupFind FrmFind;

        #endregion

        #region Constructor

        public FrmItemGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
      //      if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItGrpCode, TxtItGrpName, MeeRemark
                    }, true);
                    TxtItGrpCode.Focus();
                    ChkActiveInd.Properties.ReadOnly = true;
                   // BtnAcCode.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItGrpCode, TxtItGrpName, MeeRemark
                    }, false);
                    TxtItGrpCode.Focus();
                    ChkActiveInd.Checked = true;
                   // BtnAcCode.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtItGrpCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItGrpName
                    }, false);
                    TxtItGrpName.Focus();
                    ChkActiveInd.Properties.ReadOnly = false;
                   // BtnAcCode.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItGrpCode, TxtItGrpName, MeeRemark
            });
            ChkActiveInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmItemGroupFind(this);
           Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItGrpCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItGrpCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblItemGroup Where ItGrpCode=@ItGrpCode" };
                Sm.CmParam<String>(ref cm, "@ItGrpCode", TxtItGrpCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblItemGroup(ItGrpCode, ItGrpName, ActInd, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItGrpCode, @ItGrpName, @ActInd, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ItGrpName=@ItGrpName, ActInd=@ActInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItGrpCode", TxtItGrpCode.Text);
                Sm.CmParam<String>(ref cm, "@ItGrpName", TxtItGrpName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtItGrpCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItGrpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItGrpCode", ItGrpCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.ItGrpCode, A.ItGrpName, ActInd, A.Remark From TblItemGroup A " +
                        "Where A.ItGrpCode=@ItGrpCode",
                        new string[] 
                        {
                            "ItGrpCode", "ItGrpName", "ActInd", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtItGrpCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtItGrpName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                            MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItGrpCode, "Item's group code", false) ||
                Sm.IsTxtEmpty(TxtItGrpName, "Item's group name", false) ||
                IsItCtCodeExisted();
        }

        private bool IsItCtCodeExisted()
        {
            if (!TxtItGrpCode.Properties.ReadOnly && Sm.IsDataExist("Select ItGrpCode From TblItemGroup Where ItGrpCode='" + TxtItGrpCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Item's group code ( " + TxtItGrpCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion


        #endregion

        #region Event

        private void TxtItGrpCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItGrpCode);
        }

        private void TxtItGrpName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItGrpName);
        }
        #endregion
    }
}
