﻿#region
/*
 * 23/02/2022 [SET/PHT] New apps
 */ 

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPositionAdjustmentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPositionAdjustment mFrmParent;

        #endregion

        #region Constructor

        public FrmPositionAdjustmentDlg(FrmPositionAdjustment FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.LevelCode, F.LevelName, B.ServiceYrRange, H.OptDesc, B.AllowanceAmt, B.AllowanceAmt2, C.GrdLvlCode, E.GrdLvlName, ");
            SQL.AppendLine("C.LevelCode AS LevelCodedtl2, G.LevelName AS LevelNamedtl2, C.ServiceYrRange AS ServiceYrRangedtl2, I.OptDesc AS OptDescdtl2, C.AllowanceAmt AS AllowanceAmtdtl2, D.ServiceYr, D.AllowanceAmt AS AllowanceAmtdtl3 ");
            SQL.AppendLine("FROM tblpositionadjustmenthdr A ");
            SQL.AppendLine("INNER JOIN tblpositionadjustmentdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN tblpositionadjustmentdtl2 C ON A.DocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN tblpositionadjustmentdtl3 D ON A.DocNo = D.DocNo ");
            SQL.AppendLine("LEFT JOIN tblgradelevelhdr E ON C.GrdLvlCode = E.GrdLvlCode ");
            SQL.AppendLine("LEFT JOIN tbllevelhdr F ON B.LevelCode = F.LevelCode ");
            SQL.AppendLine("LEFT JOIN tbllevelhdr G ON B.LevelCode = G.LevelCode ");
            SQL.AppendLine("LEFT JOIN tbloption H ON B.ServiceYrRange = H.OptCode AND H.OptCat = 'PositionAdjustmentServiceYrRange' ");
            SQL.AppendLine("LEFT JOIN tbloption I ON C.ServiceYrRange = I.OptCode AND I.OptCat = 'PositionAdjustmentServiceYrRange' ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");
            //SQL.AppendLine("Select  ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "LevelCode",
                        "Level",
                        "YoSCode",
                        "Years of Service",
                        
                        //6-10
                        "Allowance 1",
                        "Allowance 2",
                        "Document#",
                        "GrdLvlCode",
                        "Grade",

                        //11-15
                        "LevelCodedtl2",
                        "Level",
                        "YoSdtl2",
                        "Years of Service",
                        "Allowance",

                        //16-18
                        "Document#",
                        "Years of Service",
                        "Allowance",
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 9, 11, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 9, 12 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            //if (
            //    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            //    Sm.IsDteEmpty(DteDocDt2, "End date") ||
            //    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            //    ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "WHERE 1=1 ";

                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL() + Filter + " Order By DocDt, DocNo;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "LevelCode", "LevelName", "ServiceYrRange", "OptDesc", "AllowanceAmt", 
                            
                            //6-10
                            "AllowanceAmt2", "GrdLvlCode", "GrdLvlName", "LevelCodedtl2", "LevelNamedtl2",

                            //11-15
                            "ServiceYrRangedtl2", "OptDescdtl2", "AllowanceAmtdtl2", "ServiceYr", "AllowanceAmtdtl3"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
                {
                    mFrmParent.TxtDocNo, mFrmParent.MeeRemark
                });
                mFrmParent.ChkCancelInd.Checked = false;
                mFrmParent.SetFormControl(mState.Insert);
                //mFrmParent.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                //mFrmParent.Grd2.Cols.
                this.Close();
            }
        }

        #endregion

    }
}
