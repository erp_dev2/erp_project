﻿#region Update
/* 
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    03/03/2020 [DITA/SIER] Tambah choose data untuk kolom discount amt
    20/04/2020 [IBL/KSM] memunculkan foreign name berdasarkan parameter IsShowForeignName 3
    31/05/2021 [DITA/SIER] Menambahkan kolom Tax (%), amount after tax , dan amount after discount saat choose data set = 0\
    10/06/2021 [RDA/SIER] Menambahkan filter IsItCtFilteredByGroup untuk menampilkan item berdasarkan group login
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;
using System.Net;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmCtQtDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmCtQt mFrmParent;
        private string mSQL = string.Empty, mDocDt = string.Empty, mCurCode = string.Empty, mCtQtDocNoReplace = string.Empty;
        private string mCtCode = string.Empty;
        private bool mIsShowDefault = true;
   

        #endregion

        #region Constructor

        public FrmCtQtDlg2(FrmCtQt FrmParent, string DocDt, string CurCode, string CtCode, string CtQtDocNoReplace)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
            mCurCode = CurCode;
            mCtCode = CtCode;
            mCtQtDocNoReplace = CtQtDocNoReplace;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueItCtCode(ref LueItCtCode);
                SetGrd();
                SetSQL();
                ShowData();
                mIsShowDefault = false;
            }
            catch(Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                   Grd1, new string[] 
                    { 
                        //0
                        "No.",

                        //1-5
                        "",
                        "Item's"+Environment.NewLine+"Code",
                        "",
                        "Local"+Environment.NewLine+"Code",
                        "Item's"+Environment.NewLine+"Name",
                        
                        //6-10
                        "Category",
                        "Document#",
                        "DNo",
                        "",
                        "Currency",
                        
                        //11-15
                        "Previous" +Environment.NewLine+"Price",
                        "Unit Price",
                        "Price After Discount"+Environment.NewLine+"(Default)",
                        "UoM",
                        "Start Date",

                        //16-19
                        "Specification",
                        "Customer"+Environment.NewLine+"Item's Code",
                        "Customer"+Environment.NewLine+"Item's Name",
                        "Foreign Name"
                    }
               );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3, 9 });
            Sm.GrdFormatDate(Grd1, new int[] { 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Grd1.Cols[16].Move(7);
            Grd1.Cols[18].Move(8);
            Grd1.Cols[17].Move(8);
            Grd1.Cols[19].Move(6);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 7, 8, 9, 15, 16 }, false);
            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 18 });
            if (!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 19 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 15, 16 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            if (mFrmParent.mCtQtBusinessProcess != "1")
                SetSQL2();
            else
                SetSQL1();
        }

        private void SetSQL1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, A.ForeignName, A.Specification, F.CtItCode, F.CtItName, B.ItCtName, C.PriceUomCode, ");
            SQL.AppendLine("C.DocNo, C.DNo, C.CurCode, C.UPrice, C.StartDt, IfNull(D.UPrice, 0) As PrevUPrice, ");
            if (mCtQtDocNoReplace.Length > 0)
                SQL.AppendLine("IfNull(E.PriceAfterDisc, C.UPrice) As PriceAfterDisc ");
            else
                SQL.AppendLine("IfNull(D.UPrice, C.UPrice) As PriceAfterDisc ");    
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C1.PriceUomCode, C1.StartDt, C2.ItCode, C2.UPrice ");
            SQL.AppendLine("    From TblItemPriceHdr C1 ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("    Where C1.ActInd='Y' ");
            SQL.AppendLine("    And C1.CurCode=@CurCode ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.ItCode, T1.CurCode, T3.PriceUomCode, T2.UPrice  ");
            SQL.AppendLine("    From TblCtQtHdr T1 ");
            SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join (");
            SQL.AppendLine("        Select X4.ItCode, X3.CurCode, X3.PriceUomCode, Max(Concat(X1.DocDt, X1.DocNo)) As Key1 ");
            SQL.AppendLine("        From TblCtQtHdr X1 ");
            SQL.AppendLine("        Inner Join TblCtQtDtl X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr X3 On X2.ItemPriceDocNo=X3.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl X4 On X2.ItemPriceDocNo=X4.DocNo And X2.ItemPriceDNo=X4.DNo ");
            SQL.AppendLine("        Where X1.CtCode=@CtCode ");
            SQL.AppendLine("        And X1.CurCode=@CurCode ");
            SQL.AppendLine("        Group By X4.ItCode, X3.CurCode, X3.PriceUomCode ");
            SQL.AppendLine("        ) T5 ");
            SQL.AppendLine("            On T4.ItCode=T5.ItCode ");
            SQL.AppendLine("            And T1.CurCode=T5.CurCode ");
            SQL.AppendLine("            And T3.PriceUomCode=T5.PriceUomCode ");
            SQL.AppendLine("            And Concat(T1.DocDt, T1.DocNo)=T5.Key1 ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.ItCode=D.ItCode ");
            SQL.AppendLine("    And C.CurCode=D.CurCode ");
            SQL.AppendLine("    And C.PriceUomCode=D.PriceUomCode ");

            if (mCtQtDocNoReplace.Length > 0)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T4.ItCode, T2.UPrice As PriceAfterDisc ");
                SQL.AppendLine("    From TblCtQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
                SQL.AppendLine("    Where T1.DocNo=@CtQtDocNoReplace ");
                SQL.AppendLine(") E On A.ItCode=E.ItCode ");
            }

            SQL.AppendLine("LEFT JOIN TblCustomerItem F ON A.ItCode = F.ItCode And F.CtCode=@CtCode ");
            SQL.AppendLine("Where A.SalesItemInd = 'Y' ");
            SQL.AppendLine("And A.ActInd = 'Y' ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetSQL2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, A.ForeignName, A.Specification, F.CtItCode, F.CtItName, B.ItCtName, C.PriceUomCode, ");
            SQL.AppendLine("C.DocNo, C.DNo, C.CurCode, IfNull(C.UPrice, 0) As UPrice, C.StartDt, IfNull(D.UPrice, 0) As PrevUPrice, ");
            SQL.AppendLine("IfNull(C.UPrice, 0) As PriceAfterDisc ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C1.PriceUomCode, C1.StartDt, C2.ItCode, C2.UPrice ");
            SQL.AppendLine("    From TblItemPriceHdr C1 ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("    Where C1.ActInd='Y' ");
            SQL.AppendLine("    And C1.CurCode=@CurCode ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.ItCode, T1.CurCode, T3.PriceUomCode, T2.UPrice  ");
            SQL.AppendLine("    From TblCtQtHdr T1 ");
            SQL.AppendLine("    Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine("    Where T1.CtCode=@CtCode ");
            SQL.AppendLine("    And T1.CurCode=@CurCode ");
            SQL.AppendLine("    And T1.ActInd='Y' ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.ItCode=D.ItCode ");
            SQL.AppendLine("    And C.CurCode=D.CurCode ");
            SQL.AppendLine("    And C.PriceUomCode=D.PriceUomCode ");
            SQL.AppendLine("LEFT JOIN TblCustomerItem F ON A.ItCode = F.ItCode And F.CtCode=@CtCode ");
            SQL.AppendLine("Where A.SalesItemInd = 'Y' ");
            SQL.AppendLine("And A.ActInd = 'Y' ");

            if (mFrmParent.mIsFilterByItCt || mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
           
            mSQL = SQL.ToString();
        }


        private string GetDefaultFilter()
        {
            var SQL = new StringBuilder();

            if (mFrmParent.mCtQtBusinessProcess != "1")
            {
                SQL.AppendLine(" And A.ItCode In ( ");
                SQL.AppendLine("    Select X3.ItCode ");
                SQL.AppendLine("    From TblCtQtHdr X1 ");
                SQL.AppendLine("    Inner Join TblCtQtDtl X2 On X1.DocNo=X2.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPriceDtl X3 On X2.ItemPriceDocNo=X3.DocNo And X2.ItemPriceDNo=X3.DNo ");
                SQL.AppendLine("    Where X1.ActInd='Y' ");
                SQL.AppendLine("    And X1.CurCode=@CurCode ");
                SQL.AppendLine("    And X1.CtCode=@CtCode ");
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("    And A.ItCode In ( ");
                SQL.AppendLine("        Select Distinct T4.ItCode ");
                SQL.AppendLine("        From TblCtQtHdr T1 ");
                SQL.AppendLine("        Inner Join TblCtQtDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo=T3.DocNo ");
                SQL.AppendLine("        Inner Join TblItemPriceDtl T4 On T2.ItemPriceDocNo=T4.DocNo And T2.ItemPriceDNo=T4.DNo ");
                SQL.AppendLine("        Where T1.CtCode=@CtCode ");
                SQL.AppendLine("        And T1.CurCode=@CurCode ");
                if (mCtQtDocNoReplace.Length == 0)
                {
                    SQL.AppendLine("        And Concat(T1.DocDt, T1.DocNo) In ( ");
                    SQL.AppendLine("            Select Max(Concat(DocDt, DocNo)) As Key1 ");
                    SQL.AppendLine("            From TblCtQtHdr ");
                    SQL.AppendLine("            Where CtCode=@CtCode ");
                    SQL.AppendLine("            And CurCode=@CurCode ");
                    SQL.AppendLine("            ) ");
                }
                else
                {
                    SQL.AppendLine("        And T1.DocNo=@CtQtDocNoReplace ");
                }
                SQL.AppendLine("    ) ");
            }

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                if (mFrmParent.mCtQtBusinessProcess != "1")
                    Filter = " And A.ItCode Not In (" + mFrmParent.GetSelectedItem() + ") " +
                        (mIsShowDefault ? GetDefaultFilter() : "");
                else
                    Filter =
                        " And A.ItCode Not In (" + mFrmParent.GetSelectedItem() + ") " +
                        (mIsShowDefault ? GetDefaultFilter() : "");
            
                var cm = new MySqlCommand();
                if (mFrmParent.mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@DocDt", mDocDt);
                Sm.CmParam<String>(ref cm, "@CurCode", mCurCode);
                Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", mCtQtDocNoReplace);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName", "F.CtItCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName, A.ItCode, C.PriceuomCode;",
                        new string[] 
                        { 
                        
                            //0
                            "ItCode", 

                            //1-5
                            "ItCodeInternal", "ItName", "ItCtName", "DocNo", "DNo",  
                            
                            //6-10
                            "CurCode", "PrevUPrice", "UPrice", "PriceAfterDisc", "PriceUomCode", 
                            
                            //11-15
                            "StartDt", "Specification", "CtItCode", "CtItName", "ForeignName"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = mIsShowDefault;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                        }, !mIsShowDefault, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 8, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 9, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 10, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 11, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 14, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 15, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 16, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 22, Grd1, Row2, 19);
                        mFrmParent.ComputeDiscount(Row1);

                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 9, 10, 11, 12, 21, 23, 24, 25 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd2, Index, 2), Sm.GetGrdStr(Grd1, Row, 2)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItemPrice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItemPrice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Additional Method

        private void SetLueItCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T1.ItCtCode As Col1, T1.ItCtName As Col2 ");
            SQL.AppendLine("FROM tblitemcategory T1 ");
            if (mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("WHERE  (T1.ItCtCode Is Null OR (T1.ItCtCode Is Not Null And Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("        Where ItCtCode=T1.ItCtCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order By T1.ItCtName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion 

        #region Event
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

        
    }
}
