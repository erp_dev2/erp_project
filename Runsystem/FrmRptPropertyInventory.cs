﻿#region Update
/*
    29/12/2022 [MAU/BBT] Menu baru 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptPropertyInventory : RunSystem.FrmBase2
    {
        #region Field

        internal string mMenuCode = "", mAccessInd = "";

        #endregion

        #region Constructor
        public FrmRptPropertyInventory(string MenuCode) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
            //mFrmParent = FrmParent;
        }
        #endregion

        #region Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                SetLuePropertyCategory(ref LuePropertyCategory);

                SetGrd();
                Sl.SetLueSiteCode(ref LueSiteCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "property Code",
                        "Active",
                        "Cancel",
                        "Registration Date",
                        "Property Name",

                        //6-10
                        "Initial Cost Center",
                        "Site",
                        "Property Inventory Value",
                        "Property Category",
                        "Property"+Environment.NewLine+"Quantity",

                        
                        //11-15
                        "UoM",
                        "Remaining Stock"+Environment.NewLine+"Quantity",
                        "Remaining Stock"+Environment.NewLine+"Value",
                        "COA#",
                        "COA Description",

                        //16-20
                        "Created By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",

                        //21
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        200, 80, 80, 150, 250, 
                        
                        //6-10
                        350, 250, 150, 250, 100, 

                        //11-15
                        100, 100, 100, 100, 350,

                        //16-20
                        100,100,100,100,100,

                        //21
                        100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColCheck(Grd1, new int[] { 2, 3 });
            Sm.GrdFormatTime(Grd1, new int[] { 18, 21 });
            Sm.GrdFormatDate(Grd1, new int[] { 17, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
            Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            Sm.IsDteEmpty(DteDocDt2, "End date") ||
            Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 1=1 and A.RegistrationDt BETWEEN @DocDt1 AND @DocDt2 ";
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtPropertyInventoryCode.Text, new string[] { "A.PropertyCode", "A.PropertyName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePropertyCategory), "A.PropertyCategoryCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                SQL.AppendLine(" SELECT ");
                SQL.AppendLine(" A.PropertyCode, A.ActInd, A.CancelInd, A.RegistrationDt, A.PropertyName, ");
                SQL.AppendLine(" D.CCName, E.SiteName, A.SiteCode, A.PropertyInventoryValue, A.PropertyCategoryCode, C.PropertyCategoryName, A.InventoryQty, ");
                SQL.AppendLine(" A.UomCode, A.RemStockQty, A.RemStockValue, A.AcNo, B.AcDesc, ");
                SQL.AppendLine(" A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine(" FROM tblpropertyinventoryhdr A ");
                SQL.AppendLine(" LEFT JOIN tblcoa B ON A.AcNo = B.AcNo ");
                SQL.AppendLine(" LEFT JOIN tblpropertyinventorycategory C ON A.PropertyCategoryCode = C.PropertyCategoryCode ");
                SQL.AppendLine(" LEFT JOIN tblcostcenter D ON A.CCCode = D.CCCode ");
                SQL.AppendLine(" LEFT JOIN tblsite E ON A.SiteCode = E.SiteCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By A.PropertyCode ; ");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "PropertyCode", 
                    
                            //1-5
                            "ActInd", "CancelInd", "RegistrationDt", "PropertyName", "CCName",  
                    
                            //6-10
                            "SiteName", "PropertyInventoryValue", "PropertyCategoryName", "InventoryQty", "UomCode", 
                    
                            //11-15
                            "RemStockQty", "RemStockValue", "AcNo" , "AcDesc", "CreateBy",

                            //16-18
                            "CreateDt", "LastUpBy", "LastUpDt"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 18);

                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 18);

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }
        public void SetLuePropertyCategory(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine(" Select PropertyCategoryCode As Col1, PropertyCategoryName As Col2 From TblPropertyInventoryCategory where Actind = 'Y' ");
                SQL.AppendLine(" Order By PropertyCategoryName ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #endregion

        #region Misc Control Event
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
        private void LueSite_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            //Sm.FilterLueSetCheckEdit(this, sender);
            ChkSite.Checked = true;
        }
        private void ChkSite_CheckedChanged(object sender, EventArgs e)
        {
            //Sm.FilterSetTextEdit(this, sender, "Site");


            if (!ChkSite.Checked) 
            { 
                Sm.IsLueEmpty(LueSiteCode, "Site");
                return;
            }
            else
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueSiteCode });
            }
   
        }
        private void LuePropertyCategory_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropertyCategory, new Sm.RefreshLue1(SetLuePropertyCategory));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkPropertyCategoryCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Property Category");
        }
        private void TxtPropertyInventoryCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkPropertyInventoryCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property");
        }
        #endregion
    }
}
