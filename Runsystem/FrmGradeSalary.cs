﻿#region Update
/*
    16/10/2020 [ICA/PHT] New apps
 *  25/11/2020 [ICA/PHT] Menambah kolom Month 
 *  08/12/2020 [ICA/PHT] Kolom Month dan Work Period bisa di isi 0. 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGradeSalary : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmGradeSalaryFind FrmFind;

        #endregion

        #region Constructor

        public FrmGradeSalary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Grade Salary";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Sequence",

                        //1-3
                        "Work Period"+Environment.NewLine+"(year)",
                        "Month",
                        "Amount",
                    },
                     new int[] 
                    { 
                        120,
                        100, 100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 1, 3 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtGradeSalaryCode, TxtGradeSalaryName, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtGradeSalaryCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtGradeSalaryCode, TxtGradeSalaryName, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtGradeSalaryCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtGradeSalaryName, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtGradeSalaryName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtGradeSalaryCode, TxtGradeSalaryName, MeeRemark });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGradeSalaryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGradeSalaryCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;
                var cml = new List<MySqlCommand>();
                cml.Add(SaveGradeSalaryHdr());
                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0) cml.Add(SaveGradeSalaryDtl(Row));
                }
                Sm.ExecCommands(cml);
                ShowData(TxtGradeSalaryCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                //Sm.SetGrdNumValueZero(ref Grd1, e.RowIndex, new int[] { 3 });
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string GradeSalaryCode)
        {
            try
            {
                ClearData();
                ShowGradeSalaryHdr(GradeSalaryCode);
                ShowGradeSalaryDtl(GradeSalaryCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowGradeSalaryHdr(string GradeSalaryCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@GradeSalaryCode", GradeSalaryCode);

            Sm.ShowDataInCtrl(
                 ref cm,
                  "Select GrdSalaryCode, GrdSalaryName, Remark " +
                  "From TblGradeSalaryHdr Where GrdSalaryCode=@GradeSalaryCode;",
                 new string[] 
                   {
                      //0
                      "GrdSalaryCode",

                      //1-5
                      "GrdSalaryName", "Remark"
                   },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtGradeSalaryCode.EditValue = Sm.DrStr(dr, c[0]);
                     TxtGradeSalaryName.EditValue = Sm.DrStr(dr, c[1]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[2]);
                 }, true
             );
        }

        private void ShowGradeSalaryDtl(string GradeSalaryCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sequence, WorkPeriod, Month, Amt ");
            SQL.AppendLine("From TblGradeSalaryDtl ");
            SQL.AppendLine("Where GrdSalaryCode=@GradeSalaryCode ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@GradeSalaryCode", GradeSalaryCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "Sequence",

                           //1-3
                           "WorkPeriod","Month", "Amt"

                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtGradeSalaryCode, "Grade Salary's code", false) ||
                Sm.IsTxtEmpty(TxtGradeSalaryName, "Grade Salary's name", false) ||
                IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 0, true, "Sequence is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Work Period is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Month is empty.")) return true;
                    if (Sm.GetGrdDec(Grd1, r, 3) <= 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Amount should be greater than 0.");
                        return true;
                    }
                }
            }

            else
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveGradeSalaryHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGradeSalaryHdr ");
            SQL.AppendLine("(GrdSalaryCode, GrdSalaryName, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@GradeSalaryCode, @GradeSalaryName, @Remark, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("    GrdSalaryName=@GradeSalaryName, Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("DELETE FROM tblgradesalarydtl WHERE grdsalarycode = @GradeSalaryCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@GradeSalaryCode", TxtGradeSalaryCode.Text);
            Sm.CmParam<String>(ref cm, "@GradeSalaryName", TxtGradeSalaryName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveGradeSalaryDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGradeSalaryDtl (GrdSalaryCode, DNo, Sequence, WorkPeriod, Month, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@GradeSalaryCode, @DNo, @Sequence, @WorkPeriod, @Month, @Amt, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@GradeSalaryCode", TxtGradeSalaryCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@Sequence", Sm.GetGrdDec(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@WorkPeriod", Sm.GetGrdDec(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Month", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtGradeSalaryName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGradeSalaryName);
        }

        #endregion

        private void TxtGradeSalaryCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGradeSalaryCode);
        }

        #endregion
    }
}
