﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmLocationFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmLocation mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmLocationFind(FrmLocation FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            SetGrd();
            SetSQL();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Site Code",
                        "Site Name",
                        "Location"+Environment.NewLine+"Code", 
                        "Location"+Environment.NewLine+"Name",
                        "Active",

                        //6-10
                        "Parent"+Environment.NewLine+"Name",
                        "Remark",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        
                        //11-13
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 130, 100, 130, 50,
                        
                        //6-10
                        130, 150, 100, 100, 100,

                        //11-13
                        100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT a.SiteCode, b.SiteName, a.LocCode, a.LocName, a.ActInd, ");
            SQL.AppendLine("  (SELECT LocName FROM TblLocation WHERE LocCode=a.Parent) AS ParentName, ");
            SQL.AppendLine("a.Remark, a.CreateBy, a.CreateDt, a.LastUpBy, a.LastUpDt ");
            SQL.AppendLine("FROM TblLocation a ");
            SQL.AppendLine("LEFT JOIN TblSite b ");
            SQL.AppendLine("ON a.SiteCode = b.SiteCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true); 
                Sm.FilterStr(ref Filter, ref cm, TxtLoc.Text, new string[] { "a.LocCode", "a.LocName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ORDER BY b.SiteName",
                        new string[]
                        {
                            //0
                            "SiteCode",
                            
                            //1-5
                            "SiteName", "LocCode", "LocName", "ActInd", "ParentName",
                            
                            //6-10
                            "Remark", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3));
                this.Hide();
            }
        }

        #endregion

        #region Events

            #region Misc Control Events

            private void ChkLoc_CheckedChanged(object sender, EventArgs e)
            {
                Sm.FilterSetTextEdit(this, sender, "Location");
            }

            private void TxtLoc_Validated(object sender, EventArgs e)
            {
                Sm.FilterTxtSetCheckEdit(this, sender);
            }

            private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
                Sm.FilterLueSetCheckEdit(this, sender);
            }


            private void ChkLueSiteCode_CheckedChanged(object sender, EventArgs e)
            {
                Sm.FilterSetLookUpEdit(this, sender, "Site");
            }

            #endregion

        #endregion
    }
}
