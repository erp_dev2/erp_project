﻿#region Update
/*
    04/10/2017 [WED] bug fixing loop ke vendor
    15/11/2021 [AMKA/YOG] Membuat field doc outgoing payment yang tertarik di reporting Outgoing Payment Tracker berasal dari departement yang telah terfilter berdasarkan group
    25/03/2022 [VIN/ALL] BUG: group departemen -> data yg dept nya null ttp dimuncuklan 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOutgoingPayment : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterByDept = false;

        #endregion

        #region Constructor

        public FrmRptOutgoingPayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                GetParameter();
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, T3.Vdname, T2.Itname, T2.PurchaseUomCode From ( ");
            SQL.AppendLine("Select A.VdCode, A.DocDt, A.DocNo, B.InvoiceDocNo, ");
            SQL.AppendLine("Case B.InvoiceType ");
            SQL.AppendLine("When '1' then '-' ");
            SQL.AppendLine("When '2' then F.DOVdDocNo ");
            SQL.AppendLine("end As DOToVendorReturn, ");
            SQL.AppendLine("Case B.InvoiceType ");
            SQL.AppendLine("When '1' then D.RecvVdDocNo ");
            SQL.AppendLine("When '2' then H.RecvVdDocNo ");
            SQL.AppendLine("end As Receiving, ");
            SQL.AppendLine("Case B.InvoiceType ");
            SQL.AppendLine("When '1' then G.PODocNo ");
            SQL.AppendLine("When '2' then G2.PODocNo ");
            SQL.AppendLine("end As PO, J.DocNo As POR, K.DocNo As MR, ");
            SQL.AppendLine("Case B.InvoiceType ");
            SQL.AppendLine("When '1' then G.ItCode ");
            SQL.AppendLine("When '2' then G2.ItCode ");
            SQL.AppendLine("end As ItCode, ");
            SQL.AppendLine("Case B.InvoiceType ");
            SQL.AppendLine("When '1' then G.QtyPurchase ");
            SQL.AppendLine("When '2' then G2.QtyPurchase ");
            SQL.AppendLine("end As QtyPurchase, L.CurCode, M.UPrice ");
            SQL.AppendLine("From TblOutgoingpaymentHdr A ");
            SQL.AppendLine("Inner Join TblOutgoingpaymentDtl B On A.DocNo = B.DocNo And A.cancelInd = 'N' ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceHdr C On B.InvoiceDocNo = C.DocNo And B.InvoiceType = '1' And A.CancelInd = 'C' ");
            SQL.AppendLine("Left Join TblPurchaseInvoiceDtl D On B.InvoiceDocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblPurchaseReturnInvoiceHdr E On B.InvoiceDocNo = E.DocNo And B.InvoiceType = '2' And E.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblPurchaseReturnInvoiceDtl F On B.InvoiceDocNo = F.DocNo And B.InvoiceType = '2' ");
            SQL.AppendLine("Left Join TblDOVdDtl H On F.DOVdDocNo = H.DocNo And F.DOVdDNo = H.Dno And H.cancelInd = 'N' ");
            SQL.AppendLine("left Join TblRecvVdDtl G On D.RecvVdDocNo = G.DocNo And D.RecvVdDNo = G.Dno And G.CancelInd= 'N' ");
            SQL.AppendLine("left Join TblRecvVdDtl G2 On H.RecvVdDocNo = G2.DocNo And H.RecvVdDNo = G2.Dno And G2.CancelInd= 'N' ");
            SQL.AppendLine("Left Join TblPODtl I On G.PODocNo = I.DocNo And  G.PODNo = I.Dno And I.CancelInd = 'N' ");
            SQL.AppendLine("left Join TblPorequestDtl J On I.PORequestDocNo = J.DocNo And I.PORequestDno = J.Dno  And J.CancelInd= 'N' ");
            SQL.AppendLine("left Join TblMaterialRequestDtl K On J.MaterialRequestDocNo = K.DocNo And J.MaterialRequestDno = K.Dno And K.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblQtHdr L On J.QtDocNo=L.DocNo ");
            SQL.AppendLine("Left Join TblQtDtl M On J.QtDocNo=M.DocNo And J.QtDNo=M.DNo ");
            //if (mIsFilterByDept)
            //{
            //    SQL.AppendLine("    Inner Join TblDepartment Dept on Dept.DeptCode=A.DeptCode ");
            //    SQL.AppendLine("AND Exists(  ");
            //    SQL.AppendLine("        Select 1 From TblGroupdepartment ");
            //    SQL.AppendLine("        Where DeptCode=Dept.DeptCode  ");
            //    SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            //    SQL.AppendLine(") ");
            //}
            SQL.AppendLine("Where 0=0 ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode Is Null Or (A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupdepartment ");
                SQL.AppendLine("    Where EntCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine(")T ");
            SQL.AppendLine("Inner Join TblItem T2 On T.ItCode = T2.ItCode ");
            SQL.AppendLine("Inner Join TblVendor T3 On T.vdCode = T3.VdCode  ");
            //-- Group By T.DocNo, T.InvoiceDocNo, T.DOToVendorReturn, T.Receiving,  T.PO, T.POR, T.MR, T.itCode;
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Vendor"+Environment.NewLine+"Code", 
                        "",
                        "Vendor",
                        "Outgoing Payment",
                        "",
                        
                        //6-10
                        "Date",
                        "Purchase Invoice",
                        "",
                        "Returning Item",
                        "",

                        //11-15
                        "Receiving Item",
                        "",
                        "Purchase Order",
                        "",
                        "PO Request",

                        //16-20
                        "",
                        "Material Request",
                        "",
                        "Item Code",
                        "",

                        //21-25
                        "Item Name",
                        "Quantity"+Environment.NewLine+"Purchase",
                        "UoM",
                        "Unit Price",
                        "Currency"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 20, 150, 150, 20, 
                        
                        //6-10
                        100, 150, 20, 150, 20, 

                        //11-15
                        150, 20, 150, 20, 150, 

                        //16-20
                        20, 150, 20, 80, 20,

                        //21-24
                        250, 100, 80, 100, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5, 8, 10, 12, 14, 16, 18, 20 });
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 22, 24 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 9, 11, 13, 15, 17, 19, 21, 22, 23, 24, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 5, 8, 10, 12, 14, 16, 18, 19, 20 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 5, 8, 10, 12, 14, 16, 18, 19, 20 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "WHERE 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "T.DocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "T.VdCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[]
                        {
                            //0
                            "VdCode", 

                            //1-5
                            "VdName", "DocNo", "DocDt", "InvoiceDocno", "DOToVendorReturn", 

                            //6-10
                            "Receiving", "PO", "POR", "MR", "ItCode", 
                            
                            //11-15
                            "ItName",  "QtyPurchase", "PurchaseUomCode", "UPrice", "CurCode", 
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        { 
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 15);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(4);
                Grd1.GroupObject.Add(7);
                Grd1.GroupObject.Add(9);
                Grd1.GroupObject.Add(11);
                Grd1.GroupObject.Add(13);
                Grd1.GroupObject.Add(15);
                Grd1.Group();
                //AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] {  });
        }

        

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        { 
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVendor(mMenuCode);
                f.Tag = mMenuCode;
                f.mMenuCode = string.Empty;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVendor' Limit 1 ");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmOutgoingPayment(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

             if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmPurchaseInvoice' Limit 1");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
             if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
             if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }
             if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f.ShowDialog();
            }
             if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPORequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }
             if (e.ColIndex == 18 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 17);
                f.ShowDialog();
            }
             if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmVendor(mMenuCode);
                f.Tag = mMenuCode;
                f.mMenuCode = string.Empty;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVendor' Limit 1");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmOutgoingPayment(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmPurchaseInvoice' Limit 1");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmDOVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                var f = new FrmPORequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }

            if (e.ColIndex == 18 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0)
            {
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 17);
                f.ShowDialog();
            }

            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 19).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 19);
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        #endregion
    }
}
