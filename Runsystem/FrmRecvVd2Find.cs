﻿#region Update
/*
    13/10/2017 [TKG] tambah local document# dan Vendor's DO#     
    01/08/2019 [TKG] tambah informasi kawasan berikat
    28/08/2019 [WED] BUG ambiguous Discount
    28/01/2020 [WED/YK] receiving yg bukan auto DO tidak bisa di find disini
    20/03/2020 [HAR/KBN] tambah filter item category 
    06/07/2020 [TKG/IMS] tambah specification dan item local code
    11/03/2021 [TKG/IMS] menambah heat number
    20/06/2021 [TKG/PHT] 
        profit center dan cost center divalidasi berdasarkan otorisasi group terhadap cost center,
        cost center yg dimunculkan adalah cost center yang tidak menjadi parent di cost center yg lain.
    13/09/2022 [HPH] tambah filter vendor category
    31/01/2023 [BRI/SIER] tambah filter department
    27/04/2023 [SET/BBT] hide warehouse berdasar param WhsRecvVd2ForAsset
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVd2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvVd2 mFrmParent;
        private string mSQL = string.Empty;
        

        #endregion

        #region Constructor

        public FrmRecvVd2Find(FrmRecvVd2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                //Sl.SetLueVdCode(ref LueVdCode);
                SetLueVdCode2(ref LueVdCode, string.Empty);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.VdDONo, B.CancelInd, E.WhsName, D.VdName, A.CurCode, B.ItCode, C.ItName, C.ItCodeInternal, C.Specification, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.UPrice, B.Qty, C.InventoryUomCode, B.Qty2, C.InventoryUomCode2, B.Qty3, C.InventoryUomCode3, B.Discount, B.RoundingValue, ");
            SQL.AppendLine("((B.UPrice*B.QtyPurchase)-(B.Discount*0.01*(B.UPrice*B.QtyPurchase))+B.RoundingValue) As Amt, ");
            SQL.AppendLine("A.KBPackaging, A.KBPackagingQty, A.KBContractNo, A.KBContractDt, A.KBPLNo, A.KBPLDt, A.KBRegistrationNo, A.KBRegistrationDt, A.KBNonDocInd, A.KBSubmissionNo, A.CustomsDocCode, ");
            if (mFrmParent.mIsRecvVd2HeatNumberEnabled)
                SQL.AppendLine("B.HeatNumber, ");
            else
                SQL.AppendLine("Null As HeatNumber, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt, C.ItGrpCode, C.ForeignName ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblVendor D On A.VdCode=D.VdCode ");
            SQL.AppendLine("Inner Join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=E.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblPODtl F On B.PODocNo=F.DocNo And B.PODNo=F.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl G On F.PORequestDocNo=G.DocNo And F.PORequestDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr H On G.MaterialRequestDocNo=H.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (H.SiteCode Is Null Or ( ");
                SQL.AppendLine("    H.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(H.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("    And (H.DeptCode Is Null Or ( ");
                SQL.AppendLine("    H.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(H.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsRecvVd2UseProfitCenter && mFrmParent.mMenuCodeForRecvVd2AutoCreateDO)
            {
                SQL.AppendLine("Inner Join TblDODeptHdr I ");
                SQL.AppendLine("    On I.RecvVdDocNo Is Not Null ");
                SQL.AppendLine("    And A.DocNo=I.RecvVdDocNo ");
                SQL.AppendLine("    And I.CCCode Is Not Null ");
                SQL.AppendLine("Inner Join TblCostCenter J ");
                SQL.AppendLine("    On J.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And I.CCCode=J.CCCode ");
                SQL.AppendLine("Inner Join TblGroupProfitCenter K On J.ProfitCenterCode= K.ProfitCenterCode ");
                SQL.AppendLine("Inner Join TblUser L On K.GrpCode=L.GrpCode And L.UserCode=@UserCode ");                
            }
            SQL.AppendLine("Where A.POInd='N' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");

            if (mFrmParent.mMenuCodeForRecvVd2AutoCreateDO)
                SQL.AppendLine("And A.DocNo In (Select Distinct RecvVdDocNo From TblDODeptHdr Where RecvVdDocNo Is Not Null) ");
            else
                SQL.AppendLine("And A.DocNo Not In (Select Distinct RecvVdDocNo From TblDODeptHdr Where RecvVdDocNo Is Not Null) ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=C.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            if (mFrmParent.mIsFilterByVendorCategory)
            {
                SQL.AppendLine("And Exists(");
                SQL.AppendLine("Select 1 From tblgroupvendorcategory");
                SQL.AppendLine("Where VdCtCode = D.VdCtCode");
                SQL.AppendLine("And GrpCode In(Select GrpCode From TblUser Where UserCode = @UserCode)");
                SQL.AppendLine(")");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 48;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Local#",
                    "Warehouse",

                    //6-10
                    "Vendor",
                    "DO#",
                    "Item's"+Environment.NewLine+"Code",
                    "",
                    "Item's Name",
                    
                    //11-15
                    "Foreign Name",                        
                    "Group",
                    "Batch#",
                    "Source",
                    "Lot",
                    
                    //16-20
                    "Bin",
                    "Currency", 
                    "Price",
                    "Quantity",
                    "UoM",
                    
                    //21-25
                    "Quantity",
                    "UoM",
                    "Quantity",
                    "UoM",
                    "Discount"+Environment.NewLine+"%",
                    
                    //26-30
                    "Rounding",
                    "Amount",
                    "Created By",
                    "Created Date", 
                    "Created Time", 
                    
                    //31-35
                    "Last Updated By", 
                    "Last Updated Date", 
                    "Last Updated Time",
                    "Packaging",
                    "Packaging Quantity",

                    //36-40
                    "Contract#",
                    "Contract Date",
                    "Packing List#",
                    "Packing List Date",
                    "Registration#",
                    
                    //41-45
                    "Registration Date",
                    "Submission#",
                    "Non Document",
                    "Customs Document Code",
                    "Local Code",

                    //46-47
                    "Specification",
                    "Heat Number"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 60, 130, 200, 
                    
                    //6-10
                    200, 120, 80, 20, 200,

                    //11-15
                    150, 150, 180, 180, 60, 

                    //16-20
                    60, 60, 120, 100, 100,
                    
                    //21-25
                    100, 100, 100, 100, 100,

                    //26-30
                    100, 120, 130, 130, 130,

                    //31-35
                    130, 130, 130, 150, 130, 
                    
                    //36-40
                    150, 120, 150, 120, 150, 
                    
                    //41-45
                    120, 150, 100, 180, 120,

                    //46-47
                    200, 200
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3, 43 });
            Sm.GrdColButton(Grd1, new int[] { 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 19, 21, 23, 25, 26, 27, 35 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 29, 32, 37, 39, 41 });
            Sm.GrdFormatTime(Grd1, new int[] { 30, 33 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 14, 21, 22, 23, 24, 28, 29, 30 , 31, 32, 33 }, false);
            if (!mFrmParent.mIsKawasanBerikatEnabled) Sm.GrdColInvisible(Grd1, new int[] { 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, 
            new int[] { 
                0, 
                1, 2, 3, 4, 5, 6, 8, 10, 
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                41, 42, 43, 44, 45, 46, 47 
            });
            if (!mFrmParent.mIsShowForeignName) Grd1.Cols[11].Visible = false;
            if (!mFrmParent.mIsItGrpCodeShow) Grd1.Cols[12].Visible = false;
            if (!mFrmParent.mIsRecvVd2ShowSpecificationEnabled) Grd1.Cols[46].Visible = false;
            Grd1.Cols[45].Move(11);
            Grd1.Cols[46].Move(12);
            if (mFrmParent.mIsRecvVd2HeatNumberEnabled)
                Grd1.Cols[47].Move(17);
            else
                Grd1.Cols[47].Visible = false;
            if (mFrmParent.mWhsRecvVd2ForAsset.Length > 0)
                Grd1.Cols[5].Visible = false;
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 14, 28, 29, 30, 31, 32, 33 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22 }, true);
            
            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And B.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "A.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtVdDONo.Text, "A.VdDONo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItName", "C.ForeignName", "C.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSource.Text, "B.Source", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "LocalDocNo", "WhsName", "VdName", 
                            
                            //6-10
                            "VdDONo", "ItCode", "ItName", "ForeignName", "ItGrpCode", 
                            
                            //11-15
                            "BatchNo", "Source", "Lot", "Bin", "CurCode", 
                            
                            //16-20
                            "UPrice", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                            
                            //21-25
                            "Qty3", "InventoryUomCode3", "Discount", "RoundingValue", "Amt", 
                            
                            //26-30
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "KBPackaging", 
                            
                            //31-35
                            "KBPackagingQty", "KBContractNo", "KBContractDt", "KBPLNo", "KBPLDt", 
                            
                            //36-40
                            "KBRegistrationNo", "KBRegistrationDt", "KBSubmissionNo", "KBNonDocInd", "CustomsDocCode",

                            //41-43
                            "ItCodeInternal", "Specification", "HeatNumber"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 29, 27);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 32, 29);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 33, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 30);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 32);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 37, 33);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 34);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 39, 35);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 36);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 41, 37);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 38);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 43, 39);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 40);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 45, 41);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 46, 42);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 47, 43);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 19, 21, 23, 26, 27, 35 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (Grd1.Rows.Count <= 1)
                    Sm.FocusGrd(Grd1, 0, 0);
                else
                    Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Additional methods
        internal void SetLueVdCode2(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.VdCode As Col1 ");
            if (Sm.GetParameterBoo("IsVendorComboShowCategory"))
                SQL.AppendLine(", CONCAT(T.VdName, ' [', IFNULL(T2.VdCtName, ' '), ']') As Col2 ");
            else
                SQL.AppendLine(", T.VdName As Col2 ");
            SQL.AppendLine("From TblVendor T ");
            SQL.AppendLine("LEFT JOIN TblVendorCategory T2 ON T.VdCtCode = T2.VdCtCode ");
            //SQL.AppendLine("inner JOIN tblgroupvendorcategory T3 ON T.VdCtCode = T3.VdCtCode");

            if (Code.Length > 0)
            {
                SQL.AppendLine("Where T.VdCode=@Code ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (mFrmParent.mIsFilterByVendorCategory)
                {
                    SQL.AppendLine("And Exists(");
                    SQL.AppendLine("Select 1 From tblgroupvendorcategory");
                    SQL.AppendLine("Where VdCtCode = T.VdCtCode");
                    SQL.AppendLine("And GrpCode In(Select GrpCode From TblUser Where UserCode = @UserCode)");
                    SQL.AppendLine(");");
                }
            }



            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 8));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 8));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode2), string.Empty); ;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSource_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Source");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdDONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor's DO#");
        }

        #endregion  
        
        #endregion
    }
}
