﻿namespace RunSystem
{
    partial class FrmParameterFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtParCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkParCode = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtParCtName = new DevExpress.XtraEditors.TextEdit();
            this.ChkParCtName = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkParCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkParCtName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtParCtName);
            this.panel2.Controls.Add(this.ChkParCtName);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtParCode);
            this.panel2.Controls.Add(this.ChkParCode);
            this.panel2.Size = new System.Drawing.Size(672, 62);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 411);
            this.Grd1.TabIndex = 15;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Parameter";
            // 
            // TxtParCode
            // 
            this.TxtParCode.EnterMoveNextControl = true;
            this.TxtParCode.Location = new System.Drawing.Point(74, 8);
            this.TxtParCode.Name = "TxtParCode";
            this.TxtParCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParCode.Properties.Appearance.Options.UseFont = true;
            this.TxtParCode.Properties.MaxLength = 40;
            this.TxtParCode.Size = new System.Drawing.Size(301, 20);
            this.TxtParCode.TabIndex = 10;
            this.TxtParCode.Validated += new System.EventHandler(this.TxtParCode_Validated);
            // 
            // ChkParCode
            // 
            this.ChkParCode.Location = new System.Drawing.Point(378, 6);
            this.ChkParCode.Name = "ChkParCode";
            this.ChkParCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkParCode.Properties.Appearance.Options.UseFont = true;
            this.ChkParCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkParCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkParCode.Properties.Caption = " ";
            this.ChkParCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkParCode.Size = new System.Drawing.Size(31, 22);
            this.ChkParCode.TabIndex = 11;
            this.ChkParCode.ToolTip = "Remove filter";
            this.ChkParCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkParCode.ToolTipTitle = "Run System";
            this.ChkParCode.CheckedChanged += new System.EventHandler(this.ChkParCode_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Category";
            // 
            // TxtParCtName
            // 
            this.TxtParCtName.EnterMoveNextControl = true;
            this.TxtParCtName.Location = new System.Drawing.Point(74, 33);
            this.TxtParCtName.Name = "TxtParCtName";
            this.TxtParCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtParCtName.Properties.MaxLength = 40;
            this.TxtParCtName.Size = new System.Drawing.Size(301, 20);
            this.TxtParCtName.TabIndex = 13;
            this.TxtParCtName.Validated += new System.EventHandler(this.TxtParCtName_Validated);
            // 
            // ChkParCtName
            // 
            this.ChkParCtName.Location = new System.Drawing.Point(378, 31);
            this.ChkParCtName.Name = "ChkParCtName";
            this.ChkParCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkParCtName.Properties.Appearance.Options.UseFont = true;
            this.ChkParCtName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkParCtName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkParCtName.Properties.Caption = " ";
            this.ChkParCtName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkParCtName.Size = new System.Drawing.Size(31, 22);
            this.ChkParCtName.TabIndex = 14;
            this.ChkParCtName.ToolTip = "Remove filter";
            this.ChkParCtName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkParCtName.ToolTipTitle = "Run System";
            this.ChkParCtName.CheckedChanged += new System.EventHandler(this.ChkParCtName_CheckedChanged);
            // 
            // FrmParameterFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmParameterFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkParCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkParCtName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtParCode;
        private DevExpress.XtraEditors.CheckEdit ChkParCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtParCtName;
        private DevExpress.XtraEditors.CheckEdit ChkParCtName;
    }
}