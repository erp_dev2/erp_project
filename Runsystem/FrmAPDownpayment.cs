﻿#region Update
/*
    04/12/2017 [HAR] tambah Local Document defaultnya ambil dari PO 
    03/05/2017 [TKG] nilai PO ditambah customs tax.
    18/05/2017 [TKG] tambah info downpayment dari dokumen ap downpayment yg lain
    26/10/2017 [ARI] bug printout
    14/11/2017 [WED] tambahan opsi untuk Paid To Bank apakah berdasarkan vendor atau tidak.
    18/12/2017 [TKG] department ada opsi difilter berdasarkan otorisasi di group
    15/03/2017 [TKG] tambah tax.
    28/04/2018 [TKG] tambah info pi di grid document qr code.
    02/05/2018 [ARI] tambah printout kim
    05/05/2018 [TKG] set voucher request document number berdasarkan standard document
    15/05/2018 [TKG] filter berdasarkan entity berdasarkan paramere IsFilterByEnt
    19/07/2018 [TKG] warning saat print apabila belum diapprove.
    05/10/2018 [HAR] tambah payment date dan ftp file
    06/10/2018 [TKG] update dept di vr berdasarkan setting approval
    24/05/2020 [TKG/MMM] Berdasarkan parameter MenuCodeForVendorAPDownpaymentWithoutPO, Vendor's AP Downpayment bisa diinput tanpa PO
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    03/03/2021 [RDH/YK] Menambakan field bank account di APDP 
    10/03/2021 [VIN/KSM] bug: saat IsAPDownpaymentUseBankAccount = N tidak muncul warning lue empty 
    25/03/2021 [TKG/IMS] kemungkinan bug: untuk DP dengan PO, currency harusnya sama dengan currency PO
    17/06/2021 [WED/IOK] bisa pilih vendor tanpa harus ada PO nya, berdasarkan parameter VdCtCodeForRawMaterial
    14/07/2021 [VIN/IOK] bug show data vendor tidak muncul parameter VdCtCodeForRawMaterial
    19/07/2021 [DITA/PHT] tambah fieldtype untuk AcNo based on param : IsItemCategoryUseCOAAPAR
    13/08/2021 [VIN/ALL] BUG: TxtGiroNo hilang
    03/09/2021 [BRI/AMKA] Department mandatory param IsAPDPDepartmentMandatory
    15/09/2021 [VIN/AMKA] bug validasi department IsAPDPDepartmentMandatory
    25/11/2021 [BRI/AMKA] Merubah rujukan amount dari item's category berdasarkan param APDPCOAAmtCalculationMethod
    14/12/2021 [IBL/AMKA] Tambah CheckBox AddCost dan akan berpengaruh ketika checkbox tercentang
                          VR APDP dan VR APDP Additional Amt jadi satu berdasarkan parameter IsAPDPProcessTo1Journal
    22/12/2021 [IBL/AMKA] Feedback: Amount AddCost tidak melihat natura COA. Jika dicentang di debit maka menjadi penambah, jika dicentang di credit maka menjadi pengurang.
    08/02/2021 [TKG/GSS] ubah GetParameter dan proses save.
	09/02/2022 [RIS/PRODUCT] Menambah tax menjadi 3 dengan param IsAPDPUse3Tax
    29/06/2022 [DITA/SIER+PRODUCT] ketika param mIsEntityMandatory = N maka entcode ambil dari entity yg diisikan, jika null maka ambil dari param : IsAPDownpaymentUseDefaultEntity->DefaultEntity
    30/06/2022 [DITA/SIER+PRODUCT] saat param IsAPDPProcessTo1Journal aktif maka downpayment amount ketambahan nilai coa, termasuk vr nya juga
    07/07/2022 [TRI/AMKA] bug TxtTaxAmt2 angka tidak tampil saat show data
    08/07/2022 [DITA/SIER+PRODUCT] Bug saat show apdp, downpayment after tax masih include nilai coa
    28/07/2022 [IBL/SIER] memunculkan tombol print ketika dipanggil dari docapproval & menu lain
    02/08/2022 [TYO/SIER] filter group vendor berdasarkan parameter IsFilterByVendorCategory
    04/08/2022 [SET/SIER] penyesuaian filter group vendor berdasarkan parameter IsFilterByVendorCategory
    11/08/2022 [TYO/SIER] menanbah kolom jabatan di tab approval
    16/08/2022 [MAU/PRODUCT] Membuat validasi COA berdasarkan apa yang telah terpasang di Group (system tools).
    16/09/2022 [MYA/SIER] FEEDBACK : Memunculkan seluruh approver pada tab approval (seperti pada monitoring document approval setting) dan menambahkan kolom jabatan
    14/11/2022 [IBL/BBT] Menambah tab upload file. Berdasarkan IsAPDPAllowToUploadFile
    15/11/2022 [IBL/BBT] Penyesuaian print out
    28/11/2022 [IBL/BBT] Bug : Saat upload file kena validasi, tetap menyimpan dokumen tanpa mengupload file.
    28/11/2022 [IBL/BBT] FEEDBACK : Letak approval printout belum sesuai
    03/02/2023 [SET/BBT] add field Site, menampilkan Site ketika Find & Printout
    28/03/2023 [RDA/MNET] bug fix untuk param mIsAPDownpaymentPaidToBankBasedOnVendor
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmAPDownpayment : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mVdCode = string.Empty,
            mVdCtCodeForRawMaterial = string.Empty,
            mMainCurCode = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mDocTitle = string.Empty;
        internal FrmAPDownpaymentFind FrmFind;
        private bool
            mIsRemarkForApprovalMandatory = false,
            mIsAPDownpaymentApprovalBasedOnDept = false,
            IsFilterBySite = false,
            mIsAPDownpaymentUseCOA = false,
            mIsAPDownpaymentPaidToBankBasedOnVendor = false,
            mIsEntityMandatory = false,
            mIsAPDownpaymentUseTax = false,
            mIsAPDownpaymentUseQRCode = false,
            mIsAPDownpaymentAbleToEditTax = false,
            mIsItemCategoryUseCOAAPAR = false,
            mIsAPDPDepartmentMandatory = false,
            mIsAPDPProcessTo1Journal = false,
            mIsAPDPUse3Tax = false,
            mIsAPDownpaymentUseDefaultEntity = false,
            mIsTransactionUseDetailApprovalInformation = false,
            mIsAPDPUploadFileMandatory = false;
        public bool mIsAPDPAllowToUploadFile = false;
        internal bool
            mIsFilterByDept = false,
            mIsFilterByEnt = false,
            mIsWithoutPO = false,
            mIsAPDownpaymentUseBankAccount = false,
            mIsFilterByVendorCategory = false,
            mIsCOAFilteredByGroup = false,
            mIsAPDownpaymentUseSite = false;
        private string
            mPIQRCodeTaxDocType = string.Empty,
            mVoucherCodeFormatType = string.Empty,
            mAPDPCOAAmtCalculationMethod = string.Empty,
            mDefaultEntity = string.Empty
            ;
        iGCell fCell;
        bool fAccept;
        public string
           mPortForFTPClient = string.Empty,
           mHostAddrForFTPClient = string.Empty,
           mSharedFolderForFTPClient = string.Empty,
           mUsernameForFTPClient = string.Empty,
           mPasswordForFTPClient = string.Empty,
           mFileSizeMaxUploadFTPClient = string.Empty;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmAPDownpayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Vendor's AP Downpayment";

                GetParameter();
                SetGrd();
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);

                TcAPDownpayment.SelectedTabPage = TpAdditional;
                Sl.SetLueOption(ref LueAcNoType, "AcNoTypeForAPDP");
                TpAdditional.PageVisible = mIsItemCategoryUseCOAAPAR;

                TcAPDownpayment.SelectedTabPage = TpTax;
                Sl.SetLueTaxCode(ref LueTaxCode);
                Sl.SetLuePurchaseInvoiceDocType(ref LueDocType);
                Sl.SetLuePurchaseInvoiceDocInd(ref LueDocInd);
                LueDocType.Visible = false;
                LueDocInd.Visible = false;
                TpTax.PageVisible = mIsAPDownpaymentUseTax;

                TcAPDownpayment.SelectedTabPage = TpCOA;
                TpCOA.PageVisible = mIsAPDownpaymentUseCOA;

                TcAPDownpayment.SelectedTabPage = TpUploadFile;
                if (mIsAPDPUploadFileMandatory)
                    lblFileName.ForeColor = Color.Red;
                TpUploadFile.PageVisible = mIsAPDPAllowToUploadFile;

                TcAPDownpayment.SelectedTabPage = TpGeneral;
                Sl.SetLueVdCode(ref LueVdCode, mIsFilterByVendorCategory ? "Y" : "N");
                if (mIsWithoutPO) LblVdCode.ForeColor = Color.Red;
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueUserCode(ref LuePIC);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                if (mIsAPDownpaymentUseSite)
                {
                    Sl.SetLueSiteCode(ref LueSiteCode);
                }
                if(mIsWithoutPO && !mIsAPDownpaymentUseSite)
                {
                    LblSiteCode.Visible = false; LueSiteCode.Visible = false;
                    LblVoucherRequest.Top = 247; TxtVoucherRequestDocNo.Top = 245;
                    LblVoucher.Top = 269; TxtVoucherDocNo.Top = 266;
                    LblRemark.Top = 290; MeeRemark.Top = 287;
                }

                if (!mIsAPDownpaymentUseBankAccount) label10.Visible = LueBankAcCode.Visible = false;
                
                if (!mIsAPDownpaymentPaidToBankBasedOnVendor)
                    Sl.SetLueBankCode(ref LuePaidToBankCode);
                if (mIsEntityMandatory) LblEntCode.ForeColor = Color.Red;
                Sl.SetLueEntCode(ref LueEntCode);
                if (mIsAPDownpaymentApprovalBasedOnDept || mIsAPDPDepartmentMandatory)
                    LblDeptCode.ForeColor = Color.Red;

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    if(mDocTitle != "SIER") BtnPrint.Visible = false;
                }
				
				if(!mIsAPDPUse3Tax)
                {
                    LueTaxCode2.Visible = TxtTaxInvoiceNo2.Visible = DteTaxInvoiceDt2.Visible = TxtTaxAmt2.Visible = false;
                    LueTaxCode3.Visible = TxtTaxInvoiceNo3.Visible = DteTaxInvoiceDt3.Visible = TxtTaxAmt3.Visible = false;
                }
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsAPDownpaymentApprovalBasedOnDept = Sm.IsApprovalNeedXCode("APDownpayment", "Dept");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'APDPCOAAmtCalculationMethod', 'VdCtCodeForRawMaterial', 'PIQRCodeTaxDocType', 'VoucherCodeFormatType', ");
            SQL.AppendLine("'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient', 'IsAPDPDepartmentMandatory', 'IsItemCategoryUseCOAAPAR', 'IsAPDPProcessTo1Journal', 'IsAPDownpaymentUseBankAccount', ");
            SQL.AppendLine("'IsAPDPAllowToUploadFile', 'IsAPDownpaymentAbleToEditTax', 'IsFilterByEnt', 'IsAPDownpaymentUseQRCode', 'IsAPDownpaymentUseTax', ");
            SQL.AppendLine("'IsFilterBySite', 'IsAPDownpaymentUseCOA', 'IsEntityMandatory', 'IsAPDownpaymentPaidToBankBasedOnVendor', 'IsFilterByDept', ");
            SQL.AppendLine("'IsAPDownpaymentRemarkForApprovalMandatory', 'MenuCodeForVendorAPDownpaymentWithoutPO', 'IsAPDPUse3Tax', 'IsAPDownpaymentUseDefaultEntity', 'DefaultEntity','DocTitle', ");
            SQL.AppendLine("'IsFilterByVendorCategory', 'IsCOAFilteredByGroup', 'IsTransactionUseDetailApprovalInformation','IsAPDPUploadFileMandatory', 'IsAPDownpaymentUseSite' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "MenuCodeForVendorAPDownpaymentWithoutPO": mIsWithoutPO = Sm.CompareStr(ParValue, mMenuCode); ; break;
                            case "IsAPDownpaymentPaidToBankBasedOnVendor": mIsAPDownpaymentPaidToBankBasedOnVendor = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseCOA": mIsAPDownpaymentUseCOA = ParValue == "Y"; break;
                            case "IsEntityMandatory": mIsEntityMandatory = ParValue == "Y"; break;
                            case "IsFilterBySite": IsFilterBySite = ParValue == "Y"; break;
                            case "IsAPDPAllowToUploadFile": mIsAPDPAllowToUploadFile = ParValue == "Y"; break;
                            case "IsAPDownpaymentAbleToEditTax": mIsAPDownpaymentAbleToEditTax = ParValue == "Y"; break;
                            case "IsAPDownpaymentRemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsFilterByEnt": mIsFilterByEnt = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseQRCode": mIsAPDownpaymentUseQRCode = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseTax": mIsAPDownpaymentUseTax = ParValue == "Y"; break;
                            case "IsAPDPDepartmentMandatory": mIsAPDPDepartmentMandatory = ParValue == "Y"; break;
                            case "IsItemCategoryUseCOAAPAR": mIsItemCategoryUseCOAAPAR = ParValue == "Y"; break;
                            case "IsAPDPProcessTo1Journal": mIsAPDPProcessTo1Journal = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseBankAccount": mIsAPDownpaymentUseBankAccount = ParValue == "Y"; break;
                            case "IsAPDPUse3Tax": mIsAPDPUse3Tax = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseDefaultEntity": mIsAPDownpaymentUseDefaultEntity = ParValue == "Y"; break;
                            case "IsFilterByVendorCategory": mIsFilterByVendorCategory = ParValue == "Y"; break;
                            case "IsCOAFilteredByGroup": mIsCOAFilteredByGroup = ParValue == "Y"; break;
                            case "IsTransactionUseDetailApprovalInformation": mIsTransactionUseDetailApprovalInformation = ParValue == "Y"; break;
                            case "IsAPDPUploadFileMandatory": mIsAPDPUploadFileMandatory = ParValue == "Y"; break;
                            case "IsAPDownpaymentUseSite": mIsAPDownpaymentUseSite = ParValue == "Y"; break;

                            //string
                            case "APDPCOAAmtCalculationMethod": mAPDPCOAAmtCalculationMethod = ParValue; break;
                            case "VdCtCodeForRawMaterial": mVdCtCodeForRawMaterial = ParValue; break;
                            case "PIQRCodeTaxDocType": mPIQRCodeTaxDocType = ParValue; break;
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "DefaultEntity": mDefaultEntity = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            
                        }
                    }
                }
                dr.Close();
            }


            if (mVoucherCodeFormatType.Length == 0) mVoucherCodeFormatType = "1";
            if (mAPDPCOAAmtCalculationMethod.Length == 0) mAPDPCOAAmtCalculationMethod = "1";
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 6;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "User", 
                        "Jabatan",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 200, 200, 100, 200 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5 });

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 9;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6-8
                        "", //Checkbox debit
                        "", //Checkbox credit
                        "" //CheckBox Db Cr Indicator
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                        //6-8
                        20, 20, 0
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColCheck(Grd2, new int[] { 6, 7, 8 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd2, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2 });
            if (!mIsAPDPProcessTo1Journal)
                Sm.GrdColInvisible(Grd2, new int[] { 6, 7 });
            Grd2.Cols[6].Move(3);
            Grd2.Cols[7].Move(5);

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 14;
            Grd3.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Document Type Code", 
                        "Type", 
                        "QR Code", 
                        "", 
                        "Document#",  
                        
                        //6-10
                        "Tax Invoice"+Environment.NewLine+"Date", 
                        "Amount", 
                        "Tax", 
                        "Indicator Code", 
                        "Indicator", 
                        
                        //11-13
                        "Remark", 
                        "RemarkXML",
                        "Invoice#"
                    },
                    new int[] 
                    { 
                        //0
                        0, 

                        //1-5
                        0, 150, 250, 20, 180, 
                        
                        //6-10
                        120, 150, 150, 100, 100, 

                        //11-13
                        250, 0, 140
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 4 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 9, 13 });
            Sm.GrdFormatDate(Grd3, new int[] { 6 });
            Sm.GrdFormatDec(Grd3, new int[] { 7, 8 }, 0);
            if (!mIsAPDownpaymentUseQRCode)
                Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 3, 4, 8, 9, 12, 13 }, false);
            else
                Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 9, 12 }, false);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    {  
                        DteDocDt, ChkCancelInd, TxtLocalDocNo, LueCurCode, TxtAmt, 
                        LuePIC, LueDeptCode, MeeCancelReason, MeeRemark, LuePaymentType,LueBankAcCode, 
                        LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt, TxtPaymentUser, 
                        LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, LueEntCode, 
                        TxtAmtBefTax, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode, TxtTaxAmt, 
                        DtePaymentDt, TxtFile, TxtFile2, TxtFile3, LueVdCode, LueAcNoType, LueTaxCode2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, TxtTaxAmt2,
                        LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3, TxtTaxAmt3, LueSiteCode
                    }, true);
                    
                    BtnPODocNo.Enabled = false;
                    ChkFile.Enabled = false;
                    BtnUpload.Enabled = false;
                    BtnDownload.Enabled = true;
                    ChkFile2.Enabled = false;
                    BtnUpload2.Enabled = false;
                    BtnDownload2.Enabled = true;
                    ChkFile3.Enabled = false;
                    BtnUpload3.Enabled = false;
                    BtnDownload3.Enabled = true;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 3, 4, 5, 6, 7, 8 });
                   
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocalDocNo, DteDocDt, LueCurCode, LuePIC, LueDeptCode, 
                        LuePaymentType, MeeRemark, TxtPaymentUser, LueEntCode, TxtAmtBefTax, 
                        TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode, TxtTaxAmt, DtePaymentDt,LueBankAcCode, LueAcNoType,
                        LueTaxCode2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, TxtTaxAmt2, LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3, TxtTaxAmt3,                    
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueVdCode }, !mIsWithoutPO);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCurCode }, !mIsWithoutPO);
                    if (!mIsAPDownpaymentPaidToBankBasedOnVendor)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo }, false);
                    if (!mIsAPDownpaymentUseTax) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtAmt }, false);
                    if (mIsAPDownpaymentUseSite) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueSiteCode }, false);
                    BtnPODocNo.Enabled = !mIsWithoutPO;

                    ChkFile.Enabled = true;
                    BtnUpload.Enabled = true;
                    BtnDownload.Enabled = false;
                    ChkFile2.Enabled = true;
                    BtnUpload2.Enabled = true;
                    BtnDownload2.Enabled = false;
                    ChkFile3.Enabled = true;
                    BtnUpload3.Enabled = true;
                    BtnDownload3.Enabled = false;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 3, 4, 5, 6, 7 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    if (mIsAPDownpaymentAbleToEditTax)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode, TxtTaxAmt }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mVdCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, TxtPODocNo, TxtNoOfDownpayment, 
                TxtLocalDocNo, TxtPOLocalDocNo, LueVdCode, TxtPOCurCode, LueCurCode, 
                LuePIC, LueDeptCode, LuePaymentType, LueBankCode, LueBankAcCode, TxtGiroNo, 
                DteOpeningDt, DteDueDt, MeeCancelReason, TxtVoucherRequestDocNo, TxtVoucherDocNo, 
                MeeRemark, TxtVoucherDocNo2, TxtVoucherRequestDocNo2, TxtPaymentUser, LuePaidToBankCode, 
                TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, LueEntCode, TxtTaxInvoiceNo, 
                DteTaxInvoiceDt, LueTaxCode, DtePaymentDt, TxtFile, TxtFile2, TxtFile3, LueAcNoType, LueTaxCode2,
                TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {  
                TxtPOAmt, TxtOtherAmt, TxtAmt, TxtCOAAmt, TxtAmtBefTax,
                TxtTaxAmt, TxtAmtAftTax, TxtTaxAmt2, TxtTaxAmt3
            }, 0);
            
            ChkCancelInd.Checked = false;
            ChkFile.Checked = false;
            ChkFile2.Checked = false;
            ChkFile3.Checked = false;
            PbUpload.Value = 0;
            PbUpload2.Value = 0;
            PbUpload3.Value = 0;

            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;

            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;

            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 7, 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAPDownpaymentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                Sl.SetLueEntCode(ref LueEntCode, string.Empty, mIsFilterByEnt ? "Y" : "N");
                if (!mIsWithoutPO) BtnPODocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
			if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No || 
                    Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                    IsDataAlreadyCancelled()
                    ) return;

                string Doctitle = Sm.GetParameter("DocTitle");
                string[] TableName = { "APDPHdr", "APDPDtl", "APDPDtlBBT" };
                var l = new List<APDPHdr>();
                var lDtl = new List<APDPDtl>();
                var lDtl2 = new List<APDPDtlBBT>();
                var myLists = new List<IList>();
               
                #region Header

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                if (IsFilterBySite && Doctitle != "BBT")
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, K.Company, K.Phone, K.Address,  ");
                }
                else
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
                }
                SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, A.PoDocNo, A.CurCode As POCurCode, ");
                SQL.AppendLine("F.Total+ if(length(A2.taxCode1>0), (F.Total*B.Taxrate*0.01), 0)+  ");
                SQL.AppendLine("if(length(A2.taxCode2>0), (F.Total*C.Taxrate*0.01), 0)+   ");
                SQL.AppendLine("if(length(A2.taxCode3>0), (F.Total*D.Taxrate*0.01), 0)- ifnull(A2.DiscountAmt, 0) As POAmt, A.Amt, ");
                if (mIsWithoutPO) SQL.AppendLine("E2.VdName, ");
                else SQL.AppendLine("E.VdName, ");
                if (mIsWithoutPO && mIsAPDownpaymentUseSite)
                    SQL.AppendLine("V.SiteName, ");
                else
                    SQL.AppendLine("J.SiteName, ");
                SQL.AppendLine("A.VoucherRequestDocNo, G.VoucherDocNo,    ");
                SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
                SQL.AppendLine("H.Deptname, A.CreateBy, Date_Format(left(A.CreateDt, 8),'%d %M %Y') As CreateDt, A.Remark,  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='IsFilterBySite') As SiteInd, P.UserName As PIC, A.PaymentUser, A.GiroNo, ");
                SQL.AppendLine("L.BankName As GiroBankName, M.OptDesc As PaymentType, Date_Format(A.OpeningDt,'%d %M %Y')As GiroDt, ");
                SQL.AppendLine("N.BankName As PaidToBankName, A.PaidToBankBranch, A.PaidToBankAcName, A.PaidToBankAcNo, ");
                SQL.AppendLine("E.Address As VdAddress, O.CityName As VdCity, G.LocalDocNo As VRLocal, ");
                SQL.AppendLine("IfNull(A.AmtBefTax, 0.00) As AmtBefTax, ");
                if (mIsAPDPUse3Tax)
                    SQL.AppendLine("Concat(IfNull(Q.TaxName, ''), '\n', IfNull(T.TaxName, ''), '\n', IfNull(U.TaxName, '')) As APDPTax, ");
                else
                    SQL.AppendLine("Q.TaxName As APDPTax, ");
                SQL.AppendLine("A.CurCode As APDPCurCode, S.GrpName ");
                SQL.AppendLine("From TblAPDownpayment A  ");
                SQL.AppendLine("Left Join TblPOHdr A2 On A.PODocNo=A2.DocNo  ");
                SQL.AppendLine("Left Join TblTax B On A2.TaxCode1 = B.taxCode  ");
                SQL.AppendLine("Left Join TblTax C On A2.TaxCode2 = C.taxCode  ");
                SQL.AppendLine("Left Join TblTax D On A2.TaxCode3 = D.taxCode  ");
                SQL.AppendLine("Left Join TblVendor E On A2.VdCode = E.VdCode  ");
                SQL.AppendLine("Left Join TblVendor E2 On A.VdCode = E2.VdCode ");
                SQL.AppendLine("Left Join   ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select B.DocNo, ");
                SQL.AppendLine("    SUM((H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue) As Total ");
                SQL.AppendLine("    From TblPODtl B ");
                SQL.AppendLine("    Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                SQL.AppendLine("    Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
                SQL.AppendLine("    Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                SQL.AppendLine("    Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                SQL.AppendLine("    Left Join TblPOQtyCancel M On B.Docno = M.PODocno And B.DNo = M.PODno And M.CancelInd='N' ");
                SQL.AppendLine("    Where B.CancelInd='N' ");
                SQL.AppendLine("    And B.DocNo=@PODocNo ");
                SQL.AppendLine("    Group By B.DocNo ");
                SQL.AppendLine(")F On A2.Docno = F.DocNo ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr G On A.VoucherRequestDocNo=G.DocNo ");
                SQL.AppendLine("Left Join TblDepartment H On A.DeptCode = H.DeptCode ");
                SQL.AppendLine("Left Join tblSite J On A2.SiteCode = J.SiteCode ");
                SQL.AppendLine("Left Join TblBank L On A.BankCode=L.BankCode ");
                SQL.AppendLine("Left Join TblOption M On A.PaymentType=M.OptCode And M.OptCat='VoucherPaymentType' ");
                SQL.AppendLine("Left Join TblBank N On A.PaidToBankCode=N.BankCode ");
                SQL.AppendLine("Left Join TblCity O On E.CityCode=O.CityCode ");
                SQL.AppendLine("Left Join TblUser P On A.PIC=P.UserCode ");
                if (IsFilterBySite && Doctitle != "BBT")
                {
                    SQL.AppendLine("Left Join (");
                    SQL.AppendLine("    Select distinct A.DocNo, D.EntName As Company, D.EntPhone As Phone,  D.EntAddress As Address ");
                    SQL.AppendLine("    From TblPOhdr A  ");
                    SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                    SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                    SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                    SQL.AppendLine("    Where A.DocNo=@PODocNo ");
                    SQL.AppendLine(")K On A.PODocNo = K.DocNo ");
                }
                SQL.AppendLine("Left Join TblTax Q On A.TaxCode = Q.TaxCode ");
                if (mIsAPDPUse3Tax)
                {
                    SQL.AppendLine("Left Join TblTax T On A.TaxCode2 = T.TaxCode ");
                    SQL.AppendLine("Left Join TblTax U On A.TaxCode3 = U.TaxCode ");
                }
                SQL.AppendLine("Inner Join TblUser R On A.CreateBy = R.UserCode ");
                SQL.AppendLine("Inner Join TblGroup S On R.GrpCode = S.GrpCode ");
                if(mIsWithoutPO && mIsAPDownpaymentUseSite)
                    SQL.AppendLine("Left Join TblSite V On A.SiteCode = V.SiteCode ");

                SQL.AppendLine("Where A.DocNo=@DocNo;");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
                    if (IsFilterBySite && Doctitle != "BBT")
                    {
                        string CompanyLogo = Sm.GetValue(
                           "Select D.EntLogoName " +
                           "From TblPOhdr A  " +
                           "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                           "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                           "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                           "Where A.Docno='" + TxtPODocNo.Text + "' "
                       );
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    }

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                            {
                             //0
                             "CompanyLogo",
                             //1-5
                             "Company",
                             "Address",
                             "Phone",
                             "DocNo",
                             "DocDt",
                             //6-10
                             "PODocNo",
                             "POCurCode",
                             "POAmt",
                             "VDName",
                             "VoucherRequestDocNo",
                             //11-15
                             "VoucherDocNo",
                             "CreateBy",
                             "DeptName",
                             "SiteName",
                             "StatusDEsc",
                             //16-20
                             "Amt",
                             "CreateDt",
                             "Remark",
                             "SiteInd",
                             "PIC",
                             //21-25
                             "PaymentUser",
                             "GiroNo",
                             "GiroBankName",
                             "PaymentType",
                             "GiroDt", 

                             //26-30
                             "PaidToBankName",
                             "PaidToBankBranch",
                             "PaidToBankAcName",
                             "PaidToBankAcNo",
                             "VdCity",

                             //31-35
                             "VdAddress",
                             "VRLocal",
                             "AmtBefTax",
                             "APDPTax",
                             "APDPCurCode",

                             //36
                             "GrpName"
                            });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new APDPHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                DocDt = Sm.DrStr(dr, c[5]),

                                PODocNo = Sm.DrStr(dr, c[6]),
                                POCurCode= Sm.DrStr(dr, c[7]),
                                POAmt = Sm.DrDec(dr, c[8]),
                                VDName = Sm.DrStr(dr, c[9]),
                                VoucherRequestDocNo = Sm.DrStr(dr, c[10]),

                                VoucherDocNo = Sm.DrStr(dr, c[11]),
                                CreateBy = Sm.DrStr(dr, c[12]),
                                DeptName = Sm.DrStr(dr, c[13]),
                                SiteName = Sm.DrStr(dr, c[14]),
                                StatusDEsc = Sm.DrStr(dr, c[15]),

                                Amt = Sm.DrDec(dr, c[16]),
                                CreateDt = Sm.DrStr(dr, c[17]),
                                Remark = Sm.DrStr(dr, c[18]),
                                SiteInd = Sm.DrStr(dr, c[19]),
                                PIC = Sm.DrStr(dr, c[20]),

                                PaymentUser = Sm.DrStr(dr, c[21]),
                                GiroNo = Sm.DrStr(dr, c[22]),
                                GiroBankName = Sm.DrStr(dr, c[23]),
                                PaymentType = Sm.DrStr(dr, c[24]),
                                GiroDt = Sm.DrStr(dr, c[25]),

                                PaidToBankName = Sm.DrStr(dr, c[26]),
                                PaidToBankBranch = Sm.DrStr(dr, c[27]),
                                PaidToBankAcName = Sm.DrStr(dr, c[28]),
                                PaidToBankAcNo = Sm.DrStr(dr, c[29]),
                                VdCity = Sm.DrStr(dr, c[30]),

                                VdAddress = Sm.DrStr(dr, c[31]),
                                VRLocal = Sm.DrStr(dr, c[32]),
                                AmtBefTax = Sm.DrDec(dr, c[33]),
                                TaxName = Sm.DrStr(dr, c[34]),
                                CurCode = Sm.DrStr(dr, c[35]),

                                GrpName = Sm.DrStr(dr, c[36]),

                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[16])),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                            });
                        }
                    }

                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region Detail Approval

                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("    Select Distinct ");
                    SQLDtl.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                    SQLDtl.AppendLine("    B.ApprovalDNo As DNo, D.Level, if(D.Level=1,'Acknowledge By','Approved By') As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl.AppendLine("    From TblAPDownpayment A ");
                    SQLDtl.AppendLine("    Inner Join TblDocApproval B On B.DocType='APDownpayment' And A.DocNo=B.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'APDownpayment' ");
                    SQLDtl.AppendLine("    Where A.DocNo=@DocNo ");
                    SQLDtl.AppendLine("    Union All ");
                    SQLDtl.AppendLine("    Select Distinct ");
                    SQLDtl.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                    SQLDtl.AppendLine("    '00' As DNo, 0 As Level, 'Prepared By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                    SQLDtl.AppendLine("    From TblAPDownpayment A ");
                    SQLDtl.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl.AppendLine("    Where A.DocNo=@DocNo ");
                    SQLDtl.AppendLine(") T1 ");
                    SQLDtl.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                    SQLDtl.AppendLine("Order By T1.DNo desc; ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                            {
                             //0
                             "Signature" ,

                             //1-5
                             "Username" ,
                             "PosName",
                             "Space",
                             "Level",
                             "Title",
                             "LastupDt"
                            });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {

                            lDtl.Add(new APDPDtl()
                            {
                                Signature = Sm.DrStr(drDtl, cDtl[0]),
                                UserName = Sm.DrStr(drDtl, cDtl[1]),
                                PosName = Sm.DrStr(drDtl, cDtl[2]),
                                Space = Sm.DrStr(drDtl, cDtl[3]),
                                DNo = Sm.DrStr(drDtl, cDtl[4]),
                                Title = Sm.DrStr(drDtl, cDtl[5]),
                                LastUpDt = Sm.DrStr(drDtl, cDtl[6])
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(lDtl);
                #endregion

                #region Detail Approval BBT

                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();

                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T3.ParValue, ''), T1.UserName, '.JPG') As Signature, ");
                    SQLDtl2.AppendLine("T1.UserName, T2.GrpName As PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl2.AppendLine("From ( ");
                    SQLDtl2.AppendLine("	Select Distinct ");
                    SQLDtl2.AppendLine("	B.UserName, B.GrpCode, '00' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("	From TblAPDownpayment A ");
                    SQLDtl2.AppendLine("	Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl2.AppendLine("	Where A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("    Union All ");
                    SQLDtl2.AppendLine("	Select Distinct ");
                    SQLDtl2.AppendLine("	C.UserName, C.GrpCode, B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("	From TblAPDownpayment A ");
                    SQLDtl2.AppendLine("	Inner Join TblDocApproval B On B.DocType='APDownpayment' And A.DocNo=B.DocNo ");
                    SQLDtl2.AppendLine("	Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("	Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'APDownpayment' ");
                    SQLDtl2.AppendLine("	Where A.DocNo=@DocNo And B.Status = 'A' ");
                    SQLDtl2.AppendLine(") T1 ");
                    SQLDtl2.AppendLine("Inner Join TblGroup T2 On T1.GrpCode = T2.GrpCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter T3 On T3.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Group  By T3.ParValue, T1.UserName, T2.GrpName ");
                    SQLDtl2.AppendLine("Order By T1.Level; ");

                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                            {
                             //0
                             "Signature" ,

                             //1-5
                             "UserName" ,
                             "PosName",
                             "Space",
                             "Level",
                             "Title",

                             "LastupDt"
                            });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {

                            lDtl2.Add(new APDPDtlBBT()
                            {
                                Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                                UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                                PosName = Sm.DrStr(drDtl2, cDtl2[2]),
                                Space = "",
                                DNo = Sm.DrStr(drDtl2, cDtl2[4]),
                                Title = Sm.DrStr(drDtl2, cDtl2[5]),
                                LastUpDt = Sm.DrStr(drDtl2, cDtl2[6]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(lDtl2);
                #endregion

                if (Doctitle=="KIM")
                    Sm.PrintReport("APDownPaymentKIM", myLists, TableName, false);
                else if(Doctitle == "BBT")
                    Sm.PrintReport("APDownPaymentBBT", myLists, TableName, false);
                else
                    Sm.PrintReport("APDownPayment", myLists, TableName, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                ShowAPDownPaymentHdr(DocNo);
                Sm.ShowDocApproval(DocNo, "APDownpayment", ref Grd1, mIsTransactionUseDetailApprovalInformation);
                if (mIsAPDownpaymentUseCOA) ShowAPDownPaymentDtl(DocNo);
                ShowAPDownPaymentDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAPDownPaymentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, ");
            if (!mIsWithoutPO)
                SQL.AppendLine("A2.CurCode As POCurCode, ");
            else
                SQL.AppendLine("A.CurCode As POCurCode, ");
            SQL.AppendLine("ifnull(F.Total, 0) + if(length(A2.taxCode1>0), (ifnull(F.Total, 0)*B.Taxrate*0.01), 0)+ ");
            SQL.AppendLine("if(length(A2.taxCode2>0), (F.Total*C.Taxrate*0.01), 0)+  ");
            SQL.AppendLine("if(length(A2.taxCode3>0), (F.Total*D.Taxrate*0.01), 0)- ifnull(A2.DiscountAmt, 0)+ifnull(A2.CustomsTaxAmt, 0) As POAmt, ");
            SQL.AppendLine("E.VdName, A.VoucherRequestDocNo, G.VoucherDocNo, A.Amt,  ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("IfNull((Select Count(DocNo) From TblAPDownpayment Where PODocNo=A.PODocNo And (CancelInd='N' Or Status<>'C')), 0) As NoOfDownpayment, ");
            SQL.AppendLine("A.VoucherRequestDocNo2, H.VoucherDocNo As VoucherDocNo2, A.LocalDocNo, A2.LocalDOcNO As POLocalDocNo, ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("    Select Sum(Amt) As Amt ");
            SQL.AppendLine("    From TblAPDownpayment ");
            SQL.AppendLine("    Where PODocNo=A.PODocNo ");
            SQL.AppendLine("    And DocNo<>@DocNo ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine("    And Status In ('O', 'A') ");
            SQL.AppendLine("), 0) As OtherAmt, ");
            SQL.AppendLine("A.AmtBefTax, A.TaxInvoiceNo, A.TaxInvoiceDt, A.TaxCode, A.TaxAmt, A.PaymentDt, A.FileName, A.AcNoType,  ");
			if (mIsAPDPUse3Tax)
            {
                SQL.AppendLine("A.TaxInvoiceNo2, A.TaxInvoiceDt2, A.TaxCode2, A.TaxAmt2, ");
                SQL.AppendLine("A.TaxInvoiceNo3, A.TaxInvoiceDt3, A.TaxCode3, A.TaxAmt3, IfNull(A.AmtBefTax + A.TaxAmt + A.TaxAmt2 + A.TaxAmt3,0) AfterTaxAmt,  ");
            }
            else
            {
                SQL.AppendLine("Null As TaxInvoiceNo2, Null As TaxInvoiceDt2, Null As TaxCode2, Null As TaxAmt2, ");
                SQL.AppendLine("Null As TaxInvoiceNo3, Null As TaxInvoiceDt3, Null As TaxCode3, Null As TaxAmt3, IfNull(A.AmtBefTax + A.TaxAmt ,0) AfterTaxAmt, ");
            }
            if (mIsWithoutPO || mVdCtCodeForRawMaterial == "02" )
                SQL.AppendLine("A.VdCode As VendorCode ");
            else
                SQL.AppendLine("A2.VdCode As VendorCode ");
            SQL.AppendLine(", A.FileName2, A.FileName3");
            SQL.AppendLine("From TblAPDownpayment A ");
            SQL.AppendLine("Left Join tblPOHdr A2 On A.PODocNo=A2.DocNo ");
            SQL.AppendLine("Left Join TblTax B On A2.TaxCode1 = B.taxCode ");
            SQL.AppendLine("Left Join TblTax C On A2.TaxCode2 = C.taxCode ");
            SQL.AppendLine("Left Join TblTax D On A2.TaxCode3 = D.taxCode ");
            if (mIsWithoutPO || mVdCtCodeForRawMaterial == "02")
                SQL.AppendLine("Left Join TblVendor E On A.VdCode=E.VdCode ");
            else
                SQL.AppendLine("Left Join TblVendor E On A2.VdCode = E.VdCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.DocNo,  ");
            SQL.AppendLine("    SUM((H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue) As Total ");
            SQL.AppendLine("    From TblPODtl B  ");
            SQL.AppendLine("    Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo  ");
            SQL.AppendLine("    Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo  ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo  ");
            SQL.AppendLine("    Inner Join TblQtHdr G On D.QtDocNo=G.DocNo  ");
            SQL.AppendLine("    Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo  ");
            SQL.AppendLine("    Left Join TblPOQtyCancel M On B.Docno = M.PODocno And B.DNo = M.PODno And M.CancelInd='N'  ");
            SQL.AppendLine("    Where B.CancelInd='N' ");
            SQL.AppendLine("    And B.DocNo In ( ");
            SQL.AppendLine("        Select PODocNo From TblAPDownpayment Where DocNo=@DocNo And PODocNo is Not Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By B.DocNo ");
            SQL.AppendLine(")F On A2.Docno = F.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr G On A.VoucherRequestDocNo=G.DocNo ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr H On A.VoucherRequestDocNo2=H.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "PODocNo", "NoOfDownpayment",
                            
                            //6-10
                            "VendorCode", "POCurcode", "POAmt", "CurCode", "Amt",  
                            
                            //11-15
                            "PIC", "DeptCode", "VoucherRequestDocNo", "VoucherDocNo", "CancelReason", 
                            
                            //16-20
                            "PaymentType", "BankAcCode" , "BankCode", "GiroNo", "OpeningDt", 

                            //21-25
                            "DueDt", "Remark", "VoucherRequestDocNo2", "VoucherDocNo2", "POLocalDocNo", 

                            //26-30
                            "LocalDocNo", "OtherAmt", "PaymentUser", "PaidToBankCode", "PaidToBankBranch",  
                            
                            //31-35
                            "PaidToBankAcName", "PaidToBankAcNo", "EntCode", "AmtBefTax", "TaxInvoiceNo",  
                            
                            //36-40
                            "TaxInvoiceDt", "TaxCode", "TaxAmt", "PaymentDt", "FileName",

                            //41-45
                            "AcNoType", "TaxInvoiceNo2", "TaxInvoiceDt2", "TaxCode2", "TaxAmt2",

                            //46-50
                            "TaxInvoiceNo3", "TaxInvoiceDt3", "TaxCode3", "TaxAmt3", "AfterTaxAmt",

                            //51-53
                            "FileName2", "FileName3", "SiteCode"
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[15]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        TxtPODocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtNoOfDownpayment.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[5]), 2);
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[6]));
                        TxtPOCurCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtPOAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[9]));
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        Sm.SetLue(LuePIC, Sm.DrStr(dr, c[11]));
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[12]), string.Empty);
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[13]);
                        TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[14]);
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[18]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[19]);
                        Sm.SetDte(DteOpeningDt, Sm.DrStr(dr, c[20]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[21]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[22]);
                        TxtVoucherRequestDocNo2.EditValue = (mIsAPDPProcessTo1Journal ? Sm.DrStr(dr, c[13]) : Sm.DrStr(dr, c[23]));
                        TxtVoucherDocNo2.EditValue = (mIsAPDPProcessTo1Journal ? Sm.DrStr(dr, c[14]) : Sm.DrStr(dr, c[24]));
                        TxtPOLocalDocNo.EditValue = Sm.DrStr(dr, c[25]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[26]);
                        TxtOtherAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[27]), 0);
                        TxtPaymentUser.EditValue = Sm.DrStr(dr, c[28]);
                        if (mIsAPDownpaymentPaidToBankBasedOnVendor)
                            SetLuePaidToBankCode(ref LuePaidToBankCode, "", "1");
                        Sm.SetLue(LuePaidToBankCode, Sm.DrStr(dr, c[29]));
                        TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[30]);
                        TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[31]);
                        TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[32]);
                        Sl.SetLueEntCode(ref LueEntCode, Sm.DrStr(dr, c[33]), string.Empty);
                        TxtAmtBefTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[34]), 0);
                        TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[35]);
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[36]));
                        Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[37]));
                        TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[38]), 0);
						TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[42]);
                        Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[43]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[44]));
                        TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[45]), 0);
                        TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[46]);
                        Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[47]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[48]));
                        TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[49]), 0);
                        TxtAmtAftTax.EditValue = (mIsAPDPProcessTo1Journal ? Sm.FormatNum(Sm.DrDec(dr, c[50]), 0) : TxtAmt.EditValue);
                        Sm.SetDte(DtePaymentDt, Sm.DrStr(dr, c[39]));
                        TxtFile.EditValue = Sm.DrStr(dr, c[40]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[51]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[52]);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[53]));
                        Sm.SetLue(LueAcNoType, Sm.DrStr(dr, c[41]));
                    }, true
                );
                    if (mIsAPDownpaymentUseCOA) ComputeCOAAmt();
        }

        private void ShowAPDownPaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.ActInd, A.Remark ");
            SQL.AppendLine("From TblAPDownPaymentDtl A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "ActInd" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 5);
                    if (Sm.DrStr(dr, c[5]) == "Y")
                    {
                        if (Sm.GetGrdDec(Grd2, Row, 3) > 0)
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                        }
                        else
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 5);
                        }
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd2, 0, 1);
            ComputeCOAAmt();
        }

        private void ShowAPDownPaymentDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocType, B.OptDesc As DocTypeDesc, A.DocNumber, A.Amt, A.TaxAmt, A.DocInd, C.OptDesc As DocIndDesc, A.TaxInvDt, A.QRCode, A.Remark, A.RemarkXml, A.PurchaseInvoiceDocNo ");
            SQL.AppendLine("From TblAPDownPaymentDtl2 A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='PurchaseInvoiceDocType' ");
            SQL.AppendLine("Left Join TblOption C On A.DocInd=C.OptCode And C.OptCat='PurchaseInvoiceDocInd' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    
                    //1-5
                    "DocType", "DocTypeDesc", "QRCode", "DocNumber", "TaxInvDt",  
                    
                    //6-10
                    "Amt", "TaxAmt", "DocInd",  "DocIndDesc", "Remark", 

                    //11-12
                    "RemarkXml", "PurchaseInvoiceDocNo" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8 });
            Sm.FocusGrd(Grd3, 0, 2);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "APDownpayment", "TblAPDownpayment");
            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();
            cml.Add(SaveAPDownpayment(DocNo, VoucherRequestDocNo));
            
            if (Grd2.Rows.Count > 1)
            {
                cml.Add(SaveAPDownpaymentDtl2(DocNo));
                //for (int r = 0; r < Grd2.Rows.Count; r++)
                //    if (Sm.GetGrdStr(Grd2, r, 1).Length > 0) 
                //        cml.Add(SaveAPDownpaymentDtl2(DocNo, r));
            }

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveAPDownpaymentDtl3(DocNo));
                //for (int r = 0; r < Grd3.Rows.Count; r++)
                //    if (Sm.GetGrdStr(Grd3, r, 1).Length > 0) 
                //        cml.Add(SaveAPDownpaymentDtl3(DocNo, r));
            }

            Sm.ExecCommands(cml);

            if(!mIsAPDPProcessTo1Journal) InsertDataVR(DocNo);

            if (mIsAPDPAllowToUploadFile)
            {
                if (TxtFile.Text.Length > 1)
                    UploadFile(DocNo, TxtFile, PbUpload, "");
                if (TxtFile2.Text.Length > 1)
                    UploadFile(DocNo, TxtFile2, PbUpload2, "2");
                if (TxtFile3.Text.Length > 1)
                    UploadFile(DocNo, TxtFile3, PbUpload3, "3");
            }

            ShowData(DocNo);
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}


        private void InsertDataVR(string APDocNo)
        {
            var cml = new List<MySqlCommand>();
            if (mIsAPDownpaymentUseCOA && Grd2.Rows.Count > 1)
            {
                var VoucherRequestDocNo2 = string.Empty;

                if (mVoucherCodeFormatType == "2")
                    VoucherRequestDocNo2 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else
                    VoucherRequestDocNo2 = GenerateVoucherRequestDocNo();

                cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo2, APDocNo));
                cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo2, APDocNo));
            }
            Sm.ExecCommands(cml);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                IsPaymentTypeNotValid() ||
                (mIsAPDownpaymentUseBankAccount && Sm.GetLue(LuePaymentType) != "C" && Sm.IsLueEmpty(LueBankAcCode, "Bank Account")) ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                ((mIsAPDownpaymentApprovalBasedOnDept || mIsAPDPDepartmentMandatory) && Sm.IsLueEmpty(LueDeptCode, "Department")) ||
                (mIsItemCategoryUseCOAAPAR && Sm.IsLueEmpty(LueAcNoType, "Account Number Type")) ||
                (mIsWithoutPO && mIsAPDownpaymentUseSite && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsAmtNotValid() ||
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity")) ||
                IsGrdValueNotValid() ||
                (mIsAPDPAllowToUploadFile && mIsAPDPUploadFileMandatory && Sm.IsTxtEmpty(TxtFile, "File 1", false)) ||
                IsUploadFileNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                    if (Sm.IsGrdValueEmpty(Grd3, r, 2, false, "Document type is empty.")) return true;
            }
            return false;
        }

        private bool IsAmtNotValid()
        {
            if (mIsWithoutPO) return false;

            decimal POAmt = decimal.Parse(TxtPOAmt.Text);
            decimal OtherAmt = decimal.Parse(TxtOtherAmt.Text);
            decimal Amt = decimal.Parse(TxtAmt.Text);

            if ((OtherAmt+Amt) > POAmt)
            {
                if (Sm.StdMsgYN("Question",
                    "PO : " + Sm.FormatNum(POAmt, 0) + Environment.NewLine +
                    "Other Downpayment : " + Sm.FormatNum(OtherAmt, 0) + Environment.NewLine +
                    "Downpayment : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                    "AP downpayment amount is bigger than PO's amount." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No)
                    return true;
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsUploadFileNotValid()
        {
            return
                IsUploadFileNotValid(TxtFile.Text) ||
                IsUploadFileNotValid(TxtFile2.Text) ||
                IsUploadFileNotValid(TxtFile3.Text);
        }

        private MySqlCommand SaveAPDownpayment(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* AP Downpayment - Hdr */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            #region Save AP

            SQL.AppendLine("Insert Into TblAPDownpayment ");
            SQL.AppendLine("(DocNo, LocalDocNo, DocDt, CancelInd, Status, PODocNo, VdCode, VoucherRequestDocNo, VoucherRequestDocNo2, CurCode, Amt, ");
            SQL.AppendLine("AmtBefTax, TaxInvoiceNo, TaxInvoiceDt, TaxCode, TaxAmt, ");
			if (mIsAPDPUse3Tax)
            {
                SQL.AppendLine("TaxInvoiceNo2, TaxInvoiceDt2, TaxCode2, TaxAmt2, TaxInvoiceNo3, TaxInvoiceDt3, TaxCode3, TaxAmt3, ");
            }
            if (mIsWithoutPO && mIsAPDownpaymentUseSite)
                SQL.AppendLine("SiteCode, ");
            SQL.AppendLine("PIC, DeptCode, PaymentType, BankAcCode, BankCode, GiroNo, OpeningDt, DueDt, PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo, EntCode,  PaymentDt, AcNoType, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, @DocDt, 'N', 'O', @PODocNo, @VdCode, @VoucherRequestDocNo, @VoucherRequestDocNo2, @CurCode, @Amt, ");
            SQL.AppendLine("@AmtBefTax, @TaxInvoiceNo, @TaxInvoiceDt, @TaxCode, @TaxAmt, ");
            if (mIsAPDPUse3Tax)
            {
                SQL.AppendLine("@TaxInvoiceNo2, @TaxInvoiceDt2, @TaxCode2, @TaxAmt2, @TaxInvoiceNo3, @TaxInvoiceDt3, @TaxCode3, @TaxAmt3, ");
            }
            if (mIsWithoutPO && mIsAPDownpaymentUseSite)
                SQL.AppendLine("@SiteCode, ");
            SQL.AppendLine("@PIC, @DeptCode, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @OpeningDt, @DueDt, @PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo, @EntCode, @PaymentDt, @AcNoType, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='APDownpayment' ");
            if (mIsAPDownpaymentApprovalBasedOnDept || mIsAPDPDepartmentMandatory)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblAPDownpayment Where DocNo=@DocNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblAPDownpayment A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblAPDownpayment Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='APDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            #endregion 

            #region Save VR utama

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, ");
            SQL.AppendLine("DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, OpeningDt, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', ");
            if (mIsAPDownpaymentApprovalBasedOnDept || mIsAPDPDepartmentMandatory)
                SQL.AppendLine("@DeptCode, ");
            else
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='APDownPaymentDeptCode'), ");
            SQL.AppendLine("'04', Null, 'C', Null, @PaymentType, @GiroNo, @BankCode, @OpeningDt, @DueDt, @PIC, 0, @CurCode, ");
            SQL.AppendLine("@Amt, Null, Concat('AP Downpayment ', @APDocNo, ' (', @VdName, '). ', @Remark), @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', Concat('AP Downpayment ', @APDocNo, ' (', @VdName, '). ', @Remark), " + (mIsAPDPProcessTo1Journal ? "@Amt2" : "@Amt") + ", Null, @UserCode, @Dt); ");

            if (mIsAPDPProcessTo1Journal && Decimal.Parse(TxtCOAAmt.Text) != 0)
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@VoucherRequestDocNo, '002', 'Vendor Account Payable Downpayment', @Amt3, Null, @UserCode, @Dt); ");
            }

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='APDownPayment' "); 
            SQL.AppendLine("    And DocNo=@APDocNo ");
            SQL.AppendLine("    ); ");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@APDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@VdName", LueVdCode.GetColumnValue("Col2").ToString());
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt.Text) - Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@OpeningDt", Sm.GetDte(DteOpeningDt));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            if (Sm.GetLue(LuePaidToBankCode).Length == 0)
                Sm.CmParam<String>(ref cm, "@PaidToBankCode", "");
            else
            {
                if (mIsAPDownpaymentPaidToBankBasedOnVendor)
                    Sm.CmParam<String>(ref cm, "@PaidToBankCode", Sm.Left(Sm.GetLue(LuePaidToBankCode), Sm.GetLue(LuePaidToBankCode).Length - 3));
                else
                    Sm.CmParam<String>(ref cm, "@PaidToBankCode", Sm.GetLue(LuePaidToBankCode));
            }
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length <= 0 && mIsAPDownpaymentUseDefaultEntity ? mDefaultEntity : Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@AmtBefTax", decimal.Parse(TxtAmtBefTax.Text));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
			Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));            
            Sm.CmParamDt(ref cm, "@PaymentDt", Sm.GetDte(DtePaymentDt));
            Sm.CmParam<String>(ref cm, "@AcNoType", Sm.GetLue(LueAcNoType));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand SaveAPDownpaymentDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AP Downpayment - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblAPDownPaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@DocNo, @DNo_" + r.ToString() + 
                        ", @AcNo_" + r.ToString() + 
                        ", @DAmt_" + r.ToString() + 
                        ", @CAmt_" + r.ToString() + 
                        ", @ActInd_" + r.ToString() + 
                        ", @Remark_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 5));
                    Sm.CmParam<String>(ref cm, "@ActInd_" + r.ToString(), Sm.GetGrdBool(Grd2, r, 8) ? "Y" : "N");
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAPDownpaymentDtl3(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AP Downpayment - Dtl2 */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblAPDownPaymentDtl2(DocNo, DNo, DocType, QRCode, DocNumber, TaxInvDt,  Amt, TaxAmt, DocInd, Remark, RemarkXml, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @DocType_" + r.ToString() +
                        ", @QRCode_" + r.ToString() +
                        ", @DocNumber_" + r.ToString() +
                        ", @TaxInvDt_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @TaxAmt_" + r.ToString() +
                        ", @DocInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @RemarkXml_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@DocType_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<String>(ref cm, "@QRCode_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 3));
                    Sm.CmParam<String>(ref cm, "@DocNumber_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 5));
                    Sm.CmParamDt(ref cm, "@TaxInvDt_" + r.ToString(), Sm.GetGrdDate(Grd3, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 8));
                    Sm.CmParam<String>(ref cm, "@DocInd_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 9));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 11));
                    Sm.CmParam<String>(ref cm, "@RemarkXml_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 12));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveAPDownpaymentDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblAPDownPaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @ActInd, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd2, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd2, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@ActInd", Sm.GetGrdBool(Grd2, Row, 8) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveAPDownpaymentDtl3(string DocNo, int r)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblAPDownPaymentDtl2(DocNo, DNo, DocType, QRCode, DocNumber, TaxInvDt,  Amt, TaxAmt, DocInd, Remark, RemarkXml, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @DocType, @QRCode, @DocNumber,  @TaxInvDt, @Amt, @TaxAmt, @DocInd, @Remark, @RemarkXml, @UserCode, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd3, r, 1));
        //    Sm.CmParam<String>(ref cm, "@QRCode", Sm.GetGrdStr(Grd3, r, 3));
        //    Sm.CmParam<String>(ref cm, "@DocNumber", Sm.GetGrdStr(Grd3, r, 5));
        //    Sm.CmParamDt(ref cm, "@TaxInvDt", Sm.GetGrdDate(Grd3, r, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, r, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd3, r, 8));
        //    Sm.CmParam<String>(ref cm, "@DocInd", Sm.GetGrdStr(Grd3, r, 9));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, r, 11));
        //    Sm.CmParam<String>(ref cm, "@RemarkXml", Sm.GetGrdStr(Grd3, r, 12));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        //save vr additional COA
        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string APDocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, OpeningDt, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, EntCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', 'N', ");
            if (mIsAPDownpaymentApprovalBasedOnDept || mIsAPDPDepartmentMandatory)
                SQL.AppendLine("@DeptCode, ");
            else
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='APDownPaymentDeptCode'), ");
            SQL.AppendLine("'19', Null, ");
            SQL.AppendLine("'C', @PaymentType, Null, @BankCode, @GiroNo, @OpeningDt, @DueDt, ");
            SQL.AppendLine("@PIC, @CurCode, @Amt, null, @EntCode, Concat('AP Downpayment ', @APDocNo, '. ', @Remark),");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");

            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequest' ");
            if (mIsAPDownpaymentApprovalBasedOnDept || mIsAPDPDepartmentMandatory) SQL.AppendLine("And T.DeptCode=@DeptCode ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblAPDownpayment A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");
            
            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblAPDownPayment Set VoucherRequestDocNo2=@DocNo ");
            SQL.AppendLine("Where DocNo=@APDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@APDocNo", APDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            //Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetParameter("IncomingPaymentDeptCode"));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@OpeningDt", Sm.GetDte(DteOpeningDt));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode).Length <= 0 && mIsAPDownpaymentUseDefaultEntity ? mDefaultEntity : Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo, string APDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, Concat('AP Downpayment ', @APDocNo, ' (', @VdName, '). ', @Remark), @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@APDocNo", APDocNo);
            Sm.CmParam<String>(ref cm, "@VdName", LueVdCode.GetColumnValue("Col2").ToString());
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAPDownpayment());

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveAPDownpaymentDtl3(TxtDocNo.Text));
                //for (int r = 0; r < Grd3.Rows.Count; r++)
                //    if (Sm.GetGrdStr(Grd3, r, 1).Length > 0) cml.Add(SaveAPDownpaymentDtl3(TxtDocNo.Text, r));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataAlreadyCancelled() ||
                (!mIsAPDownpaymentAbleToEditTax && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation")) ||
                IsDataAlreadyProcessed();
        }

        private bool IsDataNotApproved()
        { 
            bool IsApproved =
                Sm.IsDataExist(
                "Select 1 from TblAPDownpayment Where DocNo=@Param And CancelInd='N' And Status='A';",
                TxtDocNo.Text);

            if (!IsApproved)
            {
                Sm.StdMsg(mMsgType.Warning, "This document has not been approved yet.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblAPDownpayment Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsDataAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select 1 " +
                "From TblVoucherRequestHdr A, TblVoucherHdr B " +
                "Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' And A.DocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "This document already processed into voucher.");
        }

        private MySqlCommand EditAPDownpayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAPDownpayment Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set CancelInd=@CancelInd, CancelReason=@CancelReason, Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblVoucherRequestDtl Set Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo; ");

            SQL.AppendLine("Delete From TblAPDownPaymentDtl2 Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetLuePaidToBankCode(ref DXE.LookUpEdit Lue, string VdCode, string Type)
        {
            try
            {
                //Type = 1 -> For View
                //Type = 2 -> For Insert

                var SQL = new StringBuilder();

                if (Type == "1")
                {
                    SQL.AppendLine("Select BankCode As Col1, BankName As Col2, ");
                    SQL.AppendLine("'-' As Col3, 'xxx' As Col4  ");
                    SQL.AppendLine("From TblBank Order By BankName;");
                }
                else
                {
                    SQL.AppendLine("Select Concat(A.BankCode, A.DNo) As Col1, B.BankName As Col2, ");
                    SQL.AppendLine("Trim(Concat(");
                    SQL.AppendLine("    Case When IfNull(A.BankBranch,'')='' Then '' Else Concat('Branch : ', A.BankBranch) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcName,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Then '' Else ', ' End, 'Name : ', A.BankAcName) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcNo,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Or IfNull(A.BankAcName,'')='' Then '' Else ', ' End, 'Account Number : ', A.BankAcNo) End ");
                    SQL.AppendLine(")) As Col3, ");
                    SQL.AppendLine("A.DNo As Col4  ");
                    SQL.AppendLine("From TblVendorBankAccount A ");
                    SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
                    SQL.AppendLine("Where VdCode='" + VdCode + "' ");
                    SQL.AppendLine("Order By B.BankName, A.DNo; ");
                }
                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    0, 170, 300, 0, false, true, true, false,
                    "Code", "Bank", "Bank Branch/Account Name/Account No.", "DNo", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowPaidToBankCodeInfo(string VdCode, string DNo)
        {
            var cm = new MySqlCommand();
            try
            {
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select BankBranch, BankAcName, BankAcNo " +
                        "From TblVendorBankAccount " +
                        "Where VdCode=@VdCode And DNo=@DNo; ",
                        new string[] { "BankBranch", "BankAcName", "BankAcNo" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[2]);
                        }, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetForm(string VdCode)
        {
            if (mIsAPDownpaymentPaidToBankBasedOnVendor)
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
                    });
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, true);
                if (VdCode.Length != 0)
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, false);
                    SetLuePaidToBankCode(ref LuePaidToBankCode, VdCode, "2");
                }
            }
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeCOAAmt()
        {
            decimal COAAmt = 0m, AfterTaxAmt = 0m;
            if(TxtAmtAftTax.Text.Length > 0)
                AfterTaxAmt = decimal.Parse(TxtAmtAftTax.Text);
            try
            {
                var SQL = new StringBuilder();
                if (mIsAPDPProcessTo1Journal)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0 && Sm.GetGrdBool(Grd2, Row, 8))
                        {
                            if (Sm.GetGrdBool(Grd2, Row, 6) && Sm.GetGrdDec(Grd2, Row, 3) != 0)
                            {
                                //kalau dicentang di debit, (+)
                                COAAmt += Sm.GetGrdDec(Grd2, Row, 3);
                            }
                            if (Sm.GetGrdBool(Grd2, Row, 7) && Sm.GetGrdDec(Grd2, Row, 4) != 0)
                            {
                                //kalau dicentang di credit, (-)
                                COAAmt -= Sm.GetGrdDec(Grd2, Row, 4);
                            }
                        }
                    }
                }
                else
                {
                    if (mAPDPCOAAmtCalculationMethod == "1")
                    {
                        SQL.AppendLine("Select Concat( ");
                        SQL.AppendLine("   C.ParValue, A.VdCode ");
                        SQL.AppendLine(") AS AcNo ");
                        SQL.AppendLine("From TblPOHdr A ");
                        SQL.AppendLine("Inner Join TblVendor B ON A.VdCode = B.VdCode ");
                        SQL.AppendLine("Left Join TblParameter C ON C.Parcode = 'VendorAcNoAP' ");
                        SQL.AppendLine("Where A.DocNo = @PODocNo LIMIT 1; ");
                    }
                    if (mAPDPCOAAmtCalculationMethod == "2")
                    {
                        SQL.AppendLine("Select Property1 As AcNo ");
                        SQL.AppendLine("From TblOption Where OptCat = 'AcNoTypeForAPDP' And OptCode = @OptCode Limit 1 ");
                    }
                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@PODocNo", TxtPODocNo.Text);
                    Sm.CmParam<String>(ref cm, "@OptCode", Sm.GetLue(LueAcNoType));
                    var AcNo = Sm.GetValue(cm);

                    cm.CommandText = "Select AcType From TblCOA Where AcNo=@AcNo;";
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                    string AcType = Sm.GetValue(cm);

                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (AcNo.Length > 0 && Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd2, Row, 1)))
                        {
                            if (Sm.GetGrdDec(Grd2, Row, 3) != 0)
                            {
                                if (AcType == "D")
                                    COAAmt += Sm.GetGrdDec(Grd2, Row, 3);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd2, Row, 3);
                            }
                            if (Sm.GetGrdDec(Grd2, Row, 4) != 0)
                            {
                                if (AcType == "C")
                                    COAAmt += Sm.GetGrdDec(Grd2, Row, 4);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd2, Row, 4);
                            }
                        }
                    }
                }
                TxtCOAAmt.EditValue = Sm.FormatNum(COAAmt, 0);
                if(mIsAPDPProcessTo1Journal && BtnSave.Enabled)TxtAmt.EditValue = Sm.FormatNum(COAAmt + AfterTaxAmt, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeOtherAmt()
        {
            var OtherAmt = Sm.GetValue(
                "Select Amt From TblAPDownpayment " +
                "Where PODocNo=@Param And CancelInd='N' And Status In ('O', 'A');", 
                TxtPODocNo.Text);
            if (OtherAmt.Length == 0) OtherAmt = "0";
            TxtOtherAmt.EditValue = Sm.FormatNum(decimal.Parse(OtherAmt), 0);
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex - 1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ReadData(int Row)
        {
            try
            {
                bool IsCopy = (mPIQRCodeTaxDocType.Length > 0 && Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, Row, 1)));
                string Detail = "";
                string QRCode = Sm.GetGrdStr(Grd3, Row, 3);
                XmlReader xmlReader = XmlReader.Create(QRCode);

                getXML(Row);

                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "nomorFaktur"))
                    {
                        Grd3.Cells[Row, 5].Value = xmlReader.ReadString();
                    }
                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "tanggalFaktur"))
                    {
                        string x = xmlReader.ReadString();

                        string dateFaktur = string.Concat(Sm.Right(x, 4), x.Substring(3, 2), Sm.Left(x, 2));
                        Grd3.Cells[Row, 6].Value = Sm.ConvertDate(dateFaktur);
                    }
                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "jumlahDpp"))
                    {
                        Grd3.Cells[Row, 7].Value = decimal.Parse(xmlReader.ReadString());
                    }
                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "jumlahPpn"))
                    {
                        Grd3.Cells[Row, 8].Value = decimal.Parse(xmlReader.ReadString());
                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "nama"))
                    {
                        Detail += string.Concat(xmlReader.ReadString(), " , ");
                    }
                }
                Grd3.Cells[Row, 11].Value = Detail.Remove(Detail.Length - 2);

                if (IsCopy)
                {
                    var TaxInvoiceNo = Sm.GetGrdStr(Grd3, Row, 5);
                    var TaxInvDt = Sm.GetGrdDate(Grd3, Row, 6);
                    var TaxAmt = 0m;

                    if (Sm.GetGrdStr(Grd3, Row, 8).Length > 0)
                        TaxAmt = Sm.GetGrdDec(Grd3, Row, 8);

                    if (TaxInvoiceNo.Length > 0)
                    {
                        TxtTaxInvoiceNo.EditValue = TaxInvoiceNo;
                    }
                    else
                        TxtTaxInvoiceNo.EditValue = null;

                    if (TaxInvDt.Length > 0)
                    {
                        TaxInvDt = TaxInvDt.Substring(0, 8);
                        Sm.SetDte(DteTaxInvoiceDt, TaxInvDt);
                    }
                    else
                        DteTaxInvoiceDt.EditValue = null;

                    TaxAmt = 0m;
                    for (int r = 0; r < Grd3.Rows.Count; r++)
                    {
                        if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                        {
                            if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                                TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                        }
                    }

                    TxtTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
                }
                ComputeAmtAftTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void getXML(int Row)
        {
            try
            {
                string alfa = string.Empty;

                bool IsCopy = (mPIQRCodeTaxDocType.Length > 0 && Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, Row, 1)));
                string QRCode = Sm.GetGrdStr(Grd3, Row, 3);
                XmlReader xmlReader = XmlReader.Create(QRCode);

                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            alfa += ("<" + xmlReader.Name + ">");
                            break;
                        case XmlNodeType.Text:
                            alfa += (xmlReader.Value);
                            break;
                        case XmlNodeType.EndElement:
                            alfa += ("</" + xmlReader.Name + ">") + "\n"; ;
                            break;
                    }
                }

                Grd3.Cells[Row, 12].Value = alfa;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetQRCodeInfo()
        {
            if (mPIQRCodeTaxDocType.Length == 0) return;

            TxtTaxInvoiceNo.EditValue = null;
            DteTaxInvoiceDt.EditValue = null;
            TxtTaxAmt.EditValue = Sm.FormatNum(0m, 0);

            int Row = -1;
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                {
                    Row = r;
                    break;
                }
            }
            if (Row >= 0)
            {
                var TaxInvoiceNo = Sm.GetGrdStr(Grd3, Row, 5);
                var TaxInvDt = Sm.GetGrdDate(Grd3, Row, 6);
                var TaxAmt = 0m;

                if (Sm.GetGrdStr(Grd3, Row, 8).Length > 0)
                    TaxAmt = Sm.GetGrdDec(Grd3, Row, 8);

                if (TaxInvoiceNo.Length > 0)
                {
                    TxtTaxInvoiceNo.EditValue = TaxInvoiceNo;
                }
                else
                    TxtTaxInvoiceNo.EditValue = null;

                if (TaxInvDt.Length > 0)
                {
                    TaxInvDt = TaxInvDt.Substring(0, 8);
                    Sm.SetDte(DteTaxInvoiceDt, TaxInvDt);
                }
                else
                    DteTaxInvoiceDt.EditValue = null;

                TaxAmt = 0m;
                for (int r = 0; r < Grd3.Rows.Count; r++)
                {
                    if (Sm.CompareStr(mPIQRCodeTaxDocType, Sm.GetGrdStr(Grd3, r, 1)))
                    {
                        if (Sm.GetGrdStr(Grd3, r, 8).Length > 0)
                            TaxAmt += Sm.GetGrdDec(Grd3, r, 8);
                    }
                }

                TxtTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
            }
            ComputeAmtAftTax();
        }

        private void ComputeAmtAftTax()
        {
            decimal AmtBefTax = 0m, TaxAmt = 0m, TaxAmt2 = 0m, TaxAmt3 = 0m, COAAmt = 0m;
            string TaxCode = Sm.GetLue(LueTaxCode), TaxCode2 = Sm.GetLue(LueTaxCode2), TaxCode3 = Sm.GetLue(LueTaxCode3);

            if (TxtAmtBefTax.Text.Length > 0) 
                AmtBefTax = decimal.Parse(TxtAmtBefTax.Text);
            if (TxtCOAAmt.Text.Length > 0)
                COAAmt = decimal.Parse(TxtCOAAmt.Text);

            if (mIsAPDownpaymentUseQRCode)
            {
                if (TxtTaxAmt.Text.Length>0) TaxAmt = decimal.Parse(TxtTaxAmt.Text);
				if (TxtTaxAmt2.Text.Length > 0) TaxAmt2 = decimal.Parse(TxtTaxAmt2.Text);
                if (TxtTaxAmt3.Text.Length > 0) TaxAmt3 = decimal.Parse(TxtTaxAmt3.Text);            }
            else
            {
                if (TaxCode.Length != 0 && AmtBefTax!=0m)
                {
                    var TaxRate = Sm.GetValue("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode);
                    if (TaxRate.Length>0) TaxAmt = decimal.Parse(TaxRate)*0.01m*AmtBefTax;
                }
				if (TaxCode2.Length != 0 && AmtBefTax != 0m)
                {
                    var TaxRate = Sm.GetValue("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode2);
                    if (TaxRate.Length > 0) TaxAmt2 = decimal.Parse(TaxRate) * 0.01m * AmtBefTax;
                }
                if (TaxCode3.Length != 0 && AmtBefTax != 0m)
                {
                    var TaxRate = Sm.GetValue("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode3);
                    if (TaxRate.Length > 0) TaxAmt3 = decimal.Parse(TaxRate) * 0.01m * AmtBefTax;
                }            }
            TxtTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
            TxtTaxAmt2.EditValue = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.EditValue = Sm.FormatNum(TaxAmt3, 0);
            TxtAmtAftTax.EditValue = Sm.FormatNum(AmtBefTax+TaxAmt+TaxAmt2+TaxAmt3, 0);            
            TxtAmt.EditValue = TxtAmtAftTax.EditValue;
            if (mIsAPDPProcessTo1Journal) TxtAmt.EditValue = Sm.FormatNum(COAAmt + AmtBefTax + TaxAmt + TaxAmt2 + TaxAmt3, 0);
        }

        #endregion

        #region Ftp

        private void AddFile(DevExpress.XtraEditors.TextEdit TxtFile, DevExpress.XtraEditors.CheckEdit ChkFile)
        {
            try
            {
                ChkFile.Checked = true;
                OD1.InitialDirectory = "c:";
                OD1.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|Rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD1.FilterIndex = 2;
                OD1.ShowDialog();

                TxtFile.Text = OD1.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void UploadFile(string DocNo, DevExpress.XtraEditors.TextEdit TxtFile, System.Windows.Forms.ProgressBar PbUpload, string Code)
        {
            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateAttachmentFile(DocNo, toUpload.Name, Code));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdateAttachmentFile(string DocNo, string FileName, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAPDownPayment Set ");
            SQL.AppendLine("    FileName"+Code+" =@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private void DownloadFileKu(DevExpress.XtraEditors.TextEdit TxtFile, System.Windows.Forms.ProgressBar PbUpload)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, PbUpload);
            SFD1.FileName = TxtFile.Text;
            SFD1.DefaultExt = Path.GetExtension(SFD1.FileName);
            SFD1.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared, System.Windows.Forms.ProgressBar PbUpload)
        {
            downloadedData = new byte[0];

            try
            {
                
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

              
                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private bool IsUploadFileNotValid(string FileName)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid(FileName)
                //IsFileNameAlreadyExisted(FileName)
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsAPDPAllowToUploadFile && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsAPDPAllowToUploadFile && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsAPDPAllowToUploadFile && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsAPDPAllowToUploadFile && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(string FileName)
        {
            if (mIsAPDPAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(string FileName)
        {
            if (mIsAPDPAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo From TblAPDownpayment A ");
                SQL.AppendLine("Where A.FileName=@FileName ");
                SQL.AppendLine("And A.CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnPODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmAPDownpaymentDlg(this));
        }
 
        private void BtnPODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPODocNo, "PO#", false))
            {
                try
                {
                    var f = new FrmPO(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtPODocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmAPDownpaymentDlg2(this));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmAPDownpaymentDlg2(this));
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
                Grd2.Cells[e.RowIndex, 4].Value = 0;

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
                Grd2.Cells[e.RowIndex, 3].Value = 0;

            if (e.ColIndex == 6 && Sm.GetGrdBool(Grd2, e.RowIndex, 6))
                Grd2.Cells[e.RowIndex, 7].Value = false;

            if (e.ColIndex == 7 && Sm.GetGrdBool(Grd2, e.RowIndex, 7))
                Grd2.Cells[e.RowIndex, 6].Value = false;

            if (e.ColIndex == 6 || e.ColIndex == 7)
                Grd2.Cells[e.RowIndex, 8].Value = Sm.GetGrdBool(Grd2, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 6, 7 }, e.ColIndex))
                ComputeCOAAmt();
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeCOAAmt();
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void BtnUpload_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile, ChkFile);
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            if (TxtFile.Text.Length > 0)
                DownloadFileKu(TxtFile, PbUpload);
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
                TxtFile.EditValue = string.Empty;
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile2, ChkFile2);
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            if (TxtFile.Text.Length > 0)
                DownloadFileKu(TxtFile2, PbUpload2);
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
                TxtFile2.EditValue = string.Empty;
        }

        private void BtnUpload3_Click(object sender, EventArgs e)
        {
            AddFile(TxtFile3, ChkFile3);
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            if (TxtFile.Text.Length > 0)
                DownloadFileKu(TxtFile3, PbUpload3);
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
                TxtFile3.EditValue = string.Empty;
        }

        #endregion

        #region Misc Control Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            //if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            //if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode), mIsFilterByVendorCategory ? "Y" : "N");

            if (BtnSave.Enabled)
            {
                if (mIsAPDownpaymentPaidToBankBasedOnVendor)
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
                    {
                        LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
                    });
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, true);
                    Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode), mIsFilterByVendorCategory ? "Y" : "N");
                    if (Sm.GetLue(LueVdCode).Length != 0)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, false);
                        SetLuePaidToBankCode(ref LuePaidToBankCode, Sm.GetLue(LueVdCode), "2");
                    }
                }
            }


        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void TxtTaxAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTaxAmt, 0);
        }

        private void TxtTaxAmt2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTaxAmt2, 0);
        }

        private void TxtTaxAmt3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTaxAmt3, 0);
        }
        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }
       
        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteOpeningDt, DteDueDt });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteOpeningDt, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                    return;
                }

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteOpeningDt, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                    return;
                }

                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{LueBankAcCode, LueBankCode}, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteOpeningDt, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void TxtPaymentUser_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaymentUser);
        }

        private void LuePaidToBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsAPDownpaymentPaidToBankBasedOnVendor)
                {
                    Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue3(SetLuePaidToBankCode), Sm.GetLue(LueVdCode), "2");
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo });
                    if (Sm.GetLue(LuePaidToBankCode).Length != 0)
                        ShowPaidToBankCodeInfo(
                            Sm.GetLue(LueVdCode),
                            LuePaidToBankCode.GetColumnValue("Col4").ToString()
                            );
                }
                else
                    Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
            }
        }

        private void TxtPaidToBankBranch_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankBranch);
        }

        private void TxtPaidToBankAcName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcName);
        }

        private void TxtPaidToBankAcNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcNo);
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue3(Sl.SetLueEntCode), string.Empty, mIsFilterByEnt ? "Y" : "N");
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

		private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }
        private void TxtAmtBefTax_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmtBefTax, 0);
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

		private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueDocType_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueDocType.Visible = false;
        }

        private void LueDocType_Leave(object sender, EventArgs e)
        {
            if (LueDocType.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueDocType).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 1].Value =
                    Grd3.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueDocType);
                    Grd3.Cells[fCell.RowIndex, 2].Value = LueDocType.GetColumnValue("Col2");
                }
                if (mPIQRCodeTaxDocType.Length > 0) ComputeAmtAftTax();
            }
        }

        private void LueDocType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLuePurchaseInvoiceDocType));
        }

        private void LueDocInd_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueDocInd.Visible = false;
        }

        private void LueDocInd_Leave(object sender, EventArgs e)
        {
            if (LueDocInd.Visible && fAccept && fCell.ColIndex == 10)
            {
                if (Sm.GetLue(LueDocInd).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 9].Value =
                    Grd3.Cells[fCell.RowIndex, 10].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 9].Value = Sm.GetLue(LueDocInd);
                    Grd3.Cells[fCell.RowIndex, 10].Value = LueDocInd.GetColumnValue("Col2");
                }
            }
        }

        private void LueDocInd_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueDocInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocInd, new Sm.RefreshLue1(Sl.SetLuePurchaseInvoiceDocInd));
        }


        private void DteTaxInvDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteTaxInvDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteTaxInvDt, ref fCell, ref fAccept);
        }

        private void LueAcNoType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcNoType, new Sm.RefreshLue2(Sl.SetLueOption), "AcNoTypeForAPDP");
            ComputeCOAAmt();
        }

        #endregion

        #region Grid Event

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2, 5, 6, 7, 10, 11 }, e.ColIndex))
            {
                if (e.ColIndex == 6) Sm.DteRequestEdit(Grd3, DteTaxInvDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 2) LueRequestEdit(Grd3, LueDocType, ref fCell, ref fAccept, e);
                if (e.ColIndex == 10) LueRequestEdit(Grd3, LueDocInd, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 7, 8 });
            }
            if (BtnSave.Enabled && e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 3).Length > 0)
            {
                ReadData(e.RowIndex);
            }
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd3, new int[] { 5, 11 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd3, new int[] { 7, 8 }, e);
            if (Sm.IsGrdColSelected(new int[] { 2, 3, 5, 6, 7, 8 }, e.ColIndex))
            {
                if (mPIQRCodeTaxDocType.Length > 0)
                {
                    SetQRCodeInfo();
                    ComputeAmtAftTax();
                }
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 3).Length > 0)
            {
                ReadData(e.RowIndex);
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            SetQRCodeInfo();
            if (mPIQRCodeTaxDocType.Length > 0) ComputeAmtAftTax();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #region Report Class

        class APDPHdr
        {
            public string CompanyLogo { get; set; }
            public string Company{ get; set; }
            public string Address{ get; set; }
            public string Phone{ get; set; }
            public string DocNo{ get; set; }
            public string DocDt{ get; set; }
            public string PODocNo{ get; set; }
            public string POCurCode{ get; set; }
            public decimal POAmt { get; set; }
            public string VDName{ get; set; }
            public string VoucherRequestDocNo{ get; set; }
            public string VoucherDocNo{ get; set; }
            public string CreateBy{ get; set; }
            public string DeptName{ get; set; }
            public string SiteName{ get; set; }
            public string StatusDEsc{ get; set; }
            public decimal Amt { get; set; }
            public string CreateDt { get; set; }
            public string Remark{ get; set; }
            public string SiteInd { get; set; }
            public string PrintBy { get; set; }
            public string PIC { get; set; }
            public string PaymentUser { get; set; }
            public string GiroNo { get; set; }
            public string GiroDt { get; set; }
            public string GiroBankName { get; set; }
            public string PaymentType { get; set; }
            public string Terbilang { get; set; }
            public string PaidToBankName { get; set; }
            public string PaidToBankBranch { get; set; }
            public string PaidToBankAcName { get; set; }
            public string PaidToBankAcNo { get; set; }
            public string VdAddress { get; set; }
            public string VdCity { get; set; }
            public string VRLocal { get; set; }
            public decimal AmtBefTax { get; set; }
            public string TaxName { get; set; }
            public string CurCode { get; set; }
            public string GrpName { get; set; }
        }

        class APDPDtl
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        class APDPDtlBBT
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        #endregion

    }
}
