﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTI2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmTI2Find FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmTI2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Taxable Income (Gross Up Method)";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocNo }, true);
            SetGrd();
            SetFormControl(mState.View);
            SetLueSeqNo(ref LueSeqNo);
            LueSeqNo.Visible = false;

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }

            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                       //0
                        "DNo",
                        
                        //1-5
                        "Sequence#",
                        "From",
                        "To",
                        "Value 1",
                        "Value 2",

                        //6
                        "Value 3"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        80, 150, 150, 120, 120,

                        //6
                        130
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 2, 3, 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, ChkActInd, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, ChkActInd, MeeRemark, LueSeqNo
            });
            ChkActInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 3, 4, 5, 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTI2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (Sm.IsGrdColSelected(new int[] { 1, 2, 3, 4, 5, 6 }, e.ColIndex))
                    {
                        if (e.ColIndex == 1) LueRequestEdit(Grd1, LueSeqNo, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 4, 5, 6 });
                    }
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2, 3, 4, 5, 6 }, e);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid() ||
                Sm.StdMsgYN("Question", "Do you want to save this data ?") == DialogResult.No
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TI2", "TblTI2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTI2Hdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    cml.Add(SaveTI2Dtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Sequence# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "From should be bigger than 0.00.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 3, true, "To should be bigger than 0.00.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 4, true, "Value 1 should be bigger than 0.00.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Value 2 should be bigger than 0.00.") 
                    ) return true;

                for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                {
                    if (
                        Row != Row2 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row2, 1))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Sequence# : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                            "Duplicate entry."
                            );
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveTI2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTI2Hdr Set ");
            SQL.AppendLine("    ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where ActInd='Y'; ");

            SQL.AppendLine("Insert Into TblTI2Hdr(DocNo, DocDt, ActInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'Y', @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));            
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTI2Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTI2Dtl(DocNo, DNo, SeqNo, Amt1, Amt2, Value1, Value2, Value3, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @SeqNo, @Amt1, @Amt2, @Value1, @Value2, @Value3, @CreateBy, CurrentDateTime()); ");

        
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SeqNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Value1", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Value2", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Value3", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditTI2());
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataInactiveAlready();
        }

        private bool IsDataInactiveAlready()
        {
            return 
                Sm.IsDataExist(
                "Select 1 From TblTI2Hdr Where ActInd='N' And DocNo=@Param;",
                TxtDocNo.Text, 
                "This data already not actived.");       
        }

        private MySqlCommand EditTI2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTI2Hdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowTI2Hdr(DocNo);
                ShowTI2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTI2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, ActInd, Remark From TblTI2Hdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "ActInd", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowTI2Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, SeqNo, Amt1, Amt2, Value1, Value2, Value3 ");
            SQL.AppendLine("From TblTI2Dtl ");
            SQL.AppendLine("Where DocNo=@DocNo Order By SeqNo, DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "SeqNo", "Amt1", "Amt2", "Value1", "Value2",
                    "Value3"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 6);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 4, 5, 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetLueSeqNo(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                for (int i=1;i<=8;i++)
                    SQL.AppendLine("Select '"+i+"' As Col1, '"+i+"' As Col2 Union All ");
                SQL.AppendLine("Select '9' As Col1, '9' As Col2 ;");
                
                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 1).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueSeqNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSeqNo, new Sm.RefreshLue1(SetLueSeqNo));
        }

        private void LueSeqNo_Leave(object sender, EventArgs e)
        {
            if (LueSeqNo.Visible && fAccept && fCell.ColIndex == 1)
            {
                Grd1.Cells[fCell.RowIndex, 1].Value = 
                    (Sm.GetLue(LueSeqNo).Length == 0)
                    ?null:Sm.GetLue(LueSeqNo);
                LueSeqNo.Visible = false;
            }
        }

        private void LueSeqNo_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        #endregion

        #endregion
    }
}
