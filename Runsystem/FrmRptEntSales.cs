﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEntSales : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<Tax> ml = new List<Tax>();
        private string mMainCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEntSales(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueEntCode(ref LueEntCode);
                GetParameter();
                SetSQL();
                SetGrd();
                SetTax();
                Grd1.Cols[19].Move(Grd1.Cols.Count - 1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select G.EntName, A.DocDt, A.DocNo, D.WhsName, C.CtName, ");
            SQL.AppendLine("I.DocDt As SalesInvoiceDocDt, I.DocNo As SalesInvoiceDocNo, I.TaxInvDocument, ");
            SQL.AppendLine("J.ItName, B.Qty, J.InventoryUomCode, ");
            SQL.AppendLine("A.CurCode, B.UPrice, ");
            SQL.AppendLine("Case When A.CurCode='IDR' Then 1.00 Else ");
            SQL.AppendLine("    Case When I.DocDt Is Null Then 0.00 Else "); 
            SQL.AppendLine("        IfNull((  ");
	        SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
            SQL.AppendLine("            Where RateDt<=I.DocDt And CurCode1=A.CurCode And CurCode2='IDR' ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
            SQL.AppendLine("        ), 0.00)  ");
            SQL.AppendLine("    End  ");
            SQL.AppendLine("End As ExcRate,  ");
            SQL.AppendLine("I.TaxCode1, I.TaxCode2, I.TaxCode3 ");    
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("Inner Join TblCostCenter E On D.CCCode=E.CCCode ");
            SQL.AppendLine("Inner Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
            SQL.AppendLine("Inner Join TblEntity G On F.EntCode=G.EntCode ");
            SQL.AppendLine("Left Join TblSalesInvoiceDtl H ");
            SQL.AppendLine("    On B.DocNo=H.DOCtDocNo ");
            SQL.AppendLine("    And B.DNo=H.DOCtDNo ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblSalesInvoiceHdr ");
            SQL.AppendLine("        Where CancelInd='N' And DocNo=H.DocNo ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("Left Join TblSalesInvoiceHdr I On H.DocNo=I.DocNo And I.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItem J On B.ItCode=J.ItCode ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@Period  ");
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Entity",
                        "DO"+Environment.NewLine+"Date",
                        "DO#", 
                        "Warehouse",
                        "Customer",

                        //6-10
                        "Invoice"+Environment.NewLine+"Date", 
                        "Invoice#",
                        "Tax#",
                        "Foreign"+Environment.NewLine+"Name",
                        "Quantity",
                        
                        //11-15
                        "UoM",
                        "Currency",
                        "Price",
                        "Amount",
                        "Rate",
                        
                        //16-19
                        "Currency",
                        "Price",
                        "Amount",
                        "Total"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 80, 130, 200, 200, 
                        
                        //6-10
                        80, 130, 120, 200, 80, 

                        //11-15
                        80, 60, 100, 120, 100, 

                        //16-19
                        60, 100, 120, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 13, 14, 15, 17, 18, 19 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
            
        }

        override protected void ShowData()
        {
            string Yr = Sm.GetLue(LueYr), Mth = Sm.GetLue(LueMth);
           
            if (Yr.Length==0)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                return;
            }

            if (Mth.Length==0)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ", TaxCode1 = string.Empty, TaxCode2 = string.Empty, TaxCode3 = string.Empty;
                decimal Total = 0m;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@Period", string.Concat(Yr, Mth));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEntCode), "G.EntCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + 
                    " Order By G.EntName, A.DocDt, A.DocNo, J.ItName;",
                    new string[]
                    { 
                        //0
                        "EntName", 

                        //1-5
                        "DocDt", "DocNo", "WhsName", "CtName", "SalesInvoiceDocDt", 

                        //6-10
                        "SalesInvoiceDocNo", "TaxInvDocument", "ItName", "Qty", "InventoryUomCode", 

                        //11-15
                        "CurCode", "UPrice", "ExcRate", "TaxCode1", "TaxCode2", 
                        
                        //16
                        "TaxCode3"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        TaxCode1 = Sm.DrStr(dr, 14);
                        TaxCode2 = Sm.DrStr(dr, 15);
                        TaxCode3 = Sm.DrStr(dr, 16);
                        Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd1, Row, 10) * Sm.GetGrdDec(Grd1, Row, 13);
                        Grd.Cells[Row, 16].Value = mMainCurCode;
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd, Row, 12), mMainCurCode))
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 12);
                        else
                            Grd.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 13) * Sm.GetGrdDec(Grd1, Row, 15);
                        Grd.Cells[Row, 18].Value = Sm.GetGrdDec(Grd1, Row, 10) * Sm.GetGrdDec(Grd1, Row, 14);
                        for (int i = 20; i < Grd.Cols.Count; i++)
                            Grd.Cells[Row, i].Value = 0m;
                        if (TaxCode1.Length > 0) ComputeTax(Row, TaxCode1);
                        if (TaxCode2.Length > 0) ComputeTax(Row, TaxCode2);
                        if (TaxCode3.Length > 0) ComputeTax(Row, TaxCode3);

                        Total = Sm.GetGrdDec(Grd, Row, 18);
                        for (int i = 20; i < Grd.Cols.Count; i++)
                            Total += Sm.GetGrdDec(Grd, Row, i);
                        Grd.Cells[Row, 19].Value = Total;
                    }, true, false, false, false
                );

                Grd1.BeginUpdate();
                for (int i = 20; i < Grd1.Cols.Count; i++)
                {
                    Grd1.Cols[i].Visible = false;
                    for (int j = 0; j < Grd1.Rows.Count; j++)
                    {
                        if (Sm.GetGrdDec(Grd1, j, i)!=0)
                        {
                            Grd1.Cols[i].Visible = true;
                            break;
                        }
                    }
                }
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 14, 18, 19 });
                for (int i = 20; i < Grd1.Cols.Count; i++)
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { i });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ComputeTax(int Row, string TaxCode)
        {
            foreach(var x in ml.Where(t=> Sm.CompareStr(t.TaxCode, TaxCode)))
            {
                Grd1.Cells[Row, x.Col].Value = Sm.GetGrdDec(Grd1, Row, 18)*x.TaxRate*0.01m;
                break;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 14, 18, 19 });
            for (int i = 20; i < Grd1.Cols.Count; i++)
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { i });
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetTax()
        {
            int Column = Grd1.Cols.Count-1;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandTimeout = 600,
                    CommandText = "Select TaxCode, TaxName, TaxRate From TblTax Order By TaxName Desc;"
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "TaxCode", "TaxRate", "TaxName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Column += 1;
                        ml.Add(new Tax()
                        {
                            TaxCode = Sm.DrStr(dr, c[0]),
                            TaxRate = Sm.DrDec(dr, c[1]),
                            Col = Column
                        });
                        Grd1.Cols.Count += 1;
                        Grd1.Cols[Column].Text = Sm.DrStr(dr, c[2]);
                        Grd1.Cols[Column].Width = 150;
                        Grd1.Header.Cells[0, Column].TextAlign = iGContentAlignment.TopCenter;
                        Grd1.Cols[Column].CellStyle.ValueType = typeof(decimal);
                        Sm.GrdFormatDec(Grd1, new int[] { Column }, 0);
                    }
                }
                dr.Close();
            }

        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEntCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Entity");
        }

        #endregion

        #endregion

        #region Class

        private class Tax
        {
            public string TaxCode { get; set; }
            public decimal TaxRate { get; set; }
            public int Col { get; set; }
        }

        #endregion
    }
}
