﻿namespace RunSystem
{
    partial class FrmPORequestDlg4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.MeeRemarkTotal = new DevExpress.XtraEditors.MemoExEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtConditionDesc = new DevExpress.XtraEditors.TextEdit();
            this.TxtTransferOfTitle = new DevExpress.XtraEditors.TextEdit();
            this.TxtRevisionNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtFormNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.DteRevisionDt = new DevExpress.XtraEditors.DateEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.MeeBasisForProposal = new DevExpress.XtraEditors.MemoExEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtTax = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteRepairDt = new DevExpress.XtraEditors.DateEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtConditionDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTransferOfTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRevisionNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFormNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRevisionDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRevisionDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBasisForProposal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRepairDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRepairDt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(547, 0);
            this.panel1.Size = new System.Drawing.Size(70, 201);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 179);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.TabIndex = 4;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteRepairDt);
            this.panel2.Controls.Add(this.MeeRemarkTotal);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtConditionDesc);
            this.panel2.Controls.Add(this.TxtTransferOfTitle);
            this.panel2.Controls.Add(this.TxtRevisionNo);
            this.panel2.Controls.Add(this.TxtFormNo);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.DteRevisionDt);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.MeeBasisForProposal);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtTax);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(547, 201);
            this.panel2.TabIndex = 5;
            // 
            // MeeRemarkTotal
            // 
            this.MeeRemarkTotal.EnterMoveNextControl = true;
            this.MeeRemarkTotal.Location = new System.Drawing.Point(120, 150);
            this.MeeRemarkTotal.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkTotal.Name = "MeeRemarkTotal";
            this.MeeRemarkTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkTotal.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkTotal.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkTotal.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkTotal.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkTotal.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkTotal.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkTotal.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkTotal.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkTotal.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkTotal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkTotal.Properties.MaxLength = 400;
            this.MeeRemarkTotal.Properties.PopupFormSize = new System.Drawing.Size(520, 20);
            this.MeeRemarkTotal.Properties.ShowIcon = false;
            this.MeeRemarkTotal.Size = new System.Drawing.Size(257, 20);
            this.MeeRemarkTotal.TabIndex = 23;
            this.MeeRemarkTotal.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkTotal.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkTotal.ToolTipTitle = "Run System";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(34, 152);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 14);
            this.label9.TabIndex = 22;
            this.label9.Text = "Remark Total";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtConditionDesc
            // 
            this.TxtConditionDesc.EnterMoveNextControl = true;
            this.TxtConditionDesc.Location = new System.Drawing.Point(120, 3);
            this.TxtConditionDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtConditionDesc.Name = "TxtConditionDesc";
            this.TxtConditionDesc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtConditionDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtConditionDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtConditionDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtConditionDesc.Properties.MaxLength = 255;
            this.TxtConditionDesc.Size = new System.Drawing.Size(257, 20);
            this.TxtConditionDesc.TabIndex = 9;
            // 
            // TxtTransferOfTitle
            // 
            this.TxtTransferOfTitle.EnterMoveNextControl = true;
            this.TxtTransferOfTitle.Location = new System.Drawing.Point(120, 45);
            this.TxtTransferOfTitle.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTransferOfTitle.Name = "TxtTransferOfTitle";
            this.TxtTransferOfTitle.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTransferOfTitle.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTransferOfTitle.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTransferOfTitle.Properties.Appearance.Options.UseFont = true;
            this.TxtTransferOfTitle.Properties.MaxLength = 255;
            this.TxtTransferOfTitle.Size = new System.Drawing.Size(257, 20);
            this.TxtTransferOfTitle.TabIndex = 13;
            // 
            // TxtRevisionNo
            // 
            this.TxtRevisionNo.EnterMoveNextControl = true;
            this.TxtRevisionNo.Location = new System.Drawing.Point(120, 108);
            this.TxtRevisionNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRevisionNo.Name = "TxtRevisionNo";
            this.TxtRevisionNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRevisionNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRevisionNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRevisionNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRevisionNo.Properties.MaxLength = 255;
            this.TxtRevisionNo.Size = new System.Drawing.Size(257, 20);
            this.TxtRevisionNo.TabIndex = 19;
            // 
            // TxtFormNo
            // 
            this.TxtFormNo.EnterMoveNextControl = true;
            this.TxtFormNo.Location = new System.Drawing.Point(120, 87);
            this.TxtFormNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFormNo.Name = "TxtFormNo";
            this.TxtFormNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFormNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFormNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFormNo.Properties.Appearance.Options.UseFont = true;
            this.TxtFormNo.Properties.MaxLength = 255;
            this.TxtFormNo.Size = new System.Drawing.Size(257, 20);
            this.TxtFormNo.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(33, 132);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 14);
            this.label8.TabIndex = 20;
            this.label8.Text = "Revision Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteRevisionDt
            // 
            this.DteRevisionDt.EditValue = null;
            this.DteRevisionDt.EnterMoveNextControl = true;
            this.DteRevisionDt.Location = new System.Drawing.Point(120, 129);
            this.DteRevisionDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteRevisionDt.Name = "DteRevisionDt";
            this.DteRevisionDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRevisionDt.Properties.Appearance.Options.UseFont = true;
            this.DteRevisionDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRevisionDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteRevisionDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteRevisionDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteRevisionDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRevisionDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteRevisionDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRevisionDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteRevisionDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteRevisionDt.Size = new System.Drawing.Size(152, 20);
            this.DteRevisionDt.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(16, 111);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 14);
            this.label7.TabIndex = 18;
            this.label7.Text = "Revision Number";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeBasisForProposal
            // 
            this.MeeBasisForProposal.EnterMoveNextControl = true;
            this.MeeBasisForProposal.Location = new System.Drawing.Point(120, 66);
            this.MeeBasisForProposal.Margin = new System.Windows.Forms.Padding(5);
            this.MeeBasisForProposal.Name = "MeeBasisForProposal";
            this.MeeBasisForProposal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBasisForProposal.Properties.Appearance.Options.UseFont = true;
            this.MeeBasisForProposal.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBasisForProposal.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeBasisForProposal.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBasisForProposal.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeBasisForProposal.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBasisForProposal.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeBasisForProposal.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeBasisForProposal.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeBasisForProposal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeBasisForProposal.Properties.MaxLength = 400;
            this.MeeBasisForProposal.Properties.PopupFormSize = new System.Drawing.Size(520, 20);
            this.MeeBasisForProposal.Properties.ShowIcon = false;
            this.MeeBasisForProposal.Size = new System.Drawing.Size(257, 20);
            this.MeeBasisForProposal.TabIndex = 15;
            this.MeeBasisForProposal.ToolTip = "F4 : Show/hide text";
            this.MeeBasisForProposal.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeBasisForProposal.ToolTipTitle = "Run System";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(32, 89);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 14);
            this.label6.TabIndex = 16;
            this.label6.Text = "Form Number";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(11, 69);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 14);
            this.label5.TabIndex = 14;
            this.label5.Text = "Basis For Proposal";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(16, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 14);
            this.label3.TabIndex = 12;
            this.label3.Text = "Transfer Of Title";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTax
            // 
            this.TxtTax.EnterMoveNextControl = true;
            this.TxtTax.Location = new System.Drawing.Point(120, 24);
            this.TxtTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTax.Name = "TxtTax";
            this.TxtTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTax.Properties.Appearance.Options.UseFont = true;
            this.TxtTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTax.Properties.MaxLength = 255;
            this.TxtTax.Size = new System.Drawing.Size(257, 20);
            this.TxtTax.TabIndex = 11;
            this.TxtTax.Validated += new System.EventHandler(this.TxtTax_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(55, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 14);
            this.label2.TabIndex = 8;
            this.label2.Text = "Condition";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(86, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 14);
            this.label4.TabIndex = 10;
            this.label4.Text = "Tax";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(43, 174);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 14);
            this.label1.TabIndex = 24;
            this.label1.Text = "Repair Date";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteRepairDt
            // 
            this.DteRepairDt.EditValue = null;
            this.DteRepairDt.EnterMoveNextControl = true;
            this.DteRepairDt.Location = new System.Drawing.Point(120, 171);
            this.DteRepairDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteRepairDt.Name = "DteRepairDt";
            this.DteRepairDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRepairDt.Properties.Appearance.Options.UseFont = true;
            this.DteRepairDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteRepairDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteRepairDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteRepairDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteRepairDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRepairDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteRepairDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteRepairDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteRepairDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteRepairDt.Size = new System.Drawing.Size(152, 20);
            this.DteRepairDt.TabIndex = 25;
            // 
            // FrmPORequestDlg4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 201);
            this.Controls.Add(this.panel2);
            this.Name = "FrmPORequestDlg4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Additional Information";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtConditionDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTransferOfTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRevisionNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFormNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRevisionDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRevisionDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeBasisForProposal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRepairDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteRepairDt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        protected internal DevExpress.XtraEditors.TextEdit TxtTransferOfTitle;
        protected internal DevExpress.XtraEditors.TextEdit TxtRevisionNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtFormNo;
        protected internal DevExpress.XtraEditors.DateEdit DteRevisionDt;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeBasisForProposal;
        protected internal DevExpress.XtraEditors.TextEdit TxtTax;
        protected internal DevExpress.XtraEditors.TextEdit TxtConditionDesc;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeRemarkTotal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        protected internal DevExpress.XtraEditors.DateEdit DteRepairDt;
    }
}