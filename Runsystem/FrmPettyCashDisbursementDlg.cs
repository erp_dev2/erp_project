﻿#region update

/*
    08/10/2022 [SET/PRODUCT] New application
    05/12/2022 [WED/PRODUCT] item di inner join item category + item active indicator Y, untuk minimalisir adanya item yang salah
    04/04/2023 [MAU/BBT] show item berdasarkan cost center
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPettyCashDisbursementDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPettyCashDisbursement mFrmParent;
        private string mSQL = string.Empty, nSQL = string.Empty, oSQL = string.Empty, mSQL2 = string.Empty, mDeptCode = string.Empty, mReqType = string.Empty;

        #endregion

        #region Constructor

        public FrmPettyCashDisbursementDlg(FrmPettyCashDisbursement FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
                //Sl.SetLueItGrpCode(ref LueItGrpCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Item's Code",
                        "Item's Name",
                        "UoM",
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 120, 100, 
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] {  });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4 });
            Sm.GrdFormatDec(Grd1, new int[] {  }, 0);
            Sm.GrdFormatDate(Grd1, new int[] {  });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.PurchaseUoMCode UoM, A.ItCtCode ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            SQL.AppendLine("INNER JOIN tblitemcostcategory C ON A.ItCode = C.ItCode AND C.CCtCode IS NOT NULL  ");
            SQL.AppendLine("Where A.ActInd = 'Y' and C.CCCode = @CCCode");
            

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            //if (
            //    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            //    Sm.IsDteEmpty(DteDocDt2, "End date") ||
            //    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            //    ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                
                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CCCode", mFrmParent.GetCCCode2());
                //Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                //Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.CmParam<String>(ref cm, "@VoucherDocTypeCASBA", mFrmParent.mVoucherDocTypeCASBA);
                //Sm.CmParam<String>(ref cm, "@BankAccountTypeForPCD", mFrmParent.mBankAccountTypeForPCD);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueUserCode), "B.PIC", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL() + Filter,
                    new string[]
                    { 
                    
                        //0
                        "ItCode",
                        
                        //1-5
                        "ItName", "UoM"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);

                        //mFrmParent.Grd1.Cells[Row1, 3].Value = null;
                        //mFrmParent.Grd1.Cells[Row1, 4].Value = null;
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 4, 5, 7 });

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 4, 5, 7 });
                        mFrmParent.GetCostCatgegory();

                        //mFrmParent.ComputeAmt1();
                        //mFrmParent.SetCostCategoryInfo();
                        //mFrmParent.SetVoucherInfo();
                        //mFrmParent.ComputeAmt2();
                        //mFrmParent.SetDeptCode();
                        //if (mFrmParent.mIsCASUseCostCenter) mFrmParent.GetCCCode();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        #endregion


        #endregion

        #region Event

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

    }
}
