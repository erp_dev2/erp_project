﻿#region Update
/*
    19/02/2020 [DITA/IMS] ganti source dari BOM ke ITEM
    19/02/2020 [DITA/IMS] price list mengambil dari unit price contract, penambahan informasi total setelah PPN 10%
    19/06/2020 [TKG/IMS] bug saat menampilkan remark
    26/10/2020 [DITA/IMS] Price disesuaikan dengan item berdasarkan nomor item nya dan it code nya
    04/12/2020 [DITA/IMS] tambah boq no di grid
    24/02/2021 [DITA/IMS] tambah boq seq no + data diurutkan berdasrkan seqno nya
    22/04/2021 [TKG/IMS] ubah query di SetSQL()
    29/04/2021 [TKG/IMS] tambah try catch saat Choose
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContract2Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContract2Dlg3 mFrmParent;
        private string mSQL = string.Empty;
        private string mBOQDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContract2Dlg4(FrmSOContract2Dlg3 FrmParent, string BOQDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mBOQDocNo = BOQDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetGrd();
            SetSQL();
            ShowData();
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, A.Specification, B.CtItCode, B.CtItName, D.Remark, D.No, D.SeqNo ");
            //SQL.AppendLine("From TblItem A  ");
            //SQL.AppendLine("Left Join TblCustomerItem B On A.ItCode=B.ItCode And B.CtCode=@CtCode ");
            //SQL.AppendLine("Left Join tblitempackagingunit C On A.ItCode = C.ItCode And A.SalesUomCode = C.UomCode  ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select ItCode, Remark, No, SeqNo ");
            //SQL.AppendLine("    From TblBOQDtl2 ");
            //SQL.AppendLine("    Where DocNo=@DocNo ");
            //SQL.AppendLine("    And Amt<>0.00 ");
            //SQL.AppendLine("    Union All ");
            //SQL.AppendLine("    Select ItCode, Remark, No, SeqNo ");
            //SQL.AppendLine("    From TblBOQDtl3 ");
            //SQL.AppendLine("    Where DocNo = @DocNo ");
            //SQL.AppendLine("    And CostOfGoodsAmt<>0.00 ");
            //SQL.AppendLine(") D On A.ItCode=D.ItCode ");
            //SQL.AppendLine("Where A.SalesitemInd = 'Y' ");
            //SQL.AppendLine("And A.ItCode In ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    SELECT T2.ItCode ");
            //SQL.AppendLine("    FROM TblBOQDtl2 T1, TblItem T2  ");
            //SQL.AppendLine("    Where T1.ItCode = T2.ItCode ");
            //SQL.AppendLine("    And T1.DocNo=@DocNo ");
            //SQL.AppendLine("    WHERE T1.Amt>0.00 ");
            //SQL.AppendLine("    UNION All ");
            //SQL.AppendLine("    Select T2.ItCode ");
            //SQL.AppendLine("    From TblBOQDtl3 T1, TblItem T2 ");
            //SQL.AppendLine("    Where T1.ItCode=T2.ItCode ");
            //SQL.AppendLine("    And T1.DocNo = @DocNo ");
            //SQL.AppendLine("    And T1.CostOfGoodsAmt>0.00 ");
            //SQL.AppendLine(") ");

            SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, A.Specification, B.CtItCode, B.CtItName, ");
            SQL.AppendLine("C.Remark, C.No, C.SeqNo ");
            SQL.AppendLine("From TblItem A  ");
            SQL.AppendLine("Left Join TblCustomerItem B On A.ItCode=B.ItCode And B.CtCode=@CtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select ItCode, Remark, No, SeqNo ");
            SQL.AppendLine("    FROM TblBOQDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo And Amt>0.00 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select ItCode, Remark, No, SeqNo ");
            SQL.AppendLine("    From TblBOQDtl3 ");
            SQL.AppendLine("    Where DocNo = @DocNo And CostOfGoodsAmt>0.00 ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.SalesitemInd = 'Y' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Item's Code", 
                        "",
                        "Local Code",
                        "Item's Name",
                        "Specification",

                        //6-10
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Customer's"+Environment.NewLine+"Item Name",
                        "Remark",
                        "BOQ No",
                        "BOQ Sequence No"
                    },
                    new int[]
                    {
                        //0
                        50,
                        
                        //1-5
                        80, 20, 80, 300, 130,

                        //6-10
                        130, 250, 500, 100, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6, 10 }, false);
            Grd1.Cols[9].Move(1);
            Grd1.Cols[10].Move(2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mBOQDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName"});

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " ORDER BY Right(concat('0000000000', C.SeqNo), 10); ",
                    new string[] 
                    { 
                        //0
                        "ItCode",
                        //1-5
                        "ItCodeInternal", "ItName", "Specification", "CtItCode", "CtItName",
                        //6-8
                        "Remark", "No", "SeqNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 0))
                {
                    mFrmParent.TxtItCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.TxtItName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                    mFrmParent.TxtSpecification.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                    mFrmParent.TxtCtItCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                    mFrmParent.TxtCtItName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                    mFrmParent.MeeRemark.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                    mFrmParent.TxtNoItem.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                    mFrmParent.ShowItemInfo();
                    mFrmParent.SetLuePackaging(ref mFrmParent.LuePackaging, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), "");
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
