﻿#region Update
/*
    26/10/2018 [HAR] bug fixing employee code
    09/04/2019 [WED] bug fixing credit code
    26/08/2019 [WED] bug cast mysql ke int
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    25/02/2021 [HAR/PHT] BUG query saat save document approval
    06/01/2022 [TKG/PHT] Kasih remark pada query save
    14/01/2022 [TKG/PHT] mengganti GetParameter()
    29/06/2022 [DITA/PHT] tambah validasi untuk Credit, amount, month dan year
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLoanImport : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmEmpLoanImportFind FrmFind;
        private string
            mAdvancePaymentDocAbbr = string.Empty,
            mVoucherRequestDocAbbr = string.Empty,
            mBankAcCodeAdvancePayment = string.Empty,
            mMainCurCode = string.Empty,
            mVoucherRequestPayrollDeptCode = string.Empty,
            mDocSeqNo = string.Empty,
            mDocTitle = string.Empty;
        private bool
            mIsEmpLoanImportSaveMultiDataOnceEnabled = false,
            mIsDocSeqNoEnabled = false;

        #endregion

        #region Constructor

        public FrmEmpLoanImport(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Loan Import Data";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Employee's Code",
                        
                        //1-5
                        "Old Code",
                        "Employee's Name",
                        "Department",
                        "Position",
                        "Amount",
                        //6-10
                        "Credit Code",
                        "Month",
                        "Year",
                        "Advance Payment",
                        "",
                        //11-14
                        "Voucher Request",
                        "",
                        "Number",
                        "Credit"
                    },
                     new int[] 
                    {
                        //0
                        130, 

                        //1-5
                        100, 200, 200, 180, 100, 
                        
                        //6-10
                        80, 80, 80, 120, 20,

                        //11-14
                        120, 20, 40, 150
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, false);
            Sm.GrdColButton(Grd1, new int[] { 10, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 13, 14});
            Grd1.Cols[13].Move(0);
            Grd1.Cols[14].Move(8);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        TxtNo, DteDocDt,
                    }, true);
                    BtnProcess.Enabled = false;
                    TxtNo.Focus();
                    break;
                case mState.Insert:
                    BtnProcess.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtNo, DteDocDt, }, false);
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtNo, DteDocDt });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpLoanImportFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtNo.Text.Length == 0) InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;

            string Number = Sm.GetValue("Select ifnull(Max(Cast(No as Decimal(10, 0))), 0)+1 From TblEmpLoanImport;");
            if (Number.Length == 0) Number = "1";

            if (mIsEmpLoanImportSaveMultiDataOnceEnabled)
            {
                var cml = new List<MySqlCommand>();
                cml.Add(SaveAdvancePayment(Number));
                Sm.ExecCommands(cml);
            }
            else
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 0).Length > 0) 
                        SaveData(Number, r);
            }
            BtnInsertClick(sender, e);
        }

        private MySqlCommand SaveAdvancePayment(string Number)
        {
            // Voucher Request

            string
                AdvancePaymentDocNo = string.Empty,
                VoucherRequestDocNo = string.Empty,
                YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6),
                Yr = YrMth.Substring(2, 2),
                Mth = YrMth.Substring(4, 2),
                DocSeqNo = "4";

            if (mIsDocSeqNoEnabled) DocSeqNo = mDocSeqNo;

            string IterasiNum = string.Empty;
            for (int i = 0; i < Convert.ToInt32(DocSeqNo); i++)
                IterasiNum = string.Concat(IterasiNum, "0");

            var VoucherRequestLastNo = Sm.GetValueDec(
                "Select Convert(Substring(DocNo,7, " + DocSeqNo + "), Decimal) As DocNoTemp " +
                "From TblVoucherRequestHdr " +
                "Where Left(DocDt, 6)=@Param " +
                "Order By Convert(Substring(DocNo,7, " + DocSeqNo + "), Decimal) Desc Limit 1;",
                YrMth
                );

            var AdvancePaymentLastNo = Sm.GetValueDec(
                "Select Convert(Left(DocNo," + DocSeqNo + "), Decimal) As DocNoTemp " +
                "From TblAdvancePaymentHdr " +
                "Where Left(DocDt, 6)=@Param " +
                "Order By Convert(Left(DocNo," + DocSeqNo + "), Decimal) Desc Limit 1;",
                YrMth
                );

            var SQL1 = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var SQL4 = new StringBuilder();
            var SQL5 = new StringBuilder();
            var SQL6 = new StringBuilder();
            var SQL7 = new StringBuilder();
            var Query = new StringBuilder();

            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL1.AppendLine("/* Import Employee Loan */ ");
            SQL1.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                {
                    AdvancePaymentLastNo++;
                    VoucherRequestLastNo++;

                    AdvancePaymentDocNo = string.Concat(Sm.Right(string.Concat(IterasiNum, AdvancePaymentLastNo.ToString()), Convert.ToInt32(DocSeqNo)), "/", mDocTitle, "/", mAdvancePaymentDocAbbr, "/", Mth, "/", Yr);
                    VoucherRequestDocNo = string.Concat(Yr, "/", Mth, "/", Sm.Right(string.Concat(IterasiNum, VoucherRequestLastNo.ToString()), Convert.ToInt32(DocSeqNo)), "/", mVoucherRequestDocAbbr);

                    if (IsFirstOrExisted)
                    {
                        SQL1.AppendLine("Insert Into TblAdvancePaymenthdr(DocNo, DocDt, CancelInd, EmpCode, CreditCode, Amt, InterestRate, InterestRateAmt, Top, StartMth, Yr, Remark, VoucherRequestDocNo, CreateBy, CreateDt) ");
                        SQL1.AppendLine("Values ");

                        SQL2.AppendLine("Insert Into TblVoucherRequestHdr ");
                        SQL2.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
                        SQL2.AppendLine("DocType, AcType, BankAcCode, PIC, DocEnclosure, CurCode, Amt, Remark, CreateBy, CreateDt) ");
                        SQL2.AppendLine("Values ");

                        SQL3.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");

                        SQL4.AppendLine("Insert Into TblAdvancePaymentDtl(DocNo, DNo, Mth, Yr,  Amt, CreateBy, CreateDt) ");
                        SQL4.AppendLine("Values ");

                        SQL5.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                        SQL5.AppendLine("Values ");

                        SQL6.AppendLine("Insert Into TblEmpLoanImport(No, DocDt, EmpCode, AdvancePaymentDocNo, VoucherRequestDocNo, CreateBy, CreateDt) ");
                        SQL6.AppendLine("Values ");
                        
                        IsFirstOrExisted = false;
                    }
                    else
                    {
                        SQL1.AppendLine(", ");
                        SQL2.AppendLine(", ");
                        SQL3.AppendLine(" Union All ");
                        SQL4.AppendLine(", ");
                        SQL5.AppendLine(", ");
                        SQL6.AppendLine(", ");
                    }

                    SQL1.AppendLine(
                        "(@AdvancePaymentDocNo_" + r.ToString() + ", @DocDt, 'N', @EmpCode_" + r.ToString() + 
                        ", @CreditCode_" + r.ToString() + 
                        ", @Amt_" + r.ToString() +
                        ", @InterestRate, @InterestRateAmt, @Top, @StartMth_" + r.ToString() +
                        ", @Yr_" + r.ToString() +
                        ",  @Remark, @VoucherRequestDocNo_" + r.ToString() + ", @UserCode, @Dt) ");

                    SQL2.AppendLine(
                        "(@VoucherRequestDocNo_" + r.ToString() + ", @DocDt, 'N', 'O', @VoucherRequestPayrollDeptCode, " +
                        "'09', 'C', @BankAcCode, @UserCode, 0, @CurCode, " +
                        "@TotalAmt_" + r.ToString() +
                        ", Concat('Advance Payment# : ', @AdvancePaymentDocNo_" + r.ToString() + "), @UserCode, @Dt) ");

                    SQL3.AppendLine("Select DocType, @AdvancePaymentDocNo_" + r.ToString() + ", '001', DNo, @UserCode, @Dt ");
                    SQL3.AppendLine("From TblDocApprovalSetting ");
                    SQL3.AppendLine("Where DocType='AdvancePayment' ");
                    SQL3.AppendLine("And DeptCode Is Not Null ");
                    SQL3.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode_"+r.ToString()+") ");

                    SQL4.AppendLine(
                        "(@AdvancePaymentDocNo_" + r.ToString() + 
                        ", '001', @Mth_" + r.ToString() + 
                        ", @Yr_" + r.ToString() + 
                        ", @Amt_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    SQL5.AppendLine(
                        "(@VoucherRequestDocNo_" + r.ToString() + 
                        ", '001', Concat('Advance Payment# : ', @AdvancePaymentDocNo_" + r.ToString() + 
                        "), @Amt_" + r.ToString() + 
                        ", Null, @UserCode, @Dt) ");

                    SQL6.AppendLine(
                        "(@Number, @DocDt, @EmpCode_" + r.ToString() +
                        ", @AdvancePaymentDocNo_" + r.ToString() +
                        ", @VoucherRequestDocNo_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Query.AppendLine(
                        " , @AdvancePaymentDocNo_" + r.ToString());

                    Sm.CmParam<String>(ref cm, "@AdvancePaymentDocNo_" + r.ToString(), AdvancePaymentDocNo);
                    Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo_" + r.ToString(), VoucherRequestDocNo);
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                    Sm.CmParam<String>(ref cm, "@CreditCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                    Sm.CmParam<Decimal>(ref cm, "@TotalAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                    Sm.CmParam<String>(ref cm, "@StartMth_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7).Length == 2 ? Sm.GetGrdStr(Grd1, r, 7) : string.Concat("0", Sm.GetGrdStr(Grd1, r, 7)));
                    Sm.CmParam<String>(ref cm, "@Mth_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7).Length == 2 ? Sm.GetGrdStr(Grd1, r, 7) : string.Concat("0", Sm.GetGrdStr(Grd1, r, 7)));
                    Sm.CmParam<String>(ref cm, "@Yr_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@AmtMain_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                }
            }
            if (!IsFirstOrExisted)
            {
                SQL1.AppendLine("; ");
                SQL2.AppendLine("; ");
                SQL3.AppendLine("; ");
                SQL4.AppendLine("; ");
                SQL5.AppendLine("; ");
                SQL6.AppendLine("; ");
            }

            SQL7.AppendLine("Update TblAdvancePaymentHdr A ");
            SQL7.AppendLine("Set Status='A' ");
            SQL7.AppendLine("Where DocNo In ('***'");
            SQL7.AppendLine(Query.ToString());
            SQL7.AppendLine(") ");
            SQL7.AppendLine("And DocNo Not In ( ");
            SQL7.AppendLine("   Select DocNo From TblDocApproval ");
            SQL7.AppendLine("   Where DocType='AdvancePayment' ");
            SQL7.AppendLine("   And DocNo In ('***'");
            SQL7.AppendLine(Query.ToString());
            SQL7.AppendLine("   )); ");

            SQL7.AppendLine("Update TblVoucherRequestHdr A ");
            SQL7.AppendLine("Inner Join TblAdvancePaymentHdr B ");
            SQL7.AppendLine("   On B.VoucherRequestDocNo Is Null ");
            SQL7.AppendLine("   And A.DocNo=B.VoucherRequestDocNo ");
            SQL7.AppendLine("   And B.DocNo In ('***'");
            SQL7.AppendLine(Query.ToString());
            SQL7.AppendLine("   ) ");
            SQL7.AppendLine("   And B.Status='A' ");
            SQL7.AppendLine("Set A.Status='A'; ");
            
            cm.CommandText = 
                SQL1.ToString() +
                SQL2.ToString() +
                SQL3.ToString() +
                SQL4.ToString() +
                SQL5.ToString() +
                SQL6.ToString() +
                SQL7.ToString();

            Sm.CmParam<String>(ref cm, "@Number", Number);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DepreciationCode", "1");
            Sm.CmParam<Decimal>(ref cm, "@InterestRate", 0m);
            Sm.CmParam<Decimal>(ref cm, "@InterestRateAmt", 0m);
            Sm.CmParam<Decimal>(ref cm, "@Top", 1);
            Sm.CmParam<Decimal>(ref cm, "@AmtRate", 0m);
            Sm.CmParam<String>(ref cm, "@Remark", string.Empty);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", mBankAcCodeAdvancePayment);
            Sm.CmParam<String>(ref cm, "@VoucherRequestPayrollDeptCode", mVoucherRequestPayrollDeptCode);
            

            return cm;
        }


        private void SaveData(string Number, int r)
        {
            var cml = new List<MySqlCommand>();

            string AdvancePaymentDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AdvancePayment", "TblAdvancePaymenthdr");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();
            cml.Add(SaveAdvancePaymentHdr(Number, AdvancePaymentDocNo, VoucherRequestDocNo, r));

            Sm.ExecCommands(cml);
        }
        
        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false,"Employee's Name is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, r, 5, true, "Amount could not be empty or zero.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, r, 14, false, "Credit Name is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, r, 7, false, "Month is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, r, 8, false, "Year is empty.")) return true;
            }
            return false;
        }

        private MySqlCommand SaveAdvancePaymentHdr(string Number, string AdvancePaymentDocNo, string VoucherRequestDocNo,  int r)
        {
            var SQL = new StringBuilder();

            // untuk tabel baru
            //SQL.AppendLine("Insert Into TblAdvancePaymenthdr(DocNo, DocDt, CancelInd, EmpCode, CreditCode, DepreciationCode, Amt, InterestRate, InterestRateAmt, Top, StartMth, Yr, Remark, VoucherRequestDocNo, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values(@AdvancePaymentDocNo, @DocDt, 'N', @EmpCode, @CreditCode, @DepreciationCode, @Amt, @InterestRate, @InterestRateAmt, @Top, @StartMth, @Yr, @Remark, @VoucherRequestDocNo, @UserCode, CurrentDateTime()); ");

            // untuk tabel lama
            SQL.AppendLine("/* Import Employee Loan */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblAdvancePaymenthdr(DocNo, DocDt, CancelInd, EmpCode, CreditCode, Amt, InterestRate, InterestRateAmt, Top, StartMth, Yr, Remark, VoucherRequestDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AdvancePaymentDocNo, @DocDt, 'N', @EmpCode, @CreditCode, @Amt, @InterestRate, @InterestRateAmt, @Top, @StartMth, @Yr, @Remark, @VoucherRequestDocNo, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @AdvancePaymentDocNo, '001', DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='AdvancePayment' ");
            SQL.AppendLine("And DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@EmpCode); ");

            SQL.AppendLine("Update TblAdvancePaymentHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@AdvancePaymentDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='AdvancePayment' And DocNo=@AdvancePaymentDocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, BankAcCode, PIC, DocEnclosure, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VoucherRequestDocNo, A.DocDt, 'N', A.Status, B.ParValue, ");
            SQL.AppendLine("'09', 'C', @BankAcCode, @UserCode, 0, @CurCode, @TotalAmt, ");
            SQL.AppendLine("Concat('Advance Payment# : ', A.DocNo), A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblAdvancePaymentHdr A ");
            SQL.AppendLine("Inner Join TblParameter B On B.ParCode='VoucherRequestPayrollDeptCode' ");
            SQL.AppendLine("Where A.DocNo=@AdvancePaymentDocNo; ");
            
            // untuk tabel baru
            //SQL.AppendLine("Insert Into TblAdvancePaymentDtl(DocNo, DNo, Mth, Yr, AmtMain, AmtRate, Amt, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values (@AdvancePaymentDocNo, '001', @Mth, @Yr, @AmtMain, @AmtRate, @Amt, @CreateBy, CurrentDateTime()); ");

            // untuk tabel lama
            SQL.AppendLine("Insert Into TblAdvancePaymentDtl(DocNo, DNo, Mth, Yr,  Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@AdvancePaymentDocNo, '001', @Mth, @Yr, @Amt, @CreateBy, @Dt); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, '001', Remark, Amt, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherRequestHdr ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo; ");

            SQL.AppendLine("Insert Into TblEmpLoanImport(No, DocDt, EmpCode, AdvancePaymentDocNo, VoucherRequestDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@Number, @DocDt, @EmpCode, @AdvancePaymentDocNo, @VoucherRequestDocNo, @UserCode, @Dt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AdvancePaymentDocNo", AdvancePaymentDocNo);
            Sm.CmParam<String>(ref cm, "@Number", Number);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<String>(ref cm, "@CreditCode", Sm.GetGrdStr(Grd1, r, 6));
            Sm.CmParam<String>(ref cm, "@DepreciationCode", "1");
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, r, 5));
            Sm.CmParam<Decimal>(ref cm, "@InterestRate", 0m);
            Sm.CmParam<Decimal>(ref cm, "@InterestRateAmt", 0m);
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Sm.GetGrdDec(Grd1, r, 5)); 
            Sm.CmParam<Decimal>(ref cm, "@Top", 1);
            Sm.CmParam<String>(ref cm, "@StartMth", Sm.GetGrdStr(Grd1, r, 7).Length == 2 ? Sm.GetGrdStr(Grd1, r, 7) : string.Concat("0", Sm.GetGrdStr(Grd1, r, 7)));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, r, 7).Length == 2 ? Sm.GetGrdStr(Grd1, r, 7) : string.Concat("0", Sm.GetGrdStr(Grd1, r, 7)));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, r, 8));
            Sm.CmParam<Decimal>(ref cm, "@AmtMain", Sm.GetGrdDec(Grd1, r, 5));
            Sm.CmParam<Decimal>(ref cm, "@AmtRate", 0m);
            Sm.CmParam<String>(ref cm, "@Remark", "");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", mBankAcCodeAdvancePayment);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string No)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpLoanImportHdr(No);
                ShowEmpLoanImportDtl(No);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpLoanImportHdr(string No)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@No", No);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select No, DocDt ");
            SQL.AppendLine("From TblEmploanimport ");
            SQL.AppendLine("Where No=@No;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                    //0
                    "No", 

                    //1-5
                    "DocDt",
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                }, true
            );
        }

        private void ShowEmpLoanImportDtl(string No)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EMpCode, C.EmpCodeOld, C.Empname, D.DeptName, E.PosName, B.Amt,");
            SQL.AppendLine("F.CreditCode, F.CreditName, B.StartMth, B.Yr, A.AdvancePaymentDocNo, A.VoucherRequestDocNO ");
            SQL.AppendLine("From TblEmploanImport A ");
            SQL.AppendLine("Inner join TblAdvancePaymentHdr B On A.AdvancePaymentDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join Tblemployee C On A.EmpCode = C.empcode ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode = D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode = E.PosCode ");
            SQL.AppendLine("Left Join TblCredit F On B.CreditCode = F.CreditCode ");
            SQL.AppendLine("Where A.No=@No Order By C.EmpName; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@No", No);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",
 
                    //1-5
                    "EmpCodeOld", "Empname", "DeptName",  "PosName", "Amt", 
                    
                    //6-10
                    "CreditName", "StartMth", "Yr", "AdvancePaymentDocNo", "VoucherRequestDocNO", 

                    //11
                    "CreditCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Grd1.Cells[Row, 13].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mVoucherRequestDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest';");
            mAdvancePaymentDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='AdvancePayment';");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'BankAcCodeAdvancePayment', 'MainCurCode', 'IsEmpLoanImportSaveMultiDataOnceEnabled', 'VoucherRequestPayrollDeptCode', 'IsDocSeqNoEnabled', ");
            SQL.AppendLine("'DocSeqNo', 'DocTitle' ");            
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDocSeqNoEnabled": mIsDocSeqNoEnabled = (ParValue == "Y"); break;
                            case "IsEmpLoanImportSaveMultiDataOnceEnabled": mIsEmpLoanImportSaveMultiDataOnceEnabled = (ParValue=="Y"); break;

                            //string
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "DocSeqNo": mDocSeqNo = ParValue; break;
                            case "VoucherRequestPayrollDeptCode": mVoucherRequestPayrollDeptCode = ParValue; break;
                            case "BankAcCodeAdvancePayment": mBankAcCodeAdvancePayment = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;

                        }
                    }
                }
                dr.Close();
            }
            if (mDocSeqNo.Length==0) mDocSeqNo = "4";
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + mBankAcCodeAdvancePayment + "' "),
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + mBankAcCodeAdvancePayment + "' "),
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");

        //    var SQL = new StringBuilder();

        //    SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //    SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //    SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //    SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //    if (Type.Length != 0) SQL.Append("And Right(DocNo, '" + Type.Length + "') = '" + Type + "' ");
        //    SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //    SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/' ");
        //    if (Type.Length != 0) SQL.Append(", '" + Type + "' ");
        //    SQL.Append(") As DocNo ");

        //    return Sm.GetValue(SQL.ToString());
        //}

        private void ProcessData2()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;
            
            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();

            ClearGrd();
            try
            {
                Process2a(ref lResult);
                if (lResult.Count > 0)
                {
                    Process2b(ref lResult);
                    Process2c();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process2a(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;
            var EmpCodeTemp = string.Empty;
            var AmtTemp = 0m;
            var CreditCodeTemp = string.Empty;
            var YrTemp = string.Empty;
            var MthTemp = string.Empty;
            int mMaxLengthCreditCode = 0;

            mMaxLengthCreditCode = Int32.Parse(
                                    Sm.GetValue("Select IfNull(Max(Length(CreditCode)), 4) MaxLength From TblCredit Where ActInd = 'Y'; ")
                                    );

            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            { 
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            if (arr[0].Trim().Length != 8)
                            {
                                EmpCodeTemp = Sm.Right(string.Concat("0000000" + arr[0].Trim()), 8);
                                //if (arr[0].Trim().Length == 6)
                                //{
                                //    EmpCodeTemp = string.Concat("00"+arr[0].Trim());
                                //}
                                //else if (arr[0].Trim().Length == 7)
                                //{
                                //    EmpCodeTemp = string.Concat("0" + arr[0].Trim());
                                //}
                            }
                            else
                            {
                                EmpCodeTemp = arr[0].Trim();
                            }
                            string AmtTrim = arr[2].Trim();
                            if (arr[2].Trim().Length > 0)
                                AmtTemp = Decimal.Parse(AmtTrim);
                            else
                                AmtTemp = 0m;
                            if (arr[3].Trim().Length != mMaxLengthCreditCode)
                            {
                                CreditCodeTemp = Sm.Right(string.Concat("00000" + arr[3].Trim()), mMaxLengthCreditCode);
                                //if (arr[3].Trim().Length == 2)
                                //    CreditCodeTemp = string.Concat("00" + arr[3].Trim());
                                //else if (arr[3].Trim().Length == 3)
                                //    CreditCodeTemp = string.Concat("0" + arr[3].Trim());
                                //else if (arr[3].Trim().Length == 1)
                                //    CreditCodeTemp = string.Concat("000" + arr[3].Trim());
                            }
                            else
                            {
                                CreditCodeTemp = arr[3].Trim();
                            }
                            MthTemp = arr[4].Trim();
                            YrTemp = arr[5].Trim();
                            l.Add(new Result()
                            {
                                EmpCode = EmpCodeTemp,
                                EmpCodeOld = string.Empty,
                                EmpName = string.Empty,
                                DeptName = string.Empty,
                                PosName = string.Empty,
                                Amt = AmtTemp,
                                CreditCode = CreditCodeTemp,
                                Mth = MthTemp,
                                Yr = YrTemp,
                            });
                        }
                    }
                }
            }
        }

        private void Process2b(ref List<Result> l)
        {
            //decimal Amt = 0m;
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = l[i].EmpCode;
                r.Cells[1].Value = string.Empty;
                r.Cells[2].Value = string.Empty;
                r.Cells[3].Value = string.Empty;
                r.Cells[4].Value = string.Empty;
                r.Cells[5].Value = l[i].Amt;
                r.Cells[6].Value = l[i].CreditCode;
                r.Cells[7].Value = l[i].Mth;
                r.Cells[8].Value = l[i].Yr;
                r.Cells[13].Value = i+1;
                r.Cells[14].Value = Sm.GetValue("Select CreditName From TblCredit Where ActInd = 'Y' And CreditCode=@Param; ", l[i].CreditCode);
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Grd1.EndUpdate();
        }


        private void Process2c()
        {
            Grd1.BeginUpdate();
         
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EmpCode = string.Empty;
            var Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + Row.ToString(), Sm.GetGrdStr(Grd1, Row, 0));
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, ");
            SQL.AppendLine("B.DeptName, C.PosName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpCodeOld", 
                    "EmpName", 
                    "DeptName", 
                    "PosName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                        {
                            if (EmpCode == Sm.GetGrdStr(Grd1, Row, 0))
                            {
                                Grd1.Cells[Row, 1].Value = Sm.DrStr(dr, c[1]);
                                Grd1.Cells[Row, 2].Value = Sm.DrStr(dr, c[2]);
                                Grd1.Cells[Row, 3].Value = Sm.DrStr(dr, c[3]);
                                Grd1.Cells[Row, 4].Value = Sm.DrStr(dr, c[4]);
                            }
                        }
                    }
                }
                dr.Close();
            }
            Grd1.EndUpdate();
        }

        private string getDataEmployee(string EmpCode, int kode)
        {
            var DataEmp = string.Empty;
            switch(kode)
            {
                case 1:
                    DataEmp = Sm.GetValue("Select Empname from tblEmployee Where EmpCode = '"+EmpCode+"'");
                    break;
                case 2:
                    DataEmp = Sm.GetValue("Select EmpCodeOld from tblEmployee Where EmpCode = '" + EmpCode + "'");
                    break;
                case 3:
                    DataEmp = Sm.GetValue("Select B.DeptName from tblEmployee A Inner Join TblDepartment B On A.DeptCode = B.DeptCode Where A.EmpCode = '" + EmpCode + "'");
                    break;
                case 4:
                    DataEmp = Sm.GetValue("Select B.PosName from tblEmployee A Left Join TblPosition B On A.PosCode = B.PosCode Where A.EmpCode = '" + EmpCode + "'");
                    break;

            }
            return DataEmp;
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd();
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f1 = new FrmAdvancePayment(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f1.ShowDialog();
            }
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f1 = new FrmVoucherRequest(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f1.ShowDialog();
            }

        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f1 = new FrmAdvancePayment(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f1.ShowDialog();
            }

            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f1 = new FrmVoucherRequest(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Button Event

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            ProcessData2();
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public decimal Amt { get; set; }
            public string CreditCode { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }        }

        #endregion

    }
}
