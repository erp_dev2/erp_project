﻿#region Update 
/*
    19/11/2020 [VIN/IMS] new apps
    06/12/2020 [DITA/IMS] data DR yg ke cancel masih muncul dan tambah kolom DR local docno
    06/04/2021 [VIN/IMS] DO ambil dari DO biasa dan DOCDR
    07/04/2021 [WED/IMS] perbaikan query (karena jalur DO Verify ada 2, bisa DOCt biasa dan DOCt based DR)
    17/05/2021 [DITA/IMS] penyesuaian reporting : tambah item dismantle, quantity invoice disesuaikan dengan quantity di sales invoice for project
    04/06/2021 [VIN/IMS] DR terdouble dan belum sesuai DO 
    11/06/2021 [VIN/IMS] join ke tblsocontractdownpayment2dtl2
    16/06/2021 [VIN/IMS] perbaikan query doct (karena jalur DO Verify ada 2, bisa DOCt biasa dan DOCt based DR)
    29/07/2021 [VIN/IMS] Outs SOC dikurangi qty DR bertahap--> tambah process1
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptLOPToVoucher : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptLOPToVoucher(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method 

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            #region Old Code

            //SQL.AppendLine("SELECT A.SPPH, A.LOPDocNo, A.SPHNo, A.BOQDocNo, A.LOPStatus, ");
            //SQL.AppendLine("A.CtName, A.ProjectName, A.ProjectCode, A.PO, A.SOCDocNo, ");
            //SQL.AppendLine("A.No, A.ItCode, A.ItName, A.Specification, A.PackagingUnitUomCode, ");
            //SQL.AppendLine("A.SOCQty, A.SOCPrice, A.BOMDocNo, A.DRDocNo, A.DRQty, ");
            //SQL.AppendLine("A.OutstandingSOQty, A.DeliveryDt, IFNULL(A.DODRDocDt,A.DOCtDocDt) DOCtDocDt, ifnull(A.DOCtDocNo, A.DODRDocNo) DOCtDocNo, A.SJNNO, ");
            //SQL.AppendLine("A.DOQty, ifnull(A.DRQty-(A.DOQty+A.QtyDODR),0.00) OutstandingDRQty, A.LPPBDate, A.DOVDocNo, A.LPPBNo, ");
            //SQL.AppendLine("A.DOVQty, A.NCRQty, ifnull((A.DOQty+A.QtyDODR)-A.DOVQty, 0.00) OutstandingSJNQty, A.InvoiceQty, ifnull(A.DOVQty-A.InvoiceQty, 0.00)OutstandingLPPB, ");
            //SQL.AppendLine("A.SOCDP2DocNo, A.SOCDP2DocDt, A.DRLocalDocNo ");
            //SQL.AppendLine("FROM ( ");

            //SQL.AppendLine("SELECT L.DocNo DODRDocNo, ifnull(M.Qty, 0.00) QtyDODR, L.DocDt DODRDocDt, ");
            //SQL.AppendLine("A.QtLetterNo SPPH, A.DocNo LOPDocNo, B.SPHNo, B.DocNo BOQDocNo, ");
            //SQL.AppendLine("case ");
            //SQL.AppendLine("    when A.Processind='P' then 'On Progress' ");
            //SQL.AppendLine("    when A.Processind='L' then 'Close' ");
            //SQL.AppendLine("    when A.Processind='C' then 'Cancelled' ");
            //SQL.AppendLine("    ELSE  'Lose' ");
            //SQL.AppendLine("END LOPStatus, ");
            //SQL.AppendLine("C.CtName, D.ProjectName, D.PGCode ProjectCode, E.PONo PO, E.DocNo SOCDocNo, F.No, G.ItCodeInternal ItCode, G.ItName, ");
            //SQL.AppendLine("F.Remark Specification, F.PackagingUnitUomCode, ");
            //SQL.AppendLine("ifnull(F.QtyPackagingUnit, 0.00) SOCQty, ifnull(F.UPriceAfTax, 0.00) SOCPrice, H.DocNo BOMDocNo, I.DocNo DRDocNo, ");
            //SQL.AppendLine("IFNULL(I.Qty, 0.00) DRQty, ifnull(F.QtyPackagingUnit-I.Qty, 0.00)OutstandingSOQty, F.DeliveryDt, J.DocDt DOCtDocDt, ");
            //SQL.AppendLine("J.DocNo DOCtDocNo, L.LocalDocNo SJNNO, ifnull(K.Qty, 0.00) DOQty, IFNULL(I.Qty-K.Qty, 0.00) OutstandingDRQty, N.DocDt LPPBDate, ");
            //SQL.AppendLine("N.DocNo DOVDocNo, N.LPPBNo, ifnull(O.Qty, 0.00) DOVQty, ifnull((Q.Qty+Q2.Qty), 0.00) NCRQty, IFNULL(K.Qty-O.Qty, 0.00) OutstandingSJNQty, ");
            //SQL.AppendLine("ifnull(S.Qty, 0.00) InvoiceQty, IFNULL(O.Qty-S.Qty, 0.00) OutstandingLPPB, R.DocNo SOCDP2DocNo, R.DocDt SOCDP2DocDt, I2.LocalDocNo DRLocalDocNo ");
            //SQL.AppendLine("From TblLOPHdr A ");
            //SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNo = B.LOPDocNo ");
            ////SQL.AppendLine("    And A.DocNo = @LOPDocNo ");
            //SQL.AppendLine("    And A.`Status` = 'A' And A.CancelInd = 'N' ");
            //SQL.AppendLine("    And B.`Status` = 'A' ");
            //SQL.AppendLine("    And B.ActInd = 'Y' ");
            //SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            //SQL.AppendLine("Left Join TblProjectGroup D On A.PGCode = D.PGCode ");
            //SQL.AppendLine("Inner Join TblSOContractHdr E On B.DocNo = E.BOQDocNo ");
            //SQL.AppendLine("    And E.`Status` = 'A' ");
            //SQL.AppendLine("    And E.CancelInd = 'N' ");
            //SQL.AppendLine("Inner JOin TblSOContractDtl F On E.DocNo = F.DocNo ");
            //SQL.AppendLine("Inner Join TblItem G On F.ItCode = G.ItCode ");
            //SQL.AppendLine("Left Join TblBOM2Hdr H On H.SOContractDocNo = E.DocNo  And H.ItCode = F.ItCode ");
            //SQL.AppendLine("   And H.CancelInd = 'N' And H.`Status` = 'A' ");
            //SQL.AppendLine("Left Join TblDRDtl I On F.DocNo = I.SODocNo And F.DNo = I.SODNo AND I.DocNo IN (SELECT DocNo FROM TblDRHdr WHERE CancelInd = 'N') ");
            //SQL.AppendLine("Left Join TblDRHdr I2 ON I.DocNo=I2.DocNo  ");
            //SQL.AppendLine("Left Join TblDOCtHdr J On E.DocNo = J.SOContractDocNo And J.`Status` = 'A' ");
            //SQL.AppendLine("Left Join TblDOCtDtl K On J.DocNo = K.DocNo And K.CancelInd = 'N' AND K.SOContractDNo = F.DNo");
            //SQL.AppendLine("Left Join TblDOCt2Hdr L On I.DocNo = L.DRDocNo And L.DRDocNo Is Not Null ");
            //SQL.AppendLine("Left Join TblDOCt2Dtl M On L.DocNo = M.DocNo AND M.SOContractDocno = F.DocNo AND M.SOCOntractDNo = F.DNo ");
            //SQL.AppendLine("Left Join TblDOCtVerifyHdr N On L.DocNo = N.DOCt2DocNo And N.CancelInd = 'N' ");
            //SQL.AppendLine("Left Join TblDOCtVerifyDtl O On N.DocNo = O.DocNo And O.DOCt2DNo = M.DNo ");
            ////SQL.AppendLine("Left Join TblRecvCtHdr P On P.DOCtDocNo = J.DocNo ");
            //SQL.AppendLine("Left Join TblRecvCtDtl Q On Q.DOCtDocNo = J.DocNo And Q.DOCtDNo = K.DNo And Q.CancelInd = 'N' ");
            //SQL.AppendLine("Left Join TblRecvCtDtl Q2 On Q2.DOCtDocNo = L.DocNo And Q2.DOCtDNo = M.DNo And Q.CancelInd = 'N' ");
            ////SQL.AppendLine("Left Join TblRecvCtDtl Q On P.DocNo = Q.DocNo And Q.DOCtDNo = K.DNo And Q2.CancelInd = 'N' ");
            //SQL.AppendLine("Left Join TblSOContractDownpayment2Hdr R On R.SOContractDocNo = E.DocNo And R.CancelInd = 'N' ");
            //SQL.AppendLine("Left Join TblSOContractDownpayment2Dtl S On R.DocNo = S.DocNo And S.DOCtVerifyDNo = O.DNo ");
            //SQL.AppendLine("Left Join TblIncomingPaymentDtl T On T.InvoiceDocNo = R.DocNo ");
            //SQL.AppendLine("Left Join TblIncomingPaymentHdr U On T.DocNo = U.DocNo And U.CancelInd = 'N' ");

            //SQL.AppendLine(") A ");

            #endregion

            SQL.AppendLine("SELECT ");
            SQL.AppendLine("A.SPPH, A.LOPDocNo, A.SPHNo, A.BOQDocNo, A.LOPStatus, ");
            SQL.AppendLine("A.CtName, A.ProjectName, A.ProjectCode, A.PO, A.SOCDocNo, ");
            SQL.AppendLine("A.No, A.ItCode, A.ItName, A.Specification, A.PackagingUnitUomCode, ");
            SQL.AppendLine("A.SOCQty, A.SOCPrice, A.BOMDocNo, A.DRDocNo, A.DRQty, ");
            SQL.AppendLine("A.OutstandingSOQty, A.DeliveryDt, IFNULL(A.DODRDocDt,A.DOCtDocDt) DOCtDocDt, ifnull(A.DOCtDocNo, A.DODRDocNo) DOCtDocNo, A.SJNNO, ");
            SQL.AppendLine("A.DOQty, Case A.DocType When '1' Then (A.SOCQty - IfNull(A.DOQty, 0.00)) Else ifnull((A.DRQty-A.DOQty),0.00) End As OutstandingDRQty, A.LPPBDate, A.DOVDocNo, A.LPPBNo, ");
            SQL.AppendLine("A.DOVQty, A.NCRQty, ifnull((A.DOQty-A.DOVQty), 0.00) OutstandingSJNQty, A.InvoiceQty, ifnull((A.DOVQty-A.InvoiceQty), 0.00) OutstandingLPPB, ");
            SQL.AppendLine("A.SOCDP2DocNo, A.SOCDP2DocDt, A.DRLocalDocNo, A.ItNameDismantle ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("SELECT J.DocType, J.DocNo DODRDocNo, IfNull(J.Qty, 0.00) QtyDODR, J.DocDt DODRDocDt, ");
            SQL.AppendLine("A.QtLetterNo SPPH, A.DocNo LOPDocNo, B.SPHNo, B.DocNo BOQDocNo, ");
            SQL.AppendLine("case ");
            SQL.AppendLine("    when A.Processind='P' then 'On Progress' ");
            SQL.AppendLine("    when A.Processind='L' then 'Close' ");
            SQL.AppendLine("    when A.Processind='C' then 'Cancelled' ");
            SQL.AppendLine("    ELSE  'Lose' ");
            SQL.AppendLine("END LOPStatus, ");
            SQL.AppendLine("C.CtName, D.ProjectName, D.PGCode ProjectCode, E.PONo PO, E.DocNo SOCDocNo, F.No, G.ItCodeInternal ItCode, G.ItName, ");
            SQL.AppendLine("F.Remark Specification, F.PackagingUnitUomCode, ");
            SQL.AppendLine("ifnull(F.QtyPackagingUnit, 0.00) SOCQty, ifnull(F.UPriceAfTax, 0.00) SOCPrice, H.DocNo BOMDocNo, I.DocNo DRDocNo, ");
            SQL.AppendLine("IFNULL(I.Qty, 0.00) DRQty, (F.QtyPackagingUnit - IfNull(I.Qty, 0.00)) OutstandingSOQty, F.DeliveryDt, J.DocDt DOCtDocDt, ");
            SQL.AppendLine("J.DocNo DOCtDocNo, J.LocalDocNo SJNNO, ifnull(J.Qty, 0.00) DOQty, (IFNULL(I.Qty, 0.00) - IfNull(J.Qty, 0.00)) OutstandingDRQty, K.DocDt LPPBDate, ");
            SQL.AppendLine("K.DocNo DOVDocNo, K.LPPBNo, ifnull(K.Qty, 0.00) DOVQty, ifnull(L.Qty, 0.00) NCRQty, (IFNULL(J.Qty, 0.00) - IfNull(K.Qty, 0.00)) OutstandingSJNQty, ");
            SQL.AppendLine("ifnull(N.Qty, 0.00) InvoiceQty, (IFNULL(K.Qty, 0.00) - IfNull(N.Qty, 0.00)) OutstandingLPPB, M.DocNo SOCDP2DocNo, M.DocDt SOCDP2DocDt, I2.LocalDocNo DRLocalDocNo, F2.ItNameDismantle ");
            SQL.AppendLine("From TblLOPHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNo = B.LOPDocNo ");
            SQL.AppendLine("    And A.`Status` = 'A' And A.CancelInd = 'N' ");
            SQL.AppendLine("    And B.`Status` = 'A' ");
            SQL.AppendLine("    And B.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL.AppendLine("Left Join TblProjectGroup D On A.PGCode = D.PGCode ");
            SQL.AppendLine("Inner Join TblSOContractHdr E On B.DocNo = E.BOQDocNo ");
            SQL.AppendLine("    And E.`Status` = 'A' ");
            SQL.AppendLine("    And E.CancelInd = 'N' ");
            SQL.AppendLine("Inner JOin TblSOContractDtl F On E.DocNo = F.DocNo ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("    Select X1.DocNo, X1.No, X1.ItCode, X4.ItName ItNameDismantle ");
            SQL.AppendLine("    From TblSOContractDtl4 X1 ");
            SQL.AppendLine("    Inner Join TblBOQDtl2 X2 On X1.BOQDocNo = X2.DocNo ");
            SQL.AppendLine("    And X1.ItCode = X2.ItCode And X1.SeqNo = X2.SeqNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr X3 On X2.DocNo = X3.DocNo And X3.ActInd = 'Y' And X3.`Status` = 'A' ");
            SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCodeDismantle = X4.ItCode  ");
            SQL.AppendLine(") F2 On F.DocNo=F2.DocNo And F.No = F2.No And F.ItCode = F2.ItCode  ");
            SQL.AppendLine("Inner Join TblItem G On F.ItCode = G.ItCode ");
            SQL.AppendLine("Left Join TblBOM2Hdr H On H.SOContractDocNo = E.DocNo  And H.ItCode = F.ItCode ");
            SQL.AppendLine("   And H.CancelInd = 'N' And H.`Status` = 'A' ");
            SQL.AppendLine("Left Join TblDRDtl I On F.DocNo = I.SODocNo And F.DNo = I.SODNo AND I.DocNo IN (SELECT DocNo FROM TblDRHdr WHERE CancelInd = 'N')  ");
            SQL.AppendLine("Left Join TblDRHdr I2 ON I.DocNo=I2.DocNo And I2.CancelInd = 'N' ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select '1' As DocType, Null As DRDocNo,  T1.DocNo, T1.DocDt, T2.DNo, T1.SOContractDocNo, T2.SOContractDNo, T2.Qty, T1.DocNoInternal As LocalDocNo ");
            SQL.AppendLine("    From TblDOCtHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.`Status` = 'A' ");
            SQL.AppendLine("        And T1.SOContractDocNo Is Not Null ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As DocType, T1.DRDocNo, T1.DocNo, T1.DocDt, T2.DNo, T2.SOContractDocNo, T2.SOContractDNo, T2.Qty, T1.LocalDocNo ");
            SQL.AppendLine("    From TblDOCt2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DRDocNo Is Not Null ");
            SQL.AppendLine("        And T2.CancelInd = 'N' ");
            SQL.AppendLine("        And T2.SOContractDocNo Is Not Null ");
            SQL.AppendLine(") J On F.DocNo = J.SOContractDocNo And F.DNo = J.SOContractDNo ");
            SQL.AppendLine("AND  CASE  ");
            SQL.AppendLine("        WHEN J.DocType = '2'  THEN J.DRDocNo = I2.DocNo ");
            SQL.AppendLine("        ELSE 0=0 ");
            SQL.AppendLine("END  ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DocDt, T1.LPPBNo, T2.DNo, T1.DOCtDocNo, T2.DOCtDNo, T2.Qty ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo, T1.DocDt, T1.LPPBNo, T2.DNo, T1.DOCt2DocNo As DOCtDocNo, T2.DOCt2DNo As DOCtDNo, T2.Qty ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.DOCt2DocNo Is Not Null ");                
            SQL.AppendLine(") K On J.DocNo = K.DOCtDocNo And J.DNo = K.DOCtDNo ");
            SQL.AppendLine("Left Join TblRecvCtDtl L On J.DocNo = L.DOCtDocNo And J.DNo = L.DOCtDNo And L.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblSOContractDownpayment2Hdr M On E.DocNo = M.SOContractDocNo And M.CancelInd = 'N' ");
            SQL.AppendLine("    And M.`Status` = 'A' ");
            //SQL.AppendLine("Left Join TblSOContractDownpayment2Dtl N On M.DocNo = N.DocNo ");
            //SQL.AppendLine("    And N.DOCtVerifyDocNo = K.DocNo ");
            //SQL.AppendLine("    And N.DOCtVerifyDNo = K.DNo ");
            SQL.AppendLine("Left JOIN ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    SELECT B.DOCtVerifyDocNo, B.DOCtVerifyDNo, Qty, A.DocNo "); 
	        SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A  ");
	        SQL.AppendLine("    inner join tblsocontractdownpayment2dtl B on A.DocNo=B.DocNo ");
	        SQL.AppendLine("    WHERE A.CancelInd='N' ");
	        SQL.AppendLine("    UNION ALL  ");
	        SQL.AppendLine("    SELECT B.DOCtVerifyDocNo, B.DOCtVerifyDNo, Qty, A.DocNo  ");
	        SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A  ");
	        SQL.AppendLine("    inner join tblsocontractdownpayment2dtl2 B on A.DocNo=B.DocNo ");
	        SQL.AppendLine("    WHERE A.CancelInd='N' ");
            SQL.AppendLine(")N On M.DocNo = N.DocNo And N.DOCtVerifyDocNo = K.DocNo And N.DOCtVerifyDNo = K.DNo "); 
            SQL.AppendLine("Left Join TblIncomingPaymentDtl O On M.DocNo = O.InvoiceDocNo ");
            SQL.AppendLine("    And O.DocNo In (Select DocNo From TblIncomingPaymentHdr Where CancelInd = 'N' And Status = 'A') ");
            SQL.AppendLine("Left Join TblIncomingPaymentHdr P On O.DocNo = P.DocNo And P.CancelInd = 'N' And P.`Status` = 'A' ");
            SQL.AppendLine(") A ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 40;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "SPPH#", 
                        "LOP#",
                        "SPH#",
                        "BOQ#",
                        "Status"+Environment.NewLine+"(LOP)",
                        
                        //6-10
                        "Customer",
                        "Project Name",
                        "Project Code",
                        "Customer's PO",
                        "SO Contract#",

                        //11-15
                        "No",
                        "Material Code",
                        "Material Name",
                        "Specification",
                        "UOM",

                        //16-20
                        "Quantity",
                        "Price",
                        "BOM#",
                        "DR#",
                        "Quantity",

                        //21-25
                        "Outstanding"+Environment.NewLine+"SO Quantity",
                        "Delivery Date",
                        "Date",
                        "DO To Customer#",
                        "SJN",

                        //26-30
                        "Quantity",
                        "Outstanding"+Environment.NewLine+"DR Quantity",
                        "Date",
                        "DO To Customer Verification#",
                        "LPPB",

                        //31-35
                        "Quantity",
                        "NCR",
                        "Outstanding"+Environment.NewLine+"SJN/BAK",
                        "Quantity Invoice",
                        "Outstanding"+Environment.NewLine+"LPPB//BAPP",

                        //36-39
                        "Invoice#",
                        "Date",
                        "STTP#",
                        "Item Dismantle Name"
                    },
                     new int[] 
                    {
                        //0
                        30,

                        //1-5
                        150, 150, 150, 150, 100, 
                        
                        //6-10
                        200, 150, 150, 150, 150, 

                        //11-15
                        30, 150, 200, 150, 80, 

                        //16-20
                        100, 100, 150, 150, 100, 

                        //21-25
                        100, 100, 100, 150, 150, 

                        //26-30
                        100, 100, 100, 150, 150, 

                        //31-35
                        100, 100, 100, 100, 100, 

                        //36-39
                        150, 100, 120, 180
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 16, 17, 20, 21, 26, 27, 31, 32, 33, 34, 35 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 22, 23, 28, 37 });
            Grd1.Cols[38].Move(20);
            Grd1.Cols[39].Move(13);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtLOPDocNo.Text, new string[] { "A.LOPDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtBOMDocNo.Text, new string[] { "A.BOMDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtBOQDocNo.Text, new string[] { "A.BOQDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtDOCtDocNo.Text, new string[] { "A.DOCtDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtDOVDocNo.Text, new string[] { "A.DOVDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtDRDocNo.Text, new string[] { "A.DRDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtSOCDocNo.Text, new string[] { "A.SOCDocNo" });
               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "SPPH",  

                            //1-5
                            "LOPDocNo", "SPHNo", "BOQDocNo", "LOPStatus", "CtName", 

                            //6-10
                            "ProjectName", "ProjectCode", "PO", "SOCDocNo", "No",

                            //11-15
                            "ItCode", "ItName", "Specification", "PackagingUnitUomCode", "SOCQty", 

                            //16-20
                            "SOCPrice", "BOMDocNo", "DRDocNo", "DRQty", "OutstandingSOQty", 

                            //21-25
                            "DeliveryDt", "DOCtDocDt", "DOCtDocNo", "SJNNO", "DOQty",

                            //26-30
                            "OutstandingDRQty", "LPPBDate", "DOVDocNo", "LPPBNo", "DOVQty", 

                            //31-35
                            "NCRQty", "OutstandingSJNQty", "InvoiceQty", "OutstandingLPPB", "SOCDP2DocNo", 

                            //36-38
                            "SOCDP2DocDt", "DRLocalDocNo", "ItNameDismantle"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);

                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);

                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 32); 
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 34);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 35);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 37, 36);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 37);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 38);
                        }, true, false, false, false
                    );
                Process1();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void Process1()
        {
            if (Grd1.Rows.Count > 1)
            {
                string SOCDocNo = string.Empty, ItCode = string.Empty;
                for (int i = 1; i < Grd1.Rows.Count; i++)
                {
                    if (SOCDocNo.Length == 0) SOCDocNo = Sm.GetGrdStr(Grd1, i, 10);
                    if (ItCode.Length == 0) ItCode = Sm.GetGrdStr(Grd1, i, 12);
                    if (SOCDocNo == Sm.GetGrdStr(Grd1, i, 10) && ItCode == Sm.GetGrdStr(Grd1, i, 12))
                    {
                        Grd1.Cells[i, 21].Value = Sm.GetGrdDec(Grd1, i - 1, 21) - Sm.GetGrdDec(Grd1, i, 20);
                    }
                    else
                    {
                        SOCDocNo = Sm.GetGrdStr(Grd1, i, 10);
                        ItCode = Sm.GetGrdStr(Grd1, i, 12);
                    }
                }
            }
        }

        #endregion


        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Event
        private void TxtLOPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLOPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "LOP#");
        }

        private void TxtBOQDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBOQDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "BOQ#");
        }

        private void TxtSOCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOCDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void TxtBOMDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBOMDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "BOM#");
        }

        private void TxtDOCtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDOCtDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO To Customer#");
        }

        private void TxtDRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Delivery Request#");
        }

        private void TxtDOVDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDOVDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO Verification#");
        }
        #endregion 

    }
}
