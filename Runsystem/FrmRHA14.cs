﻿#region Update
/* 
    03/05/2020 [TKG] New application
    30/09/2022 [BRI/SKI] bug ecxel
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

//using System.IO;
//using System.Net;
//using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmRHA14 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mMainCurCode = string.Empty;
        internal FrmRHA14Find FrmFind;
        
        #endregion

        #region Constructor

        public FrmRHA14(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Religius Holiday Allowance";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueCurCode(ref LueCurCode);
                
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "", 

                        //1-5
                        "Employee's Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",

                        //6-10
                        "Religion",
                        "Join",
                        "Resign",
                        "Bank",
                        "Bank"+Environment.NewLine+"Account#",
                        
                        //11-15
                        "Salary (A)",
                        "Allowance (B)",
                        "A+B",
                        "Prorate",
                        "Tax",

                        //16
                        "Amount",
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        130, 200, 150, 150, 150,  
                        
                        //6-10
                        100, 100, 100, 150, 120, 

                        //11-15
                        120, 120, 120, 80, 120,

                        //16
                        120
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColCheck(Grd1, new int[] { 14 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 15, 16 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16 });

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, DteHolidayDt, LueSiteCode, 
                        LuePaymentType, LueBankAcCode, MeeRemark 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteHolidayDt, LuePaymentType, LueBankAcCode, LueSiteCode, 
                        MeeRemark 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, DteHolidayDt, 
                LueSiteCode, LuePaymentType, LueBankAcCode, LueCurCode, 
                TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 14 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 13, 15, 16 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        private void ClearGrd1()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 13, 15, 16 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRHA14Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RHA", "TblRHAHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveRHA(DocNo));
            
            Sm.ExecCommands(cml);

            Sm.StdMsg(mMsgType.Info, 
                "New document# : " + DocNo + Environment.NewLine +
                "Saving data is completed.");

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteHolidayDt, "Holiday date") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtAmt, "Total amount", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsEmployeeNotValid() ||
                IsEmployeeWithTheSameRHAYrExisted()
                ;
        }

        private bool IsEmployeeWithTheSameRHAYrExisted()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty, Filter = string.Empty;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Concat(EmpName, ' (', EmpCode, ')')  From TblEmployee ");
            SQL.AppendLine("Where EmpCode In (");
            SQL.AppendLine("    Select T2.EmpCode ");
            SQL.AppendLine("    From TblRHAHdr T1 ");
            SQL.AppendLine("    Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And Left(HolidayDt, 4)=@HolidayYr ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Limit 1;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@HolidayYr", Sm.Left(Sm.GetDte(DteHolidayDt), 4));

            EmpCode = Sm.GetValue(cm);

            if (EmpCode.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee : " + EmpCode + Environment.NewLine +
                    "This employee already processed in another document."
                    );
                return true;
            }
            return false;
        }   

        private bool IsEmployeeNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    if (IsEmployeeNotValid(r)) return true;
            }
            return false;
        }

        private bool IsEmployeeNotValid(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Inner Join TblRHADtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Holidaydt=@HolidayDt ");
            SQL.AppendLine("And A.Status In ('O', 'A') Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 1));

            return Sm.IsDataExist(cm,
                "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                "This employee already processed in another document."
                );
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Employee is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, r, 16, true, 
                    "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine + Environment.NewLine +
                    "Amount should be bigger than 0.")
                ) return true;
            }
            return false;
        }

        private MySqlCommand SaveRHA(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
            SQL.AppendLine("Set @VoucherRequestDocNo:=");
            SQL.AppendLine(Sm.GetNewVoucherRequestDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr"));
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblRHAHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, HolidayDt, DeptCode, SiteCode, ");
            SQL.AppendLine("PaymentType, BankAcCode, CSVInd, PayrunCode, CurCode, Amt, VoucherRequestDocNo, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'O', @HolidayDt, Null, @SiteCode, ");
            SQL.AppendLine("@PaymentType, @BankAcCode, 'N', Null, @CurCode, @Amt, @VoucherRequestDocNo, @Remark, ");
            SQL.AppendLine("@UserCode, @Dt); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirst)
                    {
                        SQL.AppendLine("Insert Into TblRHADtl ");
                        SQL.AppendLine("(DocNo, EmpCode, DeptCode, Value, Value2, Value3, Tax, Amt, ProrateInd, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirst = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @EmpCode_" + r.ToString() + ", (Select DeptCode From TblEmployee Where EmpCode=@EmpCode_" + r.ToString() + "), @Value_" + r.ToString() + ", @Value2_" + r.ToString() + ", ");
                    SQL.AppendLine("@Value3_" + r.ToString() + ", @Tax_" + r.ToString() + ", @Amt_" + r.ToString() + ", @ProrateInd_" + r.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Value_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@Value2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 12));
                    Sm.CmParam<Decimal>(ref cm, "@Value3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@Tax_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 16));
                    Sm.CmParam<String>(ref cm, "@ProrateInd_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 14) ? "Y" : "N");
                }
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, BankAcCode, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='VoucherRequestPayrollDeptCode'), ");
            SQL.AppendLine("'13', 'C', @PaymentType, @BankAcCode, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@UserCode), ");
            SQL.AppendLine("0, @CurCode, @Amt, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl ");
            SQL.AppendLine("(DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', @Remark, @Amt, @UserCode, @Dt);");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='RHA'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RHA' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblRHAHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RHA' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelRHAHdr());
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyProcessed();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblRHAHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsDataAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select 1 From TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "This document already processed to voucher."
                );
        }

        private MySqlCommand CancelRHAHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRHAHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status In ('O', 'A'); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status In ('O', 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRHAHdr(DocNo);
                ShowRHADtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRHAHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.HolidayDt, A.SiteCode, A.PaymentType, A.BankAcCode, ");
            SQL.AppendLine("A.CurCode, A.Amt, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "StatusDesc", "HolidayDt", 
                    
                    //6-10
                    "SiteCode", "PaymentType", "BankAcCode", "CurCode", "Amt",
                    
                    //11-13
                     "VoucherRequestDocNo", "VoucherDocNo", "Remark"

                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                     TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                     Sm.SetDte(DteHolidayDt, Sm.DrStr(dr, c[5]));
                     Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[7]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[8]));
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[9]));
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[11]);
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[12]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                 }, true
             );
        }

        private void ShowRHADtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.OptDesc As Religious, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, F.BankName, B.BankAcNo, ");
            SQL.AppendLine("A.Value3, A.Value2, A.Value, A.Tax, A.Amt, A.ProrateInd ");
            SQL.AppendLine("From TblRHADtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.Religion=E.OptCode And E.OptCat='Religion' ");
            SQL.AppendLine("Left Join TblBank F On B.BankCode=F.BankCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.EmpName;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode", 

                        //1-5
                        "EmpName", "EmpCodeOld","PosName", "DeptName", "Religious", 

                        //6-10
                        "JoinDt", "ResignDt", "BankName", "BankAcNo", "Value", 
                        
                        //11-15
                        "Value2", "Value3", "ProrateInd" ,"Tax", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] {11, 12, 13, 15, 16 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.UserName, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When A.LastUpDt Is Not Null Then A.LastUpDt Else Null End As LastUpDt, A.Remark ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='RHA' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status In ('A', 'C') ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            if (Grd2.Rows.Count > 0)
                Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        internal void ComputeAmt()
        {
            var Amt = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Amt += Sm.GetGrdDec(Grd1, r, 16);
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 3].Value = "'" + Sm.GetGrdStr(Grd1, Row, 3);
                Grd1.Cells[Row, 10].Value = "'" + Sm.GetGrdStr(Grd1, Row, 10);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if(Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                    Grd1.Cells[Row, 3].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 3).Length - 1);
                if (Sm.GetGrdStr(Grd1, Row, 10).Length > 0)
                    Grd1.Cells[Row, 10].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 10), Sm.GetGrdStr(Grd1, Row, 10).Length - 1);
            }
            Grd1.EndUpdate();
        }

        private void DteHolidayDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd1();
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
                ClearGrd1();
            }
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue2(Sl.SetLueOption), "VoucherPaymentType");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                        if (e.ColIndex == 0 &&
                            !Sm.IsDteEmpty(DteHolidayDt, "Holiday date") &&
                            !Sm.IsLueEmpty(LueSiteCode, "Site"))
                        {
                            e.DoDefault = false;
                            if (e.KeyChar == Char.Parse(" "))
                                Sm.FormShowDialog(new FrmRHA14Dlg(
                                    this, 
                                    Sm.GetDte(DteHolidayDt), 
                                    Sm.GetLue(LueSiteCode)
                                    ));
                        }
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (
                    e.ColIndex == 0 &&
                    !Sm.IsDteEmpty(DteHolidayDt, "Holiday date") &&
                    !Sm.IsLueEmpty(LueSiteCode, "Site")
                    )
                    Sm.FormShowDialog(new FrmRHA14Dlg(
                        this,
                        Sm.GetDte(DteHolidayDt),
                        Sm.GetLue(LueSiteCode)
                        ));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }


        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
           ComputeAmt();
        }

        #endregion

        #region Button Event

        private void BtnVoucherRequest_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                //var f = new FrmVoucherRequest("***");
                //f.Tag = "***";
                //f.WindowState = FormWindowState.Normal;
                //f.StartPosition = FormStartPosition.CenterScreen;
                //f.mDocNo = TxtVoucherRequestDocNo.Text;
                //f.ShowDialog();
            }
        }

        private void BtnVoucher_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                //var f = new FrmVoucher("***");
                //f.Tag = "***";
                //f.WindowState = FormWindowState.Normal;
                //f.StartPosition = FormStartPosition.CenterScreen;
                //f.mDocNo = TxtVoucherDocNo.Text;
                //f.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
