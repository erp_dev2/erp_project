﻿#region Update
/*
    26/04/2017 [WED] list MTS yang muncul yang statusnya = 'A'
    12/12/2019 [WED/MAI] panggil fungsi untuk otomatis isi item dan detail nya, berdasarkan parameter IsProductionOrderUseAutoRoutingItem
    13/01/2021 [WED/KSM] tambah tarik dari Sales Contract berdasarkan parameter IsSalesContractEnabled
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionOrderDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmProductionOrder mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProductionOrderDlg(FrmProductionOrder FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueCtCode(ref LueCtCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);

                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "DNo",
                        "",
                        "Date",
                        "Customer",
                        
                        //6-10
                        "Agent",
                        "Item's"+Environment.NewLine+"Code", 
                        "",
                        "Item's"+Environment.NewLine+"Name",
                        "Local Code",

                        //11-15
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "UoM",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "UoM",

                        //16-20
                        "DeliveryDate/"+Environment.NewLine+"Usage Date",
                        "CodeInd",
                        "Remark",
                        "Convert12",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 0, 20, 80, 200, 
                        
                        //6-10
                        180, 100, 20, 180, 100,  
                        
                        //11-15
                        100, 100, 60, 100, 60, 
                        
                        //16-20
                        100, 0, 150, 0, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3, 8 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 19 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 16 });
            Grd1.Cols[20].Move(11);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 7, 8, 10, 11, 17, 19, 20 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 10, 11, 20 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DNo, DocDt, CtName, AgtName, ");
            SQL.AppendLine("ItCode, ItName, ItCodeInternal, CtItCode, Specification, ");
            SQL.AppendLine("Qty, Qty*PlanningUomCodeConvert12 As Qty2, PlanningUomCode, PlanningUomCode2, ");
            SQL.AppendLine("DeliveryDt, CodeInd, Remark, PlanningUomCodeConvert12, CtCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.DocNo, B.DNo, A.DocDt, A.CtCode, E.CtName, F.AgtName, ");
            SQL.AppendLine("    D.ItCode, G.ItName, G.ItCodeInternal, J.CtItCode, G.Specification, ");
            SQL.AppendLine("    B.Qty-IfNull(H.ProductionOrderQty, 0)-IfNull(I.DRQty, 0) As Qty, G.PlanningUomCodeConvert12, ");
            SQL.AppendLine("    G.PlanningUomCode, G.PlanningUomCode2, B.DeliveryDt, '1' As CodeInd, B.Remark ");
            SQL.AppendLine("    From TblSOHdr A ");
            SQL.AppendLine("    Inner Join TblSODtl B ");
            SQL.AppendLine("        On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And B.ProcessInd<>'F' ");
            SQL.AppendLine("        And B.ProcessInd3<>'F' ");
            SQL.AppendLine("    Left Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
            SQL.AppendLine("    Left Join TblItemPriceDtl D On C.ItemPriceDocNo=D.DocNo And C.ItemPriceDNo=D.DNo ");
            SQL.AppendLine("    Left Join TblCustomer E On A.CtCode=E.CtCode ");
            SQL.AppendLine("    Left Join TblAgent F On B.AgtCode=F.AgtCode ");
            SQL.AppendLine("    Left Join TblItem G On D.ItCode=G.ItCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.SODocNo, T1.SODNo, Sum(T1.Qty) As ProductionOrderQty ");
            SQL.AppendLine("        From TblProductionOrderHdr T1 ");
            SQL.AppendLine("        Inner Join TblSODtl T2 ");
            SQL.AppendLine("            On T1.SODocNo=T2.DocNo And T1.SODNo=T2.DNo ");
            SQL.AppendLine("            And T2.ProcessInd<>'F' And T2.ProcessInd3<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' And T1.DocType='1' ");
            SQL.AppendLine("        Group By T1.SODocNo, T1.SODNo ");
            SQL.AppendLine("    ) H On A.DocNo=H.SODocNo And B.DNo=H.SODNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.SODocNo, T2.SODNo, Sum(T2.Qty) As DRQty ");
            SQL.AppendLine("        From TblDRHdr T1 ");
            SQL.AppendLine("        Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join TblSODtl T3 ");
            SQL.AppendLine("            On T2.SODocNo=T3.DocNo And T2.SODNo=T3.DNo ");
            SQL.AppendLine("            And T3.ProcessInd<>'F' And T3.ProcessInd3<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T2.SODocNo, T2.SODNo ");
            SQL.AppendLine("    ) I On A.DocNo=I.SODocNo And B.DNo=I.SODNo ");
            SQL.AppendLine("    Left Join TblCustomerItem J On A.CtCode=J.CtCode And D.ItCode=J.ItCode ");
            SQL.AppendLine("    Where A.Status Not In ('M', 'F') ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And B.Qty-IfNull(H.ProductionOrderQty, 0)-IfNull(I.DRQty, 0)>0 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select A.DocNo, B.DNo, A.DocDt, Null As CtCode, Null As CtName, Null As AgtName, ");
            SQL.AppendLine("    B.ItCode, C.ItName, C.ItCodeInternal, Null As CtItCode, C.Specification, ");
            SQL.AppendLine("    B.Qty-IfNull(D.MakeToStockQty, 0) As Qty, C.PlanningUomCodeConvert12, ");
            SQL.AppendLine("    C.PlanningUomCode, C.PlanningUomCode2, B.UsageDt As DeliveryDt, '2' As CodeInd, A.Remark ");
            SQL.AppendLine("    From TblMakeToStockHdr A ");
            SQL.AppendLine("    Inner Join TblMakeToStockDtl B On A.DocNo = B.DocNo And B.ProcessInd<>'F' ");
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.MakeToStockDocNo, T1.MakeToStockDNo, Sum(T1.Qty) As MakeToStockQty ");
            SQL.AppendLine("        From TblProductionOrderHdr T1 ");
            SQL.AppendLine("        Inner Join TblMakeToStockDtl T2 ");
            SQL.AppendLine("            On T1.MakeToStockDocNo=T2.DocNo And T1.MakeToStockDNo=T2.DNo ");
            SQL.AppendLine("            And T1.ProcessInd<>'F' ");
            SQL.AppendLine("        Inner Join TblMakeToStockHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' ");
            SQL.AppendLine("        Where T1.CancelInd='N' And T1.DocType='2' ");
            SQL.AppendLine("        Group By T1.MakeToStockDocNo, T1.MakeToStockDNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.MakeToStockDocNo And B.DNo=D.MakeToStockDNo ");
            SQL.AppendLine("    Where A.CancelInd = 'N' And A.Status='A' ");
            if (mFrmParent.mIsSalesContractEnabled)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("    Select A.DocNo, C.DNo, A.DocDt, B.CtCode, E.CtName, Null As AgtName, ");
                SQL.AppendLine("    C.ItCode, D.ItName, D.ItCodeInternal, H.CtItCode, D.Specification, ");
                SQL.AppendLine("    (C.Qty - IfNull(F.ProductionOrderQty, 0.00) - IfNull(G.DRQty, 0.00)) Qty, D.PlanningUomCodeConvert12, ");
                SQL.AppendLine("    D.PlanningUomCode, D.PlanningUomCode2, C.DeliveryDt, '3' As CodeInd, C.Remark ");
                SQL.AppendLine("    From TblSalesContract A ");
                SQL.AppendLine("    Inner Join TblSalesMemoHdr B On A.SalesMemoDocNo = B.DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And B.`Status` = 'A' ");
                SQL.AppendLine("        And B.CancelInd = 'N' ");
                SQL.AppendLine("        And B.ProcessInd Not In ('M', 'F') ");
                SQL.AppendLine("    Inner Join TblSalesMemoDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("        And C.ProcessInd <> 'F' ");
                SQL.AppendLine("        And C.ProcessInd3 <> 'F' ");
                SQL.AppendLine("    Inner Join TblItem D On C.ItCode = D.ItCode ");
                SQL.AppendLine("    Inner Join TblCustomer E On B.CtCode = E.CtCode ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select T1.SCDocNo, T1.SMDNo, Sum(T1.Qty) ProductionOrderQty ");
                SQL.AppendLine("        From TblProductionOrderHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesContract T2 On T1.SCDocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.CancelInd = 'N' ");
                SQL.AppendLine("            And T1.DocType = '3' ");
                SQL.AppendLine("        Inner Join TblSalesMemoDtl T3 On T2.SalesMemoDocNo = T3.DocNo And T1.SMDNo = T3.DNo ");
                SQL.AppendLine("            And T3.ProcessInd3 <> 'F' And T3.ProcessInd <> 'F' ");
                SQL.AppendLine("        Group By T1.SCDocNo, T1.SMDNo ");
                SQL.AppendLine("    ) F On A.DocNo = F.SCDocNo And C.DNo = F.SMDNo ");
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select T2.SCDocNo, T2.SODNo, Sum(T2.Qty) DRQty ");
                SQL.AppendLine("        From TblDRHdr T1 ");
                SQL.AppendLine("        Inner Join TblDRDtl T2 On T1.DocNo And T2.DocNo ");
                SQL.AppendLine("            And T1.CancelInd = 'N' ");
                SQL.AppendLine("            And T2.SCDocNo Is Not Null ");
                SQL.AppendLine("        Inner Join TblSalesContract T3 On T2.SCDocNo = T3.DocNo ");
                SQL.AppendLine("        Inner Join TblSalesMemoHdr T4 On T3.SalesMemoDocNo = T4.DocNo ");
                SQL.AppendLine("        Inner Join TblSalesMemoDtl T5 On T4.DocNo = T5.DocNo And T2.SODNo = T5.DNo ");
                SQL.AppendLine("            And T5.ProcessInd <> 'F' And T5.ProcessInd3 <> '3' ");
                SQL.AppendLine("        Group By T2.SCDocNo, T2.SODNo ");
                SQL.AppendLine("    ) G On A.DocNo = G.SCDocNo And C.DNo = G.SODNo ");
                SQL.AppendLine("    Left Join TblCustomerItem H On B.CtCode = H.CtCode And C.ItCode = H.ItCode ");
                SQL.AppendLine("    Where C.Qty - IfNull(F.ProductionOrderQty, 0.00) - IfNull(G.DRQty, 0.00) > 0.00 ");
            }
            
            SQL.AppendLine(" ) T ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.ItName, T.CodeInd, T.DocDt;",
                        new string[] 
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DNo", 
                            "DocDt",
                            "CtName", 
                            "AgtName",
                            "ItCode",

                            //6-10
                            "ItName",
                            "ItCodeInternal",
                            "CtItCode", 
                            "Qty",
                            "PlanningUomCode",

                            //11-15
                            "Qty2",
                            "PlanningUomCode2",
                            "DeliveryDt",
                            "CodeInd",
                            "Remark",

                            //16-17
                            "PlanningUomCodeConvert12",
                            "Specification"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtSODocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.mSODNo = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.TxtItName.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.TxtItCodeInternal.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                mFrmParent.TxtSpecification.EditValue = Sm.GetGrdStr(Grd1, Row, 20);
                mFrmParent.TxtCtItCode.EditValue = Sm.GetGrdStr(Grd1, Row, 11);
                mFrmParent.TxtQty.EditValue = Sm.FormatNum(0m, 0);
                mFrmParent.TxtPlanningUomCode.EditValue = Sm.GetGrdStr(Grd1, Row, 13);
                mFrmParent.TxtQty2.EditValue = Sm.FormatNum(0m, 0);
                mFrmParent.TxtPlanningUomCode2.EditValue = Sm.GetGrdStr(Grd1, Row, 15);
                mFrmParent.mDocType = Sm.GetGrdStr(Grd1, Row, 17);
                mFrmParent.mPlanningUomCodeConvert12 = Sm.GetGrdDec(Grd1, Row, 19);
                mFrmParent.TxtProductionRoutingDocNo.EditValue = null;
                mFrmParent.ClearGrd();
                if (mFrmParent.mIsProductionOrderUseAutoRoutingItem) mFrmParent.ShowRoutingItem(Sm.GetGrdStr(Grd1, Row, 7));
                mFrmParent.TxtQty.Focus();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 17) == "1")
                {
                    var f = new FrmSO2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        Sm.GetGrdStr(Grd1, e.RowIndex, 1));

                    var f = new FrmMakeToStock(mFrmParent.mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockStandard;
                            f.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 17) == "1")
                {
                    var f = new FrmSO2(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        Sm.GetGrdStr(Grd1, e.RowIndex, 1));

                    var f = new FrmMakeToStock(mFrmParent.mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockStandard;
                            f.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
