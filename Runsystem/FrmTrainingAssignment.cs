﻿#region Update
/*
    29/03/2018 [HAR] departmetn dan positiion dihapus, tamabh inputan site, hanya TR internal yang bisa diambil, training date ambil dari schedule
    20/07/2018 [HAR] employee filter by site dan filte by filter header
    04/10/2018 [HAR] default training date kosong, data training date ambil dari data training schdule dimana date lbh dari sama dengan document date
    07/11/2018 [HAR] emplolyee yg muncul yg training requestnya blm di assigmnetkan
 *  03/12/2018 [HAR] tmbh validasi loop cek ada employee code tidak
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmTrainingAssignment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal bool mIsFilterByDeptHR = false, mIsFilterBySiteHR = false;
        iGCell fCell;
        bool fAccept;
        internal FrmTrainingAssignmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmTrainingAssignment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Training Assignment";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                LueTrainingSchDt.Visible = false;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "", 
                    "Employee Code",
                    "",
                    "Employee Name",
                    "TrainingCode",

                    //6-10
                    "Training",
                    "Quantity",
                    "TrainingDt",
                    "Training Date",
                    "Training Request"+Environment.NewLine+"Document",
                    //11
                    "Remark"
                },
                new int[] 
                { 
                    0, 
                    20, 100, 20, 250, 0, 
                    250, 80, 20, 200, 150, 
                    250
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 5, 8 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, MeeRemark, LueSiteCode, LueTrainingSchDt
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark,  LueSiteCode, LueTrainingSchDt
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 9, 10, 11 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    if (!ChkCancelInd.Checked)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            MeeRemark, LueTrainingSchDt
                        }, false);
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 9, 11 });
                    }
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, MeeRemark, LueSiteCode, LueTrainingSchDt
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTrainingAssignmentFind(this);
                Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR?"Y":"N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TrainingAssignment", "TblTrainingAssignmentHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTrainingAssignmentHdr(DocNo));

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    cml.Add(SaveTrainingAssignmentDtl(DocNo, r));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsEmployeeExistsInOtherDocument("I") ||
                IsGrdExceedMaxRecords()||
                IsTrainingScheduleNotValid()
                ;
        }

        private bool IsEmployeeExistsInOtherDocument(string ActionType)
        {
            var SQL = new StringBuilder();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    SQL.AppendLine("Select A.DocNo From TblTrainingAssignmentHdr A ");
                    SQL.AppendLine("Inner Join TblTrainingAssignmentDtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
                    SQL.AppendLine("Where A.CancelInd='N' And B.TrainingSchDt=@TrainingSchDt ");
                    if (ActionType == "E")
                    {
                        SQL.AppendLine("And A.DocNo != @DocNo ");
                    }
                    SQL.AppendLine("Limit 1; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    if (ActionType == "E") Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@TrainingSchDt", Sm.GetGrdStr(Grd1, Row, 8));
                    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
                    if (Sm.IsDataExist(cm))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Employee Code     : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Employee Name    : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Training Schedule : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                            "Duplicate Training Assignment on " + Sm.GetGrdStr(Grd1, Row, 6) + "."
                            );
                        Sm.FocusGrd(Grd1, Row, 4);
                        return true;
                    }
                }
            }
            
            return false;
        }

        private bool IsTrainingScheduleNotValid()
        {
            var SQL = new StringBuilder();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    SQL.AppendLine("Select DocNo From tblTrainingSchDtl ");
                    SQL.AppendLine("Where SchInd = 'Y' And TrainingCode=@TrainingCode And Dt=@TrainingDt ");
                    SQL.AppendLine("Limit 1; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd1, Row, 5));
                    Sm.CmParam<String>(ref cm, "@TrainingDt", Sm.GetGrdStr(Grd1, Row, 8));

                    if (!Sm.IsDataExist(cm))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Employee Name  : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                            "Training Name  : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                            "Training date Schedule not found."
                            );
                        Sm.FocusGrd(Grd1, Row, 4);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveTrainingAssignmentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTrainingAssignmentHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @CancelReason, @CancelInd, @SiteCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTrainingAssignmentDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTrainingAssignmentDtl ");
            SQL.AppendLine("(DocNo, DNo, EmpCode, TrainingCode, Qty, TrainingSchDt, TrainingRequestDocNo,  Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @EmpCode, @TrainingCode, @Qty, @TrainingSchDt, @TrainingRequestDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@TrainingSchDt", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@TrainingRequestDocNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateTrainingAssignment());

            if (!ChkCancelInd.Checked)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        cml.Add(SaveTrainingAssignmentDtl(TxtDocNo.Text, r));
                    }
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsEmployeeExistsInOtherDocument("E");
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblTrainingAssignmentHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand UpdateTrainingAssignment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTrainingAssignmentHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, Remark = @Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            if (!ChkCancelInd.Checked)
            {
                SQL.AppendLine("Delete From TblTrainingAssignmentDtl ");
                SQL.AppendLine("Where DocNo = @DocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowTrainingAssignmentHdr(DocNo);
                ShowTrainingAssignmentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowTrainingAssignmentHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT DocNo, DocDt, CancelReason, CancelInd, SiteCode, Remark ");
            SQL.AppendLine("FROM TblTrainingAssignmentHdr ");
            SQL.AppendLine("WHERE DocNo=@DocNo; ");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "SiteCode",  "Remark",
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                     Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), string.Empty);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                 }, true
             );
        }

        private void ShowTrainingAssignmentDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, A.TrainingCode, C.TrainingName, A.Qty, A.TrainingSchDt, A.Remark, ");
            SQL.AppendLine("DATE_FORMAT(A.TrainingSchDt, '%a, %d-%b-%Y') As TrainingSchDtDesc, A.TrainingRequestDocNo ");
            SQL.AppendLine("From TblTrainingAssignmentDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Left Join TblTraining C On A.TrainingCode = C.TrainingCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;  ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "TrainingCode", "TrainingName", "Qty", 
                    
                    //6-9
                    "TrainingSchDt", "Remark", "TrainingSchDtDesc", "TrainingRequestDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Methods

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmTrainingAssignmentDlg(this, Sm.GetLue(LueSiteCode)));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueTrainingSchDt, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                SetLueTrainingSchDt(ref LueTrainingSchDt, Sm.GetGrdStr(Grd1, e.RowIndex, 5));
            }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
                Sm.FormShowDialog(new FrmTrainingAssignmentDlg(this, Sm.GetLue(LueSiteCode)));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Additional Methods

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##" + Sm.GetGrdStr(Grd1, Row, 5) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##XXX##" : SQL);
        }

        private void GetParameter()
        {
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR =  Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private void SetLueTrainingSchDt(ref DXE.LookUpEdit Lue, string TrainingCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct B.Dt As Col1, DATE_FORMAT(B.Dt, '%a, %d-%b-%Y') As Col2 ");
            SQL.AppendLine("From TblTrainingSchHdr A ");
            SQL.AppendLine("Inner Join TblTrainingSchDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.Status = 'A' ");
            SQL.AppendLine("And B.SchInd = 'Y' ");
            SQL.AppendLine("And B.TrainingCode = @TrainingCode ");
            SQL.AppendLine("And B.Dt >= @DocDt ");
            SQL.AppendLine("Order By B.Dt; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@TrainingCode", TrainingCode);
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt).Substring(0, 8));
            
            Sm.SetLue2(
                ref Lue, ref cm,
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        //internal void SetLueSiteCode(ref LookUpEdit Lue)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
        //    SQL.AppendLine("From TblSite T ");
        //    SQL.AppendLine("Where T.ActInd = 'Y' ");

        //    if (mIsFilterBySiteHR)
        //    {
        //        SQL.AppendLine("And T.SiteCode Is Not Null ");
        //        SQL.AppendLine("And Exists( ");
        //        SQL.AppendLine("    Select SiteCode From TblGroupSite ");
        //        SQL.AppendLine("    Where SiteCode=IfNull(T.SiteCode, '') ");
        //        SQL.AppendLine("    And GrpCode In ( ");
        //        SQL.AppendLine("        Select GrpCode From TblUser ");
        //        SQL.AppendLine("        Where UserCode= @UserCode ");
        //        SQL.AppendLine("    ) ");
        //        SQL.AppendLine(") ");
        //    }

        //    SQL.AppendLine("Order By T.SiteName; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    Sm.SetLue2(
        //     ref Lue, ref cm,
        //     0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
            Sm.ClearGrd(Grd1, true);
        }

        private void LueTrainingSchDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //for(int r = 0; r < (Grd1.Rows.Count - 1); r++)
                //{
                Sm.RefreshLookUpEdit(LueTrainingSchDt, new Sm.RefreshLue2(SetLueTrainingSchDt), Sm.GetGrdStr(Grd1, fCell.RowIndex, 5));
                //}
            }
        }

        private void LueTrainingSchDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTrainingSchDt_Leave(object sender, EventArgs e)
        {
            if (LueTrainingSchDt.Visible && fAccept && fCell.ColIndex == 9)
            {
                if (Sm.GetLue(LueTrainingSchDt).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 8].Value =
                    Grd1.Cells[fCell.RowIndex, 9].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 8].Value = Sm.GetLue(LueTrainingSchDt);
                    Grd1.Cells[fCell.RowIndex, 9].Value = LueTrainingSchDt.GetColumnValue("Col2");
                }
                LueTrainingSchDt.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd1, (fCell.RowIndex + 1), new int[] { 7 });
            }
        }

        #endregion

        #endregion
    }
}
