﻿#region Update
/*
    02/05/2019 [WED/MAI] new apps
    10/10/2019 [WED/MAI] dibuat berdasarkan bulan dan tahun
    24/10/2019 [WED/MAI] set lue yr dan mth masih error saat ShowData. DNo 8 digit
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmKBOpeningBalance : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty;

        private string mDocDt = string.Empty;

        internal FrmKBOpeningBalanceFind FrmFind;

        #endregion

        #region Constructor

        public FrmKBOpeningBalance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmKBOpeningBalance");
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Item Code",
                    "Item Name",
                    "Item Group Code",
                    "Item Group Name",

                    //6
                    "Opening Balance"+Environment.NewLine+"Quantity"
                },
                new int[] { 50, 20, 100, 280, 150, 200, 120 }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, MeeCancelReason, MeeRemark, LueMth, LueYr
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeRemark, LueMth, LueYr
                    }, false);
                    string CurrentDateTime = Sm.ServerCurrentDateTime();
                    Sl.SetLueYr(LueYr, string.Empty);
                    Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                    Sl.SetLueMth(LueMth);
                    Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                    mDocDt = Sm.Left(CurrentDateTime, 8);
                    Grd1.ReadOnly = false;
                    LueMth.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mDocDt = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, MeeCancelReason, MeeRemark, LueMth, LueYr
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmKBOpeningBalanceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(mDocDt, "KBOpeningBalance", "TblKBOpeningBalanceHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveKBOpeningBalanceHdr(DocNo));

            for (int i = 0; i < Grd1.Rows.Count; i++)
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    cml.Add(SaveKBOpeningBalanceDtl(DocNo, i));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsDataExists();
        }

        private bool IsDataExists()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblKBOpeningBalanceHdr ");
            SQL.AppendLine("Where Mth = @Param1 ");
            SQL.AppendLine("And Yr = @Param2 ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueMth), Sm.GetLue(LueYr), string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Opening Balance has been created in this document #" + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueMth), Sm.GetLue(LueYr), string.Empty));
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if(Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, i, 2, false, "Item is empty")) return true;
                }
            }

            return false;
        }

        private MySqlCommand SaveKBOpeningBalanceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKBOpeningBalanceHdr ");
            SQL.AppendLine("(DocNo, DocDt, Mth, Yr, CancelInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @Mth, @Yr, 'N', @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", mDocDt);
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveKBOpeningBalanceDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblKBOpeningBalanceDtl(DocNo, DNo, ItCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @Qty, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000000" + (Row + 1).ToString(), 8));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelKBOpeningBalance());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblKBOpeningBalanceHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelKBOpeningBalance()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblKBOpeningBalanceHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowKBOpeningBalanceHdr(DocNo);
                ShowKBOpeningBalanceDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowKBOpeningBalanceHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DocDt, Mth, Yr, CancelInd, CancelReason, Remark ");
            SQL.AppendLine("From TblKBOpeningBalanceHdr ");
            SQL.AppendLine("Where DocNo = @DocNo; ");
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    //1-5
                    "Mth", "Yr", "CancelReason", "CancelInd", "Remark"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sl.SetLueYr(LueYr, string.Empty);
                     Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                     Sl.SetLueMth(LueMth);
                     Sm.SetLue(LueMth, Sm.DrStr(dr, c[1]));
                     //Sm.SetLue(LueMth, Sm.DrStr(dr, c[1]));
                     //Sm.SetLue(LueYr, Sm.DrStr(dr, c[2]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                     MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                 }, true
             );
        }

        private void ShowKBOpeningBalanceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, B.ItGrpCode, C.ItGrpName, A.Qty ");
            SQL.AppendLine("From TblKBOpeningBalanceDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "ItCode",
                    "ItName","ItGrpCode", "ItGrpName", "Qty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Grid Methods

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmKBOpeningBalanceDlg(this));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                Sm.FormShowDialog(new FrmKBOpeningBalanceDlg(this));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Additional Methods

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion

    }
}
