﻿#region Update
/*
    08/07/2021 [IBL/PHT] new dialog apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestMulti3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestMulti3 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestMulti3Dlg(FrmVoucherRequestMulti3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BCCode, A.LocalCode, A.BCName, B.DeptName, D.CCName, C.OptDesc As BudgetType, E.AcNo, E.AcDesc ");
            SQL.AppendLine("From TblBudgetCategory A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblOption C On A.BudgetType=C.OptCode And C.OptCat='BudgetType' ");
            if (TxtCCCode.Text.Length > 0)
                SQL.AppendLine("Inner ");
            else
                SQL.AppendLine("Left ");
            SQL.AppendLine("Join TblCostCenter D On A.CCCode=D.CCCode ");
            SQL.AppendLine("Left Join TblCOA E On A.AcNo=E.AcNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Local Code", 
                        "Name",
                        "Department",
                        "Cost center",
                        
                        //6-9
                        "Budget Type",
                        "Account#",
                        "Account Description",
                      },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 100, 200, 200, 200,
                        
                        //6-9
                        200, 150, 200
                    }
                );
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtBCName.Text, new string[] { "A.BCCode", "A.BCName" });
                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, new string[] { "D.CCCode", "D.CCName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.DeptName, A.BCName;",
                        new string[] 
                        { 
                             //0
                            "BCCode",

                            //1-5
                            "LocalCode",
                            "BCName",
                            "DeptName",
                            "CCName",
                            "BudgetType",
                            
                            //6-7
                            "AcNo",
                            "AcDesc",
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.TxtBCCode.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtBCName.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                this.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBCName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBCName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget category");
        }

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost Center");
        }

        #endregion

        #region Grid Control Event

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

    }
}
