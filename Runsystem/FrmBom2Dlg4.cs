﻿#region Update
/*
    19/09/2019 [TKG/IMS] tambah informasi dimensions
    31/10/2019 [DITA/IMS] tambah informasi ItCodeInternal dan Specifications
    28/11/2019 [DITA/IMS] tambah kolom project code, project name, ntp docno, customer po no
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBom2Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBom2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBom2Dlg4(FrmBom2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "BOM#", 
                        "Date",
                        "Document Name", 
                        "Active",
                        "Item's Code",

                        //6-10
                        "",
                        "Item's Name",
                        "Type",
                        "Document#",
                        "",

                        //11-15
                        "Description",
                        "Source",
                        "Quantity",
                        "Value",
                        "Dimensions",

                        //16-20
                        "Item's Code"+Environment.NewLine+"Internal",
                        "Specification",
                        "Project Code",
                        "Project Name",
                        "Customer PO#",

                        //21
                        "Notice To Proceed#"

                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColButton(Grd1, new int[] { 6, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,21 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 16 }, false);
            if (!mFrmParent.mIsBOMShowDimensions) Sm.GrdColInvisible(Grd1, new int[] { 15 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 17 }, false);
            Sm.SetGrdProperty(Grd1, true);
            Grd1.Cols[16].Move(7);
        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 16 }, !ChkHideInfoInGrd.Checked);
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DocNo, A.DocName, A.DocDt, A.ActiveInd, B2.ItCode, C.ItName, B.DocType, B.DocCode, D.OptDesc, B.Qty, B.Value, ");
            SQL.AppendLine("Case B.DocType When '3' Then E.EmpName When '2' Then F.DocName When '1' Then G.ItName End As DocDesc, C.ItCodeInternal, C.Specification, ");
            SQL.AppendLine(" H.ProjectCode, H.ProjectName, H.DocNo As NTPDocNo, H.PONo, ");
            if (mFrmParent.mIsBOMShowDimensions)
            {
                SQL.AppendLine("Case When B.DocType='1' Then ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When IfNull(G.Length, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Length : ', Trim(Concat(Convert(Format(IfNull(G.Length, 0.00), 2) Using utf8), ' ', IfNull(G.LengthUomCode, '')))) ");
                SQL.AppendLine("    Else '' End, ' ', ");
                SQL.AppendLine("Case When IfNull(G.Height, 0.00)<>0.00 Then ");
                SQL.AppendLine("    Concat('Height : ', Trim(Concat(Convert(Format(IfNull(G.Height, 0.00), 2) Using utf8), ' ', IfNull(G.HeightUomCode, '')))) ");
                SQL.AppendLine("Else '' End, ' ', ");
                SQL.AppendLine("Case When IfNull(G.Width, 0.00)<>0.00 Then ");
                SQL.AppendLine("    Concat('Width : ', Trim(Concat(Convert(Format(IfNull(G.Width, 0.00), 2) Using utf8), ' ', IfNull(G.WidthUomCode, '')))) ");
                SQL.AppendLine("Else '' End ");
                SQL.AppendLine(")) Else Null End As Dimensions ");
            }
            else
                SQL.AppendLine("Null As Dimensions ");
            SQL.AppendLine("From TblBOMHdr A ");
            SQL.AppendLine("Inner Join TblBOMDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblBomDtl2 B2 On A.DocNo =B2.DocNo And B2.ItType = '1' ");
            SQL.AppendLine("Left Join TblItem C On B2.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblOption D On B.DocType=D.OptCode And D.OptCat='BomDocType' ");
            SQL.AppendLine("Left Join TblEmployee E On B.DocCode=E.EmpCode And '3'=B.DocType ");
            SQL.AppendLine("Left Join TblFormulaHdr F On B.DocCode=F.DocNo And '2'=B.DocType ");
            SQL.AppendLine("Left Join TblItem G On B.DocCode=G.ItCode And '1'=B.DocType ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select  X1.DocNo, X2.BOMDocNo, X2.BOMDNo,  ");
            SQL.AppendLine("IfNull(X5.ProjectCode, X6.ProjectCode2) ProjectCode, IfNull(X5.ProjectName, X4.ProjectName) ProjectName, X7.DocNo As NTPDocNo, X6.PONo ");
            SQL.AppendLine("From TblBOMRevisionHdr X1 ");
            SQL.AppendLine("Inner Join TblBOMRevisionDtl X2 On X1.DocNo = X2.DocNo");
            SQL.AppendLine("Inner Join TblBOQHdr X3 On X1.BOQDocNo = X3.DocNo");
            SQL.AppendLine("Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup X5 On X4.PGCode = X5.PGCode");
            SQL.AppendLine("Left Join TblSOContractHdr X6 On X6.BOQDocNo = X3.DocNo");
            SQL.AppendLine("Left Join TblNoticeToProceed X7 On X4.DocNo = X7.LOPDocNo ");
            SQL.AppendLine(")H On H.BOMDocNo = A.DocNo And H.BOMDNo = B.DNo ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtBomDocNo.Text, new string[] { "A.DocNo", "A.DocName" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItem.Text, new string[] { "B2.ItCode", "C.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt Desc, A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "DocName", "ActiveInd", "ItCode", "ItName", 
                            
                            //6-10
                            "DocType", "DocCode",  "DocDesc", "OptDesc", "Qty", 
                            
                            //11-15
                            "Value", "Dimensions", "ItCodeInternal", "Specification", "ProjectCode",

                            //16-18
                            "ProjectName", "PONo", "NtpDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        }, true, true, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                mFrmParent.TxtSource.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ShowData2(mFrmParent.TxtSource.Text);
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "1"))
                {
                    var f1 = new FrmItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "3"))
                {
                    var f3 = new FrmEmployee(mFrmParent.mMenuCode);
                    f3.Tag = mFrmParent.mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f3.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "1"))
                {
                    var f1 = new FrmItem(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "2"))
                {
                    var f2 = new FrmFormula(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 8), "3"))
                {
                    var f3 = new FrmEmployee(mFrmParent.mMenuCode);
                    f3.Tag = mFrmParent.mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f3.ShowDialog();
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event
        private void ChkBomDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void TxtBomDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkItem_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItem_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
