﻿#region Update
/*
    127/03/2023 [BRI/PHT] bug frmload
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingAPPaidItemDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptAgingAPPaidItem mFrmParent;
        private int mRow;
        private string mType = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAgingAPPaidItemDlg(FrmRptAgingAPPaidItem FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                TxtDocNo.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 8);
                mType = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 9);
                TxtType.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 5);
                TxtVdInvNo.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 12);
                TxtVdCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 7);
                TxtCurCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, mRow, 6);
                TxtInvoiceAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(mFrmParent.Grd1, mRow, 15), 0);
                TxtDownpayment.EditValue = Sm.FormatNum(Sm.GetGrdDec(mFrmParent.Grd1, mRow, 16), 0);
                TxtPaidAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(mFrmParent.Grd1, mRow, 17), 0);
                TxtBalance.EditValue = Sm.FormatNum(Sm.GetGrdDec(mFrmParent.Grd1, mRow, 18), 0);

                SetGrd();

                if (mType == "1") ShowData1();
                ShowData2();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.RowHeader.Visible = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "Paid Amount",
                        "AP Settlement#",
                        ""
                    },
                    new int[] 
                    {
                        //0
                        40,

                        //1-3
                        130, 130, 20
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2 });
        
            Grd2.Cols.Count = 8;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd2, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Paid Amount",
                        "Outgoing Payment#",
                        "",
                        "Voucher Request#",
                        "",

                        //6-7
                        "Voucher#",
                        ""
                    },
                    new int[] 
                    {
                        //0
                        40,

                        //1-5
                        130, 130, 20, 130, 20,

                        //6-7
                        130, 20
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 3, 5, 7 });
            Sm.GrdFormatDec(Grd2, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(true, false, Grd2, new int[] { 0, 1, 2, 4, 6 });
        }

        private void ShowData1()
        {
            Cursor.Current = Cursors.WaitCursor;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, Amt From TblAPSHdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And PurchaseInvoiceDocNo=@DocNo ");
            SQL.AppendLine("Order By DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[]{ "Amt", "DocNo" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, false, false
                );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowData2()
        {
            Cursor.Current = Cursors.WaitCursor;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Amt, A.DocNo, A.VoucherRequestDocNo, B.VoucherDocNo ");
            SQL.AppendLine("From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Left Join TblVoucherHdr C On B.VoucherDocNo=C.DocNo And C.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl D On A.DocNo=D.DocNo And D.InvoiceDocNo=@DocNo And D.InvoiceType=@Type ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status<>'N' ");
            SQL.AppendLine("Order By A.DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Type", mType);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[] { "Amt", "DocNo", "VoucherRequestDocNo", "VoucherDocNo" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    }, false, false, false, false
                );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #region Grid Method

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAPS(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAPS(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Button Method

        private void BtnInvoice_Click(object sender, EventArgs e)
        {
            if (mType == "1")
            {
                var f1 = new FrmPurchaseInvoice("XXX");
                f1.Tag = "XXX";
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtDocNo.Text;
                f1.Text = "Purchase Invoice";
                f1.ShowDialog();
            }
            else
            {
                var f2 = new FrmPurchaseReturnInvoice("XXX");
                f2.Tag = "XXX";
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.mDocNo = TxtDocNo.Text;
                f2.Text = "Purchase Return Invoice";
                f2.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Grid Event
        
        private void Grd2_AfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmOutgoingPayment(mFrmParent.mMenuCode);
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f1.ShowDialog();
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd2, e.RowIndex, 4).Length != 0)
            {
                var f2 = new FrmVoucherRequest(mFrmParent.mMenuCode);
                f2.Tag = mFrmParent.mMenuCode;
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 4);
                f2.ShowDialog();
            }
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd2, e.RowIndex, 6).Length != 0)
            {
                var f3 = new FrmVoucher(mFrmParent.mMenuCode);
                f3.Tag = mFrmParent.mMenuCode;
                f3.WindowState = FormWindowState.Normal;
                f3.StartPosition = FormStartPosition.CenterScreen;
                f3.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 6);
                f3.ShowDialog();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmOutgoingPayment(mFrmParent.mMenuCode);
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f1.ShowDialog();
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd2, e.RowIndex, 4).Length != 0)
            {
                var f2 = new FrmVoucherRequest(mFrmParent.mMenuCode);
                f2.Tag = mFrmParent.mMenuCode;
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 4);
                f2.ShowDialog();
            }
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd2, e.RowIndex, 6).Length != 0)
            {
                var f3 = new FrmVoucher(mFrmParent.mMenuCode);
                f3.Tag = mFrmParent.mMenuCode;
                f3.WindowState = FormWindowState.Normal;
                f3.StartPosition = FormStartPosition.CenterScreen;
                f3.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 6);
                f3.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
