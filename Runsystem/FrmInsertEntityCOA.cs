﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInsertEntityCOA : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmInsertEntityCOA(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Insert Entity for COA";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                BtnFind.Visible = BtnEdit.Visible = BtnPrint.Visible = false;
                Sl.SetLueEntCode(ref LueEntCode);
                SetGrd();
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 2;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "COA#", 

                    //1
                    "COA Description"
                },
                new int[] { 150, 250 }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
        }

        override protected void HideInfoInGrd()
        {

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueEntCode, TxtCOAFormulationDocNo
                    }, true);

                    BtnCOAFormulationDocNo.Enabled = false;
                    Grd1.ReadOnly = true;
                    LueEntCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueEntCode
                    }, false);
                    BtnCOAFormulationDocNo.Enabled = true;
                    Grd1.ReadOnly = false;
                    LueEntCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueEntCode, TxtCOAFormulationDocNo
            });
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    cml.Add(InsertEntityCOA(Sm.GetGrdStr(Grd1, Row, 0)));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueEntCode, "Entity") ||
                Sm.IsTxtEmpty(TxtCOAFormulationDocNo, "COA Formulation#", false) ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 COA Number.");
                BtnCOAFormulationDocNo.Focus();
                return true;
            }
            return false;
        }

        private MySqlCommand InsertEntityCOA(string AcNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCOADtl ");
            SQL.AppendLine("(AcNo, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@AcNo, @EntCode, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("  On Duplicate Key ");
            SQL.AppendLine("  Update ");
            SQL.AppendLine("  LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Additional Method

        internal void GetCOA(string COAFormulationDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string CountDocNo = Sm.GetValue("Select IfNull(Count(DocNo), 0) CountDocNo From TblCOAFormulationDtl where DocNo = '" + COAFormulationDocNo + "' Group By DocNo;");
            var lCOA = new List<COA>();

            for (int x = 0; x < Int32.Parse(CountDocNo); x++)
            {
                string AcNo = Sm.GetValue("Select AcNo From TblCOAFormulationDtl Where DocNo = '" + COAFormulationDocNo + "' And DNo = '00" + (x+1) + "';");

                SQL.AppendLine("Select T1.AcNo, T2.AcDesc From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select AcNo ");
                SQL.AppendLine("    from TblCOA ");
                SQL.AppendLine("    Where AcNo Like '" + AcNo + "%' ");
                SQL.AppendLine(")T1 ");
                SQL.AppendLine("Inner Join TblCOA T2 On T1.AcNo = T2.AcNo ");
                if(x < (Int32.Parse(CountDocNo) - 1))
                    SQL.AppendLine("Union All ");
            }

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                "Select T.AcNo, T.AcDesc From (" + SQL.ToString() + ")T Order By T.AcNo;",
                new string[]
                {
                    //0
                    "AcNo",
                    
                    //1
                    "AcDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                }, true, false, false, false
            );

            lCOA.Clear();
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        #endregion

        #region Button Events

        private void BtnCOAFormulationDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmInsertEntityCOADlg(this));
        }

        #endregion

        #endregion

        #region Class

        class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
        }

        #endregion

    }
}
