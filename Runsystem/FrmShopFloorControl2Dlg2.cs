﻿#region Update
/*
    09/07/2019 [WED] tambah fitur Asset Short Code
    09/01/2020 [WED/MMM] panggil function untuk autogenerate batchno
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControl2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmShopFloorControl2 mFrmParent;
        string mPPDocNo = string.Empty, mWorkCenterDocNo = string.Empty, mAssetShortCode = string.Empty;

        #endregion

        #region Constructor

        public FrmShopFloorControl2Dlg2(
            FrmShopFloorControl2 FrmParent,
            string PPDocNo,
            string WorkCenterDocNo,
            string AssetShortCode
            )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPPDocNo = PPDocNo;
            mWorkCenterDocNo = WorkCenterDocNo;
            mAssetShortCode = AssetShortCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode);

                ShowData(GetSubQuery());
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSubQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(" And A.ItCode In ( ");
            SQL.AppendLine("    Select Distinct G.ItCode ");
            SQL.AppendLine("    From TblPPHdr A ");
            SQL.AppendLine("    Inner Join TblPPDtl B On A.DocNo=B.DocNo And B.DocType='1' ");
            SQL.AppendLine("    Inner Join TblProductionOrderHdr C On B.ProductionOrderDocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblProductionOrderDtl D On C.DocNo=D.DocNo ");
            SQL.AppendLine("    Inner Join TblProductionRoutingDtl E ");
            SQL.AppendLine("        On C.ProductionRoutingDocNo=E.DocNo ");
            SQL.AppendLine("        And D.ProductionRoutingDNo=E.DNo ");
            SQL.AppendLine("        And E.WorkCenterDocNo=@WorkCenterDocNo ");
            if (!mFrmParent.mIsCodeForSFCStandard)
            {
                SQL.AppendLine("    Inner Join TblBOMHdr F On D.BOMDocNo=F.DocNo And F.ActiveInd='Y' ");
            }
            else
            {
                SQL.AppendLine("    Inner Join TblWorkcenterBOMFormulation E2 On E.WorkcenterDocno = E2.WorkcenterCode ");
                SQL.AppendLine("    Inner Join TblBOMHdr F On E2.BOMFormulationCode=F.DocNo And F.ActiveInd='Y'  ");
            }
            SQL.AppendLine("    Inner Join TblBOMDtl2 G On F.DocNo=G.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@PPDocNo ");
            SQL.AppendLine("    ) ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item Code", 
                        "", 
                        "Item Name", 
                        "Item's Category",

                        //6-8
                        "UoM",
                        "Uom 2",
                        "Wasted"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 8 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 7 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            ShowData("");
        }

        private void ShowData(string SubQuery)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;


                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                if (SubQuery.Length>0)
                {
                    Sm.CmParam<String>(ref cm, "@PPDocNo", mPPDocNo);
                    Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", mWorkCenterDocNo);
                }

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select A.ItCode, A.ItName, B.ItCtName, A.PlanningUomCode, A.PlanningUomCode2, B.WastedInd " +
                        "From TblItem A, TblItemCategory B " +
                        "Where A.ItCtCode=B.ItCtCode " +
                        "And A.ActInd='Y' " +
                        "And A.PlanningItemInd='Y' " + SubQuery +
                        Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode", 

                            //1-5
                            "ItName", "ItCtName", "PlanningUomCode", "PlanningUomCode2", "WastedInd" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 5);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        if (mFrmParent.mSFCDefaultBatchNo.Length != 0)
                        {
                            if (mFrmParent.mIsAssetShortCodeAddedToSFCResultBatchNo)
                                mFrmParent.Grd1.Cells[Row1, 5].Value = string.Concat(mFrmParent.mBatchNo, mAssetShortCode);
                            else
                                mFrmParent.Grd1.Cells[Row1, 5].Value = mFrmParent.mBatchNo;
                        }
                        else
                        {
                            if (mFrmParent.mIsAssetShortCodeAddedToSFCResultBatchNo)
                                mFrmParent.Grd1.Cells[Row1, 5].Value = mAssetShortCode;
                        }
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 8);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 6, 8, 10 });

                        if (mFrmParent.mIsSFCUseOptionBatchNoFormula) mFrmParent.GenerateBatchNo(Row1);
                        
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 6, 8, 10 });
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 11 });
                    }
                }
                mFrmParent.SetDNo(mFrmParent.Grd1);
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
