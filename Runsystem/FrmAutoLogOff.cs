﻿#region Update
/*
    05/09/2022 [WED/ALL] sesuai parameter : IsPasswordForLoginNeedToEncode dapat memilih login berdasarkan password yg di encode atau tidak
 */
#endregion

#region Namespace
using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
#endregion

namespace RunSystem
{
    public partial class FrmAutoLogOff : Form
    {
        #region Field

        private FrmMain mFrmParent;
        private bool ApplicationExitInd = true;
        private bool mIsPasswordForLoginNeedToEncode = false;

        #endregion

        #region Constructor

        public FrmAutoLogOff(FrmMain FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            TxtUserCode.Text = Gv.CurrentUserCode;
            mIsPasswordForLoginNeedToEncode = Sm.GetParameterBoo("IsPasswordForLoginNeedToEncode");
            TxtPwd.Focus();
        }

        #endregion

        #region Method

        private bool IsPwdValid()
        {
            bool Valid = false;
            var cm = new MySqlCommand()
            {
                CommandText = "Select UserCode From TblUser Where UserCode=@UserCode And Pwd=@Pwd;"
            };

            string mPwd = TxtPwd.Text;
            if (mIsPasswordForLoginNeedToEncode) mPwd = Sm.GetHashBase64(Sm.GetHashSha256(TxtPwd.Text));

            Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);
            Sm.CmParam<String>(ref cm, "@Pwd", mPwd);
            Valid = Sm.IsDataExist(cm);
            if (!Valid) Sm.StdMsg(mMsgType.Warning, "Wrong password.");
            return Valid;
        }

        #endregion

        #region Event

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPwd, "Password", false)) return;

            if (IsPwdValid())
            {
                ApplicationExitInd = false;
                mFrmParent.StartTimerAutoLogOff();
                this.Close();
            }
        }

        private void TxtPwd_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPwd);
        }

        private void FrmAutoLogOff_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ApplicationExitInd && Sm.StdMsgYN("Question", "Do you want to close application ?") == DialogResult.No) 
                e.Cancel = true;
        }

        private void FrmAutoLogOff_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (ApplicationExitInd) Application.Exit();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion
    }
}
