﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion



namespace RunSystem
{
    public partial class FrmPosFindProduct : Form
    {
        private int mQuantityPos = 0;
        private FrmTrnPOS mFrmParent;

        public FrmPosFindProduct(FrmTrnPOS FrmParent, int QuantityPos)
        {

            InitializeComponent();
            mFrmParent = FrmParent;
            mQuantityPos = QuantityPos;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 0;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item"+Environment.NewLine+"Code", 
                        "Item"+Environment.NewLine+"Name",
                        "Barcode",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            //Sm.GrdFormatDate(Grd1, new int[] { 5, 8 });
            //Sm.GrdFormatTime(Grd1, new int[] { 6, 9 });
            //Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdProperty(Grd1, true);
        }

        private void TxtProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                this.BtnRefresh_Click(sender, e);
            }

            if (e.KeyChar == 27)
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }

        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (TxtProduct.Text == "")
            {
                MessageBox.Show("Search Item Code is Empty !! ", "POS System");
                TxtProduct.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        FrmTrnPOS.ItemSearchSQL +
                        "Where (T.ItCode like '%" + TxtProduct.Text + "%') Or " +
                        "(Replace(T.ItCode, '-', '') =Right('" + TxtProduct.Text + "', Length(Replace(T.ItCode, '-', '')))) Or " +
                        "(T.ItName like '%" + TxtProduct.Text + "%') " +
                        "Order By ItName;",
                        new string[]
                        {
                            //0
                            "ItCode", 
                                
                            //1-5
                            "ItName",
                            "BarCode",
                            "CurCode",
                            "UPrice",
                            "TaxLiableInd"                         
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        private void FrmPosFindProduct_Load(object sender, EventArgs e)
        {
            SetGrd();
            TxtProduct.Focus();
        }

        private void DoSelected()
        {
            if (Grd1.Rows.Count > 0)
            {
                string QuantityPos = "";
                if (mQuantityPos > 0) QuantityPos = mFrmParent.LblCode.Text;
                mFrmParent.LblCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ProcessData(QuantityPos + Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4));
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void Grd1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) 
                DoSelected();
 
            if (e.KeyChar == 27)
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();

            }
        }

        private void FrmPosFindProduct_Activated(object sender, EventArgs e)
        {
            TxtProduct.Focus();
        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
            DoSelected();
        }


    }
}
