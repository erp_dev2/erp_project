﻿#region Update
/* 
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDODeptLOPFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDODeptLOP mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;

        #endregion

        #region Constructor

        public FrmDODeptLOPFind(FrmDODeptLOP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueAssetCode(ref LueAssetCode);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, L.ProjectName, B.CancelInd, C.WhsName, D.DeptName, A.JournalDocNo, J.CCtName, ");
            SQL.AppendLine("B.ItCode, E.ItCodeInternal, E.ItName, E.ForeignName, B.ReplacementInd, H.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            SQL.AppendLine("F.AssetName, F.DisplayName, G.EmpName, ");
            SQL.AppendLine("A.Remark As RemarkH, B.Remark As RemarkD, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, IfNull(A.LastUpBy, B.LastUpBy) As LastUpBy, IfNull(A.LastUpDt, B.LastUpDt) As LastUpDt, E.ItGrpCode ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblAsset F On B.AssetCode = F.AssetCode ");
            SQL.AppendLine("Left Join TblEmployee G On B.EmpCode = G.EmpCode ");
            SQL.AppendLine("Left Join TblProperty H On B.PropCode = H.PropCode ");
            SQL.AppendLine("Left Join TblItemCostCategory I On B.ItCode = I.ItCode And A.CCCode = I.CCCode ");
            SQL.AppendLine("Left Join TblCostCategory J On I.CCtCode = J.CCtCode And I.CCCode = J.CCCode ");
            SQL.AppendLine("Left Join TblCostCenter K On A.CCCode=K.CCCode ");
            SQL.AppendLine("Inner Join TblLOPhdr L On A.LopDocNo = L.Docno ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (L.SiteCode Is Null Or ( ");
                SQL.AppendLine("    L.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(L.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Where A.LOPDocNo Is Not Null ");
            SQL.AppendLine("And A.WODocNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterByCC)
            {
                SQL.AppendLine("And (A.CCCode Is Null Or (A.CCCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=K.CCCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("))) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 37;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel", 
                        "Project Name",
                        "Warehouse",
                        
                        //6-10
                        "Department",
                        "Item's Code",
                        "Local Code",
                        "Item's Name",
                        "Foreign Name",

                        //11-15
                        "Group",
                        "Replacement",
                        "Property",
                        "Batch#",
                        "Source",
                        
                        //16-20
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //21-25
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Asset",
                        "Display Name",

                        //26-30
                        "Cost Category",
                        "Requested By",
                        "DO Journal#",
                        "Remark",
                        "Remark",
                        
                        //31-35
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",

                        //36
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 50, 130, 150,  
                        
                        //6-10
                        150, 100, 100, 250, 150, 
                        
                        //11-15
                        150, 100, 80, 180, 180, 

                        //16-20
                        60, 60, 80, 80, 80, 

                        //21-25
                        80, 80, 80, 180, 180,  
                        
                        //26-30
                        180, 180, 150, 250, 250,

                        //31-35
                        100, 100, 100, 100, 100,

                        //36
                        100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 20, 22 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 32, 35 });
            Sm.GrdFormatTime(Grd1, new int[] { 33, 36 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 13, 15, 16, 17, 31, 32, 33, 34, 35, 36 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 13, 15, 16, 17, 31, 32, 33, 34, 35, 36 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "  ";

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And B.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "E.ItCodeInternal", "E.ItName", "E.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "B.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "B.Bin", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAssetCode), "B.AssetCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "ProjectName", "WhsName", "DeptName",  
                            
                            //6-10
                            "ItCode", "ItCodeInternal", "ItName", "ForeignName", "ItGrpCode", 
                            
                            //11-15
                            "ReplacementInd", "PropName", "BatchNo", "Source", "Lot", 
                            
                            //16-20
                            "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                            
                            //21-25
                            "Qty3", "InventoryUomCode3", "AssetName", "DisplayName", "CCtName", 
                            
                            //26-30
                            "EmpName", "JournalDocNo", "RemarkH", "RemarkD", "CreateBy", 
                            
                            //31-33
                            "CreateDt", "LastUpBy", "LastUpDt",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 32, 31);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 32);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 35, 33);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 36, 33);
                            
                        }, true, false, false, false
                    );
                if (mIsInventoryShowTotalQty)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 18, 20, 22 });
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void LueAssetCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAssetCode, new Sm.RefreshLue1(Sl.SetLueAssetCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset");
        }
       
        #endregion

        #endregion

    }
}
