﻿#region Update
/*
     * 22/11/2019 [HAR/SRN]  bug  employeecode masih ambil dari joindate (length=8), untuk ims pake contract date (length=9)
     */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmEmpPerformanceAllowance : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mADCodePerformance = string.Empty;
        internal decimal mNoWorkDayPerMth = 0m;
        internal FrmEmpPerformanceAllowanceFind FrmFind;
        internal bool
            mIsEmpContractDtMandatory = false;

        #endregion

        #region Constructor

        public FrmEmpPerformanceAllowance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Performance Allowance";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View); 
                GetParameter();
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Employee's"+Environment.NewLine+"Code",
                        
                        //1-5
                        "Old Code",
                        "Employee's Name",
                        "Department",
                        "Position",
                        "Index (IKP)", 

                        //6
                        "Payrun Code"
                    },
                     new int[] 
                    {
                        //0
                        100, 

                        //1-5
                        100, 200, 200, 180, 100, 
                        //
                        100
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        DteDocDt, LueYr, LueMth
                    }, true);
                    BtnProcess.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        DteDocDt, LueYr, LueMth
                    }, false);
                    BtnProcess.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, }, false);
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> {  DteDocDt, LueYr, LueMth });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmEmpPerformanceAllowanceFind(this);
           Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                 InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0 && Sm.GetGrdStr(Grd1, Row, 6).Length ==  0)
                {
                    var cml = new List<MySqlCommand>();

                    cml.Add(SaveEmpPerformanceAllowance(Row));
                    
                    Sm.ExecCommands(cml);
                }
            }


            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false,
                     "Employee's Name is empty.")) return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpPerformanceAllowance(int r)
        {
            var SQL = new StringBuilder();

            string YrMth = String.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth));
            string DataExist = Sm.GetValue("Select 1 From TblEmpPerformanceAllowance Where Concat(Yr, Mth) = '" + YrMth + "' And EmpCode = '" + Sm.GetGrdStr(Grd1, r, 0) + "' Limit 1 ");

            if (DataExist.Length > 0)
            {
                SQL.AppendLine("Update TblEmpPerformanceAllowance ");
                SQL.AppendLine("SET IKP=@IKP, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where EmpCode=@EmpCode And Yr=@Yr And Mth=@Mth And payrunCode is null ; ");
            }
            else
            {
                SQL.AppendLine("Insert Into TblEmpPerformanceAllowance(DocDt, ADCode, Yr, Mth, EmpCode, IKP, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocDt, @ADCode, @Yr, @Mth, @EmpCode, @IKP, @UserCode, CurrentDateTime()); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ADCode", mADCodePerformance);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<Decimal>(ref cm, "@IKP", Sm.GetGrdDec(Grd1, r, 5));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string Year, string Month)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpPerformanceAllowance(Year, Month);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        private void ShowEmpPerformanceAllowance(string Year, string Month)
        {
            string mdt = string.Empty;
            string myr = string.Empty;
            string mmth = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.Yr, A.mth, A.EMpCode,  ");
            SQL.AppendLine("C.Empname, C.EmpCodeOld, D.DeptName, E.PosName,  ");
            SQL.AppendLine("A.IKP, A.PayrunCode ");
            SQL.AppendLine("From TblEmpPerformanceAllowance A  ");
            SQL.AppendLine("Inner Join Tblemployee C On A.EmpCode = C.empcode ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode = D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode = E.PosCode  ");
            SQL.AppendLine("Where A.Yr=@Yr aND A.mth=@Mth Order By C.EmpName; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Year);
            Sm.CmParam<String>(ref cm, "@Mth", Month);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocDt",
 
                    //1-5
                    "Yr", "mth", "EMpCode",  "EmpCodeOld", "Empname", 
                    
                    //6-10
                    "DeptName", "PosName", "IKP", "payrunCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    mdt = Sm.DrStr(dr, c[0]);
                    myr = Sm.DrStr(dr, c[1]);
                    mmth = Sm.DrStr(dr, c[2]);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sl.SetLueYr(LueYr, "");
            Sm.SetLue(LueYr, myr);
            Sl.SetLueMth(LueMth);
            Sm.SetLue(LueMth, mmth);
            Sm.SetDte(DteDocDt, mdt);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mNoWorkDayPerMth = Sm.GetParameterDec("NoWorkDayPerMth");
            mADCodePerformance = Sm.GetParameter("ADCodePerformance");
            mIsEmpContractDtMandatory = Sm.GetParameterBoo("IsEmpContractDtMandatory");
        }

        private void ProcessData2()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;

            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<ResultEmp>();

            ClearGrd();
            try
            {
                Process2a(ref lResult);
                if (lResult.Count > 0)
                {
                    Process2b(ref lResult);
                    Process2c();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process2a(ref List<ResultEmp> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;
            var EmpCodeTemp = string.Empty;
            var IndexTemp = 0m;
           
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            if (arr[0].Trim().Length != 8)
                            {
                                if(!mIsEmpContractDtMandatory)
                                    EmpCodeTemp = Sm.Right(string.Concat("0000000" + arr[0].Trim()), 8);
                                else
                                    EmpCodeTemp = Sm.Right(string.Concat("0000000" + arr[0].Trim()), 9);
                            }
                            else
                            {
                                EmpCodeTemp = arr[0].Trim();
                            }
                            string AmtTrim = arr[2].Trim();
                            if (arr[2].Trim().Length > 0)
                                IndexTemp = Decimal.Parse(AmtTrim);
                            else
                                IndexTemp = 0m;
                            l.Add(new ResultEmp()
                            {
                                 EmpCode = EmpCodeTemp,
                                 EmpCodeOld = string.Empty,
                                 EmpName = string.Empty,
                                 DeptName = string.Empty,
                                 PosName = string.Empty,
                                 Index = IndexTemp,
                            });
                        }
                    }
                }
            }
        }

        private void Process2b(ref List<ResultEmp> l)
        {
            decimal Amt = 0m;
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = l[i].EmpCode;
                r.Cells[1].Value = string.Empty;
                r.Cells[2].Value = string.Empty;
                r.Cells[3].Value = string.Empty;
                r.Cells[4].Value = string.Empty;
                r.Cells[5].Value = l[i].Index;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Grd1.EndUpdate();
        }


        private void Process2c()
        {
            Grd1.BeginUpdate();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EmpCode = string.Empty;
            var Filter = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + Row.ToString(), Sm.GetGrdStr(Grd1, Row, 0));
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 1=0 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, ");
            SQL.AppendLine("B.DeptName, C.PosName, F.payrunCode ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");     
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("   Select A.EmpCode, A.PayrunCode");
            SQL.AppendLine("   From tblempperformanceallowance A ");
            SQL.AppendLine("   Inner Join TblPayrun B On A.payrunCode = B.PayrunCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("   And B.CancelInd = 'N' And B.Status = 'C' ");
            SQL.AppendLine("   And Concat(A.Yr, A.Mth) = '" + String.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)) + "' ");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode ");

            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpCodeOld", 
                    "EmpName", 
                    "DeptName", 
                    "PosName",
                    "PayrunCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (EmpCode == Sm.GetGrdStr(Grd1, Row, 0))
                            {
                                Grd1.Cells[Row, 1].Value = Sm.DrStr(dr, c[1]);
                                Grd1.Cells[Row, 2].Value = Sm.DrStr(dr, c[2]);
                                Grd1.Cells[Row, 3].Value = Sm.DrStr(dr, c[3]);
                                Grd1.Cells[Row, 4].Value = Sm.DrStr(dr, c[4]);
                                Grd1.Cells[Row, 6].Value = Sm.DrStr(dr, c[5]);
                            }
                        }
                    }
                }
                dr.Close();
            }
            Grd1.EndUpdate();
        }


        #endregion

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date") || Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<ResultEmp>();

            ClearGrd();
            try
            {
                Process2a(ref lResult);
                if (lResult.Count > 0)
                {
                    Process2b(ref lResult);
                    Process2c();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion
    }

    #region Class

    internal class ResultEmp
    {
        public string EmpCode { get; set; }
        public string EmpCodeOld { get; set; }
        public string EmpName { get; set; }
        public string DeptName { get; set; }
        public string PosName { get; set; }
        public decimal Index { get; set; }
    }

    #endregion
}

