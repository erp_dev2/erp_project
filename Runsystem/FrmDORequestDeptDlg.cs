﻿#region Update
/*
    16/06/2017 [WED] validasi terhadap stock dihilangkan menggunakan parameter
    03/08/2017 [TKG] menambah opsi menampilkan stock dari semua gudang.
    31/08/2017 [HAR] item baru minta dimunculkan di dialog berdasarkan param  
    09/02/2018 [HAR] filter warehouse hanya kepake saat available stock di tick 
    21/03/2018 [WED] Item ambil dari master item. Kalau show avl stock di tick, Inner Join ke TblStockSummary berdasarkan whs, kalau untick Left Join TblStockSummary berdasarkan whs
    23/05/2018 [TKG] bisa difilter berdasarkan item name
 *  08/01/2020 [HAR/SIER] BUG saat available stock di centang
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDORequestDeptDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDORequestDept mFrmParent;
        private string mSQL = string.Empty;
        private string mWhsCode = string.Empty;
        private string mCCCode = string.Empty;
        internal int mNumberOfInventoryUomCode = 1;
        private bool mIsShowForeignName = false;
        private bool mIsDOReqUseNewItem = false;

        #endregion

        #region Constructor

        public FrmDORequestDeptDlg(FrmDORequestDept FrmParent, string WhsCode, string CCCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mCCCode = CCCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
                mIsDOReqUseNewItem = Sm.GetParameter("IsDOReqUseNewItem") == "Y";
                SetNumberOfInventoryUomCode();
                ChkShowAvlStock.Checked=true;
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct X.ItCode, X.WhsName, X.ItName, X.ForeignName, X.ItCtCode, X.ItCTName, X.CCtName, X.AcNo, X.InventoryUomCode, X.InventoryUomCode2, X.InventoryUomCode3, ");
            SQL.AppendLine("X.ItGrpCode, X.Qty, X.Qty2, X.Qty3 From ( ");
            SQL.AppendLine("    Select F.WhsName, B.ItCode, B.ItName, B.ForeignName, B.ItCtCode, C.ItCtName, E.CCtName, C.AcNo, ");
            SQL.AppendLine("    B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3, ");
            
            if (mFrmParent.mIsItGrpCodeShow)
                    SQL.AppendLine("B.ItGrpCode, ");
            else
                    SQL.AppendLine("Null As ItGrpCode, ");

            SQL.AppendLine("    Sum(IfNull(Qty, 0)) Qty, Sum(IfNull(Qty2, 0)) Qty2, Sum(IfNull(Qty3, 0)) Qty3 ");
            SQL.AppendLine("    From TblItem B ");
            
            if(ChkShowAvlStock.Checked)
                SQL.AppendLine("    Inner Join TblStockSummary A On A.ItCode=B.ItCode And A.WhsCode = @WhsCode And A.Qty > 0 ");
            else
                SQL.AppendLine("    Left Join TblStockSummary A On A.ItCode=B.ItCode And A.WhsCode = @WhsCode ");
            
            SQL.AppendLine("    Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("    Left Join TblItemCostCategory D On B.ItCode=D.ItCode And D.CCCode=@CCCode ");
            SQL.AppendLine("    Left Join TblCostCategory E On D.CCCode=E.CCCode And D.CCtCode=E.CCtCode ");
            SQL.AppendLine("    Left Join TblWarehouse F On A.WhsCode=F.WhsCode ");
            SQL.AppendLine("    Where B.ActInd = 'Y' ");
            SQL.AppendLine(" " + Filter.Replace("X.ItCode", "A.ItCode").Replace("X.ItName", "B.ItName").Replace("X.ForeignName", "B.ForeignName").Replace("X.ItCtCode", "B.ItCtCode") + " ");
            SQL.AppendLine("    Group By F.WhsName, B.ItCode, B.ItName, B.ForeignName, C.ItCtName, E.CCtName, C.AcNo, ");
            SQL.AppendLine("    B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3 ");
            
            if (mIsDOReqUseNewItem && ChkShowAvlStock.Checked == false)
            {
                SQL.AppendLine("    Union ALL ");
                SQL.AppendLine("    Select '' as Whs, A.ItCode, A.ItName, A.ForeignName, A.ItCTCode, B.ItCTname, D.CCtName, B.ACno,  ");
                SQL.AppendLine("    A.InventoryUomCode, A.InventoryUOMCode2, A.InventoryUOMCode3, '' As ItGrpCode, 0 , 0 , 0 ");
                SQL.AppendLine("    From TblItem A ");
                SQL.AppendLine("    Inner JOin TblItemCategory B On A.ItCtCode = B.ItCtCode " + Filter.Replace("X.ItCode", "A.ItCode").Replace("X.ItName", "A.ItName").Replace("X.ForeignName", "A.ForeignName").Replace("X.ItCtCode", "A.ItCtCode"));
                SQL.AppendLine("    Left Join TblItemCostCategory C On A.ItCode=C.ItCode And C.CCCode=@CCCode ");
                SQL.AppendLine("    Left Join TblCostCategory D On C.CCCode=D.CCCode And C.CCtCode=D.CCtCode ");
                SQL.AppendLine("    Where A.ItCode Not In (Select ItCode From TblStockPrice) ");
                SQL.AppendLine("    And A.ActInd = 'Y' And A.InventoryItemInd = 'Y' ");
            }
            SQL.AppendLine(")X ");
            SQL.AppendLine("Where 0=0 ");



            //if (IsShowAllWhsStock)
            //{
            //    if (mFrmParent.mIsDORequestNeedStockValidation)
            //        SQL.AppendLine("And A.WhsCode=@WhsCode ");
            //}
            //else
            //    SQL.AppendLine("And A.WhsCode=@WhsCode ");
            //if (mFrmParent.mIsDORequestNeedStockValidation)
            //    SQL.AppendLine("And A.Qty>0 ");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Item's Name",
                        "Foreign Name",

                        //6-10
                        "Item's Category",
                        "Stock",
                        "UoM",
                        "Stock",
                        "UoM",
                        
                        //11-15
                        "Stock",
                        "UoM",
                        "Cost Category",
                        "Inventory's"+Environment.NewLine+"COA Account#",
                        "Group",

                        //16
                        "Warehouse"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 20, 250, 230,                        
                        
                        //6-10
                        180, 100, 80, 100, 80,
                        
                        //11-15
                        100, 80, 200, 150, 100,

                        //16
                        180
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 11 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 9, 10, 11, 12, 15, 16 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 6, 9, 10, 11, 12, 15, 16 }, false);
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[15].Visible = true;
                Grd1.Cols[15].Move(4);
            }
            Grd1.Cols[16].Move(2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 16 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10}, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12 }, true);
        }
        
      override protected void ShowData()
        {
            try
            {
                if (ChkShowAvlStock.Checked)
                    ShowData2();
                else
                    ShowData3();
            }  
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 15);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 11, 14, 17 });
                    
                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10, 11, 13, 14, 16, 17 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 6), ItCode))
                    return true;
            return false;
        }

        #endregion

        #region Additional Methods

        private void ShowData2()
        {
            Cursor.Current = Cursors.WaitCursor;
            string Filter = " ", Filter2 = string.Empty;

            var cm = new MySqlCommand();

            if (mFrmParent.Grd1.Rows.Count >= 1)
            {
                var No = "0001";
                for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd1, Row, 6).Length != 0)
                    {
                        if (Filter2.Length > 0) Filter2 += " And ";
                        Filter2 += "(X.ItCode<>@ItCode0" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ItCode0" + No.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, Row, 6));
                        No = ("000" + (int.Parse(No) + 1).ToString()).ToString();
                    }
                }
            }
            if (Filter2.Length != 0) Filter2 = " And (" + Filter2 + ")";

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(mFrmParent.LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(mFrmParent.LueCCCode));
            Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName", "X.ForeignName" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "X.ItCtCode", true);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                GetSQL(Filter) + Filter2 +
                " Order By X.ItName, X.Qty, X.WhsName  ",
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItName", "ForeignName", "ItCtName", "Qty", "InventoryUomCode",
                    
                    //6-10
                    "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CCtName",
                    
                    //11-13
                    "AcNo", "ItGrpCode", "WhsName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                }, true, false, false, false
            );
        }

        private void ShowData3()
        {
            Sm.ClearGrd(Grd1, false);
            var lI = new List<AllItem>();
            var lAS = new List<AvailableItem>();

            ListAllItem(ref lI);
            ListAvailableItem(ref lAS);

            if (lI.Count > 0)
            {
                for (int i = 0; i < lI.Count; i++)
                {
                    for (int j = 0; j < lAS.Count; j++)
                    {
                        if (lI[i].ItCode == lAS[j].ItCode)
                        {
                            lI[i].Qty = lAS[j].Qty;
                            lI[i].Qty2 = lAS[j].Qty2;
                            lI[i].Qty3 = lAS[j].Qty3;
                            lI[i].WhsName = lAS[j].WhsName;
                        }
                    }
                }

                ShowDataInGrd(ref lI);
            }
            else
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }
        }

        private void ListAllItem(ref List<AllItem> lI)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter2 = string.Empty;
            string mItCode = string.Empty;

            if (mFrmParent.Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd1, Row, 6).Length != 0)
                    {
                        if (mItCode.Length > 0) mItCode += ",";
                        mItCode += Sm.GetGrdStr(mFrmParent.Grd1, Row, 6);
                    }
                }

                if (mItCode.Length > 0) Filter2 = " And Not Find_In_Set(A.ItCode, '" + mItCode + "') ";
            }

            SQL.AppendLine("Select Distinct T.ItCode, T.ItName, T.ForeignName, T.ItCTCode, T.ItCTname, T.CCtName, T.ACno, ");
            SQL.AppendLine("T.InventoryUomCode, T.InventoryUOMCode2, T.InventoryUOMCode3, T.ItGrpCode ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.ItCode, A.ItName, A.ForeignName, A.ItCTCode, B.ItCTname, D.CCtName, B.ACno, ");
            SQL.AppendLine("    A.InventoryUomCode, A.InventoryUOMCode2, A.InventoryUOMCode3, ");
            
            if (mFrmParent.mIsItGrpCodeShow)
                SQL.AppendLine("    A.ItGrpCode ");
            else
                SQL.AppendLine("    Null As ItGrpCode ");
            
            SQL.AppendLine("    From TblItem A ");
            SQL.AppendLine("    Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            
            if (Sm.GetLue(LueItCtCode).Length > 0)
                SQL.AppendLine("      And A.ItCtCode = '" + Sm.GetLue(LueItCtCode) + "' ");
            
            SQL.AppendLine("    Left Join TblItemCostCategory C On A.ItCode = C.ItCode And C.CCCode = @CCCode ");
            SQL.AppendLine("    Left Join TblCostCategory D On C.CCCode = D.CCCode And C.CCtCode = D.CCtCode ");
            SQL.AppendLine("    Where A.ActInd = 'Y' ");
            
            if (TxtItCode.Text.Length > 0)
                SQL.AppendLine("    And (A.ItCode Like '%"+TxtItCode.Text+"%' Or A.ItName Like '%"+TxtItCode.Text+"%' Or A.ForeignName Like '%"+TxtItCode.Text+"%') ");
            
            SQL.AppendLine(Filter2);

            if (mIsDOReqUseNewItem && ChkShowAvlStock.Checked == false)
            {
                SQL.AppendLine("    Union ALL ");
                SQL.AppendLine("    Select A.ItCode, A.ItName, A.ForeignName, A.ItCTCode, B.ItCTname, D.CCtName, B.ACno, ");
                SQL.AppendLine("    A.InventoryUomCode, A.InventoryUOMCode2, A.InventoryUOMCode3, null As ItGrpCode ");
                SQL.AppendLine("    From TblItem A ");
                SQL.AppendLine("    Inner JOin TblItemCategory B On A.ItCtCode = B.ItCtCode ");
                
                if (Sm.GetLue(LueItCtCode).Length > 0)
                    SQL.AppendLine("    And A.ItCtCode = '" + Sm.GetLue(LueItCtCode) + "' ");
                
                SQL.AppendLine("    Left Join TblItemCostCategory C On A.ItCode=C.ItCode And C.CCCode=@CCCode ");
                SQL.AppendLine("    Left Join TblCostCategory D On C.CCCode=D.CCCode And C.CCtCode=D.CCtCode ");
                SQL.AppendLine("    Where A.ItCode Not In (Select ItCode From TblStockPrice) ");
                SQL.AppendLine("    And A.ActInd = 'Y' And A.InventoryItemInd = 'Y' ");
                
                if (TxtItCode.Text.Length > 0)
                    SQL.AppendLine("    And (A.ItCode Like '%" + TxtItCode.Text + "%' Or A.ItName Like '%" + TxtItCode.Text + "%' Or A.ForeignName Like '%" + TxtItCode.Text + "%') ");

                SQL.AppendLine(Filter2);
            }

            SQL.AppendLine(")T ");
            SQL.AppendLine("Order By T.ItName; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(mFrmParent.LueCCCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                     //0
                    "ItCode",

                    //1-5
                    "ItName", "ForeignName", "ItCtName", "InventoryUomCode", "InventoryUomCode2", 
                    
                    //6-9
                    "InventoryUomCode3", "CCtName", "AcNo", "ItGrpCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lI.Add(new AllItem()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            ItName = Sm.DrStr(dr, c[1]),
                            ForeignName = Sm.DrStr(dr, c[2]),
                            ItCtName = Sm.DrStr(dr, c[3]),
                            Qty = 0m,
                            InventoryUoMCode = Sm.DrStr(dr, c[4]),
                            Qty2 = 0m,
                            InventoryUoMCode2 = Sm.DrStr(dr, c[5]),
                            Qty3 = 0m,
                            InventoryUoMCode3 = Sm.DrStr(dr, c[6]),
                            CCtName = Sm.DrStr(dr, c[7]),
                            AcNo = Sm.DrStr(dr, c[8]),
                            ItGrpCode = Sm.DrStr(dr, c[9])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ListAvailableItem(ref List<AvailableItem> lAS)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ItCode, B.WhsName, Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ItCode, WhsCode, Qty, Qty2, Qty3 ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where WhsCode = @WhsCode And Qty > 0 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Group By A.ItCode, B.WhsName; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(mFrmParent.LueWhsCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                     //0
                    "ItCode",

                    //1-3
                    "WhsName", "Qty", "Qty2", "Qty3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lAS.Add(new AvailableItem()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            WhsName = Sm.DrStr(dr, c[1]),
                            Qty = Sm.DrDec(dr, c[2]),
                            Qty2 = Sm.DrDec(dr, c[3]),
                            Qty3 = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowDataInGrd(ref List<AllItem> lI)
        {
            Grd1.BeginUpdate();
            Grd1.Rows.Count = lI.Count();
            int No = 1;

            for (int i = 0; i < lI.Count; i++)
            {
                Grd1.Cells[i, 0].Value = No++;
                Grd1.Cells[i, 2].Value = lI[i].ItCode;
                Grd1.Cells[i, 4].Value = lI[i].ItName;
                Grd1.Cells[i, 5].Value = lI[i].ForeignName;
                Grd1.Cells[i, 6].Value = lI[i].ItCtName;
                Grd1.Cells[i, 7].Value = lI[i].Qty;
                Grd1.Cells[i, 8].Value = lI[i].InventoryUoMCode;
                Grd1.Cells[i, 9].Value = lI[i].Qty2;
                Grd1.Cells[i, 10].Value = lI[i].InventoryUoMCode2;
                Grd1.Cells[i, 11].Value = lI[i].Qty3;
                Grd1.Cells[i, 12].Value = lI[i].InventoryUoMCode3;
                Grd1.Cells[i, 13].Value = lI[i].CCtName;
                Grd1.Cells[i, 14].Value = lI[i].AcNo;
                Grd1.Cells[i, 15].Value = lI[i].ItGrpCode;
                Grd1.Cells[i, 16].Value = lI[i].WhsName;
            }

            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        #endregion

        #region Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #region Class

        class AllItem
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string ItCtName { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUoMCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUoMCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUoMCode3 { get; set; }
            public string CCtName { get; set; }
            public string AcNo { get; set; }
            public string ItGrpCode { get; set; }
            public string WhsName { get; set; }
        }

        class AvailableItem
        {
            public string ItCode { get; set; }
            public string WhsName { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
        }

        #endregion

    }
}
