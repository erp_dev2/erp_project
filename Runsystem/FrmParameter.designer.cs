﻿namespace RunSystem
{
    partial class FrmParameter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtCustomize = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtParValue = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtParDesc = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtParCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueParCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCustomize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParCtCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(892, 0);
            this.panel1.Size = new System.Drawing.Size(70, 300);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueParCtCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtCustomize);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtParValue);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtParDesc);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtParCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(892, 300);
            // 
            // TxtCustomize
            // 
            this.TxtCustomize.EnterMoveNextControl = true;
            this.TxtCustomize.Location = new System.Drawing.Point(154, 136);
            this.TxtCustomize.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtCustomize.Name = "TxtCustomize";
            this.TxtCustomize.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCustomize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCustomize.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCustomize.Properties.Appearance.Options.UseFont = true;
            this.TxtCustomize.Properties.MaxLength = 255;
            this.TxtCustomize.Size = new System.Drawing.Size(365, 24);
            this.TxtCustomize.TabIndex = 16;
            this.TxtCustomize.Validated += new System.EventHandler(this.TxtCustomize_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(40, 141);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "Customized For";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtParValue
            // 
            this.TxtParValue.EnterMoveNextControl = true;
            this.TxtParValue.Location = new System.Drawing.Point(154, 105);
            this.TxtParValue.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtParValue.Name = "TxtParValue";
            this.TxtParValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParValue.Properties.Appearance.Options.UseFont = true;
            this.TxtParValue.Properties.MaxLength = 9999;
            this.TxtParValue.Size = new System.Drawing.Size(365, 24);
            this.TxtParValue.TabIndex = 14;
            this.TxtParValue.Validated += new System.EventHandler(this.TxtParValue_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(101, 109);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Value";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtParDesc
            // 
            this.TxtParDesc.EnterMoveNextControl = true;
            this.TxtParDesc.Location = new System.Drawing.Point(154, 76);
            this.TxtParDesc.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtParDesc.Name = "TxtParDesc";
            this.TxtParDesc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtParDesc.Properties.MaxLength = 255;
            this.TxtParDesc.Size = new System.Drawing.Size(689, 24);
            this.TxtParDesc.TabIndex = 12;
            this.TxtParDesc.Validated += new System.EventHandler(this.TxtParDesc_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(66, 78);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtParCode
            // 
            this.TxtParCode.EnterMoveNextControl = true;
            this.TxtParCode.Location = new System.Drawing.Point(154, 46);
            this.TxtParCode.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtParCode.Name = "TxtParCode";
            this.TxtParCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParCode.Properties.Appearance.Options.UseFont = true;
            this.TxtParCode.Properties.MaxLength = 80;
            this.TxtParCode.Size = new System.Drawing.Size(365, 24);
            this.TxtParCode.TabIndex = 10;
            this.TxtParCode.Validated += new System.EventHandler(this.TxtParCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(103, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(78, 171);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 18);
            this.label5.TabIndex = 17;
            this.label5.Text = "Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParCtCode
            // 
            this.LueParCtCode.EnterMoveNextControl = true;
            this.LueParCtCode.Location = new System.Drawing.Point(153, 168);
            this.LueParCtCode.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueParCtCode.Name = "LueParCtCode";
            this.LueParCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueParCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParCtCode.Properties.DropDownRows = 25;
            this.LueParCtCode.Properties.NullText = "[Empty]";
            this.LueParCtCode.Properties.PopupWidth = 500;
            this.LueParCtCode.Size = new System.Drawing.Size(366, 24);
            this.LueParCtCode.TabIndex = 18;
            this.LueParCtCode.ToolTip = "F4 : Show/hide list";
            this.LueParCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParCtCode.EditValueChanged += new System.EventHandler(this.LueParCtCode_EditValueChanged);
            // 
            // FrmParameter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 300);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmParameter";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCustomize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParCtCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtCustomize;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtParValue;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtParDesc;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtParCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueParCtCode;
    }
}