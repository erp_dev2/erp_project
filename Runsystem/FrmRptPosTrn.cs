﻿#region Update
/*
    20/07/2020 [HAR/SRN] ada parameter untuk menampung nama database perantara
    08/12/2020 [IBL/SRN] tambah kolom warehouse + filter. Kolom total diambil dari (Qty*UPrice) - DiscAmt
                         left join item dg itemcategory berdasarkan ItCode, bukan ItCode=ItCodeInternal
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptPosTrn : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string mDBNamePOS = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPosTrn(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                TxtPosNo.EditValue = Gv.PosNo;
                ChkPosNo.Checked = Gv.PosNo.Length > 0;
                SetLueWhsCode(ref LueWhsCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mDBNamePOS = Sm.GetParameter("DBNamePOS");
         }


        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TrnNo, A.BsDate, A.ShiftNo, ");
            SQL.AppendLine("A.PosNo, A.PayTyNo, A.ItCode, B.Itname, C.ItCtName, A.Qty,  ");
            SQL.AppendLine("A.Uprice, A.DiscAmt, A.taxAmt, (IfNull(A.Qty,0.00)*IfNull(A.Uprice, 0.00))-IfNull(A.DiscAmt, 0.00) As Total, D.WhsName ");
            SQL.AppendLine("From " + mDBNamePOS + ".tblpostrn A  ");
            SQL.AppendLine("Left Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Left Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("Left Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Transaction",
                        "Business"+Environment.NewLine+"Date", 
                        "Shift",
                        "Pos"+Environment.NewLine+"Number",
                        "Payment"+Environment.NewLine+"Type",
                        
                        //6-10
                        "Item"+Environment.NewLine+"Code",
                        "Item"+Environment.NewLine+"Name",
                        "Item"+Environment.NewLine+"Category",
                        "Quantity",
                        "Price",
                        
                        //11-14
                        "Discount"+Environment.NewLine+"Amount",
                        "Tax"+Environment.NewLine+"Amount",
                        "Total",
                        "Warehouse"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 100, 80, 80, 100, 

                        //6-10
                        120, 180, 150, 80, 150,  

                        //11-14
                        150, 150, 150, 150

                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Grd1.Cols[14].Move(1);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.BsDate");
                Sm.FilterStr(ref Filter, ref cm, TxtPosNo.Text, "A.PosNo", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItName", "B.ItCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
               

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.PosNo, A.BsDate, A.TrnNo;",
                new string[]
                    {
                        //0
                       "TrnNo", 
                       //1-5
                       "BsDate", "ShiftNo", "PosNo", "PayTyNo", "ItCode", 
                       //6-10
                       "Itname", "ItCtName", "Qty", "Uprice", "DiscAmt",   
                       //11-13
                       "taxAmt", "Total", "WhsName"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                }, true, false, false, false
                );
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 11, 12, 13 });
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueWhsCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct(T1.WhsCode) As Col1, T2.WhsName As Col2 ");
            SQL.AppendLine("From " + mDBNamePOS + ".TblPosTrn T1");
            SQL.AppendLine("Inner Join TblWarehouse T2 On T1.WhsCode = T2.WhsCode ");

            Sm.SetLue2(ref Lue, SQL.ToString(),0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event
        private void TxtPosNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPosNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "POS");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }


        #endregion
        
    }
}
