﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControlDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmShopFloorControl mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmShopFloorControlDlg(FrmShopFloorControl FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "DNo",
                        "Date",
                        "Production"+Environment.NewLine+"Start Date",

                        //6-10
                        "Production"+Environment.NewLine+"End Date",
                        "SO#/MTS#",
                        "",
                        "Item Code", 
                        "",

                        //11-15
                        "Item Name",
                        "Quantity",
                        "UoM",
                        "Production"+Environment.NewLine+"Routing#",
                        "",

                        //16-20
                        "Work Center#",
                        "",
                        "Work Center"+Environment.NewLine+"Name",
                        "Location",
                        "Capacity",

                        //21-25
                        "Uom/Hour",
                        "Sequence",
                        "BOM#",
                        "",
                        "BOM"+Environment.NewLine+"Name",
                        
                        //26-28
                        "Quantity",
                        "UoM",
                        ""
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 8, 10, 15, 17, 24 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 9, 11, 12, 13, 14, 16, 18, 19, 20, 21, 22, 23, 25, 26, 27 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 20, 26 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 22 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 8, 9, 10, 14, 15, 16, 17, 19, 20, 21, 23, 24, 28 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 8, 9, 10, 14, 15, 16, 17, 19, 20, 21, 23, 24 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select * From (");
            //SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, A.DocType, A.ProdStartDt, A.ProdEndDt, ");
            //SQL.AppendLine("Case A.DocType When '1' Then A.SODocNo Else A.MakeToStockDocNo End As SOMTSDocNo, ");
            //SQL.AppendLine("Case A.DocType When '1' Then I.ItCode Else J.ItCode End As ItCode, ");
            //SQL.AppendLine("Case A.DocType When '1' Then I.ItName Else J.ItName End As ItName, ");
            //SQL.AppendLine("Case A.DocType When '1' Then I.PlanningUomCode Else J.PlanningUomCode End As ProductionOrderPlanningUomCode, ");
            //SQL.AppendLine("A.Qty As ProductionOrderQty, A.ProductionRoutingDocNo, ");
            //SQL.AppendLine("E.WorkCenterDocNo, F.DocName As WorkCenterDocName, F.Location, F.Capacity, F.UomCode, E.Sequence,  ");
            //SQL.AppendLine("B.BomDocNo, G.DocName As BomDocName, G.Qty As BomQty, H.PlanningUomCode As BomPlanningUomCode ");
            //SQL.AppendLine("From TblProductionOrderHdr A ");
            //SQL.AppendLine("Inner Join TblProductionOrderDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select T1.DocNo, T2.DNo, T4.ItCode ");
            //SQL.AppendLine("    From TblSOHdr T1 ");
            //SQL.AppendLine("    Inner Join TblSODtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    Left Join TblCtQtDtl T3 On T1.CtQtDocNo=T3.DocNo And T2.CtQtDNo=T3.DNo ");
            //SQL.AppendLine("    Left Join TblItemPriceDtl T4 On T3.ItemPriceDocNo=T4.DocNo And T3.ItemPriceDNo=T4.DNo ");
            //SQL.AppendLine(") C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
            //SQL.AppendLine("Left Join TblMakeToStockDtl D On A.MakeToStockDocNo=D.DocNo And A.MakeToStockDNo=D.DNo ");
            //SQL.AppendLine("Inner Join TblProductionRoutingDtl E On A.ProductionRoutingDocNo=E.DocNo And B.ProductionRoutingDNo=E.DNo ");
            //SQL.AppendLine("Inner Join TblWorkCenterHdr F On E.WorkCenterDocNo=F.DocNo ");
            //SQL.AppendLine("Inner Join TblBomHdr G On B.BomDocNo=G.DocNo ");
            //SQL.AppendLine("Inner Join TblItem H On G.ItCode=H.ItCode ");
            //SQL.AppendLine("Left Join TblItem I On C.ItCode=I.ItCode ");
            //SQL.AppendLine("Left Join TblItem J On D.ItCode=J.ItCode ");
            //SQL.AppendLine("Where A.Status<>'C' ");
            //SQL.AppendLine("And Concat(A.DocNo, B.DNo) Not In ( ");
            //SQL.AppendLine("    Select Concat(ProductionOrderDocNo, ProductionOrderDNo) ");
            //SQL.AppendLine("    From TblShopFloorControlHdr Where CancelInd='N' ) ");
            //SQL.AppendLine(") T ");

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, A.DocType, A.ProdStartDt, A.ProdEndDt, ");
            SQL.AppendLine("Case A.DocType When '1' Then A.SODocNo Else A.MakeToStockDocNo End As SOMTSDocNo, ");
            SQL.AppendLine("Case A.DocType When '1' Then G.ItCode Else H.ItCode End As ItCode, ");
            SQL.AppendLine("Case A.DocType When '1' Then G.ItName Else H.ItName End As ItName, ");
            SQL.AppendLine("Case A.DocType When '1' Then G.PlanningUomCode Else H.PlanningUomCode End As ProductionOrderPlanningUomCode, ");
            SQL.AppendLine("A.Qty As ProductionOrderQty, A.ProductionRoutingDocNo, ");
            SQL.AppendLine("E.WorkCenterDocNo, F.DocName As WorkCenterDocName, F.Location, F.Capacity, F.UomCode, E.Sequence,  ");
            SQL.AppendLine("B.BomDocNo, I.DocName As BomDocName, J.Qty As BomQty, K.PlanningUomCode As BomPlanningUomCode ");
            SQL.AppendLine("From TblProductionOrderHdr A ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, T4.ItCode ");
            SQL.AppendLine("    From TblSOHdr T1 ");
            SQL.AppendLine("    Inner Join TblSODtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Left Join TblCtQtDtl T3 On T1.CtQtDocNo=T3.DocNo And T2.CtQtDNo=T3.DNo ");
            SQL.AppendLine("    Left Join TblItemPriceDtl T4 On T3.ItemPriceDocNo=T4.DocNo And T3.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine(") C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
            SQL.AppendLine("Left Join TblMakeToStockDtl D On A.MakeToStockDocNo=D.DocNo And A.MakeToStockDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl E On A.ProductionRoutingDocNo=E.DocNo And B.ProductionRoutingDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr F On E.WorkCenterDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblItem G On C.ItCode=G.ItCode ");
            SQL.AppendLine("Left Join TblItem H On D.ItCode=H.ItCode ");
            SQL.AppendLine("Inner Join TblBomHdr I On B.BomDocNo=I.DocNo ");
            SQL.AppendLine("Inner Join TblBomDtl2 J On I.DocNo=J.DocNo And J.ItType='1' ");
            SQL.AppendLine("Left Join TblItem K On J.ItCode=K.ItCode ");
            SQL.AppendLine("Where IfNull(A.Status, 'X')<>'C' ");
            SQL.AppendLine("And Concat(A.DocNo, B.DNo) Not In ( ");
            SQL.AppendLine("    Select Concat(ProductionOrderDocNo, ProductionOrderDNo) ");
            SQL.AppendLine("    From TblShopFloorControlHdr Where CancelInd='N' ) ");
            SQL.AppendLine(") T ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, "SOMTSDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DocDt, DocNo, DNo;",
                        new string[] 
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DNo", 
                             "DocDt", 
                             "ProdStartDt", 
                             "ProdEndDt", 
                             "SOMTSDocNo", 
            
                             //6-10
                             "ItCode", 
                             "ItName", 
                             "ProductionOrderQty", 
                             "ProductionOrderPlanningUomCode", 
                             "ProductionRoutingDocNo", 
                             
                             //11-15
                             "WorkCenterDocNo", 
                             "WorkCenterDocName", 
                             "Location", 
                             "Capacity", 
                             "UomCode", 
                             
                             //16-20
                             "Sequence",  
                             "BomDocNo", 
                             "BomDocName", 
                             "BomQty", 
                             "BomPlanningUomCode",

                             //21
                             "DocType"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 21);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtProductionOrderDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.mProductionOrderDNo = Sm.GetGrdStr(Grd1, Row, 3);
                Sm.SetDte(mFrmParent.DteProdStartDt, Sm.GetGrdDate(Grd1, Row, 5));
                Sm.SetDte(mFrmParent.DteProdEndDt, Sm.GetGrdDate(Grd1, Row, 6));
                mFrmParent.TxtSODocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.TxtItName.EditValue = Sm.GetGrdStr(Grd1, Row, 11);
                mFrmParent.TxtProductionOrderQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0);
                mFrmParent.TxtProductionOrderPlanningUomCode.EditValue = Sm.GetGrdStr(Grd1, Row, 13);
                mFrmParent.TxtProductionRoutingDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 14);
                mFrmParent.TxtWorkCenterDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 16);
                mFrmParent.TxtWorkCenterDocName.EditValue = Sm.GetGrdStr(Grd1, Row, 18);
                mFrmParent.TxtLocation.EditValue = Sm.GetGrdStr(Grd1, Row, 19);
                mFrmParent.TxtCapacity.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 20), 0);
                mFrmParent.TxtUomCode.EditValue = Sm.GetGrdStr(Grd1, Row, 21);
                mFrmParent.TxtSequence.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 22), 2);;
                mFrmParent.TxtBomDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 23);
                mFrmParent.TxtBomDocName.EditValue = Sm.GetGrdStr(Grd1, Row, 25);
                mFrmParent.TxtBomQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 26), 0);
                mFrmParent.TxtBomPlanningUomCode.EditValue = Sm.GetGrdStr(Grd1, Row, 27);
                mFrmParent.mDocType = Sm.GetGrdStr(Grd1, Row, 28);

                mFrmParent.ClearGrd();
                mFrmParent.ShowBomInfo(mFrmParent.TxtBomDocNo.Text);

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }

            if (e.ColIndex == 24 && Sm.GetGrdStr(Grd1, e.RowIndex, 23).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 23);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }

            if (e.ColIndex == 24 && Sm.GetGrdStr(Grd1, e.RowIndex, 23).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 23);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void TxtSODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Sales order document number");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
