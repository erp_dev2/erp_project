﻿#region Update
/*
    09/05/2017 [WED] Combo untuk kolom panjang di tab retur
    07/06/2020 [TKG/IOK] tambah "if (BtnSave.Enabled)" di misc control event 
    09/06/2020 [TKG/IOK] test menggunakan ShowRecvRawMaterialDtl2_3(DocNo)
    30/08/2020 [TKG/IOK] actual item hanya yg aktif saja.
    21/10/2021 [ICA/IOK] bug nomor rak 11-15 tidak bisa edit.
    10/11/2021 [VIN/IOK] bug employee tidak bisa edit.-> tambah setlue di btnedit
    30/11/2021 [BRI/IOK] tambah rak 16 - 25
    11/04/2022 [TKG/IOK] merubah proses save data     
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRecvRawMaterial : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRecvRawMaterialFind FrmFind;
        iGCell fCell;
        bool fAccept;
        string mItCtRMPActual = string.Empty, mSQLForActualItCode = string.Empty;
        internal byte mType = 1; //1=Log;2=Balok

        #endregion

        #region Constructor

        public FrmRecvRawMaterial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Raw Material";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);

                if (Sm.CompareStr(Sm.GetParameter("RRMMenuCode2"), mMenuCode)) mType = 2;
                
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);

                tabControl1.SelectTab("tabPageReturn");
                SetLueLengthCode(ref LueLengthCode);
                LueLengthCode.Visible = false;

                SetTabControls("2", ref LueShelf2, ref LueWhsCode2, ref LueItCode2);
                SetTabControls("3", ref LueShelf3, ref LueWhsCode3, ref LueItCode3);
                SetTabControls("4", ref LueShelf4, ref LueWhsCode4, ref LueItCode4);
                SetTabControls("5", ref LueShelf5, ref LueWhsCode5, ref LueItCode5);
                SetTabControls("6", ref LueShelf6, ref LueWhsCode6, ref LueItCode6);
                SetTabControls("7", ref LueShelf7, ref LueWhsCode7, ref LueItCode7);
                SetTabControls("8", ref LueShelf8, ref LueWhsCode8, ref LueItCode8);
                SetTabControls("9", ref LueShelf9, ref LueWhsCode9, ref LueItCode9);
                SetTabControls("10", ref LueShelf10, ref LueWhsCode10, ref LueItCode10);
                SetTabControls("11", ref LueShelf11, ref LueWhsCode11, ref LueItCode11);
                SetTabControls("12", ref LueShelf12, ref LueWhsCode12, ref LueItCode12);
                SetTabControls("13", ref LueShelf13, ref LueWhsCode13, ref LueItCode13);
                SetTabControls("14", ref LueShelf14, ref LueWhsCode14, ref LueItCode14);
                SetTabControls("15", ref LueShelf15, ref LueWhsCode15, ref LueItCode15);
                SetTabControls("16", ref LueShelf16, ref LueWhsCode16, ref LueItCode16);
                SetTabControls("17", ref LueShelf17, ref LueWhsCode17, ref LueItCode17);
                SetTabControls("18", ref LueShelf18, ref LueWhsCode18, ref LueItCode18);
                SetTabControls("19", ref LueShelf19, ref LueWhsCode19, ref LueItCode19);
                SetTabControls("20", ref LueShelf20, ref LueWhsCode20, ref LueItCode20);
                SetTabControls("21", ref LueShelf21, ref LueWhsCode21, ref LueItCode21);
                SetTabControls("22", ref LueShelf22, ref LueWhsCode22, ref LueItCode22);
                SetTabControls("23", ref LueShelf23, ref LueWhsCode23, ref LueItCode23);
                SetTabControls("24", ref LueShelf24, ref LueWhsCode24, ref LueItCode24);
                SetTabControls("25", ref LueShelf25, ref LueWhsCode25, ref LueItCode25);
                SetTabControls("1", ref LueShelf1, ref LueWhsCode1, ref LueItCode1);

                LueEmpCode1.Visible =
                LueEmpCode2.Visible =
                LueEmpCode3.Visible =
                LueEmpCode4.Visible = false;

                mItCtRMPActual = Sm.GetParameter("ItCtRMPActual"+((mType==2)?"2":""));
                SetSQLForActualItCode();
                base.FrmLoad(sender, e);                

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = 
                    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, LueQueueNo, TxtLegalDocVerifyDocNo,
                        TxtVdCode, TxtVdCode2, TxtTTCode, DteUnloadStartDt, TmeUnloadStartTm, 
                        DteUnloadEndDt, TmeUnloadEndTm, TxtTotal, TxtCreateBy, MeeRemark, 
                        LueShelf1, LueShelf2, LueShelf3, LueShelf4, LueShelf5, 
                        LueShelf6, LueShelf7, LueShelf8, LueShelf9, LueShelf10,
                        LueShelf11, LueShelf12, LueShelf13, LueShelf14, LueShelf15,
                        LueShelf16, LueShelf17, LueShelf18, LueShelf19, LueShelf20,
                        LueShelf21, LueShelf22, LueShelf23, LueShelf24, LueShelf25,
                        LueWhsCode1, LueWhsCode2, LueWhsCode3, LueWhsCode4, LueWhsCode5, 
                        LueWhsCode6, LueWhsCode7, LueWhsCode8, LueWhsCode9, LueWhsCode10,
                        LueWhsCode11, LueWhsCode12, LueWhsCode13, LueWhsCode14, LueWhsCode15,
                        LueWhsCode16, LueWhsCode17, LueWhsCode18, LueWhsCode19, LueWhsCode20,
                        LueWhsCode21, LueWhsCode22, LueWhsCode23, LueWhsCode24, LueWhsCode25,
                        TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, 
                        TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                        TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15,
                        TxtTotal16, TxtTotal17, TxtTotal18, TxtTotal19, TxtTotal20,
                        TxtTotal21, TxtTotal22, TxtTotal23, TxtTotal24, TxtTotal25, LueLengthCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1 });
                    Sm.GrdColReadOnly(true, true, Grd11, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd12, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd13, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd14, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd15, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd16, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd17, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd18, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd19, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd20, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd21, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd22, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd23, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd24, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd25, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd26, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd27, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd28, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd29, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd30, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd31, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd32, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd33, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd34, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd35, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, GrdReturn, new int[] { 1, 2, 3, 4 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueQueueNo, DteUnloadStartDt, TmeUnloadStartTm, 
                        DteUnloadEndDt, TmeUnloadEndTm, MeeRemark, 
                        LueShelf1, LueShelf2, LueShelf3, LueShelf4, LueShelf5, 
                        LueShelf6, LueShelf7, LueShelf8, LueShelf9, LueShelf10,
                        LueShelf11, LueShelf12, LueShelf13, LueShelf14, LueShelf15,
                        LueShelf16, LueShelf17, LueShelf18, LueShelf19, LueShelf20,
                        LueShelf21, LueShelf22, LueShelf23, LueShelf24, LueShelf25,
                        LueWhsCode1, LueWhsCode2, LueWhsCode3, LueWhsCode4, LueWhsCode5, 
                        LueWhsCode6, LueWhsCode7, LueWhsCode8, LueWhsCode9, LueWhsCode10,
                        LueWhsCode11, LueWhsCode12, LueWhsCode13, LueWhsCode14, LueWhsCode15,
                        LueWhsCode16, LueWhsCode17, LueWhsCode18, LueWhsCode19, LueWhsCode20,
                        LueWhsCode21, LueWhsCode22, LueWhsCode23, LueWhsCode24, LueWhsCode25, LueLengthCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd11, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd12, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd13, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd14, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd15, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd16, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd17, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd18, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd19, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd20, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd21, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd22, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd23, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd24, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd25, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd26, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd27, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd28, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd29, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd30, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd31, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd32, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd33, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd34, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd35, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, GrdReturn, new int[] { 1, 2, 3, 4 });
                    DteDocDt.Focus();
                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeRemark, 
                        LueShelf1, LueShelf2, LueShelf3, LueShelf4, LueShelf5, 
                        LueShelf6, LueShelf7, LueShelf8, LueShelf9, LueShelf10,
                        LueShelf11, LueShelf12, LueShelf13, LueShelf14, LueShelf15,
                        LueShelf16, LueShelf17, LueShelf18, LueShelf19, LueShelf20,
                        LueShelf21, LueShelf22, LueShelf23, LueShelf24, LueShelf25,
                        LueWhsCode1, LueWhsCode2, LueWhsCode3, LueWhsCode4, LueWhsCode5, 
                        LueWhsCode6, LueWhsCode7, LueWhsCode8, LueWhsCode9, LueWhsCode10,
                        LueWhsCode11, LueWhsCode12, LueWhsCode13, LueWhsCode14, LueWhsCode15,
                        LueWhsCode16, LueWhsCode17, LueWhsCode18, LueWhsCode19, LueWhsCode20,
                        LueWhsCode21, LueWhsCode22, LueWhsCode23, LueWhsCode24, LueWhsCode25, LueLengthCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd11, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd12, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd13, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd14, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd15, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd16, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd17, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd18, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd19, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd20, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd21, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd22, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd23, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd24, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd25, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd26, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd27, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd28, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd29, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd30, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd31, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd32, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd33, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd34, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd35, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, GrdReturn, new int[] { 1, 2, 3, 4 });

                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueQueueNo, TxtLegalDocVerifyDocNo, TxtVdCode, TxtVdCode2, 
                TxtTTCode, DteUnloadStartDt, TmeUnloadStartTm, DteUnloadEndDt, TmeUnloadEndTm, 
                TxtCreateBy, MeeRemark,
                LueShelf1, LueShelf2, LueShelf3, LueShelf4, LueShelf5, 
                LueShelf6, LueShelf7, LueShelf8, LueShelf9, LueShelf10,
                LueShelf11, LueShelf12, LueShelf13, LueShelf14, LueShelf15,
                LueShelf16, LueShelf17, LueShelf18, LueShelf19, LueShelf20,
                LueShelf21, LueShelf22, LueShelf23, LueShelf24, LueShelf25,
                LueWhsCode1, LueWhsCode2, LueWhsCode3, LueWhsCode4, LueWhsCode5, 
                LueWhsCode6, LueWhsCode7, LueWhsCode8, LueWhsCode9, LueWhsCode10,
                LueWhsCode11, LueWhsCode12, LueWhsCode13, LueWhsCode14, LueWhsCode15,
                LueWhsCode16, LueWhsCode17, LueWhsCode18, LueWhsCode19, LueWhsCode20,
                LueWhsCode21, LueWhsCode22, LueWhsCode23, LueWhsCode24, LueWhsCode25, LueLengthCode
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtTotal, 
                TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, 
                TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15,
                TxtTotal16, TxtTotal17, TxtTotal18, TxtTotal19, TxtTotal20,
                TxtTotal21, TxtTotal22, TxtTotal23, TxtTotal24, TxtTotal25,
            }, 0);

            ChkCancelInd.Checked = false;

            ClearGrd1(new List<iGrid>(){
                Grd1, Grd2, Grd3, Grd4
            });

            ClearGrd2(new List<iGrid>(){
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25,
                Grd26, Grd27, Grd28, Grd29, Grd30,
                Grd31, Grd32, Grd33, Grd34, Grd35
            });
            ClearGrdReturn();
        }

        private void ClearGrdReturn()
        {
            Sm.ClearGrd(GrdReturn, true);
            Sm.SetGrdNumValueZero(ref GrdReturn, 0, new int[] { 2 });
            Sm.FocusGrd(GrdReturn, 0, 0);
        }

        private void ClearGrd1(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
            {
                Sm.ClearGrd(Grd, true);
                Sm.FocusGrd(Grd, 0, 1);
            });
        }

        private void ClearGrd2(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
                {
                    Sm.ClearGrd(Grd, true);
                    Sm.SetGrdNumValueZero(ref Grd, 0, new int[] { 3, 4 });
                    Sm.FocusGrd(Grd, 0, 0);
                });
        }

        private void SetGrd()
        {
            SetGrdEmployee(Grd1, "Grader");
            SetGrdEmployee(Grd2, (mType == 1)?"Asisten Grader":"Tata");
            SetGrdEmployee(Grd3, "Bongkar Dalam");
            SetGrdEmployee(Grd4, "Bongkar Luar");

            SetGrdItem(new List<iGrid> 
                { 
                    Grd11, Grd12, Grd13, Grd14, Grd15,
                    Grd16, Grd17, Grd18, Grd19, Grd20,
                    Grd21, Grd22, Grd23, Grd24, Grd25,
                    Grd26, Grd27, Grd28, Grd29, Grd30,
                    Grd31, Grd32, Grd33, Grd34, Grd35
                });

            SetGrdCanvas();

            //GrdReturn
            GrdReturn.Cols.Count = 5;
            GrdReturn.FrozenArea.ColCount = 2;
            GrdReturn.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    GrdReturn,
                    new string[] 
                        {
                            //0
                            "DNo",

                            //1-4
                            "Panjang", "Jumlah", "Keterangan", "LengthCode"
                        },
                    new int[]{ 0, 130, 130, 250, 100 }
                );
            Sm.GrdFormatDec(GrdReturn, new int[] { 2 }, 0);
            Sm.GrdColInvisible(GrdReturn, new int[] { 4 });
        }

        private void SetGrdEmployee(iGrid Grd, string PosName)
        {
            Grd.Cols.Count = 2;
            Grd.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd,
                    new string[] { "Code", PosName },
                    new int[] { 0, 150 }
                );
            Sm.GrdColReadOnly(Grd, new int[] { 0 });
        }

        private void SetGrdItem(List<iGrid> ListofGrd)
        {
            string
                Label1 = (mType == 1) ? "Panjang" : "Panjang*Tinggi",
                Label2 = (mType == 1) ? "Diameter" : "Lebar";

            ListofGrd.ForEach(Grd =>
            {
                Grd.Cols.Count = 8;
                Grd.FrozenArea.ColCount = 2;
                Grd.ReadOnly = false;
                Sm.GrdHdrWithColWidth(
                        Grd,
                        new string[] 
                        {
                            //0
                            "Item Code",

                            //1-5
                            "Item Name", Label1, Label2, "Jumlah", "UoM", 
 
                            //6-7
                            "Keterangan", "Actual"+Environment.NewLine+"Item Code"
                        },
                        new int[] 
                        { 
                            100, 
                            250, 170, 80, 80, 80, 
                            150, 150
                        }
                    );
                Sm.GrdFormatDec(Grd, new int[] { 3 }, 0);
                Sm.GrdFormatDec(Grd, new int[] { 4 }, 1);
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvRawMaterialFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = 
                DteUnloadStartDt.DateTime = 
                DteUnloadEndDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());

                SetLueQueueNo(ref LueQueueNo, "");

                SetLueEmpCode(ref LueEmpCode1, "PosCodeGrader", "");
                SetLueEmpCode(ref LueEmpCode2, (mType == 1) ? "PosCodeGraderAssistant" : "PosCodeUnloadBalok", "");
                SetLueEmpCode(ref LueEmpCode3, "PosCodeUnloadInside", "");
                SetLueEmpCode(ref LueEmpCode4, "PosCodeUnloadOutside", "");

                SetUserName();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            SetLueEmpCode(ref LueEmpCode1, "PosCodeGrader", "");
            SetLueEmpCode(ref LueEmpCode2, (mType == 1) ? "PosCodeGraderAssistant" : "PosCodeUnloadBalok", "");
            SetLueEmpCode(ref LueEmpCode3, "PosCodeUnloadInside", "");
            SetLueEmpCode(ref LueEmpCode4, "PosCodeUnloadOutside", "");
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            PrintData(TxtDocNo.Text);
        }

        private void PrintData(string DocNo)
        {
            var l = new List<RawMaterial>();
            var ldtl = new List<RawMaterialDtl>();
            var ldtl1 = new List<RawMaterialDtl1>();

            string[] TableName = { "RawMaterial", "RawMaterialDtl", "RawMaterialDtl1" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.CancelInd, ");
            SQL.AppendLine(" A.RawMaterialVerifyDocNo, D.VdName As VdLog, ");
            SQL.AppendLine(" E.VdName As VdBalok, A.DocType, C.QueueNo, C.VehicleRegNo, ");
            SQL.AppendLine("	 Concat(Left(A.UnloadStartTm, 2),':', Right(A.UnloadStartTm, 2)) As LoadStart, ");
            SQL.AppendLine("	 Concat(Left(A.UnLoadEndTm, 2), ':', Right(A.UnloadEndTm, 2)) As LoadEnd, ");
            SQL.AppendLine("	 G.TTName  ");
            SQL.AppendLine(" From TblRecvRawMaterialHdr A ");
            SQL.AppendLine(" Left Join TblRecvRawMaterialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine(" Left Join TblLegalDocVerifyHdr C On A.LegalDocVerifyDocNo = C.DocNo ");
            SQL.AppendLine(" left Join TblVendor D On D.VdCode = C.VdCode1 ");
            SQL.AppendLine(" left Join TblVendor E On E.VdCode = C.VdCode2 ");
            SQL.AppendLine("	 Left Join TblLoadingQueue F On C.QueueNo = F.DocNo And  F.Status = 'O' ");
            SQL.AppendLine("	 Left Join TblTransportType G On F.TTCode = G.TTCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         "DocNo", 
                         "DocDt", 
                         "CancelInd",
                         "RawMaterialVerifyDocNo", 
                         "VdLog",
                         "VdBalok",

                         "DocType",
                         "QueueNo", 
                         "VehicleRegNo", 
                         "LoadStart",
                         "LoadEnd",
 
                         "TTName",                       
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RawMaterial()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            CancelInd = Sm.DrStr(dr, c[2]),
                            RawMaterialVerifyDocNo = Sm.DrStr(dr, c[3]),
                            VdLog = Sm.DrStr(dr, c[4]),
                            VdBalok = Sm.DrStr(dr, c[5]),
                            DocType = Sm.DrStr(dr, c[6]),
                            QueueNo = Sm.DrStr(dr, c[7]),
                            VehicleRegNo = Sm.DrStr(dr, c[8]),
                            LoadStart = Sm.DrStr(dr, c[9]),
                            LoadEnd = Sm.DrStr(dr, c[10]),
                            TTName = Sm.DrStr(dr, c[11]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                string ItCtCode = string.Empty;
                cnDtl.Open();
                cmDtl.Connection = cnDtl;


                if (Sm.GetValue("Select DocType From  TblRecvrawmaterialhdr Where DocNo = '" + TxtDocNo.Text + "'") == "1")
                {
                    ItCtCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual'");

                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf1,  B.ItCode,  C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName,  B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '1' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf2, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '2' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf3, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '3' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf4, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '4' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf5, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '5' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf6, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '6' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf7, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '7' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf8, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '8' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf9, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '9' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf10, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '10' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf11, B.ItCode, C.Diameter,'0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '11' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf12, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '12' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf13, B.ItCode, C.Diameter,'0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '13' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf14, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '14' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf15, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '15' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf16, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '16' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf17, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '17' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf18, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '18' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf19, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '19' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf20, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '20' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf21, B.ItCode, C.Diameter,'0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '21' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf22, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '22' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf23, B.ItCode, C.Diameter,'0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '23' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf24, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '24' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf25, B.ItCode, C.Diameter, '0' As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '25' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                }
                else
                {
                    ItCtCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual2'");

                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf1,  B.ItCode, C.Height As Diameter,  C.Width As Width, C.Length,  B.Qty, C.ItName,  B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '1' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf2, B.ItCode, C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '2' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf3, B.ItCode, C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '3' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf4, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '4' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf5, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '5' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf6, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '6' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf7, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '7' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf8, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '8' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf9, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '9' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf10, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '10' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf11, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '11' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf12, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '12' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf13, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '13' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf14, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '14' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf15, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '15' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf16, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '16' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf17, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '17' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf18, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '18' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf19, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '19' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf20, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '20' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf21, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '21' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf22, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '22' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf23, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '23' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf24, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '24' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                    SQLDtl.AppendLine("Union All ");
                    SQLDtl.AppendLine("Select A.DocNo, A.Shelf25, B.ItCode,  C.Height As Diameter,  C.Width As Width, C.Length, B.Qty, C.ItName, B.DNo ");
                    SQLDtl.AppendLine("From TblRecvRawMaterialHdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvRawMaterialDtl2 B On A.DocNo= B.DOcNo And B.SectionNo = '25' ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo And C.ItCtCode = '" + ItCtCode + "' ");
                }

                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);

                cmDtl.CommandText = SQLDtl.ToString();

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "Shelf1",

                         //1-5
                         "ItCode",
                         "Diameter" ,
                         "Width",
                         "Length",
                         "Qty",

                         "ItName"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new RawMaterialDtl()
                        {
                            Shelf = Sm.DrStr(drDtl, cDtl[0]),
                            ItCode = Sm.DrStr(drDtl, cDtl[1]),
                            Diameter = Sm.DrDec(drDtl, cDtl[2]),
                            Width = Sm.DrDec(drDtl, cDtl[3]),
                            Length = Sm.DrDec(drDtl, cDtl[4]),

                            Qty = Sm.DrDec(drDtl, cDtl[5]),
                            ItName = Sm.DrStr(drDtl, cDtl[6]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Data 2
            var cmDtl1 = new MySqlCommand();
            var SQLDtl1 = new StringBuilder();

            SQLDtl1.AppendLine("Select A.DocNo, B.QueueNo As Queue, B.VehicleRegNo As LicenceNo, D.TTName, ");
            SQLDtl1.AppendLine("(   Select Group_Concat(Distinct T2.EmpName Order By T2.EmpName) ");
            SQLDtl1.AppendLine("    From TblRecvRawMaterialDtl T1, TblEmployee T2 ");
            SQLDtl1.AppendLine("    Where T1.EmpCode=T2.EmpCode ");
            SQLDtl1.AppendLine("    And T1.EmpType='1' ");
            SQLDtl1.AppendLine("    And T1.DocNo=A.DocNo ");
            SQLDtl1.AppendLine(") As Grid, ");
            SQLDtl1.AppendLine("(   Select Group_Concat(Distinct T2.EmpName Order By T2.EmpName) ");
            SQLDtl1.AppendLine("    From TblRecvRawMaterialDtl T1, TblEmployee T2 ");
            SQLDtl1.AppendLine("    Where T1.EmpCode=T2.EmpCode ");
            SQLDtl1.AppendLine("    And T1.EmpType='2' ");
            SQLDtl1.AppendLine("    And T1.DocNo=A.DocNo ");
            SQLDtl1.AppendLine(") As AsGrid, ");
            SQLDtl1.AppendLine("(   Select Group_Concat(Distinct T2.EmpName Order By T2.EmpName) ");
            SQLDtl1.AppendLine("    From TblRecvRawMaterialDtl T1, TblEmployee T2 ");
            SQLDtl1.AppendLine("    Where T1.EmpCode=T2.EmpCode ");
            SQLDtl1.AppendLine("    And T1.EmpType='3' ");
            SQLDtl1.AppendLine("    And T1.DocNo=A.DocNo ");
            SQLDtl1.AppendLine(") As BongDal, ");
            SQLDtl1.AppendLine("(   Select Group_Concat(Distinct T2.EmpName Order By T2.EmpName) ");
            SQLDtl1.AppendLine("    From TblRecvRawMaterialDtl T1, TblEmployee T2 ");
            SQLDtl1.AppendLine("    Where T1.EmpCode=T2.EmpCode ");
            SQLDtl1.AppendLine("    And T1.EmpType='4' ");
            SQLDtl1.AppendLine("    And T1.DocNo=A.DocNo ");
            SQLDtl1.AppendLine(") As BongLu, ");
            SQLDtl1.AppendLine("(   Select Group_Concat(Distinct DLCode Order By DLCode) ");
            SQLDtl1.AppendLine("    From TblLegalDocVerifyDtl ");
            SQLDtl1.AppendLine("    Where DocNo=B.DocNo ");
            SQLDtl1.AppendLine(") As Leg ");
            SQLDtl1.AppendLine("From TblRecvRawMaterialHdr A ");
            SQLDtl1.AppendLine("Inner Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQLDtl1.AppendLine("Inner Join TblLoadingQueue C On B.QueueNo=C.DocNo ");
            SQLDtl1.AppendLine("Left Join TblTransportType D On C.TTCode=D.TTCode ");
            SQLDtl1.AppendLine("Where A.DocNo = @DocNo; ");

            using (var cnDtl1 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl1.Open();
                cmDtl1.Connection = cnDtl1;
                cmDtl1.CommandText = SQLDtl1.ToString();
                Sm.CmParam<String>(ref cmDtl1, "@DocNo", DocNo);
                var drDtl1 = cmDtl1.ExecuteReader();
                var cDtl1 = Sm.GetOrdinal(drDtl1, new string[] 
                        {
                         "Grid",

                         //1-5
                         "AsGrid",
                         "BongDal",
                         "BongLu", 
                         "Leg", 
                         "TTName",
 
                         //6-10 
                         "LicenceNo",
                         "Queue"
                        });
                if (drDtl1.HasRows)
                {
                    while (drDtl1.Read())
                    {
                        ldtl1.Add(new RawMaterialDtl1()
                        {
                            Grid = Sm.DrStr(drDtl1, cDtl1[0]),
                            AsGrid = Sm.DrStr(drDtl1, cDtl1[1]),
                            BongDal = Sm.DrStr(drDtl1, cDtl1[2]),

                            BongLu = Sm.DrStr(drDtl1, cDtl1[3]),
                            Leg = Sm.DrStr(drDtl1, cDtl1[4]),
                            TTName = Sm.DrStr(drDtl1, cDtl1[5]),
                            LicenceNo = Sm.DrStr(drDtl1, cDtl1[6]),
                            Queue = Sm.DrStr(drDtl1, cDtl1[7]),
                        });
                    }
                }
                drDtl1.Close();
            }
            myLists.Add(ldtl1);
            #endregion

            Sm.PrintReport("ReceivingRawMaterial", myLists, TableName, false);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvRawMaterial", "TblRecvRawMaterialHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvRawMaterialHdr(DocNo));
            cml.Add(SaveRecvRawMaterialDtl(DocNo));
            cml.Add(SaveRecvRawMaterialDtl2(DocNo));
            cml.Add(SaveRecvRawMaterialDtl4(DocNo));

            //int DNo = 0;

            //SaveRecvRawMaterialDtl(ref cml, DocNo, ref DNo, "1", Grd1);
            //SaveRecvRawMaterialDtl(ref cml, DocNo, ref DNo, "2", Grd2);
            //SaveRecvRawMaterialDtl(ref cml, DocNo, ref DNo, "3", Grd3);
            //SaveRecvRawMaterialDtl(ref cml, DocNo, ref DNo, "4", Grd4);

            //DNo = 0;
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "1", Grd11);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "2", Grd12);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "3", Grd13);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "4", Grd14);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "5", Grd15);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "6", Grd16);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "7", Grd17);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "8", Grd18);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "9", Grd19);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "10", Grd20);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "11", Grd21);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "12", Grd22);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "13", Grd23);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "14", Grd24);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "15", Grd25);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "16", Grd26);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "17", Grd27);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "18", Grd28);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "19", Grd29);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "20", Grd30);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "21", Grd31);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "22", Grd32);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "23", Grd33);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "24", Grd34);
            //SaveRecvRawMaterialDtl2(ref cml, DocNo, ref DNo, "25", Grd35);

            //for (int r = 0; r < GrdReturn.Rows.Count - 1; r++)
            //{
            //    if (Sm.GetGrdStr(GrdReturn, r, 2).Length > 0)
            //        cml.Add(SaveRecvRawMaterialDtl4(DocNo, r));
            //}

            Sm.ExecCommands(cml);

            if (Sm.StdMsgYN("Print", "") == DialogResult.No) 
                BtnInsertClick(sender, e);
            else
            {
                ShowData(DocNo);
                PrintData(DocNo);
            }
        }

        //private void SaveRecvRawMaterialDtl(ref List<MySqlCommand> cml, string DocNo, ref int DNo, string EmpType, iGrid Grd)
        //{
        //    for (int Row = 0; Row < Grd.Rows.Count; Row++)
        //        if (Sm.GetGrdStr(Grd, Row, 0).Length > 0) cml.Add(SaveRecvRawMaterialDtl(DocNo, ref DNo, EmpType, Grd, Row));
        //}

        //private void SaveRecvRawMaterialDtl2(ref List<MySqlCommand> cml, string DocNo, ref int DNo, string SectionNo, iGrid Grd)
        //{
        //    for (int Row = 0; Row < Grd.Rows.Count; Row++)
        //        if (Sm.GetGrdStr(Grd, Row, 0).Length > 0 && Sm.GetGrdStr(Grd, Row, 1).Length > 0)
        //            cml.Add(SaveRecvRawMaterialDtl2(DocNo, ref DNo, SectionNo, Grd, Row));
        //}

        private MySqlCommand SaveRecvRawMaterialDtl4(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Receiving Raw Material (Dtl4) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < GrdReturn.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(GrdReturn, r, 2).Length > 0)
                {
                    if (Sm.GetGrdDec(GrdReturn, r, 2)!=0)
                    {
                        if (IsFirstOrExisted)
                        {
                            SQL.AppendLine("Insert Into TblRecvRawMaterialDtl4(DocNo, DNo, Length, Qty, Remark, CreateBy, CreateDt) ");
                            SQL.AppendLine("Values ");
                            IsFirstOrExisted = false;
                        }
                        else
                            SQL.AppendLine(", ");
                        SQL.AppendLine(
                            " (@DocNo, @DNo_" + r.ToString() +
                            ", @Length_" + r.ToString() +
                            ", @Qty_" + r.ToString() +
                            ", @Remark_" + r.ToString() +
                            ", @UserCode, @Dt) ");

                        Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                        Sm.CmParam<String>(ref cm, "@Length_" + r.ToString(), Sm.GetGrdStr(GrdReturn, r, 4));
                        Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(GrdReturn, r, 2));
                        Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(GrdReturn, r, 3));
                    }
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveRecvRawMaterialDtl4(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblRecvRawMaterialDtl4(DocNo, DNo, Length, Qty, Remark, CreateBy, CreateDt)");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @Length, @Qty, @Remark,  @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Length", Sm.GetGrdStr(GrdReturn, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(GrdReturn, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(GrdReturn, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueQueueNo, "Nomor antrian") ||
                Sm.IsDteEmpty(DteUnloadStartDt, "Tanggal mulai bongkar") ||
                Sm.IsTmeEmpty(TmeUnloadStartTm, "Waktu mulai bongkar") ||
                Sm.IsDteEmpty(DteUnloadEndDt, "Tanggal selesai bongkar") ||
                Sm.IsTmeEmpty(TmeUnloadEndTm, "Waktu selesai bongkar") ||
                IsUnloadNotValid() ||
                Sm.IsLueEmpty(LueShelf1, "Rak [1]") ||
                Sm.IsLueEmpty(LueWhsCode1, "Gudang [1]") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsLegalDocVerifyDocNoCancelledAlready() || 
                IsLegalDocVerifyDocNoAlreadyFulfilled() ||
                IsItemInfoNotValid(LueShelf1, LueWhsCode1, Grd11, "1") ||
                IsItemInfoNotValid(LueShelf2, LueWhsCode2, Grd12, "2") ||
                IsItemInfoNotValid(LueShelf3, LueWhsCode3, Grd13, "3") ||
                IsItemInfoNotValid(LueShelf4, LueWhsCode4, Grd14, "4") ||
                IsItemInfoNotValid(LueShelf5, LueWhsCode5, Grd15, "5") ||
                IsItemInfoNotValid(LueShelf6, LueWhsCode6, Grd16, "6") ||
                IsItemInfoNotValid(LueShelf7, LueWhsCode7, Grd17, "7") ||
                IsItemInfoNotValid(LueShelf8, LueWhsCode8, Grd18, "8") ||
                IsItemInfoNotValid(LueShelf9, LueWhsCode9, Grd19, "9") ||
                IsItemInfoNotValid(LueShelf10, LueWhsCode10, Grd20, "10") ||
                IsItemInfoNotValid(LueShelf11, LueWhsCode11, Grd21, "11") ||
                IsItemInfoNotValid(LueShelf12, LueWhsCode12, Grd22, "12") ||
                IsItemInfoNotValid(LueShelf13, LueWhsCode13, Grd23, "13") ||
                IsItemInfoNotValid(LueShelf14, LueWhsCode14, Grd24, "14") ||
                IsItemInfoNotValid(LueShelf15, LueWhsCode15, Grd25, "15") ||
                IsItemInfoNotValid(LueShelf16, LueWhsCode16, Grd26, "16") ||
                IsItemInfoNotValid(LueShelf17, LueWhsCode17, Grd27, "17") ||
                IsItemInfoNotValid(LueShelf18, LueWhsCode18, Grd28, "18") ||
                IsItemInfoNotValid(LueShelf19, LueWhsCode19, Grd29, "19") ||
                IsItemInfoNotValid(LueShelf20, LueWhsCode20, Grd30, "20") ||
                IsItemInfoNotValid(LueShelf21, LueWhsCode21, Grd31, "21") ||
                IsItemInfoNotValid(LueShelf22, LueWhsCode22, Grd32, "22") ||
                IsItemInfoNotValid(LueShelf23, LueWhsCode23, Grd33, "23") ||
                IsItemInfoNotValid(LueShelf24, LueWhsCode24, Grd34, "24") ||
                IsItemInfoNotValid(LueShelf25, LueWhsCode25, Grd35, "25") ||
                IsBinNotValid()
                ;
        }

        private bool IsItemInfoNotValid(DXE.LookUpEdit LueShelf, DXE.LookUpEdit LueWhsCode, iGrid Grd, string Number)
        {
            if (Grd.Rows.Count > 1 && 
                (Sm.IsLueEmpty(LueShelf, "Rak# " + Number) || Sm.IsLueEmpty(LueWhsCode, "Gudang# " + Number)) 
                ) 
                return true;

            if (Grd.Rows.Count == 1 &&
                ((Sm.GetLue(LueShelf)).Length != 0 || (Sm.GetLue(LueWhsCode)).Length != 0)
                )
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Rak : " + Number + Environment.NewLine +
                    "Gudang : " + Number + Environment.NewLine + Environment.NewLine + 
                    "Data item masih kosong.");
                return true;
            }

            return false;
        }

        private bool IsBinNotValid()
        { 
            string SQL = string.Empty;

            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf1));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf2));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf3));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf4));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf5));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf6));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf7));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf8));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf9));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf10));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf11));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf12));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf13));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf14));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf15));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf16));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf17));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf18));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf19));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf20));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf21));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf22));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf23));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf24));
            GetSelectedBin(ref SQL, Sm.GetLue(LueShelf25));

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Bin From TblBin " +
                    "Where (Status Not In (1, 2) Or ActInd='N') " +
                    "And Locate(Concat('##', Bin, '##'), @Param)>0 " +
                    "Order By Bin Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@Param", SQL);

            var Bin = Sm.GetValue(cm);

            if (Bin.Length>0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Shelf : " + Bin + Environment.NewLine +
                    "Anda tidak dapat menggunakan rak tersebut.");
                return true;
            }

            return false;
        }

        private void GetSelectedBin(ref string SQL, string Bin)
        {
            if (Bin.Length > 0) SQL += "##" + Bin + "##";
        }

        private bool IsUnloadNotValid()
        {
            string 
                UnloadStart = Sm.GetDte(DteUnloadStartDt) + Sm.GetTme(TmeUnloadStartTm),
                UnloadEnd = Sm.GetDte(DteUnloadEndDt) + Sm.GetTme(TmeUnloadEndTm);

            if (UnloadStart.Length != 0 && UnloadEnd.Length != 0)
            {
                if (decimal.Parse(UnloadStart) > decimal.Parse(UnloadEnd))
                {
                    Sm.StdMsg(mMsgType.Warning, "Tanggal dan waktu bongkar salah.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 grader.");
                return true;
            }

            if (Grd11.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (IsEmployeeGrdValueNotValid(Grd1, "Grader") ||
                IsEmployeeGrdValueNotValid(Grd2, "Asisten grader") ||
                IsEmployeeGrdValueNotValid(Grd3, "Karyawan bongkar dalam") ||
                IsEmployeeGrdValueNotValid(Grd4, "Karyawan bongkar luar") ||
                //IsEmployeeGrdValueNotValid2() ||
                IsItemGrdValueNotValid(Grd11, "1") ||
                IsItemGrdValueNotValid(Grd12, "2") ||
                IsItemGrdValueNotValid(Grd13, "3") ||
                IsItemGrdValueNotValid(Grd14, "4") ||
                IsItemGrdValueNotValid(Grd15, "5") ||
                IsItemGrdValueNotValid(Grd16, "6") ||
                IsItemGrdValueNotValid(Grd17, "7") ||
                IsItemGrdValueNotValid(Grd18, "8") ||
                IsItemGrdValueNotValid(Grd19, "9") ||
                IsItemGrdValueNotValid(Grd20, "10") ||
                IsItemGrdValueNotValid(Grd21, "11") ||
                IsItemGrdValueNotValid(Grd22, "12") ||
                IsItemGrdValueNotValid(Grd23, "13") ||
                IsItemGrdValueNotValid(Grd24, "14") ||
                IsItemGrdValueNotValid(Grd25, "15") ||
                IsItemGrdValueNotValid(Grd26, "16") ||
                IsItemGrdValueNotValid(Grd27, "17") ||
                IsItemGrdValueNotValid(Grd28, "18") ||
                IsItemGrdValueNotValid(Grd29, "19") ||
                IsItemGrdValueNotValid(Grd30, "20") ||
                IsItemGrdValueNotValid(Grd31, "21") ||
                IsItemGrdValueNotValid(Grd32, "22") ||
                IsItemGrdValueNotValid(Grd33, "23") ||
                IsItemGrdValueNotValid(Grd34, "24") ||
                IsItemGrdValueNotValid(Grd35, "25") ||
                IsReturnGrdValueNotValid()
                ) 
                
                return true;
            
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (IsGrdExceedMaxRecords(Grd1, "Grader") ||
                IsGrdExceedMaxRecords(Grd2, "Asisten grader") ||
                IsGrdExceedMaxRecords(Grd3, "Karyawan bongkar dalam") ||
                IsGrdExceedMaxRecords(Grd4, "Karyawan bongkar luar") ||
                IsGrdExceedMaxRecords(Grd11, "Bahan baku #1") ||
                IsGrdExceedMaxRecords(Grd12, "Bahan baku #2") ||
                IsGrdExceedMaxRecords(Grd13, "Bahan baku #3") ||
                IsGrdExceedMaxRecords(Grd14, "Bahan baku #4") ||
                IsGrdExceedMaxRecords(Grd15, "Bahan baku #5") ||
                IsGrdExceedMaxRecords(Grd16, "Bahan baku #6") ||
                IsGrdExceedMaxRecords(Grd17, "Bahan baku #7") ||
                IsGrdExceedMaxRecords(Grd18, "Bahan baku #8") ||
                IsGrdExceedMaxRecords(Grd19, "Bahan baku #9") ||
                IsGrdExceedMaxRecords(Grd20, "Bahan baku #10") ||
                IsGrdExceedMaxRecords(Grd21, "Bahan baku #11") ||
                IsGrdExceedMaxRecords(Grd22, "Bahan baku #12") ||
                IsGrdExceedMaxRecords(Grd23, "Bahan baku #13") ||
                IsGrdExceedMaxRecords(Grd24, "Bahan baku #14") ||
                IsGrdExceedMaxRecords(Grd25, "Bahan baku #15") ||
                IsGrdExceedMaxRecords(Grd26, "Bahan baku #16") ||
                IsGrdExceedMaxRecords(Grd27, "Bahan baku #17") ||
                IsGrdExceedMaxRecords(Grd28, "Bahan baku #18") ||
                IsGrdExceedMaxRecords(Grd29, "Bahan baku #19") ||
                IsGrdExceedMaxRecords(Grd30, "Bahan baku #20") ||
                IsGrdExceedMaxRecords(Grd31, "Bahan baku #21") ||
                IsGrdExceedMaxRecords(Grd32, "Bahan baku #22") ||
                IsGrdExceedMaxRecords(Grd33, "Bahan baku #23") ||
                IsGrdExceedMaxRecords(Grd34, "Bahan baku #24") ||
                IsGrdExceedMaxRecords(Grd35, "Bahan baku #25")
                ) return true;
            
            return false;
        }

        private bool IsGrdExceedMaxRecords(iGrid Grd, string Msg)
        {
            if (Grd.Rows.Count > 999)
            {
                Sm.StdMsg(mMsgType.Warning,
                    Msg + " data entered (" + (Grd.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsEmployeeGrdValueNotValid(iGrid Grd, string Msg)
        {
            if (Grd.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd, Row, 0, false, Msg + " is empty.")) return true;
            }
            return false;
        }

        //private bool IsEmployeeGrdValueNotValid2()
        //{
        //    if (Grd3.Rows.Count > 1 && Grd4.Rows.Count > 1)
        //    {
        //        Sm.StdMsg(mMsgType.Warning,
        //            "Tidak boleh ada bongkar luar dan bongkar dalam pada 1 nomor antrian."); 
        //        return true;
        //    }
        //    return false;
        //}

        private bool IsReturnGrdValueNotValid()
        {
            if (GrdReturn.Rows.Count > 1)
            {
                for (int Row = 0; Row < GrdReturn.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(GrdReturn, Row, 2).Length > 0)
                    {
                        if (Sm.IsGrdValueEmpty(GrdReturn, Row, 1, false, "Panjang bahan baku yg dikembalikan harus lebih besar dari 0.")) return true;
                        if (Sm.IsGrdValueEmpty(GrdReturn, Row, 2, true, "Jumlah bahan baku yg dikembalikan harus lebih besar dari 0.")) return true;
                        if (Sm.IsGrdValueEmpty(GrdReturn, Row, 3, false, "Keterangan bahan baku yg dikembalikan tidak boleh kosong.")) return true;
                    }
                }
            }
            return false;
        }

        private bool IsItemGrdValueNotValid(iGrid Grd, string Msg)
        {
            if (Grd.Rows.Count > 1)
            {
                SetActualItCode(Grd);
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd, Row, 0, false, "Item #" + Msg + " is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, Row, 3, true, "Bahan baku #" + Msg + Environment.NewLine + "Diameter should be greater than 0.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, Row, 4, true, "Bahan baku #" + Msg + Environment.NewLine + "Quantity should be greater than 0.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd, Row, 7, false,
                        "Actual Item #" + Msg + " is empty." + Environment.NewLine +
                        "Please contact Purchaser."
                        )) return true;
                }
            }
            return false;
        }

        private bool IsLegalDocVerifyDocNoCancelledAlready()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    "Select DocNo From TblLegalDocVerifyHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtLegalDocVerifyDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Nomor verifikasi legalitas dokumen telah dibatalkan.");
                return true;
            }

            return false;
        }

        private bool IsLegalDocVerifyDocNoAlreadyFulfilled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblLegalDocVerifyHdr " +
                    "Where (IfNull(ProcessInd1, 'O')='F' Or IfNull(ProcessInd2, 'O')='F') " +
                    "And CancelInd='N' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtLegalDocVerifyDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Dokumen legalitas telah selesai diproses.");
                return true;
            }

            return false;
        }

        private MySqlCommand SaveRecvRawMaterialHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Receiving Raw Material (Hdr) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblRecvRawMaterialHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, ProcessInd, DocType, LegalDocVerifyDocNo, UnloadStartDt, UnloadStartTm, UnloadEndDt, UnloadEndTm, ");
            SQL.AppendLine("Shelf1, Shelf2, Shelf3, Shelf4, Shelf5, Shelf6, Shelf7, Shelf8, Shelf9, Shelf10,Shelf11, Shelf12, Shelf13, Shelf14, Shelf15, ");
            SQL.AppendLine("Shelf16, Shelf17, Shelf18, Shelf19, Shelf20,Shelf21, Shelf22, Shelf23, Shelf24, Shelf25, ");
            SQL.AppendLine("WhsCode1, WhsCode2, WhsCode3, WhsCode4, WhsCode5, WhsCode6, WhsCode7, WhsCode8, WhsCode9, WhsCode10,WhsCode11, WhsCode12, WhsCode13, WhsCode14, WhsCode15, ");
            SQL.AppendLine("WhsCode16, WhsCode17, WhsCode18, WhsCode19, WhsCode20,WhsCode21, WhsCode22, WhsCode23, WhsCode24, WhsCode25, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @DocType, @LegalDocVerifyDocNo, @UnloadStartDt, @UnloadStartTm, @UnloadEndDt, @UnloadEndTm, ");
            SQL.AppendLine("@Shelf1, @Shelf2, @Shelf3, @Shelf4, @Shelf5, @Shelf6, @Shelf7, @Shelf8, @Shelf9, @Shelf10, @Shelf11, @Shelf12, @Shelf13, @Shelf14, @Shelf15, ");
            SQL.AppendLine("@Shelf16, @Shelf17, @Shelf18, @Shelf19, @Shelf20, @Shelf21, @Shelf22, @Shelf23, @Shelf24, @Shelf25, ");
            SQL.AppendLine("@WhsCode1, @WhsCode2, @WhsCode3, @WhsCode4, @WhsCode5, @WhsCode6, @WhsCode7, @WhsCode8, @WhsCode9, @WhsCode10, @WhsCode11, @WhsCode12, @WhsCode13, @WhsCode14, @WhsCode15, ");
            SQL.AppendLine("@WhsCode16, @WhsCode17, @WhsCode18, @WhsCode19, @WhsCode20, @WhsCode21, @WhsCode22, @WhsCode23, @WhsCode24, @WhsCode25, ");
            SQL.AppendLine("@Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
            SQL.AppendLine("    ProcessInd1='P', LastUpBy=@UserCode, LastUpDt=@Dt ");
            SQL.AppendLine("Where DocNo=@LegalDocVerifyDocNo; ");

            SQL.AppendLine("Update TblBin Set Status='2' Where Bin In ( ");

            for(int Index=1;Index<=24;Index++)
                SQL.AppendLine("Select IfNull(@Shelf" +Index.ToString()+ ", 'XXX') As Bin Union All ");

            SQL.AppendLine("Select IfNull(@Shelf25, 'XXX') As Bin);");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocType", mType.ToString());
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParamDt(ref cm, "@UnloadStartDt", Sm.GetDte(DteUnloadStartDt));
            Sm.CmParam<String>(ref cm, "@UnloadStartTm", Sm.GetTme(TmeUnloadStartTm));
            Sm.CmParamDt(ref cm, "@UnloadEndDt", Sm.GetDte(DteUnloadEndDt));
            Sm.CmParam<String>(ref cm, "@UnloadEndTm", Sm.GetTme(TmeUnloadEndTm));
            Sm.CmParam<String>(ref cm, "@Shelf1", Sm.GetLue(LueShelf1));
            Sm.CmParam<String>(ref cm, "@Shelf2", Sm.GetLue(LueShelf2));
            Sm.CmParam<String>(ref cm, "@Shelf3", Sm.GetLue(LueShelf3));
            Sm.CmParam<String>(ref cm, "@Shelf4", Sm.GetLue(LueShelf4));
            Sm.CmParam<String>(ref cm, "@Shelf5", Sm.GetLue(LueShelf5));
            Sm.CmParam<String>(ref cm, "@Shelf6", Sm.GetLue(LueShelf6));
            Sm.CmParam<String>(ref cm, "@Shelf7", Sm.GetLue(LueShelf7));
            Sm.CmParam<String>(ref cm, "@Shelf8", Sm.GetLue(LueShelf8));
            Sm.CmParam<String>(ref cm, "@Shelf9", Sm.GetLue(LueShelf9));
            Sm.CmParam<String>(ref cm, "@Shelf10", Sm.GetLue(LueShelf10));
            Sm.CmParam<String>(ref cm, "@Shelf11", Sm.GetLue(LueShelf11));
            Sm.CmParam<String>(ref cm, "@Shelf12", Sm.GetLue(LueShelf12));
            Sm.CmParam<String>(ref cm, "@Shelf13", Sm.GetLue(LueShelf13));
            Sm.CmParam<String>(ref cm, "@Shelf14", Sm.GetLue(LueShelf14));
            Sm.CmParam<String>(ref cm, "@Shelf15", Sm.GetLue(LueShelf15));
            Sm.CmParam<String>(ref cm, "@Shelf16", Sm.GetLue(LueShelf16));
            Sm.CmParam<String>(ref cm, "@Shelf17", Sm.GetLue(LueShelf17));
            Sm.CmParam<String>(ref cm, "@Shelf18", Sm.GetLue(LueShelf18));
            Sm.CmParam<String>(ref cm, "@Shelf19", Sm.GetLue(LueShelf19));
            Sm.CmParam<String>(ref cm, "@Shelf20", Sm.GetLue(LueShelf20));
            Sm.CmParam<String>(ref cm, "@Shelf21", Sm.GetLue(LueShelf21));
            Sm.CmParam<String>(ref cm, "@Shelf22", Sm.GetLue(LueShelf22));
            Sm.CmParam<String>(ref cm, "@Shelf23", Sm.GetLue(LueShelf23));
            Sm.CmParam<String>(ref cm, "@Shelf24", Sm.GetLue(LueShelf24));
            Sm.CmParam<String>(ref cm, "@Shelf25", Sm.GetLue(LueShelf25));
            Sm.CmParam<String>(ref cm, "@WhsCode1", Sm.GetLue(LueWhsCode1));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@WhsCode3", Sm.GetLue(LueWhsCode3));
            Sm.CmParam<String>(ref cm, "@WhsCode4", Sm.GetLue(LueWhsCode4));
            Sm.CmParam<String>(ref cm, "@WhsCode5", Sm.GetLue(LueWhsCode5));
            Sm.CmParam<String>(ref cm, "@WhsCode6", Sm.GetLue(LueWhsCode6));
            Sm.CmParam<String>(ref cm, "@WhsCode7", Sm.GetLue(LueWhsCode7));
            Sm.CmParam<String>(ref cm, "@WhsCode8", Sm.GetLue(LueWhsCode8));
            Sm.CmParam<String>(ref cm, "@WhsCode9", Sm.GetLue(LueWhsCode9));
            Sm.CmParam<String>(ref cm, "@WhsCode10", Sm.GetLue(LueWhsCode10));
            Sm.CmParam<String>(ref cm, "@WhsCode11", Sm.GetLue(LueWhsCode11));
            Sm.CmParam<String>(ref cm, "@WhsCode12", Sm.GetLue(LueWhsCode12));
            Sm.CmParam<String>(ref cm, "@WhsCode13", Sm.GetLue(LueWhsCode13));
            Sm.CmParam<String>(ref cm, "@WhsCode14", Sm.GetLue(LueWhsCode14));
            Sm.CmParam<String>(ref cm, "@WhsCode15", Sm.GetLue(LueWhsCode15));
            Sm.CmParam<String>(ref cm, "@WhsCode16", Sm.GetLue(LueWhsCode16));
            Sm.CmParam<String>(ref cm, "@WhsCode17", Sm.GetLue(LueWhsCode17));
            Sm.CmParam<String>(ref cm, "@WhsCode18", Sm.GetLue(LueWhsCode18));
            Sm.CmParam<String>(ref cm, "@WhsCode19", Sm.GetLue(LueWhsCode19));
            Sm.CmParam<String>(ref cm, "@WhsCode20", Sm.GetLue(LueWhsCode20));
            Sm.CmParam<String>(ref cm, "@WhsCode21", Sm.GetLue(LueWhsCode21));
            Sm.CmParam<String>(ref cm, "@WhsCode22", Sm.GetLue(LueWhsCode22));
            Sm.CmParam<String>(ref cm, "@WhsCode23", Sm.GetLue(LueWhsCode23));
            Sm.CmParam<String>(ref cm, "@WhsCode24", Sm.GetLue(LueWhsCode24));
            Sm.CmParam<String>(ref cm, "@WhsCode25", Sm.GetLue(LueWhsCode25));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvRawMaterialDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            int DNo = 0;

            SaveRecvRawMaterialDtl(ref SQL, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd1, "1");
            SaveRecvRawMaterialDtl(ref SQL, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd2, "2");
            SaveRecvRawMaterialDtl(ref SQL, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd3, "3");
            SaveRecvRawMaterialDtl(ref SQL, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd4, "4");

            if (!IsFirstOrExisted) 
            {
                cm.CommandText =
                    "/* Receiving Raw Material (Dtl) */ " +
                    "Set @Dt:=CurrentDateTime(); " +
                    "Insert Into TblRecvRawMaterialDtl(DocNo, DNo, EmpType, EmpCode, CreateBy, CreateDt) " +
                    "Values " + SQL.ToString() + ";";
            } 
            else
                cm.CommandText = ";";

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private void SaveRecvRawMaterialDtl(
            ref StringBuilder SQL, 
            ref MySqlCommand cm, 
            ref bool IsFirstOrExisted, 
            ref int DNo,
            ref iGrid Grd,
            string EmpType)
        {
            for (int r = 0; r < Grd.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd, r, 0).Length > 0)
                {
                    if (IsFirstOrExisted)
                        IsFirstOrExisted = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_"+EmpType+"_" + r.ToString() +
                        ", @EmpType_" + EmpType + "_" + r.ToString() +
                        ", @EmpCode_" + EmpType + "_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    DNo++;
                    Sm.CmParam<String>(ref cm, "@DNo_" + EmpType + "_" + r.ToString(), Sm.Right("000" + DNo.ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@EmpType_" + EmpType + "_" + r.ToString(), EmpType);
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + EmpType + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 0));
                }
            }
        }


        //private MySqlCommand SaveRecvRawMaterialDtl(string DocNo, ref int DNo, string EmpType, iGrid Grd, int Row)
        //{
        //    DNo += 1;
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblRecvRawMaterialDtl(DocNo, DNo, EmpType, EmpCode, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @EmpType, @EmpCode, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@EmpType", EmpType);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd, Row, 0));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveRecvRawMaterialDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            int DNo = 0;

            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd11, "1");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd12, "2");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd13, "3");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd14, "4");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd15, "5");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd16, "6");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd17, "7");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd18, "8");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd19, "9");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd20, "10");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd21, "11");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd22, "12");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd23, "13");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd24, "14");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd25, "15");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd26, "16");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd27, "17");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd28, "18");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd29, "19");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd30, "20");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd31, "21");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd32, "22");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd33, "23");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd34, "24");
            SaveRecvRawMaterialDtl2(ref SQL, ref SQL2, ref cm, ref IsFirstOrExisted, ref DNo, ref Grd35, "25");

            if (!IsFirstOrExisted)
            {
                cm.CommandText =
                    "/* Receiving Raw Material (Dtl2+3) */ " +
                    "Set @Dt:=CurrentDateTime(); " +
                    "Insert Into TblRecvRawMaterialDtl2(DocNo, DNo, SectionNo, ItCode, Diameter, Qty, CreateBy, CreateDt) " +
                    "Values " + SQL.ToString() + ";" +
                    "Insert Into TblRecvRawMaterialDtl3(DocNo, DNo, SectionNo, ItCode, Diameter, Qty, CreateBy, CreateDt) " +
                    "Values " + SQL2.ToString() + ";";
            }
            else
                cm.CommandText = ";";

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private void SaveRecvRawMaterialDtl2(
            ref StringBuilder SQL,
            ref StringBuilder SQL2,
            ref MySqlCommand cm,
            ref bool IsFirstOrExisted,
            ref int DNo,
            ref iGrid Grd,
            string SectionNo)
        {
            for (int r = 0; r < Grd.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd, r, 0).Length > 0 && Sm.GetGrdStr(Grd, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                        IsFirstOrExisted = false;
                    else
                    {
                        SQL.AppendLine(", ");
                        SQL2.AppendLine(", ");
                    }
                        
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + SectionNo + "_" + r.ToString() +
                        ", @SectionNo_" + SectionNo + "_" + r.ToString() +
                        ", @ItCodeActual_" + SectionNo + "_" + r.ToString() +
                        ", @Diameter_" + SectionNo + "_" + r.ToString() +
                        ", @Qty_" + SectionNo + "_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    SQL2.AppendLine(
                        " (@DocNo, @DNo_" + SectionNo + "_" + r.ToString() +
                        ", @SectionNo_" + SectionNo + "_" + r.ToString() +
                        ", @ItCode_" + SectionNo + "_" + r.ToString() +
                        ", @Diameter_" + SectionNo + "_" + r.ToString() +
                        ", @Qty_" + SectionNo + "_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    DNo++;
                    Sm.CmParam<String>(ref cm, "@DNo_" + SectionNo + "_" + r.ToString(), Sm.Right("000" + DNo.ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@SectionNo_" + SectionNo + "_" + r.ToString(), SectionNo);
                    Sm.CmParam<String>(ref cm, "@ItCode_" + SectionNo + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 0));
                    Sm.CmParam<String>(ref cm, "@ItCodeActual_" + SectionNo + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@Diameter_" + SectionNo + "_" + r.ToString(), Sm.GetGrdDec(Grd, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + SectionNo + "_" + r.ToString(), Sm.GetGrdDec(Grd, r, 4));
                }
            }
        }

        //private MySqlCommand SaveRecvRawMaterialDtl2(string DocNo, ref int DNo, string SectionNo, iGrid Grd, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    DNo += 1;

        //    SQL.AppendLine("Insert Into TblRecvRawMaterialDtl2 ");
        //    SQL.AppendLine("(DocNo, DNo, SectionNo, ItCode, Diameter, Qty, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @SectionNo, @ItCodeActual, @Diameter, @Qty, @CreateBy, CurrentDateTime()); ");

        //    SQL.AppendLine("Insert Into TblRecvRawMaterialDtl3 ");
        //    SQL.AppendLine("(DocNo, DNo, SectionNo, ItCode, Diameter, Qty, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @SectionNo, @ItCode, @Diameter, @Qty, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd, Row, 0));
        //    Sm.CmParam<String>(ref cm, "@ItCodeActual", Sm.GetGrdStr(Grd, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@Diameter", Sm.GetGrdDec(Grd, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}
        
        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditRecvRawMaterialHdr());
            cml.Add(SaveRecvRawMaterialDtl(TxtDocNo.Text));
            cml.Add(SaveRecvRawMaterialDtl2(TxtDocNo.Text));
            cml.Add(SaveRecvRawMaterialDtl4(TxtDocNo.Text));

            //int DNo = 0;

            //SaveRecvRawMaterialDtl(ref cml, TxtDocNo.Text, ref DNo, "1", Grd1);
            //SaveRecvRawMaterialDtl(ref cml, TxtDocNo.Text, ref DNo, "2", Grd2);
            //SaveRecvRawMaterialDtl(ref cml, TxtDocNo.Text, ref DNo, "3", Grd3);
            //SaveRecvRawMaterialDtl(ref cml, TxtDocNo.Text, ref DNo, "4", Grd4);

            //DNo = 0;
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "1", Grd11);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "2", Grd12);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "3", Grd13);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "4", Grd14);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "5", Grd15);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "6", Grd16);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "7", Grd17);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "8", Grd18);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "9", Grd19);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "10", Grd20);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "11", Grd21);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "12", Grd22);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "13", Grd23);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "14", Grd24);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "15", Grd25);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "16", Grd26);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "17", Grd27);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "18", Grd28);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "19", Grd29);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "20", Grd30);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "21", Grd31);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "22", Grd32);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "23", Grd33);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "24", Grd34);
            //SaveRecvRawMaterialDtl2(ref cml, TxtDocNo.Text, ref DNo, "25", Grd35);

            //for (int r = 0; r < GrdReturn.Rows.Count - 1; r++)
            //{
            //    if (Sm.GetGrdStr(GrdReturn, r, 2).Length > 0)
            //        cml.Add(SaveRecvRawMaterialDtl4(TxtDocNo.Text, r));
            //}

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document number", false) ||
                IsDataCancelledAlready() ||
                Sm.IsLueEmpty(LueShelf1, "Rak [1]") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsLegalDocVerifyDocNoCancelledAlready() ||
                IsLegalDocVerifyDocNoAlreadyFulfilled() ||
                IsItemInfoNotValid(LueShelf1, LueWhsCode1, Grd11, "1") ||
                IsItemInfoNotValid(LueShelf2, LueWhsCode2, Grd12, "2") ||
                IsItemInfoNotValid(LueShelf3, LueWhsCode3, Grd13, "3") ||
                IsItemInfoNotValid(LueShelf4, LueWhsCode4, Grd14, "4") ||
                IsItemInfoNotValid(LueShelf5, LueWhsCode5, Grd15, "5") ||
                IsItemInfoNotValid(LueShelf6, LueWhsCode6, Grd16, "6") ||
                IsItemInfoNotValid(LueShelf7, LueWhsCode7, Grd17, "7") ||
                IsItemInfoNotValid(LueShelf8, LueWhsCode8, Grd18, "8") ||
                IsItemInfoNotValid(LueShelf9, LueWhsCode9, Grd19, "9") ||
                IsItemInfoNotValid(LueShelf10, LueWhsCode10, Grd20, "10") ||
                IsItemInfoNotValid(LueShelf11, LueWhsCode11, Grd21, "11") ||
                IsItemInfoNotValid(LueShelf12, LueWhsCode12, Grd22, "12") ||
                IsItemInfoNotValid(LueShelf13, LueWhsCode13, Grd23, "13") ||
                IsItemInfoNotValid(LueShelf14, LueWhsCode14, Grd24, "14") ||
                IsItemInfoNotValid(LueShelf15, LueWhsCode15, Grd25, "15") ||
                IsItemInfoNotValid(LueShelf16, LueWhsCode16, Grd26, "16") ||
                IsItemInfoNotValid(LueShelf17, LueWhsCode17, Grd27, "17") ||
                IsItemInfoNotValid(LueShelf18, LueWhsCode18, Grd28, "18") ||
                IsItemInfoNotValid(LueShelf19, LueWhsCode19, Grd29, "19") ||
                IsItemInfoNotValid(LueShelf20, LueWhsCode20, Grd30, "20") ||
                IsItemInfoNotValid(LueShelf21, LueWhsCode21, Grd31, "21") ||
                IsItemInfoNotValid(LueShelf22, LueWhsCode22, Grd32, "22") ||
                IsItemInfoNotValid(LueShelf23, LueWhsCode23, Grd33, "23") ||
                IsItemInfoNotValid(LueShelf24, LueWhsCode24, Grd34, "24") ||
                IsItemInfoNotValid(LueShelf25, LueWhsCode25, Grd35, "25") ||
                IsBinNotValid()
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblRecvRawMaterialHdr ");
            SQL.AppendLine("Where CancelInd='Y' ");
            SQL.AppendLine("And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Dokumen ini telah dibatalkan.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditRecvRawMaterialHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvRawMaterialHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, Remark=@Remark, ");
            SQL.AppendLine("    Shelf1=@Shelf1, Shelf2=@Shelf2, Shelf3=@Shelf3, Shelf4=@Shelf4, Shelf5=@Shelf5, ");
            SQL.AppendLine("    Shelf6=@Shelf6, Shelf7=@Shelf7, Shelf8=@Shelf8, Shelf9=@Shelf9, Shelf10=@Shelf10, ");
            SQL.AppendLine("    Shelf11=@Shelf11, Shelf12=@Shelf12, Shelf13=@Shelf13, Shelf14=@Shelf14, Shelf15=@Shelf15, ");
            SQL.AppendLine("    Shelf16=@Shelf16, Shelf17=@Shelf17, Shelf18=@Shelf18, Shelf19=@Shelf19, Shelf20=@Shelf20, ");
            SQL.AppendLine("    Shelf21=@Shelf21, Shelf22=@Shelf22, Shelf23=@Shelf23, Shelf24=@Shelf24, Shelf25=@Shelf25, ");
            SQL.AppendLine("    WhsCode1=@WhsCode1, WhsCode2=@WhsCode2, WhsCode3=@WhsCode3, WhsCode4=@WhsCode4, WhsCode5=@WhsCode5, ");
            SQL.AppendLine("    WhsCode6=@WhsCode6, WhsCode7=@WhsCode7, WhsCode8=@WhsCode8, WhsCode9=@WhsCode9, WhsCode10=@WhsCode10, ");
            SQL.AppendLine("    WhsCode11=@WhsCode11, WhsCode12=@WhsCode12, WhsCode13=@WhsCode13, WhsCode14=@WhsCode14, WhsCode15=@WhsCode15, ");
            SQL.AppendLine("    WhsCode16=@WhsCode16, WhsCode17=@WhsCode17, WhsCode18=@WhsCode18, WhsCode19=@WhsCode19, WhsCode20=@WhsCode20, ");
            SQL.AppendLine("    WhsCode21=@WhsCode21, WhsCode22=@WhsCode22, WhsCode23=@WhsCode23, WhsCode24=@WhsCode24, WhsCode25=@WhsCode25, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblBin Set Status='2' Where Bin In ( ");

            for (int Index = 1; Index <= 24; Index++)
                SQL.AppendLine("Select IfNull(@Shelf" + Index.ToString() + ", 'XXX') As Bin Union All ");

            SQL.AppendLine("Select IfNull(@Shelf25, 'XXX') As Bin);");

            SQL.AppendLine("Delete From TblRecvRawMaterialDtl Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblRecvRawMaterialDtl2 Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblRecvRawMaterialDtl3 Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblRecvRawMaterialDtl4 Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
            SQL.AppendLine("    ProcessInd1= ");
            SQL.AppendLine("        Case When Exists( ");
            SQL.AppendLine("            Select DocNo From TblRecvRawMaterialHdr ");
            SQL.AppendLine("            Where CancelInd='N' ");
            SQL.AppendLine("            And LegalDocVerifyDocNo=@LegalDocVerifyDocNo Limit 1 ");
            SQL.AppendLine("        ) Then 'P' Else 'O' End, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@LegalDocVerifyDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Shelf1", Sm.GetLue(LueShelf1));
            Sm.CmParam<String>(ref cm, "@Shelf2", Sm.GetLue(LueShelf2));
            Sm.CmParam<String>(ref cm, "@Shelf3", Sm.GetLue(LueShelf3));
            Sm.CmParam<String>(ref cm, "@Shelf4", Sm.GetLue(LueShelf4));
            Sm.CmParam<String>(ref cm, "@Shelf5", Sm.GetLue(LueShelf5));
            Sm.CmParam<String>(ref cm, "@Shelf6", Sm.GetLue(LueShelf6));
            Sm.CmParam<String>(ref cm, "@Shelf7", Sm.GetLue(LueShelf7));
            Sm.CmParam<String>(ref cm, "@Shelf8", Sm.GetLue(LueShelf8));
            Sm.CmParam<String>(ref cm, "@Shelf9", Sm.GetLue(LueShelf9));
            Sm.CmParam<String>(ref cm, "@Shelf10", Sm.GetLue(LueShelf10));
            Sm.CmParam<String>(ref cm, "@Shelf11", Sm.GetLue(LueShelf11));
            Sm.CmParam<String>(ref cm, "@Shelf12", Sm.GetLue(LueShelf12));
            Sm.CmParam<String>(ref cm, "@Shelf13", Sm.GetLue(LueShelf13));
            Sm.CmParam<String>(ref cm, "@Shelf14", Sm.GetLue(LueShelf14));
            Sm.CmParam<String>(ref cm, "@Shelf15", Sm.GetLue(LueShelf15));
            Sm.CmParam<String>(ref cm, "@Shelf16", Sm.GetLue(LueShelf16));
            Sm.CmParam<String>(ref cm, "@Shelf17", Sm.GetLue(LueShelf17));
            Sm.CmParam<String>(ref cm, "@Shelf18", Sm.GetLue(LueShelf18));
            Sm.CmParam<String>(ref cm, "@Shelf19", Sm.GetLue(LueShelf19));
            Sm.CmParam<String>(ref cm, "@Shelf20", Sm.GetLue(LueShelf20));
            Sm.CmParam<String>(ref cm, "@Shelf21", Sm.GetLue(LueShelf21));
            Sm.CmParam<String>(ref cm, "@Shelf22", Sm.GetLue(LueShelf22));
            Sm.CmParam<String>(ref cm, "@Shelf23", Sm.GetLue(LueShelf23));
            Sm.CmParam<String>(ref cm, "@Shelf24", Sm.GetLue(LueShelf24));
            Sm.CmParam<String>(ref cm, "@Shelf25", Sm.GetLue(LueShelf25));
            Sm.CmParam<String>(ref cm, "@WhsCode1", Sm.GetLue(LueWhsCode1));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@WhsCode3", Sm.GetLue(LueWhsCode3));
            Sm.CmParam<String>(ref cm, "@WhsCode4", Sm.GetLue(LueWhsCode4));
            Sm.CmParam<String>(ref cm, "@WhsCode5", Sm.GetLue(LueWhsCode5));
            Sm.CmParam<String>(ref cm, "@WhsCode6", Sm.GetLue(LueWhsCode6));
            Sm.CmParam<String>(ref cm, "@WhsCode7", Sm.GetLue(LueWhsCode7));
            Sm.CmParam<String>(ref cm, "@WhsCode8", Sm.GetLue(LueWhsCode8));
            Sm.CmParam<String>(ref cm, "@WhsCode9", Sm.GetLue(LueWhsCode9));
            Sm.CmParam<String>(ref cm, "@WhsCode10", Sm.GetLue(LueWhsCode10));
            Sm.CmParam<String>(ref cm, "@WhsCode11", Sm.GetLue(LueWhsCode11));
            Sm.CmParam<String>(ref cm, "@WhsCode12", Sm.GetLue(LueWhsCode12));
            Sm.CmParam<String>(ref cm, "@WhsCode13", Sm.GetLue(LueWhsCode13));
            Sm.CmParam<String>(ref cm, "@WhsCode14", Sm.GetLue(LueWhsCode14));
            Sm.CmParam<String>(ref cm, "@WhsCode15", Sm.GetLue(LueWhsCode15));
            Sm.CmParam<String>(ref cm, "@WhsCode16", Sm.GetLue(LueWhsCode16));
            Sm.CmParam<String>(ref cm, "@WhsCode17", Sm.GetLue(LueWhsCode17));
            Sm.CmParam<String>(ref cm, "@WhsCode18", Sm.GetLue(LueWhsCode18));
            Sm.CmParam<String>(ref cm, "@WhsCode19", Sm.GetLue(LueWhsCode19));
            Sm.CmParam<String>(ref cm, "@WhsCode20", Sm.GetLue(LueWhsCode20));
            Sm.CmParam<String>(ref cm, "@WhsCode21", Sm.GetLue(LueWhsCode21));
            Sm.CmParam<String>(ref cm, "@WhsCode22", Sm.GetLue(LueWhsCode22));
            Sm.CmParam<String>(ref cm, "@WhsCode23", Sm.GetLue(LueWhsCode23));
            Sm.CmParam<String>(ref cm, "@WhsCode24", Sm.GetLue(LueWhsCode24));
            Sm.CmParam<String>(ref cm, "@WhsCode25", Sm.GetLue(LueWhsCode25));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRecvRawMaterialHdr(DocNo);
                ShowRecvRawMaterialDtl(DocNo, "1", ref Grd1);
                ShowRecvRawMaterialDtl(DocNo, "2", ref Grd2);
                ShowRecvRawMaterialDtl(DocNo, "3", ref Grd3);
                ShowRecvRawMaterialDtl(DocNo, "4", ref Grd4);
                ShowRecvRawMaterialDtl2(DocNo, "1", ref Grd11, TxtTotal1);
                ShowRecvRawMaterialDtl2(DocNo, "2", ref Grd12, TxtTotal2);
                //ShowRecvRawMaterialDtl2(DocNo, "3", ref Grd13, TxtTotal3);
                ShowRecvRawMaterialDtl2_3(DocNo);
                ShowRecvRawMaterialDtl2(DocNo, "4", ref Grd14, TxtTotal4);
                ShowRecvRawMaterialDtl2(DocNo, "5", ref Grd15, TxtTotal5);
                ShowRecvRawMaterialDtl2(DocNo, "6", ref Grd16, TxtTotal6);
                ShowRecvRawMaterialDtl2(DocNo, "7", ref Grd17, TxtTotal7);
                ShowRecvRawMaterialDtl2(DocNo, "8", ref Grd18, TxtTotal8);
                ShowRecvRawMaterialDtl2(DocNo, "9", ref Grd19, TxtTotal9);
                ShowRecvRawMaterialDtl2(DocNo, "10", ref Grd20, TxtTotal10);
                ShowRecvRawMaterialDtl2(DocNo, "11", ref Grd21, TxtTotal11);
                ShowRecvRawMaterialDtl2(DocNo, "12", ref Grd22, TxtTotal12);
                ShowRecvRawMaterialDtl2(DocNo, "13", ref Grd23, TxtTotal13);
                ShowRecvRawMaterialDtl2(DocNo, "14", ref Grd24, TxtTotal14);
                ShowRecvRawMaterialDtl2(DocNo, "15", ref Grd25, TxtTotal15);
                ShowRecvRawMaterialDtl2(DocNo, "16", ref Grd26, TxtTotal16);
                ShowRecvRawMaterialDtl2(DocNo, "17", ref Grd27, TxtTotal17);
                ShowRecvRawMaterialDtl2(DocNo, "18", ref Grd28, TxtTotal18);
                ShowRecvRawMaterialDtl2(DocNo, "19", ref Grd29, TxtTotal19);
                ShowRecvRawMaterialDtl2(DocNo, "20", ref Grd30, TxtTotal20);
                ShowRecvRawMaterialDtl2(DocNo, "21", ref Grd31, TxtTotal21);
                ShowRecvRawMaterialDtl2(DocNo, "22", ref Grd32, TxtTotal22);
                ShowRecvRawMaterialDtl2(DocNo, "23", ref Grd33, TxtTotal23);
                ShowRecvRawMaterialDtl2(DocNo, "24", ref Grd34, TxtTotal24);
                ShowRecvRawMaterialDtl2(DocNo, "25", ref Grd35, TxtTotal25);
                ShowRecvRawMaterialDtl4(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvRawMaterialDtl4(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, IfNull(B.OptDesc, Round(A.Length, 2)) As Length, A.Qty, A.Remark  ");
            SQL.AppendLine("From TblRecvRawMaterialDtl4 A ");
            SQL.AppendLine("Left Join TblOption B On Round(A.Length, 0) = B.OptCode And B.OptCat = 'ReturnRawMaterialLength' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.Length, A.DNo; ");

            Sm.ShowDataInGrid(
                ref GrdReturn, ref cm, SQL.ToString(),
                new string[]{ "DNo", "Length", "Qty", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", GrdReturn, dr, c, Row, 0, 0);
                    //SetLueLengthCode(ref LueLengthCode);
                    //Sm.SetLue(LueLengthCode, Sm.DrStr(dr, c[1]));
                    Sm.SetGrdValue("S", GrdReturn, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", GrdReturn, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("N", GrdReturn, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", GrdReturn, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref GrdReturn, GrdReturn.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(GrdReturn, 0, 1);
        }

        private void ShowRecvRawMaterialHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, B.QueueNo, A.LegalDocVerifyDocNo, C.VdName, G.VdName As VdName2, ");
            SQL.AppendLine("E.TTName, A.UnloadStartDt, A.UnloadStartTm, A.UnloadEndDt, A.UnloadEndTm, F.UserName, A.Remark, ");
            SQL.AppendLine("A.Shelf1, A.Shelf2, A.Shelf3, A.Shelf4, A.Shelf5, A.Shelf6, A.Shelf7, A.Shelf8, A.Shelf9, A.Shelf10, A.Shelf11, A.Shelf12, A.Shelf13, A.Shelf14, A.Shelf15, ");
            SQL.AppendLine("A.Shelf16, A.Shelf17, A.Shelf18, A.Shelf19, A.Shelf20, A.Shelf21, A.Shelf22, A.Shelf23, A.Shelf24, A.Shelf25, ");
            SQL.AppendLine("A.WhsCode1, A.WhsCode2, A.WhsCode3, A.WhsCode4, A.WhsCode5, A.WhsCode6, A.WhsCode7, A.WhsCode8, A.WhsCode9, A.WhsCode10, A.WhsCode11, A.WhsCode12, A.WhsCode13, A.WhsCode14, A.WhsCode15, ");
            SQL.AppendLine("A.WhsCode16, A.WhsCode17, A.WhsCode18, A.WhsCode19, A.WhsCode20, A.WhsCode21, A.WhsCode22, A.WhsCode23, A.WhsCode24, A.WhsCode25 ");
            SQL.AppendLine("From TblRecvRawMaterialHdr A ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblLoadingQueue D On B.QueueNo=D.DocNo ");
            SQL.AppendLine("Left Join TblTransportType E On D.TTCode=E.TTCode ");
            SQL.AppendLine("Left Join TblUser F On A.CreateBy=F.UserCode ");
            SQL.AppendLine("Left Join TblVendor G On B.VdCode"+mType.ToString()+"=G.VdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "QueueNo", "LegalDocVerifyDocNo", "VdName", 

                        //6-10
                        "VdName2", "TTName", "UnloadStartDt", "UnloadStartTm", "UnloadEndDt",  
                        
                        //11-15
                        "UnloadEndTm", "UserName", "Remark", "Shelf1", "Shelf2",   
                        
                        //16-20
                        "Shelf3", "Shelf4", "Shelf5", "Shelf6", "Shelf7",   
                        
                        //21-25
                        "Shelf8", "Shelf9", "Shelf10","Shelf11", "Shelf12",   
                        
                        //26-30
                        "Shelf13", "Shelf14", "Shelf15", "Shelf16", "Shelf17",

                        //31-35
                        "Shelf18", "Shelf19", "Shelf20", "Shelf21", "Shelf22",

                        //36-40
                        "Shelf23", "Shelf24", "Shelf25", "WhsCode1", "WhsCode2",
                        
                        //41-45
                        "WhsCode3", "WhsCode4", "WhsCode5", "WhsCode6", "WhsCode7",   
                        
                        //46-50
                        "WhsCode8", "WhsCode9", "WhsCode10","WhsCode11", "WhsCode12",   
                        
                        //51-55
                        "WhsCode13", "WhsCode14", "WhsCode15", "WhsCode16", "WhsCode17",

                        //56-60
                        "WhsCode18", "WhsCode19", "WhsCode20", "WhsCode21", "WhsCode22",

                        //61-63
                        "WhsCode23", "WhsCode24", "WhsCode25"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        SetLueQueueNo(ref LueQueueNo, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueQueueNo, Sm.DrStr(dr, c[4]));
                        TxtLegalDocVerifyDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtVdCode2.EditValue = Sm.DrStr(dr, c[6]);
                        TxtTTCode.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetDte(DteUnloadStartDt, Sm.DrStr(dr, c[8]));
                        Sm.SetTme(TmeUnloadStartTm, Sm.DrStr(dr, c[9]));
                        Sm.SetDte(DteUnloadEndDt, Sm.DrStr(dr, c[10]));
                        Sm.SetTme(TmeUnloadEndTm, Sm.DrStr(dr, c[11]));
                        TxtCreateBy.EditValue = Sm.DrStr(dr, c[12]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[13]);

                        SetLueShelf(ref LueShelf1, Sm.DrStr(dr, c[14]));
                        SetLueShelf(ref LueShelf2, Sm.DrStr(dr, c[15]));
                        SetLueShelf(ref LueShelf3, Sm.DrStr(dr, c[16]));
                        SetLueShelf(ref LueShelf4, Sm.DrStr(dr, c[17]));
                        SetLueShelf(ref LueShelf5, Sm.DrStr(dr, c[18]));
                        SetLueShelf(ref LueShelf6, Sm.DrStr(dr, c[19]));
                        SetLueShelf(ref LueShelf7, Sm.DrStr(dr, c[20]));
                        SetLueShelf(ref LueShelf8, Sm.DrStr(dr, c[21]));
                        SetLueShelf(ref LueShelf9, Sm.DrStr(dr, c[22]));
                        SetLueShelf(ref LueShelf10, Sm.DrStr(dr, c[23]));
                        SetLueShelf(ref LueShelf11, Sm.DrStr(dr, c[24]));
                        SetLueShelf(ref LueShelf12, Sm.DrStr(dr, c[25]));
                        SetLueShelf(ref LueShelf13, Sm.DrStr(dr, c[26]));
                        SetLueShelf(ref LueShelf14, Sm.DrStr(dr, c[27]));
                        SetLueShelf(ref LueShelf15, Sm.DrStr(dr, c[28]));
                        SetLueShelf(ref LueShelf16, Sm.DrStr(dr, c[29]));
                        SetLueShelf(ref LueShelf17, Sm.DrStr(dr, c[30]));
                        SetLueShelf(ref LueShelf18, Sm.DrStr(dr, c[31]));
                        SetLueShelf(ref LueShelf19, Sm.DrStr(dr, c[32]));
                        SetLueShelf(ref LueShelf20, Sm.DrStr(dr, c[33]));
                        SetLueShelf(ref LueShelf21, Sm.DrStr(dr, c[34]));
                        SetLueShelf(ref LueShelf22, Sm.DrStr(dr, c[35]));
                        SetLueShelf(ref LueShelf23, Sm.DrStr(dr, c[36]));
                        SetLueShelf(ref LueShelf24, Sm.DrStr(dr, c[37]));
                        SetLueShelf(ref LueShelf25, Sm.DrStr(dr, c[38]));

                        Sm.SetLue(LueShelf1, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueShelf2, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueShelf3, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueShelf4, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueShelf5, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueShelf6, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueShelf7, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueShelf8, Sm.DrStr(dr, c[21]));
                        Sm.SetLue(LueShelf9, Sm.DrStr(dr, c[22]));
                        Sm.SetLue(LueShelf10, Sm.DrStr(dr, c[23]));
                        Sm.SetLue(LueShelf11, Sm.DrStr(dr, c[24]));
                        Sm.SetLue(LueShelf12, Sm.DrStr(dr, c[25]));
                        Sm.SetLue(LueShelf13, Sm.DrStr(dr, c[26]));
                        Sm.SetLue(LueShelf14, Sm.DrStr(dr, c[27]));
                        Sm.SetLue(LueShelf15, Sm.DrStr(dr, c[28]));
                        Sm.SetLue(LueShelf16, Sm.DrStr(dr, c[29]));
                        Sm.SetLue(LueShelf17, Sm.DrStr(dr, c[30]));
                        Sm.SetLue(LueShelf18, Sm.DrStr(dr, c[31]));
                        Sm.SetLue(LueShelf19, Sm.DrStr(dr, c[32]));
                        Sm.SetLue(LueShelf20, Sm.DrStr(dr, c[33]));
                        Sm.SetLue(LueShelf21, Sm.DrStr(dr, c[34]));
                        Sm.SetLue(LueShelf22, Sm.DrStr(dr, c[35]));
                        Sm.SetLue(LueShelf23, Sm.DrStr(dr, c[36]));
                        Sm.SetLue(LueShelf24, Sm.DrStr(dr, c[37]));
                        Sm.SetLue(LueShelf25, Sm.DrStr(dr, c[38]));

                        SetLueWhsCode(ref LueWhsCode1, Sm.DrStr(dr, c[39]));
                        SetLueWhsCode(ref LueWhsCode2, Sm.DrStr(dr, c[40]));
                        SetLueWhsCode(ref LueWhsCode3, Sm.DrStr(dr, c[41]));
                        SetLueWhsCode(ref LueWhsCode4, Sm.DrStr(dr, c[42]));
                        SetLueWhsCode(ref LueWhsCode5, Sm.DrStr(dr, c[43]));
                        SetLueWhsCode(ref LueWhsCode6, Sm.DrStr(dr, c[44]));
                        SetLueWhsCode(ref LueWhsCode7, Sm.DrStr(dr, c[45]));
                        SetLueWhsCode(ref LueWhsCode8, Sm.DrStr(dr, c[46]));
                        SetLueWhsCode(ref LueWhsCode9, Sm.DrStr(dr, c[47]));
                        SetLueWhsCode(ref LueWhsCode10, Sm.DrStr(dr, c[48]));
                        SetLueWhsCode(ref LueWhsCode11, Sm.DrStr(dr, c[49]));
                        SetLueWhsCode(ref LueWhsCode12, Sm.DrStr(dr, c[50]));
                        SetLueWhsCode(ref LueWhsCode13, Sm.DrStr(dr, c[51]));
                        SetLueWhsCode(ref LueWhsCode14, Sm.DrStr(dr, c[52]));
                        SetLueWhsCode(ref LueWhsCode15, Sm.DrStr(dr, c[53]));
                        SetLueWhsCode(ref LueWhsCode16, Sm.DrStr(dr, c[54]));
                        SetLueWhsCode(ref LueWhsCode17, Sm.DrStr(dr, c[55]));
                        SetLueWhsCode(ref LueWhsCode18, Sm.DrStr(dr, c[56]));
                        SetLueWhsCode(ref LueWhsCode19, Sm.DrStr(dr, c[57]));
                        SetLueWhsCode(ref LueWhsCode20, Sm.DrStr(dr, c[58]));
                        SetLueWhsCode(ref LueWhsCode21, Sm.DrStr(dr, c[59]));
                        SetLueWhsCode(ref LueWhsCode22, Sm.DrStr(dr, c[60]));
                        SetLueWhsCode(ref LueWhsCode23, Sm.DrStr(dr, c[61]));
                        SetLueWhsCode(ref LueWhsCode24, Sm.DrStr(dr, c[62]));
                        SetLueWhsCode(ref LueWhsCode25, Sm.DrStr(dr, c[63]));

                        Sm.SetLue(LueWhsCode1, Sm.DrStr(dr, c[39]));
                        Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[40]));
                        Sm.SetLue(LueWhsCode3, Sm.DrStr(dr, c[41]));
                        Sm.SetLue(LueWhsCode4, Sm.DrStr(dr, c[42]));
                        Sm.SetLue(LueWhsCode5, Sm.DrStr(dr, c[43]));
                        Sm.SetLue(LueWhsCode6, Sm.DrStr(dr, c[44]));
                        Sm.SetLue(LueWhsCode7, Sm.DrStr(dr, c[45]));
                        Sm.SetLue(LueWhsCode8, Sm.DrStr(dr, c[46]));
                        Sm.SetLue(LueWhsCode9, Sm.DrStr(dr, c[47]));
                        Sm.SetLue(LueWhsCode10, Sm.DrStr(dr, c[48]));
                        Sm.SetLue(LueWhsCode11, Sm.DrStr(dr, c[49]));
                        Sm.SetLue(LueWhsCode12, Sm.DrStr(dr, c[50]));
                        Sm.SetLue(LueWhsCode13, Sm.DrStr(dr, c[51]));
                        Sm.SetLue(LueWhsCode14, Sm.DrStr(dr, c[52]));
                        Sm.SetLue(LueWhsCode15, Sm.DrStr(dr, c[53]));
                        Sm.SetLue(LueWhsCode16, Sm.DrStr(dr, c[54]));
                        Sm.SetLue(LueWhsCode17, Sm.DrStr(dr, c[55]));
                        Sm.SetLue(LueWhsCode18, Sm.DrStr(dr, c[56]));
                        Sm.SetLue(LueWhsCode19, Sm.DrStr(dr, c[57]));
                        Sm.SetLue(LueWhsCode20, Sm.DrStr(dr, c[58]));
                        Sm.SetLue(LueWhsCode21, Sm.DrStr(dr, c[59]));
                        Sm.SetLue(LueWhsCode22, Sm.DrStr(dr, c[60]));
                        Sm.SetLue(LueWhsCode23, Sm.DrStr(dr, c[61]));
                        Sm.SetLue(LueWhsCode24, Sm.DrStr(dr, c[62]));
                        Sm.SetLue(LueWhsCode25, Sm.DrStr(dr, c[63]));
                    }, true
                );
        }

        private void ShowRecvRawMaterialDtl(string DocNo, string EmpType, ref iGrid EmpGrd)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpType", EmpType);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName ");
            SQL.AppendLine("From TblRecvRawMaterialDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.EmpType=@EmpType ");
            SQL.AppendLine("Order By B.EmpName");

            Sm.ShowDataInGrid(
                ref EmpGrd, ref cm, SQL.ToString(),
                new string[]{ "EmpCode", "EmpName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(EmpGrd, 0, 1);
        
        }

        private void ShowRecvRawMaterialDtl2_3(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SectionNo", "3");

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, A.Diameter, A.Qty, B.InventoryUomCode2, C.ItCode As ItCodeActual, ");
            if (mType == 1)
                SQL.AppendLine("Trim(Concat(Convert(Format(B.Length, 2) Using utf8), ' ', IfNull(B.LengthUomCode, ''))) As Info ");
            else
                SQL.AppendLine("Trim(Concat(Convert(Format(B.Length, 2) Using utf8), ' ', IfNull(B.LengthUomCode, ''), '*', Convert(Format(B.Height, 2) Using utf8), ' ', IfNull(B.HeightUomCode, ''))) As Info ");
            SQL.AppendLine("From TblRecvRawMaterialDtl3 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblRecvRawMaterialDtl2 C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.SectionNo=@SectionNo ");
            SQL.AppendLine("Order By A.ItCode, A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd13, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "Info", "Diameter", "Qty", "InventoryUomCode2",
 
                    //6
                    "ItCodeActual"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd13, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd13, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd13, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd13, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("I", Grd13, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd13, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("I", Grd13, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd13, dr, c, Row, 7, 6);
                    ComputeTotal(Grd13, TxtTotal3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd13, Grd13.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd13, 0, 0);
        }


        private void ShowRecvRawMaterialDtl2(string DocNo, string SectionNo, ref iGrid ItemGrd, DXE.TextEdit TxtTotal)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, A.Diameter, A.Qty, B.InventoryUomCode2, C.ItCode As ItCodeActual, ");

            if (mType == 1)
                SQL.AppendLine("Trim(Concat(Convert(Format(B.Length, 2) Using utf8), ' ', IfNull(B.LengthUomCode, ''))) As Info ");
            else
                SQL.AppendLine("Trim(Concat(Convert(Format(B.Length, 2) Using utf8), ' ', IfNull(B.LengthUomCode, ''), '*', Convert(Format(B.Height, 2) Using utf8), ' ', IfNull(B.HeightUomCode, ''))) As Info ");
                
            SQL.AppendLine("From TblRecvRawMaterialDtl3 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblRecvRawMaterialDtl2 C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.SectionNo=@SectionNo ");
            SQL.AppendLine("Order By A.ItCode, A.DNo;");

            Sm.ShowDataInGrid(
                ref ItemGrd, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "Info", "Diameter", "Qty", "InventoryUomCode2",
 
                    //6
                    "ItCodeActual"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    ComputeTotal(Grd, TxtTotal);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref ItemGrd, ItemGrd.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(ItemGrd, 0, 0);
        }

        #endregion

        #region Additional Method

        private void SetUserName()
        {
            var cm = new MySqlCommand() { CommandText = "Select UserName From TblUser Where UserCode=@UserCode;" };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            TxtCreateBy.EditValue = Sm.GetValue(cm);
        }

        public static void SetLueLengthCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select T.OptCode As Col1, T.OptDesc As Col2 " +
                "From TblOption T " +
                "Where T.OptCat = 'ReturnRawMaterialLength'; ",
                 0, 35, false, true, "Code", "Length", "Col2", "Col1");
        }

        private void SetLueQueueNo(ref DXE.LookUpEdit Lue, string DocNo)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct Col1, Col2 From ( ");
                SQL.AppendLine("    Select DocNo As Col1, QueueNo As Col2 ");
                SQL.AppendLine("    From TblLegalDocVerifyHdr ");
                SQL.AppendLine("    Where CancelInd='N' ");
                SQL.AppendLine("    And IfNull(ProcessInd1, 'O')<>'F' "); 
                
                if (DocNo.Length != 0)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select DocNo As Col1, QueueNo As Col2 ");
                    SQL.AppendLine("    From TblLegalDocVerifyHdr ");
                    SQL.AppendLine("    Where DocNo='" + DocNo + "' ");
                }
                SQL.AppendLine(") Tbl Order By Col2");

                Sm.SetLue2(
                    ref Lue,
                    SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select ItCode As Col1, ItName As Col2, InventoryUomCode2 As Col3, ");
                if (mType==1)
                    SQL.AppendLine("Trim(Concat(Convert(Format(Length, 2) Using utf8), ' ', IfNull(LengthUomCode, ''))) As Col4 ");
                else
                    SQL.AppendLine("Trim(Concat(Convert(Format(Length, 2) Using utf8), ' ', IfNull(LengthUomCode, ''), '*', Convert(Format(Height, 2) Using utf8), ' ', IfNull(HeightUomCode, ''))) As Col4 ");
                
                SQL.AppendLine("From TblItem ");
                SQL.AppendLine("Where ActInd='Y' ");
                SQL.AppendLine("And ItCtCode In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='ItCtRMP" + (mType==2?"2":"") +" ') ");
                SQL.AppendLine("Order By ItName");

                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    10, 60, 0, 50, false, true, false, true, "Code", "Name", "UoM", "Length", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueShelf(ref DXE.LookUpEdit Lue, string Bin)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct Col1, Col2 From ( ");
                SQL.AppendLine("    Select Bin As Col1, Bin As Col2 From TblBin Where ActInd='Y' And (Status='1' OR Status = '2') ");
                if (Bin.Length!=0)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select Bin As Col1, Bin As Col2 From TblBin Where Bin='"+Bin+"' ");
                }
                SQL.AppendLine(") T Order By Col1;");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(), 0, 50, false, true, "Bin", "Bin", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueWhsCode(ref DXE.LookUpEdit Lue, string WhsCode)
        {
            try
            {
                var SQL = new StringBuilder();

                string RecvVdRawMaterialWhs= Sm.GetParameter("RecvVdRawMaterialWhs"); 

                SQL.AppendLine("Select Distinct Col1, Col2 From ( ");
                SQL.AppendLine("    Select WhsCode As Col1, WhsName As Col2 From TblWarehouse ");
                if (RecvVdRawMaterialWhs.Length>0)
                    SQL.AppendLine("    Where Locate(Concat('##', WhsCode, '##'), '" + RecvVdRawMaterialWhs + "')>0 ");
                if (WhsCode.Length != 0)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select WhsCode As Col1, WhsName As Col2 From TblWarehouse Where WhsCode='" + WhsCode + "' ");
                }
                SQL.AppendLine(") T Order By Col2;");
                
                Sm.SetLue2(
                    ref Lue, SQL.ToString(), 0, 50, false, true, "Bin", "Bin", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueEmpCode(ref DXE.LookUpEdit Lue, string PosCode, string EmpCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Col1, Col2, Col3 From ( ");
                SQL.AppendLine("    Select A.EmpCode As Col1, Concat(A.EmpName, ' [', A.EmpCode,']') As Col2, B.DeptName As Col3 ");
                SQL.AppendLine("    From TblEmployee A ");
                SQL.AppendLine("    Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL.AppendLine("    Where A.ResignDt Is Null ");
                SQL.AppendLine("    And A.PosCode In (Select ParValue From TblParameter Where ParCode='"+PosCode+"') ");
                if (EmpCode.Length != 0)
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select A.EmpCode As Col1, Concat(A.EmpName, ' [', A.EmpCode,']') As Col2, B.DeptName As Col3 ");
                    SQL.AppendLine("    From TblEmployee A ");
                    SQL.AppendLine("    Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
                    SQL.AppendLine("    Where A.EmpCode='" + EmpCode + "' ");
                }
                SQL.AppendLine(") Tbl Order By Col2");

                Sm.SetLue3(
                    ref Lue,
                    SQL.ToString(),
                    0, 50, 35, false, true, true, "Code", "Name", "Department", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowLegalDocVerifyInfo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, B.VdName, D.TTName, E.VdName As VdName2 ");
            SQL.AppendLine("From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("Left Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Left Join TblLoadingQueue C On A.QueueNo=C.DocNo ");
            SQL.AppendLine("Left Join TblTransportType D On C.TTCode=D.TTCode ");
            SQL.AppendLine("Left Join TblVendor E On A.VdCode" + mType.ToString() + "=E.VdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            try
            {
                Sm.CmParam<String>(ref cm, "@DocNo", LueQueueNo.GetColumnValue("Col1").ToString());

                Sm.ShowDataInCtrl(
                        ref cm,
                        SQL.ToString(),
                        new string[]{ "DocNo", "VdName", "TTName", "VdName2" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtLegalDocVerifyDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            TxtVdCode.EditValue = Sm.DrStr(dr, c[1]);
                            TxtTTCode.EditValue = Sm.DrStr(dr, c[2]);
                            TxtVdCode2.EditValue = Sm.DrStr(dr, c[3]);
                        }, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueLengthCodeRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 4).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 4));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void LueRequestEdit(iGrid Grd, DXE.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }                 

        private void SetGrdCanvas()
        {
            iGCellStyle CanvasStyle = new iGCellStyle();
            CanvasStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
            CanvasStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)(TenTec.Windows.iGridLib.iGCellFlags.DisplayImage));

            Grd11.Cols[6].CellStyle =
            Grd12.Cols[6].CellStyle =
            Grd13.Cols[6].CellStyle =
            Grd14.Cols[6].CellStyle =
            Grd15.Cols[6].CellStyle =
            Grd16.Cols[6].CellStyle =
            Grd17.Cols[6].CellStyle =
            Grd18.Cols[6].CellStyle =
            Grd19.Cols[6].CellStyle =
            Grd20.Cols[6].CellStyle =
            Grd21.Cols[6].CellStyle =
            Grd22.Cols[6].CellStyle =
            Grd23.Cols[6].CellStyle =
            Grd24.Cols[6].CellStyle =
            Grd25.Cols[6].CellStyle = 
            Grd26.Cols[6].CellStyle =
            Grd27.Cols[6].CellStyle =
            Grd28.Cols[6].CellStyle =
            Grd29.Cols[6].CellStyle =
            Grd30.Cols[6].CellStyle =
            Grd31.Cols[6].CellStyle =
            Grd32.Cols[6].CellStyle =
            Grd33.Cols[6].CellStyle =
            Grd34.Cols[6].CellStyle =
            Grd35.Cols[6].CellStyle = CanvasStyle;
        }

        private void GrdCustomDrawCellForeground(iGrid Grd, object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex != 6) return;

            try
            {
                object myObjValue = Grd.Cells[e.RowIndex, e.ColIndex].Value;

                if (myObjValue == null) return;

                Pen RedPen = new Pen(Color.Red, 2);
                Rectangle myBounds = e.Bounds;

                myBounds.Inflate(-2, -2);

                myBounds.Width = myBounds.Width - 1;

                myBounds.Height = myBounds.Height - 1;

                int X1 = myBounds.X, Y1 = myBounds.Y;

                Int32 myValue = Sm.GetGrdInt(Grd, e.RowIndex, e.ColIndex);

                Int32 DivValue = (Int32)(myValue / 5);

                Int32 RemainingValue = myValue % 5;

                for (int intX = 0; intX < DivValue; intX++)
                {

                    e.Graphics.DrawLine(RedPen, X1, Y1, X1, Y1 + myBounds.Height);

                    e.Graphics.DrawLine(RedPen, X1, Y1 + myBounds.Height, X1 + myBounds.Height, Y1 + myBounds.Height);

                    e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1 + myBounds.Height, X1 + myBounds.Height, Y1);

                    e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1, X1, Y1);

                    e.Graphics.DrawLine(RedPen, X1, Y1, X1 + myBounds.Height, Y1 + myBounds.Height);

                    X1 = X1 + myBounds.Height + 5;

                }

                if (RemainingValue >= 1)
                {
                    e.Graphics.DrawLine(RedPen, X1, Y1, X1, Y1 + myBounds.Height);
                    if (RemainingValue >= 2)
                    {
                        e.Graphics.DrawLine(RedPen, X1, Y1 + myBounds.Height, X1 + myBounds.Height, Y1 + myBounds.Height);
                        if (RemainingValue >= 3)
                        {
                            e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1 + myBounds.Height, X1 + myBounds.Height, Y1);
                            if (RemainingValue >= 4)
                            {
                                e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1, X1, Y1);
                                if (RemainingValue >= 5)
                                    e.Graphics.DrawLine(RedPen, X1, Y1, X1 + myBounds.Height, Y1 + myBounds.Height);
                            }
                        }
                    }
                }

                int TotalWidth = 0;
                for (int intX = 0; intX <= e.ColIndex; intX++)
                    TotalWidth = TotalWidth + Grd.Cols[intX].Width;

                if (X1 > TotalWidth)
                    Grd.Cols[e.ColIndex].Width = Grd.Cols[e.ColIndex].Width + (X1 - TotalWidth) + myBounds.Height;
             }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetTabControls(string TabPage, ref DXE.LookUpEdit LueShelf, ref DXE.LookUpEdit LueWhsCode, ref DXE.LookUpEdit LueItCode)
        {
            tabControl1.SelectTab("tabPage" + TabPage);
            SetLueShelf(ref LueShelf, "");
            SetLueWhsCode(ref LueWhsCode, "");
            SetLueItCode(ref LueItCode);
            LueItCode.Visible = false;
        }

        private void LueItCodeLeave(object sender, EventArgs e, DXE.LookUpEdit Lue, iGrid Grd)
        {
            if (Lue.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (Sm.GetLue(Lue).Length == 0)
                {
                    Grd.Cells[fCell.RowIndex, 0].Value =
                    Grd.Cells[fCell.RowIndex, 1].Value =
                    Grd.Cells[fCell.RowIndex, 2].Value =
                    Grd.Cells[fCell.RowIndex, 5].Value = null;
                }
                else
                {
                    Grd.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(Lue);
                    Grd.Cells[fCell.RowIndex, 1].Value = Lue.GetColumnValue("Col2");
                    Grd.Cells[fCell.RowIndex, 2].Value = Lue.GetColumnValue("Col4");
                    Grd.Cells[fCell.RowIndex, 5].Value = Lue.GetColumnValue("Col3");

                    if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length != 0 &&
                        Sm.GetGrdStr(Grd, fCell.RowIndex, 3).Length != 0 && Sm.GetGrdDec(Grd, fCell.RowIndex, 3) != 0)
                    {
                        Grd.Cells[fCell.RowIndex, 7].Value = SetActualItCode2(Grd, fCell.RowIndex);
                        if (Sm.GetGrdStr(Grd, fCell.RowIndex, 7).Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Actual item is empty.");
                        }
                    }
                }
                Sm.FocusGrd(Grd, fCell.RowIndex, 3);
                Lue.Visible = false;
            }
        }

        private void GrdItemAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e, iGrid Grd, DXE.TextEdit TxtTotal)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd, new int[] { 3, 4 }, e);

            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd, e.RowIndex, 0).Length != 0 &&
                    Sm.GetGrdStr(Grd, e.RowIndex, 3).Length != 0 && Sm.GetGrdDec(Grd, e.RowIndex, 3) != 0)
                {
                    Grd.Cells[e.RowIndex, 7].Value = SetActualItCode2(Grd, e.RowIndex);
                    if (Sm.GetGrdStr(Grd, e.RowIndex, 7).Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Actual item is empty.");
                    }
                }
                Sm.FocusGrd(Grd, e.RowIndex, 4);
            }
            
            if (e.ColIndex == 4)
            {
                Grd.Cells[e.RowIndex, 6].Value = Sm.GetGrdDec(Grd, e.RowIndex, 4);
                ComputeTotal(Grd, TxtTotal);

                //Grd.Cells[e.RowIndex + 1, 0].Value = Sm.GetGrdStr(Grd, e.RowIndex, 0);
                //Grd.Cells[e.RowIndex + 1, 1].Value = Sm.GetGrdStr(Grd, e.RowIndex, 1);
                //Grd.Cells[e.RowIndex + 1, 2].Value = Sm.GetGrdStr(Grd, e.RowIndex, 2);
                //Grd.Cells[e.RowIndex + 1, 5].Value = Sm.GetGrdStr(Grd, e.RowIndex, 5);
                //Grd.Cells[e.RowIndex + 1, 7].Value = Sm.GetGrdStr(Grd, e.RowIndex, 7);

                //Sm.FocusGrd(Grd, e.RowIndex + 1, 3);

                //if (Grd.Rows.Count - 1 == e.RowIndex+1)
                //{
                //    Grd.Rows.Add();
                //    Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 3, 4 });
                //}
            }
        }

        private void GrdItemRequestEdit(object sender, iGRequestEditEventArgs e, iGrid Grd, DXE.LookUpEdit Lue)
        {
            if (BtnSave.Enabled &&
                // TxtDocNo.Text.Length == 0 &&
                Sm.IsGrdColSelected(new int[] { 0, 3, 4 }, e.ColIndex))
            {
                if (e.ColIndex == 0) LueRequestEdit(Grd, Lue, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 3, 4 });
            }
        }

        private void GrdItemKeyDown(iGrid Grd, KeyEventArgs e, DXE.SimpleButton Btn1, DXE.SimpleButton Btn2)
        {
            //if (e.KeyCode == Keys.Enter && Grd.CurRow.Index != Grd.Rows.Count - 1)
            //{
            //    if (Grd.CurCell.Col.Index == 0)
            //        Sm.FocusGrd(Grd, Grd.CurRow.Index, 3);
            //    else
            //    {
            //        if (Grd.CurCell.Col.Index == 3)
            //            Sm.FocusGrd(Grd, Grd.CurRow.Index, 4);
            //        else
            //        {
            //            if (Grd.CurCell.Col.Index == 4)
            //            {
            //                Grd.Cells[Grd.CurRow.Index + 1, 0].Value = Sm.GetGrdStr(Grd, Grd.CurRow.Index, 0);
            //                Sm.FocusGrd(Grd, Grd.CurRow.Index + 1, 0);
            //            }
            //        }
            //    }
            //}

            try
            {
                if (Grd.CurCell!=null && Grd.CurCell.ColIndex == 4 && e.KeyCode == Keys.Enter && Grd.CurCell.RowIndex != Grd.Rows.Count - 1)
                {
                    Grd.Cells[Grd.CurCell.RowIndex + 1, 0].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 0);
                    Grd.Cells[Grd.CurCell.RowIndex + 1, 1].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 1);
                    Grd.Cells[Grd.CurCell.RowIndex + 1, 2].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 2);
                    Grd.Cells[Grd.CurCell.RowIndex + 1, 5].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 5);
                    Grd.Cells[Grd.CurCell.RowIndex + 1, 7].Value = Sm.GetGrdStr(Grd, Grd.CurCell.RowIndex, 7);

                    Sm.FocusGrd(Grd, Grd.CurCell.RowIndex + 1, 3);

                    if (Grd.Rows.Count == Grd.CurCell.RowIndex + 1)
                    {
                        Grd.Rows.Add();
                        Sm.SetGrdNumValueZero(ref Grd, Grd.Rows.Count - 1, new int[] { 3, 4 });
                    }
                }

                if (e.KeyCode == Keys.Tab && Grd.CurCell != null && Grd.CurCell.RowIndex == Grd.Rows.Count - 1)
                {
                    int LastVisibleCol = Grd.CurCell.ColIndex;
                    for (int Col = LastVisibleCol; Col <= Grd.Cols.Count - 1; Col++)
                        if (Grd.Cols[Col].Visible) LastVisibleCol = Col;

                    if (Grd.CurCell.Col.Order == LastVisibleCol)
                    {
                        if (Btn1.Enabled)
                            Btn1.Focus();
                        else
                            Btn2.Focus();
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GrdItemColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e, iGrid Grd)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd, 0, 0).Length != 0)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                {
                    Sm.CopyGrdValue(Grd, Row, 0, Grd, 0, 0);
                    Sm.CopyGrdValue(Grd, Row, 1, Grd, 0, 1);
                    Sm.CopyGrdValue(Grd, Row, 2, Grd, 0, 2);
                    Sm.CopyGrdValue(Grd, Row, 5, Grd, 0, 5);
                }
            }
            if (e.ColIndex==4)
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        private string SetActualItCode2(iGrid Grd, int Row)
        {
            var cm = new MySqlCommand() { CommandText = mSQLForActualItCode };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@P", Sm.GetGrdDec(Grd, Row, 3));
            Sm.CmParam<String>(ref cm, "@ItCtCode", mItCtRMPActual);

            string ItCode = Sm.GetValue(cm);
            return ItCode;
        }

        private void SetSQLForActualItCode()
        {
            var SQL = new StringBuilder();

            if (mType == 1)
            {
                SQL.AppendLine("Select A.ItCode ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select Trim(ItName) As ItName, Length, @P As Diameter ");
                SQL.AppendLine("    From TblItem ");
                SQL.AppendLine("    Where ItCode=@ItCode ");
                SQL.AppendLine(") B ");
                SQL.AppendLine("    On Trim(A.ItName)=B.ItName ");
                SQL.AppendLine("    And A.Length=B.Length ");
                SQL.AppendLine("    And A.Diameter=B.Diameter ");
                SQL.AppendLine("Where A.ItCtCode=@ItCtCode ");
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("Limit 1;");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select Trim(ItName) As ItName, Height, Length, @P As Width ");
                SQL.AppendLine("    From TblItem ");
                SQL.AppendLine("    Where ItCode=@ItCode ");
                SQL.AppendLine(") B ");
                SQL.AppendLine("    On Trim(A.ItName)=B.ItName ");
                SQL.AppendLine("    And A.Height=B.Height ");
                SQL.AppendLine("    And A.Length=B.Length ");
                SQL.AppendLine("    And A.Width=B.Width ");
                SQL.AppendLine(" Where A.ItCtCode=@ItCtCode ");
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("Limit 1;");
            }
            mSQLForActualItCode = SQL.ToString();
        }

        private void SetActualItCode(iGrid Grd)
        {
            if (Grd.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd, Row, 0).Length != 0)
                        Grd.Cells[Row, 7].Value = SetActualItCode2(Grd, Row);
            }
        }

        private void ComputeTotal(iGrid Grd, DXE.TextEdit Txt)
        {
            decimal Total = 0m;
            for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd, Row, 4).Length != 0) Total += Sm.GetGrdDec(Grd, Row, 4);
            Txt.Text = Sm.FormatNum(Total, 0);
            ComputeTotal();
        }

        private void ComputeTotal()
        {
            TxtTotal.Text = 
                Sm.FormatNum(
                    decimal.Parse(TxtTotal1.Text)+
                    decimal.Parse(TxtTotal2.Text)+
                    decimal.Parse(TxtTotal3.Text)+
                    decimal.Parse(TxtTotal4.Text)+
                    decimal.Parse(TxtTotal5.Text)+
                    decimal.Parse(TxtTotal6.Text)+
                    decimal.Parse(TxtTotal7.Text)+
                    decimal.Parse(TxtTotal8.Text)+
                    decimal.Parse(TxtTotal9.Text)+
                    decimal.Parse(TxtTotal10.Text)+
                    decimal.Parse(TxtTotal11.Text)+
                    decimal.Parse(TxtTotal12.Text)+
                    decimal.Parse(TxtTotal13.Text)+
                    decimal.Parse(TxtTotal14.Text)+
                    decimal.Parse(TxtTotal15.Text)+
                    decimal.Parse(TxtTotal16.Text)+
                    decimal.Parse(TxtTotal17.Text)+
                    decimal.Parse(TxtTotal18.Text)+
                    decimal.Parse(TxtTotal19.Text)+
                    decimal.Parse(TxtTotal20.Text)+
                    decimal.Parse(TxtTotal21.Text)+
                    decimal.Parse(TxtTotal22.Text)+
                    decimal.Parse(TxtTotal23.Text)+
                    decimal.Parse(TxtTotal24.Text)+
                    decimal.Parse(TxtTotal25.Text)
                    , 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueQueueNo_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueQueueNo, new Sm.RefreshLue2(SetLueQueueNo), "");
                TxtLegalDocVerifyDocNo.EditValue = null;
                TxtVdCode.EditValue = null;
                TxtVdCode2.EditValue = null;
                TxtTTCode.EditValue = null;

                if (Sm.GetLue(LueQueueNo).Length != 0) ShowLegalDocVerifyInfo();
            }
        }

        #region Item

        #region LueItCode_EditValueChanged

        private void LueItCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode1, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode2, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode3, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode4, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode5_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode5, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode6_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode6, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode7_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode7, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode8_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode8, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode9_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode9, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode10_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode10, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode11_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode11, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode12_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode12, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode13_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode13, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode14_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode14, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode15_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode15, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode16_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode16, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode17_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode17, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode18_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode18, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode19_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode19, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode20_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode20, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode21_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode21, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode22_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode22, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode23_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode23, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode24_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode24, new Sm.RefreshLue1(SetLueItCode));
        }

        private void LueItCode25_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItCode25, new Sm.RefreshLue1(SetLueItCode));
        }

        #endregion

        #region LueItCode_Leave

        private void LueItCode1_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode1, Grd11);
        }

        private void LueItCode2_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode2, Grd12);
        }

        private void LueItCode3_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode3, Grd13);
        }

        private void LueItCode4_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode4, Grd14);
        }

        private void LueItCode5_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode5, Grd15);
        }

        private void LueItCode6_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode6, Grd16);
        }

        private void LueItCode7_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode7, Grd17);
        }

        private void LueItCode8_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode8, Grd18);
        }

        private void LueItCode9_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode9, Grd19);
        }

        private void LueItCode10_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode10, Grd20);
        }

        private void LueItCode11_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode11, Grd21);
        }

        private void LueItCode12_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode12, Grd22);
        }

        private void LueItCode13_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode13, Grd23);
        }

        private void LueItCode14_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode14, Grd24);
        }

        private void LueItCode15_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode15, Grd25);
        }

        private void LueItCode16_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode16, Grd26);
        }

        private void LueItCode17_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode17, Grd27);
        }

        private void LueItCode18_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode18, Grd28);
        }

        private void LueItCode19_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode19, Grd29);
        }

        private void LueItCode20_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode20, Grd30);
        }

        private void LueItCode21_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode21, Grd31);
        }

        private void LueItCode22_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode22, Grd32);
        }

        private void LueItCode23_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode23, Grd33);
        }

        private void LueItCode24_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode24, Grd34);
        }

        private void LueItCode25_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueItCodeLeave(sender, e, LueItCode25, Grd35);
        }

        #endregion

        #region LueItCode_KeyDown

        private void LueItCode1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void LueItCode2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void LueItCode3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void LueItCode4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void LueItCode5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void LueItCode6_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueItCode7_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void LueItCode8_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void LueItCode9_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void LueItCode10_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        private void LueItCode11_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd21, ref fAccept, e);
        }

        private void LueItCode12_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd22, ref fAccept, e);
        }

        private void LueItCode13_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd23, ref fAccept, e);
        }

        private void LueItCode14_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd24, ref fAccept, e);
        }

        private void LueItCode15_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd25, ref fAccept, e);
        }

        private void LueItCode16_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd26, ref fAccept, e);
        }

        private void LueItCode17_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd27, ref fAccept, e);
        }

        private void LueItCode18_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd28, ref fAccept, e);
        }

        private void LueItCode19_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd29, ref fAccept, e);
        }

        private void LueItCode20_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd30, ref fAccept, e);
        }

        private void LueItCode21_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd31, ref fAccept, e);
        }

        private void LueItCode22_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd32, ref fAccept, e);
        }

        private void LueItCode23_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd33, ref fAccept, e);
        }

        private void LueItCode24_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd34, ref fAccept, e);
        }

        private void LueItCode25_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd35, ref fAccept, e);
        }

        #endregion

        #endregion

        #region Employee

        #region LueEmpCode_EditValueChanged

        private void LueEmpCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEmpCode1, new Sm.RefreshLue3(SetLueEmpCode), "PosCodeGrader", "");
        }

        private void LueEmpCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueEmpCode2, new Sm.RefreshLue3(SetLueEmpCode), 
                    (mType==1)?"PosCodeGraderAssistant":"PosCodeUnloadBalok", 
                    "");
        }

        private void LueEmpCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEmpCode3, new Sm.RefreshLue3(SetLueEmpCode), "PosCodeUnloadInside", "");
        }

        private void LueEmpCode4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEmpCode4, new Sm.RefreshLue3(SetLueEmpCode), "PosCodeUnloadOutside", "");
        }

        #endregion

        #region LueEmpCode_Leave

        private void LueEmpCode1_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueEmpCode1.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueEmpCode1).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 0].Value =
                        Grd1.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueEmpCode1);
                        Grd1.Cells[fCell.RowIndex, 1].Value = LueEmpCode1.GetColumnValue("Col2");
                    }
                    LueEmpCode1.Visible = false;
                }
            }
        }

        private void LueEmpCode2_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueEmpCode2.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueEmpCode2).Length == 0)
                        Grd2.Cells[fCell.RowIndex, 0].Value =
                        Grd2.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueEmpCode2);
                        Grd2.Cells[fCell.RowIndex, 1].Value = LueEmpCode2.GetColumnValue("Col2");
                    }
                    LueEmpCode2.Visible = false;
                }
            }
        }

        private void LueEmpCode3_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueEmpCode3.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueEmpCode3).Length == 0)
                        Grd3.Cells[fCell.RowIndex, 0].Value =
                        Grd3.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd3.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueEmpCode3);
                        Grd3.Cells[fCell.RowIndex, 1].Value = LueEmpCode3.GetColumnValue("Col2");
                    }
                    LueEmpCode3.Visible = false;
                }
            }
        }

        private void LueEmpCode4_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueEmpCode4.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueEmpCode4).Length == 0)
                        Grd4.Cells[fCell.RowIndex, 0].Value =
                        Grd4.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd4.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueEmpCode4);
                        Grd4.Cells[fCell.RowIndex, 1].Value = LueEmpCode4.GetColumnValue("Col2");
                    }
                    LueEmpCode4.Visible = false;
                }
            }
        }

        #endregion

        #region LueEmpCode_KeyDown

        private void LueEmpCode1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd1, ref fAccept, e);

        }

        private void LueEmpCode2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueEmpCode3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueEmpCode4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        #endregion

        #endregion

        #region Shelf

        private void LueShelf1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf1, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf2, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf3, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf4, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf5_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf5, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf6_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf6, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf7_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf7, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf8_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf8, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf9_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf9, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf10_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf10, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf11_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf11, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf12_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf12, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf13_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf13, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf14_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf14, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf15_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf15, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf16_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf16, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf17_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf17, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf18_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf18, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf19_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf19, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf20_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf20, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf21_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf21, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf22_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf22, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf23_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf23, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf24_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf24, new Sm.RefreshLue2(SetLueShelf), "");
        }

        private void LueShelf25_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueShelf25, new Sm.RefreshLue2(SetLueShelf), "");
        }

        #endregion

        #region Warehouse

        private void LueWhsCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode1, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode3, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode4, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode5_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode5, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode6_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode6, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode7_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode7, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode8_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode8, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode9_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode9, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode10_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode10, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode11_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode11, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode12_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode12, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode13_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode13, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode14_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode14, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode15_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode15, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode16_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode16, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode17_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode17, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode18_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode18, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode19_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode19, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode20_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode20, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode21_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode21, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode22_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode22, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode23_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode23, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode24_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode24, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        private void LueWhsCode25_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode25, new Sm.RefreshLue2(SetLueWhsCode), "");
        }

        #endregion

        #region Return

        private void LueLengthCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLengthCode, new Sm.RefreshLue1(SetLueLengthCode));
        }

        private void LueLengthCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(GrdReturn, ref fAccept, e);
        }

        private void LueLengthCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueLengthCode.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueLengthCode).Length == 0)
                        GrdReturn.Cells[fCell.RowIndex, 4].Value =
                        GrdReturn.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        GrdReturn.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueLengthCode);
                        GrdReturn.Cells[fCell.RowIndex, 1].Value = LueLengthCode.GetColumnValue("Col2");
                    }
                    LueLengthCode.Visible = false;
                    Sm.SetGrdNumValueZero(ref GrdReturn, (fCell.RowIndex + 1), new int[] { 2 });
                }
            }
        }

        #endregion

        #endregion

        #region Grid Event

        #region Employee

        #region Grd_RequestEdit

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd1, LueEmpCode1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd2, LueEmpCode2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd3, LueEmpCode3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1) LueRequestEdit(Grd4, LueEmpCode4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
        }

        #endregion

        #region Grd_KeyDown

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            //if (TxtDocNo.Text.Length==0) 
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            //if (TxtDocNo.Text.Length == 0)
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            //if (TxtDocNo.Text.Length == 0)
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            //if (TxtDocNo.Text.Length == 0)
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #region Item

        #region Grd_RequestEdit

        private void Grd11_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd11, LueItCode1);
        }

        private void Grd12_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd12, LueItCode2);
        }

        private void Grd13_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd13, LueItCode3);
        }

        private void Grd14_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd14, LueItCode4);
        }

        private void Grd15_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd15, LueItCode5);
        }

        private void Grd16_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd16, LueItCode6);
        }

        private void Grd17_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd17, LueItCode7);
        }

        private void Grd18_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd18, LueItCode8);
        }

        private void Grd19_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd19, LueItCode9);
        }

        private void Grd20_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd20, LueItCode10);
        }

        private void Grd21_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd21, LueItCode11);
        }

        private void Grd22_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd22, LueItCode12);
        }

        private void Grd23_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd23, LueItCode13);
        }

        private void Grd24_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd24, LueItCode14);
        }

        private void Grd25_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd25, LueItCode15);
        }

        private void Grd26_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd26, LueItCode16);
        }

        private void Grd27_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd27, LueItCode17);
        }

        private void Grd28_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd28, LueItCode18);
        }

        private void Grd29_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd29, LueItCode19);
        }

        private void Grd30_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd30, LueItCode20);
        }

        private void Grd31_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd31, LueItCode21);
        }

        private void Grd32_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd32, LueItCode22);
        }

        private void Grd33_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd33, LueItCode23);
        }

        private void Grd34_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd34, LueItCode24);
        }

        private void Grd35_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdItemRequestEdit(sender, e, Grd35, LueItCode25);
        }

        #endregion

        #region Grd_KeyDown

        private void Grd11_KeyDown(object sender, KeyEventArgs e)
        {
            //if (TxtDocNo.Text.Length == 0)
            //{
            Sm.GrdRemoveRow(Grd11, e, BtnSave);
            ComputeTotal(Grd11, TxtTotal1);
            //}
            GrdItemKeyDown(Grd11, e, BtnFind, BtnSave);
        }

        private void Grd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd12, e, BtnSave);
            ComputeTotal(Grd12, TxtTotal2);
            GrdItemKeyDown(Grd12, e, BtnFind, BtnSave);
        }

        private void Grd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd13, e, BtnSave);
            ComputeTotal(Grd13, TxtTotal3);
            GrdItemKeyDown(Grd13, e, BtnFind, BtnSave);
        }

        private void Grd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd14, e, BtnSave);
            ComputeTotal(Grd14, TxtTotal4);
            GrdItemKeyDown(Grd14, e, BtnFind, BtnSave);
        }

        private void Grd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd15, e, BtnSave);
            ComputeTotal(Grd15, TxtTotal5);
            GrdItemKeyDown(Grd15, e, BtnFind, BtnSave);
        }

        private void Grd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd16, e, BtnSave);
            ComputeTotal(Grd16, TxtTotal6);
            GrdItemKeyDown(Grd16, e, BtnFind, BtnSave);
        }

        private void Grd17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd17, e, BtnSave);
            ComputeTotal(Grd17, TxtTotal7);
            GrdItemKeyDown(Grd17, e, BtnFind, BtnSave);
        }

        private void Grd18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd18, e, BtnSave);
            ComputeTotal(Grd18, TxtTotal8);
            GrdItemKeyDown(Grd18, e, BtnFind, BtnSave);
        }

        private void Grd19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd19, e, BtnSave);
            ComputeTotal(Grd19, TxtTotal9);
            GrdItemKeyDown(Grd19, e, BtnFind, BtnSave);
        }

        private void Grd20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd20, e, BtnSave);
            ComputeTotal(Grd20, TxtTotal10);
            GrdItemKeyDown(Grd20, e, BtnFind, BtnSave);
        }

        private void Grd21_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd21, e, BtnSave);
            ComputeTotal(Grd21, TxtTotal11);
            GrdItemKeyDown(Grd21, e, BtnFind, BtnSave);
        }

        private void Grd22_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd22, e, BtnSave);
            ComputeTotal(Grd22, TxtTotal12);
            GrdItemKeyDown(Grd22, e, BtnFind, BtnSave);
        }

        private void Grd23_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd23, e, BtnSave);
            ComputeTotal(Grd23, TxtTotal13);
            GrdItemKeyDown(Grd23, e, BtnFind, BtnSave);
        }

        private void Grd24_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd24, e, BtnSave);
            ComputeTotal(Grd24, TxtTotal14);
            GrdItemKeyDown(Grd24, e, BtnFind, BtnSave);
        }

        private void Grd25_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd25, e, BtnSave);
            ComputeTotal(Grd25, TxtTotal15);
            GrdItemKeyDown(Grd25, e, BtnFind, BtnSave);
        }

        private void Grd26_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd26, e, BtnSave);
            ComputeTotal(Grd26, TxtTotal16);
            GrdItemKeyDown(Grd26, e, BtnFind, BtnSave);
        }

        private void Grd27_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd27, e, BtnSave);
            ComputeTotal(Grd27, TxtTotal17);
            GrdItemKeyDown(Grd27, e, BtnFind, BtnSave);
        }

        private void Grd28_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd28, e, BtnSave);
            ComputeTotal(Grd28, TxtTotal18);
            GrdItemKeyDown(Grd28, e, BtnFind, BtnSave);
        }

        private void Grd29_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd29, e, BtnSave);
            ComputeTotal(Grd29, TxtTotal19);
            GrdItemKeyDown(Grd29, e, BtnFind, BtnSave);
        }

        private void Grd30_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd30, e, BtnSave);
            ComputeTotal(Grd30, TxtTotal20);
            GrdItemKeyDown(Grd30, e, BtnFind, BtnSave);
        }

        private void Grd31_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd31, e, BtnSave);
            ComputeTotal(Grd31, TxtTotal21);
            GrdItemKeyDown(Grd31, e, BtnFind, BtnSave);
        }

        private void Grd32_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd32, e, BtnSave);
            ComputeTotal(Grd32, TxtTotal22);
            GrdItemKeyDown(Grd32, e, BtnFind, BtnSave);
        }

        private void Grd33_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd33, e, BtnSave);
            ComputeTotal(Grd33, TxtTotal23);
            GrdItemKeyDown(Grd33, e, BtnFind, BtnSave);
        }

        private void Grd34_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd34, e, BtnSave);
            ComputeTotal(Grd34, TxtTotal24);
            GrdItemKeyDown(Grd34, e, BtnFind, BtnSave);
        }

        private void Grd35_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd35, e, BtnSave);
            ComputeTotal(Grd35, TxtTotal25);
            GrdItemKeyDown(Grd35, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd_AfterCommitEdit

        private void Grd11_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd11, TxtTotal1);
        }

        private void Grd12_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd12, TxtTotal2);
        }

        private void Grd13_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd13, TxtTotal3);
        }

        private void Grd14_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd14, TxtTotal4);
        }

        private void Grd15_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd15, TxtTotal5);
        }

        private void Grd16_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd16, TxtTotal6);
        }

        private void Grd17_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd17, TxtTotal7);
        }

        private void Grd18_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd18, TxtTotal8);
        }

        private void Grd19_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd19, TxtTotal9);
        }

        private void Grd20_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd20, TxtTotal10);
        }

        private void Grd21_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd21, TxtTotal11);
        }

        private void Grd22_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd22, TxtTotal12);
        }

        private void Grd23_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd23, TxtTotal13);
        }

        private void Grd24_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd24, TxtTotal14);
        }

        private void Grd25_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd25, TxtTotal15);
        }

        private void Grd26_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd26, TxtTotal16);
        }

        private void Grd27_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd27, TxtTotal17);
        }

        private void Grd28_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd28, TxtTotal18);
        }

        private void Grd29_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd29, TxtTotal19);
        }

        private void Grd30_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd30, TxtTotal20);
        }

        private void Grd31_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd31, TxtTotal21);
        }

        private void Grd32_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd32, TxtTotal22);
        }

        private void Grd33_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd33, TxtTotal23);
        }

        private void Grd34_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd34, TxtTotal24);
        }

        private void Grd35_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            GrdItemAfterCommitEdit(sender, e, Grd35, TxtTotal25);
        }

        #endregion

        #region Grd_CustomDrawCellForeground

        private void Grd11_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd11, sender, e);
        }

        private void Grd12_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd12, sender, e);
        }

        private void Grd13_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd13, sender, e);
        }

        private void Grd14_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd14, sender, e);
        }

        private void Grd15_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd15, sender, e);
        }

        private void Grd16_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd16, sender, e);
        }

        private void Grd17_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd17, sender, e);
        }

        private void Grd18_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd18, sender, e);
        }

        private void Grd19_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd19, sender, e);
        }

        private void Grd20_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd20, sender, e);
        }

        private void Grd21_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd21, sender, e);
        }

        private void Grd22_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd22, sender, e);
        }

        private void Grd23_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd23, sender, e);
        }

        private void Grd24_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd24, sender, e);
        }

        private void Grd25_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd25, sender, e);
        }

        private void Grd26_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd26, sender, e);
        }

        private void Grd27_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd27, sender, e);
        }

        private void Grd28_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd28, sender, e);
        }

        private void Grd29_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd29, sender, e);
        }

        private void Grd30_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd30, sender, e);
        }

        private void Grd31_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd31, sender, e);
        }

        private void Grd32_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd32, sender, e);
        }

        private void Grd33_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd33, sender, e);
        }

        private void Grd34_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd34, sender, e);
        }

        private void Grd35_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            GrdCustomDrawCellForeground(Grd35, sender, e);
        }

        #endregion

        #region Grd_ColHdrDoubleClick

        private void Grd11_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd11);
        }

        private void Grd12_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd12);
        }

        private void Grd13_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd13);
        }

        private void Grd14_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd14);
        }

        private void Grd15_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd15);
        }

        private void Grd16_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd16);
        }

        private void Grd17_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd17);
        }

        private void Grd18_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd18);
        }

        private void Grd19_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd19);
        }

        private void Grd20_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd20);
        }

        private void Grd21_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd21);
        }

        private void Grd22_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd22);
        }

        private void Grd23_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd23);
        }

        private void Grd24_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd24);
        }

        private void Grd25_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd25);
        }

        private void Grd26_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd26);
        }

        private void Grd27_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd27);
        }

        private void Grd28_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd28);
        }

        private void Grd29_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd29);
        }

        private void Grd30_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd30);
        }

        private void Grd31_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd31);
        }

        private void Grd32_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd32);
        }

        private void Grd33_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd33);
        }

        private void Grd34_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd34);
        }

        private void Grd35_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdItemColHdrDoubleClick(sender, e, Grd35);
        }

        #endregion

        #endregion

        #region Return

        private void GrdReturn_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled &&
                Sm.IsGrdColSelected(new int[] { 1, 2, 3 }, e.ColIndex))
            {
                if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
                {
                    LueLengthCodeRequestEdit(GrdReturn, LueLengthCode, ref fCell, ref fAccept, e);
                    //Sm.GrdRequestEdit(GrdReturn, e.RowIndex);
                    SetLueLengthCode(ref LueLengthCode);
                }
                Sm.GrdRequestEdit(GrdReturn, e.RowIndex);
                Sm.SetGrdNumValueZero(ref GrdReturn, GrdReturn.Rows.Count - 1, new int[] { 2 });
            }
        }

        private void GrdReturn_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(GrdReturn, e, BtnSave);
            GrdItemKeyDown(GrdReturn, e, BtnFind, BtnSave);
        }

        private void GrdReturn_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(GrdReturn, new int[] { 2 }, e);
            Sm.GrdAfterCommitEditTrimString(GrdReturn, new int[] { 3 }, e);
        }

        #endregion

        #endregion

        #endregion

        #region Report Class

        class RawMaterial
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CancelInd { get; set; }
            public string RawMaterialVerifyDocNo { get; set; }
            public string VdLog { get; set; }
            public string VdBalok { get; set; }

            public string DocType { get; set; }
            public string QueueNo { get; set; }
            public string VehicleRegNo { get; set; }
            public string LoadStart { get; set; }
            public string LoadEnd { get; set; }

            public string TTName { get; set; }
        }

        class RawMaterialDtl
        {
            public string Shelf { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Diameter { get; set; }
            public decimal Length { get; set; }
            public decimal Width { get; set; }
            public decimal Height { get; set; }
            public decimal Qty { get; set; }
        }

        class RawMaterialDtl1
        {
            public string Grid { get; set; }
            public string AsGrid { get; set; }
            public string BongDal { get; set; }
            public string BongLu { get; set; }
            public string Leg { get; set; }
            public string TTName { get; set; }
            public string LicenceNo { get; set; }
            public string Queue { get; set; }
        }

        #endregion

    }
}
