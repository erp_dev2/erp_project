﻿#region Update
/*
    10/07/2018 [TKG] merubah query untuk mempercepat proses setelah menghapus data.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPNT2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmPNT2Find FrmFind;
        internal int mNumberOfPlanningUomCode = 1;
        private decimal mHolidayWagesIndex = 1, mNotHolidayWagesIndex = 1;

        #endregion

        #region Constructor

        public FrmPNT2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Group of Direct Labor's Production Penalty";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetParameter();

                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                Sl.SetLueWagesFormulationCode(ref LueWagesFormulationCode);
                Sl.SetLueCurCode(ref LueCurCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "SFC#",
                        "SFC DNo",
                        "Date",
                        "Shift Code",

                        //6-10
                        "Shift",
                        "Item Code",
                        "Item Name",
                        "Index",
                        "Batch#",
                        
                        //11-15
                        "Quantity",
                        "UoM",
                        "Quantity 2",
                        "Uom 2",
                        "Holiday",
                        
                        //16-18
                        "Holiday"+Environment.NewLine+"Index", 
                        "Value",
                        "Holiday Description"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 130, 0, 80, 0, 
                    
                        //6-10
                        120, 100, 200, 80, 150, 

                        //11-15
                        80, 80, 80, 80, 80,   

                        //16-18
                        80, 80, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 15 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 13, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 5, 7, 13, 14, 18 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 17, 18 });
            Grd1.Cols[18].Move(15);

            #endregion

            #region Grd2

            Grd2.Cols.Count = 9;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Date",
                        "Shift Code",
                        "Shift",
                        "Value From",
                        "Value To",

                        //6-8
                        "Value",
                        "Penalty/Unit",
                        "Penalty"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        80, 0, 150, 100, 100, 
                        
                        //6-8
                        100, 130, 130                    
                    }
                );
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 4, 5, 6, 7, 8 }, 0);

            #endregion

            #region Grd3

            Grd3.Cols.Count = 17;
            Grd3.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Date",
                        "Shift Code",
                        "Shift",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        
                        //6-10
                        "Position",
                        "Department",
                        "Grade Level",
                        "Grade"+Environment.NewLine+"Level Index",
                        "Quantity",

                        //11-15
                        "Total Quantity",
                        "Penalty (Before"+Environment.NewLine+"Holiday Index)",
                        "Holiday",
                        "Holiday Index",
                        "Penalty",

                        //16
                        "Grade Level Code"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 0, 130, 100, 200, 
                        
                        //6-10
                        150, 150, 150, 100, 100, 
                        
                        //11-15
                        100, 150, 130, 100, 150,

                        //16
                        0
                    }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 1 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdFormatDec(Grd3, new int[] { 9, 10, 11, 12, 14, 15 }, 0);

            #endregion

            #region Grd4

            Grd4.Cols.Count = 7;
            Grd4.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Penalty",

                        //6
                        "Payrun Code"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 200, 150, 150, 150,

                        //6
                        100
                    }
                );
            Sm.GrdColInvisible(Grd4, new int[] { 0, 1, 3, 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDec(Grd4, new int[] { 5 }, 0);

            #endregion

            #region Grd5

            Grd5.Cols.Count = 16;
            Grd5.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Date",
                        "Shift Code",
                        "Shift",
                        "Coordinator's"+Environment.NewLine+"Code",
                        "Coordinator's Name",

                        //6-10
                        "Position",
                        "Department",
                        "Grade"+Environment.NewLine+"Level",
                        "Grade"+Environment.NewLine+"Level Index",
                        "Penalty (Before"+Environment.NewLine+"Holiday Index)",
                        
                        //11-15
                        "Holiday",
                        "Holiday"+Environment.NewLine+"Index", 
                        "Penalty",
                        "Number of Non"+Environment.NewLine+"Coordinator",
                        "Grade Level Code"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 0, 130, 100, 200, 
                    
                        //6-10
                        150, 150, 150, 120, 150,

                        //11-15
                        150, 100, 130, 130, 0
                    }
                );
            Sm.GrdFormatDate(Grd5, new int[] { 1 });
            Sm.GrdFormatDec(Grd5, new int[] { 9, 10, 12, 13 }, 0);
            Sm.GrdFormatDec(Grd5, new int[] { 14 }, 1);
            Sm.GrdColInvisible(Grd5, new int[] { 4, 6, 7, 8, 9, 11, 14, 15 }, false);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });

            #endregion

            #region Grd6

            Grd6.Cols.Count = 7;
            Grd6.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Penalty",

                        //6
                        "Payrun Code"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        100, 200, 150, 150, 150,

                        //6
                        100
                    }
                );
            Sm.GrdColInvisible(Grd6, new int[] { 0, 1, 3, 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDec(Grd6, new int[] { 5 }, 0);

            #endregion

            #region Grd7

            Grd7.Cols.Count = 14;
            Grd7.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "SFC#",
                        "DNo",
                        "Item Code",
                        "Item Name",
                        "Batch#",

                        //6-10
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "Processed"+Environment.NewLine+"Quantity",
                        "Balance",
                        "UoM",
                        "Outstanding"+Environment.NewLine+"Quantity 2",
                        
                        //11-13
                        "Processed"+Environment.NewLine+"Quantity 2",
                        "Balance 2",
                        "Uom 2"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        130, 0, 80, 250, 200,   

                        //6-10
                        100, 100, 100, 100, 100,

                        //11-13
                        100, 100, 100
                    }
                );
            Sm.GrdColInvisible(Grd7, new int[] { 0, 1, 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdFormatDec(Grd7, new int[] { 6, 7, 8, 10, 11, 12 }, 0);

            #endregion

            ShowPlanningUomCode();
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 7, 18 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 4, 6, 7, 8, 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd5, new int[] { 4, 6, 7, 8, 9, 11, 14 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd6, new int[] { 1, 3, 4 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd7, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowPlanningUomCode()
        {
            if (mNumberOfPlanningUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);
                Sm.GrdColInvisible(Grd7, new int[] { 10, 11, 12, 13 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, LueWorkCenterDocNo, LueWagesFormulationCode, 
                        TxtUomCode, LueCurCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 11, 13, 15 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueWorkCenterDocNo, LueCurCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 11, 13, 15 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueWorkCenterDocNo, LueWagesFormulationCode, TxtUomCode, 
                 LueCurCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            ClearGrd4();
            ClearGrd5();
            ClearGrd6();
            ClearGrd7();
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 15 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 11, 13, 16, 17 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 4, 5, 6, 7, 8 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 9, 10, 11, 12, 14, 15 });
        }

        private void ClearGrd4()
        {
            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 5 });
        }

        private void ClearGrd5()
        {
            Grd5.Rows.Clear();
            Grd5.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 9, 10, 12, 13, 14 });
        }

        private void ClearGrd6()
        {
            Grd6.Rows.Clear();
            Grd6.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 5 });
        }

        private void ClearGrd7()
        {
            Grd7.Rows.Clear();
            Grd7.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd7, 0, new int[] { 6, 7, 8, 10, 11, 12 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPNT2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueWorkCenterDocNo(ref LueWorkCenterDocNo, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private string GenerateDocNo(string DocDt, string DocType, string Tbl)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('00000000', Convert(DocNo+1, Char)), 8) From ( ");
            SQL.Append("       Select Convert(Left(DocNo, 8), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, 8) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '00000001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = GenerateDocNo(Sm.GetDte(DteDocDt), "PNT2", "TblPNT2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePNT2Hdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SavePNT2Dtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SavePNT2Dtl2(DocNo, Row));

            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0) cml.Add(SavePNT2Dtl3(DocNo, Row));

            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 2).Length > 0) cml.Add(SavePNT2Dtl4(DocNo, Row));

            
            int DNo = 1;
            for (int Row = 0; Row < Grd4.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    cml.Add(SavePNT2Dtl5a(DocNo, Sm.Right("00" + DNo, 3), Row));
                    DNo += 1;
                }
            }

            for (int Row = 0; Row < Grd6.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0)
                {
                    cml.Add(SavePNT2Dtl5b(DocNo, Sm.Right("00" + DNo, 3), Row));
                    DNo += 1;
                }
            }

            for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd7, Row, 1).Length > 0) cml.Add(SavePNT2Dtl6(DocNo, Row));

            cml.Add(UpdateSFCProcessInfo(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWorkCenterDocNo, "Work center") ||
                Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsSFCAlreadyCancelled() ||
                IsSFCAlreadyFulfilled() ||
                IsGrdValueNotValid();
        }

        private void RecomputeOutstanding()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, ");
            SQL.AppendLine("B.Qty-IfNull(C.Qty, 0)-IfNull(D.Qty, 0) As Qty, ");
            SQL.AppendLine("B.Qty2-IfNull(C.Qty2, 0)-IfNull(D.Qty2, 0) As Qty2 ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNTHdr T1  ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
            SQL.AppendLine("        And Locate(Concat('##', T3.DocNo, T3.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNT2Hdr T1  ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
            SQL.AppendLine("        And Locate(Concat('##', T3.DocNo, T3.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And B.DNo=D.DNo ");
            SQL.AppendLine("Order By A.DocNo, B.DNo;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@SelectedSFC", GetSelectedSFC2());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "Qty", "Qty2" });

                if (dr.HasRows)
                {
                    Grd7.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd7, Row, 1), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd7, Row, 2), Sm.DrStr(dr, 1))
                                )
                            {
                                Sm.SetGrdValue("N", Grd7, dr, c, Row, 6, 2);
                                Sm.SetGrdValue("N", Grd7, dr, c, Row, 10, 3);

                                Grd7.Cells[Row, 8].Value = Sm.GetGrdDec(Grd7, Row, 6) - Sm.GetGrdDec(Grd7, Row, 7);
                                Grd7.Cells[Row, 12].Value = Sm.GetGrdDec(Grd7, Row, 10) - Sm.GetGrdDec(Grd7, Row, 11);
                                break;
                            }
                        }
                    }
                    Grd7.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsSFCAlreadyCancelled()
        {
            string DocNo = string.Empty;
            return Sm.IsDataNotValid(
                "Select DocNo From TblShopFloorControlHdr " +
                "Where CancelInd='Y' And Locate(Concat('##', DocNo, '##'), @Param)>0 Limit 1;",
                GetSelectedSFC2(), ref DocNo,
                "SFC# : " + DocNo + Environment.NewLine + "This document already cancelled.");
        }

        private bool IsSFCAlreadyFulfilled()
        {
            string Msg = string.Empty;
            if (Grd7.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 1).Length != 0)
                    {
                        Msg =
                            "SFC# : " + Sm.GetGrdStr(Grd7, Row, 1) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd7, Row, 4) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd7, Row, 5) + Environment.NewLine + Environment.NewLine;

                        if (IsSFCAlreadyFulfilled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "This document already fulfilled.");
                            Sm.FocusGrd(Grd7, Row, 1);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsSFCAlreadyFulfilled(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblShopFloorControlDtl " +
                    "Where ProcessInd2='F' And DocNo=@DocNo And DNo=@DNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd7, Row, 1));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd7, Row, 2));
            return Sm.IsDataExist(cm);
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    Msg =
                        "SFC# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine;

                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "SFC# should not be empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 11, true, Msg + "Quantity should be greater than 0.")) return true;
                    if (Grd1.Cols[13].Visible && Sm.IsGrdValueEmpty(Grd1, Row, 13, true, Msg + "Quantity (2) should be greater than 0.")) return true;
                }
            }

            if (Grd7.Rows.Count > 1)
            {
                //RecomputeOutstanding();

                for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 1).Length != 0)
                    {
                        Msg =
                            "SFC# : " + Sm.GetGrdStr(Grd7, Row, 1) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd7, Row, 4) + Environment.NewLine +
                            "Batch# : " + Sm.GetGrdStr(Grd7, Row, 5) + Environment.NewLine + Environment.NewLine;

                        if (Sm.GetGrdStr(Grd7, Row, 8).Length != 0 &&
                            decimal.Parse(Sm.GetGrdStr(Grd7, Row, 8)) < 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Balance should not be less than 0.");
                            Sm.FocusGrd(Grd7, Row, 8);
                            return true;
                        }

                        if (Grd7.Cols[12].Visible &&
                            Sm.GetGrdStr(Grd7, Row, 12).Length != 0 &&
                             decimal.Parse(Sm.GetGrdStr(Grd7, Row, 12)) < 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Balance (2) should not be less than 0.");
                            Sm.FocusGrd(Grd7, Row, 12);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 SFC.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePNT2Hdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPNT2Hdr(DocNo, DocDt, CancelInd, WorkCenterDocNo, WagesFormulationCode, UomCode, CurCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @WorkCenterDocNo, @WagesFormulationCode, @UomCode, @CurCode, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetLue(LueWorkCenterDocNo));
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePNT2Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNT2Dtl(DocNo, DNo, ShopFloorControlDocNo, ShopFloorControlDNo, DocDt, ProductionShiftCode, Qty, Qty2, ItIndex, HolInd, HolIndex, Value, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ShopFloorControlDocNo, @ShopFloorControlDNo, @DocDt, @ProductionShiftCode, @Qty, @Qty2, @ItIndex, @HolInd, @HolIndex, @Value, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetGrdDate(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@ItIndex", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@HolInd", Sm.GetGrdBool(Grd1, Row, 15)?"Y":"N");
            Sm.CmParam<Decimal>(ref cm, "@HolIndex", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNT2Dtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNT2Dtl2(DocNo, DNo, DocDt, ProductionShiftCode, ValueFrom, ValueTo, Value, Penalty, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocDt, @ProductionShiftCode, @ValueFrom, @ValueTo, @Value, @Penalty, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetGrdDate(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@ValueFrom", Sm.GetGrdDec(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@ValueTo", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNT2Dtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNT2Dtl3(DocNo, DNo, DocDt, ProductionShiftCode, EmpCode, GrdLvlCode, GrdLvlIndex, HolIndex, Qty, TotalQty, Amt1, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocDt, @ProductionShiftCode, @EmpCode, @GrdLvlCode, @GrdLvlIndex, @HolIndex, @Qty, @TotalQty, @Amt1, @Amt2, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetGrdDate(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@HolIndex", Sm.GetGrdDec(Grd3, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@TotalQty", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd3, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd3, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNT2Dtl4(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNT2Dtl4(DocNo, DNo, DocDt, ProductionShiftCode, EmpCode, GrdLvlCode, GrdLvlIndex, HolIndex, NoOfEmp, Amt1, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @DocDt, @ProductionShiftCode, @EmpCode, @GrdLvlCode, @GrdLvlIndex, @HolIndex, @NoOfEmp, @Amt1, @Amt2, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetGrdDate(Grd5, Row, 1));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd5, Row, 4));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd5, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd5, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@HolIndex", Sm.GetGrdDec(Grd5, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@NoOfEmp", Sm.GetGrdDec(Grd5, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd5, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd5, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNT2Dtl5a(string DocNo, string DNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNT2Dtl5(DocNo, DNo, ProcessInd, CoordinatorInd, EmpCode, Penalty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'O', 'N', @EmpCode, @Penalty, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNT2Dtl5b(string DocNo, string DNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNT2Dtl5(DocNo, DNo, ProcessInd, CoordinatorInd, EmpCode, Penalty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'O', 'Y', @EmpCode, @Penalty, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd6, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd6, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePNT2Dtl6(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPNT2Dtl6(DocNo, DNo, ShopFloorControlDocNo, ShopFloorControlDNo, Qty, Qty2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ShopFloorControlDocNo, @ShopFloorControlDNo, @Qty, @Qty2, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", Sm.GetGrdStr(Grd7, Row, 1));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", Sm.GetGrdStr(Grd7, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd7, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd7, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateSFCProcessInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblShopFloorControlDtl A ");
            SQL.AppendLine("Inner Join TblPNT2Dtl6 B ");
            SQL.AppendLine("    On A.DocNo=B.ShopFloorControlDocNo ");
            SQL.AppendLine("    And A.DNo=B.ShopFloorControlDNo ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNTHdr T1  ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNT2Hdr T1  ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("Set A.ProcessInd2=");
            SQL.AppendLine("    If(A.Qty+A.Qty2<>0 And IfNull(C.Qty, 0)+IfNull(C.Qty2, 0)+IfNull(D.Qty, 0)+IfNull(D.Qty2, 0)=0, 'O', ");
            SQL.AppendLine("        If(A.Qty+A.Qty2-IfNull(C.Qty, 0)-IfNull(C.Qty2, 0)-IfNull(D.Qty, 0)-IfNull(D.Qty2, 0)=0, 'F', 'P') ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Update TblShopFloorControlHdr T ");
            SQL.AppendLine("Set T.ProcessInd2=");
            SQL.AppendLine("    Case When Not Exists(Select DocNo From TblShopFloorControlDtl Where DocNo=T.DocNo And ProcessInd2<>'O' Limit 1) Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        If( ");
            SQL.AppendLine("        Not Exists(Select DocNo From TblShopFloorControlDtl Where DocNo=T.DocNo And ProcessInd2<>'F' Limit 1), ");
            SQL.AppendLine("            'F', 'P') ");
            SQL.AppendLine("    End ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select ShopFloorControlDocNo From TblPNT2Dtl6 Where DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPNT2Hdr());
            cml.Add(UpdateSFCProcessInfo(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToPPR();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPNT2Hdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessedToPPR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPNT2Dtl5 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to production payrun.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditPNT2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPNT2Hdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowPNT2Hdr(DocNo);
                ShowPNT2Dtl(DocNo);
                ShowPNT2Dtl2(DocNo);
                ShowPNT2Dtl3(DocNo);
                ShowPNT2Dtl4(DocNo);
                ShowPNT2Dtl5a(DocNo);
                ShowPNT2Dtl5b(DocNo);
                ShowPNT2Dtl6(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPNT2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, WorkCenterDocNo, WagesFormulationCode, UomCode, CurCode, Remark " +
                    "From TblPNT2Hdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "WorkCenterDocNo", "WagesFormulationCode", "UomCode", 

                        //6-7
                        "CurCode", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = (Sm.DrStr(dr, c[2]) == "Y");
                        Sl.SetLueWorkCenterDocNo(ref LueWorkCenterDocNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueWorkCenterDocNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueWagesFormulationCode, Sm.DrStr(dr, c[4]));
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[6]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowPNT2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ShopFloorControlDocNo, A.ShopFloorControlDNo, B.DocDt, B.ProductionShiftCode, C.ProductionShiftName, ");
            SQL.AppendLine("D.ItCode, E.ItName, A.ItIndex, D.BatchNo, ");
            SQL.AppendLine("A.Qty, E.PlanningUomCode, A.Qty2, E.PlanningUomCode2, ");
            SQL.AppendLine("F.HolName, A.HolInd, A.HolIndex, A.Value ");
            SQL.AppendLine("From TblPNT2Dtl A ");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr B On A.ShopFloorControlDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblProductionShift C On B.ProductionShiftCode=C.ProductionShiftCode ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl D On A.ShopFloorControlDocNo=D.DocNo And A.ShopFloorControlDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblHoliday F On B.DocDt=F.HolDt ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "ShopFloorControlDocNo", "ShopFloorControlDNo", "DocDt", "ProductionShiftCode", "ProductionShiftName",  
                    
                    //6-10
                    "ItCode", "ItName", "ItIndex", "BatchNo", "Qty", 
                    
                    //11-15
                    "PlanningUomCode", "Qty2", "PlanningUomCode2", "HolInd", "HolIndex", 
                    
                    //16-17
                    "Value", "HolName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 11, 13, 16, 17 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPNT2Dtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocDt, A.ProductionShiftCode, B.ProductionShiftName, ");
            SQL.AppendLine("A.ValueFrom, A.ValueTo, A.Value, A.Penalty, A.Amt ");
            SQL.AppendLine("From TblPNT2Dtl2 A ");
            SQL.AppendLine("Left Join TblProductionShift B On A.ProductionShiftCode=B.ProductionShiftCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "DocDt", "ProductionShiftCode", "ProductionShiftName", "ValueFrom", "ValueTo", 
                    
                    //6-8
                    "Value", "Penalty", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowPNT2Dtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocDt, A.ProductionShiftCode, B.ProductionShiftName, ");
            SQL.AppendLine("A.EmpCode, C.EmpName, D.PosName, E.DeptName, F.GrdLvlName, A.Qty, A.TotalQty, ");
            SQL.AppendLine("A.GrdLvlIndex, A.Amt1, G.HolName, A.HolIndex, A.Amt2, A.GrdLvlCode ");
            SQL.AppendLine("From TblPNT2Dtl3 A ");
            SQL.AppendLine("Left Join TblProductionShift B On A.ProductionShiftCode=B.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr F On A.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblHoliday G On A.DocDt=G.HolDt ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "DocDt", "ProductionShiftCode", "ProductionShiftName", "EmpCode", "EmpName", 
                    
                    //6-10
                    "PosName", "DeptName", "GrdLvlName", "GrdLvlIndex", "Qty",  
 
                    //11-15
                    "TotalQty", "Amt1", "HolName", "HolIndex", "Amt2",

                    //16
                    "GrdLvlCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 9, 10, 11, 12, 14, 15 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowPNT2Dtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocDt, A.ProductionShiftCode, B.ProductionShiftName, ");
            SQL.AppendLine("A.EmpCode, C.EmpName, D.PosName, E.DeptName, F.GrdLvlName, ");
            SQL.AppendLine("A.GrdLvlIndex, A.Amt1, G.HolName, A.HolIndex, A.Amt2, A.NoOfEmp, A.GrdLvlCode ");
            SQL.AppendLine("From TblPNT2Dtl4 A ");
            SQL.AppendLine("Left Join TblProductionShift B On A.ProductionShiftCode=B.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr F On A.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblHoliday G On A.DocDt=G.HolDt ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd5, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "DocDt", "ProductionShiftCode", "ProductionShiftName", "EmpCode", "EmpName", 
                    
                    //6-10
                    "PosName", "DeptName", "GrdLvlName", "GrdLvlIndex", "Amt1", 
 
                    //11-15
                    "HolName", "HolIndex", "Amt2", "NoOfEmp", "GrdLvlCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 15);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 9, 10, 12, 13, 14 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ShowPNT2Dtl5a(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, A.Penalty, A.PayrunCode ");
            SQL.AppendLine("From TblPNT2Dtl5 A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.CoordinatorInd='N' And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "PosName", "DeptName", "Penalty",

                    //6
                    "PayrunCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowPNT2Dtl5b(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, A.Penalty, A.PayrunCode ");
            SQL.AppendLine("From TblPNT2Dtl5 A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.CoordinatorInd='Y' And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd6, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "PosName", "DeptName", "Penalty",

                    //6
                    "PayrunCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd6, 0, 1);
        }

        private void ShowPNT2Dtl6(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ShopFloorControlDocNo, A.ShopFloorControlDNo, ");
            SQL.AppendLine("C.ItCode, D.ItName, C.BatchNo, ");
            SQL.AppendLine("C.Qty-IfNull(F.Qty, 0)-IfNull(G.Qty, 0)+if(E.CancelInd='N', A.Qty, 0) As OutstandingQty, ");
            SQL.AppendLine("A.Qty, D.PlanningUomCode, ");
            SQL.AppendLine("C.Qty2-IfNull(F.Qty2, 0)-IfNull(G.Qty2, 0)+if(E.CancelInd='N', A.Qty2, 0) As OutstandingQty2, ");
            SQL.AppendLine("A.Qty2, D.PlanningUomCode2 ");
            SQL.AppendLine("From TblPNT2Dtl6 A ");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr B On A.ShopFloorControlDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl C On A.ShopFloorControlDocNo=C.DocNo And A.ShopFloorControlDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblPNT2Hdr E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNTHdr T1 ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N'  ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") F On A.ShopFloorControlDocNo=F.DocNo And A.ShopFloorControlDNo=F.DNo ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNT2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.ShopFloorControlDocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.ShopFloorControlDNo ");
            SQL.AppendLine("        And T3.DocNo=@DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N'  ");
            SQL.AppendLine("    And T1.DocNo<>@DocNo ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") G On A.ShopFloorControlDocNo=G.DocNo And A.ShopFloorControlDNo=G.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd7, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "DNo", 

                    //1-5
                    "ShopFloorControlDocNo", "ShopFloorControlDNo", "ItCode", "ItName", "BatchNo", 
                    
                    //6-10
                    "OutstandingQty", "Qty", "PlanningUomCode", "OutstandingQty2", "Qty2", 
                    
                    //11
                    "PlanningUomCode2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Grd.Cells[Row, 8].Value = Sm.GetGrdDec(Grd, Row, 6) - Sm.GetGrdDec(Grd, Row, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Grd.Cells[Row, 12].Value = Sm.GetGrdDec(Grd, Row, 10) - Sm.GetGrdDec(Grd, Row, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 6, 7, 8, 10, 11, 12 });
            Sm.FocusGrd(Grd7, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetParameter()
        {
            var Param = string.Empty;

            Param = Sm.GetParameter("NumberOfPlanningUomCode");
            if (Param.Length != 0) mNumberOfPlanningUomCode = int.Parse(Param);

            Param = Sm.GetParameter("HolidayIndex");
            if (Param.Length != 0)
            {
                mHolidayWagesIndex = Decimal.Parse(Param)/10m;
            }
            Param = Sm.GetParameter("NotHolidayWagesIndex");
            if (Param.Length != 0) mNotHolidayWagesIndex = Decimal.Parse(Param);

        }

        private void ShowWagesFormulationUom()
        {
            try
            {
                TxtUomCode.EditValue = null;
                var WagesFormulationCode = Sm.GetLue(LueWagesFormulationCode);

                if (WagesFormulationCode.Length != 0)
                {
                    var cm = new MySqlCommand()
                    {
                        CommandText =
                            "Select UomCode From TblWagesFormulationHdr " +
                            "Where WagesFormulationCode=@WagesFormulationCode;"
                    };
                    Sm.CmParam<String>(ref cm, "@WagesFormulationCode", WagesFormulationCode);
                    TxtUomCode.EditValue = Sm.GetValue(cm);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        internal string GetSelectedData()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedSFC()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedSFC2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedNonCoordinator()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 4).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 4) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedCoordinator()
        {
            var SQL = string.Empty;
            if (Grd5.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 4).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd5, Row, 4) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ShowDocInfo()
        {
            try
            {
                ClearGrd2();
                ClearGrd3();
                ClearGrd4();
                ClearGrd5();
                ClearGrd6();
                ClearGrd7();

                ShowPenaltyBasedOnFormula();
                ShowSCFItem();
                ShowNonCoordinatorSCF();
                ShowCoordinatorSCF();
                ShowNonCoordinatorPenalty();
                ShowCoordinatorPenalty();
                ComputeItemProcessed();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        ComputePenaltyBasedOnFormula(Sm.GetGrdDate(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 5));

                ComputeNonCoordinatorSCFPenalty();
                ComputeCoordinatorSCFPenalty();
                ComputeNonCoordinatorPenalty();
                ComputeCoordinatorPenalty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowPenaltyBasedOnFormula()
        {
            var SQL = new StringBuilder();

            var cm = new MySqlCommand();
            var Filter = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 2));
                        No += 1;
                    }
                }
            }

            if (Filter.Length == 0) Filter = " 0=1 ";

            SQL.AppendLine("Select A.DocDt, A.ProductionShiftCode, B.ProductionShiftName, ");
            SQL.AppendLine("C.ValueFrom, C.ValueTo, C.Penalty ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct DocDt, ProductionShiftCode ");
            SQL.AppendLine("    From TblShopFloorControlHdr ");
            SQL.AppendLine("    Where ("+Filter+") ");
            //SQL.AppendLine("     Where Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblProductionShift B On A.ProductionShiftCode=B.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl C On C.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Order By A.DocDt, A.ProductionShiftCode;");

            //var cm = new MySqlCommand();

            //Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC2());
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));

            Sm.ShowDataInGrid(
                   ref Grd2, ref cm, SQL.ToString(),
                   new string[] 
                   { 
                       //0
                       "DocDt",

                       //1-5
                       "ProductionShiftCode", "ProductionShiftName", "ValueFrom", "ValueTo", "Penalty"
                   },
                   (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                   {
                       Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                       Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                       Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                       Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                       Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                       Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                       Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 6, 8 });
                   }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ComputeValue(int Row)
        {
            string UomCode = TxtUomCode.Text;

            Decimal Qty = 0m, ItemIndex = 0m, HolidayIndex = 0m;

            if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd1, Row, 12)))
            {
                if (Sm.GetGrdStr(Grd1, Row, 11).Length > 0) Qty = Sm.GetGrdDec(Grd1, Row, 11);
            }
            else
            {
                if (Sm.CompareStr(UomCode, Sm.GetGrdStr(Grd1, Row, 14)))
                {
                    if (Sm.GetGrdStr(Grd1, Row, 13).Length > 0) Qty = Sm.GetGrdDec(Grd1, Row, 13);
                }
            }

            if (Sm.GetGrdStr(Grd1, Row, 9).Length > 0) ItemIndex = Sm.GetGrdDec(Grd1, Row, 9);
            if (Sm.GetGrdStr(Grd1, Row, 16).Length > 0) HolidayIndex = Sm.GetGrdDec(Grd1, Row, 16);
            Grd1.Cells[Row, 17].Value = Qty * ItemIndex * HolidayIndex;
        }

        private void ComputePenaltyBasedOnFormula(string DocDt, string ProductionShiftCode)
        {
            decimal Value = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row1 = 0; Row1 < Grd1.Rows.Count; Row1++)
                    if (
                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd1, Row1, 4)) &&
                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd1, Row1, 5))
                        )
                        Value += Sm.GetGrdDec(Grd1, Row1, 17);
            }

            int Row = -1;
            if (Grd2.Rows.Count > 1)
            {
                //mendapatkan baris untuk empcode
                for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                {
                    if (
                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2))
                        )
                    {
                        Row = Row2;
                        break;
                    }
                }

                if (Row >= 0)
                {
                    //set 0 utk value dan amount 
                    for (int Row2 = Row; Row2 < Grd2.Rows.Count; Row2++)
                    {
                        if (
                            Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                            Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2))
                            )
                        {
                            Grd2.Cells[Row2, 6].Value = 0m;
                            Grd2.Cells[Row2, 8].Value = 0m;
                        }
                        else
                            break;
                    }

                    decimal Temp = Value, To = 0m;

                    for (int Row2 = Row; Row2 < Grd2.Rows.Count; Row2++)
                    {
                        if (
                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2))
                        )
                        {
                            if (Row2 == Row && Value < Sm.GetGrdDec(Grd2, Row2, 4))
                            {
                                Temp = 0m;
                                break;
                            }

                            To = Sm.GetGrdDec(Grd2, Row2, 5);

                            if (Temp <= To)
                            {
                                Grd2.Cells[Row2, 6].Value = Temp;
                                Grd2.Cells[Row2, 8].Value = Temp * Sm.GetGrdDec(Grd2, Row2, 7);
                                Temp = 0m;
                            }
                            else
                            {
                                Grd2.Cells[Row2, 6].Value = To;
                                Grd2.Cells[Row2, 8].Value = To * Sm.GetGrdDec(Grd2, Row2, 7);
                                Temp -= To;
                            }

                            if (Temp == 0m) break;
                        }
                    }
                }
            }
        }

        private void ShowSCFItem()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (Tbl.DocNo=@DocNo" + No + " And Tbl.DNo=@DNo" + No + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No, Sm.GetGrdStr(Grd1, Row, 2));
                        Sm.CmParam<String>(ref cm, "@DNo" + No, Sm.GetGrdStr(Grd1, Row, 3));
                        No += 1;
                    }
                }
            }

            if (Filter.Length == 0) Filter = " 0=1 ";

            SQL.AppendLine("Select A.DocNo, B.DNo, ");
            SQL.AppendLine("B.ItCode, B.BatchNo, E.ItName, E.PlanningUomCode, E.PlanningUomCode2, ");
            SQL.AppendLine("B.Qty-IfNull(C.Qty, 0)-IfNull(D.Qty, 0) As Qty, ");
            SQL.AppendLine("B.Qty2-IfNull(C.Qty2, 0)-IfNull(D.Qty2, 0) As Qty2 ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
//            SQL.AppendLine("    And Locate(Concat('##', B.DocNo, B.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    And ( " + Filter.Replace("Tbl.", "B.") + ") ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNTHdr T1  ");
            SQL.AppendLine("    Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
            //SQL.AppendLine("        And Locate(Concat('##', T3.DocNo, T3.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("        And ( " + Filter.Replace("Tbl.", "T3.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T2.ShopFloorControlDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2 ");
            SQL.AppendLine("    From TblPNT2Hdr T1  ");
            SQL.AppendLine("    Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T3.DNo ");
            //SQL.AppendLine("        And Locate(Concat('##', T3.DocNo, T3.DNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("        And ( " + Filter.Replace("Tbl.", "T3.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And B.DNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode=E.ItCode");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocNo, B.ItCode;");

            //Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC());
            Sm.ShowDataInGrid(
                ref Grd7, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DNo", "ItCode", "ItName", "BatchNo", "Qty", 
                    
                    //6-8
                    "PlanningUomCode", "Qty2", "PlanningUomCode2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Grd.Cells[Row, 7].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Grd.Cells[Row, 11].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd7, Grd7.Rows.Count - 1, new int[] { 6, 7, 8, 10, 11, 12 });
            Sm.FocusGrd(Grd7, 0, 0);
        }

        private void ComputeItemProcessed()
        {
            decimal Qty = 0m, Qty2 = 0m;
            string key = string.Empty;
            if (Grd7.Rows.Count > 1)
            {
                for (int Row7 = 0; Row7 < Grd7.Rows.Count; Row7++)
                {
                    if (Sm.GetGrdStr(Grd7, Row7, 1).Length != 0)
                    {
                        key = Sm.GetGrdStr(Grd7, Row7, 1) + Sm.GetGrdStr(Grd7, Row7, 2);
                        Qty = 0m;
                        Qty2 = 0m;
                        for (int Row1 = 0; Row1 < Grd1.Rows.Count; Row1++)
                        {
                            if (
                                Sm.GetGrdStr(Grd1, Row1, 2).Length != 0 &&
                                Sm.CompareStr(key, Sm.GetGrdStr(Grd1, Row1, 2) + Sm.GetGrdStr(Grd1, Row1, 3))
                                )
                            {
                                Qty += Sm.GetGrdDec(Grd1, Row1, 11);
                                Qty2 += Sm.GetGrdDec(Grd1, Row1, 13);
                            }
                        }
                        Grd7.Cells[Row7, 7].Value = Qty;
                        Grd7.Cells[Row7, 8].Value = Sm.GetGrdDec(Grd7, Row7, 6) - Qty;
                        Grd7.Cells[Row7, 11].Value = Qty2;
                        Grd7.Cells[Row7, 12].Value = Sm.GetGrdDec(Grd7, Row7, 10) - Qty2;
                    }
                }
            }
        }

        private void ShowNonCoordinatorPenalty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, C.DeptName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Where Locate(Concat('##', A.EmpCode, '##'), @Param)>0 ");
            SQL.AppendLine("Order By C.DeptName, A.EmpName, A.EmpCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Param", GetSelectedNonCoordinator());
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-3
                               "EmpName", "PosName", "DeptName"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 5 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowCoordinatorPenalty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, C.DeptName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Where Locate(Concat('##', A.EmpCode, '##'), @Param)>0 ");
            SQL.AppendLine("Order By C.DeptName, A.EmpName, A.EmpCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Param", GetSelectedCoordinator());
            Sm.ShowDataInGrid(
                    ref Grd6, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-3
                               "EmpName", "PosName", "DeptName"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 5 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd6, 0, 1);
        }

        private void ComputeNonCoordinatorPenalty()
        {
            decimal Penalty = 0m;
            string key = string.Empty;
            if (Grd4.Rows.Count > 1)
            {
                for (int Row4 = 0; Row4 < Grd4.Rows.Count; Row4++)
                {
                    if (Sm.GetGrdStr(Grd4, Row4, 1).Length != 0)
                    {
                        key = Sm.GetGrdStr(Grd4, Row4, 1);
                        Penalty = 0m;

                        for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                        {
                            if (
                                Sm.GetGrdStr(Grd3, Row3, 1).Length != 0 &&
                                Sm.CompareStr(key, Sm.GetGrdStr(Grd3, Row3, 4))
                                )
                                Penalty += Sm.GetGrdDec(Grd3, Row3, 15);
                        }
                        Grd4.Cells[Row4, 5].Value = Penalty;
                    }
                }
            }
        }

        private void ComputeCoordinatorPenalty()
        {
            decimal Penalty = 0m;
            string key = string.Empty;

            if (Grd6.Rows.Count > 1)
            {
                for (int Row6 = 0; Row6 < Grd6.Rows.Count; Row6++)
                {
                    if (Sm.GetGrdStr(Grd6, Row6, 1).Length != 0)
                    {
                        key = Sm.GetGrdStr(Grd6, Row6, 1);
                        Penalty = 0m;

                        for (int Row5 = 0; Row5 < Grd5.Rows.Count; Row5++)
                            if (Sm.CompareStr(key, Sm.GetGrdStr(Grd5, Row5, 4)))
                                Penalty += Sm.GetGrdDec(Grd5, Row5, 13);

                        Grd6.Cells[Row6, 5].Value = Penalty;
                    }
                }
            }
        }

        private void ShowNonCoordinatorSCF()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.ProductionShiftCode, I.ProductionShiftName, ");
            SQL.AppendLine("B.EmpCode, C.EmpName, D.PosName, E.DeptName, F.GrdLvlName, ");
            SQL.AppendLine("IfNull(G.GrdLvlIndexP, 1) As GrdLvlIndex, ");
            SQL.AppendLine("IfNull(J.Qty, 0) As Qty, ");
            SQL.AppendLine("IfNull(K.TotalQty, 0) As TotalQty, ");
            SQL.AppendLine("H.HolName, if(IfNull(H.HolName, '')='', 1, @HolidayWagesIndex) As HolidayPenaltyIndex, B.GrdLvlCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct DocDt, ProductionShiftCode ");
            SQL.AppendLine("    From TblShopFloorControlHdr ");
            SQL.AppendLine("    Where Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.DocDt, T.ProductionShiftCode, T.EmpCode, ");
            SQL.AppendLine("    (");
            SQL.AppendLine("        Select GrdLvlCode From TblEmployeePPS ");
            SQL.AppendLine("        Where EmpCode=T.EmpCode ");
            SQL.AppendLine("        And StartDt<=T.DocDt And (EndDt Is Null Or EndDt>=T.DocDt) ");
            SQL.AppendLine("        Order By StartDt Desc Limit 1 "); //sepertinya tidak perlu
            SQL.AppendLine("    ) As GrdLvlCode ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            SQL.AppendLine("        From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("        Inner Join TblShopFloorControl2Dtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And T2.BomType='3' ");
            SQL.AppendLine("            And T2.CoordinatorInd='N' ");
            SQL.AppendLine("        Where Locate(Concat('##', T1.DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") B On A.DocDt=B.DocDt And A.ProductionShiftCode=B.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblEmployee C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr F On B.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 G ");
            SQL.AppendLine("    On B.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("    And G.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Left Join TblHoliday H On A.DocDt=H.HolDt ");
            SQL.AppendLine("Left Join TblProductionShift I On A.ProductionShiftCode=I.ProductionShiftCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select  T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty ");
            SQL.AppendLine("    From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControl2Dtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.BomType='3' ");
            SQL.AppendLine("        And T2.CoordinatorInd='N' ");
            SQL.AppendLine("    Where Locate(Concat('##', T1.DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Group By T1.DocDt, T1.ProductionShiftCode, T2.BomCode ");
            SQL.AppendLine(") J ");
            SQL.AppendLine("    On B.DocDt=J.DocDt ");
            SQL.AppendLine("    And B.ProductionShiftCode=J.ProductionShiftCode ");
            SQL.AppendLine("    And B.EmpCode=J.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select  T1.DocDt, T1.ProductionShiftCode, ");
            SQL.AppendLine("    Sum(T2.Qty) As TotalQty ");
            SQL.AppendLine("    From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControl2Dtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.BomType='3' ");
            SQL.AppendLine("        And T2.CoordinatorInd='N' ");
            SQL.AppendLine("    Where Locate(Concat('##', T1.DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    Group By T1.DocDt, T1.ProductionShiftCode ");
            SQL.AppendLine(") K ");
            SQL.AppendLine("    On A.DocDt=K.DocDt ");
            SQL.AppendLine("    And A.ProductionShiftCode=K.ProductionShiftCode ");
            SQL.AppendLine("Order By A.DocDt, A.ProductionShiftCode, C.EmpName, B.EmpCode;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC2());
            Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex", 1);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] 
                        {
                           //0
                           "DocDt",

                           //1-5
                           "ProductionShiftCode", "ProductionShiftName", "EmpCode", "EmpName", "PosName", 

                           //6-10
                           "DeptName", "GrdLvlName", "GrdLvlIndex", "Qty", "TotalQty", 
            
                           //11-13
                           "HolName", "HolidayPenaltyIndex", "GrdLvlCode"
                        },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 12, 15 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 9, 10, 11, 12, 14, 15 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ComputeNonCoordinatorSCFPenalty(string DocDt, string ProductionShiftCode)
        {
            decimal Penalty = 0m, Amt = 0m, Qty = 0m, TotalQty = 0m, GrdLvlIndex = 0m, HolidayIndex = 1m;

            if (Grd2.Rows.Count > 1 && Grd3.Rows.Count > 1)
            {
                for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                {
                    if (
                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd3, Row3, 1)) &&
                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd3, Row3, 2))
                        )
                    {
                        Penalty = 0m;
                        Amt = 0m;
                        Qty = 0m;
                        TotalQty = 0m;
                        GrdLvlIndex = 0m;
                        HolidayIndex = 1m;

                        for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                            if (
                                Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                                Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2))
                                )
                                Amt += Sm.GetGrdDec(Grd2, Row2, 8);

                        if (Sm.GetGrdStr(Grd3, Row3, 9).Length > 0) GrdLvlIndex = Sm.GetGrdDec(Grd3, Row3, 9);
                        if (Sm.GetGrdStr(Grd3, Row3, 10).Length > 0) Qty = Sm.GetGrdDec(Grd3, Row3, 10);
                        if (Sm.GetGrdStr(Grd3, Row3, 11).Length > 0) TotalQty = Sm.GetGrdDec(Grd3, Row3, 11);
                        if (Sm.GetGrdStr(Grd3, Row3, 14).Length > 0) HolidayIndex = Sm.GetGrdDec(Grd3, Row3, 14);

                        if (TotalQty != 0) Penalty = (Qty / TotalQty) * Amt * GrdLvlIndex;

                        Grd3.Cells[Row3, 12].Value = Penalty;
                        Grd3.Cells[Row3, 15].Value = Penalty * HolidayIndex;
                    }
                }
            }
        }

        private void ComputeNonCoordinatorSCFPenalty()
        {
            decimal Penalty = 0m, Amt = 0m, Qty = 0m, TotalQty = 0m, GrdLvlIndex = 0m, HolidayIndex = 0m;

            if (Grd2.Rows.Count > 1 && Grd3.Rows.Count > 1)
            {
                for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                {
                    if (Sm.GetGrdStr(Grd3, Row3, 1).Length > 0)
                    {
                        Penalty = 0m;
                        Amt = 0m;
                        Qty = 0m;
                        TotalQty = 0m;
                        GrdLvlIndex = 0m;
                        HolidayIndex = 0m;

                        for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                            if (
                                Sm.CompareStr(Sm.GetGrdDate(Grd3, Row3, 1), Sm.GetGrdDate(Grd2, Row2, 1)) &&
                                Sm.CompareGrdStr(Grd3, Row3, 2, Grd2, Row2, 2)
                                )
                                Amt += Sm.GetGrdDec(Grd2, Row2, 8);

                        if (Sm.GetGrdStr(Grd3, Row3, 9).Length > 0) GrdLvlIndex = Sm.GetGrdDec(Grd3, Row3, 9);
                        if (Sm.GetGrdStr(Grd3, Row3, 10).Length > 0) Qty = Sm.GetGrdDec(Grd3, Row3, 10);
                        if (Sm.GetGrdStr(Grd3, Row3, 11).Length > 0) TotalQty = Sm.GetGrdDec(Grd3, Row3, 11);
                        if (Sm.GetGrdStr(Grd3, Row3, 14).Length > 0) HolidayIndex = Sm.GetGrdDec(Grd3, Row3, 14);

                        if (TotalQty != 0) Penalty = (Qty / TotalQty) * Amt * GrdLvlIndex;

                        Grd3.Cells[Row3, 12].Value = Penalty;
                        Grd3.Cells[Row3, 15].Value = Penalty * HolidayIndex;
                    }
                }
            }          
        }

        private void ShowCoordinatorSCF()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.DocDt, A.ProductionShiftCode, I.ProductionShiftName, ");
            //SQL.AppendLine("B.EmpCode, C.EmpName, D.PosName, E.DeptName, F.GrdLvlName, ");
            //SQL.AppendLine("IfNull(G.GrdLvlIndexP, 1) As GrdLvlIndex, ");
            //SQL.AppendLine("IfNull(J.NoOfEmployee, 0) As NoOfEmployee, ");
            //SQL.AppendLine("H.HolName, if(IfNull(H.HolName, '')='', 1, @HolidayWagesIndex) As HolidayPenaltyIndex ");
            //SQL.AppendLine("From ( ");
            //SQL.AppendLine("    Select Distinct DocDt, ProductionShiftCode ");
            //SQL.AppendLine("    From TblShopFloorControlHdr ");
            //SQL.AppendLine("    Where Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            //SQL.AppendLine(") A ");
            //SQL.AppendLine("Inner Join ( ");
            //SQL.AppendLine("    Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            //SQL.AppendLine("    From TblShopFloorControlHdr T1 ");
            //SQL.AppendLine("    Inner Join TblShopFloorControl2Dtl T2 ");
            //SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("        And T2.BomType='3' ");
            //SQL.AppendLine("        And T2.CoordinatorInd='Y' ");
            //SQL.AppendLine("    Where Locate(Concat('##', T1.DocNo, '##'), @GetSelectedSFC)>0 ");
            //SQL.AppendLine(") B On A.DocDt=B.DocDt And A.ProductionShiftCode=B.ProductionShiftCode ");
            //SQL.AppendLine("Left Join TblEmployee C On B.EmpCode=C.EmpCode ");
            //SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            //SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            //SQL.AppendLine("Left Join TblGradeLevelHdr F On C.GrdLvlCode=F.GrdLvlCode ");
            //SQL.AppendLine("Left Join TblWagesFormulationDtl2 G ");
            //SQL.AppendLine("    On C.GrdLvlCode=G.GrdLvlCode ");
            //SQL.AppendLine("    And G.WagesFormulationCode=@WagesFormulationCode ");
            //SQL.AppendLine("Left Join TblHoliday H On A.DocDt=H.HolDt ");
            //SQL.AppendLine("Left Join TblProductionShift I On A.ProductionShiftCode=I.ProductionShiftCode ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select T.DocDt, T.ProductionShiftCode, Count(T.EmpCode) As NoOfEmployee ");
            //SQL.AppendLine("    From ( ");
            //SQL.AppendLine("        Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            //SQL.AppendLine("        From TblShopFloorControlHdr T1 ");
            //SQL.AppendLine("        Inner Join TblShopFloorControl2Dtl T2 ");
            //SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("            And T2.BomType='3' ");
            //SQL.AppendLine("            And T2.CoordinatorInd='N' ");
            //SQL.AppendLine("        Where Locate(Concat('##', T1.DocNo, '##'), @GetSelectedSFC)>0 ");
            //SQL.AppendLine("    ) T Group By T.DocDt, T.ProductionShiftCode ");
            //SQL.AppendLine(") J ");
            //SQL.AppendLine("    On A.DocDt=J.DocDt ");
            //SQL.AppendLine("    And A.ProductionShiftCode=J.ProductionShiftCode ");
            //SQL.AppendLine("Order By A.DocDt, A.ProductionShiftCode, E.DeptName, C.EmpName, B.EmpCode;");

            SQL.AppendLine("Select A.DocDt, A.ProductionShiftCode, I.ProductionShiftName, ");
            SQL.AppendLine("B.EmpCode, C.EmpName, D.PosName, E.DeptName, F.GrdLvlName, ");
            SQL.AppendLine("IfNull(G.GrdLvlIndexP, 1) As GrdLvlIndex, ");
            SQL.AppendLine("H.HolName, if(IfNull(H.HolName, '')='', 1, @HolidayWagesIndex) As HolidayPenaltyIndex, ");
            SQL.AppendLine("IfNull(J.NoOfEmployee, 0) As NoOfEmployee, ");
            SQL.AppendLine("B.GrdLvlCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct DocDt, ProductionShiftCode ");
            SQL.AppendLine("    From TblShopFloorControlHdr ");
            SQL.AppendLine("    Where Locate(Concat('##', DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.DocDt, T.ProductionShiftCode, T.EmpCode, ");
            SQL.AppendLine("    (");
            SQL.AppendLine("        Select GrdLvlCode From TblEmployeePPS ");
            SQL.AppendLine("        Where EmpCode=T.EmpCode ");
            SQL.AppendLine("        And StartDt<=T.DocDt And (EndDt Is Null Or EndDt>=T.DocDt) ");
            SQL.AppendLine("        Order By StartDt Desc Limit 1 "); //sepertinya tidak perlu
            SQL.AppendLine("    ) As GrdLvlCode ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            SQL.AppendLine("        From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("        Inner Join TblShopFloorControl2Dtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And T2.BomType='3' ");
            SQL.AppendLine("            And T2.CoordinatorInd='Y' ");
            SQL.AppendLine("        Where Locate(Concat('##', T1.DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    ) T  ");
            SQL.AppendLine(") B On A.DocDt=B.DocDt And A.ProductionShiftCode=B.ProductionShiftCode ");
            SQL.AppendLine("Left Join TblEmployee C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr F On B.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 G ");
            SQL.AppendLine("    On B.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("    And G.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Left Join TblHoliday H On A.DocDt=H.HolDt ");
            SQL.AppendLine("Left Join TblProductionShift I On A.ProductionShiftCode=I.ProductionShiftCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocDt, T.ProductionShiftCode, Count(T.EmpCode) As NoOfEmployee ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct T1.DocDt, T1.ProductionShiftCode, T2.BomCode As EmpCode ");
            SQL.AppendLine("        From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("        Inner Join TblShopFloorControl2Dtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And T2.BomType='3' ");
            SQL.AppendLine("            And T2.CoordinatorInd='N' ");
            SQL.AppendLine("        Where Locate(Concat('##', T1.DocNo, '##'), @GetSelectedSFC)>0 ");
            SQL.AppendLine("    ) T Group By T.DocDt, T.ProductionShiftCode ");
            SQL.AppendLine(") J ");
            SQL.AppendLine("    On A.DocDt=J.DocDt ");
            SQL.AppendLine("    And A.ProductionShiftCode=J.ProductionShiftCode ");
            SQL.AppendLine("Order By A.DocDt, A.ProductionShiftCode, E.DeptName, C.EmpName, B.EmpCode;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@GetSelectedSFC", GetSelectedSFC2());
            Sm.CmParam<Decimal>(ref cm, "@HolidayWagesIndex", 1);

            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                       //0
                       "DocDt",

                       //1-5
                       "ProductionShiftCode", "ProductionShiftName", "EmpCode", "EmpName", "PosName", 
                       
                       //6-10
                       "DeptName", "GrdLvlName", "GrdLvlIndex", "HolName", "HolidayPenaltyIndex",

                       //11-12
                       "NoOfEmployee", "GrdLvlCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 10, 13 });
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 9, 10, 12, 13, 14 });
            Sm.FocusGrd(Grd5, 0, 1);
        }

        private void ComputeCoordinatorSCFPenalty(string DocDt, string ProductionShiftCode)
        {
            decimal Penalty = 0m, Amt = 0m, EmpCount = 0m, GrdLvlIndex = 0m, HolidayIndex = 1m;

            if (Grd2.Rows.Count > 1 && Grd5.Rows.Count > 1)
            {
                for (int Row5 = 0; Row5 < Grd5.Rows.Count; Row5++)
                {
                    if (
                        Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd5, Row5, 1)) &&
                        Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd5, Row5, 2))
                        )
                    {
                        Penalty = 0m;
                        Amt = 0m;
                        EmpCount = 0m;
                        GrdLvlIndex = 0m;
                        HolidayIndex = 1m;

                        for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                            if (
                                Sm.CompareStr(DocDt, Sm.GetGrdDate(Grd2, Row2, 1)) &&
                                Sm.CompareStr(ProductionShiftCode, Sm.GetGrdStr(Grd2, Row2, 2))
                                )
                                Amt += Sm.GetGrdDec(Grd2, Row2, 8);

                        if (Sm.GetGrdStr(Grd5, Row5, 9).Length > 0) GrdLvlIndex = Sm.GetGrdDec(Grd5, Row5, 9);
                        if (Sm.GetGrdStr(Grd5, Row5, 14).Length > 0) EmpCount = Sm.GetGrdDec(Grd5, Row5, 14);
                        if (Sm.GetGrdStr(Grd5, Row5, 12).Length > 0) HolidayIndex = Sm.GetGrdDec(Grd5, Row5, 12);

                        if (EmpCount != 0) Penalty = (1 / EmpCount) * Amt * GrdLvlIndex;

                        Grd5.Cells[Row5, 10].Value = Penalty;
                        Grd5.Cells[Row5, 13].Value = Penalty * HolidayIndex;
                    }
                }
            }
        }

        private void ComputeCoordinatorSCFPenalty()
        {
            decimal Amt = 0m, Penalty = 0m, EmpCount = 0m, GrdLvlIndex = 0m, HolidayIndex = 1m;

            if (Grd2.Rows.Count > 1 && Grd5.Rows.Count > 1)
            {
                for (int Row5 = 0; Row5 < Grd5.Rows.Count; Row5++)
                {
                    if (Sm.GetGrdStr(Grd5, Row5, 1).Length > 0)
                    {
                        Penalty = 0m;
                        Amt = 0m;
                        EmpCount = 0m;
                        GrdLvlIndex = 0m;
                        HolidayIndex = 1m;

                        for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                            if (
                                Sm.CompareStr(Sm.GetGrdDate(Grd5, Row5, 1), Sm.GetGrdDate(Grd2, Row2, 1)) &&
                                Sm.CompareGrdStr(Grd5, Row5, 2, Grd2, Row2, 2)
                                )
                                Amt += Sm.GetGrdDec(Grd2, Row2, 8);

                        if (Sm.GetGrdStr(Grd5, Row5, 9).Length > 0) GrdLvlIndex = Sm.GetGrdDec(Grd5, Row5, 9);
                        if (Sm.GetGrdStr(Grd5, Row5, 14).Length > 0) EmpCount = Sm.GetGrdDec(Grd5, Row5, 14);
                        if (Sm.GetGrdStr(Grd5, Row5, 12).Length > 0) HolidayIndex = Sm.GetGrdDec(Grd5, Row5, 12);

                        if (EmpCount != 0) Penalty = (1 / EmpCount) * Amt * GrdLvlIndex;

                        Grd5.Cells[Row5, 10].Value = Penalty;
                        Grd5.Cells[Row5, 13].Value = Penalty * HolidayIndex;
                    }
                }
            }
        }

        private void SetWagesFormulationCode()
        {
            LueWagesFormulationCode.EditValue = null;

            try
            {
                string WorkCenterDocNo = Sm.GetLue(LueWorkCenterDocNo);

                if (WorkCenterDocNo.Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select WagesFormulationCode ");
                    SQL.AppendLine("From TblWorkCenterWagesFormulation ");
                    SQL.AppendLine("Where WorkCenterCode=@WorkCenterDocNo ");
                    SQL.AppendLine("Order By Date Desc Limit 1;");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", WorkCenterDocNo);

                    string WagesFormulationCode = Sm.GetValue(cm);
                    if (WagesFormulationCode.Length > 0)
                        Sm.SetLue(LueWagesFormulationCode, WagesFormulationCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
                Grd3.Font = TheFont;
                Grd4.Font = TheFont;
                Grd5.Font = TheFont;
                Grd6.Font = TheFont;
                Grd7.Font = TheFont;
            }
        }

        private void LueWorkCenterDocNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWorkCenterDocNo, new Sm.RefreshLue2(Sl.SetLueWorkCenterDocNo), string.Empty);
            SetWagesFormulationCode();
            ClearGrd();
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueWagesFormulationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWagesFormulationCode, new Sm.RefreshLue1(Sl.SetLueWagesFormulationCode));
            ShowWagesFormulationUom();
            ClearGrd();
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (
                BtnSave.Enabled &&
                TxtDocNo.Text.Length == 0 &&
                e.ColIndex == 1 &&
                !Sm.IsLueEmpty(LueWorkCenterDocNo, "Work Center") &&
                !Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation")
                )
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmPNT2Dlg(
                        this, Sm.GetLue(LueWorkCenterDocNo), mHolidayWagesIndex, mNotHolidayWagesIndex
                        ));
            }

            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (!(
                    Sm.IsGrdColSelected(new int[] { 11, 13 }, e.ColIndex) ||
                    (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 18).Length > 0)
                   ))
                    e.DoDefault = false;
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete) ShowDocInfo();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled &&
                TxtDocNo.Text.Length == 0 &&
                e.ColIndex == 1 &&
                !Sm.IsLueEmpty(LueWorkCenterDocNo, "Work Center") &&
                !Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation")
                )
                Sm.FormShowDialog(new FrmPNT2Dlg(this, Sm.GetLue(LueWorkCenterDocNo), mHolidayWagesIndex, mNotHolidayWagesIndex));
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11, 13}, e);

                if (e.ColIndex == 11)
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 7, 11, 13, 12, 14);

                if (e.ColIndex == 13)
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 7, 13, 11, 14, 12);

                if (e.ColIndex == 11 || e.ColIndex == 13 || (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 18).Length > 0))
                {
                    if (e.ColIndex == 15)
                        Grd1.Cells[e.RowIndex, 16].Value = 
                            Sm.GetGrdBool(Grd1, e.RowIndex, 15)?
                                mHolidayWagesIndex:mNotHolidayWagesIndex;
                    ComputeValue(e.RowIndex);
                    ComputeItemProcessed();
                    ComputePenaltyBasedOnFormula(Sm.GetGrdDate(Grd1, e.RowIndex, 4), Sm.GetGrdStr(Grd1, e.RowIndex, 5));
                    ComputeNonCoordinatorSCFPenalty(Sm.GetGrdDate(Grd1, e.RowIndex, 4), Sm.GetGrdStr(Grd1, e.RowIndex, 5));
                    ComputeCoordinatorSCFPenalty(Sm.GetGrdDate(Grd1, e.RowIndex, 4), Sm.GetGrdStr(Grd1, e.RowIndex, 5));
                    ComputeNonCoordinatorPenalty();
                    ComputeCoordinatorPenalty();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion
    }
}
