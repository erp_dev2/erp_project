﻿#region Update
/*
    10/06/2021 [SET/PHT] Menambah ceklist exclude cancel ketika find DR for project menu FrmFind
    28/06/2021 [TKG/PHT] menambah code utk bbrp column, event masih ada yg kurang
    21/07/2021 [TKG/PHT] menghilangkan approver level
    13/08/2021 [TKG/PHT] level tidak menggunakan link ke table level lagi
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalSettingFormulaFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDocApprovalSettingFormula mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDocApprovalSettingFormulaFind(FrmDocApprovalSettingFormula FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, "N");
                Sl.SetLuePosCode(ref LuePosCode);
                Sl.SetLueLevelCode(ref LueLevelEmpCode, string.Empty);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type", 
                        "Setting's Level",
                        "Setting's Site",
                        "Setting's Department",
                        "Setting's Employee Level",

                        //6-10
                        "Approver's Site",
                        "Approver's Department",
                        "Approver's Position",
                        "Created By",   
                        "Created Date",
                        
                        //11-14
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time"
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 11, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 14 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;
            string Filter = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            try
            {
                Sm.FilterStr(ref Filter, ref cm, TxtTypeApproval.Text, new string[] { "A.TypeApproval" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DASDeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.DASSiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLevelEmpCode), "A.DASLevelCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePosCode), "A.PosCode", true);

                SQL.AppendLine("Select A.TypeApproval, A.Level, ");
                SQL.AppendLine("C.SiteName As DASSiteName, B.DeptName As DASDeptName, A.DASLevelCode, ");
                SQL.AppendLine("F.SiteName, E.DeptName, G.PosName, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblDocApprovalFormula A ");
                SQL.AppendLine("Left Join TblDepartment B On A.DASDeptCode=B.DeptCode ");
                SQL.AppendLine("Left Join TblSite C On A.DASSiteCode=C.SiteCode ");
                SQL.AppendLine("Left Join TblDepartment E On A.DeptCode=E.DeptCode ");
                SQL.AppendLine("Left Join TblSite F On A.SiteCode=F.SiteCode ");
                SQL.AppendLine("Left Join TblPosition G On A.PosCode=G.PosCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By A.TypeApproval;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "TypeApproval", 
                                
                            //1-5
                            "Level", "DASSiteName", "DASDeptName", "DASLevelCode", "SiteName", 
                            
                            //6-10
                            "DeptName", "PosName", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //11
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1),
                    Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 2)
                    );
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtTypeApproval_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTypeApproval_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Type");
        }

        private void LueLevelEmpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevelEmpCode, new Sm.RefreshLue1(Sl.SetLueLevelCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLevelEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Setting's employee level");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Setting's site");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Setting's department");
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPosCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Position");
        }

        #endregion

        #endregion
    }
}
