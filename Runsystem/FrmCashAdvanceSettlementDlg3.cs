﻿#region Update
/*
    15/01/2021 [WED/PHT] new apps
    11/02/2021 [TKG/PHT] update semua info yg berkaitan dengan quantity
    17/03/2021 [WED/PHT] item tidak melihat RKAP berdasarkan parameter baru IsCASUseItemRateNotBasedOnCBP
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCashAdvanceSettlementDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmCashAdvanceSettlement mFrmParent;
        private int mRow = 0;
        private string 
            mSQL = string.Empty,
            mYr = string.Empty,
            mCCtCode = string.Empty,
            mCCCode = string.Empty
            ;

        #endregion

        #region Constructor

        public FrmCashAdvanceSettlementDlg3(FrmCashAdvanceSettlement FrmParent, int Row, string Yr, string CCtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
            mYr = Yr;
            mCCtCode = CCtCode;
            mCCCode = GetCCCode(CCtCode);
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

            Sl.SetLueItCtCode(ref LueItCtCode);

            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-4
                    "Item's Code", 
                    "",
                    "Item's Name",
                    "Item's Category"
                }, new int[]
                {
                    //0
                    50,

                    //1-4
                    120, 20, 200, 200
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, B.ItCtName ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("    And A.ActInd = 'Y' ");
            if (!mFrmParent.mIsCASUseItemRateNotBasedOnCBP)
            {
                SQL.AppendLine("    And A.ItCode In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Distinct T2.ItCode ");
                SQL.AppendLine("        From TblCompanyBudgetPlanHdr T1 ");
                SQL.AppendLine("        Inner Join TblCompanyBudgetPlanDtl2 T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.CancelInd = 'N' ");
                SQL.AppendLine("            And T1.Yr = @Yr ");
                SQL.AppendLine("            And T1.CCCode = @CCCode ");
                SQL.AppendLine("            And T1.DocType = '2' ");
                SQL.AppendLine("        Inner Join TblCostCategory T3 On T3.CCtCode = @CCtCode ");
                SQL.AppendLine("            And T3.AcNo = T2.AcNo ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@CCtCode", mCCtCode);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.ItName",
                    new string[] 
                    { 
                        //0
                        "ItCode",

                        //1-2
                        "ItName",
                        "ItCtName"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                Sm.CopyGrdValue(mFrmParent.Grd1, mRow, 24, Grd1, Row, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, mRow, 26, Grd1, Row, 3);
                string ItCode = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.CalculateRate(mRow, ItCode, mYr, mCCCode);
                mFrmParent.ComputeAmt1();
                mFrmParent.SetCostCategoryInfo();
                mFrmParent.SetVoucherInfo();
                mFrmParent.ComputeAmt2();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private string GetCCCode(string CCtCode)
        {
            string CCCode = string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode ");
            SQL.AppendLine("From TblCostCategory ");
            SQL.AppendLine("Where CCtCode = @Param ");
            SQL.AppendLine("Limit 1; ");

            CCCode = Sm.GetValue(SQL.ToString(), CCtCode);

            return CCCode;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
