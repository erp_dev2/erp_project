﻿namespace RunSystem
{
    partial class FrmWorkSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtWSCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TmeIn1 = new DevExpress.XtraEditors.TimeEdit();
            this.TxtWSName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkHolidayInd = new DevExpress.XtraEditors.CheckEdit();
            this.TmeOut1 = new DevExpress.XtraEditors.TimeEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TmeIn2 = new DevExpress.XtraEditors.TimeEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TmeOut2 = new DevExpress.XtraEditors.TimeEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TmeOut3 = new DevExpress.XtraEditors.TimeEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TmeIn3 = new DevExpress.XtraEditors.TimeEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.LueDeductionRoundCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.DurBreakOut4 = new DevExpress.XtraEditors.TimeEdit();
            this.DurBreakIn4 = new DevExpress.XtraEditors.TimeEdit();
            this.DurBreakOut3 = new DevExpress.XtraEditors.TimeEdit();
            this.DurBreakIn3 = new DevExpress.XtraEditors.TimeEdit();
            this.TmeOut4 = new DevExpress.XtraEditors.TimeEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TmeIn4 = new DevExpress.XtraEditors.TimeEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.ChkDoubleShiftInd = new DevExpress.XtraEditors.CheckEdit();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.DurOTOut2 = new DevExpress.XtraEditors.TimeEdit();
            this.DurOTIn2 = new DevExpress.XtraEditors.TimeEdit();
            this.DurIn2 = new DevExpress.XtraEditors.TimeEdit();
            this.DurBreakOut2 = new DevExpress.XtraEditors.TimeEdit();
            this.DurBreakIn2 = new DevExpress.XtraEditors.TimeEdit();
            this.DurOut2 = new DevExpress.XtraEditors.TimeEdit();
            this.DurOTOut1 = new DevExpress.XtraEditors.TimeEdit();
            this.DurOTIn1 = new DevExpress.XtraEditors.TimeEdit();
            this.DurIn1 = new DevExpress.XtraEditors.TimeEdit();
            this.DurBreakOut1 = new DevExpress.XtraEditors.TimeEdit();
            this.DurBreakIn1 = new DevExpress.XtraEditors.TimeEdit();
            this.DurOut1 = new DevExpress.XtraEditors.TimeEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWSCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeIn1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWSName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHolidayInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeOut1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeIn2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeOut2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeOut3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeIn3.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeductionRoundCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakOut4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakIn4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakOut3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakIn3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeOut4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeIn4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDoubleShiftInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOTOut2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOTIn2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurIn2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakOut2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakIn2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOut2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOTOut1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOTIn1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurIn1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakOut1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakIn1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOut1.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // TxtWSCode
            // 
            this.TxtWSCode.EnterMoveNextControl = true;
            this.TxtWSCode.Location = new System.Drawing.Point(140, 6);
            this.TxtWSCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWSCode.Name = "TxtWSCode";
            this.TxtWSCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWSCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWSCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWSCode.Properties.Appearance.Options.UseFont = true;
            this.TxtWSCode.Properties.MaxLength = 16;
            this.TxtWSCode.Size = new System.Drawing.Size(151, 20);
            this.TxtWSCode.TabIndex = 10;
            this.TxtWSCode.Validated += new System.EventHandler(this.TxtWSCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(15, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Work Schedule Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(12, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Work Schedule Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeIn1
            // 
            this.TmeIn1.EditValue = null;
            this.TmeIn1.EnterMoveNextControl = true;
            this.TmeIn1.Location = new System.Drawing.Point(140, 50);
            this.TmeIn1.Name = "TmeIn1";
            this.TmeIn1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeIn1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeIn1.Properties.Appearance.Options.UseFont = true;
            this.TmeIn1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeIn1.Properties.Mask.EditMask = "HH:mm";
            this.TmeIn1.Size = new System.Drawing.Size(88, 20);
            this.TmeIn1.TabIndex = 16;
            // 
            // TxtWSName
            // 
            this.TxtWSName.EnterMoveNextControl = true;
            this.TxtWSName.Location = new System.Drawing.Point(140, 28);
            this.TxtWSName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWSName.Name = "TxtWSName";
            this.TxtWSName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWSName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWSName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWSName.Properties.Appearance.Options.UseFont = true;
            this.TxtWSName.Properties.MaxLength = 80;
            this.TxtWSName.Size = new System.Drawing.Size(395, 20);
            this.TxtWSName.TabIndex = 15;
            this.TxtWSName.Validated += new System.EventHandler(this.TxtWSName_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(82, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Check In";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(294, 5);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(67, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // ChkHolidayInd
            // 
            this.ChkHolidayInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkHolidayInd.Location = new System.Drawing.Point(365, 5);
            this.ChkHolidayInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkHolidayInd.Name = "ChkHolidayInd";
            this.ChkHolidayInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkHolidayInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHolidayInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkHolidayInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkHolidayInd.Properties.Appearance.Options.UseFont = true;
            this.ChkHolidayInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHolidayInd.Properties.Caption = "Holiday";
            this.ChkHolidayInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHolidayInd.Size = new System.Drawing.Size(67, 22);
            this.ChkHolidayInd.TabIndex = 12;
            // 
            // TmeOut1
            // 
            this.TmeOut1.EditValue = null;
            this.TmeOut1.EnterMoveNextControl = true;
            this.TmeOut1.Location = new System.Drawing.Point(140, 72);
            this.TmeOut1.Name = "TmeOut1";
            this.TmeOut1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeOut1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeOut1.Properties.Appearance.Options.UseFont = true;
            this.TmeOut1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeOut1.Properties.Mask.EditMask = "HH:mm";
            this.TmeOut1.Size = new System.Drawing.Size(88, 20);
            this.TmeOut1.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(72, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Check Out";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeIn2
            // 
            this.TmeIn2.EditValue = null;
            this.TmeIn2.EnterMoveNextControl = true;
            this.TmeIn2.Location = new System.Drawing.Point(140, 94);
            this.TmeIn2.Name = "TmeIn2";
            this.TmeIn2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeIn2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeIn2.Properties.Appearance.Options.UseFont = true;
            this.TmeIn2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeIn2.Properties.Mask.EditMask = "HH:mm";
            this.TmeIn2.Size = new System.Drawing.Size(88, 20);
            this.TmeIn2.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(85, 97);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 14);
            this.label5.TabIndex = 19;
            this.label5.Text = "Break In";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeOut2
            // 
            this.TmeOut2.EditValue = null;
            this.TmeOut2.EnterMoveNextControl = true;
            this.TmeOut2.Location = new System.Drawing.Point(140, 116);
            this.TmeOut2.Name = "TmeOut2";
            this.TmeOut2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeOut2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeOut2.Properties.Appearance.Options.UseFont = true;
            this.TmeOut2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeOut2.Properties.Mask.EditMask = "HH:mm";
            this.TmeOut2.Size = new System.Drawing.Size(88, 20);
            this.TmeOut2.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(75, 119);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Break Out";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(140, 270);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(395, 20);
            this.MeeRemark.TabIndex = 57;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(90, 273);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 56;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeOut3
            // 
            this.TmeOut3.EditValue = null;
            this.TmeOut3.EnterMoveNextControl = true;
            this.TmeOut3.Location = new System.Drawing.Point(140, 204);
            this.TmeOut3.Name = "TmeOut3";
            this.TmeOut3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeOut3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeOut3.Properties.Appearance.Options.UseFont = true;
            this.TmeOut3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeOut3.Properties.Mask.EditMask = "HH:mm";
            this.TmeOut3.Size = new System.Drawing.Size(88, 20);
            this.TmeOut3.TabIndex = 47;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(42, 207);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 14);
            this.label7.TabIndex = 46;
            this.label7.Text = "Routine OT Out";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeIn3
            // 
            this.TmeIn3.EditValue = null;
            this.TmeIn3.EnterMoveNextControl = true;
            this.TmeIn3.Location = new System.Drawing.Point(140, 182);
            this.TmeIn3.Name = "TmeIn3";
            this.TmeIn3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeIn3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeIn3.Properties.Appearance.Options.UseFont = true;
            this.TmeIn3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeIn3.Properties.Mask.EditMask = "HH:mm";
            this.TmeIn3.Size = new System.Drawing.Size(88, 20);
            this.TmeIn3.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(52, 185);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 14);
            this.label8.TabIndex = 40;
            this.label8.Text = "Routine OT In";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.AliceBlue;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(772, 22);
            this.button1.TabIndex = 58;
            this.button1.Text = "Allowance / Deduction";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.LueDeductionRoundCode);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.DurBreakOut4);
            this.panel3.Controls.Add(this.DurBreakIn4);
            this.panel3.Controls.Add(this.DurBreakOut3);
            this.panel3.Controls.Add(this.DurBreakIn3);
            this.panel3.Controls.Add(this.TmeOut4);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.TmeIn4);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.ChkDoubleShiftInd);
            this.panel3.Controls.Add(this.LblSiteCode);
            this.panel3.Controls.Add(this.LueSiteCode);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.DurOTOut2);
            this.panel3.Controls.Add(this.DurOTIn2);
            this.panel3.Controls.Add(this.DurIn2);
            this.panel3.Controls.Add(this.DurBreakOut2);
            this.panel3.Controls.Add(this.DurBreakIn2);
            this.panel3.Controls.Add(this.DurOut2);
            this.panel3.Controls.Add(this.DurOTOut1);
            this.panel3.Controls.Add(this.DurOTIn1);
            this.panel3.Controls.Add(this.DurIn1);
            this.panel3.Controls.Add(this.DurBreakOut1);
            this.panel3.Controls.Add(this.DurBreakIn1);
            this.panel3.Controls.Add(this.DurOut1);
            this.panel3.Controls.Add(this.TxtWSCode);
            this.panel3.Controls.Add(this.TmeOut3);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.TmeIn3);
            this.panel3.Controls.Add(this.TxtWSName);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.TmeIn1);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.TmeOut2);
            this.panel3.Controls.Add(this.ChkHolidayInd);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.TmeIn2);
            this.panel3.Controls.Add(this.TmeOut1);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 297);
            this.panel3.TabIndex = 29;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(19, 251);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(118, 14);
            this.label28.TabIndex = 54;
            this.label28.Text = "Deduction Rounding";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeductionRoundCode
            // 
            this.LueDeductionRoundCode.EnterMoveNextControl = true;
            this.LueDeductionRoundCode.Location = new System.Drawing.Point(141, 248);
            this.LueDeductionRoundCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeductionRoundCode.Name = "LueDeductionRoundCode";
            this.LueDeductionRoundCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeductionRoundCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeductionRoundCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeductionRoundCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeductionRoundCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeductionRoundCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeductionRoundCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeductionRoundCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeductionRoundCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeductionRoundCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeductionRoundCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeductionRoundCode.Properties.DropDownRows = 30;
            this.LueDeductionRoundCode.Properties.NullText = "[Empty]";
            this.LueDeductionRoundCode.Properties.PopupWidth = 400;
            this.LueDeductionRoundCode.Size = new System.Drawing.Size(395, 20);
            this.LueDeductionRoundCode.TabIndex = 55;
            this.LueDeductionRoundCode.ToolTip = "F4 : Show/hide list";
            this.LueDeductionRoundCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeductionRoundCode.EditValueChanged += new System.EventHandler(this.LueDeductionRoundCode_EditValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(432, 163);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 14);
            this.label22.TabIndex = 38;
            this.label22.Text = "-";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(432, 141);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 14);
            this.label23.TabIndex = 32;
            this.label23.Text = "-";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(265, 163);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(72, 14);
            this.label24.TabIndex = 36;
            this.label24.Text = "Time Range";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(265, 141);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(72, 14);
            this.label25.TabIndex = 30;
            this.label25.Text = "Time Range";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DurBreakOut4
            // 
            this.DurBreakOut4.EditValue = null;
            this.DurBreakOut4.EnterMoveNextControl = true;
            this.DurBreakOut4.Location = new System.Drawing.Point(447, 160);
            this.DurBreakOut4.Name = "DurBreakOut4";
            this.DurBreakOut4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurBreakOut4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurBreakOut4.Properties.Appearance.Options.UseFont = true;
            this.DurBreakOut4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurBreakOut4.Properties.Mask.EditMask = "HH:mm";
            this.DurBreakOut4.Size = new System.Drawing.Size(88, 20);
            this.DurBreakOut4.TabIndex = 39;
            // 
            // DurBreakIn4
            // 
            this.DurBreakIn4.EditValue = null;
            this.DurBreakIn4.EnterMoveNextControl = true;
            this.DurBreakIn4.Location = new System.Drawing.Point(447, 138);
            this.DurBreakIn4.Name = "DurBreakIn4";
            this.DurBreakIn4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurBreakIn4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurBreakIn4.Properties.Appearance.Options.UseFont = true;
            this.DurBreakIn4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurBreakIn4.Properties.Mask.EditMask = "HH:mm";
            this.DurBreakIn4.Size = new System.Drawing.Size(88, 20);
            this.DurBreakIn4.TabIndex = 33;
            // 
            // DurBreakOut3
            // 
            this.DurBreakOut3.EditValue = null;
            this.DurBreakOut3.EnterMoveNextControl = true;
            this.DurBreakOut3.Location = new System.Drawing.Point(341, 160);
            this.DurBreakOut3.Name = "DurBreakOut3";
            this.DurBreakOut3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurBreakOut3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurBreakOut3.Properties.Appearance.Options.UseFont = true;
            this.DurBreakOut3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurBreakOut3.Properties.Mask.EditMask = "HH:mm";
            this.DurBreakOut3.Size = new System.Drawing.Size(88, 20);
            this.DurBreakOut3.TabIndex = 37;
            // 
            // DurBreakIn3
            // 
            this.DurBreakIn3.EditValue = null;
            this.DurBreakIn3.EnterMoveNextControl = true;
            this.DurBreakIn3.Location = new System.Drawing.Point(341, 138);
            this.DurBreakIn3.Name = "DurBreakIn3";
            this.DurBreakIn3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurBreakIn3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurBreakIn3.Properties.Appearance.Options.UseFont = true;
            this.DurBreakIn3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurBreakIn3.Properties.Mask.EditMask = "HH:mm";
            this.DurBreakIn3.Size = new System.Drawing.Size(88, 20);
            this.DurBreakIn3.TabIndex = 31;
            // 
            // TmeOut4
            // 
            this.TmeOut4.EditValue = null;
            this.TmeOut4.EnterMoveNextControl = true;
            this.TmeOut4.Location = new System.Drawing.Point(140, 160);
            this.TmeOut4.Name = "TmeOut4";
            this.TmeOut4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeOut4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeOut4.Properties.Appearance.Options.UseFont = true;
            this.TmeOut4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeOut4.Properties.Mask.EditMask = "HH:mm";
            this.TmeOut4.Size = new System.Drawing.Size(88, 20);
            this.TmeOut4.TabIndex = 35;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(54, 163);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 14);
            this.label26.TabIndex = 34;
            this.label26.Text = "Break Out (2)";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TmeIn4
            // 
            this.TmeIn4.EditValue = null;
            this.TmeIn4.EnterMoveNextControl = true;
            this.TmeIn4.Location = new System.Drawing.Point(140, 138);
            this.TmeIn4.Name = "TmeIn4";
            this.TmeIn4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeIn4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeIn4.Properties.Appearance.Options.UseFont = true;
            this.TmeIn4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeIn4.Properties.Mask.EditMask = "HH:mm";
            this.TmeIn4.Size = new System.Drawing.Size(88, 20);
            this.TmeIn4.TabIndex = 29;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(64, 141);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(73, 14);
            this.label27.TabIndex = 28;
            this.label27.Text = "Break In (2)";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkDoubleShiftInd
            // 
            this.ChkDoubleShiftInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkDoubleShiftInd.Location = new System.Drawing.Point(437, 5);
            this.ChkDoubleShiftInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkDoubleShiftInd.Name = "ChkDoubleShiftInd";
            this.ChkDoubleShiftInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkDoubleShiftInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDoubleShiftInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkDoubleShiftInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkDoubleShiftInd.Properties.Appearance.Options.UseFont = true;
            this.ChkDoubleShiftInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkDoubleShiftInd.Properties.Caption = "Double-Shift";
            this.ChkDoubleShiftInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDoubleShiftInd.Size = new System.Drawing.Size(98, 22);
            this.ChkDoubleShiftInd.TabIndex = 13;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(109, 229);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 52;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(140, 226);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 400;
            this.LueSiteCode.Size = new System.Drawing.Size(395, 20);
            this.LueSiteCode.TabIndex = 53;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(432, 207);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 14);
            this.label21.TabIndex = 50;
            this.label21.Text = "-";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(432, 185);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 14);
            this.label20.TabIndex = 44;
            this.label20.Text = "-";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(432, 119);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 14);
            this.label19.TabIndex = 50;
            this.label19.Text = "-";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(432, 97);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 14);
            this.label18.TabIndex = 49;
            this.label18.Text = "-";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(432, 78);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 14);
            this.label17.TabIndex = 48;
            this.label17.Text = "-";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(432, 53);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 14);
            this.label16.TabIndex = 47;
            this.label16.Text = "-";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(265, 207);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 14);
            this.label9.TabIndex = 48;
            this.label9.Text = "Time Range";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(265, 53);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 14);
            this.label11.TabIndex = 41;
            this.label11.Text = "Time Range";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(265, 185);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 14);
            this.label12.TabIndex = 42;
            this.label12.Text = "Time Range";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(265, 119);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 14);
            this.label13.TabIndex = 44;
            this.label13.Text = "Time Range";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(265, 75);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 14);
            this.label14.TabIndex = 42;
            this.label14.Text = "Time Range";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(265, 97);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 14);
            this.label15.TabIndex = 43;
            this.label15.Text = "Time Range";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DurOTOut2
            // 
            this.DurOTOut2.EditValue = null;
            this.DurOTOut2.EnterMoveNextControl = true;
            this.DurOTOut2.Location = new System.Drawing.Point(447, 204);
            this.DurOTOut2.Name = "DurOTOut2";
            this.DurOTOut2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurOTOut2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurOTOut2.Properties.Appearance.Options.UseFont = true;
            this.DurOTOut2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurOTOut2.Properties.Mask.EditMask = "HH:mm";
            this.DurOTOut2.Size = new System.Drawing.Size(88, 20);
            this.DurOTOut2.TabIndex = 51;
            // 
            // DurOTIn2
            // 
            this.DurOTIn2.EditValue = null;
            this.DurOTIn2.EnterMoveNextControl = true;
            this.DurOTIn2.Location = new System.Drawing.Point(447, 182);
            this.DurOTIn2.Name = "DurOTIn2";
            this.DurOTIn2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurOTIn2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurOTIn2.Properties.Appearance.Options.UseFont = true;
            this.DurOTIn2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurOTIn2.Properties.Mask.EditMask = "HH:mm";
            this.DurOTIn2.Size = new System.Drawing.Size(88, 20);
            this.DurOTIn2.TabIndex = 45;
            // 
            // DurIn2
            // 
            this.DurIn2.EditValue = null;
            this.DurIn2.EnterMoveNextControl = true;
            this.DurIn2.Location = new System.Drawing.Point(447, 50);
            this.DurIn2.Name = "DurIn2";
            this.DurIn2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurIn2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurIn2.Properties.Appearance.Options.UseFont = true;
            this.DurIn2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurIn2.Properties.Mask.EditMask = "HH:mm";
            this.DurIn2.Size = new System.Drawing.Size(88, 20);
            this.DurIn2.TabIndex = 18;
            // 
            // DurBreakOut2
            // 
            this.DurBreakOut2.EditValue = null;
            this.DurBreakOut2.EnterMoveNextControl = true;
            this.DurBreakOut2.Location = new System.Drawing.Point(447, 116);
            this.DurBreakOut2.Name = "DurBreakOut2";
            this.DurBreakOut2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurBreakOut2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurBreakOut2.Properties.Appearance.Options.UseFont = true;
            this.DurBreakOut2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurBreakOut2.Properties.Mask.EditMask = "HH:mm";
            this.DurBreakOut2.Size = new System.Drawing.Size(88, 20);
            this.DurBreakOut2.TabIndex = 27;
            // 
            // DurBreakIn2
            // 
            this.DurBreakIn2.EditValue = null;
            this.DurBreakIn2.EnterMoveNextControl = true;
            this.DurBreakIn2.Location = new System.Drawing.Point(447, 94);
            this.DurBreakIn2.Name = "DurBreakIn2";
            this.DurBreakIn2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurBreakIn2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurBreakIn2.Properties.Appearance.Options.UseFont = true;
            this.DurBreakIn2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurBreakIn2.Properties.Mask.EditMask = "HH:mm";
            this.DurBreakIn2.Size = new System.Drawing.Size(88, 20);
            this.DurBreakIn2.TabIndex = 24;
            // 
            // DurOut2
            // 
            this.DurOut2.EditValue = null;
            this.DurOut2.EnterMoveNextControl = true;
            this.DurOut2.Location = new System.Drawing.Point(447, 72);
            this.DurOut2.Name = "DurOut2";
            this.DurOut2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurOut2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurOut2.Properties.Appearance.Options.UseFont = true;
            this.DurOut2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurOut2.Properties.Mask.EditMask = "HH:mm";
            this.DurOut2.Size = new System.Drawing.Size(88, 20);
            this.DurOut2.TabIndex = 21;
            // 
            // DurOTOut1
            // 
            this.DurOTOut1.EditValue = null;
            this.DurOTOut1.EnterMoveNextControl = true;
            this.DurOTOut1.Location = new System.Drawing.Point(341, 204);
            this.DurOTOut1.Name = "DurOTOut1";
            this.DurOTOut1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurOTOut1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurOTOut1.Properties.Appearance.Options.UseFont = true;
            this.DurOTOut1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurOTOut1.Properties.Mask.EditMask = "HH:mm";
            this.DurOTOut1.Size = new System.Drawing.Size(88, 20);
            this.DurOTOut1.TabIndex = 49;
            // 
            // DurOTIn1
            // 
            this.DurOTIn1.EditValue = null;
            this.DurOTIn1.EnterMoveNextControl = true;
            this.DurOTIn1.Location = new System.Drawing.Point(341, 182);
            this.DurOTIn1.Name = "DurOTIn1";
            this.DurOTIn1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurOTIn1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurOTIn1.Properties.Appearance.Options.UseFont = true;
            this.DurOTIn1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurOTIn1.Properties.Mask.EditMask = "HH:mm";
            this.DurOTIn1.Size = new System.Drawing.Size(88, 20);
            this.DurOTIn1.TabIndex = 43;
            // 
            // DurIn1
            // 
            this.DurIn1.EditValue = null;
            this.DurIn1.EnterMoveNextControl = true;
            this.DurIn1.Location = new System.Drawing.Point(341, 50);
            this.DurIn1.Name = "DurIn1";
            this.DurIn1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurIn1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurIn1.Properties.Appearance.Options.UseFont = true;
            this.DurIn1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurIn1.Properties.Mask.EditMask = "HH:mm";
            this.DurIn1.Size = new System.Drawing.Size(88, 20);
            this.DurIn1.TabIndex = 17;
            // 
            // DurBreakOut1
            // 
            this.DurBreakOut1.EditValue = null;
            this.DurBreakOut1.EnterMoveNextControl = true;
            this.DurBreakOut1.Location = new System.Drawing.Point(341, 116);
            this.DurBreakOut1.Name = "DurBreakOut1";
            this.DurBreakOut1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurBreakOut1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurBreakOut1.Properties.Appearance.Options.UseFont = true;
            this.DurBreakOut1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurBreakOut1.Properties.Mask.EditMask = "HH:mm";
            this.DurBreakOut1.Size = new System.Drawing.Size(88, 20);
            this.DurBreakOut1.TabIndex = 26;
            // 
            // DurBreakIn1
            // 
            this.DurBreakIn1.EditValue = null;
            this.DurBreakIn1.EnterMoveNextControl = true;
            this.DurBreakIn1.Location = new System.Drawing.Point(341, 94);
            this.DurBreakIn1.Name = "DurBreakIn1";
            this.DurBreakIn1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurBreakIn1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurBreakIn1.Properties.Appearance.Options.UseFont = true;
            this.DurBreakIn1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurBreakIn1.Properties.Mask.EditMask = "HH:mm";
            this.DurBreakIn1.Size = new System.Drawing.Size(88, 20);
            this.DurBreakIn1.TabIndex = 23;
            // 
            // DurOut1
            // 
            this.DurOut1.EditValue = null;
            this.DurOut1.EnterMoveNextControl = true;
            this.DurOut1.Location = new System.Drawing.Point(341, 72);
            this.DurOut1.Name = "DurOut1";
            this.DurOut1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DurOut1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DurOut1.Properties.Appearance.Options.UseFont = true;
            this.DurOut1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DurOut1.Properties.Mask.EditMask = "HH:mm";
            this.DurOut1.Size = new System.Drawing.Size(88, 20);
            this.DurOut1.TabIndex = 20;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.Grd1);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 297);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 176);
            this.panel4.TabIndex = 30;
            // 
            // Grd1
            // 
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 22);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(772, 154);
            this.Grd1.TabIndex = 59;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            // 
            // FrmWorkSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmWorkSchedule";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtWSCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeIn1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWSName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHolidayInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeOut1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeIn2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeOut2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeOut3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeIn3.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeductionRoundCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakOut4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakIn4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakOut3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakIn3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeOut4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeIn4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDoubleShiftInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOTOut2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOTIn2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurIn2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakOut2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakIn2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOut2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOTOut1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOTIn1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurIn1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakOut1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurBreakIn1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DurOut1.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TimeEdit TmeOut2;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TimeEdit TmeIn2;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TimeEdit TmeOut1;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkHolidayInd;
        internal DevExpress.XtraEditors.TextEdit TxtWSCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TimeEdit TmeIn1;
        private DevExpress.XtraEditors.TextEdit TxtWSName;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        internal DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TimeEdit TmeOut3;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TimeEdit TmeIn3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        internal DevExpress.XtraEditors.TimeEdit DurOTOut1;
        internal DevExpress.XtraEditors.TimeEdit DurOTIn1;
        internal DevExpress.XtraEditors.TimeEdit DurIn1;
        internal DevExpress.XtraEditors.TimeEdit DurBreakOut1;
        internal DevExpress.XtraEditors.TimeEdit DurBreakIn1;
        internal DevExpress.XtraEditors.TimeEdit DurOut1;
        internal DevExpress.XtraEditors.TimeEdit DurOTOut2;
        internal DevExpress.XtraEditors.TimeEdit DurOTIn2;
        internal DevExpress.XtraEditors.TimeEdit DurIn2;
        internal DevExpress.XtraEditors.TimeEdit DurBreakOut2;
        internal DevExpress.XtraEditors.TimeEdit DurBreakIn2;
        internal DevExpress.XtraEditors.TimeEdit DurOut2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label LblSiteCode;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private DevExpress.XtraEditors.CheckEdit ChkDoubleShiftInd;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TimeEdit DurBreakOut4;
        internal DevExpress.XtraEditors.TimeEdit DurBreakIn4;
        internal DevExpress.XtraEditors.TimeEdit DurBreakOut3;
        internal DevExpress.XtraEditors.TimeEdit DurBreakIn3;
        internal DevExpress.XtraEditors.TimeEdit TmeOut4;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TimeEdit TmeIn4;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        public DevExpress.XtraEditors.LookUpEdit LueDeductionRoundCode;
    }
}