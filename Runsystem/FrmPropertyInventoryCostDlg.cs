﻿#region Update
/*
    28/11/2022 [SET/BBT] Manu Baru
    10/01/2023 [MAU/BBT] Feedback : dokumen master property inventory yang sudah di cancel atau tidak aktif , tidak bisa di tarik pada menu PICC
    11/01/2023 [SET/BBT] error sql ketika menggunakan filter
    12/01/2023 [MAU/BBT] tambah validasi show data
    13/01/2023 [MAU/BBT] BUG : penyesuaian filter Property Inventory Category
    13/01/2023 [MAU/BBT] Tambah validasi show data (Remstockvalue)
    14/01/2023 [MAU/BBT] Tambah validasi show data (Remstockvalue)
    16/01/2023 [MAU/BBT] BUG pada filter di list property inventory
    19/01/2023 [SET/BBT] deklarasi value mPropertyCategoryCode dari mFrmParent
    13/02/2023 [SET/BBT] deklarasi value mSiteCode & mCCCode dari mFrmParent
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryCostDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPropertyInventoryCost mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPropertyInventoryCostDlg(FrmPropertyInventoryCost FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List of Property";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            //Sl.SetLueAssetCategoryCode(ref LuePropertyCtCode);
            SetLuePropertyCtCode(ref LuePropertyCtCode, string.Empty);
            Sl.SetLueSiteCode(ref LueSiteCode);
            ShowData();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropertyCode, A.PropertyName, A.RegistrationDt, A.PropertyCategoryCode, B.PropertyCategoryName, A.PropertyInventoryValue, ");
            SQL.AppendLine("F.SiteName, D.CCName, A.InventoryQty, A.UoMCode, A.RemStockQty, A.RemStockValue, A.AcNo, E.AcDesc, A.SiteCode, A.CCCode ");
            SQL.AppendLine("");
            SQL.AppendLine("From TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory B On A.PropertyCategoryCode = B.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode And A.CancelInd = 'N' AND A.ActInd = 'Y' And A.Status = 'A' AND A.RemStockQty > 0 ");
            SQL.AppendLine("Inner Join TblCostCenter D On A.CCCode = D.CCCode ");
            SQL.AppendLine("Left Join TblCOA E On A.AcNo = E.AcNo ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode = F.SiteCode ");
            SQL.AppendLine("Where 1=1 ");

            //SQL.AppendLine("WHERE A.CancelInd = 'N' AND A.ActInd = 'Y' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
            Grd1, new string[]
            {
                //0
                "No.",

                //1-5
                "Date of" + Environment.NewLine + "Registration",
                "Property's Code",
                "Property's Name",
                "Initial Cost Center",
                "Site",

                //6-10
                "Property Inventory" + Environment.NewLine + "Value",
                "Property Category",
                "Inventory Quantity",
                "UoM",
                "Remaining Stock" + Environment.NewLine + "Quantity",

                //11-15
                "Remaining Stock" + Environment.NewLine + "Value",
                "COA#",
                "COA Description",
                "PropertyCategoryCode",
                "SiteCode",

                //16
                "CCCode"
            },
            new int[]
            {
                //0
                50,

                //1-5
                120, 200, 250, 250, 200,

                //6-9
                200, 200, 100, 70, 100,

                //11-13
                200, 150, 200, 100, 100,

                //16
                100
            }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10, 11 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
        }
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtPropertyName.Text, new string[] { "A.PropertyName", "A.PropertyCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePropertyCtCode), "A.PropertyCategoryCode", true);
                
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "F.SiteCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, mSQL + Filter + "  Order By A.PropertyCode",
                new string[]
                {
                    //0
                    "RegistrationDt", 

                    //1-5
                    "PropertyCode", "PropertyName", "CCName", "SiteName", "PropertyInventoryValue",

                    //6-8
                    "PropertyCategoryName", "InventoryQty", "UoMCode", "RemStockQty", "RemStockValue", 

                    //11-15
                    "AcNo", "AcDesc", "PropertyCategoryCode", "SiteCode", "CCCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 15);
                    //mFrmParent.mPropertyCategoryCode = Sm.DrStr(dr, c[13]);
                    //mFrmParent.mSiteCode = Sm.DrStr(dr, c[14]);
                    //mFrmParent.mCCCode = Sm.DrStr(dr, c[15]);
                }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 0))
                {
                    //var a = mFrmParent.mPropertyCategoryCode;
                    //var b = mFrmParent.mSiteCode;
                    //var c = mFrmParent.mCCCode;
                    mFrmParent.mPropertyCategoryCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 14);
                    mFrmParent.mSiteCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 15);
                    mFrmParent.mCCCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 16);
                    mFrmParent.ShowPropertyInventoryData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2));
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        internal void SetLuePropertyCtCode(ref LookUpEdit Lue, string PropertyCategoryCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.PropertyCategoryCode As Col1, A.PropertyCategoryName As Col2 ");
            SQL.AppendLine("From TblPropertyInventoryCategory A  ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            if (PropertyCategoryCode.Length > 0)
                SQL.AppendLine("And A.PropertyCategoryCode = @PropertyCategoryCode ");
            SQL.AppendLine("Order By A.PropertyCategoryName ");

            cm.CommandText = SQL.ToString();
            if (PropertyCategoryCode.Length > 0) Sm.CmParam<String>(ref cm, "@PropertyCategoryCode", PropertyCategoryCode);

            Sm.SetLue2(
                ref Lue,
                ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (PropertyCategoryCode.Length > 0) Sm.SetLue(Lue, PropertyCategoryCode);
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePropertyCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePropertyCtCode, new Sm.RefreshLue1(Sl.SetLueAssetCategoryCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkPropertyCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Asset's Category");
        }
        private void TxtPropertyName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropertyName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset's Name");
        }
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #region Grid Event
        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
