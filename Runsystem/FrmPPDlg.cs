﻿#region Update
/*
    04/12/2017 [TKG] tambah filter dan informasi item.
    22/05/2018 [WED] 
            tambah kolom Volume (dari volume item packaging unit * qty packaging SO), 
            packaging qty (dari SO), 
            dan packaging uom (dari SO dan inner join ke item packaging unit)
            berdasarkan parameter IsPPShowPackagingValue
    14/01/2021 [WED/KSM] tambah tarik dari Sales Contract berdasarkan parameter IsSalesContractEnabled
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPPDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPP mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPPDlg(FrmPP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Select T.* From (");
            if (mFrmParent.mIsSalesContractEnabled)
            {
                SQL.AppendLine("    Select '1' As DocType, A.DocNo, A.DocDt, A.UsageDt, ");
                SQL.AppendLine("    A.ItCode, B.ItName, B.Specification, ");
                SQL.AppendLine("    Case A.DocType When '1' Then C.CtItCode When '3' Then E.CtItCode Else '' End As CtItCode, ");
                SQL.AppendLine("    A.Qty, B.PlanningUomCode, (A.Qty*B.PlanningUomCodeConvert12) As Qty2, B.PlanningUomCode2, ");
                SQL.AppendLine("    A.Remark, ");
                SQL.AppendLine("    Case A.DocType When '1' Then IfNull((D.Volume * D.QtyPackagingUnit), 0) When '3' Then IfNull((E.Volume * E.QtyPackagingUnit), 0) Else 0 End As Volume, ");
                SQL.AppendLine("    Case A.DocType When '1' Then IfNull(D.QtyPackagingUnit, 0) When '3' Then IfNull(E.QtyPackagingUnit, 0) Else 0 End As QtyPackagingUnit, ");
                SQL.AppendLine("    Case A.DocType When '1' Then IfNull(D.PackagingUnitUomCode, '') When '3' Then IfNull(E.PackagingUnitUomCode, '') Else '' End As PackagingUnitUomCode ");
            }
            else
            {
                SQL.AppendLine("    Select '1' As DocType, A.DocNo, A.DocDt, A.UsageDt, ");
                SQL.AppendLine("    A.ItCode, B.ItName, B.Specification, C.CtItCode, ");
                SQL.AppendLine("    A.Qty, B.PlanningUomCode, (A.Qty*B.PlanningUomCodeConvert12) As Qty2, B.PlanningUomCode2, ");
                SQL.AppendLine("    A.Remark, IfNull((D.Volume * D.QtyPackagingUnit), 0) Volume, IfNull(D.QtyPackagingUnit, 0) QtyPackagingUnit, IfNull(D.PackagingUnitUomCode, '') PackagingUnitUomCode ");
            }
            SQL.AppendLine("    From TblProductionOrderHdr A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("    LEFT JOIN TblCustomerItem C ON A.ItCode = C.ItCode ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select T1.DocNo, T2.DNo, T4.ItCode, T2.QtyPackagingUnit, T2.PackagingUnitUomCode, T6.Volume ");
	        SQL.AppendLine("        From TblSOHdr T1 ");
	        SQL.AppendLine("        Inner Join TblSODtl T2 On T1.DocNo = T2.DocNo ");
	        SQL.AppendLine("        Inner Join TblCtQtDtl T3 On T1.CtQtDocNo = T3.DocNo And T2.CtQtDNo = T3.DNo ");
	        SQL.AppendLine("        Inner Join TblItemPriceDtl T4 On T3.ItemPriceDocNo = T4.DocNo And T3.ItemPriceDNo = T4.DNo ");
	        SQL.AppendLine("        Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
	        SQL.AppendLine("        Inner Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T6.UomCode = T2.PackagingUnitUomCode ");
            SQL.AppendLine("    ) D On A.SODocNo = D.DocNo And B.ItCode = D.ItCode ");
            if (mFrmParent.mIsSalesContractEnabled)
            {
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select T1.DocNo, T4.Qty As QtyPackagingUnit, T5.SalesUomCode PackagingUnitUomCode, T6.Volume, T7.CtItCode ");
                SQL.AppendLine("        From TblProductionOrderHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesContract T2 On T1.SCDocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.CancelInd = 'N' ");
                SQL.AppendLine("            And T1.ProcessInd = 'O' ");
                SQL.AppendLine("        Inner Join TblSalesMemoHdr T3 On T2.SalesMemoDocNo = T3.DocNo ");
                SQL.AppendLine("        Inner Join TblSalesMemoDtl T4 On T3.DocNo = T4.DocNo And T1.SMDNo = T4.DNo ");
                SQL.AppendLine("        Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
                SQL.AppendLine("        Left Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T5.SalesUomCode = T6.UomCode ");
                SQL.AppendLine("        Left Join TblCustomerItem T7 On T3.CtCode = T7.CtCode And T5.ItCode = T7.ItCode ");
                SQL.AppendLine("    ) E On A.DocNo = E.DocNo ");
            }
            SQL.AppendLine("    Where A.ProcessInd='O' And A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And Locate(Concat('##', A.DocNo, '##'), @SelectedDocNo)<1 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As DocType, A.DocNo, A.DocDt, Null As UsageDt, ");
            SQL.AppendLine("    A.ItCode, B.ItName, B.Specification, C.CtItCode, ");
            SQL.AppendLine("    A.Qty, B.PlanningUomCode, (A.Qty*B.PlanningUomCodeConvert12) As Qty2, B.PlanningUomCode2, ");
            SQL.AppendLine("    A.Remark, 0 As Volume, 0 As QtyPackagingUnit, '' As PackagingUnitUomCode ");
            SQL.AppendLine("    From TblJCHdr A ");
            SQL.AppendLine("    Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("    LEFT JOIN TblCustomerItem C ON A.ItCode = C.ItCode ");
            SQL.AppendLine("    Where A.ProcessInd='O' And A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And Locate(Concat('##', A.DocNo, '##'), @SelectedDocNo)<1 ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "",
                        "Type",
                        "Date",
                        
                        
                        //6-10
                        "Usage Date",
                        "Item's Code",
                        "", 
                        "Item's Name", 
                        "Quantity",

                        //11-15
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Specification",

                        //16-19
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Volume",
                        "Packaging"+Environment.NewLine+"Qty",
                        "Packaging UoM"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 150, 20, 0, 80, 
                        
                        //6-10
                        80, 80, 20, 200, 100,
                        
                        //11-15
                        80, 100, 80, 200, 130,

                        //16-19
                        130, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 17, 18 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Grd1.Cols[15].Move(10);
            Grd1.Cols[16].Move(11);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 8, 15 }, false);
            if (!mFrmParent.mIsPPShowPackagingValue)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 8, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty; ;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedDocNo", mFrmParent.GetSelectedDocNo());

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By DocDt, DocNo, ItName;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocType", "DocDt", "UsageDt", "ItCode", "ItName",   
                            
                            //6-10
                            "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2", "Remark",

                            //11-15
                            "Specification", "CtItCode", "Volume", "QtyPackagingUnit", "PackagingUnitUomCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 19);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10, 12, 17, 18 });
                    }
                }
            }

            if (!IsChoose) 
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 production order.");
        }

        private bool IsDocNoAlreadyChosen(int Row)
        {
            string DocNo = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(DocNo, Sm.GetGrdStr(mFrmParent.Grd1, Index, 2)))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.GetGrdStr(Grd1, e.RowIndex, 4) == "1")
                {
                    var f1 = new FrmProductionOrder(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmJC(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 7));
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 4) == "1")
                {
                    var f1 = new FrmProductionOrder(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmJC(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
