﻿#region Update
// 24/03/2018 [HAR] BUG level tidak muncul
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;


#endregion

namespace RunSystem
{
    public partial class FrmBehaviourInd : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "";
        internal FrmBehaviourIndFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmBehaviourInd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Behaviour Indicator";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                MeeBhvGrd1.Visible = false;
                MeeBhvGrd2.Visible = false;
                MeeBhvGrd3.Visible = false;
                MeeBhvGrd4.Visible = false;
                MeeBhvGrd5.Visible = false;
                MeeRemarkGrd1.Visible = false;
                MeeRemarkGrd2.Visible = false;
                MeeRemarkGrd3.Visible = false;
                MeeRemarkGrd4.Visible = false;
                MeeRemarkGrd5.Visible = false;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Dno",
                        
                        //1-2
                        "Evidence Behaviour",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-2
                        400, 400
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            #endregion

            #region Grid 2
            Grd2.Cols.Count = 3;
            Grd2.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Dno",
                        
                        //1-5
                        "Evidence Behaviour",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        400, 400
                    }
                );
            Sm.GrdColInvisible(Grd2, new int[] { 0 });
            #endregion

            #region Grid 3
            Grd3.Cols.Count = 3;
            Grd3.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "Dno",
                        
                        //1-5
                        "Evidence Behaviour",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        400, 400
                    }
                );
            Sm.GrdColInvisible(Grd3, new int[] { 0 });
            #endregion

            #region Grid 4
            Grd4.Cols.Count = 3;
            Grd4.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "Dno",
                        
                        //1-5
                        "Evidence Behaviour",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        400, 400
                    }
                );
            Sm.GrdColInvisible(Grd4, new int[] { 0 });
            #endregion

            #region Grid 5
            Grd5.Cols.Count = 3;
            Grd5.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "Dno",
                        
                        //1-5
                        "Evidence Behaviour",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        400, 400
                    }
                );
            Sm.GrdColInvisible(Grd5, new int[] { 0 });
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, TxtCompetenceCode, TxtCompetenceName, MeeInd, LueLevel, MeeRemark }, true);
                    BtnCompetenceCode.Enabled = false;
                    BtnCompetenceCode2.Enabled = false;
                    TxtDocNo.Focus();
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2 });
                    ChkActInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueLevel, MeeInd, MeeRemark }, false);
                    BtnCompetenceCode.Enabled = true;
                    BtnCompetenceCode2.Enabled = true;
                    DteDocDt.Focus();
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 2 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 1, 2 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 1, 2 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 1, 2 });
                    ChkActInd.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtCompetenceCode, TxtCompetenceName, LueLevel, MeeRemark, MeeInd
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.FocusGrd(Grd3, 0, 0);
            Sm.FocusGrd(Grd4, 0, 0);
            Sm.FocusGrd(Grd5, 0, 0);
            ChkActInd.Checked = false;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
            Sm.ClearGrd(Grd5, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBehaviourIndFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCompetenceCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Additional Method
        public void SetLueLevelCode(ref LookUpEdit Lue, string CompetenceCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (CompetenceCode.Length > 0)
            {
                int LevelFrom = Convert.ToInt32(Sm.GetValue("Select LevelFrom From TblCompetence Where CompetenceCode = '" + CompetenceCode + "' "));
                int LevelTo = Convert.ToInt32(Sm.GetValue("Select LevelTo From TblCompetence Where CompetenceCode = '" + CompetenceCode + "' "));

                for (int j = LevelFrom; j <= LevelTo; j++)
                {
                    SQL.AppendLine("Select Distinct '" + j + "' As Col1, '" + j + "' As Col2  From TblCompetence ");
                    if (j != LevelTo)
                    {
                        SQL.AppendLine("Union All ");
                    }
                }
            }
            else
            {
                SQL.AppendLine("Select '' As Col1, '' As Col2 ");
            }

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void MeeRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.MemoExEdit Mee,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Mee.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Mee.EditValue = null;
            else
                Mee.EditValue = Sm.GetGrdStr(Grd, fCell.RowIndex, 0);

            Mee.Visible = true;
            Mee.Focus();

            fAccept = true;
        }
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;

            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BehaviourInd", "TblbehaviourIndHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            
            cml.Add(SaveBehaviourIndHdr(DocNo));
            int DNo = 0;
            DNo = 0;
            SaveBehaviourIndicatorDtl(ref cml, DocNo, ref DNo, "1", Grd1); 
            SaveBehaviourIndicatorDtl(ref cml, DocNo, ref DNo, "2", Grd2);
            SaveBehaviourIndicatorDtl(ref cml, DocNo, ref DNo, "3", Grd3);
            SaveBehaviourIndicatorDtl(ref cml, DocNo, ref DNo, "4", Grd4);
            SaveBehaviourIndicatorDtl(ref cml, DocNo, ref DNo, "5", Grd5);

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtCompetenceCode, "Competece", false) ||
                Sm.IsLueEmpty(LueLevel, "Competence Level")||
                Sm.IsMeeEmpty(MeeInd, "Indicator")||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords()||
                IsCompetenceAlreadyCreated()
                ;
        }

        private bool IsCompetenceAlreadyCreated()
        {
            string keyword = string.Concat(TxtCompetenceCode.Text, Sm.GetLue(LueLevel));
            string dataExist = Sm.GetValue("Select 1 From TblBehaviourIndhdr Where Activeind = 'Y' And Concat(CompetenceCode,Competencelevel)='"+keyword+"' ");
                   
            if(dataExist.Length>0)
            {
                Sm.StdMsg(mMsgType.Warning, "Competence for this level has been created.");
                return true;
            }
            else
                return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 evidence behaviour.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Evidence data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Evidence data entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Evidence data entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd4.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Evidence data entered (" + (Grd4.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd5.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Evidence data entered (" + (Grd5.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveBehaviourIndHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblBehaviourIndHdr(DocNo, DocDt, CompetenceCode, ActiveInd, CompetenceLevel, Indicator, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @CompetenceCode, @ActiveInd, @CompetenceLevel, @Indicator, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", TxtCompetenceCode.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActInd.Checked==true?"Y":"N");
            Sm.CmParam<String>(ref cm, "@CompetenceLevel",Sm.GetLue(LueLevel));
            Sm.CmParam<String>(ref cm, "@Indicator", MeeInd.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveBehaviourIndDtl(string DocNo, ref int DNo, string SectionNo, iGrid Grd, int Row)
        {
            var SQL = new StringBuilder();

            DNo += 1;

            SQL.AppendLine("Insert Into TblBehaviourIndDtl(DocNo, DNo, SectionNo, Evidence, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @SectionNo, @Evidence, @Remark, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);
            Sm.CmParam<String>(ref cm, "@Evidence", Sm.GetGrdStr(Grd, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void SaveBehaviourIndicatorDtl(ref List<MySqlCommand> cml, string DocNo, ref int DNo, string SectionNo, iGrid Grd)
        {
            for (int Row = 0; Row < Grd.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd, Row, 1).Length > 0)
                    cml.Add(SaveBehaviourIndDtl(DocNo, ref DNo, SectionNo, Grd, Row));
        }
        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditBehaviourInd());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsBehaviourIndNotDeactivated() ||
                IsDataAlreadyInactive();
        }

        private bool IsBehaviourIndNotDeactivated()
        {
            if (ChkActInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to deactivate this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyInactive()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBehaviourIndHdr ");
            SQL.AppendLine("Where ActiveInd='N' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is inactive already.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditBehaviourInd()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBehaviourIndHdr Set ");
            SQL.AppendLine("    ActiveInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And ActiveInd='Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBehaviourIndHdr(DocNo);
                ShowBehaviourIndDtl(DocNo, "1", ref Grd1);
                ShowBehaviourIndDtl(DocNo, "2", ref Grd2);
                ShowBehaviourIndDtl(DocNo, "3", ref Grd3);
                ShowBehaviourIndDtl(DocNo, "4", ref Grd4);
                ShowBehaviourIndDtl(DocNo, "5", ref Grd5);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBehaviourIndHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
             
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActiveInd, A.CompetenceCode, B.CompetenceName, A.CompetenceLevel, A.Indicator, A.Remark ");
            SQL.AppendLine("From TblBehaviourIndHdr A ");
            SQL.AppendLine("Inner Join TblCompetence B On A.CompetenceCode=B.CompetenceCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "ActiveInd", "CompetenceCode", "CompetenceName", "CompetenceLevel",  
                        //6
                        "Indicator", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtCompetenceCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtCompetenceName.EditValue = Sm.DrStr(dr, c[4]);
                        SetLueLevelCode(ref LueLevel, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueLevel, Sm.DrStr(dr, c[5]));
                        MeeInd.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowBehaviourIndDtl(string DocNo, string SectionNo, ref iGrid Grdxx)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, Evidence, Remark  ");
            SQL.AppendLine("From TblBehaviourIndDtl  ");
            SQL.AppendLine("Where DocNo=@DocNo And SectionNo = @SectionNo ");
            SQL.AppendLine("Order By DNo ; ");

            Sm.ShowDataInGrid(
                ref Grdxx, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "Evidence", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
        }
    

        #endregion

        #endregion

        #region Event

        private void BtnCompetenceCode2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtCompetenceCode, "Competence#", false))
            {
                var f = new FrmCompetence(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCompetenceCode = TxtCompetenceCode.Text;
                f.ShowDialog();
            }
        }

        private void BtnCompetenceCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBehaviourIndDlg(this));
        }

        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue2(SetLueLevelCode), string.Empty);

            if (TxtCompetenceCode.Text.Length > 0)
            {
                Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue2(SetLueLevelCode), TxtCompetenceCode.Text);
            }
        }
      
        private void MeeBhvGrd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void MeeBhvGrd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void MeeBhvGrd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void MeeBhvGrd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void MeeBhvGrd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void MeeRemarkGrd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void MeeRemarkGrd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void MeeRemarkGrd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void MeeRemarkGrd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void MeeRemarkGrd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void MeeBhvGrd1_Leave(object sender, EventArgs e)
        {
            if (MeeBhvGrd1.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeBhvGrd1.Text.Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = MeeBhvGrd1.Text;
                }
                MeeBhvGrd1.Visible = false;
            }
        }

        private void MeeBhvGrd2_Leave(object sender, EventArgs e)
        {
            if (MeeBhvGrd2.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeBhvGrd2.Text.Length == 0)
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 1].Value = MeeBhvGrd2.Text;
                }
                MeeBhvGrd2.Visible = false;
            }
        }

        private void MeeBhvGrd3_Leave(object sender, EventArgs e)
        {
            if (MeeBhvGrd3.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeBhvGrd3.Text.Length == 0)
                    Grd3.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 1].Value = MeeBhvGrd3.Text;
                }
                MeeBhvGrd3.Visible = false;
            }
        }

        private void MeeBhvGrd4_Leave(object sender, EventArgs e)
        {
            if (MeeBhvGrd4.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeBhvGrd4.Text.Length == 0)
                    Grd4.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 1].Value = MeeBhvGrd4.Text;
                }
                MeeBhvGrd4.Visible = false;
            }
        }

        private void MeeBhvGrd5_Leave(object sender, EventArgs e)
        {
            if (MeeBhvGrd5.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (MeeBhvGrd5.Text.Length == 0)
                    Grd5.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 1].Value = MeeBhvGrd5.Text;
                }
                MeeBhvGrd5.Visible = false;
            }
        }

        private void MeeRemarkGrd1_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd1.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeRemarkGrd1.Text.Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = MeeRemarkGrd1.Text;
                }
                MeeRemarkGrd1.Visible = false;
            }
        }

        private void MeeRemarkGrd2_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd2.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeRemarkGrd2.Text.Length == 0)
                    Grd2.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 2].Value = MeeRemarkGrd2.Text;
                }
                MeeRemarkGrd2.Visible = false;
            }
        }

        private void MeeRemarkGrd3_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd3.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeRemarkGrd3.Text.Length == 0)
                    Grd3.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 2].Value = MeeRemarkGrd3.Text;
                }
                MeeRemarkGrd3.Visible = false;
            }
        }

        private void MeeRemarkGrd4_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd4.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeRemarkGrd1.Text.Length == 0)
                    Grd4.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 2].Value = MeeRemarkGrd4.Text;
                }
                MeeRemarkGrd4.Visible = false;
            }
        }

        private void MeeRemarkGrd5_Leave(object sender, EventArgs e)
        {
            if (MeeRemarkGrd5.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (MeeRemarkGrd1.Text.Length == 0)
                    Grd5.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 2].Value = MeeRemarkGrd5.Text;
                }
                MeeRemarkGrd5.Visible = false;
            }
        }
        #endregion

        #region Grid Event
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd1, MeeBhvGrd1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd1, MeeRemarkGrd1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd2, MeeBhvGrd2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd2, MeeRemarkGrd2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd3, MeeBhvGrd3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd3, MeeRemarkGrd3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd4, MeeBhvGrd4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd4, MeeRemarkGrd4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                MeeRequestEdit(Grd5, MeeBhvGrd5, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                MeeRequestEdit(Grd5, MeeRemarkGrd5, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdEnter(Grd4, e);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            Sm.GrdEnter(Grd5, e);
            Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
        }
        #endregion
       
    }
}
