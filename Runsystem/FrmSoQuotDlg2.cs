﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSoQuotDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSoQuot mFrmParent;
        private string mSQL = string.Empty, mDocDt= string.Empty, mCurCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSoQuotDlg2(FrmSoQuot FrmParent, string DocDt, string CurCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
            mCurCode = CurCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLueItCtCode(ref LueItCtCode);
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                   Grd1, new string[] 
                    { 
                        //0
                        "No.",

                        //1-5
                        "",
                        "Item"+Environment.NewLine+"Code",
                        "",
                        "Item"+Environment.NewLine+"Name",
                        "Item"+Environment.NewLine+"Category",
                        
                        //6-10
                        "Document"+Environment.NewLine+"Number",
                        "DNo",
                        "",
                        "Currency",
                        "Unit Price",
                        
                        //11-12
                        "UoM",
                        "Start"+Environment.NewLine+"Date"
                    }
               );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, B.ItCtName, A.SalesUomCode, ");
            SQL.AppendLine("C.DocNo, C.DNo, C.CurCode, C.UPrice, C.StartDt ");
            SQL.AppendLine("From TblItem A " );
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select C1.DocNo, C2.DNo, C1.CurCode, C1.StartDt, C2.ItCode, C2.UPrice ");
            SQL.AppendLine("    From TblItemPriceHdr C1 ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select T2.ItCode, Max(Concat(T1.StartDt, T1.DocNo)) As Key1 ");
	        SQL.AppendLine("        From TblItemPriceHdr T1 ");
	        SQL.AppendLine("        Inner Join TblItemPriceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Where T1.StartDt<=@DocDt And T1.CurCode=@CurCode ");
	        SQL.AppendLine("        Group By T2.ItCode ");
            SQL.AppendLine("    ) C3 On C2.ItCode=C3.ItCode And Concat(C1.StartDt, C1.DocNo)=Key1 ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where A.ItCode Not In (" + mFrmParent.GetSelectedItem() + ") ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocDt", mDocDt);
                Sm.CmParam<String>(ref cm, "@CurCode", mCurCode);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItCode",
                        new string[] 
                        { 
                        
                            //0
                            "ItCode", 

                            //1-5
                            "ItName", "ItCtName", "DocNo", "DNo", "CurCode", 
                            
                            //6-8
                            "UPrice", "SalesUomCode", "StartDt"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 8, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 11);

                        mFrmParent.ComputePriceAfterDiscount(Row1);

                        mFrmParent.Grd2.Rows.Add();

                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 11, 12, 13, 14 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd2, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd2, Index, 4),
                    Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 4)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItemPrice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItemPrice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion
        
        #endregion

    }
}
