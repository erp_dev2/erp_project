﻿#region Update
/*
    06/02/2017 [WED] revisi nilai price
    20/06/2017 [WED] revisi summary saat grouping
    03/07/2017 [WED] revisi price
    06/10/2017 [WED] revisi grouping StockMovement, ditambah group by DocDt
    06/10/2017 [WED] revisi query, menghilangkan query abs()
    16/10/2017 [WED] revisi query, group by ditambahkan berdasarkan DocNo
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockOutAccumulated : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<Warehouse> lWhs = null;
        private int mFirstColumnForWhs = -1;
        internal int ColCounts = 0;

        #endregion

        #region Constructor

        public FrmRptStockOutAccumulated(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();

                Sl.SetLueWhsCode(ref LueWhsCode);
                SetLueDocType(ref LueDocType);

                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-29);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL(string DocType)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType, G.OptDesc As DocTypes, A.ItCode, C.ItName, C.ItCtCode, D.ItCtName, ");
            SQL.AppendLine("A.WhsCode, F.WhsName As WhsFrom, ");
            SQL.AppendLine("E.WhsCode2, E.WhsName As WhsTo, ");
            if (DocType == "09")
                SQL.AppendLine("(Sum(A.Qty) * B.UPrice * B.ExcRate * -1) As Price ");
            else
                SQL.AppendLine("Sum(A.Qty * B.UPrice * B.ExcRate * -1) As Price ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocType, T1.DocDt, T1.DocNo, T1.Source, T1.WhsCode, T1.ItCode, T1.BatchNo, Sum(T1.Qty) As Qty ");
            SQL.AppendLine("    From TblStockMovement T1 ");
            SQL.AppendLine("    Inner Join TblItem T2 On T1.ItCode = T2.ItCode ");
            //SQL.AppendLine("    Where T1.CancelInd = 'N' "); //1
            SQL.AppendLine("    Where 0 = 0 ");
            //SQL.AppendLine("    And T1.Qty < 0 ");
            SQL.AppendLine("    And T1.WhsCode = @WhsCode ");
            SQL.AppendLine("    And T1.DocType = @DocType ");
            SQL.AppendLine("    And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("    Group By T1.DocType, T1.Source, T1.WhsCode, T1.ItCode ");  //1
            SQL.AppendLine("    Group By T1.DocType, T1.DocNo, T1.DocDt, T1.WhsCode, T1.ItCode, T1.Source "); //2
            //SQL.AppendLine("    Group By T1.DocType, T1.DocDt, T1.WhsCode, T1.ItCode, T1.Source, T1.DocNo "); //3
            SQL.AppendLine("    Having Sum(T1.Qty <> 0) ");
            SQL.AppendLine(")A ");
            SQL.AppendLine("Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("Inner Join TblItem C on A.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode ");

            if (DocType == "05")
            {
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  Select T1.DocNo, T1.DeptCode As WhsCode2, T2.DeptName As WhsName ");
                SQL.AppendLine("  From TblDODeptHdr T1 ");
                SQL.AppendLine("  Inner Join TblDepartment T2 On T1.DeptCode = T2.DeptCode ");
                SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            }
            else if (DocType == "09")
            {
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  Select T1.DocNo, T1.WhsCode2, T2.WhsName ");
                SQL.AppendLine("  From TblDOWhsHdr T1 ");
                SQL.AppendLine("  Inner Join TblWarehouse T2 On T1.WhsCode2 = T2.WhsCode ");
                SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            }
            else if (DocType == "14")
            {
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  Select T1.DocNo, T1.DeptCode As WhsCode2, T2.DeptName As WhsName ");
                SQL.AppendLine("  From TblDODeptHdr T1 ");
                SQL.AppendLine("  Inner Join TblDepartment T2 On T1.DeptCode = T2.DeptCode ");
                SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            }
            else if (DocType == "15")
            {
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  Select T1.DocNo, T1.WhsCode As WhsCode2, T2.WhsName ");
                SQL.AppendLine("  From TblRecvWhs2Hdr T1 ");
                SQL.AppendLine("  Inner Join TblWarehouse T2 On T1.WhsCode = T2.WhsCode ");
                SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            }
            else if (DocType == "26")
            {
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("  Select T1.DocNo, T1.WhsCode2, T2.WhsName ");
                SQL.AppendLine("  From TblDOWhsHdr T1 ");
                SQL.AppendLine("  Inner Join TblWarehouse T2 On T1.WhsCode2 = T2.WhsCode ");
                SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            }

            SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode = F.WhsCode ");
            SQL.AppendLine("Inner Join TblOption G On A.DocType = G.OptCode And G.OptCat = 'InventoryTransType' ");
            SQL.AppendLine("Where Find_In_Set(A.DocType, (Select ParValue From TblParameter Where ParCode = 'DocTypeForStockOut')) ");
            SQL.AppendLine("And (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            ColCounts = Grd1.Cols.Count - 1;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Type",
                        "Item Code",
                        "Item Name",
                        "Item Category" + Environment.NewLine + "Code",
                        "Item Category",

                        //6
                        "Total"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        250, 100, 180, 100, 180,

                        //6
                        100
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                IsFilterByDateInvalid() ||
                Sm.IsLueEmpty(LueDocType, "Type") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse")
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                var sqldebug = SetSQL(Sm.GetLue(LueDocType)) + Filter + " Group By A.DocType, A.WhsCode, A.ItCode;";
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SetSQL(Sm.GetLue(LueDocType)) + Filter + " Group By A.DocType, A.WhsCode, A.ItCode;",
                    new string[]
                    { 
                        //0
                        "DocTypes", 
                        
                        //1-5
                        "ItCode", "ItName", "ItCtCode", "ItCtName", "Price"
                    },

                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        //Grd.Cells[Row, 6].Value = 0m;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    }, true, false, false, false
                );

                lWhs = new List<Warehouse>();
                SetWhsToColumn(SetSQL(Sm.GetLue(LueDocType)) + Filter + " Group By A.DocType, A.WhsCode, E.WhsCode2, A.ItCode");
                ProcessHarga(SetSQL(Sm.GetLue(LueDocType)) + Filter + " Group By A.DocType, A.WhsCode, E.WhsCode2, A.ItCode");

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                int a = ColCounts;
                for (int r = 0; r <= lWhs.Count; r++)
                {
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                    {  
                        (a + r)
                    });
                }
                Sm.SetGrdAlwaysShowSubTotal(Grd1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }
        
        #region Additional Methods

        private bool IsFilterByDateInvalid()
        {
            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        private void SetLueDocType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.OptCode As Col1, T.OptDesc As Col2 ");
            SQL.AppendLine("From TblOption T Where T.OptCat = 'InventoryTransType' ");
            SQL.AppendLine("And Find_In_Set(IfNull(T.OptCode, ''), @Param) ");
            SQL.AppendLine("Order By T.OptDesc Asc; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Param", Sm.GetParameter("DocTypeForStockOut"));
            
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        private void SetWhsToColumn(string SQL)
        {
            lWhs.Clear();
            Grd1.Cols.Count = 7;
            var LatestColumn = Grd1.Cols.Count - 1;
            var cm1 = new MySqlCommand();
            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                var WhsColumn = new StringBuilder();
                string Filter = " ";

                WhsColumn.AppendLine("Select Distinct T.WhsCode2, T.WhsTo ");
                WhsColumn.AppendLine("From ");
                WhsColumn.AppendLine("( ");
                WhsColumn.AppendLine(SQL);
                WhsColumn.AppendLine(")T ");

                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandTimeout = 600;
                cm1.CommandText = WhsColumn.ToString();
                Sm.CmParamDt(ref cm1, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm1, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm1, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm1, "@DocType", Sm.GetLue(LueDocType));
                //Sm.FilterDt(ref Filter, ref cm1, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] { "WhsCode2", "WhsTo" });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        LatestColumn += 1;
                        if (mFirstColumnForWhs == -1) mFirstColumnForWhs = LatestColumn;
                        lWhs.Add(new Warehouse()
                        {
                            WhsCode2 = Sm.DrStr(dr1, c1[0]),
                            WhsName2 = Sm.DrStr(dr1, c1[1]),
                            Column = LatestColumn
                        });
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = Sm.DrStr(dr1, c1[1]);
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 250;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);
                    }
                }
                dr1.Close();
            }
        }

        private void ProcessHarga(string SQL)
        {
            if (lWhs.Count > 0)
            {
                for (int Col = mFirstColumnForWhs; Col < Grd1.Cols.Count; Col++)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, Col].Value = 0m;
                }
                var l = new List<PriceWhsTo>();
                ProcessHarga1(ref l, SQL);
                if (l.Count > 0) ProcessHarga2(ref l, ref lWhs);
            }
        }

        private void ProcessHarga1(ref List<PriceWhsTo> l, string SQL)
        {
            var cm = new MySqlCommand();
            string Filter = " ";

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL;
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WhsCode2", "Price", "ItCtCode", "WhsCode", "ItCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PriceWhsTo()
                        {
                            WhsToColumn = GetWhsColumn(Sm.DrStr(dr, c[0])),
                            Price = Sm.DrDec(dr, c[1]),
                            ItCtCode = Sm.DrStr(dr, c[2]),
                            WhsCode = Sm.DrStr(dr, c[3]),
                            ItCode = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private int GetWhsColumn(string WhsCode2)
        {
            foreach (var x in lWhs.Where(x => string.Compare(x.WhsCode2, WhsCode2) == 0))
                return x.Column;
            return -1;
        }

        private void ProcessHarga2(ref List<PriceWhsTo> l, ref List<Warehouse> lWhs)
        {
            for (var i = 0; i < l.Count; i++)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1) == LueDocType.Text &&
                        string.Compare(l[i].ItCode, Sm.GetGrdStr(Grd1, Row, 2)) == 0 &&
                        string.Compare(l[i].ItCtCode, Sm.GetGrdStr(Grd1, Row, 4)) == 0 &&
                        string.Compare(l[i].WhsCode, Sm.GetLue(LueWhsCode)) == 0)
                    {
                        if (l[i].WhsToColumn >= 0)
                        {
                            //Grd1.Cells[Row, 6].Value =+ l[i].Price;
                            Grd1.Cells[Row, l[i].WhsToColumn].Value = l[i].Price;
                        }
                        break;
                    }
                }
            }            
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            var ColCount = new int[Grd1.Cols.Count - 6];
            var i = 0;
            for (int c = 6; c < Grd1.Cols.Count; c++)
            {
                ColCount[i] = c;
                i++;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, ColCount);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        #endregion

        #endregion

        #region Class

        private class Warehouse
        {
            public string WhsCode2 { get; set; }
            public string WhsName2 { get; set; }
            public int Column { get; set; }
            
        }

        private class PriceWhsTo
        {
            public int WhsToColumn { get; set; }
            public decimal Price { get; set; }
            public string ItCtCode { get; set; }
            public string WhsCode { get; set; }
            public string ItCode { get; set; }
        }

        #endregion

    }

}
