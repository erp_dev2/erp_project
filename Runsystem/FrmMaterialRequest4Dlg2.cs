﻿#region Update
/*
    06/11/2020 [WED/IMS] New App
 *  23/06/2021 [ICA/IMS] Quotation muncul berdasarkan vendor 
 *  05/07/2021 [ICA/IMS] membuka frmQt dengan VdCode
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest4Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest4 mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;
        private int mCurRow;
        private bool mIsMaterialRequest2CreateNewQtDisabled = false;

        #endregion

        #region Constructor

        public FrmMaterialRequest4Dlg2(FrmMaterialRequest4 FrmParent, int CurRow, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = CurRow;
            mVdCode = VdCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

            GetParameter();

            LblQt.Visible = !mIsMaterialRequest2CreateNewQtDisabled;

            TxtItCode.Text = Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 6);
            TxtItName.Text = Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 8);

            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -360);
            SetGrd();
            SetSQL();
            Sl.SetLueVdCode2(ref LueVdCode, mVdCode);
            Sl.SetLuePtCode(ref LuePtCode);

            ShowData();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsMaterialRequest2CreateNewQtDisabled = Sm.GetParameterBoo("IsMaterialRequest2CreateNewQtDisabled");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document#",
                        "DNo",
                        "",
                        "Date",
                        "Vendor Code",

                        //6-10
                        "Vendor",
                        "Term of Payment",
                        "Currency",
                        "Price",
                        "Delivery Type",
                        
                        //11
                        "Remark"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, false);
            if (mIsMaterialRequest2CreateNewQtDisabled) Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, A.VdCode, C.VdName, D.PtName, F.DTName, A.CurCode, B.UPrice, B.Remark ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode And C.ActInd='Y' ");
            SQL.AppendLine("Left Join TblPaymentTerm D On A.PtCode=D.PtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.VdCode, T2.ItCode, ");
            SQL.AppendLine("    Max(Concat(T1.DocDt, T1.DocNo, T2.DNo)) As Key1  ");
            SQL.AppendLine("    From TblQtHdr T1, TblQtDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.ActInd = 'Y' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    Group By T1.VdCode, T2.ItCode ");
            SQL.AppendLine("    ) E On Concat(A.DocDt, B.DocNo, B.DNo)=E.Key1 And A.VdCode=E.VdCode And B.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblDeliveryType F On A.DTCode=F.DTCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter =
                    " Where A.Status = 'A' And  Concat(A.DocNo, B.DNo) Not In (" + mFrmParent.GetSelectedQt() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 6));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePtCode), "A.PtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo, B.DNo",
                        new string[] 
                        { 
                            //0
                            "DocNo",
                            
                            //1-5
                            "DNo", "DocDt", "VdCode", "VdName", "PtName",  
                            
                            //6-9
                            "CurCode", "UPrice", "DTName", "Remark" 
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row1 = mCurRow, Row2 = Grd1.CurRow.Index;

                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 2);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 4);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 5);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 6);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 7);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 8);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 9);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 10);
                //mFrmParent.ComputeTotalPOR(Row1);
                mFrmParent.ComputeTotal(Row1);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmQt' Limit 1;");
                var f = new FrmQt(MenuCode);
                f.Tag = MenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmQt' Limit 1;");
                var f = new FrmQt(MenuCode);
                f.Tag = MenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkPtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Term of payment");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LblQt_Click(object sender, EventArgs e)
        {
            var MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmQt' Limit 1;");
            var f = new FrmQt(MenuCode);
            f.Tag = MenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.mItCode = Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 6);
            f.mVdCode = mVdCode;
            f.ShowDialog();
            ShowData();
        }
        #endregion
    }
}
