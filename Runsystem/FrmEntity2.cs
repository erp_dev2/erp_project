﻿#region Update
/*
    06/07/2022 [RIS/PRODUCT] New Apps 

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEntity2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmEntity2Find FrmFind;
        private bool mIsDocNoUseEntCode = false,
            mEdit = false;
        
        #endregion

        #region Constructor

        public FrmEntity2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetLueEntCode(ref LueParent);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
          if (FrmFind != null) FrmFind.Close();
          
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEntCode, TxtEntName, ChkActInd, LueParent, TxtEntLogo, 
                        TxtEntPhone, TxtEntFax, MeeEntAddress, TxtShortCode, ChkAutoCreate, ChkConsolidate
                    }, true);
                    BtnAcNo1.Enabled = false;
                    BtnAcNo2.Enabled = false;
                    BtnAcNo3.Enabled = false;
                    BtnAcNo4.Enabled = false;
                    BtnAcNo5.Enabled = false;
                    BtnAcNo6.Enabled = false;
                    BtnAcNo7.Enabled = false;
                    BtnAcNo8.Enabled = false;
                    TxtEntCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEntName, ChkActInd, LueParent, TxtEntLogo, TxtEntPhone, 
                        TxtEntFax, MeeEntAddress, TxtShortCode, ChkConsolidate
                    }, false);
                    BtnAcNo1.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    BtnAcNo4.Enabled = true;
                    BtnAcNo5.Enabled = true;
                    BtnAcNo6.Enabled = true;
                    BtnAcNo7.Enabled = true;
                    BtnAcNo8.Enabled = true;
                    TxtEntName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEntName, ChkActInd, LueParent, TxtEntLogo, TxtEntPhone, 
                        TxtEntFax, MeeEntAddress
                    }, false);

                    if (!mIsDocNoUseEntCode || (mIsDocNoUseEntCode && TxtShortCode.Text.Length == 0)) 
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtShortCode }, false);
                    
                    BtnAcNo1.Enabled = true;
                    BtnAcNo2.Enabled = true;
                    BtnAcNo3.Enabled = true;
                    BtnAcNo4.Enabled = true;
                    BtnAcNo5.Enabled = true;
                    BtnAcNo6.Enabled = true;
                    BtnAcNo7.Enabled = true;
                    BtnAcNo8.Enabled = true;
                    TxtEntName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtEntCode, TxtEntName, LueParent, TxtEntLogo, TxtEntPhone,
                TxtEntFax, MeeEntAddress, TxtAcNo1, TxtAcDesc1, TxtAcNo2, 
                TxtAcDesc2, TxtAcNo3, TxtAcDesc3, TxtAcNo4, TxtAcDesc4,
                TxtAcNo5, TxtAcDesc5, TxtAcNo6, TxtAcDesc6, TxtAcNo7, TxtAcDesc7,
                TxtAcNo8, TxtAcDesc8, TxtShortCode, TxtEntCode2
            });
            ChkActInd.Checked = false;
            ChkAutoCreate.Checked = false;
            ChkConsolidate.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEntity2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            mEdit = false;
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEntCode, "", false)) return;
            mEdit = true;
            SetFormControl(mState.Edit);

        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtEntCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblEntity Where EntCode=@EntCode" };
                Sm.CmParam<String>(ref cm, "@EntCode", TxtEntCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;
                
                if (TxtEntCode.Text.Length == 0)
                    TxtEntCode.EditValue = GenerateEntityCode();

                if (TxtEntCode2.Text.Length == 0 && ChkConsolidate.Checked)
                    TxtEntCode2.EditValue = GenerateEntityCode2();

                var cml = new List<MySqlCommand>();

                cml.Add(SaveEntity());

                Sm.ExecCommands(cml);

                ShowData(TxtEntCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string EntCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.EntCode, A.EntName, A.ActInd, A.Parent, A.EntLogoName, ");
                SQL.AppendLine("A.EntPhone, A.EntFax, A.EntAddress, ");
                SQL.AppendLine("A.AcNo1, B.AcDesc As AcDesc1, ");
                SQL.AppendLine("A.AcNo2, C.AcDesc As AcDesc2, ");
                SQL.AppendLine("A.AcNo3, D.AcDesc As AcDesc3, ");
                SQL.AppendLine("A.AcNo4, E.AcDesc As AcDesc4, ");
                SQL.AppendLine("A.AcNo5, F.AcDesc As AcDesc5, ");
                SQL.AppendLine("A.AcNo6, G.AcDesc As AcDesc6, ");
                SQL.AppendLine("A.AcNo7, H.AcDesc As AcDesc7, ");
                SQL.AppendLine("A.AcNo8, I.AcDesc As AcDesc8, ");
                SQL.AppendLine("A.ShortCode, A.ConsolidateInd, A.AutoCreateInd ");
                SQL.AppendLine("From TblEntity A ");
                SQL.AppendLine("Left Join TblCOA B On A.AcNo1=B.AcNo ");
                SQL.AppendLine("Left Join TblCOA C On A.AcNo2=C.AcNo ");
                SQL.AppendLine("Left Join TblCOA D On A.AcNo3=D.AcNo ");
                SQL.AppendLine("Left Join TblCOA E On A.AcNo4=E.AcNo ");
                SQL.AppendLine("Left Join TblCOA F On A.AcNo5=F.AcNo ");
                SQL.AppendLine("Left Join TblCOA G On A.AcNo6=G.AcNo ");
                SQL.AppendLine("Left Join TblCOA H On A.AcNo7=H.AcNo ");
                SQL.AppendLine("Left Join TblCOA I On A.AcNo8=I.AcNo ");
                SQL.AppendLine("Where A.EntCode=@EntCode;");

                Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "EntCode", 

                            //1-5
                            "EntName", "ActInd", "Parent", "EntLogoName", "EntPhone", 
                            
                            //6-10
                            "EntFax", "EntAddress", "AcNo1", "AcDesc1", "AcNo2", 
                            
                            //11-15
                            "AcDesc2", "AcNo3", "AcDesc3", "AcNo4", "AcDesc4", 
                            
                            //16-20
                            "AcNo5", "AcDesc5", "AcNo6", "AcDesc6", "AcNo7", 
                            
                            //21-25
                            "AcDesc7", "AcNo8","AcDesc8", "ShortCode", "ConsolidateInd",

                            //26
                            "AutoCreateInd"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtEntCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtEntName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2])=="Y";
                            Sm.SetLue(LueParent, Sm.DrStr(dr, c[3]));
                            TxtEntLogo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtEntPhone.EditValue = Sm.DrStr(dr, c[5]);
                            TxtEntFax.EditValue = Sm.DrStr(dr, c[6]);
                            MeeEntAddress.EditValue = Sm.DrStr(dr, c[7]);
                            TxtAcNo1.EditValue = Sm.DrStr(dr, c[8]);
                            TxtAcDesc1.EditValue = Sm.DrStr(dr, c[9]);
                            TxtAcNo2.EditValue = Sm.DrStr(dr, c[10]);
                            TxtAcDesc2.EditValue = Sm.DrStr(dr, c[11]);
                            TxtAcNo3.EditValue = Sm.DrStr(dr, c[12]);
                            TxtAcDesc3.EditValue = Sm.DrStr(dr, c[13]);
                            TxtAcNo4.EditValue = Sm.DrStr(dr, c[14]);
                            TxtAcDesc4.EditValue = Sm.DrStr(dr, c[15]);
                            TxtAcNo5.EditValue = Sm.DrStr(dr, c[16]);
                            TxtAcDesc5.EditValue = Sm.DrStr(dr, c[17]);
                            TxtAcNo6.EditValue = Sm.DrStr(dr, c[18]);
                            TxtAcDesc6.EditValue = Sm.DrStr(dr, c[19]);
                            TxtAcNo7.EditValue = Sm.DrStr(dr, c[20]);
                            TxtAcDesc7.EditValue = Sm.DrStr(dr, c[21]);
                            TxtAcNo8.EditValue = Sm.DrStr(dr, c[22]);
                            TxtAcDesc8.EditValue = Sm.DrStr(dr, c[23]);
                            TxtShortCode.EditValue = Sm.DrStr(dr, c[24]);
                            ChkConsolidate.Checked = Sm.DrStr(dr, c[25]) == "Y";
                            ChkAutoCreate.Checked = Sm.DrStr(dr, c[26]) == "Y";
                            TxtEntCode2.EditValue = Sm.DrStr(dr, c[0]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return 
                Sm.IsTxtEmpty(TxtEntName, "Entity name", false) ||
                (mIsDocNoUseEntCode && Sm.IsTxtEmpty(TxtShortCode, "Short Code", false))
                ;
        }

        private MySqlCommand SaveEntity()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEntity(EntCode, EntName, ActInd, EntLogoName, Parent, EntPhone, EntFax, EntAddress, AcNo1, AcNo2, AcNo3, AcNo4, AcNo5, AcNo6, AcNo7, AcNo8, ShortCode, CreateBy, CreateDt, ConsolidateInd, AutoCreateInd) ");
            SQL.AppendLine("Values(@EntCode, @EntName, @ActInd, @EntLogoName, @Parent, @EntPhone, @EntFax, @EntAddress, @AcNo1, @AcNo2, @AcNo3, @AcNo4, @AcNo5, @AcNo6, @AcNo7, @AcNo8, @ShortCode, @UserCode, CurrentDateTime(), @ConsolidateInd, @AutoCreateInd) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update EntName=@EntName, ActInd=@ActInd, EntLogoName=@EntLogoName, Parent=@parent, EntPhone=@EntPhone, EntFax=@EntFax, EntAddress=@EntAddress, ");
            SQL.AppendLine("   AcNo1=@AcNo1, AcNo2=@AcNo2, AcNo3=@AcNo3, AcNo4=@AcNo4, AcNo5=@AcNo5, AcNo6=@AcNo6, AcNo7=@AcNo7, AcNo8=@AcNo8, ShortCode = @ShortCode,  ");
            SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (mEdit == false && ChkConsolidate.Checked)
            {
                SQL.AppendLine("Insert Into TblEntity(EntCode, EntName, ActInd, EntLogoName, Parent, EntPhone, EntFax, EntAddress, AcNo1, AcNo2, AcNo3, AcNo4, AcNo5, AcNo6, AcNo7, AcNo8, ShortCode, CreateBy, CreateDt, ConsolidateInd, AutoCreateInd) ");
                SQL.AppendLine("Values(@EntCode2, @EntName2, @ActInd, @EntLogoName, @Parent, @EntPhone, @EntFax, @EntAddress, @AcNo1, @AcNo2, @AcNo3, @AcNo4, @AcNo5, @AcNo6, @AcNo7, @AcNo8, @ShortCode, @UserCode, CurrentDateTime(), @ConsolidateInd2, @AutoCreateInd2) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update EntName=@EntName2, ActInd=@ActInd, EntLogoName=@EntLogoName, Parent=@parent, EntPhone=@EntPhone, EntFax=@EntFax, EntAddress=@EntAddress, ");
                SQL.AppendLine("   AcNo1=@AcNo1, AcNo2=@AcNo2, AcNo3=@AcNo3, AcNo4=@AcNo4, AcNo5=@AcNo5, AcNo6=@AcNo6, AcNo7=@AcNo7, AcNo8=@AcNo8, ShortCode = @ShortCode,  ");
                SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EntCode", TxtEntCode.Text);
            Sm.CmParam<String>(ref cm, "@EntName", TxtEntName.Text);
            Sm.CmParam<String>(ref cm, "@ConsolidateInd", ChkConsolidate.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@AutoCreateInd", ChkAutoCreate.Checked ? "Y" : "N") ;
            if (ChkConsolidate.Checked)
            {
                Sm.CmParam<String>(ref cm, "@EntCode2", TxtEntCode2.Text);
                Sm.CmParam<String>(ref cm, "@EntName2", TxtEntName.Text + " (Consolidated)");
                Sm.CmParam<String>(ref cm, "@ConsolidateInd2", "Y");
                Sm.CmParam<String>(ref cm, "@AutoCreateInd2", "Y");
            }
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@EntLogoName", TxtEntLogo.Text);
            Sm.CmParam<String>(ref cm, "@EntPhone", TxtEntPhone.Text);
            Sm.CmParam<String>(ref cm, "@EntFax", TxtEntFax.Text);
            Sm.CmParam<String>(ref cm, "@EntAddress", MeeEntAddress.Text);
            Sm.CmParam<String>(ref cm, "@AcNo1", TxtAcNo1.Text);
            Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
            Sm.CmParam<String>(ref cm, "@AcNo3", TxtAcNo3.Text);
            Sm.CmParam<String>(ref cm, "@AcNo4", TxtAcNo4.Text);
            Sm.CmParam<String>(ref cm, "@AcNo5", TxtAcNo5.Text);
            Sm.CmParam<String>(ref cm, "@AcNo6", TxtAcNo6.Text);
            Sm.CmParam<String>(ref cm, "@AcNo7", TxtAcNo7.Text);
            Sm.CmParam<String>(ref cm, "@AcNo8", TxtAcNo8.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private string GenerateEntityCode()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Right(Concat('00', IfNull(( ");
            SQL.AppendLine("    Select Convert(EntCode, Decimal)+1 As Value ");
            SQL.AppendLine("    From TblEntity Order By EntCode Desc Limit 1 ");
            SQL.AppendLine("), 1)), 3) As EntCode");

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateEntityCode2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Right(Concat('00', IfNull(( ");
            SQL.AppendLine("    Select Convert(EntCode, Decimal)+2 As Value ");
            SQL.AppendLine("    From TblEntity Order By EntCode Desc Limit 1 ");
            SQL.AppendLine("), 1)), 3) As EntCode");

            return Sm.GetValue(SQL.ToString());
        }

        #endregion

        #region Additional  Method

        private void GetParameter()
        {
            mIsDocNoUseEntCode = Sm.GetParameterBoo("IsDocNoUseEntCode");
        }

        private void ShowEntityDlg(byte type)
        {
            var f = new FrmEntity2Dlg(this, type);
            f.Tag = mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.ShowDialog();
        }

        public static void SetLueEntCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select EntCode As Col1, EntName As Col2 From TblEntity Where ActInd='Y' And AutoCreateInd ='Y' Order By EntName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEntCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEntCode);
        }

        private void TxtEntName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEntName);
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(SetLueEntCode));
        }

        private void TxtShortCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtShortCode);
        }

        #endregion

        #region Button Event

        private void BtnAcNo1_Click(object sender, EventArgs e)
        {
            ShowEntityDlg(1);
        }

        private void BtnAcNo2_Click(object sender, EventArgs e)
        {
            ShowEntityDlg(2);
        }

        private void BtnAcNo3_Click(object sender, EventArgs e)
        {
            ShowEntityDlg(3);
        }

        private void BtnAcNo4_Click(object sender, EventArgs e)
        {
            ShowEntityDlg(4);
        }

        private void BtnAcNo5_Click(object sender, EventArgs e)
        {
            ShowEntityDlg(5);
        }

        private void BtnAcNo6_Click(object sender, EventArgs e)
        {
            ShowEntityDlg(6);
        }

        private void BtnAcNo7_Click(object sender, EventArgs e)
        {
            ShowEntityDlg(7);
        }

        private void BtnAcNo8_Click(object sender, EventArgs e)
        {
            ShowEntityDlg(8);
        }
        #endregion

        #endregion
    }
}
