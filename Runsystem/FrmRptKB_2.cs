﻿#region Update
/*
    27/02/2019 [WED/MAI] new reporting
    20/03/2019 [WED/MAI] rombak reporting. type tidak mandatory, isinya : BC25, BC41, BC30, BC261, BC27
                     NOTE : BC30 belom ada, jadi belom dimasukkan ke SetLue
    01/04/2019 [WED/MAI] tambah checkbox LueType
    15/04/2019 [WED/MAI] dibedakan antara BC27 dan BC40
    18/04/2019 [MEY/MAI] Print out dibuat desainnya
    03/07/2019 [DITA/MAI] Perubahan header pada PrintOut
    05/07/2019 [DITA/MAI] uraian barang menggunakan item group name
    11/07/2019 [WED/MAI] BC25 dan BC41 dari DOCt berdasarkan CustomsDocCode
    24/10/2019 [DITA/MAI] Data Bc 261 yang sudah terinput dengan receiving item from other warehouse without do belum ketarik ke laporan pengeluaran
    11/01/2019 [DITA/MAI] Tambah data BC 30
    13/11/2019 [WED/MAI] supplier ambil dari recvvd
    18/11/2019 [VIN/MAI] penerimaan --> pengeluaran; name transaksi warehouse ambil dari warehouse
    19/11/2019 [WED/MAI] tambah do to vendor
    25/11/2019 [WED/MAI] BC261 RecvWhs, nomor bukti pengeluaran ambil dari nomor PL kaber, 261 warehouse ambil dari To nya
    08/01/2020 [WED/MAI] BC41 dari DO To Customer belum muncul
    24/08/2020 [IBL/MAI] memunculkan BC 27 disetiap transaksi Kaber
    08/09/2020 [IBL/MAI] BUG : Beberapa BC terbaca di BC 27
    08/09/2020 [VIN/MAI] BC 30, KBContractNo diambil dari local# DO To Customer
 * 
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptKB_2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mStartDt = string.Empty, mEndDt = string.Empty;

        #endregion

        #region Constructor

        public FrmRptKB_2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueType(ref LueType);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
     
        private string GetSQL(string Type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, T2.ItGrpName ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            SQL.AppendLine("Select 'BC25' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, IfNull(A.DocNoInternal, A.KBContractNo) KBContractNo, A.DocDt, C.CtName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null  ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CustomsDocCode = '25' ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC25' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.localdocno KBContractNo, A.DocDt, C.CtName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOCt2Hdr A  ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '25' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC27' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.localdocno KBContractNo, A.DocDt, C.VdName As Name, ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status In ('O', 'A') And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.POInd='Y' ");
            SQL.AppendLine("    And Substr(A.KBSubmissionNo, 5, 2) = '27' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC27' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.localdocno KBContractNo, A.DocDt, C.CtName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOCt2Hdr A  ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '27' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC27' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.DocNoInternal KBContractNo, A.DocDt, C.CtName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '27' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC27' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.LocalDocNo KBContractNo, A.DocDt, C.VdName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOVdHdr A  ");
            SQL.AppendLine("Inner Join TblDOVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '27' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC41' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, IfNull(A.DocNoInternal, A.KBContractNo) KBContractNo, A.DocDt, C.CtName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null  ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CustomsDocCode = '41' ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC41' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.localdocno KBContractNo, A.DocDt, C.CtName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOCt2Hdr A  ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '41' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC41' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.LocalDocNo KBContractNo, A.DocDt, C.VdName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOVdHdr A  ");
            SQL.AppendLine("Inner Join TblDOVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '41' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            //SQL.AppendLine("Union All ");

            //SQL.AppendLine("Select 'BC261' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.localdocno KBContractNo, A.DocDt, H.VdName As Name, ");
            //SQL.AppendLine("C.ItGrpCode, C.ItName, C.InventoryUomCode, B.Qty, G.CurCode, G.UPrice*B.Qty As Amt ");
            //SQL.AppendLine("From TblDOWhsHdr A  ");
            //SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            //SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            //SQL.AppendLine("    And A.CancelInd='N' ");
            //SQL.AppendLine("    And A.Status In ('O', 'A') ");
            //SQL.AppendLine("    And A.TransferRequestWhsDocNo Is Not Null ");
            //SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode  ");
            ////SQL.AppendLine("Inner Join TblRecvVdDtl D On B.BatchNo = D.BatchNo ");
            ////SQL.AppendLine("Inner Join TblRecvVdHdr E On D.DocNo = E.DocNo ");
            ////SQL.AppendLine("Inner Join TblVendor F On E.VdCode = F.VdCode ");
            //SQL.AppendLine("Inner Join TblStockPrice G On B.Source = G.Source ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.DocNo, T5.VdName ");
            //SQL.AppendLine("    From TblDOWhsHdr T1 ");
            //SQL.AppendLine("    Inner Join TblDOWhsDtl T2 On T1.DOcNo = T2.DocNo And T2.CancelInd = 'N' ");
            //SQL.AppendLine("        And T1.KBRegistrationNo Is Not Null ");
            //SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.BatchNo = T3.BatchNo ");
            //SQL.AppendLine("    Inner Join TblRecvVdHdr T4 On T3.DocNo = T4.DocNo ");
            //SQL.AppendLine("    Inner Join TblVendor T5 On T4.VdCode = T5.VdCode ");
            //SQL.AppendLine(") H On A.DocNo = H.DocNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC261' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.KBPLNo KBContractNo, A.DocDt, I.WhsName As Name, ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, H.CurCode, H.UPrice*B.Qty As Amt ");
            SQL.AppendLine("From TblRecvWhsHdr A   ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '261' ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo = C.DNo And C.CancelInd='N'  ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            //SQL.AppendLine("Inner Join TblRecvVdDtl E On C.BatchNo = E.BatchNo ");
            //SQL.AppendLine("Inner Join TblRecvVdHdr F On E.DocNo = F.DocNo  ");
            //SQL.AppendLine("Inner Join TblVendor G On F.VdCode = G.VdCode ");
            SQL.AppendLine("Inner Join TblStockPrice H On B.Source = H.Source ");
            SQL.AppendLine("Inner Join TblWarehouse I On A.WhsCode = I.WhsCode ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC27' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.KBPLNo KBContractNo, A.DocDt, I.WhsName As Name, ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, H.CurCode, H.UPrice*B.Qty As Amt ");
            SQL.AppendLine("From TblRecvWhsHdr A   ");
            SQL.AppendLine("Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '27' ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo = C.DNo And C.CancelInd='N'  ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            //SQL.AppendLine("Inner Join TblRecvVdDtl E On C.BatchNo = E.BatchNo ");
            //SQL.AppendLine("Inner Join TblRecvVdHdr F On E.DocNo = F.DocNo  ");
            //SQL.AppendLine("Inner Join TblVendor G On F.VdCode = G.VdCode ");
            SQL.AppendLine("Inner Join TblStockPrice H On B.Source = H.Source ");
            SQL.AppendLine("Inner Join TblWarehouse I On A.WhsCode = I.WhsCode ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC30' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.localdocno KBContractNo, A.DocDt, C.CtName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOCt2Hdr A  ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '30' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC30' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, IfNull(A.DocNoInternal, A.KBContractNo) KBContractNo, A.DocDt, C.CtName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '30' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC30' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.LocalDocNo KBContractNo, A.DocDt, C.VdName As Name,  ");
            SQL.AppendLine("D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblDOVdHdr A  ");
            SQL.AppendLine("Inner Join TblDOVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode = '30' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode  ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source  ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Left Join TblItemGroup T2 On T.ItGrpCode = T2.ItGrpCode ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 1;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;
            Grd1.Header.Cells[1, 1].Value = "Dokumen Pabean";
            Grd1.Header.Cells[1, 1].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 1].SpanCols = 3;
            Grd1.Header.Cells[0, 1].Value = "Jenis";
            Grd1.Header.Cells[0, 1].SpanRows = 1;
            Grd1.Header.Cells[0, 2].Value = "Nomor";
            Grd1.Header.Cells[0, 2].SpanRows = 1;
            Grd1.Header.Cells[0, 3].Value = "Tanggal";
            Grd1.Header.Cells[0, 3].SpanRows = 1;
            Grd1.Header.Cells[1, 4].Value = "Bukti Pengeluaran Barang";
            Grd1.Header.Cells[1, 4].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 4].SpanCols = 2;
            Grd1.Header.Cells[0, 4].Value = "Nomor";
            Grd1.Header.Cells[0, 4].SpanRows = 1;
            Grd1.Header.Cells[0, 5].Value = "Tanggal";
            Grd1.Header.Cells[0, 5].SpanRows = 1;
            Grd1.Header.Cells[0, 6].Value = "Supplier / Customer";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;
            Grd1.Header.Cells[0, 7].Value = "Kode Barang";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;
            Grd1.Header.Cells[0, 8].Value = "Uraian Barang";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;
            Grd1.Header.Cells[0, 9].Value = "Satuan";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 9].SpanRows = 2;
            Grd1.Header.Cells[0, 10].Value = "Jumlah";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].SpanRows = 2;
            Grd1.Header.Cells[0, 11].Value = "Valas";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;
            Grd1.Header.Cells[0, 12].Value = "Nilai Barang";
            Grd1.Header.Cells[0, 12].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 12].SpanRows = 2;

            Sm.GrdFormatDate(Grd1, new int[] { 3, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12 }, 0);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            mStartDt = string.Empty; mEndDt = string.Empty;
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";
                
                var cm = new MySqlCommand();

                mStartDt = Sm.GetDte(DteDocDt1).Substring(0, 8);
                mEndDt = Sm.GetDte(DteDocDt2).Substring(0, 8);

                Sm.CmParamDt(ref cm, "@DocDt1", mStartDt);
                Sm.CmParamDt(ref cm, "@DocDt2", mEndDt);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.KBContractNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtRegistrationNo.Text, "T.KBRegistrationNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "T.DocType", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(Sm.GetLue(LueType)) + Filter + " Order By T.DocDt, T.KBContractNo, T.ItGrpCode, T.ItName; ",
                    new string[]
                    {
                        //0
                        "DocType", 

                        //1-5
                        "KBRegistrationNo", "KBRegistrationDt", "KBContractNo", "DocDt", "Name", 

                        //6-10
                        "ItGrpCode", "ItGrpName", "InventoryUomCode", "Qty", "CurCode", 

                        //11
                        "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            var l = new List<KB_2Hdr>();
            var l2 = new List<KB_2Dtl>();
            decimal Numb = 0;
            string[] TableName = { "KB_2Hdr", "KB_2Dtl"};
            List<IList> myLists = new List<IList>();
            if (Grd1.Rows.Count == 0 || mStartDt.Length == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }

            #region Header
            
            l.Add(new KB_2Hdr()
            {
                CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                CompanyName = Gv.CompanyName,
                Periode1 = DteDocDt1.Text.Replace("/", "-"),
                Periode2 = DteDocDt2.Text.Replace("/", "-"),
                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
            });
            
            myLists.Add(l);

            #endregion

            #region Detail
            
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                Numb = Numb +1; 

                l2.Add(new KB_2Dtl()
                { 
                    Number = Numb,
                    DocType =Sm.GetGrdStr(Grd1,i,1),
                    DocNo = Sm.GetGrdStr(Grd1,i,2),
                    DocDt = Sm.GetGrdText(Grd1,i,3),
                    NoRecv = Sm.GetGrdStr(Grd1, i, 4),
                    RecvDt = Sm.GetGrdText(Grd1, i, 5),
                    Supplier =  Sm.GetGrdStr(Grd1, i, 6),
                    ItemCode = Sm.GetGrdStr(Grd1, i, 7),
                    DetailItem = Sm.GetGrdStr(Grd1, i, 8),
                    Unit = Sm.GetGrdStr(Grd1, i, 9),
                    Total = Sm.GetGrdDec(Grd1, i, 10),
                    Valve = Sm.GetGrdStr(Grd1, i, 11),
                    ValueOfGoods = Sm.GetGrdDec(Grd1, i, 12),
                });
            }
            myLists.Add(l2);

            #endregion

            Sm.PrintReport("KB_2", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private string MonthNameIDN(string Mth)
        {
            string[] mMonthNames = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Novemeber", "Desember" };
            return mMonthNames[Int32.Parse(Mth) - 1];
        }

        private void SetLueType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'BC25' As Col1, 'BC 25' As Col2 ");
            SQL.AppendLine("Union All Select 'BC27' Col1, 'BC 27' Col2 ");
            SQL.AppendLine("Union All Select 'BC30' Col1, 'BC 30' Col2 ");
            SQL.AppendLine("Union All Select 'BC41' Col1, 'BC 41' Col2 ");
            SQL.AppendLine("Union All Select 'BC261' Col1, 'BC 261' Col2; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void TxtRegistrationNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRegistrationNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Registration#");
        }

        #endregion

        #endregion

        #region Report Class
        class KB_2Hdr
        {
            public string CompanyName { get; set; }
            public string CompanyLogo { set; get; }
            public string Periode1 { set; get; }
            public string Periode2 { set; get; }
            public string PrintBy { set; get; }
        }

        class KB_2Dtl
        {
            public decimal Number { get; set; }
            public string DocType { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string NoRecv { get; set; }
            public string RecvDt { get; set; }
            public string Supplier { get; set; }
            public string ItemCode { get; set; }
            public string DetailItem { get; set; }
            public string Unit { get; set; }
            public decimal Total { get; set; }
            public string Valve { get; set; }
            public decimal ValueOfGoods { get; set; }

        }
        #endregion
    }
}
