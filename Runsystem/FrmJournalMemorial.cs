﻿#region Update
/*
    21/09/2021 [TKG/AMKA] New Apps
    30/09/2021 [SET/AMKA] Prinout Journal Memorial dengan ttd dari doc approval setting JournalMemorial
    16/11/2021 [TRI/AMKA] uncomment button btnjournaldocno_click
    18/11/2021 [DEV/AMKA] Membuat Field Combo Box Cost Center di Journal Memorial hanya memunculkan Cost Center berdasarkan Group User dapat terfilter berdasarkan Group User dengan parameter IsJournalMemorialCCFilteredByGroup
    19/11/2021 [BRI/AMKA] Doc approval berdasarkan department
    26/11/2021 [ARI/AMKA] Merubah susunan TTD pada Print Out Journal Memorial
    25/02/2022 [VIN/AMKA] Bisa cancel kalau JN nya sudah di cancel
    14/03/2022 [TRI/AMKA] Bug approval, ketika departemen tidak ada di DAS dokumen masih outstanding, harusnya approve
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmJournalMemorial : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmJournalMemorialFind FrmFind;
        private string 
            mMainCurCode = string.Empty,
            mEntCode = string.Empty;
        private bool
           mIsApprovalByDepartment = false;
        iGCell fCell;
        bool fAccept;
        //private string[] mSeqNo = new string[100];
        internal bool mIsJournalMemorialCCFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmJournalMemorial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Journal memorial";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                GetValue();
                Tc1.SelectedTabPage = TpAdditional;
                Sl.SetLueCurCode(ref LueCurCode);
                SetLueEntityCode(ref LueEntCode);
                Tc1.SelectedTabPage = TpGeneral;
                LueEntCode.Visible = false;
                SetGrd();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Alias",
                        "Description",
                        "Entity's Code",
                        "Entity's Name",

                        //6-8
                        "Debit",
                        "Credit",
                        "Remark",
                    },
                     new int[] 
                    {
                        20,
                        150, 100, 400, 100, 200, 
                        130, 130, 300
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, false);

            Grd2.Cols.Count = 5;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 200, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeJnDesc, MeeRemark, LueCCCode, MeeCancelReason, ChkCancelInd, TxtPeriod }, true);
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeJnDesc, MeeRemark, LueCCCode, TxtPeriod }, false);
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, MeeJnDesc, MeeRemark, LueCurCode, 
                TxtMenuCode, TxtMenuDesc, LueCCCode, MeeCancelReason, TxtPeriod, TxtStatus
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtDbt, TxtCdt, TxtBalanced }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7 });
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJournalMemorialFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
                Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsJournalMemorialCCFilteredByGroup ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmJournalMemorialDlg(this));
            }

            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 1)
                    e.DoDefault = false;
                if (e.ColIndex == 5 || e.ColIndex == 6 || e.ColIndex == 7 || e.ColIndex == 8)
                {
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0)
                        e.DoDefault = false;
                    else
                    {
                        if (e.ColIndex == 5) 
                            LueRequestEdit(Grd1, LueEntCode, ref fCell, ref fAccept, e);
                    }
                }
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 6) Compute(e.RowIndex, 1);
            if (e.ColIndex == 7) Compute(e.RowIndex, 2);
            ComputeDebetCredit(); 
            ComputeBalanced();
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeDebetCredit(); 
                ComputeBalanced();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmJournalMemorialDlg(this));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.GetLue(LueCCCode).Length==0)
            {
                if (Sm.StdMsgYN("Question", 
                    "Cost center is empty." +Environment.NewLine + 
                    "Do you want to continue saving this data ?", 
                    mMenuCode) == DialogResult.No)
                {
                    LueCCCode.Focus();
                    return;
                }
                if (IsInsertedDataNotValid()) return;
            }
            else
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                    IsInsertedDataNotValid()) return;
            }

            Cursor.Current = Cursors.WaitCursor;


            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "JournalMemorial", "TblJournalMemorialHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveJournalMemorial(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsMeeEmpty(MeeJnDesc, "Description")||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                Sm.IsLueEmpty(LueCCCode, "Cost Center") ||
                IsGrdEmpty() || 
                IsGrdValueNotValid() ||
                IsBalancedNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 account.");
                return true;
            }
            return false;
        }

        private bool IsBalancedNotValid()
        {
            decimal Debit = 0m, Credit = 0m, Balanced = 0m;

            ComputeDebetCredit(); 
            ComputeBalanced();

            if (TxtDbt.Text.Length != 0m) Debit = decimal.Parse(TxtDbt.Text);
            if (TxtCdt.Text.Length != 0m) Credit = decimal.Parse(TxtCdt.Text);
            if (TxtBalanced.Text.Length != 0m) Balanced = decimal.Parse(TxtBalanced.Text);
            if (Debit!=Credit || Balanced != 0m)
            {
                Sm.StdMsg(mMsgType.Warning, "Debit and credit is not balanced.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "COA's account is empty.")) return true;
                if (Sm.GetGrdDec(Grd1, Row, 6) == 0m && Sm.GetGrdDec(Grd1, Row, 7) == 0m) 
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "Acount# : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "Account Description : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "It can not be 0.00.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveJournalMemorial(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblJournalMemorialHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, JnDesc, CurCode, MenuCode, MenuDesc, CCCode, Period, JournalDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @JnDesc, @CurCode, ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CCCode, @Period, Null, @Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblJournalMemorialDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, EntCode, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values");
            for (int r = 0; r<Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) 
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @AcNo_" + r.ToString() + ", IfNull(@EntCode_" + r.ToString() + ", @EntCode), @DAmt_" + r.ToString() + ", @CAmt_" + r.ToString() + ", @Remark_" + r.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@EntCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                }
            }
            SQL.AppendLine(";");

            SQL.AppendLine("Set @JournalDocNo:=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(";");

            #region Approval

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @UserCode, @Dt ");
            SQL.AppendLine("From TblDocApprovalSetting Where DocType='JournalMemorial' ");
            //if (mIsApprovalByDepartment)
            //{
                SQL.AppendLine("And DeptCode In (Select B.DeptCode From TblJournalMemorialHdr A ");
                SQL.AppendLine("    Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo And A.CCCode Is Not Null) ");
            //}
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblJournalMemorialHdr Set Status='A', JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists(Select 1 From TblDocApproval Where DocType='JournalMemorial' And DocNo=@DocNo); ");

            #endregion

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, JnDesc, CurCode, MenuCode, MenuDesc, CCCode, Period, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, 'N', JnDesc, CurCode, MenuCode, MenuDesc, CCCode, Period, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalMemorialHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And Status='A' And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, EntCode, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, B.DNo, B.AcNo, B.EntCode, B.DAmt, B.CAmt, B.Remark, B.CreateBy, B.CreateDt ");
            SQL.AppendLine("From TblJournalMemorialHdr A, TblJournalMemorialDtl B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.Status='A' And A.JournalDocNo Is Not Null And A.DocNo=B.DocNo; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt).Substring(0, 8));
            Sm.CmParam<String>(ref cm, "@JnDesc", MeeJnDesc.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Period", TxtPeriod.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();

            cml.Add(EditJournalMemorialHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(true, true, Sm.GetDte(DteDocDt)) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessToJournal();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this journal.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblJournalMemorialHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This journal already cancelled."
                );       
        }

        private bool IsDataAlreadyProcessToJournal()
        {
            return Sm.IsDataExist(
                "SELECT 1 from tbljournalhdr Where CancelInd ='N' And Docno IN "
                + "(Select journaldocno From TblJournalMemorialHdr Where JournalDocNo Is Not Null And DocNo=@Param); ",
                TxtDocNo.Text,
                "This data already processed to journal.");
        }

        private MySqlCommand EditJournalMemorialHdr()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Update TblJournalMemorialHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowJournalMemorialHdr(DocNo);
                ShowJournalMemorialDtl(DocNo);
                ComputeDebetCredit(); 
                ComputeBalanced();
                Sm.ShowDocApproval(DocNo, "JournalMemorial", ref Grd2);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowJournalMemorialHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string CCCode = string.Empty;

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DocNo, DocDt, JnDesc, CurCode, MenuCode, MenuDesc, CCCode, Period, Remark, ");
            SQL.AppendLine("CancelReason, CancelInd, JournalDocNo, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblJournalMemorialHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");
            
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "JnDesc", "CurCode", "MenuCode", "MenuDesc", 
                        "CCCode", "Remark", "CancelReason", "CancelInd", "JournalDocNo",
                        "Period", "StatusDesc"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeJnDesc.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[3]));
                        TxtMenuCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtMenuDesc.EditValue = Sm.DrStr(dr, c[5]);
                        CCCode = Sm.DrStr(dr, c[6]);
                        SetLueCCCode(ref LueCCCode, CCCode);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[8]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[9])=="Y";
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPeriod.EditValue = Sm.DrStr(dr, c[11]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[12]);
                    }, true
                );
        }

        private void ShowJournalMemorialDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.AcNo, B.Alias, B.AcDesc, A.EntCode, C.EntName, A.DAmt, A.CAmt, A.Remark ");
                SQL.AppendLine("From TblJournalMemorialDtl A ");
                SQL.AppendLine("Left Join TblCoa B On A.AcNo = B.AcNo ");
                SQL.AppendLine("Left Join TblEntity C On A.EntCode = C.EntCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("Order By CAmt Asc, A.AcNo; ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "AcNo", 
                        "Alias", "AcDesc", "EntCode", "EntName", "DAmt", "CAmt", 
                        "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string Doctitle = Sm.GetParameter("DocTitle");
            string[] TableName = { "JournalMemoHdr", "JournalMemoDtl", "JournalMemoSignAMKA" };

            var l = new List<JournalMemoHdr>();
            var ldtl = new List<JournalMemoDtl>();
            var lSignAMKA = new List<JournalSignAMKA>();

            List<IList> myLists = new List<IList>();

            #region Journal Memor Hdr

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, A.JnDesc, A.CurCode, A.MenuDesc, A.Remark ");
            SQL.AppendLine("From TblJournalMemorialHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo", 
                        
                         //6-10
                         "DocDt", 
                         "JnDesc",
                         "CurCode", 
                         "MenuDesc", 
                         "Remark",

                        
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new JournalMemoHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            JnDesc = Sm.DrStr(dr, c[7]),
                            CurCode = Sm.DrStr(dr, c[8]),
                            MenuDesc = Sm.DrStr(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),

                            //AutoJournal = Sm.GetParameterBoo("IsAutoJournalActived"),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Journal Memo Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.Dno, A.ACNo, B.Alias, B.ACDesc, C.EntName, A.DAmt, A.CAmt, A.Remark  ");
                SQLDtl.AppendLine("From TblJournalMemorialDtl A ");
                SQLDtl.AppendLine("Inner Join TblCOA B On A.AcNo = B.ACno ");
                SQLDtl.AppendLine("Left Join TblEntity C On A.EntCode = C.EntCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By CAmt Asc, A.AcNo;");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "ACNo",
                         "Alias",
                         "ACDesc",
                         "EntName",
                         "DAmt",

                         //6-7
                         "CAmt",
                         "Remark"

                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new JournalMemoDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            ACNo = Sm.DrStr(drDtl, cDtl[1]),
                            Alias = Sm.DrStr(drDtl, cDtl[2]),
                            ACDesc = Sm.DrStr(drDtl, cDtl[3]),
                            EntName = Sm.DrStr(drDtl, cDtl[4]),
                            DAmt = Sm.DrDec(drDtl, cDtl[5]),
                            CAmt = Sm.DrDec(drDtl, cDtl[6]),
                            Remark = Sm.DrStr(drDtl, cDtl[7])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            #region Signature AMKA
            

            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();
            SQL4.AppendLine("SELECT T1.EmpPict1, T1.UserCode1, T1.UserName1, Description1, ApproveDt1, T2.EmpPict2, T2.UserCode2,  T2.UserName2, T2.Description2, T2.ApproveDt2, ");
            SQL4.AppendLine("T3.EmpPict3, T3.UserCode3, T3.UserName3, T3.Description3, T3.ApproveDt3, T4.EmpPict4, T4.UserCode4, T4.UserName4, T4.Description4, T4.ApproveDt4, ");
            SQL4.AppendLine("T5.EmpPict, T5.UserCode, T5.UserName, T5.Description ");
            SQL4.AppendLine("FROM ");
            SQL4.AppendLine(GetApproval("JournalMemorial"));


            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();
                Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                {
                    //0 - 4
                    "EmpPict1",
                    "Description1",
                    "UserCode1",
                    "Username1",
                    "ApproveDt1",

                    //5 - 9
                    "EmpPict2",
                    "Description2",
                    "UserCode2",
                    "Username2",
                    "ApproveDt2",

                    //10 - 14
                    "EmpPict3",
                    "Description3",
                    "UserCode3",
                    "Username3",
                    "ApproveDt3",

                    //15 - 19
                    "EmpPict4",
                    "Description4",
                    "UserCode4",
                    "Username4",
                    "ApproveDt4",

                    //20 - 23
                    "EmpPict",
                    "Description",
                    "UserCode",
                    "Username",
                });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        lSignAMKA.Add(new JournalSignAMKA()
                        {
                            EmpPict1 = Sm.DrStr(dr4, c4[0]),
                            Description1 = Sm.DrStr(dr4, c4[1]),
                            UserCode1 = Sm.DrStr(dr4, c4[2]),
                            UserName1 = Sm.DrStr(dr4, c4[3]),
                            ApproveDt1 = Sm.DrStr(dr4, c4[4]),
                            EmpPict2 = Sm.DrStr(dr4, c4[5]),
                            Description2 = Sm.DrStr(dr4, c4[6]),
                            UserCode2 = Sm.DrStr(dr4, c4[7]),
                            UserName2 = Sm.DrStr(dr4, c4[8]),
                            ApproveDt2 = Sm.DrStr(dr4, c4[9]),
                            EmpPict3 = Sm.DrStr(dr4, c4[10]),
                            Description3 = Sm.DrStr(dr4, c4[11]),
                            UserCode3 = Sm.DrStr(dr4, c4[12]),
                            UserName3 = Sm.DrStr(dr4, c4[13]),
                            ApproveDt3 = Sm.DrStr(dr4, c4[14]),
                            EmpPict4 = Sm.DrStr(dr4, c4[15]),
                            Description4 = Sm.DrStr(dr4, c4[16]),
                            UserCode4 = Sm.DrStr(dr4, c4[17]),
                            UserName4 = Sm.DrStr(dr4, c4[18]),
                            ApproveDt4 = Sm.DrStr(dr4, c4[19]),
                            EmpPict = Sm.DrStr(dr4, c4[20]),
                            Description = Sm.DrStr(dr4, c4[21]),
                            UserCode = Sm.DrStr(dr4, c4[22]),
                            UserName = Sm.DrStr(dr4, c4[23]),
                        });
                    }
                }
                dr4.Close();
            }
            myLists.Add(lSignAMKA);


            #endregion

            Sm.PrintReport("JournalMemorialAMKA", myLists, TableName, false);
        }

        private void SetLookUpEdit(LookUpEdit Lue, object ds)
        {
            //populate data for LookUpEdit control
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }
        private void SetLueCCCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter ");
            SQL.AppendLine("Where 1=1 ");
            if (Code.Length > 0)
                SQL.AppendLine("And CCCode=@Code;");
            else
            {
                SQL.AppendLine("And ActInd='Y' ");
                SQL.AppendLine("Order By CCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void Compute(int Row, int ac)
        {
            switch (ac)
            {
                case 1:
                    if (Sm.GetGrdStr(Grd1, Row, 6).Length != 0)
                    {
                        Grd1.Cells[Row, 7].Value = 0m;
                    }
                    break;
                case 2:
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                    {
                        Grd1.Cells[Row, 6].Value = 0m;
                    }
                    break;
            }
        }

        private void ComputeBalanced()
        {
            decimal debit = 0m, credit = 0m;
            
            if (TxtDbt.Text.Length>0) debit = decimal.Parse(TxtDbt.Text);
            if (TxtCdt.Text.Length > 0) credit = decimal.Parse(TxtCdt.Text);
            
            TxtBalanced.EditValue = Sm.FormatNum(debit - credit, 0);
        }

        private void ComputeDebetCredit()
        {
            decimal DebetAmount = 0m, CreditAmount = 0m;

            for (int r= 0; r<= Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 6).Length>0)
                    DebetAmount += Sm.GetGrdDec(Grd1, r, 6);
            }
            for (int r = 0; r <= Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                    CreditAmount += Sm.GetGrdDec(Grd1, r, 7);
            }
                    
            TxtDbt.EditValue = Sm.FormatNum(DebetAmount, 0);
            TxtCdt.EditValue = Sm.FormatNum(CreditAmount, 0); 
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'JournalEntCodeDefault', 'IsJournalMemorialCCFilteredByGroup' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "JournalEntCodeDefault": mEntCode = ParValue; break;

                            //boolean
                            case "IsJournalMemorialCCFilteredByGroup": mIsJournalMemorialCCFilteredByGroup = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void GetValue()
        {
            mIsApprovalByDepartment = Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='JournalMemorial' And DeptCode Is Not Null Limit 1;");
        }

        private string GetApproval(string ApprovalDocType)
        {
            var SQL = new StringBuilder();
            string DocNo = TxtDocNo.Text;
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT A.DocNo, Concat(IfNull(E.ParValue, ''), B.UserCode, '.JPG') As EmpPict1, B.UserCode AS UserCode1,  D.UserName AS UserName1,'Diperiksa1' As Description1, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDt1 ");
            SQL.AppendLine("FROM TblJournalMemorialHdr A ");
            SQL.AppendLine("INNER JOIN TblDocApproval B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND B.DocType = '" + ApprovalDocType + "' ");
            SQL.AppendLine("INNER JOIN TblDocApprovalSetting C ON B.DocType = C.DocType ");
            SQL.AppendLine("    AND B.ApprovalDNo = C.DNo AND C.Level = '1' ");
            SQL.AppendLine("INNER JOIN TblUser D ON B.UserCode = D.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter E On E.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT A.DocNo, Concat(IfNull(E.ParValue, ''), B.UserCode, '.JPG') As EmpPict2, B.UserCode AS UserCode2, D.UserName AS UserName2,'Diperiksa2' As Description2, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDt2 ");
            SQL.AppendLine("FROM TblJournalMemorialHdr A ");
            SQL.AppendLine("INNER JOIN TblDocApproval B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND B.DocType = '" + ApprovalDocType + "' ");
            SQL.AppendLine("INNER JOIN TblDocApprovalSetting C ON B.DocType = C.DocType ");
            SQL.AppendLine("    AND B.ApprovalDNo = C.DNo AND  C.Level = '2' ");
            SQL.AppendLine("INNER JOIN TblUser D ON B.UserCode = D.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter E On E.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(")T2 ON T1.DocNo = T2.DocNo");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(E.ParValue, ''), B.UserCode, '.JPG') As EmpPict3, B.UserCode AS UserCode3, D.UserName AS UserName3,'Diperiksa3' As Description3, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDt3 ");
            SQL.AppendLine("FROM TblJournalMemorialHdr A ");
            SQL.AppendLine("INNER JOIN TblDocApproval B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND B.DocType = '" + ApprovalDocType + "' ");
            SQL.AppendLine("INNER JOIN TblDocApprovalSetting C ON B.DocType = C.DocType ");
            SQL.AppendLine("    AND B.ApprovalDNo = C.DNo AND  C.Level = '3' ");
            SQL.AppendLine("INNER JOIN TblUser D ON B.UserCode = D.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter E On E.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(")T3 ON T1.DocNo = T3.DocNo");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT DISTINCT A.DocNo, Concat(IfNull(E.ParValue, ''), B.UserCode, '.JPG') As EmpPict4, B.UserCode AS UserCode4, D.UserName AS UserName4,'disetujui' As Description4, DATE_FORMAT(Left(B.LastUpDt, 8),'%d %M %Y') As ApproveDt4 ");
            SQL.AppendLine("FROM TblJournalMemorialHdr A ");
            SQL.AppendLine("INNER JOIN TblDocApproval B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND B.DocType = '" + ApprovalDocType + "' ");
            SQL.AppendLine("INNER JOIN TblDocApprovalSetting C ON B.DocType = C.DocType ");
            SQL.AppendLine("    AND B.ApprovalDNo = C.DNo AND  C.Level = '4' ");
            SQL.AppendLine("INNER JOIN TblUser D ON B.UserCode = D.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter E On E.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(")T4 ON T1.DocNo = T4.DocNo");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("SELECT A.DocNo, Concat(IfNull(C.ParValue, ''), B.UserCode, '.JPG') As EmpPict, B.UserCode AS UserCode, B.UserName AS UserName,'Dibuat' As Description ");
            SQL.AppendLine("FROM TblJournalMemorialHdr A ");
            SQL.AppendLine("INNER JOIN TblUser B ON A.CreateBy = B.UserCode ");
            SQL.AppendLine("LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL.AppendLine("WHERE A.DocNo = '" + DocNo + "' ");
            SQL.AppendLine(") T5 On T1.DocNo = T5.DocNo" );

            return SQL.ToString();

        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex-1).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex-1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsJournalMemorialCCFilteredByGroup ? "Y" : "N");
        }

        private void SetLueEntityCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EntCode As Col1, EntName As Col2 ");
            SQL.AppendLine("From TblEntity ");
            SQL.AppendLine("Order By EntName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.SetLue2(
               ref Lue, SQL.ToString(),
               0, 35, false, true, "Code", "Entity", "Col2", "Col1");
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntityCode));
        }

        private void LueEntCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueEntCode_Leave(object sender, EventArgs e)
        {
            if (LueEntCode.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueEntCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueEntCode);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueEntCode.GetColumnValue("Col2");
                }
                LueEntCode.Visible = false;
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void DteDocDt_DateTimeChanged(object sender, EventArgs e)
        {
            if(DteDocDt.Text.Length != 0)
                TxtPeriod.EditValue = Sm.GetDte(DteDocDt).Substring(0, 6);
        }

        #endregion

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }

        }

        #region Report Class

        private class JournalMemoHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string JnDesc { get; set; }
            public string CurCode { get; set; }
            public string MenuDesc { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
            //public bool AutoJournal { get; set; }
        }

        private class JournalMemoDtl
        {
            public string DNo { get; set; }
            public string ACNo { get; set; }
            public string Alias { get; set; }
            public string ACDesc { get; set; }
            public string EntName { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Remark { get; set; }
        }

        private class JournalSignAMKA
        {
            public string EmpPict { get; set; }
            public string Description { get; set; }
            public string UserCode{ get; set; }
            public string UserName { get; set; }
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserCode1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }
            public string EmpPict2 { get; set; }
            public string Description2 { get; set; }
            public string UserCode2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }
            public string EmpPict3 { get; set; }
            public string Description3 { get; set; }
            public string UserCode3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }
            public string EmpPict4 { get; set; }
            public string Description4 { get; set; }
            public string UserCode4 { get; set; }
            public string UserName4 { get; set; }
            public string ApproveDt4 { get; set; }

        }

        #endregion

    }
}
