﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmEmployeeAmendment2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string mStateIndicator = string.Empty;
        internal bool 
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false,
            mIsAbleToInputCompetence = false;
        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmEmployeeAmendment2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmEmployeeAmendment2");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnPrint.Visible = BtnDelete.Visible = false;
                BtnSave.Visible = true;
                BtnCancel.Enabled = true;
                GetParameter();
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLuePosCode(ref LuePosCode);

                TcEmployee.SelectedTabPage = Tp4;
                SetLueTrainingCode(ref LueTrainingCode);
                LueTrainingCode.Visible = false;

                TcEmployee.SelectedTabPage = Tp3;
                Sl.SetLueCompetenceCode(ref LueCompetenceCode);
                LueCompetenceCode.Visible = false;

                TcEmployee.SelectedTabPage = Tp1;
                Sl.SetLueEmployeeEducationLevel(ref LueLevel);
                LueLevel.Visible = false;
                
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                BtnEmpCode.Focus();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "School",
                    "LevelCode",
                    "Level",
                    "Major",
                    "Highest",

                    //6
                    "Remark"
                },
                 new int[] 
                {
                    //0
                    0, 
                    
                    //1-5
                    250, 0, 180, 180, 80,
                    
                    //6
                    300
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 }, false);

            #endregion

            #region Grd2

            Grd2.Cols.Count = 5;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "D No",
                        //1-4
                        "Company",
                        "Position",
                        "Period",
                        "Remark"
                    },
                     new int[] 
                    {
                        0, 150, 150, 150, 250,
                    }
                );

            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);

            #endregion

            #region Grd3

            Grd3.Cols.Count = 5;
            Grd3.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-4
                    "Code",
                    "Name",
                    "Description",
                    "Score"
                },
                 new int[] 
                {
                    0, 
                    0, 150, 300, 100
                }
            );

            Sm.GrdColInvisible(Grd3, new int[] { 0, 1 }, false);
            Sm.GrdFormatDec(Grd3, new int[] { 4 }, 0);

            #endregion

            #region Grd4

            Grd4.Cols.Count = 8;
            Grd4.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[] 
                {
                    //0
                    "DNo",
                    
                    //1-5
                    "TrainingCode",
                    "Training",
                    "Specialization",
                    "Place",
                    "Period",

                    //6-7
                    "Month/Year",
                    "Remark",

                },
                new int[] 
                {
                    0, 
                    100, 180, 200, 200, 150, 
                    120, 300 
                }
            );
            Sm.GrdColInvisible(Grd4, new int[] { 0, 1 }, false);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            BtnSave.Enabled = false;
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpCode, TxtEmpCodeOld, TxtEmpName, LueSiteCode, LueDeptCode, LuePosCode
                    }, true);
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = false;
                    if (!mIsAbleToInputCompetence) Grd3.ReadOnly = true;
                    BtnEmpCode.Enabled = true;
                    TxtEmpCode.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mStateIndicator = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtEmpCode, TxtEmpCodeOld, TxtEmpName, LueSiteCode, LueDeptCode, LuePosCode, 
                 LueLevel, LueTrainingCode, LueCompetenceCode
            });
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, false);
            Sm.ClearGrd(Grd2, false);
            Sm.ClearGrd(Grd3, false);
            Sm.ClearGrd(Grd4, false);
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(DeleteEmployeeExistingDetail(TxtEmpCode.Text));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveEmployeeEducation(Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveEmployeeWorkExp(Row));

            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveEmployeeCompetence(Row));

            for (int r = 0; r < Grd4.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0) cml.Add(SaveEmployeeTraining(r));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtEmpCode, "Employee's Code", false) ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Education entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                TcEmployee.SelectedTabPage = Tp1;
                return true;
            }
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Competence entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                TcEmployee.SelectedTabPage = Tp2;
                return true;
            }
            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Competence entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                TcEmployee.SelectedTabPage = Tp3;
                return true;
            }
            if (Grd4.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Training entered (" + (Grd4.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                TcEmployee.SelectedTabPage = Tp4;
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if(Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "School is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Level is empty.")) return true;
                }
            }

            if(Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "Company is empty.")) return true;
                }
            }

            if(Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 2, false, "Competence is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 3, false, "Competence Description is empty.")) return true;

                }
            }

            if(Grd4.Rows.Count > 1)
            {
                for (int r = 0; r < Grd4.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd4, r, 3, false, "Specialization training is empty.")) return true;
                }
            }

            return false;
        }

        private MySqlCommand DeleteEmployeeExistingDetail(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblEmployeeEducation Where EmpCode = @EmpCode; ");
            SQL.AppendLine("Delete From TblEmployeeWorkExp Where EmpCode = @EmpCode; ");
            SQL.AppendLine("Delete From TblEmployeeCompetence Where EmpCode = @EmpCode; ");
            SQL.AppendLine("Delete From TblEmployeeTraining Where EmpCode = @EmpCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeWorkExp(int Row)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmployeeWorkExp(EmpCode, DNo, Company, Position, Period, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @DNo, @Company, @Position, @Period, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("Company=@Company, Position=@Position, Period=@Period, ");
            SQL.AppendLine("Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");
            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Company", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Position", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@Period", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveEmployeeEducation(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeEducation(EmpCode, DNo, School, Level, Major, HighestInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @DNo, @School, @Level, @Major, @HighestInd, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    School=@School, Level=@Level, Major=@Major, HighestInd=@HighestInd, ");
            SQL.AppendLine("    Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@School", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Level", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Major", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@HighestInd", Sm.GetGrdBool(Grd1, Row, 5) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeTraining(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeTraining ");
            SQL.AppendLine("(EmpCode, DNo, TrainingCode, SpecializationTraining, Place, Period, Yr, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@EmpCode, @DNo, @TrainingCode, @SpecializationTraining, @Place, @Period, @Yr, @Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<String>(ref cm, "@SpecializationTraining", Sm.GetGrdStr(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@Place", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@Period", Sm.GetGrdStr(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd4, Row, 6));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 7));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeCompetence(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmployeeCompetence(EmpCode, DNo, CompetenceCode, Description, Score, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, @DNo, @CompetenceCode, @Description, @Score, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    CompetenceCode = @CompetenceCode, Description=@Description, Score=@Score, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Score", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        internal void ShowData(string EmpCode)
        {
            try
            {
                ClearData();
                ShowEmployee(EmpCode);
                ShowEmployeeDtl(EmpCode);
                ShowEmployeeDtl2(EmpCode);
                ShowEmployeeDtl3(EmpCode);
                ShowEmployeeDtl4(EmpCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
                BtnSave.Enabled = true;
            }
        }

        private void ShowEmployee(string EmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            SQL.AppendLine("Select EmpCode, EmpCodeOld, EmpName, SiteCode, DeptCode, PosCode From TblEmployee Where EmpCode=@EmpCode; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",

                    //1-5
                    "EmpCodeOld", "EmpName", "SiteCode", "DeptCode", "PosCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[1]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[2]);
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[5]));
                }, true
            );
        }

        private void ShowEmployeeDtl(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.School, A.Level as LevelCode, B.OptDesc As Level, ");
            SQL.AppendLine("A.Major, A.HighestInd, A.Remark ");
            SQL.AppendLine("From  TblEmployeeEducation A ");
            SQL.AppendLine("Left Join TblOption B On A.Level=B.OptCode and B.OptCat='EmployeeEducationLevel' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("Order By A.HighestInd Desc, A.Level;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                 ref Grd1, ref cm, SQL.ToString(),
                 new string[] 
                { 
                    "DNo",
                    "School", "LevelCode", "Level", "Major", "HighestInd",
                    "Remark" 
                },
                 (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                 {
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                     Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 5);
                     Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                 }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowEmployeeDtl2(string EmpCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                   "select DNo, Company,Position,Period,Remark from  TblEmployeeWorkExp Where EmpCode=@EmpCode Order By DNo",

                    new string[] 
                    { 
                        "Dno",
                        "Company", "Position", "Period","Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowEmployeeDtl3(string EmpCode)
        {
            var SQLComp = new StringBuilder();
            SQLComp.AppendLine("Select A.DNo, A.CompetenceCode, B.CompetenceName, A.Description, A.Score ");
            SQLComp.AppendLine("From TblEmployeeCompetence A ");
            SQLComp.AppendLine("Inner Join TblCompetence B On A.CompetenceCode = B.CompetenceCode ");
            SQLComp.AppendLine("Where A.EmpCode = @EmpCode ");
            SQLComp.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQLComp.ToString(),
                    new string[] 
                    { 
                        "DNo",
                        "CompetenceCode", "CompetenceName", "Description", "Score"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);

                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowEmployeeDtl4(string EmpCode)
        {
            var SQL8 = new StringBuilder();
            var cm = new MySqlCommand();

            SQL8.AppendLine("Select A.DNo, A.TrainingCode, B.TrainingName, A.SpecializationTraining, A.Place, A.Period, A.Yr, A.Remark ");
            SQL8.AppendLine("From TblEmployeeTraining A ");
            SQL8.AppendLine("Left Join TblTraining B On A.TrainingCode = B.TrainingCode ");
            SQL8.AppendLine("Where EmpCode=@EmpCode ");
            SQL8.AppendLine("Order By DNo; ");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm, SQL8.ToString(),
                    new string[] 
                    { 
                        "DNo",
                        "TrainingCode", "TrainingName", "SpecializationTraining", "Place", "Period", 
                        "Yr", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);

                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAbleToInputCompetence = Sm.GetParameterBoo("IsAbleToInputCompetence");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private void LueRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.LookUpEdit Lue,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueDeptCode(ref LookUpEdit Lue, string Code, string IsFilterByDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where DeptCode=@Code ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("Or Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            else
            {
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("Where Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueSiteCode(ref LookUpEdit Lue, string Code, string IsFilterBySite)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where SiteCode=@Code ");
                SQL.AppendLine("Or (T.ActInd='Y' ");
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.SiteName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueTrainingCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.TrainingCode As Col1, T.TrainingName As Col2 ");
            SQL.AppendLine("From TblTraining T ");
            SQL.AppendLine("Where T.ActiveInd = 'Y' ");
            SQL.AppendLine("Order By T.TrainingName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(Sl.SetLueEmployeeEducationLevel));
        }

        private void LueLevel_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLevel_Leave(object sender, EventArgs e)
        {
            if (LueLevel.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (LueLevel.Text.Trim().Length == 0)
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueLevel).Trim();
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueLevel.GetColumnValue("Col2");
                }
            }
        }

        private void LueLevel_Validated(object sender, EventArgs e)
        {
            LueLevel.Visible = false;
        }

        private void LueTrainingCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTrainingCode, new Sm.RefreshLue1(SetLueTrainingCode));
            }
        }

        private void LueTrainingCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void LueTrainingCode_Leave(object sender, EventArgs e)
        {
            if (LueTrainingCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueTrainingCode).Length == 0)
                    Grd4.Cells[fCell.RowIndex, 1].Value =
                    Grd4.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueTrainingCode);
                    Grd4.Cells[fCell.RowIndex, 2].Value = LueTrainingCode.GetColumnValue("Col2");
                }
                LueTrainingCode.Visible = false;
            }
        }

        private void LueCompetenceCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCompetenceCode, new Sm.RefreshLue1(Sl.SetLueCompetenceCode));
        }

        private void LueCompetenceCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueCompetenceCode_Leave(object sender, EventArgs e)
        {
            if (LueCompetenceCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueCompetenceCode).Length == 0)
                    Grd3.Cells[fCell.RowIndex, 1].Value =
                    Grd3.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd3.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueCompetenceCode);
                    Grd3.Cells[fCell.RowIndex, 2].Value = LueCompetenceCode.GetColumnValue("Col2");
                }
                LueCompetenceCode.Visible = false;
                Sm.SetGrdNumValueZero(ref Grd3, (fCell.RowIndex + 1), new int[] { 4 });
            }
        }

        #endregion

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmployeeAmendment2Dlg(this));
        }

        #endregion

        #region Grid

        #region Grd1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 3, 4, 5, 6 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                //Untuk input combobox dalam grid
                if (e.ColIndex == 3)
                {
                    LueRequestEdit(Grd1, LueLevel, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if(!Grd3.ReadOnly)
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd3, new int[] { 4 }, e);
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd3.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
                {
                    if (e.ColIndex == 2) LueRequestEdit(Grd3, LueCompetenceCode, ref fCell, ref fAccept, e);
                    //if (Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
                    //{
                    //    LueRequestEdit(Grd4, LueCompetenceCode, ref fCell, ref fAccept, e);
                    //    SetLueCompetenceCode(ref LueCompetenceCode);
                    //}

                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4 });
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (!Grd3.ReadOnly)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.GrdEnter(Grd3, e);
                Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Grd4

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6, 7 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd4, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                LueRequestEdit(Grd4, LueTrainingCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                SetLueTrainingCode(ref LueTrainingCode);
            }
        }

        #endregion

        #endregion

        #endregion

    }
}
