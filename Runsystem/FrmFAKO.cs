﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmFAKO : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = "", 
        mDocNo=string.Empty,
        mSectionNo = string.Empty;
        internal FrmFAKOFind FrmFind;

        #endregion

        #region Constructor

        public FrmFAKO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "FAKO";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetGrd();
            SetLueSortirment(ref LueSortimen);
            SetFormControl(mState.View);
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible =
                BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible =
                BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Item Code",

                        //1-5
                        "Item Local"+ Environment.NewLine +"Code",
                        "Item Name",
                        "",
                        "Quantity",
                        "Uom(Sales)",
                        
                        //6-7
                        "Quantity",
                        "Uom" + Environment.NewLine + "(Inventory)",
                    },
                    new int[] 
                    {
                        //0
                        120,
 
                        //1-5
                        120, 250, 20, 90, 90, 
                        
                        //6-7
                        90, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3},false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7 });

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, ChkCancelInd, TxtLocalDocNo, DteStartDt, DteEndDt, TxtCustomer, TxtDkoNo, TxtLocalDocDKO, TxtPublishName, TxtRegistNo, LueSortimen,MeeNote, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7 });
                   
                    BtnDKO.Enabled = false;
                    BtnFormDKO.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, DteStartDt, DteEndDt, LueSortimen, MeeNote, MeeRemark, 
                    }, false);
                   // Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 6 });
                    BtnDKO.Enabled = true;
                    BtnFormDKO.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, TxtLocalDocNo, DteStartDt, DteEndDt, TxtDkoNo, TxtCustomer, TxtLocalDocDKO, TxtPublishName, TxtRegistNo, LueSortimen, MeeNote, MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 6 });
            Sm.FocusGrd(Grd1, 0, 0);
        }
       
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFAKOFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (ChkCancelInd.Checked == false)
            {
                ParPrint(TxtDocNo.Text);
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "FAKO", "TblFako");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveFako(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsDteEmpty(DteEndDt, "End date") ||
                Sm.IsTxtEmpty(TxtDkoNo, "DKO", false);

               // IsGrdEmpty() ||
              // IsGrdValueNotValid();
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Item Name is empty.")) return true;
            return false;
        }

        private MySqlCommand SaveFako(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblFako(DocNo, DocDt, LocalDocNo, DKODocNo, DtStart, DtEnd, CancelInd, Sortimen, Note, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @DKODocNo, @DtStart, @DtEnd, 'N', @Sortimen, @Note, @Remark, @CreateBy, CurrentDateTime());");
            SQL.AppendLine("Update TblDko Set FakoDocNo = @DocNo Where DocNo = @DkoDocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DKODocNo", TxtDkoNo.Text);
            Sm.CmParamDt(ref cm, "@DtStart", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@DtEnd", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Sortimen", Sm.GetLue(LueSortimen));
            Sm.CmParam<String>(ref cm, "@Note", MeeNote.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelFAKO());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }


        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblFAKO " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelFAKO()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblFAKO Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");
            SQL.AppendLine("Update TblDko Set ");
            SQL.AppendLine("    FakoDocNo=null ");
            SQL.AppendLine("Where DocNo=@DkoDocNo; ");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DkoDocNo", TxtDkoNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowFako(DocNo);
                ShowDataPLDR();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowFako(string DocNo)
        {
            
            var SQL = new StringBuilder();


                    SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.DtStart, A.DtEnd, A.CancelInd, C.CtName As Customer, ");
                    SQL.AppendLine("B.DocNo As DKONumber, B.LocalDocNo As LocalDocDKO, E.EmpName As PublisherName, D.PbsRegisterNo, A.Sortimen, A.Note, A.Remark" );
                    SQL.AppendLine("From TblFako A ");
                    SQL.AppendLine("Left Join TblDKO B On A.DKoDocNo=B.DocNo ");
                    SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode " );
                    SQL.AppendLine("Inner Join TblPublisher D On B.PbsCode=D.PbsCode ");
                    SQL.AppendLine("Inner Join TblEmployee E On D.PbsEmpCode=E.EmpCode" );
                    SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "LocalDocNo", "DtStart", "DtEnd", "CancelInd",
                        
                        //6-10
                        "Customer", "DKONumber", "LocalDocDKO", "PublisherName", "PbsRegisterNo",

                        //11-13
                        "Sortimen", "Note", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);

                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[4]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");

                        TxtCustomer.EditValue = Sm.DrStr(dr, c[6]);
                        TxtDkoNo.EditValue = Sm.DrStr(dr, c[7]);
                        TxtLocalDocDKO.EditValue = Sm.DrStr(dr, c[8]);
                        TxtPublishName.EditValue = Sm.DrStr(dr, c[9]);
                        TxtRegistNo.EditValue = Sm.DrStr(dr, c[10]);

                        LueSortimen.EditValue = Sm.DrStr(dr, c[11]);
                        MeeNote.EditValue = Sm.DrStr(dr, c[12]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                       
                    }, true
                );
        }

        #endregion

        #region Additional Method

        private static string[] satuan = new string[10] { "nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan" };
        private static string[] belasan = new string[10] { "sepuluh", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas" };
        private static string[] puluhan = new string[10] { "", "", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh" };
        private static string[] ribuan = new string[5] { "", "ribu", "juta", "milyar", "triliyun" };

        public static string Kebaca(Decimal d)
        {
            d = Decimal.Round(Math.Abs(d));

            string strHasil = string.Empty;
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = Kebaca(Decimal.Round(frac * 100)) + " sen";
            else
                strHasil = "";

            int nDigit = 0;
            int nPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpBuff = string.Empty;
                nDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                nPosisi = (strTemp.Length - i) + 1;
                switch (nPosisi % 3)
                {
                    case 1:
                        bool bAllZeros = false;
                        if (i == 1)
                            tmpBuff = satuan[nDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpBuff = belasan[nDigit] + " ";
                        else if (nDigit > 0)
                            tmpBuff = satuan[nDigit] + " ";
                        else
                        {
                            bAllZeros = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0") bAllZeros = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0") bAllZeros = false;
                            tmpBuff = string.Empty;
                        }

                        if ((!bAllZeros) && (nPosisi > 1))
                        {
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpBuff = "se" + ribuan[(int)Decimal.Round(nPosisi / 3m)] + " ";
                            else
                                tmpBuff = tmpBuff + ribuan[(int)Decimal.Round(nPosisi / 3)] + " ";
                        }
                        strHasil = tmpBuff + strHasil;
                        break;
                    case 2:
                        if (nDigit > 0) strHasil = puluhan[nDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (nDigit > 0)
                        {
                            if (nDigit == 1)
                                strHasil = "seratus " + strHasil;
                            else
                                strHasil = satuan[nDigit] + " ratus " + strHasil;
                        }
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }

            return strHasil;
        }

     
        public static void SetLueSortirment(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
               ref Lue,
               "Select 'Kayu Lapis' As Col1 Union All " +
               "Select 'Veneer' Union All " +
               "Select 'Serpih/Chip' Union All " +
               "Select 'LVL' Union All " +
               "Select 'Blockboard' Union All " +
               "Select 'Barecore '",
               "Jenis Sortimen");
        }

        private string ConvertFromDecToWord(Decimal d)
        {
            string[] satuan = new string[10] { "nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan" };
            string[] belasan = new string[10] { "sepuluh", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas" };
            string[] puluhan = new string[10] { "", "", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh" };
            string[] ribuan = new string[5] { "", "ribu", "juta", "milyar", "triliyun" };

            string strHasil = "";
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = ConvertFromDecToWord(Decimal.Round(frac * 100)) + "";
            else
                strHasil = "";
            int xDigit = 0;
            int xPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpx = "";
                xDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                xPosisi = (strTemp.Length - i) + 1;
                switch (xPosisi % 3)
                {
                    case 1:
                        bool allNull = false;
                        if (i == 1)
                            tmpx = satuan[xDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpx = belasan[xDigit] + " ";
                        else if (xDigit > 0)
                            tmpx = satuan[xDigit] + " ";
                        else
                        {
                            allNull = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0")
                                    allNull = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0")
                                    allNull = false;
                            tmpx = "";
                        }

                        if ((!allNull) && (xPosisi > 1))
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpx = "se" + ribuan[(int)Decimal.Round(xPosisi / 3m)] + " ";
                            else
                                tmpx = tmpx + ribuan[(int)Decimal.Round(xPosisi / 3)] + " ";
                        strHasil = tmpx + strHasil;
                        break;
                    case 2:
                        if (xDigit > 0)
                            strHasil = puluhan[xDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (xDigit > 0)
                            if (xDigit == 1)
                                strHasil = "seratus " + strHasil;
                            else
                                strHasil = satuan[xDigit] + " ratus " + strHasil;
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }
            return strHasil;
        }

        private string RemoveChar(Decimal x)
        {
            string numb = x.ToString();
            int indexChar = numb.IndexOf(".");
            numb = numb.Substring(0, indexChar);
            return numb;
        }

        private string dec(Decimal a)
        {
            string numbEnd = " Koma ";
            string numbInd = string.Empty;

            string numb = a.ToString();
            int indexChar = numb.IndexOf(".") + 1;

            numb = numb.Substring(indexChar, (numb.Length - indexChar));

            string numbDec = numb;
            for (int i = 0; i < numbDec.Length; i++)
            {
                if (numbDec[i].ToString() == "0")
                {
                    numbInd = "Nol ";
                }
                else if (numbDec[i].ToString() == "1")
                {
                    numbInd = "Satu ";
                }
                else if (numbDec[i].ToString() == "2")
                {
                    numbInd = "Dua ";
                }
                else if (numbDec[i].ToString() == "3")
                {
                    numbInd = "Tiga ";
                }
                else if (numbDec[i].ToString() == "4")
                {
                    numbInd = "Empat ";
                }
                else if (numbDec[i].ToString() == "5")
                {
                    numbInd = "Lima ";
                }
                else if (numbDec[i].ToString() == "6")
                {
                    numbInd = "Enam ";
                }
                else if (numbDec[i].ToString() == "7")
                {
                    numbInd = "Tujuh ";
                }
                else if (numbDec[i].ToString() == "8")
                {
                    numbInd = "Delapan ";
                }
                else
                {
                    numbInd = "Sembilan ";
                }

                numbEnd = numbEnd + numbInd;
            }
            
            numb = numbEnd;
            return numb;
        }

       

        internal void ShowDataPLDR()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string DocNoFromDko = Sm.GetValue("Select If(DrDocno is null, PlDocno, DrDocNo) from TblDKO where DocNo='" + TxtDkoNo.Text + "'");
            string Nilai = Sm.GetValue("Select if (DrDocNo is Null, '1', '2') from TblDKO where DocNo='" + TxtDkoNo.Text + "'");
            string Pl = Sm.GetValue("Select sectionNo from TblDKO Where DocNo='" + TxtDkoNo.Text + "'");

            if (Nilai=="2")
            {
                SQL.AppendLine("Select E.ItCode, F.ItCodeInternal, F.ItName, ");
                SQL.AppendLine("Round(A2.Qty, 0) As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, F.InventoryUomCode ");
                SQL.AppendLine("From TblDRhdr A  ");
                SQL.AppendLine("Inner Join TblDRDtl A2 On A.DocNo = A2.DocNo  ");
                SQL.AppendLine("Inner Join TblSOHdr B On A2.SODocNo=B.DocNo  ");
                SQL.AppendLine("Inner Join TblSODtl C On A2.SODocNo=C.DocNo And A2.SODNo=C.DNo ");
                SQL.AppendLine("Inner Join TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                SQL.AppendLine("Inner Join TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo  ");
                SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
                SQL.AppendLine("Inner Join TblItemPriceHdr G On D.ItemPriceDocNo = G.DocNo ");
                SQL.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And E.ItCode = I.ItCode");
                SQL.AppendLine("Where A.DocNo=@DocNo ");


                Sm.CmParam<String>(ref cm, "@DocNo", DocNoFromDko);
            }

            if (Nilai=="1")
            {
                SQL.AppendLine("Select A.DocNo, A2.Dno, A2.ItCode, B.ItCodeInternal, B.ItName, ");
                SQL.AppendLine("Round(A2.Qty, 0)As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode ");
                SQL.AppendLine("From TblPlhdr A  ");
                SQL.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo  ");
                SQL.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode ");
                SQL.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
                SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo   ");
                SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno  ");
                SQL.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo  ");
                SQL.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode  ");
                SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo  ");
                SQL.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode  ");
                SQL.AppendLine("Where A.DocNo=@DocNo And A2.SectionNo=@SectionNo ");
                SQL.AppendLine("Order By A2.ItCode, A2.DNo;");

                Sm.CmParam<String>(ref cm, "@SectionNo", Pl);
                Sm.CmParam<String>(ref cm, "@DocNo", DocNoFromDko);
            }


            Sm.ShowDataInGrid(
                ref Grd1,ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    "ItCodeInternal", "ItName", "Qty", "PriceUomCode", "QtyInventory", 
                    
                    "InventoryUomCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ParPrint(string DocNo)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<FAKOHdr>();
            var ldtl = new List<FAKODtl>();
            var ldtl2 = new List<FAKODtl2>();
            string[] TableName = { "FAKOHdr", "FAKODtl", "FAKODtl2" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'Shipper5',");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%M %d, %Y') As DocDt, A.LocalDocNo, DATE_FORMAT (A.DtStart, '%d-%m-%y') As DtStart, "); 
            SQL.AppendLine("(SELECT datediff(DtEnd,DtStart)+1)As Gap, B.Destination, C.CtName As Customer,DATE_FORMAT (A.DtEnd, '%d-%m-%y') As DtEnd, C.CtName, ");
            SQL.AppendLine("B.DocNo As DKONumber, B.LocalDocNo As LocalDocDKO, DATE_FORMAT (B.DocDt, '%d-%m-%y')As DocDtDKO, E.EmpName As PublisherName, DATE_FORMAT (D.PbsDt, '%d-%m-%y')As PbsDt, D.PbsRegisterNo, D.PbsLocation, A.Remark, F.PortName, ");
            SQL.AppendLine("if(length(B.PlDocNo) > 0, F.SpCtNotifyParty, J.SAAddress) As Address, A.Sortimen, A.Note ");
            SQL.AppendLine("From TblFako A   ");
            SQL.AppendLine("Left Join TblDKO B On A.DKoDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode  ");
            SQL.AppendLine("Inner Join TblPublisher D On B.PbsCode=D.PbsCode ");
            SQL.AppendLine("Inner Join TblEmployee E On D.PbsEmpCode=E.EmpCode ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select F.Docno, I.PortName, G.SpCtNotifyParty");
            SQL.AppendLine("    From TblPlhdr F");
            SQL.AppendLine("    Inner Join tblsihdr G On F.SIDocNo=G.DocNo ");
            SQL.AppendLine("    Inner Join tblsp H On G.SpDocNo=H.DocNo ");
            SQL.AppendLine("    Inner Join tblport I On H.PortCode1=I.PortCode ");
            SQL.AppendLine(")F On B.Pldocno=F.DocNo ");
            SQL.AppendLine("Left Join TblDrHdr J On B.DrDocNo = J.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo  ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                     //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "Shipper1",
                         "Shipper2",

                         //6-10
                         "Shipper3",
                         "Shipper5",
                         "DocNo",
                         "DocDt",
                         "LocalDocNo",

                         //11-15
                         "DtStart",
                         "DtEnd",
                         "Gap",
                         "Destination",
                         "CtName",

                         //16-20
                         "DKONumber",
                         "LocalDocDKO",
                         "DocDtDKO",
                         "PublisherName",
                         "PbsDt",

                         //21-23
                         "PbsRegisterNo",
                         "PbsLocation",
                         "Remark",
                         "PortName",
                         "Address",
                         "Sortimen",
                         "Note"
                        
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new FAKOHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            Shipper1 = Sm.DrStr(dr, c[4]),
                            Shipper2 = Sm.DrStr(dr, c[5]),

                            Shipper3 = Sm.DrStr(dr, c[6]),
                            Shipper5 = Sm.DrStr(dr, c[7]),
                            DocNo = Sm.DrStr(dr, c[8]),
                            DocDt = Sm.DrStr(dr, c[9]),
                            LocalDocNo = Sm.DrStr(dr, c[10]),

                            DteStartDt = Sm.DrStr(dr, c[11]),
                            DteEndDt = Sm.DrStr(dr, c[12]),
                            gap = Sm.DrStr(dr, c[13]),
                            Terbilang = Kebaca(Sm.DrDec(dr, c[13])),
                            Destination = Sm.DrStr(dr, c[14]),
                            CtName = Sm.DrStr(dr, c[15]),

                            DKONumber = Sm.DrStr(dr, c[16]),
                            LocalDocDKO = Sm.DrStr(dr, c[17]),
                            DocDtDKO = Sm.DrStr(dr, c[18]),
                            PublisherName = Sm.DrStr(dr, c[19]),
                            PbsDt = Sm.DrStr(dr, c[20]),

                            PbsRegisterNo = Sm.DrStr(dr, c[21]),
                            PbsLocation = Sm.DrStr(dr, c[22]),
                            Remark = Sm.DrStr(dr, c[23]),
                            Port = Sm.DrStr(dr, c[24]),
                            Address = Sm.DrStr(dr, c[25]),
                            Sortimen = Sm.DrStr(dr, c[26]),
                            Note = Sm.DrStr(dr, c[27]),
                            //CancelInd = Sm.DrStr(dr, c[24]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                           
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();

            string DocNoFromDko = Sm.GetValue("Select If(DrDocno is null, PlDocno, DrDocNo) from TblDKO where DocNo='" + TxtDkoNo.Text + "'");
            string Nilai = Sm.GetValue("Select if (DrDocNo is Null, '1', '2') from TblDKO where DocNo='" + TxtDkoNo.Text + "'");
            string Pl = Sm.GetValue("Select sectionNo from TblDKO Where DocNo='" + TxtDkoNo.Text + "'");

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                if (Nilai=="2")
                {
                    SQLDtl.AppendLine("Select A.DocNo, A2.Dno, E.ItCode, F.ItCodeInternal, J.ItGrpName As ItName, F.height, F.length, F.Width, ");
                    SQLDtl.AppendLine("Round(A2.Qty, 0) As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, F.InventoryUomCode, ");
                    SQLDtl.AppendLine("Round(if(I.Qty =0, 0, if(G.PriceUomCode = F.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 0) As Qty2 ");
                    SQLDtl.AppendLine("From TblDRhdr A  ");
                    SQLDtl.AppendLine("Inner Join TblDRDtl A2 On A.DocNo = A2.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblSOHdr B On A2.SODocNo=B.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblSODtl C On A2.SODocNo=C.DocNo And A2.SODNo=C.DNo ");
                    SQLDtl.AppendLine("Inner Join TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceHdr G On D.ItemPriceDocNo = G.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And E.ItCode = I.ItCode");
                    SQLDtl.AppendLine("Left Join TblItemGroup J On F.ItGrpCode = J.ItGrpCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNoDR ");
                }

                if (Nilai == "1")
                {
                    SQLDtl.AppendLine("Select A.DocNo, A2.Dno, A2.ItCode, B.ItCodeInternal, J.ItGrpName As ItName, B.height, B.length, B.Width, ");
                    SQLDtl.AppendLine("Round(A2.Qty, 0)As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode, ");
                    SQLDtl.AppendLine("Round(if(I.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 0) As Qty2 ");
                    SQLDtl.AppendLine("From TblPlhdr A  ");
                    SQLDtl.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode ");
                    SQLDtl.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
                    SQLDtl.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo   ");
                    SQLDtl.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno  ");
                    SQLDtl.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode  ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode  ");
                    SQLDtl.AppendLine("Left Join TblItemGroup J On B.ItGrpCode = J.ItGrpCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNoPL And A2.SectionNo=@SectionNo ");
                    SQLDtl.AppendLine("Order By A2.ItCode, A2.DNo");
                }

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@SectionNo", Pl);
                Sm.CmParam<String>(ref cmDtl, "@DocNoDR", DocNoFromDko);
                Sm.CmParam<String>(ref cmDtl, "@DocNoPL", DocNoFromDko);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    "ItCode" ,

                    "ItCodeInternal" ,
                    "ItName",
                    "Height",
                    "Length",
                    "Width",
                    
                    "Qty",
                    "Qty2",
                    "QtyInventory",
                    "PriceUomCode",
                    "InventoryUomCode",
                });

                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new FAKODtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            Height = Sm.DrDec(drDtl, cDtl[3]),
                            Length = Sm.DrDec(drDtl, cDtl[4]),
                            Width = Sm.DrDec(drDtl, cDtl[5]),

                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            
                            QtyInventory = Sm.DrDec(drDtl, cDtl[8]),
                            PriceUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[10]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion                                                    

            #region Detail2

            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();

            string DocNoFromDko2 = Sm.GetValue("Select If(DrDocno is null, PlDocno, DrDocNo) from TblDKO where DocNo='" + TxtDkoNo.Text + "'");
            string Nilai2 = Sm.GetValue("Select if (DrDocNo is Null, '1', '2') from TblDKO where DocNo='" + TxtDkoNo.Text + "'");
            string PL2 = Sm.GetValue("Select sectionNo from TblDKO Where DocNo='" + TxtDkoNo.Text + "'");

            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                if (Nilai == "2")
                {
                    SQLDtl2.AppendLine("Select T.DocNO, T.PriceUomCode, T.InventoryUomCode, SUM(T.Qty) As Quantity, SUM(T.QtyInventory)As QuantityInventory, Sum(QTY2)As Quantity2 From ( ");
                    SQLDtl2.AppendLine("Select A.DocNo, A2.Dno, E.ItCode, F.ItCodeInternal, F.ItName, F.height, F.length, F.Width, ");
                    SQLDtl2.AppendLine("Round(A2.Qty, 0) As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, F.InventoryUomCode, ");
                    SQLDtl2.AppendLine("Round(if(I.Qty =0, 0, if(G.PriceUomCode = F.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 0) As Qty2 ");
                    SQLDtl2.AppendLine("From TblDRhdr A  ");
                    SQLDtl2.AppendLine("Inner Join TblDRDtl A2 On A.DocNo = A2.DocNo  ");
                    SQLDtl2.AppendLine("Inner Join TblSOHdr B On A2.SODocNo=B.DocNo  ");
                    SQLDtl2.AppendLine("Inner Join TblSODtl C On A2.SODocNo=C.DocNo And A2.SODNo=C.DNo ");
                    SQLDtl2.AppendLine("Inner Join TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                    SQLDtl2.AppendLine("Inner Join TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo  ");
                    SQLDtl2.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
                    SQLDtl2.AppendLine("Inner Join TblItemPriceHdr G On D.ItemPriceDocNo = G.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And E.ItCode = I.ItCode");
                    SQLDtl2.AppendLine("Where A.DocNo=@DocNoDR2 ");
                    SQLDtl2.AppendLine(")T ");
                    SQLDtl2.AppendLine("Where T.DocNo=@DocNoDR2");
                    SQLDtl2.AppendLine("Group by T.DocNO,  T.PriceUomCode, T.InventoryUomCode");
                }

                if (Nilai == "1")
                {
                    SQLDtl2.AppendLine("Select T.DocNO, T.PriceUomCode, T.InventoryUomCode, SUM(T.Qty) As Quantity, SUM(T.QtyInventory) As QuantityInventory, Sum(QTY2)As Quantity2 From ( ");
                    SQLDtl2.AppendLine("Select A.DocNo, A2.Dno, A2.ItCode, B.ItCodeInternal, B.ItName, B.height, B.length, B.Width, ");
                    SQLDtl2.AppendLine("Round(A2.Qty, 0)As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode, ");
                    SQLDtl2.AppendLine("Round(if(I.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))),0) As Qty2 ");
                    SQLDtl2.AppendLine("From TblPlhdr A  ");
                    SQLDtl2.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo  ");
                    SQLDtl2.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode ");
                    SQLDtl2.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
                    SQLDtl2.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo   ");
                    SQLDtl2.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno  ");
                    SQLDtl2.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo  ");
                    SQLDtl2.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode  ");
                    SQLDtl2.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo  ");
                    SQLDtl2.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode  ");
                    SQLDtl2.AppendLine("Where A.DocNo=@DocNoPL2 And A2.SectionNo=@SectionNo2 ");
                    SQLDtl2.AppendLine("Order By A2.ItCode, A2.DNo ");
                    SQLDtl2.AppendLine(")T ");
                    SQLDtl2.AppendLine("Where T.DocNo=@DocNoPL2 ");
                    SQLDtl2.AppendLine("Group by T.DocNO,  T.PriceUomCode, T.InventoryUomCode ");
                }

                cmDtl2.CommandText = SQLDtl2.ToString();

                Sm.CmParam<String>(ref cmDtl2, "@SectionNo2", PL2);
                Sm.CmParam<String>(ref cmDtl2, "@DocNoDR2", DocNoFromDko2);
                Sm.CmParam<String>(ref cmDtl2, "@DocNoPL2", DocNoFromDko2);

                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                              
                    "Quantity" ,
                    "Quantity2",
                    "QuantityInventory",
                    "PriceUomCode",
                    "InventoryUomCode",
                });

                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new FAKODtl2()
                        {
                            Quantity = Sm.DrDec(drDtl2, cDtl2[0]),
                            Quantity2 = Sm.DrDec(drDtl2, cDtl2[1]),
                            Terbilang = ConvertFromDecToWord(Sm.DrDec(drDtl2, cDtl2[1])),
                            QuantityInventory = Sm.DrDec(drDtl2, cDtl2[2]),
                            Terbilang2 = ConvertFromDecToWord(Decimal.Parse(RemoveChar(Sm.DrDec(drDtl2, cDtl2[2])))) + dec(Sm.DrDec(drDtl2, cDtl2[2])),
                            //Terbilang3 = ConvertFromDecToWord(Sm.DrDec(drDtl2, cDtl2[2])),
                            PriceUomCode = Sm.DrStr(drDtl2, cDtl2[3]),
                            InventoryUomCode = Sm.DrStr(drDtl2, cDtl2[4]),
                          
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);

            #endregion                                                    
            Sm.PrintReport("FAKO", myLists, TableName, false);

        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void BtnDKO_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmFAKODlg(this));
        }

        private void BtnFormDKO_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDkoNo, "DKO#", false))
            {
                try
                {
                    var f = new FrmDKO(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDkoNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void LueSortirmen_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSortimen, new Sm.RefreshLue1(SetLueSortirment));
        }
        #endregion
    }
    #region Report Class

    class FAKOHdr
    {
        public string CompanyLogo { set; get; }

        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string Shipper1 { get; set; }
        public string Shipper2 { get; set; }

        public string Shipper3 { get; set; }
        public string Shipper5 { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string LocalDocNo { get; set; }

        public string DteStartDt { get; set; }
        public string DteEndDt { get; set; }
        public string Destination { get; set; }
        public string CtName { get; set; }
        public string PrintBy { get; set; }

        public string gap { get; set; }
        public string Terbilang { get; set; }
        public string DKONumber { get; set; }
        public string LocalDocDKO { get; set; }
        public string DocDtDKO { get; set; }

        public string PublisherName { get; set; }
        public string PbsDt { get; set; }
        public string PbsRegisterNo { get; set; }
        public string PbsLocation { get; set; }

        public string Remark { get; set; }
        public string Port { get; set; }
        public string Address { get; set; }
        public string Sortimen { get; set; }
        public string Note { get; set; }
    }

    class FAKODtl
    {

        public string ItCode { get; set; }

        public string ItCodeInternal { get; set; }
        public string ItName { get; set; }
        public decimal Height { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }

        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal QtyInventory { get; set; }
        public string PriceUomCode { get; set; }
        public string InventoryUomCode { get; set; }
        

    }

    class FAKODtl2
    {

        public decimal Quantity { get; set; }
        public decimal Quantity2 { get; set; }
        public decimal QuantityInventory { get; set; }
        public string PriceUomCode { get; set; }
        public string InventoryUomCode { get; set; }
        public string Terbilang { get; set; }
        public string Terbilang2 { get; set; }
       // public string Terbilang3 { get; set; }

    }

    #endregion
}
