﻿#region Update
/*
    25/08/2021 [WED/PADI] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequestDlg7 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest mFrmParent;
        private int mRow = 0;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequestDlg7(FrmMaterialRequest FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueProvCode(ref LueProvCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                 Grd1,
                 new string[] 
                {
                    //0
                    "No.", 
                    
                    //1-4
                    "Province"+Environment.NewLine+"Code",
                    "Province",
                    "City Code",
                    "City"
                },
                  new int[] 
                {
                    //0
                    50, 

                    //1-4
                    100, 200, 100, 200
                }
             );
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ProvCode, B.ProvName, A.CityCode, A.CityName ");
            SQL.AppendLine("From TblCity A ");
            SQL.AppendLine("Inner Join TblProvince B On A.ProvCode = B.ProvCode ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                string Filter = " ";

                Sm.FilterStr(ref Filter, ref cm, TxtCityName.Text, new string[] { "A.CityCode", "A.CityName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueProvCode), "A.ProvCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, mSQL + Filter + " Order By B.ProvName, A.CityName; ",
                new string[] 
                { 
                    //0
                    "ProvCode",

                    //1-3
                    "ProvName", "CityCode", "CityName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                }, true, false, false, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.Grd1.Cells[mRow, 42].Value = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.Grd1.Cells[mRow, 43].Value = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.Grd1.Cells[mRow, 44].Value = Sm.GetGrdStr(Grd1, Row, 4);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueProvCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProvCode, new Sm.RefreshLue1(Sl.SetLueProvCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProvCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Province");
        }

        private void TxtCityName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCityName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "City");
        }

        #endregion

        #endregion

    }
}
