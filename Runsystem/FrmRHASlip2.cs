﻿#region Update
  //  21/03/2019 [HAR] : tambah printout payslip SCU, Data Left trhdp dpeartment (RHA department boleh  kosong), tambah terbilang

#endregion 

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHASlip2 : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty;
        private bool mIsNotFilterByAuthorization = false;

        #endregion

        #region Constructor

        public FrmRHASlip2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method
        
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                BtnSave.Text = "  &Print";
                GetParameter();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Employee's Code",
                        "Employee's Name", 
                        "Old Code",
                        "Position",
                        "Department",
                        
                        
                        //6-7
                        "Join Date",
                        "Resign Date"
                    },
                     new int[] 
                    {
                        //0
                        50, 
                        
                        //1-5
                        100, 250, 100, 150, 150, 
                        
                        //6-7
                        80, 80
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "RHA Document", false)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, A.JoinDt, A.ResignDt ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
                SQL.AppendLine("Where A.EmpCode In ( ");
                SQL.AppendLine("    Select B.EmpCode ");
                SQL.AppendLine("    From TblRHAHdr A, TblRHADtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A' ");
                SQL.AppendLine("    And A.DocNo=@DocNo ");
                SQL.AppendLine(") ");
                
                //if (!mIsNotFilterByAuthorization)
                //{
                //    SQL.AppendLine("And Exists( ");
                //    SQL.AppendLine("    Select EmpCode From TblEmployee ");
                //    SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                //    SQL.AppendLine("    And GrdLvlCode In ( ");
                //    SQL.AppendLine("        Select T2.GrdLvlCode ");
                //    SQL.AppendLine("        From TblPPAHdr T1 ");
                //    SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                //    SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                //    SQL.AppendLine("    ) ");
                //    SQL.AppendLine(") ");
                //}

                SQL.AppendLine("Order By A.EmpName;");
                
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString(),
                        new string[] { 
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "PosName", "DeptName", "JoinDt", 
                            
                            //6
                            "ResignDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        }, true, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            try
            {
                if (IsDataNotValid() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return 
                Sm.IsTxtEmpty(TxtDocNo, "RHA Document", false) ||
                IsDataNotExisted();
        }

        private bool IsDataNotExisted()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 0)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to print minimum 1 record.");
            return true;
        }

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            if (Doctitle == "SCU")
            {
                #region SCU 

                var l = new List<RHAHeader>();
                var ldtl = new List<RHADtl>();
                var ldtl2 = new List<RHADtl2>();

                string[] TableName = { "RHAHeader", "RHADtl", "RHADtl2" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
                SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %b %Y')As DocDt, DATE_FORMAT(A.holidayDt,'%d %b %Y') As holidayDt, A.CurCode, A.Amt, ");
                SQL.AppendLine("DATE_FORMAT(A.DocDt,'%b %Y') As period, UPPER(B.BankName) BankName, C.BankAcNo, D.EmpName As EmpCodeSincerely ");
                SQL.AppendLine("From tblRHAHdr A ");
                SQL.AppendLine("Inner Join tblbank B On A.BankCode = B.bankCode ");
                SQL.AppendLine("Inner Join tblbankAccount C On A.BankAcCode = C.bankAcCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select B.EmpName From tblparameter A  ");
                SQL.AppendLine("    Inner Join TblEmployee B On A.ParValue = B.EmpCode ");
                SQL.AppendLine("    Where A.parCode = 'EmpCodeSincerely' ");
                SQL.AppendLine(")D On 0=0 ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                   
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "Company", 
                    "Address",
                    "Phone",
                    "DocNo",
                    "DocDt",

                    //6-10
                    "HolidayDt", 
                    "CurCode",
                    "Amt",
                    "Period",
                    "BankName",
                    //11
                    "BankAcNo",
                    "EmpCodeSincerely"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new RHAHeader()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                DocDt = Sm.DrStr(dr, c[5]),

                                HolidayDt = Sm.DrStr(dr, c[6]),
                                CurCode = Sm.DrStr(dr, c[7]),
                                Amt = Sm.DrDec(dr, c[8]),
                                Period = Sm.DrStr(dr, c[9]),
                                BankName = Sm.DrStr(dr, c[10]),
                                BankAcNo = Sm.DrStr(dr, c[11]),
                                EmpCodeSincerely = Sm.DrStr(dr, c[12]),
                                Signature = Sm.GetValue("Select Concat(IfNull(B.ParValue, ''), A.Parvalue, '.JPG') " +
                                            "From tblparameter A " +
                                            "Inner Join tblparameter B On 0=0  " +
                                            "Where A.ParCode = 'EmpCodeSincerely' And B.parCode = 'ImgFileSignature'"),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region RHADtl

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                decimal no = 0;
                string EmpCode = string.Empty;
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("select A.EmpCode, B.EmpCodeOld, UPPER(B.Empname) Empname, Upper(F.grdLvlname) grdLvlname, UPPER(B.DeptCode) DeptCode, Upper(C.Deptname) Deptname, UPPER(B.PosCode) PosCode, UPPER(D.posname) posname,  ");
                    SQLDtl.AppendLine("UPPER(B.SectionCode) SectionCode, UPPER(E.SectionName) SectionName, A.value as Salary, A.value4 as allowance, A.Tax, A.Amt As AmtTHR, UPPER(G.bankName) bankName, UPPER(H.BankAcNm) BankAcNm ");
                    SQLDtl.AppendLine("from tblrhaDtl A ");
                    SQLDtl.AppendLine("Inner Join tblEmployee B On A.EmpCode = B.EmpCode ");
                    SQLDtl.AppendLine("Inner Join tblDepartment C On B.DeptCode = C.DeptCode  ");
                    SQLDtl.AppendLine("Left Join tblPosition D On B.PosCode = D.PosCode ");
                    SQLDtl.AppendLine("Left Join tblSection E On B.SectionCode = E.SectionCode  ");
                    SQLDtl.AppendLine("left join tblGradelevelhdr F On B.GrdLvlCode = F.GrdLvlCode  ");
                    SQLDtl.AppendLine("left Join TblBank G On B.bankCode=G.BankCode   ");
                    SQLDtl.AppendLine("left Join TblBankAccount H On B.bankAcNO=H.BankAcNo   ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    Sm.CmParamDt(ref cm, "@CurrentDate", Sm.GetValue("Select holidayDt from tblRhahdr Where DocNo = '" + TxtDocNo.Text + "'"));

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "EmpCode",
                     //1-5
                     "EmpCodeOld",
                     "EmpName",
                     "grdLvlname",
                     "DeptCode",
                     "Deptname",
                     //6-10
                     "PosCode",
                     "posname",
                     "SectionCode",
                     "SectionName",
                     "Salary",
                     //11-15
                     "allowance",
                     "Tax",
                     "AmtTHR",
                     "bankName", 
                     "BankAcNm"
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            no = no + 1;
                            ldtl.Add(new RHADtl()
                            {
                                No = no,
                                EmpCode = Sm.DrStr(drDtl, cDtl[0]),
                                EmpCodeOld = Sm.DrStr(drDtl, cDtl[1]),
                                EmpName = Sm.DrStr(drDtl, cDtl[2]),
                                Level = Sm.DrStr(drDtl, cDtl[3]),
                                DeptCode = Sm.DrStr(drDtl, cDtl[4]),
                                DeptName = Sm.DrStr(drDtl, cDtl[5]),
                                PosCode = Sm.DrStr(drDtl, cDtl[6]),
                                PosName = Sm.DrStr(drDtl, cDtl[7]),
                                SectionCode = Sm.DrStr(drDtl, cDtl[8]),
                                SectionName = Sm.DrStr(drDtl, cDtl[9]),
                                Salary = Sm.DrDec(drDtl, cDtl[10]),
                                Allowance = Sm.DrDec(drDtl, cDtl[11]),
                                Tax = Sm.DrDec(drDtl, cDtl[12]),
                                AmtTHR = Sm.DrDec(drDtl, cDtl[13]),
                                BankName = Sm.DrStr(drDtl, cDtl[14]),
                                BankAcNo = Sm.DrStr(drDtl, cDtl[15]),
                                TerbilangAmtTHR = Sm.Terbilang(Sm.DrDec(drDtl, cDtl[13])),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region RHADtl2
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                decimal numb = 0;
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select H.EmpCode, H.ADCode, UPPER(I.ADName) ADName,  (ifnull(J.parvalue, 0) *H.Amt) As Amt   ");
                    SQLDtl2.AppendLine("From TblEmployeeAllowanceDeduction H  ");
                    SQLDtl2.AppendLine("Inner Join TblAllowanceDeduction I On H.ADCode = I.AdCode  ");
                    SQLDtl2.AppendLine("Left Join Tblparameter J On 0=0 And J.parCode = 'HOWorkingDayPerMth' ");
                    SQLDtl2.AppendLine("Where H.StartDt<=@CurrentDate   ");
                    SQLDtl2.AppendLine("And @CurrentDate<=H.EndDt   ");
                    SQLDtl2.AppendLine("And find_in_set(H.AdCode, (Select ParValue From TblParameter Where ParCode='ADCodeRHA' And ParValue Is Not Null)) >0  ");
                    SQLDtl2.AppendLine("And H.EmpCode in ");
                    SQLDtl2.AppendLine("( ");
	                SQLDtl2.AppendLine("    Select EmpCode From tblRhadtl ");
                    SQLDtl2.AppendLine("    Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine(") ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParamDt(ref cmDtl2, "@CurrentDate", Sm.GetValue("Select holidayDt from tblRhahdr Where DocNo = '"+TxtDocNo.Text+"'"));

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "EmpCode",
                     //1-4
                     "ADCode",
                     "ADName",
                     "Amt",
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            numb = numb + 1;
                            ldtl2.Add(new RHADtl2()
                            {
                                No = numb,
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                ADCode = Sm.DrStr(drDtl2, cDtl2[1]),
                                ADName = Sm.DrStr(drDtl2, cDtl2[2]),
                                Amt = Sm.DrDec(drDtl2, cDtl2[3]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                Sm.PrintReport("PaySlipRHA3", myLists, TableName, false);

                #endregion
            }
            else
            {
                #region default

                string Filter = string.Empty;
                List<IList> myLists = new List<IList>();
                var l = new List<RHA>();
                var cm = new MySqlCommand();
                string[] TableName = { "RHA" };
                var SQL = new StringBuilder();
                var EmpCode = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                        if (Sm.GetGrdBool(Grd1, r, 0) && EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(B.EmpCode=@EmpCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                        }
                    }
                }

                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ")";

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyEmail',");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
                SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %b %Y')As DocDt, DATE_FORMAT(A.holidayDt,'%d %b %Y')As HolidayDt, B.EmpCode, C.EmpName, C.EmpCodeOld, D.DeptName, ");
                SQL.AppendLine("E.PosName, F.SiteName, B.Value, B.Value2, B.Value3, B.Value4, B.Tax, B.Amt, G.GrdLvlName, DATE_FORMAT(C.JoinDt,'%d %b %Y')As JoinDt ");
                SQL.AppendLine("From TblRHAHdr A ");
                SQL.AppendLine("Inner Join TblRHADtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode ");
                SQL.AppendLine("Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine("Left Join TblPosition E On C.PosCode = E.PosCode ");
                SQL.AppendLine("Left Join tblSite F On C.SiteCode = F.SiteCode ");
                SQL.AppendLine("Left Join tblgradelevelhdr G On C.GrdLvlCode= G.GrdLvlCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And A.CancelInd='N' ");
                SQL.AppendLine("And A.Status='A' ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("Order By C.EmpName;");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    {
                    //0
                     "CompanyLogo",
                     //1-5
                     "CompanyName",
                     "CompanyAddress",
                     "CompanyAddressCity",
                     "CompanyPhone",
                     "CompanyEmail",
                     //6-10
                     "DocNo",
                     "DocDt",
                     "HolidayDt",
                     "EmpCode",
                     "EmpName",
                     //11-15
                     "EmpCodeOld",
                     "DeptName",
                     "PosName",
                     "SiteName",
                     "Value",
                     //16-20
                     "Value2",
                     "Value3",
                     "Value4",
                     "Tax",
                     "Amt",
                     //21-22
                     "GrdLvlName",
                     "JoinDt"
                });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new RHA()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressCity = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyEmail = Sm.DrStr(dr, c[5]),

                                DocNo = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7]),
                                HolidayDt = Sm.DrStr(dr, c[8]),
                                EmpCode = Sm.DrStr(dr, c[9]),
                                EmpName = Sm.DrStr(dr, c[10]),

                                EmpCodeOld = Sm.DrStr(dr, c[11]),
                                DeptName = Sm.DrStr(dr, c[12]),
                                PosName = Sm.DrStr(dr, c[13]),
                                SiteName = Sm.DrStr(dr, c[14]),
                                Value = Sm.DrDec(dr, c[15]),

                                Value2 = Sm.DrDec(dr, c[16]),
                                Value3 = Sm.DrDec(dr, c[17]),
                                Value4 = Sm.DrDec(dr, c[18]),
                                Tax = Sm.DrDec(dr, c[19]),
                                Amt = Sm.DrDec(dr, c[20]),

                                GrdLvlName = Sm.DrStr(dr, c[21]),
                                JoinDt = Sm.DrStr(dr, c[21]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                Sm.PrintReport("PaySlipRHA2", myLists, TableName, false);
                #endregion
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0) e.DoDefault = false;
        }

        #endregion

        #endregion

        #region Event

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        Grd1.Cells[Row, 0].Value = !IsSelected;
            }
        }

        private void BtnDocNo_Click(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FormShowDialog(new FrmRHASlip2Dlg(this));
        }

        #endregion

        #region Class

        class RHA
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string HolidayDt { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string SiteName { get; set; }
            public decimal Value { get; set; }
            public decimal Value2 { get; set; }
            public decimal Value3 { get; set; }
            public decimal Value4 { get; set; }
            public decimal Tax { get; set; }
            public decimal Amt { get; set; }
            public string GrdLvlName { get; set; }
            public string JoinDt { get; set; }
            public string PrintBy { get; set; }
        }

        class RHAHeader
        {
            public string CompanyLogo { get; set; }
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string HolidayDt { get; set; } 
            public string CurCode { get; set; }
            public decimal Amt { get; set; }
            public string PrintBy { get; set; }
            public string Signature { get; set; }
            public string Period { get; set; }
            public string BankName { get; set; }
            public string BankAcNo { get; set; }
            public string EmpCodeSincerely { get; set; }
        }

        class RHADtl
        {
            public decimal No { get; set; }
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string Level { get; set; }
            public string EmpName { get; set; }
            public string DeptCode { get; set; }
            public string DeptName { get; set; }
            public string PosCode { get; set; }
            public string PosName { get; set; }
            public string SectionCode { get; set; }
            public string SectionName { get; set; }
            public decimal Salary { get; set; }
            public decimal Allowance { get; set; }
            public decimal Tax { get; set; }
            public string TerbilangAmtTHR { get; set; }
            public decimal AmtTHR { get; set; }
            public string BankName { get; set; }
            public string BankAcNo { get; set; }

        }

        class RHADtl2
        {
            public decimal No { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
        }


        #endregion
    }
}
