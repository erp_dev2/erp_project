﻿#region Update
/*
    05/11/2020 [DITA/IMS] New reporting
    26/11/2020 [ICA/IMS] Mengubah kolom material menjadi component dan menambah kolom material baru. Menambah Filter Project Name.
    12/02/2020 [WED/IMS] tidak di distinct, qty dirubah jadi ambil nya qty Material * qty Finished Good
 */ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectInventoryControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectInventoryControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM ");
            SQL.AppendLine("( ");

            #region Old wed 12/02/2020
            //SQL.AppendLine("    SELECT Distinct A.DocNo SOCDocNo, A.DocDt, E.ProjectCode, E.ProjectName, B.ItCode FinishedGoodCode, H.ItName FinishedGood, H.ItCodeOld FinishedGoodCodeOld, ");
            //SQL.AppendLine("    G.ItCode ComponentCode, I.ItName ComponentName ,G.Qty BOMQty, K.Qty PRQty, M.Qty POQty, N.Qty RecvExpQty, O.Qty RecvVdQty, Q.ItName Material ");
            //SQL.AppendLine("    FROM TblSOContractHdr A ");
            //SQL.AppendLine("    INNER JOIN TblSOContractDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblBOQHdr C ON A.BOQDocNo = C.DocNo  ");
            //SQL.AppendLine("    INNER JOIN TblLOPHdr D ON C.LOPDocNo = D.DocNo AND D.CancelInd = 'N' ");
            //SQL.AppendLine("    LEFT JOIN TblProjectGroup E ON D.PGCode = E.PGCode ");
            //SQL.AppendLine("    INNER JOIN TblBOM2Hdr F ON A.DocNo = F.SOContractDocNo AND F.ItCode = B.ItCode AND F.CancelInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblBOM2Dtl G ON F.DocNo = G.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblItem H ON B.ItCode = H.ItCode  ");
            //SQL.AppendLine("    INNER JOIN TblItem I ON G.ItCode = I.ItCode ");
            //SQL.AppendLine("    LEFT JOIN TblMaterialRequestHdr J ON A.DocNo = J.SOCDocNo ");
            //SQL.AppendLine("    LEFT JOIN TblMaterialRequestdtl K ON J.DocNo = K.DocNo AND F.DocNo = K.BOM2DocNo AND G.ItCode = K.ItCode AND K.CancelInd = 'N' ");
            //SQL.AppendLine("    LEFT JOIN TblPORequestDtl L ON L.MaterialRequestDocNo = K.DocNo AND L.MaterialRequestDNo = K.DNo AND L.CancelInd = 'N' ");
            //SQL.AppendLine("    LEFT JOIN TblPODtl M ON M.PORequestDocNo = L.DocNo AND M.PORequestDNo = L.DNo AND M.CancelInd = 'N' ");
            //SQL.AppendLine("    LEFT JOIN TblRecvExpeditionDtl N ON N.PODocNo=M.DocNo AND N.PODNo=M.Dno AND N.CancelInd = 'N' ");
            //SQL.AppendLine("    LEFT JOIN TblRecvVdDtl O ON O.PODocNo = M.DocNo AND O.PODNo = M.DNo AND O.CancelInd = 'N'  ");
            //SQL.AppendLine("    LEFT JOIN TblBOM2Dtl2 P ON G.DocNo = P.DocNo And G.ItCode = P.ItCode ");
            //SQL.AppendLine("    LEFT JOIN TblItem Q ON P.ItCode2 = Q.ItCode ");
            #endregion

            SQL.AppendLine("    SELECT A.DocNo SOCDocNo, A.DocDt, E.ProjectCode, E.ProjectName, B.ItCode FinishedGoodCode, H.ItName FinishedGood, H.ItCodeOld FinishedGoodCodeOld, ");
            SQL.AppendLine("    G.ItCode ComponentCode, I.ItName ComponentName, IfNull((P.Qty * F.Qty * G.Qty), 0.00) BOMQty, K.Qty PRQty, M.Qty POQty, N.Qty RecvExpQty, O.Qty RecvVdQty, ");
            SQL.AppendLine("    Q.ItCode MaterialCode, Q.ItName Material, Q.ItCodeInternal MaterialLocalCode, Q.Specification MaterialSpecification  ");
            SQL.AppendLine("    FROM TblSOContractHdr A ");
            SQL.AppendLine("    INNER JOIN TblSOContractDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN TblBOQHdr C ON A.BOQDocNo = C.DocNo  ");
            SQL.AppendLine("    INNER JOIN TblLOPHdr D ON C.LOPDocNo = D.DocNo AND D.CancelInd = 'N' ");
            SQL.AppendLine("    LEFT JOIN TblProjectGroup E ON D.PGCode = E.PGCode ");
            SQL.AppendLine("    INNER JOIN TblBOM2Hdr F ON A.DocNo = F.SOContractDocNo AND F.ItCode = B.ItCode AND F.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN TblBOM2Dtl G ON F.DocNo = G.DocNo ");
            SQL.AppendLine("    INNER JOIN TblItem H ON B.ItCode = H.ItCode  ");
            SQL.AppendLine("    INNER JOIN TblItem I ON G.ItCode = I.ItCode ");
            SQL.AppendLine("    LEFT JOIN TblMaterialRequestHdr J ON A.DocNo = J.SOCDocNo ");
            SQL.AppendLine("    LEFT JOIN TblMaterialRequestdtl K ON J.DocNo = K.DocNo AND F.DocNo = K.BOM2DocNo AND G.ItCode = K.ItCode AND K.CancelInd = 'N' ");
            SQL.AppendLine("    LEFT JOIN TblPORequestDtl L ON L.MaterialRequestDocNo = K.DocNo AND L.MaterialRequestDNo = K.DNo AND L.CancelInd = 'N' ");
            SQL.AppendLine("    LEFT JOIN TblPODtl M ON M.PORequestDocNo = L.DocNo AND M.PORequestDNo = L.DNo AND M.CancelInd = 'N' ");
            SQL.AppendLine("    LEFT JOIN TblRecvExpeditionDtl N ON N.PODocNo=M.DocNo AND N.PODNo=M.Dno AND N.CancelInd = 'N' ");
            SQL.AppendLine("    LEFT JOIN TblRecvVdDtl O ON O.PODocNo = M.DocNo AND O.PODNo = M.DNo AND O.CancelInd = 'N'  ");
            SQL.AppendLine("    LEFT JOIN TblBOM2Dtl2 P ON G.DocNo = P.DocNo And G.ItCode = P.ItCode ");
            SQL.AppendLine("    LEFT JOIN TblItem Q ON P.ItCode2 = Q.ItCode ");
            
            SQL.AppendLine(")T ");
            SQL.AppendLine("WHERE T.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "SO#",
                        "Project Code",
                        "Project Name",
                        "Finished Good's"+Environment.NewLine+"Code",
                        "Finished Good",

                        //6-10
                        "Component's Code",
                        "Component",
                        "Material's Code",
                        "Local Code",
                        "Material",

                        //11-15
                        "Specification",
                        "BOM Quantity",
                        "PR Quantity",
                        "PO Quantity",
                        "Receive"+Environment.NewLine+"Expedition",

                        //16
                        "Receive"+Environment.NewLine+"from Vendor",

                    },
                    new int[] 
                    {
                        //0
                        25,

                        //1-5
                        170, 100, 200, 100, 180,

                        //6-10
                        100, 180, 180, 180, 180, 

                        //11-15
                        180, 120, 120, 120, 120, 
                        
                        //16
                        120,

                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 16 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtSOCDocNo.Text, new string[] { "SOCDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "FinishedGoodCode", "FinishedGood", "FinishedGoodCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "ProjectCode" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "ProjectName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By SOCDocNo, FinishedGood, ComponentName, Material;",
                        new string[]
                        { 
                            //0
                            "SOCDocNo", 

                            //1-5
                            "ProjectCode", "ProjectName", "FinishedGoodCode", "FinishedGood", "ComponentCode",
                            
                            //6-10
                            "ComponentName", "MaterialCode", "MaterialLocalCode", "Material", "MaterialSpecification", 

                            //11-15
                            "BOMQty", "PRQty", "POQty", "RecvExpQty", "RecvVdQty", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13, 14, 15, 16 });
                Grd1.Rows.CollapseAll();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtSOCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOCDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Code");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
