﻿#region Update
/*
    23/04/2018 [TKG] tambah warning apabila mau me-non aktifkan bom apabila bom tsb sudah digunakan di production order
    19/09/2019 [TKG/IMS] tambah informasi dimensions
    08/10/2019 [WED/IMS] tambah informasi specification, berdasarkan parameter IsBOMShowSpecifications
    11/12/2019 [VIN/IMS] tambah attachment file result dan material
    28/01/2020 [DITA/MMM] tambah qty per batch
    15/03/2022 [TKG/GSS] merubah GetParameter dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;

using System.Net;
using System.Threading;


#endregion

namespace RunSystem
{
    public partial class FrmBom2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmBom2Find FrmFind;
        private string mMainCurCode = string.Empty;
        private string 
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty;
        internal bool mIsBOMTemplate = false, mIsBOMShowDimensions = false, 
            mIsBOMShowSpecifications = false,
            mIsBOM2UseQtyPerBatch = false;
        iGCell fCell;
        bool fAccept;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmBom2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Bill of Material";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                SetLueOption(ref LueOption);
                LueOption.Visible = false;
                LueBomValue.Visible = false;
                if (!mIsBOM2UseQtyPerBatch)
                {
                    LblQtyPerBatch.Visible = false;
                    TxtBatchQty.Visible = false;
                }

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Item's Code",
                    "",
                    "Item's Code"+Environment.NewLine+"Internal",
                    "Item's Name",
                    "UoM",
                    
                    //6-10
                    "Option Code",
                    "Option",
                    "Quantity",
                    "Percentage"+Environment.NewLine+"(%)",
                    "UoM 2",

                    //11-15
                    "Specifications",
                    "File Name", 
                    "", 
                    "",
                    "File Name 2"
                },
                new int[] 
                {
                    20, 
                    80, 20, 100, 250, 100, 
                    80, 100, 100, 100, 80,
                    300, 200, 20, 20, 200
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0, 2, 13, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 6, 10, 15 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 11 });
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 11 });
            Grd1.Cols[10].Move(6);

            #endregion

            #region Grid2

            Grd2.Cols.Count = 19;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Type",
                        "Document#",
                        "",
                        "Name",
                        "Source",
                        
                        //6-10
                        "Quantity",
                        "UoM",
                        "Value Code",
                        "Value From",
                        "Value",

                        //11-15
                        "UoM 2",
                        "Old Code",
                        "Dimensions",
                        "Specifications",
                        "File Name", 

                        //16-18
                        "", 
                        "",
                        "File Name 2"
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        0, 130, 20, 250, 130, 
                        
                        //6-10
                        80, 80, 80, 100, 80,
 
                        //11-15
                        80, 100, 300, 300, 200, 
                        
                        //16-18
                        20, 20, 200
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0, 3, 16, 17 });
            Sm.GrdFormatDec(Grd2, new int[] { 6, 10 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 8, 11, 18 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 13, 14 });
            if (!mIsBOMShowDimensions) Sm.GrdColInvisible(Grd2, new int[] { 13 }, false);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd2, new int[] { 14 });
            Grd2.Cols[11].Move(8);
            Grd2.Cols[12].Move(5);

            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd2, new int[] { 2, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 10 }, !ChkHideInfoInGrd.Checked);
       
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtDocName,  LueBomValue, MeeRemark, TxtWCDocNo, TxtSource, TxtBatchQty
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 18 });
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 15 });
                    ChkActiveInd.Properties.ReadOnly = true;
                    BtnWCDocNo.Enabled = false;
                    BtnBomDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtDocName, MeeRemark,  LueBomValue, TxtBatchQty
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 6, 9, 16, 17 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 6, 7, 8, 13, 14 });
                    BtnWCDocNo.Enabled = true;
                    BtnBomDocNo.Enabled = true;
                    BtnWCDocNo.Focus();
                    break;
                case mState.Edit:
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkActiveInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, TxtDocName, MeeRemark,TxtWCDocNo, LueBomValue, TxtSource
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtBatchQty }, 0);
            ChkActiveInd.Checked = false;
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 6, 10 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9 });
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBom2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                ChkActiveInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        #region Grid 1 Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmBom2Dlg(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 6, 8, 9 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9 });
                }

                if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 1), "1"))
                    {
                        var f1 = new FrmItem(mMenuCode);
                        f1.Tag = mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f1.ShowDialog();
                    }
                }
                if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
                {
                    Sm.LueRequestEdit(ref Grd1, ref LueOption, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    SetLueOption(ref LueOption);
                }
            }
            
            if (e.ColIndex == 14)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 12), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    SFD.DefaultExt = "";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 12, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputePercentage();
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmBom2Dlg(this));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 14)
                {
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length > 0)
                    {
                        DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 12), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                        SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                        SFD.DefaultExt = "";
                        SFD.AddExtension = true;

                        if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 12, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                        {
                            if (SFD.ShowDialog() == DialogResult.OK)
                            {
                                Application.DoEvents();

                                //Write the bytes to a file
                                FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                                newFile.Write(downloadedData, 0, downloadedData.Length);
                                newFile.Close();
                                Sm.StdMsg(mMsgType.Info, "File Downloaded");
                            }
                        }
                        else
                            Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                    }
            }
            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 13)
                {
                    //try
                    //{
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image Files(*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG|PDF files (*.pdf)|*.pdf";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd1.Cells[e.RowIndex, 12].Value = OD.FileName;
                    //}
                    //catch (Exception Exc)
                    //{
                    //    Sm.ShowErrorMsg(Exc);
                    //}

                }
            }
        }
        
        

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
                ComputePercentage();
        }

        #endregion
        
        #region Grid 2 Method

        private void Grd2_RequestEdit(object sender, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtWCDocNo, "Workcenter still empty", false))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmBom2Dlg2(this, TxtWCDocNo.Text));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 6, 9, 10 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 6, 10 });
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 1), "1"))
                {
                    var f1 = new FrmItem(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 1), "2"))
                {
                    var f2 = new FrmFormula(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 1), "3"))
                {
                    var f3 = new FrmEmployee(mMenuCode);
                    f3.Tag = mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f3.ShowDialog();
                }
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd2, ref LueBomValue, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sl.SetLueBomValueCode(ref LueBomValue);
            }

            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 15).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 15), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd2, e.RowIndex, 15);
                    SFD.DefaultExt = "";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 15, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, TenTec.Windows.iGridLib.iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsTxtEmpty(TxtWCDocNo, "Workcenter still empty", false))
            {
                Sm.FormShowDialog(new FrmBom2Dlg2(this, TxtWCDocNo.Text));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 1), "1"))
                {
                    var f1 = new FrmItem(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 1), "2"))
                {
                    var f2 = new FrmFormula(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f2.ShowDialog();
                }


                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 1), "3"))
                {
                    var f3 = new FrmEmployee(mMenuCode);
                    f3.Tag = mMenuCode;
                    f3.WindowState = FormWindowState.Normal;
                    f3.StartPosition = FormStartPosition.CenterScreen;
                    f3.mEmpCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f3.ShowDialog();
                }
            }

            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd2, e.RowIndex, 15).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd2, e.RowIndex, 15), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd2, e.RowIndex, 15);
                    SFD.DefaultExt = "";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 15, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 16)
                {
                    //try
                    //{
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image Files(*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG|PDF files (*.pdf)|*.pdf";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd2.Cells[e.RowIndex, 15].Value = OD.FileName;
                    //}
                    //catch (Exception Exc)
                    //{
                    //    Sm.ShowErrorMsg(Exc);
                    //}

                }
            }
        }

        private void Grd2_AfterCommitEdit(object sender, TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 6, 10 }, e);
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BOM2", "TblBOMHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBomHdr(DocNo));
            cml.Add(SaveBomDtl2(DocNo));
            cml.Add(SaveBomDtl(DocNo));

            //for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveBomDtl2(DocNo, Row));

            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveBomDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 12).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 12).Length > 0 && Sm.GetGrdStr(Grd1, Row, 12) != "openFileDialog1")
                    {
                        UploadFile2(DocNo, Row, Sm.GetGrdStr(Grd1, Row, 12));
                    }
                }
            }


            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 15).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 15).Length > 0 && Sm.GetGrdStr(Grd2, Row, 15) != "openFileDialog1")
                    {
                        UploadFile(DocNo, Row, Sm.GetGrdStr(Grd2, Row, 15));
                    }
                }
            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWCDocNo, "WorkCenter", false) ||
                Sm.IsTxtEmpty(TxtDocName, "Bill Of Material's Name", false) ||
                Sm.IsDteEmpty(DteDocDt, "Bill Of date") ||
                (mIsBOM2UseQtyPerBatch && Sm.IsTxtEmpty(TxtBatchQty, "Quantity per batch", true)) ||
                IsGrdEmpty() ||
                IsGrdOptionEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdProductMoreThanOne() ||
                IsUploadFileNotValid() ||
                IsUploadFileNotValid2();
        }

        private bool IsUploadFileNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 12).Length > 0)
                {

                    if (Sm.GetGrdStr(Grd1, Row, 12).Length > 0 && Sm.GetGrdStr(Grd1, Row, 12) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd1, Row, 12))) return true;
                    }
                    
                }
            }

            return false;
        }

        private bool IsUploadFileNotValid2()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 15).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 15).Length > 0 && Sm.GetGrdStr(Grd2, Row, 15) != "openFileDialog1")
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd2, Row, 15))) return true;
                        }
                }
            }

            return false;
        }



        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the result list.");
                return true;
            }

            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No data in the material list.");
                return true;
            }
            
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, true,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "UoM : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine +
                        "Quantity should be greater than 0.")
                    ) return true;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (
                    Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Document number is empty.") ||
                    Sm.IsGrdValueEmpty(Grd2, Row, 6, true,
                        "Document# : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Document's Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                        "Document's Source : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine + Environment.NewLine +
                        "Quantity should be greater than 0.") //||
                    //Sm.IsGrdValueEmpty(Grd2, Row, 10, true,
                    //    "Document Number : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                    //    "Document Name : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine +
                    //    "Document's Source : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine + Environment.NewLine +
                    //    "Value From still empty.")
                    ) return true;

          

                return false;
        }

        private bool IsGrdOptionEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 6, false,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "UoM : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine +
                        "Option Code still empty.") 
                    ) return true;
            return false;
        }

        private bool IsGrdProductMoreThanOne()
        {
            int x = 0;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 6), "1"))
                {
                    x = x + 1;
                }
            }
            if (x > 1)
            {
                Sm.StdMsg(mMsgType.Warning, "The result should only have one product.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveBomHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/* BOM2 */ ");

            SQL.AppendLine("Insert Into TblBomHdr(DocNo, DocDt, DocName, ActiveInd, WorkCenterDocNo, TemplateInd, QtyPerBatch,Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocName, 'Y', @WorkCenterDocNo, @TemplateInd, @QtyPerBatch, @Remark, @UserCode, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", TxtWCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TemplateInd", mIsBOMTemplate ? "Y":"N");
            Sm.CmParam<Decimal>(ref cm, "@QtyPerBatch", Decimal.Parse(TxtBatchQty.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBomDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* BOM2 Dtl */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblBomDtl(DocNo, DNo, DocType, DocCode, Qty, BomValueFrom, Value, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @DocType_" + r.ToString() +
                        ", @DocCode_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @BomValueFrom_" + r.ToString() +
                        ", @Value_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@DocType_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 1));
                    Sm.CmParam<String>(ref cm, "@DocCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@BomValueFrom_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Value_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 10));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 15));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveBomDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblBomDtl(DocNo, DNo, DocType, DocCode, Qty, BomValueFrom, Value, FileName, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @DocType, @DocCode, @Qty, @BomValueFrom, @Value, @FileName, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetGrdStr(Grd2, Row, 2));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@BomValueFrom", Sm.GetGrdDec(Grd2, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd2, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd2, Row, 15));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveBomDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* BOM2 Dtl2 */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblBomDtl2(DocNo, DNo, ItCode, ItType, Qty, Percentage, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @ItCode_" + r.ToString() +
                        ", @ItType_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Percentage_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@ItType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Percentage_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveBomDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblBomDtl2(DocNo, DNo, ItCode, ItType, Qty, Percentage, FileName, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @ItCode, @ItType, @Qty, @Percentage, @FileName, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@ItType", Sm.GetGrdStr(Grd1, Row, 6));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Percentage", Sm.GetGrdDec(Grd1, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd1, Row, 12));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateBOMFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBOMDtl Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));


            return cm;
        }

        private MySqlCommand UpdateBOMFile2(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBOMDtl2 Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));


            return cm;
        }


        #endregion

       
        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            EditBOMHdr();
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Bill of material#", false) ||
                IsActiveIndEditedAlready() ||
                IsBOMAlreadyUsedInProductionOrder();
        }

        private bool IsBOMAlreadyUsedInProductionOrder()
        {
            if (ChkActiveInd.Checked) return false;

            if (Sm.IsDataExist(
                "Select 1 From TblProductionOrderHdr A, TblProductionOrderDtl B " +
                "Where A.DocNo=B.Docno And A.CancelInd='N' And B.BomDocNo=@Param;",
                TxtDocNo.Text))
            {
                if (Sm.StdMsgYN("Question",
                    "This bill of material already used in production order." + Environment.NewLine +
                    "Do you still want to disactived this bill of material ?" + Environment.NewLine +
                    "Note : You can't use these items again in SFC.") == DialogResult.Yes)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        private bool IsActiveIndEditedAlready()
        {
            var ActiveInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblBomHdr Where DocNo=@DocNo And ActiveInd=@ActiveInd "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ActiveInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is " + (Sm.CompareStr(ActiveInd, "Y") ? "" : "non") + "activated already.");
                return true;
            }
            return false;
        }

        private void EditBOMHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblBomHdr Set ActiveInd=@ActiveInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() Where DocNo=@DocNo"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowBomHdr(DocNo);
                ShowBomDtl(DocNo);
                ShowBomDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowData2(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Sm.ClearGrd(Grd1, true);
                ShowBomDtl(DocNo);
                ShowBomDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBomHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, WorkCenterDocNo, DocDt, DocName, ActiveInd, Remark, QtyPerBatch " +
                    "From TblBomHdr  " +
                    "Where DocNo=@DocNo",
                    new string[] 
                    { 
                        "DocNo", 
                        "WorkCenterDocNo", "DocDt", "DocName", "ActiveInd", "Remark",
                        "QtyPerBatch"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtWCDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        TxtDocName.EditValue = Sm.DrStr(dr, c[3]);
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        TxtBatchQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    }, true
                );
        }

        private void ShowBomDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocType, A.DocCode, B.OptDesc, A.Qty, E.PlanningUomCode, E.PlanningUomCOde2, A.Value, ");
            SQL.AppendLine("Case A.DocType ");
            SQL.AppendLine("    When '3' Then Concat(C.EmpName, ' (', IfNull(G.PosName, '-'), ' / ', IfNull(H.DeptName, '-'), ')') ");
            SQL.AppendLine("    When '2' Then D.DocName ");
            SQL.AppendLine("    When '1' Then E.ItName End As DocName, ");
            SQL.AppendLine("A.BOMValueFrom, F.OptDesc As BomValueName, ");
            SQL.AppendLine("Case When A.DocType='3' Then C.EmpCodeOld Else Null End As OldCode, ");
            SQL.AppendLine("E.Specification, A.FileName, ");
            if (mIsBOMShowDimensions)
            {
                SQL.AppendLine("Case When A.DocType='1' Then ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When IfNull(E.Length, 0.00)<>0.00 Then ");
                SQL.AppendLine("        Concat('Length : ', Trim(Concat(Convert(Format(IfNull(E.Length, 0.00), 2) Using utf8), ' ', IfNull(E.LengthUomCode, '')))) ");
                SQL.AppendLine("    Else '' End, ' ', ");
                SQL.AppendLine("Case When IfNull(E.Height, 0.00)<>0.00 Then ");
                SQL.AppendLine("    Concat('Height : ', Trim(Concat(Convert(Format(IfNull(E.Height, 0.00), 2) Using utf8), ' ', IfNull(E.HeightUomCode, '')))) ");
                SQL.AppendLine("Else '' End, ' ', ");
                SQL.AppendLine("Case When IfNull(E.Width, 0.00)<>0.00 Then ");
                SQL.AppendLine("    Concat('Width : ', Trim(Concat(Convert(Format(IfNull(E.Width, 0.00), 2) Using utf8), ' ', IfNull(E.WidthUomCode, '')))) ");
                SQL.AppendLine("Else '' End ");
                SQL.AppendLine(")) Else Null End As Dimensions ");
            }
            else
                SQL.AppendLine("Null As Dimensions ");
            SQL.AppendLine("From TblBOMDtl A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='BomDocType' ");
            SQL.AppendLine("Left Join TblEmployee C On A.DocCode=C.EmpCode And '3'=A.DocType ");
            SQL.AppendLine("Left Join TblFormulaHdr D On A.DocCode=D.DocNo And '2'=A.DocType ");
            SQL.AppendLine("Left Join TblItem E On A.DocCode=E.ItCode And '1'=A.DocType ");
            SQL.AppendLine("Left Join TblOption F On A.BomValueFrom = F.OptCode And F.OptCat='BOMValueFrom' ");
            SQL.AppendLine("Left Join TblPosition G On C.PosCode=G.PosCode ");
            SQL.AppendLine("Left Join TblDepartment H On C.DeptCode=H.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocType", 

                    //1-5
                    "DocCode", "DocName", "OptDesc", "Qty", "PlanningUomCode", 
                    
                    //6-10
                   "BOMValueFrom", "BomValueName", "Value", "PlanningUomCode2", "OldCode",

                   //11-12
                   "Dimensions", "Specification", "FileName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 6, 10 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowBomDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ItCode, C.ItCodeInternal, C.ItName, C.PlanningUomCode, C.PlanningUomCode2, ");
            SQL.AppendLine("B.ItType, D.OptDesc, B.Qty, B.Percentage, B.FileName, C.Specification ");
            SQL.AppendLine("From TblBomHdr A ");
            SQL.AppendLine("Inner Join TblBomDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblOption D On D.OptCode = B.ItType And D.OptCat = 'BOMItType' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItCodeInternal", "ItName", "PlanningUomCode", "ItType", "OptDesc",  
                    //6-10
                    "Qty","Percentage",  "PlanningUomCode2", "Specification", "FileName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }
       
        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

                var cml = new List<MySqlCommand>();
                cml.Add(UpdateBOMFile(DocNo, Row, toUpload.Name));
                Sm.ExecCommands(cml);
            
        }

        private void UploadFile2(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateBOMFile2(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName) ||
                IsFileNameAlreadyExisted2(Row, FileName) 
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File "+FileName+" too large to Upload " );
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblBOMDtl ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblBOMDtl2 ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsBOM2UseQtyPerBatch', 'IsBOMShowSpecifications', 'IsBOMShowDimensions', 'MainCurCode', 'MenuCodeForBOMStandard', ");
            SQL.AppendLine("'PortForFTPClient', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient', 'FormatFTPClient' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsBOMShowDimensions": mIsBOMShowDimensions = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsBOM2UseQtyPerBatch": mIsBOM2UseQtyPerBatch = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "MenuCodeForBOMStandard": mIsBOMTemplate = !Sm.CompareStr(mMenuCode, ParValue); break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetLueOption(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'BOMItType'  Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal string GetSelectedDocument()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += ((SQL.Length != 0 ? ", " : "") + "'" + Sm.GetGrdStr(Grd2, Row, 1) + Sm.GetGrdStr(Grd2, Row, 2) + "'");

            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += ((SQL.Length != 0 ? ", " : "") + "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'");

            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputePercentage()
        {
            decimal Qty = 0m, Percentage = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                   Sm.GetGrdStr(Grd1, Row, 1).Length != 0 &&
                   Sm.GetGrdStr(Grd1, Row, 6).Length != 0
                   )
                {
                    Qty += Sm.GetGrdDec(Grd1, Row, 8);
                }
            }

            for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
            {
                if (
                   Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                   Sm.GetGrdStr(Grd1, Row2, 6).Length != 0
                   )
                {
                    if (Qty != 0)
                    {
                        Percentage = (Sm.GetGrdDec(Grd1, Row2, 8) / Qty) * 100m;
                        Grd1.Cells[Row2, 9].Value = Sm.FormatNum(Percentage, 0);
                    }
                    else
                    {
                        Grd1.Cells[Row2, 9].Value = 0m;
                    }
                }
            }
        }

        private void ComputeValue(int Row)
        {
            try
            {
                var BOMValueFrom = Sm.GetGrdStr(Grd2, Row, 8);
                Grd2.Cells[Row, 10].Value = 0m;
                if (BOMValueFrom == "1")
                {
                    Grd2.Cells[Row, 10].ReadOnly = iGBool.False;
                    Grd2.Cells[Row, 10].BackColor = Color.White;
                }
                else
                {
                    Grd2.Cells[Row, 10].ReadOnly = iGBool.True;
                    Grd2.Cells[Row, 10].BackColor = Color.FromArgb(224, 224, 224);
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0 && Sm.GetGrdStr(Grd2, Row, 1) == "1")
                    {
                        if (BOMValueFrom == "2") ComputeValueFromStockValue(Row);
                        if (BOMValueFrom == "3") ComputeValueFromItemPrice(Row);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ComputeValueFromStockValue(int Row)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.UPrice*C.ExcRate As Value ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ItCode=@ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("Order By A.DocDt Desc, B.CreateDt Desc ");
            SQL.AppendLine("Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 2));
            var Value = Sm.GetValue(cm);
            if (Value.Length == 0) Value = "0";
            Grd2.Cells[Row, 10].Value = decimal.Parse(Value);
        }

        private void ComputeValueFromItemPrice(int Row)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.UPrice* ");
            SQL.AppendLine("Case When A.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End As Value ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode ");
            SQL.AppendLine("Order By A.DocDt Desc, B.CreateDt Desc ");
            SQL.AppendLine("Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            var Value = Sm.GetValue(cm);
            if (Value.Length == 0) Value = "0";
            Grd2.Cells[Row, 10].Value = decimal.Parse(Value);
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnBomDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBom2Dlg4(this));
        }

        private void BtnWCDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmBom2Dlg3(this));
        }

        private void BtnWCDocNo2_Click(object sender, EventArgs e)
        {
            if (TxtWCDocNo.Text.Length != 0)
            {
                var f1 = new FrmWorkCenter(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtWCDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void LueBomValue_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBomValue, new Sm.RefreshLue1(Sl.SetLueBomValueCode));
        }

        private void LueBomValue_Leave(object sender, EventArgs e)
        {
            if (LueBomValue.Visible && fAccept && fCell.ColIndex == 9)
            {
                if (Sm.GetLue(LueBomValue).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 8].Value = null;
                    Grd2.Cells[fCell.RowIndex, 9].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 8].Value = Sm.GetLue(LueBomValue);
                    Grd2.Cells[fCell.RowIndex, 9].Value = LueBomValue.GetColumnValue("Col2");
                }
                ComputeValue(fCell.RowIndex);
                LueBomValue.Visible = false;
            }
        }

        private void LueBomValue_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOption));
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 6].Value = null;
                    Grd1.Cells[fCell.RowIndex, 7].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueOption);
                    Grd1.Cells[fCell.RowIndex, 7].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }

        #endregion

        #endregion
    }
}
