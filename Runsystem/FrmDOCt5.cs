﻿#region Update
/*
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt5 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmDOCt5Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private bool 
            mIsDOCtNeedShippingAddressInd = false,
            mIsAutoJournalActived = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtRemarkEditable = false
            ;
        private string 
            mDocType = "07",
            mMainCurCode = string.Empty;
        internal bool
            mIsAmtRoundingUp = false,
            mIsShowForeignName = false,
            mIsShowLocalDocNo = false,
            mIsItGrpCodeShow = false;
        internal string
            mCityCode = string.Empty,
            mCntCode = string.Empty;
        private bool mIsDOCtProsessToSalesInvoice = false;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmDOCt5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "DO To Customer";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueCurCode(ref LueCurCode);

                LblSAName.ForeColor = mIsDOCtNeedShippingAddressInd?
                    System.Drawing.Color.Red:System.Drawing.Color.Black;

               
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsAmtRoundingUp = Sm.GetParameterBoo("AmtRoundingUp");
            mIsDOCtProsessToSalesInvoice = Sm.GetParameterBoo("IsDOCtProsessToSalesInvoice");
            mIsAcNoForSaleUseItemCategory = Sm.GetParameterBoo("IsAcNoForSaleUseItemCategory");
            mIsDOCtRemarkEditable = Sm.GetParameterBoo("IsDOCtRemarkEditable");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsShowLocalDocNo = Sm.GetParameterBoo("IsShowLocalDocNo");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 37;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "",
                        //6-10
                        "Local Code",
                        "Item's Name",
                        "Foreign Name",
                        "Property Code",
                        "Property",
                        //11-15
                        "Customer"+Environment.NewLine+"Refference",
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        //16-20
                        "Group",
                        "Quantity" +Environment.NewLine + "(Packaging)",
                        "Uom" +Environment.NewLine + "(Packaging)",
                        "Stock",
                        "Previous"+Environment.NewLine+"Quantity",
                        //21-25
                        "Current"+Environment.NewLine+"Quantity",
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Previous"+Environment.NewLine+"Quantity",
                        //26-30
                        "Current"+Environment.NewLine+"Quantity",
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Previous"+Environment.NewLine+"Quantity",
                        //31-35
                        "Current"+Environment.NewLine+"Quantity",
                        "Quantity",
                        "UoM",
                        "Unit Price",
                        "Amount",
                        //36
                        "Remark",
                        
                    },
                     new int[] 
                    {
                        //0
                        20,
                        //1-5
                        60, 20, 20, 100, 20,
                        //6-10
                        100, 180, 180, 80, 80,
                        //11-15
                        150, 150, 150, 80, 80, 
                        //16-20
                        120, 100, 100, 80, 100,
                        //21-25
                        100, 100, 80, 100, 100,
                        //26-30
                        100, 100, 80, 100, 100,
                        //31-35
                        100, 100, 80, 100, 150,
                        //
                        250
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 17, 19, 20, 21, 22, 24, 25, 26, 27, 29, 30, 31, 32, 34, 35 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 
                7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
                27, 28, 29, 30, 32, 33, 34, 35, 36
            });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5, 6, 9, 10, 11, 13, 14, 15, 16, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 }, false);
           
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        "Total Quantity", "Total Quantity 2", "Total Quantity 3"
                    },
                    new int[] 
                    {
                        130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 0, 1, 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, false);

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27, 28 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27, 28, 29, 30, 31, 32, 33 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, TxtDocNoInternal, TxtSAName, 
                        TxtResiNo, TxtDocNoInternal, LueCurCode, TxtCtQtDocNo, MeeRemark, 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 21, 26, 31, 36 });
                    TxtDocNo.Focus();
                    BtnCustomerShipAddress.Enabled = false;
                    BtnSAName.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, TxtDocNoInternal, LueCurCode, 
                        TxtResiNo, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 21, 26, 31, 36 });
                    BtnCustomerShipAddress.Enabled = true;
                    BtnSAName.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCityCode = string.Empty;
            mCntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueCtCode, TxtDocNoInternal, 
                MeeRemark, LueCurCode, TxtSAName, MeeSAAddress, TxtCity, 
                TxtCountry, TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtResiNo, 
                TxtMobile, TxtCtQtDocNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSAName, MeeSAAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile
            });
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 17, 19, 20, 21, 22, 24, 25, 26, 27, 29, 30, 31, 32, 34, 35 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 0, 1, 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOCt5Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
           
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOCt5Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
                    {
                        
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 19, 20, 21, 22, 24, 25, 26, 27, 29, 30, 31, 32, 34, 35 });
                    }
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalQty();
                ComputeAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmDOCt5Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 17, 19, 20, 21, 22, 24, 25, 26, 27, 29, 30, 31, 32, 34, 35 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 25 }, e);

            if (e.ColIndex == 21)
            {
                if (Sm.GetGrdDec(Grd1, e.RowIndex, 20) > 0)
                {
                    Grd1.Cells[e.RowIndex, 22].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 21) - Sm.GetGrdDec(Grd1, e.RowIndex, 20);
                }
                else
                {
                    Grd1.Cells[e.RowIndex, 22].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 21);
                }
                ComputeTotalQty();
            }

            if (e.ColIndex == 26)
            {
                if (Sm.GetGrdDec(Grd1, e.RowIndex, 25) > 0)
                {
                    Grd1.Cells[e.RowIndex, 27].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 26) - Sm.GetGrdDec(Grd1, e.RowIndex, 25);
                }
                else
                {
                    Grd1.Cells[e.RowIndex, 27].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 26);
                }
                ComputeTotalQty();
            }

            if (e.ColIndex == 31)
            {
                if (Sm.GetGrdDec(Grd1, e.RowIndex, 30) > 0)
                {
                    Grd1.Cells[e.RowIndex, 32].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 31) - Sm.GetGrdDec(Grd1, e.RowIndex, 30);
                }
                else
                {
                    Grd1.Cells[e.RowIndex, 32].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 31);
                }
                ComputeTotalQty();
            }

            if (e.ColIndex == 21 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 23), Sm.GetGrdStr(Grd1, e.RowIndex, 28)))
            {
                Sm.CopyGrdValue(Grd1, e.RowIndex, 27, Grd1, e.RowIndex, 22);
                Sm.CopyGrdValue(Grd1, e.RowIndex, 26, Grd1, e.RowIndex, 21);
            }

            if (e.ColIndex == 21 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 23), Sm.GetGrdStr(Grd1, e.RowIndex, 33)))
            {
                Sm.CopyGrdValue(Grd1, e.RowIndex, 32, Grd1, e.RowIndex, 22);
                Sm.CopyGrdValue(Grd1, e.RowIndex, 31, Grd1, e.RowIndex, 21);
            }

            if (e.ColIndex == 26 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 28), Sm.GetGrdStr(Grd1, e.RowIndex, 33)))
            {
                Sm.CopyGrdValue(Grd1, e.RowIndex, 32, Grd1, e.RowIndex, 27);
                Sm.CopyGrdValue(Grd1, e.RowIndex, 31, Grd1, e.RowIndex, 26);
            }

           
            if (Sm.IsGrdColSelected(new int[] { 1, 15, 18, 21 }, e.ColIndex))
            {
                ComputeTotal(e.RowIndex);
                ComputeTotalQty();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOCtHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveDOCtDtl(DocNo, Row));

            cml.Add(SaveStock(DocNo));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                (mIsDOCtNeedShippingAddressInd && Sm.IsTxtEmpty(TxtSAName, "Shipping address", false)) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
                
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;

                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                   "Property : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                   "Batch# : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                   "Source : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                   "Lot : " + Sm.GetGrdStr(Grd1, Row, 14) + Environment.NewLine +
                   "Bin : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 22) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 20) !=0 && Sm.GetGrdDec(Grd1, Row, 21) < Sm.GetGrdDec(Grd1, Row, 20))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Current Quantity should be greater than Previous Quantity.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 25) != 0 && Sm.GetGrdDec(Grd1, Row, 26) < Sm.GetGrdDec(Grd1, Row, 25))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Current Quantity 2 should be greater than Previous Quantity 2.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 30) != 0 && Sm.GetGrdDec(Grd1, Row, 31) < Sm.GetGrdDec(Grd1, Row, 30))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Current Quantity 3 should be greater than Previous Quantity 3.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 19) < Sm.GetGrdDec(Grd1, Row, 22))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than DO's quantity.");
                    return true;
                }

                if (Grd1.Cols[27].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 27) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 24) < Sm.GetGrdDec(Grd1, Row, 27))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than DO's quantity (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[32].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 32) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 29) < Sm.GetGrdDec(Grd1, Row, 32))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than DO's quantity (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveDOCtHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCtHdr(DocNo, DocDt, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, RecurringInd, CtQtDocNo,  Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @CtCode, @DocNoInternal, @ResiNo, @CurCode, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, @SAPhone, @SAFax, @SAEmail, @SAMobile, 'Y', @CtQtDocNo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocNoInternal", TxtDocNoInternal.Text);
            Sm.CmParam<String>(ref cm, "@ResiNo", TxtResiNo.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", MeeSAAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCityCode);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCntCode);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@CtQtDocNo", TxtCtQtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDOCtDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, @ProcessInd, @Source, @Lot, @Bin, @QtyPackagingUnit, @PackagingUnitUomCode, @Qty, @Qty2, @Qty3, @UPrice, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 27));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 32));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 34));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 36));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            if (mIsDOCtProsessToSalesInvoice)
            {
                Sm.CmParam<String>(ref cm, "@ProcessInd", "O");
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@ProcessInd", "F");
            }
            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where (A.Qty<>0 Or A.Qty2<>0 Or A.Qty3<>0) ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('DO To Customer : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblDOCtHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Group By E.AcNo5 ");
            }
            else
            {
                SQL.AppendLine("        Select D.ParValue As AcNo, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCtHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
            SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
            SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
            SQL.AppendLine("            From TblDOCtHdr A ");
            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ) C On 0=0 ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) X1 ");
            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
            SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
            SQL.AppendLine("        Union All ");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select X.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, Sum(X.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
                SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
                SQL.AppendLine("            As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X ");
                SQL.AppendLine("        Group By X.AcNo ");
            }
            else
            {
                SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X1 ");
                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                SQL.AppendLine("        Group By X2.ParValue ");
            }

            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mIsDOCtRemarkEditable)
            {
                cml.Add(EditDOCtHdr());
            }

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 && !Sm.GetGrdBool(Grd1, Row, 2))
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                        cml.Add(EditDOCtDtl1(Row));
                    else
                        cml.Add(EditDOCtDtl2(Row));
                }
            }

            if (mIsAutoJournalActived) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDOCtDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) || 
                IsDataAlreadyUsedInRecvCt(DNo) || IsDataAlreadyUsedInSalesInvoice(DNo);
        }

        private bool IsDataAlreadyUsedInRecvCt(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo From TblRecvCtDtl");
            SQL.AppendLine("Where DOCtDocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DOCtDNo, '##') In @DNo)>0 Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);

            var key = Sm.GetValue(cm);
            if (key.Length != 0)
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0) == key)
                    {
                        Msg =
                           "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                           "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                           "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                           "Property : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                           "Batch# : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                           "Source : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                           "Lot : " + Sm.GetGrdStr(Grd1, Row, 14) + Environment.NewLine +
                           "Bin : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine + Environment.NewLine;
                        break;
                    }
                }
                Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + "Data already used in receiving from customer transactions.");
                return true;
            }
            return false;

        }

        private bool IsDataAlreadyUsedInSalesInvoice(string DNo)
        {
            if (mIsDOCtProsessToSalesInvoice)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.DOCtDno As KeyWord ");
                SQL.AppendLine("from tblSalesinvoiceHdr A ");
                SQL.AppendLine("Inner Join TblsalesinvoiceDtl B On A.DocNo = B.Docno ");
                SQL.AppendLine("Where A.cancelInd = 'N' ");
                SQL.AppendLine("And B.DOCtDocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And Position(Concat('##', DOCtDNo, '##') In @DNo)>0 Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);

                var key = Sm.GetValue(cm);
                if (key.Length != 0)
                {
                    string Msg = string.Empty;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 0) == key)
                        {
                            Msg =
                               "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                               "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                               "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                               "Property : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                               "Batch# : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                               "Source : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine +
                               "Lot : " + Sm.GetGrdStr(Grd1, Row, 14) + Environment.NewLine +
                               "Bin : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine + Environment.NewLine;
                            break;
                        }
                    }
                    Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + "Data already used in sales invoice transactions.");
                    return true;
                }
            }
            return false;

        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##")&&!mIsDOCtRemarkEditable)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditDOCtDtl1(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty*-1, Qty2*-1, Qty3*-1, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And DocType=@DocType ");
            SQL.AppendLine("And CancelInd='N';");
            
            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And DNo=@DNo ");
            SQL.AppendLine("        And CancelInd='Y' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where A.Qty<>0 ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtDtl2(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtDtl Set UPrice=@UPrice, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 34));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var SQL = new StringBuilder();
            var EntCode = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Update TblDOCtDtl Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select 1 From TblDOCtHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Replace(CurDate(), '-', ''), ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblDOCtHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                //SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Select D.ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCtHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
            SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
            SQL.AppendLine("            From TblDOCtHdr A ");
            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ) C On 0=0 ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) X1 ");
            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
            SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
            SQL.AppendLine("        Union All ");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select X.AcNo, ");
                SQL.AppendLine("        Sum(X.Amt) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
                SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
                SQL.AppendLine("            As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X ");
                SQL.AppendLine("        Group By X.AcNo ");
            }
            else
            {
                SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X1 ");
                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                SQL.AppendLine("        Group By X2.ParValue ");
            }
            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDOCtHdr(DocNo);
                ShowDOCtDtl(DocNo);
                ComputeTotalQty();
                ComputeAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOCtHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WhsCode, A.CtCode, A.DocNoInternal, A.CurCode, ");
            SQL.AppendLine("A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, A.SAPostalCd, ");
            SQL.AppendLine("A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile,  A.ResiNo, A.CtQTDocNo, A.Remark ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(), 
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "WhsCode", "CtCode", "DocNoInternal", "SAName",  
                        //6-10
                        "SAAddress", "SACityCode", "CityName", "SACntCode", "CntName",  
                        //11-15
                        "SAPostalCd", "SAPhone", "SAFax", "SAEmail", "SAMobile",   
                        //16-19
                        "CurCode", "ResiNo", "CtQtDocNo", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                        TxtDocNoInternal.EditValue = Sm.DrStr(dr, c[4]);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[5]);
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[6]);
                        mCityCode = Sm.DrStr(dr, c[7]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[8]);
                        mCntCode = Sm.DrStr(dr, c[9]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[12]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[13]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[14]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[15]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[16]));
                        TxtResiNo.EditValue = Sm.DrStr(dr, c[17]);
                        TxtCtQtDocNo.EditValue = Sm.DrStr(dr, c[18]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                    }, true
                );
        }

        private void ShowDOCtDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("IfNull(E.Qty, 0) + Case When B.CancelInd='N' Then B.Qty Else 0 End As Stock, ");
            SQL.AppendLine("IfNull(E.Qty2, 0) + Case When B.CancelInd='N' Then B.Qty2 Else 0 End As Stock2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0) + Case When B.CancelInd='N' Then B.Qty3 Else 0 End As Stock3, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.UPrice, B.Remark, C.ItGrpCode, B.QtyPackagingUnit, B.PackagingUnitUomCode, F.CtItCode,  ");
            SQL.AppendLine("G.QtyPrev, G.Qty2prev, G.Qty3Prev, (B.Qty2 * B.UPrice) As Amt  ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.ItCode=E.ItCode ");
            SQL.AppendLine("    And B.PropCode=E.PropCode ");
            SQL.AppendLine("    And B.BatchNo=E.BatchNo ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem ");
            SQL.AppendLine(")F On A.CtCode = F.CtCode And B.ItCode = F.itCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X.CtCode, X.itCode, X2.Qty As QtyPrev,  X2.Qty2 As Qty2Prev, X2.Qty3 As Qty3prev  ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    (  ");
            SQL.AppendLine("        Select MAX(Concat(A.DocDt, A.DocNo)) As KeyWord,  A.CtCode, B.ItCode  ");
            SQL.AppendLine("        From TblDoctHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where B.cancelInd = 'N' And A.CtCode = @CtCode And A.DocNo <> @DocNo ");
            SQL.AppendLine("        group by A.CtCode, B.ItCode ");
            SQL.AppendLine("    )X ");
            SQL.AppendLine("    Inner Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Concat(A.DocDt, A.DocNO) As Kode, B.ItCode, B.Qty, B.Qty2, B.Qty3  ");
            SQL.AppendLine("        From tblDocthdr A ");
            SQL.AppendLine("        Inner Join tblDoctDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Where B.cancelInd = 'N' And A.CtCode = @CtCode And A.DocNo <> @DocNo ");
            SQL.AppendLine("    )X2 On X.keyword = X2.kode And X.ItCode = X2.itCode ");
            SQL.AppendLine(")G On F.ItCode = G.ItCode And F.CtCode = G.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "ForeignName",  
                    
                    //6-10
                    "PropCode", "PropName", "CtItCode", "BatchNo", "Source",  
                    
                    //11-15
                    "Lot", "Bin", "ItGrpCode", "QtyPackagingUnit", "PackagingUnitUomCode", 
                    
                    //16-20
                    "Stock", "QtyPrev", "Qty", "InventoryUomCode", "Stock2",  
                    
                    //21-25
                    "Qty2prev", "Qty2", "InventoryUomCode2", "Stock3", "Qty3Prev",  
                    
                    //26-30
                    "Qty3", "InventoryUomCode3", "UPrice", "Amt", "Remark",  
                       
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);

                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 23);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 24);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 25);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 26);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 27);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 28);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 30);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 19, 20, 21, 22, 24, 25, 26, 27, 29, 30, 31, 32, 34, 35 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowShippingAddressData(string CtCode, string ShipName)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ShipName", ShipName);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.Address, A.CityCode, B.CityName, A.CntCode, C.CntName, A.PostalCd, A.Phone, A.Fax, A.Email, A.Mobile " +
                    "From TblCustomerShipAddress A " +
                    "Left Join TblCity B On A.CityCode = B.CityCode " +
                    "Left Join TblCountry C On A.CntCode = C.CntCode Where A.CtCode=@CtCode And A.Name=@ShipName ", 
                    new string[] 
                    { 
                        "Address", 
                        "CityCode", "CityName", "CntCode", "CntName", "PostalCd", 
                        "Phone", "Fax", "Email", "Mobile" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[0]);
                        mCityCode = Sm.DrStr(dr, c[1]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[2]);
                        mCntCode = Sm.DrStr(dr, c[3]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[7]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
                    }, false
                );
        }

        #endregion

        #region Additional Method

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += 
                            "##" + 
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            Sm.GetGrdStr(Grd1, Row, 14) +
                            Sm.GetGrdStr(Grd1, Row, 15) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 13).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 13);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 14), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 15), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 19, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 24, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 29, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ParPrint(int parValue)
        {
            
        }

        internal void ComputeTotalQty()
        {
            decimal Total = 0m;
            int col = 22;
            int col2 = 0;

            while (col <= 32)
            {
                Total = 0m;
                for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, col).Length != 0 &&
                        Sm.GetGrdStr(Grd1, row, 4).Length != 0 &&
                        !Sm.GetGrdBool(Grd1, row, 1))
                    {
                        Total += Sm.GetGrdDec(Grd1, row, col);
                    }
                }
                if (col == 22) col2 = 0;
                if (col == 27) col2 = 1;
                if (col == 32) col2 = 2;
                Grd2.Cells[0, col2].Value = Total;
                col += 5;
            }
        }

        private void ComputeTotal(int row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, row, 22).Length != 0) Qty = Sm.GetGrdDec(Grd1, row, 22);
            if (Sm.GetGrdStr(Grd1, row, 34).Length != 0) UPrice = Sm.GetGrdDec(Grd1, row, 34);
            if (mIsAmtRoundingUp)
                Grd1.Cells[row, 35].Value = Sm.FormatNum(Math.Ceiling(Qty*UPrice), 0);
            else
                Grd1.Cells[row, 35].Value = Sm.FormatNum(Qty * UPrice, 0);
            ComputeAmt();
        }

        private void ComputeAmt()
        {
            decimal Amt = 0m;
            for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                if (Sm.GetGrdStr(Grd1, row, 35).Length != 0 && !Sm.GetGrdBool(Grd1, row, 1)) Amt += Sm.GetGrdDec(Grd1, row, 35);
            if(mIsAmtRoundingUp)
                TxtAmt.EditValue = Sm.FormatNum(Math.Ceiling(Amt),0); 
            else
                TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
                ClearData2();
            }
            if (Sm.GetLue(LueCtCode).Length > 0)
            {
                TxtCtQtDocNo.EditValue = Sm.GetValue("Select DocNo From TblCtQTHdr Where CtCode = '"+Sm.GetLue(LueCtCode)+"' And ActInd = 'Y' And Status = 'A'");
            }
        }

        private void TxtDocNoInternal_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNoInternal);
        }

       

        private void LuePackagingUnitUomCode_EditValueChanged(object sender, EventArgs e)
        {
            var ItCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
            if (ItCode.Length == 0) ItCode = "XXX";
        }
      
        #endregion

        private void BtnCustomerShipAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmDOCt5Dlg2(this, Sm.GetLue(LueCtCode)));
        }

        private void BtnCtQtDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCtQt(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtCtQtDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnSAName_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion

     
    }

}
