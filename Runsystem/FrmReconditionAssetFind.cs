﻿#region Update
/*
    24/01/2022 [IBL/Product] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmReconditionAssetFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmReconditionAsset mFrmParent;
        private string mSQL = string.Empty;


        #endregion

        #region Constructor

        public FrmReconditionAssetFind(FrmReconditionAsset FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ReconditionValue, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As Status, B.AssetCode, B.AssetName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblReconditionAssetHdr A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
            Grd1, new string[]
            {
                //0
                "No.",

                //1-5
                "Document#",
                "Date",
                "Cancel",
                "Recondition Value",
                "Status",

                //6-10
                "Asset's Code",
                "Asset's Name",
                "Created By",
                "Created"+Environment.NewLine+"Date",
                "Created"+Environment.NewLine+"Time",

                //11-12
                "Last"+Environment.NewLine+"Updated By",
                "Last"+Environment.NewLine+"Updated Date",
                "Last"+Environment.NewLine+"Updated Time"
            },
            new int[]
            {
                //0
                50,

                //1-5
                150, 150, 50, 200, 150,

                //6-10
                150, 200, 100, 100, 100,

                //11-12
                100, 100, 100
            }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL+ Filter + " Order By A.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "CancelInd", "ReconditionValue", "Status", "AssetCode", 
                            
                            //6
                            "AssetName", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 10);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Event

        #region Misc Control Event

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }
        #endregion

        #endregion
    }
}
