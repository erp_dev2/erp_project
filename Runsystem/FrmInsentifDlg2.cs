﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInsentifDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmInsentif mFrmParent;
        private string mSQL = string.Empty;
        private string mPWGDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmInsentifDlg2(FrmInsentif FrmParent, string PWGDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPWGDocNo = PWGDocNo;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, A.DocNo, B.EmpCode, C.EmpCodeOld, C.EmpName, G.DeptName,  D.PosName, ");
            SQL.AppendLine("A.WorkcenterDocNo, F.DocName, E.ParValue As MinWages, ");
            SQL.AppendLine("B.Wages*B.HolidayWagesindex As AmtWages  ");
            SQL.AppendLine("From TblPwgHdr A ");
            SQL.AppendLine("Inner Join TblPwgdtl5 B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode And C.ResignDt Is Null  ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode  ");
            SQL.AppendLine("Left Join TblParameter E On E.ParCode='MinimumDailyWages' ");
            SQL.AppendLine("Inner Join tblWorkcenterHdr F On A.WorkcenterDocNo = F.DocNo  ");
            SQL.AppendLine("Inner Join TblDepartment G On C.DeptCode = G.DeptCode ");
            SQL.AppendLine("Where A.DocNo='" + mPWGDocNo + "' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Production Wages",
                        "Employee Code", 
                        "Employee Old Code",
                        "Employee Name",

                        //6-10
                        "Department",
                        "Position", 
                        "Workcenter",
                        "Workcenter Name",
                        "Minimum Wages",

                        //11
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        20, 150, 100, 100, 250,   
                        
                        //6-10
                        200, 180, 120, 150, 150,

                        //11
                        150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] {10, 11}, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4}, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3,4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
           

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "C.EmpCodeOld", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[]
                        {
                            //0 
                            "Dno", 
                            //1-5
                            "DocNo", "EmpCode", "EmpCodeOld", "EmpName", "DeptName",  
                            //6-10
                            "PosName", "WorkcenterDocNo", "DocName", "MinWages", "AmtWages" 
                           
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Grd.Cells[Row, 1].Value = ChkAutoChoose.Checked;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7); 
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 11);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 5, 6, 7 });
                    }
                }
                mFrmParent.ComputeInsentif();
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        private bool IsCodeAlreadyChosen(int Row)
        {
            string EmpCode = Sm.GetGrdStr(Grd1, Row, 3);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), EmpCode))
                    return true;
            return false;
        }


        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }
       

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

     
        #endregion
             
    }
}
