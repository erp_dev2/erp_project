﻿#region Update
/*
    19/12/2017 [HAR] tambah cancel reason dan validasi jia beda site berdasarkan parameter IsRecvValidateBySite
    13/03/2018 [TKG] tambah import indicator dari PO
    19/03/2018 [TKG] konversi tidak jalan
    29/05/2018 [TKG] tambah status PO.
    02/09/2018 [TKG] Menampilkan nomor urut berdasarkan parameter
    05/11/2018 [WED] tambah kolom ProjectCode
    13/11/2018 [DITA] inventory perubahan proses receiving untuk jasa
    25/02/2019 [TKG] tambah informasi kawasan berikat
    04/11/2019 [DITA/IMS] tambah informasi Specifications & ItCodeInternal
    28/11/2019 [WED/IMS] tambah kolom project code, project name, customer po# berdasarkan parameter IsBOMShowSpecifications
    09/01/2020 [WED/MMM] panggil function untuk autogenerate batchno
    24/01/2020 [WED/YK] item nya khusus service dan lihat ke Cost center, jika mMenuCodeForRecvVdAutoDO true
    30/01/2020 [TKG/IMS] Berdasarkan parameter IsRecvVdBatchNoUseProjectCode, batch# diisi kode proyek.
    26/03/2020 [TKG/IMS] Receiving item from vendor bisa memproses data receiving expedition secara partial.
    02/10/2020 [TKG/IMS] receiving expedition ketika diproses di recv vd sisanya akan hangus kalau tidak diproses, dan harus diproses kembali di recv expedition
    07/10/2020 [TKG/IMS] merubah logic filter by site
    14/12/2020 [WED/IMS] ambil data receiving expedition yang approved
    22/02/2021 [VIN/SIER] Jika parameter IsPOUseContract -> PO yang ditampilkan sesuai contract yang dipilih di main form 
    24/02/2021 [WED/SIER] BUG salah nangkep goal task. hanya menampilkan informasi PO Contract.
    03/03/2021 [WED/IMS] Lot ambil dari Expedition
    04/04/2021 [WED/IMS] expedition masih bisa ditarik
 *  31/05/2021 [HAR/IMS] saat pilih item bisa double klik header utk pilih semua item
    14/07/2021 [WED/PADI] PO yang ditarik hanya yang outstanding dan partial (karena ada PO yang cancel order dari web e-catalog)
    06/08/2021 [VIN/IMS] Tambah Informasi PO for Service 
    18/10/2021 [WED/IMS] kolom readonly untuk Document# terakhir
    25/10/2021 [WED/RM] TakeOrder tidak jadi dipakai di PO, tapi PO Request
    11/01/2022 [ISD/PHT] Kolom Item terfilter berdasar group login dengan parameter IsFilterByItCt
    27/01/2022 [RIS/PHT] Membuat Outstanding quantity dapat mengambil nilai dari tab revision dengan param mIsRecvVdUsePORevision
    24/03/2022 [TRI/PHT] bug saat refresh data dan ada filter docno PO nya
    08/07/2021 [VIN/PHT] Mengganti judul Dlg menjdai List of PO

*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVdDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvVd mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty, 
            mSiteCode = string.Empty, 
            mRecvExpeditionDocNo = string.Empty;
        private bool mIsUseECatalog = false;

        #endregion

        #region Constructor

        public FrmRecvVdDlg(FrmRecvVd FrmParent, string VdCode, string SiteCode, string RecvExpeditionDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
            mSiteCode = SiteCode;
            mRecvExpeditionDocNo = RecvExpeditionDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = ""+ mFrmParent.mMenuCode + "-List of PO";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -360);
                mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
                mIsUseECatalog = Sm.GetParameterBoo("IsUseECatalog");
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 41;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Document#",
                    "DNo", 
                    "Date",
                    "Item's Code", 

                    //6-10
                    "", 
                    "Item's Name", 
                    "Outstanding"+Environment.NewLine+"Quantity",
                    "UoM"+Environment.NewLine+"(Purchase)",
                    "UoM"+Environment.NewLine+"(Inventory)",

                    //11-15
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "PO's Remark",
                    "Department",
                    "ItScCode",

                    //16-20
                    "Sub-Category",
                    "Source",
                    "Local#",
                    "Foreign Name",
                    "Import",

                    //21-25
                    "ProjectCode", 
                    "Item's Local Code",
                    "Specification",
                    "Project's Code",
                    "Project's Name",

                    //26-30
                    "Customer's PO#",
                    "Cost Category Code",
                    "Cost Category Name",
                    "LOP#",
                    "Inventory COA#",

                    //31-35
                    "Expedition's Received#",
                    "Expedition's Received DNo",
                    "Exp's Outstanding"+Environment.NewLine+"Quantity (Purchase)",
                    "Exp's Outstanding"+Environment.NewLine+"Quantity (Inventory)",
                    "Exp's Outstanding"+Environment.NewLine+"Quantity (Inventory)",

                    //36-40
                    "Exp's Outstanding"+Environment.NewLine+"Quantity (Inventory)",
                    "PO Contract",
                    "Lot",
                    "Bin",
                    "Document#",
                }
            );

            if (mFrmParent.mIsPOUseContract)
            {
                Grd1.Cols[37].Move(5);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 37 });

            Sm.GrdColCheck(Grd1, new int[] { 1, 20 });
            Sm.GrdColButton(Grd1, new int[] { 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 33, 34, 35, 36 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 32, 38, 39, 2 });
            if (!mFrmParent.mMenuCodeForRecvVdAutoCreateDO)
                Sm.GrdColInvisible(Grd1, new int[] { 27, 28, 29, 30 });
            if (mFrmParent.mIsShowForeignName)
            {
                Grd1.Cols[19].Move(8);
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 15, 16, 17 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 15, 17 }, false);
                    Grd1.Cols[16].Move(9);
                }
            }
            else
            {
                if (Sm.GetParameter("ProcFormatDocNo") == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 15, 16, 17, 19 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 10, 11, 12, 15, 17, 19 }, false);
                    Grd1.Cols[16].Move(9);
                }
            }
            Grd1.Cols[18].Move(3);
            Grd1.Cols[20].Move(6);
            if (!mFrmParent.mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 18 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 23, 24, 25, 26 });
            Grd1.Cols[22].Move(7);

            if (mFrmParent.mIsRecvExpeditionEnabled)
                Grd1.Cols[33].Move(11);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 31, 33 });
            Sm.GrdColInvisible(Grd1, new int[] { 34, 35, 36 });

            Sm.SetGrdProperty(Grd1 , true);
            Grd1.Cols[40].Move(2);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 22 }, !ChkHideInfoInGrd.Checked);
            if (!mFrmParent.mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 18 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.DocNo, B.DNo, A.ImportInd, A.DocNoSource, A.DocDt, G.DeptName, D.ItCode, E.ItName, E.ForeignName, ");
            SQL.AppendLine("    E.PurchaseUomCode, E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            SQL.AppendLine("    Trim(Concat(IfNull(A.Remark, ''), ' ', IfNull(B.Remark, ''))) As PORemark, ");
            if (mFrmParent.mIsRecvVdUsePORevision)
                SQL.AppendLine("    (IfNull(Q.Qty, B.Qty)-IfNull(H.Qty2, 0)-IfNull(I.Qty3, 0)+IfNull(J.Qty4, 0)) As OutstandingQty, E.ItScCode, K.ItScName, ");
            else
                SQL.AppendLine("    (B.Qty-IfNull(H.Qty2, 0)-IfNull(I.Qty3, 0)+IfNull(J.Qty4, 0)) As OutstandingQty, E.ItScCode, K.ItScName, ");
            SQL.AppendLine("    A.LocalDocNo, L.ProjectCode, E.ItCodeInternal, E.Specification, ");
            SQL.AppendLine("    N.ProjectCode As ProjectCode2, N.ProjectName, M.PONo, IfNull(E4.DocNo, A.DocNo) PODocNo, ");
            if (mFrmParent.mIsPOUseContract)
                SQL.AppendLine("   P.OptDesc As POContract, ");
            else
                SQL.AppendLine("    Null As POContract, ");
            if (mFrmParent.mMenuCodeForRecvVdAutoCreateDO)
                SQL.AppendLine("    E3.CCtCode, E3.CCtName, F5.LOPDocNo, E1.AcNo, ");
            else
                SQL.AppendLine("    null As CCtCode, null as CCtName, null as LOPDocNo, null As AcNo, ");
            if (mRecvExpeditionDocNo.Length > 0)
            {
                SQL.AppendLine("O.Lot, O.Bin, ");
                SQL.AppendLine("O.DocNo As RecvExpeditionDocNo, O.DNo As RecvExpeditionDNo, ");
                SQL.AppendLine("O.QtyPurchase-O.RecvVdQtyPurchase As RecvExpeditionQtyPurchase, ");
                SQL.AppendLine("O.Qty-O.RecvVdQty As RecvExpeditionQty, ");
                SQL.AppendLine("O.Qty2-O.RecvVdQty2 As RecvExpeditionQty2, ");
                SQL.AppendLine("O.Qty3-O.RecvVdQty3 As RecvExpeditionQty3 ");
            }
            else
            {
                SQL.AppendLine("Null As Lot, Null As Bin, ");
                SQL.AppendLine("Null As RecvExpeditionDocNo, Null As RecvExpeditionDNo, ");
                SQL.AppendLine("0.00 As RecvExpeditionQtyPurchase, ");
                SQL.AppendLine("0.00 As RecvExpeditionQty, ");
                SQL.AppendLine("0.00 As RecvExpeditionQty2, ");
                SQL.AppendLine("0.00 As RecvExpeditionQty3 ");
            }
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd In ('O','P') ");
            //if (mIsUseECatalog)
            //    SQL.AppendLine("    And B.TakeOrderInd = 'Y' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode ");
            if(mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("    And (E.ItCtCode Is Null Or ( ");
                SQL.AppendLine("    E.ItCtCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("        Where ItCtCode=IfNull(E.ItCtCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (!mFrmParent.mMenuCodeForRecvVdAutoCreateDO)
            {
                if (mFrmParent.TxtRecvExpeditionDocNo.Text.Length==0)
                    SQL.AppendLine("        And E.ServiceItemInd = 'N' ");
            }
            else
            {
                SQL.AppendLine("        And E.ServiceItemInd = 'Y' ");
                SQL.AppendLine("    Inner Join TblItemCategory E1 On E.ItCtCode=E1.ItCtCode ");
                SQL.AppendLine("    Left Join TblItemCostCategory E2 On E.ItCode=E2.ItCode And E2.CCCode=@CCCode ");
                SQL.AppendLine("    Left Join TblCostCategory E3 On E2.CCCode=E3.CCCode And E2.CCtCode=E3.CCtCode ");
            }
            SQL.AppendLine("    Left Join TblMaterialRequestDtl E4 On C.MaterialRequestDocNo=E4.DocNo And C.MaterialRequestDNo=E4.DNo And E4.MaterialRequestServiceDocNo is Not Null");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr F On D.DocNo=F.DocNo ");
            //if (mFrmParent.mIsFilterBySite)
            //{
            //    SQL.AppendLine("    And (F.SiteCode Is Null Or ( ");
            //    SQL.AppendLine("    F.SiteCode Is Not Null ");
            //    SQL.AppendLine("    And Exists( ");
            //    SQL.AppendLine("        Select 1 From TblGroupSite ");
            //    SQL.AppendLine("        Where SiteCode=IfNull(F.SiteCode, '') ");
            //    SQL.AppendLine("        And GrpCode In ( ");
            //    SQL.AppendLine("            Select GrpCode From TblUser ");
            //    SQL.AppendLine("            Where UserCode=@UserCode ");
            //    SQL.AppendLine("            ) ");
            //    SQL.AppendLine("        ) ");
            //    SQL.AppendLine(")) ");
            //}
            if (mFrmParent.mMenuCodeForRecvVdAutoCreateDO)
            {
                SQL.AppendLine("    LEFT JOIN TblDroppingRequestHdr F1 ON F.DroppingRequestDocNo = F1.DocNo ");
                SQL.AppendLine("    LEFT JOIN TblProjectImplementationHdr F2 ON F1.PRJIDocno = F2.DocNo ");
                SQL.AppendLine("    LEFT JOIN TblSOContractRevisionHdr F3 ON F2.SOContractDocNo = F3.DocNo ");
                SQL.AppendLine("    LEFT JOIN TblSOContractHdr F4 ON F3.SOCDocNo = F4.DocNo ");
                SQL.AppendLine("    LEFT JOIN TblBOQHdr F5 ON F4.BOQDocNo = F5.DocNo ");
            }
            SQL.AppendLine("    Inner Join TblDepartment G On F.DeptCode=G.DeptCode ");
            SQL.AppendLine("    Left Join ( ");
		    SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As Qty2 ");
		    SQL.AppendLine("        From TblRecvVdDtl T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            if (mFrmParent.TxtRecvExpeditionDocNo.Text.Length > 0)
                SQL.AppendLine("            And T1.RejectedInd = 'N' ");
            SQL.AppendLine("        Where T1.CancelInd='N' Group By T1.PODocNo, T1.PODNo ");
	        SQL.AppendLine("    ) H On A.DocNo=H.DocNo And B.DNo=H.DNo ");
	        SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
            SQL.AppendLine("        From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) I On A.DocNo=I.DocNo And B.DNo=I.DNo  ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T4.DocNo, T4.DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty4 ");
            SQL.AppendLine("        From TblDOVdHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo And T3.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblPODtl T4 On T3.PODocNo=T4.DocNo And T3.PODNo=T4.DNo And T4.CancelInd='N' And T4.ProcessInd<>'F' ");
            SQL.AppendLine("        Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("        Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine("    ) J On A.DocNo=J.DocNo And B.DNo=J.DNo ");
            SQL.AppendLine("    left Join TblItemSubcategory K On E.ItScCode = K.ItScCode ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X1.DocNo, X3.ProjectCode ");
            SQL.AppendLine("        From TblLOPHdr X1 ");
            SQL.AppendLine("        Inner Join TblBOQHdr X2 On X1.DocNo = X2.LOPDocNo ");
            SQL.AppendLine("        Inner Join TblSOContractHdr X3 On X2.DocNo = X3.BOQDocNo And X3.Status = 'A' And X3.CancelInd = 'N' ");
            SQL.AppendLine("    ) L On B.LOPDocNo = L.DocNo ");
            SQL.AppendLine("    Left Join TblSOContractHdr M ON F.SOCDocNo=M.DocNo ");
            SQL.AppendLine("    Left Join TblProjectGroup N On F.PGCode=N.PGCode ");
            if (mRecvExpeditionDocNo.Length > 0)
            {
                SQL.AppendLine("    Inner Join TblRecvExpeditionDtl O ");
                SQL.AppendLine("        On B.DocNo=O.PODocNo ");
                SQL.AppendLine("        And B.DNo=O.PODNo ");
                SQL.AppendLine("        And O.CancelInd='N' ");
                SQL.AppendLine("        And O.Status = 'A' ");
                SQL.AppendLine("        And O.DocNo=@RecvExpeditionDocNo ");
                SQL.AppendLine("        And Not Exists( ");
                SQL.AppendLine("            Select 1 ");
                SQL.AppendLine("            From TblRecvVdHdr X1, TblRecvVdDtl X2 ");
                SQL.AppendLine("            Where X1.DocNo=X2.DocNo ");
                SQL.AppendLine("            And X2.CancelInd='N' And X2.Status In ('O', 'A') ");
                SQL.AppendLine("            And X1.RecvExpeditionDocNo Is Not Null ");
                SQL.AppendLine("            And X1.RecvExpeditionDocNo=@RecvExpeditionDocNo ");
                SQL.AppendLine("            And X2.RecvExpeditionDocNo=@RecvExpeditionDocNo ");
                SQL.AppendLine("            And X2.RecvExpeditionDNo=O.DNo ");
                SQL.AppendLine("            And X2.RejectedInd = 'N' ");
                SQL.AppendLine("        ) ");
            }
            if (mFrmParent.mIsPOUseContract)
                SQL.AppendLine("    Left Join TblOPtion P On P.OPtCode = A.Contract And P.OptCat = 'POContract' ");
            if (mFrmParent.mIsRecvVdUsePORevision)
            {
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("    SELECT A.DocNo AS PODoc, A.DNo AS PODNo, B.DocNo AS POREV, B.Qty, B.`Status` ");
                SQL.AppendLine("    FROM tblpodtl A ");
                SQL.AppendLine("    INNER JOIN tblporevision B ON A.DocNo = B.PODocNo AND A.DNo = B.PODNo AND B.status = 'A' ");
                SQL.AppendLine("    WHERE B.DocNo = (SELECT MAX(docno) FROM tblporevision WHERE podocno = B.podocno AND status = 'A') ");
                SQL.AppendLine(") Q ON B.DocNo = Q.PODoc AND B.DNo = Q.PODNo ");
            }
            SQL.AppendLine("    Where A.VdCode=@VdCode ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.Status='A' ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And A.SiteCode=@SiteCode ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.TxtKBContractNo.Text.Length > 0)
            {
                SQL.AppendLine("    And A.LocalDocNo Is Not Null And A.LocalDocNo=@KBContractNo ");
            }
            SQL.AppendLine(") X ");
            if (!mFrmParent.mIsRecvVdUsePORevision)
                SQL.AppendLine("Where X.OutstandingQty>0 ");
            else
                SQL.AppendLine("Where 1=1 ");
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@KBContractNo", mFrmParent.TxtKBContractNo.Text);
                Sm.CmParam<String>(ref cm, "@RecvExpeditionDocNo", mRecvExpeditionDocNo);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                if (mFrmParent.mMenuCodeForRecvVdAutoCreateDO) Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(mFrmParent.LueCCCode));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "X.DocNo", "X.PODocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "X.ItCode", "X.ItName", "X.ForeignName", "X.ItCodeInternal" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By X.DocDt, X.DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",
                        
                        //1-5
                        "DNo", "DocDt", "ItCode", "ItName", "OutstandingQty", 
                        
                        //6-10
                        "PurchaseUomCode", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3", "PORemark",
                        
                        //11-15
                        "DeptName", "ItScCode", "ItScName", "DocNoSource", "LocalDocNo",

                        //16-20
                        "ForeignName", "ImportInd", "ProjectCode", "ItCodeInternal", "Specification",

                        //21-25
                        "ProjectCode2", "ProjectName", "PONo", "CCtCode", "CCtName",

                        //26-30
                        "LOPDocNo", "AcNo", "RecvExpeditionDocNo", "RecvExpeditionDNo", "RecvExpeditionQtyPurchase", 

                        //31-35
                        "RecvExpeditionQty", "RecvExpeditionQty2", "RecvExpeditionQty3", "POContract", "Lot", 

                        //36-37
                        "Bin", "PODocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                        if (mFrmParent.mMenuCodeForRecvVdAutoCreateDO)
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                        }
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 29);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 30);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 31);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 32);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 33);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 34);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 35);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 36);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 37);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 21);
                        if (mFrmParent.mIsRecvVdBatchNoUseProjectCode)
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 23);
                        if (mFrmParent.mMenuCodeForRecvVdAutoCreateDO)
                        {
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 27);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 28);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 38, Grd1, Row2, 29);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 39, Grd1, Row2, 30);
                        }
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 40, Grd1, Row2, 31);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 41, Grd1, Row2, 32);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 42, Grd1, Row2, 33);
                        if (mFrmParent.mIsRecvExpeditionEnabled)
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 33);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 43, Grd1, Row2, 34);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 44, Grd1, Row2, 35);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 45, Grd1, Row2, 36);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 48, Grd1, Row2, 37);

                        if (mFrmParent.TxtRecvExpeditionDocNo.Text.Length > 0)
                        {
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 38);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 39);
                        }
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 50, Grd1, Row2, 40);

                        //mFrmParent.Grd1.Cells[Row1, 0].Value = null;
                        //mFrmParent.Grd1.Cells[Row1, 1].Value = false;
                        //mFrmParent.Grd1.Cells[Row1, 2].Value = false;
                        //mFrmParent.Grd1.Cells[Row1, 10].Value = null;
                        //mFrmParent.Grd1.Cells[Row1, 11].Value = null;
                        //mFrmParent.Grd1.Cells[Row1, 12].Value = null;
                        //mFrmParent.Grd1.Cells[Row1, 13].Value = null;

                        if (!mFrmParent.mIsRecvVdBatchNoUseProjectCode)
                            Sm.SetGrdStringValueEmpty(mFrmParent.Grd1, Row1, new int[] { 10 });
                        if (mFrmParent.TxtRecvExpeditionDocNo.Text.Length <= 0) Sm.SetGrdStringValueEmpty(mFrmParent.Grd1, Row1, new int[] { 0, 11, 12, 13 });
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, Row1, new int[] { 1, 2 });

                        if (mFrmParent.mIsRecvExpeditionEnabled)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 10)))
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 33);
                            else
                                mFrmParent.Grd1.Cells[Row1, 16].Value = 0m;

                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 11)))
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 33);
                            else
                                mFrmParent.Grd1.Cells[Row1, 18].Value = 0m;

                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 12)))
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 33);
                            else
                                mFrmParent.Grd1.Cells[Row1, 20].Value = 0m;
                        }
                        else
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 10)))
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 8);
                            else
                                mFrmParent.Grd1.Cells[Row1, 16].Value = 0;

                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 11)))
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 8);
                            else
                                mFrmParent.Grd1.Cells[Row1, 18].Value = 0;

                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 12)))
                                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 8);
                            else
                                mFrmParent.Grd1.Cells[Row1, 20].Value = 0;
                        }
                        if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row2, 9), Sm.GetGrdStr(Grd1, Row2, 10)))
                        {
                            Sm.ComputeQtyBasedOnConvertionFormula("12", mFrmParent.Grd1, Row1, 8, 16, 18, 20, 17, 19, 21);
                            Sm.ComputeQtyBasedOnConvertionFormula("13", mFrmParent.Grd1, Row1, 8, 16, 20, 18, 17, 21, 19);
                        }

                        mFrmParent.Grd1.Cells[Row1, 22].Value = null;
                        mFrmParent.ShowPOInfo(Row1);

                        if (mFrmParent.mIsAutoGenerateBatchNo &&
                            mFrmParent.mIsAutoGenerateBatchNoEditable &&
                            mFrmParent.mIsRecvVdUseOptionBatchNoFormula)
                        {
                            mFrmParent.GenerateBatchNo(Row1);
                        }

                        mFrmParent.Grd1.Rows.Add();
                        int r = mFrmParent.Grd1.Rows.Count - 1;
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, r, new int[] { 14, 16, 18, 20, 42, 43, 44, 45 });
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, r, new int[] { 1, 2 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");

            mFrmParent.SetSeqNo();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
