﻿#region Update
/*
    10/09/2018 [HAR] jumlah kolomnya jadi in 7
    19/09/2018 [TKG] difilter otorisasi berdasarkan site di grup
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptTrainingSchedule : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmRptTrainingSchedule(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                SetGrd(Sm.GetLue(LueMth));
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private void SetGrd(string mth)
        {
            Grd1.Cols.Count = 7;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-6
                        "",
                        "",
                        "", 
                        "",
                        "",
                        ""
                    },
                    new int[] 
                    {
                        //0
                        150,

                        //1-6
                        180, 180, 180, 180, 180, 180
                    }
                );
            int dt =0;
            for (int Ro = 0; Ro < 5; Ro++)
            {
                Grd1.Rows.Add();
                Grd1.Rows[Ro].Height = 120;
                for(int co = 0; co < Grd1.Cols.Count; co++)
                {
                    if (mth == "01" || mth == "03" || mth == "05" || mth == "07" || mth == "08" || mth == "10" || mth == "12")
                    {
                        if (dt < 31)
                        {
                            Grd1.Cells[Ro, co].Value = dt + 1;
                            dt = dt + 1;
                        }
                    }
                    if (mth == "04" || mth == "06" || mth == "09" || mth == "11")
                    {
                        if (dt < 30)
                        {
                            Grd1.Cells[Ro, co].Value = dt + 1;
                            dt = dt + 1;
                        }
                    }

                    if (mth == "02")
                    {
                        if (dt < 29)
                        {
                            Grd1.Cells[Ro, co].Value = dt + 1;
                            dt = dt + 1;
                        }
                    }
                }
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);

        }

        override protected void ShowData()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);
                string Yr = Sm.GetLue(LueYr), Mth = Sm.GetLue(LueMth);
                int Temp = 0;

                if (Yr.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                    return;
                }

                if (Mth.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
                    return;
                }

                SetGrd(Mth);
                var LSI = new List<L1>();
                LSI.Clear();
                Process1(ref LSI);
                Grd1.BeginUpdate();
                if (LSI.Count > 0)
                {
                    for (var i = Temp; i < LSI.Count; i++)
                    {
                        int ddy = Convert.ToInt32(LSI[i].Dy);
                        if (ddy <= 7)
                        {
                            int Colx = ddy;
                            string RowText = Sm.GetGrdStr(Grd1, 0, Colx-1);
                            Grd1.Cells[0, Colx-1].Value = RowText + Environment.NewLine + LSI[i].Jadwal;
                            Grd1.Cells[0, Colx-1].BackColor = Color.MediumSlateBlue;
                        }
                        if (ddy > 7 && ddy <= 14)
                        {
                            int Colx = ddy - 7;
                            string RowText = Sm.GetGrdStr(Grd1, 1, Colx - 1);
                            Grd1.Cells[1, Colx - 1].Value = RowText + Environment.NewLine + LSI[i].Jadwal;
                            Grd1.Cells[1, Colx - 1].BackColor = Color.MediumSlateBlue;
                        }
                        if (ddy > 14 && ddy <= 21)
                        {
                            int Colx = ddy - 14;
                            string RowText = Sm.GetGrdStr(Grd1, 2, Colx - 1);
                            Grd1.Cells[2, Colx - 1].Value = RowText + Environment.NewLine + LSI[i].Jadwal;
                            Grd1.Cells[2, Colx - 1].BackColor = Color.MediumSlateBlue;
                        }
                        if (ddy > 21 && ddy <= 28)
                        {
                            int Colx = ddy - 21;
                            string RowText = Sm.GetGrdStr(Grd1, 3, Colx - 1);
                            Grd1.Cells[3, Colx - 1].Value = RowText + Environment.NewLine + LSI[i].Jadwal;
                            Grd1.Cells[3, Colx - 1].BackColor = Color.MediumSlateBlue;
                        }
                        if (ddy > 28)
                        {
                            int Colx = ddy - 28;
                            string RowText = Sm.GetGrdStr(Grd1, 4, Colx - 1);
                            Grd1.Cells[4, Colx - 1].Value = RowText + Environment.NewLine + LSI[i].Jadwal;
                            Grd1.Cells[4, Colx - 1].BackColor = Color.MediumSlateBlue;
                        }
                    }
                }
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<L1> LP1)
        {
            LP1.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string Filter = "And 0=0";

            string Yr = Sm.GetLue(LueYr), Mth = Sm.GetLue(LueMth);

            if (Yr.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
                return;
            }

            if (Mth.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
                return;
            }

            Sm.CmParam<String>(ref cm, "@Period", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
          
            SQL.AppendLine("Select X.Dt, X.SiteCode, Right(dt, 2)As Dy, group_concat(X.Class, ' - ',  X.trainingname Separator '\n' ) As jadwal ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select B.Dt, C.SiteCode, D.Class, C.trainingname  ");
            SQL.AppendLine("    From TblTrainingSchhdr A ");
            SQL.AppendLine("    Inner Join TbltrainingSchDtl B On A.DocNo = b.DoCno ");
            SQL.AppendLine("    Inner Join Tbltraining C On A.TrainingCode = C.trainingCode  ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Inner Join TbltrainingDtl D On B.TrainingCode = D.TrainingCode And B.TrainingDno = D.Dno ");
            SQL.AppendLine("    Where A.Status = 'A' And  B.Schind = 'Y' ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Where Left(X.Dt, 6)=@Period  ");
            if (Sm.GetLue(LueSiteCode).Length > 0)
            {
                SQL.AppendLine("And X.SiteCode=@SiteCode ");
            }
            SQL.AppendLine("Group by X.Dt, X.SiteCode  ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString() + Filter + " order by Right(dt, 2)";

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Dt",  
                    //1-5
                    "SiteCode", "Dy", "jadwal"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LP1.Add(new L1()
                        {
                            Dt = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1]),
                            Dy = Sm.DrStr(dr, c[2]),
                            Jadwal = Sm.DrStr(dr, c[3]),
                            
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
        
        #region Class

        private class L1
        {
            public string Dt { get; set; }
            public string SiteCode { get; set; }
            public string Dy { get; set; }
            public string Jadwal { get; set; }
        }

        #endregion
    }
}
