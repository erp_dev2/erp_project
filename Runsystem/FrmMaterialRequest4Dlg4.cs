﻿#region Update
/*
    06/04/2020 [WED/SRN] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest4Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest4 mFrmParent;
        private string mSQL = string.Empty,
            mDeptCode = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequest4Dlg4(FrmMaterialRequest4 FrmParent, string DeptCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDeptCode = DeptCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "Date",
                        "Item's Code",
                        "",

                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Remark",
                        "Item's Code"+Environment.NewLine+"Internal",
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 20, 80, 100, 20,
                        
                        //6-10
                        200, 200, 200, 100, 300
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 9 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 10 });
            Grd1.Cols[9].Move(6);
            Grd1.Cols[10].Move(8);

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.DocNo, A.DocDt, B.ItCode, C.ItName, C.ForeignName, B.Remark ");
            //SQL.AppendLine("From TblDORequestDeptHdr A ");
            //SQL.AppendLine("Inner Join TblDORequestDeptDtl B ON A.DocNo = B.DocNo And B.ProcessInd = 'O' ");
            //SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode And C.ActInd = 'Y' ");
            //SQL.AppendLine("Inner Join  ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("  Select ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            //SQL.AppendLine("  From TblStockSummary ");
            //SQL.AppendLine("  Group By ItCode ");
            ////SQL.AppendLine("  Having Sum(Qty) = 0 Or Sum(Qty2) = 0 Or Sum(Qty3) = 0 ");
            //SQL.AppendLine(") D On B.ItCode = D.ItCode ");
            //SQL.AppendLine("Where A.WODocNo Is Null ");
            //SQL.AppendLine("And A.DeptCode = @DeptCode ");
            //SQL.AppendLine("And B.CancelInd = 'N'  ");
            //SQL.AppendLine("And B.Status = 'A' ");
            ////SQL.AppendLine("And ( B.Qty > D.Qty Or B.Qty2 > D.Qty2 Or B.Qty3 > D.Qty3 ) ");
            //SQL.AppendLine("And Not Exists ( ");
            //SQL.AppendLine("  Select 1 ");
            //SQL.AppendLine("  From TblMaterialRequestDtl ");
            //SQL.AppendLine("  Where DORequestDocNo Is Not Null ");
            //SQL.AppendLine("  And DORequestDNo Is Not Null ");
            //SQL.AppendLine("  And DORequestDocNo = A.DocNo ");
            //SQL.AppendLine(") ");

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.ItCode, C.ItName, C.ForeignName, B.Remark, C.ItCodeInternal, C.Specification ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.ProcessInd='O' ");
            SQL.AppendLine("    And B.CancelInd='N'  ");
            SQL.AppendLine("    And B.Status='A' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.ActInd='Y' ");
            SQL.AppendLine("Where A.WODocNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.DeptCode=@DeptCode ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblMaterialRequestDtl ");
            SQL.AppendLine("    Where DORequestDocNo Is Not Null ");
            SQL.AppendLine("    And DORequestDocNo = A.DocNo ");
            SQL.AppendLine("    ) ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItName", "C.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                             
                            //1-5
                            "DocDt", 
                            "ItCode", 
                            "ItName", 
                            "ForeignName", 
                            "Remark",

                            //6-7
                            "ItCodeInternal",
                            "Specification"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtDORequestDocNo.EditValue = string.Empty;
                mFrmParent.Grd1.Rows.Count = 1;
                mFrmParent.TxtDORequestDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ShowDORequestItem(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDORequestDept(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmDORequestDept(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
