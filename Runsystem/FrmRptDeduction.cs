﻿#region Update
/*
    26/03/2018 [TKG] tambah informasi fixed deduction
    25/04/2018 [TKG] bug reporting
    21/05/2018 [TKG] bug reporting
    24/01/2019 [HAR] tambah informasi section, dplk, net income dan transfer
    05/04/2019 [TKG] resignee tidak dimunculkan
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDeduction : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptDeduction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSectionCode(ref LueSection);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select 'Loan' As Type, A.EmpCode, D.EmpName, D.EmpCodeOld, ");
            SQL.AppendLine("A.Yr, A.PayrunCode, ");
            SQL.AppendLine("Case A.Mth ");
            SQL.AppendLine("    When '01' Then 'January' ");
            SQL.AppendLine("    When '02' Then 'February' ");
            SQL.AppendLine("    When '03' Then 'March' ");
            SQL.AppendLine("    When '04' Then 'April' ");
            SQL.AppendLine("    When '05' Then 'May' ");
            SQL.AppendLine("    When '06' Then 'June' ");
            SQL.AppendLine("    When '07' Then 'July' ");
            SQL.AppendLine("    When '08' Then 'August' ");
            SQL.AppendLine("    When '09' Then 'September' ");
            SQL.AppendLine("    When '10' Then 'October' ");
            SQL.AppendLine("    When '11' Then 'November' ");
            SQL.AppendLine("    When '12' Then 'December' End As Mth, ");
            SQL.AppendLine("D.SiteCode, E.SiteName, D.DeptCode, F.DeptName, G.PosName, H.CreditName, A.Amt, D.SectionCode, J.SectionName ");
            SQL.AppendLine("From TblAdvancePaymentProcess A ");
            SQL.AppendLine("Inner Join TblAdvancePaymentHdr B On A.DocNo=B.DocNo And A.EmpCode=B.EmpCode And B.Status='A' And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblAdvancePaymentDtl C On B.DocNo=C.DocNo And C.Yr=@Yr And C.Mth=@Mth ");
            SQL.AppendLine("Inner Join TblEmployee D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("    And (D.ResignDt Is Null Or (D.ResignDt Is Not Null And D.ResignDt>@CurrentDate)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And D.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(D.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblSite E On D.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Left Join TblPosition G On D.PosCode=G.PosCode ");
            SQL.AppendLine("Left Join TblCredit H On B.CreditCode=H.CreditCode ");
            SQL.AppendLine("Inner Join TblPayrun I On A.PayrunCode=I.PayrunCode And I.CancelInd='N' ");
            SQL.AppendLine("Left Join TblSection J On D.SectionCode = J.SectionCode ");
            SQL.AppendLine("Where A.Yr=@Yr ");
            SQL.AppendLine("And A.Mth=@Mth ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'Deduction' As Type, A.EmpCode, C.EmpName, C.EmpCodeOld, ");
            SQL.AppendLine("Null As Yr, Null As PayrunCode, Null As Mth, ");
            SQL.AppendLine("C.SiteCode, D.SiteName, C.DeptCode, E.DeptName, F.PosName, B.ADName As CreditName, A.Amt, C.SectionCode, G.SectionName ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.AmtType='1' ");
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("    And (C.ResignDt Is Null Or (C.ResignDt Is Not Null And C.ResignDt>@CurrentDate)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblSite D On C.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblPosition F On C.PosCode=F.PosCode ");
            SQL.AppendLine("Left Join TblSection G On C.SectionCode = G.SectionCode ");
            SQL.AppendLine("Where A.StartDt<=@CurrentDate ");
            SQL.AppendLine("And @CurrentDate<=A.EndDt ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select  ");
            SQL.AppendLine("X1.type, X1.EmpCode, C.EmpName, C.EmpCodeOld, X1.startDt As Yr,  ");
            SQL.AppendLine("X1.PayrunCode, x1.mth, C.siteCode, D.Sitename, C.DeptCode, E.Deptname, F.PosName, ");
            SQL.AppendLine("X1.type As Name, X1.Amt, ");
            SQL.AppendLine("C.SectionCode, G.SectionName ");
            SQL.AppendLine("From  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select B.EmpCode, 'Net Income' As type,  Left(A.startDt, 4) As StartDt,  ");
            SQL.AppendLine("    A.PayrunCode, SubString(A.Startdt, 5,2) As mth,  ");
            SQL.AppendLine("    (B.Salary+B.FixAllowance+B.meal+B.transport+B.VariableAllowance+B.IncEmployee+B.ADLeave+B.SalaryAdjustment) As Amt ");
            SQL.AppendLine("    From tblpayrun A ");
            SQL.AppendLine("    Inner Join TblpayrollProcess1 B On A.PayrunCode = B.payrunCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select B.EmpCode, 'SS DPLK' As type,  Left(A.startDt, 4) As StartDt,  ");
            SQL.AppendLine("    A.PayrunCode, SubString(A.Startdt, 5,2) As mth,  ");
            SQL.AppendLine("    (B.SSErDPLK+ B.SSEeDPLK) as Amt ");
            SQL.AppendLine("    From tblpayrun A ");
            SQL.AppendLine("    inner Join TblpayrollProcess1 B On A.PayrunCode = B.payrunCode ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select B.EmpCode, 'Transfer' As type, Left(A.startDt, 4) As StartDt,  ");
            SQL.AppendLine("    A.PayrunCode, SubString(A.Startdt, 5,2) As mth,  ");
            SQL.AppendLine("    (B.Amt) as Amt ");
            SQL.AppendLine("    From tblpayrun A ");
            SQL.AppendLine("    inner Join TblpayrollProcess1 B On A.PayrunCode = B.payrunCode ");
            SQL.AppendLine(")X1 ");
            SQL.AppendLine("Inner Join TblEmployee C On X1.EmpCode=C.EmpCode ");
            SQL.AppendLine("    And (C.ResignDt Is Null Or (C.ResignDt Is Not Null And C.ResignDt>@CurrentDate)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblSite D On C.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblPosition F On C.PosCode=F.PosCode ");
            SQL.AppendLine("Left Join TblSection G On C.SectionCode = G.SectionCode ");
            SQL.AppendLine("Where X1.startDt=@Yr ");
            SQL.AppendLine("And X1.Mth=@Mth ");

            SQL.AppendLine(") T ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Year",
                        "Month",

                        //6-10
                        "Payrun",
                        "Site",
                        "Department",
                        "Position",
                        "Section Code",
                        
                        //11-13
                        "Section",
                        "Type",
                        "Credit/"+Environment.NewLine+"Deduction",
                        "Amount"
                   },
                     new int[] 
                    {
                        //0
                        40,

                        //1-5
                        100, 220, 100, 50, 70,
                        
                        //6-10
                        100, 170, 170, 220, 100,  
                        
                        //11-12
                        180, 120, 200, 130
                   }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = string.Empty;

                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "EmpCode", "EmpName", "EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSection), "SectionCode", true);
               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By EmpName, Type Desc;",
                        new string[]
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "Yr", "Mth", "PayrunCode", 
                            
                            //6-10
                            "SiteName", "DeptName", "PosName", "sectionCode", "SectionName", 

                            //11
                             "Type", "CreditName",  "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 14 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkSection_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Section");
        }

        private void LueSection_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSection, new Sm.RefreshLue1(Sl.SetLueSectionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

        #endregion
    }
}
