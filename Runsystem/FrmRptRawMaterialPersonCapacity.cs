﻿#region Update
// 25/07/2018 [HAR] feedback : tanggal verifikasi waktu di group nggak muncul
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptRawMaterialPersonCapacity : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool IsReCompute = false;

        #endregion

        #region Constructor

        public FrmRptRawMaterialPersonCapacity(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                
                SetLuePosition(ref LuePosition);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);

                SetGrd();
                
                base.FrmLoad(sender, e);
                BtnChart.Visible = (Sm.GetValue("Select IfNull(ParValue, '') from tblparameter where ParCode='ChartAvailable'") == "Yes");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL(bool IsFilterByDocDt)
        {
            //SQL1 dan SQL2 1 bagian

            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select Z1.DocNo As RawMaterialVerifyDocNo, Z2.DocNo As RecvRawMaterialDocNo, Z2.DocType, ");
            SQL1.AppendLine("(Select Count(DocNo) From TblRecvRawMaterialDtl Where EmpType='1' And DocNo=Z2.DocNo) As Count1, ");
            SQL1.AppendLine("(Select Count(DocNo) From TblRecvRawMaterialDtl Where EmpType='2' And DocNo=Z2.DocNo) As Count2, ");
            SQL1.AppendLine("(Select Count(DocNo) From TblRecvRawMaterialDtl Where EmpType='3' And DocNo=Z2.DocNo) As Count3, "); 
            SQL1.AppendLine("(Select Count(DocNo) From TblRecvRawMaterialDtl Where EmpType='4' And DocNo=Z2.DocNo) As Count4, "); 
            SQL1.AppendLine("(Select Count(DocNo) From TblRecvRawMaterialDtl Where EmpType='2' And DocNo=Z2.DocNo) As Count5, ");
            SQL1.AppendLine("1 As Count6, ");
            SQL1.AppendLine("Qty1, Qty2, if(Z2.DocType='1', 'LOG', 'BALOK') As DocTypeKayu  ");   
            SQL1.AppendLine("From TblRawMaterialVerify Z1 ");
            SQL1.AppendLine("Inner Join TblRecvRawMaterialHdr Z2 On Z1.LegalDocVerifyDocNo=Z2.LegalDocVerifyDocNo And Z2.CancelInd='N' ");
            SQL1.AppendLine("Inner Join ( ");
		    SQL1.AppendLine("   Select RawMaterialVerifyDocNo, RecvRawMaterialDocNo, "); 
		    SQL1.AppendLine("   Sum(Qty1) As Qty1, Sum(Qty2) As Qty2 ");
		    SQL1.AppendLine("   From (  ");
			SQL1.AppendLine("       Select "); 
			SQL1.AppendLine("       Z3a.DocNo As RawMaterialVerifyDocNo, "); 
			SQL1.AppendLine("       Z3b.DocNo As RecvRawMaterialDocNo, "); 
			SQL1.AppendLine("       Z3c.Qty As Qty1, ");
			SQL1.AppendLine("       (Z3c.Qty * ( ");
	        SQL1.AppendLine("           Case Z3b.DocType ");
	        SQL1.AppendLine("           When '1' Then Truncate(((0.25*3.1415926*Z3d.Diameter*Z3d.Diameter*Z3d.Length)/1000000), 4) "); 
	        SQL1.AppendLine("           When '2' Then Truncate(((Z3d.Length*Z3d.Width*Z3d.Height)/1000000), 4) "); 
	        SQL1.AppendLine("           Else 0 End ");
	        SQL1.AppendLine("       )) As Qty2 ");
			SQL1.AppendLine("       From TblRawMaterialVerify Z3a ");
			SQL1.AppendLine("       Inner Join TblRecvRawMaterialHdr Z3b On Z3a.LegalDocVerifyDocNo=Z3b.LegalDocVerifyDocNo And Z3b.CancelInd='N' ");
			SQL1.AppendLine("       Inner Join TblRecvRawMaterialDtl2 Z3c On Z3b.DocNo=Z3c.DocNo ");
			SQL1.AppendLine("       Inner Join TblItem Z3d On Z3c.ItCode=Z3d.ItCode ");
			SQL1.AppendLine("       Where Z3a.CancelInd='N' ");
            SQL1.AppendLine(FilterByDocDt(IsFilterByDocDt, "Z3a"));
		    SQL1.AppendLine("   ) T Group By RawMaterialVerifyDocNo, RecvRawMaterialDocNo ");
            SQL1.AppendLine(") Z3 On Z1.DocNo=Z3.RawMaterialVerifyDocNo And Z2.DocNo=Z3.RecvRawMaterialDocNo ");
            SQL1.AppendLine("Where Z1.CancelInd='N' ");
            SQL1.AppendLine(FilterByDocDt(IsFilterByDocDt, "Z1"));

            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select RawMaterialVerifyDocNo, RecvRawMaterialDocNo, DocType, Z.DocTypeKayu, ");
            SQL2.AppendLine("IfNull((Qty1/Count1), 0) As Pos1Qty1, IfNull((Qty2/Count1), 0) As Pos1Qty2, ");
            SQL2.AppendLine("Case When DocType='1' Then IfNull((Qty1/Count2), 0) Else 0 End As Pos2Qty1, "); 
            SQL2.AppendLine("Case When DocType='1' Then IfNull((Qty2/Count2), 0) Else 0 End As Pos2Qty2, ");
            SQL2.AppendLine("IfNull((Qty1/Count3), 0) As Pos3Qty1, IfNull((Qty2/Count3), 0) As Pos3Qty2, ");
            SQL2.AppendLine("IfNull((Qty1/Count4), 0) As Pos4Qty1, IfNull((Qty2/Count4), 0) As Pos4Qty2, ");
            SQL2.AppendLine("Case When DocType='2' Then IfNull((Qty1/Count5), 0) Else 0 End As Pos5Qty1, "); 
            SQL2.AppendLine("Case When DocType='2' Then IfNull((Qty2/Count5), 0) Else 0 End As Pos5Qty2, ");
            SQL2.AppendLine("IfNull((Qty1/Count6), 0) As Pos6Qty1, IfNull((Qty2/Count6), 0) As Pos6Qty2, ");
	        SQL2.AppendLine("ifnull(1/Count1, 0) As Count1, ifnull(1/Count2, 0) As Count2, ifnull(1/Count3, 0) As Count3, ");
            SQL2.AppendLine("ifnull(1/Count4, 0) As Count4, ifnull(1/Count5, 0) As Count5, ifnull(1/Count6, 0) As Count6  ");
            SQL2.AppendLine("From ( " + SQL1.ToString() + ") Z ");

            //SQL3 1 bagian

            var SQL3 = new StringBuilder();

            SQL3.AppendLine("Select T1.DocNo As RawMaterialVerifyDocNo, T2.DocNo As RecvRawMaterialDocNo, ");
            SQL3.AppendLine("'1' As PosType, T4.EmpName ");
            SQL3.AppendLine("From TblRawMaterialVerify T1 ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialHdr T2 On T1.LegalDocVerifyDocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialDtl T3 On T2.DocNo=T3.DocNo And T3.EmpType='1' ");
            SQL3.AppendLine("Inner Join TblEmployee T4 On T3.EmpCode=T4.EmpCode ");
            SQL3.AppendLine("Where T1.CancelInd='N' ");
            SQL3.AppendLine(FilterByDocDt(IsFilterByDocDt, "T1"));
            SQL3.AppendLine("   Union All ");
            SQL3.AppendLine("Select T1.DocNo As RawMaterialVerifyDocNo, T2.DocNo As RecvRawMaterialDocNo, '2' As PosType, T4.EmpName ");
            SQL3.AppendLine("From TblRawMaterialVerify T1 ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialHdr T2 On T1.LegalDocVerifyDocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' And T2.DocType='1' ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialDtl T3 On T2.DocNo=T3.DocNo And T3.EmpType='2' ");
            SQL3.AppendLine("Inner Join TblEmployee T4 On T3.EmpCode=T4.EmpCode ");
            SQL3.AppendLine("Where T1.CancelInd='N' ");
            SQL3.AppendLine(FilterByDocDt(IsFilterByDocDt, "T1"));
            SQL3.AppendLine("   Union All ");
            SQL3.AppendLine("Select T1.DocNo As RawMaterialVerifyDocNo, T2.DocNo As RecvRawMaterialDocNo, '3' As PosType, T4.EmpName  ");
            SQL3.AppendLine("From TblRawMaterialVerify T1 ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialHdr T2 On T1.LegalDocVerifyDocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialDtl T3 On T2.DocNo=T3.DocNo And T3.EmpType='3' ");
            SQL3.AppendLine("Inner Join TblEmployee T4 On T3.EmpCode=T4.EmpCode ");
            SQL3.AppendLine("Where T1.CancelInd='N' ");
            SQL3.AppendLine(FilterByDocDt(IsFilterByDocDt, "T1"));
            SQL3.AppendLine("   Union All ");
            SQL3.AppendLine("Select T1.DocNo As RawMaterialVerifyDocNo, T2.DocNo As RecvRawMaterialDocNo, '4' As PosType, T4.EmpName ");
            SQL3.AppendLine("From TblRawMaterialVerify T1 ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialHdr T2 On T1.LegalDocVerifyDocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialDtl T3 On T2.DocNo=T3.DocNo And T3.EmpType='4' ");
            SQL3.AppendLine("Inner Join TblEmployee T4 On T3.EmpCode=T4.EmpCode ");
            SQL3.AppendLine("Where T1.CancelInd='N' ");
            SQL3.AppendLine(FilterByDocDt(IsFilterByDocDt, "T1"));
            SQL3.AppendLine("   Union All ");
            SQL3.AppendLine("Select T1.DocNo As RawMaterialVerifyDocNo, T2.DocNo As RecvRawMaterialDocNo, '5' As PosType, T4.EmpName ");
            SQL3.AppendLine("From TblRawMaterialVerify T1 ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialHdr T2 On T1.LegalDocVerifyDocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N'And T2.DocType='2' ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialDtl T3 On T2.DocNo=T3.DocNo And T3.EmpType='2' ");
            SQL3.AppendLine("Inner Join TblEmployee T4 On T3.EmpCode=T4.EmpCode ");
            SQL3.AppendLine("Where T1.CancelInd='N' ");
            SQL3.AppendLine(FilterByDocDt(IsFilterByDocDt, "T1"));
            SQL3.AppendLine("   Union All ");
            SQL3.AppendLine("Select T1.DocNo As RawMaterialVerifyDocNo, T2.DocNo As RecvRawMaterialDocNo, '6' As PosType, T3.UserName As EmpName  ");
            SQL3.AppendLine("From TblRawMaterialVerify T1 ");
            SQL3.AppendLine("Inner Join TblRecvRawMaterialHdr T2 On T1.LegalDocVerifyDocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' ");
            SQL3.AppendLine("Inner Join TblUser T3 On T2.CreateBy=T3.UserCode ");
            SQL3.AppendLine("Where T1.CancelInd='N' ");
            SQL3.AppendLine(FilterByDocDt(IsFilterByDocDt, "T1"));

            //SQL4 gabungan SQL2 dan SQL3

            var SQL4 = new StringBuilder();

            SQL4.AppendLine("Select X1.RawMaterialVerifyDocNo, X1.PosType, X1.EmpName, ");
            SQL4.AppendLine("Case X1.PosType  ");
            SQL4.AppendLine("   When '1' Then Pos1Qty1 ");
            SQL4.AppendLine("   When '2' Then Pos2Qty1 ");
            SQL4.AppendLine("   When '3' Then Pos3Qty1 ");
            SQL4.AppendLine("   When '4' Then Pos4Qty1 ");
            SQL4.AppendLine("   When '5' Then Pos5Qty1 ");
            SQL4.AppendLine("   When '6' Then Pos6Qty1 ");
            SQL4.AppendLine("   Else 0 End ");
            SQL4.AppendLine("As Qty1, ");
            SQL4.AppendLine("Case X1.PosType ");
            SQL4.AppendLine("   When '1' Then Pos1Qty2 ");
            SQL4.AppendLine("   When '2' Then Pos2Qty2 ");
            SQL4.AppendLine("   When '3' Then Pos3Qty2 ");
            SQL4.AppendLine("   When '4' Then Pos4Qty2 ");
            SQL4.AppendLine("   When '5' Then Pos5Qty2 ");
            SQL4.AppendLine("   When '6' Then Pos6Qty2 ");
            SQL4.AppendLine("   Else 0 End ");
            SQL4.AppendLine("As Qty2, X2.DocTypeKayu, ");
            SQL4.AppendLine("Case X1.PosType ");
            SQL4.AppendLine("   When '1' Then cast(Count1 as decimal(4,2)) ");
            SQL4.AppendLine("   When '2' Then cast(Count2 as decimal(4,2)) ");
            SQL4.AppendLine("   When '3' Then cast(Count3 as decimal(4,2)) ");
            SQL4.AppendLine("   When '4' Then cast(Count4 as decimal(4,2)) ");
            SQL4.AppendLine("   When '5' Then cast(Count5 as decimal(4,2)) ");
            SQL4.AppendLine("   When '6' Then cast(Count6 as decimal(4,2)) ");
            SQL4.AppendLine("   Else 0 End ");
            SQL4.AppendLine("As TTCount ");
            SQL4.AppendLine("From ( ");
            SQL4.AppendLine(SQL3.ToString());
            SQL4.AppendLine(") X1 ");
            SQL4.AppendLine("Inner Join ( " + SQL2.ToString());
            SQL4.AppendLine(") X2 On X1.RawMaterialVerifyDocNo = X2.RawMaterialVerifyDocNo And X1.RecvRawMaterialDocNo=X2.RecvRawMaterialDocNo ");

            var SQL5 = new StringBuilder();

            SQL5.AppendLine(SQL4.ToString());
            SQL5.AppendLine("Union All ");
            SQL5.AppendLine("Select T1.DocNo As RawMaterialVerifyDocNo, '7' As PosType, T4.UserName As EmpName, ");
            SQL5.AppendLine("Sum(Qty) As Qty1, 0 As Qty2, if(T2.DocType='1', 'LOG', 'BALOK') As DocTypeKayu, cast(1 as decimal(4,2)) As TTCount  ");
            SQL5.AppendLine("From TblRawMaterialVerify T1 ");
            SQL5.AppendLine("Inner Join TblRawMaterialOpnameHdr T2 On T1.LegalDocVerifyDocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' ");
            SQL5.AppendLine("Inner Join TblRawMaterialOpnameDtl T3 On T2.DocNo=T3.DocNo ");
            SQL5.AppendLine("Inner Join TblUser T4 On T2.CreateBy=T4.UserCode ");
            SQL5.AppendLine("Where T1.CancelInd='N' ");
            SQL5.AppendLine(FilterByDocDt(IsFilterByDocDt, "T1"));
            SQL5.AppendLine("Group By T1.DocNo, T4.UserName, T2.DocType ");

            var SQL6 = new StringBuilder();

            SQL6.AppendLine("Select A.DocNo, A.DocDt, B.QueueNo, D.TTName, E.PosType, E.EmpName, E.Qty1, E.Qty2, ");
            SQL6.AppendLine("Case E.PosType ");
            SQL6.AppendLine("    When '1' Then 'Grader' ");
            SQL6.AppendLine("    When '2' Then 'Asisten Grader' ");
            SQL6.AppendLine("    When '3' Then 'Bongkar Dalam' ");
            SQL6.AppendLine("    When '4' Then 'Bongkar Luar' ");
            SQL6.AppendLine("    When '5' Then 'Tata' ");
            SQL6.AppendLine("    When '6' Then 'Admin' ");
            SQL6.AppendLine("    When '7' Then 'Opname' ");
            SQL6.AppendLine("End As PosTypeDesc, E.DocTypeKayu As DocTypeKayu, TTCount ");
            SQL6.AppendLine("From TblRawMaterialVerify A ");
            SQL6.AppendLine("Inner Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo And B.CancelInd='N' ");
            SQL6.AppendLine("Inner Join TblLoadingQueue C On B.QueueNo=C.DocNo ");
            SQL6.AppendLine("Left Join TblTransportType D On C.TTCode=D.TTCode ");
            SQL6.AppendLine("Inner Join ( ");
            SQL6.AppendLine(SQL5.ToString());
            SQL6.AppendLine(") E On A.DocNo=E.RawMaterialVerifyDocNo ");
            SQL6.AppendLine("Where A.CancelInd='N' ");
            SQL6.AppendLine(FilterByDocDt(IsFilterByDocDt, "A"));

            return SQL6.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Position",
                        "Name",
                        "Verification"+Environment.NewLine+"Document#",
                        "Verification"+Environment.NewLine+" Date",
                        "Queue#",

                        //6-8
                        "Quantity"+Environment.NewLine+"Transport",
                        "Transport",
                        "Quantity"+Environment.NewLine+"(Batang)",
                        "Quantity"+Environment.NewLine+"(Kubik)",
                        "Type",
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 150, 130, 80, 80,
                        
                        //6-10
                        90, 90, 90, 90, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, false);
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePosition), "E.PosType", true);

                if (ChkDocDt.Checked)
                {
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                }

                SetSQL(ChkDocDt.Checked);
                IsReCompute = false;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(ChkDocDt.Checked) + Filter + " Order By E.PosType, E.EmpName, A.DocDt, A.DocNo;",
                        new string[]
                        {
                            //0
                            "PosTypeDesc", 

                            //1-5
                            "EmpName", "DocNo", "DocDt", "QueueNo", "TTCount",   
                            
                            //6-7
                            "TTName", "Qty1", "Qty2", "DocTypeKayu",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        }, true, false, false, false
                    );   
                Grd1.GroupObject.Add(10);
                Grd1.GroupObject.Add(1);
                //Grd1.GroupObject.Add(2);
                Grd1.Group();
                Grd1.Rows.CollapseAll();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                IsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }
       
        #endregion

        #region Additional Method

        private string FilterByDocDt(bool IsFilterByDocDt, string TableAlias)
        {
            return (IsFilterByDocDt) ? " And " + TableAlias + ".DocDt Between @DocDt1 And @DocDt2 " : "";
        }

        private void SetLuePosition(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Col1, Col2 From (");
                SQL.AppendLine("    Select '1' As Col1, 'Grader' As Col2 Union All");
                SQL.AppendLine("    Select '2' As Col1, 'Asisten Grader' As Col2 Union All");
                SQL.AppendLine("    Select '3' As Col1, 'Bongkar Dalam' As Col2 Union All");
                SQL.AppendLine("    Select '4' As Col1, 'Bongkar Luar' As Col2 Union All");
                SQL.AppendLine("    Select '5' As Col1, 'Tata' As Col2 Union All");
                SQL.AppendLine("    Select '6' As Col1, 'Admin' As Col2 Union All");
                SQL.AppendLine("    Select '7' As Col1, 'Opname' As Col2 ");
                SQL.AppendLine(") T Order By Col1");

                Sm.SetLue2(
                    ref Lue,
                    SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (IsReCompute) Sm.GrdExpand(Grd1);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LuePosition_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosition, new Sm.RefreshLue1(SetLuePosition));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPosition_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Position");
        }
        
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Verification date");
        }

        #endregion
    }
}
