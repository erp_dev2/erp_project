﻿#region Update
/*
    30/11/2021 [BRI/IOK] tambah rak 16 - 25 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptRawMaterialComparison : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptRawMaterialComparison(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -14);
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var SQL4 = new StringBuilder();
            var SQL5 = new StringBuilder();

            #region A

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblLegalDocVerifyHdr ");
            SQL.AppendLine("Where CancelInd='N' And ProcessInd1='P' And ProcessInd2='P' ");
            SQL.AppendLine("And DocNo Not In ( ");

            #region Query 1

            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.DocNo, ");
            SQL.AppendLine("        Case T3.SectionNo ");
            SQL.AppendLine("            When '1' Then T2.Shelf1 ");
            SQL.AppendLine("            When '2' Then T2.Shelf2 ");
            SQL.AppendLine("            When '3' Then T2.Shelf3 ");
            SQL.AppendLine("            When '4' Then T2.Shelf4 ");
            SQL.AppendLine("            When '5' Then T2.Shelf5 ");
            SQL.AppendLine("            When '6' Then T2.Shelf6 ");
            SQL.AppendLine("            When '7' Then T2.Shelf7 ");
            SQL.AppendLine("            When '8' Then T2.Shelf8 ");
            SQL.AppendLine("            When '9' Then T2.Shelf9 ");
            SQL.AppendLine("            When '10' Then T2.Shelf10 ");
            SQL.AppendLine("            When '11' Then T2.Shelf11 ");
            SQL.AppendLine("            When '12' Then T2.Shelf12 ");
            SQL.AppendLine("            When '13' Then T2.Shelf13 ");
            SQL.AppendLine("            When '14' Then T2.Shelf14 ");
            SQL.AppendLine("            When '15' Then T2.Shelf15 ");
            SQL.AppendLine("            When '16' Then T2.Shelf16 ");
            SQL.AppendLine("            When '17' Then T2.Shelf17 ");
            SQL.AppendLine("            When '18' Then T2.Shelf18 ");
            SQL.AppendLine("            When '19' Then T2.Shelf19 ");
            SQL.AppendLine("            When '20' Then T2.Shelf20 ");
            SQL.AppendLine("            When '21' Then T2.Shelf21 ");
            SQL.AppendLine("            When '22' Then T2.Shelf22 ");
            SQL.AppendLine("            When '23' Then T2.Shelf23 ");
            SQL.AppendLine("            When '24' Then T2.Shelf24 ");
            SQL.AppendLine("            When '25' Then T2.Shelf25 ");
            SQL.AppendLine("        End As Bin, ");
            SQL.AppendLine("        Sum(T3.Qty) As Qty1, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(Z3.Qty) Qty2 ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr Z1 ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameHdr Z2 On Z1.DocNo=Z2.LegalDocVerifyDocNo And Z2.CancelInd='N' And Z2.DocType='1' ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameDtl Z3 On Z2.DocNo=Z3.DocNo ");
            SQL.AppendLine("            Where Z1.CancelInd='N' And Z1.ProcessInd1='P' And Z1.ProcessInd2='P' ");
            SQL.AppendLine("            And Z1.DocNo=T1.DocNo ");
            SQL.AppendLine("            And Bin=Z3.Shelf ");
            SQL.AppendLine("        ), 0) As Qty2 ");
            SQL.AppendLine("        From TblLegalDocVerifyHdr T1 ");
            SQL.AppendLine("        Inner Join TblRecvrawMaterialHdr T2 On T1.DocNo=T2.LegalDocVerifyDocNo And T2.CancelInd='N' And T2.DocType='1' ");
            SQL.AppendLine("        Inner Join TblRecvrawMaterialDtl2 T3 On T2.DocNo=T3.DocNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' And T1.ProcessInd1='P' And T1.ProcessInd2='P' ");
            SQL.AppendLine("        Group By T1.DocNo, Bin ");
            SQL.AppendLine("        Having IfNull(Qty1, 0)<>IfNull(Qty2, 0) ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select DocNo From ( ");
            SQL.AppendLine("        Select T.DocNo, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            SQL.AppendLine("            From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            SQL.AppendLine("            And A.CancelInd='N' ");
            SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            SQL.AppendLine("            And A.DocType='1' ");
            SQL.AppendLine("        ), 0) As Qty1, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Sum(B.Qty) As Qty2 ");
            SQL.AppendLine("            From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            SQL.AppendLine("            And A.CancelInd='N' ");
            SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            SQL.AppendLine("            And A.DocType='1' ");
            SQL.AppendLine("        ), 0) As Qty2 ");
            SQL.AppendLine("    From TblLegalDocVerifyHdr T ");
            SQL.AppendLine("    Where T.CancelInd='N' And T.ProcessInd1='P' And T.ProcessInd2='P' ");
            SQL.AppendLine("    ) X Where IfNull(Qty1, 0)<>IfNull(Qty2, 0) ");

            #region old code
            //SQL.AppendLine("    Select Distinct Tbl.DocNo From ( ");

            //SQL.AppendLine("        Select T.DocNo, ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            //SQL.AppendLine("            From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='1' ");
            //SQL.AppendLine("        ), 0) As Qty1, ");
            //SQL.AppendLine("        IfNull((");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty2 ");
            //SQL.AppendLine("            From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='1' ");
            //SQL.AppendLine("        ), 0) As Qty2 ");
            //SQL.AppendLine("        From TblLegalDocVerifyHdr T ");
            //SQL.AppendLine("        Where T.CancelInd='N' ");
            //SQL.AppendLine("        And T.ProcessInd1='P' ");
            //SQL.AppendLine("        And T.ProcessInd2='P' ");

            //SQL.AppendLine("    ) Tbl Where IfNull(Tbl.Qty1, 0)<>IfNull(Tbl.Qty2, 0) ");

            #endregion

            #endregion

            SQL.AppendLine("    Union All ");

            #region Query 2

            //SQL.AppendLine("    Select Distinct Tbl1.DocNo From ( ");

            //SQL.AppendLine("        Select T.DocNo, ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty1 ");
            //SQL.AppendLine("            From TblRecvRawMaterialHdr A, TblRecvRawMaterialDtl2 B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='2' ");
            //SQL.AppendLine("        ), 0) As Qty1, ");
            //SQL.AppendLine("        IfNull((");
            //SQL.AppendLine("            Select Sum(B.Qty) As Qty2 ");
            //SQL.AppendLine("            From TblRawMaterialOpnameHdr A, TblRawMaterialOpnameDtl B ");
            //SQL.AppendLine("            Where A.DocNo=B.DocNo ");
            //SQL.AppendLine("            And A.CancelInd='N' ");
            //SQL.AppendLine("            And A.LegalDocVerifyDocNo=T.DocNo ");
            //SQL.AppendLine("            And A.DocType='2' ");
            //SQL.AppendLine("        ), 0) As Qty2 ");
            //SQL.AppendLine("        From TblLegalDocVerifyHdr T ");
            //SQL.AppendLine("        Where T.CancelInd='N' ");
            //SQL.AppendLine("        And T.ProcessInd1='P' ");
            //SQL.AppendLine("        And T.ProcessInd2='P' ");

            //SQL.AppendLine("    ) Tbl1 ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("            Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            //SQL.AppendLine("            From TblParameter Where ParCode='BalokVerifyPercentage' ");
            //SQL.AppendLine("        ) Tbl2 On 0=0 ");
            //SQL.AppendLine("        Where ( ");
            //SQL.AppendLine("            Qty2<(Qty1*(100-BalokInterval)*0.01) ");
            //SQL.AppendLine("            Or Qty2>(Qty1*(100+BalokInterval)*0.01) ");
            //SQL.AppendLine("        ) ");

            SQL.AppendLine("        Select Distinct DocNo From ( ");
            SQL.AppendLine("            Select T1.DocNo ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("                From ( ");
            SQL.AppendLine("                    Select A.DocNo, ");
            SQL.AppendLine("                        Case C.SectionNo ");
            SQL.AppendLine("                            When '1' Then B.Shelf1 ");
            SQL.AppendLine("                            When '2' Then B.Shelf2 ");
            SQL.AppendLine("                            When '3' Then B.Shelf3 ");
            SQL.AppendLine("                            When '4' Then B.Shelf4 ");
            SQL.AppendLine("                            When '5' Then B.Shelf5 ");
            SQL.AppendLine("                            When '6' Then B.Shelf6 ");
            SQL.AppendLine("                            When '7' Then B.Shelf7 ");
            SQL.AppendLine("                            When '8' Then B.Shelf8 ");
            SQL.AppendLine("                            When '9' Then B.Shelf9 ");
            SQL.AppendLine("                            When '10' Then B.Shelf10 ");
            SQL.AppendLine("                            When '11' Then B.Shelf11 ");
            SQL.AppendLine("                            When '12' Then B.Shelf12 ");
            SQL.AppendLine("                            When '13' Then B.Shelf13 ");
            SQL.AppendLine("                            When '14' Then B.Shelf14 ");
            SQL.AppendLine("                            When '15' Then B.Shelf15 ");
            SQL.AppendLine("                            When '16' Then B.Shelf16 ");
            SQL.AppendLine("                            When '17' Then B.Shelf17 ");
            SQL.AppendLine("                            When '18' Then B.Shelf18 ");
            SQL.AppendLine("                            When '19' Then B.Shelf19 ");
            SQL.AppendLine("                            When '20' Then B.Shelf20 ");
            SQL.AppendLine("                            When '21' Then B.Shelf21 ");
            SQL.AppendLine("                            When '22' Then B.Shelf22 ");
            SQL.AppendLine("                            When '23' Then B.Shelf23 ");
            SQL.AppendLine("                            When '24' Then B.Shelf24 ");
            SQL.AppendLine("                            When '25' Then B.Shelf25 ");
            SQL.AppendLine("                    End As Bin, C.Qty ");
            SQL.AppendLine("                    From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("                    Inner Join TblRecvRawMaterialHdr B ");
            SQL.AppendLine("                            On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                            And B.CancelInd='N' ");
            SQL.AppendLine("                            And B.DocType='2' ");
            SQL.AppendLine("                    Inner Join TblRecvRawMaterialDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                    Where A.CancelInd='N' ");
            SQL.AppendLine("                    And A.ProcessInd1='P' ");
            SQL.AppendLine("                    And A.ProcessInd2='P' ");
            SQL.AppendLine("                ) T Group By DocNo, Bin ");
            SQL.AppendLine("            ) T1 ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select A.DocNo, C.Shelf As Bin, C.Qty ");
            SQL.AppendLine("                From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("                Inner Join TblRawMaterialOpnameHdr B ");
            SQL.AppendLine("                    On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                    And B.CancelInd='N' ");
            SQL.AppendLine("                    And B.DocType='2' ");
            SQL.AppendLine("                Inner Join TblRawMaterialOpnameDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                Where A.CancelInd='N' ");
            SQL.AppendLine("                And A.ProcessInd1='P' ");
            SQL.AppendLine("                And A.ProcessInd2='P' ");
            SQL.AppendLine("            ) T Group By DocNo, Bin ");
            SQL.AppendLine("        ) T2 On T1.DocNo=T2.DocNo And T1.Bin=T2.Bin ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            SQL.AppendLine("            From TblParameter Where ParCode='BalokVerifyPercentage' ");
            SQL.AppendLine("        ) T3 On 0=0 ");
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("            IfNull(T2.Qty, 0)<(IfNull(T1.Qty, 0)*(100-IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("            Or IfNull(T2.Qty, 0)>(IfNull(T1.Qty, 0)*(100+IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo From ( ");
            SQL.AppendLine("        Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select A.DocNo, C.Shelf As Bin, C.Qty ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameHdr B ");
            SQL.AppendLine("                On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("                And B.CancelInd='N' ");
            SQL.AppendLine("                And B.DocType='2' ");
            SQL.AppendLine("            Inner Join TblRawMaterialOpnameDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            And A.ProcessInd1='P' ");
            SQL.AppendLine("            And A.ProcessInd2='P' ");
            SQL.AppendLine("        ) T Group By DocNo, Bin ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select DocNo, Bin, Sum(Qty) Qty ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select A.DocNo, ");
            SQL.AppendLine("                Case C.SectionNo ");
            SQL.AppendLine("                    When '1' Then B.Shelf1 ");
            SQL.AppendLine("                    When '2' Then B.Shelf2 ");
            SQL.AppendLine("                    When '3' Then B.Shelf3 ");
            SQL.AppendLine("                    When '4' Then B.Shelf4 ");
            SQL.AppendLine("                    When '5' Then B.Shelf5 ");
            SQL.AppendLine("                    When '6' Then B.Shelf6 ");
            SQL.AppendLine("                    When '7' Then B.Shelf7 ");
            SQL.AppendLine("                    When '8' Then B.Shelf8 ");
            SQL.AppendLine("                    When '9' Then B.Shelf9 ");
            SQL.AppendLine("                    When '10' Then B.Shelf10 ");
            SQL.AppendLine("                    When '11' Then B.Shelf11 ");
            SQL.AppendLine("                    When '12' Then B.Shelf12 ");
            SQL.AppendLine("                    When '13' Then B.Shelf13 ");
            SQL.AppendLine("                    When '14' Then B.Shelf14 ");
            SQL.AppendLine("                    When '15' Then B.Shelf15 ");
            SQL.AppendLine("                    When '16' Then B.Shelf16 ");
            SQL.AppendLine("                    When '17' Then B.Shelf17 ");
            SQL.AppendLine("                    When '18' Then B.Shelf18 ");
            SQL.AppendLine("                    When '19' Then B.Shelf19 ");
            SQL.AppendLine("                    When '20' Then B.Shelf20 ");
            SQL.AppendLine("                    When '21' Then B.Shelf21 ");
            SQL.AppendLine("                    When '22' Then B.Shelf22 ");
            SQL.AppendLine("                    When '23' Then B.Shelf23 ");
            SQL.AppendLine("                    When '24' Then B.Shelf24 ");
            SQL.AppendLine("                    When '25' Then B.Shelf25 ");
            SQL.AppendLine("                End As Bin, C.Qty ");
            SQL.AppendLine("            From TblLegalDocVerifyHdr A ");
            SQL.AppendLine("            Inner Join TblRecvRawMaterialHdr B ");
            SQL.AppendLine("            On A.DocNo=B.LegalDocVerifyDocNo ");
            SQL.AppendLine("            And B.CancelInd='N' ");
            SQL.AppendLine("            And B.DocType='2' ");
            SQL.AppendLine("            Inner Join TblRecvRawMaterialDtl2 C On B.DocNo=C.DocNo ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            And A.ProcessInd1='P' ");
            SQL.AppendLine("            And A.ProcessInd2='P' ");
            SQL.AppendLine("        ) T Group By DocNo, Bin ");
            SQL.AppendLine("    ) T2 On T1.DocNo=T2.DocNo And T1.Bin=T2.Bin ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Cast(ParValue As Decimal(5, 2)) As BalokInterval ");
            SQL.AppendLine("        From TblParameter ");
            SQL.AppendLine("        Where ParCode='BalokVerifyPercentage' ");
            SQL.AppendLine("    ) T3 On 0=0 ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("        IfNull(T1.Qty, 0)<(IfNull(T2.Qty, 0)*(100-IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        Or IfNull(T1.Qty, 0)>(IfNull(T2.Qty, 0)*(100+IfNull(T3.BalokInterval, 0))*0.01) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) Tbl ");

            #endregion

            SQL.AppendLine(") ");

            #endregion

            #region B

            SQL2.AppendLine("Select Distinct LegalDocVerifyDocNo, Shelf From (");
            SQL2.AppendLine("Select T1.LegalDocVerifyDocNo, T2.Shelf ");
            SQL2.AppendLine("From TblRawMaterialOpnameHdr T1 ");
            SQL2.AppendLine("Inner Join TblRawMaterialOpnameDtl T2 On T1.DocNo=T2.DocNo ");
            SQL2.AppendLine("Where T1.CancelInd='N' And IfNull(T1.ProcessInd, 'O')<>'F' ");

            for (int Index = 1; Index <= 25; Index++)
            {
                SQL2.AppendLine("Union All ");
                SQL2.AppendLine("Select LegalDocVerifyDocNo, Shelf" + Index.ToString() + " As Shelf ");
                SQL2.AppendLine("From TblRecvRawMaterialHdr ");
                SQL2.AppendLine("Where CancelInd='N' And IfNull(ProcessInd, 'O')<>'F' ");
            }

            SQL2.AppendLine(") T ");

            #endregion

            #region C

            SQL3.AppendLine("Select LegalDocVerifyDocNo, Shelf, Sum(Qty1) As Qty1, ");
            SQL3.AppendLine("Group_Concat(Concat(DocNo, ' (', CreateBy, ')') Separator ', ###') As DocNoWithCreateBy, ");
            SQL3.AppendLine("Group_Concat(DocNo Separator ', ') As DocNo From (");
            
            SQL3.AppendLine("   Select LegalDocVerifyDocNo, Shelf, DocNo, CreateBy, Sum(Qty) As Qty1 From (");
            for (int Index = 1; Index <= 25; Index++)
            {
                if (Index != 1) SQL3.AppendLine("       Union All ");
                SQL3.AppendLine("       Select T1.LegalDocVerifyDocNo, T1.DocNo, T1.CreateBy, T1.Shelf" + Index.ToString() + " As Shelf, T2.Qty ");
                SQL3.AppendLine("       From TblRecvRawMaterialHdr T1 ");
                SQL3.AppendLine("       Inner Join TblRecvRawMaterialDtl2 T2 On T1.DocNo=T2.DocNo And T2.SectionNo='" + Index.ToString() + "' ");
                SQL3.AppendLine("       Where T1.CancelInd='N' And IfNull(T1.ProcessInd, 'O')<>'F' ");
            }
            SQL3.AppendLine("   ) T Group By LegalDocVerifyDocNo, DocNo, Shelf, CreateBy ");
            SQL3.AppendLine("   Having Sum(Qty)<>0 ");
            SQL3.AppendLine(") Tbl Group By LegalDocVerifyDocNo, Shelf ");

            #endregion

            #region D

            SQL4.AppendLine("Select LegalDocVerifyDocNo, Shelf, Sum(Qty2) Qty2, ");
            SQL4.AppendLine("Group_Concat(Concat(DocNo, ' (', CreateBy, ')') Separator ', ###') As DocNoWithCreateBy, ");
            SQL4.AppendLine("Group_Concat(DocNo Separator ', ') As DocNo From (");
            SQL4.AppendLine("   Select T1.LegalDocVerifyDocNo, T2.Shelf, T1.DocNo, T1.CreateBy, Sum(T2.Qty) As Qty2 ");
            SQL4.AppendLine("   From TblRawMaterialOpnameHdr T1 ");
            SQL4.AppendLine("   Inner Join TblRawMaterialOpnameDtl T2 On T1.DocNo=T2.DocNo ");
            SQL4.AppendLine("   Where T1.CancelInd='N' And IfNull(T1.ProcessInd, 'O')<>'F' ");
            SQL4.AppendLine("   Group By T1.LegalDocVerifyDocNo, T2.Shelf, T1.DocNo, T1.CreateBy ");
            SQL4.AppendLine("   Having Sum(T2.Qty)<>0 ");
            SQL4.AppendLine(") Tbl Group By LegalDocVerifyDocNo, Shelf ");

            #endregion

            #region E

            SQL5.AppendLine("Select A.DocNo, C.VdName, A.QueueNo, Left(B.CreateDt, 8) As QueueDt, A.VehicleRegNo, ");
            SQL5.AppendLine("D.Shelf, E.DocNoWithCreateBy As RecvRawMaterialDocNoWithCreateBy, ");
            SQL5.AppendLine("E.DocNo As RecvRawMaterialDocNo, E.Qty1, F.DocNoWithCreateBy As RawMaterialOpnameDocNoWithCreateBy, F.DocNo As RawMaterialOpnameDocNo, F.Qty2 ");
            SQL5.AppendLine("From TblLegalDocVerifyHdr A ");
            SQL5.AppendLine("Inner Join TblLoadingQueue B On A.QueueNo=B.DocNo ");
            SQL5.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            
            SQL5.AppendLine("Inner Join (");
            SQL5.AppendLine(SQL2.ToString());
            SQL5.AppendLine("   ) D On A.DocNo=D.LegalDocVerifyDocNo ");

            SQL5.AppendLine("Left Join (");
            SQL5.AppendLine(SQL3.ToString());
            SQL5.AppendLine("   ) E On A.DocNo=E.LegalDocVerifyDocNo And D.Shelf=E.Shelf ");


            SQL5.AppendLine("Left Join (");
            SQL5.AppendLine(SQL4.ToString());
            SQL5.AppendLine("   ) F On A.DocNo=F.LegalDocVerifyDocNo And D.Shelf=F.Shelf ");

            SQL5.AppendLine("Where A.CancelInd='N' ");
            SQL5.AppendLine("And (A.ProcessInd1='P' Or A.ProcessInd2='P') ");
            SQL5.AppendLine("And (E.Qty1<>0 Or F.Qty2<>0) ");
            SQL5.AppendLine("And A.DocNo Not In ( ");
            SQL5.AppendLine(SQL.ToString());
            SQL5.AppendLine(") ");

            #endregion

            mSQL = SQL5.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Verifikasi#", 
                        "Vendor",
                        "Nomor"+Environment.NewLine+"Antrian",   
                        "Tanggal"+Environment.NewLine+"Antrian", 
                        "Nomor Polisi", 

                        //6-10
                        "Rak",
                        "Jumlah"+Environment.NewLine+"Terima",   
                        "Jumlah"+Environment.NewLine+"Opname",
                        "Dokumen"+Environment.NewLine+"Penerimaan",  
                        "",
                        
                        //11-14
                        "Dokumen"+Environment.NewLine+"Opname",  
                        "",
                        "Dokumen"+Environment.NewLine+"Penerimaan", 
                        "Dokumen"+Environment.NewLine+"Opname"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 200, 100, 80, 80, 
                        
                        //6-10
                        80, 80, 80, 200, 20, 
                        
                        //11-14
                        200, 20, 0, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 10, 12 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 13, 14 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 13, 14 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, !ChkHideInfoInGrd.Checked);
            Grd1.Rows.AutoHeight();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.QueueNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "B.CreateDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By B.CreateDt, A.QueueNo, D.Shelf",
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "VdName", "QueueNo", "QueueDt", "VehicleRegNo", "Shelf", 
                        
                        //6-10
                        "Qty1", "Qty2", "RecvRawMaterialDocNoWithCreateBy", "RawmaterialOpnameDocNoWithCreateBy", "RecvRawMaterialDocNo",

                        //11
                        "RawmaterialOpnameDocNo"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                    Grd1.Cells[Row, 9].Value = (dr[c[8]] == DBNull.Value) ? "" : dr.GetString(c[8]).Replace("###", Environment.NewLine).Trim();
                    Grd1.Cells[Row, 11].Value = (dr[c[9]] == DBNull.Value) ? "" : dr.GetString(c[9]).Replace("###", Environment.NewLine).Trim();
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 11);
                }, true, true, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (Grd1.Rows.Count > 1)
                    Sm.FocusGrd(Grd1, 1, 2);
                else
                    Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            string DocNo = string.Empty, MenuCode = string.Empty;

            e.DoDefault = false;
         
            try
            {
                if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
                {
                    DocNo = GetFirstDocNo(Sm.GetGrdStr(Grd1, e.RowIndex, 13));
                    MenuCode = GetMenuCode("FrmRecvRawMaterial", DocNo);
                    var f = new FrmRecvRawMaterial2(MenuCode);
                    f.Tag = MenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = DocNo;
                    f.ShowDialog();
                }

                if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
                {
                    DocNo = GetFirstDocNo(Sm.GetGrdStr(Grd1, e.RowIndex, 14));
                    MenuCode = GetMenuCode("FrmRawMaterialOpname", "");
                    var f = new FrmRawMaterialOpname2(MenuCode);
                    f.Tag = MenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = DocNo;
                    f.ShowDialog();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            string DocNo = string.Empty, MenuCode = string.Empty;
            try
            {
                if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
                {
                    DocNo = GetFirstDocNo(Sm.GetGrdStr(Grd1, e.RowIndex, 13));
                    MenuCode = GetMenuCode("FrmRecvRawMaterial", DocNo);
                    var f = new FrmRecvRawMaterial2(MenuCode);
                    f.Tag = MenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = DocNo;
                    f.ShowDialog();
                }

                if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
                {
                    DocNo = GetFirstDocNo(Sm.GetGrdStr(Grd1, e.RowIndex, 14));
                    MenuCode = GetMenuCode("FrmRawMaterialOpname", "");
                    var f = new FrmRawMaterialOpname2(MenuCode);
                    f.Tag = MenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = DocNo;
                    f.ShowDialog();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private string GetFirstDocNo(string DocNo)
        {
            if (DocNo.Length != 0)
            {
                int Index = DocNo.IndexOf(",");
                if (Index >= 0)
                    DocNo = (Sm.Left(DocNo, Index)).Trim();
            }
            return DocNo;
        }

        private string GetMenuCode(string Frm, string DocNo)
        {
            string MenuCode="";
            var cm = new MySqlCommand();
            if (Sm.CompareStr(Frm, "FrmRawMaterialOpname"))
            {
                cm.CommandText = "Select MenuCode From TblMenu Where Param=@Frm Limit 1";
                Sm.CmParam<String>(ref cm, "@Frm", Frm);
            }
            if (Sm.CompareStr(Frm, "FrmRecvRawMaterial"))
            {
                cm.CommandText = 
                    "Select MenuCode From TblMenu " +
                    "Where Param=@Frm " +
                    "And MenuCode=Case When " +
                    "Exists( " +
                    "   Select DocNo From TblRecvRawMaterialDtl2 A, TblItem B " +
                    "   Where A.DocNo=@DocNo And A.ItCode=B.ItCode " +
                    "   And B.ItCtCode=(Select ParValue From TblParameter Where ParCode='ItCtRMPActual') Limit 1 " +
                    ") Then (Select ParValue From TblParameter Where ParCode='RRMMenuCode1') " +
                    "Else (Select ParValue From TblParameter Where ParCode='RRMMenuCode2') End " +
                    "Limit 1";
                Sm.CmParam<String>(ref cm, "@Frm", Frm);
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            }
            MenuCode = Sm.GetValue(cm);
            return (MenuCode.Length==0)?"XXX":MenuCode;
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion
    }
}
