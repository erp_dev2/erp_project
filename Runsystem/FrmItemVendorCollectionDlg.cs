﻿#region Update
/*
  04/11/2019 [DITA/IMS] tambah informasi Specifications
  03/09/2021 [VIN/IMS] Union All ke MR service 
  12/11/2021 [VIN/IMS] BUG Filter  
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemVendorCollectionDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmItemVendorCollection mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsFilterBySite = false;


        #endregion

        #region Constructor

        public FrmItemVendorCollectionDlg(FrmItemVendorCollection FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueItCtCode(ref LueItCtCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            string mDoctitle = Sm.GetParameter("DocTitle");

            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 6;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Item Code",
                        "Item Local Code",
                        "",
                        "Item Name",

                        //6-10
                        "Foreign Name",
                        "Category Code",
                        "Category",
                        (mDoctitle=="IMS") ? "Purchase Requisition" : "Material Request",
                        (mDoctitle=="IMS") ? "Purchase Requisition DNo" : "Material Request DNo",

                        //11-14
                        "Date",
                        "Vendor Code",
                        "Vendor Name",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        20, 100, 150, 20, 250, 
                        //6-10
                        200, 80, 150, 150, 80, 
                        //11-14
                        100, 100, 100, 300
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 11 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7, 10, 12, 13 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 14 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.* From( ");

            SQL.AppendLine("Select A.DocNo, B.ItCode, C.ItCodeInternal, C.itName, C.Foreignname, C.ItCTCode, D.ItCtName,  ");
            SQL.AppendLine("B.Dno, A.DocDt,  Concat(E.VdCode, '#') As VdCode, E.Vdname, C.Specification ");
            SQL.AppendLine("From TblMaterialRequesthdr A ");
            SQL.AppendLine("Inner Join TblmaterialRequestDtl B on A.DocNo  = B.DocNo  ");
            SQL.AppendLine("Inner Join Tblitem C On B.ItCode = C.itCode ");
            SQL.AppendLine("Inner Join tblitemcategory D On C.ItCTCode = D.ItCTCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.ItCtCode, group_concat(distinct A.VdCode separator '#') As VdCode,  ");
	        SQL.AppendLine("    group_Concat(distinct B.Vdname) As VdName ");
 	        SQL.AppendLine("    From TblvendorItemCategory A ");
 	        SQL.AppendLine("    Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("    Group BY A.ItCtCode ");
            SQL.AppendLine(")E On C.ItCtCode = E.ItCtCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) And  B.Status = 'A' And B.cancelInd = 'N' ");
            SQL.AppendLine("And Concat(B.DocNo, B.DNo) Not in  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select Concat(MaterialRequestDocno, MaterialRequestDno) From tblPoRequestDtl  ");
	        SQL.AppendLine("    Where cancelInd = 'N' ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    Select Concat(MRDocno, MRDno) From tblItemVendorCollectionDtl  ");
            SQL.AppendLine("    Where cancelInd = 'N' ");
            SQL.AppendLine(")  ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("And Position(Concat('##', B.Docno, B.Dno, '##') In @SelectedItem)<1 ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DocNo, B.ItCode, C.ItCodeInternal, C.itName, C.Foreignname, C.ItCTCode, D.ItCtName,  B.Dno, A.DocDt,  Concat(E.VdCode, '#') As VdCode, E.Vdname, C.Specification  ");
            SQL.AppendLine("From tblmaterialrequestservicehdr A ");
            SQL.AppendLine("Inner Join tblmaterialrequestservicedtl B on A.DocNo  = B.DocNo  ");
            SQL.AppendLine("Inner Join Tblitem C On B.ItCode = C.itCode ");
            SQL.AppendLine("Inner Join tblitemcategory D On C.ItCTCode = D.ItCTCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.ItCtCode, group_concat(distinct A.VdCode separator '#') As VdCode,  ");
            SQL.AppendLine("    group_Concat(distinct B.Vdname) As VdName ");
            SQL.AppendLine("    From TblvendorItemCategory A ");
            SQL.AppendLine("    Inner Join TblVendor B On A.VdCode = B.VdCode");
            SQL.AppendLine("   Group BY A.ItCtCode");
            SQL.AppendLine(")E On C.ItCtCode = E.ItCtCode  ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) And  B.Status = 'A' And B.cancelInd = 'N' ");
            SQL.AppendLine("And Concat(B.DocNo, B.DNo) Not in ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select Concat(MaterialRequestServiceDocno, MaterialRequestServiceDno) From Tblmaterialrequestdtl ");
            SQL.AppendLine("    Where cancelInd = 'N' AND MaterialRequestServiceDocno LIKE '%SPPJP%' ");
            SQL.AppendLine("    UNION ALL  ");
            SQL.AppendLine("    Select Concat(MRDocno, MRDno) From tblItemVendorCollectionDtl  ");
            SQL.AppendLine("    Where cancelInd = 'N' AND MRDocno LIKE '%SPPJP%' ");
            SQL.AppendLine(")  ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("And Position(Concat('##', B.Docno, B.Dno, '##') In @SelectedItem)<1  ");
            SQL.AppendLine(") A ");
           


            mSQL = SQL.ToString();
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtItemCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.itName", "A.Foreignname" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName Desc;",
                        new string[] 
                        { 
                             //0
                             "ItCode", 
                             
                             //1-5
                             "itCodeInternal", "Itname", "ForeignName", "ItCtCode", "ItCtname",
                             //6-10
                             "DocNo", "Dno", "DocDt", "vdCode", "Vdname", 
                             //11
                             "Specification"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {

            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 14);

                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 4), Sm.GetGrdStr(Grd1, Row, 9))
                    && Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 5), Sm.GetGrdStr(Grd1, Row, 10))                    
                    ) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Additional Method

        #endregion

        #endregion

        #region event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkItemCode_CheckedChanged(object sender, EventArgs e)
        {
           Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        private void TxtItemCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
