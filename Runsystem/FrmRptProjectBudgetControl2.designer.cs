﻿namespace RunSystem
{
    partial class FrmRptProjectBudgetControl2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptProjectBudgetControl2));
            this.LblProjectCode = new System.Windows.Forms.Label();
            this.LueProjectCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.ChkSOContractDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSOContractDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkSOContractDocNo);
            this.panel2.Controls.Add(this.TxtSOContractDocNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.BtnSOContractDocNo);
            this.panel2.Controls.Add(this.LblProjectCode);
            this.panel2.Controls.Add(this.LueProjectCode);
            this.panel2.Size = new System.Drawing.Size(772, 49);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 424);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 49);
            this.panel3.Size = new System.Drawing.Size(772, 424);
            // 
            // LblProjectCode
            // 
            this.LblProjectCode.AutoSize = true;
            this.LblProjectCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProjectCode.ForeColor = System.Drawing.Color.Red;
            this.LblProjectCode.Location = new System.Drawing.Point(42, 6);
            this.LblProjectCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProjectCode.Name = "LblProjectCode";
            this.LblProjectCode.Size = new System.Drawing.Size(46, 14);
            this.LblProjectCode.TabIndex = 10;
            this.LblProjectCode.Text = "Project";
            this.LblProjectCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProjectCode
            // 
            this.LueProjectCode.EnterMoveNextControl = true;
            this.LueProjectCode.Location = new System.Drawing.Point(92, 3);
            this.LueProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectCode.Name = "LueProjectCode";
            this.LueProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.Appearance.Options.UseFont = true;
            this.LueProjectCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectCode.Properties.DropDownRows = 30;
            this.LueProjectCode.Properties.NullText = "[Empty]";
            this.LueProjectCode.Properties.PopupWidth = 650;
            this.LueProjectCode.Size = new System.Drawing.Size(623, 20);
            this.LueProjectCode.TabIndex = 11;
            this.LueProjectCode.ToolTip = "F4 : Show/hide list";
            this.LueProjectCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectCode.EditValueChanged += new System.EventHandler(this.LueProjectCode_EditValueChanged);
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(92, 24);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 30;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(230, 20);
            this.TxtSOContractDocNo.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(5, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 14);
            this.label4.TabIndex = 25;
            this.label4.Text = "SO Contract#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(349, 25);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(24, 18);
            this.BtnSOContractDocNo.TabIndex = 27;
            this.BtnSOContractDocNo.ToolTip = "Find SO Contract";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo.Click += new System.EventHandler(this.BtnSOContractDocNo_Click);
            // 
            // ChkSOContractDocNo
            // 
            this.ChkSOContractDocNo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkSOContractDocNo.Location = new System.Drawing.Point(325, 23);
            this.ChkSOContractDocNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkSOContractDocNo.Name = "ChkSOContractDocNo";
            this.ChkSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSOContractDocNo.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.ChkSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkSOContractDocNo.Properties.Appearance.Options.UseForeColor = true;
            this.ChkSOContractDocNo.Properties.Caption = "";
            this.ChkSOContractDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSOContractDocNo.Size = new System.Drawing.Size(22, 22);
            this.ChkSOContractDocNo.TabIndex = 28;
            this.ChkSOContractDocNo.CheckedChanged += new System.EventHandler(this.ChkSOContractDocNo_CheckedChanged);
            // 
            // FrmRptProjectBudgetControl2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptProjectBudgetControl2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSOContractDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblProjectCode;
        public DevExpress.XtraEditors.LookUpEdit LueProjectCode;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        protected internal DevExpress.XtraEditors.CheckEdit ChkSOContractDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
    }
}