﻿#region Update
/*
    06/08/2019 [TKG] New application
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMProductCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmIMMProductCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmIMMProductCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProdCtCode, TxtProdCtName
                    }, true);
                    TxtProdCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProdCtCode, TxtProdCtName
                    }, false);
                    TxtProdCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtProdCtCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProdCtName
                    }, false);
                    TxtProdCtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtProdCtCode, TxtProdCtName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMProductCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtProdCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblIMMProductCategory(ProdCtCode, ProdCtName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ProdCtCode, @ProdCtName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ProdCtName=@ProdCtName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ProdCtCode", TxtProdCtCode.Text);
                Sm.CmParam<String>(ref cm, "@ProdCtName", TxtProdCtName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtProdCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ProdCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ProdCtCode", ProdCtCode);
                Sm.ShowDataInCtrl(
                    ref cm,
                    "Select ProdCtCode, ProdCtName From TblIMMProductCategory Where ProdCtCode=@ProdCtCode",
                    new string[] 
                    {
                        "ProdCtCode", "ProdCtName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtProdCtCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtProdCtName.EditValue = Sm.DrStr(dr, c[1]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtProdCtCode, "Product category code", false) ||
                Sm.IsTxtEmpty(TxtProdCtName, "Product category name", false) ||
                IsProdCtCodeExisted();
        }

        private bool IsProdCtCodeExisted()
        {
            if (!TxtProdCtCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select ProdCtCode From TblIMMProductCategory Where ProdCtCode=@Param Limit 1;", TxtProdCtCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Product category code ( " + TxtProdCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtProdCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProdCtCode);
        }

        private void TxtProdCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProdCtName);
        }

        #endregion

        #endregion
    }
}
