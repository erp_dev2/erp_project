﻿#region Update
/*
    20/01/2020 [TKG/IMS] tambah department group
    20/06/2022 [RIS/PHT] Merubah source department group dengan param GroupDepartementSource
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmDepartmentFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDepartment mFrmParent;
        internal bool mIsDeptFilterByDivision = false;

        #endregion

        #region Constructor

        public FrmDepartmentFind(FrmDepartment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsDeptFilterByDivision = Sm.GetParameterBoo("IsDeptFilterByDivision");
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "Authority To"+Environment.NewLine+"Control Certain Items",
                        "Division",
                        
                        //6-10
                        "Group",
                        "Created By",   
                        "Created Date",  
                        "Created Time", 
                        "Last Updated By", 

                        //11-12
                        "Last Updated Date",
                        "Last Updated Time"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 8, 11 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 12 });
            if (mIsDeptFilterByDivision)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 8, 9, 10, 11, 12 }, false);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDeptName.Text, new string[] { "A.DeptCode", "A.DeptName" });

                SQL.AppendLine("Select A.DeptCode, A.DeptName, A.ActInd, A.ControlItemInd, B.DivisionName, ");
                if (mFrmParent.mGroupDepartementSource == "1")
                    SQL.AppendLine("C.OptDesc as OptDesc, ");
                else if (mFrmParent.mGroupDepartementSource == "2")
                    SQL.AppendLine("C.DirectorateGroupName as OptDesc, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblDepartment A ");
                SQL.AppendLine("Left Join TblDivision B On A.DivisionCode=B.DivisionCode ");
                if(mFrmParent.mGroupDepartementSource == "1")
                    SQL.AppendLine("Left Join TblOption C On A.DeptGrpCode=C.OptCode And C.OptCat='DepartmentGroup' ");
                else if(mFrmParent.mGroupDepartementSource == "2")
                    SQL.AppendLine("Left Join TblDirectorateGroup C On A.DeptGrpCode=C.DirectorateGroupCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By A.DeptName;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "DeptCode", 
                                
                            //1-5
                            "DeptName", "ActInd", "ControlItemInd", "DivisionName", "OptDesc", 
                            //6-9
                            "CreateBy", "CreateDt","LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 9);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDeptName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDeptName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
