﻿#region Update
/*
    04/11/2021 [BRI/PHT] New Apps. Based on MaterialRequest
    24/11/2021 [TYO/PHT] Menambah kolom total independent estimated price di detail
    26/11/2021 [IBL/PHT] Mengaktifkan upload file. File mandatory berdasarkan parameter MandatoryFileForIndependentEstimatedPrice
    20/01/2022 [DEV/PHT] Menambahkan Grand Total Independent Estimated Price pada menu Independent Estimated Price dan centangan "Addendum"
    27/01/2022 [TRI/PHT] Benerin typo
    18/05/2022 [RIS/PHT] Available budget berdasarkan param SourceAvailableBudgetForMR
    06/07/2022 [VIN/PHT] BUG IndependentEstPrice decimal bukan string 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion


namespace RunSystem
{
    public partial class FrmIndependentEstimatedPrice : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mDroppingRequestBCCode = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mItGrpCodeNotShowOnMaterialRequest = string.Empty,
            mSourceAvailableBudgetForMR = string.Empty;
        internal FrmIndependentEstimatedPriceFind FrmFind;
        private bool
            mIsUseItemConsumption = false,
            mIsRemarkForApprovalMandatory = false,
            IsAutoGeneratePurchaseLocalDocNo = false,
            mIsApprovalBySiteMandatory = false,
            mIsDocNoWithDeptShortCode = false,
            mIsDeptFilterBySite = false,
            mIsShowSeqNoInMaterialRequest = false,
            mIsMaterialRequestMaxStockValidationEnabled = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsMaterialRequestDocNoUseDifferentAbbr = false,
            mIsMaterialRequestSPPJBDocNoUseDifferentAbbr = false,
            mIsUsageDateMaterialRequestMandatory = false,
            mIsMaterialRequestShowReorderPoint =false,
            mIsMaterialRequestShowMinimumStock =false,
            mIsBudget2YearlyFormat = false,
            mIsMRDurationMandatory = false,
            mIsMRApprovalByAmount = false,
            mIsCASUsedForBudget = false,
            mIsMRWithTenderEnabled = false,
            mIsMRUseReview = false,
            mIsMRUseReference = false
            ;

        internal bool
            mIsSiteMandatory = false,
            mIsPICInMRMandatory = false,
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsFilterByItCt = false,
            mIsShowForeignName = false,
            mIsBudgetActive = false,
            mIsDORequestNeedStockValidation = false,
            mIsDORequestUseItemIssued = false,
            mIsMRShowEstimatedPrice = false,
            mIsMRAllowToUploadFile = false,
            mIsMaterialRequestForDroppingRequestEnabled = false,
            mIsBOMShowSpecifications = false,
            mIsMRSPPJBEnabled = false,
            mIsMRSPPJB = false,
            mIsMRAutomaticGetLastVendorQuotation = false,
            mIsMRBudgetBasedOnBudgetCategory = false,
            mIsRemarkMRNotMandatory = false,
            mIsMRShowTotalPrice = false,
            mIsMRQuotationNotShow = false,
            mIsPICGrouped = false,
            mIsMRUseProcurementType = false,
            mIsMRItemGroupNotShow = false,
            mIsUseECatalog = false,
            mIsMRLocalDocNoFromReference = false
            ;
        private string
            mBudgetBasedOn = "1",
            mIsPrintOutUseDigitalSignature = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mMRAvailableBudgetSubtraction = string.Empty,
            mLabelFileForMR = string.Empty,
            mMandatoryFileForMR = string.Empty
            ;

        private string mFormPrintOutMaterialRequestWithoutConsumption = string.Empty;
        private string mFormPrintOutMaterialRequestWithConsumption = string.Empty;
        private string mFormPrintOutMaterialRequestSPPJB = string.Empty;
        private string mFormPrintOutMaterialRequestSPPJB2 = string.Empty;
        private string Doctitle = Sm.GetParameter("Doctitle");
           
        private iGCopyPasteManager fCopyPasteManager;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;
        internal List<Job> mlJob;

        #endregion

        #region Constructor

        public FrmIndependentEstimatedPrice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (mLabelFileForMR.Length > 0 && Doctitle == "PHT")
                    LabelForMR();
                if (mMandatoryFileForMR.Length > 0)
                    MandatoryFile();
                LblExpDt.Visible = DteExpDt.Visible = false;

                if (!mIsMRUseReview)
                    Tc1.TabPages.Remove(Tp5);

                if (!mIsMRUseReference)
                    LblReference.Visible = TxtReference.Visible = BtnReference.Visible = false;

                Tc1.SelectedTabPage = Tp1;

                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                if (mIsPICInMRMandatory) LblPICCode.ForeColor = Color.Red;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocNo, TxtPOQtyCancelDocNo, TxtRemainingBudget, TxtDORequestDocNo, TxtGrandTotalIndependentEstimatedPrice }, true);
                SetLueCurCode(ref LueCurCode);
                Sl.SetLueOption(ref LueDurationUom, "DurationUom");
                SetGrd();
                LueCurCode.Visible = false;
                LueDurationUom.Visible = false;
                if (mIsUseECatalog) Sl.SetLuePtCode(ref LuePtCode);
                LuePtCode.Visible = false;
                ChkRMInd.Visible = mIsUseECatalog;
                Sl.SetLueOption(ref LueProcurementType, "ProcurementType");
                if (!mIsMRUseProcurementType)
                {
                    LueProcurementType.Visible = false;
                    label22.Visible = false;
                }
                SetFormControl(mState.View);
                SetLuePICCode(ref LuePICCode);
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept?"Y":"N");
                ChkTenderInd.Visible = mIsMRWithTenderEnabled;
                mlJob = new List<Job>();

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            IsProcFormat = Sm.GetParameter("ProcFormatDocNo");
            mIsRemarkForApprovalMandatory = Sm.GetParameterBoo("IsMaterialRequestRemarkForApprovalMandatory");
            IsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mIsBudgetActive = Sm.GetParameterBoo("IsBudgetActive");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mIsDocNoWithDeptShortCode = Sm.GetParameterBoo("IsDocNoWithDeptShortCode");
            mIsDORequestNeedStockValidation = Sm.GetParameterBoo("IsDORequestNeedStockValidation");
            mIsDORequestUseItemIssued = Sm.GetParameterBoo("IsDORequestUseItemIssued");
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsUseItemConsumption = Sm.GetParameterBoo("IsUseItemConsumption");
            mIsPICInMRMandatory = Sm.GetParameterBoo("IsPICInMRMandatory");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsMRAutomaticGetLastVendorQuotation = Sm.GetParameterBoo("IsMRAutomaticGetLastVendorQuotation");
            mIsMaterialRequestShowReorderPoint = Sm.GetParameterBoo("IsMaterialRequestShowReorderPoint");
            mIsMaterialRequestShowMinimumStock = Sm.GetParameterBoo("IsMaterialRequestShowMinimumStock");
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mIsRemarkMRNotMandatory = Sm.GetParameterBoo("IsRemarkMRNotMandatory");
            mIsMRQuotationNotShow = Sm.GetParameterBoo("IsMRQuotationNotShow");
            mIsMRItemGroupNotShow = Sm.GetParameterBoo("IsMRItemGroupNotShow");
            mIsUsageDateMaterialRequestMandatory = Sm.GetParameterBoo("IsUsageDateMaterialRequestMandatory");
            mIsPICGrouped = Sm.GetParameterBoo("IsPICGrouped");
            mIsMRWithTenderEnabled = Sm.GetParameterBoo("IsMRWithTenderEnabled");

            if (mIsSiteMandatory)
            {
                mIsDeptFilterBySite = Sm.IsDataExist(
                    "Select 1 From TblDepartment A, TblDepartmentBudgetSite B " +
                    "Where A.DeptCode=B.DeptCode And A.ActInd='Y' " +
                    "And Exists(Select 1 from TblParameter " +
                    "Where ParCode='IsBudgetActive' And ParValue='Y') " +
                    "Limit 1;");
            }

            mIsPrintOutUseDigitalSignature = Sm.GetParameter("IsPrintOutUseDigitalSignature");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mIsMRAllowToUploadFile = Sm.GetParameterBoo("IsMRAllowToUploadFile");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsShowSeqNoInMaterialRequest = Sm.GetParameterBoo("IsShowSeqNoInMaterialRequest");
            mIsMaterialRequestMaxStockValidationEnabled = Sm.GetParameterBoo("IsMaterialRequestMaxStockValidationEnabled");
            mMRAvailableBudgetSubtraction = Sm.GetParameter("MRAvailableBudgetSubtraction");
            mIsMaterialRequestForDroppingRequestEnabled = Sm.GetParameterBoo("IsMaterialRequestForDroppingRequestEnabled");
            mIsMRSPPJBEnabled = Sm.GetParameterBoo("IsMRSPPJBEnabled");
            if (mIsMRSPPJBEnabled) mIsMRSPPJB = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForMRSPPJB"));
            mIsMRBudgetBasedOnBudgetCategory = Sm.GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mIsMaterialRequestDocNoUseDifferentAbbr = Sm.GetParameterBoo("IsMaterialRequestDocNoUseDifferentAbbr");
            mIsMaterialRequestSPPJBDocNoUseDifferentAbbr = Sm.GetParameterBoo("IsMaterialRequestSPPJBDocNoUseDifferentAbbr");
            mIsMRShowTotalPrice = Sm.GetParameterBoo("IsMRShowTotalPrice");

            mItGrpCodeNotShowOnMaterialRequest = Sm.GetParameter("ItGrpCodeNotShowOnMaterialRequest");

            mFormPrintOutMaterialRequestWithoutConsumption = Sm.GetParameter("FormPrintOutMaterialRequestWithoutConsumption");
            mFormPrintOutMaterialRequestWithConsumption = Sm.GetParameter("FormPrintOutMaterialRequestWithConsumption");
            mFormPrintOutMaterialRequestSPPJB = Sm.GetParameter("FormPrintOutMaterialRequestSPPJB");
            mFormPrintOutMaterialRequestSPPJB2 = Sm.GetParameter("FormPrintOutMaterialRequestSPPJB2");
            mIsMRDurationMandatory = Sm.GetParameterBoo("IsMRDurationMandatory");
            mIsMRApprovalByAmount = Sm.GetParameterBoo("IsMRApprovalByAmount");
            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mIsUseECatalog = Sm.GetParameterBoo("IsUseECatalog");
            mIsMRUseProcurementType = Sm.GetParameterBoo("IsMRUseProcurementType");
            mLabelFileForMR = Sm.GetParameter("LabelFileForMR");
            mMandatoryFileForMR = Sm.GetParameter("MandatoryFileForMR");
            mIsMRUseReview = Sm.GetParameterBoo("IsMRUseReview");
            mIsMRUseReference = Sm.GetParameterBoo("IsMRUseReference");
            mIsMRLocalDocNoFromReference = Sm.GetParameterBoo("IsMRLocalDocNoFromReference");
            mSourceAvailableBudgetForMR = Sm.GetParameter("SourceAvailableBudgetForMR");

            if (mFormPrintOutMaterialRequestWithoutConsumption.Length == 0) mFormPrintOutMaterialRequestWithoutConsumption = "MaterialRequest";
            if (mFormPrintOutMaterialRequestWithConsumption.Length == 0) mFormPrintOutMaterialRequestWithConsumption = "MaterialRequest2";
        }

        private void SetGrd()
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            #region Grid 1

            Grd1.Cols.Count = 47;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "Cancel Reason",
                        "",
                        "Status",
                        
                        
                        //6-10
                        "Checked by",
                        "",
                        "Item's Code",
                        "Local Code",
                        "",
                        
                        //11-15
                        "Item's Name",
                        "Foreign Name",
                        "Item Sub Category code",
                        "Sub Category",
                        "Minimum Stock",
                       
                        //16-20
                        "Reoder Point",
                        "Quantity",
                        "UoM",
                        "Usage Date",
                        "Quotation#",
                        
                        //21-25
                        "Quotation Dno",
                        "Quotation Date",
                        "Price",
                        "Total",
                        "Remark",

                        //26-30
                        "DORequestDocNo",
                        "DORequestDNo",
                        "Currency",
                        "Estimated Price",
                        "No.",

                        //31-35
                        "Dropping Request Quantity",
                        "Dropping Request's"+Environment.NewLine+"Amount",
                        "Specification",
                        "BOQ",
                        "Duration",

                        //36-40
                        "Duration UOM Code",
                        "Duration UOM",
                        "Total Price",
                        "PtCode",
                        "Payment Term",

                        //41-45
                        "",
                        "CityCode",
                        "Province",
                        "Delivery Location",
                        "Independent"+Environment.NewLine+"Estimated Price",

                        //46
                        "Total Independent"+Environment.NewLine+"Estimated Price"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 200, 20, 80, 
                        
                        //6-10
                        100, 20, 100, 150, 20,  
                        
                        //11-15
                        230, 200, 80, 100, 100, 
                        
                        //16-20
                        100, 120, 100, 100, 130,  
                        
                        //21-25
                        80, 100, 150, 150, 400,

                        //26-30
                        0, 0, 100, 120, 50,

                        //31-35
                        0, 130, 300, 50, 80,

                        //36-40
                        0, 150, 150, 0, 200,

                        //41-45
                        20, 0, 200, 200, 200,

                        //46
                        200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 23, 24, 29, 31, 32, 35, 38, 45, 46 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4, 7, 10, 34, 41 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 36 }, false);
            if (!mIsMRShowTotalPrice)
                Sm.GrdColInvisible(Grd1, new int[] { 38 });

            if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                Sm.GrdColInvisible(Grd1, new int[] { 7 });
            
            if (!mIsMRShowEstimatedPrice)
                Sm.GrdColInvisible(Grd1, new int[] { 28, 29 });
            
            if (mIsShowForeignName)
            {
                if (IsProcFormat == "1")
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 10, 21, 22, 23, 24, 13 }, false);
                else
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 10, 21, 22, 23, 24, 13, 14 }, false);
            }
            else
            {
                if (IsProcFormat == "1")
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 10, 12, 13, 21, 22, 23, 24 }, false);
                else
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14, 21, 22, 23, 24 }, false);
            }
            Sm.GrdColInvisible(Grd1, new int[] { 30 }, mIsShowSeqNoInMaterialRequest);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46 });
            if (!mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 39, 40, 41, 42, 43, 44 });
            fCopyPasteManager = new iGCopyPasteManager(Grd1);
            Grd1.Cols[28].Move(25);
            Grd1.Cols[29].Move(26);
            Grd1.Cols[38].Move(27);
            Grd1.Cols[30].Move(0);
            Grd1.Cols[45].Move(32);
            Grd1.Cols[46].Move(33);
            
            if (Doctitle == "SIER")
            {
                if (!mIsMaterialRequestForDroppingRequestEnabled && !mIsMRSPPJB)
                    Sm.GrdColInvisible(Grd1, new int[] { 32 });
                
            }
            else
            {
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 23, 24, 32 }, true);
                    Grd1.Cols[32].Move(26);
                }
            }
            if (!mIsMRSPPJB)
            {
                if (!mIsMaterialRequestShowMinimumStock)
                    Sm.GrdColInvisible(Grd1, new int[] { 15 });
                if (!mIsMaterialRequestShowReorderPoint)
                    Sm.GrdColInvisible(Grd1, new int[] { 16 });
            }
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 33 });
            Grd1.Cols[33].Move(13);

            if (!mIsMRSPPJB)
                Sm.GrdColInvisible(Grd1, new int[] { 34 }, false);
            else
                Grd1.Cols[34].Move(28);
            


            if(mIsMRQuotationNotShow)
                Sm.GrdColInvisible(Grd1, new int[] { 20 });

            if (!mIsUsageDateMaterialRequestMandatory)
                Grd1.Cols[19].Visible = false;

            #endregion

            #region Grid Review
            GrdReview.Cols.Count = 6;
            GrdReview.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                GrdReview,
                new string[]
                { 
                     //0
                    "No",
                    
                    //1-5
                    "Item's Code", 
                    "Item's Name",
                    "Specification",
                    "Reviewed By",
                    "Remark Review"

                },
                new int[] { 50, 150, 150, 250, 100, 150 }
            );
            Sm.GrdColReadOnly(true, false, GrdReview, new int[] { 0, 1, 2, 3, 4, 5 });
            Sm.GrdColInvisible(GrdReview, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 8, 9, 10, 20, 22 }, !ChkHideInfoInGrd.Checked);
            if(!mIsUsageDateMaterialRequestMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, !ChkHideInfoInGrd.Checked);
            if (mIsMRUseReview)
                Sm.GrdColInvisible(GrdReview, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, LueBCCode, 
                       LueSiteCode, MeeRemark, TxtFile, TxtFile2, TxtFile3, LuePICCode, 
                       ChkTenderInd, DteExpDt, LueProcurementType, ChkRMInd,
                       TxtReference, TxtMRDocNo, MeeCancelReason, ChkCancelInd,

                       TxtFile4, TxtFile5, TxtFile6, TxtFile7

                    }, true);
                    ChkAddendumInd.Properties.ReadOnly = true;
                    BtnPOQtyCancelDocNo.Enabled = false;
                    BtnDORequestDocNo.Enabled = false;
                    BtnMRDocNo.Enabled = false;
                    BtnReference.Enabled = false;
                    if (!mIsDORequestNeedStockValidation)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 29, 33, 35, 37, 40, 41, 45, 46 });

                    SetControlEnabled(new List<Control>
                    {
                        BtnFile, BtnDownload, BtnFile2, BtnDownload2, BtnFile3, BtnDownload3,
                        BtnFile4, BtnDownload4, BtnFile5, BtnDownload5, BtnFile6, BtnDownload6,
                        BtnFile7, BtnDownload7, ChkFile, ChkFile2, ChkFile3, ChkFile4, ChkFile5,
                        ChkFile6, ChkFile7
                    }, false);

                    if (TxtDocNo.Text.Length > 0)
                    {
                        SetControlEnabled(new List<Control>
                        {
                            BtnFile, BtnDownload, BtnFile2, BtnDownload2, BtnFile3, BtnDownload3,
                            BtnFile4, BtnDownload4, BtnFile5, BtnDownload5, BtnFile6, BtnDownload6,
                            BtnFile7, BtnDownload7
                        }, true);
                    }
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 45 });
                    SetControlEnabled(new List<Control>
                        {
                            BtnMRDocNo, BtnFile, BtnDownload, BtnFile2, BtnDownload2, BtnFile3, BtnDownload3,
                            BtnFile4, BtnDownload4, BtnFile5, BtnDownload5, BtnFile6, BtnDownload6,
                            BtnFile7, BtnDownload7, ChkFile, ChkFile2, ChkFile3, ChkFile4, ChkFile5,
                            ChkFile6, ChkFile7
                        }, true);
                    ChkAddendumInd.Properties.ReadOnly = false;
                    BtnMRDocNo.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 45 });
                    TxtDocNo.Focus();
                    if (!mIsDORequestNeedStockValidation)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    break;
            }
        }

        private void ClearData()
        {
            if (mIsMRSPPJB) mlJob.Clear();
            mDroppingRequestBCCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, TxtFile,
                LueSiteCode, TxtPOQtyCancelDocNo, LueBCCode, MeeRemark, TxtDORequestDocNo, 
                LuePICCode, TxtFile2, TxtFile3, DteExpDt, LueProcurementType,
                TxtFile4, TxtFile5, TxtFile6, TxtFile7, TxtReference, TxtMRDocNo, MeeCancelReason, ChkCancelInd, 
                ChkAddendumInd
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtRemainingBudget, TxtGrandTotalIndependentEstimatedPrice
            }, 0);
            ClearGrd();
            ChkAddendumInd.Checked = false;
            ChkCancelInd.Checked = false;
            ChkTenderInd.Checked = false;
            ChkRMInd.Checked = false;

            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
            ChkFile4.Checked = false;
            PbUpload4.Value = 0;
            ChkFile5.Checked = false;
            PbUpload5.Value = 0;
            ChkFile6.Checked = false;
            PbUpload6.Value = 0;
            ChkFile7.Checked = false;
            PbUpload7.Value = 0;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            if (mIsMRUseReview) Sm.ClearGrd(GrdReview, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 15, 16, 17, 23, 24, 29, 31, 32, 35, 46 });
            if (mIsBudgetActive) Grd1.Cells[0, 11].ForeColor = Color.Black;
            SetSeqNo();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIndependentEstimatedPriceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            InsertData();
        }

        private void InsertData()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueReqType(ref LueReqType, string.Empty);
                if (mReqTypeForNonBudget.Length > 0) Sm.SetLue(LueReqType, mReqTypeForNonBudget);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || (Sm.GetParameter("DocTitle") == "KIM" && ShowPrintApproval()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile4_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile4.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile4.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile5_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile5.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile5.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile6_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile6.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile6.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile7_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile7.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile7.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {

            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {

            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload4_Click(object sender, EventArgs e)
        {
            DownloadFile4(mHostAddrForFTPClient, mPortForFTPClient, TxtFile4.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile4.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile4, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload5_Click(object sender, EventArgs e)
        {
            DownloadFile5(mHostAddrForFTPClient, mPortForFTPClient, TxtFile5.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile5.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile5, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload6_Click(object sender, EventArgs e)
        {
            DownloadFile6(mHostAddrForFTPClient, mPortForFTPClient, TxtFile6.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile6.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile6, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload7_Click(object sender, EventArgs e)
        {
            DownloadFile6(mHostAddrForFTPClient, mPortForFTPClient, TxtFile7.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile7.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile7, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion 

        #region Grid Method

        #region Grid 1

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            //if (BtnSave.Enabled)
            //{
            //    if (TxtDocNo.Text.Length == 0)
            //    {
            //        if (mIsMRSPPJB && e.RowIndex != 0) 
            //        {
            //            e.DoDefault = false;
            //            return;
            //        }
            //        if (e.ColIndex == 7 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
            //        {
            //            e.DoDefault = false;
            //            if (e.KeyChar == Char.Parse(" "))
            //                Sm.FormShowDialog(new FrmIndependentEstimatedPriceDlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));
            //        }

            //        //if (e.ColIndex == 17 && TxtDroppingRequestDocNo.Text.Length>0)
            //        //    e.DoDefault = false;

            //        if (Sm.IsGrdColSelected(new int[] { 3, 17, 19, 25 }, e.ColIndex))
            //        {
            //            if (e.ColIndex == 19) Sm.DteRequestEdit(Grd1, DteUsageDt, ref fCell, ref fAccept, e);
            //            if (mIsDORequestNeedStockValidation)
            //            {
            //                if (!(mIsMRSPPJB && Grd1.Rows.Count > 1))
            //                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            //            }
            //            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            //            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 23, 24, 29, 31, 32 });
            //        }

            //        if (e.ColIndex == 28)
            //        {
            //            LueRequestEdit(Grd1, LueCurCode, ref fCell, ref fAccept, e);
            //            if (mIsDORequestNeedStockValidation)
            //            {
            //                if (!(mIsMRSPPJB && Grd1.Rows.Count > 1))
            //                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            //            }
            //        }

            //        if (e.ColIndex == 37)
            //        {
            //            LueRequestEdit(Grd1, LueDurationUom, ref fCell, ref fAccept, e);
            //            Sm.GrdRequestEdit(Grd1, e.RowIndex);                      
            //        }

            //        if (e.ColIndex == 40 && mIsUseECatalog && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 11, false, "Item is empty."))
            //        {
            //            LueRequestEdit(Grd1, LuePtCode, ref fCell, ref fAccept, e);
            //            Sm.GrdRequestEdit(Grd1, e.RowIndex);                      
            //        }
            //    }
            //    else
            //    {
            //        if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length == 0))
            //            e.DoDefault = false;
            //    }
            //}

            //if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0 ) ShowMRApprovalInfo(e.RowIndex);
            //if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            //{
            //    e.DoDefault = false;
            //    var f = new FrmItem(mMenuCode);
            //    f.Tag = mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
            //    f.ShowDialog();
            //}
            //if (e.ColIndex == 34 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            //    Sm.FormShowDialog(new FrmIndependentEstimatedPriceDlg5(
            //        this, 
            //        e.RowIndex, 
            //        TxtDocNo.Text.Length == 0 && BtnSave.Enabled
            //        ));
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //diremark oleh TKG on 14/8/2018
            //if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //if (mIsDORequestNeedStockValidation)
            //{
            if (TxtDocNo.Text.Length == 0)
            {
                if (mIsMRSPPJB)
                {
                    var ItCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                    mlJob.RemoveAll(x =>Sm.CompareStr(x.ItCode, ItCode));
                }
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                if (mSourceAvailableBudgetForMR == "1") ComputeRemainingBudget();
                SetSeqNo();
            }
            //}
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e) //
        {
            if (mIsMRSPPJB && e.RowIndex != 0) return;
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0) ShowMRApprovalInfo(e.RowIndex);
            //if (e.ColIndex == 7 && TxtDocNo.Text.Length == 0 && Sm.IsTxtEmpty(TxtMRDocNo, "MR#", false))
            //    Sm.FormShowDialog(new FrmIndependentEstimatedPriceDlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
            //if (e.ColIndex == 34 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            //    Sm.FormShowDialog(new FrmIndependentEstimatedPriceDlg5(
            //        this,
            //        e.RowIndex,
            //        TxtDocNo.Text.Length == 0 && BtnSave.Enabled
            //        ));

            //if (mIsUseECatalog && e.ColIndex == 41 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 11, false, "Item is empty."))
            //{
            //    Sm.FormShowDialog(new FrmIndependentEstimatedPriceDlg7(this, e.RowIndex));
            //}
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 25 }, e);

            if (e.ColIndex == 17 || e.ColIndex == 29)
            {
                ComputeTotal(e.RowIndex);
            }
            if (e.ColIndex == 17 || e.ColIndex == 29)
            {
                ComputeTotalPrice(e.RowIndex);
            }
            if(e.ColIndex == 17 || e.ColIndex == 45)
            {
                ComputeTotalIndependentPrice(e.RowIndex);
            }
            if(e.ColIndex == 45)
            {
                ComputeGrandTotalIndependentEstimatedPrice();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 19)
            {
                if (Sm.GetGrdDate(Grd1, 0, 19).Length != 0)
                {
                    var UsageDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 19));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) Grd1.Cells[Row, 19].Value = UsageDt;
                }
            }

            if (e.ColIndex == 25)
            {
                var Remark = Sm.GetGrdStr(Grd1, 0, 25);
                if (Remark.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) Grd1.Cells[Row, 25].Value = Remark;
                }
            }

            if (e.ColIndex == 28)
            {
                var CurCode = Sm.GetGrdStr(Grd1, 0, 28);
                if (CurCode.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) 
                            Grd1.Cells[Row, 28].Value = CurCode;
                }
            }

            if (e.ColIndex == 37)
            {
                var DurationUOM = Sm.GetGrdStr(Grd1, 0, 37);
                if (DurationUOM.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                            Grd1.Cells[Row, 37].Value = DurationUOM;
                }
            }

            if (e.ColIndex == 29)
            {
                decimal Total = 0m;

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0)
                        Total += Sm.GetGrdDec(Grd1, Row, 29);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        override protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 19)
                e.Text = "Double click title to copy data based on the first row's value.";
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;
            string DocNo = string.Empty; 
            string LocalDocNo = string.Empty;
 
            Cursor.Current = Cursors.WaitCursor;

            string DeptCode = Sm.GetLue(LueDeptCode);
            string SubCategory = Sm.GetGrdStr(Grd1, 0, 13);
            DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "IndependentEstimatedPrice", "TblIndependentEstimatedPriceHdr", SubCategory);
            LocalDocNo = TxtLocalDocNo.Text;
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveIndependentEstimatedPriceHdr(DocNo, LocalDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                {
                    cml.Add(SaveIndependentEstimatedPriceDtl(DocNo, Row));
                }
            }
            
            Sm.ExecCommands(cml);
            if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);
            if (TxtFile4.Text.Length > 0 && TxtFile4.Text != "openFileDialog1")
                UploadFile4(DocNo);
            if (TxtFile5.Text.Length > 0 && TxtFile5.Text != "openFileDialog1")
                UploadFile5(DocNo);
            if (TxtFile6.Text.Length > 0 && TxtFile6.Text != "openFileDialog1")
                UploadFile6(DocNo);
            if (TxtFile7.Text.Length > 0 && TxtFile7.Text != "openFileDialog1")
                UploadFile7(DocNo);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtMRDocNo, "Material Request", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsFileMandatory() ||
                IsUploadFileNotValid() ||
                IsUploadFileNotValid2() ||
                IsUploadFileNotValid3() ||
                IsUploadFileNotValid4() ||
                IsUploadFileNotValid5() ||
                IsUploadFileNotValid6() ||
                IsUploadFileNotValid7() ;
        }

        private bool IsJobCurCodeInvalid()
        {
            string ItCode = string.Empty;
            string CurCode = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 28, false, "Estimated currency is empty.") ||
                        Sm.IsGrdValueEmpty(Grd1, r, 29, true, "Estimated price should be bigger than 0.00."))
                        return true;
                    ItCode = Sm.GetGrdStr(Grd1, r, 8);
                    CurCode = Sm.GetGrdStr(Grd1, r, 28);
                    foreach (var i in mlJob)
                    {
                        if (Sm.CompareStr(ItCode, i.ItCode))
                        {
                            if (!Sm.CompareStr(CurCode, i.CurCode))
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                    "Item's Code : " + ItCode + Environment.NewLine +
                                    "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                                    "Job's Code : " + i.JobCode + Environment.NewLine +
                                    "Job's Name : " + i.JobName + Environment.NewLine +
                                    "Job's Currency : " + i.CurCode + Environment.NewLine +
                                    "Estimated Currency : " + CurCode + Environment.NewLine + Environment.NewLine +
                                    "Invalid job's currency."
                                    );
                                return true;
                            }
                        }
                    }
                }

            }
            return false;
        }

        private bool IsDR_QtyInvalid()
        {
            decimal Qty1 = 0m, Qty2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty1 = 0m;  
                Qty2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 17).Length != 0) Qty1 = Sm.GetGrdDec(Grd1, r, 17);
                if (Sm.GetGrdStr(Grd1, r, 31).Length != 0) Qty2 = Sm.GetGrdDec(Grd1, r, 31);

                if (Qty1 > Qty2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                        "Requested Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 17), 0) + Environment.NewLine +
                        "Dropping Request's Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 31), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Quantity is bigger than Dropping Request's Quantity.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_AmtInvalid()
        {
            decimal Amt1 = 0m, Amt2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Amt1 = 0m;
                Amt2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 24).Length != 0) Amt1 = Sm.GetGrdDec(Grd1, r, 24);
                if (Sm.GetGrdStr(Grd1, r, 32).Length != 0) Amt2 = Sm.GetGrdDec(Grd1, r, 32);

                if (Amt1 > Amt2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                        "Requested Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 24), 0) + Environment.NewLine +
                        "Dropping Request's Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 32), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Amount is bigger than Dropping Request's Amount.");
                    return true;
                }
            }
            return false;
        }

        private bool IsMaxStockInvalid()
        {
            //membandingkan maximum stock dengan quantity yg diminta + stock semua warehouse + outang MR yg belum di-PO-kan.
            //MR yg belum di-received inidikator belum ada/Uom bisa beda/proses bisa lama
            //Hanya yg maximum stocknya diisi dan uom purchase dan inventory-nya sama

            if (!mIsMaterialRequestMaxStockValidationEnabled) return false;

            string ItCode = string.Empty, Filter = string.Empty, Filter2 = string.Empty;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 8);
                if (ItCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.ItCode=@ItCode0" + r.ToString() + ") ";
                    
                    if (Filter2.Length > 0) Filter2 += " Union All ";
                    Filter2 += " Select @ItCode0" + r.ToString() + " As ItCode, @Qty0" + r.ToString() + " As Qty ";

                    Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    Sm.CmParam<Decimal>(ref cm, "@Qty0" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                }
            }

            if (Filter.Length == 0)
                return false;
            else
                Filter = " And ( " + Filter + " ) ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.MaxStock, IfNull(B.Qty, 0.00) As Stock, IfNull(C.Qty, 0.00) As OutstandingMR, IfNull(D.Qty, 0.00) As MRQty  ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select A.ItCode, Sum(A.Qty) As Qty  ");
            SQL.AppendLine("    From TblStockSummary A ");
            SQL.AppendLine("    Where A.Qty>0.00 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Group By A.ItCode ");
            SQL.AppendLine("    ) B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.ItCode, Sum(T2.Qty) As Qty  ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And T2.ProcessInd In ('O', 'P') ");
            SQL.AppendLine(Filter.Replace("A.", "T2."));
            SQL.AppendLine("    Where T1.CancelInd='N' And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By T2.ItCode ");
            SQL.AppendLine("    ) C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    ) D On A.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.MaxStock>0.00 ");
            SQL.AppendLine("And A.PurchaseUomCode=A.InventoryUomCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And A.MaxStock<IfNull(B.Qty, 0.00)+IfNull(C.Qty, 0.00)+IfNull(D.Qty, 0.00) ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "ItCode", 
                        "ItName", "MaxStock", "Stock", "OutstandingMR", "MRQty"
                    });

                if (dr.HasRows)
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sm.StdMsg(mMsgType.Warning,
                            "Item's Code : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Item's Name : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Maximum Stock : " + Sm.FormatNum(Sm.DrDec(dr, 2), 0) + Environment.NewLine +
                            "Current Stock : " + Sm.FormatNum(Sm.DrDec(dr, 3), 0) + Environment.NewLine +
                            ((Doctitle == "IMS") ? "Outstanding Purchase Request : " : "Outstanding Material Request : ") + Sm.FormatNum(Sm.DrDec(dr, 4), 0) + Environment.NewLine +
                            "Requested Quantity : " + Sm.FormatNum(Sm.DrDec(dr, 5), 0) + Environment.NewLine + Environment.NewLine +
                            "Invalid maximum stock."
                            );
                            return true;
                        }
                    }
                }
                dr.Close();
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 45, true, "Estimated price should be bigger than 0.00."))
                    return true;
                if (Sm.GetGrdDec(Grd1, Row, 45) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value should not be less than 0.");
                    return true;
                }
            }
            return false;
        }

        private bool IsUsageDtNotValid(string DocDt, int Row)
        {
            var UsageDt = Sm.GetGrdDate(Grd1, Row, 19);
            if (Sm.CompareDtTm(UsageDt, DocDt) < 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Document Date : " + Sm.FormatDate2("dd/MMM/yyyy", DocDt) + Environment.NewLine +
                    "Usage Date : " + Sm.FormatDate2("dd/MMM/yyyy", UsageDt) + Environment.NewLine + Environment.NewLine +
                    "Usage date is earlier than document date.");
                Sm.FocusGrd(Grd1, Row, 19);
                return true;
            }
            return false;
        }

        private bool IsRemainingBudgetNotValid()
        {
            decimal RemainingBudget = 0m;

            if (TxtRemainingBudget.Text.Length != 0) RemainingBudget = decimal.Parse(TxtRemainingBudget.Text);
           //harusnya <0
            if (RemainingBudget<0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid remaining budget.");
                return true;
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where UserCode Is Not Null ");
            if (mIsMRSPPJB)
                SQL.AppendLine("And DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("And DocType = 'MaterialRequest' ");
            SQL.AppendLine("And DeptCode Is Not Null ");
            SQL.AppendLine("And DeptCode = @Param1 ");
            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("And SiteCode Is Not Null ");
                SQL.AppendLine("And SiteCode=@Param2 ");
            }

            SQL.AppendLine("Limit 1; ");

            if (!Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode), string.Empty)) 
                return true;
            else 
                return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 13).Length == 0)
                {
                    Grd1.Cells[Row, 13].Value = Grd1.Cells[Row, 14].Value = "XXX";
                }
            }
        }

        private bool IsSubCategoryXXX()
        {
            if (IsProcFormat == "1")
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14) == "XXX")
                    {
                        Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine;

                        Sm.StdMsg(mMsgType.Warning, Msg + "doesn't have Sub-Category.");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 13);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 13))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsPOQtyCancelDocNoNotValid()
        {
            if (TxtPOQtyCancelDocNo.Text.Length == 0) return false;

            var cm = new MySqlCommand() 
            { 
                CommandText = 
                "Select DocNo From TblPOQtyCancel " +
                "Where DocNo=@Param And CancelInd='N' And ProcessInd='O' And NewMRInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@Param", TxtPOQtyCancelDocNo.Text);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Cancellation PO# is invalid.");
                return true;
            }
            return false;
        }

        private bool IsCancelReasonEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveIndependentEstimatedPriceHdr(string DocNo, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIndependentEstimatedPriceHdr(DocNo, DocDt, AddendumInd, CancelInd, MRDocNo, GrandTotalIndependentEstimatedPrice, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @AddendumInd, 'N', @MRDocNo, @GrandTotalIndependentEstimatedPrice, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AddendumInd", ChkAddendumInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@MRDocNo", TxtMRDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@GrandTotalIndependentEstimatedPrice", decimal.Parse(TxtGrandTotalIndependentEstimatedPrice.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveIndependentEstimatedPriceDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIndependentEstimatedPriceDtl(DocNo, DNo, MRDocNo, MRDNo, IndependentEstPrice, CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DNo, @MRDocNo, @MRDNo, @IndependentEstPrice, @CreateBy, CurrentDateTime()); ");
        
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@MRDocNo", TxtMRDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MRDNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@IndependentEstPrice", Sm.GetGrdDec(Grd1, Row, 45));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveMaterialRequestDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            foreach (var x in mlJob)
            {
                SQL.AppendLine("Insert Into TblMaterialRequestDtl2 ");
                SQL.AppendLine("(DocNo, ItCode, JobCode, PrevCurCode, PrevUPrice, CurCode, UPrice, Qty, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");
                SQL.AppendLine("(@DocNo, @ItCode" + i + ", @JobCode" + i + ", @PrevCurCode" + i + ", @PrevUPrice" + i + ", @CurCode" + i +", @UPrice" + i + ", @Qty"+ i +", @Remark" + i + ", @UserCode, @Dt); ");

                SQL.AppendLine("Update TblJob Set ");
                SQL.AppendLine("    CurCode=@CurCode" + i +", UPrice=@UPrice" + i);
                SQL.AppendLine(" Where JobCode=@JobCode" + i + ";");

                Sm.CmParam<String>(ref cm, "@ItCode" + i, x.ItCode);
                Sm.CmParam<String>(ref cm, "@JobCode" + i, x.JobCode);
                Sm.CmParam<String>(ref cm, "@PrevCurCode" + i, x.PrevCurCode);
                Sm.CmParam<Decimal>(ref cm, "@PrevUPrice" + i, x.PrevUPrice);
                Sm.CmParam<String>(ref cm, "@CurCode" + i, x.CurCode);
                Sm.CmParam<Decimal>(ref cm, "@UPrice" + i, x.UPrice);
                Sm.CmParam<Decimal>(ref cm, "@Qty" + i, x.Qty);
                Sm.CmParam<String>(ref cm, "@Remark" + i, x.Remark);

                i++;
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveMaterialRequestDtl3(string DocNo, int i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblMaterialRequestDtl3(DocNo, DNo, SectorCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @SectorCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", i.ToString()), 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelIndependentEstimatedPriceHdr(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select DNo, CancelInd From TblMaterialRequestDtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready()
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblIndependentEstimatedPriceHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledItemCheckedAlready(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPORequestDtl ");
            SQL.AppendLine("Where MaterialRequestDocNo=@DocNo ");
            SQL.AppendLine("And (CancelInd='N' And IfNull(Status, 'O')<>'C') ");
            SQL.AppendLine("And MaterialRequestDNo In (" + DNo + ") ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been processed.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelIndependentEstimatedPriceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIndependentEstimatedPriceHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);            

            return cm;
        }

        private MySqlCommand CancelMaterialRequestDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelReason=@CancelReason ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@Dno ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));

            return cm;
        }

        private MySqlCommand UpdateMRFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIndependentEstimatedPriceHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIndependentEstimatedPriceHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIndependentEstimatedPriceHdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile4(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIndependentEstimatedPriceHdr Set ");
            SQL.AppendLine("    FileName4=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile5(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIndependentEstimatedPriceHdr Set ");
            SQL.AppendLine("    FileName5=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile6(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIndependentEstimatedPriceHdr Set ");
            SQL.AppendLine("    FileName6=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile7(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIndependentEstimatedPriceHdr Set ");
            SQL.AppendLine("    FileName7=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowIndependentEstimatedPriceHdr(DocNo);
                ShowIndependentEstimatedPriceDtl(DocNo, false);
                ComputeGrandTotalIndependentEstimatedPrice();
                ShowMaterialRequestReview(TxtMRDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void ShowMaterialRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, LocalDocNo, DocDt, POQtyCancelDocNo, ");
            SQL.AppendLine("SiteCode, DeptCode, PICCode, ReqType, DORequestDocNo, BCCode, FileName, filename2, filename3, Remark, DroppingRequestDocNo, DroppingRequestBCCode, ");
            SQL.AppendLine("ProcurementType, ");

            if (mSourceAvailableBudgetForMR == "2") SQL.AppendLine("AvailableBudget, ");
            else SQL.AppendLine("Null As AvailableBudget, ");

            if (mIsUseECatalog) SQL.AppendLine("ExpDt, RMInd, ");
            else SQL.AppendLine("Null As ExpDt, 'N' AS RMInd,");

            if (mIsMRWithTenderEnabled) SQL.AppendLine("TenderInd ");
            else SQL.AppendLine("'N' As TenderInd ");

            SQL.AppendLine(", FileName4, FileName5, FileName6, ReferenceDocNo ");

            SQL.AppendLine("From TblMaterialRequestHdr Where DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DocNo", 
                    "LocalDocNo", "DocDt", "POQtyCancelDocNo", "SiteCode", "DeptCode",  
                    "ReqType", "BCCode", "FileName", "Remark", "DORequestDocNo",
                    "PICCode", "DroppingRequestDocNo", "DroppingRequestBCCode", "filename2", "filename3",
                    "TenderInd", "ExpDt", "ProcurementType", "FileName4", "FileName5",
                    "FileName6", "ReferenceDocNo", "RMInd", "AvailableBudget"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtMRDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), "N");
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]), "N");
                    SetLueReqType(ref LueReqType, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LueReqType, Sm.DrStr(dr, c[6]));
                    Sl.SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[7]), string.Empty);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    TxtDORequestDocNo.EditValue = Sm.DrStr(dr, c[10]);
                    mDroppingRequestBCCode = Sm.DrStr(dr, c[13]);
                    Sl.SetLueUserCode(ref LuePICCode, Sm.DrStr(dr, c[11]));
                    ChkTenderInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[16]), "Y") ? true : false;
                    if (mIsUseECatalog) Sm.SetDte(DteExpDt, Sm.DrStr(dr, c[17]));
                    if (mIsMRUseProcurementType) Sm.SetLue(LueProcurementType, Sm.DrStr(dr, c[18]));

                    TxtReference.EditValue = Sm.DrStr(dr, c[22]);
                    ChkRMInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[23]), "Y") ? true : false;
                    if (mSourceAvailableBudgetForMR == "2") TxtRemainingBudget.EditValue = String.Format("{0:#,##0.00}", Sm.DrDec(dr, c[24]));

                }, true
            );
        }

        internal void ShowIndependentEstimatedPriceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.AddendumInd, A.CancelInd, A.CancelReason, A.GrandTotalIndependentEstimatedPrice, B.DocNo MRDocNo, B.LocalDocNo, ");
            SQL.AppendLine("B.POQtyCancelDocNo, B.SiteCode, B.DeptCode, B.PICCode, B.ReqType, B.DORequestDocNo, B.BCCode, B.Remark, B.DroppingRequestDocNo, ");

            SQL.AppendLine("B.DroppingRequestBCCode, B.ProcurementType, ");

            if (mIsUseECatalog) SQL.AppendLine("B.ExpDt, B.RMInd, ");
            else SQL.AppendLine("Null As ExpDt, 'N' AS RMInd,");

            if (mIsMRWithTenderEnabled) SQL.AppendLine("B.TenderInd, ");
            else SQL.AppendLine("'N' As TenderInd, ");

            SQL.AppendLine("B.ReferenceDocNo, ");
            SQL.AppendLine("A.FileName, A.FileName2, A.FileName3, A.FileName4, A.FileName5, A.FileName6, A.FileName7 ReviewFile ");
            SQL.AppendLine("From TblIndependentEstimatedPriceHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.MRDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "MRDocNo",
 
                    //1-5
                    "LocalDocNo", "DocDt", "POQtyCancelDocNo", "SiteCode", "DeptCode",  

                    //6-10
                    "ReqType", "BCCode", "Remark", "DORequestDocNo", "PICCode",

                    //11-15
                    "DroppingRequestDocNo", "DroppingRequestBCCode", "TenderInd", "ExpDt", "ProcurementType", 

                    //16-20
                    "ReferenceDocNo", "RMInd", "DocNo", "AddendumInd", "CancelReason", 

                    //21-25
                    "CancelInd", "GrandTotalIndependentEstimatedPrice", "FileName", "FileName2", "FileName3", 

                    //26-29
                    "FileName4", "FileName5", "FileName6", "ReviewFile",

                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtMRDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), "N");            
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]), "N");
                    SetLueReqType(ref LueReqType, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LueReqType, Sm.DrStr(dr, c[6]));
                    Sl.SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[7]), string.Empty);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    TxtDORequestDocNo.EditValue = Sm.DrStr(dr, c[9]);
                    Sl.SetLueUserCode(ref LuePICCode, Sm.DrStr(dr, c[10]));
                    mDroppingRequestBCCode = Sm.DrStr(dr, c[12]);
                    ChkTenderInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[13]), "Y") ? true : false;
                    if (mIsUseECatalog) Sm.SetDte(DteExpDt, Sm.DrStr(dr, c[14]));
                    if (mIsMRUseProcurementType) Sm.SetLue(LueProcurementType, Sm.DrStr(dr, c[15]));
                    TxtReference.EditValue = Sm.DrStr(dr, c[16]);
                    ChkRMInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[17]), "Y") ? true : false;
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[18]);
                    ChkAddendumInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[19]), "Y") ? true : false;
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[20]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[21]), "Y") ? true : false;
                    TxtGrandTotalIndependentEstimatedPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                    TxtFile.EditValue = Sm.DrStr(dr, c[23]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[24]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[25]);
                    TxtFile4.EditValue = Sm.DrStr(dr, c[26]);
                    TxtFile5.EditValue = Sm.DrStr(dr, c[27]);
                    TxtFile6.EditValue = Sm.DrStr(dr, c[28]);
                    TxtFile7.EditValue = Sm.DrStr(dr, c[29]);

                }, true
            );
        }

        internal void ShowMaterialRequestData(string DocNo)
        {
            ShowMaterialRequestHdr(DocNo);
            ShowMaterialRequestDtl(DocNo, false);
            ShowMaterialRequestReview(DocNo);
            if (mSourceAvailableBudgetForMR == "1") ComputeRemainingBudget();
        }

        internal void ShowMaterialRequestDtl(string DocNo, bool ReferenceInd)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, ");
            SQL.AppendLine(" A.CancelInd, A.CancelReason, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
            SQL.AppendLine("(   Select T2.UserName From TblDocApproval T1, TblUser T2 ");
            if (mIsMRSPPJB)
                SQL.AppendLine("    Where T1.DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("    Where T1.DocType='MaterialRequest' ");
            SQL.AppendLine("    And T1.DocNo=@DocNo And T1.DNo=A.DNo And T1.UserCode=T2.UserCode And T1.UserCode Is Not Null ");
            SQL.AppendLine("    Order By ApprovalDNo Desc Limit 1");
            SQL.AppendLine(") As UserName, A.DORequestDocNo, A.DORequestDNo,  ");
            SQL.AppendLine("A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName,  B.ItScCode, D.ItScName, B.MinStock, B.ReorderStock, A.Qty, B.PurchaseUomCode, B.Specification, ");
            SQL.AppendLine("A.UsageDt, A.QtDocNo, A.QtDNo, C.DocDt, A.UPrice, (A.Qty*A.UPrice) As Total, TotalPrice, A.Remark, A.CurCode, A.EstPrice, A.Duration, E.OptDesc DurationUom, ");

            if (mIsUseECatalog) SQL.AppendLine("A.PtCode, F.PtName, A.CityCode, H.ProvName, G.CityName, ");
            else SQL.AppendLine("Null AS PtCode, Null As PtName, Null As CityCode, Null As ProvName, Null As CityName, ");
            SQL.AppendLine("0.00 As IndependentEstPrice, ");
            {
                SQL.AppendLine("0.00 As DroppingRequestAmt ");
            }

            SQL.AppendLine("From TblMaterialRequestDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemSubCategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Left Join TblOption E On E.OptCode = A.DurationUom And E.OptCat = 'DurationUom' ");
            if (mIsUseECatalog)
            {
                SQL.AppendLine("Left Join TblPaymentTerm F On A.PtCode = F.PtCode ");
                SQL.AppendLine("Left Join TblCity G On A.CityCode = G.CityCode ");
                SQL.AppendLine("Left Join TblProvince H On G.ProvCode = H.ProvCode ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Order By A.ItCode");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "CancelReason", "StatusDesc", "UserName", "ItCode",  
                    
                    //6-10
                    "ItCodeInternal", "ItName", "ForeignName", "ItScCode", "ItScName",  
                    
                    //11-15
                    "MinStock", "ReorderStock", "Qty", "PurchaseUomCode", "UsageDt", 
                    
                    //16-20
                    "QtDocNo", "QtDNo", "DocDt", "UPrice", "Total", 
                    
                    //21-25
                    "Remark", "DORequestDocNo", "DORequestDNo", "CurCode", "EstPrice",
                    
                    //26-30
                    "DroppingRequestAmt", "Specification", "Duration", "DurationUom", "TotalPrice",

                    //31-35
                    "PtCode", "PtName", "CityCode", "ProvName", "CityName",

                    //36-37
                    "IndependentEstPrice"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 25);
                    Grd.Cells[Row, 31].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 27);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 29);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 31);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 32);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 33);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 34);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 35);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 36);
                    Grd.Cells[Row, 46].Value = Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 45);

                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        //ReferenceInd digunakan di MaterialRequestDlg8, yg mana ketika choose data, hanya yg approvalnya cancel
        internal void ShowIndependentEstimatedPriceDtl(string DocNo, bool ReferenceInd)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select I.DNo, ");
            SQL.AppendLine(" A.CancelInd, A.CancelReason, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
            SQL.AppendLine("(   Select T2.UserName From TblDocApproval T1, TblUser T2 ");
            if (mIsMRSPPJB)
                SQL.AppendLine("    Where T1.DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("    Where T1.DocType='MaterialRequest' ");
            SQL.AppendLine("    And T1.DocNo=@DocNo And T1.DNo=A.DNo And T1.UserCode=T2.UserCode And T1.UserCode Is Not Null ");
            SQL.AppendLine("    Order By ApprovalDNo Desc Limit 1");
            SQL.AppendLine(") As UserName, A.DORequestDocNo, A.DORequestDNo,  ");
            SQL.AppendLine("A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName,  B.ItScCode, D.ItScName, B.MinStock, B.ReorderStock, A.Qty, B.PurchaseUomCode, B.Specification, ");
            SQL.AppendLine("A.UsageDt, A.QtDocNo, A.QtDNo, C.DocDt, A.UPrice, (A.Qty*A.UPrice) As Total, TotalPrice, A.Remark, A.CurCode, A.EstPrice, A.Duration, E.OptDesc DurationUom, ");

            if (mIsUseECatalog) SQL.AppendLine("A.PtCode, F.PtName, A.CityCode, H.ProvName, G.CityName, ");
            else SQL.AppendLine("Null AS PtCode, Null As PtName, Null As CityCode, Null As ProvName, Null As CityName, ");
            SQL.AppendLine("I.IndependentEstPrice, ");
            {
                SQL.AppendLine("0.00 As DroppingRequestAmt ");
            }

            SQL.AppendLine("From TblMaterialRequestDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemSubCategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Left Join TblOption E On E.OptCode = A.DurationUom And E.OptCat = 'DurationUom' ");
            if (mIsUseECatalog)
            {
                SQL.AppendLine("Left Join TblPaymentTerm F On A.PtCode = F.PtCode ");
                SQL.AppendLine("Left Join TblCity G On A.CityCode = G.CityCode ");
                SQL.AppendLine("Left Join TblProvince H On G.ProvCode = H.ProvCode ");
            }
            SQL.AppendLine("Inner Join TblIndependentEstimatedPriceDtl I On A.DocNo = I.MRDocNo And A.DNo = I.MRDNo ");
            SQL.AppendLine("Where I.DocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Order By A.ItCode");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "CancelReason", "StatusDesc", "UserName", "ItCode",  
                    
                    //6-10
                    "ItCodeInternal", "ItName", "ForeignName", "ItScCode", "ItScName",  
                    
                    //11-15
                    "MinStock", "ReorderStock", "Qty", "PurchaseUomCode", "UsageDt", 
                    
                    //16-20
                    "QtDocNo", "QtDNo", "DocDt", "UPrice", "Total", 
                    
                    //21-25
                    "Remark", "DORequestDocNo", "DORequestDNo", "CurCode", "EstPrice",
                    
                    //26-30
                    "DroppingRequestAmt", "Specification", "Duration", "DurationUom", "TotalPrice",

                    //31-35
                    "PtCode", "PtName", "CityCode", "ProvName", "CityName",

                    //36-37
                    "IndependentEstPrice"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 29, 25);
                    Grd.Cells[Row, 31].Value = 0m;
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 32, 26);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 33, 27);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 35, 28);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 37, 29);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 38, 30);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 39, 31);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 40, 32);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 42, 33);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 43, 34);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 44, 35);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 45, 36);
                    Grd.Cells[Row, 46].Value = Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 45);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 29, 31, 32, 35, 36 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMaterialRequestDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ItCode, A.JobCode, B.JobName, C.JobCtName, B.UomCode, ");
            SQL.AppendLine("A.PrevCurCode, A.PrevUPrice, A.CurCode, A.UPrice, A.Qty, A.Remark ");
            SQL.AppendLine("From TblMaterialRequestDtl2 A ");
            SQL.AppendLine("Inner Join TblJob B On A.JobCode=B.JobCode ");
            SQL.AppendLine("Inner Join TblJobCategory C On B.JobCtCode=C.JobCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By C.JobCtName, B.JobName;");

            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                int No = 0;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 
                    //1-5
                    "JobCode", "JobName", "JobCtName", "UomCode", "PrevCurCode", 
                    //6-10
                    "PrevUPrice", "CurCode", "UPrice", "Qty", "Remark"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        No = No + 1;
                        mlJob.Add(new Job()
                        {
                            No=  No,
                            ItCode = Sm.DrStr(dr, c[0]),
                            JobCode = Sm.DrStr(dr, c[1]),
                            JobName = Sm.DrStr(dr, c[2]),
                            JobCtName = Sm.DrStr(dr, c[3]),
                            UomCode = Sm.DrStr(dr, c[4]),
                            PrevCurCode = Sm.DrStr(dr, c[5]),
                            PrevUPrice = Sm.DrDec(dr, c[6]),
                            CurCode = Sm.DrStr(dr, c[7]),
                            UPrice = Sm.DrDec(dr, c[8]),
                            Qty = Sm.DrDec(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),
                            Total = Sm.DrDec(dr, c[9]) * Sm.DrDec(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        internal void ShowMaterialRequestReview(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(" SELECT B.ItCode, B.ItName, B.Specification, D.EmpName, D.Remark ");
            SQL.AppendLine(" FROM tblmaterialrequestdtl A ");
            SQL.AppendLine(" INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
            SQL.AppendLine(" LEFT JOIN ( SELECT  ");
            SQL.AppendLine(" 		MAX(T2.`Level`) LV, T1.DocNo, T1.DNo  ");
            SQL.AppendLine(" 		FROM tbldocapproval T1 ");
            SQL.AppendLine(" 		INNER JOIN tbldocapprovalsetting T2 ON T1.DocType = T2.DocType  AND T1.ApprovalDNo = T2.DNo ");
            SQL.AppendLine(" 		WHERE T1.DocType = 'MaterialRequest' AND T1.DocNo = @DocNo AND (T1.UserCode IS NOT NULL AND T1.UserCode <> '') ");
            SQL.AppendLine(" 		GROUP BY T1.DocNo, T1.DNo ");
            SQL.AppendLine(" 				) C ON A.DocNo = C.DocNo AND A.DNo = C.Dno ");
            SQL.AppendLine(" LEFT JOIN ( SELECT  ");
            SQL.AppendLine(" 		T2.`Level` LV, T1.DocNo, T1.DNo, T1.Remark, T3.EmpName ");
            SQL.AppendLine(" 		FROM tbldocapproval T1 ");
            SQL.AppendLine(" 		INNER JOIN tbldocapprovalsetting T2 ON T1.DocType = T2.DocType  AND T1.ApprovalDNo = T2.DNo ");
            SQL.AppendLine(" 		INNER JOIN tblemployee T3 ON T1.UserCode = T3.UserCode ");
            SQL.AppendLine(" 		WHERE T1.DocType = 'MaterialRequest' AND T1.DocNo = @DocNo AND (T1.UserCode IS NOT NULL AND T1.UserCode <> '') ");
            SQL.AppendLine(" 				) D ON C.DocNo = D.DocNo AND C.DNo = D.Dno AND C.LV = D.LV ");
            SQL.AppendLine(" WHERE A.DocNo = @DocNo ");
            SQL.AppendLine(" AND A.CancelInd = 'N' ");
            SQL.AppendLine(" AND A.Status = 'A' ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref GrdReview, ref cm, SQL.ToString(),
                new string[] { 
                                // 0
                                "ItCode",
                                
                                //1-4
                                "Itname","Specification", "EmpName", "Remark" 
                              },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    GrdReview.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 5, 4);

                }, false, false, true, false
            );
        }

        #endregion

        #region Additional Method
        private void SetControlEnabled(List<Control> Ctrls, bool Enabled)
        {
            foreach (var Ctrl in Ctrls)
            {
                switch(Ctrl.GetType().ToString())
                {
                    case "DevExpress.XtraEditors.SimpleButton":
                        ((SimpleButton)Ctrl).Enabled = Enabled;
                        break;
                    case "DevExpress.XtraEditors.CheckEdit":
                        ((CheckEdit)Ctrl).Enabled = Enabled;
                        break;
                }
            }
        }
            
        internal void SetLocalDocNoFromReference(string DocNo)
        {
            TxtLocalDocNo.EditValue = Sm.GetValue("Select "+
                        " IF(ISNULL(A.ReferenceDocNo), A.DocNo, A.LocalDocNo) AS Ref "+
                        " From TblMaterialRequestHdr A WHERE A.DocNo = @Param;", DocNo);
        }

        internal void SetSeqNo()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Grd1.Cells[r, 30].Value = r + 1;
        }

        public static string GetNumber(string Dno)
        {
            string number = string.Empty;
            for (int ind = 0; ind < Dno.Length; ind++)
            {
                if (Char.IsNumber(Dno[ind]) == true)
                {
                    number = number + Dno[ind];
                }

            }
            return number;
        }

        private string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                ShortCode = string.Empty,
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            if (mIsDocNoWithDeptShortCode)
                ShortCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode='" + Sm.GetLue(LueDeptCode) + "';");
            if (DocTitle.Length == 0) DocTitle = "XXX";
            
            var SQL = new StringBuilder();

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', ");
                if (mIsDocNoWithDeptShortCode && ShortCode.Length > 0)
                    SQL.Append("'" + ShortCode + "', '/', ");
                SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', ");
                    if (mIsDocNoWithDeptShortCode && ShortCode.Length > 0)
                        SQL.Append("'" + ShortCode + "', '/', ");
                    SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateDocNo2(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty;
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);
            string DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType);
            string ShortCode = Sm.GetValue("Select B.ShortCode From TblDepartment A Inner Join TblDivision B ON A.DivisionCode = B.DivisionCode WHERE A.DeptCode = @Param; ", Sm.GetLue(LueDeptCode));

            SQL.AppendLine("Select Concat(  ");
            SQL.AppendLine("IfNull((  ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From (  ");
            SQL.AppendLine("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.AppendLine("       Where Right(DocNo, LENGTH(CONCAT(@DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr)))=Concat(@DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr)  ");
            SQL.AppendLine("       Order By Left(DocNo, 4) Desc Limit 1  ");
            SQL.AppendLine("       ) As Temp  ");
            SQL.AppendLine("   ), '0001')  ");
            SQL.AppendLine(", '/', @DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.AppendLine(") As DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@ShortCode", ShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        private string GenerateLocalDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(0, 4),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DeptCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode = '"+Sm.GetLue(LueDeptCode)+"' ");
            if (DocTitle.Length == 0) DocTitle = "XXX";


            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000000', Convert(LocalDocNo+1, Char)), 6) From ( ");
            SQL.Append("       Select Convert(ifnull(Max(LocalDocNo), 0), Decimal) As LocalDocNo From " + Tbl);
            SQL.Append("       Order By Convert(ifnull(Max(LocalDocNo), 0), Decimal) Desc Limit 1");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '000001'), ");
            SQL.Append(" '/', '" + DocAbbr + "', '/', '"+DeptCode+"', '/', '"+SubCategory+"', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As LocalDocNo");

            return Sm.GetValue(SQL.ToString());
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 8) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 17).Length!=0) Qty = Sm.GetGrdDec(Grd1, Row, 17);
            if (Sm.GetGrdStr(Grd1, Row, 23).Length!=0) UPrice = Sm.GetGrdDec(Grd1, Row, 23);

            Grd1.Cells[Row, 24].Value = Qty*UPrice;

            if (mSourceAvailableBudgetForMR == "1") ComputeRemainingBudget();
        }

        private decimal ComputeAvailableBudget()
        {
            string AvailableBudget = "0";

            if (Sm.GetLue(LueDeptCode).Length != 0 && Sm.GetDte(DteDocDt).Length!=0 && Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
            {
                var SQL = new StringBuilder();

                if (mIsMRBudgetBasedOnBudgetCategory)
                {
                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select  ");
                    if(!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        Amt2 ");
                    else
                        SQL.AppendLine("        SUM(Amt2) ");
                    SQL.AppendLine("        From TblBudgetSummary A");
                    SQL.AppendLine("        Where A.DeptCode=@DeptCode AND A.BCCode = @BCCode ");
                    SQL.AppendLine("        And A.Yr=Left(@DocDt, 4) ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And A.Mth=Substring(@DocDt, 5, 2) ");
                    SQL.AppendLine("    ), 0.00)- ");
                }
                else
                {

                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Amt From TblBudget ");
                    SQL.AppendLine("        Where DeptCode=@DeptCode ");
                    SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                    SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                    //SQL.AppendLine("        And UserCode Is Not Null ");
                    SQL.AppendLine("    ), 0.00)- ");
                    
                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(Amt) ");
                SQL.AppendLine("        From TblVoucherRequestHdr ");
                SQL.AppendLine("        Where DocNo <> @DocNo ");
                SQL.AppendLine("        And Find_In_Set(DocType, ");
                SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("        And CancelInd = 'N' ");
                SQL.AppendLine("        And Status In ('O', 'A') ");
                SQL.AppendLine("        And ReqType Is Not Null ");
                SQL.AppendLine("        And ReqType <> @ReqTypeForNonBudget ");
                SQL.AppendLine("        And DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) - ");

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                SQL.AppendLine("        From TblTravelRequestHdr A ");
                SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(B.BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) - ");

                if (mMRAvailableBudgetSubtraction == "1")
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                    SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory)
                    {
                        SQL.AppendLine("        And A.BCCode=@BCCode ");
                    }
                    SQL.AppendLine("        And A.ReqType='1' ");
                    SQL.AppendLine("        And B.CancelInd='N' ");
                    SQL.AppendLine("        And B.Status<>'C' ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    SQL.AppendLine("    ),0.00)+");
                }
                
                if (mMRAvailableBudgetSubtraction == "2")
                {
                    SQL.AppendLine("    IfNull(( ");

                    #region Old Code

                    //SQL.AppendLine("        Select Sum(E.Qty*D.UPrice)  ");
                    //SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    //SQL.AppendLine("        Inner Join TblMaterialRequestDtl B on A.Docno = B.DocNo ");
                    //SQL.AppendLine("        Inner Join TblPORequestDtl C  On B.DocNo=C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo ");
                    //SQL.AppendLine("        Inner Join TblQTDtl D On D.DocNo = C.QtDocNo And D.DNo = C.QtDNo ");
                    //SQL.AppendLine("        Inner Join TblPODtl E on E.PORequestDocNo = C.DocNo And E.PORequestDNo = C.DNo And E.CancelInd = 'N' ");

                    //if (mBudgetBasedOn == "1")
                    //    SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    //if (mBudgetBasedOn == "2")
                    //    SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    //SQL.AppendLine("        And A.ReqType='1' ");
                    //SQL.AppendLine("        And B.CancelInd='N' ");
                    //SQL.AppendLine("        And B.Status<>'C' ");
                    //SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    //SQL.AppendLine("       And A.DocNo<>@DocNo ");

                    #endregion

                    SQL.AppendLine("    Select Sum( ");
                    SQL.AppendLine("    X2.Amt -  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    SQL.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("          IfNull(( ");
                    SQL.AppendLine("              Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("              Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End ");
                    SQL.AppendLine("      , 0.00) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, Sum(  ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("            And A.DocNo <> @DocNo ");
                    SQL.AppendLine("            And B.CancelInd='N'  ");
                    SQL.AppendLine("            And B.Status In ('A', 'O')  ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1'   ");

                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory)
                        SQL.AppendLine("        And A.BCCode=@BCCode ");
                    
                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");

                    SQL.AppendLine("    ),0.00)+");
                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
                SQL.AppendLine("        From TblPOQtyCancel A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
                SQL.AppendLine("            On D.DocNo=E.DocNo  ");
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine("        And E.DeptCode=@DeptCode ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("        And E.SiteCode=@DeptCode ");
                if (mIsMRBudgetBasedOnBudgetCategory)
                    SQL.AppendLine("        And E.BCCode=@BCCode ");

                SQL.AppendLine("            And E.ReqType='1' ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("            And Left(E.DocDt, 4)=Left(@DocDt, 4) ");
                SQL.AppendLine("            And E.DocNo<>@DocNo ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
                SQL.AppendLine("    ),0.00) + ");

                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Sum(T.Amt) Amt From ( ");
		        SQL.AppendLine("        Select Case when A.AcType = 'D' Then IfNull(A.Amt, 0.00) Else IfNull(A.Amt*-1, 0.00) END As Amt ");
                SQL.AppendLine("        From TblVoucherHdr A  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X1.DocNo, X1.VoucherRequestDocNo   ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblVoucherRequestHdr X2 ON X2.DocNo = X1.VoucherRequestDocNo  ");
                SQL.AppendLine("                And X1.DocType = '58' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("                AND X2.Status In ('O', 'A')  ");
                SQL.AppendLine("        ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblCashAdvanceSettlementDtl X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("            INNER JOIN TblVoucherRequestHdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("                AND X1.DocType = '56' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N' ");
                SQL.AppendLine("                AND X3.Status In ('O', 'A') ");
                SQL.AppendLine("        ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And D.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(D.BCCode, '') = IfNull(@BCCode, '')  ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine("), 0.00 ) ");

                if (mIsCASUsedForBudget)
                {
                    SQL.AppendLine(" - IfNull(( ");
                    SQL.AppendLine("    Select Sum(T.Amt) Amt From ( ");
                    SQL.AppendLine("        Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) As Mth, C.BCCode, C.DeptCode, B.Amt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                    SQL.AppendLine("            And A.DocStatus = 'F' ");
                    SQL.AppendLine("            And B.CCtCode Is Not Null ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                    SQL.AppendLine("            And C.CCtCode Is Not Null ");
                    SQL.AppendLine("            And C.BCCode = @BCCode ");
                    SQL.AppendLine("            And C.DeptCOde = @DeptCode ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("), 0.00) ");
                }

                SQL.AppendLine("As RemainingBudget");

                var cm = new MySqlCommand()
                { CommandText =SQL.ToString() };
                if (mBudgetBasedOn == "1")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                if (mBudgetBasedOn == "2")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueSiteCode));

                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length!=0)?TxtDocNo.Text:"XXX");
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));
                
                AvailableBudget = Sm.GetValue(cm);
            }
            
            return decimal.Parse(AvailableBudget);
        }

        public void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                if (Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
                {
                    AvailableBudget = Sm.ComputeAvailableBudget(
                        (TxtMRDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        );
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 24).Length != 0)
                        {
                            if (mIsBudgetCalculateFromEstimatedPrice && mIsMRShowEstimatedPrice)
                                RequestedBudget += (Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 29));
                            else
                                RequestedBudget += Sm.GetGrdDec(Grd1, Row, 24);
                        }
                    }
                
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget-RequestedBudget, 0);
        }

        public void ComputeGrandTotalIndependentEstimatedPrice()
        {
            decimal GrandTotalIndependentEstimatedPrice = 0m;
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 46).Length != 0)
                {
                    GrandTotalIndependentEstimatedPrice += Sm.GetGrdDec(Grd1, i, 46);
                }
            }
            TxtGrandTotalIndependentEstimatedPrice.EditValue = Sm.FormatNum(GrandTotalIndependentEstimatedPrice, 0);
        }

        internal void ComputeTotalPrice(int Row)
        {
            decimal Qty = 0m, EstPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 17).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 17);
            if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0) EstPrice = Sm.GetGrdDec(Grd1, Row, 29);

            Grd1.Cells[Row, 38].Value = Qty * EstPrice;
        }

        internal void ComputeTotalIndependentPrice(int Row)
        {
            decimal Qty = 0m, EstIndependentPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 17).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 17);
            if (Sm.GetGrdStr(Grd1, Row, 45).Length != 0) EstIndependentPrice = Sm.GetGrdDec(Grd1, Row, 45);

            Grd1.Cells[Row, 46].Value = Qty * EstIndependentPrice;
        }
    
        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            var l = new List<MatReq>();
            var l1 = new List<MatReq1>();
            var l2 = new List<MatReq2>();
            var ldtl = new List<MatReqDtl>();
            var ldtl2 = new List<MatReqDtl2>();
            var ldtl3 = new List<MatReqDtl3>();
            var ldtl4 = new List<MatReqDtl4>();
            var l3 = new List<MatReqSier>();
            var ldtl5 = new List<MatReqSPPJB>();
            var lsign = new List<SignatureSIER>();
            var l4 = new List<MRIMS>();
            var ldtl6 = new List<MRIMSDtl>();
            var lsign2 = new List<MRIMSSign>();
            var lsign3 = new List<MRIMSSign2>();

            string[] TableName = { "MatReq", "MatReqDtl", "MatReqDtl2", "MatReq1", "MatReq2", "MatReqDtl3", "MatReqDtl4", "MatReqSPPJB", "MatReqSier", "SignatureSIER", "MRIMS", "MRIMSDtl", "MRIMSSign", "MRIMSSign2", "Job" };
            List<IList> myLists = new List<IList>();
            int Nomor = 1;
            decimal mUsageAmt = 0m;
            decimal mUsageAmt2 = 0m;
            var cm = new MySqlCommand();
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region Header
            var SQL = new StringBuilder();

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, F.CompanyName, F.CompanyPhone, F.CompanyFax, F.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            }
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, E.SiteName, B.DeptName, C.OptDesc, A.Remark, D.UserName As CreateBy, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='isfilterbysite') As SiteInd, A.LocalDocNo, Concat('(',A.BCCode,')',' ', G.BCName) As BCCode ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblOption C On A.ReqType = C.OptCode ");
            SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblMaterialRequesthdr A  ");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(")F On A.DocNo = F.DocNo ");
            }
            SQL.AppendLine("Left Join TblBudgetCategory G On A.BCCode=G.BCCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo And C.OptCat = 'ReqType' ");
            //  SQL.AppendLine("And A.Status = '' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select D.EntLogoName " +
                       "From TblMaterialRequesthdr A  " +
                       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                       "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                       "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "DocNo",
                         //6-10
                         "DocDt",
                         "SiteName",
                         "DeptName",
                         "OptDesc",
                         "Remark",
                         //11-14
                         "CreateBy",
                         "SiteInd",
                         "LocalDocNo",
                         "BCCode"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new MatReq()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            SiteName = Sm.DrStr(dr, c[7]),
                            DeptName = Sm.DrStr(dr, c[8]),
                            OptDesc = Sm.DrStr(dr, c[9]),
                            HRemark = Sm.DrStr(dr, c[10]),
                            CreateBy = Sm.DrStr(dr, c[11]),
                            SiteInd = Sm.DrStr(dr, c[12]),
                            LocalDocNo = Sm.DrStr(dr, c[13]),
                            BCCode = Sm.DrStr(dr, c[14]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.Qty, B.PurchaseUomCode, ");
                SQLDtl.AppendLine("DATE_FORMAT(A.UsageDt,'%d-%m-%Y') As UsageDt, ");
                if (mIsUseItemConsumption)
                {
                    SQLDtl.AppendLine("IfNull(C.Qty01, 0.00) As Mth01, ");
                    SQLDtl.AppendLine("IfNull(C.Qty03, 0.00) As Mth03, ");
                    SQLDtl.AppendLine("IfNull(C.Qty06, 0.00) As Mth06, ");
                    SQLDtl.AppendLine("IfNull(C.Qty09, 0.00) As Mth09, ");
                    SQLDtl.AppendLine("IfNull(C.Qty12, 0.00) As Mth12, ");
                }
                else
                {
                    SQLDtl.AppendLine("0.00 As Mth01, ");
                    SQLDtl.AppendLine("0.00 As Mth03, ");
                    SQLDtl.AppendLine("0.00 As Mth06, ");
                    SQLDtl.AppendLine("0.00 As Mth09, ");
                    SQLDtl.AppendLine("0.00 As Mth12, ");
                }
                SQLDtl.AppendLine("A.Remark, ");
                SQLDtl.AppendLine("B.ForeignName, A.EstPrice, A.CurCode, A.Duration, D.OptDesc DurationUOM ");
                SQLDtl.AppendLine("From TblMaterialRequestDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                if (mIsUseItemConsumption)
                {
                    SQLDtl.AppendLine("Left Join ( ");
                    SQLDtl.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                    SQLDtl.AppendLine("        From ( ");
                    SQLDtl.AppendLine("        select Z1.itCode, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                    SQLDtl.AppendLine("            From ");
                    SQLDtl.AppendLine("            ( ");
                    SQLDtl.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                    SQLDtl.AppendLine("                From ");
                    SQLDtl.AppendLine("                ( ");
                    SQLDtl.AppendLine("                    Select convert('01' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('03' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('06' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('09' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('12' using latin1) As Mth  ");
                    SQLDtl.AppendLine("                )T1 ");
                    SQLDtl.AppendLine("                Inner Join ");
                    SQLDtl.AppendLine("                ( ");
                    SQLDtl.AppendLine("                    Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt And last_day(@MthDocDt) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode  ");
                    SQLDtl.AppendLine("                    Union ALL ");
                    SQLDtl.AppendLine("                    Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt2 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt4 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt5 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt6 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                    SQLDtl.AppendLine("            Group By T1.mth, T2.ItCode ");
                    SQLDtl.AppendLine("        )Z1 ");
                    SQLDtl.AppendLine("   )Z2 Group By Z2.ItCode ");
                    SQLDtl.AppendLine(" ) C On C.ItCode = A.ItCode ");
                }
                SQLDtl.AppendLine("Left Join TblOption D On  D.OptCode = A.DurationUom And D.OptCat = 'DurationUOM' ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine("And A.CancelInd = 'N' ");
                SQLDtl.AppendLine("Order By A.ItCode;");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                Sm.CmParamDt(ref cmDtl, "@MthDocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "Qty",
                     "PurchaseUomCode",
                     "UsageDt",
                     "Mth01",
                     //6-10
                     "Mth03",
                     "Mth06",
                     "Mth09",
                     "Mth12",
                     "Remark",
                     //11-15
                     "ForeignName",
                     "EstPrice",
                     "Curcode",
                     "Duration",
                     "DurationUOM"
                    });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new MatReqDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            UsageDt = Sm.DrStr(drDtl, cDtl[4]),
                            Mth01 = Sm.DrDec(drDtl, cDtl[5]),
                            Mth03 = Sm.DrDec(drDtl, cDtl[6]),
                            Mth06 = Sm.DrDec(drDtl, cDtl[7]),
                            Mth09 = Sm.DrDec(drDtl, cDtl[8]),
                            Mth12 = Sm.DrDec(drDtl, cDtl[9]),
                            DRemark = Sm.DrStr(drDtl, cDtl[10]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[11]),
                            EstPrice = Sm.DrDec(drDtl, cDtl[12]),
                            Curcode = Sm.DrStr(drDtl, cDtl[13]),
                            Duration = Sm.DrStr(drDtl, cDtl[14]),
                            DurationUom = Sm.DrStr(drDtl, cDtl[15]),
                        });
                        mUsageAmt += Sm.DrDec(drDtl, cDtl[12]);
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd, E.Posname ");
                SQLDtl2.AppendLine("From TblMaterialRequestHdr A ");
                SQLDtl2.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLDtl2.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Left Join tblemployee D On A.CreateBy=D.UserCode ");
                SQLDtl2.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
                SQLDtl2.AppendLine("Where DocNo=@DocNo ");
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0-1
                         "UserCode" ,
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "Posname"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new MatReqDtl2()
                        {
                            UserCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpPict = Sm.DrStr(drDtl2, cDtl2[2]),
                            SignInd = Sm.DrStr(drDtl2, cDtl2[3]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[4]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Approve1
            var cm1 = new MySqlCommand();
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd, E.PosName, ");
            SQL1.AppendLine("DATE_FORMAT(SUBSTRING(A.LastUpDt, 1, 8),'%d %M %Y') As LastUpDt ");
            SQL1.AppendLine("from TblDocApproval A ");
            SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL1.AppendLine("Left Join tblemployee D On A.UserCode=D.UserCode ");
            SQL1.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            if (mIsMRSPPJB)
                SQL1.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            else
                SQL1.AppendLine("Where DocType = 'MaterialRequest' ");
            SQL1.AppendLine("And DocNo =@DocNo ");
            SQL1.AppendLine("Group by ApprovalDno limit 1");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-5
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "PosName",
                         //6
                         "LastUpDt"

                        
                        });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l1.Add(new MatReq1()
                        {
                            ApprovalDno = Sm.DrStr(dr1, c1[0]),
                            UserCode = Sm.DrStr(dr1, c1[1]),
                            UserName = Sm.DrStr(dr1, c1[2]),
                            EmpPict = Sm.DrStr(dr1, c1[3]),
                            SignInd = Sm.DrStr(dr1, c1[4]),
                            PosName = Sm.DrStr(dr1, c1[5]),
                            LastUpDt = Sm.DrStr(dr1, c1[6]),
                        });
                    }
                }

                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Approve2

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL2.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict,  '" + Doctitle + "' As SignInd, E.PosName, ");
            SQL2.AppendLine("DATE_FORMAT(SUBSTRING(A.LastUpDt, 1, 8),'%d %M %Y') As LastUpDt ");
            SQL2.AppendLine("from TblDocApproval A ");
            SQL2.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine("Left Join tblemployee D On A.UserCode=D.UserCode ");
            SQL2.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            if (mIsMRSPPJB)
                SQL2.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            else
                SQL2.AppendLine("Where DocType = 'MaterialRequest' ");
            SQL2.AppendLine("And DocNo =@DocNo ");
            SQL2.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-5
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "PosName",

                         "LastUpDt"

                        
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new MatReq2()
                        {
                            ApprovalDno = Sm.DrStr(dr2, c2[0]),
                            UserCode = Sm.DrStr(dr2, c2[1]),
                            UserName = Sm.DrStr(dr2, c2[2]),
                            EmpPict = Sm.DrStr(dr2, c2[3]),
                            SignInd = Sm.DrStr(dr2, c2[4]),
                            PosName = Sm.DrStr(dr2, c2[5]),
                            LastUpDt = Sm.DrStr(dr2, c2[6]),
                        });
                    }
                }

                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail Signature KIM
            // level 2 tidak ditampilkan

            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select Distinct Concat(T4.ParValue, T1.UserCode, '.JPG') As Signature, ");
                SQLDtl3.AppendLine("T5.UserName, T3.PosName, T1.DNo, ");
                SQLDtl3.AppendLine("Case T1.Title When 'P' Then 'Prepared By' When 'A' Then 'Approved By' End As Title, ");
                SQLDtl3.AppendLine("Date_Format(T1.LastUpDt,'%d %M %Y') As LastUpDt ");
                SQLDtl3.AppendLine("From ( ");
                SQLDtl3.AppendLine("    Select Title, UserCode, Min(DNo) As DNo, Min(LastUpDt) LastUpDt ");
                SQLDtl3.AppendLine("    From ( ");
                SQLDtl3.AppendLine("        Select Distinct 'A' As Title, B.UserCode, B.ApprovalDNo As DNo, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl3.AppendLine("        From TblMaterialRequestDtl A ");
                SQLDtl3.AppendLine("        Inner Join TblDocApproval B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                if (mIsMRSPPJB)
                    SQLDtl3.AppendLine("            And B.DocType='MaterialRequestSPPJB' ");
                else
                    SQLDtl3.AppendLine("            And B.DocType='MaterialRequest' ");
                SQLDtl3.AppendLine("        Where A.DocNo=@DocNo And A.CancelInd='N' And A.Status='A' ");
                SQLDtl3.AppendLine("        Union All ");
                SQLDtl3.AppendLine("        Select 'P' As Title, CreateBy As UserCode, '00' As DNo, Left(CreateDt, 8) As LastUpDt ");
                SQLDtl3.AppendLine("        From TblMaterialRequestDtl Where DocNo=@DocNo And CancelInd='N' And Status='A' ");
                SQLDtl3.AppendLine("    ) T Group By Title, UserCode ");
                SQLDtl3.AppendLine(") T1 ");
                SQLDtl3.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl3.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl3.AppendLine("Inner Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' And T4.ParValue Is Not Null ");
                SQLDtl3.AppendLine("Inner Join TblUser T5 On T1.UserCode=T5.UserCode ");
                SQLDtl3.AppendLine("Order By T1.DNo; ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", DocNo);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                {
                    //0
                    "Signature" ,

                    //1-5
                    "Username", "PosName", "DNo", "Title", "LastupDt"
                });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new MatReqDtl3()
                        {
                            Signature = Sm.DrStr(drDtl3, cDtl3[0]),
                            UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                            PosName = Sm.DrStr(drDtl3, cDtl3[2]),
                            DNo = Sm.DrStr(drDtl3, cDtl3[3]),
                            Title = Sm.DrStr(drDtl3, cDtl3[4]),
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[5]),
                            Space = "-------------------------",
                            IsPrintOutUseDigitalSignature = mIsPrintOutUseDigitalSignature
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);

            #endregion

            #region Detail Signature TWC

            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl4.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl4.AppendLine("From ( ");
                SQLDtl4.AppendLine("    Select Distinct ");
                SQLDtl4.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtl4.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl4.AppendLine("    From TblMaterialRequestDTL A ");
                SQLDtl4.AppendLine("    Inner Join TblDocApproval B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                if (mIsMRSPPJB)
                    SQLDtl4.AppendLine("        And B.DocType='MaterialRequestSPPJB' ");
                else
                    SQLDtl4.AppendLine("        And B.DocType='MaterialRequest' ");
                SQLDtl4.AppendLine("    Left Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl4.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = B.DocType ");
                SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine("    Union All ");
                SQLDtl4.AppendLine("    Select Distinct ");
                SQLDtl4.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                SQLDtl4.AppendLine("    '1' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                SQLDtl4.AppendLine("    From TblMaterialRequestDTL A ");
                SQLDtl4.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine(") T1 ");
                SQLDtl4.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl4.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl4.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl4.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.level ");
                SQLDtl4.AppendLine("Order By T1.Level; ");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", DocNo);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {

                        ldtl4.Add(new MatReqDtl4()
                        {
                            Signature = Sm.DrStr(drDtl4, cDtl4[0]),
                            UserName = Sm.DrStr(drDtl4, cDtl4[1]),
                            PosName = Sm.DrStr(drDtl4, cDtl4[2]),
                            Space = Sm.DrStr(drDtl4, cDtl4[3]),
                            DNo = Sm.DrStr(drDtl4, cDtl4[4]),
                            Title = Sm.DrStr(drDtl4, cDtl4[5]),
                            LastUpDt = Sm.DrStr(drDtl4, cDtl4[6]),
                            IsPrintOutUseDigitalSignature = mIsPrintOutUseDigitalSignature
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            #region Header SPPJB

            #region Old Code
            /*
            int No = 0;
            string mJobCtName = string.Empty;
            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();

            SQL4.AppendLine(" SELECT A.Qty, A.UPrice, B.JobName, C.JobCtName ");
            SQL4.AppendLine(" FROM TblMaterialRequestDtl2 A ");
            SQL4.AppendLine(" LEFT JOIN TblJob B ON A.JobCode = b.JobCode ");
            SQL4.AppendLine(" INNER JOIN TblJobCategory C ON B.JobCtCode = C.JobCtCode ");
            SQL4.AppendLine(" WHERE DocNo = @DocNo Order By C.JobCtName");

            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();
                Sm.CmParam<String>(ref cm4, "@DocNo", DocNo);
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                        {
                        //0
                         "JobName",

                        // 1-2
                         "Qty",
                         "UPrice",

                        });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        No = No + 1;

                        ldtl5.Add(new MatReqSPPJB()
                        {
                            No = No,
                            ItName = Sm.GetGrdStr(Grd1, 1, 11),
                            JobName = Sm.DrStr(dr4, c4[0]),
                            Qty = Sm.DrDec(dr4, c4[1]),
                            UPrice = Sm.DrDec(dr4, c4[2]),
                            Total = Sm.DrDec(dr4, c4[1]) * Sm.DrDec(dr4, c4[2])

                        });
                        //mJobCtName = Sm.DrStr(dr4, c4[1]);
                        //mUsageAmt2 += Sm.DrDec(dr4, c4[2]);
                    }

                }
                dr4.Close();
            }
            myLists.Add(ldtl5);
            */
            #endregion

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (!Sm.GetGrdBool(Grd1, i, 1)) // tidak di cancel
                {
                    ldtl5.Add(new MatReqSPPJB()
                    {
                        No = Nomor,
                        ItName = Sm.GetGrdStr(Grd1, i, 11),
                        
                    });
                    Nomor += 1;
                }
            }
            myLists.Add(ldtl);

            #endregion

            #region HeaderSier

            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            SQL3.AppendLine("Select DISTINCT B.DocNo, A.Amt2 BudgetAmt, (A.Amt2-SUM(C.Totalprice)) RemainingBudgetAmt, ");
            SQL3.AppendLine("SUM(C.TotalPrice) UsageAmt, ");
            SQL3.AppendLine("IFNULL ");
            SQL3.AppendLine("((SELECT F.Amt2 - SUM(B.TotalPrice) ");
            SQL3.AppendLine("FROM TblMaterialRequestHdr A ");
            SQL3.AppendLine("INNER JOIN TblMaterialRequestDtl B ON A.DocNo = B.DocNo  ");
            SQL3.AppendLine("INNER JOIN TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            SQL3.AppendLine("INNER JOIN TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            SQL3.AppendLine("INNER Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            SQL3.AppendLine("INNER JOIN TblBudgetSummary F ON A.BCCode = F.BCCode ");
            SQL3.AppendLine("Where A.DeptCode=@DeptCode  ");
            SQL3.AppendLine("AND F.BCCode = @BCCode  ");
            SQL3.AppendLine("AND F.Yr=LEFT(@DocDt, 4)  ");
            SQL3.AppendLine("AND B.DocNo=@DocNo  ");
            SQL3.AppendLine("GROUP BY A.DocNo) ");
            SQL3.AppendLine(", A.Amt2 ");
            SQL3.AppendLine(") AfterUsageAmt ");
            SQL3.AppendLine("From TblBudgetSummary A  ");
            SQL3.AppendLine("INNER JOIN TblMaterialRequestHdr B ON A.BCCode=B.BCCode  ");
            SQL3.AppendLine("INNER JOIN TblMaterialRequestDtl C ON B.DocNo=C.DocNo  ");
            SQL3.AppendLine("Where A.DeptCode=@DeptCode "); 
            SQL3.AppendLine(" AND A.BCCode = @BCCode  ");
            SQL3.AppendLine(" AND A.Yr=LEFT(@DocDt, 4)  ");
            if(!mIsBudget2YearlyFormat)
	             SQL3.AppendLine("   And A.Mth=SUBSTRING(@DocDt, 5, 2) ");
            SQL3.AppendLine("   AND B.DocNo=@DocNo ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm3, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm3, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);

                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                        //0
                        "BudgetAmt",

                        //1-3
                        "RemainingBudgetAmt",
                        "UsageAmt",
                        "AfterUsageAmt"
                        });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new MatReqSier()
                        {
                            Budget = Sm.DrDec(dr3, c3[0]),
                            AvailableBudget = Sm.DrDec(dr3, c3[1]),
                            UsageAmt = Sm.DrDec(dr3, c3[2]),
                            BudgetCategory = LueBCCode.Text,
                            AfterUsageAmt = Sm.DrDec(dr3, c3[3]),
                        });
                    }
                }
                dr3.Close();
            }
            myLists.Add(l3);
            #endregion

            #region Detail Signature SIER

            var cmDtl6 = new MySqlCommand();

            var SQLDtl6 = new StringBuilder();
            using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl6.Open();
                cmDtl6.Connection = cnDtl6;

                SQLDtl6.AppendLine("SELECT C.EmpName , D.PosName Position,  DATE_FORMAT(left(B.LastUpDt, 8), '%d %M %Y') AS Date ");
                SQLDtl6.AppendLine("FROM tbluser A ");
                SQLDtl6.AppendLine("INNER JOIN tbldocapproval B ON A.UserCode=B.UserCode ");
                SQLDtl6.AppendLine("INNER JOIN tblemployee C ON A.UserCode=C.UserCode ");
                SQLDtl6.AppendLine("INNER JOIN tblposition D ON C.PosCode=D.PosCode ");
                SQLDtl6.AppendLine("WHERE B.Docno =@DocNo ");

                cmDtl6.CommandText = SQLDtl6.ToString();
                Sm.CmParam<String>(ref cmDtl6, "@DocNo", DocNo);
                var drDtl6 = cmDtl6.ExecuteReader();
                var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1-5
                         "Position" ,
                         "Date"
                        });
                if (drDtl6.HasRows)
                {
                    while (drDtl6.Read())
                    {

                        lsign.Add(new SignatureSIER()
                        {
                            EmpName = Sm.DrStr(drDtl6, cDtl6[0]),
                            Position = Sm.DrStr(drDtl6, cDtl6[1]),
                            Date = Sm.DrStr(drDtl6, cDtl6[2])
                        });
                    }
                }
                drDtl6.Close();
            }
            myLists.Add(lsign);
            #endregion

            #region Header MRIMS

            var SQLH = new StringBuilder();
            var cmh = new MySqlCommand();

            SQLH.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo, A.DocNo, ");
            SQLH.AppendLine("DATE_FORMAT(A.DocDt, '%d-%m-%Y') DocDt, ");
            SQLH.AppendLine("B.SiteName, C.ProjectCode, ");
            SQLH.AppendLine("C.ProjectName, GROUP_CONCAT(Distinct T9.FinishedGood SEPARATOR ' \n') DocName  ");
            SQLH.AppendLine("FROM TblMaterialRequestHdr A  ");
            SQLH.AppendLine("LEFT JOIN TblSite B ON A.SiteCode = B.SiteCode ");
            SQLH.AppendLine("INNER JOIN tblmaterialrequestdtl T9 ON A.DocNo=T9.DocNo ");
            SQLH.AppendLine("LEFT JOIN TblProjectGroup C ON A.PGCode = C.PGCOde ");
            SQLH.AppendLine("WHERE A.DocNo = @DocNo; ");
            //SQL.AppendLine("And Exists(Select 1 From TblMaterialRequestDtl Where DocNo = @DocNo And ItCode In (Select ItCode From TblItem Where InventoryItemInd ='Y')); ");

            using (var cnh = new MySqlConnection(Gv.ConnectionString))
            {
                cnh.Open();
                cmh.Connection = cnh;
                cmh.CommandText = SQLH.ToString();
                Sm.CmParam<String>(ref cmh, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cmh, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cmh.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt",
                        "SiteName",
                        "ProjectCode",
                        "ProjectName",
                        "DocName",
                        //6
                        "CompanyLogo"
                    });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new MRIMS()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            ProjectCode = Sm.DrStr(dr,c[3]),
                            ProjectName = Sm.DrStr(dr, c[4]),
                            DocName = Sm.DrStr(dr, c[5]),
                            CompanyLogo = Sm.DrStr(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l4);

            #endregion

            #region Detail MRIMS

            var SQLD = new StringBuilder();
            var cmd = new MySqlCommand();

            SQLD.AppendLine("SELECT A.DocNo, A.DNo, B.ItCodeInternal, B.ItName, B.Specification, A.Qty, B.InventoryUomCode Uom, ");
            SQLD.AppendLine("C.ProjectCode, DATE_FORMAT(A.UsageDt, '%d-%m-%Y') UsageDt, A.Remark ");
            SQLD.AppendLine("FROM TblMaterialRequestDtl A ");
            SQLD.AppendLine("INNER JOIN TblItem B ON A.ItCode = B.ItCode ");
            SQLD.AppendLine("    AND A.DocNo = @DocNo ");
            SQLD.AppendLine("LEFT JOIN ");
            SQLD.AppendLine("( ");
            SQLD.AppendLine("    SELECT T1.DocNo, T1.DNo, GROUP_CONCAT(DISTINCT T5.ProjectCode) ProjectCode ");
            SQLD.AppendLine("    FROM TblMaterialRequestDtl T1 ");
            SQLD.AppendLine("    INNER JOIN TblBOMRevisionHdr T2 ON T1.BOMRDocNo = T2.DocNo ");
            SQLD.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLD.AppendLine("    INNER JOIN TblBOQHdr T3 ON T2.BOQDocNo = T3.DocNo ");
            SQLD.AppendLine("    INNER JOIN TblLOPHdr T4 ON T3.LOPDocNo = T4.DocNo ");
            SQLD.AppendLine("    LEFT JOIN TblProjectGroup T5 ON T4.PGCode = T5.PGCode ");
            SQLD.AppendLine("    GROUP BY T1.DocNo, T1.DNo ");
            SQLD.AppendLine(") C ON A.DocNo = C.DocNo AND A.DNo = C.DNo ");
            SQLD.AppendLine("Where A.CancelInd = 'N' ");
            SQLD.AppendLine("Order by A.ItCode; ");

            using (var cnd = new MySqlConnection(Gv.ConnectionString))
            {
                cnd.Open();
                cmd.Connection = cnd;
                cmd.CommandText = SQLD.ToString();
                Sm.CmParam<String>(ref cmd, "@DocNo", DocNo);

                var drd = cmd.ExecuteReader();
                var cd = Sm.GetOrdinal(drd, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DNo",
                        "ItCodeInternal",
                        "ItName",
                        "Specification",
                        "Qty",
                        //6-9
                        "Uom",
                        "ProjectCode",
                        "UsageDt",
                        "Remark"
                    });

                if (drd.HasRows)
                {
                    int mNo = 1;
                    while (drd.Read())
                    {
                        ldtl6.Add(new MRIMSDtl()
                        {
                            No = mNo,
                            DocNo = Sm.DrStr(drd, cd[0]),
                            DNo = Sm.DrStr(drd, cd[1]),
                            ItCodeInternal = Sm.DrStr(drd, cd[2]),
                            ItName = Sm.DrStr(drd, cd[3]),
                            Specification = Sm.DrStr(drd, cd[4]),
                            Qty = Sm.DrDec(drd, cd[5]),
                            Uom = Sm.DrStr(drd, cd[6]),
                            ProjectCode = Sm.DrStr(drd, cd[7]),
                            UsageDt = Sm.DrStr(drd, cd[8]),
                            Remark = Sm.DrStr(drd, cd[9])
                        });
                        mNo += 1;
                    }
                }
                drd.Close();
            }
            myLists.Add(ldtl6);

            #endregion

            #region Detail Signature MRIMS
            var SQLS = new StringBuilder();
            var cms = new MySqlCommand();

            SQLS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
            SQLS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, ");
            SQLS.AppendLine("T1.DNo, T1.Seq As Level, @Space As Space, ");
            SQLS.AppendLine("T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
            SQLS.AppendLine("From ( ");

            SQLS.AppendLine("    Select UserCode, UserName, DNo, Seq, Title, LastUpDt From ( ");
            SQLS.AppendLine("        Select Distinct B.UserCode, C.UserName, ");
            SQLS.AppendLine("        B.ApprovalDNo As DNo, 900+D.Level As Seq, ");
            SQLS.AppendLine("        'Approved By' As Title, ");
            SQLS.AppendLine("        Left(B.LastUpDt, 8) As LastUpDt ");
            SQLS.AppendLine("        From TblMaterialRequestDtl A ");
            SQLS.AppendLine("        Inner Join TblDocApproval B On A.DocNo=B.DocNo AND A.DocNo = @DocNo ");
            SQLS.AppendLine("        Inner Join TblUser C On B.UserCode=C.UserCode ");
            SQLS.AppendLine("        Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType='MaterialRequest' ");
            SQLS.AppendLine("        Where A.Status='A' ");
            SQLS.AppendLine("        And A.DocNo=@DocNo ");
            SQLS.AppendLine("        And B.UserCode Not In (Select CreateBy From TblMaterialRequestHdr Where DocNo = @DocNo) ");
            SQLS.AppendLine("        Order By D.Level Desc ");
            SQLS.AppendLine("    ) Tbl ");

            SQLS.AppendLine("    Union All ");
            SQLS.AppendLine("    Select Distinct ");
            SQLS.AppendLine("    A.CreateBy As UserCode, B.UserName, ");
            SQLS.AppendLine("    '1' As DNo, 0 As Seq, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
            SQLS.AppendLine("    From TblMaterialRequestHdr A ");
            SQLS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode And A.DocNo=@DocNo ");
            SQLS.AppendLine(") T1 ");
            SQLS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
            SQLS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
            SQLS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
            SQLS.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
            SQLS.AppendLine("Order By T1.Seq; ");

            using (var cns = new MySqlConnection(Gv.ConnectionString))
            {
                cns.Open();
                cms.Connection = cns;
                cms.CommandText = SQLS.ToString();
                Sm.CmParam<String>(ref cms, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cms, "@DocNo", DocNo);

                var drs = cms.ExecuteReader();
                var cs = Sm.GetOrdinal(drs, new string[]
                {
                    //0
                     "Signature",
                     //1-5
                     "Username",
                     "PosName",
                     "Space",
                     "Level",
                     "Title",
                     "LastupDt"
                });

                if (drs.HasRows)
                {
                    while (drs.Read())
                    {
                        lsign2.Add(new MRIMSSign()
                        {
                            Signature = Sm.DrStr(drs, cs[0]),
                            UserName = Sm.DrStr(drs, cs[1]),
                            PosName = Sm.DrStr(drs, cs[2]),
                            Space = Sm.DrStr(drs, cs[3]),
                            DNo = Sm.DrStr(drs, cs[4]),
                            Title = Sm.DrStr(drs, cs[5]),
                            LastUpDt = Sm.DrStr(drs, cs[6])
                        });
                    }
                }
                drs.Close();
            }
            myLists.Add(lsign2);
            #endregion

            #region Detail Signature2 MRIMS

            var SQLS2 = new StringBuilder();
            var cms2 = new MySqlCommand();

            SQLS2.AppendLine("SELECT * ");
            SQLS2.AppendLine("FROM  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT UPPER(B.UserName) CreateUserName, DATE_FORMAT(LEFT(A.CreateDt, 8), '%d-%m-%Y') CreateDocDt ");
            SQLS2.AppendLine("    FROM TblMaterialRequestHdr A ");
            SQLS2.AppendLine("    INNER JOIN TblUser B ON A.CreateBy = B.UserCode ");
            SQLS2.AppendLine("        AND A.DocNo = @DocNo ");
            SQLS2.AppendLine(") A ");
            SQLS2.AppendLine("LEFT JOIN ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) ApproveUserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') ApproveDocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("	            SELECT MAX(B.Level) MaxLevel ");
            SQLS2.AppendLine("	            FROM TblDocApproval A ");
            SQLS2.AppendLine("	            INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("	                AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("	                AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine(") B ON 0 = 0 ");
            SQLS2.AppendLine("LEFT JOIN  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) Approve2UserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') Approve2DocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level != 1 ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("	            SELECT MAX(B.Level) - 1 MaxLevel ");
            SQLS2.AppendLine("	            FROM TblDocApproval A ");
            SQLS2.AppendLine("	            INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("	                AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("	                AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine(") C ON 0 = 0 ");
            SQLS2.AppendLine("LEFT JOIN  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct UPPER(T3.UserName) Approve3UserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') Approve3DocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("                SELECT MAX(B.Level) - 2 MaxLevel ");
            SQLS2.AppendLine("                FROM TblDocApproval A ");
            SQLS2.AppendLine("                INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("                    AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("                    AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine(") D ON 0 = 0 ");

            using (var cns2 = new MySqlConnection(Gv.ConnectionString))
            {
                cns2.Open();
                cms2.Connection = cns2;
                cms2.CommandText = SQLS2.ToString();
                Sm.CmParam<String>(ref cms2, "@DocNo", DocNo);

                var drs2 = cms2.ExecuteReader();
                var cs2 = Sm.GetOrdinal(drs2, new string[]
                {
                    //0
                     "CreateUserName",
                     //1-5
                     "CreateDocDt",
                     "ApproveUserName",
                     "ApproveDocDt",
                     "Approve2UserName",
                     "Approve2DocDt",
                     //6-7
                     "Approve3UserName",
                     "Approve3DocDt"
                });

                if (drs2.HasRows)
                {
                    while (drs2.Read())
                    {
                        lsign3.Add(new MRIMSSign2()
                        {
                            CreateUserName = Sm.DrStr(drs2, cs2[0]),
                            CreateDocDt = Sm.DrStr(drs2, cs2[1]),
                            ApproveUserName = Sm.DrStr(drs2, cs2[2]),
                            ApproveDocDt = Sm.DrStr(drs2, cs2[3]),
                            Approve2UserName = Sm.DrStr(drs2, cs2[4]),
                            Approve2DocDt = Sm.DrStr(drs2, cs2[5]),
                            Approve3UserName = Sm.DrStr(drs2, cs2[6]),
                            Approve3DocDt = Sm.DrStr(drs2, cs2[7])
                        });
                    }
                }
                drs2.Close();
            }

            myLists.Add(lsign3);

            #endregion

            myLists.Add(mlJob);

            #region Old Code by wed
            //if (!mIsUseItemConsumption)
            //{
            //    if (Doctitle == "TWC") 
            //        Sm.PrintReport("MaterialRequestTWC", myLists, TableName, false);
            //    else if (Doctitle == "SIER")
            //    {
            //        if (mIsMRSPPJBEnabled)
            //        {
            //            if (mIsMRSPPJB)
            //            {
            //                Sm.PrintReport("MaterialRequestSPPJB", myLists, TableName, false);
            //                Sm.PrintReport("MaterialRequestSPPJB_2", myLists, TableName, false);
            //            }

            //            else
            //                Sm.PrintReport("MaterialRequestSIER", myLists, TableName, false);
            //        }
            //    }
            //    else if (Doctitle == "SRN")
            //    {
            //        Sm.PrintReport("MaterialRequest2TWC", myLists, TableName, false);
            //    }
            //    else
            //        Sm.PrintReport("MaterialRequest", myLists, TableName, false);
            //}
            //else
            //{
            //    if (Sm.GetParameter("DocTitle") == "KIM")
            //        Sm.PrintReport("MaterialRequestKIM", myLists, TableName, false);
            //    else
            //        Sm.PrintReport("MaterialRequest2", myLists, TableName, false);
            //}
            #endregion

            if (!mIsUseItemConsumption)
            {
                if (mIsMRSPPJBEnabled)
                {
                    if (mIsMRSPPJB)
                    {
                        Sm.PrintReport(mFormPrintOutMaterialRequestSPPJB, myLists, TableName, false);
                        Sm.PrintReport(mFormPrintOutMaterialRequestSPPJB2, myLists, TableName, false);
                    }
                    else
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                }
                else
                {
                    if (Doctitle == "IMS")
                    {
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                    }
                    else
                    {
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                    }
                }
            }
            else
            {
                Sm.PrintReport(mFormPrintOutMaterialRequestWithConsumption, myLists, TableName, false);
            }
        }

        internal void ShowPOQtyCancelInfo(string DocNo)
        {
            TxtRemainingBudget.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, D.DocDt, D.ReqType, D.DeptCode, Concat('Replacement for ', D.DocNo) As RemarkH, ");
            SQL.AppendLine("E.ItCode, F.ItName, A.Qty, F.PurchaseUomCode, E.UsageDt, E.Remark As RemarkD, ");
            SQL.AppendLine("F.ItSCCode, H.ItSCName ");
            SQL.AppendLine("From TblPOQtyCancel A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
            SQL.AppendLine("Inner Join TblDepartment G On D.DeptCode=G.DeptCode ");
            SQL.AppendLine("Left Join TblItemSubCategory H On F.ItSCCode=H.ItSCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "ReqType", "DeptCode", "RemarkH", "ItCode", 
                        //6-10
                        "ItName", "Qty", "PurchaseUomCode", "UsageDt", "RemarkD",
                        //11-12
                        "ItSCCode", "ItSCName"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, 0);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 8, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 11, 6);
                        Sm.SetGrdValue("N", Grd1, dr, c, 0, 17, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 18, 8);
                        Sm.SetGrdValue("D", Grd1, dr, c, 0, 19, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 25, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 13, 11);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 14, 12);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(ref Grd1, 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 1, new int[] { 17, 23, 24, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private string ItemSelection()
        {
            var SQL = new StringBuilder();

            if (Sm.CompareStr(Sm.GetLue(LueReqType), "2"))
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("ifnull(C.Qty01, 0) As Mth01, ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, D.ItScName, A.ItCodeInternal, A.ItGrpCode, E.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) C On C.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup E On A.ItGrpCode=E.ItGrpCode ");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("C.UPrice*");
                SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1 ");
                SQL.AppendLine("    Else IfNull(E.Amt, 0) ");
                SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, G.ItScName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL.AppendLine("    From TblQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                SQL.AppendLine("        Group By T3b.ItCode ");
                SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL.AppendLine("    From TblCurrency T1 ");
                SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                SQL.AppendLine("        From TblCurrencyRate T3a  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) F On F.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory G On A.ItScCode=G.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");

            }

            SQL.AppendLine("Where A.ActInd = 'Y' ");

            if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

            return SQL.ToString();
        }

        internal void ShowDORequestItem(string DocNo)
        {
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As DORequestDocNo, B.DNo As DORequestDNo, B.ItCode, ");
            SQL.AppendLine("X.ItCtName, X.PurchaseUomCode, X.DocNo, X.DNo, X.DocDt, X.UPrice, X.MinStock, X.ReorderStock, ");
            SQL.AppendLine("X.ItName, X.ForeignName, ");
            SQL.AppendLine("X.Mth01, X.Mth03, X.Mth06, X.Mth09, X.Mth12, ");
            SQL.AppendLine("X.ItScCode, X.ItScName, X.ItCodeInternal, X.ItGrpCode, X.ItGrpName, ");
            SQL.AppendLine("(IfNull(B.Qty, 0.00)-IfNull(D.Qty, 0.00)) As MRQty ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo And B.ProcessInd = 'O' ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine(ItemSelection());

            SQL.AppendLine(")X On B.ItCode = X.ItCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("  From TblStockSummary ");
            SQL.AppendLine("  Where WhsCode = @WhsCode ");
            SQL.AppendLine("  And Qty>0 ");
            SQL.AppendLine("  Group By ItCode ");
            SQL.AppendLine(") D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And IfNull(B.Qty, 0.00)>IfNull(D.Qty, 0.00) ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetValue("Select WhsCode From TblDORequestDeptHdr Where DocNo = '" + DocNo + "'; "));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItCodeInternal", "ItGrpName", "ItName", "ForeignName",  "ItCtName",   
                    
                    //6-7
                    "ItScCode", "ItScName", "PurchaseUomCode", "DocNo", "DNo", 

                    //11-15
                    "DocDt", "UPrice", "MinStock", "ReorderStock", "Mth01",   

                    //16-20
                    "Mth03", "Mth06", "Mth09", "Mth12", "DORequestDocNo",
                    
                    //21-22
                    "DORequestDNo", "MRQty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 22);
                    Grd1.Cells[Row, 29].Value = 0;

                    if (mIsBudgetActive)
                        Grd1.Cells[Row, 11].ForeColor = Sm.GetGrdStr(Grd1, Row, 20).Length > 0 ? Color.Black : Color.Red;
                    ComputeTotal(Row);
                }, false, false, true, false
            );
            
            Sm.FocusGrd(Grd1, 0, 1);

            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 23, 24, 29, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine("And ActInd = 'Y' ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsDeptFilterBySite)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblDepartmentBudgetSite ");
                SQL.AppendLine("    Where SiteCode=@SiteCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();
                
                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueReqType(ref DXE.LookUpEdit Lue, string ReqType)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='ReqType' ");
                if (ReqType.Length > 0)
                    SQL.AppendLine("And OptCode=@ReqType ");
                else
                {
                    if (!mIsMaterialRequestForDroppingRequestEnabled)
                    {
                        if (mIsBudgetActive && mReqTypeForNonBudget.Length > 0)
                            SQL.AppendLine("And OptCode<>@ReqTypeForNonBudget ");
                    }
                }
                SQL.AppendLine("Order By OptDesc;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@ReqType", ReqType);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        //private void SetBudgetCategory()
        //{
        //    LueBCCode.EditValue = null;
        //    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, true);

        //    var ReqType = Sm.GetLue(LueReqType);
        //    var DeptCode = Sm.GetLue(LueDeptCode);

        //    if (
        //        mReqTypeForNonBudget.Length == 0 ||
        //        ReqType.Length == 0 ||
        //        Sm.CompareStr(ReqType, mReqTypeForNonBudget) ||
        //        DeptCode.Length == 0
        //        ) return;

        //    Sl.SetLueBCCode(ref LueBCCode, string.Empty, DeptCode);
        //    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, false);
        //}

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblMaterialRequestDtl " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        private void ShowMRApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            if (mIsMRSPPJB)
                SQL.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("Where DocType='MaterialRequest' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo "); //
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "UserCode", "StatusDesc", "LastUpDt", "Remark" });
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private void UploadFile(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;

                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            //if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            //if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile4(string DocNo)
        {
            //if (IsUploadFileNotValid4()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile4.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile4.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload4.Invoke(
                    (MethodInvoker)delegate { PbUpload4.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload4.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload4.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile4(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile5(string DocNo)
        {
            //if (IsUploadFileNotValid5()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile5.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile5.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload5.Invoke(
                    (MethodInvoker)delegate { PbUpload5.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload5.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload5.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile5(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile6(string DocNo)
        {
            //if (IsUploadFileNotValid6()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile6.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile6.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload6.Invoke(
                    (MethodInvoker)delegate { PbUpload6.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload6.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload6.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile6(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile7(string DocNo)
        {
            //if (IsUploadFileNotValid7()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile7.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile7.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload7.Invoke(
                    (MethodInvoker)delegate { PbUpload7.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload7.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload7.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile7(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }
        
        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsUploadFileNotValid4()
        {
            return
                IsFTPClientDataNotValid4() ||
                IsFileSizeNotvalid4() ||
                IsFileNameAlreadyExisted4();
        }

        private bool IsUploadFileNotValid5()
        {
            return
                IsFTPClientDataNotValid5() ||
                IsFileSizeNotvalid5() ||
                IsFileNameAlreadyExisted5();
        }

        private bool IsUploadFileNotValid6()
        {
            return
                IsFTPClientDataNotValid6() ||
                IsFileSizeNotvalid6() ||
                IsFileNameAlreadyExisted6();
        }

        private bool IsUploadFileNotValid7()
        {
            return
                IsFTPClientDataNotValid7() ||
                IsFileSizeNotvalid7() ||
                IsFileNameAlreadyExisted7();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length>0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }
            

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }
            
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }
           

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            
            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }
            
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }
            
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }
           
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

           if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

           
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid4()
        {

            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid5()
        {

            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid6()
        {

            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid7()
        {

            if (TxtFile7.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtFile7.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (TxtFile7.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtFile7.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 )
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

               
                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsMRAllowToUploadFile  && TxtFile2.Text.Length > 0 )
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile2.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile3.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid4()
        {
            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile4.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile4.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid5()
        {
            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile5.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile5.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid6()
        {
            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile6.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile6.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid7()
        {
            if (TxtFile7.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile7.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > double.Parse("50"))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile7.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblIndependentEstimatedPriceHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2 ()
        {
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblIndependentEstimatedPriceHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblIndependentEstimatedPriceHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted4()
        {
            if (TxtFile4.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile4.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblIndependentEstimatedPriceHdr ");
                SQL.AppendLine("Where FileName4=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted5()
        {
            if (TxtFile5.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile5.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblIndependentEstimatedPriceHdr ");
                SQL.AppendLine("Where FileName5=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted6()
        {
            if (TxtFile6.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile6.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblIndependentEstimatedPriceHdr ");
                SQL.AppendLine("Where FileName6=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted7()
        {
            if (TxtFile7.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile7.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblIndependentEstimatedPriceHdr ");
                SQL.AppendLine("Where FileName7=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }        

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();
                
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress,":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true; 

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false; 

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }
                       
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile4(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload4.Value = 0;
                PbUpload4.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload4.Value = PbUpload4.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload4.Value + bytesRead <= PbUpload4.Maximum)
                        {
                            PbUpload4.Value += bytesRead;

                            PbUpload4.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile5(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload5.Value = 0;
                PbUpload5.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload5.Value = PbUpload5.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload5.Value + bytesRead <= PbUpload5.Maximum)
                        {
                            PbUpload5.Value += bytesRead;

                            PbUpload5.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile6(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload6.Value = 0;
                PbUpload6.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload6.Value = PbUpload6.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload6.Value + bytesRead <= PbUpload6.Maximum)
                        {
                            PbUpload6.Value += bytesRead;

                            PbUpload6.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile7(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload7.Value = 0;
                PbUpload7.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload7.Value = PbUpload7.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload7.Value + bytesRead <= PbUpload7.Maximum)
                        {
                            PbUpload7.Value += bytesRead;

                            PbUpload7.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void LabelForMR()
        {
            string[] mLabelFile = { };

            if (mLabelFileForMR.Length > 0)
            {
                mLabelFile = mLabelFileForMR.Split(',');
            }

            Label[] lblFile = new Label[6] { LblFile, LblFile2, LblFile3, LblFile4, LblFile5, LblFile6 };

            for (int i = 0; i < mLabelFile.Length; i++)
            {
                lblFile[i].Text = mLabelFile[i];
            }

            int distance = mLabelFile.Max(s => s.Length);

            splitContainer1.SplitterDistance = distance * 7;

            for (int i = 0; i < lblFile.Length; i++)
            {
                int x = splitContainer1.SplitterDistance - lblFile[i].Width;
                lblFile[i].Left = x;
            }
        }

        private void MandatoryFile()
        {
            string[] mMandatoryFile = { };

            if (mMandatoryFileForMR.Length > 0)
            {
                mMandatoryFile = mMandatoryFileForMR.Split(',');

                foreach (string lbl in mMandatoryFile)
                {
                    switch (lbl.Trim().ToUpper())
                    {
                        case "FILE1":
                            LblFile.ForeColor = Color.Red;
                            break;
                        case "FILE2":
                            LblFile2.ForeColor = Color.Red;
                            break;
                        case "FILE3":
                            LblFile3.ForeColor = Color.Red;
                            break;
                        case "FILE4":
                            LblFile4.ForeColor = Color.Red;
                            break;
                        case "FILE5":
                            LblFile5.ForeColor = Color.Red;
                            break;
                        case "FILE6":
                            LblFile6.ForeColor = Color.Red;
                            break;
                    }
                }
            }
        }

        private bool IsFileMandatory()
        {
            string[] mMandatoryFile = { };

            if (mMandatoryFileForMR.Length > 0)
            {
                mMandatoryFile = mMandatoryFileForMR.Split(',');

                foreach (var mMandatoryInd in mMandatoryFile)
                {
                    if (mMandatoryInd.Trim().ToUpper() == "FILE1")
                    {
                        if (TxtFile.Text == "" || TxtFile.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile.Text + " is Empty");
                            TxtFile.Focus();
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE2")
                    {
                        if (TxtFile2.Text == "" || TxtFile2.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile2.Text + " is Empty");
                            TxtFile2.Focus();
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE3")
                    {
                        if (TxtFile3.Text == "" || TxtFile3.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile3.Text + " is Empty");
                            TxtFile3.Focus();
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE4")
                    {
                        if (TxtFile4.Text == "" || TxtFile4.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile4.Text + " is Empty");
                            TxtFile4.Focus();
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE5")
                    {
                        if (TxtFile5.Text == "" || TxtFile5.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile5.Text + " is Empty");
                            TxtFile5.Focus();
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE6")
                    {
                        if (TxtFile6.Text == "" || TxtFile6.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile6.Text + " is Empty");
                            TxtFile6.Focus();
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        internal void SetDroppingReqProjImplItQty()
        {
            //if (TxtDR_PRJIDocNo.Text.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select T2.ResourceItCode, T1.Qty, IfNull(T3.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl T1 ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr T2 On T1.PRBPDocNo=T2.DocNo ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl2 T3 On T2.PRJIDocNo=T3.DocNo And T2.ResourceItCode=T3.ResourceItCode ");
            SQL.AppendLine("Where T1.DocNo=@DroppingRequestDocNo And T1.MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                //Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "ResourceItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) && Sm.GetGrdDec(Grd1, r, 31)==0m)
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void SetDroppingReqBCItQty()
        {
            if (mDroppingRequestBCCode.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select ItCode, Qty, Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl2 ");
            SQL.AppendLine("Where DocNo=@DroppingRequestDocNo ");
            SQL.AppendLine("And BCCode=@DroppingRequestBCCode ");
            SQL.AppendLine("And MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                //Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) &&
                                Sm.GetGrdDec(Grd1, r, 31) == 0m
                                )
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueProcurementType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcurementType, new Sm.RefreshLue2(Sl.SetLueOption), "ProcurementType");
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
                if (mIsDeptFilterBySite)
                {
                    if (Sm.GetLue(LueSiteCode).Length > 0)
                    {
                        Sm.SetControlReadOnly(LueDeptCode, false);
                        LueDeptCode.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                    }
                    else
                    {
                        LueDeptCode.EditValue = null;
                        Sm.SetControlReadOnly(LueDeptCode, true);
                    }
                }
                if (mIsMRSPPJB && mIsFilterBySite)
                {
                    ClearGrd();
                    mlJob.Clear();
                    if (mSourceAvailableBudgetForMR == "1")
                        ComputeRemainingBudget();
                }
            }
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(Sl.SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
            if (mSourceAvailableBudgetForMR == "1") ComputeRemainingBudget();
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }
        private void TxtGrandTotalIndependentEstimatedPrice_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtGrandTotalIndependentEstimatedPrice, 0);            
        }

        public static void SetLueCurCode(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select CurCode As Col1 From tblCurrency ", "Currency");
        }

        private void SetLuePICCode (ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select UserCode As Col1, UserName As Col2 ");
                SQL.AppendLine("From TblUser ");
                if(TxtDocNo.Text.Length == 0 && mIsPICGrouped)
                    SQL.AppendLine("where UserCode=@UserCode ");
                SQL.AppendLine("Order By UserName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (LueCurCode.Visible && fAccept && fCell.ColIndex == 28)
            {
                if (Sm.GetLue(LueCurCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 28].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 28].Value = Sm.GetLue(LueCurCode);
                }
                LueCurCode.Visible = false;
            }
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void ChkFile4_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile4.Checked == false)
            {
                TxtFile4.EditValue = string.Empty;
            }
        }

        private void ChkFile5_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile5.Checked == false)
            {
                TxtFile5.EditValue = string.Empty;
            }
        }

        private void ChkFile6_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile6.Checked == false)
            {
                TxtFile6.EditValue = string.Empty;
            }
        }
        
        private void ChkFile7_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile7.Checked == false)
            {
                TxtFile7.EditValue = string.Empty;
            }
        }

        private void LuePICCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(SetLuePICCode));
            
        }

        private void LueDurationUom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDurationUom, new Sm.RefreshLue2(Sl.SetLueOption), "DurationUom");
        }

        private void LueDurationUom_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDurationUom_Leave(object sender, EventArgs e)
        {
            if (LueDurationUom.Visible && fAccept && fCell.ColIndex == 37)
            {
                if (Sm.GetLue(LueDurationUom).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 36].Value =
                    Grd1.Cells[fCell.RowIndex, 37].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 36].Value = Sm.GetLue(LueDurationUom);
                    Grd1.Cells[fCell.RowIndex, 37].Value = LueDurationUom.GetColumnValue("Col2");
                }
                LueDurationUom.Visible = false;
            }
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LuePtCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePtCode_Leave(object sender, EventArgs e)
        {
            if (LuePtCode.Visible && fAccept && fCell.ColIndex == 40)
            {
                if (Sm.GetLue(LuePtCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 39].Value =
                    Grd1.Cells[fCell.RowIndex, 40].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 39].Value = Sm.GetLue(LuePtCode);
                    Grd1.Cells[fCell.RowIndex, 40].Value = LuePtCode.GetColumnValue("Col2");
                }
                LuePtCode.Visible = false;
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Button  Event

        private void BtnMRDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmIndependentEstimatedPriceDlg(this));
        }

        private void BtnMRDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtMRDocNo, "Material Request#", false))
            {
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtMRDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnPOQtyCancelDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPOQtyCancelDocNo, "Cancellation PO#", false))
            {
                var f = new FrmPOQtyCancel(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPOQtyCancelDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDORequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false))
            {
                var f = new FrmDORequestDept(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDORequestDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        internal class Job
        {
            public string ItCode { get; set; }
            public string JobCode { get; set; }
            public string JobName { get; set; }
            public string JobCtName { get; set; }
            public string UomCode { get; set; }
            public string PrevCurCode { get; set; }
            public decimal PrevUPrice { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Qty { get; set; }
            public string Remark { get; set; }
            public decimal Total { get; set; }
            public int No { get; set; }
        }

        #region Report Class

        private class MatReq
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string OptDesc { get; set; }
            public string HRemark { get; set; }
            public string CreateBy { get; set; }
            public string PrintBy { get; set; }
            public string SiteInd { get; set; }
            public string LocalDocNo { get; set; }
            public string BCCode { get; set; }

        }

        class MatReq1
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MatReq2
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MatReqDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string UsageDt { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth12 { get; set; }
            public string DRemark { get; set; }
            public string ForeignName { get; set; }
            public decimal EstPrice { get; set; }
            public string Curcode { get; set; }
            public string Duration { get; set; }
            public string DurationUom { get; set; }
        }

        private class MatReqDtl2
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }

        }

        private class MatReqDtl3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string IsPrintOutUseDigitalSignature { get; set; }
        }

        private class MatReqDtl4
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string IsPrintOutUseDigitalSignature { get; set; }
        }

        private class MatReqSier
        {
            public string BudgetCategory { get; set; }
            public decimal Budget { get; set; }
            public decimal AfterUsageAmt { get; set; }
            public decimal UsageAmt { get; set; }
            public decimal BalanceAmt { get; set; }
            public decimal UsageAmt2 { get; set; }
            public decimal AvailableBudget { get; set; }
            public decimal Amt { get; set; }


            
        }

        private class MatReqSPPJB
        {
            public int No { get; set;  }
            public string JobName { get; set; }
            public string JobCtName { get; set; }
            public decimal UPrice { get; set; }
            public decimal Qty { get; set; }
            public string ItName { get; set; }
            public decimal Total { get; set; }
        }

        private class SignatureSIER
        {
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Date { get; set; }


        }

        private class MRIMS
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string DocName { get; set; }
            public string CompanyLogo { get; set; }
        }

        private class MRIMSDtl
        {
            public int No { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string ProjectCode { get; set; }
            public string UsageDt { get; set; }
            public string Remark { get; set; }
        }

        private class MRIMSSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MRIMSSign2
        {
            public string CreateUserName { get; set; }
            public string CreateDocDt { get; set; }
            public string ApproveUserName { get; set; }
            public string ApproveDocDt { get; set; }
            public string Approve2UserName { get; set; }
            public string Approve2DocDt { get; set; }
            public string Approve3UserName { get; set; }
            public string Approve3DocDt { get; set; }
        }
        #endregion            

        #endregion

    }
}
