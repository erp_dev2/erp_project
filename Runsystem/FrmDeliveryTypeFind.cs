﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmDeliveryTypeFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDeliveryType mFrmParent;

        #endregion

        #region Constructor

        public FrmDeliveryTypeFind(FrmDeliveryType FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Delivery Type"+Environment.NewLine+"Code", 
                        "Delivery Type"+Environment.NewLine+"Name",
                        "Customer Quotation"+Environment.NewLine+"Indicator",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        
                        //6-8
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 5, 8 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatTime(Grd1, new int[] { 6, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDTName.Text, new string[] { "DTCode", "DTName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select DTCode, DTName, CtQtInd, CreateBy, CreateDt, LastUpBy, LastUpDt " +
                        "From TblDeliveryType " +   
                        Filter + " Order By DTName",
                        new string[]
                        {
                            //0
                            "DTCode", 
                                
                            //1-5
                            "DTName", "CtQtInd", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 9, 6);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDTName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDTName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Delivery type");
        }

        #endregion

        #endregion
    }
}
