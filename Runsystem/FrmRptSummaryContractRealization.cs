﻿#region Update
/*
    10/03/2020 [WED/YK] new apps
    12/03/2020 [WED/YK] rename file DetailProductionRealization --> DetailContractRealization. ubah nama parameter juga
    18/03/2020 [WED/YK] rename file DetailContractRealization --> SummaryContractRealization. ubah nama parameter juga
    24/03/2020 [WED/YK] site nya pakai paramete SiteCodeForAuctionInfo
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSummaryContractRealization : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mCOAForSummaryRealization = string.Empty,
            mDescForSummaryRealization = string.Empty,
            mSiteCodeForAuctionInfo = string.Empty;

        private int[] mColDec = { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };

        #endregion

        #region Constructor

        public FrmRptSummaryContractRealization(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Project Description",
                    "Site Code",
                    "Site Name", 
                    "RKAP Amount",
                    "Carry Over", 
                    
                    //6-10
                    "Realization"+Environment.NewLine+"January",
                    "Realization"+Environment.NewLine+"February",
                    "Realization"+Environment.NewLine+"March",
                    "Total 1st Quarterly",
                    "Realization"+Environment.NewLine+"April",

                    //11-15
                    "Realization"+Environment.NewLine+"May",
                    "Realization"+Environment.NewLine+"June",
                    "Total 2nd Quarterly",
                    "Realization"+Environment.NewLine+"July",
                    "Realization"+Environment.NewLine+"August",

                    //16-20
                    "Realization"+Environment.NewLine+"September",
                    "Total 3rd Quaterly",
                    "Realization"+Environment.NewLine+"October",
                    "Realization"+Environment.NewLine+"November",
                    "Realization"+Environment.NewLine+"December",

                    //21-25
                    "Total 4th Quarterly",
                    "Total New Contract",
                    "Total Co +"+Environment.NewLine+"New Contract",
                    "Percentage"+Environment.NewLine+"towards Target",
                    "Balance"
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    130, 100, 150, 150, 150,  
                    
                    //6-10
                    150, 150, 150, 150, 150,

                    //11-15
                    150, 150, 150, 150, 150, 

                    //16-20
                    150, 150, 150, 150, 150, 

                    //21-25
                    150, 150, 150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, mColDec, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ProjectSite>();
                var l2 = new List<ContractAmt>();

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l2);
                    if (l2.Count > 0) Process3(ref l, ref l2);
                    Process4(ref l);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }

                l.Clear(); l2.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mCOAForSummaryRealization = Sm.GetParameter("COAForSummaryRealization");
            mDescForSummaryRealization = Sm.GetParameter("DescForSummaryRealization");
            mSiteCodeForAuctionInfo = Sm.GetParameter("SiteCodeForAuctionInfo");

            if (mCOAForSummaryRealization.Length == 0) mCOAForSummaryRealization = "4.00,4.10";
            if (mDescForSummaryRealization.Length == 0) mDescForSummaryRealization = "PROYEK DITANGAN,PROYEK MEMBER DALAM KSO";
        }

        private void Process1(ref List<ProjectSite> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string[] mProjectDesc = mDescForSummaryRealization.Split(',');
            string[] mCOA = mCOAForSummaryRealization.Split(',');

            SQL.AppendLine("SELECT T.ProjectDesc, T.SiteCode, T.SiteName, T.RKAPAmt ");
            SQL.AppendLine("FROM ( ");

            for (int i = 0; i < mProjectDesc.Count(); ++i)
            {
                SQL.AppendLine("    SELECT '" + mProjectDesc[i] + "' AS ProjectDesc, A.SiteCode, A.SiteName, IFNULL(B.Amt, 0.00) RKAPAmt ");
                SQL.AppendLine("    FROM TblSite A ");
                SQL.AppendLine("    LEFT JOIN ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        SELECT T2.AcNo, SUM(T2.Amt) Amt ");
                SQL.AppendLine("        FROM TblCompanyBudgetPlanHdr T1 ");
                SQL.AppendLine("        INNER JOIN TblCompanyBudgetPlanDtl T2 ON T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            AND T1.Yr = @Yr ");
                SQL.AppendLine("            AND T1.CancelInd = 'N' ");
                SQL.AppendLine("            And T2.Amt != 0.00 ");
                SQL.AppendLine("            AND  ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                  T2.AcNo LIKE '" + mCOA[i] + "%' ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        INNER JOIN TblCOA T3 ON T2.AcNo = T3.AcNo ");
                SQL.AppendLine("            AND T3.ActInd = 'Y' ");
                SQL.AppendLine("        INNER JOIN TblParameter T4 ON T4.ParCode = 'COALevelFicoSettingJournalToCBP' ");
                SQL.AppendLine("            AND T4.ParValue IS NOT NULL ");
                SQL.AppendLine("            AND T3.Level = CONVERT(T4.ParValue, DECIMAL)+1 ");
                SQL.AppendLine("        Inner Join TblSite T5 On Right(T2.AcNo, 3) = T5.SiteCode ");
                SQL.AppendLine("            And Find_In_Set(T5.SiteCode, @SiteCode) ");
                SQL.AppendLine("        GROUP BY T2.AcNo ");
                SQL.AppendLine("    ) B ON A.SiteCode = RIGHT(B.AcNo, 3) ");
                SQL.AppendLine("    WHERE Find_In_set(A.SiteCode, @SiteCode) ");

                if (i != (mProjectDesc.Count() - 1))
                    SQL.AppendLine("    UNION ALL ");
            }

            SQL.AppendLine(") T ");
            SQL.AppendLine("ORDER BY T.ProjectDesc, T.SiteCode; ");
            
            //SQL.AppendLine("    SELECT 'PROYEK MEMBER DALAM KSO' AS ProjectDesc, A.SiteCode, A.SiteName, IFNULL(B.Amt, 0.00) RKAPAmt ");
            //SQL.AppendLine("    FROM TblSite A ");
            //SQL.AppendLine("    LEFT JOIN ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        SELECT T2.AcNo, SUM(T2.Amt) Amt ");
            //SQL.AppendLine("        FROM TblCompanyBudgetPlanHdr T1 ");
            //SQL.AppendLine("        INNER JOIN TblCompanyBudgetPlanDtl T2 ON T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("            AND T1.Yr = @Yr ");
            //SQL.AppendLine("            AND T1.CancelInd = 'N' ");
            //SQL.AppendLine("            AND  ");
            //SQL.AppendLine("              ( ");
            //SQL.AppendLine("                  T2.AcNo LIKE '4.10%' ");
            //SQL.AppendLine("            ) ");
            //SQL.AppendLine("        INNER JOIN TblCOA T3 ON T2.AcNo = T3.AcNo ");
            //SQL.AppendLine("            AND T3.ActInd = 'Y' ");
            //SQL.AppendLine("        INNER JOIN TblParameter T4 ON T4.ParCode = 'COALevelFicoSettingJournalToCBP' ");
            //SQL.AppendLine("             AND T4.ParValue IS NOT NULL ");
            //SQL.AppendLine("             AND T3.Level = CONVERT(T4.ParValue, DECIMAL)+1 ");
            //SQL.AppendLine("        GROUP BY T2.AcNo ");
            //SQL.AppendLine("    ) B ON A.SiteCode = RIGHT(B.AcNo, 3) ");
            //SQL.AppendLine("    WHERE A.ActInd = 'Y' ");
            //SQL.AppendLine("    AND A.HOInd = 'N' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCodeForAuctionInfo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "ProjectDesc",
                    //1-3
                    "SiteCode", "SiteName", "RKAPAmt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProjectSite()
                        {
                            ProjectDesc = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            RKAPAmt = Sm.DrDec(dr, c[3]),
                            CarryOverAmt = 0m,
                            Amt1 = 0m,
                            Amt2 = 0m,
                            Amt3 = 0m,
                            Total1 = 0m,
                            Amt4 = 0m,
                            Amt5 = 0m,
                            Amt6 = 0m,
                            Total2 = 0m,
                            Amt7 = 0m,
                            Amt8 = 0m,
                            Amt9 = 0m,
                            Total3 = 0m,
                            Amt10 = 0m,
                            Amt11 = 0m,
                            Amt12 = 0m,
                            Total4 = 0m,
                            TotalNewContract = 0m,
                            TotalCoNewContract = 0m,
                            PercentageTowardsTarget = 0m,
                            Balance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ContractAmt> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string[] mTblAlias = { "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O" };

            SQL.AppendLine("SELECT A.SiteCode, IFNULL(C.Amt, 0.00) CarryOverAmt, ");
            
            for (int i = 1; i < mTblAlias.Count() + 1; ++i)
            {
                SQL.AppendLine("IfNull(" + mTblAlias[i - 1] + ".Amt, 0.00) Amt" + i);
                if (i != (mTblAlias.Count()))
                {
                    SQL.Append(", ");
                }
            }

            SQL.AppendLine("FROM TblSite A ");
            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T5.SiteCode, SUM(T2.Amt) Amt ");
            SQL.AppendLine("    FROM (SELECT MAX(DocNo) DocNo FROM TblSOContractRevisionHdr GROUP BY SOCDocNo) T1 ");
            SQL.AppendLine("    INNER JOIN TblSOContractRevisionHdr T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    INNER JOIN TblSOContractHdr T3 ON T2.SOCDocNo = T3.DocNo ");
	        SQL.AppendLine("         AND LEFT(T3.DocDt, 4) < @Yr ");
	        SQL.AppendLine("         AND T3.CancelInd = 'N' ");
	        SQL.AppendLine("         AND T3.Status = 'A' ");
            SQL.AppendLine("    INNER JOIN TblBOQHDr T4 ON T3.BOQDocNo = T4.DocNo ");
            SQL.AppendLine("    INNER JOIN TblLOPHdr T5 ON T4.LOPDocNo = T5.DocNo ");
            SQL.AppendLine("        And Find_In_Set(T5.SiteCode, @SiteCode) ");
            SQL.AppendLine("    GROUP BY T5.SiteCode ");
            SQL.AppendLine(") C ON A.SiteCode = C.SiteCode ");

            for (int i = 1; i < mTblAlias.Count() + 1; ++i)
            {
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT T5.SiteCode, SUM(T2.Amt) Amt ");
                SQL.AppendLine("    FROM (SELECT MAX(DocNo) DocNo FROM TblSOContractRevisionHdr GROUP BY SOCDocNo) T1 ");
                SQL.AppendLine("    INNER JOIN TblSOContractRevisionHdr T2 ON T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    INNER JOIN TblSOContractHdr T3 ON T2.SOCDocNo = T3.DocNo ");
                SQL.AppendLine("	     AND LEFT(T3.DocDt, 6) = Concat(@Yr, '" + Sm.Right(string.Concat("00", i), 2) + "') ");
                SQL.AppendLine("	     AND T3.CancelInd = 'N' ");
                SQL.AppendLine("	     AND T3.Status = 'A' ");
                SQL.AppendLine("    INNER JOIN TblBOQHDr T4 ON T3.BOQDocNo = T4.DocNo ");
                SQL.AppendLine("    INNER JOIN TblLOPHdr T5 ON T4.LOPDocNo = T5.DocNo ");
                SQL.AppendLine("        And Find_In_Set(T5.SiteCode, @SiteCode) ");
                SQL.AppendLine("    GROUP BY T5.SiteCode ");
                SQL.AppendLine(") " + mTblAlias[i - 1] + " ON A.SiteCode = " + mTblAlias[i - 1] + ".SiteCode ");
            }

            SQL.AppendLine("WHERE A.ActInd = 'Y' ");
            SQL.AppendLine("AND A.HOInd = 'N'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCodeForAuctionInfo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "SiteCode",
                    //1-5
                    "CarryOverAmt", "Amt1", "Amt2", "Amt3", "Amt4", 
                    //6-10
                    "Amt5", "Amt6", "Amt7", "Amt8", "Amt9", 
                    //11-13
                    "Amt10", "Amt11", "Amt12"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ContractAmt()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            CarryOverAmt = Sm.DrDec(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            Amt3 = Sm.DrDec(dr, c[4]),
                            Amt4 = Sm.DrDec(dr, c[5]),
                            Amt5 = Sm.DrDec(dr, c[6]),
                            Amt6 = Sm.DrDec(dr, c[7]),
                            Amt7 = Sm.DrDec(dr, c[8]),
                            Amt8 = Sm.DrDec(dr, c[9]),
                            Amt9 = Sm.DrDec(dr, c[10]),
                            Amt10 = Sm.DrDec(dr, c[11]),
                            Amt11 = Sm.DrDec(dr, c[12]),
                            Amt12 = Sm.DrDec(dr, c[13]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<ProjectSite> l, ref List<ContractAmt> l2)
        {
            foreach(var x in l)
            {
                foreach(var y in l2)
                {
                    if(x.SiteCode == y.SiteCode)
                    {
                        x.CarryOverAmt = y.CarryOverAmt;
                        x.Amt1 = y.Amt1;
                        x.Amt2 = y.Amt2;
                        x.Amt3 = y.Amt3;
                        x.Total1 = y.Amt1 + y.Amt2 + y.Amt3;
                        x.Amt4 = y.Amt4;
                        x.Amt5 = y.Amt5;
                        x.Amt6 = y.Amt6;
                        x.Total2 = y.Amt4 + y.Amt5 + y.Amt6;
                        x.Amt7 = y.Amt7;
                        x.Amt8 = y.Amt8;
                        x.Amt9 = y.Amt9;
                        x.Total3 = y.Amt7 + y.Amt8 + y.Amt9;
                        x.Amt10 = y.Amt10;
                        x.Amt11 = y.Amt11;
                        x.Amt12 = y.Amt12;
                        x.Total4 = y.Amt10 + y.Amt11 + y.Amt12;
                        x.TotalNewContract = x.Total1 + x.Total2 + x.Total3 + x.Total4;
                        x.TotalCoNewContract = x.TotalNewContract + x.CarryOverAmt;
                        if (x.RKAPAmt != 0m) x.PercentageTowardsTarget = (x.TotalCoNewContract / x.RKAPAmt) * 100m;
                        x.Balance = x.TotalCoNewContract - x.RKAPAmt;

                        break;
                    }
                }
            }
        }

        private void Process4(ref List<ProjectSite> l)
        {
            int Row = 0;
            foreach(var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.ProjectDesc;
                Grd1.Cells[Row, 2].Value = x.SiteCode;
                Grd1.Cells[Row, 3].Value = x.SiteName;
                Grd1.Cells[Row, 4].Value = x.RKAPAmt;
                Grd1.Cells[Row, 5].Value = x.CarryOverAmt;
                Grd1.Cells[Row, 6].Value = x.Amt1;
                Grd1.Cells[Row, 7].Value = x.Amt2;
                Grd1.Cells[Row, 8].Value = x.Amt3;
                Grd1.Cells[Row, 9].Value = x.Total1;
                Grd1.Cells[Row, 10].Value = x.Amt4;
                Grd1.Cells[Row, 11].Value = x.Amt5;
                Grd1.Cells[Row, 12].Value = x.Amt6;
                Grd1.Cells[Row, 13].Value = x.Total2;
                Grd1.Cells[Row, 14].Value = x.Amt7;
                Grd1.Cells[Row, 15].Value = x.Amt8;
                Grd1.Cells[Row, 16].Value = x.Amt9;
                Grd1.Cells[Row, 17].Value = x.Total3;
                Grd1.Cells[Row, 18].Value = x.Amt10;
                Grd1.Cells[Row, 19].Value = x.Amt11;
                Grd1.Cells[Row, 20].Value = x.Amt12;
                Grd1.Cells[Row, 21].Value = x.Total4;
                Grd1.Cells[Row, 22].Value = x.TotalNewContract;
                Grd1.Cells[Row, 23].Value = x.TotalCoNewContract;
                Grd1.Cells[Row, 24].Value = x.PercentageTowardsTarget;
                Grd1.Cells[Row, 25].Value = x.Balance;

                Row += 1;
            }

            Grd1.GroupObject.Add(1);
            Grd1.Group();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, mColDec);
        }

        #endregion

        #endregion

        #region Class

        private class ProjectSite
        {
            public string ProjectDesc { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public decimal RKAPAmt { get; set; }
            public decimal CarryOverAmt { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Total1 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Total2 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
            public decimal Amt9 { get; set; }
            public decimal Total3 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
            public decimal Total4 { get; set; }
            public decimal TotalNewContract { get; set; }
            public decimal TotalCoNewContract { get; set; }
            public decimal PercentageTowardsTarget { get; set; }
            public decimal Balance { get; set; }
        }

        private class ContractAmt
        {
            public string SiteCode { get; set; }
            public decimal CarryOverAmt { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
            public decimal Amt9 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
        }

        #endregion
    }
}
