﻿#region Update
/*
    22/07/2019 [DITA] master baru keperluan IMM
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMPlatform : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmIMMPlatformFind FrmFind;

        #endregion

        #region Constructor

        public FrmIMMPlatform(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPlatformCode, TxtPlatformName
                    }, true);
                    TxtPlatformCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPlatformCode, TxtPlatformName
                    }, false);
                    TxtPlatformCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtPlatformCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPlatformName
                    }, false);
                    TxtPlatformName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtPlatformCode, TxtPlatformName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMPlatformFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPlatformCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblIMMPlatform(PFCode, PFName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PFCode, @PFName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update PFName=@PFName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PFCode", TxtPlatformCode.Text);
                Sm.CmParam<String>(ref cm, "@PFName", TxtPlatformName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPlatformCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PFCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PFCode", PFCode);
                Sm.ShowDataInCtrl(
                    ref cm,
                    "Select PFCode, PFName From TblIMMPlatform Where PFCode=@PFCode",
                    new string[] 
                    {
                        "PFCode", "PFName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtPlatformCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtPlatformName.EditValue = Sm.DrStr(dr, c[1]);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPlatformCode, "Platform code", false) ||
                Sm.IsTxtEmpty(TxtPlatformName, "Platform name", false) ||
                IsPlatformCodeExisted();
        }

        private bool IsPlatformCodeExisted()
        {
            if (!TxtPlatformCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select PFCode From TblIMMPlatform Where PFCode=@Param Limit 1;", TxtPlatformCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Platform code ( " + TxtPlatformCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events
        private void TxtPlatformCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPlatformCode);
        }

        #endregion

        private void TxtPlatformName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPlatformName);
        }

        #endregion

       
    }
}
