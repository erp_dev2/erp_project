﻿#region Update
/*
    29/05/2018 [TKG] Quantity PO yang ditampilkan hanya PO yg sudah di-approved
    28/12/2021 [RDA/PHT] Support : opsi lue department yang muncul belum terfilter berdasarkan group
    28/01/2021 [TYO/PHT] Menambah kolom Price Revision dan Quantity Revision berdasarkan parameter IsPORequestProcessUsePORevision
    14/04/2022 [RIS/PHT] Merubah parameter IsPORequestProcessUsePORevision menjadi IsRptProcurementUsePORevision
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPORequestDeptStatus : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool 
            mIsShowForeignName = false, 
            mIsFilterByItCt = false,
            mIsRptProcurementUsePORevision = false;

        #endregion

        #region Constructor

        public FrmRptPORequestDeptStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo, B.DNo, A.LocalDocNo, E.DeptCode, F.DeptName, A.DocDt, C.ItCode, D.ItName, D.ForeignName, ");
            SQL.AppendLine("Case B.Status When 'C' Then 'Cancel' When 'O' Then 'Outstanding' When 'A' Then 'Approved' End As StatusDesc, ");
            SQL.AppendLine("IfNull(M.Level, 0) As level, ");
            SQL.AppendLine("I.UserPor As User, ");
            SQL.AppendLine("IfNull(J.PORequestQty, 0) As PORequestQty, ");
            SQL.AppendLine("D.PurchaseUomCode, ");
            SQL.AppendLine("IfNull(K.POQty, 0) As POQty, "); 
            SQL.AppendLine("IfNull(L.RecvVdQty, 0) As RecvVdQty,  ");
            if(mIsRptProcurementUsePORevision)
                SQL.AppendLine("IfNull(P.PORevisionQty, 0) As PORevisionQty, IfNull(P.PORevisionPrice, 0) As PORevisionPrice, ");
            else
                SQL.AppendLine("Null As PORevisionQty, Null As PORevisionPrice, ");
            SQL.AppendLine("N.CurCode, O.UPrice  ");
            SQL.AppendLine(" From TblPORequestHdr A ");
            SQL.AppendLine(" Inner Join TblPORequestDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine(" Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo ");
            SQL.AppendLine(" Inner Join TblItem D On C.ItCode = D.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(" Inner Join TblMaterialRequestHdr E On C.DocNo = E.DocNo ");
            SQL.AppendLine(" Inner Join TblDepartment F On E.DeptCode = F.DeptCode  ");
            SQL.AppendLine(" Inner Join TblDocApproval G On B.DocNo = G.DocNo And B.DNo = G.DNo And G.DocType='PORequest' ");
            SQL.AppendLine(" Inner Join TblUser H On G.UserCode = H.UserCode ");
            SQL.AppendLine(" Left Join (");
            SQL.AppendLine("     Select A.DocNo, A.DNo, Group_Concat(B.UserName Separator ', ') As UserPOR ");
            SQL.AppendLine("     From TblDocApproval A ");
            SQL.AppendLine("     Inner Join TblUser B On A.UserCode = B.UserCode "); 
            SQL.AppendLine("     Where A.DocType = 'PORequest' ");
            SQL.AppendLine("     Group By A.DocNo, A.DNo) I On B.DocNo = I.DocNo And B.DNo = I.Dno ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("     Select T1.DocNo, T1.DNo, Sum(T1.Qty) As PORequestQty ");
            SQL.AppendLine("     From TblPORequestDtl T1 ");
            SQL.AppendLine("    Inner Join TblPORequestHdr T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo "); 
            SQL.AppendLine("        And (Left(T2.DocDt, 8)>=Left(@DocDt1, 8) And Left(T2.DocDt, 8)<=Left(@DocDt2, 8)) ");
            SQL.AppendLine("     Where T1.CancelInd='N' And T1.Status<>'C' ");
            SQL.AppendLine("     Group By T1.DocNo, T1.DNo ");
            SQL.AppendLine(") J On A.DocNo=J.DocNo And B.DNo=J.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.DocNo, T3.DNo, Sum(T2.Qty) As POQty  ");
            SQL.AppendLine("    From TblPOHdr T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblPORequestHdr T4 ");
            SQL.AppendLine("        On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("        And (Left(T4.DocDt, 8)>=Left(@DocDt1, 8) And Left(T4.DocDt, 8)<=Left(@DocDt2, 8)) ");
            SQL.AppendLine("    Where T1.Status='A' ");
            SQL.AppendLine("    Group By T3.DocNo, T3.DNo ");
            SQL.AppendLine(") K On A.DocNo=K.DocNo And B.DNo=K.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("     Select  T3.DocNo, T3.DNo, Sum(T1.Qty) As RecvVdQty ");
            SQL.AppendLine("     From TblRecvVdDtl T1 ");
            SQL.AppendLine("     Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo ");
            SQL.AppendLine("     Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblPORequestHdr T4 ");
            SQL.AppendLine("        On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("        And (Left(T4.DocDt, 8)>=Left(@DocDt1, 8) And Left(T4.DocDt, 8)<=Left(@DocDt2, 8)) ");
            SQL.AppendLine("     Where T1.CancelInd='N' ");
            SQL.AppendLine("     Group By T3.DocNo, T3.DNo ");
            SQL.AppendLine(") L On A.DocNo=L.DocNo And B.DNo=L.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DNo, Count(T1.DNo) As Level ");
            SQL.AppendLine("    From TblDocApproval T1 ");
            SQL.AppendLine("    Inner Join TblPORequestHdr T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (Left(T2.DocDt, 8)>=Left(@DocDt1, 8) And Left(T2.DocDt, 8)<=Left(@DocDt2, 8)) ");
            SQL.AppendLine("     Where T1.DocType='PORequest' ");
            SQL.AppendLine("     Group By T1.DocNo, T1.DNo "); 
            SQL.AppendLine(") M On A.DocNo=M.DocNo And B.DNo=M.DNo ");
            SQL.AppendLine("Left Join TblQtHdr N On B.QtDocNo=N.DocNo ");
            SQL.AppendLine("Left Join TblQtDtl O On B.QtDocNo=O.DocNo And B.QtDNo=O.DNo ");
            if (mIsRptProcurementUsePORevision)
            {
                SQL.AppendLine("Left Join (  ");
                SQL.AppendLine("    SELECT T3.DocNo, T3.DNo, T1.Qty PORevisionQty, T4.UPrice PORevisionPrice  ");
                SQL.AppendLine("    FROM TblPORevision T1   ");
                SQL.AppendLine("    INNER JOIN TblPODtl T2 ON T1.PODocNo = T2.DocNo AND T1.PODNo = T2.DNo AND T2.CancelInd = 'N'   ");
                SQL.AppendLine("    INNER JOIN TblPORequestDtl T3 ON T2.PORequestDocNo = T3.DocNo AND T2.PORequestDNo = T3.DNo   ");
                SQL.AppendLine("    INNER JOIN TblQtDtl T4 ON T1.QtDocNo = T4.DocNo AND T1.QtDNo = T4.DNo   ");
                SQL.AppendLine("    WHERE T1.CreateDt = (SELECT MAX(CreateDt) FROM TblPORevision WHERE PODocNo = T2.DocNo AND PODNo = T2.DNo)   ");
                SQL.AppendLine("    GROUP BY T3.DocNo, T3.DNo, T1.Qty, T4.UPrice   ");
                SQL.AppendLine(") P On A.DocNo = P.DocNo And B.DNo = P.DNo  ");
            }
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "PO Request#",
                        "Date",
                        "DNo",
                        "Local#",
                        "Department",
                        
                        //6-10
                        "Department",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "PO Request"+Environment.NewLine+"Quantity",
                        "UoM",

                        //11-15
                        "Status",
                        "",
                        "Level",
                        "By",
                        "PO"+Environment.NewLine+"Quantity",

                        //16-20
                        "Received"+Environment.NewLine+"Quantity",
                        "Currency",
                        "Price",
                        "Foreign Name",
                        "PO Quantity"+Environment.NewLine+"Revision",

                        //21
                        "Price"+Environment.NewLine+"Revision"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 0, 150, 0, 

                        //6-10
                        180, 100, 250, 100, 70,  

                        //11-15
                        80, 20, 70, 200, 100, 

                        //16-20
                        100, 60, 120, 150, 100,
                        
                        //21
                        120
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 15, 16, 18, 20, 21 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21 }, false);
            Grd1.Cols[19].Move(9);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 7, 10, 20, 21 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, false);

            if(mIsRptProcurementUsePORevision)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21 }, true);
                Grd1.Cols[20].Move(17);
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtPORequestDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "E.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "D.ItName", "D.ForeignName" });

                SetSQL();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt;",
                        new string[]
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DNo", "LocalDocNo", "DeptCode", "DeptName", 
                            
                            //6-10
                            "ItCode", "ItName", "PORequestQty", "PurchaseUomCode", "StatusDesc",  
 
                            //11-15
                            "level", "User", "POQty", "RecvVdQty", "CurCode",

                            //16-19
                            "UPrice", "ForeignName", "PORevisionQty", "PORevisionPrice"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            if (Row==0) Grd.Rows[Row].BackColor = Color.White;
                            if (Sm.DrStr(dr, 8) == "Cancel")
                            {
                                Grd.Rows[Row].BackColor = Color.Red;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            }
                            else
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            }

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                ShowPORequestApprovalInfo(e.RowIndex);
            }            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                ShowPORequestApprovalInfo(e.RowIndex);
            }
           
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion 

        #region Additional Method

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptProcurementUsePORevision = Sm.GetParameterBoo("IsRptProcurementUsePORevision");        }

        private void ShowPORequestApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='PORequest' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 3));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                         new string[] 
                        { 
                            "UserCode", 
                            "StatusDesc", "LastUpDt", "Remark" 
                        }
                    );
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkPORequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO request#");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtPORequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
        
    }
}
