﻿#region Update
/*
    26/03/2019 [DITA] Print Out PPH 23: ambil Remark dari Purchase invoice bagian Note (sebelah Footer tab tax, PPH23)  
    27/03/2019 [MEY] Tambah filter Vendor
    28/03/2019 [MEY] data yang ditampilkan hanya data yang belum disave
    02/04/2019 [WED] PI tidak perlu di voucher kan untuk bisa diproses di Purchase Invoice PPH
    05/04/2019 [WED] BUG PI tidak perlu melalui Outgoing Payment
    12/05/2019 [TKG] ubah proses validasi berdasarkan voucher date
    12/05/2019 [TKG] hanya menampilkan data PI yg menggunakan pajak PPH 23
    18/06/2019 [TKG] automatic dipilih apabila title diklik
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoicePPHDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseInvoicePPH mFrmParent;
        private string 
            mPPH23TaxCode = string.Empty, 
            mMainCurCode = string.Empty, 
            mPPH23TaxGrpCode = string.Empty;

        #endregion

        #region Constructor

        public FrmPurchaseInvoicePPHDlg(FrmPurchaseInvoicePPH FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;
                GetParameter();
                Sm.SetPeriod(ref DteVoucherDt1, ref DteVoucherDt2, ref ChkVoucherDt, -7);
                Sl.SetLueVdCode(ref LueVdCode);
                SetGrd();
                GetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mPPH23TaxGrpCode = Sm.GetParameter("PPH23TaxGrpCode");
            mPPH23TaxCode = Sm.GetParameter("PPH23TaxCode");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, DATE_FORMAT(A.DocDt, '%d/%m/%Y') As DocDt, A.VdCode, replace(B.TIN, ';', '') As TIN, replace(B.VdName, ';', '') As VdName, replace(B.Address, ';', '') As VdAddress, ");
            SQL.AppendLine("A.TaxCode1, A.TaxCode2, A.TaxCode3, C.TaxName As Tax1, D.TaxName As Tax2, E.TaxName As Tax3, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo, ';', '') As TaxInvoiceNo, replace(A.TaxInvoiceNo2, ';', '') As TaxInvoiceNo2, ");
            SQL.AppendLine("replace(A.TaxInvoiceNo3, ';', '') As TaxInvoiceNo3, IfNull(DATE_FORMAT(A.TaxInvoiceDt, '%d/%m/%Y'), '') As TaxInvoiceDt, ");
            SQL.AppendLine("IfNull(DATE_FORMAT(A.TaxInvoiceDt2, '%d/%m/%Y'), '') As TaxInvoiceDt2, IfNull(DATE_FORMAT(A.TaxInvoiceDt3, '%d/%m/%Y'), '') As TaxInvoiceDt3, ");
            SQL.AppendLine("A.CurCode, A.COATaxInd, Case When A.TaxRateAmt=0.00 Then 1.00 Else A.TaxRateAmt End As TaxRateAmt, F.Voucher, ");
            SQL.AppendLine("A.ServiceNote1, A.ServiceNote2, A.ServiceNote3 ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode = B.VdCode ");
            SQL.AppendLine("Left Join TblTax C On A.TaxCode1 = C.TaxCode ");
            SQL.AppendLine("Left Join TblTax D On A.TaxCode2 = D.TaxCode ");
            SQL.AppendLine("Left Join TblTax E On A.TaxCode3 = E.TaxCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T4.DocNo, ' (', Date_Format(T4.DocDt, '%d/%m/%Y'), ')') Order By T4.DocDt, T4.DocNo Separator ', ') As Voucher ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo = T2.InvoiceDocNo And T2.InvoiceType = '1' ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo = T3.DocNo And T3.Status = 'A' And T3.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo = T4.VoucherRequestDocNo And T4.CancelInd = 'N' ");
            if (ChkVoucherDt.Checked)
                SQL.AppendLine("        And T4.DocDt Between @VoucherDt1 And @VoucherDt2 ");
            SQL.AppendLine("    Inner Join TblParameter T5 On T5.ParCode='PPH23TaxCode' And T5.ParValue Is Not Null ");
            SQL.AppendLine("        And ((T1.TaxCode1 is Not Null And Find_In_Set(T1.TaxCode1,T5.ParValue)) Or ");
            SQL.AppendLine("        (T1.TaxCode2 is Not Null And Find_In_Set(T1.TaxCode2,T5.ParValue)) Or ");
            SQL.AppendLine("        (T1.TaxCode3 is Not Null And Find_In_Set(T1.TaxCode3,T5.ParValue))) ");
            if (ChkDocDt.Checked)
                SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And T1.ProcessInd = 'F' ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") F On A.DocNo = F.DocNo ");
            SQL.AppendLine("Where Exists (Select 1 From TblParameter Where ParCode = 'DocTitle' And ParValue = 'IOK') ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct A.PIDocNo ");
            SQL.AppendLine("    From TblPIPPHDtl A ");
            SQL.AppendLine("    Inner Join TblPIPPHHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine(") ");
            if (ChkDocDt.Checked) SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (ChkVoucherDt.Checked)
            {
                SQL.AppendLine("And A.DocNo In (");
                SQL.AppendLine("        Select T2.InvoiceDocNo ");
                SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
                SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo ");
                SQL.AppendLine("            And T3.DocDt Between @VoucherDt1 And @VoucherDt2 ");
                SQL.AppendLine("            And T3.CancelInd='N' ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("And (");
            SQL.AppendLine("    (A.TaxCode1 Is Not Null And A.TaxCode1 In (Select TaxCode From TblTax Where TaxGrpCode=@TaxGrpCode)) Or ");
            SQL.AppendLine("    (A.TaxCode2 Is Not Null And A.TaxCode2 In (Select TaxCode From TblTax Where TaxGrpCode=@TaxGrpCode)) Or ");
            SQL.AppendLine("    (A.TaxCode3 Is Not Null And A.TaxCode3 In (Select TaxCode From TblTax Where TaxGrpCode=@TaxGrpCode)) ");
            SQL.AppendLine("    )   ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                   //0
                    "No.",

                    //1-5
                    "",
                    "Document#",
                    "",
                    "Date",
                    "Vendor",
                    
                    //6-10
                    "Vendor Address",
                    "Taxpayer" +Environment.NewLine+"Identification#",
                    "Tax Invoice#",
                    "Tax Invoice"+Environment.NewLine+"Date",
                    "PPH Amount",

                    //11-13
                    "Total Without Tax",
                    "Voucher",
                    "Remark"
                },
                  new int[] 
                    {
                    //0
                    50,

                    //1-5
                    20, 120, 20, 80, 180, 
                    
                    //6-10
                    280, 120, 120, 120, 120,

                    //11-13
                    150, 180, 200
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<Hdr>();
                AddDataHdr(ref l);
                if (l.Count > 0)
                {
                    ShowDataHdr(ref l);
                    ComputeTotalWithoutTax(ref l);
                    ProcessPPHAmt(ref l);
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11 });
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsCodeAlreadyChosen(int Row)
        {
            string ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), ItCode))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmPurchaseInvoice");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoice(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmPurchaseInvoice");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }          
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
                    else
                        Grd1.Cells[r, 1].Value = false;
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional

        override protected void ChooseData()
        {
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                     if (Sm.GetGrdBool(Grd1, Row, 1) && !IsPurchaseInvoiceDocNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;
                            InsertData(Row);
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 purchase invoice.");
        }

        private void InsertData(int Row)
        {
            int Row1 = 0, Row2 = 0;
 
            Row1 = mFrmParent.Grd1.Rows.Count - 1;
            Row2 = Row;

            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 4);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 5);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6); 
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 7);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 8);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 9);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 10);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 11);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 12);
           Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 13);
            
            mFrmParent.Grd1.Rows.Add();

        }
        private bool IsPurchaseInvoiceDocNoAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        private void AddDataHdr(ref List<Hdr> l)
        {
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                string Filter = " ";
                
                cn.Open();
                cm.Connection = cn;
                Sm.CmParam<string>(ref cm, "@TaxGrpCode", mPPH23TaxGrpCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                cm.CommandText = GetSQL() + Filter + " Order By A.DocDt, A.DocNo; ";
               
                if (ChkDocDt.Checked)
                {
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                }

                if (ChkVoucherDt.Checked)
                {
                    Sm.CmParamDt(ref cm, "@VoucherDt1", Sm.GetDte(DteVoucherDt1));
                    Sm.CmParamDt(ref cm, "@VoucherDt2", Sm.GetDte(DteVoucherDt2));
                }
              
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocNo", 
                    
                    //1-5
                    "DocDt", "VdCode", "VdName", "VdAddress", "TaxCode1", 
                    
                    //6-10
                    "TaxCode2", "TaxCode3", "Tax1", "Tax2", "Tax3", 

                    //11-15
                    "TaxInvoiceNo", "TaxInvoiceNo2", "TaxInvoiceNo3", "TaxInvoiceDt", "TaxInvoiceDt2", 
                    
                    //16-20
                    "TaxInvoiceDt3", "COATaxInd", "TIN", "TaxRateAmt", "Voucher",

                    //21-23
                    "ServiceNote1", "ServiceNote2", "ServiceNote3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Hdr()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),

                            DocDt = Sm.DrStr(dr, c[1]),
                            VdCode = Sm.DrStr(dr, c[2]),
                            VdName = Sm.DrStr(dr, c[3]).Replace("\r\n", " ").Replace(",", " "),
                            VdAddress = Sm.DrStr(dr, c[4]).Replace("\r\n", " ").Replace(",", " "),
                            TaxCode1 = Sm.DrStr(dr, c[5]),

                            TaxCode2 = Sm.DrStr(dr, c[6]),
                            TaxCode3 = Sm.DrStr(dr, c[7]),
                            Tax1 = Sm.DrStr(dr, c[8]),
                            Tax2 = Sm.DrStr(dr, c[9]),
                            Tax3 = Sm.DrStr(dr, c[10]),

                            TaxInvoiceNo = Sm.DrStr(dr, c[11]),
                            TaxInvoiceNo2 = Sm.DrStr(dr, c[12]),
                            TaxInvoiceNo3 = Sm.DrStr(dr, c[13]),
                            TaxInvoiceDt = Sm.DrStr(dr, c[14]),
                            TaxInvoiceDt2 = Sm.DrStr(dr, c[15]),

                            TaxInvoiceDt3 = Sm.DrStr(dr, c[16]),
                            COATaxInd = Sm.DrStr(dr, c[17]),
                            TIN = Sm.DrStr(dr, c[18]).Replace("\r\n", " "),
                            TaxRateAmt = Sm.DrDec(dr, c[19]),
                            Voucher = Sm.DrStr(dr, c[20]),

                            ServiceNote1 = Sm.DrStr(dr,c[21]),
                            ServiceNote2 = Sm.DrStr(dr, c[22]),
                            ServiceNote3 = Sm.DrStr(dr, c[23])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowDataHdr(ref List<Hdr> l)
        {
            string mTaxInvoiceNo = string.Empty, mTaxInvoiceDt = string.Empty, mServiceNote = string.Empty;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;

            iGRow r;

            int nos = 1;
            for (int x = 0; x < l.Count; x++)
            {
                string[] taxcode = mPPH23TaxCode.Split(',');
                foreach (string TaxCodePPH in taxcode)
                    
                {
                    if (l[x].TaxCode1 == TaxCodePPH)
                    {
                        mTaxInvoiceNo = l[x].TaxInvoiceNo;
                        mTaxInvoiceDt = l[x].TaxInvoiceDt;
                        mServiceNote = l[x].ServiceNote1;
                    }

                    if (l[x].TaxCode2 == TaxCodePPH)
                    {
                        mTaxInvoiceNo = l[x].TaxInvoiceNo2;
                        mTaxInvoiceDt = l[x].TaxInvoiceDt2;
                        mServiceNote = l[x].ServiceNote2;
                    }

                    if (l[x].TaxCode3 == TaxCodePPH)
                    {
                        mTaxInvoiceNo = l[x].TaxInvoiceNo3;
                        mTaxInvoiceDt = l[x].TaxInvoiceDt3;
                        mServiceNote = l[x].ServiceNote3;
                    }
                }
                

                r = Grd1.Rows.Add();
                var checkauto = ChkAutoChoose.Checked == true ? true : false;
                r.Cells[0].Value = nos++;
                r.Cells[1].Value = checkauto;
                r.Cells[2].Value = l[x].DocNo;
                r.Cells[4].Value = l[x].DocDt;
                r.Cells[5].Value = l[x].VdName;
                r.Cells[6].Value = l[x].VdAddress;
                r.Cells[8].Value = mTaxInvoiceNo;
                r.Cells[9].Value = mTaxInvoiceDt;
                r.Cells[10].Value = 0m;
                r.Cells[11].Value = 0m;
                r.Cells[7].Value = l[x].TIN;
                r.Cells[12].Value = l[x].Voucher;
                r.Cells[13].Value = mServiceNote;
            }

            Grd1.EndUpdate();
        }

        private void ComputeTotalWithoutTax(ref List<Hdr> l)
        {
            var ld = new List<Dtl>();
            var ld2 = new List<Dtl2>();
            var lc = new List<AmtCOA>();

            ProcessDtl(ref l, ref ld);
            ProcessDtl2(ref l, ref ld2);
            ProcessAmtCOA(ref l, ref lc);

            for (int x = 0; x < l.Count; x++)
            {
                decimal mTotalWithoutTax = 0;

                if (ld.Count > 0)
                {
                    for (int y = 0; y < ld.Count; y++)
                    {
                        if (l[x].DocNo == ld[y].DocNo)
                        {
                            mTotalWithoutTax += ld[y].Total;
                            break;
                        }
                    }
                }

                if (ld2.Count > 0)
                {
                    for (int z = 0; z < ld2.Count; z++)
                    {
                        if (l[x].DocNo == ld2[z].DocNo)
                        {
                            mTotalWithoutTax += ld2[z].Amt;
                        }
                    }
                }

                if (lc.Count > 0)
                {
                    for (int a = 0; a < lc.Count; a++)
                    {
                        if (l[x].DocNo == lc[a].DocNo)
                        {
                            string mVendorAcNoDownpayment = Sm.GetValue("Select Concat(ParValue, '" + l[x].VdCode + "') As AcNo From TblParameter Where ParCode = 'VendorAcNoAP'; ");
                            if (lc[a].AcNo == mVendorAcNoDownpayment)
                            {
                                string mAcType = Sm.GetValue("Select AcType From TblCoa Where AcNo = '" + lc[a].AcNo + "'");
                                if (lc[a].DAmt != 0)
                                {
                                    string AcType = "D";
                                    if (AcType == mAcType) mTotalWithoutTax += lc[a].DAmt;
                                    else mTotalWithoutTax -= lc[a].DAmt;
                                }

                                if (lc[a].CAmt != 0)
                                {
                                    string AcType = "C";
                                    if (AcType == mAcType) mTotalWithoutTax += lc[a].CAmt;
                                    else mTotalWithoutTax -= lc[a].CAmt;
                                }
                            }
                        }
                    }
                }
                if (Sm.CompareStr(l[x].CurCode, mMainCurCode))
                    Grd1.Cells[x, 11].Value = Math.Truncate(mTotalWithoutTax);
                else
                    Grd1.Cells[x, 11].Value = Math.Truncate(mTotalWithoutTax * l[x].TaxRateAmt);
            }

            ld.Clear();
            ld2.Clear();
            lc.Clear();
        }

        private void ProcessDtl(ref List<Hdr> l, ref List<Dtl> ld)
        {
            string mDocNo = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mDocNo.Length > 0) mDocNo += ",";
                mDocNo += l[i].DocNo;
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, ");
            SQL.AppendLine("Sum( ");
            SQL.AppendLine("    ((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0.00)=0.00 Then 1.00 Else (100.00-D.Discount)/100.00 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+D.RoundingValue) ");
            SQL.AppendLine(") As Total ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.RecvVdDocNo=B.DocNo And A.RecvVdDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblRecvVdhdr B2 On A.RecvVdDocNo=B2.DocNo ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblItem I On B.ItCode=I.ItCode ");
            SQL.AppendLine("Where Find_In_Set(A.DocNo, '" + mDocNo + "') ");
            SQL.AppendLine("Group By A.DocNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Total" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld.Add(new Dtl()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Total = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessDtl2(ref List<Hdr> l, ref List<Dtl2> ld2)
        {
            string mDocNo = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mDocNo.Length > 0) mDocNo += ",";
                mDocNo += l[i].DocNo;
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.AmtType, Sum(A.Amt) As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceDtl2 A ");
            SQL.AppendLine("Inner Join TblOption B On A.AmtType=B.OptCode And B.OptCat='InvoiceAmtType' ");
            SQL.AppendLine("Where Find_In_Set(A.DocNo, '" + mDocNo + "') ");
            SQL.AppendLine("Group By A.DocNo, A.AmtType; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AmtType", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld2.Add(new Dtl2()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            AmtType = Sm.DrStr(dr, c[1]),
                            Amt = (Sm.DrStr(dr, c[1]) == "2") ? Sm.DrDec(dr, c[2]) : (Sm.DrDec(dr, c[2]) * -1)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAmtCOA(ref List<Hdr> l, ref List<AmtCOA> lc)
        {
            string mDocNo = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].COATaxInd == "N")
                {
                    if (mDocNo.Length > 0) mDocNo += ",";
                    mDocNo += l[i].DocNo;
                }
            }

            if (mDocNo.Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo, AcNo, DAMt, CAmt ");
                SQL.AppendLine("From TblPurchaseInvoiceDtl4 ");
                SQL.AppendLine("Where Find_In_Set(DocNo, '" + mDocNo + "'); ");

                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "AcNo", "DAmt", "CAMt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lc.Add(new AmtCOA()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                AcNo = Sm.DrStr(dr, c[1]),
                                DAmt = Sm.DrDec(dr, c[2]),
                                CAmt = Sm.DrDec(dr, c[3])
                            });
                        }
                    }
                    dr.Close();
                }
            }
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select TaxRate from TblTax Where TaxCode=@TaxCode"
            };
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);
            string TaxRate = Sm.GetValue(cm);

            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        private void ProcessPPHAmt(ref List<Hdr> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                string[] taxcode = mPPH23TaxCode.Split(',');
                decimal mTaxAmt = 0m, mTotalWithoutTax = 0m;
                if (Sm.GetGrdStr(Grd1, i, 11).Length != 0) mTotalWithoutTax = Sm.GetGrdDec(Grd1, i, 11);
                foreach (string TaxCodePPH in taxcode)
                {

                    if (l[i].TaxCode1 == TaxCodePPH) mTaxAmt = GetTaxRate(l[i].TaxCode1) / 100 * mTotalWithoutTax;
                    if (l[i].TaxCode2 == TaxCodePPH) mTaxAmt = GetTaxRate(l[i].TaxCode2) / 100 * mTotalWithoutTax;
                    if (l[i].TaxCode3 == TaxCodePPH) mTaxAmt = GetTaxRate(l[i].TaxCode3) / 100 * mTotalWithoutTax;
                }
                Grd1.Cells[i, 10].Value = Math.Truncate((mTaxAmt < 0) ? (mTaxAmt * -1) : mTaxAmt);
            }
        }

        private bool IsDataNotValid()
        {
            return
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsChosen = false;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (!IsChosen && Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length > 0) IsChosen = true;
            }
            if (!IsChosen)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Invoice date");
        }

        private void DteVoucherDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteVoucherDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkVoucherDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Voucher date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        #endregion

        #region Class

        private class Dtl
        {
            public string DocNo { get; set; }
            public decimal Total { get; set; }
        }

        private class Dtl2
        {
            public string DocNo { get; set; }
            public string AmtType { get; set; }
            public decimal Amt { get; set; }
        }

        private class AmtCOA
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class Hdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string VdAddress { get; set; }
            public string TaxCode1 { get; set; }
            public string Tax1 { get; set; }
            public string TaxInvoiceNo { get; set; }
            public string TaxInvoiceDt { get; set; }
            public string ServiceNote1 { get; set; }
            public string TaxCode2 { get; set; }
            public string Tax2 { get; set; }
            public string TaxInvoiceNo2 { get; set; }
            public string TaxInvoiceDt2 { get; set; }
            public string ServiceNote2 { get; set; }
            public string TaxCode3 { get; set; }
            public string Tax3 { get; set; }
            public string TaxInvoiceNo3 { get; set; }
            public string TaxInvoiceDt3 { get; set; }
            public string ServiceNote3 { get; set; }
            public string COATaxInd { get; set; }
            public string TIN { get; set; }
            public string CurCode { get; set; }
            public decimal TaxRateAmt { get; set; }
            public string Voucher { get; set; }
        }

        #endregion
    }
}
