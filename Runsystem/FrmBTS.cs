﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBTS : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmBTSFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmBTS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtBTSCode, TxtBTSName, TxtLocation, ChkActInd }, true);
                    TxtBTSCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtBTSCode, TxtBTSName, TxtLocation }, false);
                    TxtBTSCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtBTSName, TxtLocation, ChkActInd }, false);
                    TxtBTSName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtBTSCode, TxtBTSName });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBTSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBTSCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBTSCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblBTS Where BTSCode=@BTSCode" };
                Sm.CmParam<String>(ref cm, "@BTSCode", TxtBTSCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblBTS(BTSCode, BTSName, Location, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@BTSCode, @BTSName, @Location, @ActInd, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update BTSName=@BTSName, Location=@Location, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@BTSCode", TxtBTSCode.Text);
                Sm.CmParam<String>(ref cm, "@BTSName", TxtBTSName.Text);
                Sm.CmParam<String>(ref cm, "@Location", TxtLocation.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtBTSCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string BTSCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@BTSCode", BTSCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select BTSCode, BTSName, Location, ActInd From TblBTS Where BTSCode=@BTSCode;",
                        new string[]{ "BTSCode", "BTSName", "Location", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtBTSCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtBTSName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtLocation.EditValue = Sm.DrStr(dr, c[2]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtBTSCode, "BTS code", false) ||
                Sm.IsTxtEmpty(TxtBTSName, "BTS name", false) ||
                Sm.IsTxtEmpty(TxtLocation, "Location", false) ||
                IsCodeAlreadyExisted();
        }

        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtBTSCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select BTSCode From TblBTS Where BTSCode=@Param;",
                    TxtBTSCode.Text,
                    "BTS code ( " + TxtBTSCode.Text + " ) already existed."
                    );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBTSCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBTSCode);
        }

        private void TxtBTSName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBTSName);
        }

        private void TxtLocation_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocation);
        }

        #endregion

        #endregion
    }
}
