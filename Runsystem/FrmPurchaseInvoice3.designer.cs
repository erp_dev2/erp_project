﻿namespace RunSystem
{
    partial class FrmPurchaseInvoice3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPurchaseInvoice3));
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LblRemark = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtVdInvNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTotalWithoutTax = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgPurchaseInvoiceDtl5 = new System.Windows.Forms.TabPage();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgPurchaseInvoiceDtl1 = new System.Windows.Forms.TabPage();
            this.MeeServiceNote3 = new DevExpress.XtraEditors.MemoExEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.MeeServiceNote2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.MeeServiceNote1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.LueServiceCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.LueServiceCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.LueServiceCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtAlias3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAlias2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAlias1 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt3 = new DevExpress.XtraEditors.DateEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceNo3 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtTaxInvoiceNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.PnlQRCode = new System.Windows.Forms.Panel();
            this.DteQRCodeTaxInvoiceDt = new DevExpress.XtraEditors.DateEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.TxtQRCodeTaxInvoiceNo = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtQRCodeTaxAmt = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TxtTaxAmtDifference = new DevExpress.XtraEditors.TextEdit();
            this.LueTaxCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LblTaxRateAmt = new System.Windows.Forms.Label();
            this.TxtTotalTaxAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxRateAmt = new DevExpress.XtraEditors.TextEdit();
            this.LblTotalTaxAmt2 = new System.Windows.Forms.Label();
            this.TxtTotalTaxAmt1 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.DteTaxInvoiceDt = new DevExpress.XtraEditors.DateEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtTaxAmt3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtTaxAmt1 = new DevExpress.XtraEditors.TextEdit();
            this.LueTaxCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTaxCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueTaxCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxInvoiceNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TpgPurchaseInvoiceDtl2 = new System.Windows.Forms.TabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.TxtAmt3 = new DevExpress.XtraEditors.TextEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.ChkCOATaxInd = new DevExpress.XtraEditors.CheckEdit();
            this.TpgPurchaseInvoiceDtl4 = new System.Windows.Forms.TabPage();
            this.DteTaxInvDt = new DevExpress.XtraEditors.DateEdit();
            this.LueDocType = new DevExpress.XtraEditors.LookUpEdit();
            this.LueDocInd = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgPurchaseInvoiceDtl6 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtRateAmt = new DevExpress.XtraEditors.TextEdit();
            this.MeeDescription = new DevExpress.XtraEditors.MemoExEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.LuePIC = new DevExpress.XtraEditors.LookUpEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.DteDueDate2 = new DevExpress.XtraEditors.DateEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.LuePaymentType2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueAcType = new DevExpress.XtraEditors.LookUpEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtVCDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtVRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TpgPurchaseInvoiceDtl3 = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgPurchaseInvoiceDtl7 = new System.Windows.Forms.TabPage();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgPurchaseInvoiceDtl8 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteVdInvDt = new DevExpress.XtraEditors.DateEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtTotalWithTax = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtDownPayment = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.DtePaymentDt = new DevExpress.XtraEditors.DateEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.TxtSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LblDeptCode = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.BtnDueDt = new DevExpress.XtraEditors.SimpleButton();
            this.LblPaymentType = new System.Windows.Forms.Label();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdInvNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWithoutTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgPurchaseInvoiceDtl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpgPurchaseInvoiceDtl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeServiceNote3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeServiceNote2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeServiceNote1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            this.PnlQRCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteQRCodeTaxInvoiceDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQRCodeTaxInvoiceDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQRCodeTaxInvoiceNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQRCodeTaxAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmtDifference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalTaxAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxRateAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalTaxAmt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).BeginInit();
            this.TpgPurchaseInvoiceDtl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCOATaxInd.Properties)).BeginInit();
            this.TpgPurchaseInvoiceDtl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgPurchaseInvoiceDtl6.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRateAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDate2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDate2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOPDocNo.Properties)).BeginInit();
            this.TpgPurchaseInvoiceDtl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpgPurchaseInvoiceDtl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpgPurchaseInvoiceDtl8.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVdInvDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVdInvDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWithTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDownPayment.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(849, 0);
            this.panel1.Size = new System.Drawing.Size(70, 311);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnDueDt);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.TxtLocalDocNo);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.LblPaymentType);
            this.panel2.Controls.Add(this.LuePaymentType);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.DteVdInvDt);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.DteDueDt);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtVdInvNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueVdCode);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(849, 198);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 198);
            this.panel3.Size = new System.Drawing.Size(849, 113);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 289);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowModeHasCurCell = true;
            this.Grd1.SelectionMode = TenTec.Windows.iGridLib.iGSelectionMode.MultiExtended;
            this.Grd1.Size = new System.Drawing.Size(849, 113);
            this.Grd1.TabIndex = 47;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(121, 173);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(229, 20);
            this.MeeRemark.TabIndex = 46;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LblRemark
            // 
            this.LblRemark.AutoSize = true;
            this.LblRemark.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemark.Location = new System.Drawing.Point(69, 176);
            this.LblRemark.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemark.Name = "LblRemark";
            this.LblRemark.Size = new System.Drawing.Size(47, 14);
            this.LblRemark.TabIndex = 45;
            this.LblRemark.Text = "Remark";
            this.LblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(146, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(108, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(109, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(146, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(205, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(69, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdInvNo
            // 
            this.TxtVdInvNo.EnterMoveNextControl = true;
            this.TxtVdInvNo.Location = new System.Drawing.Point(146, 110);
            this.TxtVdInvNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdInvNo.Name = "TxtVdInvNo";
            this.TxtVdInvNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdInvNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdInvNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdInvNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVdInvNo.Properties.MaxLength = 30;
            this.TxtVdInvNo.Size = new System.Drawing.Size(273, 20);
            this.TxtVdInvNo.TabIndex = 22;
            this.TxtVdInvNo.Validated += new System.EventHandler(this.TxtVdInvNo_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(35, 113);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 14);
            this.label4.TabIndex = 21;
            this.label4.Text = "Vendor\'s Invoice#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(95, 92);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 19;
            this.label8.Text = "Vendor";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(146, 89);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 30;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 300;
            this.LueVdCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.LueVdCode.Size = new System.Drawing.Size(273, 20);
            this.LueVdCode.TabIndex = 20;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode.EditValueChanged += new System.EventHandler(this.LueVdCode_EditValueChanged);
            this.LueVdCode.Validated += new System.EventHandler(this.LueVdCode_Validated);
            // 
            // TxtTotalWithoutTax
            // 
            this.TxtTotalWithoutTax.EnterMoveNextControl = true;
            this.TxtTotalWithoutTax.Location = new System.Drawing.Point(121, 47);
            this.TxtTotalWithoutTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalWithoutTax.Name = "TxtTotalWithoutTax";
            this.TxtTotalWithoutTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalWithoutTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalWithoutTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalWithoutTax.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalWithoutTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalWithoutTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalWithoutTax.Properties.ReadOnly = true;
            this.TxtTotalWithoutTax.Size = new System.Drawing.Size(229, 20);
            this.TxtTotalWithoutTax.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(8, 50);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 14);
            this.label6.TabIndex = 33;
            this.label6.Text = "Total Without Tax";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(145, 173);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(116, 20);
            this.DteDueDt.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(83, 176);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 14);
            this.label9.TabIndex = 27;
            this.label9.Text = "Due Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgPurchaseInvoiceDtl5);
            this.tabControl1.Controls.Add(this.TpgPurchaseInvoiceDtl1);
            this.tabControl1.Controls.Add(this.TpgPurchaseInvoiceDtl2);
            this.tabControl1.Controls.Add(this.TpgPurchaseInvoiceDtl4);
            this.tabControl1.Controls.Add(this.TpgPurchaseInvoiceDtl6);
            this.tabControl1.Controls.Add(this.TpgPurchaseInvoiceDtl3);
            this.tabControl1.Controls.Add(this.TpgPurchaseInvoiceDtl7);
            this.tabControl1.Controls.Add(this.TpgPurchaseInvoiceDtl8);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 311);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(919, 288);
            this.tabControl1.TabIndex = 48;
            // 
            // TpgPurchaseInvoiceDtl5
            // 
            this.TpgPurchaseInvoiceDtl5.Controls.Add(this.Grd5);
            this.TpgPurchaseInvoiceDtl5.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchaseInvoiceDtl5.Name = "TpgPurchaseInvoiceDtl5";
            this.TpgPurchaseInvoiceDtl5.Size = new System.Drawing.Size(911, 258);
            this.TpgPurchaseInvoiceDtl5.TabIndex = 5;
            this.TpgPurchaseInvoiceDtl5.Text = "Vendor\'s Deposit";
            this.TpgPurchaseInvoiceDtl5.UseVisualStyleBackColor = true;
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(911, 258);
            this.Grd5.TabIndex = 49;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd5_EllipsisButtonClick);
            // 
            // TpgPurchaseInvoiceDtl1
            // 
            this.TpgPurchaseInvoiceDtl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.MeeServiceNote3);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label54);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.MeeServiceNote2);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label53);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.MeeServiceNote1);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label52);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label51);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.LueServiceCode3);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label50);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.LueServiceCode2);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label49);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.LueServiceCode1);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label48);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label47);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label17);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtAlias3);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtAlias2);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtAlias1);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label31);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.DteTaxInvoiceDt3);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label32);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label38);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtTaxInvoiceNo3);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label39);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label16);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.DteTaxInvoiceDt2);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label28);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label29);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtTaxInvoiceNo2);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label30);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label5);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.panel8);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.DteTaxInvoiceDt);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label27);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtTaxAmt3);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtTaxAmt2);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtTaxAmt1);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.LueTaxCode3);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.LueTaxCode2);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label10);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.LueTaxCode1);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.TxtTaxInvoiceNo);
            this.TpgPurchaseInvoiceDtl1.Controls.Add(this.label3);
            this.TpgPurchaseInvoiceDtl1.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchaseInvoiceDtl1.Name = "TpgPurchaseInvoiceDtl1";
            this.TpgPurchaseInvoiceDtl1.Size = new System.Drawing.Size(911, 258);
            this.TpgPurchaseInvoiceDtl1.TabIndex = 4;
            this.TpgPurchaseInvoiceDtl1.Text = "Tax";
            this.TpgPurchaseInvoiceDtl1.UseVisualStyleBackColor = true;
            // 
            // MeeServiceNote3
            // 
            this.MeeServiceNote3.EnterMoveNextControl = true;
            this.MeeServiceNote3.Location = new System.Drawing.Point(414, 233);
            this.MeeServiceNote3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeServiceNote3.Name = "MeeServiceNote3";
            this.MeeServiceNote3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote3.Properties.Appearance.Options.UseFont = true;
            this.MeeServiceNote3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeServiceNote3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeServiceNote3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeServiceNote3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeServiceNote3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeServiceNote3.Properties.MaxLength = 400;
            this.MeeServiceNote3.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.MeeServiceNote3.Properties.ShowIcon = false;
            this.MeeServiceNote3.Size = new System.Drawing.Size(207, 20);
            this.MeeServiceNote3.TabIndex = 90;
            this.MeeServiceNote3.ToolTip = "F4 : Show/hide text";
            this.MeeServiceNote3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeServiceNote3.ToolTipTitle = "Run System";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(374, 235);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 14);
            this.label54.TabIndex = 89;
            this.label54.Text = "Note";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeServiceNote2
            // 
            this.MeeServiceNote2.EnterMoveNextControl = true;
            this.MeeServiceNote2.Location = new System.Drawing.Point(414, 150);
            this.MeeServiceNote2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeServiceNote2.Name = "MeeServiceNote2";
            this.MeeServiceNote2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote2.Properties.Appearance.Options.UseFont = true;
            this.MeeServiceNote2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeServiceNote2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeServiceNote2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeServiceNote2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeServiceNote2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeServiceNote2.Properties.MaxLength = 400;
            this.MeeServiceNote2.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.MeeServiceNote2.Properties.ShowIcon = false;
            this.MeeServiceNote2.Size = new System.Drawing.Size(207, 20);
            this.MeeServiceNote2.TabIndex = 76;
            this.MeeServiceNote2.ToolTip = "F4 : Show/hide text";
            this.MeeServiceNote2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeServiceNote2.ToolTipTitle = "Run System";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(374, 152);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 14);
            this.label53.TabIndex = 75;
            this.label53.Text = "Note";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeServiceNote1
            // 
            this.MeeServiceNote1.EnterMoveNextControl = true;
            this.MeeServiceNote1.Location = new System.Drawing.Point(414, 66);
            this.MeeServiceNote1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeServiceNote1.Name = "MeeServiceNote1";
            this.MeeServiceNote1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote1.Properties.Appearance.Options.UseFont = true;
            this.MeeServiceNote1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeServiceNote1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeServiceNote1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeServiceNote1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeServiceNote1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeServiceNote1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeServiceNote1.Properties.MaxLength = 400;
            this.MeeServiceNote1.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.MeeServiceNote1.Properties.ShowIcon = false;
            this.MeeServiceNote1.Size = new System.Drawing.Size(207, 20);
            this.MeeServiceNote1.TabIndex = 62;
            this.MeeServiceNote1.ToolTip = "F4 : Show/hide text";
            this.MeeServiceNote1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeServiceNote1.ToolTipTitle = "Run System";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(374, 68);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 14);
            this.label52.TabIndex = 61;
            this.label52.Text = "Note";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(362, 215);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(46, 14);
            this.label51.TabIndex = 87;
            this.label51.Text = "Service";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueServiceCode3
            // 
            this.LueServiceCode3.EnterMoveNextControl = true;
            this.LueServiceCode3.Location = new System.Drawing.Point(414, 212);
            this.LueServiceCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueServiceCode3.Name = "LueServiceCode3";
            this.LueServiceCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.Appearance.Options.UseFont = true;
            this.LueServiceCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueServiceCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueServiceCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueServiceCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueServiceCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueServiceCode3.Properties.DropDownRows = 25;
            this.LueServiceCode3.Properties.NullText = "[Empty]";
            this.LueServiceCode3.Properties.PopupWidth = 200;
            this.LueServiceCode3.Size = new System.Drawing.Size(207, 20);
            this.LueServiceCode3.TabIndex = 88;
            this.LueServiceCode3.ToolTip = "F4 : Show/hide list";
            this.LueServiceCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueServiceCode3.EditValueChanged += new System.EventHandler(this.LueServiceCode3_EditValueChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(362, 132);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(46, 14);
            this.label50.TabIndex = 73;
            this.label50.Text = "Service";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueServiceCode2
            // 
            this.LueServiceCode2.EnterMoveNextControl = true;
            this.LueServiceCode2.Location = new System.Drawing.Point(414, 129);
            this.LueServiceCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueServiceCode2.Name = "LueServiceCode2";
            this.LueServiceCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.Appearance.Options.UseFont = true;
            this.LueServiceCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueServiceCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueServiceCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueServiceCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueServiceCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueServiceCode2.Properties.DropDownRows = 25;
            this.LueServiceCode2.Properties.NullText = "[Empty]";
            this.LueServiceCode2.Properties.PopupWidth = 200;
            this.LueServiceCode2.Size = new System.Drawing.Size(207, 20);
            this.LueServiceCode2.TabIndex = 74;
            this.LueServiceCode2.ToolTip = "F4 : Show/hide list";
            this.LueServiceCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueServiceCode2.EditValueChanged += new System.EventHandler(this.LueServiceCode2_EditValueChanged);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(362, 48);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(46, 14);
            this.label49.TabIndex = 59;
            this.label49.Text = "Service";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueServiceCode1
            // 
            this.LueServiceCode1.EnterMoveNextControl = true;
            this.LueServiceCode1.Location = new System.Drawing.Point(414, 45);
            this.LueServiceCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueServiceCode1.Name = "LueServiceCode1";
            this.LueServiceCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.Appearance.Options.UseFont = true;
            this.LueServiceCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueServiceCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueServiceCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueServiceCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueServiceCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueServiceCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueServiceCode1.Properties.DropDownRows = 25;
            this.LueServiceCode1.Properties.NullText = "[Empty]";
            this.LueServiceCode1.Properties.PopupWidth = 200;
            this.LueServiceCode1.Size = new System.Drawing.Size(207, 20);
            this.LueServiceCode1.TabIndex = 60;
            this.LueServiceCode1.ToolTip = "F4 : Show/hide list";
            this.LueServiceCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueServiceCode1.EditValueChanged += new System.EventHandler(this.LueServiceCode1_EditValueChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(59, 216);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(30, 14);
            this.label48.TabIndex = 85;
            this.label48.Text = "Alias";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(59, 133);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(30, 14);
            this.label47.TabIndex = 71;
            this.label47.Text = "Alias";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(59, 48);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 14);
            this.label17.TabIndex = 57;
            this.label17.Text = "Alias";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAlias3
            // 
            this.TxtAlias3.EnterMoveNextControl = true;
            this.TxtAlias3.Location = new System.Drawing.Point(92, 213);
            this.TxtAlias3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAlias3.Name = "TxtAlias3";
            this.TxtAlias3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAlias3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAlias3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAlias3.Properties.Appearance.Options.UseFont = true;
            this.TxtAlias3.Properties.MaxLength = 30;
            this.TxtAlias3.Size = new System.Drawing.Size(250, 20);
            this.TxtAlias3.TabIndex = 86;
            // 
            // TxtAlias2
            // 
            this.TxtAlias2.EnterMoveNextControl = true;
            this.TxtAlias2.Location = new System.Drawing.Point(92, 129);
            this.TxtAlias2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAlias2.Name = "TxtAlias2";
            this.TxtAlias2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAlias2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAlias2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAlias2.Properties.Appearance.Options.UseFont = true;
            this.TxtAlias2.Properties.MaxLength = 30;
            this.TxtAlias2.Size = new System.Drawing.Size(250, 20);
            this.TxtAlias2.TabIndex = 72;
            // 
            // TxtAlias1
            // 
            this.TxtAlias1.EnterMoveNextControl = true;
            this.TxtAlias1.Location = new System.Drawing.Point(92, 45);
            this.TxtAlias1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAlias1.Name = "TxtAlias1";
            this.TxtAlias1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAlias1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAlias1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAlias1.Properties.Appearance.Options.UseFont = true;
            this.TxtAlias1.Properties.MaxLength = 30;
            this.TxtAlias1.Size = new System.Drawing.Size(250, 20);
            this.TxtAlias1.TabIndex = 58;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(357, 195);
            this.label31.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 14);
            this.label31.TabIndex = 83;
            this.label31.Text = "Amount";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt3
            // 
            this.DteTaxInvoiceDt3.EditValue = null;
            this.DteTaxInvoiceDt3.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt3.Location = new System.Drawing.Point(414, 171);
            this.DteTaxInvoiceDt3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt3.Name = "DteTaxInvoiceDt3";
            this.DteTaxInvoiceDt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt3.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt3.Size = new System.Drawing.Size(107, 20);
            this.DteTaxInvoiceDt3.TabIndex = 80;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(375, 175);
            this.label32.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(33, 14);
            this.label32.TabIndex = 79;
            this.label32.Text = "Date";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(62, 195);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 14);
            this.label38.TabIndex = 81;
            this.label38.Text = "Tax";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceNo3
            // 
            this.TxtTaxInvoiceNo3.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo3.Location = new System.Drawing.Point(92, 171);
            this.TxtTaxInvoiceNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo3.Name = "TxtTaxInvoiceNo3";
            this.TxtTaxInvoiceNo3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo3.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo3.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo3.TabIndex = 78;
            this.TxtTaxInvoiceNo3.Validated += new System.EventHandler(this.TxtTaxInvoiceNo3_Validated);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(10, 174);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(79, 14);
            this.label39.TabIndex = 77;
            this.label39.Text = "Tax Invoice#";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(357, 111);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 14);
            this.label16.TabIndex = 69;
            this.label16.Text = "Amount";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt2
            // 
            this.DteTaxInvoiceDt2.EditValue = null;
            this.DteTaxInvoiceDt2.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt2.Location = new System.Drawing.Point(414, 87);
            this.DteTaxInvoiceDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt2.Name = "DteTaxInvoiceDt2";
            this.DteTaxInvoiceDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt2.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt2.Size = new System.Drawing.Size(107, 20);
            this.DteTaxInvoiceDt2.TabIndex = 66;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(375, 90);
            this.label28.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(33, 14);
            this.label28.TabIndex = 65;
            this.label28.Text = "Date";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(62, 110);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(27, 14);
            this.label29.TabIndex = 67;
            this.label29.Text = "Tax";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxInvoiceNo2
            // 
            this.TxtTaxInvoiceNo2.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo2.Location = new System.Drawing.Point(92, 87);
            this.TxtTaxInvoiceNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo2.Name = "TxtTaxInvoiceNo2";
            this.TxtTaxInvoiceNo2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo2.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo2.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo2.TabIndex = 64;
            this.TxtTaxInvoiceNo2.Validated += new System.EventHandler(this.TxtTaxInvoiceNo2_Validated);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(10, 90);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(79, 14);
            this.label30.TabIndex = 63;
            this.label30.Text = "Tax Invoice#";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(357, 27);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 14);
            this.label5.TabIndex = 55;
            this.label5.Text = "Amount";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.PnlQRCode);
            this.panel8.Controls.Add(this.LueTaxCurCode);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.LblTaxRateAmt);
            this.panel8.Controls.Add(this.TxtTotalTaxAmt2);
            this.panel8.Controls.Add(this.TxtTaxRateAmt);
            this.panel8.Controls.Add(this.LblTotalTaxAmt2);
            this.panel8.Controls.Add(this.TxtTotalTaxAmt1);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(639, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(272, 258);
            this.panel8.TabIndex = 91;
            // 
            // PnlQRCode
            // 
            this.PnlQRCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.PnlQRCode.Controls.Add(this.DteQRCodeTaxInvoiceDt);
            this.PnlQRCode.Controls.Add(this.label46);
            this.PnlQRCode.Controls.Add(this.TxtQRCodeTaxInvoiceNo);
            this.PnlQRCode.Controls.Add(this.label45);
            this.PnlQRCode.Controls.Add(this.label41);
            this.PnlQRCode.Controls.Add(this.TxtQRCodeTaxAmt);
            this.PnlQRCode.Controls.Add(this.label40);
            this.PnlQRCode.Controls.Add(this.TxtTaxAmtDifference);
            this.PnlQRCode.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PnlQRCode.Location = new System.Drawing.Point(0, 169);
            this.PnlQRCode.Name = "PnlQRCode";
            this.PnlQRCode.Size = new System.Drawing.Size(272, 89);
            this.PnlQRCode.TabIndex = 100;
            // 
            // DteQRCodeTaxInvoiceDt
            // 
            this.DteQRCodeTaxInvoiceDt.EditValue = null;
            this.DteQRCodeTaxInvoiceDt.EnterMoveNextControl = true;
            this.DteQRCodeTaxInvoiceDt.Location = new System.Drawing.Point(98, 65);
            this.DteQRCodeTaxInvoiceDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQRCodeTaxInvoiceDt.Name = "DteQRCodeTaxInvoiceDt";
            this.DteQRCodeTaxInvoiceDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteQRCodeTaxInvoiceDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQRCodeTaxInvoiceDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteQRCodeTaxInvoiceDt.Properties.Appearance.Options.UseFont = true;
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQRCodeTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteQRCodeTaxInvoiceDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQRCodeTaxInvoiceDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQRCodeTaxInvoiceDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQRCodeTaxInvoiceDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQRCodeTaxInvoiceDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQRCodeTaxInvoiceDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQRCodeTaxInvoiceDt.Properties.ReadOnly = true;
            this.DteQRCodeTaxInvoiceDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQRCodeTaxInvoiceDt.Size = new System.Drawing.Size(107, 20);
            this.DteQRCodeTaxInvoiceDt.TabIndex = 108;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(60, 68);
            this.label46.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(33, 14);
            this.label46.TabIndex = 107;
            this.label46.Text = "Date";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQRCodeTaxInvoiceNo
            // 
            this.TxtQRCodeTaxInvoiceNo.EnterMoveNextControl = true;
            this.TxtQRCodeTaxInvoiceNo.Location = new System.Drawing.Point(98, 44);
            this.TxtQRCodeTaxInvoiceNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQRCodeTaxInvoiceNo.Name = "TxtQRCodeTaxInvoiceNo";
            this.TxtQRCodeTaxInvoiceNo.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtQRCodeTaxInvoiceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQRCodeTaxInvoiceNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQRCodeTaxInvoiceNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQRCodeTaxInvoiceNo.Properties.MaxLength = 30;
            this.TxtQRCodeTaxInvoiceNo.Properties.ReadOnly = true;
            this.TxtQRCodeTaxInvoiceNo.Size = new System.Drawing.Size(164, 20);
            this.TxtQRCodeTaxInvoiceNo.TabIndex = 106;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(14, 47);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(79, 14);
            this.label45.TabIndex = 105;
            this.label45.Text = "Tax Invoice#";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(18, 26);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(75, 14);
            this.label41.TabIndex = 103;
            this.label41.Text = "Tax Amount";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQRCodeTaxAmt
            // 
            this.TxtQRCodeTaxAmt.EnterMoveNextControl = true;
            this.TxtQRCodeTaxAmt.Location = new System.Drawing.Point(98, 23);
            this.TxtQRCodeTaxAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQRCodeTaxAmt.Name = "TxtQRCodeTaxAmt";
            this.TxtQRCodeTaxAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQRCodeTaxAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQRCodeTaxAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQRCodeTaxAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtQRCodeTaxAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQRCodeTaxAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQRCodeTaxAmt.Properties.ReadOnly = true;
            this.TxtQRCodeTaxAmt.Size = new System.Drawing.Size(164, 20);
            this.TxtQRCodeTaxAmt.TabIndex = 104;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(45, 5);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(48, 14);
            this.label40.TabIndex = 101;
            this.label40.Text = "Balance";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmtDifference
            // 
            this.TxtTaxAmtDifference.EnterMoveNextControl = true;
            this.TxtTaxAmtDifference.Location = new System.Drawing.Point(98, 2);
            this.TxtTaxAmtDifference.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmtDifference.Name = "TxtTaxAmtDifference";
            this.TxtTaxAmtDifference.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmtDifference.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmtDifference.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmtDifference.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmtDifference.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmtDifference.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmtDifference.Properties.ReadOnly = true;
            this.TxtTaxAmtDifference.Size = new System.Drawing.Size(164, 20);
            this.TxtTaxAmtDifference.TabIndex = 102;
            // 
            // LueTaxCurCode
            // 
            this.LueTaxCurCode.EnterMoveNextControl = true;
            this.LueTaxCurCode.Location = new System.Drawing.Point(98, 25);
            this.LueTaxCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCurCode.Name = "LueTaxCurCode";
            this.LueTaxCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueTaxCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueTaxCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCurCode.Properties.DropDownRows = 25;
            this.LueTaxCurCode.Properties.NullText = "[Empty]";
            this.LueTaxCurCode.Properties.PopupWidth = 280;
            this.LueTaxCurCode.Properties.ReadOnly = true;
            this.LueTaxCurCode.Size = new System.Drawing.Size(164, 20);
            this.LueTaxCurCode.TabIndex = 95;
            this.LueTaxCurCode.ToolTip = "F4 : Show/hide list";
            this.LueTaxCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCurCode.EditValueChanged += new System.EventHandler(this.LueTaxCurCode_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(38, 29);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 14);
            this.label12.TabIndex = 94;
            this.label12.Text = "Currency";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblTaxRateAmt
            // 
            this.LblTaxRateAmt.AutoSize = true;
            this.LblTaxRateAmt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTaxRateAmt.ForeColor = System.Drawing.Color.Black;
            this.LblTaxRateAmt.Location = new System.Drawing.Point(61, 50);
            this.LblTaxRateAmt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTaxRateAmt.Name = "LblTaxRateAmt";
            this.LblTaxRateAmt.Size = new System.Drawing.Size(32, 14);
            this.LblTaxRateAmt.TabIndex = 96;
            this.LblTaxRateAmt.Text = "Rate";
            this.LblTaxRateAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalTaxAmt2
            // 
            this.TxtTotalTaxAmt2.EnterMoveNextControl = true;
            this.TxtTotalTaxAmt2.Location = new System.Drawing.Point(98, 68);
            this.TxtTotalTaxAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalTaxAmt2.Name = "TxtTotalTaxAmt2";
            this.TxtTotalTaxAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalTaxAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalTaxAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalTaxAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalTaxAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalTaxAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalTaxAmt2.Properties.ReadOnly = true;
            this.TxtTotalTaxAmt2.Size = new System.Drawing.Size(164, 20);
            this.TxtTotalTaxAmt2.TabIndex = 99;
            // 
            // TxtTaxRateAmt
            // 
            this.TxtTaxRateAmt.EnterMoveNextControl = true;
            this.TxtTaxRateAmt.Location = new System.Drawing.Point(98, 46);
            this.TxtTaxRateAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxRateAmt.Name = "TxtTaxRateAmt";
            this.TxtTaxRateAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxRateAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxRateAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxRateAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxRateAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxRateAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxRateAmt.Size = new System.Drawing.Size(164, 20);
            this.TxtTaxRateAmt.TabIndex = 97;
            this.TxtTaxRateAmt.Validated += new System.EventHandler(this.TxtTaxRateAmt_Validated);
            // 
            // LblTotalTaxAmt2
            // 
            this.LblTotalTaxAmt2.AutoSize = true;
            this.LblTotalTaxAmt2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalTaxAmt2.ForeColor = System.Drawing.Color.Black;
            this.LblTotalTaxAmt2.Location = new System.Drawing.Point(34, 71);
            this.LblTotalTaxAmt2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTotalTaxAmt2.Name = "LblTotalTaxAmt2";
            this.LblTotalTaxAmt2.Size = new System.Drawing.Size(59, 14);
            this.LblTotalTaxAmt2.TabIndex = 98;
            this.LblTotalTaxAmt2.Text = "Total Tax";
            this.LblTotalTaxAmt2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalTaxAmt1
            // 
            this.TxtTotalTaxAmt1.EnterMoveNextControl = true;
            this.TxtTotalTaxAmt1.Location = new System.Drawing.Point(98, 4);
            this.TxtTotalTaxAmt1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalTaxAmt1.Name = "TxtTotalTaxAmt1";
            this.TxtTotalTaxAmt1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalTaxAmt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalTaxAmt1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalTaxAmt1.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalTaxAmt1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalTaxAmt1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalTaxAmt1.Properties.ReadOnly = true;
            this.TxtTotalTaxAmt1.Size = new System.Drawing.Size(164, 20);
            this.TxtTotalTaxAmt1.TabIndex = 93;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(34, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 14);
            this.label11.TabIndex = 92;
            this.label11.Text = "Total Tax";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTaxInvoiceDt
            // 
            this.DteTaxInvoiceDt.EditValue = null;
            this.DteTaxInvoiceDt.EnterMoveNextControl = true;
            this.DteTaxInvoiceDt.Location = new System.Drawing.Point(414, 4);
            this.DteTaxInvoiceDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvoiceDt.Name = "DteTaxInvoiceDt";
            this.DteTaxInvoiceDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvoiceDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteTaxInvoiceDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvoiceDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvoiceDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvoiceDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvoiceDt.Size = new System.Drawing.Size(107, 20);
            this.DteTaxInvoiceDt.TabIndex = 52;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(375, 7);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(33, 14);
            this.label27.TabIndex = 51;
            this.label27.Text = "Date";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxAmt3
            // 
            this.TxtTaxAmt3.EnterMoveNextControl = true;
            this.TxtTaxAmt3.Location = new System.Drawing.Point(414, 192);
            this.TxtTaxAmt3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt3.Name = "TxtTaxAmt3";
            this.TxtTaxAmt3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt3.Properties.ReadOnly = true;
            this.TxtTaxAmt3.Size = new System.Drawing.Size(207, 20);
            this.TxtTaxAmt3.TabIndex = 84;
            // 
            // TxtTaxAmt2
            // 
            this.TxtTaxAmt2.EnterMoveNextControl = true;
            this.TxtTaxAmt2.Location = new System.Drawing.Point(414, 108);
            this.TxtTaxAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt2.Name = "TxtTaxAmt2";
            this.TxtTaxAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt2.Properties.ReadOnly = true;
            this.TxtTaxAmt2.Size = new System.Drawing.Size(207, 20);
            this.TxtTaxAmt2.TabIndex = 70;
            // 
            // TxtTaxAmt1
            // 
            this.TxtTaxAmt1.EnterMoveNextControl = true;
            this.TxtTaxAmt1.Location = new System.Drawing.Point(414, 25);
            this.TxtTaxAmt1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt1.Name = "TxtTaxAmt1";
            this.TxtTaxAmt1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt1.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt1.Properties.ReadOnly = true;
            this.TxtTaxAmt1.Size = new System.Drawing.Size(207, 20);
            this.TxtTaxAmt1.TabIndex = 56;
            // 
            // LueTaxCode3
            // 
            this.LueTaxCode3.EnterMoveNextControl = true;
            this.LueTaxCode3.Location = new System.Drawing.Point(92, 192);
            this.LueTaxCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode3.Name = "LueTaxCode3";
            this.LueTaxCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode3.Properties.DropDownRows = 25;
            this.LueTaxCode3.Properties.NullText = "[Empty]";
            this.LueTaxCode3.Properties.PopupWidth = 200;
            this.LueTaxCode3.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode3.TabIndex = 82;
            this.LueTaxCode3.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode3.EditValueChanged += new System.EventHandler(this.LueTaxCode3_EditValueChanged);
            // 
            // LueTaxCode2
            // 
            this.LueTaxCode2.EnterMoveNextControl = true;
            this.LueTaxCode2.Location = new System.Drawing.Point(92, 108);
            this.LueTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode2.Name = "LueTaxCode2";
            this.LueTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode2.Properties.DropDownRows = 25;
            this.LueTaxCode2.Properties.NullText = "[Empty]";
            this.LueTaxCode2.Properties.PopupWidth = 200;
            this.LueTaxCode2.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode2.TabIndex = 68;
            this.LueTaxCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode2.EditValueChanged += new System.EventHandler(this.LueTaxCode2_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(62, 27);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 14);
            this.label10.TabIndex = 53;
            this.label10.Text = "Tax";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode1
            // 
            this.LueTaxCode1.EnterMoveNextControl = true;
            this.LueTaxCode1.Location = new System.Drawing.Point(92, 24);
            this.LueTaxCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode1.Name = "LueTaxCode1";
            this.LueTaxCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode1.Properties.DropDownRows = 25;
            this.LueTaxCode1.Properties.NullText = "[Empty]";
            this.LueTaxCode1.Properties.PopupWidth = 200;
            this.LueTaxCode1.Size = new System.Drawing.Size(250, 20);
            this.LueTaxCode1.TabIndex = 54;
            this.LueTaxCode1.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode1.EditValueChanged += new System.EventHandler(this.LueTaxCode1_EditValueChanged);
            // 
            // TxtTaxInvoiceNo
            // 
            this.TxtTaxInvoiceNo.EnterMoveNextControl = true;
            this.TxtTaxInvoiceNo.Location = new System.Drawing.Point(92, 3);
            this.TxtTaxInvoiceNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxInvoiceNo.Name = "TxtTaxInvoiceNo";
            this.TxtTaxInvoiceNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxInvoiceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxInvoiceNo.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxInvoiceNo.Properties.MaxLength = 30;
            this.TxtTaxInvoiceNo.Size = new System.Drawing.Size(250, 20);
            this.TxtTaxInvoiceNo.TabIndex = 50;
            this.TxtTaxInvoiceNo.Validated += new System.EventHandler(this.TxtTaxInvoiceNo_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(10, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 14);
            this.label3.TabIndex = 49;
            this.label3.Text = "Tax Invoice#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgPurchaseInvoiceDtl2
            // 
            this.TpgPurchaseInvoiceDtl2.Controls.Add(this.Grd4);
            this.TpgPurchaseInvoiceDtl2.Controls.Add(this.panel9);
            this.TpgPurchaseInvoiceDtl2.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchaseInvoiceDtl2.Name = "TpgPurchaseInvoiceDtl2";
            this.TpgPurchaseInvoiceDtl2.Size = new System.Drawing.Size(911, 258);
            this.TpgPurchaseInvoiceDtl2.TabIndex = 3;
            this.TpgPurchaseInvoiceDtl2.Text = "Discount, Cost, Etc";
            this.TpgPurchaseInvoiceDtl2.UseVisualStyleBackColor = true;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 34);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(911, 224);
            this.Grd4.TabIndex = 21;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            this.Grd4.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd4_AfterCommitEdit);
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.panel11);
            this.panel9.Controls.Add(this.ChkCOATaxInd);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(911, 34);
            this.panel9.TabIndex = 49;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.TxtAmt3);
            this.panel11.Controls.Add(this.label57);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(541, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(370, 34);
            this.panel11.TabIndex = 18;
            // 
            // TxtAmt3
            // 
            this.TxtAmt3.EnterMoveNextControl = true;
            this.TxtAmt3.Location = new System.Drawing.Point(184, 6);
            this.TxtAmt3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt3.Name = "TxtAmt3";
            this.TxtAmt3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt3.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt3.Properties.ReadOnly = true;
            this.TxtAmt3.Size = new System.Drawing.Size(178, 20);
            this.TxtAmt3.TabIndex = 20;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(7, 9);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(171, 14);
            this.label57.TabIndex = 19;
            this.label57.Text = "Invoice Amount Without COA";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCOATaxInd
            // 
            this.ChkCOATaxInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCOATaxInd.Location = new System.Drawing.Point(6, 6);
            this.ChkCOATaxInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCOATaxInd.Name = "ChkCOATaxInd";
            this.ChkCOATaxInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCOATaxInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCOATaxInd.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ChkCOATaxInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCOATaxInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCOATaxInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCOATaxInd.Properties.Caption = "For Tax Purpose";
            this.ChkCOATaxInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCOATaxInd.Size = new System.Drawing.Size(130, 22);
            this.ChkCOATaxInd.TabIndex = 17;
            this.ChkCOATaxInd.CheckedChanged += new System.EventHandler(this.ChkCOATaxInd_CheckedChanged);
            // 
            // TpgPurchaseInvoiceDtl4
            // 
            this.TpgPurchaseInvoiceDtl4.BackColor = System.Drawing.Color.SkyBlue;
            this.TpgPurchaseInvoiceDtl4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgPurchaseInvoiceDtl4.Controls.Add(this.DteTaxInvDt);
            this.TpgPurchaseInvoiceDtl4.Controls.Add(this.LueDocType);
            this.TpgPurchaseInvoiceDtl4.Controls.Add(this.LueDocInd);
            this.TpgPurchaseInvoiceDtl4.Controls.Add(this.Grd3);
            this.TpgPurchaseInvoiceDtl4.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchaseInvoiceDtl4.Name = "TpgPurchaseInvoiceDtl4";
            this.TpgPurchaseInvoiceDtl4.Size = new System.Drawing.Size(911, 258);
            this.TpgPurchaseInvoiceDtl4.TabIndex = 2;
            this.TpgPurchaseInvoiceDtl4.Text = "Document Info";
            this.TpgPurchaseInvoiceDtl4.UseVisualStyleBackColor = true;
            // 
            // DteTaxInvDt
            // 
            this.DteTaxInvDt.EditValue = null;
            this.DteTaxInvDt.EnterMoveNextControl = true;
            this.DteTaxInvDt.Location = new System.Drawing.Point(510, 20);
            this.DteTaxInvDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTaxInvDt.Name = "DteTaxInvDt";
            this.DteTaxInvDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt.Properties.Appearance.Options.UseFont = true;
            this.DteTaxInvDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTaxInvDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTaxInvDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTaxInvDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTaxInvDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTaxInvDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTaxInvDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTaxInvDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTaxInvDt.Size = new System.Drawing.Size(125, 20);
            this.DteTaxInvDt.TabIndex = 52;
            this.DteTaxInvDt.Visible = false;
            this.DteTaxInvDt.Leave += new System.EventHandler(this.DteTaxInvDt_Leave);
            this.DteTaxInvDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteTaxInvDt_KeyDown);
            // 
            // LueDocType
            // 
            this.LueDocType.EnterMoveNextControl = true;
            this.LueDocType.Location = new System.Drawing.Point(130, 20);
            this.LueDocType.Margin = new System.Windows.Forms.Padding(5);
            this.LueDocType.Name = "LueDocType";
            this.LueDocType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.Appearance.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDocType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDocType.Properties.DropDownRows = 12;
            this.LueDocType.Properties.NullText = "[Empty]";
            this.LueDocType.Properties.PopupWidth = 300;
            this.LueDocType.Size = new System.Drawing.Size(210, 20);
            this.LueDocType.TabIndex = 50;
            this.LueDocType.ToolTip = "F4 : Show/hide list";
            this.LueDocType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDocType.EditValueChanged += new System.EventHandler(this.LueDocType_EditValueChanged);
            this.LueDocType.Validated += new System.EventHandler(this.LueDocType_Validated);
            this.LueDocType.Leave += new System.EventHandler(this.LueDocType_Leave);
            this.LueDocType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueDocType_KeyDown);
            // 
            // LueDocInd
            // 
            this.LueDocInd.EnterMoveNextControl = true;
            this.LueDocInd.Location = new System.Drawing.Point(350, 20);
            this.LueDocInd.Margin = new System.Windows.Forms.Padding(5);
            this.LueDocInd.Name = "LueDocInd";
            this.LueDocInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocInd.Properties.Appearance.Options.UseFont = true;
            this.LueDocInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDocInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDocInd.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocInd.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDocInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDocInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDocInd.Properties.DropDownRows = 5;
            this.LueDocInd.Properties.NullText = "[Empty]";
            this.LueDocInd.Properties.PopupWidth = 200;
            this.LueDocInd.Size = new System.Drawing.Size(149, 20);
            this.LueDocInd.TabIndex = 51;
            this.LueDocInd.ToolTip = "F4 : Show/hide list";
            this.LueDocInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDocInd.EditValueChanged += new System.EventHandler(this.LueDocInd_EditValueChanged);
            this.LueDocInd.Validated += new System.EventHandler(this.LueDocInd_Validated);
            this.LueDocInd.Leave += new System.EventHandler(this.LueDocInd_Leave);
            this.LueDocInd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueDocInd_KeyDown);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(907, 254);
            this.Grd3.TabIndex = 49;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            // 
            // TpgPurchaseInvoiceDtl6
            // 
            this.TpgPurchaseInvoiceDtl6.Controls.Add(this.panel4);
            this.TpgPurchaseInvoiceDtl6.Controls.Add(this.panel7);
            this.TpgPurchaseInvoiceDtl6.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchaseInvoiceDtl6.Name = "TpgPurchaseInvoiceDtl6";
            this.TpgPurchaseInvoiceDtl6.Padding = new System.Windows.Forms.Padding(3);
            this.TpgPurchaseInvoiceDtl6.Size = new System.Drawing.Size(911, 258);
            this.TpgPurchaseInvoiceDtl6.TabIndex = 6;
            this.TpgPurchaseInvoiceDtl6.Text = "Voucher";
            this.TpgPurchaseInvoiceDtl6.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.TxtGiroNo);
            this.panel4.Controls.Add(this.LueBankCode);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.DteDueDate2);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.LuePaymentType2);
            this.panel4.Controls.Add(this.LueAcType);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.LueBankAcCode);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(905, 218);
            this.panel4.TabIndex = 29;
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(126, 90);
            this.TxtGiroNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 80;
            this.TxtGiroNo.Size = new System.Drawing.Size(283, 20);
            this.TxtGiroNo.TabIndex = 58;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(126, 69);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 20;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(283, 20);
            this.LueBankCode.TabIndex = 56;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(53, 72);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(68, 14);
            this.label43.TabIndex = 55;
            this.label43.Text = "Bank Name";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(5, 93);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(116, 14);
            this.label44.TabIndex = 57;
            this.label44.Text = "Giro Bilyet / Cheque";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.TxtAmt2);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.LueCurCode);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.TxtRateAmt);
            this.panel6.Controls.Add(this.MeeDescription);
            this.panel6.Controls.Add(this.label33);
            this.panel6.Controls.Add(this.label42);
            this.panel6.Controls.Add(this.LuePIC);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(481, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(424, 218);
            this.panel6.TabIndex = 52;
            // 
            // TxtAmt2
            // 
            this.TxtAmt2.EnterMoveNextControl = true;
            this.TxtAmt2.Location = new System.Drawing.Point(112, 48);
            this.TxtAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt2.Name = "TxtAmt2";
            this.TxtAmt2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt2.Size = new System.Drawing.Size(234, 20);
            this.TxtAmt2.TabIndex = 66;
            this.TxtAmt2.ToolTip = "Based On Outgoing Payment\'s Currency";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(31, 50);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 14);
            this.label25.TabIndex = 65;
            this.label25.Text = "Paid Amount";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(112, 6);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 20;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 150;
            this.LueCurCode.Size = new System.Drawing.Size(155, 20);
            this.LueCurCode.TabIndex = 62;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(53, 8);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 14);
            this.label20.TabIndex = 61;
            this.label20.Text = "Currency";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(19, 29);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(89, 14);
            this.label21.TabIndex = 63;
            this.label21.Text = "Exchange Rate";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRateAmt
            // 
            this.TxtRateAmt.EnterMoveNextControl = true;
            this.TxtRateAmt.Location = new System.Drawing.Point(112, 27);
            this.TxtRateAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRateAmt.Name = "TxtRateAmt";
            this.TxtRateAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRateAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRateAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRateAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtRateAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRateAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRateAmt.Size = new System.Drawing.Size(234, 20);
            this.TxtRateAmt.TabIndex = 64;
            this.TxtRateAmt.Validated += new System.EventHandler(this.TxtRateAmt_Validated);
            // 
            // MeeDescription
            // 
            this.MeeDescription.EditValue = "";
            this.MeeDescription.EnterMoveNextControl = true;
            this.MeeDescription.Location = new System.Drawing.Point(112, 90);
            this.MeeDescription.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDescription.Name = "MeeDescription";
            this.MeeDescription.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.Appearance.Options.UseFont = true;
            this.MeeDescription.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDescription.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDescription.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDescription.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDescription.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDescription.Properties.MaxLength = 350;
            this.MeeDescription.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeDescription.Properties.ShowIcon = false;
            this.MeeDescription.Size = new System.Drawing.Size(306, 20);
            this.MeeDescription.TabIndex = 70;
            this.MeeDescription.ToolTip = "F4 : Show/hide text";
            this.MeeDescription.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDescription.ToolTipTitle = "Run System";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(41, 93);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(67, 14);
            this.label33.TabIndex = 69;
            this.label33.Text = "Description";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(7, 72);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(101, 14);
            this.label42.TabIndex = 67;
            this.label42.Text = "Person In Charge";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePIC
            // 
            this.LuePIC.EnterMoveNextControl = true;
            this.LuePIC.Location = new System.Drawing.Point(112, 69);
            this.LuePIC.Margin = new System.Windows.Forms.Padding(5);
            this.LuePIC.Name = "LuePIC";
            this.LuePIC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.Appearance.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePIC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePIC.Properties.DropDownRows = 25;
            this.LuePIC.Properties.NullText = "[Empty]";
            this.LuePIC.Properties.PopupWidth = 500;
            this.LuePIC.Size = new System.Drawing.Size(306, 20);
            this.LuePIC.TabIndex = 68;
            this.LuePIC.ToolTip = "F4 : Show/hide list";
            this.LuePIC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePIC.EditValueChanged += new System.EventHandler(this.LuePIC_EditValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(36, 9);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(85, 14);
            this.label35.TabIndex = 49;
            this.label35.Text = "Account Type";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDate2
            // 
            this.DteDueDate2.EditValue = null;
            this.DteDueDate2.EnterMoveNextControl = true;
            this.DteDueDate2.Location = new System.Drawing.Point(126, 111);
            this.DteDueDate2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDate2.Name = "DteDueDate2";
            this.DteDueDate2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDate2.Properties.Appearance.Options.UseFont = true;
            this.DteDueDate2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDate2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDate2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDate2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDate2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDate2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDate2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDate2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDate2.Properties.MaxLength = 8;
            this.DteDueDate2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDate2.Size = new System.Drawing.Size(121, 20);
            this.DteDueDate2.TabIndex = 60;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(61, 114);
            this.label34.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 14);
            this.label34.TabIndex = 59;
            this.label34.Text = "Due Date";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(34, 51);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(87, 14);
            this.label36.TabIndex = 53;
            this.label36.Text = "Payment Type";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePaymentType2
            // 
            this.LuePaymentType2.EnterMoveNextControl = true;
            this.LuePaymentType2.Location = new System.Drawing.Point(126, 48);
            this.LuePaymentType2.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType2.Name = "LuePaymentType2";
            this.LuePaymentType2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType2.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType2.Properties.DropDownRows = 20;
            this.LuePaymentType2.Properties.NullText = "[Empty]";
            this.LuePaymentType2.Properties.PopupWidth = 250;
            this.LuePaymentType2.Size = new System.Drawing.Size(283, 20);
            this.LuePaymentType2.TabIndex = 54;
            this.LuePaymentType2.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType2.EditValueChanged += new System.EventHandler(this.LuePaymentType2_EditValueChanged);
            // 
            // LueAcType
            // 
            this.LueAcType.EnterMoveNextControl = true;
            this.LueAcType.Location = new System.Drawing.Point(126, 6);
            this.LueAcType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcType.Name = "LueAcType";
            this.LueAcType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.Appearance.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcType.Properties.DropDownRows = 20;
            this.LueAcType.Properties.NullText = "[Empty]";
            this.LueAcType.Properties.PopupWidth = 143;
            this.LueAcType.Size = new System.Drawing.Size(121, 20);
            this.LueAcType.TabIndex = 50;
            this.LueAcType.ToolTip = "F4 : Show/hide list";
            this.LueAcType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcType.EditValueChanged += new System.EventHandler(this.LueAcType_EditValueChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(68, 30);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 14);
            this.label37.TabIndex = 51;
            this.label37.Text = "Account";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(126, 27);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 20;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 500;
            this.LueBankAcCode.Size = new System.Drawing.Size(283, 20);
            this.LueBankAcCode.TabIndex = 52;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel7.Controls.Add(this.TxtVCDocNo);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.TxtVRDocNo);
            this.panel7.Controls.Add(this.label23);
            this.panel7.Controls.Add(this.TxtOPDocNo);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(3, 221);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(905, 34);
            this.panel7.TabIndex = 30;
            // 
            // TxtVCDocNo
            // 
            this.TxtVCDocNo.EnterMoveNextControl = true;
            this.TxtVCDocNo.Location = new System.Drawing.Point(624, 7);
            this.TxtVCDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVCDocNo.Name = "TxtVCDocNo";
            this.TxtVCDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVCDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVCDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVCDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVCDocNo.Properties.MaxLength = 16;
            this.TxtVCDocNo.Properties.ReadOnly = true;
            this.TxtVCDocNo.Size = new System.Drawing.Size(205, 20);
            this.TxtVCDocNo.TabIndex = 76;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(593, 9);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 14);
            this.label26.TabIndex = 75;
            this.label26.Text = "VC";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVRDocNo
            // 
            this.TxtVRDocNo.EnterMoveNextControl = true;
            this.TxtVRDocNo.Location = new System.Drawing.Point(339, 7);
            this.TxtVRDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVRDocNo.Name = "TxtVRDocNo";
            this.TxtVRDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVRDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVRDocNo.Properties.MaxLength = 16;
            this.TxtVRDocNo.Properties.ReadOnly = true;
            this.TxtVRDocNo.Size = new System.Drawing.Size(205, 20);
            this.TxtVRDocNo.TabIndex = 74;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(305, 9);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 14);
            this.label23.TabIndex = 73;
            this.label23.Text = "VR";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOPDocNo
            // 
            this.TxtOPDocNo.EnterMoveNextControl = true;
            this.TxtOPDocNo.Location = new System.Drawing.Point(32, 8);
            this.TxtOPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOPDocNo.Name = "TxtOPDocNo";
            this.TxtOPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtOPDocNo.Properties.MaxLength = 16;
            this.TxtOPDocNo.Properties.ReadOnly = true;
            this.TxtOPDocNo.Size = new System.Drawing.Size(205, 20);
            this.TxtOPDocNo.TabIndex = 72;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(6, 11);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 14);
            this.label22.TabIndex = 71;
            this.label22.Text = "OP ";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgPurchaseInvoiceDtl3
            // 
            this.TpgPurchaseInvoiceDtl3.BackColor = System.Drawing.Color.SkyBlue;
            this.TpgPurchaseInvoiceDtl3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgPurchaseInvoiceDtl3.Controls.Add(this.Grd2);
            this.TpgPurchaseInvoiceDtl3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgPurchaseInvoiceDtl3.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchaseInvoiceDtl3.Name = "TpgPurchaseInvoiceDtl3";
            this.TpgPurchaseInvoiceDtl3.Size = new System.Drawing.Size(911, 258);
            this.TpgPurchaseInvoiceDtl3.TabIndex = 0;
            this.TpgPurchaseInvoiceDtl3.Text = "PO (Disc Amount/Custom Tax)";
            this.TpgPurchaseInvoiceDtl3.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(907, 254);
            this.Grd2.TabIndex = 48;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // TpgPurchaseInvoiceDtl7
            // 
            this.TpgPurchaseInvoiceDtl7.Controls.Add(this.Grd6);
            this.TpgPurchaseInvoiceDtl7.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchaseInvoiceDtl7.Name = "TpgPurchaseInvoiceDtl7";
            this.TpgPurchaseInvoiceDtl7.Size = new System.Drawing.Size(911, 258);
            this.TpgPurchaseInvoiceDtl7.TabIndex = 7;
            this.TpgPurchaseInvoiceDtl7.Text = "AP Downpayment";
            this.TpgPurchaseInvoiceDtl7.UseVisualStyleBackColor = true;
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(911, 258);
            this.Grd6.TabIndex = 48;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            // 
            // TpgPurchaseInvoiceDtl8
            // 
            this.TpgPurchaseInvoiceDtl8.Controls.Add(this.panel10);
            this.TpgPurchaseInvoiceDtl8.Location = new System.Drawing.Point(4, 26);
            this.TpgPurchaseInvoiceDtl8.Name = "TpgPurchaseInvoiceDtl8";
            this.TpgPurchaseInvoiceDtl8.Size = new System.Drawing.Size(911, 258);
            this.TpgPurchaseInvoiceDtl8.TabIndex = 8;
            this.TpgPurchaseInvoiceDtl8.Text = "Approval";
            this.TpgPurchaseInvoiceDtl8.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.TxtStatus);
            this.panel10.Controls.Add(this.label56);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(911, 258);
            this.panel10.TabIndex = 0;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(54, 5);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 23;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(229, 20);
            this.TxtStatus.TabIndex = 52;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(10, 8);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(42, 14);
            this.label56.TabIndex = 51;
            this.label56.Text = "Status";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(420, 47);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(65, 22);
            this.ChkCancelInd.TabIndex = 16;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // DteVdInvDt
            // 
            this.DteVdInvDt.EditValue = null;
            this.DteVdInvDt.EnterMoveNextControl = true;
            this.DteVdInvDt.Location = new System.Drawing.Point(146, 131);
            this.DteVdInvDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteVdInvDt.Name = "DteVdInvDt";
            this.DteVdInvDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.Appearance.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteVdInvDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteVdInvDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteVdInvDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteVdInvDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteVdInvDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteVdInvDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteVdInvDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteVdInvDt.Size = new System.Drawing.Size(116, 20);
            this.DteVdInvDt.TabIndex = 24;
            this.DteVdInvDt.EditValueChanged += new System.EventHandler(this.DteVdInvDt_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(14, 134);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Vendor\'s Invoice Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalWithTax
            // 
            this.TxtTotalWithTax.EnterMoveNextControl = true;
            this.TxtTotalWithTax.Location = new System.Drawing.Point(121, 68);
            this.TxtTotalWithTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalWithTax.Name = "TxtTotalWithTax";
            this.TxtTotalWithTax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalWithTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalWithTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalWithTax.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalWithTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalWithTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalWithTax.Properties.ReadOnly = true;
            this.TxtTotalWithTax.Size = new System.Drawing.Size(229, 20);
            this.TxtTotalWithTax.TabIndex = 36;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(27, 71);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 14);
            this.label13.TabIndex = 35;
            this.label13.Text = "Total With Tax";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(121, 110);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(229, 20);
            this.TxtAmt.TabIndex = 40;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(22, 113);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 14);
            this.label14.TabIndex = 40;
            this.label14.Text = "Invoice Amount";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDownPayment
            // 
            this.TxtDownPayment.EnterMoveNextControl = true;
            this.TxtDownPayment.Location = new System.Drawing.Point(121, 89);
            this.TxtDownPayment.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDownPayment.Name = "TxtDownPayment";
            this.TxtDownPayment.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDownPayment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDownPayment.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDownPayment.Properties.Appearance.Options.UseFont = true;
            this.TxtDownPayment.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDownPayment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDownPayment.Size = new System.Drawing.Size(229, 20);
            this.TxtDownPayment.TabIndex = 38;
            this.TxtDownPayment.Validated += new System.EventHandler(this.TxtDownPayment_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(29, 92);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 14);
            this.label15.TabIndex = 37;
            this.label15.Text = "Downpayment";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.DtePaymentDt);
            this.panel5.Controls.Add(this.label55);
            this.panel5.Controls.Add(this.TxtSiteCode);
            this.panel5.Controls.Add(this.LblSiteCode);
            this.panel5.Controls.Add(this.LblDeptCode);
            this.panel5.Controls.Add(this.TxtCurCode);
            this.panel5.Controls.Add(this.LueDeptCode);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.TxtTotalWithoutTax);
            this.panel5.Controls.Add(this.TxtAmt);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.TxtDownPayment);
            this.panel5.Controls.Add(this.LblRemark);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.TxtTotalWithTax);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(491, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(358, 198);
            this.panel5.TabIndex = 27;
            // 
            // DtePaymentDt
            // 
            this.DtePaymentDt.EditValue = null;
            this.DtePaymentDt.EnterMoveNextControl = true;
            this.DtePaymentDt.Location = new System.Drawing.Point(121, 4);
            this.DtePaymentDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DtePaymentDt.Name = "DtePaymentDt";
            this.DtePaymentDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt.Properties.Appearance.Options.UseFont = true;
            this.DtePaymentDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DtePaymentDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DtePaymentDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DtePaymentDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DtePaymentDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtePaymentDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DtePaymentDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DtePaymentDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DtePaymentDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePaymentDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DtePaymentDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DtePaymentDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DtePaymentDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DtePaymentDt.Size = new System.Drawing.Size(116, 20);
            this.DtePaymentDt.TabIndex = 50;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(29, 7);
            this.label55.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(85, 14);
            this.label55.TabIndex = 49;
            this.label55.Text = "Payment Date";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSiteCode
            // 
            this.TxtSiteCode.EnterMoveNextControl = true;
            this.TxtSiteCode.Location = new System.Drawing.Point(121, 152);
            this.TxtSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteCode.Name = "TxtSiteCode";
            this.TxtSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCode.Properties.MaxLength = 23;
            this.TxtSiteCode.Properties.ReadOnly = true;
            this.TxtSiteCode.Size = new System.Drawing.Size(229, 20);
            this.TxtSiteCode.TabIndex = 44;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(88, 155);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 43;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDeptCode
            // 
            this.LblDeptCode.AutoSize = true;
            this.LblDeptCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeptCode.ForeColor = System.Drawing.Color.Black;
            this.LblDeptCode.Location = new System.Drawing.Point(43, 134);
            this.LblDeptCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeptCode.Name = "LblDeptCode";
            this.LblDeptCode.Size = new System.Drawing.Size(73, 14);
            this.LblDeptCode.TabIndex = 41;
            this.LblDeptCode.Text = "Department";
            this.LblDeptCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(121, 26);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 3;
            this.TxtCurCode.Size = new System.Drawing.Size(229, 20);
            this.TxtCurCode.TabIndex = 32;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(121, 131);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 250;
            this.LueDeptCode.Size = new System.Drawing.Size(229, 20);
            this.LueDeptCode.TabIndex = 42;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(61, 29);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 14);
            this.label24.TabIndex = 31;
            this.label24.Text = "Currency";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDueDt
            // 
            this.BtnDueDt.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDueDt.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDueDt.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDueDt.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDueDt.Appearance.Options.UseBackColor = true;
            this.BtnDueDt.Appearance.Options.UseFont = true;
            this.BtnDueDt.Appearance.Options.UseForeColor = true;
            this.BtnDueDt.Appearance.Options.UseTextOptions = true;
            this.BtnDueDt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDueDt.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDueDt.Image = ((System.Drawing.Image)(resources.GetObject("BtnDueDt.Image")));
            this.BtnDueDt.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDueDt.Location = new System.Drawing.Point(263, 171);
            this.BtnDueDt.Name = "BtnDueDt";
            this.BtnDueDt.Size = new System.Drawing.Size(24, 21);
            this.BtnDueDt.TabIndex = 29;
            this.BtnDueDt.ToolTip = "Auto Fill Due Date";
            this.BtnDueDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDueDt.ToolTipTitle = "Run System";
            this.BtnDueDt.Click += new System.EventHandler(this.BtnDueDt_Click);
            // 
            // LblPaymentType
            // 
            this.LblPaymentType.AutoSize = true;
            this.LblPaymentType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPaymentType.ForeColor = System.Drawing.Color.Black;
            this.LblPaymentType.Location = new System.Drawing.Point(6, 155);
            this.LblPaymentType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPaymentType.Name = "LblPaymentType";
            this.LblPaymentType.Size = new System.Drawing.Size(136, 14);
            this.LblPaymentType.TabIndex = 25;
            this.LblPaymentType.Text = "Request Payment Type";
            this.LblPaymentType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(146, 152);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 30;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 300;
            this.LuePaymentType.Size = new System.Drawing.Size(273, 20);
            this.LuePaymentType.TabIndex = 26;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(146, 68);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 250;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(273, 20);
            this.TxtLocalDocNo.TabIndex = 18;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(99, 71);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 14);
            this.label18.TabIndex = 17;
            this.label18.Text = "Local#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 50);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(135, 14);
            this.label19.TabIndex = 14;
            this.label19.Text = "Reason For Cancellation";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(146, 47);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(273, 20);
            this.MeeCancelReason.TabIndex = 15;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmPurchaseInvoice3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 599);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmPurchaseInvoice3";
            this.Text = " ";
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdInvNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWithoutTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgPurchaseInvoiceDtl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpgPurchaseInvoiceDtl1.ResumeLayout(false);
            this.TpgPurchaseInvoiceDtl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeServiceNote3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeServiceNote2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeServiceNote1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueServiceCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo2.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.PnlQRCode.ResumeLayout(false);
            this.PnlQRCode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteQRCodeTaxInvoiceDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQRCodeTaxInvoiceDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQRCodeTaxInvoiceNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQRCodeTaxAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmtDifference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalTaxAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxRateAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalTaxAmt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvoiceDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxInvoiceNo.Properties)).EndInit();
            this.TpgPurchaseInvoiceDtl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCOATaxInd.Properties)).EndInit();
            this.TpgPurchaseInvoiceDtl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTaxInvDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgPurchaseInvoiceDtl6.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRateAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDate2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDate2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOPDocNo.Properties)).EndInit();
            this.TpgPurchaseInvoiceDtl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpgPurchaseInvoiceDtl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpgPurchaseInvoiceDtl8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVdInvDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVdInvDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWithTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDownPayment.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtePaymentDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtTotalWithoutTax;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueVdCode;
        internal DevExpress.XtraEditors.TextEdit TxtVdInvNo;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label LblRemark;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgPurchaseInvoiceDtl3;
        private System.Windows.Forms.TabPage TpgPurchaseInvoiceDtl4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.TabPage TpgPurchaseInvoiceDtl2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraEditors.LookUpEdit LueDocType;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage TpgPurchaseInvoiceDtl1;
        internal DevExpress.XtraEditors.DateEdit DteVdInvDt;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode3;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode2;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode1;
        internal DevExpress.XtraEditors.TextEdit TxtTotalTaxAmt1;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtTaxRateAmt;
        private System.Windows.Forms.Label LblTaxRateAmt;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt3;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt2;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt1;
        internal DevExpress.XtraEditors.TextEdit TxtTotalTaxAmt2;
        private System.Windows.Forms.Label LblTotalTaxAmt2;
        internal DevExpress.XtraEditors.TextEdit TxtTotalWithTax;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtDownPayment;
        private System.Windows.Forms.Label label15;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TabPage TpgPurchaseInvoiceDtl5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.LookUpEdit LueDocInd;
        private System.Windows.Forms.Label LblDeptCode;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label LblPaymentType;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtSiteCode;
        private System.Windows.Forms.Label LblSiteCode;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.TabPage TpgPurchaseInvoiceDtl6;
        protected System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtGiroNo;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        protected System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.DateEdit DteDueDate2;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.MemoExEdit MeeDescription;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.LookUpEdit LuePIC;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType2;
        private DevExpress.XtraEditors.LookUpEdit LueAcType;
        private System.Windows.Forms.Label label37;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        internal DevExpress.XtraEditors.TextEdit TxtAmt2;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtRateAmt;
        protected System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtVCDocNo;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtVRDocNo;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtOPDocNo;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvDt;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel8;
        public DevExpress.XtraEditors.SimpleButton BtnDueDt;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.DateEdit DteTaxInvoiceDt3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtTaxInvoiceNo3;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel9;
        private DevExpress.XtraEditors.CheckEdit ChkCOATaxInd;
        private System.Windows.Forms.Panel PnlQRCode;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit TxtQRCodeTaxAmt;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmtDifference;
        internal DevExpress.XtraEditors.DateEdit DteQRCodeTaxInvoiceDt;
        private System.Windows.Forms.Label label46;
        internal DevExpress.XtraEditors.TextEdit TxtQRCodeTaxInvoiceNo;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.TextEdit TxtAlias1;
        internal DevExpress.XtraEditors.TextEdit TxtAlias3;
        internal DevExpress.XtraEditors.TextEdit TxtAlias2;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label51;
        private DevExpress.XtraEditors.LookUpEdit LueServiceCode3;
        private System.Windows.Forms.Label label50;
        private DevExpress.XtraEditors.LookUpEdit LueServiceCode2;
        private System.Windows.Forms.Label label49;
        private DevExpress.XtraEditors.LookUpEdit LueServiceCode1;
        private DevExpress.XtraEditors.MemoExEdit MeeServiceNote2;
        private System.Windows.Forms.Label label53;
        private DevExpress.XtraEditors.MemoExEdit MeeServiceNote1;
        private System.Windows.Forms.Label label52;
        private DevExpress.XtraEditors.MemoExEdit MeeServiceNote3;
        private System.Windows.Forms.Label label54;
        internal DevExpress.XtraEditors.DateEdit DtePaymentDt;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.TabPage TpgPurchaseInvoiceDtl7;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private System.Windows.Forms.TabPage TpgPurchaseInvoiceDtl8;
        private System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Panel panel11;
        internal DevExpress.XtraEditors.TextEdit TxtAmt3;
        private System.Windows.Forms.Label label57;
    }
}