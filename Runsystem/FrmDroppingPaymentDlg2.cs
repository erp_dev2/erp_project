﻿#region Update
/*
    27/06/2019 [WED] ambil dari Project Implementation RBP
    01/07/2019 [WED] Direct Cost > 0
    24/09/2019 [WED/YK] alur diubah ke SO Contract Revision
    27/11/2019 [WED/VIR] Dropping Request yg budget, tidak melihat site
    28/01/2020 [WED/VIR] remuneration bisa di dropping payment, berdasarkan parameter IsDroppingRequestRemunerationProceedToPayment
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDroppingPaymentDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDroppingPayment mFrmParent;
        private string mSQL = string.Empty, mDRQDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmDroppingPaymentDlg2(FrmDroppingPayment FrmParent, string DRQDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDRQDocNo = DRQDocNo;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select '1' As DocType, A.DocNo, B.DNo, C.ResourceItCode ResourceCode, D.ItName ResourceName, B.OutstandingAmt ");
            SQL.AppendLine("    From TblDroppingRequestHdr A ");
            SQL.AppendLine("    Inner Join TblDroppingRequestDtl B On A.DocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("        And B.PaidInd In ('O', 'P') ");
            if(!mFrmParent.mIsDroppingRequestRemunerationProceedToPayment)
                SQL.AppendLine("        And B.DirectCostAmt > 0.00 ");
            SQL.AppendLine("        And Locate(Concat('##', B.DNo, '##'), @SelectedDRQDNo) < 1 ");
            SQL.AppendLine("    Inner Join TblProjectImplementationRBPHdr B1 On B.PRBPDocNo = B1.DocNo ");
            SQL.AppendLine("    Inner Join TblProjectImplementationDtl2 C On B1.PRJIDocNo = C.DocNo And B1.ResourceItCode = C.ResourceItCode ");
            SQL.AppendLine("    Inner Join TblItem D On C.ResourceItCode = D.ItCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblProjectImplementationHdr E On A.PRJIDocNo=E.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr E1 On E.SOContractDocNo = E1.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr F On E1.SOCDocNo=F.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr G On F.BOQDocNo=G.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr H On G.LOPDocNo=H.DocNo ");
                SQL.AppendLine("And (H.SiteCode Is Null Or ( ");
                SQL.AppendLine("    H.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(H.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As DocType, X1.DocNo, X2.DNo, X2.BCCode ResourceCode, X3.BCName ResourceName, X2.OutstandingAmt ");
            SQL.AppendLine("    From TblDroppingRequestHdr X1 ");
            SQL.AppendLine("    Inner Join TblDroppingRequestDtl2 X2 On X1.DocNo = X2.DocNo And X1.DocNo = @DocNo ");
            SQL.AppendLine("        And X2.PaidInd In ('O', 'P') ");
            SQL.AppendLine("        And Locate(Concat('##', X2.DNo, '##'), @SelectedDRQDNo) < 1 ");
            SQL.AppendLine("    Inner Join TblBudgetCategory X3 On X2.BCCode = X3.BCCode ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "DNo",
                    "Resource Code",
                    "Resource / Category",
                    "Outstanding Amount",
                }, new int[]
                {
                    //0
                    50,

                    //1-4
                    20, 20, 50, 220, 180
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DocNo", mDRQDocNo);
                Sm.CmParam<String>(ref cm, "@SelectedDRQDNo", mFrmParent.GetSelectedDRQDNo());

                Sm.FilterStr(ref Filter, ref cm, TxtResourceItCode.Text, new string[] { "T.ResourceCode", "T.ResourceName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.DNo; ",
                    new string[]
                    {
                        //0
                        "DNo", 

                        //1-3
                        "ResourceCode", "ResourceName", "OutstandingAmt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 5);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 4 });

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 3, 4 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 resource / category.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), ItCode)) return true;
            return false;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtResourceItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkResourceItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Resource / Category");
        }

        #endregion

        #endregion

    }
}
