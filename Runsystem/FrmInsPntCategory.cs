﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmInsPntCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmInsPntCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmInsPntCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }
        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

            base.FrmLoad(sender, e);
            SetLueType(ref LueTypeCode);
        }
         
        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
          if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtInspntCtCode, TxtInspntCtName, LueTypeCode, MeeRemark
                    }, true);
                    TxtInspntCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtInspntCtCode, TxtInspntCtName, LueTypeCode, MeeRemark
                    }, false);
                    TxtInspntCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtInspntCtName, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtInspntCtName, LueTypeCode
                    }, false);
                    TxtInspntCtName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtInspntCtCode, TxtInspntCtName, LueTypeCode, MeeRemark
            });
        }

        #endregion

        #region Button Method

       override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInsPntCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        } 

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtInspntCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into tblinspntcategory(InspntCtCode, InspntCtName, InspntType, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@InspntCtCode, @InspntCtName, @Inspnttype, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("Update InspntCtName=@InspntCtName, InspntType=@InspntType, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@InspntCtCode", TxtInspntCtCode.Text);
                Sm.CmParam<String>(ref cm, "@InspntCtName", TxtInspntCtName.Text);
                Sm.CmParam<String>(ref cm, "@InspntType", Sm.GetLue(LueTypeCode));
                Sm.CmParam<String>(ref cm, "@Remark",MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtInspntCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ProvCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@InspntCtCode", ProvCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select InspntCtCode, InspntCtName, InspntType, Remark From tblinspntcategory Where InspntCtCode=@InspntCtCode",
                        new string[] 
                        {
                            "InspntCtCode", "InspntCtName", "InspntType", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtInspntCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtInspntCtName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueTypeCode, Sm.DrStr(dr, c[2]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtInspntCtCode, "Insentif/penalty category code", false) ||
                Sm.IsTxtEmpty(TxtInspntCtName, "Insentif/penalty category name", false) ||
                Sm.IsTxtEmpty(LueTypeCode, "Type", false) ||
                IsCityCodeExisted();
        }

        private bool IsCityCodeExisted()
        {
            if (!TxtInspntCtCode.Properties.ReadOnly && Sm.IsDataExist("Select InspntCtCode From tblinspntcategory Where InspntCtCode='" + TxtInspntCtCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Insentif & penalty category code ( " + TxtInspntCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void SetLueType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
               ref Lue,
               "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='EmployeeDecisionType'",
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtInspntCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtInspntCtCode);
        }
        private void LueTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTypeCode, new Sm.RefreshLue1(SetLueType));
        }

        private void TxtInspntCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtInspntCtName);
        }

        #endregion

    }
}
