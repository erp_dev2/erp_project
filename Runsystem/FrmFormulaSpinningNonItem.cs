﻿#region Update
/*
    16/03/2018 [TKG] New appllication
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFormulaSpinningNonItem : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmFormulaSpinningNonItemFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmFormulaSpinningNonItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtFSNICode, TxtFSNIName, MeeQuery, TxtParam1, TxtParam2,  
                        TxtParam3, TxtParam4, TxtParam5, TxtParam6, MeeRemark
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    ChkTotalInd.Properties.ReadOnly = true;
                    TxtFSNICode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtFSNICode, TxtFSNIName, MeeQuery, TxtParam1, TxtParam2,  
                        TxtParam3, TxtParam4, TxtParam5, TxtParam6, MeeRemark
                    }, false);
                    ChkTotalInd.Properties.ReadOnly = false;
                    TxtFSNICode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtFSNIName, MeeQuery, TxtParam1, TxtParam2,  TxtParam3, 
                        TxtParam4, TxtParam5, TxtParam6, MeeRemark
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    ChkTotalInd.Properties.ReadOnly = false;
                    TxtFSNIName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtFSNICode, TxtFSNIName, MeeQuery, TxtParam1, TxtParam2,  
                TxtParam3, TxtParam4, TxtParam5, TxtParam6, MeeRemark
            });
            ChkActInd.Checked = false;
            ChkTotalInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFormulaSpinningNonItemFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFSNICode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtFSNICode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblFormulaSpinningNonItem Where FSNICode=@FSNICode;" };
                Sm.CmParam<String>(ref cm, "@FSNICode", TxtFSNICode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblFormulaSpinningNonItem ");
                SQL.AppendLine("(FSNICode, FSNIName, ActInd, TotalInd, Query, Param1, Param2, Param3, Param4, Param5, Param6, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@FSNICode, @FSNIName, @ActInd, @TotalInd, @Query, @Param1, @Param2, @Param3, @Param4, @Param5, @Param6, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update FSNIName=@FSNIName, ActInd=@ActInd, TotalInd=@TotalInd, ");
                SQL.AppendLine("   Query=@Query, Param1=@Param1, Param2=@Param2, Param3=@Param3, Param4=@Param4, Param5=@Param5, Param6=@Param6, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FSNICode", TxtFSNICode.Text);
                Sm.CmParam<String>(ref cm, "@FSNIName", TxtFSNIName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@TotalInd", ChkTotalInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@Query", MeeQuery.Text);
                Sm.CmParam<String>(ref cm, "@Param1", TxtParam1.Text);
                Sm.CmParam<String>(ref cm, "@Param2", TxtParam2.Text);
                Sm.CmParam<String>(ref cm, "@Param3", TxtParam3.Text);
                Sm.CmParam<String>(ref cm, "@Param4", TxtParam4.Text);
                Sm.CmParam<String>(ref cm, "@Param5", TxtParam5.Text);
                Sm.CmParam<String>(ref cm, "@Param6", TxtParam6.Text);
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtFSNICode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string Code)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@FSNICode", Code);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FSNICode, FSNIName, ActInd, TotalInd, Query, Param1, Param2, Param3, Param4, Param5, Param6, Remark ");
                SQL.AppendLine("From TblFormulaSpinningNonItem ");
                SQL.AppendLine("Where FSNICode=@FSNICode;");

                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "FSNICode", 

                            //1-5
                            "FSNIName", "ActInd", "TotalInd", "Query", "Param1", 
                            
                            //6-10
                            "Param2", "Param3", "Param4", "Param5", "Param6", 
                            
                            //11
                            "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtFSNICode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtFSNIName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            ChkTotalInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                            MeeQuery.EditValue = Sm.DrStr(dr, c[4]);
                            TxtParam1.EditValue = Sm.DrStr(dr, c[5]);
                            TxtParam2.EditValue = Sm.DrStr(dr, c[6]);
                            TxtParam3.EditValue = Sm.DrStr(dr, c[7]);
                            TxtParam4.EditValue = Sm.DrStr(dr, c[8]);
                            TxtParam5.EditValue = Sm.DrStr(dr, c[9]);
                            TxtParam6.EditValue = Sm.DrStr(dr, c[10]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtFSNICode, "Code", false) ||
                Sm.IsTxtEmpty(TxtFSNIName, "Name", false) ||
                Sm.IsMeeEmpty(MeeQuery, "Query") ||
                IsCodeExisted();
        }

        private bool IsCodeExisted()
        {
            if (TxtFSNICode.Properties.ReadOnly) return false;
            return Sm.IsDataExist(
                "Select 1 From TblFormulaSpinningNonItem " +
                "Where FSNICode=@Param Limit 1;", 
                TxtFSNICode.Text,
                "Code ( " + TxtFSNICode.Text + " ) already existed."
                );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtFSNICode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtFSNICode);
        }

        private void TxtFSNIName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtFSNIName);
        }

        private void MeeQuery_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeTrim(MeeQuery);
        }

        private void TxtParam1_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtParam1);
        }

        private void TxtParam2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtParam2);
        }

        private void TxtParam3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtParam3);
        }

        private void TxtParam4_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtParam4);
        }

        private void TxtParam5_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtParam5);
        }

        private void TxtParam6_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtParam6);
        }

        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeTrim(MeeRemark);
        }

        #endregion

        #endregion
    }
}
