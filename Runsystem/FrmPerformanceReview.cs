﻿#region Update
/*
    31/03/2018 [TKG] tambah filter site dan level 
    20/04/2018 [HAR] feedback result indicator dijadiin satu, achievement juga disatuin tpi berdasarkan type nya jika SUM ya di SUm klo AVg ya dirata rata
    23/08/2018 [WED] rubah rumus perhitungan Average, berdasarkan TblKPIDtl.ControlInd
                    ControlInd == "N", Avg = (((Achievement / Target) * 100) * Bobot KPIDtl) / Total Bobot KPI
                    ControlInd == "Y", Avg = (((Target / Achievement) * 100) * Bobot KPIDtl) / Total Bobot KPI
    23/08/2018 [WED] Average di Hdr ==> Total Average. Sehingga, data yang dihitung adalah Sum(Average Dtl), bukan lagi Sum(Average Dtl) / Count (Dtl)
    24/09/2018 [HAR] urutan Result di sesuain dengan KPI 
    11/12/2018 [HAR] bug nilai achievment
    19/12/2018 [DITA] tambah textbox PIC Name pada header
    15/01/2019 [DITA] perhitungan average saat target = 0
    23/04/2019 [WED] perhitungan average saat pencapaian = 0, ControlInd = 'Y', target != 0
    15/10/2019 [DITA/HIN] Menyamakan nilai ACHIEVEMENTantara Reporting KPI Process & Performance Review dengan Reporting KPI sebagai nilai acuan
    17/05/2022 [TYO/Product] Menambah checkbox cancelInd
    29/08/2022 [IBL/TWC] Tambah CancelInd = 'N' di method IsKPIAlreadyProcess();
    13/09/2022 [HPH/TWC] Tambah parameter MaxTotalAveragePerformanceReview, Nilai/Skor Average pada KPI perlu ada Maksimum Value yang berlaku di TWC yaitu 120
    19/09/2022 [HPH/TWC] Bug : Nilai skor average pada field total average saat insert belum sesuai parameter MaxTotalAveragePerformanceReview
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmPerformanceReview : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty, 
            mCity = string.Empty, 
            mCnt = string.Empty, 
            mExpCity = string.Empty,
            mMaxTotalAveragePerformanceReview = string.Empty;
        internal bool
            mIsFilterBySiteHR = false,
            mIsFilterByLevelHR = false;           
        internal FrmPerformanceReviewFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPerformanceReview(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Performance Evaluation";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLuePosCode(ref LuePosCode);
                SetLuePerformanceGrade(ref LuePerformanceGrade);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByLevelHR = Sm.GetParameterBoo("IsFilterByLevelHR");
            mMaxTotalAveragePerformanceReview = Sm.GetParameter("MaxTotalAveragePerformanceReview");
        }

                        #region Set Grid

                        private void SetGrd()
        {
            #region Grid 1 - KPI Process

            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "KPI Process DocNo",
                    "KPI Process DNo",
                    "Result" + Environment.NewLine + "Indicator",
                    "Bobot",
                    "Unit of Measurement",
                    
                    //6-10
                    "Year / Month",
                    "Type",
                    "Target",
                    "Achievement",
                    "Average (%)",

                    //11
                    "ControlInd"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    120, 20, 250, 80, 150,   
                    
                    //6-10
                    100, 80, 150, 150, 130,

                    //11
                    0
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 8, 9, 10 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 4, 7, 11 }, false);

            #endregion

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtKPIDocNo, LuePosCode, TxtPeriod, MeeRemark, LuePerformanceGrade, TxtTotalAvg, TxtPICName
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnKPIDocNo.Enabled = false;
                    BtnKPIDocNo2.Enabled = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, TxtPeriod, MeeRemark, LuePerformanceGrade
                    }, false);
                    BtnKPIDocNo.Enabled = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    BtnKPIDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = false;
                    Grd1.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, TxtPeriod, TxtKPIDocNo, LuePosCode, MeeRemark, LuePerformanceGrade, TxtPICName, TxtTotalAvg,
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
            ChkCancelInd.Checked = false;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 8, 9, 10 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPerformanceReviewFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnKPIDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPerformanceReviewDlg(this));
        }

        private void BtnKPIDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtKPIDocNo, "KPI#", false))
            {
                var f = new FrmKPI(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = "KPI";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtKPIDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PerformanceReview", "TblPerformanceReviewHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePerformanceReviewHdr(DocNo));
            int CountData = 0;

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    string KPIProcessDocNo = Sm.GetGrdStr(Grd1, r, 1);
                    string mKPIProcessDocNo = String.Empty;
                    int indexDocNo = 0;
                    for (int i = 1; i < KPIProcessDocNo.Length; i++)
                    {
                        if (KPIProcessDocNo[i] == '#')
                        {
                            mKPIProcessDocNo = KPIProcessDocNo.Substring(indexDocNo, i - indexDocNo);
                            indexDocNo = i + 1;
                            CountData = CountData + 1;

                            cml.Add(SavePerformanceReviewDtl(DocNo, CountData, Sm.Left(mKPIProcessDocNo, (mKPIProcessDocNo.Length-3)), Sm.Right(mKPIProcessDocNo, 3)));
                        }
                    }
                }
            }
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtKPIDocNo, "KPI#", false) ||
                Sm.IsTxtEmpty(TxtPeriod, "Period", false) ||
                Sm.IsLueEmpty(LuePerformanceGrade, "Grade") ||
                IsKPIAlreadyProcess()||
                IsGrd1Empty() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdExceedMaxRecords()
        {
            if ((Grd1.Rows.Count > 1000))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered ( KPI Review : " + (Grd1.Rows.Count - 1).ToString() + "  ) exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 KPI Evaluation.");
                return true;
            }
            return false;
        }

        private bool IsKPIAlreadyProcess()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "Select concat(KPIDocNo, Period) From TblPerformanceReviewHdr " +
                "Where Concat(KPIDocNo, Period) = @Param And CancelInd = 'N';"
            };
            Sm.CmParam<String>(ref cm, "@Param", string.Concat(TxtKPIDocNo.Text, TxtPeriod.Text));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "KPI for this period has been created.");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePerformanceReviewHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPerformanceReviewHdr(DocNo, DocDt, KPIDocNo, CancelInd, PosCode, Period, Average, Grade, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @KPIDocNo, @CancelInd, @PosCode, @Period, @Average, @Grade, @Remark, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@KPIDocNo", TxtKPIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@Period", TxtPeriod.Text);
            Sm.CmParam<Decimal>(ref cm, "@Average", Decimal.Parse(TxtTotalAvg.Text));
            Sm.CmParam<String>(ref cm, "@Grade", Sm.GetLue(LuePerformanceGrade));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePerformanceReviewDtl(string DocNo, int Row, string KPIProcessDocNo, string KPIProcessDNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblPerformanceReviewDtl(DocNo, DNo, KPIProcessDocNo, KPIProcessDNo, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @KPIProcessDocNo, @KPIProcessDNo, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + Row.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@KPIProcessDocNo", KPIProcessDocNo);
            Sm.CmParam<String>(ref cm, "@KPIProcessDNo", KPIProcessDNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            string DocNo = TxtDocNo.Text;
            cml.Add(CancelPeroformanceReview(DocNo));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private MySqlCommand CancelPeroformanceReview(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblPerformanceReviewHdr ");
            SQL.AppendLine("Set CancelInd = @CancelInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where Docno =@Docno ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPerformanceReviewHdr(DocNo);
                ShowPerformanceReviewDtl(DocNo);
                ComputeTotalAverage();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPerformanceReviewHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.KPIDocNo, B.PosCode, Period, Average, Grade, A.Remark, C.EmpName, A.CancelInd ");
            SQL.AppendLine("From TblPerformanceReviewHdr A ");
            SQL.AppendLine("Inner Join TblKPIHdr B On A.KPIDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "KPIDocNo", "PosCode", "Period", "Average", 
                        //6-9
                        "Grade", "Remark", "EmpName", "CancelInd"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtKPIDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[3]));
                        TxtPeriod.EditValue = Sm.DrStr(dr, c[4]);
                        TxtTotalAvg.EditValue = Sm.DrDec(dr, c[5]);
                        Sm.SetLue(LuePerformanceGrade, Sm.DrStr(dr, c[6]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        TxtPICName.EditValue = Sm.DrStr(dr, c[8]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[9]), "Y");
                    }, true
                );
        }

        private void ShowPerformanceReviewDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select Group_Concat(Distinct X.Dno) Dno, Concat(group_Concat(Distinct X.KPIProcessDocNo Separator '#'), '#') KPIProcessDocNo, Group_Concat(Distinct X.KPIProcessDNo Separator '#') KPIProcessDNo, Group_Concat(Distinct X.Result) Result, ");
            SQLDtl.AppendLine("Group_Concat(Distinct X.Bobot) Bobot, Group_Concat(Distinct X.Uom) Uom, Group_Concat(Distinct X.Option) option,   ");
            SQLDtl.AppendLine("CAST(Group_Concat(Distinct X.Target)AS DECIMAL(18,4)) target, Group_Concat(Distinct X.type) Type,   ");
            SQLDtl.AppendLine("if(X.Type = 'SUM', SUM(Value), (SUM(Value)/12)) As Value, Group_Concat(Distinct X.ControlInd) ControlInd ");
            SQLDtl.AppendLine("From ( ");
            SQLDtl.AppendLine("     Select B.DocNo, B.Dno, B.KpiProcessDocNo, B.KPIProcessDno, D.Result,  ");
            SQLDtl.AppendLine("     D.Bobot, D.Uom, D.Option, D.Target, D.Type, C.Value, D.ControlInd ");
            SQLDtl.AppendLine("     From TblPerformanceReviewHdr A  ");
            SQLDtl.AppendLine("     Inner Join TblPerformanceReviewDtl B On A.DocNo = B.DocNo   ");
            SQLDtl.AppendLine("     Inner Join TblKPIProcessDtl C On B.KPIProcessDocNo = C.Docno And B.KPIProcessDNo = C.DNo  ");
            SQLDtl.AppendLine("     Inner Join TblKPIDtl D On A.KPIDocNo = D.DocNo  And C.KPIDno = D.Dno  ");
            SQLDtl.AppendLine("     Where A.DocNo = @DocNo  ");
            SQLDtl.AppendLine("     Order By B.DNo ");
            SQLDtl.AppendLine(")X ");
            SQLDtl.AppendLine("Group By X.Result order by X.Dno ; ");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "KPIProcessDocNo", "KPIProcessDNo", "Result", "Bobot", "Uom", 

                    //6-10
                    "Option", "Type", "Target", "Value", "ControlInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 9);
                    Grd1.Cells[Row, 10].Value = 0m;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                    //Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 10);
                }, false, false, true, false
            );
            decimal mTotalBobotKPIDtl = ComputeTotalBobotKPIDtl();
            ComputeAverageRow(mTotalBobotKPIDtl);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 8, 9, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
          
        }

        #endregion

        #region Additional Method

        private static void SetLuePerformanceGrade(ref LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select grdStatus As Col1 ");
                SQL.AppendLine("From tblperformancegrade ");
                SQL.AppendLine("Order By grdStatus; ");

                Sm.SetLue1(
                    ref Lue, SQL.ToString(),
                    "Grade");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeAverageRow(decimal TotalBobotKPIDtl)
        {
            #region OldCode
            //if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0 && Sm.GetGrdDec(Grd1, Row, 8) > 0)
            //{
            //    Grd1.Cells[Row, 10].Value = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row, 9) / Sm.GetGrdDec(Grd1, Row, 8)) * 100, 0);
            //}
            //else
            //{
            //    Grd1.Cells[Row, 10].Value = Sm.FormatNum(0, 0);
            //}
            #endregion

            if (TotalBobotKPIDtl != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if ((Sm.GetGrdDec(Grd1, Row, 8) == 0) ||
                        ((Sm.GetGrdDec(Grd1, Row, 8) == 0) && (Sm.GetGrdDec(Grd1, Row, 9) == 0))) // target = 0 || target = 0 & achievement = 0;
                    {
                        Grd1.Cells[Row, 10].Value = ((Sm.GetGrdDec(Grd1, Row, 4) - Sm.GetGrdDec(Grd1, Row, 9)));
                    }
                    
                    if ((Sm.GetGrdDec(Grd1, Row, 9) == 0) && (Sm.GetGrdDec(Grd1, Row, 8) != 0)) // achievement = 0; target != 0
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11) == "Y") // controlInd
                            Grd1.Cells[Row, 10].Value = Sm.GetGrdDec(Grd1, Row, 4);
                        else
                            Grd1.Cells[Row, 10].Value = 0m;
                    }

                    if ((Sm.GetGrdDec(Grd1, Row, 9) != 0) && (Sm.GetGrdDec(Grd1, Row, 8) != 0))
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11) == "N")
                        {
                            Grd1.Cells[Row, 10].Value = (((Sm.GetGrdDec(Grd1, Row, 9) / Sm.GetGrdDec(Grd1, Row, 8)) * 100) * Sm.GetGrdDec(Grd1, Row, 4)) / TotalBobotKPIDtl;
                        }
                        else
                        {
                            Grd1.Cells[Row, 10].Value = (((Sm.GetGrdDec(Grd1, Row, 8) / Sm.GetGrdDec(Grd1, Row, 9)) * 100) * Sm.GetGrdDec(Grd1, Row, 4)) / TotalBobotKPIDtl;
                        }
                    }
                }
            }
        }

        private decimal ComputeTotalBobotKPIDtl()
        {
            decimal mTotBot = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                        mTotBot += Sm.GetGrdDec(Grd1, i, 4);
                }
            }

            return mTotBot;
        }

        

        private void ComputeTotalAverage()
        {
            decimal total = 0;
            //decimal rowCount = 0;
            decimal avg = 0;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    total = total + Sm.GetGrdDec(Grd1, Row, 10);
                }

            }

            avg = total;
            //rowCount =  Grd1.Rows.Count-1;
            //if(rowCount>0)
            //    avg = total/rowCount;

            if (mMaxTotalAveragePerformanceReview.Length > 0)
            {
                var value = decimal.Parse(mMaxTotalAveragePerformanceReview);
                if (avg > value) TxtTotalAvg.EditValue = Sm.FormatNum(value, 0);
                else 
                    TxtTotalAvg.EditValue = Sm.FormatNum(avg, 0);
            }
            else
                TxtTotalAvg.EditValue = Sm.FormatNum(avg, 0);
            getPerformanceGrade();
        }

        private void getPerformanceGrade()
        {
            string Score = string.Empty;
            decimal Scorer = 0m;
            Scorer = Decimal.Parse(TxtTotalAvg.Text);

            var cm = new MySqlCommand();
            Sm.CmParam<Decimal>(ref cm, "@Scorer", Scorer);

            cm.CommandText =
                    "Select if(@Scorer>(Select max(grdProsentaseEnd)), 'A', " +
                    "if(@Scorer<(Select min(grdProsentaseStart)), " +
                    "(Select grdStatus From Tblperformancegrade  Where GrdProsentaseStart = (Select Min(GrdProsentaseStart) from Tblperformancegrade)), GrdStatus " +
                    ")) As GrdStatus  " +
                    "From tblperformancegrade " +
                    "Where @Scorer between GrdprosentaseStart And GrdprosentaseEnd;";

            Score = Sm.GetValue(cm);
            if(Score.Length == 0)
                Score = Sm.GetValue("Select grdStatus From Tblperformancegrade  Where GrdProsentaseStart = (Select Min(GrdProsentaseStart) from Tblperformancegrade)");

            Sm.SetLue(LuePerformanceGrade, Score);
        }


        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowKPIProcess(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select Concat(group_Concat(Distinct X.DocNo Separator '#'), '#') DocNo, Group_Concat(Distinct X.Dno Separator '#') Dno, Group_Concat(Distinct X.Result) Result, ");
            SQLDtl.AppendLine("Group_Concat(Distinct X.Bobot) Bobot, Group_Concat(Distinct X.Uom) Uom, Group_Concat(Distinct X.Option) option,  ");
            SQLDtl.AppendLine("CAST(Group_Concat(Distinct X.Target)AS DECIMAL(18,4)) target, Group_Concat(Distinct X.type) Type,  ");
            SQLDtl.AppendLine("if(X.Type = 'SUM', SUM(Value), (SUM(Value)/12)) As Value, Group_Concat(Distinct X.ControlInd) ControlInd ");
            SQLDtl.AppendLine("From (  ");
            SQLDtl.AppendLine("    Select Concat(A.DocNO, B.DNo) as DocNO, B.DNo, C.Result, C.Bobot, C.Uom, C.Option, C.Target, C.Type, B.Value, C.ControlInd ");
            SQLDtl.AppendLine("    From TblKPIProcessHdr A  ");
            SQLDtl.AppendLine("    Inner Join TblKPIProcessDtl B On A.DocNo = B.Docno ");
            SQLDtl.AppendLine("    Inner Join TblKPIDtl C On A.KPIDocNo = C.DocNo And B.KPIDno = C.DNo  ");
            SQLDtl.AppendLine("    Where C.DocNo = @DocNo And A.CancelInd = 'N' ");
            SQLDtl.AppendLine(")X ");
            SQLDtl.AppendLine("Group By X.Result order by X.Dno ");

            //SQLDtl.AppendLine("Select A.DocNO, B.DNo, C.Result, C.Bobot, C.Uom, C.Option, C.Target, C.Type, B.Value ");
            //SQLDtl.AppendLine("From TblKPIProcessHdr A ");
            //SQLDtl.AppendLine("Inner Join TblKPIProcessDtl B On A.DocNo = B.Docno");
            //SQLDtl.AppendLine("Inner Join TblKPIDtl C On A.KPIDocNo = C.DocNo And B.KPIDno = C.DNo ");
            //SQLDtl.AppendLine("Where C.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",  

                    //1-5
                    "DNo", "Result", "Bobot", "Uom", "Option", 

                    //6-9
                    "Type", "Target", "Value", "ControlInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                    Grd1.Cells[Row, 10].Value = 0m;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                }, false, false, true, false
            );
            decimal mTotalBobotKPIDtl = ComputeTotalBobotKPIDtl();
            ComputeAverageRow(mTotalBobotKPIDtl);
            ComputeTotalAverage();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 8, 9, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Misc Control Methods

       

        #endregion

        
        #endregion

        #region Event
        private void TxtTaxAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalAvg, 0);
        }

        private void LuePerformanceGrade_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePerformanceGrade, new Sm.RefreshLue1(SetLuePerformanceGrade));
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked == false)
                {
                    string ActiveInd = Sm.GetValue("SELECT A.CancelInd FROM TblPerformanceReviewhdr A WHERE A.DocNo = '" + TxtDocNo.Text + "'");
                    if (ActiveInd == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "Performance Review Is Already Cancelled");
                        ChkCancelInd.Checked = true;
                    }
                }

            }
        }
        #endregion
    }
}
