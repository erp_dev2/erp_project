﻿#region Update
/*
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentItemFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmInvestmentItem mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmInvestmentItemFind(FrmInvestmentItem FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                mFrmParent.SetLueInvestmentCtCode(ref LueItCtCode, string.Empty);
                ChkExcludingInactiveData.Checked = true;
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 35;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Investment's Code", 
                        "Investment's Name",
                        "Foreign Name",
                        "Local Code",
                        "Old Code",
                        
                        //6-10
                        "Active",
                        "Category",
                        "Sub Category",
                        "Group",
                        "HS Code",
                        
                        //11-15
                        "UoM",
                        "Length",
                        "UoM",
                        "Height",
                        "UoM",
                        
                        //16-20
                        "Width",
                        "UoM",
                        "Diameter",
                        "UoM",
                        "Volume",
                        
                        //21-25
                        "UoM",
                        "Inventory",
                        "Sales",
                        "Purchase",
                        "Fixed"+Environment.NewLine+"Asset",
                        
                        //26-30
                        "Planning",
                        "Specification",
                        "Pricing Group",
                        "Created By",
                        "Created Date",
                        
                        //31-34
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 300, 200, 100, 130, 
                        
                        //6-10
                        60, 200, 150, 120, 150, 
                        
                        //11-15
                        100, 80, 100, 80, 100, 

                        //16-20
                        80, 100, 80, 100, 80, 

                        //21-25
                        100, 100, 100, 100, 100, 

                        //26-30
                        100, 200, 200, 130, 130, 
                        
                        //31-34
                        130, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 6, 22, 23, 24, 25, 26 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16, 18, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 30, 33 });
            Sm.GrdFormatTime(Grd1, new int[] { 31, 34 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 28, 29, 30, 31, 32, 33, 34 }, false);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.InvestmentCode, A.InvestmentName, A.ForeignName, A.InvestmentCodeInternal, A.InvestmentCodeOld, A.ActInd, B.InvestmentCtName, ");
            SQL.AppendLine("A.ItScCode, A.ItGrpCode, A.InventoryUomCode, ");
            SQL.AppendLine("A.Length, A.LengthUomCode, A.Height, A.HeightUomCode, A.Width, A.WidthUomCode, ");
            SQL.AppendLine("A.Diameter, A.DiameterUomCode, A.Volume, A.VolumeUomCode, A.HSCode, ");
            SQL.AppendLine("A.InventoryItemInd, A.SalesItemInd, A.PurchaseItemInd, A.FixedItemInd, A.PlanningItemInd, A.Specification, A.PGCode, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblInvestmentItem A ");
            SQL.AppendLine("Inner Join TblInvestmentCategory B On A.InvestmentCtCode=B.InvestmentCtCode ");
            SQL.AppendLine("Where 1=1 ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (mFrmParent.mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                if (ChkExcludingInactiveData.Checked) Filter = " And A.ActInd='Y' ";
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.InvestmentCode", "A.InvestmentName", "A.InvestmentCodeInternal" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.InvestmentCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.InvestmentName;",
                        new string[]
                        {
                            //0
                            "InvestmentCode", 
                                
                            //1-5
                            "InvestmentName", "ForeignName", "InvestmentCodeInternal", "InvestmentCodeOld", "ActInd", 
                            
                            //6-10
                             "InvestmentCtName", "ItScCode", "ItGrpCode", "HSCode",  "InventoryUomCode", 
                            
                            //11-15
                            "Length", "LengthUomCode", "Height",  "HeightUomCode", "Width", 
                            
                            //16-20
                            "WidthUomCode", "Diameter", "DiameterUomCode", "Volume", "VolumeUomCode", 
                            
                            //21-25
                            "InventoryItemInd", "SalesItemInd", "PurchaseItemInd", "FixedItemInd", "PlanningItemInd", 
                            
                            //26-30
                            "Specification", "PGCode", "CreateBy", "CreateDt", "LastUpBy", 
                            
                            //31
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 24, 23);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 25, 24);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 26, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 30, 29);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 31, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 30);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 33, 31);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 34, 31);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Button Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 27].Value = "'" + Sm.GetGrdStr(Grd1, Row, 27);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 27].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 27), Sm.GetGrdStr(Grd1, Row, 27).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(mFrmParent.SetLueInvestmentCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Investment's category");
        }

        #endregion

        #endregion
    }
}
