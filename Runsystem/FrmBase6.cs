﻿#region Update
/*
    29/09/2017 [WED] tambah function print yang bisa di override
    25/02/2019 [TKG] print manager dan check box bisa diakses dari form lain.
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
    20/05/2022 [DITA/PHT] GrdAfterContentSorted dibikin virtual supaya bisa di override
    18/10/2022 [HAR/AMKA] tambah parameter untuk mengaktifkan warning payment
    20/04/2023 [WED/ALL] activity log di simpan berdasarkan parameter IsActivityLogStored
 */
#endregion

#region Namespace

using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBase6 : Form
    {

        #region Constructor

        public FrmBase6()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
            Sm.SetLue(LueFontSize, "9");
        }

        virtual protected void SetSQL()
        {
        }

        virtual protected string SetReportName()
        {
            return "";
        }

        virtual protected void ShowData()
        {

        }

        virtual protected void ShowChart()
        {

        }

        virtual protected void HideInfoInGrd()
        {

        }

        virtual protected void ExportToExcel()
        {
            Sm.ExportToExcel(Grd1);
        }

        virtual protected void PrintData()
        {
            iGSubtotalManager.ForeColor = Color.Black;

            string ReportName = SetReportName();

            if (ReportName.Length == 0)
            {
                var Title = this.Text.Trim();
                int Pos = Title.IndexOf("-");
                if (Pos > 0) ReportName = Sm.Right(Title, Title.Length - Pos - 1).Trim();
            }

            PM1.PageHeader.LeftSection.Text = Environment.NewLine + "Report : " + ReportName;
            Sm.SetPrintManagerProperty(PM1, Grd1, ChkAutoWidth.Checked);
            iGSubtotalManager.ForeColor = Color.White;
        }

        #region Grid Method

        virtual protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {

        }

        virtual protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        virtual protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

        }

        virtual protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.SetGrdAutoSize(Grd1);
        }

        virtual protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
            //if (e.Expanded) Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        virtual protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Sm.SetGrdAutoSize(Grd1);
            }
        }

        virtual protected void GrdAfterContentSorted(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, false);
        }

        #endregion

        #region Event

        #region Form Event

        private void FrmBase6_Load(object sender, EventArgs e)
        {
            BtnChart.Visible = false;
            FrmLoad(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (Sm.GetParameterBoo("WMenu"))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "You can't show this data." + Environment.NewLine +
                    "Please contact finance department.");
            }
            else
            {
                ShowData();

                if (Gv.IsActivityLogStored)
                {
                    //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                    var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                    //string FrmName = o_name.ToString();
                    string MenuCode = o_tag.ToString();
                    string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                    string CtCode = Sm.GetParameter("DocTitle");
                    string DbName = Gv.Database;
                    string DbHost = Gv.Server;
                    string Action = "R";

                    Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
                }
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            PrintData();

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "N";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            //Sm.ExportToExcel(Grd1);
            ExportToExcel();

            if (Gv.IsActivityLogStored)
            {
                //var o_name = (sender as DXE.SimpleButton).Parent.Parent.Name;
                var o_tag = (sender as DXE.SimpleButton).Parent.Parent.Tag;

                //string FrmName = o_name.ToString();
                string MenuCode = o_tag.ToString();
                string MenuDesc = Sm.GetValue("Select MenuDesc From TblMenu Where MenuCode = @Param ", MenuCode);
                string CtCode = Sm.GetParameter("DocTitle");
                string DbName = Gv.Database;
                string DbHost = Gv.Server;
                string Action = "X";

                Sm.SaveLogAction(CtCode, DbName, DbHost, MenuCode, MenuDesc, Action);
            }
        }

        private void BtnChart_Click(object sender, EventArgs e)
        {
            ShowChart();
        }

        #endregion

        #region Misc Control event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            LueFontSizeEditValueChanged(sender, e);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        #endregion

        #region Grid Event

        private void Grd1_AfterContentsGrouped(object sender, EventArgs e)
        {
            GrdAfterContentsGrouped(sender, e);
        }

        private void Grd1_AfterContentsSorted(object sender, EventArgs e)
        {
            GrdAfterContentSorted(sender, e);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdTabInLastCell(Grd1, e, BtnRefresh);
        }

        private void Grd1_AfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
            GrdAfterRowStateChanged(sender, e);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            GrdEllipsisButtonClick(sender, e);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(sender, e);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdColHdrDoubleClick(sender, e);
        }

        #endregion

        #region Print Manager Event

        private void PM1_CustomDrawPageHeader(object sender, TenTec.Windows.iGridLib.Printing.iGCustomDrawPageBandEventArgs e)
        {
            try
            {
                e.Graphics.DrawImage(
                    Image.FromFile(@Sm.CompanyLogo()),
                    //new Rectangle() { Height = 50, Width = Gv.CompanyLogoWidth, X = 100, Y = 50 }
                     new Rectangle() { Height = 50, Width = Gv.CompanyLogoWidth, X = 78, Y = 78 }
                    );

                e.Graphics.DrawString(
                    Gv.CompanyName,
                    new Font("Times New Roman", 12, FontStyle.Regular),
                    SystemBrushes.WindowText,
                    //new Rectangle() { Height = 100, Width = 300, X = 350, Y = 50 });
                    new Rectangle() { Height = 100, Width = 300, X = Gv.CompanyLogoWidth + 85, Y = 90 });
            }
            catch (FileNotFoundException)
            {

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void PM1_CustomDrawPageHeaderGetBounds(object sender, TenTec.Windows.iGridLib.Printing.iGCustomDrawPageBandGetBoundsEventArgs e)
        {
            e.Bounds.Height = 20;
        }

        #endregion

        #endregion
    }
}
