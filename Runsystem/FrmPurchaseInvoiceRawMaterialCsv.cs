﻿#region Update
/*
    10/02/2018 [TKG] New Application 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoiceRawMaterialCsv : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty, mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmPurchaseInvoiceRawMaterialCsv(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetLueDocType(ref LueDocType);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.DocNo, B.DocNo As PurchaseInvoiceDocNo, ");
            SQL.AppendLine("E.TIN, Case B.DocType When '1' Then 'Log' When '2' Then 'Balok' End As DocTypeDesc, ");
            SQL.AppendLine("E.VdName, F.CityName, B.Amt, B.Tax ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("Inner Join TblRawMaterialVerify C On B.RawMaterialVerifyDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblVendor E On D.VdCode=E.VdCode ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocType='11' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Month",
                        "Year",
                        "Voucher#",
                        "",
                        
                        //6-10
                        "Voucher"+Environment.NewLine+"Date",
                        "PI#",
                        "",
                        "Taxpayer" +Environment.NewLine+"Identification#",
                        "Type",
                        
                        //11-14
                        "Vendor",
                        "City",
                        "Amount",
                        "Tax"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 50, 50, 110, 20, 
                        
                        //6-10
                        100, 130, 20, 180, 100,

                        //11-14
                        200, 200, 130, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 5, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 6, 7, 9, 10, 11, 12, 13, 14 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        #region Standard Method

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();
                var DocDt = string.Empty;
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "B.DocType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocDt", 
                            
                            //1-5
                            "DocNo", "PurchaseInvoiceDocNo", "TIN", "DocTypeDesc", "VdName", 
                            
                            //6-8
                            "CityName", "Amt", "Tax"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = true;
                            DocDt = dr.GetString(c[0]);
                            Grd.Cells[Row, 2].Value = DocDt.Substring(4, 2);
                            Grd.Cells[Row, 3].Value = Sm.Left(DocDt, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Grd.Cells[Row, 6].Value = Sm.ConvertDate(DocDt);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 8);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 14 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            if (
                Sm.StdMsgYN("Question", "Do you want to generate csv file ?") == DialogResult.No ||
                IsDataNotValid()
                ) 
                return;


            var l = new List<Csv>();
            try
            {
                var sf = new SaveFileDialog();
                sf.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
                sf.FileName = "RawMaterial";

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    for (int r=0;r< Grd1.Rows.Count;r++)
                    {
                        if (Sm.GetGrdBool(Grd1, r, 1))
                        {
                            l.Add(new Csv()
                            {
                                Mth = Sm.GetGrdStr(Grd1, r, 2),
                                Yr = Sm.GetGrdStr(Grd1, r, 3),
                                TIN = Sm.GetGrdStr(Grd1, r, 9).Length>0 ? Sm.GetGrdStr(Grd1, r, 9) : "000000000000000",
                                VdName = Sm.GetGrdStr(Grd1, r, 11),
                                CityName = Sm.GetGrdStr(Grd1, r, 12),
                                DocNo = Sm.GetGrdStr(Grd1, r, 4),
                                DocDt = Sm.GetGrdDate(Grd1, r, 6),
                                Amt = Sm.GetGrdDec(Grd1, r, 13),
                                Tax = Sm.GetGrdDec(Grd1, r, 14),
                            });
                        }
                    }
                    if (l.Count > 0)
                    {
                        for (int i = 0; i < l.Count; i++)
                        {
                            l[i].TIN = l[i].TIN.Replace(".", string.Empty).Replace("-", string.Empty).Replace(",", string.Empty);
                            l[i].DocNo = string.Concat("00", l[i].DocNo.Substring(6, 4), "/PPH22");
                            l[i].DocDt = string.Concat(
                                l[i].DocDt.Substring(6, 2), 
                                "/", 
                                l[i].DocDt.Substring(4, 2), 
                                "/", 
                                Sm.Left(l[i].DocDt, 4)
                                );
                            if (l[i].VdName.Length > 0)
                                l[i].VdName = l[i].VdName.Replace(",", string.Empty).Replace(";", string.Empty);
                            if (l[i].CityName.Length > 0)
                                l[i].CityName = l[i].CityName.Replace(",", string.Empty).Replace(";", string.Empty);
                            l[i].Amt = decimal.Truncate(l[i].Amt);
                            l[i].Tax = decimal.Truncate(l[i].Tax);
                        }
                        CreateCSVFile(ref l, sf.FileName);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            l.Clear();
        }

        #endregion

        #region Additional Method

        private void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Log' As Col2 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Balok' As Col2;");
            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsDataNotValid()
        {
            return
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsChosen = false;
            for (int r = 0; r< Grd1.Rows.Count - 1; r++)
            {
                if (!IsChosen && Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length > 0) IsChosen = true;
                if (Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length>0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Month is empty.")) return true;
                }
            }
            if (!IsChosen)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        private void CreateCSVFile(ref List<Csv> l, string FileName)
        {
            bool IsFirst = true;
            var sb = new StringBuilder();
            StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1252));
            
            foreach (var x in l)
            {
                if (IsFirst)
                {
                    IsFirst = false;
                }
                else
                {
                    sb.Length = 0;
                    sb.Capacity = 0;
                    sw.Write(sw.NewLine);
                }
                sb.Append("F113304;");
                sb.Append(x.Mth);
                sb.Append(";");
                sb.Append(x.Yr);
                sb.Append(";0;");
                sb.Append(x.TIN);
                sb.Append(";");
                sb.Append(x.VdName);
                sb.Append(";");
                sb.Append(x.CityName);
                sb.Append(";");
                sb.Append(x.DocNo);
                sb.Append(";");
                sb.Append(x.DocDt);
                sb.Append(";0;0.25;0;0;0.1;0;0;0.3;0;0;0.45;0;PENGOLAHAN KAYU;");
                sb.Append((x.Amt.ToString()).Replace(",", "."));
                sb.Append(";0.25;");
                sb.Append((x.Tax.ToString()).Replace(",", "."));
                sb.Append(";;0;0;0;;0;0;0;;0;1.5;0;;0;1.5;0;;0;0;0;;0;0;0;");
                sb.Append((x.Amt.ToString()).Replace(",", "."));
                sb.Append(";");
                sb.Append((x.Tax.ToString()).Replace(",", "."));
                sw.Write(sb.ToString());
            }
            sw.Close();
            System.Diagnostics.Process.Start(FileName);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
                e.DoDefault = false;

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher("X");
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoiceRawMaterial("X");
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmVoucher("X");
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmPurchaseInvoiceRawMaterial("X");
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 2).Length>0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        #endregion

        #endregion

        #region Class

        private class Csv
        {
            public string Mth { get; set; }
            public string Yr { get; set; }
            public bool TaxInd { get; set; }
            public string TIN { get; set; }
            public string VdName { get; set; }
            public string CityName { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Tax { get; set; }
        }

        #endregion
    }
}
