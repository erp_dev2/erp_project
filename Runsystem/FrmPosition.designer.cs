﻿namespace RunSystem
{
    partial class FrmPosition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtPosName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPosCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkRequestItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.LueGrdLvlCodeMin = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueGrdLvlCodeMax = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtLevel = new DevExpress.XtraEditors.TextEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ChkActingOfficial = new DevExpress.XtraEditors.CheckEdit();
            this.PnlActivity = new System.Windows.Forms.Panel();
            this.ChkCashAdvancePIC = new DevExpress.XtraEditors.CheckEdit();
            this.LueActivityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTeritoryCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtPercentage = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TcPosition = new System.Windows.Forms.TabControl();
            this.Tp1 = new System.Windows.Forms.TabPage();
            this.LueTrainingCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp2 = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRequestItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCodeMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCodeMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficial.Properties)).BeginInit();
            this.PnlActivity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCashAdvancePIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueActivityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTeritoryCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPercentage.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.TcPosition.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 406);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.TxtLevel);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LuePosCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueGrdLvlCodeMax);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.LueGrdLvlCodeMin);
            this.panel2.Controls.Add(this.ChkRequestItemInd);
            this.panel2.Controls.Add(this.TxtPosName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtPosCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 406);
            // 
            // TxtPosName
            // 
            this.TxtPosName.EnterMoveNextControl = true;
            this.TxtPosName.Location = new System.Drawing.Point(138, 32);
            this.TxtPosName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosName.Name = "TxtPosName";
            this.TxtPosName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPosName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosName.Properties.Appearance.Options.UseFont = true;
            this.TxtPosName.Properties.MaxLength = 100;
            this.TxtPosName.Size = new System.Drawing.Size(319, 20);
            this.TxtPosName.TabIndex = 12;
            this.TxtPosName.Validated += new System.EventHandler(this.TxtPosName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(47, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Position Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosCode
            // 
            this.TxtPosCode.EnterMoveNextControl = true;
            this.TxtPosCode.Location = new System.Drawing.Point(138, 10);
            this.TxtPosCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosCode.Name = "TxtPosCode";
            this.TxtPosCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPosCode.Properties.MaxLength = 16;
            this.TxtPosCode.Size = new System.Drawing.Size(76, 20);
            this.TxtPosCode.TabIndex = 10;
            this.TxtPosCode.Validated += new System.EventHandler(this.TxtPosCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(50, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Position Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkRequestItemInd
            // 
            this.ChkRequestItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkRequestItemInd.Location = new System.Drawing.Point(138, 142);
            this.ChkRequestItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkRequestItemInd.Name = "ChkRequestItemInd";
            this.ChkRequestItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkRequestItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRequestItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkRequestItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkRequestItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkRequestItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkRequestItemInd.Properties.Caption = "Able Request Item";
            this.ChkRequestItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkRequestItemInd.Size = new System.Drawing.Size(134, 22);
            this.ChkRequestItemInd.TabIndex = 21;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(6, 123);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(125, 14);
            this.label47.TabIndex = 19;
            this.label47.Text = "Maximum Grade Level";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGrdLvlCodeMin
            // 
            this.LueGrdLvlCodeMin.EnterMoveNextControl = true;
            this.LueGrdLvlCodeMin.Location = new System.Drawing.Point(138, 98);
            this.LueGrdLvlCodeMin.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrdLvlCodeMin.Name = "LueGrdLvlCodeMin";
            this.LueGrdLvlCodeMin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMin.Properties.Appearance.Options.UseFont = true;
            this.LueGrdLvlCodeMin.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMin.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdLvlCodeMin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdLvlCodeMin.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMin.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdLvlCodeMin.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMin.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdLvlCodeMin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdLvlCodeMin.Properties.DropDownRows = 25;
            this.LueGrdLvlCodeMin.Properties.NullText = "[Empty]";
            this.LueGrdLvlCodeMin.Properties.PopupWidth = 500;
            this.LueGrdLvlCodeMin.Size = new System.Drawing.Size(319, 20);
            this.LueGrdLvlCodeMin.TabIndex = 18;
            this.LueGrdLvlCodeMin.ToolTip = "F4 : Show/hide list";
            this.LueGrdLvlCodeMin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrdLvlCodeMin.EditValueChanged += new System.EventHandler(this.LueGrdLvlCodeMin_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(9, 101);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Minimum Grade Level";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGrdLvlCodeMax
            // 
            this.LueGrdLvlCodeMax.EnterMoveNextControl = true;
            this.LueGrdLvlCodeMax.Location = new System.Drawing.Point(138, 120);
            this.LueGrdLvlCodeMax.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrdLvlCodeMax.Name = "LueGrdLvlCodeMax";
            this.LueGrdLvlCodeMax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMax.Properties.Appearance.Options.UseFont = true;
            this.LueGrdLvlCodeMax.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMax.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdLvlCodeMax.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMax.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdLvlCodeMax.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMax.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdLvlCodeMax.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCodeMax.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdLvlCodeMax.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdLvlCodeMax.Properties.DropDownRows = 25;
            this.LueGrdLvlCodeMax.Properties.NullText = "[Empty]";
            this.LueGrdLvlCodeMax.Properties.PopupWidth = 500;
            this.LueGrdLvlCodeMax.Size = new System.Drawing.Size(319, 20);
            this.LueGrdLvlCodeMax.TabIndex = 20;
            this.LueGrdLvlCodeMax.ToolTip = "F4 : Show/hide list";
            this.LueGrdLvlCodeMax.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrdLvlCodeMax.EditValueChanged += new System.EventHandler(this.LueGrdLvlCodeMax_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(96, 56);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Level";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(88, 79);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 14);
            this.label5.TabIndex = 15;
            this.label5.Text = "Parent";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(138, 76);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 25;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 500;
            this.LuePosCode.Size = new System.Drawing.Size(319, 20);
            this.LuePosCode.TabIndex = 16;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            // 
            // TxtLevel
            // 
            this.TxtLevel.EnterMoveNextControl = true;
            this.TxtLevel.Location = new System.Drawing.Point(138, 54);
            this.TxtLevel.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevel.Name = "TxtLevel";
            this.TxtLevel.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevel.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevel.Properties.Appearance.Options.UseFont = true;
            this.TxtLevel.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLevel.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLevel.Size = new System.Drawing.Size(76, 20);
            this.TxtLevel.TabIndex = 14;
            this.TxtLevel.Validated += new System.EventHandler(this.TxtLevel_Validated);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ChkActingOfficial);
            this.panel3.Controls.Add(this.PnlActivity);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 165);
            this.panel3.TabIndex = 22;
            // 
            // ChkActingOfficial
            // 
            this.ChkActingOfficial.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActingOfficial.Location = new System.Drawing.Point(280, 142);
            this.ChkActingOfficial.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActingOfficial.Name = "ChkActingOfficial";
            this.ChkActingOfficial.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActingOfficial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActingOfficial.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActingOfficial.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActingOfficial.Properties.Appearance.Options.UseFont = true;
            this.ChkActingOfficial.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActingOfficial.Properties.Caption = "Acting  Official (PLT)";
            this.ChkActingOfficial.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActingOfficial.Size = new System.Drawing.Size(175, 22);
            this.ChkActingOfficial.TabIndex = 22;
            // 
            // PnlActivity
            // 
            this.PnlActivity.Controls.Add(this.ChkCashAdvancePIC);
            this.PnlActivity.Controls.Add(this.LueActivityCode);
            this.PnlActivity.Controls.Add(this.LueTeritoryCode);
            this.PnlActivity.Controls.Add(this.label8);
            this.PnlActivity.Controls.Add(this.label6);
            this.PnlActivity.Controls.Add(this.TxtPercentage);
            this.PnlActivity.Controls.Add(this.label7);
            this.PnlActivity.Dock = System.Windows.Forms.DockStyle.Right;
            this.PnlActivity.Location = new System.Drawing.Point(465, 0);
            this.PnlActivity.Name = "PnlActivity";
            this.PnlActivity.Size = new System.Drawing.Size(307, 165);
            this.PnlActivity.TabIndex = 29;
            // 
            // ChkCashAdvancePIC
            // 
            this.ChkCashAdvancePIC.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCashAdvancePIC.Location = new System.Drawing.Point(77, 76);
            this.ChkCashAdvancePIC.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCashAdvancePIC.Name = "ChkCashAdvancePIC";
            this.ChkCashAdvancePIC.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCashAdvancePIC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCashAdvancePIC.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCashAdvancePIC.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCashAdvancePIC.Properties.Appearance.Options.UseFont = true;
            this.ChkCashAdvancePIC.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCashAdvancePIC.Properties.Caption = "Cash Adv. PIC";
            this.ChkCashAdvancePIC.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCashAdvancePIC.Size = new System.Drawing.Size(134, 22);
            this.ChkCashAdvancePIC.TabIndex = 29;
            // 
            // LueActivityCode
            // 
            this.LueActivityCode.EnterMoveNextControl = true;
            this.LueActivityCode.Location = new System.Drawing.Point(79, 9);
            this.LueActivityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueActivityCode.Name = "LueActivityCode";
            this.LueActivityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueActivityCode.Properties.Appearance.Options.UseFont = true;
            this.LueActivityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueActivityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueActivityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueActivityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueActivityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueActivityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueActivityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueActivityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueActivityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueActivityCode.Properties.DropDownRows = 25;
            this.LueActivityCode.Properties.NullText = "[Empty]";
            this.LueActivityCode.Properties.PopupWidth = 500;
            this.LueActivityCode.Size = new System.Drawing.Size(221, 20);
            this.LueActivityCode.TabIndex = 24;
            this.LueActivityCode.ToolTip = "F4 : Show/hide list";
            this.LueActivityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueActivityCode.EditValueChanged += new System.EventHandler(this.LueActivityCode_EditValueChanged);
            // 
            // LueTeritoryCode
            // 
            this.LueTeritoryCode.EnterMoveNextControl = true;
            this.LueTeritoryCode.Location = new System.Drawing.Point(79, 53);
            this.LueTeritoryCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTeritoryCode.Name = "LueTeritoryCode";
            this.LueTeritoryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTeritoryCode.Properties.Appearance.Options.UseFont = true;
            this.LueTeritoryCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTeritoryCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTeritoryCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTeritoryCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTeritoryCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTeritoryCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTeritoryCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTeritoryCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTeritoryCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTeritoryCode.Properties.DropDownRows = 25;
            this.LueTeritoryCode.Properties.NullText = "[Empty]";
            this.LueTeritoryCode.Properties.PopupWidth = 500;
            this.LueTeritoryCode.Size = new System.Drawing.Size(221, 20);
            this.LueTeritoryCode.TabIndex = 28;
            this.LueTeritoryCode.ToolTip = "F4 : Show/hide list";
            this.LueTeritoryCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTeritoryCode.EditValueChanged += new System.EventHandler(this.LueTeritoryCode_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(24, 56);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 14);
            this.label8.TabIndex = 27;
            this.label8.Text = "Teritory";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(27, 12);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "Activity";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPercentage
            // 
            this.TxtPercentage.EnterMoveNextControl = true;
            this.TxtPercentage.Location = new System.Drawing.Point(79, 31);
            this.TxtPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPercentage.Name = "TxtPercentage";
            this.TxtPercentage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPercentage.Size = new System.Drawing.Size(76, 20);
            this.TxtPercentage.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(4, 34);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Percentage";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.TcPosition);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 165);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 241);
            this.panel4.TabIndex = 23;
            // 
            // TcPosition
            // 
            this.TcPosition.Controls.Add(this.Tp1);
            this.TcPosition.Controls.Add(this.Tp2);
            this.TcPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcPosition.Location = new System.Drawing.Point(0, 0);
            this.TcPosition.Name = "TcPosition";
            this.TcPosition.SelectedIndex = 0;
            this.TcPosition.Size = new System.Drawing.Size(772, 241);
            this.TcPosition.TabIndex = 31;
            // 
            // Tp1
            // 
            this.Tp1.Controls.Add(this.LueTrainingCode);
            this.Tp1.Controls.Add(this.Grd1);
            this.Tp1.Location = new System.Drawing.Point(4, 23);
            this.Tp1.Name = "Tp1";
            this.Tp1.Padding = new System.Windows.Forms.Padding(3);
            this.Tp1.Size = new System.Drawing.Size(764, 214);
            this.Tp1.TabIndex = 0;
            this.Tp1.Text = "List of Training";
            this.Tp1.UseVisualStyleBackColor = true;
            // 
            // LueTrainingCode
            // 
            this.LueTrainingCode.EnterMoveNextControl = true;
            this.LueTrainingCode.Location = new System.Drawing.Point(67, 36);
            this.LueTrainingCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingCode.Name = "LueTrainingCode";
            this.LueTrainingCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingCode.Properties.DropDownRows = 20;
            this.LueTrainingCode.Properties.NullText = "[Empty]";
            this.LueTrainingCode.Properties.PopupWidth = 500;
            this.LueTrainingCode.Size = new System.Drawing.Size(143, 20);
            this.LueTrainingCode.TabIndex = 30;
            this.LueTrainingCode.ToolTip = "F4 : Show/hide list";
            this.LueTrainingCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingCode.EditValueChanged += new System.EventHandler(this.LueTrainingCode_EditValueChanged);
            this.LueTrainingCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTrainingCode_KeyDown);
            this.LueTrainingCode.Leave += new System.EventHandler(this.LueTrainingCode_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(3, 3);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(758, 208);
            this.Grd1.TabIndex = 29;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // Tp2
            // 
            this.Tp2.Controls.Add(this.Grd2);
            this.Tp2.Location = new System.Drawing.Point(4, 23);
            this.Tp2.Name = "Tp2";
            this.Tp2.Padding = new System.Windows.Forms.Padding(3);
            this.Tp2.Size = new System.Drawing.Size(764, 41);
            this.Tp2.TabIndex = 1;
            this.Tp2.Text = "Level";
            this.Tp2.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(3, 3);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(758, 35);
            this.Grd2.TabIndex = 13;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // FrmPosition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 406);
            this.Name = "FrmPosition";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRequestItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCodeMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCodeMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevel.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficial.Properties)).EndInit();
            this.PnlActivity.ResumeLayout(false);
            this.PnlActivity.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCashAdvancePIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueActivityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTeritoryCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPercentage.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.TcPosition.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtPosName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtPosCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkRequestItemInd;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueGrdLvlCodeMax;
        private System.Windows.Forms.Label label47;
        private DevExpress.XtraEditors.LookUpEdit LueGrdLvlCodeMin;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtLevel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingCode;
        private DevExpress.XtraEditors.CheckEdit ChkActingOfficial;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueTeritoryCode;
        internal DevExpress.XtraEditors.TextEdit TxtPercentage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueActivityCode;
        private System.Windows.Forms.Panel PnlActivity;
        private System.Windows.Forms.TabControl TcPosition;
        private System.Windows.Forms.TabPage Tp1;
        private System.Windows.Forms.TabPage Tp2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.CheckEdit ChkCashAdvancePIC;
    }
}