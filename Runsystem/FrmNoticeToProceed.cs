﻿#region Update
/*
    20/09/2019 [DITA/IMS] Aplikasi baru
    24/09/2019 [WED/IMS] BOQ2 diubah ke BOQ untuk IMS
    26/09/2019 [WED/IMS] BOQ yang sudah di MR kan, tidak boleh di cancel
    20/11/2019 [DITA/IMS] ubah query di IsBOQNotProceed() dan IsBOQAlreadyProceedToMR()
    03/12/2019 [DITA+VIN/IMS] Printout SPMK
    19/01/2020 [TKG/IMS] Tambah memo
    07/02/2020 [TKG/IMS] Tambah informasi dari project group
    20/03/2020 [TKG/IMS] tambah informasi so contract#
    13/05/2020 [VIN/IMS] Upload multiple attachment
    10/07/2020 [DITA+ICA/IMS] Ubah Printout SPMK
    01/12/2020 [ICA/IMS] Feedback printout. Mengambil No Document dari Memo dan Bagian Approval 
    24/12/2021 [RDA/IMS] Tambah gambar tanda tangan printout
    
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;
using FastReport;
using FastReport.Data;
using System.Collections;


#endregion

namespace RunSystem
{
    public partial class FrmNoticeToProceed : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mCtCode = string.Empty;
        private string mNTPAutoApprovedByEmpCode = string.Empty,
            mPGCode = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;
        internal bool
            mIsNTPAllowToUploadFile = false;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;

        internal FrmNoticeToProceedFind FrmFind;



        #endregion

        #region Constructor

        public FrmNoticeToProceed(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
              
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, MeeFrom, MeeTo, 
                        MeeAbout, MeeContent, TxtMemo, TxtFile, TxtFile2, TxtFile3
                    }, true);
                   
                    BtnPGCode.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsNTPAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsNTPAllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsNTPAllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    TxtDocNo.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    BtnPGCode.Enabled = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeFrom, MeeTo, MeeAbout, MeeContent,
                        TxtMemo
                    }, false);
                    if (mIsNTPAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;

                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mPGCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, MeeFrom, 
               MeeTo, MeeAbout, MeeContent, TxtMemo, TxtProjectCode, 
               TxtProjectName, TxtCtCode, TxtSOContractDocNo, TxtFile, TxtFile2, 
               TxtFile3
            });
            ChkCancelInd.Checked = false;
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmNoticeToProceedFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint();
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {

            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {

            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()
               ) return;


            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "NTP", "TblNoticeToProceed");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveNoticeToProceed(DocNo));

            Sm.ExecCommands(cml);

            if (mIsNTPAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (mIsNTPAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (mIsNTPAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtProjectCode, "Project's Code", false) ||
                Sm.IsMeeEmpty(MeeFrom, "From") ||
                Sm.IsMeeEmpty(MeeTo, "To") ||
                Sm.IsMeeEmpty(MeeAbout, "About") ||
                Sm.IsMeeEmpty(MeeContent, "Content") ||
                (mIsNTPAllowToUploadFile && IsUploadFileNotValid());
                
        }

        private MySqlCommand SaveNoticeToProceed(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblNoticeToProceed(DocNo, DocDt, Status, CancelInd, PGCode, NtpFrom, NtpTo, NtpAbout, NtpContent, Memo, FileName, FileName2, FileName3, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'N', @PGCode, @NtpFrom, @NtpTo, @NtpAbout, @NtpContent, @Memo, @FileName, @FileName2, @FileName3, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='NTP'; ");

            SQL.AppendLine("Update TblNoticeToProceed Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='NTP' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@NtpFrom", MeeFrom.Text);
            Sm.CmParam<String>(ref cm, "@NtpTo", MeeTo.Text);
            Sm.CmParam<String>(ref cm, "@NtpAbout", MeeAbout.Text);
            Sm.CmParam<String>(ref cm, "@NtpContent", MeeContent.Text);
            Sm.CmParam<String>(ref cm, "@Memo", TxtMemo.Text);
            Sm.CmParam<String>(ref cm, "@FileName", TxtFile.Text);
            Sm.CmParam<String>(ref cm, "@FileName2", TxtFile2.Text);
            Sm.CmParam<String>(ref cm, "@FileName3", TxtFile3.Text);

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateNTPFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblNoticeToProceed Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateNTPFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblNoticeToProceed Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateNTPFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblNoticeToProceed Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }


        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataInvalid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateNoticeToProceed());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataInvalid()
        {
            return
                IsNoDataCancelled() ||
                IsDataAlreadyCancelled() 
                // || IsBOQAlreadyProceedToSOC() || IsBOQAlreadyProceedToMR()
                ;
        }

        private bool IsNoDataCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblNoticeToProceed ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }

            return false;
        }

        //private bool IsBOQAlreadyProceedToSOC()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select A.DocNo ");
        //    SQL.AppendLine("From TblSOContractHdr A ");
        //    SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
        //    SQL.AppendLine("Inner Join TblLOPHdr C On B.LOPDocNo = C.DocNo And C.DocNo = @Param ");
        //    SQL.AppendLine("Where A.CancelInd = 'N' ");
        //    SQL.AppendLine("Limit 1; ");

        //    if (Sm.IsDataExist(SQL.ToString(), TxtProjectCode.Text))
        //    {
        //        Sm.StdMsg(mMsgType.Warning, "This BOQ data is already proceed to SO Contract #" + Sm.GetValue(SQL.ToString(), TxtProjectCode.Text));
        //        return true;
        //    }            

        //    return false;
        //}

        //private bool IsBOQAlreadyProceedToMR()
        //{
        //    var SQL = new StringBuilder();

        //     SQL.AppendLine("Select A.DocNo ");
        //     SQL.AppendLine("From TblMaterialRequestDtl A ");
        //     SQL.AppendLine("Inner Join TblBOMRevisionDtl B ON A.BOMRDocNo = B.DocNo And A.BOMRDNo = B.DNo ");
        //     SQL.AppendLine("    And A.CancelInd = 'N' And A.Status In ('O', 'A') ");
        //     SQL.AppendLine("Inner Join TblBOMRevisionHdr C On B.DocNo = C.DocNo ");
        //     SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo And D.LOPDocNo = @Param ");
        //     SQL.AppendLine("Limit 1; ");

        //    if (Sm.IsDataExist(SQL.ToString(), TxtProjectCode.Text))
        //    {
        //        Sm.StdMsg(mMsgType.Warning, "This BOQ data already proceed to MR#" + Sm.GetValue(SQL.ToString(), TxtProjectCode.Text));
        //    }
        //    return false;
        //}

        private MySqlCommand UpdateNoticeToProceed()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblNoticeToProceed Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowNoticeToProceed(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowNoticeToProceed(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Status, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("A.NtpFrom, A.NtpTo, A.NtpAbout, A.NtpContent, A.Memo, ");
            SQL.AppendLine("A.PGCode, B.ProjectCode, B.ProjectName, C.CtName,  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Group_Concat(Distinct DocNo Order By DocNo Separator ', ') As SOContractDocNo ");
            SQL.AppendLine("    From TblSOContractHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Status In ('O', 'A') ");
            SQL.AppendLine("    And NTPDocNo=A.DocNo ");
            SQL.AppendLine(") As SOContractDocNo,  A.fileName, A.filename2, A.filename3");
            SQL.AppendLine("From TblNoticeToproceed A ");
            SQL.AppendLine("Left Join TblProjectGroup B ON A.PGCode=B.PGCode ");
            SQL.AppendLine("Left Join TblCustomer C ON B.CtCode=C.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "Status", "CancelReason", "CancelInd", "NtpFrom", 
                    
                    //6-10
                    "NtpTo", "NtpAbout", "NtpContent", "Memo", "ProjectCode", 
                    
                    //11-15
                    "ProjectName", "CtName", "SOContractDocNo", "FileName", "FileName2",

                    //16
                    "FileName3"

                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    switch (Sm.DrStr(dr, c[2]))
                    {
                        case "A":
                            TxtStatus.EditValue = "Approved";
                            break;
                        case "C":
                            TxtStatus.EditValue = "Cancelled";
                            break;
                        case "O":
                            TxtStatus.EditValue = "Outstanding";
                            break;
                    }
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    MeeFrom.EditValue = Sm.DrStr(dr, c[5]);
                    MeeTo.EditValue = Sm.DrStr(dr, c[6]);
                    MeeAbout.EditValue = Sm.DrStr(dr, c[7]);
                    MeeContent.EditValue = Sm.DrStr(dr, c[8]);
                    TxtMemo.EditValue = Sm.DrStr(dr, c[9]);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[10]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[11]);
                    TxtCtCode.EditValue = Sm.DrStr(dr, c[12]);
                    TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[13]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[14]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[15]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[16]);
                }, true
            );
        }

        #endregion

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mNTPAutoApprovedByEmpCode = Sm.GetParameter("NTPAutoApprovedByEmpCode");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsNTPAllowToUploadFile = Sm.GetParameterBoo("IsNTPAllowToUploadFile");

        }

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<NTP>();
            var lDtlS = new List<Sign>();

            string[] TableName = { "NTP", "Sign" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
            //  DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region NTP

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo, @CompanyFooterImage As CompanyFooterImage, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle7') As 'CompanyWebsite', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle8') As 'CompanyEmail', ");
            SQL.AppendLine(" DocNo, DATE_FORMAT(DocDt,'%d %M %Y')As DocDt, NtpFrom, ");
            SQL.AppendLine(" NtpTo, NtpAbout, NtpContent, Memo ");
            SQL.AppendLine(" From TblNoticeToProceed    ");
            SQL.AppendLine(" Where DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@CompanyFooterImage", "FooterImage.png");
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                       //0
                      "CompanyLogo",

                       //1-5
                       "CompanyName",
                       "CompanyAddress",
                       "CompanyAddressCity",
                       "CompanyPhone",
                       "CompanyFax",

                       //6-10
                       "DocNo",
                       "DocDt",
                       "NtpFrom",
                       "NtpTo",
                       "NtpAbout",

                       //11-15
                       "NtpContent",
                       "CompanyFooterImage",
                       "CompanyWebsite",
                       "CompanyEmail",
                       "Memo"
                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTP()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyLongAddress = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            NtpFrom = Sm.DrStr(dr, c[8]),
                            NtpTo = Sm.DrStr(dr, c[9]),
                            NtpAbout = Sm.DrStr(dr, c[10]),

                            NtpContent = Sm.DrStr(dr, c[11]),
                            CompanyFooterImage = Sm.DrStr(dr, c[12]),
                            CompanyWebsite = Sm.DrStr(dr, c[13]),
                            CompanyEmail = Sm.DrStr(dr, c[14]),
                            Memo = Sm.DrStr(dr, c[15])
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region Detail Signature 
           
            //Disetujui Oleh
            var cmDtlS = new MySqlCommand();

            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                //SQLDtlS.AppendLine("Select A.EmpName, B.PosName  ");
                //SQLDtlS.AppendLine("From TblEmployee A  ");
                //SQLDtlS.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode  ");
                //SQLDtlS.AppendLine("Where A.EmpCode=@EmpCode  ");

                SQLDtlS.AppendLine("SELECT C.EmpName, D.PosName, Concat(IfNull(G.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
                SQLDtlS.AppendLine("FROM TblDocApproval A ");
                SQLDtlS.AppendLine("INNER JOIN TblDocApprovalSetting B On A.DocType = B.DocType ");
                SQLDtlS.AppendLine("    AND A.DocNo = @DocNo ");
                SQLDtlS.AppendLine("    AND A.Status = 'A' ");
                SQLDtlS.AppendLine("    AND B.DNo = A.ApprovalDNo ");
                SQLDtlS.AppendLine("    AND B.Level = (");
                SQLDtlS.AppendLine("        SELECT MAX(Level) ");
                SQLDtlS.AppendLine("        FROM TblDocApprovalSetting X1 ");
                SQLDtlS.AppendLine("        Inner Join TblDocApproval X2 On X1.DocType = X2.DocType ");
                SQLDtlS.AppendLine("            And X1.DNo = X2.ApprovalDNo ");
                SQLDtlS.AppendLine("            Where X2.DocNo = @DocNo ");
                SQLDtlS.AppendLine("        )");
                SQLDtlS.AppendLine("INNER JOIN tblemployee C ON A.UserCode = C.UserCode ");
                SQLDtlS.AppendLine("LEFT JOIN tblPosition D ON C.PosCode = D.PosCode ");
                SQLDtlS.AppendLine("LEFT JOIN TblParameter G ON G.ParCode = 'ImgFileSignature' ");

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                //Sm.CmParam<String>(ref cmDtlS, "@EmpCode", mNTPAutoApprovedByEmpCode);
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1-2
                         "PosName",
                         "EmpPict"
                        });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {

                        lDtlS.Add(new Sign()
                        {
                            EmpName = Sm.DrStr(drDtlS, cDtlS[0]),
                            PosName = Sm.DrStr(drDtlS, cDtlS[1]),
                            EmpPict = Sm.DrStr(drDtlS, cDtlS[2]),
                        });
                    }
                }
                drDtlS.Close();
            }
            myLists.Add(lDtlS);


            #endregion

            #region Approval



            #endregion

            Sm.PrintReport("NTPMemo", myLists, TableName, false);

        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateNTPFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateNTPFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateNTPFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsNTPAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsNTPAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsNTPAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsNTPAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsNTPAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsNTPAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsNTPAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsNTPAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsNTPAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsNTPAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsNTPAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsNTPAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsNTPAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsNTPAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsNTPAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsNTPAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblNoticeToProceed ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (mIsNTPAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblNoticeToProceed ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsNTPAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblNoticeToProceed ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }



        #endregion 

        #region Event

        #region Button Click

        private void BtnPGCode_Click(object sender, EventArgs e)
        {
            var f = new FrmProjectGroupStdDlg();
            f.TopLevel = true;
            f.ShowDialog();
            if (f.mPGCode.Length > 0)
            {
                mPGCode = f.mPGCode;
                TxtProjectCode.EditValue = f.mProjectCode;
                TxtProjectName.EditValue = f.mProjectName;
                TxtCtCode.EditValue = f.mCtName;
            }
            else
            {
                mPGCode = string.Empty;
                TxtProjectCode.EditValue = null;
                TxtProjectName.EditValue = null;
                TxtCtCode.EditValue = null;
            }
            f.Close();
        }

        #endregion

        #region Misc Control Event

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtMemo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMemo);
        }

        #endregion

        #endregion

        #region Class

        private class NTP
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string CompanyEmail { get; set; }
            public string CompanyWebsite { get; set; }
            public string CompanyFooterImage { get; set; }
            public string NtpFrom { get; set; }
            public string NtpTo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string NtpContent { get; set; }
            public string NtpAbout { get; set; }
            public string Memo { get; set; }

        }

        private class Sign
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
            public string EmpPict { get; set; }
        }

        #endregion

        
        
    }
}
