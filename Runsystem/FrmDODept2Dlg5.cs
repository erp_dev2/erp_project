﻿#region Update
/*
    01/06/2021 [DITA/IOK] New apps
    02/08/2021 [DITA/IOK] bug: saat memilih wor baru detail item di grid tidak terhapus
    25/11/2021 [TYO/IOK] Menampilkan data yg status = 'Open'
    01/12/2021 [MYA/IOK] Pada menu DO to Departement Without DO Request (01050305), Asset Name, Display Name otomatis muncul jika input WOR
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDODept2Dlg5 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDODept2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDODept2Dlg5(FrmDODept2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.DownDt, IfNull(Concat(Left(A.DownTm, 2), ':', Right(A.DownTm, 2)), '') DownTm, ");
            SQL.AppendLine("Case A.WOStatus When 'C' Then 'Closed' When 'O' Then 'Open' Else '' End As WOStatus, ");
            SQL.AppendLine("D.OptDesc As MtcStatusDesc, E.OptDesc As MtcTypeDesc, F.OptDesc As SymptomProblemDesc,  ");
            SQL.AppendLine("A.Description, B.AssetName, C.ItName, C.ForeignName, B.DisplayName, B.AssetCode ");
            SQL.AppendLine("From TblWOR A ");
            SQL.AppendLine("Inner Join TblAsset B On A.TOCode = B.AssetCode ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("   And A.Status = 'A' ");
            SQL.AppendLine("Inner Join (Select ItCode, ItName, ForeignName From TblItem Where ItCode In (Select Distinct ItCode From TblAsset)) C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblOption D On A.MtcStatus = D.OptCode And D.OptCat = 'MaintenanceStatus' ");
            SQL.AppendLine("Left Join TblOption E On A.MtcType = E.OptCode And E.OptCat = 'MaintenanceType' ");
            SQL.AppendLine("Left Join TblOption F On A.SymProblem = F.OptCode And F.OptCat = 'SymptomProblem' ");
            SQL.AppendLine("Where 0 = 0 ");
            if (mFrmParent.mIsWORStatusOpenOnly) 
            {
                SQL.AppendLine("And A.WOStatus = 'O' ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document", 
                        "",
                        "Date",
                        "Status",
                        "Down Date",

                        //6-10
                        "Down Time",
                        "Maintenance"+Environment.NewLine+"Status",
                        "Maintenance"+Environment.NewLine+"Type",
                        "Symptom"+Environment.NewLine+"Problem",
                        "Description",

                        //11-15
                        "Asset",
                        "Item Name",
                        "Foreign Name",
                        "Display Name",
                        "Asset Code"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 80, 100, 100, 
                        
                        //6-10
                        100, 120, 120, 120, 120, 
                        
                        //11-15
                        120, 150, 150, 150, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 5 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9,10, 11, 12, 13, 14, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "WOStatus", "DownDt", "DownTm", "MtcStatusDesc", 
                            
                            //6-10
                            "MtcTypeDesc", "SymptomProblemDesc", "Description", "AssetName", "ItName",

                            //11-13
                            "ForeignName", "DisplayName", "AssetCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int r = Grd1.CurRow.Index;

                mFrmParent.TxtWORDocNo.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                if (mFrmParent.mIsAssetForDODept2BasedOnWOR)
                {
                    mFrmParent.mAssetCode = Sm.GetGrdStr(Grd1, r, 15);
                    mFrmParent.mAssetName = Sm.GetGrdStr(Grd1, r, 11);
                    mFrmParent.mDisplayName = Sm.GetGrdStr(Grd1, r, 14);
                }
                mFrmParent.ClearGrd();

                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWOR(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWOR(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
           
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }


        #endregion

    }
}
