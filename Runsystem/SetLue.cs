﻿#region Update
/*
    03/08/2017 [WED] Tambah SetLueLocCode
    10/09/2017 [TKG] Tambah SetLueLevelCode
    03/10/2017 [WED] Tambah SetLueCreditCode
    09/10/2017 [TKG] Tambah SetLueCostCenterCode
    10/10/2017 [TKG] Tambah SetLueSiteCode dengan parameter IsFilterBySite
    10/10/2017 [TKG] Tambah SetLueDeptCode dengan parameter IsFilterByDept
    13/10/2017 [TKG] bug di SetLueCCCode
    20/10/2017 [TKG] Tambah SetLueClaimCode
    29/12/2017 [TKG] tambah SetLueItCtCodeActive
    24/01/2018 [TKG] ubah SetLuePtCode
    06/02/2018 [TKG] ubah SetLueBankAcCode
    28/02/2018 [WED] tambah SetLueTrainingCode
    09/05/2018 [TKG] fasilitas untuk merubah item category saat edit data berdasarkan parameter IsAbleToEditItemCategory.
    12/05/2018 [TKG] new method SetLueEntCode
    07/06/2018 [WED] tambah SetLueProjectTaskCode, SetLueProjectDocumentCode, SetLueProjectStageCode
    08/06/2018 [WED] tambah SetLueCtGrpCode
    05/07/2018 [TKG] tambah SetLueBCCode, SetLueBSCode
    06/07/2018 [TKG] tambah SetLueDocTypeCode
    01/08/2018 [WED] tambah SetLueServiceCode
    18/09/2018 [TKG] tambah SetLueRegionCode
    27/09/2018 [TKG] tambah SetLueCurCode2
    11/02/2019 [TKG] tambah SetLueDAGCode
    20/02/2019 [DITA] tambah SetLueMajorCode, SetLueEmployeEducationField
    06/03/2019 [TKG] SetLueFacCode
    11/04/2019 [WED] SetLueDeptCode2
    11/09/2019 [TKG] Tambah SetLueBin
    17/09/2019 [TKG] tambah SetLueCityCode dimana city berdasarkan country yg telah dipilih.
    24/09/2019 [TKG] tambah SetLuePGCode
    24/09/2020 [TKG] tambah SetLueCtCode dan SetLueCtCtCode (menggunakan tambahan filter berdasarkan customer's category)
    24/11/2020 [TKG/PHT] tambah SetLueLevelCode dengan parameter Code
    01/12/2020 [ICA/SIER] Tambah keterangan vendor category pada SetLueVdCode, SetLueVdCode2 berdasarkan parameter IsVendorComboShowCategory 
    02/12/2020 [ICA/SIER] Tambah keterangan customer category pada SetLueCtCode berdasarkan parameter IsCustomerComboShowCategory
    10/12/2020 [TKG/PHT] tambah SetLueBCCode
    28/12/2020 [WED/PHT] tambah SetLueGradeSalary
    12/01/2021 [VIN/KSM] parameter IsVendorComboShowCategory dan IsCustomerComboShowCategory pakai ifnull
    23/02/2021 [WED/SIER] tambah SetLueCtCodeBasedOnCategory
    04/03/2021 [RDH/PHT] Modify SetLueUserCode agar PIC di multi voucher request dapat terfilter nama usernya,
    09/04/2021 [ICA/SIER] menambag setLueBankAcCode dengan paramater bankacccode
    05/05/2021 [HAR/SRN] tambah SetLuePosPayCode
    17/06/2021 [TKG/PHT] tambah method SetLueCCCodeFilterByProfitCenter
    20/06/2021 [TKG/PHT] ubah method SetLueCCCodeFilterByProfitCenter spy lbh cepat
    05/07/2021 [TKG/PHT+IMS] tambah SetLuePeriod
    06/08/2021 [WED/PADI] tambah SetLueSector
    10/11/2021 [DEV/PHT] tambah SetLueCashType
    22/03/2022 [ISD/ALL] tambah SetLueYr dengan tambahan Range
    25/04/2022 [DITA/PHT] tambah SetLueProfitCenterCode 
    26/07/2022 [TYO/SIER] tambah SetLueVoucherDocType dengan parameter
    03/08/2022 [TYO/SIER] tambah SetLueVdCode dengan parameter untuk filter group
    04/08/2022 [SET/SIER] penyesuaian SetLueVdCode dengan parameter untuk filter group
    05/04/2022 [TYO/SIER] penyesuaian SetLueVoucherDocType dengan parameter untuk filter group
    18/08/2022 [TYO/SIER] penyesuaian SetLueCCCode dengan parameter 
    11/11/2022 [VIN/TWC] tambah IsCostCenterNotParentIndEnabled
    21/03/2023 [WED/PHT] tambah SetLueDeptCodeBasedOnSiteCode
    23/03/2023 [WED/PHT] tambah SetLuePosCodeBasedOnDeptCode
    23/03/2023 [WED/PHT] tambah SetLueLevelCodeBasedOnPosCode
    03/04/2023 [WED/PHT] tambah SetLuePositionStatus dengan 2 parameter args
    03/05/2023 [WED/PHT] SetLueDeptCodeBasedOnSiteCode ditambah union ambil dari parent nya profit center
*/
#endregion

using System;
using System.Text;
using DevExpress.XtraEditors;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

using Sm = RunSystem.StdMtd;
using Gv = RunSystem.GlobalVar;

namespace RunSystem
{
    public sealed class SetLue
    {
        public static void SetLueYr(LookUpEdit Lue, string YearStart)
        {
            if (YearStart.Length == 0) YearStart = Sm.GetParameter("StartYr");
            int YearEnd = DateTime.Now.Year;
            if (YearStart.Length != 0)
            {
                if (YearEnd - int.Parse(YearStart) >= 0)
                {
                    int Element = YearEnd - int.Parse(YearStart) + 3;
                    string[] Yr = new string[Element];
                    int Index2 = 0;
                    Yr[Index2++] = null;
                    for (int Index = YearEnd + 1; Index >= int.Parse(YearStart); Index--)
                        Yr[Index2++] = Index.ToString();
                    SetLookUpEdit(Lue, Yr);
                }
            }
        }

        public static void SetLueYr(LookUpEdit Lue, string YearStart, int Range)
        {
            if (YearStart.Length == 0) YearStart = Sm.GetParameter("StartYr");
            int YearEnd = DateTime.Now.AddYears(Range).Year;
            if (YearStart.Length != 0)
            {
                if (YearEnd - int.Parse(YearStart) >= 0)
                {
                    int Element = YearEnd - int.Parse(YearStart) + 3;
                    string[] Yr = new string[Element];
                    int Index2 = 0;
                    Yr[Index2++] = null;
                    for (int Index = YearEnd; Index >= int.Parse(YearStart); Index--)
                        Yr[Index2++] = Index.ToString();
                    SetLookUpEdit(Lue, Yr);
                }
            }
        }

        public static void SetLueMth(LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { null, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" });
        }

        public static void SetLueAcCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
             ref Lue,
             "Select AcCtCode As Col1, AcCtName As Col2 From TblAccountCategory Order By AcCtCode",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueAcNo(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select AcNo As Col1, Concat(AcNo, ' [ ', AcDesc, ' ]') As Col2 From TblCOA Order By AcNo",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueAcType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueAGCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr Where ActInd='Y' Order By AGName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueAgtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
             ref Lue,
             "Select AgtCode As Col1, AgtName As Col2 From TblAgent Order By AgtName",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVdCode2(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.VdCode As Col1 ");
            if(Sm.GetParameterBoo("IsVendorComboShowCategory"))
                SQL.AppendLine(", CONCAT(T.VdName, ' [', IFNULL(T2.VdCtName, ' '), ']') As Col2 ");
            else
                SQL.AppendLine(", T.VdName As Col2 ");
            SQL.AppendLine("From TblVendor T ");
            SQL.AppendLine("LEFT JOIN TblVendorCategory T2 ON T.VdCtCode = T2.VdCtCode ");

            if (Code.Length > 0)
                SQL.AppendLine("Where T.VdCode=@Code;");
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                SQL.AppendLine("Order By T.VdName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueAgtCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue2(
             ref Lue,
             "Select AgtCode As Col1, AgtName As Col2 From TblAgent Where CtCode="+CtCode+" Order By AgtName",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueAssetCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
             ref Lue,
             "Select AssetCode As Col1, AssetName As Col2 From TblAsset Order By AssetName",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueAssetCode(ref LookUpEdit Lue, string AssetCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AssetCode As Col1, AssetName As Col2 From TblAsset ");
            if (AssetCode.Length > 0)
                SQL.AppendLine("Where (ActiveInd='Y' Or AssetCode=@AssetCode) ");
            else
                SQL.AppendLine("Where ActiveInd='Y' ");
            SQL.AppendLine("Order By AssetName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (AssetCode.Length > 0) Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (AssetCode.Length > 0) Sm.SetLue(Lue, AssetCode);
        }

        public static void SetLueAssetCategoryCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
             ref Lue, "Select AssetCategoryCode As Col1, AssetCategoryName As Col2 From TblAssetCategory Order By AssetCategoryName;",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueBankAcCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Sm.GetParameter("BankAccountFormat") == "1")
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }
            else
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }

            if (Sm.GetParameterBoo("IsVoucherBankAccountFilteredByGrp"))
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            
            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Sm.GetParameter("BankAccountFormat") == "1")
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }
            else
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }

            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode =@BankAcCode ");
            else
                SQL.AppendLine("Where A.ActInd = 'Y' ");

            if (Sm.GetParameterBoo("IsVoucherBankAccountFilteredByGrp"))
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        public static void SetLueAccountPeriodStatus(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountPeriodStatus' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueADAmtType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='ADAmtType' Order By OptDesc;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueADCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select ADCode As Col1, ADName As Col2 From TblAllowanceDeduction Order By ADName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueADType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='ADType' Order By OptDesc;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueBankCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select BankCode As Col1, BankName As Col2 From TblBank Order By BankName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueBatchNo(ref LookUpEdit Lue)
        {
            Sm.SetLue1(
                ref Lue,
                "Select Distinct BatchNo As Col1 From TblStockSummary Order By BatchNo",
                "Batch Number");
        }

        public static void SetLueBCCode(ref LookUpEdit Lue, string BCCode, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode As Col1, BCName As Col2 From TblBudgetCategory ");
            if (BCCode.Length > 0)
                SQL.AppendLine("Where BCCode=@BCCode;");
            else
            {
                SQL.AppendLine("Where DeptCode=@DeptCode And ActInd='Y' ");
                SQL.AppendLine("Order By BCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (BCCode.Length > 0) Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            if (DeptCode.Length > 0) Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BCCode.Length > 0) Sm.SetLue(Lue, BCCode);
        }

        public static void SetLueBCCode(ref LookUpEdit Lue, string BCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode As Col1, BCName As Col2 From TblBudgetCategory ");
            if (BCCode.Length > 0)
                SQL.AppendLine("Where BCCode=@BCCode;");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                SQL.AppendLine("Order By BCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (BCCode.Length > 0) Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BCCode.Length > 0) Sm.SetLue(Lue, BCCode);
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Bin As Col1 from tblBin Where ActInd = 'Y' Order By Bin",
                "Bin");
        }

        public static void SetLueBin(ref LookUpEdit Lue, string Bin)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Bin As Col1 From TblBin ");
            if (Bin.Length > 0)
                SQL.AppendLine("Where Bin=@Bin;");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                SQL.AppendLine("Order By Bin;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Bin.Length > 0) Sm.CmParam<String>(ref cm, "@Bin", Bin);
            Sm.SetLue1(ref Lue, ref cm, "Bin");
            if (Bin.Length > 0) Sm.SetLue(Lue, Bin);
        }

        public static void SetLueBomValueCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'BOMValueFrom' Order By OptDesc  ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCCCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CCCode As Col1, CCName As Col2 From TblCostCenter Order By CCName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCCCode(ref LookUpEdit Lue, string Code, string IsFilterByCC)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CCCode As Col1, T.CCName As Col2 From TblCostCenter T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where CCCode=@Code;");
            }
            else
            {
                SQL.AppendLine("Where T.CCCode Not In ( ");
                SQL.AppendLine("    Select Parent From TblCostCenter ");
                SQL.AppendLine("    Where Parent Is Not Null ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And T.ActInd='Y' ");

                if (IsFilterByCC=="Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                    SQL.AppendLine("    Where CCCode=T.CCCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By T.CCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueCCCode(ref LookUpEdit Lue, string IsFilterByCC)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select T.CCCode As Col1, T.CCName As Col2 From TblCostCenter T ");
            if(IsFilterByCC == "Y")
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=T.CCCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.CCName ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        public static void SetLueCCCodeFilterByProfitCenter(ref LookUpEdit Lue, string Code, string Editable)
        {
            var SQL = new StringBuilder();

            if (Code.Length > 0)
            {
                if (Editable == "N")
                {
                    SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter ");
                    SQL.AppendLine("Where CCCode=@Code;");
                }
                else
                {
                    SQL.AppendLine("Select CCCode As Col1, CCName As Col2 ");
                    SQL.AppendLine("From TblCostCenter ");
                    SQL.AppendLine("Where CCCode=@Code Or (1=1 ");
                    if (Sm.GetParameterBoo("IsCostCenterNotParentIndEnabled"))
                        SQL.AppendLine("    And NotParentInd='Y' ");
                    else
                    {
                        SQL.AppendLine("    And 1 = Case When Exists( ");
                        SQL.AppendLine("    SELECT cccode FROM tblcostcenter WHERE cccode NOT IN (SELECT parent FROM tblcostcenter WHERE parent IS NOT NULL) ");
                        SQL.AppendLine("    ) Then 1 Else 0 End ");
                    }
                    SQL.AppendLine("    And ActInd='Y' ");
                    SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("    And ProfitCenterCode In (");
                    SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) Order By CCName;");
                }
            }
            else
            {
                SQL.AppendLine("Select A.CCCode As Col1, A.CCName As Col2 ");
                SQL.AppendLine("From TblCostCenter A ");
                SQL.AppendLine("Inner Join TblGroupProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
                SQL.AppendLine("Inner Join TblUser C On B.GrpCode=C.GrpCode And C.UserCode=@UserCode ");
                if (Sm.GetParameterBoo("IsCostCenterNotParentIndEnabled"))
                    SQL.AppendLine("    And NotParentInd='Y' ");
                else
                {
                    SQL.AppendLine("    And 1 = Case When Exists( ");
                    SQL.AppendLine("    SELECT cccode FROM tblcostcenter WHERE cccode NOT IN (SELECT parent FROM tblcostcenter WHERE parent IS NOT NULL) ");
                    SQL.AppendLine("    ) Then 1 Else 0 End ");
                }
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("And A.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("Order By A.CCName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueCCCodeByEntity(ref LookUpEdit Lue, string IsFilterByCC, string EntCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CCCode As Col1, T.CCName As Col2 From TblCostCenter T ");
            SQL.AppendLine("Where T.CCCode Not In ( ");
            SQL.AppendLine("    Select Parent From TblCostCenter ");
            SQL.AppendLine("    Where Parent Is Not Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T.ProfitCenterCode Is Not Null  ");
            SQL.AppendLine("And T.ProfitCenterCode In (Select ProfitCenterCode From TblProfitCenter Where EntCode Is Not Null And EntCode=@EntCode) ");
            if (IsFilterByCC == "Y")
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=T.CCCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.CCName;");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCityCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CityCode As Col1, Concat(A.CityName, ' ( ', IfNull(B.ProvName, ''), ' ', IfNull(C.CntName, ''), ' )')  As Col2 " +
                "From TblCity A " +
                "Left Join TblProvince B On A.ProvCode=B.ProvCode " +
                "Left Join TblCountry C On B.CntCode=C.CntCode " +
                "Order By C.CntName, A.CityName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCityCode(ref LookUpEdit Lue, string CntCode, string CityCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CityCode As Col1, Concat(A.CityName, ' ( ', IfNull(B.ProvName, ''), ' ', IfNull(C.CntName, ''), ' )')  As Col2 ");
            SQL.AppendLine("From TblCity A ");
            if (CityCode.Length > 0)
            {
                SQL.AppendLine("Left Join TblProvince B On A.ProvCode=B.ProvCode ");
                SQL.AppendLine("Left Join TblCountry C On B.CntCode=C.CntCode ");
                SQL.AppendLine("Where A.CityCode=@CityCode ");
                SQL.AppendLine("Or A.CityCode In ( ");
                SQL.AppendLine("    Select T1.CityCode ");
                SQL.AppendLine("    From TblCity T1, TblProvince T2 ");
                SQL.AppendLine("    Where T1.ProvCode=T2.ProvCode ");
                SQL.AppendLine("    And T1.ProvCode=T2.ProvCode ");
                SQL.AppendLine("    And T2.CntCode=@CntCode ");
                SQL.AppendLine(")  ");
                SQL.AppendLine("Order By C.CntName, A.CityName;");
            }
            else
            {
                SQL.AppendLine("Inner Join TblProvince B On A.ProvCode=B.ProvCode ");
                SQL.AppendLine("Inner Join TblCountry C On B.CntCode=C.CntCode And C.CntCode=@CntCode ");
                SQL.AppendLine("Order By C.CntName, A.CityName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CntCode", CntCode);
            if (CityCode.Length > 0) Sm.CmParam<String>(ref cm, "@CityCode", CityCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CityCode.Length > 0) Sm.SetLue(Lue, CityCode);
        }

        public static void SetLueClaimCode(ref LookUpEdit Lue, string ClaimCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ClaimCode As Col1, ClaimName As Col2 From TblClaim ");
            if (ClaimCode.Length > 0)
                SQL.AppendLine("Where ClaimCode=@ClaimCode;");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                SQL.AppendLine("Order By ClaimName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (ClaimCode.Length > 0) Sm.CmParam<String>(ref cm, "@ClaimCode", ClaimCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (ClaimCode.Length > 0) Sm.SetLue(Lue, ClaimCode);
        }

        public static void SetLueCntCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CntCode As Col1, CntName As Col2 From TblCountry Order By CntName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct CCtCode As Col1, CCtName As Col2 From TblCostCategory Order By CCtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCCtCode(ref LookUpEdit Lue, string CCCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CCtCode As Col1, CCtName As Col2 From TblCostCategory " +
                ((CCCode.Length==0)?string.Empty:" Where CCCode='"+CCCode+"' ") +
                "Order By CCtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCompetenceCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CompetenceCode As Col1, CompetenceName As Col2 From TblCompetence Order By CompetenceName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCompetenceLevel(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
               ref Lue,
               "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='CompetenceLevel' Order By OptDesc",
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCreditCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
             ref Lue,
             "Select CreditCode As Col1, CreditName As Col2 From TblCredit Where ActInd = 'Y' Order By CreditName",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT T.CtCode As Col1, ");
            if (Sm.GetParameterBoo("IsCustomerComboShowCategory"))
                SQL.AppendLine("CONCAT(T.CtName, ' [', IFNULL(T2.CtCtName, ' '), ']') As Col2  ");
            else
                SQL.AppendLine("T.CtName As Col2 ");
            SQL.AppendLine("From TblCustomer T ");
            SQL.AppendLine("Left Join TblCustomerCategory T2 On T.CtCtCode=T2.CtCtCode ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("Order By T.CtName ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                
            Sm.SetLue2(
                ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCtCode(ref LookUpEdit Lue, string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.CtCode As Col1, ");
            if (Sm.GetParameterBoo("IsCustomerComboShowCategory"))
                SQL.AppendLine("CONCAT(T.CtName, ' [', IFNULL(T2.CtCtName, ' '), ']') As Col2  ");
            else
                SQL.AppendLine("T.CtName As Col2 ");
            SQL.AppendLine("From TblCustomer T ");
            SQL.AppendLine("Left Join TblCustomerCategory T2 On T.CtCtCode=T2.CtCtCode ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (CtCode.Length!=0) SQL.AppendLine("Or T.CtCode=@Param ");
            SQL.AppendLine("Order By T.CtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (CtCode.Length != 0) Sm.CmParam<String>(ref cm, "@Param", CtCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CtCode.Length > 0) Sm.SetLue(Lue, CtCode);
        }

        public static void SetLueCtCode(ref LookUpEdit Lue, string CtCode, string IsFilterByCtCt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CtCode As Col1, ");
            if (Sm.GetParameterBoo("IsCustomerComboShowCategory"))
                SQL.AppendLine("Concat(T.CtName, '[',T1.CtCtName,']') As Col2 ");
            else
                SQL.AppendLine("T.CtName As Col2 ");
            SQL.AppendLine("From TblCustomer T ");
            SQL.AppendLine("Inner Join TblCustomerCategory T1 On T.CtCtCode = T1.CtCtCode ");
            SQL.AppendLine("Where T.ActInd = 'Y' ");
            if (CtCode.Length != 0)
                SQL.AppendLine("Or T.CtCode=@Param ");
            else
            {
                if (IsFilterByCtCt == "Y")
                {
                    SQL.AppendLine("And (T.CtCtCode Is Null Or (T.CtCtCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                    SQL.AppendLine("    Where CtCtCode=T.CtCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ))) ");
                }
            }
            SQL.AppendLine("Order By T.CtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (CtCode.Length != 0)
                Sm.CmParam<String>(ref cm, "@Param", CtCode);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CtCode.Length > 0) Sm.SetLue(Lue, CtCode);
        }

        public static void SetLueCtCtCode(ref LookUpEdit Lue, string Code, string IsFilterByCtCt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CtCtCode As Col1, T.CtCtName As Col2 From TblCustomerCategory T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.CtCtCode=@Code;");
            else
            {
                SQL.AppendLine("Where 1=1 ");
                if (IsFilterByCtCt == "Y")
                {
                    SQL.AppendLine("And (T.CtCtCode Is Null Or (T.CtCtCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                    SQL.AppendLine("    Where CtCtCode=T.CtCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ))) ");
                }
                SQL.AppendLine("Order By T.CtCtName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueCtCodeBasedOnCategory(ref LookUpEdit Lue, string Code, string IsFilterByCtCt, string CtCtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CtCode As Col1, T.CtName As Col2 ");
            SQL.AppendLine("From TblCustomer T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.CtCode=@Code ");
            else
            {
                SQL.AppendLine("Where 1=1 ");
                SQL.AppendLine("And T.CtCtCode = @CtCtCode ");
                if (IsFilterByCtCt == "Y")
                {
                    SQL.AppendLine("And (T.CtCtCode Is Null Or (T.CtCtCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                    SQL.AppendLine("    Where CtCtCode=T.CtCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ))) ");
                }
                SQL.AppendLine("Order By T.CtName ");
            }

            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@CtCtCode", CtCtCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueCtCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CtCtCode As Col1, CtCtName As Col2 From TblCustomerCategory Order By CtCtName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        public static void SetLueCtGrpCode(ref LookUpEdit Lue, string Code, string IsShowActivedCtGrp)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CtGrpCode As Col1, CtGrpName As Col2 From TblCustomerGroup ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (CtGrpCode=@Code Or ActInd='Y') ");
            }
            else
            {
                if (IsShowActivedCtGrp=="Y") SQL.AppendLine("Where ActInd='Y' ");
            }
            SQL.AppendLine("Order By CtGrpName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode= '" + CtCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        public static void SetLueCurCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CurCode As Col1, CurName As Col2 From TblCurrency Order By CurName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCurCode2(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CurCode As Col1, CurCode As Col2 From TblCurrency Order By CurCode;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueDAGCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DAGCode As Col1, DAGName As Col2 From TblDocApprovalGroupHdr ");
            if (Code.Length > 0)
                SQL.AppendLine("Where (DAGCode=@Code Or ActInd='Y') Order By DAGName;");
            else
                SQL.AppendLine("Where ActInd='Y' Order By DAGName;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueDivisionCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DivisionCode As Col1, DivisionName As Col2 From TblDivision Order By DivisionName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        } 

        public static void SetLueDTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DTCode As Col1, DTName As Col2 From TblDeliveryType Order By DTName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueDeptCode2(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 From TblDepartment T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.DeptCode=@Code;");
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                SQL.AppendLine("Order By T.DeptName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueDeptCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DeptCode As Col1, DeptName As Col2 From TblDepartment Order By DeptName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueDeptCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 From TblDepartment T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where DeptCode=@Code;");
            }
            else
            {
                SQL.AppendLine(" Where T.ActInd = 'Y' ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Order By T.DeptName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueDeptCode(ref LookUpEdit Lue, string Code, string IsFilterByDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 From TblDepartment T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where DeptCode=@Code;");
            }
            else
            {
                SQL.AppendLine(" Where T.ActInd = 'Y' ");
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By T.DeptName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueDeptCodeBasedOnSiteCode(ref LookUpEdit Lue, string Code, string SiteCode, string IsFilterByDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 From TblDepartment T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where DeptCode=@Code;");
            }
            else
            {
                SQL.AppendLine(" Where T.ActInd = 'Y' ");
                
                if (IsFilterByDept == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }

                if (SiteCode.Length > 0)
                {
                    SQL.AppendLine("And T.DeptCode In ( ");
                    SQL.AppendLine("    Select Distinct T2.DeptCode ");
                    SQL.AppendLine("    From TblSite T1 ");
                    SQL.AppendLine("    Inner Join TblCostCenter T2 On T1.ProfitCenterCode = T2.ProfitCenterCode ");
                    SQL.AppendLine("    Where T2.ActInd = 'Y' ");
                    SQL.AppendLine("    And T1.SiteCode = @SiteCode ");
                    SQL.AppendLine("    Union ");
                    SQL.AppendLine("    Select Distinct X3.DeptCode ");
                    SQL.AppendLine("    From TblSite X1 ");
                    SQL.AppendLine("    Inner Join TblProfitCenter X2 On X1.ProfitCenterCode = X2.Parent And X2.Parent Is Not Null ");
                    SQL.AppendLine("    Inner Join TblCostCenter X3 On X2.ProfitCenterCode = X3.ProfitCenterCode And X3.ActInd = 'Y' ");
                    SQL.AppendLine("    Where X1.SiteCode = @SiteCode ");
                    SQL.AppendLine(") ");
                }

                SQL.AppendLine("Order By T.DeptName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueDocCat(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='InvDocCat' Order By OptCode;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueDocTypeCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(ref Lue, "Select DocTypeCode As Col1, DocTypeName As Col2 From TblRHADocType Order By DocTypeCode;", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueEntCode(ref LookUpEdit Lue, string Code, string IsFilterByEnt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.EntCode As Col1, T.EntName As Col2 From TblEntity T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where EntCode=@Code;");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (IsFilterByEnt == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupEntity ");
                    SQL.AppendLine("    Where EntCode=T.EntCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By T.EntName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueEmployeeEducationLevel(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='EmployeeEducationLevel' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueEmployeePayrollType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='EmployeePayrollType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueEmpStatus(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Active Employee' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Resignee' As Col2; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        public static void SetLueEntCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select EntCode As Col1, EntName As Col2 From TblEntity Where ActInd='Y' Order By EntName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueEntCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EntCode As Col1, EntName As Col2 From TblEntity ");
            if (Code.Length > 0)
                SQL.AppendLine("Where EntCode=@Code;");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                SQL.AppendLine("Order By EntName;");
            }
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueFacCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select FacCode As Col1, FacName As Col2 From TblFaculty ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (Code.Length > 0) SQL.AppendLine(" Or FacCode=@Code ");
            SQL.AppendLine("Order By FacName;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueFamilyStatus(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='FamilyStatus' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueGender(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='Gender' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueGrdLvlCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select GrdLvlCode As Col1, GrdLvlName As Col2 " +
                "From TblGradeLevelHdr " +
                "Order By GrdLvlName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueGradeSalary(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select GrdSalaryCode Col1, GrdSalaryName Col2 ");
            SQL.AppendLine("From TblGradeSalaryHdr ");
            SQL.AppendLine("Order By GrdSalaryName ");
            SQL.AppendLine("; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueFileCategory(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
            ref Lue,
            "Select CategoryCode As Col1, CategoryName As Col2 From TblVendorFileUploadCategory Order By CategoryName",
            0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTypesofLoans(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='TypesOfLoans' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueGrdLvlGrpCode(ref LookUpEdit Lue, string Code)
        {
            Sm.SetLue2(
                ref Lue,
                "Select GrdLvlGrpCode As Col1, GrdLvlGrpName As Col2 " +
                "From TblGradeLevelGroup " +
                "Order By GrdLvlGrpName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueGrpCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select GrpCode As Col1, GrpName As Col2 From TblGroup Order By GrpName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueIMMLocCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select LocCode As Col1, LocName As Col2 From TblIMMLocation Order By LocName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueIMMProdCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ProdCtCode As Col1, ProdCtName As Col2 From TblIMMProductCategory Order By ProdCtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueIMMSellerCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select SellerCode As Col1, SellerName As Col2 From TblIMMSeller Order By SellerName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueItBrCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ItBrCode As Col1, ItBrName As Col2 From TblItemBrand Order By ItBrName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueItCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory Order By ItCtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueItCtCodeFilterByItCt(ref LookUpEdit Lue, string IsFilterByItCt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItCtCode As Col1, T.ItCtName As Col2 ");
            SQL.AppendLine("From TblItemCategory T ");
            if (IsFilterByItCt=="Y")
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T.ItCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueItCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItCtCode As Col1, T.ItCtName As Col2 From TblItemCategory T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.ItCtCode=@Code;");
            else
            {
                if (Sm.GetParameter("IsFilterByItCt") == "Y")
                {
                    SQL.AppendLine("Where Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By T.ItCtName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueItCtCodeActive(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItCtCode As Col1, T.ItCtName As Col2 From TblItemCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.ItCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                if (Sm.GetParameter("IsFilterByItCt") == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (Sm.GetParameter("IsFilterByItCt") == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By T.ItCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        //public static void SetLueItCtCode(ref LookUpEdit Lue, string Code)
        //{
        //    SetLueWithValue(ref Lue, Code,
        //        "Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory " +
        //        (Code.Length == 0 ? " Order By ItCtName;" : " Where ItCtCode=@Code;"));
        //}

        public static void SetLueItGrpCode(ref LookUpEdit Lue, string Code)
        {
            SetLueWithValue(ref Lue, Code,
                "Select ItGrpCode As Col1, ItGrpName As Col2 From TblItemGroup " +
                (Code.Length == 0 ? " Order By ItGrpName;" : " Where ItGrpCode=@Code;"));
        }

        public static void SetLueItScCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ItScCode As Col1, ItScName As Col2 From TblItemSubCategory Order By ItScName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueJobCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue, "Select JobCtCode As Col1, JobCtName As Col2 From TblJobCategory Order By JobCtName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueItPropCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PropCode As Col1, PropName As Col2 From TblProperty Order By PropName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueItPropCode(List<LookUpEdit> ListOfLue)
        {
            Sm.SetLue2(
                ref ListOfLue,
                "Select PropCode As Col1, PropName As Col2 From TblProperty Order By PropName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueItSCCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ItSCCode As Col1, ItSCName As Col2 From TblItemSubCategory Order By ItSCName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueDLCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DLCode As Col1, DLName As Col2 From TblDocLegality Order By DLName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueLeaveCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select LeaveCode As Col1, LeaveName As Col2 From TblLeave Order By LeaveName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueLeaveCode(ref LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select LeaveCode As Col1, LeaveName As Col2 ");
                SQL.AppendLine("From TblLeave Where ");

                if (Code.Length == 0)
                    SQL.AppendLine("ActInd='Y' Order By LeaveName; ");
                else
                    SQL.AppendLine("LeaveCode=@Code;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void SetLueLevelCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select LevelCode As Col1, LevelName As Col2 From TblLevelHdr Order By LevelName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
        
        public static void SetLueLoadingArea(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='LoadingArea' Order By CreateDt",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueLoadingItemCat(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='LoadingItemCat' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select LocCode As Col1, LocName As Col2 From TblLocation Where ActInd = 'Y'  ");
            SQL.AppendLine("Order By LocName");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Lot As Col1 from tblLotHdr Where ActInd = 'Y' Order By Lot",
                "Lot");
        }

        public static void SetLueMajorCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select MajorCode As Col1, MajorName As Col2 From TblMajor Order By MajorName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        public static void SetLueEmployeeEducationField(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='EmployeeEducationField' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueOption(ref LookUpEdit Lue, string OptCat)
        {
            var cm = new MySqlCommand() 
            {CommandText = "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat=@Param Order By OptDesc;"};
            if (OptCat.Length > 0) Sm.CmParam<String>(ref cm, "@Param", OptCat);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");            
        }

        public static void SetLuePGCode(ref LookUpEdit Lue, string PGCode, string IsActive)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select PGCode As Col1, PGName As Col2 ");
            SQL.AppendLine("From TblProjectGroup ");
            if (PGCode.Length > 0)
                SQL.AppendLine("Where PGCode=@PGCode ");
            else
            { 
                if (IsActive=="Y") SQL.AppendLine("Where ActInd='Y' ");
            }
            SQL.AppendLine("Order By PGName;");
            cm.CommandText = SQL.ToString();
            if (PGCode.Length > 0) Sm.CmParam<String>(ref cm, "@PGCode", PGCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (PGCode.Length > 0) Sm.SetLue(Lue, PGCode);
        }

        public static void SetLuePGCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PGCode As Col1, PGName As Col2 From TblPricingGroup Order By PGName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePayrollGrpCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PGCode As Col1, PGName As Col2 From TblPayrollGrpHdr Order By PGName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePayrunCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PayrunCode As Col1, Concat(PayrunCode, ' [', PayrunName, ']') As Col2 From TblPayrun Where CancelInd='N' Order By PayrunCode Desc;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePeriod(LookUpEdit Lue)
        {
            SetLookUpEdit(Lue, new string[] { null, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" });
        }

        public static void SetLueProfitCenterCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ProfitCenterCode As Col1, ProfitCenterName As Col2 From TblProfitCenter Order By ProfitCenterName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueProfitCenterCode(ref LookUpEdit Lue, string Code, string IsFilterByProfitCenter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    Select ProfitCenterCode As Col1, ProfitCenterName As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            if (Code.Length > 0)
                SQL.AppendLine("Where ProfitCenterCode=@Code;");
            else
            {
                if (IsFilterByProfitCenter == "Y")
                {
                    SQL.AppendLine("    Where ProfitCenterCode In ( ");
                    SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        )");
                }
                SQL.AppendLine("Order By ProfitCenterCode; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueCashType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CashTypeCode As Col1, CashTypeName As Col2 From TblCashType Order By CashTypeName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueProjectDocumentCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocumentCode As Col1, DocumentName As Col2 From TblProjectDocument ");
            SQL.AppendLine("Order By DocumentName; ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueProjectStageCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select StageCode As Col1, StageName As Col2 From TblProjectStage ");
            SQL.AppendLine("Order By StageName; ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueProjectTaskCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select TaskCode As Col1, TaskName As Col2 From TblProjectTask ");
            SQL.AppendLine("Order By TaskName; ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        public static void SetLuePosCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PosCode As Col1, PosName As Col2 From TblPosition Order By PosName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePosCodeBasedOnDeptCode(ref LookUpEdit Lue, string Code, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.PosCode As Col1, T.PosName As Col2 From TblPosition T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where PosCode=@Code;");
            }
            else
            {
                SQL.AppendLine(" Where 0 = 0 ");

                if (DeptCode.Length > 0)
                {
                    SQL.AppendLine("And T.PosCode In ( ");
                    SQL.AppendLine("    Select Distinct T1.PosCode ");
                    SQL.AppendLine("    From TblDepartmentPosition T1 ");
                    SQL.AppendLine("    Where T1.DeptCode = @DeptCode ");
                    SQL.AppendLine(") ");
                }

                SQL.AppendLine("Order By T.PosName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLuePositionStatusCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PositionStatusCode As Col1, PositionStatusName As Col2 From TblPositionStatus Order By PositionStatusName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePositionStatusCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select PositionStatusCode As Col1, PositionStatusName As Col2 ");
            SQL.AppendLine("From TblPositionStatus ");
            
            if (Code.Length > 0)
                SQL.AppendLine("Where PositionStatusCode = @Code ");
            
            SQL.AppendLine("Order by PositionStatusName ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);

            Sm.SetLue2(
                ref Lue,
                ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePosPayCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                 ref Lue,
                 " Select PayTpNo As Col1, PayTpNm As Col2 From TblPosPayByType order By PayTpNo ",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        public static void SetLueProductionShiftCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ProductionShiftCode As Col1, ProductionShiftName As Col2 From TblProductionShift Order By ProductionShiftName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueProductionWagesSource(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='ProductionWagesSource' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePropCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PropCode As Col1, PropName As Col2 From TblProperty Order By PropName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            
            if (Sm.GetParameterBoo("IsGroupPaymentTermActived"))
            {
                SQL.AppendLine("Select PtCode As Col1, PtName As Col2 ");
                SQL.AppendLine("From TblPaymentTerm ");
                SQL.AppendLine("Where PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Order By PtName;");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            }
            else
            {
                SQL.AppendLine("Select PtCode As Col1, PtName As Col2 ");
                SQL.AppendLine("From TblPaymentTerm Order By PtName;");
            }

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Code.Length > 0)
            {
                SQL.AppendLine("Select PtCode As Col1, PtName As Col2 ");
                SQL.AppendLine("From TblPaymentTerm Where PtCode=@Code;");

                Sm.CmParam<String>(ref cm, "@Code", Code);
            }
            else
            {
                if (Sm.GetParameterBoo("IsGroupPaymentTermActived"))
                {
                    SQL.AppendLine("Select PtCode As Col1, PtName As Col2 ");
                    SQL.AppendLine("From TblPaymentTerm ");
                    SQL.AppendLine("Where PtCode In (");
                    SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                    SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("Order By PtName;");

                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                }
                else
                {
                    SQL.AppendLine("Select PtCode As Col1, PtName As Col2 ");
                    SQL.AppendLine("From TblPaymentTerm Order By PtName;");
                }
            }

            cm.CommandText = SQL.ToString();
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLuePTKP(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='NonTaxableIncome' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueProvCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select A.ProvCode As Col1, Concat(A.ProvName, ' ( ', B.CntName, ' )')  As Col2 " +
                "From TblProvince A " +
                "Left Join TblCountry B On A.CntCode=B.CntCode " +
                "Order By B.CntName, A.ProvName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePurchaseInvoiceDocType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='PurchaseInvoiceDocType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLuePurchaseInvoiceDocInd(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='PurchaseInvoiceDocInd' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueQCParameterUomCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select UomCode As Col1, UomName As Col2 From TblQCParameterUom Order By UomName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueReligion(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='Religion' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueReqType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='ReqType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueRingCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select RingCode As Col1, RingName As Col2 From TblRingArea Order By RingName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueReasonCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select RsnNo As Col1, RsnDesc As Col2 From TblSOQuotPromoReason Order By RsnDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueRegionCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select RegionCode As Col1, RegionName As Col2 From TblRegion Order By RegionName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        public static void SetLueSDCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SDCode As Col1, Concat(A.SDName, ' ( ', IfNull(B.CityName, ''), ' ', IfNull(C.ProvName, ''), ' ', IfNull(D.CntName, ''), ' )')  As Col2 ");
            SQL.AppendLine("From TblSubDistrict A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblProvince C On B.ProvCode=C.ProvCode ");
            SQL.AppendLine("Left Join TblCountry D On C.CntCode=D.CntCode ");
            SQL.AppendLine("Order By D.CntName, C.ProvName, B.CityName, A.SDName");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSDCode(ref LookUpEdit Lue, string CityCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SDCode As Col1, SDName As Col2 ");
            SQL.AppendLine("From TblSubDistrict ");
            SQL.AppendLine("Where CityCode='"+CityCode+"' ");
            SQL.AppendLine("Order By SDName");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSectionCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(ref Lue, "Select SectionCode As Col1, SectionName As Col2 From TblSection Order By SectionName;", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSector(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SectorCode As Col1, SectorName As Col2 ");
            SQL.AppendLine("From TblSector ");
            if (Code.Length > 0)
                SQL.AppendLine("Where SectorCode = @Code ");
            else
                SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("Order By SectorName ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSiteCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupSite ");
            SQL.AppendLine("    Where SiteCode=T.SiteCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By T.SiteName;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSiteCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.SiteCode=@Code;");
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Order By T.SiteName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueSiteCode2(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.SiteCode=@Code;");
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                SQL.AppendLine("Order By T.SiteName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueSiteCode(ref LookUpEdit Lue, string Code, string IsFilterBySite)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where SiteCode=@Code;");
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By T.SiteName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueBomDocType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='BomDocType' Order By OptCode",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSSCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SSCode As Col1, SSName As Col2 From TblSS Where SSPCode In (Select SSPCode From TblSSProgram Where FamilyInd='Y') Order By SSName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SSPCode As Col1, SSPName As Col2 From TblSSProgram Order By SSPName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTaxCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TaxCode As Col1, TaxName As Col2 From TblTax Order By TaxName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTaxCode(List<DevExpress.XtraEditors.LookUpEdit> Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TaxCode As Col1, TaxName As Col2 From TblTax Order By TaxName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTaxGrpCode(List<DevExpress.XtraEditors.LookUpEdit> Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TaxGrpCode As Col1, TaxGrpName As Col2 From TblTaxGrp Order By TaxGrpName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTaCode(List<DevExpress.XtraEditors.LookUpEdit> ListOfLue)
        {
            Sm.SetLue2(
                ref ListOfLue,
                "Select TaxCode As Col1, TaxName As Col2 From TblTax Order By TaxName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTaxGrpCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(ref Lue, "Select TaxGrpCode As Col1, TaxGrpName As Col2 From TblTaxGrp Order By TaxGrpName;", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueAgingAP(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AgingAP' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTrainerType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='TrainerType' Order By OptCode",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTrainingCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TrainingCode As Col1, TrainingName As Col2 From TblTraining Order By TrainingName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTrainingCode(ref LookUpEdit Lue, string Code, string IsFilterBySite)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.TrainingCode As Col1, T.TrainingName As Col2 From TblTraining T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where T.TrainingCode=@Code ;");
            }
            else
            {
                if (IsFilterBySite == "Y")
                {
                    SQL.AppendLine("Where T.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By T.TrainingName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueTransType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='InventoryTransType' Order By OptCode",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueTTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TTCode As Col1, TTName As Col2 From TblTransportType Order By TTName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueUomCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select UomCode As Col1, UomName As Col2 From TblUom Order By UomName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueUomCode(List<DevExpress.XtraEditors.LookUpEdit> ListOfLue)
        {
            Sm.SetLue2(
                ref ListOfLue,
                "Select UomCode As Col1, UomName As Col2 From TblUom Order By UomName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueUserCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select UserCode As Col1, UserName As Col2 From TblUser ");            
            if (Sm.GetParameterBoo("IsPICGrouped"))
            {
                SQL.AppendLine("WHERE UserCode = @UserCode ");
            }
            SQL.AppendLine("Order By UserName ");

            cm = new MySqlCommand { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");

            //Sm.SetLue2(
            //    ref Lue,
            //    "Select UserCode As Col1, UserName As Col2 From TblUser Order By UserName;",
            //    0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueUserCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select UserCode As Col1, UserName As Col2 From TblUser T ");
            if (Code.Length > 0)
                SQL.AppendLine("Where UserCode=@Code;");
            else
                SQL.AppendLine("Where ExpDt Is Null Order By UserName;");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        public static void SetLueVdCode(ref LookUpEdit Lue)
        {
            if(!Sm.GetParameterBoo("IsVendorComboShowCategory"))
                Sm.SetLue2(
                    ref Lue,
                    "Select VdCode As Col1, VdName As Col2 From TblVendor Order By VdName",
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            else
                Sm.SetLue2(
                    ref Lue,
                    "Select T.VdCode As Col1, CONCAT(T.VdName, ' [', IFNULL(T2.VdCtName, ' '), ']') As Col2 From TblVendor T LEFT JOIN TblVendorCategory T2 ON T.VdCtCode = T2.VdCtCode Order By T.VdName",
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVdCode(ref LookUpEdit Lue, string FilteredByVdCt)
        {
            var SQL = new StringBuilder();
            if (!Sm.GetParameterBoo("IsVendorComboShowCategory"))
            {
                SQL.AppendLine("Select A.VdCode As Col1, A.VdName As Col2 From TblVendor A ");
                if(FilteredByVdCt == "Y")
                {
                    SQL.AppendLine("Where EXISTS");
                    SQL.AppendLine("(  ");
                    SQL.AppendLine("	SELECT 1   ");
                    SQL.AppendLine("	FROM TblGroupVendorCategory  ");
                    SQL.AppendLine("	WHERE VdCtCode =A.VdCtCode  ");
                    SQL.AppendLine("	AND GrpCode IN   ");
                    SQL.AppendLine("	(  ");
                    SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                    SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                    SQL.AppendLine("	)  ");
                    SQL.AppendLine(")  ");
                }
                SQL.AppendLine("Order By A.VdName ");
            }

            else
            {
                SQL.AppendLine("Select T.VdCode As Col1, CONCAT(T.VdName, ' [', IFNULL(T2.VdCtName, ' '), ']') As Col2");
                SQL.AppendLine("From TblVendor T");
                SQL.AppendLine("LEFT JOIN TblVendorCategory T2 ON T.VdCtCode = T2.VdCtCode ");
                if (FilteredByVdCt == "Y")
                {
                    SQL.AppendLine("Where EXISTS");
                    SQL.AppendLine("(  ");
                    SQL.AppendLine("	SELECT 1   ");
                    SQL.AppendLine("	FROM TblGroupVendorCategory  ");
                    SQL.AppendLine("	WHERE VdCtCode =T.VdCtCode  ");
                    SQL.AppendLine("	AND GrpCode IN   ");
                    SQL.AppendLine("	(  ");
                    SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                    SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                    SQL.AppendLine("	)  ");
                    SQL.AppendLine(")  ");
                }

                SQL.AppendLine("Order By T.VdName ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVdCode(List<LookUpEdit> ListOfLue)
        {
            Sm.SetLue2(
                ref ListOfLue,
                "Select VdCode As Col1, VdName As Col2 From TblVendor Order By VdName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVoucherDocType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='VoucherDocType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVoucherDocType(ref LookUpEdit Lue, string FilteredByType)
        {
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.OptCode As Col1, A.OptDesc As Col2  ");
            SQL.AppendLine("From TblOption A ");
            SQL.AppendLine("Where A.OptCat='VoucherDocType' ");
            if (FilteredByType == "Y")
            {
                SQL.AppendLine("And EXISTS");
                SQL.AppendLine("(  ");
                SQL.AppendLine("	SELECT 1   ");
                SQL.AppendLine("	FROM TblGroupType  ");
                SQL.AppendLine("	WHERE TypeCode =A.OptCode  ");
                SQL.AppendLine("	AND GrpCode IN   ");
                SQL.AppendLine("	(  ");
                SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                SQL.AppendLine("	)  ");
                SQL.AppendLine(")  ");
            }
            SQL.AppendLine("ORDER BY A.OptDesc ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVoucherPaymentType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='VoucherPaymentType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueWagesFormulationCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select WagesFormulationCode As Col1, WagesFormulationName As Col2 " +
                "From TblWagesFormulationHdr Order By WagesFormulationName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueWhsCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select WhsCode As Col1, WhsName As Col2 From TblWarehouse Order By WhsName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueWorkCenterDocNo(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DocNo As Col1, DocName As Col2 " +
                "From TblWorkCenterHdr Where ActiveInd='Y' Order By DocName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueWhsCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.WhsCode As Col1, T.WhsName As Col2 From TblWarehouse T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where WhsCode=@Code;");
            }
            else 
            { 
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=T.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Order By T.WhsName;");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private static void SetLueWithValue(ref LookUpEdit Lue, string Code, string SQL)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            Sm.SetLue(Lue, Code);
        }

        public static void SetLueWorkCenterDocNo(ref LookUpEdit Lue, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo As Col1, DocName As Col2 ");
            SQL.AppendLine("From TblWorkCenterHdr ");
            if (DocNo.Length>0)
                SQL.AppendLine("Where DocNo='"+DocNo+"' ");
            else
                SQL.AppendLine("Where ActiveInd='Y' ");
            SQL.AppendLine("Order By DocName;");
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVdCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select VdCtCode As Col1, VdCtName As Col2 From TblVendorCategory Order By VdCtName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVilCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.VilCode As Col1, Concat(A.VilName, ' ( ', IfNull(B.SDName, ''), ' ', IfNull(C.CityName, ''), ' ', IfNull(D.ProvName, ''), ' ', IfNull(E.CntName, ''), ' )')  As Col2 ");
            SQL.AppendLine("From TblVillage A ");
            SQL.AppendLine("Left Join TblSubDistrict B On A.SDCode=B.SDCode ");
            SQL.AppendLine("Left Join TblCity C On B.CityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblProvince D On C.ProvCode=D.ProvCode ");
            SQL.AppendLine("Left Join TblCountry E On D.CntCode=E.CntCode ");
            SQL.AppendLine("Order By E.CntName, D.ProvName, C.CityName, B.SDName, A.VilName");
            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueVilCode(ref LookUpEdit Lue, string SDCode)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select VilCode As Col1, VilName As Col2 ");
            SQL.AppendLine("From TblVillage Where SDCode='"+SDCode+"' Order By VilName");
            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueWhsCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select WhsCtCode As Col1, WhsCtName As Col2 From TblWarehouseCategory Order By WhsCtName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueWLCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select WLCode As Col1, WLName As Col2 From TblWarningLetter Order By WLName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueWSCode(ref LookUpEdit Lue)
        {
            Sm.SetLue3(
                ref Lue,
                "Select WSCode As Col1, WSName As Col2, HolidayInd As Col3 From TblWorkSchedule " +
                "Where ActInd='Y' Order By WSName;",
                0, 35, 0, false, true, false, "Code", "Name", "Holiday", "Col2", "Col1");
        }

        public static void SetLueBCCode(ref LookUpEdit Lue)
        {
            var cm = new MySqlCommand() { CommandText = "Select BCCode As Col1, BCName As Col2 From TblBusinessCategory Order By BCName;" };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueBSCode(ref LookUpEdit Lue)
        {
            var cm = new MySqlCommand() { CommandText = "Select BSCode As Col1, BSName As Col2 From TblBusinessSector Order By BSName;" };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueServiceCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ServiceCode As Col1, ServiceName As Col2 From TblService Order By ServiceName; ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueLevelCode(ref LookUpEdit Lue, string Code)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select LevelCode As Col1, LevelName As Col2 ");
                SQL.AppendLine("From TblLevelHdr ");
                if (Code.Length == 0)
                    SQL.AppendLine("Order By LevelName; ");
                else
                    SQL.AppendLine("Where LevelCode=@Code;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (Code.Length > 0) Sm.CmParam<String>(ref cm, "@Code", Code);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (Code.Length > 0) Sm.SetLue(Lue, Code);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static void SetLueLevelCodeBasedOnPosCode(ref LookUpEdit Lue, string Code, string PosCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.LevelCode As Col1, T.LevelName As Col2 From TblLevelHdr T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where LevelCode=@Code;");
            }
            else
            {
                SQL.AppendLine(" Where 0 = 0 ");

                if (PosCode.Length > 0)
                {
                    SQL.AppendLine("And T.LevelCode In ( ");
                    SQL.AppendLine("    Select Distinct T1.LevelCode ");
                    SQL.AppendLine("    From TblPositionLevel T1 ");
                    SQL.AppendLine("    Where T1.PosCode = @PosCode ");
                    SQL.AppendLine(") ");
                }

                SQL.AppendLine("Order By T.LevelName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #region Cuilin

        internal static void SetLookUpEdit(LookUpEdit Lue, object ds)
        {
            //populate data for LookUpEdit control
            try
            {
                Lue.DataBindings.Clear();
                Lue.Properties.DataSource = ds;
                Lue.Properties.PopulateColumns();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Lue.EditValue = null;
            }
        }

        public static void SetLueOption(ref LookUpEdit Lue, string Category, string[] Value, string[] ChangeValue)
        {
            string OptValue = string.Empty;
            string FilterValue = string.Empty;
            for (int Index = 0; Index < Value.Length; Index++)
                FilterValue = FilterValue + (FilterValue == string.Empty ? "" : ",") + "'" + Value[Index] + "'";

            for (int Index = 0; Index < ChangeValue.Length; Index++)
                OptValue = OptValue + " when OptCode='" + Value[Index] + "' then '" + ChangeValue[Index] + "' ";

            Sm.SetLue2(
                ref Lue,
                " Select " + (OptValue == string.Empty ? " OptCode " : " case " + OptValue + " end ") + " As Col1, " +
                " OptDesc As Col2 From tbloption " +
                " Where OptCat='" + Category + "' " +
                (FilterValue == string.Empty ? "" : " and OptCode IN (" + FilterValue + ") ") +
                " Order By OptCode",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        public static void SetLueAllCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CtCode As Col1, CtName As Col2 From TblCustomer " +
                "Order By CtName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueRingArea(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select RingCode As Col1, RingDesc As Col2 From tblRingArea Order By RingCode",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson Order By SPName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        public static void SetLueShpMCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ShpMCode As Col1, ShpMName As Col2 From TblShipmentMethod Order By ShpMName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        public static void SetLueQtPriceList(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                " Select DISTINCT A.PLQtNo As Col1, A.PLQtDesc As Col2 From tblslspricelisthdr A " +
                " INNER JOIN tblslspricelistdtl B ON A.PLQtNo=B.PLQtNo " +
                " INNER JOIN tblitem C ON B.ItCode=C.ItCode " +
                " WHERE A.ActInd='Y' " +
                " Order By A.PLQtNo DESC",
                10, 35, true, true, "Code", "Name", "Col2", "Col1");

        }

        public static void SetLueCtPsNo(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue2(
                ref Lue,
                " Select CtPsNo As Col1, CtPsName As Col2 From tblcustomercontactperson Where CtCode='" + CtCode + "' " +
                " Order By CtPsName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        public static void SetLueLRevQtNo(ref LookUpEdit Lue, string CtCode, string MyQtNo)
        {
            Sm.SetLue2(
                ref Lue,
                " Select SOQtNo As Col1, Concat(SOQtNo, ' ', IfNull(SOQtDt, '')) As Col2 From tblsoquot Where CtCode='" + CtCode + "' AND SOQtNo <> '" + MyQtNo + "' Order By SOQtDt ",
                15, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCtPtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue3(
                ref Lue,
                " Select PtCode As Col1, PtName As Col2, PtDay As Col3 From tblpaymentterm Order By PtName ",
                0, 35, 0, false, true, false, "Code", "Name", "PtDays", "Col2", "Col1");

        }

        public static void SetLueSOQtNo(ref LookUpEdit Lue, string CtCode, string SOQtNo)
        {
            Sm.SetLue4(
                ref Lue,
                " Select A.SOQtNo As Col1, A.QtTitle As Col2, " +
                " Replace((Select group_concat(concat(round(discrt1, 2), '%+', round(discrt2, 2), '%+', round(discrt3, 2), '%+', " +
                "    round(discrt4, 2), '%+', round(discrt5, 2), '%')) " +
                " From tblsoquotprod B " +
                " Where A.SOQtNo=B.SOQtNo), '+0.00%', '') As Col3, " +
                " (Select group_concat(D.CityName) " +
                " From tblsoquotcity C " +
                " Left Join tblcity D ON C.CityCode=D.CityCode " +
                " Where A.SOQtNo=C.SOQtNo) As Col4 " +
                " From tblsoquot A " +
                " Where A.CtCode='" + CtCode + "' And A.ActInd='Y' And " +
                " A.SOQtNo NOT IN " +
                " (Select SOQtNo FROM tblsoquotapproval where ifnull(Status, '') <> 'A') ",
                30, 35, 30, 30, true, true, true, true, "Code", "Title", "Discount", "Area", "Col2", "Col1");
        }

        public static void SetLueSOQtPrNo(ref LookUpEdit Lue, string CtCode, string SOQtPrNo)
        {

            Sm.SetLue2(
                ref Lue,
                " Select SOQtPrNo As Col1, concat('Period : ', substring(SOQtPrDS, 7,2), '/', " +
                " substring(SOQtPrDS, 5,2), '/', substring(SOQtPrDS, 1,4), ' to ', " +
                " substring(SOQtPrDE, 7,2), '/', substring(SOQtPrDE, 5,2), '/', " +
                " substring(SOQtPrDE, 1,4)) As Col2 From tblsoquotpromo Where CtCode='" + CtCode + "' " +
                " And SOQtPrNo NOT IN (Select SOQtPrNo FROM tblsoquotpromoapproval where ifnull(Status, '') <> 'A') " +
                " Order By SOQtPrNo",
                30, 35, true, true, "Code", "Name", "Col2", "Col1");

        }

        public static void SetLueLRevSONo(ref LookUpEdit Lue, string CtCode, string MySONo)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DISTINCT SONo As Col1, CustPONO As Col2 From TblSOHdr WHERE IsGlobal='Y' AND Status<>'C' AND CtCode='" + CtCode + "' Order By CustPONo ",
                30, 35, false, true, "Code", "Customer PO No", "Col2", "Col1");

        }

        public static void SetLueSOPdCode(ref LookUpEdit Lue, string SOQtNo, string ItCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct ItCode As Col1, ItName As Col2 From (" +
                "   Select Distinct A.ItCode, B.ItName " +
                "   From tblsoquotprod A, tblitem B " +
                "   Where A.ItCode=B.ItCode And A.SOQtNo='" + SOQtNo + "' " +
                (ItCode.Length == 0 ? "" : "Union All Select ItCode, ItName From tblitem Where ItCode='" + ItCode + "' ") +
                ") T Order By ItCode",
                30, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueSOAgtCode(ref LookUpEdit Lue, string CtCode, string SOQtNo, string AgtCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct AgtCode As Col1, AgtName As Col2 From (" +
                "   Select AgtCode, AgtName From tblagent " +
                "   Where ActInd='Y' AND CtCode='" + CtCode + "' " +
                "   And SCityCd In (Select CityCode From tblsoquotcity Where SOQtNo='" + SOQtNo + "') " +
                (AgtCode.Length == 0 ? "" : "Union All Select AgtCode, AgtName From tblagent Where ActInd='Y' AND AgtCode='" + AgtCode + "' ") +
                ") T Order By AgtName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueAgtCityCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct SCityCd Col1, B.CityName Col2 From TblAgent A, TblCity B " +
                "Where A.SCityCd=B.CityCode " +
                "And A.CtCode='" + CtCode + "' " +
                "Order By CityName",
                10, 35,
                false, true,
                "City Code", "City Name",
                "Col2", "Col1");
        }

        public static void SetLueTrFeeUOMALL(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct A.UOMCode Col1, B.UomName Col2 From TblTransportFee A, TblUom B " +
                "Where A.UOMCode=B.UomCode " +
                "Order By B.UomName",
                10, 35,
                false, true,
                "Uom Code", "Uom Name",
                "Col2", "Col1");
        }

        public static void SetLueTrFeeUOM(ref LookUpEdit Lue, string TrptNo, string TrTpNo, string CityCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select Distinct A.UOMCode, B.UomName From TblTransportFee A, TblUom B " +
                "Where A.UOMCode=B.UomCode And A.TrptNo='" + TrptNo + "' And A.TrTpNo='" + TrTpNo + "' " +
                " AND CityCode='" + CityCode + "' " +
                "Order By B.UomName",
                10, 35,
                false, true,
                "Uom Code", "Uom Name",
                "Col2", "Col1");

        }

        public static void SetLueTrTpNo(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TrTpNo Col1, TrTpDesc Col2 From TblTransportType Order By TrTpDesc",
                10, 35,
                false, true,
                "Code", "Name",
                "Col2", "Col1");
        }

        public static void SetLueTrTpNo(ref LookUpEdit Lue, string SchdDate, string TrptNo)
        {
            Sm.SetLue2(
                ref Lue,
                    "Select A.TrTpNo As Col1, B.TrTpDesc As Col2 From TblTransportSchedule A, TblTransportType B " +
                    "Where A.TrTpNo=B.TrTpNo And A.SchdDate='" + SchdDate.Substring(0, 8) + "' " +
                    "And A.TrptNo='" + TrptNo + "' " +
                    "Order By B.TrTpDesc",
                10, 35,
                false, true,
                "Code", "Name",
                "Col2", "Col1");
        }


        public static void SetLueTrptNO(ref LookUpEdit Lue, string SchdDate)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DISTINCT A.TrptNo As Col1, B.TrptName As Col2 From TblTransportSchedule A, TblTransporter B " +
                "Where A.TrptNo=B.TrptNo And A.SchdDate='" + SchdDate.Substring(0, 8) + "' Order By TrptName",
                10, 35,
                false, true,
                "Code", "Name",
                "Col2", "Col1");
        }


        public static void SetLueTrptNo(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TrptNo Col1, TrptName+' ('+TrptNo+')' Col2 From TblTransporter Order By TrptName",
                10, 35,
                false, true,
                "Code", "Transporter Name",
                "Col2", "Col1");
        }

        public static void SetLueTrptNOByFee(ref LookUpEdit Lue, string SchdDate, string TrTpNo, string UOMCode, string CityCode, string TrFee, string LoadingCityCode)
        {
            Sm.SetLue2(
                ref Lue,
                        "Select A.TrptNo As Col1, B.TrptName As Col2 From TblTransportSchedule A " +
                        "INNER JOIN TblTransporter B ON A.TrptNo=B.TrptNo " +
                        "INNER JOIN TblTransportFee C ON A.TrptNo=C.TrptNo AND A.TrTpNo='" + TrTpNo +
                        "' AND UOMCode='" + UOMCode + "' AND C.CityCode='" + CityCode + "' " +
                        "WHERE A.SchdDate='" + SchdDate.Substring(0, 8) + "' AND C.TrFee=" + TrFee +
                        " AND C.DepartFr='" + LoadingCityCode + "' " +
                        " Limit 1 " +
                        " Order By TrptName",
                15, 35,
                false, true,
                "Code", "Transporter Name",
                "Col2", "Col1");
        }

        public static void SetLueLoadingCityCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DISTINCT WH.WHSCode Col1, " +
                "WH.WHSName Col2 " +
                "From TblWarehouse WH " +
                "INNER JOIN TblCity A ON WH.CityCode=A.CityCode " +
                "Where A.ActInd='Y' " +
                "Order By Col2",
                0, 35,
                false, true,
                "City Code", "City Name",
                "Col2", "Col1");
        }

        #endregion

        #region DMK

        public static void SetLueBrCode(ref LookUpEdit Lue, string Code)
        {
            SetLueWithValue(ref Lue, Code,
                "Select BrCode As Col1, BrName As Col2 From TblBrand " +
                (Code.Length == 0 ? " Where ActInd='Y' Order By BrName;" : " Where BrCode=@Code;"));
        }

        public static void SetLueAntennaCode(ref LookUpEdit Lue, string Code)
        {
            SetLueWithValue(ref Lue, Code,
                "Select AntennaCode As Col1, AntennaName As Col2 From TblAntenna " +
                (Code.Length == 0 ? " Where ActInd='Y' Order By AntennaName;" : " Where AntennaCode=@Code;"));
        }

        public static void SetLueChannelWidthCode(ref LookUpEdit Lue, string Code)
        {
            SetLueWithValue(ref Lue, Code,
                "Select ChannelWidthCode As Col1, ChannelWidthName As Col2 From TblChannelWidth " +
                (Code.Length == 0 ? " Where ActInd='Y' Order By ChannelWidthName;" : " Where ChannelWidthCode=@Code;"));
        }

        public static void SetLueWLanCardCode(ref LookUpEdit Lue, string Code)
        {
            SetLueWithValue(ref Lue, Code,
                "Select WLanCardCode As Col1, WLanCardName As Col2 From TblWLanCard " +
                (Code.Length == 0 ? " Where ActInd='Y' Order By WLanCardName;" : " Where WLanCardCode=@Code;"));
        }

        public static void SetLueBTSCode(ref LookUpEdit Lue, string Code)
        {
            SetLueWithValue(ref Lue, Code,
                "Select BTSCode As Col1, BTSName As Col2 From TblBTS " +
                (Code.Length == 0 ? " Where ActInd='Y' Order By BTSName;" : " Where BTSCode=@Code;"));
        }

        public static void SetLueAPModelCode(ref LookUpEdit Lue, string Code)
        {
            SetLueWithValue(ref Lue, Code,
                "Select APModelCode As Col1, APModelName As Col2 From TblAPModel " +
                (Code.Length == 0 ? " Where ActInd='Y' Order By APModelName;" : " Where APModelCode=@Code;"));
        }

        #endregion
    }
}
