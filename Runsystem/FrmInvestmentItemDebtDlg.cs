﻿#region Update
/*
    20/04/2022 [RDA/PRODUCT] new dialog
    24/05/2022 [SET/PRODUCT] tambahan Last coupon date & Next coupon date
                             merubah Annual days assumption menjadi Lue
    27/06/2022 [IBL/PRODUCT] PortofolioID yg udah dipake, ngga bisa ditarik lagi
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentItemDebtDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmInvestmentItemDebt mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmInvestmentItemDebtDlg(FrmInvestmentItemDebt FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Investment"+Environment.NewLine+"Code",
                        "Investment"+Environment.NewLine+"Name",
                        "Investment"+Environment.NewLine+"Category Code",
                        "Investment"+Environment.NewLine+"Category",
                        "Currency"+Environment.NewLine+"Code",

                        //6-10
                        "Currency",
                        "Stock"+Environment.NewLine+"Exchange",
                        "Maturity"+Environment.NewLine+"Date",
                        "Issuer",
                        "Interest"+Environment.NewLine+"Rate",

                        //11-15
                        "Interest"+Environment.NewLine+"Type",
                        "Interest"+Environment.NewLine+"Frequency",
                        "UpdateDt",
                        "CreateBy",
                        "CreateDt",

                        //16-17
                        "Listing"+Environment.NewLine+"Date",
                        "Issued"+Environment.NewLine+"Amount"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 150, 100, 150, 100,

                        //6-10
                        150, 200, 150, 250, 150,

                        //11-15
                        150, 150, 150, 150, 150,

                        //16-17
                        150, 150
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdFormatDate(Grd1, new int[] { 8, 13, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 13, 14, 15, 16, 17 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT ");
            SQL.AppendLine("A.PortofolioId, A.PortofolioName, A.InvestmentCtCode, B.InvestmentCtName, C.CurCode, ");
            SQL.AppendLine("C.CurName, A.StockExchange, A.MaturityDt, A.Issuer, A.InterestRate, ");
            SQL.AppendLine("A.InterestType, A.InterestFreq, A.UpdateDt, A.CreateBy, A.CreateDt, A.ListingDt, A.IssuedAmt ");
            SQL.AppendLine("From TblInvestmentPortofolio A ");
            SQL.AppendLine("Inner Join TblInvestmentCategory B ON A.InvestmentCtCode = B.InvestmentCtCode ");
            SQL.AppendLine("Inner Join TblCurrency C ON A.CurCode = C.CurCode ");
            SQL.AppendLine("Where Find_In_Set(A.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine("And A.portofolioId Not in ( ");
            SQL.AppendLine("    Select PortofolioId From TblInvestmentItemDebt Where ActInd = 'Y' ");
            SQL.AppendLine(")");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                string Filter2 = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DebtInvestmentCtCode", mFrmParent.mDebtInvestmentCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtInvestment.Text, new string[] { "A.PortofolioId", "A.PortofolioName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + Filter2 + " Order By A.PortofolioId;",
                        new string[]
                        { 
                            //0
                            "PortofolioId",

                            //1-5
                            "PortofolioName", "InvestmentCtCode", "InvestmentCtName", "CurCode", "CurName",

                            //6-10
                            "StockExchange", "MaturityDt", "Issuer", "InterestRate", "InterestType",

                            //11-15
                            "InterestFreq", "UpdateDt", "CreateBy", "CreateDt", "ListingDt",

                            //16
                            "IssuedAmt"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0); //PortofolioId
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1); //PortofolioName
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2); //InvestmentCtCode
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3); //InvestmentCtName
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4); //CurCode
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5); //CurName
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6); //StockExchange
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7); //MaturityDt
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8); //Issuer
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9); //InterestRate
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10); //InterestType
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11); //InterestFreq
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12); //UpdateDt
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13); //CreateBy
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14); //CreateDt
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15); //ListingDt
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16); //IssuedAmt
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtPortofolioId.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtInvestmentName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.SetLueInvestmentCtCode(ref mFrmParent.LueInvestmentCtCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3));
                mFrmParent.SetLueCurCode(ref mFrmParent.LueCurCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5));
                Sm.SetDte(mFrmParent.DteMaturityDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 8));
                mFrmParent.TxtIssuer.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                mFrmParent.TxtInterestRateAmt.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10);
                mFrmParent.TxtInterestType.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11);
                mFrmParent.TxtInterestFreq.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 12);
                Sm.SetDte(mFrmParent.DteUpdateDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 13));
                Sm.SetDte(mFrmParent.DteListingDt, Sm.GetGrdDate(Grd1, Grd1.CurRow.Index, 16));
                mFrmParent.TxtIssuedAmt.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 17);
                //Sm.FormatNumTxt(mFrmParent.TxtDaysAssumption, 0);

                this.Close();
            }
        }


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void TxtInvestment_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvestment_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment");
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion


    }
}
