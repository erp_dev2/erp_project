﻿#region Update
/* 
  17/02/2020 [HAR/IMS] : leave hour ambil dari leeave yg type nya cuti ijin pribadi, tambah info break 
 * 24/02/2020 [HAR/IMS] : bug filter
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave4ProcessFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmEmpLeave4Process mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpLeave4ProcessFind(FrmEmpLeave4Process FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, C.EmpCode, D.EmpCodeOld, D.EmpName, ");
            SQL.AppendLine("E.DeptName, F.PosName,G.SiteName, C.StartDt, B.AcTualStartTm,  ");
            SQL.AppendLine("B.AcTualEndTm, H.LeaveName, B.leaveDocNo, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmpLeaveHourProcessHdr A ");
            SQL.AppendLine("Inner Join TblEmpleaveHourProcessDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmpleaveHdr C On B.LeaveHourDocNo = C.DocNo  ");
            SQL.AppendLine("Inner Join TblEmployee D On C.EmpCode = D.EmpCode  ");
            SQL.AppendLine("Left Join TblDepartment E On D.DeptCode = E.DeptCode  ");
            SQL.AppendLine("Left Join Tblposition F On D.Poscode = F.PosCode ");
            SQL.AppendLine("Left Join TblSite G On D.SiteCode = G.SiteCode ");
            SQL.AppendLine("Left Join TblLeave H On B.LeaveCode = H.LeaveCode ");


            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel", 
                        "Employee"+Environment.NewLine+"Code", 
                        "Employee "+Environment.NewLine+" Code Old",
                        //6-10
                        "Employee",
                        "Department", 
                        "Position",
                        "Site", 
                        "Start"+Environment.NewLine+"Date",
                        //11-15
                        "Actual"+Environment.NewLine+"Start Time",
                        "AcTual"+Environment.NewLine+"End Time", 
                        "Leave", 
                        "Leave"+Environment.NewLine+"Document",
                        "Created"+Environment.NewLine+"By",
                        //16-20
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        130, 100, 80, 100, 100, 
                        //6-10
                        150, 150, 120, 120, 100, 
                        //11-15
                        100, 100, 100, 100, 100, 
                        //16-20
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 10, 16, 19 });
            Sm.GrdFormatTime(Grd1, new int[] { 17, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "D.EmpCodeOld", "D.EmpName", });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "D.DeptCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By A.DocNo, D.EmpName;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            //1-5
                            "DocDt", "CancelInd", "EmpCode", "EmpCodeOld", "EmpName",
                            //6-10
                            "DeptName", "PosName","SiteName", "StartDt", "AcTualStartTm", 
                            //11-15
                            "AcTualEndTm", "LeaveName", "leaveDocNo", "CreateBy", "CreateDt", 
                            //16-17
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion
    }
}
