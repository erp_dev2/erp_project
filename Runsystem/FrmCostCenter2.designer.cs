﻿namespace RunSystem
{
    partial class FrmCostCenter2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCostCenter2));
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueCostCenterCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkCOA = new DevExpress.XtraEditors.CheckEdit();
            this.ChkCostCategory = new DevExpress.XtraEditors.CheckEdit();
            this.TxtCostCenterNew = new DevExpress.XtraEditors.TextEdit();
            this.TxtParent = new DevExpress.XtraEditors.TextEdit();
            this.BtnCOA = new DevExpress.XtraEditors.SimpleButton();
            this.LueParent = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkItemCostCategory = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCostCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostCenterNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItemCostCategory.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkItemCostCategory);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueParent);
            this.panel2.Controls.Add(this.BtnCOA);
            this.panel2.Controls.Add(this.TxtParent);
            this.panel2.Controls.Add(this.TxtCostCenterNew);
            this.panel2.Controls.Add(this.ChkCostCategory);
            this.panel2.Controls.Add(this.ChkCOA);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueCostCenterCode);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(47, 50);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(101, 14);
            this.label15.TabIndex = 27;
            this.label15.Text = "New Cost Center";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(26, 28);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 14);
            this.label8.TabIndex = 25;
            this.label8.Text = "Cost Center\'s Source";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCostCenterCode
            // 
            this.LueCostCenterCode.EnterMoveNextControl = true;
            this.LueCostCenterCode.Location = new System.Drawing.Point(154, 26);
            this.LueCostCenterCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCostCenterCode.Name = "LueCostCenterCode";
            this.LueCostCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterCode.Properties.Appearance.Options.UseFont = true;
            this.LueCostCenterCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCostCenterCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCostCenterCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCostCenterCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCostCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCostCenterCode.Properties.DropDownRows = 20;
            this.LueCostCenterCode.Properties.MaxLength = 40;
            this.LueCostCenterCode.Properties.NullText = "[Empty]";
            this.LueCostCenterCode.Properties.PopupWidth = 500;
            this.LueCostCenterCode.Size = new System.Drawing.Size(374, 20);
            this.LueCostCenterCode.TabIndex = 26;
            this.LueCostCenterCode.ToolTip = "F4 : Show/hide list";
            this.LueCostCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCostCenterCode.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.LueCostCenterCode_EditValueChanging);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(69, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 14);
            this.label1.TabIndex = 29;
            this.label1.Text = "Parent\'s COA";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCOA
            // 
            this.ChkCOA.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCOA.Location = new System.Drawing.Point(151, 115);
            this.ChkCOA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCOA.Name = "ChkCOA";
            this.ChkCOA.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCOA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCOA.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCOA.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCOA.Properties.Appearance.Options.UseFont = true;
            this.ChkCOA.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCOA.Properties.Caption = "COA";
            this.ChkCOA.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCOA.Size = new System.Drawing.Size(76, 22);
            this.ChkCOA.TabIndex = 31;
            // 
            // ChkCostCategory
            // 
            this.ChkCostCategory.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCostCategory.Location = new System.Drawing.Point(151, 138);
            this.ChkCostCategory.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCostCategory.Name = "ChkCostCategory";
            this.ChkCostCategory.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCostCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCostCategory.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCostCategory.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCostCategory.Properties.Appearance.Options.UseFont = true;
            this.ChkCostCategory.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCostCategory.Properties.Caption = "Cost Category";
            this.ChkCostCategory.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCostCategory.Size = new System.Drawing.Size(153, 22);
            this.ChkCostCategory.TabIndex = 32;
            // 
            // TxtCostCenterNew
            // 
            this.TxtCostCenterNew.EnterMoveNextControl = true;
            this.TxtCostCenterNew.Location = new System.Drawing.Point(154, 48);
            this.TxtCostCenterNew.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCostCenterNew.Name = "TxtCostCenterNew";
            this.TxtCostCenterNew.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCostCenterNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCostCenterNew.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCostCenterNew.Properties.Appearance.Options.UseFont = true;
            this.TxtCostCenterNew.Properties.MaxLength = 255;
            this.TxtCostCenterNew.Size = new System.Drawing.Size(374, 20);
            this.TxtCostCenterNew.TabIndex = 33;
            // 
            // TxtParent
            // 
            this.TxtParent.EnterMoveNextControl = true;
            this.TxtParent.Location = new System.Drawing.Point(154, 92);
            this.TxtParent.Margin = new System.Windows.Forms.Padding(5);
            this.TxtParent.Name = "TxtParent";
            this.TxtParent.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParent.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParent.Properties.Appearance.Options.UseFont = true;
            this.TxtParent.Properties.MaxLength = 80;
            this.TxtParent.Properties.ReadOnly = true;
            this.TxtParent.Size = new System.Drawing.Size(226, 20);
            this.TxtParent.TabIndex = 34;
            // 
            // BtnCOA
            // 
            this.BtnCOA.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCOA.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCOA.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCOA.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCOA.Appearance.Options.UseBackColor = true;
            this.BtnCOA.Appearance.Options.UseFont = true;
            this.BtnCOA.Appearance.Options.UseForeColor = true;
            this.BtnCOA.Appearance.Options.UseTextOptions = true;
            this.BtnCOA.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCOA.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCOA.Image = ((System.Drawing.Image)(resources.GetObject("BtnCOA.Image")));
            this.BtnCOA.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCOA.Location = new System.Drawing.Point(384, 91);
            this.BtnCOA.Name = "BtnCOA";
            this.BtnCOA.Size = new System.Drawing.Size(24, 21);
            this.BtnCOA.TabIndex = 47;
            this.BtnCOA.ToolTip = "List Of COA\'s Account#";
            this.BtnCOA.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCOA.ToolTipTitle = "Run System";
            this.BtnCOA.Click += new System.EventHandler(this.BtnCOA_Click);
            // 
            // LueParent
            // 
            this.LueParent.EnterMoveNextControl = true;
            this.LueParent.Location = new System.Drawing.Point(154, 70);
            this.LueParent.Margin = new System.Windows.Forms.Padding(5);
            this.LueParent.Name = "LueParent";
            this.LueParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.Appearance.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParent.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParent.Properties.DropDownRows = 20;
            this.LueParent.Properties.MaxLength = 40;
            this.LueParent.Properties.NullText = "[Empty]";
            this.LueParent.Properties.PopupWidth = 500;
            this.LueParent.Size = new System.Drawing.Size(374, 20);
            this.LueParent.TabIndex = 49;
            this.LueParent.ToolTip = "F4 : Show/hide list";
            this.LueParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParent.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.LueParent_EditValueChanging);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(28, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 14);
            this.label3.TabIndex = 50;
            this.label3.Text = "Cost Center\'s Parent";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkItemCostCategory
            // 
            this.ChkItemCostCategory.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkItemCostCategory.Location = new System.Drawing.Point(151, 160);
            this.ChkItemCostCategory.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkItemCostCategory.Name = "ChkItemCostCategory";
            this.ChkItemCostCategory.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkItemCostCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItemCostCategory.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkItemCostCategory.Properties.Appearance.Options.UseBackColor = true;
            this.ChkItemCostCategory.Properties.Appearance.Options.UseFont = true;
            this.ChkItemCostCategory.Properties.Appearance.Options.UseForeColor = true;
            this.ChkItemCostCategory.Properties.Caption = "Item Cost Category";
            this.ChkItemCostCategory.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItemCostCategory.Size = new System.Drawing.Size(153, 22);
            this.ChkItemCostCategory.TabIndex = 51;
            // 
            // FrmCostCenter2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmCostCenter2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCostCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCostCenterNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItemCostCategory.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueCostCenterCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkCOA;
        private DevExpress.XtraEditors.CheckEdit ChkCostCategory;
        private DevExpress.XtraEditors.TextEdit TxtCostCenterNew;
        public DevExpress.XtraEditors.SimpleButton BtnCOA;
        internal DevExpress.XtraEditors.LookUpEdit LueParent;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.CheckEdit ChkItemCostCategory;
        internal DevExpress.XtraEditors.TextEdit TxtParent;

    }
}