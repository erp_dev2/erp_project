﻿#region Update
/*
    28/12/2022 [IBL/BBT] New Apps
    12/01/2023 [IBL/BBT] Feedback : kurang validasi mutated qty tidak boleh lebh dari stock qty
    13/02/2023 [IBL/BBT] Update Inventory qty dan UPrice di master property inventory saat Property Inventory Mutataion diapprove
    11/04/2023 [WED/BBT] mutated from bisa dibuat many-to-many
    04/05/2023 [SET/BBT] Nama Menu untuk muncul dari transaksi lain
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryMutation : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mMainCurCode = string.Empty,
            mDocNo = string.Empty;
        private string
            mEntCode = string.Empty,
            mWhsCodeForPropertyInventoryMutation = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mIsFilterByDept = string.Empty;
        private bool
            mIsBatchNoUseDocDtIfEmpty = false,
            mIsAutoJournalActived = false,
            mIsPropInventoryMutationAllowToUploadFile = false,
            mIsTransactionUseDetailApprovalInformation = false;
        public string
            mDocType1 = "57",
            mDocType2 = "58";

        iGCell fCell;
        bool fAccept;
        private byte[] downloadedData;

        internal FrmPropertyInventoryMutationFind FrmFind;

        #endregion

        #region Constructor

        public FrmPropertyInventoryMutation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Property Inventory Mutation";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Property Code",
                        "Property Name",
                        "Property Category",
                        "",
                        
                        //6-10
                        "Item's Code",
                        "Item's Name",
                        "Unit Price",
                        "Stock Value",
                        "UoM",

                        //11-15
                        "Stock" + Environment.NewLine + "Quantity",
                        "Mutated" + Environment.NewLine + "Quantity",
                        "UoM",
                        "Stock" + Environment.NewLine + "Quantity",
                        "Mutated" + Environment.NewLine + "Quantity",
                        
                        //16-20
                        "UoM",
                        "Stock" + Environment.NewLine + "Quantity",
                        "Mutated" + Environment.NewLine + "Quantity",
                        "Mutated" + Environment.NewLine + "Amount",
                        "Remaining" + Environment.NewLine + "Stock Qty",

                        //21-25
                        "Remaining" + Environment.NewLine + "Stock Value",
                        "Property Category Code",
                        "Cost Center Code",
                        "Cost Center",
                        "Site Code",

                        //26
                        "Site"

                    },
                     new int[]
                    {
                        //0
                        0,

                        //1-5
                        20, 200, 200, 200, 20,

                        //6-10
                        120, 200, 100, 100, 80, 

                        //11-15
                        100, 100, 80, 100, 100,

                        //16-20
                        80, 100, 100, 100, 100, 
                        
                        //21-25
                        100, 0, 0, 180, 0,

                        //26
                        180
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 11, 12, 14, 15, 17, 18, 19, 20, 21 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 6, 13, 14, 15, 16, 17, 18, 22, 23, 25 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });
            Grd1.Cols[26].Move(5);
            Grd1.Cols[24].Move(5);
            Grd1.Cols[8].Move(11);
            Grd1.Cols[9].Move(12);

            #endregion

            #region Grd2

            Grd2.Cols.Count = 17;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Property Code",
                        "Property Name",
                        "Property Category" + Environment.NewLine + "Code",
                        "Property Category",
                        
                        //6-10
                        "",
                        "Item's Code",
                        "Item's Name",
                        "Unit Price" + Environment.NewLine + "(Based on Formula)",
                        "UoM",

                        //11-15
                        "Mutated" + Environment.NewLine + "Quantity",
                        "UoM",
                        "Mutated" + Environment.NewLine + "Quantity",
                        "UoM",
                        "Mutated" + Environment.NewLine + "Quantity",

                        //16
                        "Mutated" + Environment.NewLine + "Amount",

                    },
                     new int[]
                    {
                        //0
                        0,

                        //1-5
                        20, 200, 200, 200, 200,

                        //6-10
                        20, 120, 200, 130, 80, 

                        //11-15
                        100, 80, 100, 80, 100, 

                        //16
                        100,
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1, 6 });
            Sm.GrdFormatDec(Grd2, new int[] { 9, 11, 13, 15, 16 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 4, 7, 12, 13, 14, 15 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Grd2.Cols[9].Move(10);

            #endregion

            #region Grd3
            Grd3.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[]
                {
                    //0
                    "No",

                    //1-4
                    "User",
                    "Position",
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[] { 50, 150, 150, 100, 80, 200 }
            );
            Sm.GrdFormatDate(Grd3, new int[] { 4 });
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{
                        TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, ChkCancelInd, MeeRemark,
                        TxtTotal1A, TxtTotal2A, TxtTotal1B, TxtTotal2B, TxtFile, TxtFile2, TxtFile3,
                        ChkFile, ChkFile2, ChkFile3
                    }, true);
                    BtnFile.Enabled = BtnFile2.Enabled = BtnFile3.Enabled = false;
                    ChkFile.Checked = ChkFile2.Checked = ChkFile3.Checked = false;
                    if (TxtFile.Text.Length == 0) BtnDownload.Enabled = false;
                    else BtnDownload.Enabled = true;
                    if (TxtFile2.Text.Length == 0) BtnDownload2.Enabled = false;
                    else BtnDownload2.Enabled = true;
                    if (TxtFile3.Text.Length == 0) BtnDownload3.Enabled = false;
                    else BtnDownload3.Enabled = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeRemark, ChkFile, ChkFile2, ChkFile3 }, false);
                    BtnFile.Enabled = BtnFile2.Enabled = BtnFile3.Enabled = true;
                    ChkFile.Checked = ChkFile2.Checked = ChkFile3.Checked = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 12 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 3, 6, 11 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, ChkCancelInd, MeeRemark,
                TxtTotal1A, TxtTotal2A, TxtTotal1B, TxtTotal2B, TxtFile, TxtFile2, TxtFile3
            });
            ChkFile.Checked = ChkFile2.Checked = ChkFile3.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTotal1A, TxtTotal2A, TxtTotal1B, TxtTotal2B }, 0);

            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 11, 12, 19, 20, 21 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 9, 11, 16 });

            Sm.ClearGrd(Grd3, true);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 13, 14, 15, 16, 17, 18, 22 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 4, 7, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPropertyInventoryMutationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PropertyInventoryMutation", "TblPropertyInventoryMutationHdr");
            string JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
            GetEntCode(Sm.GetGrdStr(Grd1, 0, 2));

            var cml = new List<MySqlCommand>();

            cml.Add(SavePropertyInventoryMutationHdr(DocNo));
            if (Grd1.Rows.Count > 1)
            {
                cml.Add(SavePropertyInventoryMutationDtl(DocNo));
                cml.Add(SaveStockMovement1("N", DocNo));
                cml.Add(SaveStockSummary1a(DocNo));
                cml.Add(SaveStockPrice1(DocNo, Sm.GetDte(DteDocDt)));
            }

            if (Grd2.Rows.Count > 1)
            {
                cml.Add(SavePropertyInventoryMutationDtl2(DocNo));
                cml.Add(SaveStockMovement2("N", DocNo));
                cml.Add(SaveStockSummary2a(DocNo));
                cml.Add(SaveStockPrice2(DocNo, Sm.GetDte(DteDocDt)));
            }

            cml.Add(UpdatePropertyInventoryRemainingValue(DocNo, "N"));
            cml.Add(UpdatePropertyInventory(DocNo, "N"));
            cml.Add(SavePropertyInventory(DocNo));

            if (mIsAutoJournalActived)
                cml.Add(SaveJournal(DocNo, JournalDocNo));

            Sm.ExecCommands(cml);

            if (mIsPropInventoryMutationAllowToUploadFile)
            {
                if (TxtFile.Text.Length > 1)
                    UploadFile(DocNo, TxtFile, PbUpload, "");
                if (TxtFile2.Text.Length > 1)
                    UploadFile(DocNo, TxtFile2, PbUpload2, "2");
                if (TxtFile3.Text.Length > 1)
                    UploadFile(DocNo, TxtFile3, PbUpload3, "3");
            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsMeeEmpty(MeeRemark, "Remarks") ||
                IsPropertyCodeOnOtherOutstandingDoc() ||
                IsGrdEmpty(Grd1) ||
                IsGrdEmpty(Grd2) ||
                IsGrdValueNotValid() ||
                IsGrdValueNotValid2() ||
                IsTotalInvalid() ||
                IsUploadFileNotValid();
        }

        private bool IsPropertyParent(int Row)
        {
            return Sm.IsDataExist("Select 1 From TblPropertyInventoryHdr Where PropertyCode = @Param And Parent Is Null;", Sm.GetGrdStr(Grd1, Row, 2));
        }

        private bool IsPropertyCodeOnOtherOutstandingDoc()
        {
            var SQL = new StringBuilder();

            string PropCode = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (PropCode.Length > 0) PropCode += ",";
                PropCode += Sm.GetGrdStr(Grd1, i, 2);
            }

            SQL.AppendLine("Select A.DocNo From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where Find_In_Set(B.PropertyCode, @Param) And A.CancelInd ='N' And A.Status = 'O' Limit 1; ");

            string MutationDocNo = Sm.GetValue(SQL.ToString(), PropCode);
            if (MutationDocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "One or more property code already has a Property Inventory Mutation document that is still outstanding." + Environment.NewLine +
                    "Mutation Doc# : " + MutationDocNo
                    //+ Environment.NewLine + "Property Code : " + Sm.GetGrdStr(Grd1, 0, 2)
                    );
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty(iGrid Grd)
        {
            if (Grd.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty, PropCode = string.Empty, PropName = string.Empty;
            decimal MutatedQty = 0m, StockQty = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 6, false, "Item is empty.")) return true;

                PropCode = Sm.GetGrdStr(Grd1, Row, 2);
                PropName = Sm.GetGrdStr(Grd1, Row, 3);
                MutatedQty = Sm.GetGrdDec(Grd1, Row, 12);
                StockQty = Sm.GetGrdDec(Grd1, Row, 11);

                Msg =
                    "Mutated From " + Environment.NewLine +
                    "Row           : " + Sm.Right(string.Concat("000", (Row + 1).ToString()), 3) + Environment.NewLine +
                    "Property Code : " + PropCode + Environment.NewLine +
                    "Property Name : " + PropName + Environment.NewLine + Environment.NewLine;

                if (MutatedQty <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Mutated Quantity should be greater than 0.");
                    return true;
                }

                if (IsPropertyParent(Row) && MutatedQty != StockQty)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Mutated Quantity should be equal to Stock Quantity.");
                    return true;
                }

                if (!IsPropertyParent(Row) && MutatedQty > StockQty)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Mutated Quantity should not be more than Stock Quantity.");
                    return true;
                }

            }
            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 7, false, "Item is empty.")) return true;
                Msg =
                    "Mutated To " + Environment.NewLine +
                    "Row           : " + Sm.Right(string.Concat("000", (Row + 1).ToString()), 3) + Environment.NewLine +
                    "Property Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                    "Property Name : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine + Environment.NewLine;

                if (Sm.IsGrdValueEmpty(Grd2, Row, 3, false, "Property Name is empty.")) return true;

                if (Sm.GetGrdDec(Grd2, Row, 11) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Mutated Quantity should be greater than 0.");
                    return true;
                }
            }
            return false;
        }

        private bool IsTotalInvalid()
        {
            if (!IsPropertyParent(0))
            {
                if (Sm.FormatNum(TxtTotal1B.Text, 0) != Sm.FormatNum(TxtTotal1A.Text, 0))
                {
                    Sm.StdMsg(mMsgType.Warning, "Mutated Quantity To should be equal to Mutated Quantity From.");
                    return true;
                }
            }

            return false;
        }

        private bool IsUploadFileNotValid()
        {
            return
                IsUploadFileNotValid(TxtFile.Text) ||
                IsUploadFileNotValid(TxtFile2.Text) ||
                IsUploadFileNotValid(TxtFile3.Text);
        }

        private string GetDeptCode()
        {
            var DeptCodeSQL = new StringBuilder();
            DeptCodeSQL.AppendLine("Select B.DeptCode ");
            DeptCodeSQL.AppendLine("From TblPropertyInventoryHdr A ");
            DeptCodeSQL.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            DeptCodeSQL.AppendLine("Where A.PropertyCode = @Param ");

            return Sm.GetValue(DeptCodeSQL.ToString(), Sm.GetGrdStr(Grd1, 0, 2));
        }

        private string GetSiteCode()
        {
            var SiteCodeSQL = new StringBuilder();
            SiteCodeSQL.AppendLine("Select SiteCode ");
            SiteCodeSQL.AppendLine("From TblPropertyInventoryHdr ");
            SiteCodeSQL.AppendLine("Where PropertyCode = @Param ");

            return Sm.GetValue(SiteCodeSQL.ToString(), Sm.GetGrdStr(Grd1, 0, 2));
        }

        private MySqlCommand SavePropertyInventoryMutationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPropertyInventoryMutationHdr(DocNo, DocDt, Status, CancelInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'N', @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='PropertyInventoryMutation' ");
            SQL.AppendLine("And T.DeptCode = @DeptCode ");
            SQL.AppendLine("And T.SiteCode = @SiteCode; ");

            SQL.AppendLine("Update TblPropertyInventoryMutationHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation'");
            SQL.AppendLine("    And DocNo = @DocNo");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", GetDeptCode());
            Sm.CmParam<String>(ref cm, "@SiteCode", GetSiteCode());
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePropertyInventoryMutationDtl(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblPropertyInventoryMutationDtl(DocNo, DNo, PropertyCode, ItCode, Stock, Stock2, Stock3, Qty, Qty2, Qty3, UPrice, CurCode, ");
            SQL.AppendLine("StockValue, MutatedAmt, PropCode, BatchNo, Source, Lot, Bin, CreateBy, CreateDt)");
            SQL.AppendLine("Values");
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + i.ToString() + ", @PropertyCode_" + i.ToString() + ", @ItCode_" + i.ToString() + ", @Stock_" + i.ToString() + ", @Stock2_" + i.ToString() + ", @Stock3_" + i.ToString() + ", @Qty_" + i.ToString() + ", @Qty2_" + i.ToString() + ", @Qty3_" + i.ToString());
                    SQL.AppendLine(", @UPrice_" + i.ToString() + ", 'IDR', @StockValue_" + i.ToString() + ", @MutatedAmt_" + i.ToString() + ", '-', " + (mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'") + ", @Source_" + i.ToString() + ", '-', '-', @CreateBy, CurrentDateTime())");
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("0000" + (i + 1).ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@PropertyCode_" + i.ToString(), Sm.GetGrdStr(Grd1, i, 2));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + i.ToString(), Sm.GetGrdStr(Grd1, i, 6));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 8));
                    Sm.CmParam<Decimal>(ref cm, "@StockValue_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 9));
                    Sm.CmParam<Decimal>(ref cm, "@Stock_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 11));
                    Sm.CmParam<Decimal>(ref cm, "@Stock2_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 14));
                    Sm.CmParam<Decimal>(ref cm, "@Stock3_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 17));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 12));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 15));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 18));
                    Sm.CmParam<Decimal>(ref cm, "@MutatedAmt_" + i.ToString(), Sm.GetGrdDec(Grd1, i, 19));
                    Sm.CmParam<String>(ref cm, "@Source_" + i.ToString(), mDocType1 + "*" + DocNo + "*" + Sm.Right("0000" + (i + 1).ToString(), 4));

                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }

        private MySqlCommand SavePropertyInventoryMutationDtl2(string DocNo)
        {
            bool IsFirst = true;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblPropertyInventoryMutationDtl2(DocNo, DNo, PropertyCode, PropertyName, PropertyCategoryCode, ItCode, UPrice, CurCode, ");
            SQL.AppendLine("Qty, Qty2, Qty3, MutatedAmt, PropCode, BatchNo, Source, Lot, Bin, CreateBy, CreateDt) ");
            SQL.AppendLine("Values");
            for (int i = 0; i < Grd2.Rows.Count - 1; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 2).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo_" + i.ToString() + ", @PropertyCode_" + i.ToString() + ", @PropertyName_" + i.ToString() + ", @PropertyCategoryCode_" + i.ToString() + ", @ItCode_" + i.ToString());
                    SQL.AppendLine(", @UPrice_" + i.ToString() + ", 'IDR', @Qty_" + i.ToString() + ", @Qty2_" + i.ToString() + ", @Qty3_" + i.ToString() + ", @MutatedAmt_" + i.ToString() + ",'-', " + (mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'") + ", @Source_" + i.ToString() + ", '-', '-', @CreateBy, CurrentDateTime())");
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), Sm.Right("0000" + (i + 1).ToString(), 4));
                    Sm.CmParam<String>(ref cm, "@PropertyCode_" + i.ToString(), Sm.GetGrdStr(Grd2, i, 2));
                    Sm.CmParam<String>(ref cm, "@PropertyName_" + i.ToString(), Sm.GetGrdStr(Grd2, i, 3));
                    Sm.CmParam<String>(ref cm, "@PropertyCategoryCode_" + i.ToString(), Sm.GetGrdStr(Grd2, i, 4));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + i.ToString(), Sm.GetGrdStr(Grd2, i, 7));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + i.ToString(), Sm.GetGrdDec(Grd2, i, 9));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + i.ToString(), Sm.GetGrdDec(Grd2, i, 11));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + i.ToString(), Sm.GetGrdDec(Grd2, i, 13));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + i.ToString(), Sm.GetGrdDec(Grd2, i, 15));
                    Sm.CmParam<Decimal>(ref cm, "@MutatedAmt_" + i.ToString(), Sm.GetGrdDec(Grd2, i, 16));
                    Sm.CmParam<String>(ref cm, "@Source_" + i.ToString(), mDocType2 + "*" + DocNo + "*" + Sm.Right("0000" + (i + 1).ToString(), 4));
                }
            }
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();

            return cm;
        }

        public MySqlCommand UpdatePropertyInventoryRemainingValue(string DocNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.PropertyCode = B.PropertyCode ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationHdr C On B.DocNo = C.DocNo ");
            if (CancelInd == "N")
                SQL.AppendLine("    Set A.RemStockQty = A.RemStockQty - B.Qty, A.RemStockValue = A.RemStockValue - B.MutatedAmt, ");
            else
                SQL.AppendLine("    Set A.RemStockQty = A.RemStockQty + B.Qty, A.RemStockValue = A.RemStockValue + B.MutatedAmt, ");
            SQL.AppendLine("    A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo ");
            SQL.AppendLine("And C.Status = 'A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation'");
            //SQL.AppendLine("    And DocNo = @DocNo");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand UpdatePropertyInventory(string DocNo, string CancelInd)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("	Select T1.DocNo, T1.PropertyCode, T2.UPrice, Sum(T2.Qty) As Qty, Sum(T2.MutatedAmt) As PropInvValue ");
            SQL.AppendLine("	From TblPropertyInventoryMutationDtl T1 ");
            SQL.AppendLine("	Inner Join TblPropertyInventoryMutationDtl2 T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("	Where T1.DocNo = @DocNo ");
            SQL.AppendLine(") B On A.PropertyCode = B.PropertyCode ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationHdr C On B.DocNo = C.DocNo ");
            if (CancelInd == "N")
                SQL.AppendLine("Set A.InventoryQty = IfNull(B.Qty, A.InventoryQty), A.Uprice = B.UPrice ");
            else
                SQL.AppendLine("Set A.InventoryQty = 1.00, A.UPrice = B.PropInvValue ");
            SQL.AppendLine("Where C.Status = 'A' ");
            SQL.AppendLine("And A.Parent Is Null And A.Level = 0 ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation'");
            //SQL.AppendLine("    And DocNo = @DocNo");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        public MySqlCommand SavePropertyInventory(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPropertyInventoryHdr(PropertyCode, PropertyName, DisplayName, Parent, Level, Status, CompleteInd, ActInd, CancelInd, PropertyCategoryCode, ItCode, RegistrationDt, ");
            SQL.AppendLine("PropertyMutationDocNo, ExternalCode, CCCode, AcNo, SiteCode, UomCode, InventoryQty, PropertyInventoryValue, UPrice, RemStockQty, RemStockValue,  ");
            SQL.AppendLine("AcquisitionType, ProvCode, CityCode, District, Village, RightsNumberAndNIB, CertificatePublishDt, RightsHolder, RightsExpiredDt, ");
            SQL.AppendLine("DecreeDt, DecreeNo, RightsReleaseBasis, IndicativePotentialArea, SpatialInfoPage, CreateBy, CreateDt) ");
            SQL.AppendLine("Select C.PropertyCode, C.PropertyName, C.PropertyName, B.PropertyCode, D.Level+1, 'A', 'Y', 'Y', 'N', C.PropertyCategoryCode, C.ItCode, A.DocDt, ");
            SQL.AppendLine("A.DocNo, D.ExternalCode, D.CCCode, E.AcNo1, D.SiteCode, F.InventoryUOMCode, C.Qty, C.MutatedAmt, C.MutatedAmt/C.Qty, C.Qty, C.Qty * (C.MutatedAmt/C.Qty), ");
            SQL.AppendLine("D.AcquisitionType, D.ProvCode, D.CityCode, D.District, D.Village, D.RightsNumberAndNIB, D.CertificatePublishDt, D.RightsHolder, D.RightsExpiredDt, ");
            SQL.AppendLine("D.DecreeDt, D.DecreeNo, D.RightsReleaseBasis, D.IndicativePotentialArea, D.SpatialInfoPage, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo And B.DNo = '0001' ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl2 C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblPropertyInventoryHdr D On B.PropertyCode = D.PropertyCode ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory E On D.PropertyCategoryCode = E.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblItem F On C.ItCode = F.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.Status = 'A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation' ");
            //SQL.AppendLine("    And DocNo = @DocNo ");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<string>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        public MySqlCommand SaveStockMovement1(string CancelInd, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovementPropertyMutation(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, @WhsCode, '-', '-', B.ItCode, '-', B.BatchNo, B.Source, ");
            if (CancelInd == "N")
                SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            else
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("A.Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation' ");
            //SQL.AppendLine("    And DocNo = @DocNo ");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType1);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCodeForPropertyInventoryMutation);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveStockSummary1a(string DocNo) //Insert
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockSummaryPropertyMutation(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, 0.00, 0.00, 0.00, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation' ");
            //SQL.AppendLine("    And DocNo = @DocNo ");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCodeForPropertyInventoryMutation);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveStockSummary1b(string DocNo) //Cancel
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummaryPropertyMutation A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.ItCode = B.ItCode ");
            SQL.AppendLine("	And A.Source = B.Source ");
            SQL.AppendLine("	And A.WhsCode = @WhsCode ");
            SQL.AppendLine("	And A.Lot = B.Lot ");
            SQL.AppendLine("	And A.Bin = B.Bin ");
            SQL.AppendLine("Set A.Qty=A.Qty-B.Qty, A.Qty2=A.Qty2-B.Qty2, A.Qty3=A.Qty3-B.Qty3, ");
            SQL.AppendLine("A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCodeForPropertyInventoryMutation);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveStockPrice1(string DocNo, string DocDt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockPricePropertyMutation ");
            SQL.AppendLine("(ItCode, PropCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.PropCode, B.BatchNo, B.Source, B.CurCode, B.UPrice, ");
            SQL.AppendLine("Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=@DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation' ");
            //SQL.AppendLine("    And DocNo = @DocNo ");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveStockMovement2(string CancelInd, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovementPropertyMutation(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, @WhsCode, '-', '-', B.ItCode, '-', B.BatchNo, B.Source, ");
            if (CancelInd == "N")
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            else
                SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("A.Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation' ");
            //SQL.AppendLine("    And DocNo = @DocNo ");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType2);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCodeForPropertyInventoryMutation);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveStockSummary2a(string DocNo) //Insert
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockSummaryPropertyMutation(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation' ");
            //SQL.AppendLine("    And DocNo = @DocNo ");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCodeForPropertyInventoryMutation);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveStockSummary2b(string DocNo) //Cancel
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummaryPropertyMutation A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl2 B On A.ItCode = B.ItCode ");
            SQL.AppendLine("	And A.Source = B.Source ");
            SQL.AppendLine("	And A.WhsCode = @WhsCode ");
            SQL.AppendLine("	And A.Lot = B.Lot ");
            SQL.AppendLine("	And A.Bin = B.Bin ");
            SQL.AppendLine("Set A.Qty=A.Qty-B.Qty, A.Qty2=A.Qty2-B.Qty2, A.Qty3=A.Qty3-B.Qty3, ");
            SQL.AppendLine("A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCodeForPropertyInventoryMutation);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveStockPrice2(string DocNo, string DocDt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockPricePropertyMutation ");
            SQL.AppendLine("(ItCode, PropCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.PropCode, B.BatchNo, B.Source, B.CurCode, B.UPrice, ");
            SQL.AppendLine("Case When B.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=@DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0) End As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status = 'A' ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation' ");
            //SQL.AppendLine("    And DocNo = @DocNo ");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand SaveJournal(string DocNo, string JournalDocNo)
        {
            var SQL = new StringBuilder();
            string mMenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmPropertyInventoryMutation' Limit 1;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblPropertyInventoryMutationHdr Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Status = 'A'; ");
            //SQL.AppendLine("And Not Exists (Select 1 From TblDocApproval Where DocNo = @DocNo And IfNull(Status, '') Not In ('A', 'C')); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Property Inventory Mutation : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("C.CCCode, Null, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo And B.DNo = '0001' ");
            SQL.AppendLine("Inner Join TblPropertyInventoryHdr C On B.PropertyCode = C.PropertyCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            //SQL.AppendLine("And Not Exists( ");
            //SQL.AppendLine("    Select DocNo From TblDocApproval ");
            //SQL.AppendLine("    Where DocType= 'PropertyInventoryMutation' ");
            //SQL.AppendLine("    And DocNo = @DocNo ");
            //SQL.AppendLine("    And IfNull(Status, '') Not In ('A', 'C') ");
            //SQL.AppendLine(") ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select X2.AcNo1 As AcNo, Sum(IfNull(X1.MutatedAmt, 0.00)) As DAmt, 0.00 As CAMt ");
            SQL.AppendLine("    From TblPropertyInventoryMutationDtl2 X1 ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCategory X2 On X1.PropertyCategoryCode = X2.PropertyCategoryCode ");
            SQL.AppendLine("    Where X1.DocNo = @DocNo ");
            SQL.AppendLine("    Group By X2.AcNo1 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select X3.AcNo1 As AcNo, 0.00 As DAmt, Sum(IfNull(X1.MutatedAmt, 0.00)) As CAMt ");
            SQL.AppendLine("    From TblPropertyInventoryMutationDtl X1 ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryHdr X2 On X1.PropertyCode = X2.PropertyCode ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCategory X3 On X2.PropertyCategoryCode = X3.PropertyCategoryCode ");
            SQL.AppendLine("    Where X1.DocNo = @DocNo ");
            SQL.AppendLine("    Group By X3.AcNo1 ");
            SQL.AppendLine(")B On 1 = 1 ");
            SQL.AppendLine("And B.AcNo Is Not Null ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<string>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<string>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<string>(ref cm, "@MenuCode", mMenuCode);

            Sm.CmParam<string>(ref cm, "@JournalDocNo", JournalDocNo);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditPropertyInventoryMutationHdr());

            if (Grd1.Rows.Count > 0)
            {
                cml.Add(SaveStockMovement1("Y", TxtDocNo.Text));
                cml.Add(SaveStockSummary1b(TxtDocNo.Text));
            }

            if (Grd2.Rows.Count > 0)
            {
                cml.Add(SaveStockMovement2("Y", TxtDocNo.Text));
                cml.Add(SaveStockSummary2b(TxtDocNo.Text));
            }

            cml.Add(UpdatePropertyInventoryRemainingValue(TxtDocNo.Text, "Y"));
            cml.Add(UpdatePropertyInventory(TxtDocNo.Text, "Y"));

            if (mIsAutoJournalActived)
                cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                IsDataCancelledAlready() ||
                IsPropCodeAlreadyProceedToOtherTrx() ||
                IsPropCodeChildAlreadyProceedToOtherTrx();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPropertyInventoryMutationHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo = @DocNo ;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsPropCodeAlreadyProceedToOtherTrx()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Set @CreateDt := (Select CreateDt From TblPropertyInventoryMutationHdr Where DocNo = @DocNo);");

            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction'\t': ', B.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("Left Join TblMenu B On 1 = 1 And B.Param = 'FrmPropertyInventoryCost' ");
            SQL.AppendLine("Where PropertyCode = @PropertyCode ");
            SQL.AppendLine("And A.DocNo <> @DocNo ");
            SQL.AppendLine("And A.CreateDt >= @CreateDt ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction'\t': ', C.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryMutation' ");
            SQL.AppendLine("Where B.PropertyCode = @PropertyCode ");
            SQL.AppendLine("And A.DocNo <> @DocNo ");
            SQL.AppendLine("And A.CreateDt >= @CreateDt ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction'\t': ', C.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#'\t': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date'\t\t': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code'\t': ', @PropertyCode, '\n' ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryTransferDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryTransfer' ");
            SQL.AppendLine("Where B.PropertyCode = @PropertyCode ");
            SQL.AppendLine("And A.DocNo <> @DocNo ");
            SQL.AppendLine("And A.CreateDt >= @CreateDt ");
            SQL.AppendLine("And A.CancelInd = 'N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<string>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<string>(ref cm, "@PropertyCode", Sm.GetGrdStr(Grd1, 0, 2));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Remarks", });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "This Property Code is already proceed to other transaction." +
                                Environment.NewLine + Environment.NewLine +
                                Sm.DrStr(dr, c[0]));
                            return true;
                        }
                    }
                }
                dr.Close();
            }

            return false;
        }

        private bool IsPropCodeChildAlreadyProceedToOtherTrx()
        {
            string Remarks = string.Empty;
            bool mResult = false;
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @CreateDt := (Select CreateDt From TblPropertyInventoryMutationHdr Where DocNo = @Param3);");
            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction','\t',': ', B.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#','\t',': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date','\t\t',': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code','\t',': ', @Param1 ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryCostComponentHdr A ");
            SQL.AppendLine("Left Join TblMenu B On 1 = 1 And B.Param = 'FrmPropertyInventoryCost' ");
            SQL.AppendLine("Where A.PropertyCode = @Param1 ");
            SQL.AppendLine("And A.DocDt >= @CreateDt ");
            SQL.AppendLine("And A.DocNo <> @Param2 ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction','\t',': ', C.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#','\t',': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date','\t\t',': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code','\t',': ', @Param1 ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryMutation' ");
            SQL.AppendLine("Where B.PropertyCode = @Param1 ");
            SQL.AppendLine("And A.DocDt >= @CreateDt ");
            SQL.AppendLine("And A.DocNo <> @Param2 ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'Transaction','\t',': ', C.MenuDesc, '\n', ");
            SQL.AppendLine("    'Document#','\t',': ', A.DocNo, '\n', ");
            SQL.AppendLine("    'Date','\t\t',': ', Date_Format(A.DocDt, '%d/%m/%Y'), '\n', ");
            SQL.AppendLine("    'Property Code','\t',': ', @Param1 ");
            SQL.AppendLine(") As Remarks ");
            SQL.AppendLine("From TblPropertyInventoryTransferHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryTransferDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblMenu C On 1 = 1 And C.Param = 'FrmPropertyInventoryTransfer' ");
            SQL.AppendLine("Where B.PropertyCode = @Param1 ");
            SQL.AppendLine("And A.DocDt >= @CreateDt ");
            SQL.AppendLine("And A.DocNo <> @Param2 ");
            SQL.AppendLine("And A.CancelInd = 'N'; ");

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                Remarks = Sm.GetValue(SQL.ToString(), Sm.GetGrdStr(Grd2, Row, 2), TxtDocNo.Text, string.Empty);
                if (Remarks.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "This Property Code is already proceed to other transaction." +
                                Environment.NewLine + Environment.NewLine +
                                Remarks);
                    mResult = true;
                    break;
                }
            }
            return mResult;
        }

        private MySqlCommand EditPropertyInventoryMutationHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryMutationHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationHdr B On A.PropertyMutationDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPropertyInventoryMutationDtl2 C On B.DocNo = C.DocNo ");
            SQL.AppendLine("	Set A.CancelInd = 'Y', A.ActInd = 'N', A.CancelReason = B.CancelReason, A.DeactivationReason = B.CancelReason, ");
            SQL.AppendLine("		 A.LastUpBy = B.LastUpBy, A.LastUpDt = B.LastUpDt ");
            SQL.AppendLine("Where B.CancelInd = 'Y' ");
            SQL.AppendLine("And B.DocNo = @DocNo ");
            SQL.AppendLine("And A.PropertyCode = C.PropertyCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblPropertyInventoryMutationHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblPropertyInventoryMutationHdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblPropertyInventoryMutationHdr Where DocNo=@DocNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowPropertyInventoryMutationHdr(DocNo);
                ShowPropertyInventoryMutationDtl(DocNo);
                ShowPropertyInventoryMutationDtl2(DocNo);
                Sm.ShowDocApproval(DocNo, "PropertyInventoryMutation", ref Grd3, false);
                ComputeQuantity(false);
                ComputeUPrice();
                ComputeQuantity2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPropertyInventoryMutationHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DocDt, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As Status, CancelReason, CancelInd, Remark, ");
            SQL.AppendLine("FileName, FileName2, FileName3 ");
            SQL.AppendLine("From TblPropertyInventoryMutationHdr ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    "DocNo",

                    "DocDt", "Status", "CancelReason", "CancelInd", "Remark",

                    "FileName", "FileName2", "FileName3"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[6]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[7]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[8]);
                }, true
            );
        }

        private void ShowPropertyInventoryMutationDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropertyCode, B.PropertyName, C.PropertyCategoryName, B.ItCode, ");
            SQL.AppendLine("D.ItName, IfNull(A.UPrice, 0.00) As UPrice, IfNull(A.StockValue, 0.00) As StockValue, D.InventoryUOMCode, IfNull(A.Stock, 0.00) As Stock, IfNull(A.Qty, 0.00) As Qty, ");
            SQL.AppendLine("D.InventoryUOMCode2, IfNull(A.Stock2, 0.00) As Stock2, IfNull(A.Qty2, 0.00) As Qty2, D.InventoryUOMCode3, IfNull(A.Stock3, 0.00) As Stock3, IfNull(A.Qty3, 0.00) As Qty3, ");
            SQL.AppendLine("A.MutatedAmt, IfNull(A.Stock, 0.00) - IfNull(A.Qty, 0.00) As RemStockQty, IfNull(A.StockValue, 0.00) - IfNull(A.MutatedAmt, 0.00) As RemStockQty, C.PropertyCategoryCode, ");
            SQL.AppendLine("B.CCCode, E.CCName, B.SiteCode, F.SiteName ");
            SQL.AppendLine("From TblPropertyInventoryMutationDtl A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryHdr B On A.PropertyCode = B.PropertyCode ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory C On B.PropertyCategoryCode = C.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Inner Join TblCostCenter E On B.CCCode = E.CCCode ");
            SQL.AppendLine("Inner Join TblSite F On B.SiteCode = F.SiteCode ");
            SQL.AppendLine("Where DocNo = @DocNo Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "PropertyCode",
                    //1-5
                    "PropertyName", "PropertyCategoryName", "ItCode", "ItName", "UPrice", 
                    //6-10
                    "StockValue", "InventoryUOMCode", "Stock", "Qty", "InventoryUOMCode2", 
                    //11-15
                    "Stock2", "Qty2", "InventoryUOMCode3", "Stock3", "Qty3", 
                    //16-20
                    "MutatedAmt", "RemStockQty", "RemStockQty", "PropertyCategoryCode", "CCCode",
                    //21-23
                    "CCName", "SiteCode", "SiteName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 14, 15, 17, 18, 19, 20, 21 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPropertyInventoryMutationDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropertyCode, A.PropertyName, A.PropertyCategoryCode, B.PropertyCategoryName, ");
            SQL.AppendLine("C.ItCode, C.ItName, IfNull(A.UPrice, 0.00) As UPrice, C.InventoryUOMCode, IfNull(A.Qty, 0.00) As Qty, ");
            SQL.AppendLine("C.InventoryUOMCode2, IfNull(A.Qty2, 0.00) As Qty2, C.InventoryUOMCode3, IfNull(A.Qty3, 0.00) As Qty3, ");
            SQL.AppendLine("IfNull(A.MutatedAmt, 0.00) As MutatedAmt ");
            SQL.AppendLine("From TblPropertyInventoryMutationDtl2 A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory B On A.PropertyCategoryCode = B.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                {
                    //0
                    "PropertyCode",
                    //1-5
                    "PropertyName", "PropertyCategoryCode", "PropertyCategoryName", "ItCode", "ItName",
                    //6-10
                    "UPrice", "InventoryUOMCode", "Qty", "InventoryUOMCode2", "Qty2",
                    //11-13
                    "InventoryUOMCode3", "Qty3", "MutatedAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 9, 11, 13, 15, 16 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        public void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'MainCurCode', 'WhsCodeForPropertyInventoryMutation', 'IsBatchNoUseDocDtIfEmpty', 'IsAutoJournalActived', 'PortForFTPClient', ");
            SQL.AppendLine("'HostAddrForFTPClient','SharedFolderForFTPClient','UsernameForFTPClient','PasswordForFTPClient','FileSizeMaxUploadFTPClient','IsFilterByDept', ");
            SQL.AppendLine("'IsPropInventoryMutationAllowToUploadFile', 'IsTransactionUseDetailApprovalInformation' ); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "WhsCodeForPropertyInventoryMutation": mWhsCodeForPropertyInventoryMutation = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue; break;

                            //boolean
                            case "IsBatchNoUseDocDtIfEmpty": mIsBatchNoUseDocDtIfEmpty = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsPropInventoryMutationAllowToUploadFile": mIsPropInventoryMutationAllowToUploadFile = ParValue == "Y"; break;
                            case "IsTransactionUseDetailApprovalInformation": mIsTransactionUseDetailApprovalInformation = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        public void GetEntCode(string PropertyCode)
        {
            mEntCode = Sm.GetValue("Select C.EntCode From TblPropertyInventoryHdr A " +
                       "Inner Join TblCostCenter B On A.CCCode = B.CCCode " +
                       "Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode " +
                       "Where A.PropertyCode = @Param;", PropertyCode);
        }

        internal string GetItCode()
        {
            string mItCode = string.Empty;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (mItCode.Length > 0) mItCode += ",";
                mItCode += Sm.GetGrdStr(Grd2, row, 7);
            }

            return mItCode;
        }

        internal void ShowPropertyInventory(int CurRow, string PropertyCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<Detail>();

            SQL.AppendLine("Select A.PropertyCode, A.PropertyName, B.PropertyCategoryName, A.ItCode, F.ItName, IfNull(A.UPrice, 0.00) As UPrice, ");
            SQL.AppendLine("IfNull(A.RemStockValue, 0.00) As RemStockValue, F.InventoryUOMCode, IfNull(A.RemStockQty, 0.00) As RemStockQty, ");
            SQL.AppendLine("F.InventoryUOMCode2, F.InventoryUOMCode3, B.PropertyCategoryCode, A.CCCode, A.SiteCode, C.CCName, D.SiteName ");
            SQL.AppendLine("From TblPropertyInventoryHdr A ");
            SQL.AppendLine("Inner Join TblPropertyInventoryCategory B On A.PropertyCategoryCode = B.PropertyCategoryCode ");
            SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode = C.CCCode ");
            SQL.AppendLine("Inner Join TblSite D On A.SiteCode = D.SiteCode ");
            SQL.AppendLine("Left Join TblCOA E On A.AcNo = E.AcNo ");
            SQL.AppendLine("Inner Join TblItem F On A.ItCode = F.ItCode ");
            SQL.AppendLine("Where A.PropertyCode = @PropertyCode ");

            Sm.CmParam<string>(ref cm, "@PropertyCode", PropertyCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "PropertyCode",

                    //1-5
                    "PropertyName", "PropertyCategoryName", "ItCode", "ItName", "UPrice",

                    //6-10
                    "RemStockValue", "InventoryUOMCode", "RemStockQty", "InventoryUOMCode2", "InventoryUOMCode3",

                    //11-15
                    "PropertyCategoryCode", "CCCode", "CCName", "SiteCode", "SiteName"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Detail()
                        {
                            PropertyCode = Sm.DrStr(dr, c[0]),
                            PropertyName = Sm.DrStr(dr, c[1]),
                            PropertyCategoryName = Sm.DrStr(dr, c[2]),
                            ItCode = Sm.DrStr(dr, c[3]),
                            ItName = Sm.DrStr(dr, c[4]),
                            UPrice = Sm.DrDec(dr, c[5]),
                            RemStockValue = Sm.DrDec(dr, c[6]),
                            InventoryUOMCode = Sm.DrStr(dr, c[7]),
                            RemStockQty = Sm.DrDec(dr, c[8]),
                            InventoryUOMCode2 = Sm.DrStr(dr, c[9]),
                            InventoryUOMCode3 = Sm.DrStr(dr, c[10]),
                            PropertyCategoryCode = Sm.DrStr(dr, c[11]),
                            CCCode = Sm.DrStr(dr, c[12]),
                            CCName = Sm.DrStr(dr, c[13]),
                            SiteCode = Sm.DrStr(dr, c[14]),
                            SiteName = Sm.DrStr(dr, c[15])
                        });
                    }
                }
                dr.Close();
            }

            Grd1.BeginUpdate();
            foreach(var x in l)
            {
                Grd1.Cells[CurRow, 2].Value = x.PropertyCode;
                Grd1.Cells[CurRow, 3].Value = x.PropertyName;
                Grd1.Cells[CurRow, 4].Value = x.PropertyCategoryName;
                Grd1.Cells[CurRow, 6].Value = x.ItCode;
                Grd1.Cells[CurRow, 7].Value = x.ItName;
                Grd1.Cells[CurRow, 8].Value = x.UPrice;
                Grd1.Cells[CurRow, 9].Value = x.RemStockValue;
                Grd1.Cells[CurRow, 10].Value = x.InventoryUOMCode;
                Grd1.Cells[CurRow, 11].Value = x.RemStockQty;
                Grd1.Cells[CurRow, 22].Value = x.PropertyCategoryCode;
                Grd1.Cells[CurRow, 12].Value = (IsPropertyParent(CurRow) ? x.RemStockQty : 0m);
                Grd1.Cells[CurRow, 13].Value = x.InventoryUOMCode2;
                Grd1.Cells[CurRow, 14].Value = 0m;
                Grd1.Cells[CurRow, 15].Value = 0m;
                Grd1.Cells[CurRow, 16].Value = x.InventoryUOMCode3;
                Grd1.Cells[CurRow, 17].Value = 0m;
                Grd1.Cells[CurRow, 18].Value = 0m;
                Grd1.Cells[CurRow, 19].Value = 0m;
                Grd1.Cells[CurRow, 20].Value = 0m;
                Grd1.Cells[CurRow, 21].Value = 0m;
                Grd1.Cells[CurRow, 23].Value = x.CCCode;
                Grd1.Cells[CurRow, 24].Value = x.CCName;
                Grd1.Cells[CurRow, 25].Value = x.SiteCode;
                Grd1.Cells[CurRow, 26].Value = x.SiteName;
            }
            Grd1.EndUpdate();

            l.Clear();

            ComputeQuantity(true);
        }

        internal void SetGrd2Data(int Row)
        {
            Grd2.Cells[Row, 2].Value = GeneratePropertyCode(Row);
            Grd2.Cells[Row, 9].Value = (Grd2.Rows.Count > 1 ? Sm.GetGrdDec(Grd2, Row - 1, 9) : 0m);
            Sm.CopyGrdValue(Grd2, Row, 4, Grd1, 0, 22);
            Sm.CopyGrdValue(Grd2, Row, 5, Grd1, 0, 4);
            Sm.CopyGrdValue(Grd2, Row, 10, Grd1, 0, 10);
            Sm.CopyGrdValue(Grd2, Row, 12, Grd1, 0, 13);
            Sm.CopyGrdValue(Grd2, Row, 14, Grd1, 0, 16);
        }

        private void ComputeQuantity(bool IsChoose)
        {
            decimal Total1A = 0m, Total2A = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (IsChoose) Grd1AfterCommitEdit(Row, 12);
                Grd1.Cells[Row, 19].Value = Sm.GetGrdDec(Grd1, Row, 8) * Sm.GetGrdDec(Grd1, Row, 12);
                Grd1.Cells[Row, 20].Value = Sm.GetGrdDec(Grd1, Row, 11) - Sm.GetGrdDec(Grd1, Row, 12);
                Grd1.Cells[Row, 21].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 19);

                Total1A += Sm.GetGrdDec(Grd1, Row, 12);
                Total2A += Sm.GetGrdDec(Grd1, Row, 19);
            }

            TxtTotal1A.Text = Sm.FormatNum(Total1A, 0);
            TxtTotal2A.Text = Sm.FormatNum(Total2A, 0);
        }

        private void ComputeQuantity2()
        {
            decimal Total1B = 0m, Total2B = 0m;

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                Grd2.Cells[Row, 16].Value = Sm.GetGrdDec(Grd2, Row, 9) * Sm.GetGrdDec(Grd2, Row, 11);
                Total1B += Sm.GetGrdDec(Grd2, Row, 11);
                Total2B += Sm.GetGrdDec(Grd2, Row, 16);
            }

            TxtTotal1B.Text = Sm.FormatNum(Total1B, 0);
            TxtTotal2B.Text = Sm.FormatNum(Total2B, 0);
        }

        private void ComputeUPrice()
        {
            decimal Total1B = 0m;

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                Total1B += Sm.GetGrdDec(Grd2, Row, 11);

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Total1B > 0) Grd2.Cells[Row, 9].Value = Decimal.Parse(TxtTotal2A.Text) / Total1B;
        }

        private string GeneratePropertyCode(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(@Param ,'.', IfNull((  ");
            SQL.AppendLine("	Select Right(Concat('00', Convert(PropertyCode + 1 + " + Row + ", Char)), 2) As PropertyCode From(  ");
            SQL.AppendLine("		Select Convert(T.PropertyCode, Decimal) As PropertyCode From (  ");
            SQL.AppendLine("			Select substring_index(PropertyCode, '.', -1) As PropertyCode  ");
            SQL.AppendLine("			From TblpropertyInventoryHdr  ");
            SQL.AppendLine("			Where PropertyCode Like Concat(@Param,'%')  ");
            SQL.AppendLine("			And Parent Is Not Null  ");
            SQL.AppendLine("			And Level = (Select Level + 1 From TblpropertyInventoryHdr Where PropertyCode = @Param) ");
            SQL.AppendLine("			Union All ");
            SQL.AppendLine("			Select substring_index(C.PropertyCode, '.', -1) As PropertyCode  ");
            SQL.AppendLine("			From TblPropertyInventoryMutationHdr A ");
            SQL.AppendLine("			Inner Join TblPropertyInventoryMutationDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("				And B.PropertyCode = @Param ");
            SQL.AppendLine("				And A.CancelInd = 'N' ");
            SQL.AppendLine("				And A.Status = 'A' ");
            SQL.AppendLine("			Inner Join TblPropertyInventoryMutationDtl2 C On A.DocNo = C.DocNo ");
            SQL.AppendLine("			Where C.PropertyCode Not In ( ");
            SQL.AppendLine("				Select PropertyCode From TblPropertyInventoryHdr ");
            SQL.AppendLine("				Where PropertyCode Like Concat(@Param,'%') ");
            SQL.AppendLine("			) ");
            SQL.AppendLine("		)T Order By T.PropertyCode Desc Limit 1  ");
            SQL.AppendLine("	) As Temp  ");
            SQL.AppendLine("), Right(Concat('00', Convert(1 + " + Row + ", Char)), 2))); ");

            return Sm.GetValue(SQL.ToString(), Sm.GetGrdStr(Grd1, 0, 2));
        }

        internal void Grd1AfterCommitEdit(int Row, int Col)
        {
            #region Stock Qty
            if (Col == 11)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, Row, 2, 11, 14, 17, 10, 13, 16);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, Row, 2, 11, 17, 14, 10, 16, 13);
            }

            if (Col == 14)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, Row, 2, 14, 11, 17, 13, 10, 16);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, Row, 2, 14, 17, 11, 13, 16, 10);
            }

            if (Col == 17)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, Row, 2, 17, 11, 14, 16, 10, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, Row, 2, 17, 14, 11, 16, 13, 10);
            }

            if (Col == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Sm.GetGrdStr(Grd1, Row, 13)))
                Sm.CopyGrdValue(Grd1, Row, 14, Grd1, Row, 11);

            if (Col == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Sm.GetGrdStr(Grd1, Row, 16)))
                Sm.CopyGrdValue(Grd1, Row, 17, Grd1, Row, 11);

            if (Col == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 13), Sm.GetGrdStr(Grd1, Row, 16)))
                Sm.CopyGrdValue(Grd1, Row, 17, Grd1, Row, 14);
            #endregion

            #region Mutated Qty
            if (Col == 12)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, Row, 2, 12, 15, 18, 10, 13, 16);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, Row, 2, 12, 18, 15, 10, 16, 13);
            }

            if (Col == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, Row, 2, 15, 12, 18, 13, 10, 16);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, Row, 2, 15, 18, 12, 13, 16, 10);
            }

            if (Col == 18)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, Row, 2, 18, 12, 15, 16, 10, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, Row, 2, 18, 15, 12, 16, 13, 10);
            }

            if (Col == 12 && Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Sm.GetGrdStr(Grd1, Row, 13)))
                Sm.CopyGrdValue(Grd1, Row, 15, Grd1, Row, 12);

            if (Col == 12 && Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Sm.GetGrdStr(Grd1, Row, 16)))
                Sm.CopyGrdValue(Grd1, Row, 18, Grd1, Row, 12);

            if (Col == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 13), Sm.GetGrdStr(Grd1, Row, 16)))
                Sm.CopyGrdValue(Grd1, Row, 18, Grd1, Row, 15);

            //if (Col == 13 || Col == 16 || Col == 19) ComputeUnitPriceBasedOnFormula();

            //if (Sm.IsGrdColSelected(new int[] { 13, 16, 19 }, Col)) ComputeTotalB();
            #endregion
        }

        internal void Grd2AfterCommitEdit(int Row, int Col)
        {
            if (Col == 11)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd2, Row, 2, 11, 13, 15, 10, 12, 14);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd2, Row, 2, 11, 15, 13, 10, 14, 12);
            }

            if (Col == 13)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd2, Row, 2, 13, 11, 15, 12, 10, 14);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd2, Row, 2, 13, 15, 11, 12, 14, 10);
            }

            if (Col == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd2, Row, 2, 15, 11, 13, 14, 10, 12);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd2, Row, 2, 15, 13, 11, 14, 12, 10);
            }

            if (Col == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 10), Sm.GetGrdStr(Grd2, Row, 12)))
                Sm.CopyGrdValue(Grd2, Row, 13, Grd2, Row, 11);

            if (Col == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 10), Sm.GetGrdStr(Grd2, Row, 14)))
                Sm.CopyGrdValue(Grd2, Row, 15, Grd2, Row, 11);

            if (Col == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 12), Sm.GetGrdStr(Grd2, Row, 14)))
                Sm.CopyGrdValue(Grd2, Row, 15, Grd2, Row, 13);
        }

        private void OpenFileDialog(DXE.TextEdit TextFile)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TextFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void UploadFile(string DocNo, DXE.TextEdit TextFile, System.Windows.Forms.ProgressBar ProgressUpload, string Sequence)
        {
            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TextFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TextFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                ProgressUpload.Invoke(
                    (MethodInvoker)delegate { ProgressUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    ProgressUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            ProgressUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateAttachmentFile(DocNo, toUpload.Name, Sequence));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdateAttachmentFile(string DocNo, string FileName, string Sequence)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPropertyInventoryMutationHdr Set ");
            SQL.AppendLine("    FileName" + Sequence + " =@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private void DownloadFileClick(DXE.TextEdit TextFile, System.Windows.Forms.ProgressBar ProgressUpload)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TextFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, ProgressUpload);
            SFD.FileName = TextFile.Text;
            SFD.DefaultExt = Path.GetExtension(SFD.FileName);
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TextFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared, System.Windows.Forms.ProgressBar ProgressUpload)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                ProgressUpload.Value = 0;
                ProgressUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        ProgressUpload.Value = ProgressUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (ProgressUpload.Value + bytesRead <= ProgressUpload.Maximum)
                        {
                            ProgressUpload.Value += bytesRead;

                            ProgressUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
                this.Text = "Property Inventory Mutation";
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private bool IsUploadFileNotValid(string FileName)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid(FileName)
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsPropInventoryMutationAllowToUploadFile && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsPropInventoryMutationAllowToUploadFile && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsPropInventoryMutationAllowToUploadFile && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsPropInventoryMutationAllowToUploadFile && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(string FileName)
        {
            if (mIsPropInventoryMutationAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
            }
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                OpenFileDialog(TxtFile);
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                OpenFileDialog(TxtFile2);
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                OpenFileDialog(TxtFile3);
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFileClick(TxtFile, PbUpload);
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFileClick(TxtFile, PbUpload2);
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFileClick(TxtFile, PbUpload3);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
                {
                    if (e.RowIndex == 0)
                        Sm.FormShowDialog(new FrmPropertyInventoryMutationDlg(this, e.RowIndex));
                    else
                    {
                        if (Sm.GetGrdStr(Grd1, 0, 2).Length > 0)
                            Sm.StdMsg(mMsgType.Warning, "You can only choose 1 Property Inventory");
                        else
                            Sm.FormShowDialog(new FrmPropertyInventoryMutationDlg(this, e.RowIndex));
                    }
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
                {
                    Sm.FormShowDialog(new FrmPropertyInventoryMutationDlg(this, e.RowIndex));
                    //if (e.RowIndex == 0)
                    //    Sm.FormShowDialog(new FrmPropertyInventoryMutationDlg(this, e.RowIndex));
                    //else
                    //{
                    //    if(Sm.GetGrdStr(Grd1, 0, 2).Length > 0)
                    //        Sm.StdMsg(mMsgType.Warning, "You can only choose 1 Property Inventory");
                    //    else
                    //        Sm.FormShowDialog(new FrmPropertyInventoryMutationDlg(this, e.RowIndex));
                    //}
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Grd1AfterCommitEdit(e.RowIndex, e.ColIndex);
                ComputeQuantity(false);
                ComputeUPrice();
                ComputeQuantity2();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtTotal1A, TxtTotal2A, TxtTotal1B, TxtTotal2B });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTotal1A, TxtTotal2A, TxtTotal1B, TxtTotal2B }, 0);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(Grd2, Grd2.Rows.Count - 1, new int[] { 9, 11, 13, 15, 16 });
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 6 && TxtDocNo.Text.Length == 0)
                {
                    if (!Sm.IsGrdValueEmpty(Grd1, 0, 2, false, "Property Code"))
                        Sm.FormShowDialog(new FrmPropertyInventoryMutationDlg2(this, e.RowIndex));
                }
            }

            if (e.ColIndex == 1)
            {
                string PropertyCode = Sm.GetValue("Select PropertyCode From TblPropertyInventoryHdr Where PropertyCode = @Param;", Sm.GetGrdStr(Grd2, e.RowIndex, 2));
                if (PropertyCode.Length > 0)
                {
                    var f = new FrmPropertyInventory(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mPropertyCode = PropertyCode;
                    f.ShowDialog();
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 6 && TxtDocNo.Text.Length == 0)
                {
                    if (!Sm.IsGrdValueEmpty(Grd1, 0, 2, false, "Property Code is empty"))
                        Sm.FormShowDialog(new FrmPropertyInventoryMutationDlg2(this, e.RowIndex));
                }
            }

            if (e.ColIndex == 1)
            {
                string PropertyCode = Sm.GetValue("Select PropertyCode From TblPropertyInventoryHdr Where PropertyCode = @Param;", Sm.GetGrdStr(Grd2, e.RowIndex, 2));
                if (PropertyCode.Length > 0)
                {
                    var f = new FrmPropertyInventory(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mPropertyCode = PropertyCode;
                    f.ShowDialog();
                }
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Grd2AfterCommitEdit(e.RowIndex, e.ColIndex);
                ComputeUPrice();
                ComputeQuantity2();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeUPrice();
            ComputeQuantity2();
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #region Class

        private class Detail
        {
            public string PropertyCode { get; set; }
            public string PropertyName { get; set; }
            public string PropertyCategoryName { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal UPrice { get; set; }
            public decimal RemStockValue { get; set; }
            public string InventoryUOMCode { get; set; }
            public decimal RemStockQty { get; set; }
            public string InventoryUOMCode2 { get; set; }
            public string InventoryUOMCode3 { get; set; }
            public string PropertyCategoryCode { get; set; }
            public string CCCode { get; set; }
            public string CCName { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
        }

        #endregion
    }
}
