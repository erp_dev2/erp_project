﻿#region Update 
/*
 07/11/2019 [VIN] Tambah filter COA Alias
 
*/
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesReturnInvoiceDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesReturnInvoice mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesReturnInvoiceDlg2(FrmSalesReturnInvoice FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();

                Sl.SetLueAcCtCode(ref LueAcCtCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Account#", 
                        "Description", 
                        "Type",
                        "Category",

                        //6
                        "Alias"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcTypeDesc, C.AcCtName, A.Alias ");
            SQL.AppendLine("From TblCOA A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            SQL.AppendLine("Left Join TblAccountCategory C On Left(A.AcNo, 1)=C.AcCtCode ");
            SQL.AppendLine("Where A.AcNo Not In (Select Parent From TblCOA Where Parent is Not Null) ");
            SQL.AppendLine("And Locate(Concat('##', A.AcNo, '##'), @SelectedAcNo)<1 ");
            SQL.AppendLine("And A.ActInd='Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedAcNo", mFrmParent.GetSelectedAcNo());
                
                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, "A.AcNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "C.AcCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtCOAAlias.Text, "A.Alias", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By A.AcNo;",
                        new string[] 
                        { 
                            "AcNo", "AcDesc", "AcTypeDesc", "AcCtName", "Alias"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsAcNoAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 3);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, Row1, new int[] { 3, 4 });

                        mFrmParent.Grd2.Rows.Add();                        
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 3, 4 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
        }

        private bool IsAcNoAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd2, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account number");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account description");
        }

        private void LueAcCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCtCode, new Sm.RefreshLue1(Sl.SetLueAcCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAcCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Account category");
        }
        private void TxtCOAAlias_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCOAAlias_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA Alias");
        }

        #endregion

       
        #endregion

        

    }
}
