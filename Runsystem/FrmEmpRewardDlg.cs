﻿#region
// 20/Des/2017 [HAR] format num ke form main
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpRewardDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpReward mFrmParent;
        private string mSQL = string.Empty;
        private string mDocDt;

        #endregion

        #region Constructor

        public FrmEmpRewardDlg(FrmEmpReward FrmParent, string DocDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
        }

        #endregion


        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.DeptName, C.PosName, A.JoinDt, A.ResignDt, D.OptDesc as Gender, ");
            SQL.AppendLine("TimeStampdiff(YEAR, A.JoinDt, left(Replace(CurDate(), '-', ''), 8)) WorkYr ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblOption D On A.Gender=D.OptCode And D.OptCat='Gender' ");
            SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>=@DocDt ) Or A.ResignDt Is Null) ");
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                         //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's Name",
                        "Old Code",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",

                        //6-9
                        "Department",
                        "Position",
                        "Gender",
                        "Years of Service",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 100, 80, 80, 
                        
                        //6-9
                        200, 200, 100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 8, 9 }, false);
            Sm.GrdColReadOnly(Grd1, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                String Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocDt", mDocDt);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.EmpName ",
                        new string[] 
                        { 
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "EmpCodeOld", "JoinDt", "ResignDt", "DeptName", 
                            
                            //6-8
                            "PosName", "Gender", "WorkYr"
                           
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtEmpCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtEmpName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtPosCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                mFrmParent.TxtYearsService.EditValue = Sm.FormatNum(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9), 0);
                this.Close();
            }
        }

        private void Grd1_CellDoubleClick_1(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

       
    }
}
