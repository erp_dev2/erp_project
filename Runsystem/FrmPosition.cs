﻿#region Update
/* 
    12/11/2017 [HAR] tambah parent dan level untuk organisasi structure
    09/02/2018 [HAR] tambah inputan training
    14/03/2018 [HAR] bug fixing position training code
    07/11/2019 [HAR/SIER] tambah inputam PLT indicator
    14/10/2020 [IBL/PHT] tambah inputan activity, presentase, dan teritorial, berdasarkan parameter IsPositionUseActivity
    21/03/2023 [WED/PHT] tambah tab Level berdasarkan parameter IsPositionUseLevel dan IsPositionLevelMandatory
    25/03/2023 [MAU/PHT] menampilkan checkbox Acting Official (PLT) berdasarkan parameter IsPositionNotUseActingOfficial
    29/03/2023 [MAU/PHT] menambahkan indikator Cash Advance PIC berdasarkan parameter IsVRCashAdvanceUseEmployeePIC
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPosition : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmPositionFind FrmFind;

        internal bool mIsPositionUseActivity = false, 
            mIsPositionUseLevel = false, 
            mIsPositionLevelMandatory = false, 
            mIsPositionNotUseActingOfficial = false,
            mIsVRCashAdvanceUseEmployeePIC = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPosition(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            ExecQuery();
            GetParameter();
            PnlActivity.Visible = mIsPositionUseActivity;

            TcPosition.SelectedTab = Tp2;
            if (!mIsPositionUseLevel) TcPosition.TabPages.Remove(Tp2);
            TcPosition.SelectedTab = Tp1;

            Sl.SetLueGrdLvlCode(ref LueGrdLvlCodeMin);
            Sl.SetLueGrdLvlCode(ref LueGrdLvlCodeMax);
            Sl.SetLuePosCode(ref LuePosCode);
            Sl.SetLueOption(ref LueActivityCode, "PositionActivity");
            Sl.SetLueOption(ref LueTeritoryCode, "PositionTeritory");
            LueTrainingCode.Visible = false;

            ChkActingOfficial.Visible = false;
            if(mIsPositionNotUseActingOfficial) ChkActingOfficial.Visible = true;

            //ChkCashAdvancePIC.Visible = false;
            //if(mIsVRCashAdvanceUseEmployeePIC) ChkCashAdvancePIC.Visible = true;
            ChkCashAdvancePIC.Visible = mIsVRCashAdvanceUseEmployeePIC;

            SetGrd();
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPosCode, TxtPosName, TxtLevel, LuePosCode, LueGrdLvlCodeMin, LueGrdLvlCodeMax, ChkRequestItemInd, ChkActingOfficial,
                        LueActivityCode, TxtPercentage, LueTeritoryCode, ChkCashAdvancePIC
                    }, true);
                    Grd1.ReadOnly = Grd2.ReadOnly = true;
                    TxtPosCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPosCode, TxtPosName, TxtLevel, LuePosCode, LueGrdLvlCodeMin, LueGrdLvlCodeMax, ChkRequestItemInd, ChkActingOfficial,
                        LueActivityCode, TxtPercentage, LueTeritoryCode, ChkCashAdvancePIC
                    }, false);
                    Grd1.ReadOnly = Grd2.ReadOnly = false;
                    TxtPosCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtPosCode, true);
                    Grd1.ReadOnly =false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPosName, TxtLevel, LuePosCode, LueGrdLvlCodeMin, LueGrdLvlCodeMax, ChkRequestItemInd, ChkActingOfficial,
                        LueActivityCode, TxtPercentage, LueTeritoryCode, ChkCashAdvancePIC
                    }, false);
                    Grd2.ReadOnly = false;
                    TxtPosName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtPosCode, TxtPosName, LuePosCode, LueGrdLvlCodeMin, LueGrdLvlCodeMax, ChkActingOfficial,
                LueActivityCode, TxtPercentage, LueTeritoryCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtLevel, TxtPercentage }, 2);
            ChkRequestItemInd.Checked = false;
            ChkActingOfficial.Checked = false;
            ChkCashAdvancePIC.Checked = false;


            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }


        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Dno",
                        //1-3
                        "Training Code",
                        "Training",
                        "Minimum"+Environment.NewLine+"Duration"
                    },
                     new int[] 
                    {
                        10, 
                        10, 250, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[]{3}, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1 });
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "Dno",
                    //1-3
                    "LevelCode",
                    "",
                    "Level"
                },
                    new int[]
                {
                    10,
                    10, 20, 150
                }
            );
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 3 });
            Sm.GrdColButton(Grd2, new int[] { 2 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1 });

            #endregion
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPositionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPosCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPosCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblPosition Where PosCode=@PosCode" };
                Sm.CmParam<String>(ref cm, "@PosCode", TxtPosCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SavePosition(TxtPosCode.Text));
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SavePositionTraining(TxtPosCode.Text, Row));

                if (mIsPositionUseLevel && Grd2.Rows.Count > 1)
                {
                    cml.Add(SavePositionLevel(TxtPosCode.Text));
                }

                Sm.ExecCommands(cml);
                ShowData(TxtPosCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PosCode)
        {
            try
            {
                ClearData();
                ShowPosition(PosCode);
                ShowPositionTraining(PosCode);
                if (mIsPositionUseLevel) ShowPositionLevel(PosCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPosition(string PosCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select PosCode PosCode, PosName, LEVEL, Parent, GrdLvlCodeMin, GrdLvlCodeMax, RequestItemInd,");
            SQL.AppendLine("ActingOfficialInd, ActivityCode, Percentage, TeritoryCode");
            
            if (mIsVRCashAdvanceUseEmployeePIC) 
                SQL.AppendLine(" , CashAdvPIC");
            else 
                SQL.AppendLine(" , Null As CashAdvPIC");

            SQL.AppendLine("From tblposition");
            SQL.AppendLine("Where PosCode = @PosCode; ");

            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.ShowDataInCtrl(
                    ref cm,  SQL.ToString(),
                    new string[] 
                        {
                            // 0
                            "PosCode", 
                            // 1-5
                            "PosName", "Level", "Parent", "GrdLvlCodeMin", "GrdLvlCodeMax", 
                            // 6-10
                            "RequestItemInd", "ActingOfficialInd", "ActivityCode", "Percentage", "TeritoryCode", 
                            // 11
                            "CashAdvPIC"
                        },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtPosName.EditValue = Sm.DrStr(dr, c[1]);
                        TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 2);
                        Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueGrdLvlCodeMin, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueGrdLvlCodeMax, Sm.DrStr(dr, c[5]));
                        ChkRequestItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[6]), "Y");
                        ChkActingOfficial.Checked = Sm.CompareStr(Sm.DrStr(dr, c[7]), "Y");
                        Sm.SetLue(LueActivityCode, Sm.DrStr(dr, c[8]));
                        TxtPercentage.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9 ]), 2);
                        Sm.SetLue(LueTeritoryCode, Sm.DrStr(dr, c[10]));
                        ChkCashAdvancePIC.Checked = Sm.CompareStr(Sm.DrStr(dr, c[11]), "Y");
                    }, true
                );
        }

        private void ShowPositionTraining(string PosCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.PosCode, B.Dno, B.TrainingCode, C.TrainingName, B.Duration");
            SQL.AppendLine("From TblPosition A ");
            SQL.AppendLine("Inner Join TblPositionTraining B On A.PosCode = B.PosCode ");
            SQL.AppendLine("Inner Join TblTraining C On B.TrainingCode = C.TrainingCode ");
            SQL.AppendLine("Where A.PosCode=@PosCode ");
            SQL.AppendLine("Order By B.DNo;");


            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "Dno", 
                        "TrainingCode", "TrainingName", "Duration"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPositionLevel(string PosCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.LevelCode, B.LevelName ");
            SQL.AppendLine("From TblPositionLevel A ");
            SQL.AppendLine("Left Join TblLevelHdr B On A.LevelCode = B.LevelCode ");
            SQL.AppendLine("Where A.PosCode = @PosCode ");
            SQL.AppendLine("Order By A.DNo ");
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[]
                {
                    "DNo",
                    "LevelCode", "LevelName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Save Data

        private MySqlCommand SavePosition(string PosCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPosition(PosCode, PosName, Level, Parent, GrdLvlCodeMin, GrdLvlCodeMax, RequestItemInd, ActingOfficialInd, ActivityCode, Percentage, TeritoryCode,"); 
            
            if (mIsVRCashAdvanceUseEmployeePIC) 
                SQL.AppendLine("CashAdvPIC,  ");
            
            SQL.AppendLine("CreateBy, CreateDt)");
            SQL.AppendLine("Values(@PosCode, @PosName, @Level, @Parent, @GrdLvlCodeMin, @GrdLvlCodeMax, @RequestItemInd, @ActingOfficialInd, @ActivityCode, @Percentage, @TeritoryCode, ");

            if (mIsVRCashAdvanceUseEmployeePIC)
                SQL.AppendLine(" @CashAdvPIC, ");
            
            SQL.AppendLine("@UserCode,  CurrentDateTime() )");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update PosName=@PosName, Level=@Level, Parent=@Parent, GrdLvlCodeMin=@GrdLvlCodeMin, GrdLvlCodeMax=@GrdLvlCodeMax, RequestItemInd=@RequestItemInd, ActingOfficialInd=@ActingOfficialInd, ActivityCode=@ActivityCode, Percentage=@Percentage, TeritoryCode=@TeritoryCode,");
            if (mIsVRCashAdvanceUseEmployeePIC)
                SQL.AppendLine("CashAdvPIC = @CashAdvPIC, ");
            SQL.AppendLine(" LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblPositionTraining Where PosCode=@PosCode; ");
            if (mIsPositionUseLevel) SQL.AppendLine("Delete From TblPositionLevel Where PosCode=@PosCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@PosName", TxtPosName.Text);
            Sm.CmParam<decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@GrdLvlCodeMin", Sm.GetLue(LueGrdLvlCodeMin));
            Sm.CmParam<String>(ref cm, "@GrdLvlCodeMax", Sm.GetLue(LueGrdLvlCodeMax));
            Sm.CmParam<String>(ref cm, "@RequestItemInd", ChkRequestItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ActingOfficialInd", ChkActingOfficial.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ActivityCode", Sm.GetLue(LueActivityCode));
            Sm.CmParam<decimal>(ref cm, "@Percentage", decimal.Parse(TxtPercentage.Text));
            Sm.CmParam<String>(ref cm, "@TeritoryCode", Sm.GetLue(LueTeritoryCode));
            if (mIsVRCashAdvanceUseEmployeePIC) Sm.CmParam<String>(ref cm, "@CashAdvPIC", ChkCashAdvancePIC.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePositionTraining(string PosCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPositionTraining(PosCode, DNo, TrainingCode, Duration, CreateBy, CreateDt) " +
                    "Values(@PosCode, @DNo, @TrainingCode, @Duration, @CreateBy, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Duration", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePositionLevel(string PosCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblPositionLevel(PosCode, DNo, LevelCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");

            int i = 0; bool IsFirst = true;

            for (int row = 0; row < Grd2.Rows.Count - 1; ++row)
            {
                if (Sm.GetGrdStr(Grd2, row, 1).Length > 0)
                {
                    if (IsFirst) IsFirst = false;
                    else SQL.AppendLine(", ");

                    SQL.AppendLine("(@PosCode, @DNo__" + i.ToString() + ", @LevelCode__" + i.ToString() + ",  ");
                    SQL.AppendLine("@CreateBy, CurrentDateTime()) ");

                    Sm.CmParam<String>(ref cm, "@DNo__" + i.ToString(), Sm.Right(string.Concat("000", (i+1).ToString()), 3));
                    Sm.CmParam<String>(ref cm, "@LevelCode__" + i.ToString(), Sm.GetGrdStr(Grd2, row, 1));

                    i++;
                }
            }

            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPosCode, "Position code", false) ||
                Sm.IsTxtEmpty(TxtPosName, "Position name", false) ||
                IsCntCodeExisted() ||
                IsLevelInvalid();
        }

        private bool IsLevelInvalid()
        {
            if (!mIsPositionUseLevel) return false;

            if (mIsPositionLevelMandatory)
            {
                if (Grd2.Rows.Count <= 1)
                {
                    TcPosition.SelectedTab = Tp2;
                    Sm.StdMsg(mMsgType.Warning, "You need to insert at least 1 level.");
                    return true;
                }
            }

            for (int row = 0; row < Grd2.Rows.Count - 1; ++row)
            {
                if (Sm.IsGrdValueEmpty(Grd2, row, 3, false, "Level is empty. You need to fill it completely or delete the empty row.")) { TcPosition.SelectedTab = Tp2; return true; }
            }

            return false;
        }

        private bool IsCntCodeExisted()
        {
            if (!TxtPosCode.Properties.ReadOnly && Sm.IsDataExist("Select PosCode From TblPosition Where PosCode='" + TxtPosCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Position code ( " + TxtPosCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additioanl method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsPositionUseLevel', 'Apakah master Position menggunakan Level ? [Y = Ya, N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");
            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('IsPositionLevelMandatory', 'Apakah tab Level di master Position mandatory? [Y = Ya, N = Tidak]', 'N', 'PHT', NULL, 'Y', 'WEDHA', '202303211455', NULL, NULL); ");

            SQL.AppendLine("CREATE TABLE IF NOT EXISTS `tblpositionlevel` ( ");
            SQL.AppendLine("    `PosCode` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `DNo` VARCHAR(3) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LevelCode` VARCHAR(16) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateBy` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `CreateDt` VARCHAR(12) NOT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpBy` VARCHAR(255) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    `LastUpDt` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci', ");
            SQL.AppendLine("    PRIMARY KEY(`PosCode`, `DNo`) USING BTREE, ");
            SQL.AppendLine("   INDEX `PosCode` (`PosCode`, `DNo`, `LevelCode`) USING BTREE ");
            SQL.AppendLine(") ");
            SQL.AppendLine("COLLATE = 'latin1_swedish_ci' ");
            SQL.AppendLine("ENGINE = InnoDB ");
            SQL.AppendLine("; ");


            Sm.ExecQuery(SQL.ToString());
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In ( ");
            SQL.AppendLine("'IsPositionUseActivity', 'IsPositionUseLevel', 'IsPositionLevelMandatory', 'IsPositionNotUseActingOfficial', 'IsVRCashAdvanceUseEmployeePIC' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            
                            case "IsPositionUseActivity": mIsPositionUseActivity = ParValue == "Y"; break;
                            case "IsPositionUseLevel": mIsPositionUseLevel = ParValue == "Y"; break;
                            case "IsPositionLevelMandatory": mIsPositionLevelMandatory = ParValue == "Y"; break;
                            case "IsPositionNotUseActingOfficial": mIsPositionNotUseActingOfficial = ParValue == "Y";break;
                            case "IsVRCashAdvanceUseEmployeePIC": mIsVRCashAdvanceUseEmployeePIC = ParValue == "Y"; break;

                        }
                    }
                }
                dr.Close();
            }
        }

        public void SetLueTrainingCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select TrainingCode As Col1, TrainingName As Col2 " +
                "From TblTraining " +
                "Order By TrainingName",
                 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Grid Events

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd1, ref LueTrainingCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                SetLueTrainingCode(ref LueTrainingCode);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2) Sm.FormShowDialog(new FrmPositionDlg(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #region Misc Control Event

        private void TxtPosName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPosName);
        }
        private void TxtPosCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPosCode);
        }

        private void LueGrdLvlCodeMin_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGrdLvlCodeMin, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
        }

        private void LueGrdLvlCodeMax_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueGrdLvlCodeMax, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
        }

        private void TxtLevel_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtLevel, 2);
        }

        private void LueTrainingCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingCode, new Sm.RefreshLue1(Sl.SetLueTrainingCode));
        }

        private void LueTrainingCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTrainingCode_Leave(object sender, EventArgs e)
        {
            if (LueTrainingCode.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueTrainingCode).Length == 2)
                    Grd1.Cells[fCell.RowIndex, 1].Value =
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueTrainingCode);
                    Grd1.Cells[fCell.RowIndex, 2].Value = LueTrainingCode.GetColumnValue("Col2");
                }
                LueTrainingCode.Visible = false;
            }
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueActivityCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueActivityCode, new Sm.RefreshLue2(Sl.SetLueOption), "PositionActivity");
        }

        private void LueTeritoryCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTeritoryCode, new Sm.RefreshLue2(Sl.SetLueOption), "PositionTeritory");
        }

        #endregion

        #endregion
       
    }
}
