﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmQt2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmQt2 mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty, mPtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmQt2Dlg(FrmQt2 FrmParent, string VdCode, string PtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
            mPtCode = PtCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.* ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.PGCode, T1.PGName, T2.UPrice, T2.DocDt ");
            SQL.AppendLine("    From TblPricingGroup T1 ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select T2b.PGCode, T2a.DocDt, T2b.UPrice  ");
            SQL.AppendLine("        From TblQt2Hdr T2a ");
            SQL.AppendLine("        Inner Join TblQt2Dtl T2b On T2a.DocNo=T2b.DocNo ");
            SQL.AppendLine("        Where T2a.VdCode=@VdCode And T2a.PtCode=@PtCode And T2b.ActInd='Y' ");
            SQL.AppendLine("    ) T2 On T1.PGCode=T2.PGCode  ");
            SQL.AppendLine("    Union All  ");
            SQL.AppendLine("    Select T.PGCode, T.PGName, 0 As UPrice, Null As DocDt ");
            SQL.AppendLine("    From TblPricingGroup T ");
            SQL.AppendLine("    Where T.PGCode Not In  ");
            SQL.AppendLine("    (  ");
            SQL.AppendLine("        Select T2.PGCode ");
            SQL.AppendLine("        From TblQt2Hdr T1 ");
            SQL.AppendLine("        Inner Join TblQt2Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Where T1.VdCode=@VdCode And T1.PtCode=@PtCode And T2.ActInd='Y' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Where Position(Concat('##', A.PGCode, '##') In @SelectedPricingGroup)<1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Pricing Group"+Environment.NewLine+"Code", 
                        "Pricing Group"+Environment.NewLine+"Name", 
                        "Latest Price"+Environment.NewLine+"(Based On Vendor"+Environment.NewLine+"And Term of Payment)",
                        "Latest"+Environment.NewLine+"Quotation Date"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 150, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedPricingGroup", mFrmParent.GetSelectedPricingGroup());
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParam<String>(ref cm, "@PtCode", mPtCode);

                Sm.FilterStr(ref Filter, ref cm, TxtPGCode.Text, new string[] { "A.PGCode", "A.PGName" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.PGCode",
                        new string[] 
                        { 
                            //0
                            "PGCode",
 
                            //1-3
                            "PGName", "UPrice", "DocDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsPGCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 6 });

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 3 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 4, 6 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 pricing group.");
        }

        private bool IsPGCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void TxtPGCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Pricing group");
        }

        #endregion

        #endregion

    }
}
