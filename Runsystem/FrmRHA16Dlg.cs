﻿#region Update
/*
    04/04/2020 [TKG/IMS] New application
    03/11/2020 [TKG/IMS] tambah payroll group
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHA16Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRHA16 mFrmParent;
        private string mHolidayDt = string.Empty, mPGCode = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRHA16Dlg(FrmRHA16 FrmParent, string HolidayDt, string PGCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mHolidayDt = HolidayDt;
            mPGCode = PGCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueOption(ref LueReligion, "Religion");
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Employee's Code",
                    "Employee's Name",
                    "Position",
                    "Department",
                    

                    //6-10
                    "Religion",
                    "Join",
                    "Resign",
                    "Salary (A)", // Grade
                    "Grade's"+Environment.NewLine+"Allowance (B)", // Grade
                    
                    //11-14
                    "Employee's"+Environment.NewLine+"Allowance (C)", // Employee
                    "A+B+C",
                    "Prorated",
                    "Amount"
                  },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 200, 180, 180, 
                        
                    //6-10
                    150, 80, 80, 130, 130, 
                    
                    //11-14
                    130, 130, 100, 130
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 13 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            var subSQL = new StringBuilder();

            subSQL.AppendLine(" (");
            subSQL.AppendLine("     Select EmpCode From TblEmployee ");
            subSQL.AppendLine("     Where Religion Is Not Null ");
            subSQL.AppendLine(Filter);
            subSQL.AppendLine("     And PGCode=@PGCode ");
            subSQL.AppendLine("     And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@HolidayDt)) ");
            subSQL.AppendLine("     And TimeStampDiff(MONTH, Concat(Left(JoinDt, 4), '-', Substring(JoinDt, 5, 2), '-', Substring(JoinDt, 7, 2)), @HolidayDate)>0 ");
            subSQL.AppendLine("     And EmpCode Not In (");
            subSQL.AppendLine("         Select T2.EmpCode ");
            subSQL.AppendLine("         From TblRHAHdr T1 ");
            subSQL.AppendLine("         Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo ");
            subSQL.AppendLine("         Where T1.CancelInd='N' ");
            subSQL.AppendLine("         And T1.Status In ('O', 'A') ");
            subSQL.AppendLine("         And Left(HolidayDt, 4)=Left(@HolidayDt, 4) ");
            subSQL.AppendLine("         ) ");
            subSQL.AppendLine(" ) ");

            var Query = subSQL.ToString();

            SQL.AppendLine("Select T.EmpCode, T.EmpName, T.PosName, T.DeptName, T.Religion, T.JoinDt, T.ResignDt, ");
            SQL.AppendLine("T.Value, T.Value2, T.Value3, T.Value4, ");
            SQL.AppendLine("Case When T.WorkingTime<12.00 Then 'Y' Else 'N' End As ProrateInd, ");
            SQL.AppendLine("Case When T.WorkingTime<12.00 Then (T.WorkingTime/12.00)*T.Value4 Else T.Value4 End As Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.EmpCode, A.EmpName, B.PosName, C.DeptName, D.OptDesc As Religion, ");
            SQL.AppendLine("    A.JoinDt, A.ResignDt, ");
            SQL.AppendLine("    IfNull(E.BasicSalary, 0.00) As Value, ");
            SQL.AppendLine("    IfNull(F.Value2, 0.00) As Value2, ");
            SQL.AppendLine("    IfNull(G.Value3, 0.00) As Value3, ");
            SQL.AppendLine("    IfNull(E.BasicSalary, 0.00)+IfNull(F.Value2, 0.00)+IfNull(G.Value3, 0.00) As Value4, ");
            SQL.AppendLine("    TimeStampDiff(MONTH, Concat(Left(A.JoinDt, 4), '-', Substring(A.JoinDt, 5, 2), '-', Substring(A.JoinDt, 7, 2)), @HolidayDate) As WorkingTime ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("    Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("    Left Join TblOption D On A.Religion=D.OptCode And D.OptCat='Religion' ");
            SQL.AppendLine("    Left Join TblGradeLevelHdr E On A.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("    Left Join (");
            SQL.AppendLine("        Select T1.GrdLvlCode, Sum(T1.Amt) Value2 ");
            SQL.AppendLine("        From TblGradeLevelDtl T1, TblAllowanceDeduction T2 ");
            SQL.AppendLine("        Where T1.ADCode=T2.ADCode ");
            SQL.AppendLine("        And T2.ADType='A' ");
            SQL.AppendLine("        And T2.AmtType='1' ");
            SQL.AppendLine("        Group By T1.GrdLvlCode ");
            SQL.AppendLine("    ) F On A.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.EmpCode, Sum(T1.Amt) As Value3 ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("        Inner Join TblAllowanceDeduction T2 On T1.ADCode=T2.ADCode And T2.ADType='A' And T2.AmtType='1' ");
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("        (T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("        (T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt<=@HolidayDt) Or ");
            SQL.AppendLine("        (T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt<=T1.EndDt) Or ");
            SQL.AppendLine("        (T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt<=@HolidayDt And @HolidayDt<=T1.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And T1.EmpCode In ");
            SQL.AppendLine(Query);
            SQL.AppendLine("        Group By T1.EmpCode ");
            SQL.AppendLine("    ) G On A.EmpCode=G.EmpCode ");
            SQL.AppendLine("    Where A.EmpCode In ");
            SQL.AppendLine(Query);
            SQL.AppendLine("     And A.PGCode=@PGCode ");
            SQL.AppendLine(") T Where Case When T.WorkingTime<12.00 Then (T.WorkingTime/12.00)*T.Value4 Else T.Value4 End>0.00 Order By T.DeptName, T.EmpName;");
            
            return SQL.ToString();
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, EmpCode = string.Empty;

                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        if (EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " EmpCode=@EmpCode0" + r.ToString();
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And Not (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParamDt(ref cm, "@HolidayDt", mHolidayDt);
                Sm.CmParam<String>(ref cm, "@HolidayDate", 
                    string.Concat(
                        Sm.Left(mHolidayDt, 4), "-",
                        mHolidayDt.Substring(4, 2), "-",
                        mHolidayDt.Substring(6, 2)
                        ));
                Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "EmpCode", "EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueReligion), "Religion", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "EmpCode", 

                            //1-5
                            "EmpName", "PosName", "DeptName", "Religion", "JoinDt",

                            //6-10
                            "ResignDt", "Value", "Value2", "Value3", "Value4", 
                            
                            //11-12
                            "ProrateInd", "Amt" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 14);

                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 12 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 9, 10, 11, 13 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            mFrmParent.ComputeAmt();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 1), EmpCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueReligion_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueReligion, new Sm.RefreshLue2(Sl.SetLueOption), "Religion");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkReligion_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Religion");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion
       
        #endregion
    }
}
