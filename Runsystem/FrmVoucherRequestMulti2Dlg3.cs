﻿#region Update
/*
    07/07/2021 [IBL/PHT] new dialog apps
    12/11/2021 [RDA/PHT] penambahan pilihan bank account debit kredit dibuat berdasarkan bank group
    18/11/2021 [SET/PHT] Feedback : Validasi CASBA cost center debit kredit dibuat berdasarkan bank group berdasar parameter IsFilterByBankAccount
    12/05/2022 [VIN/PHT] BUG mode manual currency ambil dr banck acc pertama 

 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestMulti2Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestMulti2 mFrmParent;
        private string mSQL = string.Empty;
        private int mRow;

        #endregion

        #region Constructor

        public FrmVoucherRequestMulti2Dlg3(FrmVoucherRequestMulti2 FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BankAcCode, BankAcName, CurCode From ( ");
            SQL.AppendLine("    Select BankAcCode, ");
            SQL.AppendLine("    Trim(Concat( ");
            SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
            SQL.AppendLine("    Case When A.BankAcNo Is Not Null Then Concat(A.BankAcNo) Else IfNull(A.BankAcNm, '') End, ");
            SQL.AppendLine("    Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
            SQL.AppendLine(")) As BankAcName, CurCode ");
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            if (mFrmParent.mIsFilterByBankAccount)
            {
                SQL.AppendLine("And BankAcCode In ( ");
                SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Account Code", 
                        "Bank",
                        "CurCode",
                    }
                );
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColInvisible(Grd1, new int[] { 3 });


            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Account Code", 
                        "Bank",
                        "CurCode",
                      },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 450, 0
                    }
                );
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 3 });


        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtBankAcCode.Text, new string[] { "T.BankAcCode", "T.BankAcName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.BankAcName;",
                        new string[] 
                        { 
                             //0-2
                            "BankAcCode", "BankAcName", "CurCode"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    Sm.CopyGrdValue(mFrmParent.Grd1, mRow, 1, Grd1, Grd1.CurRow.Index, 1);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mRow, 2, Grd1, Grd1.CurRow.Index, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mRow, 23, Grd1, Grd1.CurRow.Index, 3);
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBankAcCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bank account");
        }

        #endregion

        #endregion
    }
}
