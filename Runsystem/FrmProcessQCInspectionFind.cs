﻿#region Update
/*
    16/07/2018 [WED] menampilkan CancelInd per item
    15/06/2022 [RIS/IOK] Menambahkan kolom End Time dengan parameter IsMaterialQCInspectionWOShowEndTimeInspector
    15/06/2022 [RIS/IOK] Menambahkan kolom Inspector's Name dengan parameter IsMaterialQCInspectionWOShowEndTimeInspector
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmProcessQCInspectionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProcessQCInspection mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsMaterialQCInspectionWOShowEndTimeInspector = false;

        #endregion

        #region Constructor

        public FrmProcessQCInspectionFind(FrmProcessQCInspection FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A1.CancelInd, A.QCPlanningDocNo, C.DocName, A.StartTm, A.EndTm, ");
            if (mIsMaterialQCInspectionWOShowEndTimeInspector)
                SQL.AppendLine("Group_CONCAT(E.EmpName ORDER BY E.EmpName SEPARATOR ', ') as Inspection, ");
            else
                SQL.AppendLine("null as Inspection, ");
            SQL.AppendLine("A1.CreateBy, A1.CreateDt, A1.LastUpBy, A1.LastUpDt ");
            SQL.AppendLine("From TblProcessQCInspectionHdr A ");
            SQL.AppendLine("Inner Join TblProcessQCInspectionDtl A1 On A.DocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join TblQCPlanningHdr B On A.QCPlanningDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr C On B.WorkCenterDocNo=C.DocNo ");
            if(mIsMaterialQCInspectionWOShowEndTimeInspector)
            {
                SQL.AppendLine("Inner Join TblProcessQCInspectionDtl2 D On A.DocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblEmployee E On D.EmpCode = E.EmpCode ");
            }
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Planning Document#",
                        "",
                        
                        //6-10
                        "Work Center",
                        "Inspection Time",
                        "End Time",
                        "Inspector's Name",
                        "Created By",
                        
                        //11-15
                        "Created Date",
                        "Created Time", 
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 80, 150, 20, 
                        
                        //6-10
                        250, 100, 100, 250, 100, 
                        
                        //11-14
                        100, 100, 130, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 7, 8, 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, false);
            if (!mIsMaterialQCInspectionWOShowEndTimeInspector)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtQCPlanningDocNo.Text, "A.QCPlanningDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenter.Text, new string[] { "B.WorkCenterDocNo", "C.DocName", "C.ShortCode" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Group By A.DocNo, A1.DNo Order By A.DocDt, A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CancelInd", "QCPlanningDocNo", "DocName", "CreateBy", 
                            
                            //6-10
                            "CreateDt", "LastUpBy", "LastUpDt", "StartTm", "EndTm", "Inspection"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            if (Sm.DrStr(dr, 2) == "Y")
                            {
                                Grd.Rows[Row].BackColor = Color.LightCoral;
                                Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            }
                            else
                            {
                                Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            if(mIsMaterialQCInspectionWOShowEndTimeInspector)
                            {
                                Sm.SetGrdValue("T2", Grd, dr, c, Row, 7, 9);
                                Sm.SetGrdValue("T2", Grd, dr, c, Row, 8, 10);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 11);
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 6);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 8);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 8);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmQCPlanning(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmQCPlanning(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsMaterialQCInspectionWOShowEndTimeInspector = Sm.GetParameterBoo("IsMaterialQCInspectionWOShowEndTimeInspector");
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtQCPlanningDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkQCPlanningDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "QC Planning Document#");
        }

        private void TxtWorkCenter_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work Center");
        }

        #endregion

        #endregion

    }
}
