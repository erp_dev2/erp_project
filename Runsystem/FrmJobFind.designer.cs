﻿namespace RunSystem
{
    partial class FrmJobFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtJobName = new DevExpress.XtraEditors.TextEdit();
            this.ChkJobName = new DevExpress.XtraEditors.CheckEdit();
            this.ChkJobCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueJobCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJobName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJobName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJobCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJobCtCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkJobCtCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueJobCtCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtJobName);
            this.panel2.Controls.Add(this.ChkJobName);
            this.panel2.Size = new System.Drawing.Size(672, 54);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 419);
            this.Grd1.TabIndex = 12;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(39, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "Job";
            // 
            // TxtJobName
            // 
            this.TxtJobName.EnterMoveNextControl = true;
            this.TxtJobName.Location = new System.Drawing.Point(70, 5);
            this.TxtJobName.Name = "TxtJobName";
            this.TxtJobName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJobName.Properties.Appearance.Options.UseFont = true;
            this.TxtJobName.Properties.MaxLength = 250;
            this.TxtJobName.Size = new System.Drawing.Size(319, 20);
            this.TxtJobName.TabIndex = 10;
            this.TxtJobName.Validated += new System.EventHandler(this.TxtJobName_Validated);
            // 
            // ChkJobName
            // 
            this.ChkJobName.Location = new System.Drawing.Point(393, 4);
            this.ChkJobName.Name = "ChkJobName";
            this.ChkJobName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkJobName.Properties.Appearance.Options.UseFont = true;
            this.ChkJobName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkJobName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkJobName.Properties.Caption = " ";
            this.ChkJobName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkJobName.Size = new System.Drawing.Size(31, 22);
            this.ChkJobName.TabIndex = 11;
            this.ChkJobName.ToolTip = "Remove filter";
            this.ChkJobName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkJobName.ToolTipTitle = "Run System";
            this.ChkJobName.CheckedChanged += new System.EventHandler(this.ChkJobName_CheckedChanged);
            // 
            // ChkJobCtCode
            // 
            this.ChkJobCtCode.Location = new System.Drawing.Point(393, 25);
            this.ChkJobCtCode.Name = "ChkJobCtCode";
            this.ChkJobCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkJobCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkJobCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkJobCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkJobCtCode.Properties.Caption = " ";
            this.ChkJobCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkJobCtCode.Size = new System.Drawing.Size(31, 22);
            this.ChkJobCtCode.TabIndex = 17;
            this.ChkJobCtCode.ToolTip = "Remove filter";
            this.ChkJobCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkJobCtCode.ToolTipTitle = "Run System";
            this.ChkJobCtCode.CheckedChanged += new System.EventHandler(this.ChkJobCtCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(9, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueJobCtCode
            // 
            this.LueJobCtCode.EnterMoveNextControl = true;
            this.LueJobCtCode.Location = new System.Drawing.Point(70, 26);
            this.LueJobCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueJobCtCode.Name = "LueJobCtCode";
            this.LueJobCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueJobCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueJobCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueJobCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueJobCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueJobCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueJobCtCode.Properties.DropDownRows = 30;
            this.LueJobCtCode.Properties.NullText = "[Empty]";
            this.LueJobCtCode.Properties.PopupWidth = 350;
            this.LueJobCtCode.Size = new System.Drawing.Size(319, 20);
            this.LueJobCtCode.TabIndex = 16;
            this.LueJobCtCode.ToolTip = "F4 : Show/hide list";
            this.LueJobCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueJobCtCode.EditValueChanged += new System.EventHandler(this.LueJobCtCode_EditValueChanged);
            // 
            // FrmJobFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmJobFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJobName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJobName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJobCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJobCtCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtJobName;
        private DevExpress.XtraEditors.CheckEdit ChkJobName;
        private DevExpress.XtraEditors.CheckEdit ChkJobCtCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueJobCtCode;
    }
}