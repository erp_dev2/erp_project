﻿#region Update
/*
    14/06/2017 [WED] tambah MeeRemark di ClearData()
    31/10/2017 [WED] tambah filter position ke dialog 2
    26/03/2018 [WED] panel diganti ke split container
    26/03/2018 [WED] Position ambil dari ManualPosName TblOrganizationalStructure
    26/03/2018 [WED] tambah tab experience dan tab education
    31/03/2018 [TKG] tambah filter site dan level 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProposeCandidate : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty, 
            mCity = string.Empty, 
            mCnt = string.Empty, 
            mExpCity = string.Empty;
        internal bool
            mIsFilterBySiteHR = false,
            mIsFilterByLevelHR = false;
        internal FrmProposeCandidateFind FrmFind;

        #endregion

        #region Constructor

        public FrmProposeCandidate(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmProposeCandidate");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLuePosCode(ref LueParentCode);
                SetLuePosCode2(ref LuePosCode2);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByLevelHR = Sm.GetParameterBoo("IsFilterByLevelHR");
        }

        #region Set Grid

        private void SetGrd()
        {
            #region Grid 1 - Competence

            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "DNo",

                    //1-4
                    "",
                    "Competence"+Environment.NewLine+"Code",
                    "Competence"+Environment.NewLine+"Name",
                    "Minimal"+Environment.NewLine+"Score"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-4
                    20, 100, 200, 80
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 2);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 }, false);

            #endregion

            #region Grid 2 - Employee

            Grd2.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "DNo", 
                    
                    //1-5
                    "",
                    "Employee Code",
                    "Employee Name",
                    "Competence",
                    "DocType",

                    //6-7
                    "Education",
                    "Experience"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    20, 100, 200, 200, 0,

                    //6-7
                    250, 200
                }
            );
            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 5 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7 });

            #endregion

            #region Grid 3 - Experience

            Grd3.Cols.Count = 9;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                {
                    //0
                    "DNo",

                    //1-2
                    "",
                    "PosCode",
                    "PosCode2",
                    "OrgStrDNo",
                    "Name",

                    //6-8
                    "Description",
                    "LevelCode",
                    "Level Working"+Environment.NewLine+"Experience",
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    20, 0, 0, 0, 120, 
                    
                    //6-7
                    200, 0, 180
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 3, 4, 7 }, false);

            #endregion

            #region Grid 4 - Education

            Grd4.Cols.Count = 10;
            Grd4.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd4, new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "",
                    "PosCode",
                    "PosCode2",
                    "OrgStrDNo",
                    "Education",

                    //6-9
                    "Education Level",
                    "Major",
                    "Name",
                    "Description"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    20, 0, 0, 0, 180, 
                    
                    //6-8
                    250, 120, 180, 250
                }
            );
            Sm.GrdColButton(Grd4, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd4, new int[] { 0, 2, 3, 4, 5, 7 }, false);

            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd2, new int[] { 2 }, !ChkHideInfoInGrd.Checked);

            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtProcessInd, LuePosCode2, LueParentCode, MeeRemark
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd2.ReadOnly = 
                    Grd1.ReadOnly =
                    Grd3.ReadOnly =
                    Grd4.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LuePosCode2, MeeRemark
                    }, false);
                    Grd2.ReadOnly = 
                    Grd1.ReadOnly =
                    Grd3.ReadOnly =
                    Grd4.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         MeeCancelReason, ChkCancelInd
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    Grd2.ReadOnly =
                    Grd1.ReadOnly =
                    Grd3.ReadOnly =
                    Grd4.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, MeeCancelReason, TxtProcessInd, LuePosCode2, LueParentCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProposeCandidateFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method 

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LuePosCode2, "Position"))
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmProposeCandidateDlg(this, Sm.GetLue(LuePosCode2)));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
                !Sm.IsLueEmpty(LuePosCode2, "Position"))
            {
                Sm.FormShowDialog(new FrmProposeCandidateDlg(this, Sm.GetLue(LuePosCode2)));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 competence");
                return;
            }
            if (e.ColIndex == 1 && Grd1.Rows.Count > 1)
            {
                string competencecode = string.Empty, educationcode = string.Empty, experiencecode = string.Empty;

                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (competencecode.Length != 0) competencecode += ",";
                    competencecode += Sm.GetGrdStr(Grd1, r, 2);
                }

                if(Grd3.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd3.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd3, r, 5).Length > 0)
                        {
                            if (experiencecode.Length > 0) experiencecode += ",";
                            experiencecode += Sm.GetGrdStr(Grd3, r, 5);
                        }
                    }
                }

                if (Grd4.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd4.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd4, r, 5).Length > 0)
                        {
                            if (educationcode.Length > 0) educationcode += ",";
                            educationcode += Sm.GetGrdStr(Grd4, r, 5);
                        }
                    }
                }

                string mPosCode = Sm.GetValue("Select PosCode From TblOrganizationalStructure Where PosCode2 = '" + Sm.GetLue(LuePosCode2) + "' Limit 1");

                Sm.FormShowDialog(new FrmProposeCandidateDlg2(this, competencecode, mPosCode, Sm.GetLue(LuePosCode2), experiencecode, educationcode));
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
            }
            //Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 competence");
                return;
            }

            if (e.ColIndex == 1 && Grd1.Rows.Count > 1)
            {
                e.DoDefault = false;

                string competencecode = string.Empty, educationcode = string.Empty, experiencecode = string.Empty;

                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (competencecode.Length != 0) competencecode += ",";
                    competencecode += Sm.GetGrdStr(Grd1, r, 2);
                }

                if (Grd3.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd3.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd3, r, 5).Length > 0)
                        {
                            if (experiencecode.Length > 0) experiencecode += ",";
                            experiencecode += Sm.GetGrdStr(Grd3, r, 5);
                        }
                    }
                }

                if (Grd4.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd4.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(Grd4, r, 5).Length > 0)
                        {
                            if (educationcode.Length > 0) educationcode += ",";
                            educationcode += Sm.GetGrdStr(Grd4, r, 5);
                        }
                    }
                }

                string mPosCode = Sm.GetValue("Select PosCode From TblOrganizationalStructure Where PosCode2 = '" + Sm.GetLue(LuePosCode2) + "' Limit 1");

                Sm.FormShowDialog(new FrmProposeCandidateDlg2(this, competencecode, mPosCode, Sm.GetLue(LuePosCode2), experiencecode, educationcode));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                Sm.FormShowDialog(new FrmProposeCandidateDlg3(this, Sm.GetLue(LuePosCode2)));
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.ClearGrd(Grd2, true);
            }
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmProposeCandidateDlg3(this, Sm.GetLue(LuePosCode2)));
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                Sm.FormShowDialog(new FrmProposeCandidateDlg4(this, Sm.GetLue(LuePosCode2)));
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
                Sm.ClearGrd(Grd2, true);
            }
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmProposeCandidateDlg4(this, Sm.GetLue(LuePosCode2)));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProposeCandidate", "TblProposeCandidateHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProposeCandidateHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveProposeCandidateDtl(DocNo, Row));

            for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
                if (Sm.GetGrdStr(Grd2, Row2, 2).Length > 0) cml.Add(SaveProposeCandidateDtl2(DocNo, Row2));

            for (int Row3 = 0; Row3 < Grd3.Rows.Count; Row3++)
                if (Sm.GetGrdStr(Grd3, Row3, 2).Length > 0) cml.Add(SaveProposeCandidateDtl3(DocNo, Row3));

            for (int Row4 = 0; Row4 < Grd4.Rows.Count; Row4++)
                if (Sm.GetGrdStr(Grd4, Row4, 2).Length > 0) cml.Add(SaveProposeCandidateDtl4(DocNo, Row4));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LuePosCode2, "Position") ||
                IsGrd1Empty() ||
                IsGrd2Empty() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdExceedMaxRecords()
        {
            if ((Grd1.Rows.Count > 1000) || (Grd2.Rows.Count > 1000))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered ( Competence : " + (Grd1.Rows.Count - 1).ToString() + " ; Employee : " + (Grd2.Rows.Count - 1).ToString() + " ) exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 competence.");
                return true;
            }
            return false;
        }

        private bool IsGrd2Empty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 proposed employee.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveProposeCandidateHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProposeCandidateHdr(DocNo, DocDt, CancelInd, ProcessInd, PosCode2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, 'O', @PosCode2, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PosCode2", Sm.GetLue(LuePosCode2));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveProposeCandidateDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProposeCandidateDtl(DocNo, DNo, CompetenceCode, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @CompetenceCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProposeCandidateDtl2(string DocNo, int Row)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Insert Into TblProposeCandidateDtl2(DocNo, DNo, EmpCode, DocType, CreateBy, CreateDt) ");
            SQLDtl2.AppendLine("Values(@DocNo, @DNo, @EmpCode, @DocType, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl2.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProposeCandidateDtl3(string DocNo, int Row)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Insert Into TblProposeCandidateDtl3(DocNo, DNo, PosCode, PosCode2, OrgStrDNo, Name, Description, LevelCode, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Values(@DocNo, @DNo, @PosCode, @PosCode2, @OrgStrDNo, @Name, @Description, @LevelCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl3.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@PosCode2", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParam<String>(ref cm, "@OrgStrDNo", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd3, Row, 5));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProposeCandidateDtl4(string DocNo, int Row)
        {
            var SQLDtl4 = new StringBuilder();

            SQLDtl4.AppendLine("Insert Into TblProposeCandidateDtl4(DocNo, DNo, PosCode, PosCode2, OrgStrDNo, Education, Major, Name, Description, CreateBy, CreateDt) ");
            SQLDtl4.AppendLine("Values(@DocNo, @DNo, @PosCode, @PosCode2, @OrgStrDNo, @Education, @Major, @Name, @Description, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl4.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetGrdStr(Grd4, Row, 2));
            Sm.CmParam<String>(ref cm, "@PosCode2", Sm.GetGrdStr(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@OrgStrDNo", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@Education", Sm.GetGrdStr(Grd4, Row, 5));
            Sm.CmParam<String>(ref cm, "@Major", Sm.GetGrdStr(Grd4, Row, 7));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd4, Row, 8));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd4, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateProposeCandidateHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsLueEmpty(LuePosCode2, "Position") ||
                IsProposeCandidateNotCancelled() ||
                IsProposeCandidateAlreadyCancelled() ||
                IsProposeCandidateAlreadyFullfiled() ||
                IsProposeCandidateAlreadyProcessed();
        }

        private MySqlCommand UpdateProposeCandidateHdr()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProposeCandidateHdr Set ");
            SQL.AppendLine("    ProcessInd='O', CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsProposeCandidateNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsProposeCandidateAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblProposeCandidateHdr " +
                    "Where DocNo=@DocNo And CancelInd='Y' ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already cancelled.");
                ChkCancelInd.Checked = true;
                return true;
            }
            return false;
        }

        private bool IsProposeCandidateAlreadyFullfiled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblProposeCandidateHdr " +
                    "Where DocNo=@DocNo And ProcessInd = 'F';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already fullfiled.");
                return true;
            }
            return false;
        }

        private bool IsProposeCandidateAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            string PPSDocNo = Sm.GetValue("Select IfNull(PPSDocNo, '') From TblProposeCandidateDtl2 Where DocNo = '" + TxtDocNo.Text + "';");
            SQL.AppendLine("Select PPSDocNo ");
            SQL.AppendLine("From TblProposeCandidateDtl2 ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            //var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            //Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            //if (Sm.IsDataExist(cm))
            if (PPSDocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "One of employee from this data is processed to PPS.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowProposeCandidateHdr(DocNo);
                ShowProposeCandidateDtl(DocNo);
                ShowProposeCandidateDtl2(DocNo);
                ShowProposeCandidateDtl3(DocNo);
                ShowProposeCandidateDtl4(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProposeCandidateHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.PosCode2, B.Parent As DeptCode, A.Remark, ");
            SQL.AppendLine("Case IfNull(A.ProcessInd, '') When 'F' Then 'Fulfilled' When 'O' Then 'Outstanding' Else '' End As ProcessInd ");
            SQL.AppendLine("From TblProposeCandidateHdr A ");
            SQL.AppendLine("Left Join TblOrganizationalStructure B On A.PosCode2 = B.PosCode2 ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "PosCode2", "DeptCode",

                        //6-7
                        "ProcessInd", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LuePosCode2, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueParentCode, Sm.DrStr(dr, c[5]));
                        TxtProcessInd.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowProposeCandidateDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select B.DNo, B.CompetenceCode, C.CompetenceName, D.MinScore ");
            SQLDtl.AppendLine("From TblProposeCandidateHdr A ");
            SQLDtl.AppendLine("Inner Join TblProposeCandidateDtl B On A.DocNo = B.DocNo");
            SQLDtl.AppendLine("Inner Join TblCompetence C On B.CompetenceCode = C.CompetenceCode ");
            SQLDtl.AppendLine("Inner Join TblOrganizationalStructureDtl4 D On A.PosCode2 = D.PosCode2 And B.CompetenceCode = D.CompetenceCode ");
            SQLDtl.AppendLine("Where A.DocNo = @DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-3
                    "CompetenceCode", "CompetenceName", "MinScore"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 1);

        }

        private void ShowProposeCandidateDtl2(string DocNo)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select T.DocType, T.DocNo, T.DNo, T.EmpCode, T.EmpName, T.Competence, T.EducationLevel, T.Experience ");
            SQLDtl2.AppendLine("From ");
            SQLDtl2.AppendLine("( ");
            SQLDtl2.AppendLine("    Select A.DocType, A.DocNo, A.DNo, A.EmpCode, B.EmpName, Group_Concat(D.CompetenceName Separator ', ') As Competence, ");
            SQLDtl2.AppendLine("    G.OptDesc As EducationLevel, H.Experience ");
            SQLDtl2.AppendLine("    From TblProposeCandidateDtl2 A ");
            SQLDtl2.AppendLine("    Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQLDtl2.AppendLine("    Left Join TblEmployeeCompetence C On A.EmpCode = C.EmpCode ");
            SQLDtl2.AppendLine("    Left Join TblCompetence D On C.CompetenceCode = D.CompetenceCode ");
            SQLDtl2.AppendLine("    Left Join TblEmployeeRecruitment E On A.EmpCode = E.DocNo ");
            SQLDtl2.AppendLine("    Left JOin ");
            SQLDtl2.AppendLine("    ( ");
            SQLDtl2.AppendLine("        Select T1.EmpCode, MIN(T1.Level) As Level ");
            SQLDtl2.AppendLine("        From TblEmployeeEducation T1 ");
            SQLDtl2.AppendLine("        Group By T1.EmpCode ");
            SQLDtl2.AppendLine("    ) F On B.EmpCode = F.EmpCode ");
            SQLDtl2.AppendLine("    Left Join TblOption G On F.Level = G.OptCode And G.OptCat = 'EmployeeEducationLevel' ");
            SQLDtl2.AppendLine("    Left Join ");
            SQLDtl2.AppendLine("    ( ");
            SQLDtl2.AppendLine("        Select X1.EmpCode, Group_Concat(Concat(X1.Position, ' [', X1.Period, ']') Separator '; ') As Experience ");
            SQLDtl2.AppendLine("        From TblEmployeeWorkExp X1 ");
            SQLDtl2.AppendLine("        Group By X1.EmpCode ");
            SQLDtl2.AppendLine("    ) H On B.EmpCode = H.EmpCode ");
            SQLDtl2.AppendLine("    Where A.DocNo = @DocNo ");
            SQLDtl2.AppendLine("    Group By A.DocNo, A.DNo, A.EmpCode ");
            SQLDtl2.AppendLine("    Union All ");
            SQLDtl2.AppendLine("    Select A.DocType, A.DocNo, A.DNo, A.EmpCode, B.EmpName, Null As Competence, Null As EducationLevel, Null As Experience ");
            SQLDtl2.AppendLine("    From TblProposeCandidateDtl2 A ");
            SQLDtl2.AppendLine("    Inner Join TblEmployeeRecruitment B On A.EmpCode = B.DocNo ");
            SQLDtl2.AppendLine("    Where A.DocNo = @DocNo ");
            SQLDtl2.AppendLine(") T ");
            SQLDtl2.AppendLine("Order By T.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQLDtl2.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "Competence", "DocType", "EducationLevel",

                    //6
                    "Experience"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);

        }

        private void ShowProposeCandidateDtl3(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.DNo, A.PosCode, A.PosCode2, A.OrgStrDNo, A.Name, A.Description, A.LevelCode, B.LevelName ");
            SQLDtl.AppendLine("From TblProposeCandidateDtl3 A ");
            SQLDtl.AppendLine("Left Join TblLevelHdr B On A.LevelCode = B.LevelCode ");
            SQLDtl.AppendLine("Where A.DocNo = @DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "PosCode", "PosCode2", "OrgStrDNo", "Name", "Description",
                    
                    //6-7
                    "LevelCode", "LevelName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowProposeCandidateDtl4(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.*, B.OptDesc As EducationLevel ");
            SQLDtl.AppendLine("From TblProposeCandidateDtl4 A ");
            SQLDtl.AppendLine("Left Join TblOption B On A.Education = B.OptCode And B.OptCat = 'EmployeeEducationLevel' ");
            SQLDtl.AppendLine("Where DocNo = @DocNo Order By DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "PosCode", "PosCode2", "OrgStrDNo", "Education", "EducationLevel", 
                    
                    //6-8
                    "Major", "Name", "Description"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);

        }

        #endregion

        #region Additional Method

        public static void SetLuePosCode2(ref LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T1.PosCode2 As Col1, T1.PosName2 As Col2 ");
                SQL.AppendLine("From TblOrganizationalStructure T1 ");
                SQL.AppendLine("Where Length(Trim(T1.PosName2)) > 0 ");
                SQL.AppendLine("Order By T1.PosName2; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Position", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedCompetence()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedEducation()
        {
            var SQL = string.Empty;
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd4, Row, 2) + "##" + Sm.GetGrdStr(Grd4, Row, 3) + "##" + Sm.GetGrdStr(Grd4, Row, 4) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX##XXX##XXX'" : SQL);
        }

        internal string GetSelectedExperience()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd3, Row, 2) + "##" + Sm.GetGrdStr(Grd3, Row, 3) + "##" + Sm.GetGrdStr(Grd3, Row, 4) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX##XXX##XXX'" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePosCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode2, new Sm.RefreshLue1(SetLuePosCode2));
            ClearGrd();
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  LueParentCode
            });

            if (BtnSave.Enabled && Sm.GetLue(LuePosCode2).Length > 0)
            {
                string DeptCode = Sm.GetValue("Select Parent From TblOrganizationalStructure Where PosCode2 = '" + Sm.GetLue(LuePosCode2) + "'; ");
                Sm.SetLue(LueParentCode, DeptCode);
                //SetLueDeptCode(ref LueParentCode, DeptCode);
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion

    }
}
