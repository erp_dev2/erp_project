#region Update
/*
    01/05/2017 [TKG] Modifikasi tampilan dan proses Sales Return Invoice
    18/05/2017 [TKG] tambah validasi apabila data sudah diproses di voucher request vat ketika diedit
    11/06/2017 [TKG] tambah validasi vat settlement ketika data hendak dicancel. 
    12/06/2017 [TKG] bug fixing item muncul berulang kali.
    30/07/2017 [TKG] 1 dokumen 1 received# berdasarkan parameter.
    23/08/2017 [HAR] query function ShowRecvCtInfo tambah union ke sales invoice CBD.
    12/09/2017 [TKG] perhitungan journal menggunakan pajak.
    06/10/2020 [WED/SIER] dapat menarik item dari do to customer pada menu SALES RETURN INVOICE. Tolong ditambahkan di ALL CLIENT.
    06/01/2021 [IBL/SRN] validasi coa kosong saat save journal berdasarkan parameter IsCheckCOAJournalNotExists 
    05/08/2021 [RDA/KSM] penambahan field "Department" mandatory dan menggunakan parameter IsSalesReturnInvoiceUseDepartment
    14/09/2021 [IBL/ALL] validasi tidak bisa save ketika terdapat setting coa yang masih kosong. Berdasarkan parameter IsJournalValidationSalesReturnInvoiceEnabled
    10/10/2022 [MAU/PRODUCT] tambah dialog rujukan DO To Customer , Sales Invoice, dan Voucher. 
    11/10/2022 [SET/PRODUCT] add tab Tax bersumber dari Tax SLI
    13/10/2022 [SET/PRODUCT] Penyesuaian Jurnal berdasar param SalesReturnInvoiceJournalFormat
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesReturnInvoice : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mCtCode = string.Empty;
        internal FrmSalesReturnInvoiceFind FrmFind;
        private bool 
            mIsAutoJournalActived = false,
            mIsSalesReturnInvoiceCanOnlyHave1Record = false,
            mIsCheckCOAJournalNotExists = false,
            mIsSalesReturnInvoiceUseDepartment = false,
            mIsJournalValidationSalesReturnInvoiceEnabled = false,
            mSalesReturnInvoiceShowSource = false,
            mIsAcNoForSalesReturnUseItemCategory = false
            ;
        private string
            mMainCurCode = string.Empty,
            mCustomerAcNoAR = string.Empty,
            mAcNoForSalesReturnInvoice = string.Empty,
            mSalesReturnInvoiceJournalFormat = string.Empty;
        internal bool
            mIsSalesInvoiceTaxEnabled = false;

        #endregion

        #region Constructor

        public FrmSalesReturnInvoice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Sales Return Invoice";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mIsSalesReturnInvoiceUseDepartment)
                {
                    LueDeptCode.Visible = true;
                    LblDeptCode.Visible = true;
                    Sl.SetLueDeptCode(ref LueDeptCode);
                }

                if (!mIsSalesInvoiceTaxEnabled)
                    TpTax.PageVisible = false;

                if (mIsSalesInvoiceTaxEnabled)
                {
                    label15.Visible = false;
                    TxtTaxRate.Visible = false;
                    label16.Visible = false;
                }

                if (!mSalesReturnInvoiceShowSource)
                {
                    label18.Visible = false;
                    TxtDOCt.Visible = false;
                    BtnDOCt.Visible = false;

                    label19.Visible = false;
                    TxtSalesInvoice.Visible = false;
                    BtnSalesInvoice.Visible = false;

                    label20.Visible = false;
                    TxtVoucher.Visible = false;
                    BtnVoucher.Visible = false;
                }

                }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Received#",
                        "Received DNo",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Quantity",

                        //6-10
                        "UoM",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                        "UoM",
                        "Amount",
                        
                        //11-14
                        "Tax Invoice#",
                        "Tax Invoice"+Environment.NewLine+"Date",
                        "Tax Rate",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        0,

                        //1-5
                        0, 0, 100, 200, 80, 

                        //6-10
                        80, 60, 120, 60, 130, 
                        
                        //11-14
                        150, 120, 100, 250
                    }
                );

            Sm.GrdFormatDate(Grd1, new int[] { 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 8, 10, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 6;
            Grd2.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit"+Environment.NewLine+"Amount",
                        "Credit"+Environment.NewLine+"Amount",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5 });
            Sm.GrdFormatDec(Grd2, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, false);

            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason, ChkCancelInd, DteDocDt, TxtLocalDocNo, TxtRecvCtDocNo, TxtTaxInvoiceDocument, DteTaxInvoiceDt, MeeRemark, 
                        LueDeptCode, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, LueTaxCode1, LueTaxCode2, LueTaxCode3, TxtAlias1, 
                        TxtAlias2, TxtAlias3, DteTaxInvoiceDt1, DteTaxInvoiceDt2, DteTaxInvoiceDt3, TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtDOCt, TxtSalesInvoice, TxtVoucher
                    }, true);
                    BtnRecvCtDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 3, 4, 5 });
                    TcSalesReturnInvoice.SelectedTabPage = this.TpTax;
                    Sl.SetLueTaxCode(ref LueTaxCode1);
                    Sl.SetLueTaxCode(ref LueTaxCode2);
                    Sl.SetLueTaxCode(ref LueTaxCode3);
                    TcSalesReturnInvoice.SelectedTabPage = this.TpGeneral;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, TxtTaxInvoiceDocument, DteTaxInvoiceDt, MeeRemark, LueDeptCode
                    }, false);
                    BtnRecvCtDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 3, 4, 5 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCtCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, TxtLocalDocNo, MeeCancelReason, TxtCtCode, 
                 TxtRecvCtDocNo, TxtTaxInvoiceDocument, DteTaxInvoiceDt, TxtCurCode, MeeRemark,
                 TxtVoucherRequestPPNDocNo, TxtJournalDocNo, TxtJournalDocNo2, LueDeptCode,
                 TxtAlias1, TxtAlias2, TxtAlias3, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3,
                 LueTaxCode1, LueTaxCode2, LueTaxCode3, DteTaxInvoiceDt1, DteTaxInvoiceDt2, DteTaxInvoiceDt3,
                 TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3, TxtDOCt, TxtSalesInvoice, TxtVoucher
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { TxtAmtBefTax, TxtTaxRate, TxtTaxAmt, TxtTotalAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 8, 10, 13 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3, 4 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesReturnInvoiceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesReturnInvoice", "TblSalesReturnInvoiceHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesReturnInvoiceHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveSalesReturnInvoiceDtl(DocNo, Row));

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveSalesReturnInvoiceDtl2(DocNo, Row));
            }

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            
            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtRecvCtDocNo, "Received#", false) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsCurrencyNotValid() ||
                IsTaxInvoiceNoNotValid() ||
                IsTaxInvoiceDtNotValid() ||
                IsTaxRateNotValid() ||
                IsRecvCtAlreadyProcessed() ||
                IsRecvCtAlreadyCancelled() ||
                IsJournalAmtNotBalanced() ||
                IsSalesReturnInvoiceNotValid() ||
                //(mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsCOAJournalNotValid()) ||
                IsJournalSettingInvalid() ||
                IsLueDeptCodeNotValid()
                ;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsJournalValidationSalesReturnInvoiceEnabled) return false;

            string
                AcNoForSalesReturnInvoice = Sm.GetParameter("AcNoForSalesReturnInvoice"),
                CustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR"),
                AcNoForVATOut = Sm.GetParameter("AcNoForVATOut");
            var Msg =
                    "Journal's setting is invalid." + Environment.NewLine +
                    "Please contact Finance/Accounting department." + Environment.NewLine;

            if (AcNoForSalesReturnInvoice.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForSalesReturnInvoice is empty.");
                return true;
            }
            if (CustomerAcNoAR.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoAR is empty.");
                return true;
            }
            if (AcNoForVATOut.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForVATOut is empty.");
                return true;
            }

            return false;
        }
        
        private bool IsLueDeptCodeNotValid()
        {
            if (mIsSalesReturnInvoiceUseDepartment && Sm.GetLue(LueDeptCode) == String.Empty) 
            {
                Sm.StdMsg(mMsgType.Warning, "Department still empty");
                return true;
            }
            return false;
        }
        

        private bool IsCOAJournalNotValid()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

	        SQL.AppendLine("Select ParValue As AcNo From TblParameter Where ParCode='AcNoForSalesReturnInvoice' ");
	        SQL.AppendLine("Union All ");
	        SQL.AppendLine("Select ParValue As AcNo From TblParameter Where ParCode='AcNoForVATOut' ");
	        SQL.AppendLine("Union All ");
	        SQL.AppendLine("Select ParValue As AcNo From TblParameter Where ParCode='CustomerAcNoAR' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    "AcNo",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Sm.DrStr(dr, c[0]).Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
                            return true;
                        }
                    }
                }
                dr.Close();
            }

            return false;
        }

        private bool IsSalesReturnInvoiceNotValid()
        {
            if (mIsSalesReturnInvoiceCanOnlyHave1Record)
            {
                int x = 0;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    {
                        x += 1;
                        if (x > 1)
                        {
                            Sm.StdMsg(mMsgType.Warning, "One document can only process 1 Received#.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Additional cost, discount information entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd2, Row, 3) == 0m && Sm.GetGrdDec(Grd2, Row, 4) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount# : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd2, Row, 3) != 0m && Sm.GetGrdDec(Grd2, Row, 4) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount# : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount should not be bigger than 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsRecvCtAlreadyProcessed()
        {
            if (Sm.IsDataExist("Select DocNo From TblRecvCtHdr Where SalesReturnInvoiceInd='F' And DocNo=@Param;", TxtRecvCtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This received# already processed to sales return invoice.");
                return true;
            }
            return false;
        }

        private bool IsRecvCtAlreadyCancelled()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                    {
                        if (IsRecvCtAlreadyCancelled(r)) return true;
                    }
                }
            }
            return false;
        }

        private bool IsRecvCtAlreadyCancelled(int r)
        {
            var SQL = 
                "Select DocNo From TblRecvCtDtl " +
                "Where CancelInd='Y' And DocNo=@DocNo And DNo=@DNo;";

            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 2));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Item's Code : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine + 
                    "Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 5), 0) + Environment.NewLine + 
                    "UoM : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine + Environment.NewLine +
                    "This item already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsCurrencyNotValid()
        {
            bool NotValid = false;
            string CurCode = TxtCurCode.Text;

            if (Grd1.Rows.Count > 1 && CurCode.Length != 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0 && !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, r, 7)))
                    {
                        NotValid = true;
                        break;
                    }
                }
            }
            if (NotValid) Sm.StdMsg(mMsgType.Warning, "One document only allowed 1 currency.");
            return NotValid;
        }

        private bool IsTaxInvoiceNoNotValid()
        {
            string TaxInvoiceNo = TxtTaxInvoiceDocument.Text;
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0 && !Sm.CompareStr(TaxInvoiceNo, Sm.GetGrdStr(Grd1, r, 11)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item's code : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                            "Item's name : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                            "Invoice Tax# : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine + Environment.NewLine +
                            "Invalid invoice tax#.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsTaxRateNotValid()
        {
            decimal TaxRate = decimal.Parse(TxtTaxRate.Text);
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0 && TaxRate!=Sm.GetGrdDec(Grd1, r, 13))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item's code : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                            "Item's name : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                            "Tax Rate : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 13), 0) + Environment.NewLine + Environment.NewLine +
                            "Invalid tax rate.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsTaxInvoiceDtNotValid()
        {
            bool NotValid = false;
            string TaxInvoiceDt = Sm.GetGrdStr(Grd1, 0, 12);

            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0 &&
                        !Sm.CompareStr(TaxInvoiceDt, Sm.GetGrdStr(Grd1, r, 12)))
                    {
                        NotValid = true;
                        break;
                    }
                }
            }

            if (NotValid) Sm.StdMsg(mMsgType.Warning, "Only allowed 1 tax invoice date in 1 document.");
            return NotValid;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0) Debit += Sm.GetGrdDec(Grd2, Row, 3);
                if (Sm.GetGrdStr(Grd2, Row, 4).Length > 0) Credit += Sm.GetGrdDec(Grd2, Row, 4);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }
        
        private MySqlCommand SaveSalesReturnInvoiceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesReturnInvoiceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, DeptCode, IncomingPaymentInd, CtCode, LocalDocNo, CurCode, RecvCtDocno, TaxInvDocument, TaxInvoiceDt, AmtBefTax, TaxRate, TaxAmt, TotalAmt, PaidInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, Null, 'N', @LueDeptCode, 'O', @CtCode, @LocalDocNo, @CurCode, @RecvCtDocno, @TaxInvDocument, @TaxInvoiceDt, @AmtBefTax, @TaxRate, @TaxAmt, @TotalAmt, @PaidAmt, @Remark, @CreateBy, CurrentDateTime());");

            SQL.AppendLine("Update TblRecvCtHdr Set SalesReturnInvoiceInd='F' ");
            SQL.AppendLine("Where DocNo=@RecvCtDocNo; ");

            var cm = new MySqlCommand(){CommandText = SQL.ToString()};
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LueDeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@RecvCtDocno", TxtRecvCtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvDocument", TxtTaxInvoiceDocument.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<Decimal>(ref cm, "@AmtBefTax", decimal.Parse(TxtAmtBefTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", decimal.Parse(TxtTaxRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<String>(ref cm, "@PaidInd", GetPayedInd());
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            
            return cm;
        }

        private MySqlCommand SaveSalesReturnInvoiceDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesReturnInvoiceDtl ");
            SQL.AppendLine("(DocNo, DNo, RecvCtDocNo, RecvCtDNo, ItCode, Qty, InventoryUomCode, CurCode, UPrice, PriceUomCode, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, TaxRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @RecvCtDocNo, @RecvCtDNo, @ItCode, @Qty, @InventoryUomCode, @CurCode, @UPrice, @PriceUomCode, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceDt, @TaxRate, @Amt, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@RecvCtDocNo", TxtRecvCtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@RecvCtDNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@InventoryUomCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@PriceUomCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetGrdDate(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSalesReturnInvoiceDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSalesReturnInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd2, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var CurCode = TxtCurCode.Text;
            var DOCtDocDt = string.Empty;

            if (!Sm.CompareStr(CurCode, mMainCurCode))
                DOCtDocDt = 
                    Sm.GetValue(
                        "Select DocDt From (" +
                        "Select E.DocDt "+
                        "From TblSalesReturnInvoiceHdr A " +
                        "Inner join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo " +
                        "Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo " +
                        "Inner join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='1' " +
                        "Inner join TblDOCtHdr E On D.DOCtDocNo=E.DocNo " +
                        "Where A.DocNo=@Param " +
                        "Union All " +
                        "Select E.DocDt " +
                        "From TblSalesReturnInvoiceHdr A " +
                        "Inner Join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo " +
                        "Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo " +
                        "Inner Join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='2' " +
                        "Inner Join TblDOCt2Hdr E On D.DOCtDocNo=E.DocNo " +
                        "Where A.DocNo=@Param " +
                        ") T Limit 1;", 
                        DocNo);
            
            SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Sales Return Invoice : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblSalesReturnInvoiceHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mSalesReturnInvoiceJournalFormat == "1")
            {
                SQL.AppendLine("        Select B.ParValue As AcNo, ");

                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        A.AmtBefTax As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForSalesReturnInvoice' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select B.ParValue As AcNo, ");
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        A.TaxAmt As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForVATOut' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        A.TotalAmt As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            if(mSalesReturnInvoiceJournalFormat == "2")
            {
                SQL.AppendLine("        Select CONCAT(B.ParValue, A.CtCode) As AcNo, ");

                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        A.AmtBefTax As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                //Tax
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        @TaxAmt1 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        INNER JOIN tbltax B ON B.TaxCode = @TaxCOde1 ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        @TaxAmt2 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        INNER JOIN tbltax B ON B.TaxCode = @TaxCOde2 ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        @TaxAmt3 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        INNER JOIN tbltax B ON B.TaxCode = @TaxCOde3 ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                SQL.AppendLine("        Union All ");
                if(mIsAcNoForSalesReturnUseItemCategory)
                    SQL.AppendLine("        Select D.AcNo6 AS AcNo, ");
                else
                    SQL.AppendLine("        Select E.ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (!Sm.CompareStr(CurCode, mMainCurCode))
                {
                    SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=@DOCtDocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) End* ");
                }
                SQL.AppendLine("        A.TotalAmt As CAmt ");
                SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
                SQL.AppendLine("        INNER JOIN tblsalesreturninvoicedtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        INNER JOIN tblitem C ON B.ItCode = C.ItCode ");
                SQL.AppendLine("        INNER JOIN tblitemcategory D ON C.ItCtCode = D.ItCtCode ");
                SQL.AppendLine("        Left Join TblParameter E On E.ParCode='CustomerAcNoAR' And E.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            #region Old Code

            ////Debit

            //SQL.AppendLine("        Select @AcNoForSalesReturnInvoice As AcNo, ");
            //SQL.AppendLine("        Case When E.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=E.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("        ), 0.00) End*F.UPrice*B.Qty As DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
            //SQL.AppendLine("        Inner join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo ");
            //SQL.AppendLine("        Inner join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='1' ");
            //SQL.AppendLine("        Inner join TblDOCtHdr E On D.DOCtDocNo=E.DocNo ");
            //SQL.AppendLine("        Inner join TblDOCtDtl F On D.DOCtDocNo=F.DocNo And D.DOCtDNo=F.DNo ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");

            //SQL.AppendLine("        Union All ");
            //SQL.AppendLine("        Select @AcNoForSalesReturnInvoice As AcNo, ");
            //SQL.AppendLine("        Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("        ), 0.00) End*G.UPrice*B.Qty As DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
            //SQL.AppendLine("        Inner join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo ");
            //SQL.AppendLine("        Inner join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='2' ");
            //SQL.AppendLine("        Inner join TblDOCt2Hdr E On D.DOCtDocNo=E.DocNo And E.DrDocNo Is Not Null ");
            //SQL.AppendLine("        Inner join TblDOCt2Dtl F On D.DOCtDocNo=F.DocNo And D.DOCtDNo=F.DNo ");
            //SQL.AppendLine("        Inner Join  ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, ");
            //SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0.00))) As UPrice ");
            //SQL.AppendLine("            From TblDrHdr A ");
            //SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            //SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            //SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
            //SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            //SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            //SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            //SQL.AppendLine("        ) G On E.DrDocno = G.DocNo And F.ItCode=G.ItCode ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");

            //SQL.AppendLine("        Union All ");
            //SQL.AppendLine("        Select @AcNoForSalesReturnInvoice As AcNo, ");
            //SQL.AppendLine("        Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("        ), 0.00) End*G.UPrice*B.Qty As DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
            //SQL.AppendLine("        Inner join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo ");
            //SQL.AppendLine("        Inner join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='2' ");
            //SQL.AppendLine("        Inner join TblDOCt2Hdr E On D.DOCtDocNo=E.DocNo And E.PLDocNo Is Not Null ");
            //SQL.AppendLine("        Inner join TblDOCt2Dtl F On D.DOCtDocNo=F.DocNo And D.DOCtDNo=F.DNo ");
            //SQL.AppendLine("        Inner Join  ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, ");
            //SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0.00))) As UPrice ");
            //SQL.AppendLine("            From TblPLHdr A ");
            //SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            //SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            //SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
            //SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            //SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            //SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            //SQL.AppendLine("        ) G On E.DrDocno = G.DocNo And F.ItCode=G.ItCode ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");

            ////Credit
            //SQL.AppendLine("        Union All ");
            //SQL.AppendLine("        Select Concat(@CustomerAcNoAR, A.CtCode) As AcNo, 0.00 As DAmt, ");
            //SQL.AppendLine("        Case When E.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=E.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("        ), 0.00) End*F.UPrice*B.Qty As CAmt ");
            //SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
            //SQL.AppendLine("        Inner join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo ");
            //SQL.AppendLine("        Inner join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='1' ");
            //SQL.AppendLine("        Inner join TblDOCtHdr E On D.DOCtDocNo=E.DocNo ");
            //SQL.AppendLine("        Inner join TblDOCtDtl F On D.DOCtDocNo=F.DocNo And D.DOCtDNo=F.DNo ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");

            //SQL.AppendLine("        Union All ");
            //SQL.AppendLine("        Select Concat(@CustomerAcNoAR, A.CtCode) As AcNo, 0.00 As DAmt, ");
            //SQL.AppendLine("        Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("        ), 0.00) End*G.UPrice*B.Qty As CAmt ");
            //SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
            //SQL.AppendLine("        Inner join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo ");
            //SQL.AppendLine("        Inner join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='2' ");
            //SQL.AppendLine("        Inner join TblDOCt2Hdr E On D.DOCtDocNo=E.DocNo And E.DrDocNo Is Not Null ");
            //SQL.AppendLine("        Inner join TblDOCt2Dtl F On D.DOCtDocNo=F.DocNo And D.DOCtDNo=F.DNo ");
            //SQL.AppendLine("        Inner Join  ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, ");
            //SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
            //SQL.AppendLine("            From TblDrHdr A ");
            //SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            //SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            //SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
            //SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            //SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            //SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            //SQL.AppendLine("        ) G On E.DrDocno = G.DocNo And F.ItCode=G.ItCode ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");

            //SQL.AppendLine("        Union All ");
            //SQL.AppendLine("        Select Concat(@CustomerAcNoAR, A.CtCode) As AcNo, 0.00 As DAmt, ");
            //SQL.AppendLine("        Case When G.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("        IfNull(( ");
            //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=G.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("        ), 0.00) End*G.UPrice*B.Qty As CAmt ");
            //SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
            //SQL.AppendLine("        Inner join TblSalesReturnInvoiceDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        Inner Join TblRecvCtHdr C On A.RecvCtDocNo=C.DocNo ");
            //SQL.AppendLine("        Inner join TblRecvCtDtl D On A.RecvCtDocNo=D.DocNo And B.RecvCtDNo=D.DNo And D.DOType='2' ");
            //SQL.AppendLine("        Inner join TblDOCt2Hdr E On D.DOCtDocNo=E.DocNo And E.PLDocNo Is Not Null ");
            //SQL.AppendLine("        Inner join TblDOCt2Dtl F On D.DOCtDocNo=F.DocNo And D.DOCtDNo=F.DNo ");
            //SQL.AppendLine("        Inner Join  ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, ");
            //SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0.00))) As UPrice ");
            //SQL.AppendLine("            From TblPLHdr A ");
            //SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            //SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            //SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
            //SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            //SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            //SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            //SQL.AppendLine("        ) G On E.DrDocno = G.DocNo And F.ItCode=G.ItCode ");
            //SQL.AppendLine("        Where A.DocNo=@DocNo ");

            #endregion

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@DOCtDocDt", DOCtDocDt);
            if (mSalesReturnInvoiceJournalFormat == "2")
            {
                Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
                Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
                Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
                Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", decimal.Parse(TxtTaxAmt1.Text));
                Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
                Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            }
            
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesReturnInvoiceHdr());
          
            if (mIsAutoJournalActived) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(Sm.ServerCurrentDate()) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToIncomingPayment() ||
                (ChkCancelInd.Checked && IsVoucherRequestPPNExisted()) ||
                (ChkCancelInd.Checked && IsVATSettlementExisted())
                ;
        }

        private bool IsVoucherRequestPPNExisted()
        {
            if (Sm.IsDataExist(
                "SELECT 1 FROM TblSalesReturnInvoiceHdr " +
                "WHERE DocNo=@Param AND VoucherRequestPPNDocNo IS NOT NULL;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher Request VAT.");
                return true;
            }
            return false;
        }

        private bool IsVATSettlementExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblSalesReturnInvoiceHdr " +
                "Where DocNo=@Param And VATSettlementDocNo Is Not Null;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to VAT settlement.");
                return true;
            }

            return false;
        }


        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist(
                "Select DocNo From TblSalesReturnInvoiceHdr Where CancelInd='Y' And DocNo=@Param;", 
                TxtDocNo.Text)
                )
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToIncomingPayment()
        {
            if (Sm.IsDataExist(
                "Select DocNo From TblSalesReturnInvoiceHdr Where IncomingPaymentInd<>'O' And DocNo=@Param;", 
                TxtDocNo.Text)
                )
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to incoming payment.");
                return true;
            }
            return false;
        }

        private bool IsJournalDataExisted()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblSalesReturnInvoiceHdr Where JournalDocNo Is Not Null And DocNo=@Param;", 
                TxtDocNo.Text);
        }

        private MySqlCommand EditSalesReturnInvoiceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblRecvCtHdr Set SalesReturnInvoiceInd='O' ");
            SQL.AppendLine("Where DocNo=@RecvCtDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@RecvCtDocNo", TxtRecvCtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesReturnInvoiceHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Replace(CurDate(), '-', ''), Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblSalesReturnInvoiceHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblSalesReturnInvoiceHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalesReturnInvoiceHdr(DocNo);
                ShowSalesReturnInvoiceDtl(DocNo);
                ShowSalesReturnInvoiceDtl2(DocNo);
                if (mIsSalesInvoiceTaxEnabled)
                    ShowSLITaxInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesReturnInvoiceHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.CtCode, B.CtName, ");
            SQL.AppendLine("A.LocalDocNo, A.RecvCtDocNo, ");
            SQL.AppendLine("A.TaxInvDocument, A.TaxInvoiceDt, A.TaxRate, A.CurCode, A.AmtBefTax, A.TaxAmt, A.TotalAmt, A.Remark, ");
            SQL.AppendLine("A.VoucherRequestPPNDocNo, A.JournalDocNo, A.JournalDocNo2, A.DeptCode ");
            SQL.AppendLine(", GROUP_CONCAT(DISTINCT(C.DOCtDocNo)) As DOCtDocNo, GROUP_CONCAT(DISTINCT(D.DocNo)) As SLIDocNo, GROUP_CONCAT(DISTINCT(G.VoucherDocNo)) As VCDocNo ");
            SQL.AppendLine("From TblSalesReturnInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left JOIN tblrecvctdtl C ON A.RecvCtDocNo = C.DocNo ");
            SQL.AppendLine("Left JOIN tblsalesinvoicedtl D ON C.DOCtDocNo = D.DOCtDocNo ");
            SQL.AppendLine("Left JOIN tblincomingpaymentdtl E ON D.DocNo = E.InvoiceDocNo ");
            SQL.AppendLine("Left JOIN tblincomingpaymenthdr F ON E.DocNo = F.Docno ");
            SQL.AppendLine("Left JOIN tblvoucherrequesthdr G ON F.VoucherRequestDocNo = G.DocNo ");
            SQL.AppendLine("Left JOIN tblsalesreturninvoicedtl H ON C.DocNo = H.RecvCtDocNo ");
            SQL.AppendLine("Left JOIN tblsalesinvoicehdr I ON D.DocNo = I.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "LocalDocNo", "RecvCtDocNo", 
                        
                        //6-10
                        "CtCode", "CtName", "TaxInvDocument", "TaxInvoiceDt", "CurCode", 
                        
                        //11-15
                        "AmtBefTax", "TaxRate", "TaxAmt", "TotalAmt", "Remark", 
                        
                        //16-20
                        "VoucherRequestPPNDocNo", "JournalDocNo", "JournalDocNo2", "DeptCode", "DOCtDocNo",
                        
                        //21-25
                        "SLIDocNo", "VCDocNo",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtRecvCtDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        mCtCode = Sm.DrStr(dr, c[6]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtTaxInvoiceDocument.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[9]));
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[10]);
                        TxtAmtBefTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        TxtTaxRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                        TxtVoucherRequestPPNDocNo.EditValue = Sm.DrStr(dr, c[16]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[17]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[18]);
                        LueDeptCode.EditValue = Sm.DrStr(dr, c[19]);
                        TxtDOCt.EditValue = Sm.DrStr(dr, c[20]);
                        TxtSalesInvoice.EditValue = Sm.DrStr(dr, c[21]);
                        TxtVoucher.EditValue = Sm.DrStr(dr, c[22]);
                    }, true
                );
        }

        private void ShowSalesReturnInvoiceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.RecvCtDocNo, A.RecvCtDNo, ");
            SQL.AppendLine("A.ItCode, C.ItName, A.Qty, A.InventoryUomCode, ");
            SQL.AppendLine("A.CurCode, A.UPrice, A.PriceUomCode, A.Amt, A.TaxInvoiceNo, A.TaxInvoiceDt, A.TaxRate, B.Remark ");
            SQL.AppendLine("From TblSalesReturnInvoiceDtl A  ");
            SQL.AppendLine("Inner Join TblRecvCtDtl B On A.RecvCtDocNo=B.DocNo And A.RecvCtDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "RecvCtDocNo", "RecvCtDNo", "ItCode", "ItName", "Qty", 
                        
                        //6-10
                        "InventoryUomCode", "CurCode", "UPrice", "PriceUomCode", "Amt", 
                        
                        //11-14
                        "TaxInvoiceNo", "TaxInvoiceDt", "TaxRate", "Remark"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 14);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 8, 10, 13 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesReturnInvoiceDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark ");
            SQL.AppendLine("From TblSalesReturnInvoiceDtl2 A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", "AcDesc", "DAmt", "CAmt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional method

        private string GetPayedInd()
        {
            string mPayedInd = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine(" SELECT ");
            SQL.AppendLine("    case ");
            SQL.AppendLine("        when A.Amt  = C.PayedAmt Then 'P' ");
            SQL.AppendLine("        ELSE 'O' END AS PayedInd ");
            SQL.AppendLine(" FROM tblsalesinvoicehdr A  ");
            SQL.AppendLine(" INNER JOIN tblsalesinvoicedtl B ON A.DocNo = B.DocNo  ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
            SQL.AppendLine("    AND A.Status = 'A' ");
            SQL.AppendLine(" LEFT JOIN ( ");
            SQL.AppendLine("    SELECT X2.InvoiceDocNo, SUM(X2.Amt) AS PayedAmt ");
            SQL.AppendLine("    FROM tblincomingpaymenthdr X1 ");
            SQL.AppendLine("    INNER JOIN tblincomingpaymentdtl X2 ON X1.DocNo = X2.DocNo ");
            SQL.AppendLine(" 		AND X1.CancelInd = 'N' ");
            SQL.AppendLine(" 		AND X1.Status = 'A' ");
            SQL.AppendLine(" 	INNER JOIN tblvoucherrequesthdr X3 ON X1.voucherrequestdocno = X3.docno ");
            SQL.AppendLine(" 		AND X3.VoucherDocNo IS NOT NULL  ");
            SQL.AppendLine(" 	GROUP BY X2.InvoiceDocNo ");
            SQL.AppendLine(" )C ON A.DocNo = C.InvoiceDocNo ");
            SQL.AppendLine(" WHERE FIND_IN_SET(B.DOCtDocNo, @DOCtDocNo) ");
            SQL.AppendLine(" GROUP BY A.DocNo; ");



            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DOCtDocNo", TxtDOCt.Text);
            return mPayedInd = Sm.GetValue(cm);
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        public void ComputeAmt()
        {
            decimal
                AmtBefTax = 0m,
                AmtAftTax = 0m,
                Tax = 0m;

            string Value = string.Empty;

            TxtAmtBefTax.EditValue = Sm.FormatNum(0m, 0);
            TxtTotalAmt.EditValue = Sm.FormatNum(0m, 0);
            TxtTaxAmt.EditValue = Sm.FormatNum(0m, 0);

            TxtTaxInvoiceDocument.EditValue = null;
            DteTaxInvoiceDt.EditValue = null;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        if (r == 0)
                        {
                            Value = Sm.GetGrdStr(Grd1, r, 7);
                            if (Value.Length > 0) TxtCurCode.EditValue = Value;

                            Value = Sm.GetGrdStr(Grd1, r, 11);
                            if (Value.Length > 0) TxtTaxInvoiceDocument.EditValue = Value;

                            Value = Sm.GetGrdStr(Grd1, r, 12);
                            if (Value.Length > 0)
                            {
                                Value = Sm.GetGrdDate(Grd1, r, 12).Substring(0, 8);
                                Sm.SetDte(DteTaxInvoiceDt, Value);
                            }

                            Value = Sm.GetGrdStr(Grd1, r, 13);
                            if (Value.Length > 0) TxtTaxRate.EditValue = Value;
                        }
                        if (Sm.GetGrdStr(Grd1, r, 10).Length > 0) AmtBefTax += Sm.GetGrdDec(Grd1, r, 10);
                    }
                }
            }

            var CustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR");

            if (mCtCode.Length > 0 && CustomerAcNoAR.Length > 0)
            {
                var AcNo = CustomerAcNoAR + mCtCode;
                var AcType = string.Empty;
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd2, Row, 1)))
                    {
                        AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", Sm.GetGrdStr(Grd2, Row, 1));
                        if (Sm.GetGrdDec(Grd2, Row, 3) != 0)
                        {
                            if (AcType == "D")
                                AmtBefTax += Sm.GetGrdDec(Grd2, Row, 3);
                            else
                                AmtBefTax -= Sm.GetGrdDec(Grd2, Row, 3);
                        }
                        if (Sm.GetGrdDec(Grd2, Row, 4) != 0)
                        {
                            if (AcType == "C")
                                AmtBefTax += Sm.GetGrdDec(Grd2, Row, 4);
                            else
                                AmtBefTax -= Sm.GetGrdDec(Grd2, Row, 4);
                        }
                    }
                }
            }

            TxtAmtBefTax.EditValue = Sm.FormatNum(AmtBefTax, 0);

            AmtAftTax = AmtBefTax;

            Tax = decimal.Parse(TxtTaxRate.Text) * AmtBefTax * 0.01m;
            TxtTaxAmt.EditValue = Sm.FormatNum(Tax, 0);
            AmtAftTax += Tax;

            TxtTotalAmt.EditValue = Sm.FormatNum(AmtAftTax, 0);
        }
        
        public void ComputeTaxAmtSLI()
        {
            decimal
                AmtBefTax = 0m,
                AmtAftTax = 0m,
                Tax = 0m;

            string Value = string.Empty;

            TxtAmtBefTax.EditValue = Sm.FormatNum(0m, 0);
            TxtTotalAmt.EditValue = Sm.FormatNum(0m, 0);
            TxtTaxAmt.EditValue = Sm.FormatNum(0m, 0);

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        if (r == 0)
                        {
                            Value = Sm.GetGrdStr(Grd1, r, 7);
                            if (Value.Length > 0) TxtCurCode.EditValue = Value;

                            Value = Sm.GetGrdStr(Grd1, r, 11);
                            if (Value.Length > 0) TxtTaxInvoiceDocument.EditValue = Value;

                            Value = Sm.GetGrdStr(Grd1, r, 12);
                            if (Value.Length > 0)
                            {
                                Value = Sm.GetGrdDate(Grd1, r, 12).Substring(0, 8);
                                Sm.SetDte(DteTaxInvoiceDt, Value);
                            }
                        }
                        if (Sm.GetGrdStr(Grd1, r, 10).Length > 0) AmtBefTax += Sm.GetGrdDec(Grd1, r, 10);
                    }
                }
            }

            var CustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR");

            if (mCtCode.Length > 0 && CustomerAcNoAR.Length > 0)
            {
                var AcNo = CustomerAcNoAR + mCtCode;
                var AcType = string.Empty;
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd2, Row, 1)))
                    {
                        AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", Sm.GetGrdStr(Grd2, Row, 1));
                        if (Sm.GetGrdDec(Grd2, Row, 3) != 0)
                        {
                            if (AcType == "D")
                                AmtBefTax += Sm.GetGrdDec(Grd2, Row, 3);
                            else
                                AmtBefTax -= Sm.GetGrdDec(Grd2, Row, 3);
                        }
                        if (Sm.GetGrdDec(Grd2, Row, 4) != 0)
                        {
                            if (AcType == "C")
                                AmtBefTax += Sm.GetGrdDec(Grd2, Row, 4);
                            else
                                AmtBefTax -= Sm.GetGrdDec(Grd2, Row, 4);
                        }
                    }
                }
            }

            TxtAmtBefTax.EditValue = Sm.FormatNum(AmtBefTax, 0);

            AmtAftTax = AmtBefTax;

            string
                TaxCode1 = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3 }, 0);

            if (TaxCode1.Length != 0) TxtTaxAmt1.Text = Sm.FormatNum(decimal.Truncate(GetTaxRate(TaxCode1) * 0.01m * AmtBefTax), 0);
            if (TaxCode2.Length != 0) TxtTaxAmt2.Text = Sm.FormatNum(decimal.Truncate(GetTaxRate(TaxCode2) * 0.01m * AmtBefTax), 0);
            if (TaxCode3.Length != 0) TxtTaxAmt3.Text = Sm.FormatNum(decimal.Truncate(GetTaxRate(TaxCode3) * 0.01m * AmtBefTax), 0);

            Tax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);
            TxtTaxAmt.EditValue = Sm.FormatNum(Tax, 0);
            TxtTotalAmt.EditValue = Sm.FormatNum(AmtAftTax + Tax, 0);
        }

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mCustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR");
            mSalesReturnInvoiceJournalFormat = Sm.GetParameter("SalesReturnInvoiceJournalFormat");
            mAcNoForSalesReturnInvoice = Sm.GetParameter("AcNoForSalesReturnInvoice");
            mIsSalesReturnInvoiceCanOnlyHave1Record = Sm.GetParameter("IsSalesReturnInvoiceCanOnlyHave1Record") == "Y";
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
            mIsSalesReturnInvoiceUseDepartment = Sm.GetParameterBoo("IsSalesReturnInvoiceUseDepartment");
            mIsJournalValidationSalesReturnInvoiceEnabled = Sm.GetParameterBoo("IsJournalValidationSalesReturnInvoiceEnabled");
            mSalesReturnInvoiceShowSource = Sm.GetParameterBoo("SalesReturnInvoiceShowSource");
            mIsSalesInvoiceTaxEnabled = Sm.GetParameterBoo("IsSalesInvoiceTaxEnabled");
            mIsAcNoForSalesReturnUseItemCategory = Sm.GetParameterBoo("IsAcNoForSalesReturnUseItemCategory");
        }

        public void ShowRecvCtInfo()
        {
            var SQL = new StringBuilder();
            try
            {
                //untuk DR TOP
                SQL.AppendLine("Select Distinct A.DocNo, A.DNo, D.ItCode, E.ItName, ");
                SQL.AppendLine("Case D.PriceUomCode ");
                SQL.AppendLine("    When E.InventoryUomCode Then A.Qty ");
                SQL.AppendLine("    When E.InventoryUomCode2 Then A.Qty2 ");
                SQL.AppendLine("    When E.InventoryUomCode3 Then A.Qty3 ");
                SQL.AppendLine("    Else 0 End As Qty, ");
                SQL.AppendLine("Case D.PriceUomCode ");
                SQL.AppendLine("    When E.InventoryUomCode Then E.InventoryUomCode ");
                SQL.AppendLine("    When E.InventoryUomCode2 Then E.InventoryUomCode2 ");
                SQL.AppendLine("    When E.InventoryUomCode3 Then E.InventoryUomCode3 ");
                SQL.AppendLine("    Else Null End As InventoryUomCode, ");
                SQL.AppendLine("D.CurCode, D.UPrice, D.PriceUomCode, ");
                SQL.AppendLine("H.TaxInvDocument, H.DocDt As TaxInvoiceDt, I.DOCtDocNo, I.SLIDocNo, I.VCDocNo, ");
                if(mIsSalesInvoiceTaxEnabled)
                    SQL.AppendLine("0.00 TaxRate, A.Remark ");
                else
                    SQL.AppendLine("G.TaxRate, A.Remark ");
                //SQL.AppendLine("Null As TaxInvDocument, Null As TaxInvoiceDt, 0 As TaxRate, A.Remark ");
                SQL.AppendLine("From TblRecvCtDtl A ");
                SQL.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo And B.DRDocNo Is Not Null ");
                SQL.AppendLine("Inner Join TblDOCt2Dtl C On A.DOCtDocNo=C.DocNo And A.DOCtDNo=C.DNo ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select Distinct T1.DocNo, T2.DNo, T7.ItCode, T6.CurCode, T6.PriceUomCode, ");
                SQL.AppendLine("    (T5.UPrice-(T5.UPrice*0.01*IfNull(T8.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("    From TblDRHdr T1 ");
                SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo  ");
                SQL.AppendLine("    Inner Join TblSODtl T4 On T3.DocNo=T4.DocNo ");
                SQL.AppendLine("    Inner Join TblCtQtDtl T5 On T3.CtQTDocno=T5.DocNo And T4.CtQTDno=T5.DNo ");
                SQL.AppendLine("    Inner Join TblItemPriceHdr T6 On T5.ItemPriceDocNo=T6.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPriceDtl T7 On T5.ItemPriceDocNo=T7.DocNo And T5.ItemPriceDNo=T7.DNo ");
                SQL.AppendLine("    Left Join TblSOQuotPromoItem T8 On T3.SOQuotPromoDocNo=T8.DocNo And T7.ItCode=T8.ItCode  ");
                SQL.AppendLine("    Where T1.CancelInd='N' ");
                SQL.AppendLine("    And T1.CBDInd='N' ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblRecvCtDtl X1 ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr X2 On X1.DOCtDocNo=X2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl X3 On X1.DOCtDocNo=X3.DocNo And X1.DOCtDNo=X3.DNo ");
                SQL.AppendLine("        Where X1.DocNo=@DocNo ");
                SQL.AppendLine("        And X2.DRDocNo=T1.DocNo ");
                SQL.AppendLine("        And X3.ItCode=T7.ItCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select Distinct T1.DocNo, T2.DNo, T5.ItCode, T3.CurCode, T5.SalesUomCode As PriceUomCode, ");
                SQL.AppendLine("    if(T3.TaxInd = 'Y', (T4.UPrice/1.1), T4.UPrice) UPrice ");
                SQL.AppendLine("    From TblDRHdr T1 ");
                SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo And T2.SCDocNo Is Not Null ");
                SQL.AppendLine("    Inner Join TblSalesMemoHdr T3 On T2.SODocNo = T3.DocNo ");
                SQL.AppendLine("    Inner Join TblSalesMemoDtl T4 On T3.DocNo = T4.DocNo And T2.SODNo = T4.DNo ");
                SQL.AppendLine("    Inner Join TblItem T5 ON T4.ItCode = T5.ItCode ");
                SQL.AppendLine("    Where T1.CancelInd = 'N' ");
                SQL.AppendLine("    And Exists( ");
	            SQL.AppendLine("        Select 1 ");
	            SQL.AppendLine("        From TblRecvCtDtl X1 ");
	            SQL.AppendLine("        Inner Join TblDOCt2Hdr X2 On X1.DOCtDocNo=X2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl X3 On X1.DOCtDocNo=X3.DocNo And X1.DOCtDNo=X3.DNo ");
	            SQL.AppendLine("        Where X1.DocNo=@DocNo ");
	            SQL.AppendLine("        And X2.DRDocNo=T1.DocNo ");
	            SQL.AppendLine("        And X3.ItCode=T5.ItCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") D On B.DrDocno=D.DocNo And C.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("Inner Join TblDOCt2Dtl2 F On B.DocNo=F.DocNo And F.Qty>0 And D.DNo=F.DRDNo ");
                SQL.AppendLine("Inner Join TblSalesInvoiceDtl G On C.DocNo=G.DOCtDocNo And F.DNo=G.DOCtDNo ");
                SQL.AppendLine("Inner Join TblSalesInvoiceHdr H On G.DocNo=H.DocNo And H.CancelInd='N' ");
                SQL.AppendLine("LEFT JOIN(");
                SQL.AppendLine(" SELECT J1.DocNo, GROUP_CONCAT(DISTINCT(J3.DocNo)) DOCtDocNo, GROUP_CONCAT(DISTINCT(J5.DocNo)) SLIDocNo,  ");
                SQL.AppendLine(" GROUP_CONCAT(DISTINCT(J9.VoucherDocNo)) VCDocNo ");
                SQL.AppendLine(" FROM tblrecvctdtl J1 ");
                SQL.AppendLine(" LEFT JOIN tbldoct2hdr J2 ON J1.DOCtDocNo = J2.DocNo ");
                SQL.AppendLine(" LEFT JOIN tbldoct2dtl J3 ON J1.DOCtDocNo = J3.DocNo AND J1.DOCtDNo = J3.DNo AND J3.CancelInd = 'N'-- AND J3.Qty > 0 ");
                SQL.AppendLine(" LEFT JOIN tblsalesinvoicedtl J5 ON J3.DocNo = J5.DOCtDocNo AND J3.DNo = J5.DOCtDNo ");
                SQL.AppendLine(" LEFT JOIN tblsalesinvoicehdr J6 ON J5.DocNo = J6.DocNo AND J6.CancelInd = 'N' ");
                SQL.AppendLine(" LEFT JOIN tblincomingpaymentdtl J7 ON J5.DocNo = J7.InvoiceDocNo ");
                SQL.AppendLine(" LEFT JOIN tblincomingpaymenthdr J8 ON J7.DocNo = J8.DocNo AND J8.CancelInd = 'N' ");
                SQL.AppendLine(" LEFT JOIN tblvoucherrequesthdr J9 ON J8.VoucherRequestDocNo = J9.DocNo AND J9.CancelInd = 'N' ");
                SQL.AppendLine(" WHERE J1.DocNo = @DocNo ");
                SQL.AppendLine(" ) I ON A.DocNo = I.DocNo ");
                SQL.AppendLine("Where A.CancelInd='N' ");
                SQL.AppendLine("And A.DocNo=@DocNo ");

                //untuk PL
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Distinct A.DocNo, A.DNo, D.ItCode, E.ItName, ");
                SQL.AppendLine("Case D.PriceUomCode ");
                SQL.AppendLine("    When E.InventoryUomCode Then A.Qty ");
                SQL.AppendLine("    When E.InventoryUomCode2 Then A.Qty2 ");
                SQL.AppendLine("    When E.InventoryUomCode3 Then A.Qty3 ");
                SQL.AppendLine("    Else 0 End As Qty, ");
                SQL.AppendLine("Case D.PriceUomCode ");
                SQL.AppendLine("    When E.InventoryUomCode Then E.InventoryUomCode ");
                SQL.AppendLine("    When E.InventoryUomCode2 Then E.InventoryUomCode2 ");
                SQL.AppendLine("    When E.InventoryUomCode3 Then E.InventoryUomCode3 ");
                SQL.AppendLine("    Else Null End As InventoryUomCode, ");
                SQL.AppendLine("D.CurCode, D.UPrice, D.PriceUomCode, ");
                SQL.AppendLine("H.TaxInvDocument, H.DocDt As TaxInvoiceDt, G.TaxRate, A.Remark, I.DOCtDocNo, I.SLIDocNo, I.VCDocNo ");
                //SQL.AppendLine("Null As TaxInvDocument, Null As TaxInvoiceDt, 0 As TaxRate, A.Remark ");
                SQL.AppendLine("From TblRecvCtDtl A ");
                SQL.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo And B.PLDocNo Is Not Null ");
                SQL.AppendLine("Inner Join TblDOCt2Dtl C On A.DOCtDocNo=C.DocNo And A.DOCtDNo=C.DNo ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select Distinct T1.DocNo, T2.DNo, T7.ItCode, T6.CurCode, T6.PriceUomCode, ");
                SQL.AppendLine("    (T5.UPrice-(T5.UPrice*0.01*IfNull(T8.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("    From TblPLHdr T1 ");
                SQL.AppendLine("    Inner Join TblPLDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo  ");
                SQL.AppendLine("    Inner Join TblSODtl T4 On T3.DocNo=T4.DocNo ");
                SQL.AppendLine("    Inner Join TblCtQtDtl T5 On T3.CtQTDocno=T5.DocNo And T4.CtQTDno=T5.DNo ");
                SQL.AppendLine("    Inner Join TblItemPriceHdr T6 On T5.ItemPriceDocNo=T6.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPriceDtl T7 On T5.ItemPriceDocNo=T7.DocNo And T5.ItemPriceDNo=T7.DNo ");
                SQL.AppendLine("    Left Join TblSOQuotPromoItem T8 On T3.SOQuotPromoDocNo=T8.DocNo And T7.ItCode=T8.ItCode  ");
                SQL.AppendLine("    Where Exists( ");
                SQL.AppendLine("        Select X1.DocNo ");
                SQL.AppendLine("        From TblRecvCtDtl X1 ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr X2 On X1.DOCtDocNo=X2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl X3 On X1.DOCtDocNo=X3.DocNo And X1.DOCtDNo=X3.DNo ");
                SQL.AppendLine("        Where X1.DocNo=@DocNo ");
                SQL.AppendLine("        And X2.PLDocNo=T1.DocNo ");
                SQL.AppendLine("        And X3.ItCode=T7.ItCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") D On B.PLDocno=D.DocNo And C.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("Inner Join TblDOCt2Dtl3 F On B.DocNo=F.DocNo And F.Qty>0 And D.DNo=F.PLDNo ");
                SQL.AppendLine("Left Join TblSalesInvoiceDtl G On C.DocNo=G.DOCtDocNo And F.DNo=G.DOCtDNo ");
                SQL.AppendLine("Left Join TblSalesInvoiceHdr H On G.DocNo=H.DocNo And H.CancelInd='N' ");
                SQL.AppendLine("LEFT JOIN(");
                SQL.AppendLine(" SELECT J1.DocNo, GROUP_CONCAT(DISTINCT(J3.DocNo)) DOCtDocNo, GROUP_CONCAT(DISTINCT(J5.DocNo)) SLIDocNo,  ");
                SQL.AppendLine(" GROUP_CONCAT(DISTINCT(J9.VoucherDocNo)) VCDocNo ");
                SQL.AppendLine(" FROM tblrecvctdtl J1 ");
                SQL.AppendLine(" LEFT JOIN tbldoct2hdr J2 ON J1.DOCtDocNo = J2.DocNo ");
                SQL.AppendLine(" LEFT JOIN tbldoct2dtl J3 ON J1.DOCtDocNo = J3.DocNo AND J1.DOCtDNo = J3.DNo AND J3.CancelInd = 'N'-- AND J3.Qty > 0 ");
                SQL.AppendLine(" LEFT JOIN tblsalesinvoicedtl J5 ON J3.DocNo = J5.DOCtDocNo AND J3.DNo = J5.DOCtDNo ");
                SQL.AppendLine(" LEFT JOIN tblsalesinvoicehdr J6 ON J5.DocNo = J6.DocNo AND J6.CancelInd = 'N' ");
                SQL.AppendLine(" LEFT JOIN tblincomingpaymentdtl J7 ON J5.DocNo = J7.InvoiceDocNo ");
                SQL.AppendLine(" LEFT JOIN tblincomingpaymenthdr J8 ON J7.DocNo = J8.DocNo AND J8.CancelInd = 'N' ");
                SQL.AppendLine(" LEFT JOIN tblvoucherrequesthdr J9 ON J8.VoucherRequestDocNo = J9.DocNo AND J9.CancelInd = 'N' ");
                SQL.AppendLine(" WHERE J1.DocNo = @DocNo ");
                SQL.AppendLine(" ) I ON A.DocNo = I.DocNo ");
                SQL.AppendLine("Where A.CancelInd='N' ");
                SQL.AppendLine("And A.DocNo=@DocNo ");

                //untuk DR CBD
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Distinct  ");
                SQL.AppendLine("A.DocNo, A.DNo, D.ItCode, E.ItName, ");
                SQL.AppendLine("Case D.PriceUomCode ");
                SQL.AppendLine("When E.InventoryUomCode Then A.Qty ");
                SQL.AppendLine("When E.InventoryUomCode2 Then A.Qty2 ");
                SQL.AppendLine("When E.InventoryUomCode3 Then A.Qty3 ");
                SQL.AppendLine("Else 0 End As Qty, ");
                SQL.AppendLine("Case D.PriceUomCode ");
                SQL.AppendLine("When E.InventoryUomCode Then E.InventoryUomCode ");
                SQL.AppendLine("When E.InventoryUomCode2 Then E.InventoryUomCode2 ");
                SQL.AppendLine("When E.InventoryUomCode3 Then E.InventoryUomCode3 ");
                SQL.AppendLine("Else Null End As InventoryUomCode, ");
                SQL.AppendLine("D.CurCode, D.UPrice, D.PriceUomCode, ");
                SQL.AppendLine("J.TaxInvDocument, J.DocDt As TaxInvoiceDt, I.TaxRate, A.Remark, K.DOCtDocNo, K.SLIDocNo, K.VCDocNo ");
                SQL.AppendLine("From TblRecvCtDtl A  ");
                SQL.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo And B.DRDocNo Is Not Null ");
                SQL.AppendLine("Inner Join TblDOCt2Dtl C On A.DOCtDocNo=C.DocNo And A.DOCtDNo=C.DNo ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select Distinct T1.DocNo, T2.DNo, T7.ItCode, T6.CurCode, T6.PriceUomCode, ");
                SQL.AppendLine("    (T5.UPrice-(T5.UPrice*0.01*IfNull(T8.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("    From TblDRHdr T1 ");
                SQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo ");
                SQL.AppendLine("    Inner Join TblSODtl T4 On T3.DocNo=T4.DocNo ");
                SQL.AppendLine("    Inner Join TblCtQtDtl T5 On T3.CtQTDocno=T5.DocNo And T4.CtQTDno=T5.DNo ");
                SQL.AppendLine("    Inner Join TblItemPriceHdr T6 On T5.ItemPriceDocNo=T6.DocNo ");
                SQL.AppendLine("    Inner Join TblItemPriceDtl T7 On T5.ItemPriceDocNo=T7.DocNo And T5.ItemPriceDNo=T7.DNo ");
                SQL.AppendLine("    Left Join TblSOQuotPromoItem T8 On T3.SOQuotPromoDocNo=T8.DocNo And T7.ItCode=T8.ItCode ");
                SQL.AppendLine("    Where T1.CancelInd='N' ");
                SQL.AppendLine("    And T1.CBDInd='Y' ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select X1.DocNo ");
                SQL.AppendLine("        From TblRecvCtDtl X1 ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr X2 On X1.DOCtDocNo=X2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl X3 On X1.DOCtDocNo=X3.DocNo And X1.DOCtDNo=X3.DNo ");
                SQL.AppendLine("        Where X1.DocNo=@DocNo ");
                SQL.AppendLine("        And X2.DRDocNo=T1.DocNo ");
                SQL.AppendLine("        And X3.ItCode=T7.ItCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") D On B.DrDocno=D.DocNo And C.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("Inner Join TblDOCt2Dtl2 F On B.DocNo=F.DocNo And F.Qty>0 And D.DNo=F.DRDNo ");
                SQL.AppendLine("Inner Join TblDRDtl G On B.DRDocNo = G.DocNo ");
                SQL.AppendLine("Inner JOin TBlSODtl H On G.SODocNo = H.DocNo And G.SODNo = H.DNo  ");
                SQL.AppendLine("Left Join TblSalesInvoiceDtl I On H.DocNo=I.DOCtDocNo And H.DNo=I.DOCtDNo ");
                SQL.AppendLine("Left Join TblSalesInvoiceHdr J On I.DocNo=J.DocNo And J.CancelInd='N' ");
                SQL.AppendLine("LEFT JOIN(");
                SQL.AppendLine(" SELECT J1.DocNo, GROUP_CONCAT(DISTINCT(J3.DocNo)) DOCtDocNo, GROUP_CONCAT(DISTINCT(J5.DocNo)) SLIDocNo,  ");
                SQL.AppendLine(" GROUP_CONCAT(DISTINCT(J9.VoucherDocNo)) VCDocNo ");
                SQL.AppendLine(" FROM tblrecvctdtl J1 ");
                SQL.AppendLine(" LEFT JOIN tbldoct2hdr J2 ON J1.DOCtDocNo = J2.DocNo ");
                SQL.AppendLine(" LEFT JOIN tbldoct2dtl J3 ON J1.DOCtDocNo = J3.DocNo AND J1.DOCtDNo = J3.DNo AND J3.CancelInd = 'N'-- AND J3.Qty > 0 ");
                SQL.AppendLine(" LEFT JOIN tblsalesinvoicedtl J5 ON J3.DocNo = J5.DOCtDocNo AND J3.DNo = J5.DOCtDNo ");
                SQL.AppendLine(" LEFT JOIN tblsalesinvoicehdr J6 ON J5.DocNo = J6.DocNo AND J6.CancelInd = 'N' ");
                SQL.AppendLine(" LEFT JOIN tblincomingpaymentdtl J7 ON J5.DocNo = J7.InvoiceDocNo ");
                SQL.AppendLine(" LEFT JOIN tblincomingpaymenthdr J8 ON J7.DocNo = J8.DocNo AND J8.CancelInd = 'N' ");
                SQL.AppendLine(" LEFT JOIN tblvoucherrequesthdr J9 ON J8.VoucherRequestDocNo = J9.DocNo AND J9.CancelInd = 'N' ");
                SQL.AppendLine(" WHERE J1.DocNo = @DocNo ");
                SQL.AppendLine(" ) K ON A.DocNo = K.DocNo ");
                SQL.AppendLine("Where A.CancelInd='N' ");
                SQL.AppendLine("And A.DocNo=@DocNo ");

                // DO biasa
                // dapat menarik item dari do to customer pada menu SALES RETURN INVOICE. Tolong ditambahkan di ALL CLIENT.
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Distinct A.DocNo, A.DNo, D.ItCode, D.ItName, ");
                SQL.AppendLine("A.Qty, D.InventoryUOMCode, ");
                SQL.AppendLine("B.CurCode, C.UPrice, C.PackagingUnitUomCode PriceUomCode, ");
                SQL.AppendLine("F.TaxInvDocument, F.DocDt As TaxInvoiceDt, E.TaxRate, A.Remark, G.DOCtDocNo, G.SLIDocNo, G.VCDocNo ");
                SQL.AppendLine("From TblRecvCtDtl A ");
                SQL.AppendLine("Inner Join TblDOCtHdr B On A.DOCtDocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblDOCtDtl C On A.DOCtDocNo = C.DocNo And A.DOCtDNo = C.DNo And C.Qty > 0 ");
                SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
                SQL.AppendLine("Left Join TblSalesInvoiceDtl E ON E.DOCtDocNo = C.DOcNo And E.DOCtDNo = C.DNo ");
                SQL.AppendLine("Left Join TblSalesInvoiceHdr F On E.DocNo = F.DocNo ");
                SQL.AppendLine("LEFT JOIN(");
                SQL.AppendLine(" SELECT J1.DocNo, GROUP_CONCAT(DISTINCT(J3.DocNo)) DOCtDocNo, GROUP_CONCAT(DISTINCT(J5.DocNo)) SLIDocNo,  ");
                SQL.AppendLine(" GROUP_CONCAT(DISTINCT(J9.VoucherDocNo)) VCDocNo ");
                SQL.AppendLine(" FROM tblrecvctdtl J1 ");
                SQL.AppendLine(" LEFT JOIN tbldocthdr J2 ON J1.DOCtDocNo = J2.DocNo ");
                SQL.AppendLine(" LEFT JOIN tbldoctdtl J3 ON J1.DOCtDocNo = J3.DocNo AND J1.DOCtDNo = J3.DNo AND J3.CancelInd = 'N'-- AND J3.Qty > 0 ");
                SQL.AppendLine(" LEFT JOIN tblsalesinvoicedtl J5 ON J3.DocNo = J5.DOCtDocNo AND J3.DNo = J5.DOCtDNo ");
                SQL.AppendLine(" LEFT JOIN tblsalesinvoicehdr J6 ON J5.DocNo = J6.DocNo AND J6.CancelInd = 'N' ");
                SQL.AppendLine(" LEFT JOIN tblincomingpaymentdtl J7 ON J5.DocNo = J7.InvoiceDocNo ");
                SQL.AppendLine(" LEFT JOIN tblincomingpaymenthdr J8 ON J7.DocNo = J8.DocNo AND J8.CancelInd = 'N' ");
                SQL.AppendLine(" LEFT JOIN tblvoucherrequesthdr J9 ON J8.VoucherRequestDocNo = J9.DocNo AND J9.CancelInd = 'N' ");
                SQL.AppendLine(" WHERE J1.DocNo = @DocNo ");
                SQL.AppendLine(" ) G ON A.DocNo = G.DocNo ");
                SQL.AppendLine("Where A.CancelInd = 'N' ");
                SQL.AppendLine("And A.DocNo = @DocNo; ");

                #region Old Code
                //SQL.AppendLine("Select B.DNo, B.DOCtDocNo, B.DOCtDNo, ");
                //SQL.AppendLine("D.ItCode, F.ItName, ");
                //SQL.AppendLine("D.BatchNo, D.Source, D.Lot, D.Bin, ");
                //SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
                //SQL.AppendLine("F.InventoryUomCode, ");
                //SQL.AppendLine("F.InventoryUomCode2, ");
                //SQL.AppendLine("F.InventoryUomCode3, ");
                //SQL.AppendLine("E.CurCode, ");
                //SQL.AppendLine("B.Uprice, ");
                //SQL.AppendLine("(Case When F.InventoryUomCode=E.PriceUomCode Then B.Qty Else ");
                //SQL.AppendLine("    Case When F.InventoryUomCode2=E.PriceUomCode Then B.Qty2 Else B.Qty3 End ");
                //SQL.AppendLine("End*B.UPrice) As Amt, ");
                //SQL.AppendLine("B.DOType, B.Remark ");
                //SQL.AppendLine("    From TblRecvCtHdr A  ");
                //SQL.AppendLine("    Inner Join TblRecvCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                //SQL.AppendLine("    Inner Join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo ");
                //SQL.AppendLine("    Inner Join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDno=D.Dno ");
                //SQL.AppendLine("    Inner Join  ");
                //SQL.AppendLine("        ( ");
                //SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, F.CurCode, F.PriceUomCode ");
                //SQL.AppendLine("            From TblDrhdr A ");
                //SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                //SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                //SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                //SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
                //SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                //SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                //SQL.AppendLine("        ) E On C.DrDocno = E.DocNo And D.ItCode=E.ItCode ");
                //SQL.AppendLine("    Inner Join TblItem F On D.ItCode=F.ItCode ");
                //SQL.AppendLine("    Where A.DocNo=@DocNo ");
                //SQL.AppendLine("    Union All ");
                //SQL.AppendLine("Select B.DNo, B.DOCtDocNo, B.DOCtDNo, ");
                //SQL.AppendLine("D.ItCode, F.ItName, ");
                //SQL.AppendLine("D.BatchNo, D.Source, D.Lot, D.Bin, ");
                //SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
                //SQL.AppendLine("F.InventoryUomCode, ");
                //SQL.AppendLine("F.InventoryUomCode2, ");
                //SQL.AppendLine("F.InventoryUomCode3, ");
                //SQL.AppendLine("E.CurCode, ");
                //SQL.AppendLine("B.Uprice, ");
                //SQL.AppendLine("(Case When F.InventoryUomCode=E.PriceUomCode Then B.Qty Else ");
                //SQL.AppendLine("    Case When F.InventoryUomCode2=E.PriceUomCode Then B.Qty2 Else B.Qty3 End ");
                //SQL.AppendLine("End*B.UPrice) As Amt, ");
                //SQL.AppendLine("B.DOType, B.Remark ");
                //SQL.AppendLine("    From TblRecvCtHdr A  ");
                //SQL.AppendLine("    Inner Join TblRecvCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                //SQL.AppendLine("    Inner Join TblDOCt2Hdr C On B.DOCtDocNo=C.DocNo ");
                //SQL.AppendLine("    Inner Join TblDOCt2Dtl D On B.DOCtDocNo=D.DocNo And B.DOCtDno=D.Dno ");
                //SQL.AppendLine("    Inner Join  ");
                //SQL.AppendLine("        ( ");
                //SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, F.CurCode, F.PriceUomCode ");
                //SQL.AppendLine("            From TblPLHdr A ");
                //SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
                //SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                //SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                //SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
                //SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                //SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                //SQL.AppendLine("        ) E On C.PLDocNo = E.DocNo And D.ItCode=E.ItCode ");
                //SQL.AppendLine("    Inner Join TblItem F On D.ItCode=F.ItCode ");
                //SQL.AppendLine("    Where A.DocNo=@DocNo ");
                #endregion

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtRecvCtDocNo.Text);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DNo", "ItCode", "ItName", "Qty", "InventoryUomCode", 
                        
                        //6-10
                        "CurCode", "UPrice", "PriceUomCode", "TaxInvDocument", "TaxInvoiceDt", 
                        
                        //11-12
                        "TaxRate", "Remark", "DOCtDocNo", "SLIDocNo", "VCDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = null;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Grd.Cells[Row, 10].Value = Sm.GetGrdDec(Grd, Row, 5) * Sm.GetGrdDec(Grd, Row, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        TxtDOCt.EditValue = Sm.DrStr(dr, c[13]);
                        TxtSalesInvoice.EditValue = Sm.DrStr(dr, c[14]);
                        TxtVoucher.EditValue = Sm.DrStr(dr, c[15]);
                    }, false, false, false, false
                );
                Grd1.Rows.Add();
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 8, 10, 13 });
                Sm.FocusGrd(Grd1, 0, 1);
                if (!mIsSalesInvoiceTaxEnabled)
                    ComputeAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public void ShowSLITaxInfo()
        {
            var SQL = new StringBuilder();
            try
            {
                SQL.AppendLine("SELECT E.TaxInvoiceNo, E.TaxInvoiceNo2, E.TaxInvoiceNo3, E.TaxCode1, E.TaxCode2, E.TaxCode3, E.TaxAlias1, E.TaxAlias2, E.TaxAlias3,");
                SQL.AppendLine("E.TaxInvoiceDt, E.TaxInvoiceDt2, E.TaxInvoiceDt3 ");
                SQL.AppendLine("FROM tblrecvcthdr A ");
                SQL.AppendLine("INNER JOIN tblrecvctdtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("LEFT JOIN tbldoct2dtl C ON B.DOCtDocNo = C.DocNo AND B.DOCtDNo = C.DNo ");
                SQL.AppendLine("LEFT JOIN tblsalesinvoicedtl D ON C.DocNo = D.DOCtDocNo AND C.DNo = D.DOCtDNo ");
                SQL.AppendLine("LEFT JOIN tblsalesinvoicehdr E ON D.DocNo = E.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtRecvCtDocNo.Text);

                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "TaxInvoiceNo", 

                        //1-5
                        "TaxInvoiceNo2", "TaxInvoiceNo3", "TaxCode1", "TaxCode2", "TaxCode3", 
                        
                        //6-10
                        "TaxAlias1", "TaxAlias2", "TaxAlias3", "TaxInvoiceDt", "TaxInvoiceDt2", 
                        
                        //11
                        "TaxInvoiceDt3",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[1]);
                        TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[5]));
                        TxtAlias1.EditValue = Sm.DrStr(dr, c[6]);
                        TxtAlias2.EditValue = Sm.DrStr(dr, c[7]);
                        TxtAlias3.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetDte(DteTaxInvoiceDt1, Sm.DrStr(dr, c[9]));
                        Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[10]));
                        Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[11]));
                    }, true
                );
                if (mIsSalesInvoiceTaxEnabled)
                    ComputeTaxAmtSLI();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private decimal GetTaxRate(string TaxCode)
        {
            return Sm.GetValueDec("Select TaxRate from TblTax Where TaxCode=@Param;", TaxCode);
        }

        #endregion

        #endregion 

        #region Event

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesReturnInvoiceDlg2(this));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSalesReturnInvoiceDlg2(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                Grd2.Cells[e.RowIndex, 4].Value = 0;
                ComputeAmt();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                Grd2.Cells[e.RowIndex, 3].Value = 0;
                ComputeAmt();
            }
        }

        #endregion

        #region Button Event

        private void BtnDOCt_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesReturnInvoiceDlg3(this));
        }
        private void BtnSalesInvoice_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesReturnInvoiceDlg4(this));
        }
        private void BtnVoucher_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesReturnInvoiceDlg5(this));
        }

        private void BtnRecvDocNo_Click(object sender, EventArgs e)
        {
            //TxtRecvCtDocNo.Text = string.Empty;
            //Sm.ClearGrd(Grd1, true);
            Sm.FormShowDialog(new FrmSalesReturnInvoiceDlg(this));
        }

        private void BtnRecvCtDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRecvCtDocNo, "Received#", false))
            {
                var f1 = new FrmRecvCt(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtRecvCtDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            }
        }

        #endregion




        #endregion
    }
}
