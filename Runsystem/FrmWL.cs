﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmWL : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        string mDocNo = string.Empty; 
        internal FrmWLFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmWL(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Leave Group";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                MeeNotify.Visible = false;
               
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                SetLueCtWLType(ref LueCtWL);
                SetLueWLType(ref LueWL);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count =1;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Violation of the Company Rules and Regulations", 
                    },
                     new int[] 
                    {
                        //0
                        300
                    }
                );
            #endregion

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo,DteDocDt, ChkCancelInd, TxtEmpCode, TxtEmpName, TxtWLS, LueCtWL, LueWL,
                        DteStartDt, DteEndDt
                    }, true);
                    TxtDocNo.Focus();
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtWL, LueWL,
                        DteStartDt,DteEndDt
                    }, false);
                    DteDocDt.Focus();
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    Grd1.ReadOnly = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, ChkCancelInd, TxtEmpCode, TxtEmpName, TxtWLS, LueCtWL, LueWL,
                DteStartDt, DteEndDt
            });
            
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion
    
        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWLFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            for (int Row = 0; Row <= 3; Row++)
            {
                Grd1.Rows.Add();
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

   
        #endregion

        #region Grid Method
        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd1, MeeNotify, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd1_KeyDown_1(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpWL", "TblEmpWL");
            
            var cml = new List<MySqlCommand>();
            
            cml.Add(SaveEmpWL(DocNo));
     
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee", false) ||
                Sm.IsLueEmpty(LueCtWL, "Category Warning Letter type") ||
                Sm.IsLueEmpty(LueWL, "warning Letter type") ||
                Sm.IsDteEmpty(DteStartDt, "Warning Letter started date") ||
                IsDataWLAlready() ||
                IsGrdEmpty()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Sm.GetGrdStr(Grd1, 0, 0).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 reason.");
                return true;
            }
            return false;
        }


        private bool IsDataWLAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.EmpCode "+
                    "From TblEmpWL A, TblWarningLetter B "+
                    "Where A.WLCode=B.WLCode  "+
                    "And A.EmpCode=@EmpCode And A.CancelInd = 'N' " +
                    "And B.WLType=(Select WlType From tblWarningletter Where WlCode = @WLCode) " +
                    "And STR_TO_DATE(@DocDt, '%Y%m%d')>=STR_TO_DATE(A.StartDt, '%Y%m%d') "+
                    "And STR_TO_DATE(@DocDt, '%Y%m%d')<DATE_ADD(STR_TO_DATE(A.StartDt, '%Y%m%d'), INTERVAL B.NoOfMth MONTH) "+
                    "Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@WLCode", Sm.GetLue(LueWL));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteStartDt));


            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Please choose another warning letter type.");
                return true;
            }
            return false;
        }
       
        private MySqlCommand SaveEmpWL(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpWl ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, EmpCode, WlCtCode, WlCode, ");
            SQL.AppendLine("Reason1, Reason2, Reason3, Reason4, Reason5, StartDt, EndDt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @EmpCode, @WlCtCode, @WlCode, ");
            SQL.AppendLine("@Reason1, @Reason2, @Reason3, @Reason4, @Reason5, @StartDt, @EndDt, "); 
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@WlCtCode", Sm.GetLue(LueCtWL));
            Sm.CmParam<String>(ref cm, "@WLCode", Sm.GetLue(LueWL));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            //if (Sm.GetGrdStr(Grd1, 0, 0).Length != 0)
            //{
                Sm.CmParam<String>(ref cm, "@Reason1", Sm.GetGrdStr(Grd1, 0, 0));
            //}
            //if (Sm.GetGrdStr(Grd1, 1, 0).Length != 0)
            //{
                Sm.CmParam<String>(ref cm, "@Reason2", Sm.GetGrdStr(Grd1, 1, 0));
            //}
            //if (Sm.GetGrdStr(Grd1, 2, 0).Length != 0)
            //{
                Sm.CmParam<String>(ref cm, "@Reason3", Sm.GetGrdStr(Grd1, 2, 0));
            //}
            //if (Sm.GetGrdStr(Grd1, 3, 0).Length != 0)
            //{
                Sm.CmParam<String>(ref cm, "@Reason4", Sm.GetGrdStr(Grd1, 3, 0));
           // }
            //if (Sm.GetGrdStr(Grd1, 4, 0).Length != 0)
            //{
                Sm.CmParam<String>(ref cm, "@Reason5", Sm.GetGrdStr(Grd1, 4, 0));
            //}
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

      
        #endregion

        #region Edit data

        private void EditData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelEmpWL());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready()||
                IsDocumentNotCancelled();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblEmpWL " +
                    "Where CancelInd='Y'  And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelEmpWL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpWL Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpWL(DocNo);
                ShowEmpWLDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpWL(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.EmpCode, B.EmpName, A.WlCtCode, A.WlCode, ");
            SQL.AppendLine("A.Reason1, A.Reason2, A.Reason3, A.Reason4, A.Reason5, A.StartDt, A.EndDt ");
            SQL.AppendLine("From TblEmpWL A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelInd", "EmpCode", "EmpName", "WLCtCode",  
                        
                        //6-10
                        "WLCode", "StartDt", "EndDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetLue(LueCtWL, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueWL, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[7]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[8]));
                    }, true
                );
        }

        private void ShowEmpWLDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Reason From ( ");
            SQL.AppendLine("Select Reason1 As Reason ");
            SQL.AppendLine("From TblEmpWL ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select Reason2 As Reason ");
            SQL.AppendLine("From TblEmpWL ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select Reason3 As Reason ");
            SQL.AppendLine("From TblEmpWL ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select Reason4 As Reason ");
            SQL.AppendLine("From TblEmpWL ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Union ALL ");
            SQL.AppendLine("Select Reason5 As Reason ");
            SQL.AppendLine("From TblEmpWL ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine(" ) T ");


            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] { "Reason" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);

           
        }

        public void ShowEmployeeSp(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WlCode ");
            SQL.AppendLine("From TblEmpWL A, TblWarningLetter B ");
            SQL.AppendLine("Where A.WLCode=B.WLCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And B.WLType=(Select WlType From tblWarningletter Where WlCode = @WLCode) ");
            SQL.AppendLine("And STR_TO_DATE(@DocDt, '%Y%m%d')>=STR_TO_DATE(A.DocDt, '%Y%m%d') ");
            SQL.AppendLine("And STR_TO_DATE(@DocDt, '%Y%m%d')<DATE_ADD(STR_TO_DATE(A.DocDt, '%Y%m%d'), INTERVAL B.NoOfMth MONTH) ");
            SQL.AppendLine("Limit 1; ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
          

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "WLCode",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    TxtWLS.EditValue = Sm.GetGrdStr(Grd, Row, 0);
                }, false, false, true, false
            );
           // Sm.FocusGrd(Grd1, 0, 1);
        }


        #endregion

        #region Additional Setting

      
        private void SetLueCtWLType(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='WLtype'; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueWLType(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select WlCode As Col1, WlName As Col2 ");
                SQL.AppendLine("From TblWarningLetter ; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        private void MeeRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.MemoExEdit Mee,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Mee.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Mee.EditValue = null;
            else
                Mee.EditValue = Sm.GetGrdStr(Grd, fCell.RowIndex, 0);

            Mee.Visible = true;
            Mee.Focus();

            fAccept = true;
        }
        #endregion
      
        #endregion

        #region Event

        #region Misc Control Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmWLDlg(this));
        }

        private void MeeNotify_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void MeeNotify_Leave(object sender, EventArgs e)
        {
            if (MeeNotify.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeNotify.Text.Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = MeeNotify.Text;
                }
                MeeNotify.Visible = false;
            }
        }

        private void LueCtWL_EditValueChanged(object sender, EventArgs e)
        {
            string Sp = Sm.GetValue(
                    "Select A.WLCode "+
                    "From TblEmpWL A, TblWarningLetter B "+
                    "Where A.WLCode=B.WLCode  "+
                    "And A.EmpCode='"+TxtEmpCode.Text+"' And A.CancelInd = 'N' " +
                    "And A.WLCtCode='"+Sm.GetLue(LueCtWL)+"' " +
                    "And STR_TO_DATE('"+Sm.GetDte(DteStartDt)+"', '%Y%m%d')>=STR_TO_DATE(A.StartDt, '%Y%m%d') "+
                    "And STR_TO_DATE('"+Sm.GetDte(DteStartDt)+"', '%Y%m%d')<DATE_ADD(STR_TO_DATE(A.StartDt, '%Y%m%d'), INTERVAL B.NoOfMth MONTH) "+
                    "Limit 1;");

            if (Sp.Length != 0)
            {
                TxtWLS.EditValue = Sp;
            }
            else
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtWLS
                });
            }
        }

        private void DteStartDt_EditValueChanged_1(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteStartDt).Length != 0)
            {
                DteEndDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteStartDt)).AddDays(180);
            }
        }

        #endregion

        #endregion
}
}
