﻿#region Update
/*
    06/04/2017 [WED] tambah query Order By Qty Desc
    06/04/2017 [WED] ItName -> ItCode di tampilan chart nya
    06/04/2017 [WED] Function Print langsung dari PrintPreviewDialog (karena via print dialog masih nggak mau nangkep perubahan di print orientation, grayscale, dll a.k.a tetep printnya hasilnya potrait colored)
    07/04/2017 [WED] menampilkan point di tiap series Chart
    19/05/2017 [WED] ubah source ke DR
    19/05/2017 [WED] layout : bar menjadi 25 buah, font X-Axis 8 -> 6.5
    22/08/2017 [WED] design bar dibuat horizontal
    11/09/2017 [WED] tambah source dari POS
    09/01/2018 [WED] ubah nama menu
    10/01/2018 [WED] tambah filter seperti di pie omset
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using MySql.Data.MySqlClient;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using Syncfusion.Windows;
using Syncfusion.Windows.Forms.Chart;
using SyXL = Syncfusion.XlsIO;
using SyDoc = Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmChrTopSeller : RunSystem.FrmBase10
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            exportFileName = string.Empty,
            file = string.Empty,
            mChartTitle = string.Empty;
        private bool mIsItGrpCodeShow = false, mIsRptDOCtShowPriceInfo = false;
        private bool mIsShowForeignName = false;

        #endregion

        #region Constructor

        public FrmChrTopSeller(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnExcel.Visible = BtnPDF.Visible = BtnWord.Visible = false;
            string CurrentDate = Sm.ServerCurrentDateTime();
            DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-30);
            DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
            SetLueCategoryCode(ref LueCategoryCode);
            GetParameter();
            mChartTitle = Sm.GetValue("Select IfNull(MenuDesc, '') as MenuDesc From TblMenu Where Param = 'FrmChrTopSeller' Limit 1;");
            this.Chart.Title.Text = mChartTitle;

            base.FrmLoad(sender, e);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    IsFilterByDateInvalid() ||
                    Sm.IsLueEmpty(LueCategoryCode, "Category")
                ) return;

                ChartAppearance.ApplyChartStyles(this.Chart);
                LoadData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Info, Exc.Message);
            }
        }

        private string SubQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ItCode, D.ItName, B.Qty, D.ItScCode, E.ItScName, D.ItGrpCode, F.ItGrpName ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblCustomer C on A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblItem D on B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblItemSubCategory E On D.ItScCode = E.ItScCode ");
            SQL.AppendLine("Left Join TblItemGroup F On D.ItGrpCode = F.ItGrpCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select X1.ItCode, X2.ItName, X1.Qty, X2.ItScCode, X3.ItScName, X2.ItGrpCode, X4.ItGrpName ");
            SQL.AppendLine("From TblPosTrnDtl X1 ");
            SQL.AppendLine("Inner Join TblItem X2 On X1.ItCode = X2.ItCode ");
            SQL.AppendLine("Left Join TblItemSubCategory X3 On X2.ItScCode = X3.ItScCode ");
            SQL.AppendLine("Left Join TblItemGroup X4 On X2.ItGrpCode = X4.ItGrpCode ");
            SQL.AppendLine("Where (X1.BsDate Between @DocDt1 And @DocDt2) ");

            return SQL.ToString();
        }

        private void LoadData()
        {
            var l = new List<TopSellingProduct>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string CategoryCode = Sm.GetLue(LueCategoryCode);

            #region Old Code
            //SQL.AppendLine("Select A.DocDt, A.DocNo, C.CtName, B.ItCode, D.ItName, D.ForeignName, B.BatchNo, ");
            //SQL.AppendLine("B.Qty, D.InventoryUOMCode, ");
            //if (mIsItGrpCodeShow)
            //    SQL.AppendLine("E.ItGrpName, ");
            //if (mIsRptDOCtShowPriceInfo)
            //    SQL.AppendLine("B.UPrice As UPrice1, F.UPrice As UPrice2, ");
            //SQL.AppendLine("A.Remark ");
            //SQL.AppendLine("From TblDOCtHdr A ");
            //SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            //SQL.AppendLine("Inner Join TblCustomer C on A.CtCode=C.CtCode ");
            //SQL.AppendLine("Inner Join TblItem D on B.ItCode=D.ItCode ");
            //if (mIsItGrpCodeShow)
            //    SQL.AppendLine("Left join TblItemGroup E On D.ItGrpCode=E.ItGrpCode ");
            //if (mIsRptDOCtShowPriceInfo)
            //    SQL.AppendLine("Left join TblStockPrice F On B.Source=F.Source ");
            //SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("Order By B.Qty Desc LIMIT 25 ");
            #endregion

            #region New Code
            //SQL.AppendLine("    Select D.ItName, Sum(B.Qty) As Qty ");
            //SQL.AppendLine("    From TblDOCt2Hdr A ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            //SQL.AppendLine("    Inner Join TblCustomer C on A.CtCode=C.CtCode ");
            //SQL.AppendLine("    Inner Join TblItem D on B.ItCode=D.ItCode ");
            //SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("    Group By D.ItName ");
            //SQL.AppendLine("    Order By Sum(B.Qty) Desc Limit 25 ");

            if (CategoryCode == "1")
            {
                GetSQLItemSubCategory();
            }
            else if (CategoryCode == "2")
            {
                GetSQLItemGroup();
            }
            else if (CategoryCode == "3")
            {
                GetSQLItem();
            }

            #endregion

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = mSQL;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]

                {
                    //0
                    "ItName",

                    //1
                    "Qty"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TopSellingProduct()
                        {
                            ItName = Sm.DrStr(dr, c[0]),
                            Qty = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                else
                    Sm.StdMsg(mMsgType.NoData, "");

                dr.Close();
            }

            BindChart(ref l);
        }

        private void BindChart(ref List<TopSellingProduct> dataChart)
        {
            this.Chart.Series.Clear();
            ChartSeries series = new ChartSeries(mChartTitle, ChartSeriesType.Bar);

            dataChart.ForEach(i =>
            { series.Points.Add(i.ItName, (double)i.Qty); });

            //foreach (var i in dataChart.OrderByDescending(x => x.Qty))
            //{
            //    series.Points.Add(i.ItName, (double)i.Qty);
            //}

            //display point on top of series
            series.Style.DisplayText = true;
            series.Style.TextOrientation = ChartTextOrientation.Right;

            // sorting data berdasarkan qty
            series.SortPoints = true;
            series.SortOrder = ChartSeriesSortingOrder.Ascending;
            series.SortBy = ChartSeriesSortingType.Y;

            Chart.Series.Add(series);
            Chart.PrimaryXAxis.Title = "Quantity";
            //Chart.PrimaryYAxis.Title = "Item's Name";
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsItGrpCodeShow = Sm.GetParameter("IsItGrpCodeShow") == "Y";
            mIsRptDOCtShowPriceInfo = Sm.GetParameter("IsRptDOCtShowPriceInfo") == "Y";
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
        }

        private bool IsFilterByDateInvalid()
        {
            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        protected void OpenFile(string filetype, string exportFileName)
        {
            try
            {
                //if (filetype == "Grid")
                //    gridForm.ShowDialog();
                //else
                System.Diagnostics.Process.Start(exportFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void SetLueCategoryCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.OptCode As Col1, T.OptDesc As Col2 ");
            SQL.AppendLine("From TblOption T ");
            SQL.AppendLine("Where T.OptCat = 'DashboardOmsetCategory' ");
            SQL.AppendLine("Order By T.OptCode; ");

            cm.CommandText = SQL.ToString();

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItScCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ItScCode As Col1, ItScName As Col2 " +
                "From Tblitemsubcategory " + 
                "Order By ItScName ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItGrpCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ItGrpCode As Col1, ItGrpName As Col2 " +
                "From tblitemgroup " + 
                "Order By ItGrpName ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string GetSQLItem()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItName, Sum(T.Qty) As Qty From ");
            SQL.AppendLine("( ");
            SQL.AppendLine(SubQuery());
            SQL.AppendLine(")T ");
            SQL.AppendLine("Group By T.ItCode, T.ItName ");
            SQL.AppendLine("Order By Sum(T.Qty) Desc Limit 25; ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private string GetSQLItemSubCategory()
        {
            var SQL = new StringBuilder();

            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("Select T.ItCode, T.ItName, Sum(T.Qty) As Qty From ");
            else
                SQL.AppendLine("Select T.ItScCode, T.ItScName As ItName, Sum(T.Qty) As Qty From ");

            SQL.AppendLine("( ");
            SQL.AppendLine(SubQuery());
            SQL.AppendLine(")T ");

            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("    Where T.ItScCode='" + Sm.GetLue(LueFilterCategory) + "'  ");

            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("Group By T.ItCode, T.ItName ");
            else
                SQL.AppendLine("Group By T.ItScCode, T.ItScName ");

            SQL.AppendLine("Order By Sum(T.Qty) Desc Limit 25; ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private string GetSQLItemGroup()
        {
            var SQL = new StringBuilder();

            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("Select T.ItCode, T.ItName, Sum(T.Qty) As Qty From ");
            else
                SQL.AppendLine("Select T.ItGrpCode, T.ItGrpName As ItName, Sum(T.Qty) As Qty From ");

            SQL.AppendLine("( ");
            SQL.AppendLine(SubQuery());
            SQL.AppendLine(")T ");

            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("    Where T.ItGrpCode='" + Sm.GetLue(LueFilterCategory) + "'  ");

            if (Sm.GetLue(LueFilterCategory).Length > 0)
                SQL.AppendLine("Group By T.ItCode, T.ItName ");
            else
                SQL.AppendLine("Group By T.ItGrpCode, T.ItGrpName ");

            SQL.AppendLine("Order By Sum(T.Qty) Desc Limit 25; ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        #endregion

        #region Button Click

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            PrintDialog pr = new PrintDialog();
            PrintPreviewDialog ppd = new PrintPreviewDialog();

            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.Info, "No Data");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Warning, "The chart is empty");
                else
                {
                    //var a = Chart.Series[0].Points[4].X;
                    //var b = Chart.Series[0].Points[4].YValues[0];

                    pr.AllowSomePages = true;
                    pr.AllowSelection = true;
                    pr.PrinterSettings.Clone();
                    pr.Document = Chart.PrintDocument;
                    if (pr.ShowDialog() == DialogResult.OK)
                        pr.Document.Print();
                }
            }

            //if (pr.ShowDialog() == DialogResult.OK)
            //{
            //    pr.Document = Chart.PrintDocument;
            //    ppd.Document = Chart.PrintDocument;
            //    if (ppd.ShowDialog() == DialogResult.OK)
            //        //pr.Document.Print();
            //        ppd.Document.Print();
            //}
        }

        private void BtnWord_Click(object sender, EventArgs e)
        {
            try
            {
                if (Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + mChartTitle + ".doc";
                        file = Application.StartupPath + "\\ChartExport_" + mChartTitle + ".gif";

                        //Application.StartupPath + "\\ChartExport_" + Chart.Title.Text + "_" + Sm.ConvertDate(Sm.GetDte(DteDocDt1)) + "_TO_" + Sm.ConvertDate(Sm.GetDte(DteDocDt2)) + ".gif";
                        //if (!System.IO.File.Exists(file))
                        Chart.SaveImage(file);

                        //Create a new document
                        WordDocument document = new WordDocument();
                        //Adding a new section to the document.
                        IWSection section = document.AddSection();
                        //Adding a paragraph to the section
                        IWParagraph paragraph = section.AddParagraph();
                        //Writing text.
                        paragraph.AppendText(this.Chart.Title.Text);
                        //Adding a new paragraph		
                        paragraph = section.AddParagraph();
                        paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        //Inserting chart.
                        paragraph.AppendPicture(Image.FromFile(file));
                        //Save the Document to disk.
                        document.Save(exportFileName, Syncfusion.DocIO.FormatType.Doc);
                        System.Diagnostics.Process.Start(exportFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            if (Chart.Series.Count == 0)
                Sm.StdMsg(mMsgType.NoData, "");
            else
            {
                if (Chart.Series[0].Points.Count == 0)
                    Sm.StdMsg(mMsgType.Info, "The chart is empty.");
                else
                {
                    exportFileName = Application.StartupPath + "\\ChartExport_" + mChartTitle + ".xls";

                    // A new workbook with a worksheet should be created. 
                    SyXL.IWorkbook chartBook = SyXL.ExcelUtils.CreateWorkbook(1);
                    SyXL.IWorksheet sheet = chartBook.Worksheets[0];

                    //if chart is not empty.
                    // Fill the worksheet with chart data. 
                    for (int i = 1; i <= Chart.Series[0].Points.Count; i++)
                    {
                        sheet.Range[i, 1].Number = Chart.Series[0].Points[i - 1].X;
                        sheet.Range[i, 2].Number = Chart.Series[0].Points[i - 1].YValues[0];
                    }

                    // Create a chart worksheet. 
                    SyXL.IChart chart = chartBook.Charts.Add(Chart.Title.Text);

                    // Specify the title of the Chart.
                    chart.ChartTitle = Chart.Title.Text;

                    // Initialize a new series instance and add it to the series collection of the chart. 
                    SyXL.IChartSerie series = chart.Series.Add(mChartTitle);

                    // Specify the chart type of the series. 
                    series.SerieType = SyXL.ExcelChartType.Column_Clustered;

                    // Specify the name of the series. This will be displayed as the text of the legend. 
                    //series.Name = Chart.Name;

                    // Specify the value ranges for the series. 
                    series.Values = sheet.Range["B1:B10"];

                    // Specify the Category labels for the series. 
                    series.CategoryLabels = sheet.Range["A1:A10"];

                    // Make the chart as active sheet. 
                    chart.Activate();

                    // Save the Chart book. 
                    chartBook.SaveAs(exportFileName); chartBook.Close();
                    SyXL.ExcelUtils.Close();

                    // Launches the file. 
                    System.Diagnostics.Process.Start(exportFileName);
                }
            }
        }

        private void BtnPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Chart.Series.Count == 0)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    if (Chart.Series[0].Points.Count == 0)
                        Sm.StdMsg(mMsgType.Info, "The chart is empty");
                    else
                    {
                        exportFileName = Application.StartupPath + "\\ChartExport_" + mChartTitle + ".pdf";
                        file = Application.StartupPath + "\\ChartExport_" + mChartTitle + ".gif";

                        //if (!System.IO.File.Exists(file))
                        this.Chart.SaveImage(file);

                        //Create a new PDF Document. The pdfDoc object represents the PDF document.
                        //This document has one page by default and additional pages have to be added.
                        PdfDocument pdfDoc = new PdfDocument();

                        pdfDoc.Pages.Add();

                        pdfDoc.Pages[0].Graphics.DrawImage(PdfImage.FromFile(file), new PointF(10, 30));

                        //Save the PDF Document to disk.
                        pdfDoc.Save(exportFileName);
                        OpenFile("Pdf", exportFileName);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0)
                DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCategoryCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCategoryCode, new Sm.RefreshLue1(SetLueCategoryCode));
            var ItCtCode = Sm.GetLue(LueCategoryCode);
            if (ItCtCode == "1")
            {
                LblSubCategory.Text = "Sub Category";
                SetLueItScCode(ref LueFilterCategory);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueFilterCategory }, false);
            }
            if (ItCtCode == "2")
            {
                LblSubCategory.Text = "Item Group";
                SetLueItGrpCode(ref LueFilterCategory);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueFilterCategory }, false);
            }
            if (ItCtCode == "3")
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueFilterCategory });
                LblSubCategory.Text = "Item Name";
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueFilterCategory }, true);
            }
        }

        private void LueFilterCategory_EditValueChanged(object sender, EventArgs e)
        {
            var ItCtCode = Sm.GetLue(LueCategoryCode);
            if (ItCtCode == "1")
                Sm.RefreshLookUpEdit(LueFilterCategory, new Sm.RefreshLue1(SetLueItScCode));
            if (ItCtCode == "2")
                Sm.RefreshLookUpEdit(LueFilterCategory, new Sm.RefreshLue1(SetLueItGrpCode));
        }

        #endregion

        #endregion

    }

    #region Class

    class TopSellingProduct
    {
        public string ItName { get; set; }
        public decimal Qty { get; set; }
    }

    #endregion
}
