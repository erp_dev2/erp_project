﻿#region Update
/*
    29/11/2021 [NJP/RM] New Apss Login History Vendor RunMarket
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmRptLog2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptLog2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteLog1, ref DteLog2, -1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            mSQL =
                "SELECT "+
                "A.UserCode, B.UserName, GrpCode AS GrpName, left(A.Login, 12) as Login, Logout, A.Ip,A.Machine, A.SysVer "+
                 "FROM tbllog A "+
                "INNER JOIN tbluservendor B ON A.UserCode = B.UserCode "+
                "Where Left(A.Login, 8) Between @DocDt1 And @DocDt2 " +
                "Order By A.Login;";
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "User Code", 
                        "User Name",
                        "Group",
                        "Login Date",
                        "Login"+Environment.NewLine+"Time",
                        
                        //6-10
                        "Logout Date",
                        "Logout"+Environment.NewLine+"Time",
                        "IP",
                        "Machine",
                        "Version"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 200, 0, 100, 80, 
                        
                        //6-10
                        100, 80, 100, 100, 80
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 6 });
            Sm.GrdFormatTime(Grd1, new int[] { 5, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 7, 8, 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteLog1, "Start date") ||
                Sm.IsDteEmpty(DteLog2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteLog1, ref DteLog2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteLog1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteLog2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[]
                        {
                            //0
                            "UserCode", 

                            //1-5
                            "UserName", "GrpName", "Login", "Logout", "Ip", 
                            
                            //6-7
                            "Machine", "SysVer"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteLog1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteLog2).Length == 0) DteLog2.EditValue = DteLog1.EditValue;
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion
        
        #endregion
    }
}
