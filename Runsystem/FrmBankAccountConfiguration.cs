﻿#region Update
/*
  29/11/2021 [DITA/PHT] New Apps
 */ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBankAccountConfiguration : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, mDocNo = string.Empty;
        internal FrmBankAccountConfigurationFind FrmFind;

        #endregion

        #region Constructor

        public FrmBankAccountConfiguration(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Bank Account For Transaction";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueType, "BankAccountTransactionType");

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "",

                    //1-5
                    "Account Code",
                    "Bank Code",
                    "Bank",
                    "Account#",
                    "Account Name",
                },
                new int[] 
                { 
                    20, 
                    150, 100, 250, 200, 250
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        LueType
                    }, true);

                    Grd1.ReadOnly = true;
                    LueType.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        LueType
                    }, false);
                    Grd1.ReadOnly = false;
                    LueType.Focus();
                    break;
                case mState.Edit:
                    Grd1.ReadOnly = false;
                    LueType.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                LueType
            });
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBankAccountConfigurationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueType, "")) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                    SaveData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void SaveData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveBankAccountConfigurationHdr(Sm.GetLue(LueType)));

            cml.Add(DeleteBankAccountConfigurationDtl(Sm.GetLue(LueType)));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    cml.Add(SaveBankAccountConfigurationDtl(Sm.GetLue(LueType), Row));

            Sm.ExecCommands(cml);

            ShowData(Sm.GetLue(LueType));
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueType, "Transaction type") ||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 COA Number.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveBankAccountConfigurationHdr(string Type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBankAccountConfigurationHdr ");
            SQL.AppendLine("(Type, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@Type, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update Type=@Type, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Type", Type);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBankAccountConfigurationDtl(string Type, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBankAccountConfigurationDtl ");
            SQL.AppendLine("(Type, DNo, BankAcCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@Type, @DNo, @BankAcCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Type", Type);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (r + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteBankAccountConfigurationDtl(string Type)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Delete From TblBankAccountConfigurationDtl Where Type = @Type; ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@Type", Type);

            return cm;
        }

        #endregion


        #region Show Data

        public void ShowData(string Type)
        {
            try
            {
                ClearData();
                ShowBankAccountConfigurationHdr(Type);
                ShowBankAccountConfigurationDtl(Type);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowBankAccountConfigurationHdr(string Type)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT Type ");
            SQL.AppendLine("FROM TblBankAccountConfigurationHdr A ");
            SQL.AppendLine("Inner Join TblOption B On A.Type = B.OptCode And OptCat = 'BankAccountTransactionType' ");
            SQL.AppendLine("WHERE Type=@Type; ");

            Sm.CmParam<String>(ref cm, "@Type", Type);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "Type",
                    
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     Sm.SetLue(LueType, Sm.DrStr(dr, c[0]));
                 }, true
             );
        }

        private void ShowBankAccountConfigurationDtl(string Type)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BankAcCode, C.BankCode, C.BankName, B.BankAcNo, B.BankAcNm ");
            SQL.AppendLine("From TblBankAccountConfigurationDtl A ");
            SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode ");
            SQL.AppendLine("Left Join TblBank C On B.BankCode=C.BankCode ");
            SQL.AppendLine("Where A.Type=@Type;");

            Sm.CmParam<String>(ref cm, "@Type", Type);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "BankAcCode",
                    
                    //1-4
                    "BankCode","BankName", "BankAcNo", "BankAcNm"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmBankAccountConfigurationDlg(this));
            }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0)
                Sm.FormShowDialog(new FrmBankAccountConfigurationDlg(this));
        }

        #endregion


        #region Additional Methods

        internal string GetSelectedBankAcCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Event

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "BankAccountTransactionType");
        }

        #endregion
    }
}
