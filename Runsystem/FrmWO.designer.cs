﻿namespace RunSystem
{
    partial class FrmWO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWO));
            this.panel3 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtAssetName = new DevExpress.XtraEditors.TextEdit();
            this.BtnTOCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtTOCode = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueEquipmentCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblEquipmentCode = new System.Windows.Forms.Label();
            this.LueSymptomProblem = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnRecv = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDODept = new DevExpress.XtraEditors.SimpleButton();
            this.LueMaintenanceType = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnDOReq = new DevExpress.XtraEditors.SimpleButton();
            this.BtnMR = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRecvDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtDODeptDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtDOReqDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtMRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSite = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LblSymptom = new System.Windows.Forms.Label();
            this.TxtHoursMeter = new DevExpress.XtraEditors.TextEdit();
            this.label102 = new System.Windows.Forms.Label();
            this.ChkSettleInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeDescription = new DevExpress.XtraEditors.MemoExEdit();
            this.BtnWORDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtWORDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnWORDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.LueBreakDown = new DevExpress.XtraEditors.LookUpEdit();
            this.LueWOActivity = new DevExpress.XtraEditors.LookUpEdit();
            this.TmeTm = new DevExpress.XtraEditors.TimeEdit();
            this.DteDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.ChkFixedInd = new DevExpress.XtraEditors.CheckEdit();
            this.BtnRPL = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTOCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEquipmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSymptomProblem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaintenanceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDODeptDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOReqDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHoursMeter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSettleInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWORDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBreakDown.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWOActivity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeTm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFixedInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnRPL);
            this.panel1.Location = new System.Drawing.Point(877, 0);
            this.panel1.Size = new System.Drawing.Size(70, 540);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.BtnRPL, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(877, 540);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.TxtDisplayName);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.TxtAssetName);
            this.panel3.Controls.Add(this.BtnTOCode);
            this.panel3.Controls.Add(this.TxtTOCode);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.TxtHoursMeter);
            this.panel3.Controls.Add(this.label102);
            this.panel3.Controls.Add(this.ChkSettleInd);
            this.panel3.Controls.Add(this.MeeDescription);
            this.panel3.Controls.Add(this.BtnWORDocNo2);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.TxtWORDocNo);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.BtnWORDocNo);
            this.panel3.Controls.Add(this.MeeCancelReason);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(877, 200);
            this.panel3.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(32, 133);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 14);
            this.label16.TabIndex = 26;
            this.label16.Text = "Asset Display Name";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(149, 131);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 16;
            this.TxtDisplayName.Properties.ReadOnly = true;
            this.TxtDisplayName.Size = new System.Drawing.Size(244, 20);
            this.TxtDisplayName.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(72, 112);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 14);
            this.label15.TabIndex = 24;
            this.label15.Text = "Asset Name";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAssetName
            // 
            this.TxtAssetName.EnterMoveNextControl = true;
            this.TxtAssetName.Location = new System.Drawing.Point(149, 110);
            this.TxtAssetName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAssetName.Name = "TxtAssetName";
            this.TxtAssetName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAssetName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAssetName.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetName.Properties.MaxLength = 16;
            this.TxtAssetName.Properties.ReadOnly = true;
            this.TxtAssetName.Size = new System.Drawing.Size(244, 20);
            this.TxtAssetName.TabIndex = 25;
            // 
            // BtnTOCode
            // 
            this.BtnTOCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnTOCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnTOCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTOCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnTOCode.Appearance.Options.UseBackColor = true;
            this.BtnTOCode.Appearance.Options.UseFont = true;
            this.BtnTOCode.Appearance.Options.UseForeColor = true;
            this.BtnTOCode.Appearance.Options.UseTextOptions = true;
            this.BtnTOCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnTOCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnTOCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnTOCode.Image")));
            this.BtnTOCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnTOCode.Location = new System.Drawing.Point(398, 90);
            this.BtnTOCode.Name = "BtnTOCode";
            this.BtnTOCode.Size = new System.Drawing.Size(24, 21);
            this.BtnTOCode.TabIndex = 23;
            this.BtnTOCode.ToolTip = "Show TO Information";
            this.BtnTOCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnTOCode.ToolTipTitle = "Run System";
            this.BtnTOCode.Click += new System.EventHandler(this.BtnTOCode_Click);
            // 
            // TxtTOCode
            // 
            this.TxtTOCode.EnterMoveNextControl = true;
            this.TxtTOCode.Location = new System.Drawing.Point(149, 89);
            this.TxtTOCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTOCode.Name = "TxtTOCode";
            this.TxtTOCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTOCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTOCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTOCode.Properties.Appearance.Options.UseFont = true;
            this.TxtTOCode.Properties.MaxLength = 16;
            this.TxtTOCode.Properties.ReadOnly = true;
            this.TxtTOCode.Size = new System.Drawing.Size(244, 20);
            this.TxtTOCode.TabIndex = 22;
            this.TxtTOCode.EditValueChanged += new System.EventHandler(this.TxtTOCode_EditValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(88, 92);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 14);
            this.label14.TabIndex = 21;
            this.label14.Text = "TO Code";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LueEquipmentCode);
            this.panel4.Controls.Add(this.LblEquipmentCode);
            this.panel4.Controls.Add(this.LueSymptomProblem);
            this.panel4.Controls.Add(this.BtnRecv);
            this.panel4.Controls.Add(this.BtnDODept);
            this.panel4.Controls.Add(this.LueMaintenanceType);
            this.panel4.Controls.Add(this.BtnDOReq);
            this.panel4.Controls.Add(this.BtnMR);
            this.panel4.Controls.Add(this.TxtRecvDocNo);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtDODeptDocNo);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.TxtDOReqDocNo);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.TxtMRDocNo);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtSite);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.LblSymptom);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(479, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(398, 200);
            this.panel4.TabIndex = 32;
            // 
            // LueEquipmentCode
            // 
            this.LueEquipmentCode.EnterMoveNextControl = true;
            this.LueEquipmentCode.Location = new System.Drawing.Point(120, 47);
            this.LueEquipmentCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEquipmentCode.Name = "LueEquipmentCode";
            this.LueEquipmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEquipmentCode.Properties.Appearance.Options.UseFont = true;
            this.LueEquipmentCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEquipmentCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEquipmentCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEquipmentCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEquipmentCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEquipmentCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEquipmentCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEquipmentCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEquipmentCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEquipmentCode.Properties.DropDownRows = 5;
            this.LueEquipmentCode.Properties.NullText = "[Empty]";
            this.LueEquipmentCode.Properties.PopupWidth = 350;
            this.LueEquipmentCode.Size = new System.Drawing.Size(244, 20);
            this.LueEquipmentCode.TabIndex = 38;
            this.LueEquipmentCode.ToolTip = "F4 : Show/hide list";
            this.LueEquipmentCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEquipmentCode.EditValueChanged += new System.EventHandler(this.LueEquipment_EditValueChanged);
            // 
            // LblEquipmentCode
            // 
            this.LblEquipmentCode.AutoSize = true;
            this.LblEquipmentCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEquipmentCode.ForeColor = System.Drawing.Color.Black;
            this.LblEquipmentCode.Location = new System.Drawing.Point(48, 50);
            this.LblEquipmentCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEquipmentCode.Name = "LblEquipmentCode";
            this.LblEquipmentCode.Size = new System.Drawing.Size(66, 14);
            this.LblEquipmentCode.TabIndex = 37;
            this.LblEquipmentCode.Text = "Equipment";
            this.LblEquipmentCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSymptomProblem
            // 
            this.LueSymptomProblem.EnterMoveNextControl = true;
            this.LueSymptomProblem.Location = new System.Drawing.Point(120, 26);
            this.LueSymptomProblem.Margin = new System.Windows.Forms.Padding(5);
            this.LueSymptomProblem.Name = "LueSymptomProblem";
            this.LueSymptomProblem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSymptomProblem.Properties.Appearance.Options.UseFont = true;
            this.LueSymptomProblem.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSymptomProblem.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSymptomProblem.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSymptomProblem.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSymptomProblem.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSymptomProblem.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSymptomProblem.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSymptomProblem.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSymptomProblem.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSymptomProblem.Properties.DropDownRows = 5;
            this.LueSymptomProblem.Properties.NullText = "[Empty]";
            this.LueSymptomProblem.Properties.PopupWidth = 350;
            this.LueSymptomProblem.Size = new System.Drawing.Size(244, 20);
            this.LueSymptomProblem.TabIndex = 36;
            this.LueSymptomProblem.ToolTip = "F4 : Show/hide list";
            this.LueSymptomProblem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSymptomProblem.EditValueChanged += new System.EventHandler(this.LueSymptomProblem_EditValueChanged);
            // 
            // BtnRecv
            // 
            this.BtnRecv.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRecv.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRecv.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRecv.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRecv.Appearance.Options.UseBackColor = true;
            this.BtnRecv.Appearance.Options.UseFont = true;
            this.BtnRecv.Appearance.Options.UseForeColor = true;
            this.BtnRecv.Appearance.Options.UseTextOptions = true;
            this.BtnRecv.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRecv.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRecv.Image = ((System.Drawing.Image)(resources.GetObject("BtnRecv.Image")));
            this.BtnRecv.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRecv.Location = new System.Drawing.Point(367, 130);
            this.BtnRecv.Name = "BtnRecv";
            this.BtnRecv.Size = new System.Drawing.Size(24, 21);
            this.BtnRecv.TabIndex = 50;
            this.BtnRecv.ToolTip = "Show Receiving Information";
            this.BtnRecv.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRecv.ToolTipTitle = "Run System";
            this.BtnRecv.Click += new System.EventHandler(this.BtnRecv_Click);
            // 
            // BtnDODept
            // 
            this.BtnDODept.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDODept.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDODept.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDODept.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDODept.Appearance.Options.UseBackColor = true;
            this.BtnDODept.Appearance.Options.UseFont = true;
            this.BtnDODept.Appearance.Options.UseForeColor = true;
            this.BtnDODept.Appearance.Options.UseTextOptions = true;
            this.BtnDODept.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDODept.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDODept.Image = ((System.Drawing.Image)(resources.GetObject("BtnDODept.Image")));
            this.BtnDODept.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDODept.Location = new System.Drawing.Point(367, 109);
            this.BtnDODept.Name = "BtnDODept";
            this.BtnDODept.Size = new System.Drawing.Size(24, 21);
            this.BtnDODept.TabIndex = 47;
            this.BtnDODept.ToolTip = "Show DO Department Information";
            this.BtnDODept.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDODept.ToolTipTitle = "Run System";
            this.BtnDODept.Click += new System.EventHandler(this.BtnDODept_Click);
            // 
            // LueMaintenanceType
            // 
            this.LueMaintenanceType.EnterMoveNextControl = true;
            this.LueMaintenanceType.Location = new System.Drawing.Point(120, 5);
            this.LueMaintenanceType.Margin = new System.Windows.Forms.Padding(5);
            this.LueMaintenanceType.Name = "LueMaintenanceType";
            this.LueMaintenanceType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaintenanceType.Properties.Appearance.Options.UseFont = true;
            this.LueMaintenanceType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaintenanceType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMaintenanceType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaintenanceType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMaintenanceType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaintenanceType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMaintenanceType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaintenanceType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMaintenanceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMaintenanceType.Properties.DropDownRows = 5;
            this.LueMaintenanceType.Properties.NullText = "[Empty]";
            this.LueMaintenanceType.Properties.PopupWidth = 350;
            this.LueMaintenanceType.Size = new System.Drawing.Size(244, 20);
            this.LueMaintenanceType.TabIndex = 34;
            this.LueMaintenanceType.ToolTip = "F4 : Show/hide list";
            this.LueMaintenanceType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMaintenanceType.EditValueChanged += new System.EventHandler(this.LueMaintenanceType_EditValueChanged);
            // 
            // BtnDOReq
            // 
            this.BtnDOReq.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDOReq.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDOReq.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDOReq.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDOReq.Appearance.Options.UseBackColor = true;
            this.BtnDOReq.Appearance.Options.UseFont = true;
            this.BtnDOReq.Appearance.Options.UseForeColor = true;
            this.BtnDOReq.Appearance.Options.UseTextOptions = true;
            this.BtnDOReq.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDOReq.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDOReq.Image = ((System.Drawing.Image)(resources.GetObject("BtnDOReq.Image")));
            this.BtnDOReq.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDOReq.Location = new System.Drawing.Point(367, 89);
            this.BtnDOReq.Name = "BtnDOReq";
            this.BtnDOReq.Size = new System.Drawing.Size(24, 21);
            this.BtnDOReq.TabIndex = 44;
            this.BtnDOReq.ToolTip = "Show DO Request Information";
            this.BtnDOReq.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDOReq.ToolTipTitle = "Run System";
            this.BtnDOReq.Click += new System.EventHandler(this.BtnDOReq_Click);
            // 
            // BtnMR
            // 
            this.BtnMR.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMR.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMR.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMR.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMR.Appearance.Options.UseBackColor = true;
            this.BtnMR.Appearance.Options.UseFont = true;
            this.BtnMR.Appearance.Options.UseForeColor = true;
            this.BtnMR.Appearance.Options.UseTextOptions = true;
            this.BtnMR.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMR.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMR.Image = ((System.Drawing.Image)(resources.GetObject("BtnMR.Image")));
            this.BtnMR.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMR.Location = new System.Drawing.Point(367, 67);
            this.BtnMR.Name = "BtnMR";
            this.BtnMR.Size = new System.Drawing.Size(24, 21);
            this.BtnMR.TabIndex = 41;
            this.BtnMR.ToolTip = "Show Material Request WO";
            this.BtnMR.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMR.ToolTipTitle = "Run System";
            this.BtnMR.Click += new System.EventHandler(this.BtnMR_Click);
            // 
            // TxtRecvDocNo
            // 
            this.TxtRecvDocNo.EnterMoveNextControl = true;
            this.TxtRecvDocNo.Location = new System.Drawing.Point(120, 131);
            this.TxtRecvDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRecvDocNo.Name = "TxtRecvDocNo";
            this.TxtRecvDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRecvDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRecvDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRecvDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRecvDocNo.Properties.MaxLength = 30;
            this.TxtRecvDocNo.Properties.ReadOnly = true;
            this.TxtRecvDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtRecvDocNo.TabIndex = 49;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(48, 134);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 14);
            this.label12.TabIndex = 48;
            this.label12.Text = "Receiving#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDODeptDocNo
            // 
            this.TxtDODeptDocNo.EnterMoveNextControl = true;
            this.TxtDODeptDocNo.Location = new System.Drawing.Point(120, 110);
            this.TxtDODeptDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDODeptDocNo.Name = "TxtDODeptDocNo";
            this.TxtDODeptDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDODeptDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDODeptDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDODeptDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDODeptDocNo.Properties.MaxLength = 30;
            this.TxtDODeptDocNo.Properties.ReadOnly = true;
            this.TxtDODeptDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtDODeptDocNo.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(12, 113);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 14);
            this.label11.TabIndex = 45;
            this.label11.Text = "DO Department#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDOReqDocNo
            // 
            this.TxtDOReqDocNo.EnterMoveNextControl = true;
            this.TxtDOReqDocNo.Location = new System.Drawing.Point(120, 89);
            this.TxtDOReqDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDOReqDocNo.Name = "TxtDOReqDocNo";
            this.TxtDOReqDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDOReqDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDOReqDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDOReqDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDOReqDocNo.Properties.MaxLength = 30;
            this.TxtDOReqDocNo.Properties.ReadOnly = true;
            this.TxtDOReqDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtDOReqDocNo.TabIndex = 43;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(33, 92);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 14);
            this.label9.TabIndex = 42;
            this.label9.Text = "DO Request#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(7, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 14);
            this.label3.TabIndex = 33;
            this.label3.Text = "Maintenance Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMRDocNo
            // 
            this.TxtMRDocNo.EnterMoveNextControl = true;
            this.TxtMRDocNo.Location = new System.Drawing.Point(120, 68);
            this.TxtMRDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMRDocNo.Name = "TxtMRDocNo";
            this.TxtMRDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMRDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtMRDocNo.Properties.MaxLength = 30;
            this.TxtMRDocNo.Properties.ReadOnly = true;
            this.TxtMRDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtMRDocNo.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(9, 71);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 14);
            this.label7.TabIndex = 39;
            this.label7.Text = "Material Request#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSite
            // 
            this.TxtSite.EnterMoveNextControl = true;
            this.TxtSite.Location = new System.Drawing.Point(120, 152);
            this.TxtSite.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSite.Name = "TxtSite";
            this.TxtSite.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSite.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSite.Properties.Appearance.Options.UseFont = true;
            this.TxtSite.Properties.MaxLength = 80;
            this.TxtSite.Properties.ReadOnly = true;
            this.TxtSite.Size = new System.Drawing.Size(244, 20);
            this.TxtSite.TabIndex = 52;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(87, 155);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 14);
            this.label6.TabIndex = 51;
            this.label6.Text = "Site";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(120, 173);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 500;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(244, 20);
            this.MeeRemark.TabIndex = 54;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(68, 176);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 53;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSymptom
            // 
            this.LblSymptom.AutoSize = true;
            this.LblSymptom.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSymptom.ForeColor = System.Drawing.Color.Black;
            this.LblSymptom.Location = new System.Drawing.Point(8, 29);
            this.LblSymptom.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSymptom.Name = "LblSymptom";
            this.LblSymptom.Size = new System.Drawing.Size(107, 14);
            this.LblSymptom.TabIndex = 35;
            this.LblSymptom.Text = "Symptom Problem";
            this.LblSymptom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtHoursMeter
            // 
            this.TxtHoursMeter.EnterMoveNextControl = true;
            this.TxtHoursMeter.Location = new System.Drawing.Point(149, 152);
            this.TxtHoursMeter.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHoursMeter.Name = "TxtHoursMeter";
            this.TxtHoursMeter.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtHoursMeter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHoursMeter.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHoursMeter.Properties.Appearance.Options.UseFont = true;
            this.TxtHoursMeter.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtHoursMeter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtHoursMeter.Size = new System.Drawing.Size(109, 20);
            this.TxtHoursMeter.TabIndex = 29;
            this.TxtHoursMeter.Validated += new System.EventHandler(this.TxtHoursMeter_Validated);
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(70, 155);
            this.label102.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(74, 14);
            this.label102.TabIndex = 28;
            this.label102.Text = "Hours Meter";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkSettleInd
            // 
            this.ChkSettleInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkSettleInd.Location = new System.Drawing.Point(397, 4);
            this.ChkSettleInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkSettleInd.Name = "ChkSettleInd";
            this.ChkSettleInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkSettleInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSettleInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkSettleInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkSettleInd.Properties.Appearance.Options.UseFont = true;
            this.ChkSettleInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkSettleInd.Properties.Caption = "Settled";
            this.ChkSettleInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSettleInd.Properties.ReadOnly = true;
            this.ChkSettleInd.Size = new System.Drawing.Size(67, 22);
            this.ChkSettleInd.TabIndex = 11;
            // 
            // MeeDescription
            // 
            this.MeeDescription.EnterMoveNextControl = true;
            this.MeeDescription.Location = new System.Drawing.Point(149, 173);
            this.MeeDescription.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDescription.Name = "MeeDescription";
            this.MeeDescription.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeDescription.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.Appearance.Options.UseBackColor = true;
            this.MeeDescription.Properties.Appearance.Options.UseFont = true;
            this.MeeDescription.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDescription.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDescription.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDescription.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDescription.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDescription.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDescription.Properties.MaxLength = 500;
            this.MeeDescription.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDescription.Properties.ReadOnly = true;
            this.MeeDescription.Properties.ShowIcon = false;
            this.MeeDescription.Size = new System.Drawing.Size(244, 22);
            this.MeeDescription.TabIndex = 31;
            this.MeeDescription.ToolTip = "F4 : Show/hide text";
            this.MeeDescription.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDescription.ToolTipTitle = "Run System";
            // 
            // BtnWORDocNo2
            // 
            this.BtnWORDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnWORDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWORDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWORDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWORDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnWORDocNo2.Appearance.Options.UseFont = true;
            this.BtnWORDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnWORDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnWORDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWORDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnWORDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnWORDocNo2.Image")));
            this.BtnWORDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnWORDocNo2.Location = new System.Drawing.Point(429, 68);
            this.BtnWORDocNo2.Name = "BtnWORDocNo2";
            this.BtnWORDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnWORDocNo2.TabIndex = 20;
            this.BtnWORDocNo2.ToolTip = "Show WO Request Information";
            this.BtnWORDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnWORDocNo2.ToolTipTitle = "Run System";
            this.BtnWORDocNo2.Click += new System.EventHandler(this.BtnWORDocNo2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(77, 177);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 14);
            this.label4.TabIndex = 30;
            this.label4.Text = "Description";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWORDocNo
            // 
            this.TxtWORDocNo.EnterMoveNextControl = true;
            this.TxtWORDocNo.Location = new System.Drawing.Point(149, 68);
            this.TxtWORDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWORDocNo.Name = "TxtWORDocNo";
            this.TxtWORDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtWORDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWORDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWORDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtWORDocNo.Properties.MaxLength = 16;
            this.TxtWORDocNo.Properties.ReadOnly = true;
            this.TxtWORDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtWORDocNo.TabIndex = 18;
            this.TxtWORDocNo.EditValueChanged += new System.EventHandler(this.TxtWORDocNo_EditValueChanged);
            this.TxtWORDocNo.Validated += new System.EventHandler(this.TxtWORDocNo_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(58, 71);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 14);
            this.label8.TabIndex = 17;
            this.label8.Text = "WO Request#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnWORDocNo
            // 
            this.BtnWORDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnWORDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWORDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWORDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWORDocNo.Appearance.Options.UseBackColor = true;
            this.BtnWORDocNo.Appearance.Options.UseFont = true;
            this.BtnWORDocNo.Appearance.Options.UseForeColor = true;
            this.BtnWORDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnWORDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWORDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnWORDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnWORDocNo.Image")));
            this.BtnWORDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnWORDocNo.Location = new System.Drawing.Point(398, 68);
            this.BtnWORDocNo.Name = "BtnWORDocNo";
            this.BtnWORDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnWORDocNo.TabIndex = 19;
            this.BtnWORDocNo.ToolTip = "Find WO Request";
            this.BtnWORDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnWORDocNo.ToolTipTitle = "Run System";
            this.BtnWORDocNo.Click += new System.EventHandler(this.BtnWORDocNo_Click);
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(149, 47);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 500;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(244, 20);
            this.MeeCancelReason.TabIndex = 15;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(397, 45);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 16;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 51);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 14);
            this.label13.TabIndex = 14;
            this.label13.Text = "Reason For Cancellation";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(149, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 50;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(244, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(71, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(111, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(149, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 200);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Grd1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LueBreakDown);
            this.splitContainer1.Panel2.Controls.Add(this.LueWOActivity);
            this.splitContainer1.Panel2.Controls.Add(this.TmeTm);
            this.splitContainer1.Panel2.Controls.Add(this.DteDt);
            this.splitContainer1.Panel2.Controls.Add(this.Grd2);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFixedInd);
            this.splitContainer1.Size = new System.Drawing.Size(877, 340);
            this.splitContainer1.SplitterDistance = 122;
            this.splitContainer1.TabIndex = 1;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(877, 122);
            this.Grd1.TabIndex = 53;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            // 
            // LueBreakDown
            // 
            this.LueBreakDown.EnterMoveNextControl = true;
            this.LueBreakDown.Location = new System.Drawing.Point(295, 116);
            this.LueBreakDown.Margin = new System.Windows.Forms.Padding(5);
            this.LueBreakDown.Name = "LueBreakDown";
            this.LueBreakDown.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBreakDown.Properties.Appearance.Options.UseFont = true;
            this.LueBreakDown.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBreakDown.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBreakDown.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBreakDown.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBreakDown.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBreakDown.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBreakDown.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBreakDown.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBreakDown.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBreakDown.Properties.DropDownRows = 12;
            this.LueBreakDown.Properties.NullText = "[Empty]";
            this.LueBreakDown.Properties.PopupWidth = 350;
            this.LueBreakDown.Size = new System.Drawing.Size(182, 20);
            this.LueBreakDown.TabIndex = 59;
            this.LueBreakDown.ToolTip = "F4 : Show/hide list";
            this.LueBreakDown.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBreakDown.EditValueChanged += new System.EventHandler(this.LueBreakDown_EditValueChanged);
            this.LueBreakDown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBreakDown_KeyDown);
            this.LueBreakDown.Leave += new System.EventHandler(this.LueBreakDown_Leave);
            // 
            // LueWOActivity
            // 
            this.LueWOActivity.EnterMoveNextControl = true;
            this.LueWOActivity.Location = new System.Drawing.Point(111, 64);
            this.LueWOActivity.Margin = new System.Windows.Forms.Padding(5);
            this.LueWOActivity.Name = "LueWOActivity";
            this.LueWOActivity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWOActivity.Properties.Appearance.Options.UseFont = true;
            this.LueWOActivity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWOActivity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWOActivity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWOActivity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWOActivity.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWOActivity.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWOActivity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWOActivity.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWOActivity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWOActivity.Properties.DropDownRows = 12;
            this.LueWOActivity.Properties.NullText = "[Empty]";
            this.LueWOActivity.Properties.PopupWidth = 350;
            this.LueWOActivity.Size = new System.Drawing.Size(182, 20);
            this.LueWOActivity.TabIndex = 56;
            this.LueWOActivity.ToolTip = "F4 : Show/hide list";
            this.LueWOActivity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWOActivity.EditValueChanged += new System.EventHandler(this.LueWOActivity_EditValueChanged);
            this.LueWOActivity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueWOActivity_KeyDown);
            this.LueWOActivity.Leave += new System.EventHandler(this.LueWOActivity_Leave);
            // 
            // TmeTm
            // 
            this.TmeTm.EditValue = null;
            this.TmeTm.EnterMoveNextControl = true;
            this.TmeTm.Location = new System.Drawing.Point(449, 65);
            this.TmeTm.Name = "TmeTm";
            this.TmeTm.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TmeTm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeTm.Properties.Appearance.Options.UseFont = true;
            this.TmeTm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeTm.Properties.Mask.EditMask = "HH:mm";
            this.TmeTm.Size = new System.Drawing.Size(88, 20);
            this.TmeTm.TabIndex = 58;
            this.TmeTm.Visible = false;
            this.TmeTm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TmeTm_KeyDown);
            this.TmeTm.Leave += new System.EventHandler(this.TmeTm_Leave);
            // 
            // DteDt
            // 
            this.DteDt.EditValue = null;
            this.DteDt.EnterMoveNextControl = true;
            this.DteDt.Location = new System.Drawing.Point(303, 65);
            this.DteDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDt.Name = "DteDt";
            this.DteDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDt.Properties.Appearance.Options.UseFont = true;
            this.DteDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDt.Size = new System.Drawing.Size(138, 20);
            this.DteDt.TabIndex = 57;
            this.DteDt.Visible = false;
            this.DteDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDt_KeyDown);
            this.DteDt.Leave += new System.EventHandler(this.DteDt_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 22);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(877, 192);
            this.Grd2.TabIndex = 55;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // ChkFixedInd
            // 
            this.ChkFixedInd.Dock = System.Windows.Forms.DockStyle.Top;
            this.ChkFixedInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkFixedInd.Location = new System.Drawing.Point(0, 0);
            this.ChkFixedInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkFixedInd.Name = "ChkFixedInd";
            this.ChkFixedInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkFixedInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFixedInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkFixedInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkFixedInd.Properties.Appearance.Options.UseFont = true;
            this.ChkFixedInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkFixedInd.Properties.Caption = "Fixed";
            this.ChkFixedInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFixedInd.Size = new System.Drawing.Size(877, 22);
            this.ChkFixedInd.TabIndex = 54;
            // 
            // BtnRPL
            // 
            this.BtnRPL.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRPL.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRPL.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRPL.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRPL.Appearance.Options.UseBackColor = true;
            this.BtnRPL.Appearance.Options.UseFont = true;
            this.BtnRPL.Appearance.Options.UseForeColor = true;
            this.BtnRPL.Appearance.Options.UseTextOptions = true;
            this.BtnRPL.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRPL.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRPL.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnRPL.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnRPL.Location = new System.Drawing.Point(0, 217);
            this.BtnRPL.Name = "BtnRPL";
            this.BtnRPL.Size = new System.Drawing.Size(70, 31);
            this.BtnRPL.TabIndex = 9;
            this.BtnRPL.Text = "  &RPL ";
            this.BtnRPL.ToolTip = "Go to Request Part List";
            this.BtnRPL.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRPL.ToolTipTitle = "Run System";
            this.BtnRPL.Click += new System.EventHandler(this.BtnRPL_Click);
            // 
            // FrmWO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 540);
            this.Name = "FrmWO";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTOCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEquipmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSymptomProblem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaintenanceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecvDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDODeptDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOReqDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHoursMeter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSettleInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWORDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBreakDown.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWOActivity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeTm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFixedInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.MemoExEdit MeeDescription;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.SimpleButton BtnWORDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtWORDocNo;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnWORDocNo;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LblSymptom;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDt;
        internal DevExpress.XtraEditors.TimeEdit TmeTm;
        private DevExpress.XtraEditors.CheckEdit ChkSettleInd;
        private DevExpress.XtraEditors.LookUpEdit LueWOActivity;
        private System.Windows.Forms.Label label102;
        public DevExpress.XtraEditors.TextEdit TxtHoursMeter;
        private DevExpress.XtraEditors.LookUpEdit LueBreakDown;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtSite;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtMRDocNo;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtRecvDocNo;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtDODeptDocNo;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtDOReqDocNo;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.SimpleButton BtnRecv;
        public DevExpress.XtraEditors.SimpleButton BtnDODept;
        public DevExpress.XtraEditors.SimpleButton BtnDOReq;
        public DevExpress.XtraEditors.SimpleButton BtnMR;
        protected internal DevExpress.XtraEditors.LookUpEdit LueMaintenanceType;
        protected internal DevExpress.XtraEditors.LookUpEdit LueSymptomProblem;
        internal DevExpress.XtraEditors.TextEdit TxtTOCode;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton BtnTOCode;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtAssetName;
        protected DevExpress.XtraEditors.SimpleButton BtnRPL;
        private DevExpress.XtraEditors.CheckEdit ChkFixedInd;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtDisplayName;
        protected internal DevExpress.XtraEditors.LookUpEdit LueEquipmentCode;
        private System.Windows.Forms.Label LblEquipmentCode;
    }
}