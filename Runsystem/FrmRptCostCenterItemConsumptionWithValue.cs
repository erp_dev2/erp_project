﻿/*
    25/07/2019 [TKG] amount dikali dengan rate
*/ 
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCostCenterItemConsumptionWithValue : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        internal bool mIsShowForeignName = false;
        private bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptCostCenterItemConsumptionWithValue(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueCCCode(ref LueCCCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);

                iGCellStyle myPercentBarStyle = new iGCellStyle();
                myPercentBarStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
                myPercentBarStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)((TenTec.Windows.iGridLib.iGCellFlags.DisplayText | TenTec.Windows.iGridLib.iGCellFlags.DisplayImage)));
                Grd1.Cols[19].CellStyle = myPercentBarStyle;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select H.CCName As CCName0, G.CCName As CCName1, F.CCName As CCName2, C.CCName, T1.DocNo, T1.DocDt, T1.ItCode, D.ItName, T1.Lot, T1.Bin, SUM(T1.Qty) As Qty, SUM(Amount) As Amount, T1.CurCode, ");
            SQL.AppendLine("SUM(Qty2) As Qty2, SUM(Qty3) As Qty3, D.InventoryUomCode, D.InventoryUomCode2, D.InventoryUomCode3, T1.CCCode, D.ForeignName, J.EntName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.CCCode, A.DocNo, A.DocDt, A.DeptCode, B.ItCode, B.Lot, ");
            SQL.AppendLine("B.Bin, B.Source, B.Qty, F.UPrice, ");
            SQL.AppendLine("B.Qty*F.UPrice*F.ExcRate As Amount, B.Qty2, B.Qty3, F.CurCode ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblStockPrice F On B.Source=F.Source ");
            SQL.AppendLine("Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Group By A.DocNo, B.DNo, B.ItCode, B.BatchNo, B.Source ");
            SQL.AppendLine(")T1 ");
            SQL.AppendLine("Inner Join TblCostCenter C On T1.CCCode = C.CCCode ");
            SQL.AppendLine("Inner Join TblItem D On T1.ItCode=D.ItCode  ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment E On T1.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblCostCenter F On C.Parent = F.CCCode ");
            SQL.AppendLine("Left Join TblCostCenter G On F.Parent = G.CCCode ");
            SQL.AppendLine("Left Join TblCostCenter H On G.Parent = H.CCCode ");
            SQL.AppendLine("LEFT JOIN TblProfitCenter I ON C.ProfitCenterCode = I.ProfitCenterCode ");
            SQL.AppendLine("LEFT JOIN TblEntity J ON I.EntCode = J.EntCode ");
            SQL.AppendLine("Group By H.CCName, G.CCName, F.CCName, T1.CCCode, T1.ItCode, T1.DocNo, T1.DocDt, D.ItName, T1.Lot, T1.Bin, T1.CurCode, D.InventoryUomCode Order By T1.Qty Desc ");
            SQL.AppendLine(") Y1 Where DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Cost Center",
                        "Cost Center",
                        "Cost Center",
                        "Cost Center",
                        "Document#",
                        //6-10
                        "Date",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",                      
                        "Lot",
                        "Bin",
                        //11-15
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        //16-20
                        "UoM",
                        "Amount",
                        "Currency",
                        "Percentage",
                        "Foreign Name",

                        //21
                        "Entity Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 100, 100, 100, 150,   
                        
                        //6-10
                        100, 100, 250, 80, 80,   
                        
                        //11-15
                        100, 80, 100, 80, 100,  
                        
                        //16-20
                        80, 100, 80, 100, 170,

                        //21
                        180
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 17 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Grd1.Cols[21].Move(5);
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 7, 13, 14, 15, 18 },false);
                Grd1.Cols[20].Move(9);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 7, 13, 14, 15, 18, 20 }, false);
            }
           
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0";
                decimal TotalAmt = 0;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName", "ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "CCCode", true);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 13, 15, 17 });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "CCName0",  
                            
                            //1-5
                            "CCName1", "CCName2", "CCName", "DocNo", "DocDt",    
                            
                            //6-10
                            "ItCode", "ItName",  "Lot", "Bin", "Qty",   
                            
                            //11-15
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                            
                            //16-19
                            "Amount", "CurCode", "ForeignName", "EntName"
                 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 19);
                            TotalAmt = TotalAmt + Sm.DrDec(dr, 11);
                        }, true, false, false, false
                    );
               
                //Grd1.Rows.CollapseAll();
                for (int intX = 0; intX < Grd1.Rows.Count; intX++)
                {
                    if (Sm.GetGrdDec(Grd1, intX, 11) != 0)
                    Grd1.Cells[intX, 19].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, intX, 17) / TotalAmt);
                }
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(3);
                Grd1.GroupObject.Add(4);
                Grd1.Group();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }
        
        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_CustomDrawCellForeground_1(object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex == 19)
            {
                object myObjValue = Grd1.Cells[e.RowIndex, e.ColIndex].Value;
                if (myObjValue == null)
                    return;

                Rectangle myBounds = e.Bounds;
                myBounds.Inflate(-2, -2);
                myBounds.Width = myBounds.Width - 1;
                myBounds.Height = myBounds.Height - 1;
                if (myBounds.Width > 0)
                {
                    e.Graphics.FillRectangle(Brushes.Bisque, myBounds);
                    double myValue = (double)myObjValue;
                    int myWidth = (int)(myBounds.Width * myValue);
                    e.Graphics.FillRectangle(Brushes.SandyBrown, myBounds.X, myBounds.Y, myWidth, myBounds.Height);

                    e.Graphics.DrawRectangle(Pens.SaddleBrown, myBounds);

                    StringFormat myStringFormat = new StringFormat();
                    myStringFormat.Alignment = StringAlignment.Center;
                    myStringFormat.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(string.Format("{0:F2}%", myValue * 100), Font, SystemBrushes.ControlText, new RectangleF(myBounds.X, myBounds.Y, myBounds.Width, myBounds.Height), myStringFormat);
                }
            }
        }
      
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion


        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion
    }
}
