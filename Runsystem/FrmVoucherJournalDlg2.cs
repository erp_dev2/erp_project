﻿#region Update 
/*
    11/06/2019 [VIN] menambah filter coa alias 
    13/11/2019 [HAR/TWC] Bug Chkbox Account#
    19/01/2020 [TKG/IMS] menambah kolom alias
    03/03/2020 [HAR/KBN] COA bisa dipilih berkali kali berdasarkan parameter IsJournalUseDuplicateCOA
    04/03/2020 [HAR/KBN] Bug filter COA
    19/05/2020 [WED/YK] dibatasi berdasarkan parameter IsCOAFilteredByGroup
    28/10/2021 [RDA/ALL] merubah agar coa journal voucher tidak boleh ambil akun coa yang didaftarkan di cash/bank account kecuali akun coa di cash/bank account yang dipilih di voucher
    17/11/2021 [RDA/PHT] fitur coa journal voucher tetap boleh mengambil akun coa yang didaftarkan di cash/bank account meskipun akun coa sudah dipilih di voucher berdasarkan param IsJournalVoucherShowAllCashBankAccount
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherJournalDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherJournal mFrmParent;
        private string
            mSQL = string.Empty;


        #endregion

        #region Constructor

        public FrmVoucherJournalDlg2(FrmVoucherJournal FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueAcCtCode(ref LueAcCtCode);
                SetVoucherBankAccountAcNo();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Account#", 
                        "Description", 
                        "Type",
                        "Category",

                        //6
                        "Alias"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6 });
            Grd1.Cols[6].Visible = mFrmParent.mIsCOAUseAlias;
            Grd1.Cols[6].Move(4);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcType, C.AcCtName, A.Alias ");
            //SQL.AppendLine("From TblCOA A ");
            //SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
            //SQL.AppendLine("Left Join TblAccountCategory C On Left(A.AcNo, 1)=C.AcCtCode ");
            //SQL.AppendLine("Where A.ActInd='Y' ");
            //SQL.AppendLine("And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            //SQL.AppendLine("And A.Parent Is Not Null ");
            //if (mFrmParent.mIsCOAFilteredByGroup)
            //{
            //    SQL.AppendLine("    And Exists ");
            //    SQL.AppendLine("    ( ");
            //    SQL.AppendLine("        Select 1 ");
            //    SQL.AppendLine("        From TblGroupCOA ");
            //    SQL.AppendLine("        Where GrpCode In ");
            //    SQL.AppendLine("        ( ");
            //    SQL.AppendLine("            Select GrpCode ");
            //    SQL.AppendLine("            From TblUser ");
            //    SQL.AppendLine("            Where UserCode = @UserCode ");
            //    SQL.AppendLine("        ) ");
            //    //SQL.AppendLine("        And AcNo = A.AcNo ");
            //    SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
            //    SQL.AppendLine("    ) ");
            //}
            //SQL.AppendLine("AND A.AcNo NOT IN( ");
            //SQL.AppendLine("    SELECT DISTINCT T1.COAAcNo FROM tblbankaccount T1 ");
            //SQL.AppendLine("    WHERE T1.COAAcNo NOT IN( ");
            //SQL.AppendLine("      SELECT X2.COAAcNo ");
            //SQL.AppendLine("      FROM tblvoucherhdr X1 ");
            //SQL.AppendLine("      INNER JOIN tblbankaccount X2 ON X1.bankaccode = X2.BankAcCode ");
            //SQL.AppendLine("      WHERE X1.DocNo = @DocNo ");
            //SQL.AppendLine("      UNION ALL ");
            //SQL.AppendLine("      SELECT X2.COAAcNo ");
            //SQL.AppendLine("      From tblvoucherhdr X1 ");
            //SQL.AppendLine("      INNER JOIN tblbankaccount X2 ON X1.bankaccode2 = X2.bankaccode ");
            //SQL.AppendLine("      WHERE X1.DocNo = @DocNo ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine(") ");

            if (mFrmParent.mIsJournalVoucherShowAllCashBankAccount)
            {
                SQL.AppendLine("Select A.AcNo, A.AcDesc, B.OptDesc As AcType, C.AcCtName, A.Alias ");
                SQL.AppendLine("From TblCOA A ");
                SQL.AppendLine("Left Join TblOption B On B.OptCat = 'AccountType' And A.AcType=B.OptCode ");
                SQL.AppendLine("Left Join TblAccountCategory C On Left(A.AcNo, 1)=C.AcCtCode ");
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine("And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("And A.Parent Is Not Null ");
            }
            else
            {
                SQL.AppendLine("Select A.AcNo, B.AcDesc, C.OptDesc As AcType, D.AcCtName, B.Alias ");
                SQL.AppendLine("FROM ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT A.AcNo ");
                SQL.AppendLine("    From TblCOA A ");
                SQL.AppendLine("    Where A.ActInd='Y' ");
                SQL.AppendLine("    And A.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("    And A.Parent Is Not Null ");
                SQL.AppendLine("    AND NOT EXISTS (SELECT 1 FROM TblBankAccount WHERE COAAcNo = A.AcNo) ");

                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT B.COAAcNo AcNo ");
                SQL.AppendLine("    FROM TblVoucherHdr A ");
                SQL.AppendLine("    INNER JOIN TblBankAccount B ON A.BankAcCode = B.BankAcCode ");
                SQL.AppendLine("    WHERE A.DocNo = @DocNo ");

                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("    SELECT B.COAAcNo AcNo ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    INNER JOIN TblBankAccount B ON A.BankAcCode2 = B.BankAcCode ");
                SQL.AppendLine("    WHERE A.DocNo = @DocNo ");
                SQL.AppendLine(")A ");
                SQL.AppendLine("INNER JOIN TblCOA B ON A.AcNo = B.AcNo ");
                SQL.AppendLine("Left Join TblOption C On C.OptCat = 'AccountType' And B.AcType=C.OptCode ");
                SQL.AppendLine("Left Join TblAccountCategory D On Left(A.AcNo, 1)=D.AcCtCode ");
                SQL.AppendLine("Where B.ActInd='Y' ");
                SQL.AppendLine("And B.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("And B.Parent Is Not Null ");
            }
            if (mFrmParent.mIsCOAFilteredByGroup)
            {
                SQL.AppendLine("    And Exists ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblGroupCOA ");
                SQL.AppendLine("        Where GrpCode In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select GrpCode ");
                SQL.AppendLine("            From TblUser ");
                SQL.AppendLine("            Where UserCode = @UserCode ");
                SQL.AppendLine("        ) ");
                //SQL.AppendLine("        And AcNo = A.AcNo ");
                SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
                SQL.AppendLine("    ) ");
            }


            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                if (!mFrmParent.mIsJournalUseDuplicateCOA)
                    Filter = " And A.AcNo Not In (" + mFrmParent.GetSelectedJournal() + ") ";
                else
                    Filter = "And 0=0 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, "A.AcNo", false);

                if (mFrmParent.mIsJournalVoucherShowAllCashBankAccount) 
                {
                    Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "A.AcDesc", false);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "C.AcCtCode", true);
                    Sm.FilterStr(ref Filter, ref cm, TxtCOAAlias.Text, "A.Alias", false);
                }
                else
                {
                    Sm.FilterStr(ref Filter, ref cm, TxtAcDesc.Text, "B.AcDesc", false);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueAcCtCode), "D.AcCtCode", true);
                    Sm.FilterStr(ref Filter, ref cm, TxtCOAAlias.Text, "B.Alias", false);
                    Sm.CmParam<String>(ref cm, "@DocNo", mFrmParent.TxtVoucherDocNo.Text);
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By AcNo;",
                        new string[] 
                        { 
                            //0
                            "AcNo",
 
                            //1-4
                            "AcDesc", "AcType", "AcCtName", "Alias"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        mFrmParent.Grd1.Cells[Row1, 3].Value = 0;
                        mFrmParent.Grd1.Cells[Row1, 4].Value = 0;

                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 3].Value = 0;
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 4].Value = 0;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            if (mFrmParent.mIsJournalUseDuplicateCOA) return false;

            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #region Additional Method

        private void SetVoucherBankAccountAcNo()
        {
            if (mFrmParent.Grd1.Rows.Count > 0 && 
                Sm.GetGrdStr(mFrmParent.Grd1, 0, 1).Length > 0) 
                return;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select COAAcNo ");
            SQL.AppendLine("From TblBankAccount ");
            SQL.AppendLine("Where BankAcCode In ( ");
            SQL.AppendLine("    Select BankAcCode From TblVoucherHdr ");
            SQL.AppendLine("    Where DocNo=@Param ");
            SQL.AppendLine(");");

            var AcNo = Sm.GetValue(SQL.ToString(), mFrmParent.TxtVoucherDocNo.Text);

            if (AcNo.Length > 0)
            {
                TxtAcNo.EditValue = AcNo;
                ChkCOAAlias.Checked = true;
                ShowData();
                if (Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 2).Length > 0)
                    Grd1.Cells[0, 1].Value = true;
            }
        }

        #endregion

        #endregion

        #endregion

        #endregion

        #region Event

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender); 
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account#");
        }

        private void TxtAcDesc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcDesc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Description");
        }

        private void LueAcCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcCtCode, new Sm.RefreshLue1(Sl.SetLueAcCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkAcCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Category");
        }

        private void TxtCOAAlias_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCOAAlias_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA Alias");
        }

        #endregion
       
    }
}
