﻿#region Update
/*
    10/03/2018 [TKG] tambah import PO.
    29/05/2018 [TKG] tambah status PO.
    07/02/2019 [DITA] Tambah kolom property
    07/11/2019 [DITA/IMS] tambah informasi Specifications
    28/11/2019 [WED/IMS] tambah kolom project code, project name
    27/05/2020 [TKG/IMS] tambah filter Local Document#, Item's Local Code
    03/09/2020 [WED/IMS] Project Code dan Project Name ganti link query
    19/01/2021 [VIN/IMS] menampilkan informasi item berdasarkan parameter IsBOMShowSpecifications
    18/03/2021 [VIN/IMS] menampilkan informasi project code, project name berdasarkan MR
    31/03/2021 [IBL/SIER] tambah kolom contract berdasarkan parameter IsPOUseContract
    14/06/2021 [RDH/IMS] Menambah ceklist di header untuk meng excludekan dokumen PO hasil dr PO For Service
    09/08/2021 [WED/PADI] tambah informasi Take Order berdasarkan parameter IsUseECatalog
    25/10/2021 [WED/RM] Informasi Take Order tidak jadi dipakai, karena dipakainya jadinya di PO Request
    28/12/2021 [RDA/PHT] Support : penyesuaian data kolom site ketika param IsSiteMandatory dan IsFilterBySite aktif
    07/10/2022 [RDA/SIER] vendor yang muncul terfilter sesuai grup berdasarkan parameter IsFilterByVendorCategory
    12/04/2023 [ISN/SIER] penyesuaian filter vendor yang muncul terfilter sesuai grup berdasarkan parameter IsFilterByVendorCategory
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPOFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPO mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPOFind(FrmPO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode, mFrmParent.mIsFilterByVendorCategory ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySite?"Y":"N");
                ChkExcludedCancelledItem.Checked = true;
                ChkExcludePOService.Checked = true;
                if (!mFrmParent.mIsPOFindUseExlucedPOService)
                {
                    label5.Visible = false;
                    ChkExcludePOService.Visible = false; 
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.LocalDocNo, A.ImportInd, ");
            if (mFrmParent.mIsSiteMandatory)
                SQL.AppendLine("L.SiteName, ");
            else
                SQL.AppendLine("Null As SiteName, ");
            SQL.AppendLine("B.PORequestDocNo, F.ItCode, B.Qty, G.PurchaseUomCode, ");
            SQL.AppendLine("G.ItName, G.ForeignName, ");
            SQL.AppendLine("J.VdName, K.DeptName, H.CurCode, I.UPrice, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt, ");
            SQL.AppendLine("G.ItCodeInternal, M.PropName, G.Specification, ");

            //if (mFrmParent.mIsUseECatalog)
            //    SQL.AppendLine("Case B.TakeOrderInd When 'Y' Then 'Yes' When 'N' Then 'No' Else '' End As TakeOrderInd, ");
            //else
            //    SQL.AppendLine("Null As TakeOrderInd, ");

            if (mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("E.PGCode ProjectCode, R.PGName ProjectName, ");
            else
                SQL.AppendLine("Null As ProjectCode, Null As ProjectName, ");
            if (mFrmParent.mIsPOUseContract)
                SQL.AppendLine("S.OptDesc As Contract ");
            else
                SQL.AppendLine("Null As Contract ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (E.SiteCode Is Null Or ( ");
                SQL.AppendLine("    E.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblItem G On F.ItCode=G.ItCode ");
            SQL.AppendLine("Inner Join TblQtHdr H On D.QtDocNo=H.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("    And (H.PtCode Is Null Or ( ");
                SQL.AppendLine("    And H.PtCode Is Not Null ");
                SQL.AppendLine("    And H.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl I On D.QtDocNo=I.DocNo And D.QtDNo=I.DNo ");
            SQL.AppendLine("Inner Join TblVendor J On H.VdCode=J.VdCode ");
            if (mFrmParent.mIsFilterByVendorCategory)
            {
                SQL.AppendLine("And (H.VdCode Is Null Or (H.VdCode Is Not Null ");
                SQL.AppendLine("And EXISTS ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("	SELECT 1 ");
                SQL.AppendLine("	FROM TblGroupVendorCategory ");
                SQL.AppendLine("	WHERE VdCtCode = J.VdCtCode  ");
                SQL.AppendLine("	AND GrpCode IN ");
                SQL.AppendLine("	(  ");
                SQL.AppendLine("		SELECT GrpCode FROM tbluser  ");
                SQL.AppendLine("		WHERE UserCode = @UserCode  ");
                SQL.AppendLine("	)  ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Inner Join TblDepartment K On E.DeptCode=K.DeptCode ");
            if (mFrmParent.mIsSiteMandatory)
            {
                SQL.AppendLine("Left Join TblSite L On A.SiteCode=L.SiteCode ");
                if (mFrmParent.mIsFilterBySite)
                {
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(L.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                }
            }
            SQL.AppendLine("Left Join TblProperty M On D.PropCode=M.PropCode ");
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                #region Old Code
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select T1.DocNo, T6.DNo, Group_Concat(Distinct IfNull(T4.ProjectCode, T5.ProjectCode2)) ProjectCode, Group_Concat(Distinct IfNull(T4.ProjectName, T3.ProjectName)) ProjectName ");
                //SQL.AppendLine("    From TblBOMRevisionHdr T1 ");
                //SQL.AppendLine("    Inner Join TblBOQHdr T2 On T1.BOQDocNo = T2.DocNo ");
                //SQL.AppendLine("    Inner Join TblLOPHdr T3 On T2.LOPDocNo ");
                //SQL.AppendLine("    Left Join TblProjectGroup T4 On T3.PGCode = T4.PGCode ");
                //SQL.AppendLine("    Left Join TblSOContractHdr T5 On T2.DocNo = T5.BOQDocNo ");
                //SQL.AppendLine("    Inner JOin TblBOMRevisionDtl T6 On T1.DocNo = T6.DOcNo ");
                //SQL.AppendLine("    Inner Join TblMaterialRequestDtl T7 On T1.DocNo = T7.BOMRDocNo And T6.DNo = T7.BOMRDNo ");
                //SQL.AppendLine("    Group By T1.DocNo, T6.DNo ");
                //SQL.AppendLine(") Q On F.BOMRDocNo = Q.DocNo And F.BOMRDNo = Q.DNo ");
                #endregion

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T31.DocNo, T31.DNo, Group_Concat(Distinct IfNull(T7.ProjectCode, T4.ProjectCode2)) ProjectCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T7.ProjectName, T6.ProjectName)) ProjectName ");
                SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
                SQL.AppendLine("    Inner join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.SOCDocNo Is not Null ");
                SQL.AppendLine("    Inner join TblPORequestDtl T3 On T2.DocNo = T3.MaterialRequestDocNo ");
                SQL.AppendLine("        And T2.DNo = T3.MaterialRequestDNo ");
                SQL.AppendLine("    Inner join TblPODtl T31 On T31.PORequestDocNo = T3.DocNo And T31.PORequestDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T4 On T1.SOCDocNo = T4.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T5 On T4.BOQDocNo = T5.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T6 On T5.LOPDocNo = T6.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T7 On T6.PGCode = T7.PGCode ");
                SQL.AppendLine("    Group By T3.DocNo, T3.DNo ");
                SQL.AppendLine(") Q On B.DocNo = Q.DocNo And B.DNo = Q.DNo ");
                SQL.AppendLine("Left Join tblprojectgroup R ON E.PGCode=R.PGCode ");
            }
            if (mFrmParent.mIsPOUseContract)
                SQL.AppendLine("Left Join TblOption S On A.Contract=S.OptCode And S.OptCat = 'POContract' ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Import",
                    
                    //6-10
                    "Local#",
                    "Site",
                    "PO Request#",
                    "Department", 
                    "Item's"+Environment.NewLine+"Code",
                    
                    //11-15
                    "Item's Name",
                    "Local Code",
                    "Foreign Name",
                    "Quantity",
                    "UoM",
                    
                    //16-20
                    "Vendor",
                    "Currency",
                    "Price",
                    "Created By",
                    "Created Date", 
                    
                    //21-25
                    "Created Time", 
                    "Last Updated By", 
                    "Last Updated Date",
                    "Last Updated Time",
                    "Property",

                    //26-29
                    "Specification",
                    "Project Code",
                    "Project Name",
                    "Contract",
                    //"Take Order"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 80, 60, 100, 60, 
                    
                    //6-10
                    150, 150, 150, 150, 80, 
                    
                    //11-15
                    200, 100, 200, 80, 60, 
                    
                    //16-20
                    200, 60, 120, 130, 130, 
                    
                    //21-25
                    130, 130, 130, 130, 150,

                    //26-29
                    300, 100, 200, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 10, 12, 19, 20, 21, 22, 23, 24 }, false);
            //if (!mFrmParent.mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 30 });
            //else Grd1.Cols[30].Move(19);

            if (mFrmParent.mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 7 }, true);
            Sm.SetGrdProperty(Grd1, false);
            if (!mFrmParent.mPORequestPropCodeEnabled) Sm.GrdColInvisible(Grd1, new int[] { 25 }, false);
            if (!mFrmParent.mIsPOUseContract) Sm.GrdColInvisible(Grd1, new int[] { 29 }, false);

            Grd1.Cols[25].Move(14);
            Grd1.Cols[26].Move(13);
            Grd1.Cols[29].Move(3);

            if (!mFrmParent.mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 28 });
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] {12, 26, 27, 28 }, true);
                Grd1.Cols[12].Move(10);
            }

            
        }
        override protected void HideInfoInGrd()
        {
            if(!mFrmParent.mIsBOMShowSpecifications) 
                Sm.GrdColInvisible(Grd1, new int[] { 10, 12, 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 10, 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);

        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter = " And A.Status In ('O', 'A') And B.CancelInd='N' ";


                if (mFrmParent.mIsPOFindUseExlucedPOService && ChkExcludePOService.Checked)
                    Filter = Filter + " AND F.MaterialRequestServiceDocNo IS NULL ";
                
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "H.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "F.ItCode", "G.ItName", "G.ForeignName", "G.ItCodeInternal" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {

                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "ImportInd", "LocalDocNo", 
                        
                        //6-10
                        "SiteName", "PORequestDocNo", "DeptName", "ItCode", "ItName", 
                        
                        //11-15
                        "ItCodeInternal", "ForeignName", "Qty", "PurchaseUomCode", "VdName", 
                        
                        //16-20
                        "CurCode", "UPrice", "CreateBy", "CreateDt", "LastUpBy", 
                        
                        //21-25
                        "LastUpDt", "PropName", "Specification", "ProjectCode", "ProjectName",

                        //26-27
                        "Contract", /*"TakeOrderInd"*/
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                        //Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Event

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode), mFrmParent.mIsFilterByVendorCategory ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySite?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
