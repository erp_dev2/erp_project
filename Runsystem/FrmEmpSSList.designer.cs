﻿namespace RunSystem
{
    partial class FrmEmpSSList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LueSSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.LueMth = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmployer = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtEmployee = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteCutOffDt = new DevExpress.XtraEditors.DateEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnProcess = new DevExpress.XtraEditors.SimpleButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueEmploymentStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.ChkEmploymentStatus = new DevExpress.XtraEditors.CheckEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCutOffDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCutOffDt.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEmploymentStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.Tp2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(772, 292);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 292);
            this.panel3.Size = new System.Drawing.Size(772, 181);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.LayoutObject.Flags = ((TenTec.Windows.iGridLib.iGLayoutFlags)((((TenTec.Windows.iGridLib.iGLayoutFlags.Sorting | TenTec.Windows.iGridLib.iGLayoutFlags.ColVisibility)
                        | TenTec.Windows.iGridLib.iGLayoutFlags.ColWidth)
                        | TenTec.Windows.iGridLib.iGLayoutFlags.ColOrder)));
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.None;
            this.Grd1.SingleClickEdit = false;
            this.Grd1.Size = new System.Drawing.Size(772, 181);
            this.Grd1.TabIndex = 37;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.AfterContentsSorted += new System.EventHandler(this.Grd1_AfterContentsSorted);
            this.Grd1.BeforeContentsSorted += new System.EventHandler(this.Grd1_BeforeContentsSorted);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.Visible = true;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(194, 218);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Properties.ReadOnly = true;
            this.TxtTotal.Size = new System.Drawing.Size(180, 20);
            this.TxtTotal.TabIndex = 32;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(194, 239);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(359, 20);
            this.MeeRemark.TabIndex = 34;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(143, 242);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 33;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(378, 6);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.ShowToolTips = false;
            this.ChkCancelInd.Size = new System.Drawing.Size(73, 22);
            this.ChkCancelInd.TabIndex = 12;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(194, 28);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(157, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(194, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(180, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(117, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(155, 221);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 14);
            this.label9.TabIndex = 31;
            this.label9.Text = "Total";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSSPCode
            // 
            this.LueSSPCode.EnterMoveNextControl = true;
            this.LueSSPCode.Location = new System.Drawing.Point(194, 91);
            this.LueSSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSSPCode.Name = "LueSSPCode";
            this.LueSSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSSPCode.Properties.DropDownRows = 30;
            this.LueSSPCode.Properties.NullText = "[Empty]";
            this.LueSSPCode.Properties.PopupWidth = 300;
            this.LueSSPCode.Size = new System.Drawing.Size(286, 20);
            this.LueSSPCode.TabIndex = 20;
            this.LueSSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSSPCode.EditValueChanged += new System.EventHandler(this.LueSSPCode_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(56, 95);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(134, 14);
            this.label12.TabIndex = 19;
            this.label12.Text = "Social Security Program";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(158, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Year";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(194, 49);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 500;
            this.LueYr.Size = new System.Drawing.Size(67, 20);
            this.LueYr.TabIndex = 16;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LueMth
            // 
            this.LueMth.EnterMoveNextControl = true;
            this.LueMth.Location = new System.Drawing.Point(194, 70);
            this.LueMth.Margin = new System.Windows.Forms.Padding(5);
            this.LueMth.Name = "LueMth";
            this.LueMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.Appearance.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMth.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMth.Properties.DropDownRows = 13;
            this.LueMth.Properties.NullText = "[Empty]";
            this.LueMth.Properties.PopupWidth = 500;
            this.LueMth.Size = new System.Drawing.Size(67, 20);
            this.LueMth.TabIndex = 18;
            this.LueMth.ToolTip = "F4 : Show/hide list";
            this.LueMth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(148, 74);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 14);
            this.label6.TabIndex = 17;
            this.label6.Text = "Month";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmployer
            // 
            this.TxtEmployer.EnterMoveNextControl = true;
            this.TxtEmployer.Location = new System.Drawing.Point(194, 176);
            this.TxtEmployer.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmployer.Name = "TxtEmployer";
            this.TxtEmployer.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtEmployer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmployer.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmployer.Properties.Appearance.Options.UseFont = true;
            this.TxtEmployer.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEmployer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEmployer.Properties.ReadOnly = true;
            this.TxtEmployer.Size = new System.Drawing.Size(180, 20);
            this.TxtEmployer.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(133, 179);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 14);
            this.label4.TabIndex = 27;
            this.label4.Text = "Employer";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmployee
            // 
            this.TxtEmployee.EnterMoveNextControl = true;
            this.TxtEmployee.Location = new System.Drawing.Point(194, 197);
            this.TxtEmployee.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmployee.Name = "TxtEmployee";
            this.TxtEmployee.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtEmployee.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmployee.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmployee.Properties.Appearance.Options.UseFont = true;
            this.TxtEmployee.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEmployee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEmployee.Properties.ReadOnly = true;
            this.TxtEmployee.Size = new System.Drawing.Size(180, 20);
            this.TxtEmployee.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(130, 200);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 14);
            this.label5.TabIndex = 29;
            this.label5.Text = "Employee";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteCutOffDt
            // 
            this.DteCutOffDt.EditValue = null;
            this.DteCutOffDt.EnterMoveNextControl = true;
            this.DteCutOffDt.Location = new System.Drawing.Point(194, 112);
            this.DteCutOffDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteCutOffDt.Name = "DteCutOffDt";
            this.DteCutOffDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCutOffDt.Properties.Appearance.Options.UseFont = true;
            this.DteCutOffDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteCutOffDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteCutOffDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteCutOffDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteCutOffDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCutOffDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteCutOffDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteCutOffDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteCutOffDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteCutOffDt.Size = new System.Drawing.Size(120, 20);
            this.DteCutOffDt.TabIndex = 22;
            this.DteCutOffDt.EditValueChanged += new System.EventHandler(this.DteCutOffDt_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(113, 116);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 21;
            this.label7.Text = "Cut-Off Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnProcess.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BtnProcess.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnProcess.Location = new System.Drawing.Point(0, 271);
            this.BtnProcess.Name = "BtnProcess";
            this.BtnProcess.Size = new System.Drawing.Size(92, 21);
            this.BtnProcess.TabIndex = 36;
            this.BtnProcess.Text = "Process Data";
            this.BtnProcess.ToolTip = "Process Data";
            this.BtnProcess.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnProcess.ToolTipTitle = "Run System";
            this.BtnProcess.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.BtnProcess);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(680, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(92, 292);
            this.panel4.TabIndex = 35;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(162, 136);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 23;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(194, 133);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(286, 20);
            this.LueSiteCode.TabIndex = 24;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(7, 157);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(183, 14);
            this.LblEntCode.TabIndex = 25;
            this.LblEntCode.Text = "Registered Social Security Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(194, 154);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.MaxLength = 16;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 300;
            this.LueEntCode.Size = new System.Drawing.Size(286, 20);
            this.LueEntCode.TabIndex = 26;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // LueEmploymentStatus
            // 
            this.LueEmploymentStatus.EnterMoveNextControl = true;
            this.LueEmploymentStatus.Location = new System.Drawing.Point(133, 13);
            this.LueEmploymentStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmploymentStatus.Name = "LueEmploymentStatus";
            this.LueEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmploymentStatus.Properties.DropDownRows = 30;
            this.LueEmploymentStatus.Properties.MaxLength = 16;
            this.LueEmploymentStatus.Properties.NullText = "[Empty]";
            this.LueEmploymentStatus.Properties.PopupWidth = 200;
            this.LueEmploymentStatus.Size = new System.Drawing.Size(180, 20);
            this.LueEmploymentStatus.TabIndex = 11;
            this.LueEmploymentStatus.ToolTip = "F4 : Show/hide list";
            this.LueEmploymentStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmploymentStatus.EditValueChanged += new System.EventHandler(this.LueEmploymentStatus_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 14);
            this.label10.TabIndex = 10;
            this.label10.Text = "Employment Status";
            // 
            // ChkEmploymentStatus
            // 
            this.ChkEmploymentStatus.Location = new System.Drawing.Point(317, 12);
            this.ChkEmploymentStatus.Name = "ChkEmploymentStatus";
            this.ChkEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.ChkEmploymentStatus.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkEmploymentStatus.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkEmploymentStatus.Properties.Caption = " ";
            this.ChkEmploymentStatus.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEmploymentStatus.Size = new System.Drawing.Size(19, 22);
            this.ChkEmploymentStatus.TabIndex = 12;
            this.ChkEmploymentStatus.ToolTip = "Remove filter";
            this.ChkEmploymentStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkEmploymentStatus.ToolTipTitle = "Run System";
            this.ChkEmploymentStatus.CheckedChanged += new System.EventHandler(this.ChkEmploymentStatus_CheckedChanged);
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(680, 292);
            this.Tc1.TabIndex = 39;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.ChkCancelInd);
            this.Tp1.Controls.Add(this.TxtDocNo);
            this.Tp1.Controls.Add(this.label9);
            this.Tp1.Controls.Add(this.label7);
            this.Tp1.Controls.Add(this.LueSSPCode);
            this.Tp1.Controls.Add(this.label1);
            this.Tp1.Controls.Add(this.label12);
            this.Tp1.Controls.Add(this.TxtEmployee);
            this.Tp1.Controls.Add(this.label2);
            this.Tp1.Controls.Add(this.DteCutOffDt);
            this.Tp1.Controls.Add(this.label6);
            this.Tp1.Controls.Add(this.LblEntCode);
            this.Tp1.Controls.Add(this.TxtTotal);
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.DteDocDt);
            this.Tp1.Controls.Add(this.MeeRemark);
            this.Tp1.Controls.Add(this.LueMth);
            this.Tp1.Controls.Add(this.LueEntCode);
            this.Tp1.Controls.Add(this.LueSiteCode);
            this.Tp1.Controls.Add(this.label3);
            this.Tp1.Controls.Add(this.TxtEmployer);
            this.Tp1.Controls.Add(this.label4);
            this.Tp1.Controls.Add(this.label8);
            this.Tp1.Controls.Add(this.LblSiteCode);
            this.Tp1.Controls.Add(this.LueYr);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(674, 264);
            this.Tp1.Text = "General";
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.LueEmploymentStatus);
            this.Tp2.Controls.Add(this.label10);
            this.Tp2.Controls.Add(this.ChkEmploymentStatus);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(674, 264);
            this.Tp2.Text = "Filter";
            // 
            // FrmEmpSSList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmEmpSSList";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCutOffDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteCutOffDt.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEmploymentStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            this.Tp2.ResumeLayout(false);
            this.Tp2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.LookUpEdit LueSSPCode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private DevExpress.XtraEditors.LookUpEdit LueMth;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtEmployee;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtEmployer;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteCutOffDt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        public DevExpress.XtraEditors.SimpleButton BtnProcess;
        private System.Windows.Forms.Label LblSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label LblEntCode;
        internal DevExpress.XtraEditors.LookUpEdit LueEntCode;
        private DevExpress.XtraEditors.LookUpEdit LueEmploymentStatus;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.CheckEdit ChkEmploymentStatus;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
    }
}