﻿namespace RunSystem
{
    partial class FrmRptStockMovement4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkWhsCode = new DevExpress.XtraEditors.CheckEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.ChkItCode = new DevExpress.XtraEditors.CheckEdit();
            this.LueItCatCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkItCatCode = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkItName = new DevExpress.XtraEditors.CheckEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtBin = new DevExpress.XtraEditors.TextEdit();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.ChkBin = new DevExpress.XtraEditors.CheckEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtLot = new DevExpress.XtraEditors.TextEdit();
            this.ChkLot = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.ChkShowTotal = new DevExpress.XtraEditors.CheckEdit();
            this.ChkBatchNo = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtBatchNo = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCatCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCatCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkShowTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkWhsCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.ChkItCode);
            this.panel2.Controls.Add(this.LueItCatCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.LueWhsCode);
            this.panel2.Controls.Add(this.ChkItCatCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Size = new System.Drawing.Size(772, 116);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 357);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 116);
            this.panel3.Size = new System.Drawing.Size(772, 357);
            // 
            // ChkWhsCode
            // 
            this.ChkWhsCode.Location = new System.Drawing.Point(375, 24);
            this.ChkWhsCode.Name = "ChkWhsCode";
            this.ChkWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkWhsCode.Properties.Appearance.Options.UseFont = true;
            this.ChkWhsCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkWhsCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkWhsCode.Properties.Caption = " ";
            this.ChkWhsCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkWhsCode.Size = new System.Drawing.Size(19, 22);
            this.ChkWhsCode.TabIndex = 31;
            this.ChkWhsCode.ToolTip = "Remove filter";
            this.ChkWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkWhsCode.ToolTipTitle = "Run System";
            this.ChkWhsCode.CheckedChanged += new System.EventHandler(this.ChkWhsCode_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(27, 28);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Warehouse";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkItCode
            // 
            this.ChkItCode.Location = new System.Drawing.Point(375, 65);
            this.ChkItCode.Name = "ChkItCode";
            this.ChkItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCode.Properties.Caption = " ";
            this.ChkItCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItCode.TabIndex = 40;
            this.ChkItCode.ToolTip = "Remove filter";
            this.ChkItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCode.ToolTipTitle = "Run System";
            this.ChkItCode.CheckedChanged += new System.EventHandler(this.ChkItCode_CheckedChanged);
            // 
            // LueItCatCode
            // 
            this.LueItCatCode.EnterMoveNextControl = true;
            this.LueItCatCode.Location = new System.Drawing.Point(102, 45);
            this.LueItCatCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCatCode.Name = "LueItCatCode";
            this.LueItCatCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCatCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCatCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCatCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCatCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCatCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCatCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCatCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCatCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCatCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCatCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCatCode.Properties.DropDownRows = 30;
            this.LueItCatCode.Properties.NullText = "[Empty]";
            this.LueItCatCode.Properties.PopupWidth = 300;
            this.LueItCatCode.Size = new System.Drawing.Size(268, 20);
            this.LueItCatCode.TabIndex = 36;
            this.LueItCatCode.ToolTip = "F4 : Show/hide list";
            this.LueItCatCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCatCode.EditValueChanged += new System.EventHandler(this.LueItCatCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 38;
            this.label4.Text = "Item\'s Code";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(2, 49);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 14);
            this.label5.TabIndex = 35;
            this.label5.Text = "Item\'s Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(102, 66);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 250;
            this.TxtItCode.Size = new System.Drawing.Size(268, 20);
            this.TxtItCode.TabIndex = 39;
            this.TxtItCode.Validated += new System.EventHandler(this.TxtItCode_Validated);
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(102, 24);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 300;
            this.LueWhsCode.Size = new System.Drawing.Size(268, 20);
            this.LueWhsCode.TabIndex = 30;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // ChkItCatCode
            // 
            this.ChkItCatCode.Location = new System.Drawing.Point(375, 44);
            this.ChkItCatCode.Name = "ChkItCatCode";
            this.ChkItCatCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCatCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCatCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCatCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCatCode.Properties.Caption = " ";
            this.ChkItCatCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCatCode.Size = new System.Drawing.Size(19, 22);
            this.ChkItCatCode.TabIndex = 37;
            this.ChkItCatCode.ToolTip = "Remove filter";
            this.ChkItCatCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCatCode.ToolTipTitle = "Run System";
            this.ChkItCatCode.CheckedChanged += new System.EventHandler(this.ChkItCatCode_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(229, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 27;
            this.label3.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(63, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 25;
            this.label1.Text = "Date";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(247, 3);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(123, 20);
            this.DteDocDt2.TabIndex = 28;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(102, 3);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(123, 20);
            this.DteDocDt1.TabIndex = 26;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkItName);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.TxtBin);
            this.panel4.Controls.Add(this.TxtItName);
            this.panel4.Controls.Add(this.ChkBin);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.TxtLot);
            this.panel4.Controls.Add(this.ChkLot);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.ChkShowTotal);
            this.panel4.Controls.Add(this.ChkBatchNo);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.TxtBatchNo);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(418, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(350, 112);
            this.panel4.TabIndex = 41;
            // 
            // ChkItName
            // 
            this.ChkItName.Location = new System.Drawing.Point(322, 3);
            this.ChkItName.Name = "ChkItName";
            this.ChkItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItName.Properties.Appearance.Options.UseFont = true;
            this.ChkItName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItName.Properties.Caption = " ";
            this.ChkItName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItName.Size = new System.Drawing.Size(19, 22);
            this.ChkItName.TabIndex = 28;
            this.ChkItName.ToolTip = "Remove filter";
            this.ChkItName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItName.ToolTipTitle = "Run System";
            this.ChkItName.CheckedChanged += new System.EventHandler(this.ChkItName_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(59, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 14);
            this.label10.TabIndex = 32;
            this.label10.Text = "Bin";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 14);
            this.label11.TabIndex = 26;
            this.label11.Text = "Item\'s Name";
            // 
            // TxtBin
            // 
            this.TxtBin.EnterMoveNextControl = true;
            this.TxtBin.Location = new System.Drawing.Point(87, 46);
            this.TxtBin.Name = "TxtBin";
            this.TxtBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBin.Properties.Appearance.Options.UseFont = true;
            this.TxtBin.Properties.MaxLength = 40;
            this.TxtBin.Size = new System.Drawing.Size(231, 20);
            this.TxtBin.TabIndex = 33;
            this.TxtBin.Validated += new System.EventHandler(this.TxtBin_Validated);
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(87, 4);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Size = new System.Drawing.Size(231, 20);
            this.TxtItName.TabIndex = 27;
            this.TxtItName.Validated += new System.EventHandler(this.TxtItName_Validated);
            // 
            // ChkBin
            // 
            this.ChkBin.Location = new System.Drawing.Point(322, 45);
            this.ChkBin.Name = "ChkBin";
            this.ChkBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBin.Properties.Appearance.Options.UseFont = true;
            this.ChkBin.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBin.Properties.Caption = " ";
            this.ChkBin.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBin.Size = new System.Drawing.Size(19, 22);
            this.ChkBin.TabIndex = 34;
            this.ChkBin.ToolTip = "Remove filter";
            this.ChkBin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBin.ToolTipTitle = "Run System";
            this.ChkBin.CheckedChanged += new System.EventHandler(this.ChkBin_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(57, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 14);
            this.label9.TabIndex = 29;
            this.label9.Text = "Lot";
            // 
            // TxtLot
            // 
            this.TxtLot.EnterMoveNextControl = true;
            this.TxtLot.Location = new System.Drawing.Point(87, 25);
            this.TxtLot.Name = "TxtLot";
            this.TxtLot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLot.Properties.Appearance.Options.UseFont = true;
            this.TxtLot.Properties.MaxLength = 40;
            this.TxtLot.Size = new System.Drawing.Size(231, 20);
            this.TxtLot.TabIndex = 30;
            this.TxtLot.Validated += new System.EventHandler(this.TxtLot_Validated);
            // 
            // ChkLot
            // 
            this.ChkLot.Location = new System.Drawing.Point(322, 24);
            this.ChkLot.Name = "ChkLot";
            this.ChkLot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLot.Properties.Appearance.Options.UseFont = true;
            this.ChkLot.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLot.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLot.Properties.Caption = " ";
            this.ChkLot.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLot.Size = new System.Drawing.Size(19, 22);
            this.ChkLot.TabIndex = 31;
            this.ChkLot.ToolTip = "Remove filter";
            this.ChkLot.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLot.ToolTipTitle = "Run System";
            this.ChkLot.CheckedChanged += new System.EventHandler(this.ChkLot_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(241, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 38;
            this.label8.Text = "Show Total";
            // 
            // ChkShowTotal
            // 
            this.ChkShowTotal.Location = new System.Drawing.Point(322, 89);
            this.ChkShowTotal.Name = "ChkShowTotal";
            this.ChkShowTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkShowTotal.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ChkShowTotal.Properties.Appearance.Options.UseFont = true;
            this.ChkShowTotal.Properties.Appearance.Options.UseForeColor = true;
            this.ChkShowTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkShowTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkShowTotal.Properties.Caption = "";
            this.ChkShowTotal.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkShowTotal.Size = new System.Drawing.Size(19, 22);
            this.ChkShowTotal.TabIndex = 39;
            this.ChkShowTotal.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkShowTotal.ToolTipTitle = "Run System";
            // 
            // ChkBatchNo
            // 
            this.ChkBatchNo.Location = new System.Drawing.Point(322, 65);
            this.ChkBatchNo.Name = "ChkBatchNo";
            this.ChkBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBatchNo.Properties.Appearance.Options.UseFont = true;
            this.ChkBatchNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBatchNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBatchNo.Properties.Caption = " ";
            this.ChkBatchNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBatchNo.Size = new System.Drawing.Size(19, 22);
            this.ChkBatchNo.TabIndex = 37;
            this.ChkBatchNo.ToolTip = "Remove filter";
            this.ChkBatchNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBatchNo.ToolTipTitle = "Run System";
            this.ChkBatchNo.CheckedChanged += new System.EventHandler(this.ChkBatchNo_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 14);
            this.label2.TabIndex = 35;
            this.label2.Text = "Batch#";
            // 
            // TxtBatchNo
            // 
            this.TxtBatchNo.EnterMoveNextControl = true;
            this.TxtBatchNo.Location = new System.Drawing.Point(87, 67);
            this.TxtBatchNo.Name = "TxtBatchNo";
            this.TxtBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBatchNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBatchNo.Properties.MaxLength = 250;
            this.TxtBatchNo.Size = new System.Drawing.Size(231, 20);
            this.TxtBatchNo.TabIndex = 36;
            this.TxtBatchNo.Validated += new System.EventHandler(this.TxtBatchNo_Validated);
            // 
            // FrmRptStockMovement4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptStockMovement4";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCatCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCatCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkShowTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBatchNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkWhsCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.CheckEdit ChkItCode;
        private DevExpress.XtraEditors.LookUpEdit LueItCatCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtItCode;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private DevExpress.XtraEditors.CheckEdit ChkItCatCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.CheckEdit ChkItName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.TextEdit TxtBin;
        private DevExpress.XtraEditors.TextEdit TxtItName;
        private DevExpress.XtraEditors.CheckEdit ChkBin;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit TxtLot;
        private DevExpress.XtraEditors.CheckEdit ChkLot;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.CheckEdit ChkShowTotal;
        private DevExpress.XtraEditors.CheckEdit ChkBatchNo;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtBatchNo;
    }
}