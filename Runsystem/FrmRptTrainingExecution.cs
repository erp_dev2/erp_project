﻿#region Update
/*
    19/09/2018 [TKG] difilter otorisasi berdasarkan site di grup
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptTrainingExecution : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterByDeptHR = false, mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmRptTrainingExecution(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                SetLueStatus(ref LueStatus);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sm.SetLue(LueStatus, "1");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.TrainingCode, T1.TrainingName, Group_Concat(Distinct T2.Remark Separator '; ') as Objective, T4.EmpCode, T6.EmpName, ");
            SQL.AppendLine("T7.PosName, T8.DeptName, T9.SiteName, Group_Concat(Distinct IfNull(T10.TrainerCode, '') Separator ', ') TrainerCode, ");
            SQL.AppendLine("Group_Concat(Distinct IfNull(T10.TrainerName, '') Separator ', ') TrainerName, ");
            SQL.AppendLine("T4.TrainingSchDt, Group_Concat(Distinct IfNull(Concat(Left(T3.Tm,2), ':', Right(T3.Tm,2)), '') Separator ', ') Tm, ");
            SQL.AppendLine("Sum(IfNull(T10.NoOfHr, 0)) NoOfHr ");
            SQL.AppendLine("From TblTraining T1 ");
            SQL.AppendLine("Inner Join TblTrainingRequestHdr T2 On T1.TrainingCode = T2.TrainingCode And T1.ActiveInd = 'Y' ");
            SQL.AppendLine("Inner Join TblTrainingRequestDtl2 T3 On T2.DocNo = T3.DocNo And T3.Status = 'A' And T3.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblTrainingAssignmentDtl T4 On T3.EmpCode = T4.EmpCode And T2.TrainingCode = T4.TrainingCode ");
            SQL.AppendLine("    And T4.TrainingSchDt Is Not Null ");
            SQL.AppendLine("    And (T4.TrainingSchDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Inner Join TblTrainingAssignmentHdr T5 On T4.DocNo = T5.DocNo And T5.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblEmployee T6 On T4.EmpCode = T6.EmpCode ");
            SQL.AppendLine("Left Join TblPosition T7 On T6.PosCode = T7.PosCode ");
            SQL.AppendLine("Left Join TblDepartment T8 On T6.DeptCode = T8.DeptCode ");
            SQL.AppendLine("Left Join TblSite T9 On T6.SiteCode = T9.SiteCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.DocNo, B.TrainerCode, C.TrainerName, C.NoOfHr ");
	        SQL.AppendLine("    From TblTrainingRequestHdr A ");
	        SQL.AppendLine("    Inner Join TblTrainingRequestDtl B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("    Inner Join TblTrainer C On B.TrainerCode = C.TrainerCode ");
            SQL.AppendLine(") T10 On T2.DocNo = T10.DocNo ");
            SQL.AppendLine("Where 1=1 ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And T1.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(T1.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Training Code",
                    "Learning & Development" + Environment.NewLine + "Program",
                    "Objectives",
                    "Participants" + Environment.NewLine + "(Pax)",
                    "Employee Code",

                    //6-10
                    "Actual Participants",
                    "Position",
                    "Department",
                    "Site",
                    "Trainer Code",

                    //11-15
                    "Trainer(s)",
                    "Date",
                    "Time",
                    "Duration of Session" + Environment.NewLine + "(hour)",
                    "TrainingSchDt"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 180, 180, 100, 100, 
                    
                    //6-10
                    180, 150, 180, 120, 100, 
                    
                    //11-15
                    200, 130, 130, 120, 0
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 10, 15 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                switch (Sm.GetLue(LueStatus))
                {
                    case "1":
                        Filter += " And (T6.ResignDt Is Null Or (T6.ResignDt Is Not Null And T6.ResignDt>@CurrentDate)) ";
                        break;
                    case "2":
                        Filter += " And T6.ResignDt Is Not Null And T6.ResignDt<=@CurrentDate ";
                        break;
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T6.EmpCode", "T6.EmpCodeOld", "T6.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T6.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T6.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Group By T1.TrainingCode, T1.TrainingName, T4.EmpCode, T6.EmpName, T7.PosName, T8.DeptName, T9.SiteName, T4.TrainingSchDt; ",
                    new string[]
                    {
                        //0
                        "TrainingCode", 

                        //1-5
                        "TrainingName", "Objective", "EmpCode", "EmpName", "PosName", 
                        
                        //6-10
                        "DeptName", "SiteName", "TrainerCode", "TrainerName", "TrainingSchDt", 
                        
                        //11-12
                        "Tm", "NoOfHr"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Grd1.Cells[Row, 4].Value = 0m;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);                        
                    }, true, false, false, false
                );
                CalculateParticipants();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 14 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void CalculateParticipants()
        {
            if(Grd1.Rows.Count > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                var l = new List<Pax>();

                SQL.AppendLine("Select T.TrainingCode, T.TrainingSchDt, Count(T.EmpCode) Participants ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select A.TrainingCode, A.TrainingSchDt, A.EmpCode ");
	            SQL.AppendLine("    From TblTrainingAssignmentDtl A ");
	            SQL.AppendLine("    Inner Join TblTrainingAssignmentHdr B On A.DocNo = B.DocNo And B.CancelInd = 'N' ");
	            SQL.AppendLine("        And A.TrainingSchDt Is Not Null ");
	            SQL.AppendLine("        And (A.TrainingSchDt Between @DocDt1 And @DocDt2) ");
	            SQL.AppendLine("    Inner Join TblTrainingRequestDtl2 C On A.EmpCode = C.EmpCode And A.TrainingCode = C.TrainingCode ");
	            SQL.AppendLine("        And C.Status = 'A' And C.CancelInd = 'N' ");
	            SQL.AppendLine("    Inner Join TblTrainingRequestHdr D On C.DocNo = D.DocNo ");
	            SQL.AppendLine("    Group By A.TrainingCode, A.TrainingSchDt, A.EmpCode ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Group By T.TrainingCode, T.TrainingSchDt; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "TrainingCode", "TrainingSchDt", "Participants" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new Pax()
                            {
                                TrainingCode = Sm.DrStr(dr, c[0]),
                                TrainingSchDt = Sm.DrStr(dr, c[1]),
                                Participants = Sm.DrDec(dr, c[2])
                            });
                        }
                    }
                    dr.Close();
                }

                if(l.Count > 0)
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        for (int j = 0; j < Grd1.Rows.Count; j++)
                        {
                            if (l[i].TrainingCode == Sm.GetGrdStr(Grd1, j, 1) && l[i].TrainingSchDt == Sm.GetGrdStr(Grd1, j, 15))
                            {
                                Grd1.Cells[j, 4].Value = l[i].Participants;
                            }
                        }
                    }
                }

                l.Clear();
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Statue");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Class

        private class Pax
        {
            public string TrainingCode { get; set; }
            public string TrainingSchDt { get; set; }
            public decimal Participants { get; set; }
        }

        #endregion
    }
}
