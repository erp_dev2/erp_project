﻿#region Update
/*
    23/04/2020 [DITA/IMS] new apps
    09/04/2021 [VIN/IMS] ardp yang di vr kan tidak ditarik
    23/06/2021 [RDA/IMS] tarik kolom LocalDocNo ketika choose data untuk form parent TxtLocalDocNo
    23/06/2021 [DITA/IMS] outstanding amount menghitung dari dp settlement ampount dikurangin juga ard yg sudah diincoming kan
    25/06/2021 [RDA/IMS] feedback menampilkan kolom Local Document#
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContractDPSettlementDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContractDPSettlement mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSOContractDPSettlementDlg2(FrmSOContractDPSettlement FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -60);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CtCode, B.CtCode, C.Ctname, A.CurCode, ");
            SQL.AppendLine("A.Amt, A.Amt-ifnull(D.Amt, 0.00)-ifnull(E.Amt, 0.00) As OutstandingAmt, A.VoucherRequestDocNo, A.SOContractDocNo, A.LocalDocno ");
            SQL.AppendLine("From TblSOContractDownpaymentHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.SOContractDownpaymentDocNo As DocNo, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblSOContractDPSettlementHdr T1, TblSOContractDownpaymentHdr T2 ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SOContractDownpaymentDocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.CancelInd = 'N' ");
            SQL.AppendLine("    And T2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.SOContractDownpaymentDocNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    SELECT T3.DocNo, Sum(T2.Amt) Amt ");
            SQL.AppendLine("    From TblIncomingPaymentHdr T1  ");
            SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T2 On T1.DocNo = T2.DocNo  ");
            SQL.AppendLine("        And T2.InvoiceType = '6'  ");
            SQL.AppendLine("    Inner Join TblSOContractDownpaymentHdr T3 On T2.InvoiceDocNo = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractHdr T4 On T3.SOContractDocNo = T4.DocNo  ");
            SQL.AppendLine("    Where T1.CancelInd = 'N'  ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O') <> 'C'  ");
            SQL.AppendLine("    Group By T3.DocNo ");
            SQL.AppendLine(") E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Amt-ifnull(D.Amt, 0.00)-ifnull(E.Amt, 0.00) > 0 ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "",
                        "Date",
                        "Customer's Code",
                        "Customer",
                        
                        //6-10
                        "Currency",
                        "Amount",
                        "Outstanding",
                        "Voucher Request#",
                        "SO Contract#",

                        //11
                        "Local Document#"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 20, 80, 130, 200, 
                        
                        //6-10
                        80, 130, 130, 30, 30,

                        //11
                        150
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 9 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[11].Move(2);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt Desc, A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CtCode", "CtName", "CurCode", "Amt", 
                            
                            //6-9
                            "OutstandingAmt", "VoucherRequestDocNo", "SOContractDocNo", "LocalDocNo"                  
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                int r = Grd1.CurRow.Index;
                if (Sm.IsFindGridValid(Grd1, 0))
                {
                    mFrmParent.TxtSOContractDownpaymentDocNo.EditValue = Sm.GetGrdStr(Grd1, r, 1);
                    mFrmParent.mCtCode = Sm.GetGrdStr(Grd1, r, 4);
                    mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, r, 5);
                    mFrmParent.TxtCurCode.EditValue = Sm.GetGrdStr(Grd1, r, 6);
                    mFrmParent.TxtSOContractDownpaymentAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 7), 0);
                    mFrmParent.TxtOutstandingAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 8), 0);
                    mFrmParent.TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                    mFrmParent.TxtVoucherRequestDocNo.EditValue = Sm.GetGrdStr(Grd1, r, 9);
                    mFrmParent.mSOContractDocNo = Sm.GetGrdStr(Grd1, r, 10);
                    mFrmParent.TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, r, 11);
                    this.Close();
                }
            }
        }

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContractDownpayment("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContractDownpayment("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

    }
}
