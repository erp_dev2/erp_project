﻿#region Update
/*
    27/02/2019 [TKG] untuk kawasan berikat
    17/03/2019 [TKG] tambah informasi BC 2.6.1
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhs4Dlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvWhs4 mFrmParent;
        private string mSQL = string.Empty,
            mKB_Server = string.Empty,
            mKB_Database = string.Empty,
            mKB_DBUser = string.Empty,
            mKB_DBPwd = string.Empty,
            mKB_Port = string.Empty;

        #endregion

        #region Constructor

        public FrmRecvWhs4Dlg3(FrmRecvWhs4 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                GetParameter();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Registration#",
                        "Registration Date",
                        "Packing List#",
                        "Packing List Date",
                        "Contract#",

                        //6-9
                        "Contract Date",
                        "Used",
                        "Used For",
                        "BC 2.6.1"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 120, 150, 120, 180,
                        
                        //6-9
                        120, 80, 200, 150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 4, 6 });
            Sm.GrdColCheck(Grd1, new int[] { 7 });
            Grd1.Cols[7].Move(3);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            var l = new List<KB_Data>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<KB_Data> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.Nomor_Daftar", false);

            SQL.AppendLine("Select A.Nomor_Daftar As KBRegistrationNo, Date_Format(A.Tanggal_Daftar, '%Y%m%d') As KBRegistrationDt, ");
            SQL.AppendLine("B.Nomor_Dokumen As KBContractNo, Date_Format(B.Tanggal_Dokumen, '%Y%m%d') As KBContractDt, ");
            SQL.AppendLine("C.Nomor_Dokumen As KBPLNo, Date_Format(C.Tanggal_Dokumen, '%Y%m%d') As KBPLDt, ");
            SQL.AppendLine("D.Nomor_Dokumen As KBBC261 ");
            SQL.AppendLine("From tpb_header A ");
            SQL.AppendLine("Left Join tpb_dokumen B On A.ID=B.ID_HEADER And B.Kode_Jenis_Dokumen='315' ");
            SQL.AppendLine("Left Join tpb_dokumen C On A.ID=C.ID_HEADER And C.Kode_Jenis_Dokumen='217' ");
            SQL.AppendLine("Left Join tpb_dokumen D On A.ID=D.ID_HEADER And D.Kode_Jenis_Dokumen='261' ");
            SQL.AppendLine("Where A.Kode_Dokumen_Pabean='262' ");
            SQL.AppendLine("And Date_Format(A.Tanggal_Daftar, '%Y%m%d') Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.Tanggal_Daftar, A.Nomor_Daftar;");

            using (var cn = new MySqlConnection(ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "KBRegistrationNo", 
                    
                    //1-5
                    "KBRegistrationDt", 
                    "KBContractNo", 
                    "KBContractDt", 
                    "KBPLNo", 
                    "KBPLDt",

                    //6
                    "KBBC261"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new KB_Data()
                        {
                            KBRegistrationNo = Sm.DrStr(dr, c[0]),
                            KBRegistrationDt = Sm.DrStr(dr, c[1]),
                            KBContractNo = Sm.DrStr(dr, c[2]), 
                            KBContractDt = Sm.DrStr(dr, c[3]), 
                            KBPLNo = Sm.DrStr(dr, c[4]),
                            KBPLDt = Sm.DrStr(dr, c[5]),
                            KBBC261 = Sm.DrStr(dr, c[6]),
                            DocNo = string.Empty,
                            UsedInd = false
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<KB_Data> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var Filter = string.Empty;
            var l2 = new List<KB_Data2>();

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.KBRegistrationNo=@KBRegistrationNo0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@KBRegistrationNo0" + i.ToString(), l[i].KBRegistrationNo);
            }

            if (Filter.Length != 0) Filter = " And (" + Filter + ") ";

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            SQL.AppendLine("Select A.KBRegistrationNo, ");
            SQL.AppendLine("Group_Concat(Distinct A.DocNo Order By A.DocNo Separator ', ') As DocNo ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where A.KBRegistrationNo Is Not Null ");
            SQL.AppendLine("And A.KBRegistrationDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Group By A.KBRegistrationNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "KBRegistrationNo", "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new KB_Data2()
                        {
                            KBRegistrationNo = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }

            if (l2.Count > 0)
            {
                foreach (var i in l)
                {
                    foreach (var j in l2.Where(w => Sm.CompareStr(w.KBRegistrationNo, i.KBRegistrationNo)))
                    {
                        if (j.DocNo.Length > 0)
                        {
                            i.DocNo = j.DocNo;
                            i.UsedInd = true;
                        }
                        break;
                    }
                }
                l2.Clear();
            }
        }

        private void Process3(ref List<KB_Data> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = l[i].KBRegistrationNo;
                if (l[i].KBRegistrationDt.Length > 0) r.Cells[2].Value = Sm.ConvertDate(l[i].KBRegistrationDt);
                r.Cells[3].Value = l[i].KBPLNo;
                if (l[i].KBPLDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].KBPLDt);
                r.Cells[5].Value = l[i].KBContractNo;
                if (l[i].KBContractDt.Length > 0) r.Cells[6].Value = Sm.ConvertDate(l[i].KBContractDt);
                r.Cells[7].Value = l[i].UsedInd;
                r.Cells[8].Value = l[i].DocNo;
                r.Cells[9].Value = l[i].KBBC261;
            }
            Grd1.EndUpdate();
        }

        private string ConnectionString
        {
            get
            {
                return
                    string.Concat(
                    @"Server=", mKB_Server, ";Database=", mKB_Database, ";Uid=", mKB_DBUser, ";Password=", mKB_DBPwd, ";Allow User Variables=True;Connection Timeout=1200;",
                    mKB_Port.Length == 0 ? string.Empty : string.Concat("Port=", mKB_Port, ";"));
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.TxtKBRegistrationNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                if (Sm.GetGrdDate(Grd1, Row, 2).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBRegistrationDt, Sm.GetGrdDate(Grd1, Row, 2).Substring(0, 8));
                mFrmParent.TxtKBPLNo.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                if (Sm.GetGrdDate(Grd1, Row, 4).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBPLDt, Sm.GetGrdDate(Grd1, Row, 4).Substring(0, 8));
                mFrmParent.TxtKBContractNo.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                if (Sm.GetGrdDate(Grd1, Row, 6).Length >= 8)
                    Sm.SetDte(mFrmParent.DteKBContractDt, Sm.GetGrdDate(Grd1, Row, 6).Substring(0, 8));
                this.Close();
            }
        }

        private void GetParameter()
        {
            mKB_Server = Sm.GetParameter("KB_Server");
            mKB_Database = Sm.GetParameter("KB_Database");
            mKB_DBUser = Sm.GetParameter("KB_DBUser");
            mKB_DBPwd = Sm.GetParameter("KB_DBPwd");
            mKB_Port = Sm.GetParameter("KB_Port");
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion

        #region Class

        private class KB_Data
        {
            public string KBRegistrationNo { get; set; }
            public string KBRegistrationDt { get; set; }
            public string KBContractNo { get; set; }
            public string KBContractDt { get; set; }
            public string KBPLNo { get; set; }
            public string KBPLDt { get; set; }
            public string KBBC261 { get; set; }
            public string DocNo { get; set; }
            public bool UsedInd { get; set; }
        }

        private class KB_Data2
        {
            public string KBRegistrationNo { get; set; }
            public string DocNo { get; set; }
        }

        #endregion

    }
}
