﻿namespace RunSystem
{
    partial class FrmIndependentEstimatedPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmIndependentEstimatedPrice));
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueReqType = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRemainingBudget = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtPOQtyCancelDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnPOQtyCancelDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPOQtyCancelDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDORequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblDORequestDocNo = new System.Windows.Forms.Label();
            this.BtnDORequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDORequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.LblPICCode = new System.Windows.Forms.Label();
            this.LuePICCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ChkAddendumInd = new DevExpress.XtraEditors.CheckEdit();
            this.BtnMRDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnMRDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtMRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkRMInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel8 = new System.Windows.Forms.Panel();
            this.TxtGrandTotalIndependentEstimatedPrice = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnReference = new DevExpress.XtraEditors.SimpleButton();
            this.LblReference = new System.Windows.Forms.Label();
            this.TxtReference = new DevExpress.XtraEditors.TextEdit();
            this.LueProcurementType = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.LblExpDt = new System.Windows.Forms.Label();
            this.DteExpDt = new DevExpress.XtraEditors.DateEdit();
            this.ChkTenderInd = new DevExpress.XtraEditors.CheckEdit();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.LblFile6 = new System.Windows.Forms.Label();
            this.LblFile2 = new System.Windows.Forms.Label();
            this.LblFile5 = new System.Windows.Forms.Label();
            this.LblFile = new System.Windows.Forms.Label();
            this.LblFile4 = new System.Windows.Forms.Label();
            this.LblFile3 = new System.Windows.Forms.Label();
            this.TxtFile6 = new DevExpress.XtraEditors.TextEdit();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile6 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload6 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile6 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.PbUpload6 = new System.Windows.Forms.ProgressBar();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.TxtFile5 = new DevExpress.XtraEditors.TextEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.ChkFile5 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload5 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.BtnFile5 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload5 = new System.Windows.Forms.ProgressBar();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile4 = new DevExpress.XtraEditors.TextEdit();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.ChkFile4 = new DevExpress.XtraEditors.CheckEdit();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload4 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload4 = new System.Windows.Forms.ProgressBar();
            this.BtnFile4 = new DevExpress.XtraEditors.SimpleButton();
            this.Tp5 = new DevExpress.XtraTab.XtraTabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.BtnFile7 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload7 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile7 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload7 = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtFile7 = new DevExpress.XtraEditors.TextEdit();
            this.GrdReview = new TenTec.Windows.iGridLib.iGrid();
            this.LueDurationUom = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOQtyCancelDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAddendumInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRMInd.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotalIndependentEstimatedPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcurementType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTenderInd.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            this.Tp5.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdReview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDurationUom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(967, 0);
            this.panel1.Size = new System.Drawing.Size(70, 461);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(967, 332);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LuePtCode);
            this.panel3.Controls.Add(this.LueDurationUom);
            this.panel3.Controls.Add(this.LueCurCode);
            this.panel3.Controls.Add(this.DteUsageDt);
            this.panel3.Location = new System.Drawing.Point(0, 332);
            this.panel3.Size = new System.Drawing.Size(967, 129);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.DteUsageDt, 0);
            this.panel3.Controls.SetChildIndex(this.LueCurCode, 0);
            this.panel3.Controls.SetChildIndex(this.LueDurationUom, 0);
            this.panel3.Controls.SetChildIndex(this.LuePtCode, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 439);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(967, 129);
            this.Grd1.TabIndex = 60;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.GrdAfterCommitEdit);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(105, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(30, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(105, 90);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(70, 93);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 28;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(19, 135);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 14);
            this.label3.TabIndex = 34;
            this.label3.Text = "Request Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReqType
            // 
            this.LueReqType.EnterMoveNextControl = true;
            this.LueReqType.Location = new System.Drawing.Point(105, 132);
            this.LueReqType.Margin = new System.Windows.Forms.Padding(5);
            this.LueReqType.Name = "LueReqType";
            this.LueReqType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.Appearance.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReqType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReqType.Properties.DropDownRows = 30;
            this.LueReqType.Properties.NullText = "[Empty]";
            this.LueReqType.Properties.PopupWidth = 300;
            this.LueReqType.Size = new System.Drawing.Size(349, 20);
            this.LueReqType.TabIndex = 35;
            this.LueReqType.ToolTip = "F4 : Show/hide list";
            this.LueReqType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(30, 177);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 38;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(105, 174);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(349, 20);
            this.LueDeptCode.TabIndex = 39;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtRemainingBudget
            // 
            this.TxtRemainingBudget.EnterMoveNextControl = true;
            this.TxtRemainingBudget.Location = new System.Drawing.Point(105, 258);
            this.TxtRemainingBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemainingBudget.Name = "TxtRemainingBudget";
            this.TxtRemainingBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemainingBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemainingBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemainingBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemainingBudget.Properties.ReadOnly = true;
            this.TxtRemainingBudget.Size = new System.Drawing.Size(166, 20);
            this.TxtRemainingBudget.TabIndex = 49;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(6, 262);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 14);
            this.label11.TabIndex = 48;
            this.label11.Text = "Available Budget";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(105, 279);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 2000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(349, 20);
            this.MeeRemark.TabIndex = 51;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(56, 283);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 50;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(63, 69);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(125, 20);
            this.DteUsageDt.TabIndex = 61;
            this.DteUsageDt.Visible = false;
            this.DteUsageDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteUsageDt_KeyDown);
            this.DteUsageDt.Leave += new System.EventHandler(this.DteUsageDt_Leave);
            // 
            // TxtPOQtyCancelDocNo
            // 
            this.TxtPOQtyCancelDocNo.EnterMoveNextControl = true;
            this.TxtPOQtyCancelDocNo.Location = new System.Drawing.Point(105, 69);
            this.TxtPOQtyCancelDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPOQtyCancelDocNo.Name = "TxtPOQtyCancelDocNo";
            this.TxtPOQtyCancelDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPOQtyCancelDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPOQtyCancelDocNo.Properties.MaxLength = 16;
            this.TxtPOQtyCancelDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtPOQtyCancelDocNo.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(3, 72);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 14);
            this.label8.TabIndex = 24;
            this.label8.Text = "Cancellation PO#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPOQtyCancelDocNo2
            // 
            this.BtnPOQtyCancelDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOQtyCancelDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOQtyCancelDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOQtyCancelDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseFont = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnPOQtyCancelDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnPOQtyCancelDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOQtyCancelDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOQtyCancelDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPOQtyCancelDocNo2.Image")));
            this.BtnPOQtyCancelDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOQtyCancelDocNo2.Location = new System.Drawing.Point(302, 69);
            this.BtnPOQtyCancelDocNo2.Name = "BtnPOQtyCancelDocNo2";
            this.BtnPOQtyCancelDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnPOQtyCancelDocNo2.TabIndex = 27;
            this.BtnPOQtyCancelDocNo2.ToolTip = "Show Cancellation PO Document";
            this.BtnPOQtyCancelDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOQtyCancelDocNo2.ToolTipTitle = "Run System";
            this.BtnPOQtyCancelDocNo2.Click += new System.EventHandler(this.BtnPOQtyCancelDocNo2_Click);
            // 
            // BtnPOQtyCancelDocNo
            // 
            this.BtnPOQtyCancelDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOQtyCancelDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOQtyCancelDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOQtyCancelDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseFont = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPOQtyCancelDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPOQtyCancelDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOQtyCancelDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOQtyCancelDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPOQtyCancelDocNo.Image")));
            this.BtnPOQtyCancelDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOQtyCancelDocNo.Location = new System.Drawing.Point(274, 69);
            this.BtnPOQtyCancelDocNo.Name = "BtnPOQtyCancelDocNo";
            this.BtnPOQtyCancelDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPOQtyCancelDocNo.TabIndex = 26;
            this.BtnPOQtyCancelDocNo.ToolTip = "Find Cancellation PO Document";
            this.BtnPOQtyCancelDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOQtyCancelDocNo.ToolTipTitle = "Run System";
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(105, 111);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 250;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(350, 20);
            this.TxtLocalDocNo.TabIndex = 33;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(8, 114);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(95, 14);
            this.label76.TabIndex = 32;
            this.label76.Text = "Local Document";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(75, 156);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 36;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(105, 153);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(349, 20);
            this.LueSiteCode.TabIndex = 37;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.Validated += new System.EventHandler(this.LueSiteCode_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(3, 240);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 14);
            this.label7.TabIndex = 46;
            this.label7.Text = "Budget Category";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(105, 237);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 300;
            this.LueBCCode.Size = new System.Drawing.Size(349, 20);
            this.LueBCCode.TabIndex = 47;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // TxtDORequestDocNo
            // 
            this.TxtDORequestDocNo.EnterMoveNextControl = true;
            this.TxtDORequestDocNo.Location = new System.Drawing.Point(105, 216);
            this.TxtDORequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDORequestDocNo.Name = "TxtDORequestDocNo";
            this.TxtDORequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDORequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDORequestDocNo.Properties.MaxLength = 250;
            this.TxtDORequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtDORequestDocNo.TabIndex = 43;
            // 
            // LblDORequestDocNo
            // 
            this.LblDORequestDocNo.AutoSize = true;
            this.LblDORequestDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDORequestDocNo.Location = new System.Drawing.Point(21, 219);
            this.LblDORequestDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDORequestDocNo.Name = "LblDORequestDocNo";
            this.LblDORequestDocNo.Size = new System.Drawing.Size(82, 14);
            this.LblDORequestDocNo.TabIndex = 42;
            this.LblDORequestDocNo.Text = "DO Request#";
            this.LblDORequestDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDORequestDocNo2
            // 
            this.BtnDORequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDORequestDocNo2.Image")));
            this.BtnDORequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo2.Location = new System.Drawing.Point(302, 215);
            this.BtnDORequestDocNo2.Name = "BtnDORequestDocNo2";
            this.BtnDORequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo2.TabIndex = 45;
            this.BtnDORequestDocNo2.ToolTip = "Show DO Request Document";
            this.BtnDORequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo2.ToolTipTitle = "Run System";
            this.BtnDORequestDocNo2.Click += new System.EventHandler(this.BtnDORequestDocNo2_Click);
            // 
            // BtnDORequestDocNo
            // 
            this.BtnDORequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDORequestDocNo.Image")));
            this.BtnDORequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo.Location = new System.Drawing.Point(274, 215);
            this.BtnDORequestDocNo.Name = "BtnDORequestDocNo";
            this.BtnDORequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo.TabIndex = 44;
            this.BtnDORequestDocNo.ToolTip = "Find DO Request Document";
            this.BtnDORequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo.ToolTipTitle = "Run System";
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(196, 69);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 14;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 200;
            this.LueCurCode.Size = new System.Drawing.Size(125, 20);
            this.LueCurCode.TabIndex = 62;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            this.LueCurCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode_KeyDown);
            this.LueCurCode.Leave += new System.EventHandler(this.LueCurCode_Leave);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // LblPICCode
            // 
            this.LblPICCode.AutoSize = true;
            this.LblPICCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPICCode.ForeColor = System.Drawing.Color.Black;
            this.LblPICCode.Location = new System.Drawing.Point(78, 199);
            this.LblPICCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPICCode.Name = "LblPICCode";
            this.LblPICCode.Size = new System.Drawing.Size(25, 14);
            this.LblPICCode.TabIndex = 40;
            this.LblPICCode.Text = "PIC";
            this.LblPICCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePICCode
            // 
            this.LuePICCode.EnterMoveNextControl = true;
            this.LuePICCode.Location = new System.Drawing.Point(105, 195);
            this.LuePICCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePICCode.Name = "LuePICCode";
            this.LuePICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.Appearance.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePICCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePICCode.Properties.DropDownRows = 30;
            this.LuePICCode.Properties.NullText = "[Empty]";
            this.LuePICCode.Properties.PopupWidth = 300;
            this.LuePICCode.Size = new System.Drawing.Size(350, 20);
            this.LuePICCode.TabIndex = 41;
            this.LuePICCode.ToolTip = "F4 : Show/hide list";
            this.LuePICCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePICCode.EditValueChanged += new System.EventHandler(this.LuePICCode_EditValueChanged);
            // 
            // Tc1
            // 
            this.Tc1.Appearance.Options.UseTextOptions = true;
            this.Tc1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(967, 332);
            this.Tc1.TabIndex = 11;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp3,
            this.Tp5});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(961, 304);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.ChkAddendumInd);
            this.panel5.Controls.Add(this.BtnMRDocNo);
            this.panel5.Controls.Add(this.BtnMRDocNo2);
            this.panel5.Controls.Add(this.MeeCancelReason);
            this.panel5.Controls.Add(this.ChkCancelInd);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.TxtMRDocNo);
            this.panel5.Controls.Add(this.ChkRMInd);
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.LblExpDt);
            this.panel5.Controls.Add(this.DteExpDt);
            this.panel5.Controls.Add(this.ChkTenderInd);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.LblPICCode);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.LuePICCode);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.LueReqType);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.LueDeptCode);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.BtnDORequestDocNo2);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.BtnDORequestDocNo);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.TxtDORequestDocNo);
            this.panel5.Controls.Add(this.TxtRemainingBudget);
            this.panel5.Controls.Add(this.LblDORequestDocNo);
            this.panel5.Controls.Add(this.BtnPOQtyCancelDocNo);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.LueBCCode);
            this.panel5.Controls.Add(this.TxtPOQtyCancelDocNo);
            this.panel5.Controls.Add(this.LblSiteCode);
            this.panel5.Controls.Add(this.BtnPOQtyCancelDocNo2);
            this.panel5.Controls.Add(this.LueSiteCode);
            this.panel5.Controls.Add(this.label76);
            this.panel5.Controls.Add(this.TxtLocalDocNo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(961, 304);
            this.panel5.TabIndex = 22;
            // 
            // ChkAddendumInd
            // 
            this.ChkAddendumInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkAddendumInd.Location = new System.Drawing.Point(280, 3);
            this.ChkAddendumInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkAddendumInd.Name = "ChkAddendumInd";
            this.ChkAddendumInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkAddendumInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAddendumInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkAddendumInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkAddendumInd.Properties.Appearance.Options.UseFont = true;
            this.ChkAddendumInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkAddendumInd.Properties.Caption = "Addendum";
            this.ChkAddendumInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAddendumInd.Size = new System.Drawing.Size(86, 22);
            this.ChkAddendumInd.TabIndex = 14;
            // 
            // BtnMRDocNo
            // 
            this.BtnMRDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMRDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMRDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMRDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMRDocNo.Appearance.Options.UseBackColor = true;
            this.BtnMRDocNo.Appearance.Options.UseFont = true;
            this.BtnMRDocNo.Appearance.Options.UseForeColor = true;
            this.BtnMRDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnMRDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMRDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMRDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnMRDocNo.Image")));
            this.BtnMRDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMRDocNo.Location = new System.Drawing.Point(279, 29);
            this.BtnMRDocNo.Name = "BtnMRDocNo";
            this.BtnMRDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnMRDocNo.TabIndex = 19;
            this.BtnMRDocNo.ToolTip = "Find Cancellation PO Document";
            this.BtnMRDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMRDocNo.ToolTipTitle = "Run System";
            this.BtnMRDocNo.Click += new System.EventHandler(this.BtnMRDocNo_Click);
            // 
            // BtnMRDocNo2
            // 
            this.BtnMRDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMRDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMRDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMRDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMRDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnMRDocNo2.Appearance.Options.UseFont = true;
            this.BtnMRDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnMRDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnMRDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMRDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMRDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnMRDocNo2.Image")));
            this.BtnMRDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMRDocNo2.Location = new System.Drawing.Point(307, 29);
            this.BtnMRDocNo2.Name = "BtnMRDocNo2";
            this.BtnMRDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnMRDocNo2.TabIndex = 20;
            this.BtnMRDocNo2.ToolTip = "Show Cancellation PO Document";
            this.BtnMRDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMRDocNo2.ToolTipTitle = "Run System";
            this.BtnMRDocNo2.Click += new System.EventHandler(this.BtnMRDocNo2_Click);
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(105, 48);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(166, 20);
            this.MeeCancelReason.TabIndex = 22;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(280, 49);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 23;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(18, 51);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 14);
            this.label13.TabIndex = 21;
            this.label13.Text = "Cancel Reason";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(70, 29);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 14);
            this.label9.TabIndex = 17;
            this.label9.Text = "MR#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMRDocNo
            // 
            this.TxtMRDocNo.EnterMoveNextControl = true;
            this.TxtMRDocNo.Location = new System.Drawing.Point(105, 27);
            this.TxtMRDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMRDocNo.Name = "TxtMRDocNo";
            this.TxtMRDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMRDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtMRDocNo.Properties.MaxLength = 16;
            this.TxtMRDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtMRDocNo.TabIndex = 18;
            // 
            // ChkRMInd
            // 
            this.ChkRMInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkRMInd.Location = new System.Drawing.Point(456, 3);
            this.ChkRMInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkRMInd.Name = "ChkRMInd";
            this.ChkRMInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkRMInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRMInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkRMInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkRMInd.Properties.Appearance.Options.UseFont = true;
            this.ChkRMInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkRMInd.Properties.Caption = "RUNMarket";
            this.ChkRMInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkRMInd.Size = new System.Drawing.Size(91, 22);
            this.ChkRMInd.TabIndex = 16;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.TxtGrandTotalIndependentEstimatedPrice);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.BtnReference);
            this.panel8.Controls.Add(this.LblReference);
            this.panel8.Controls.Add(this.TxtReference);
            this.panel8.Controls.Add(this.LueProcurementType);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(552, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(409, 304);
            this.panel8.TabIndex = 59;
            // 
            // TxtGrandTotalIndependentEstimatedPrice
            // 
            this.TxtGrandTotalIndependentEstimatedPrice.EnterMoveNextControl = true;
            this.TxtGrandTotalIndependentEstimatedPrice.Location = new System.Drawing.Point(237, 47);
            this.TxtGrandTotalIndependentEstimatedPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrandTotalIndependentEstimatedPrice.Name = "TxtGrandTotalIndependentEstimatedPrice";
            this.TxtGrandTotalIndependentEstimatedPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrandTotalIndependentEstimatedPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrandTotalIndependentEstimatedPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrandTotalIndependentEstimatedPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtGrandTotalIndependentEstimatedPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrandTotalIndependentEstimatedPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGrandTotalIndependentEstimatedPrice.Properties.ReadOnly = true;
            this.TxtGrandTotalIndependentEstimatedPrice.Size = new System.Drawing.Size(167, 20);
            this.TxtGrandTotalIndependentEstimatedPrice.TabIndex = 58;
            this.TxtGrandTotalIndependentEstimatedPrice.Validated += new System.EventHandler(this.TxtGrandTotalIndependentEstimatedPrice_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(2, 50);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(235, 14);
            this.label10.TabIndex = 57;
            this.label10.Text = "Grand Total Independent Estimated Price";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnReference
            // 
            this.BtnReference.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnReference.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnReference.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnReference.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnReference.Appearance.Options.UseBackColor = true;
            this.BtnReference.Appearance.Options.UseFont = true;
            this.BtnReference.Appearance.Options.UseForeColor = true;
            this.BtnReference.Appearance.Options.UseTextOptions = true;
            this.BtnReference.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnReference.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnReference.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnReference.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnReference.Location = new System.Drawing.Point(380, 26);
            this.BtnReference.Name = "BtnReference";
            this.BtnReference.Size = new System.Drawing.Size(24, 21);
            this.BtnReference.TabIndex = 56;
            this.BtnReference.ToolTip = "Find Cancellation MR Document";
            this.BtnReference.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnReference.ToolTipTitle = "Run System";
            // 
            // LblReference
            // 
            this.LblReference.AutoSize = true;
            this.LblReference.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblReference.ForeColor = System.Drawing.Color.Black;
            this.LblReference.Location = new System.Drawing.Point(145, 29);
            this.LblReference.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblReference.Name = "LblReference";
            this.LblReference.Size = new System.Drawing.Size(92, 14);
            this.LblReference.TabIndex = 54;
            this.LblReference.Text = "Reference MR#";
            this.LblReference.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtReference
            // 
            this.TxtReference.EnterMoveNextControl = true;
            this.TxtReference.Location = new System.Drawing.Point(237, 26);
            this.TxtReference.Margin = new System.Windows.Forms.Padding(5);
            this.TxtReference.Name = "TxtReference";
            this.TxtReference.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtReference.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtReference.Properties.Appearance.Options.UseBackColor = true;
            this.TxtReference.Properties.Appearance.Options.UseFont = true;
            this.TxtReference.Properties.MaxLength = 16;
            this.TxtReference.Size = new System.Drawing.Size(137, 20);
            this.TxtReference.TabIndex = 55;
            // 
            // LueProcurementType
            // 
            this.LueProcurementType.EnterMoveNextControl = true;
            this.LueProcurementType.Location = new System.Drawing.Point(237, 5);
            this.LueProcurementType.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcurementType.Name = "LueProcurementType";
            this.LueProcurementType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.Appearance.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcurementType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcurementType.Properties.DropDownRows = 30;
            this.LueProcurementType.Properties.NullText = "[Empty]";
            this.LueProcurementType.Properties.PopupWidth = 300;
            this.LueProcurementType.Size = new System.Drawing.Size(167, 20);
            this.LueProcurementType.TabIndex = 53;
            this.LueProcurementType.ToolTip = "F4 : Show/hide list";
            this.LueProcurementType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(127, 8);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 14);
            this.label22.TabIndex = 52;
            this.label22.Text = "Procurement Type";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblExpDt
            // 
            this.LblExpDt.AutoSize = true;
            this.LblExpDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblExpDt.ForeColor = System.Drawing.Color.Red;
            this.LblExpDt.Location = new System.Drawing.Point(296, 92);
            this.LblExpDt.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblExpDt.Name = "LblExpDt";
            this.LblExpDt.Size = new System.Drawing.Size(47, 14);
            this.LblExpDt.TabIndex = 30;
            this.LblExpDt.Text = "Expired";
            this.LblExpDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteExpDt
            // 
            this.DteExpDt.EditValue = null;
            this.DteExpDt.EnterMoveNextControl = true;
            this.DteExpDt.Location = new System.Drawing.Point(346, 90);
            this.DteExpDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteExpDt.Name = "DteExpDt";
            this.DteExpDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpDt.Properties.Appearance.Options.UseFont = true;
            this.DteExpDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteExpDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteExpDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteExpDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteExpDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteExpDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteExpDt.Size = new System.Drawing.Size(109, 20);
            this.DteExpDt.TabIndex = 31;
            // 
            // ChkTenderInd
            // 
            this.ChkTenderInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTenderInd.Location = new System.Drawing.Point(366, 3);
            this.ChkTenderInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTenderInd.Name = "ChkTenderInd";
            this.ChkTenderInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTenderInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTenderInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTenderInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTenderInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTenderInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTenderInd.Properties.Caption = "For Tender";
            this.ChkTenderInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTenderInd.Size = new System.Drawing.Size(91, 22);
            this.ChkTenderInd.TabIndex = 15;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.Tp3.Appearance.PageClient.Options.UseBackColor = true;
            this.Tp3.Controls.Add(this.panel4);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(961, 304);
            this.Tp3.Text = "Upload File";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.splitContainer1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(961, 304);
            this.panel4.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.LblFile6);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile2);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile5);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile4);
            this.splitContainer1.Panel1.Controls.Add(this.LblFile3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile6);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile2);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile6);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload2);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload6);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile6);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile3);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload6);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile2);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile5);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile2);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile5);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile3);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload5);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload3);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile5);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload5);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload3);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile4);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload2);
            this.splitContainer1.Panel2.Controls.Add(this.TxtFile);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile4);
            this.splitContainer1.Panel2.Controls.Add(this.ChkFile3);
            this.splitContainer1.Panel2.Controls.Add(this.BtnDownload4);
            this.splitContainer1.Panel2.Controls.Add(this.PbUpload4);
            this.splitContainer1.Panel2.Controls.Add(this.BtnFile4);
            this.splitContainer1.Size = new System.Drawing.Size(754, 259);
            this.splitContainer1.SplitterDistance = 52;
            this.splitContainer1.TabIndex = 48;
            // 
            // LblFile6
            // 
            this.LblFile6.AutoSize = true;
            this.LblFile6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile6.ForeColor = System.Drawing.Color.Black;
            this.LblFile6.Location = new System.Drawing.Point(25, 221);
            this.LblFile6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile6.Name = "LblFile6";
            this.LblFile6.Size = new System.Drawing.Size(24, 14);
            this.LblFile6.TabIndex = 42;
            this.LblFile6.Text = "File";
            this.LblFile6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile2
            // 
            this.LblFile2.AutoSize = true;
            this.LblFile2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile2.ForeColor = System.Drawing.Color.Black;
            this.LblFile2.Location = new System.Drawing.Point(25, 50);
            this.LblFile2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile2.Name = "LblFile2";
            this.LblFile2.Size = new System.Drawing.Size(24, 14);
            this.LblFile2.TabIndex = 18;
            this.LblFile2.Text = "File";
            this.LblFile2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile5
            // 
            this.LblFile5.AutoSize = true;
            this.LblFile5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile5.ForeColor = System.Drawing.Color.Black;
            this.LblFile5.Location = new System.Drawing.Point(25, 179);
            this.LblFile5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile5.Name = "LblFile5";
            this.LblFile5.Size = new System.Drawing.Size(24, 14);
            this.LblFile5.TabIndex = 36;
            this.LblFile5.Text = "File";
            this.LblFile5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile
            // 
            this.LblFile.AutoSize = true;
            this.LblFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile.ForeColor = System.Drawing.Color.Black;
            this.LblFile.Location = new System.Drawing.Point(25, 7);
            this.LblFile.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile.Name = "LblFile";
            this.LblFile.Size = new System.Drawing.Size(24, 14);
            this.LblFile.TabIndex = 12;
            this.LblFile.Text = "File";
            this.LblFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile4
            // 
            this.LblFile4.AutoSize = true;
            this.LblFile4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile4.ForeColor = System.Drawing.Color.Black;
            this.LblFile4.Location = new System.Drawing.Point(25, 136);
            this.LblFile4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile4.Name = "LblFile4";
            this.LblFile4.Size = new System.Drawing.Size(24, 14);
            this.LblFile4.TabIndex = 30;
            this.LblFile4.Text = "File";
            this.LblFile4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblFile3
            // 
            this.LblFile3.AutoSize = true;
            this.LblFile3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile3.ForeColor = System.Drawing.Color.Black;
            this.LblFile3.Location = new System.Drawing.Point(25, 93);
            this.LblFile3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile3.Name = "LblFile3";
            this.LblFile3.Size = new System.Drawing.Size(24, 14);
            this.LblFile3.TabIndex = 24;
            this.LblFile3.Text = "File";
            this.LblFile3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFile6
            // 
            this.TxtFile6.EnterMoveNextControl = true;
            this.TxtFile6.Location = new System.Drawing.Point(3, 216);
            this.TxtFile6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile6.Name = "TxtFile6";
            this.TxtFile6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile6.Properties.Appearance.Options.UseFont = true;
            this.TxtFile6.Properties.MaxLength = 16;
            this.TxtFile6.Size = new System.Drawing.Size(390, 20);
            this.TxtFile6.TabIndex = 43;
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(3, 25);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(390, 17);
            this.PbUpload.TabIndex = 17;
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(426, 44);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 21;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // ChkFile6
            // 
            this.ChkFile6.Location = new System.Drawing.Point(398, 217);
            this.ChkFile6.Name = "ChkFile6";
            this.ChkFile6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile6.Properties.Appearance.Options.UseFont = true;
            this.ChkFile6.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile6.Properties.Caption = " ";
            this.ChkFile6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile6.Size = new System.Drawing.Size(20, 22);
            this.ChkFile6.TabIndex = 44;
            this.ChkFile6.ToolTip = "Remove filter";
            this.ChkFile6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile6.ToolTipTitle = "Run System";
            this.ChkFile6.CheckedChanged += new System.EventHandler(this.ChkFile6_CheckedChanged);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(457, 44);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 22;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // BtnDownload6
            // 
            this.BtnDownload6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload6.Appearance.Options.UseBackColor = true;
            this.BtnDownload6.Appearance.Options.UseFont = true;
            this.BtnDownload6.Appearance.Options.UseForeColor = true;
            this.BtnDownload6.Appearance.Options.UseTextOptions = true;
            this.BtnDownload6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload6.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload6.Location = new System.Drawing.Point(457, 217);
            this.BtnDownload6.Name = "BtnDownload6";
            this.BtnDownload6.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload6.TabIndex = 46;
            this.BtnDownload6.ToolTip = "DownloadFile";
            this.BtnDownload6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload6.ToolTipTitle = "Run System";
            this.BtnDownload6.Click += new System.EventHandler(this.BtnDownload6_Click);
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(426, 3);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 15;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // BtnFile6
            // 
            this.BtnFile6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile6.Appearance.Options.UseBackColor = true;
            this.BtnFile6.Appearance.Options.UseFont = true;
            this.BtnFile6.Appearance.Options.UseForeColor = true;
            this.BtnFile6.Appearance.Options.UseTextOptions = true;
            this.BtnFile6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile6.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile6.Location = new System.Drawing.Point(426, 217);
            this.BtnFile6.Name = "BtnFile6";
            this.BtnFile6.Size = new System.Drawing.Size(24, 21);
            this.BtnFile6.TabIndex = 45;
            this.BtnFile6.ToolTip = "BrowseFile";
            this.BtnFile6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile6.ToolTipTitle = "Run System";
            this.BtnFile6.Click += new System.EventHandler(this.BtnFile6_Click);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(3, 88);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(390, 20);
            this.TxtFile3.TabIndex = 25;
            // 
            // PbUpload6
            // 
            this.PbUpload6.Location = new System.Drawing.Point(3, 239);
            this.PbUpload6.Name = "PbUpload6";
            this.PbUpload6.Size = new System.Drawing.Size(390, 17);
            this.PbUpload6.TabIndex = 47;
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(398, 44);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 20;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // TxtFile5
            // 
            this.TxtFile5.EnterMoveNextControl = true;
            this.TxtFile5.Location = new System.Drawing.Point(3, 174);
            this.TxtFile5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile5.Name = "TxtFile5";
            this.TxtFile5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile5.Properties.Appearance.Options.UseFont = true;
            this.TxtFile5.Properties.MaxLength = 16;
            this.TxtFile5.Size = new System.Drawing.Size(390, 20);
            this.TxtFile5.TabIndex = 37;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(457, 3);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 16;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(3, 45);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(390, 20);
            this.TxtFile2.TabIndex = 19;
            // 
            // ChkFile5
            // 
            this.ChkFile5.Location = new System.Drawing.Point(398, 175);
            this.ChkFile5.Name = "ChkFile5";
            this.ChkFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile5.Properties.Appearance.Options.UseFont = true;
            this.ChkFile5.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile5.Properties.Caption = " ";
            this.ChkFile5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile5.Size = new System.Drawing.Size(20, 22);
            this.ChkFile5.TabIndex = 38;
            this.ChkFile5.ToolTip = "Remove filter";
            this.ChkFile5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile5.ToolTipTitle = "Run System";
            this.ChkFile5.CheckedChanged += new System.EventHandler(this.ChkFile5_CheckedChanged);
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(426, 87);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 27;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // BtnDownload5
            // 
            this.BtnDownload5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload5.Appearance.Options.UseBackColor = true;
            this.BtnDownload5.Appearance.Options.UseFont = true;
            this.BtnDownload5.Appearance.Options.UseForeColor = true;
            this.BtnDownload5.Appearance.Options.UseTextOptions = true;
            this.BtnDownload5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload5.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload5.Location = new System.Drawing.Point(457, 175);
            this.BtnDownload5.Name = "BtnDownload5";
            this.BtnDownload5.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload5.TabIndex = 40;
            this.BtnDownload5.ToolTip = "DownloadFile";
            this.BtnDownload5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload5.ToolTipTitle = "Run System";
            this.BtnDownload5.Click += new System.EventHandler(this.BtnDownload5_Click);
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(3, 111);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(390, 17);
            this.PbUpload3.TabIndex = 29;
            // 
            // BtnFile5
            // 
            this.BtnFile5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile5.Appearance.Options.UseBackColor = true;
            this.BtnFile5.Appearance.Options.UseFont = true;
            this.BtnFile5.Appearance.Options.UseForeColor = true;
            this.BtnFile5.Appearance.Options.UseTextOptions = true;
            this.BtnFile5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile5.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile5.Location = new System.Drawing.Point(426, 175);
            this.BtnFile5.Name = "BtnFile5";
            this.BtnFile5.Size = new System.Drawing.Size(24, 21);
            this.BtnFile5.TabIndex = 39;
            this.BtnFile5.ToolTip = "BrowseFile";
            this.BtnFile5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile5.ToolTipTitle = "Run System";
            this.BtnFile5.Click += new System.EventHandler(this.BtnFile5_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(398, 3);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 14;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // PbUpload5
            // 
            this.PbUpload5.Location = new System.Drawing.Point(3, 197);
            this.PbUpload5.Name = "PbUpload5";
            this.PbUpload5.Size = new System.Drawing.Size(390, 17);
            this.PbUpload5.TabIndex = 41;
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(457, 87);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 28;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // TxtFile4
            // 
            this.TxtFile4.EnterMoveNextControl = true;
            this.TxtFile4.Location = new System.Drawing.Point(3, 131);
            this.TxtFile4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile4.Name = "TxtFile4";
            this.TxtFile4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile4.Properties.Appearance.Options.UseFont = true;
            this.TxtFile4.Properties.MaxLength = 16;
            this.TxtFile4.Size = new System.Drawing.Size(390, 20);
            this.TxtFile4.TabIndex = 31;
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(3, 68);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(390, 17);
            this.PbUpload2.TabIndex = 23;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(3, 2);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(390, 20);
            this.TxtFile.TabIndex = 13;
            // 
            // ChkFile4
            // 
            this.ChkFile4.Location = new System.Drawing.Point(398, 132);
            this.ChkFile4.Name = "ChkFile4";
            this.ChkFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile4.Properties.Appearance.Options.UseFont = true;
            this.ChkFile4.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile4.Properties.Caption = " ";
            this.ChkFile4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile4.Size = new System.Drawing.Size(20, 22);
            this.ChkFile4.TabIndex = 32;
            this.ChkFile4.ToolTip = "Remove filter";
            this.ChkFile4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile4.ToolTipTitle = "Run System";
            this.ChkFile4.CheckedChanged += new System.EventHandler(this.ChkFile4_CheckedChanged);
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(398, 87);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 26;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // BtnDownload4
            // 
            this.BtnDownload4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload4.Appearance.Options.UseBackColor = true;
            this.BtnDownload4.Appearance.Options.UseFont = true;
            this.BtnDownload4.Appearance.Options.UseForeColor = true;
            this.BtnDownload4.Appearance.Options.UseTextOptions = true;
            this.BtnDownload4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload4.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnDownload4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload4.Location = new System.Drawing.Point(457, 132);
            this.BtnDownload4.Name = "BtnDownload4";
            this.BtnDownload4.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload4.TabIndex = 34;
            this.BtnDownload4.ToolTip = "DownloadFile";
            this.BtnDownload4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload4.ToolTipTitle = "Run System";
            this.BtnDownload4.Click += new System.EventHandler(this.BtnDownload4_Click);
            // 
            // PbUpload4
            // 
            this.PbUpload4.Location = new System.Drawing.Point(3, 154);
            this.PbUpload4.Name = "PbUpload4";
            this.PbUpload4.Size = new System.Drawing.Size(390, 17);
            this.PbUpload4.TabIndex = 35;
            // 
            // BtnFile4
            // 
            this.BtnFile4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile4.Appearance.Options.UseBackColor = true;
            this.BtnFile4.Appearance.Options.UseFont = true;
            this.BtnFile4.Appearance.Options.UseForeColor = true;
            this.BtnFile4.Appearance.Options.UseTextOptions = true;
            this.BtnFile4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile4.Image = global::RunSystem.Properties.Resources.SearchGlyph;
            this.BtnFile4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile4.Location = new System.Drawing.Point(426, 132);
            this.BtnFile4.Name = "BtnFile4";
            this.BtnFile4.Size = new System.Drawing.Size(24, 21);
            this.BtnFile4.TabIndex = 33;
            this.BtnFile4.ToolTip = "BrowseFile";
            this.BtnFile4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile4.ToolTipTitle = "Run System";
            this.BtnFile4.Click += new System.EventHandler(this.BtnFile4_Click);
            // 
            // Tp5
            // 
            this.Tp5.Appearance.Header.Options.UseTextOptions = true;
            this.Tp5.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp5.Controls.Add(this.panel9);
            this.Tp5.Controls.Add(this.GrdReview);
            this.Tp5.Name = "Tp5";
            this.Tp5.Size = new System.Drawing.Size(961, 304);
            this.Tp5.Text = "Review";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.BtnFile7);
            this.panel9.Controls.Add(this.BtnDownload7);
            this.panel9.Controls.Add(this.ChkFile7);
            this.panel9.Controls.Add(this.PbUpload7);
            this.panel9.Controls.Add(this.label6);
            this.panel9.Controls.Add(this.TxtFile7);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(961, 47);
            this.panel9.TabIndex = 11;
            // 
            // BtnFile7
            // 
            this.BtnFile7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile7.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile7.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile7.Appearance.Options.UseBackColor = true;
            this.BtnFile7.Appearance.Options.UseFont = true;
            this.BtnFile7.Appearance.Options.UseForeColor = true;
            this.BtnFile7.Appearance.Options.UseTextOptions = true;
            this.BtnFile7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile7.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile7.Image")));
            this.BtnFile7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile7.Location = new System.Drawing.Point(513, 1);
            this.BtnFile7.Name = "BtnFile7";
            this.BtnFile7.Size = new System.Drawing.Size(24, 21);
            this.BtnFile7.TabIndex = 15;
            this.BtnFile7.ToolTip = "BrowseFile";
            this.BtnFile7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile7.ToolTipTitle = "Run System";
            this.BtnFile7.Click += new System.EventHandler(this.BtnFile7_Click);
            // 
            // BtnDownload7
            // 
            this.BtnDownload7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload7.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload7.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload7.Appearance.Options.UseBackColor = true;
            this.BtnDownload7.Appearance.Options.UseFont = true;
            this.BtnDownload7.Appearance.Options.UseForeColor = true;
            this.BtnDownload7.Appearance.Options.UseTextOptions = true;
            this.BtnDownload7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload7.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload7.Image")));
            this.BtnDownload7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload7.Location = new System.Drawing.Point(543, 1);
            this.BtnDownload7.Name = "BtnDownload7";
            this.BtnDownload7.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload7.TabIndex = 16;
            this.BtnDownload7.ToolTip = "DownloadFile";
            this.BtnDownload7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload7.ToolTipTitle = "Run System";
            this.BtnDownload7.Click += new System.EventHandler(this.BtnDownload7_Click);
            // 
            // ChkFile7
            // 
            this.ChkFile7.Location = new System.Drawing.Point(487, 2);
            this.ChkFile7.Name = "ChkFile7";
            this.ChkFile7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile7.Properties.Appearance.Options.UseFont = true;
            this.ChkFile7.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile7.Properties.Caption = " ";
            this.ChkFile7.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile7.Size = new System.Drawing.Size(20, 22);
            this.ChkFile7.TabIndex = 14;
            this.ChkFile7.ToolTip = "Remove filter";
            this.ChkFile7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile7.ToolTipTitle = "Run System";
            this.ChkFile7.CheckedChanged += new System.EventHandler(this.ChkFile7_CheckedChanged);
            // 
            // PbUpload7
            // 
            this.PbUpload7.Location = new System.Drawing.Point(90, 24);
            this.PbUpload7.Name = "PbUpload7";
            this.PbUpload7.Size = new System.Drawing.Size(390, 17);
            this.PbUpload7.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(16, 5);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 12;
            this.label6.Text = "Upload File";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFile7
            // 
            this.TxtFile7.EnterMoveNextControl = true;
            this.TxtFile7.Location = new System.Drawing.Point(90, 2);
            this.TxtFile7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile7.Name = "TxtFile7";
            this.TxtFile7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile7.Properties.Appearance.Options.UseFont = true;
            this.TxtFile7.Properties.MaxLength = 16;
            this.TxtFile7.Size = new System.Drawing.Size(390, 20);
            this.TxtFile7.TabIndex = 13;
            // 
            // GrdReview
            // 
            this.GrdReview.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.GrdReview.DefaultRow.Height = 20;
            this.GrdReview.DefaultRow.Sortable = false;
            this.GrdReview.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GrdReview.ForeColor = System.Drawing.SystemColors.WindowText;
            this.GrdReview.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.GrdReview.Header.Height = 21;
            this.GrdReview.Location = new System.Drawing.Point(0, 47);
            this.GrdReview.Name = "GrdReview";
            this.GrdReview.RowHeader.Visible = true;
            this.GrdReview.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.GrdReview.SingleClickEdit = true;
            this.GrdReview.Size = new System.Drawing.Size(961, 257);
            this.GrdReview.TabIndex = 18;
            this.GrdReview.TreeCol = null;
            this.GrdReview.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // LueDurationUom
            // 
            this.LueDurationUom.EnterMoveNextControl = true;
            this.LueDurationUom.Location = new System.Drawing.Point(331, 69);
            this.LueDurationUom.Margin = new System.Windows.Forms.Padding(5);
            this.LueDurationUom.Name = "LueDurationUom";
            this.LueDurationUom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.Appearance.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDurationUom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDurationUom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDurationUom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDurationUom.Properties.DropDownRows = 14;
            this.LueDurationUom.Properties.NullText = "[Empty]";
            this.LueDurationUom.Properties.PopupWidth = 200;
            this.LueDurationUom.Size = new System.Drawing.Size(125, 20);
            this.LueDurationUom.TabIndex = 63;
            this.LueDurationUom.ToolTip = "F4 : Show/hide list";
            this.LueDurationUom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDurationUom.EditValueChanged += new System.EventHandler(this.LueDurationUom_EditValueChanged);
            this.LueDurationUom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueDurationUom_KeyDown);
            this.LueDurationUom.Leave += new System.EventHandler(this.LueDurationUom_Leave);
            // 
            // LuePtCode
            // 
            this.LuePtCode.EnterMoveNextControl = true;
            this.LuePtCode.Location = new System.Drawing.Point(466, 69);
            this.LuePtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePtCode.Name = "LuePtCode";
            this.LuePtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.Appearance.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePtCode.Properties.DropDownRows = 14;
            this.LuePtCode.Properties.NullText = "[Empty]";
            this.LuePtCode.Properties.PopupWidth = 200;
            this.LuePtCode.Size = new System.Drawing.Size(125, 20);
            this.LuePtCode.TabIndex = 64;
            this.LuePtCode.ToolTip = "F4 : Show/hide list";
            this.LuePtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePtCode.EditValueChanged += new System.EventHandler(this.LuePtCode_EditValueChanged);
            this.LuePtCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePtCode_KeyDown);
            this.LuePtCode.Leave += new System.EventHandler(this.LuePtCode_Leave);
            // 
            // FrmIndependentEstimatedPrice2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1037, 461);
            this.Name = "FrmIndependentEstimatedPrice2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOQtyCancelDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAddendumInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRMInd.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotalIndependentEstimatedPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcurementType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTenderInd.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            this.Tp5.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdReview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDurationUom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtRemainingBudget;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        public DevExpress.XtraEditors.SimpleButton BtnPOQtyCancelDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtPOQtyCancelDocNo;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnPOQtyCancelDocNo;
        private System.Windows.Forms.Label label76;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label LblSiteCode;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.LookUpEdit LueBCCode;
        private System.Windows.Forms.Label LblDORequestDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtDORequestDocNo;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.Label LblPICCode;
        public DevExpress.XtraEditors.LookUpEdit LuePICCode;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.LookUpEdit LueReqType;
        private DevExpress.XtraEditors.LookUpEdit LueDurationUom;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Label LblExpDt;
        internal DevExpress.XtraEditors.DateEdit DteExpDt;
        private DevExpress.XtraEditors.LookUpEdit LuePtCode;
        protected System.Windows.Forms.Panel panel8;
        internal DevExpress.XtraEditors.LookUpEdit LueProcurementType;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraTab.XtraTabPage Tp5;
        protected internal TenTec.Windows.iGridLib.iGrid GrdReview;
        public DevExpress.XtraEditors.SimpleButton BtnReference;
        private System.Windows.Forms.Label LblReference;
        internal DevExpress.XtraEditors.TextEdit TxtReference;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        internal DevExpress.XtraEditors.MemoExEdit MeeRemark;
        internal DevExpress.XtraEditors.CheckEdit ChkTenderInd;
        internal DevExpress.XtraEditors.CheckEdit ChkRMInd;
        private System.Windows.Forms.Panel panel9;
        public DevExpress.XtraEditors.SimpleButton BtnFile7;
        public DevExpress.XtraEditors.SimpleButton BtnDownload7;
        private DevExpress.XtraEditors.CheckEdit ChkFile7;
        private System.Windows.Forms.ProgressBar PbUpload7;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtFile7;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtMRDocNo;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label13;
        public DevExpress.XtraEditors.SimpleButton BtnMRDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnMRDocNo2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label LblFile6;
        private System.Windows.Forms.Label LblFile2;
        private System.Windows.Forms.Label LblFile5;
        private System.Windows.Forms.Label LblFile;
        private System.Windows.Forms.Label LblFile4;
        private System.Windows.Forms.Label LblFile3;
        internal DevExpress.XtraEditors.TextEdit TxtFile6;
        private System.Windows.Forms.ProgressBar PbUpload;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private DevExpress.XtraEditors.CheckEdit ChkFile6;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload6;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        public DevExpress.XtraEditors.SimpleButton BtnFile6;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        private System.Windows.Forms.ProgressBar PbUpload6;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        internal DevExpress.XtraEditors.TextEdit TxtFile5;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private DevExpress.XtraEditors.CheckEdit ChkFile5;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload5;
        private System.Windows.Forms.ProgressBar PbUpload3;
        public DevExpress.XtraEditors.SimpleButton BtnFile5;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        private System.Windows.Forms.ProgressBar PbUpload5;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        internal DevExpress.XtraEditors.TextEdit TxtFile4;
        private System.Windows.Forms.ProgressBar PbUpload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private DevExpress.XtraEditors.CheckEdit ChkFile4;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload4;
        private System.Windows.Forms.ProgressBar PbUpload4;
        public DevExpress.XtraEditors.SimpleButton BtnFile4;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.CheckEdit ChkAddendumInd;
        internal DevExpress.XtraEditors.TextEdit TxtGrandTotalIndependentEstimatedPrice;
    }
}