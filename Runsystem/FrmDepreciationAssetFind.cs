﻿#region Update
/*
    29/08/2017 [ARI] tambah kolom display name
    20/11/2017 [TKG] tambah asset value
    17/05/2017 [TKG] tambah coa utk depresiasi aset
    10/11/2021 [RIS/AMKA] menampilkan asset berdasarkan cost center group user
    03/02/2022 [SET/PHT] membuat filter Multi Profit Center dengan validasi group user dan parameter IsFicoUseMultiProfitCenterFilter 
    01/03/2022 [SET/PHT] Feedback : Menambahkan filter Multi profit center di Depreciation Business Asset ketika find
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDepreciationAssetFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDepreciationAsset mFrmParent;
        private string mSQL = string.Empty;
        private List<String> mlProfitCenter = null;
        private bool mIsAllProfitCenterSelected = false;

        #endregion

        #region Constructor

        public FrmDepreciationAssetFind(FrmDepreciationAsset FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -60);
                SetGrd();
                if(mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                else
                    LblMultiProfitCenterCode.Visible = CcbProfitCenterCode.Visible = ChkProfitCenterCode.Visible = mFrmParent.mIsFicoUseMultiProfitCenterFilter;
                //SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel", 
                        "Asset Code",
                        "Asset Name",

                        //6-10
                        "Display Name",
                        "Initial"+Environment.NewLine+"Cost Center",
                        "Year",
                        "Month",
                        "Active",
                        
                        //11-15
                        "Depreciation"+Environment.NewLine+"Value",
                        "Asset"+Environment.NewLine+"Value",
                        "Cost Center",
                        "Annual Cost Value",
                        "Journal#",
                        
                        //16-20
                        "Account#"+Environment.NewLine+"Depreciation Asset",
                        "Account Description"+Environment.NewLine+"Depreciation Asset",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 

                        //21-23
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",                         
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 47, 100, 200,
                        
                        //6-10
                        200, 150, 80, 120, 80, 

                        //11-15
                        120, 120, 150, 120, 130, 

                        //16-20
                        120, 200, 100, 100, 100, 100, 100,

                        //21-23
                        100, 100, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 19, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 23 });
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22, 23 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21, 22, 23 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.AssetCode, B.AssetName, ");
            SQL.AppendLine("B.DisplayName, C.CCName As InitialCCName, D.Yr, D.Mth, D.ActiveInd, ");
            SQL.AppendLine("D.DepreciationValue, B.AssetValue, E.CCName, ");
            SQL.AppendLine("Round(D.AnnualCostValue, 2) As AnnualCostValue, D.JournalDocNo, F.AcNo, G.AcDesc,");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblDepreciationAssetHdr A ");
            SQL.AppendLine("Left Join TblAsset B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Left Join TblCostCenter C ON B.CCCode=C.CCCode ");
            SQL.AppendLine("Inner Join TblDepreciationAssetDtl D On A.DocNo=D.DocNo ");
            SQL.AppendLine("left Join TblCostCenter E ON D.CCCode=E.CCCode ");
            SQL.AppendLine("Left Join TblAssetCategory F On B.AssetCategoryCode=F.AssetCategoryCode ");
            SQL.AppendLine("Left Join TblCOA G On F.AcNo=G.AcNo ");
            SQL.AppendLine("LEFT JOIN tblprofitcenter H ON E.ProfitCenterCode = H.ProfitCenterCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            if (mFrmParent.mIsDBACCFilteredByGroup)
            {
                SQL.AppendLine("AND EXISTS ( ");
                SQL.AppendLine("    SELECT 1 FROM TblGroupCostCenter ");
                SQL.AppendLine("    WHERE CCCode=D.CCCode ");
                SQL.AppendLine("    AND GrpCode IN (SELECT GrpCode FROM TblUser WHERE ");
                SQL.AppendLine("    UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }


            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                if (IsProfitCenterInvalid()) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ", Filter2 = string.Empty;

                var cm = new MySqlCommand();

                SetProfitCenter();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAsset.Text, new string[] { "A.AssetCode", "B.AssetName" });
                if (mFrmParent.mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter_ = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter_.Length > 0) Filter_ += " Or ";
                            Filter_ += " (H.ProfitCenterCode=@ProfitCenter1_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter1_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter_.Length == 0)
                            Filter2 += "    And 1=0 ";
                        else
                            Filter2 += "    And (" + Filter_ + ") ";
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            Filter2 += "    And Find_In_Set(H.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            Filter2 += "    And H.ProfitCenterCode In ( ";
                            Filter2 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            Filter2 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            Filter2 += "    ) ";
                        }
                    }
                }
                if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + Filter2 + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "CancelInd", "AssetCode", "AssetName", "DisplayName", 

                            //6-10
                            "InitialCCName", "Yr", "Mth", "ActiveInd", "DepreciationValue",  
                            
                            //11-15
                            "AssetValue", "CCName", "AnnualCostValue", "JournalDocNo", "AcNo", 
                            
                            //16-20
                            "AcDesc", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 20);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
                {
                    if (Sm.GetLue(LueFontSize).Length != 0)
                    {
                        Grd1.Font = new Font(
                            Grd1.Font.FontFamily.Name.ToString(),
                            int.Parse(Sm.GetLue(LueFontSize))
                            );
                    }
                }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            //if (mIsFilterByProfitCenter)
            //{
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            //}
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param)) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Misc Control Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mFrmParent.mIsFicoUseMultiProfitCenterFilter) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtAsset_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAsset_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit Center");
        }

        #endregion
    }
}
