﻿#region Update
/*
    08/11/2017 [TKG] update perhitungan take home pay.
    11/01/2018 [TKG] tambah BPJS pensiun
    26/02/2018 [TKG] ubah perhitungan brutto
    09/03/2018 [TKG] filter site
    15/03/2018 [TKG] tambah indikator pensiun
    22/03/2018 [TKG] ubah rumus brutto, tambah nomor rekening dan nama bank.
    10/04/2018 [TKG] tambah tombol untuk menampilkan rincian fixed allowance dan fixed deduction
    14/05/2018 [TKG] untuk ho brutto ditambah sci
    10/07/2018 [TKG] ubah rumus brutto
    24/07/2018 [TKG] menampilkan rincian ss
    18/06/2020 [TKG/HIN] menambah nontaxable salary adjustment dan taxable salary adjustment
    15/02/2022 [TKG/HIN] merubah GetParameter()
    25/04/2022 [TYO/HIN] menambah kolom Employement Status
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPayrollProcessSummary3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal string mSalaryInd = "1";
        private bool 
            mIsNotFilterByAuthorization = false,
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummary3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd(); 
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");               
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsFilterBySiteHR', 'IsFilterByDeptHR', 'SalaryInd', 'IsPayrollDataFilterByAuthorization' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsFilterByDeptHR ": mIsFilterByDeptHR = ParValue == "Y"; break;

                            //string
                            case "SalaryInd": mSalaryInd = ParValue; break;
                            case "IsPayrollDataFilterByAuthorization": mIsNotFilterByAuthorization = ParValue == "N"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, A.EmpCode, C.EmpName, ");
            SQL.AppendLine("C.EmpCodeOld, E.PosName, D.DeptName, C.JoinDt, C.ResignDt, ");
            SQL.AppendLine("F.OptDesc As SystemTypeDesc, G.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("H.PGName, J.SiteName, A.NPWP, I.OptDesc As NonTaxableIncomeDesc, ");
            SQL.AppendLine("A.Salary, A.WorkingDay, A.PLDay, A.PLHr, ");
            SQL.AppendLine("A.PLAmt, A.ProcessPLAmt, A.UPLDay, A.UPLHr, A.UPLAmt, A.ProcessUPLAmt, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, ");
            SQL.AppendLine("A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.TaxableFixAllowance, A.NonTaxableFixAllowance, A.FixAllowance, A.EmploymentPeriodAllowance, ");
            SQL.AppendLine("A.IncEmployee, A.IncMinWages, A.IncProduction, A.IncPerformance, A.PresenceReward, ");
            SQL.AppendLine("A.HolidayEarning, A.ExtraFooding, A.Meal, A.Transport, A.ServiceChargeIncentive, A.ADLeave, A.FieldAssignment, A.SSEmployerHealth, A.SSEmployerEmployment, A.SSEmployerPension, A.NonTaxableFixDeduction, A.TaxableFixDeduction, A.FixDeduction, ");
            SQL.AppendLine("A.DedEmployee, A.DedProduction, A.DedProdLeave, A.EmpAdvancePayment, ");
            SQL.AppendLine("A.SalaryAdjustment, A.Tax, A.TaxAllowance, A.Amt, A.VoucherRequestPayrollDocNo, ");
            SQL.AppendLine("(IfNull(A.OT1Hr,0)+IfNull(OT2Hr,0)+IfNull(OTHolidayHr,0))As OTTotalHr, (IfNull(A.OT1Amt,0)+IfNull(OT2Amt,0)+IfNull(OTHolidayAmt,0))As OTTotalAmt, A.PensionInd, ");
            SQL.AppendLine("C.BankAcNo, K.BankName, ");
            SQL.AppendLine("A.Salary + ");
            SQL.AppendLine("A.TaxableFixAllowance + ");
            SQL.AppendLine("A.Transport + ");
            SQL.AppendLine("A.Meal + ");
            SQL.AppendLine("A.ADLeave + ");
            SQL.AppendLine("A.SalaryAdjustment + ");
            SQL.AppendLine("A.ServiceChargeIncentive + ");
            SQL.AppendLine("A.SSEmployerHealth + ");
            SQL.AppendLine("A.SSErLifeInsurance + ");
            SQL.AppendLine("A.SSErWorkingAccident ");
            SQL.AppendLine("As Brutto, ");
            SQL.AppendLine("SSEmployerHealth, SSErRetirement, SSErLifeInsurance, SSErWorkingAccident, SSErPension, ");
            SQL.AppendLine("SSEmployerPension, SSEmployerPension2, ");
            SQL.AppendLine("SSEmployeeHealth, SSEeRetirement, SSEeLifeInsurance, SSEeWorkingAccident, SSEePension, ");
            SQL.AppendLine("SSEmployeePension, SSEmployeePension2,  ");
            SQL.AppendLine("NontaxableSalaryAdjustment, TaxableSalaryAdjustment, L.OptDesc AS EmployementStatus ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B ");
            SQL.AppendLine("    On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And B.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(B.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblPosition E On C.PosCode=E.PosCode ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Inner Join TblOption G On B.PayrunPeriod=G.OptCode And G.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr H On B.PGCode=H.PGCode ");
            SQL.AppendLine("Left Join TblOption I On A.PTKP=I.OptCode And I.OptCat='NonTaxableIncome' ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left Join TblBank K On C.BankCode=K.BankCode ");
            SQL.AppendLine("Inner JOIN TblOption L ON C.EmploymentStatus = L.OptCode AND L.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Where 1=1 ");

            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 73;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Payrun"+Environment.NewLine+"Code",
                        "Payrun Name",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",

                        //6-10
                        "Old Code",
                        "Position",
                        "Department",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",
                        
                        //11-15
                        "Type", //hide
                        "Period",//hide
                        "Group",//hide
                        "Site",
                        "NPWP",

                        //16-20
                        "PTKP",             
                        "Working Day", 
                        "Paid Leave"+Environment.NewLine+"(Day)", //hide
                        "Paid Leave"+Environment.NewLine+"(Hour)",  //hide
                        "Unpaid Leave"+Environment.NewLine+"(Hour)", //hide
                        
                        //21-25
                        "Pension",
                        "Salary",
                        "Taxable Fixed"+Environment.NewLine+"Allowance", //hide
                        "Non Taxable Fixed"+Environment.NewLine+"Allowance", //hide
                        "Fixed"+Environment.NewLine+"Allowance", 

                        //26-30
                        "Field"+Environment.NewLine+"Assignment", //hide
                        "Meal",
                        "Transport",
                        "Production"+Environment.NewLine+"Incentive", //hide
                        "Service"+Environment.NewLine+"Charge",
                        
                        //31-35
                        "Annual Leave"+Environment.NewLine+"Allowance",
                        "OT 1"+Environment.NewLine+"(Hour)", //hide
                        "OT 2"+Environment.NewLine+"(Hour)", //hide
                        "OT Holiday"+Environment.NewLine+"(Hour)", //hide
                        "Total OT"+Environment.NewLine+"(Hour)",
                        
                        //36-40
                        "OT 1"+Environment.NewLine+"(Amount)", //hide
                        "OT 2"+Environment.NewLine+"(Amount)",//hide
                        "OT Holiday"+Environment.NewLine+"(Amount)", //hide
                        "Total OT"+Environment.NewLine+"(Amount)",
                        "Salary"+Environment.NewLine+"Adjustment",
                        
                        //41-45
                        "Employment Period"+Environment.NewLine+"Allowance", //Hide
                        "Brutto",
                        "Tax",
                        "Tax Allowance",
                        "Employee Loan",
                      
                        //46-50
                        "Unpaid Leave"+Environment.NewLine+"(Amount)", //Hide
                        "Take Home"+Environment.NewLine+"Pay",
                        "Non Taxable Employee's"+Environment.NewLine+"Deduction", //Hide
                        "Fixed"+Environment.NewLine+"Deduction",
                        "Voucher Request",
                        
                        //51-55
                        "Tax-Tax Allowance",
                        "Bank Account#",
                        "Bank",
                        "",
                        "",

                        //56-60
                        "BPJS Kesehatan"+Environment.NewLine+"(Employer)",
                        "BPJS Jaminan Hari Tua"+Environment.NewLine+"(Employer)",
                        "BPJS Jaminan Kematian"+Environment.NewLine+"(Employer)",
                        "BPJS Jaminan Keselamatan Kerja"+Environment.NewLine+"(Employer)",
                        "BPJS Jaminan Pensiun"+Environment.NewLine+"(Employer)",
                        
                        //61-65
                        "Pensiun HII"+Environment.NewLine+"(Employer)",
                        "Pensiun Natour"+Environment.NewLine+"(Employer)",
                        "BPJS Kesehatan"+Environment.NewLine+"(Employee)",
                        "BPJS Jaminan Hari Tua"+Environment.NewLine+"(Employee)",
                        "BPJS Jaminan Kematian"+Environment.NewLine+"(Employee)",
                        
                        //66-70
                        "BPJS Jaminan Keselamatan Kerja"+Environment.NewLine+"(Employee)",
                        "BPJS Jaminan Pensiun"+Environment.NewLine+"(Employee)",
                        "Pensiun HII"+Environment.NewLine+"(Employee)",
                        "Pensiun Natour"+Environment.NewLine+"(Employee)",
                        "Nontaxable"+Environment.NewLine+"Salary Adjustment",

                        //71-73
                        "Taxable"+Environment.NewLine+"Salary Adjustment",
                        "Employement"+Environment.NewLine+"Status"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 80, 200, 
                        
                        //6-10
                        80, 150, 150, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 150, 130, 
                        
                        //16-20
                        150, 100, 100, 100, 100,  
                        
                        //21-25
                        80, 130, 130, 130, 100, 
                        
                        //26-30
                        100, 100, 100, 100, 120, 

                        //31-35
                        100, 100, 100, 100, 100, 

                        //36-40
                        100, 100, 100, 100, 100, 

                        //41-45
                        150, 130, 100, 100, 120,

                        //46-50
                        120, 120, 150, 130, 130, 
                        
                        //51-55
                        150, 150, 200, 20, 20,

                        //56-60
                        150, 150, 150, 180, 150,

                        //61-65
                        150, 150, 150, 150, 150,

                        //66-70
                        180, 150, 150, 150, 150,

                        //71-72
                        150, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 54, 55 });
            Sm.GrdColCheck(Grd1, new int[] { 21 });
            Sm.GrdFormatDec(Grd1, new int[] { 
                17, 18, 19, 20, 
                22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                41, 42, 43, 44, 45, 46, 47, 48, 49, 
                51, 56, 57, 58, 59, 60,
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                71
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] 
            { 
                11, 12, 13, 18, 19, 20, 
                23, 24, 26, 29, 
                33, 34, 35, 37, 38, 39, 
                52, 53
            }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 
                0, 
                2, 3, 4, 5, 6, 7, 9, 10, 
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 52, 53, 56, 57, 58, 59, 60,
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                71, 72
            }, false);
            Grd1.Cols[47].Move(43);
            Grd1.Cols[51].Move(42);
            Grd1.Cols[54].Move(26);
            Grd1.Cols[55].Move(52);
            Grd1.Cols[70].Move(41);
            Grd1.Cols[71].Move(42);
            Grd1.Cols[72].Move(7);
        }

        override protected void HideInfoInGrd()
        {
          Sm.GrdColInvisible(Grd1, new int[] { 
              11, 12, 13, 18, 19, 20, 
              23, 24, 26, 29, 
              33, 34, 35, 37, 38, 39,  
              52, 53 }, 
              !ChkHideInfoInGrd.Checked);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPayCod.Text, new string[] { "A.PayrunCode", "B.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By A.PayrunCode, C.EmpName;",
                        new string[]
                        {
                            //0
                            "PayrunCode",
                        
                            //1-5
                            "PayrunName",
                            "EmpCode",
                            "EmpName",
                            "EmpCodeOld",
                            "PosName",
                            
                            //6-10
                            "DeptName",
                            "JoinDt",
                            "ResignDt",
                            "SystemTypeDesc", 
                            "PayrunPeriodDesc",
                            
                            //11-15
                            "PGName",
                            "SiteName",
                            "NPWP",
                            "NonTaxableIncomeDesc",
                            "WorkingDay",

                            //16-20
                            "PLDay",
                            "PLHr",
                            "UPLHr",
                            "Salary",
                            "TaxableFixAllowance",
                            
                            //21-25
                            "NonTaxableFixAllowance",
                            "FixAllowance",
                            "FieldAssignment",
                            "Meal",
                            "Transport",
                            
                            //26-30
                            "IncProduction",
                            "ServiceChargeIncentive",
                            "ADLeave",
                            "OT1Hr",
                            "OT2Hr",
                            
                            //31-35
                            "OTHolidayHr",
                            "OTTotalHr",
                            "OT1Amt",
                            "OT2Amt",
                            "OTHolidayAmt",
                            
                            //36-40
                            "OTTotalAmt",
                            "SalaryAdjustment",
                            "EmploymentPeriodAllowance",
                            "Tax",
                            "TaxAllowance",
                            
                            //41-45
                            "EmpAdvancePayment",
                            "UPLAmt",
                            "Amt",
                            "NonTaxableFixDeduction",
                            "FixDeduction",
                            
                            //46-50
                            "VoucherRequestPayrollDocNo",
                            "PensionInd",
                            "BankAcNo",
                            "BankName",
                            "Brutto",

                            //51-55
                            "SSEmployerHealth",
                            "SSErRetirement",
                            "SSErLifeInsurance",
                            "SSErWorkingAccident",
                            "SSErPension",

                            //56-60
                            "SSEmployerPension",
                            "SSEmployerPension2",
                            "SSEmployeeHealth",
                            "SSEeRetirement",
                            "SSEeLifeInsurance",

                            //61-65
                            "SSEeWorkingAccident",
                            "SSEePension",
                            "SSEmployeePension",
                            "SSEmployeePension2",
                            "NontaxableSalaryAdjustment",

                            //66-67
                            "TaxableSalaryAdjustment",
                            "EmployementStatus"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 28);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 30);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 34, 31);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 35, 32);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 34);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 35);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 36);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 37);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 38);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 39);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 40);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 41);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 42);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 43);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 44);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 49, 45);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 50, 46);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 21, 47);
                            Grd.Cells[Row, 51].Value = Sm.GetGrdDec(Grd, Row, 43) - Sm.GetGrdDec(Grd, Row, 44);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 52, 48);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 53, 49);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 50);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 56, 51);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 57, 52);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 58, 53);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 59, 54);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 60, 55);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 61, 56);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 62, 57);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 63, 58);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 64, 59);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 65, 60);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 66, 61);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 67, 62);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 68, 63);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 69, 64);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 70, 65);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 71, 66);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 72, 67);
                        }, true, false, false, false
                    );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                    { 
                        17, 18, 19, 20, 
                        22, 23, 24, 25, 26, 27, 28, 29, 30, 
                        31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 
                        51, 52, 54, 56, 57, 58, 59, 60,
                        61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                        71
                    });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }
    
        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    Sm.FormShowDialog(
                        new FrmRptPayrollProcessSummary3Dlg(
                            this,
                            Sm.GetGrdStr(Grd1, r, 2),
                            Sm.GetGrdStr(Grd1, r, 4)
                            ));
                }
            }
            if (e.ColIndex == 54 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 25) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
            if (e.ColIndex == 55 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, r, 49) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, r, 2).Length > 0)
            {
                Sm.FormShowDialog(
                    new FrmRptPayrollProcessSummary3Dlg(
                        this,
                        Sm.GetGrdStr(Grd1, r, 2),
                        Sm.GetGrdStr(Grd1, r, 4)
                        ));
            }
            if (e.ColIndex == 54 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 25) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("A", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
            if (e.ColIndex == 55 && Sm.GetGrdStr(Grd1, r, 2).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 49) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessAD("D", Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 4));
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void ShowPayrollProcessAD(string ADType, string PayrunCode, string EmpCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select D.ADName, Sum(C.Amt) As Amt ");
            SQL.AppendLine("From TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode ");
            SQL.AppendLine("Inner Join TblEmployeeAllowanceDeduction C ");
            SQL.AppendLine("    On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (C.StartDt Is Null And C.EndDt Is Null) Or ");
            SQL.AppendLine("    (C.StartDt Is Not Null And C.EndDt Is Null And C.StartDt<=B.EndDt) Or ");
            SQL.AppendLine("    (C.StartDt Is Null And C.EndDt Is Not Null And B.EndDt<=C.EndDt) Or ");
            SQL.AppendLine("    (C.StartDt Is Not Null And C.EndDt Is Not Null And C.StartDt<=B.EndDt And B.EndDt<=C.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction D On C.ADCode=D.ADCode And D.ADType=@ADType And D.AmtType='1' ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Group By D.ADName ");
            SQL.AppendLine("Having Sum(C.Amt)<>0.00 ");
            SQL.AppendLine("Order By D.ADName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                    Sm.CmParam<String>(ref cm, "@ADType", ADType);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[0]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtPayCod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayCod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        #endregion

        #endregion
    }
}
