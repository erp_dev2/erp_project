﻿namespace RunSystem
{
    partial class FrmLOP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLOP));
            this.label14 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.MeeScope = new DevExpress.XtraEditors.MemoExEdit();
            this.LuePICSales = new DevExpress.XtraEditors.LookUpEdit();
            this.LblPICSales = new System.Windows.Forms.Label();
            this.TxtQtLetterNo = new DevExpress.XtraEditors.TextEdit();
            this.LblQtLetterNo = new System.Windows.Forms.Label();
            this.LueConfidentLvl = new DevExpress.XtraEditors.LookUpEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.BtnCopyData = new DevExpress.XtraEditors.SimpleButton();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.lblResource = new System.Windows.Forms.Label();
            this.TxtPICName = new DevExpress.XtraEditors.TextEdit();
            this.LueResource = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.lblProjectScope = new System.Windows.Forms.Label();
            this.TxtEstimated = new DevExpress.XtraEditors.TextEdit();
            this.LueScope = new DevExpress.XtraEditors.LookUpEdit();
            this.lblEstimatedValue = new System.Windows.Forms.Label();
            this.BtnPICCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPICCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueCityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTypeCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LueProcessInd = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePhaseCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteStartDt1 = new DevExpress.XtraEditors.DateEdit();
            this.lblSite = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.lblCostCenter = new System.Windows.Forms.Label();
            this.LueCostCenter = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.DteEndDt1 = new DevExpress.XtraEditors.DateEdit();
            this.LblPGCode = new System.Windows.Forms.Label();
            this.LuePGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.MeeApprovalRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LblApprovalRemark = new System.Windows.Forms.Label();
            this.TcLOP = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LueRouteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnBOQDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtBOQDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeScope.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICSales.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtLetterNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueConfidentLvl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueResource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstimated.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueScope.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTypeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePhaseCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeApprovalRemark.Properties)).BeginInit();
            this.TcLOP.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueRouteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(878, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnBOQDocNo);
            this.panel2.Controls.Add(this.MeeApprovalRemark);
            this.panel2.Controls.Add(this.TxtBOQDocNo);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.LblApprovalRemark);
            this.panel2.Controls.Add(this.LblPGCode);
            this.panel2.Controls.Add(this.LuePGCode);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.lblCostCenter);
            this.panel2.Controls.Add(this.LueCostCenter);
            this.panel2.Controls.Add(this.lblSite);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueProcessInd);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueCtCode);
            this.panel2.Controls.Add(this.TxtProjectName);
            this.panel2.Controls.Add(this.label76);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(878, 271);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.TcLOP);
            this.panel3.Location = new System.Drawing.Point(0, 271);
            this.panel3.Size = new System.Drawing.Size(878, 202);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.TcLOP, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(878, 202);
            this.Grd1.TabIndex = 61;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(7, 29);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(133, 14);
            this.label14.TabIndex = 14;
            this.label14.Text = "Reason for Cancellation";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(144, 27);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 300;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(242, 20);
            this.MeeCancelReason.TabIndex = 15;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(390, 26);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 16;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(130, 215);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(279, 20);
            this.MeeRemark.TabIndex = 59;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(75, 217);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 14);
            this.label10.TabIndex = 58;
            this.label10.Text = "Remark";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(144, 48);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceFocused.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(107, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 17;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(144, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(67, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(144, 132);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 250;
            this.TxtProjectName.Size = new System.Drawing.Size(300, 20);
            this.TxtProjectName.TabIndex = 24;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Red;
            this.label76.Location = new System.Drawing.Point(59, 135);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(81, 14);
            this.label76.TabIndex = 23;
            this.label76.Text = "Project Name";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(81, 177);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 25;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(144, 174);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(300, 20);
            this.LueCtCode.TabIndex = 26;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.MeeScope);
            this.panel5.Controls.Add(this.LuePICSales);
            this.panel5.Controls.Add(this.LblPICSales);
            this.panel5.Controls.Add(this.TxtQtLetterNo);
            this.panel5.Controls.Add(this.LblQtLetterNo);
            this.panel5.Controls.Add(this.LueConfidentLvl);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.BtnCopyData);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.DteEndDt);
            this.panel5.Controls.Add(this.DteStartDt);
            this.panel5.Controls.Add(this.lblResource);
            this.panel5.Controls.Add(this.TxtPICName);
            this.panel5.Controls.Add(this.LueResource);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.lblProjectScope);
            this.panel5.Controls.Add(this.TxtEstimated);
            this.panel5.Controls.Add(this.LueScope);
            this.panel5.Controls.Add(this.lblEstimatedValue);
            this.panel5.Controls.Add(this.BtnPICCode);
            this.panel5.Controls.Add(this.TxtPICCode);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.LueCityCode);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.LueTypeCode);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(458, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(420, 271);
            this.panel5.TabIndex = 34;
            // 
            // MeeScope
            // 
            this.MeeScope.EnterMoveNextControl = true;
            this.MeeScope.Location = new System.Drawing.Point(130, 26);
            this.MeeScope.Margin = new System.Windows.Forms.Padding(5);
            this.MeeScope.Name = "MeeScope";
            this.MeeScope.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeScope.Properties.Appearance.Options.UseFont = true;
            this.MeeScope.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeScope.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeScope.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeScope.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeScope.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeScope.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeScope.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeScope.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeScope.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeScope.Properties.MaxLength = 400;
            this.MeeScope.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeScope.Properties.ShowIcon = false;
            this.MeeScope.Size = new System.Drawing.Size(279, 20);
            this.MeeScope.TabIndex = 40;
            this.MeeScope.ToolTip = "F4 : Show/hide text";
            this.MeeScope.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeScope.ToolTipTitle = "Run System";
            this.MeeScope.Visible = false;
            // 
            // LuePICSales
            // 
            this.LuePICSales.EnterMoveNextControl = true;
            this.LuePICSales.Location = new System.Drawing.Point(130, 272);
            this.LuePICSales.Margin = new System.Windows.Forms.Padding(5);
            this.LuePICSales.Name = "LuePICSales";
            this.LuePICSales.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICSales.Properties.Appearance.Options.UseFont = true;
            this.LuePICSales.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICSales.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePICSales.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICSales.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePICSales.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICSales.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePICSales.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICSales.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePICSales.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePICSales.Properties.DropDownRows = 30;
            this.LuePICSales.Properties.NullText = "[Empty]";
            this.LuePICSales.Properties.PopupWidth = 300;
            this.LuePICSales.Size = new System.Drawing.Size(279, 20);
            this.LuePICSales.TabIndex = 48;
            this.LuePICSales.ToolTip = "F4 : Show/hide list";
            this.LuePICSales.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LblPICSales
            // 
            this.LblPICSales.AutoSize = true;
            this.LblPICSales.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPICSales.ForeColor = System.Drawing.Color.Red;
            this.LblPICSales.Location = new System.Drawing.Point(66, 275);
            this.LblPICSales.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPICSales.Name = "LblPICSales";
            this.LblPICSales.Size = new System.Drawing.Size(56, 14);
            this.LblPICSales.TabIndex = 47;
            this.LblPICSales.Text = "PIC Sales";
            this.LblPICSales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtLetterNo
            // 
            this.TxtQtLetterNo.EnterMoveNextControl = true;
            this.TxtQtLetterNo.Location = new System.Drawing.Point(130, 194);
            this.TxtQtLetterNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtLetterNo.Name = "TxtQtLetterNo";
            this.TxtQtLetterNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQtLetterNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtLetterNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtLetterNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQtLetterNo.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQtLetterNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TxtQtLetterNo.Properties.MaxLength = 50;
            this.TxtQtLetterNo.Size = new System.Drawing.Size(279, 20);
            this.TxtQtLetterNo.TabIndex = 57;
            this.TxtQtLetterNo.Validated += new System.EventHandler(this.TxtQtLetterNo_Validated);
            // 
            // LblQtLetterNo
            // 
            this.LblQtLetterNo.AutoSize = true;
            this.LblQtLetterNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblQtLetterNo.ForeColor = System.Drawing.Color.Black;
            this.LblQtLetterNo.Location = new System.Drawing.Point(9, 196);
            this.LblQtLetterNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblQtLetterNo.Name = "LblQtLetterNo";
            this.LblQtLetterNo.Size = new System.Drawing.Size(109, 14);
            this.LblQtLetterNo.TabIndex = 56;
            this.LblQtLetterNo.Text = "Quotation Letter#";
            this.LblQtLetterNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueConfidentLvl
            // 
            this.LueConfidentLvl.EnterMoveNextControl = true;
            this.LueConfidentLvl.Location = new System.Drawing.Point(130, 173);
            this.LueConfidentLvl.Margin = new System.Windows.Forms.Padding(5);
            this.LueConfidentLvl.Name = "LueConfidentLvl";
            this.LueConfidentLvl.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueConfidentLvl.Properties.Appearance.Options.UseFont = true;
            this.LueConfidentLvl.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueConfidentLvl.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueConfidentLvl.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueConfidentLvl.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueConfidentLvl.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueConfidentLvl.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueConfidentLvl.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueConfidentLvl.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueConfidentLvl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueConfidentLvl.Properties.DropDownRows = 30;
            this.LueConfidentLvl.Properties.NullText = "[Empty]";
            this.LueConfidentLvl.Properties.PopupWidth = 300;
            this.LueConfidentLvl.Size = new System.Drawing.Size(279, 20);
            this.LueConfidentLvl.TabIndex = 55;
            this.LueConfidentLvl.ToolTip = "F4 : Show/hide list";
            this.LueConfidentLvl.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueConfidentLvl.EditValueChanged += new System.EventHandler(this.LueConfidentLvl_EditValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(30, 175);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 14);
            this.label19.TabIndex = 54;
            this.label19.Text = "Confident Level";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCopyData
            // 
            this.BtnCopyData.Appearance.ForeColor = System.Drawing.Color.Black;
            this.BtnCopyData.Appearance.Options.UseForeColor = true;
            this.BtnCopyData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyData.Location = new System.Drawing.Point(315, 241);
            this.BtnCopyData.Name = "BtnCopyData";
            this.BtnCopyData.Size = new System.Drawing.Size(94, 23);
            this.BtnCopyData.TabIndex = 60;
            this.BtnCopyData.Text = "Copy LOP";
            this.BtnCopyData.ToolTip = "Copy LOP Based First Record";
            this.BtnCopyData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyData.ToolTipTitle = "Run System";
            this.BtnCopyData.Click += new System.EventHandler(this.BtnCopyData_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(237, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 14);
            this.label17.TabIndex = 37;
            this.label17.Text = "-";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(80, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 14);
            this.label18.TabIndex = 35;
            this.label18.Text = "Period";
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(253, 5);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(103, 20);
            this.DteEndDt.TabIndex = 38;
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(130, 5);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(103, 20);
            this.DteStartDt.TabIndex = 36;
            // 
            // lblResource
            // 
            this.lblResource.AutoSize = true;
            this.lblResource.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResource.ForeColor = System.Drawing.Color.Black;
            this.lblResource.Location = new System.Drawing.Point(65, 51);
            this.lblResource.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblResource.Name = "lblResource";
            this.lblResource.Size = new System.Drawing.Size(57, 14);
            this.lblResource.TabIndex = 41;
            this.lblResource.Text = "Resource";
            this.lblResource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPICName
            // 
            this.TxtPICName.EnterMoveNextControl = true;
            this.TxtPICName.Location = new System.Drawing.Point(130, 131);
            this.TxtPICName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICName.Name = "TxtPICName";
            this.TxtPICName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPICName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICName.Properties.Appearance.Options.UseFont = true;
            this.TxtPICName.Properties.MaxLength = 16;
            this.TxtPICName.Size = new System.Drawing.Size(279, 20);
            this.TxtPICName.TabIndex = 51;
            // 
            // LueResource
            // 
            this.LueResource.EnterMoveNextControl = true;
            this.LueResource.Location = new System.Drawing.Point(130, 47);
            this.LueResource.Margin = new System.Windows.Forms.Padding(5);
            this.LueResource.Name = "LueResource";
            this.LueResource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResource.Properties.Appearance.Options.UseFont = true;
            this.LueResource.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResource.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueResource.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResource.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueResource.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResource.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueResource.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueResource.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueResource.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueResource.Properties.DropDownRows = 30;
            this.LueResource.Properties.NullText = "[Empty]";
            this.LueResource.Properties.PopupWidth = 300;
            this.LueResource.Size = new System.Drawing.Size(279, 20);
            this.LueResource.TabIndex = 42;
            this.LueResource.ToolTip = "F4 : Show/hide list";
            this.LueResource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueResource.EditValueChanged += new System.EventHandler(this.LueResource_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(62, 134);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 14);
            this.label7.TabIndex = 50;
            this.label7.Text = "PIC Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblProjectScope
            // 
            this.lblProjectScope.AutoSize = true;
            this.lblProjectScope.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProjectScope.ForeColor = System.Drawing.Color.Black;
            this.lblProjectScope.Location = new System.Drawing.Point(33, 30);
            this.lblProjectScope.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblProjectScope.Name = "lblProjectScope";
            this.lblProjectScope.Size = new System.Drawing.Size(89, 14);
            this.lblProjectScope.TabIndex = 39;
            this.lblProjectScope.Text = "Scope of Work";
            this.lblProjectScope.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEstimated
            // 
            this.TxtEstimated.EnterMoveNextControl = true;
            this.TxtEstimated.Location = new System.Drawing.Point(130, 152);
            this.TxtEstimated.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEstimated.Name = "TxtEstimated";
            this.TxtEstimated.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEstimated.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEstimated.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEstimated.Properties.Appearance.Options.UseFont = true;
            this.TxtEstimated.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtEstimated.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtEstimated.Properties.MaxLength = 20;
            this.TxtEstimated.Size = new System.Drawing.Size(198, 20);
            this.TxtEstimated.TabIndex = 53;
            this.TxtEstimated.Validated += new System.EventHandler(this.TxtEstimated_Validated);
            // 
            // LueScope
            // 
            this.LueScope.EnterMoveNextControl = true;
            this.LueScope.Location = new System.Drawing.Point(130, 26);
            this.LueScope.Margin = new System.Windows.Forms.Padding(5);
            this.LueScope.Name = "LueScope";
            this.LueScope.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueScope.Properties.Appearance.Options.UseFont = true;
            this.LueScope.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueScope.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueScope.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueScope.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueScope.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueScope.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueScope.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueScope.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueScope.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueScope.Properties.DropDownRows = 30;
            this.LueScope.Properties.NullText = "[Empty]";
            this.LueScope.Properties.PopupWidth = 300;
            this.LueScope.Size = new System.Drawing.Size(279, 20);
            this.LueScope.TabIndex = 40;
            this.LueScope.ToolTip = "F4 : Show/hide list";
            this.LueScope.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueScope.EditValueChanged += new System.EventHandler(this.LueScope_EditValueChanged);
            // 
            // lblEstimatedValue
            // 
            this.lblEstimatedValue.AutoSize = true;
            this.lblEstimatedValue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstimatedValue.ForeColor = System.Drawing.Color.Black;
            this.lblEstimatedValue.Location = new System.Drawing.Point(27, 154);
            this.lblEstimatedValue.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblEstimatedValue.Name = "lblEstimatedValue";
            this.lblEstimatedValue.Size = new System.Drawing.Size(95, 14);
            this.lblEstimatedValue.TabIndex = 52;
            this.lblEstimatedValue.Text = "Estimated Value";
            this.lblEstimatedValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPICCode
            // 
            this.BtnPICCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPICCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPICCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPICCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPICCode.Appearance.Options.UseBackColor = true;
            this.BtnPICCode.Appearance.Options.UseFont = true;
            this.BtnPICCode.Appearance.Options.UseForeColor = true;
            this.BtnPICCode.Appearance.Options.UseTextOptions = true;
            this.BtnPICCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPICCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPICCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnPICCode.Image")));
            this.BtnPICCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPICCode.Location = new System.Drawing.Point(331, 108);
            this.BtnPICCode.Name = "BtnPICCode";
            this.BtnPICCode.Size = new System.Drawing.Size(24, 21);
            this.BtnPICCode.TabIndex = 49;
            this.BtnPICCode.ToolTip = "Find PIC";
            this.BtnPICCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPICCode.ToolTipTitle = "Run System";
            this.BtnPICCode.Click += new System.EventHandler(this.BtnPICCode_Click);
            // 
            // TxtPICCode
            // 
            this.TxtPICCode.EnterMoveNextControl = true;
            this.TxtPICCode.Location = new System.Drawing.Point(130, 110);
            this.TxtPICCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICCode.Name = "TxtPICCode";
            this.TxtPICCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPICCode.Properties.MaxLength = 16;
            this.TxtPICCode.Size = new System.Drawing.Size(198, 20);
            this.TxtPICCode.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(65, 114);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 14);
            this.label6.TabIndex = 47;
            this.label6.Text = "PIC Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(94, 93);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 14);
            this.label5.TabIndex = 45;
            this.label5.Text = "City";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCityCode
            // 
            this.LueCityCode.EnterMoveNextControl = true;
            this.LueCityCode.Location = new System.Drawing.Point(130, 89);
            this.LueCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCityCode.Name = "LueCityCode";
            this.LueCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.Appearance.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCityCode.Properties.DropDownRows = 30;
            this.LueCityCode.Properties.NullText = "[Empty]";
            this.LueCityCode.Properties.PopupWidth = 300;
            this.LueCityCode.Size = new System.Drawing.Size(279, 20);
            this.LueCityCode.TabIndex = 46;
            this.LueCityCode.ToolTip = "F4 : Show/hide list";
            this.LueCityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCityCode.EditValueChanged += new System.EventHandler(this.LueCityCode_EditValueChanged);
            // 
            // LueTypeCode
            // 
            this.LueTypeCode.EnterMoveNextControl = true;
            this.LueTypeCode.Location = new System.Drawing.Point(130, 68);
            this.LueTypeCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTypeCode.Name = "LueTypeCode";
            this.LueTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.Appearance.Options.UseFont = true;
            this.LueTypeCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTypeCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTypeCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTypeCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTypeCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTypeCode.Properties.DropDownRows = 30;
            this.LueTypeCode.Properties.NullText = "[Empty]";
            this.LueTypeCode.Properties.PopupWidth = 300;
            this.LueTypeCode.Size = new System.Drawing.Size(279, 20);
            this.LueTypeCode.TabIndex = 44;
            this.LueTypeCode.ToolTip = "F4 : Show/hide list";
            this.LueTypeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTypeCode.EditValueChanged += new System.EventHandler(this.LueTypeCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(86, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 14);
            this.label4.TabIndex = 43;
            this.label4.Text = "Type";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(92, 114);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "Process";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProcessInd
            // 
            this.LueProcessInd.EnterMoveNextControl = true;
            this.LueProcessInd.Location = new System.Drawing.Point(144, 111);
            this.LueProcessInd.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcessInd.Name = "LueProcessInd";
            this.LueProcessInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.Appearance.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcessInd.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcessInd.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcessInd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcessInd.Properties.DropDownRows = 5;
            this.LueProcessInd.Properties.NullText = "[Empty]";
            this.LueProcessInd.Properties.PopupWidth = 350;
            this.LueProcessInd.Size = new System.Drawing.Size(242, 20);
            this.LueProcessInd.TabIndex = 22;
            this.LueProcessInd.ToolTip = "F4 : Show/hide list";
            this.LueProcessInd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProcessInd.EditValueChanged += new System.EventHandler(this.LueProcessInd_EditValueChanged);
            // 
            // LuePhaseCode
            // 
            this.LuePhaseCode.EnterMoveNextControl = true;
            this.LuePhaseCode.Location = new System.Drawing.Point(516, 76);
            this.LuePhaseCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePhaseCode.Name = "LuePhaseCode";
            this.LuePhaseCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.Appearance.Options.UseFont = true;
            this.LuePhaseCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePhaseCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePhaseCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePhaseCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePhaseCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePhaseCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePhaseCode.Properties.DropDownRows = 20;
            this.LuePhaseCode.Properties.NullText = "[Empty]";
            this.LuePhaseCode.Properties.PopupWidth = 500;
            this.LuePhaseCode.Size = new System.Drawing.Size(171, 20);
            this.LuePhaseCode.TabIndex = 60;
            this.LuePhaseCode.ToolTip = "F4 : Show/hide list";
            this.LuePhaseCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePhaseCode.EditValueChanged += new System.EventHandler(this.LuePhaseCode_EditValueChanged);
            this.LuePhaseCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePhaseCode_KeyDown);
            this.LuePhaseCode.Leave += new System.EventHandler(this.LuePhaseCode_Leave);
            // 
            // DteStartDt1
            // 
            this.DteStartDt1.EditValue = null;
            this.DteStartDt1.EnterMoveNextControl = true;
            this.DteStartDt1.Location = new System.Drawing.Point(157, 76);
            this.DteStartDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt1.Name = "DteStartDt1";
            this.DteStartDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt1.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt1.Size = new System.Drawing.Size(125, 20);
            this.DteStartDt1.TabIndex = 58;
            this.DteStartDt1.Visible = false;
            this.DteStartDt1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDt1_KeyDown);
            this.DteStartDt1.Leave += new System.EventHandler(this.DteStartDt1_Leave);
            // 
            // lblSite
            // 
            this.lblSite.AutoSize = true;
            this.lblSite.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSite.ForeColor = System.Drawing.Color.Red;
            this.lblSite.Location = new System.Drawing.Point(112, 198);
            this.lblSite.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblSite.Name = "lblSite";
            this.lblSite.Size = new System.Drawing.Size(28, 14);
            this.lblSite.TabIndex = 27;
            this.lblSite.Text = "Site";
            this.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(144, 195);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(300, 20);
            this.LueSiteCode.TabIndex = 28;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // lblCostCenter
            // 
            this.lblCostCenter.AutoSize = true;
            this.lblCostCenter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCostCenter.ForeColor = System.Drawing.Color.Black;
            this.lblCostCenter.Location = new System.Drawing.Point(68, 219);
            this.lblCostCenter.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblCostCenter.Name = "lblCostCenter";
            this.lblCostCenter.Size = new System.Drawing.Size(72, 14);
            this.lblCostCenter.TabIndex = 29;
            this.lblCostCenter.Text = "Cost Center";
            this.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LueCostCenter
            // 
            this.LueCostCenter.EnterMoveNextControl = true;
            this.LueCostCenter.Location = new System.Drawing.Point(144, 216);
            this.LueCostCenter.Margin = new System.Windows.Forms.Padding(5);
            this.LueCostCenter.Name = "LueCostCenter";
            this.LueCostCenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenter.Properties.Appearance.Options.UseFont = true;
            this.LueCostCenter.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenter.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCostCenter.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenter.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCostCenter.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenter.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCostCenter.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenter.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCostCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCostCenter.Properties.DropDownRows = 30;
            this.LueCostCenter.Properties.NullText = "[Empty]";
            this.LueCostCenter.Properties.PopupWidth = 300;
            this.LueCostCenter.Size = new System.Drawing.Size(300, 20);
            this.LueCostCenter.TabIndex = 30;
            this.LueCostCenter.ToolTip = "F4 : Show/hide list";
            this.LueCostCenter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCostCenter.EditValueChanged += new System.EventHandler(this.LueCostCenter_EditValueChanged);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(144, 69);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(242, 20);
            this.TxtStatus.TabIndex = 20;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(98, 72);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 14);
            this.label16.TabIndex = 19;
            this.label16.Text = "Status";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteEndDt1
            // 
            this.DteEndDt1.EditValue = null;
            this.DteEndDt1.EnterMoveNextControl = true;
            this.DteEndDt1.Location = new System.Drawing.Point(315, 76);
            this.DteEndDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt1.Name = "DteEndDt1";
            this.DteEndDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt1.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt1.Size = new System.Drawing.Size(125, 20);
            this.DteEndDt1.TabIndex = 59;
            this.DteEndDt1.Visible = false;
            this.DteEndDt1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEndDt1_KeyDown);
            this.DteEndDt1.Leave += new System.EventHandler(this.DteEndDt1_Leave);
            // 
            // LblPGCode
            // 
            this.LblPGCode.AutoSize = true;
            this.LblPGCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPGCode.ForeColor = System.Drawing.Color.Black;
            this.LblPGCode.Location = new System.Drawing.Point(57, 156);
            this.LblPGCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPGCode.Name = "LblPGCode";
            this.LblPGCode.Size = new System.Drawing.Size(83, 14);
            this.LblPGCode.TabIndex = 25;
            this.LblPGCode.Text = "Project Group";
            this.LblPGCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePGCode
            // 
            this.LuePGCode.EnterMoveNextControl = true;
            this.LuePGCode.Location = new System.Drawing.Point(144, 153);
            this.LuePGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePGCode.Name = "LuePGCode";
            this.LuePGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.Appearance.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePGCode.Properties.DropDownRows = 30;
            this.LuePGCode.Properties.NullText = "[Empty]";
            this.LuePGCode.Properties.PopupWidth = 300;
            this.LuePGCode.Size = new System.Drawing.Size(300, 20);
            this.LuePGCode.TabIndex = 25;
            this.LuePGCode.ToolTip = "F4 : Show/hide list";
            this.LuePGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePGCode.EditValueChanged += new System.EventHandler(this.LuePGCode_EditValueChanged);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // MeeApprovalRemark
            // 
            this.MeeApprovalRemark.EnterMoveNextControl = true;
            this.MeeApprovalRemark.Location = new System.Drawing.Point(144, 90);
            this.MeeApprovalRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeApprovalRemark.Name = "MeeApprovalRemark";
            this.MeeApprovalRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeApprovalRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeApprovalRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeApprovalRemark.Properties.MaxLength = 1000;
            this.MeeApprovalRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeApprovalRemark.Properties.ShowIcon = false;
            this.MeeApprovalRemark.Size = new System.Drawing.Size(242, 20);
            this.MeeApprovalRemark.TabIndex = 46;
            this.MeeApprovalRemark.ToolTip = "F4 : Show/hide text";
            this.MeeApprovalRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeApprovalRemark.ToolTipTitle = "Run System";
            // 
            // LblApprovalRemark
            // 
            this.LblApprovalRemark.AutoSize = true;
            this.LblApprovalRemark.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblApprovalRemark.Location = new System.Drawing.Point(34, 94);
            this.LblApprovalRemark.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblApprovalRemark.Name = "LblApprovalRemark";
            this.LblApprovalRemark.Size = new System.Drawing.Size(106, 14);
            this.LblApprovalRemark.TabIndex = 45;
            this.LblApprovalRemark.Text = "Approval\'s Remark";
            this.LblApprovalRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TcLOP
            // 
            this.TcLOP.Controls.Add(this.tabPage1);
            this.TcLOP.Controls.Add(this.tabPage2);
            this.TcLOP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcLOP.Location = new System.Drawing.Point(0, 0);
            this.TcLOP.Name = "TcLOP";
            this.TcLOP.SelectedIndex = 0;
            this.TcLOP.Size = new System.Drawing.Size(878, 202);
            this.TcLOP.TabIndex = 77;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LueRouteCode);
            this.tabPage1.Controls.Add(this.LuePhaseCode);
            this.tabPage1.Controls.Add(this.DteEndDt1);
            this.tabPage1.Controls.Add(this.DteStartDt1);
            this.tabPage1.Controls.Add(this.Grd2);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(870, 175);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LueRouteCode
            // 
            this.LueRouteCode.EnterMoveNextControl = true;
            this.LueRouteCode.Location = new System.Drawing.Point(11, 76);
            this.LueRouteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueRouteCode.Name = "LueRouteCode";
            this.LueRouteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRouteCode.Properties.Appearance.Options.UseFont = true;
            this.LueRouteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRouteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueRouteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRouteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueRouteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRouteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueRouteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueRouteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueRouteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueRouteCode.Properties.DropDownRows = 20;
            this.LueRouteCode.Properties.NullText = "[Empty]";
            this.LueRouteCode.Properties.PopupWidth = 500;
            this.LueRouteCode.Size = new System.Drawing.Size(125, 20);
            this.LueRouteCode.TabIndex = 78;
            this.LueRouteCode.ToolTip = "F4 : Show/hide list";
            this.LueRouteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueRouteCode.EditValueChanged += new System.EventHandler(this.LueRouteCode_EditValueChanged);
            this.LueRouteCode.Leave += new System.EventHandler(this.LueRouteCode_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(3, 3);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(864, 169);
            this.Grd2.TabIndex = 77;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.Grd3);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(764, 175);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Approval Information";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(3, 3);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(758, 169);
            this.Grd3.TabIndex = 80;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnBOQDocNo
            // 
            this.BtnBOQDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBOQDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBOQDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOQDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBOQDocNo.Appearance.Options.UseBackColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseFont = true;
            this.BtnBOQDocNo.Appearance.Options.UseForeColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnBOQDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBOQDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBOQDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnBOQDocNo.Image")));
            this.BtnBOQDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBOQDocNo.Location = new System.Drawing.Point(412, 235);
            this.BtnBOQDocNo.Name = "BtnBOQDocNo";
            this.BtnBOQDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnBOQDocNo.TabIndex = 33;
            this.BtnBOQDocNo.ToolTip = "Find PIC";
            this.BtnBOQDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBOQDocNo.ToolTipTitle = "Run System";
            this.BtnBOQDocNo.Click += new System.EventHandler(this.BtnBOQDocNo_Click);
            // 
            // TxtBOQDocNo
            // 
            this.TxtBOQDocNo.EnterMoveNextControl = true;
            this.TxtBOQDocNo.Location = new System.Drawing.Point(144, 237);
            this.TxtBOQDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBOQDocNo.Name = "TxtBOQDocNo";
            this.TxtBOQDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBOQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBOQDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBOQDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBOQDocNo.Properties.MaxLength = 16;
            this.TxtBOQDocNo.Size = new System.Drawing.Size(265, 20);
            this.TxtBOQDocNo.TabIndex = 32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(45, 240);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 14);
            this.label9.TabIndex = 31;
            this.label9.Text = "Bill of Quantity#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmLOP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 473);
            this.Name = "FrmLOP";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeScope.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICSales.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtLetterNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueConfidentLvl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueResource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEstimated.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueScope.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTypeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcessInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePhaseCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeApprovalRemark.Properties)).EndInit();
            this.TcLOP.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueRouteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.LookUpEdit LueCtCode;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.LookUpEdit LueTypeCode;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.LookUpEdit LueCityCode;
        public DevExpress.XtraEditors.SimpleButton BtnPICCode;
        internal DevExpress.XtraEditors.TextEdit TxtPICCode;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtPICName;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtEstimated;
        private System.Windows.Forms.Label lblEstimatedValue;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueProcessInd;
        private DevExpress.XtraEditors.LookUpEdit LuePhaseCode;
        internal DevExpress.XtraEditors.DateEdit DteStartDt1;
        private System.Windows.Forms.Label lblSite;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label lblResource;
        public DevExpress.XtraEditors.LookUpEdit LueResource;
        private System.Windows.Forms.Label lblProjectScope;
        public DevExpress.XtraEditors.LookUpEdit LueScope;
        private System.Windows.Forms.Label lblCostCenter;
        public DevExpress.XtraEditors.LookUpEdit LueCostCenter;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        internal DevExpress.XtraEditors.DateEdit DteEndDt1;
        public DevExpress.XtraEditors.SimpleButton BtnCopyData;
        private System.Windows.Forms.Label label19;
        public DevExpress.XtraEditors.LookUpEdit LueConfidentLvl;
        private System.Windows.Forms.Label LblPGCode;
        public DevExpress.XtraEditors.LookUpEdit LuePGCode;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        internal DevExpress.XtraEditors.TextEdit TxtQtLetterNo;
        private System.Windows.Forms.Label LblQtLetterNo;
        private DevExpress.XtraEditors.MemoExEdit MeeApprovalRemark;
        private System.Windows.Forms.Label LblApprovalRemark;
        private System.Windows.Forms.TabControl TcLOP;
        private System.Windows.Forms.TabPage tabPage1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.TabPage tabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraEditors.LookUpEdit LueRouteCode;
        public DevExpress.XtraEditors.SimpleButton BtnBOQDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtBOQDocNo;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.LookUpEdit LuePICSales;
        private System.Windows.Forms.Label LblPICSales;
        private DevExpress.XtraEditors.MemoExEdit MeeScope;
    }
}