﻿#region Update
/*
    20/10/2017 [TKG] untuk uang muka, nomor rekening coa yg dibuat akan memiliki tipe credit.  
    17/11/2017 [TKG] merubah deskripsi nomor rekening COA yg berhubngan dengan vendor apabila nama vendor diubah. 
    31/01/2018 [HAR] tambah inputan NPWP, dan customer ShortCode
    28/02/2018 [HAR] tambah inputan asset yang masuk ke rent ind / Sold ind 
    03/04/2018 [HAR] tambah parameter IsShippingAddressAliasActive buat nentuin alias di shiiping address
    16/04/2018 [HAR] tambah nomor contract dan remark di tab asset
    26/04/2018 [HAR] tambah inputan NIB (Nomor Induk Berusaha)
    06/06/2018 [TKG] panjang customer name menjadi 1000 karakter.
    08/06/2018 [TKG] tambah customer group
    16/07/2018 [WED] tambah shipping mark, untuk keperluan stuffing (create packing list)
    18/07/2018 [HAR] tambah parameter buat nenetuin otomatis kebikin COA nya apa ndak
    21/07/2018 [TKG] bug saat edit data
    17/09/2019 [TKG] city berdasarkan country yg telah dipilih.
    25/10/2019 [TKG/KBN] informasi wide di bagian asset bisa diedit manual, tambah kolom due date.
    12/11/2019 [VIN/SIER] menambah kolom luasan di menu Customer SHIPPING ADDRESS (data with BRD )
    26/11/2019 [TKG/SIER] otomatis generate short code
    08/06/2020 [IBL/MMM] Membuat COA otomatis berdasarkan kategori Customer yang dikombinasikan dengan CtCtCode. Dengan parameter IsCustomerCOAUseCustomerCategory
    10/12/2020 [DITA/KIM] Tambah tab baru untuk insert virtual account
 *  10/03/2021 [ICA/SIER] Fitur Copy Data header, tab contact person, tab bank account dan shipping Adress  based on IsCustomerAllowToCopyData
 *  16/04/2021 [ICA/KIM] Menambah Email di Tab Contact Person
 *  16/05/2021 [VIN/SIER] customer category tidak bisa di edit
 *  28/05/2021 [MYA/SIER] Kolom category pada menu Customer Data terfilter berdasarkan grup log in
 *  03/06/2021 [VIN/SIER] Bug Customer Category
 *  13/09/2021 [SET/KIM] Menambahkan attachment upload di master customer
    21/10/2021 [DITA/KIM] Menambahkan fitur view file dari ftp + tambah parameter baru
    25/10/2021 [DITA/KIM] Tambah fitur downlad all files
    30/13/2022 [VIN/ALL] BUG grd key down grd1
    24/01/2023 [IBL/MNET] Tambah field Customer Segmentation berdasarkan param IsCustomerDataUseCustomerSegmentation
    02/02/2023 [DITA/MNET] tambah value 3 untuk param GenerateCustomerCOAFormat
    03/03/2023 [WED/KBN] tambah kolom input Water Meter berdasarkan parameter IsCustomerShpAddressDisplayWaterMeter
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;
using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmCustomer : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mCtCode = string.Empty,
            mCustomerParam = string.Empty,

            mHostAddrForFTPClient = string.Empty,
            mPortForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;
        private int mCtCodeDefaultLength = 8;
        private bool IsInsert = false;
        private bool 
            mIsCustomerShippingAddressFormatDNoCustomized = false,
            mIsShippingAddressAliasActive = false,
            mIsCtAssetWideEditabled = false,
            mIsCtAssetDueDtEnabled = false,
            mIsCustomerShpAddressDisplayWide = false,
            mIsAutoGenerateCtShortCode = false,
            mIsCustomerUseBankVirtualAccount = false,
            mIsCustomerAllowToCopyData = false,
            mIsCustomerCategoryFilteredByGroup = false;
        private string
            mCustomerAcNoDownPayment = string.Empty,
            mCustomerAcDescDownPayment = string.Empty,
            mAcNoForGiroAR = string.Empty,
            mAcDescForGiroAR= string.Empty,
            mCustomerAcNoAR = string.Empty,
            mCustomerAcNoNonInvoice = string.Empty,
            mCustomerAcDescAR = string.Empty,
            mCustomerAcDescNonInvoice = string.Empty,
            mFormatFTPClient = string.Empty,
            mEditableBankCodeVirtualAccount = string.Empty,
            mDeptCodeForCustomerViewFTP = string.Empty,
            mDeptCodeForCustomerUploadDownloadAndViewFTP = string.Empty,
            mPathToSaveDownloadedFilesFromFtp = string.Empty
            ;
        private byte[] downloadedData;
        internal string mGenerateCustomerCOAFormat = string.Empty, DocTitle = string.Empty;
        internal bool
            mIsCustomerItemNameMandatory = false,
            mIsCustomerCOAUseCustomerCategory = false,
            mCopyDataActive = false,
            mIsCustomerUseAttachment = false,
            mIsCustomerDataUseCustomerSegmentation = false,
            mIsCustomerShpAddressDisplayWaterMeter = false
            ;
        iGCell fCell;
        bool fAccept;
        internal FrmCustomerFind FrmFind;

        #endregion

        #region Constructor

        public FrmCustomer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Customer";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                ExecQuery();
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueCtCtCode(ref LueCtCtCode);
                Sl.SetLueCntCode(ref LueCountryCode);
                Sl.SetLueOption(ref LueCtSegmentCode, "CustomerSegmentation");
                //Sl.SetLueCityCode(ref LueCityCode);
                if (!mIsCustomerDataUseCustomerSegmentation)
                    LblCtSegmentCode.Visible = LueCtSegmentCode.Visible = false;
                LueBankCode.Visible = false;
                LueBankCode2.Visible = false;
                LueCity2.Visible = false;
                LueCntCode.Visible = false;
                MeeNotify.Visible = false;
                if (!mIsCustomerUseBankVirtualAccount) tabControl1.TabPages.Remove(TpVA);
                if (!mIsCustomerUseAttachment) tabControl1.TabPages.Remove(TpFiles);
                base.FrmLoad(sender, e);

                mCopyDataActive = false;
                LblCopyGeneral.Visible = false;
                BtnCopyGeneral.Visible = false;
                BtnCopyGeneral.Enabled = false;
                //if this application is called from other application
                if (mCtCode.Length != 0)
                {
                    ShowData(mCtCode);
                    if (mMenuCode.Length == 0)
                        BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                    else
                        SetFormControl(mState.Edit);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        "Contact Person Name",
                        "Position",
                        "Contact Number", 
                        "Email"
                    },
                     new int[] 
                    {
                        300, 200, 200, 200
                    }
                );
           
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Code", "Bank Name", "Branch Name", "Account Name", "Account Number", "Swift Code" },
                    new int[] { 50, 250, 150, 200, 180, 80 }
                );
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 14;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] { 
                        "Name",
                        "Address", "CountryCode", "Country", "CityCode", "City",  
                        mIsShippingAddressAliasActive?"Wide":"PostalCode", "Phone", mIsShippingAddressAliasActive?"Item Product":"Fax", "Email Address", mIsShippingAddressAliasActive?"Contact person":"Mobile", 
                        mIsShippingAddressAliasActive?"Contract Document":"Remark", "Wide", "Water Meter" },
                    new int[] { 150, 200, 80, 100, 80, 200, 80, 100, 100, 150, 100, 200, 100, 150 }
                );
            Sm.GrdColInvisible(Grd3, new int[] { 2, 4 }, false);
            Sm.GrdFormatDec(Grd3, new int[] { 12 }, 0);
            if (!mIsCustomerShpAddressDisplayWide) Sm.GrdColInvisible(Grd3, new int[] { 12 }, false);
            
            if (mIsCustomerShpAddressDisplayWaterMeter) Grd3.Cols[13].Move(6);
            else Sm.GrdColInvisible(Grd3, new int[] { 12 });

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 1;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] { "Notify Party" },
                    new int[] { 400 }
                );

            #endregion

            #region Grid 5

            Grd5.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] { "", "Item's Code", "Item's Name", "Customer's" + Environment.NewLine + "Item Code", "Customer's"+Environment.NewLine+"Item Name" },
                    new int[]{ 20, 150, 250, 150, 250 }
                );
            Sm.GrdColButton(Grd5, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 1, 2 });

            #endregion

            #region Grid 6

            Grd6.Cols.Count = 10;
            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] { 
                        //0
                        "",
                        //1-5
                        "Asset Code", "Asset Name", "Display Name", "Wide", "UoM", 
                        //6-9
                        "Location", "Contract#", "Remark", "Due Date" 
                    },
                    new int[] { 
                        20, 
                        130, 200, 200, 100, 80, 
                        250, 150, 250, 100 }
                );
            Sm.GrdFormatDate(Grd6, new int[] { 9 });
            Sm.GrdColButton(Grd6, new int[] { 0 });
            Sm.GrdFormatDec(Grd6, new int[] { 4 }, 0);
            Sm.GrdColReadOnly(true, true, Grd6, new int[] { 1, 6, 7, 8 });
            if (!mIsCtAssetDueDtEnabled) Sm.GrdColInvisible(Grd6, new int[] { 9 }, false);

            #endregion

            #region Grid 7

            Grd7.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[] { "Code", "Bank Name", "VA Account#", "EditableInd"},
                    new int[] { 50, 250, 180, 0}
                );
            Sm.GrdColInvisible(Grd7, new int[] { 0, 3}, false);

            #endregion

            #region Grid 8

            Grd8.Cols.Count = 6;
            //Grd8.FrozenArea.ColCount = 0;

            Sm.GrdHdrWithColWidth(
                    Grd8,
                    new string[] 
                    {
                        //0
                        "File Name",

                        //1-5
                        "Remark",
                        "U",
                        "D",
                        "File Name2",
                        "V"

                        
                    },
                     new int[] 
                    {
                        //0
                        250,  

                        //1-5
                        250, 20, 20, 250,20
                    }
                );

            Sm.GrdColInvisible(Grd8, new int[] { 4 }, false);
            Sm.GrdColButton(Grd8, new int[] { 2 }, "1");
            Sm.GrdColButton(Grd8, new int[] { 3 }, "2");
            Sm.GrdColButton(Grd8, new int[] { 5 }, "3");
            Sm.GrdColReadOnly(Grd8, new int[] { 0, 1, 2, 4 }, true);

            if (IsUserViewFtpOnly())
                Sm.GrdColInvisible(Grd8, new int[] { 2, 3, 4 }, false);

            else if (IsUserCanUploadDownloadFtp())
                Sm.GrdColInvisible(Grd8, new int[] { 4 }, false);
            else
                Sm.GrdColInvisible(Grd8, new int[] { 2, 3, 4, 5 }, false);




            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCtCode, TxtCtName, ChkActInd, ChkAgentInd, LueCtCtCode, 
                        LueCtGrpCode, MeeAddress, LueCityCode, LueCountryCode, TxtPostalCd, 
                        TxtPhone, TxtFax, TxtEmail, TxtMobile, LueBankCode, 
                        LueCity2, LueCntCode, MeeRemark, MeeNotify, TxtCtShortCode, 
                        TxtNPWP, TxtNIB, MeeShippingMark, LueBankCode2, LueCtSegmentCode
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;
                    Grd7.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 4 });
                    Sm.GrdColReadOnly(true, true, Grd8, new int[] { 1, 2 });
                    LblCopyGeneral.Visible = BtnCopyGeneral.Visible = BtnCopyGeneral.Enabled = false;
                    TxtCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(TxtCtCode, (mCustomerParam == "Y"));
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            TxtCtName, ChkAgentInd, LueCtCtCode, LueCtGrpCode, MeeAddress, 
                            LueCity2, LueCountryCode, TxtPostalCd, TxtPhone, TxtFax, 
                            TxtEmail, TxtMobile, LueBankCode, LueCntCode, MeeRemark, 
                            MeeNotify, TxtCtShortCode, TxtNPWP, TxtNIB, MeeShippingMark, LueBankCode2, LueCtSegmentCode
                        }, false);
                    if (mIsAutoGenerateCtShortCode) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtCtShortCode }, true);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    Grd7.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 0, 6, 7, 8, 9 });
                    Sm.GrdColReadOnly(false, true, Grd7, new int[] { 1 });
                    if (IsUserCanUploadDownloadFtp()) Sm.GrdColReadOnly(false, true, Grd8, new int[] { 1, 2 });
                    if (mIsCtAssetWideEditabled) Sm.GrdColReadOnly(false, true, Grd6, new int[] { 4 });
                    if (mIsCustomerAllowToCopyData) LblCopyGeneral.Visible = BtnCopyGeneral.Visible = BtnCopyGeneral.Enabled = true;
                    TxtCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCtName, ChkActInd, ChkAgentInd, LueCtCtCode, LueCtGrpCode, 
                        MeeAddress, LueCityCode, TxtPostalCd, LueCountryCode, TxtPhone, 
                        TxtFax, TxtEmail, TxtMobile, MeeRemark, LueBankCode, 
                        LueCity2, LueCntCode, MeeNotify, TxtCtShortCode, TxtNPWP, 
                        TxtNIB , MeeShippingMark, LueBankCode2, LueCtSegmentCode
                    }, false);
                    if (mIsCustomerCOAUseCustomerCategory) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode }, true);
                    if (mIsAutoGenerateCtShortCode) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtCtShortCode }, true);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    Grd7.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 0, 3, 4 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 0, 6, 7, 8, 9 });
                    if (IsUserCanUploadDownloadFtp()) Sm.GrdColReadOnly(false, true, Grd8, new int[] { 1, 2 });
                    if (mIsCtAssetWideEditabled) Sm.GrdColReadOnly(false, true, Grd6, new int[] { 4 });
                    for (int i = 0; i < Grd7.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd7, i, 3) == "N") Grd7.Rows[i].ReadOnly = iGBool.True;
                        if (Sm.GetGrdStr(Grd7, i, 2).Length > 0) Sm.GrdColReadOnly(true, true, Grd7, new int[] { 1 });
                        else Sm.GrdColReadOnly(false, true, Grd7, new int[] { 1, 2 });

                    }
                    LblCopyGeneral.Visible = BtnCopyGeneral.Visible = BtnCopyGeneral.Enabled = false;
                    TxtCtName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCtCode, TxtCtName, LueCtCtCode, LueCtGrpCode, MeeAddress, 
                LueCityCode, LueCity2, LueCntCode, TxtPostalCd, TxtPhone, 
                TxtFax, TxtEmail, TxtMobile, LueBankCode, LueCountryCode, 
                MeeRemark, MeeNotify, TxtCtShortCode, TxtNPWP, TxtNIB, MeeShippingMark,
                LueCtSegmentCode
            });
            ChkActInd.Checked = false;
            ChkAgentInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            ClearGrd3();
            Sm.ClearGrd(Grd4, true);
            Sm.ClearGrd(Grd5, true);
            ClearGrd6();
            Sm.ClearGrd(Grd7, true);
            Sm.ClearGrd(Grd8, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.FocusGrd(Grd3, 0, 0);
            Sm.FocusGrd(Grd4, 0, 0);
            Sm.FocusGrd(Grd5, 0, 0);
            Sm.FocusGrd(Grd6, 0, 0);
            Sm.FocusGrd(Grd7, 0, 0);
            Sm.FocusGrd(Grd8, 0, 0);
            PbDownload.Value = 0;
        }

        private void ClearGrd3()
        {
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 12 });
        }

        private void ClearGrd6()
        {
            Sm.ClearGrd(Grd6, true);
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCustomerFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                IsInsert = true;
                Sl.SetLueCtGrpCode(ref LueCtGrpCode, string.Empty, "Y");
                mCopyDataActive = false;
                if (mIsCustomerCategoryFilteredByGroup)
                {
                    SetLueCtCtCode(ref LueCtCtCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCtCode, "", false)) return;
            SetFormControl(mState.Edit);
            
            IsInsert = false;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblCustomer Where CtCode=@CtCode" };
                Sm.CmParam<String>(ref cm, "@CtCode", TxtCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string CtCode = TxtCtCode.Text;
                if (mCustomerParam == "Y" && TxtCtCode.Text.Length == 0)
                {
                    if (!mIsCustomerCOAUseCustomerCategory)
                        CtCode = GenerateCtCode();
                    else
                        CtCode = GenerateCtCode2();
                }

                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid(CtCode) || IsUploadMaxFile()) return;

                var IsLocalCustomer = IsCustomerLocal();
                var IsCtNameEdited = false;
                if (TxtCtCode.Text.Length > 0 && TxtCtCode.Properties.ReadOnly)
                    IsCtNameEdited = IsUserEditCtName();

                var cml = new List<MySqlCommand>();

                cml.Add(SaveCustomer(CtCode, IsLocalCustomer, IsCtNameEdited));
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveCustomerContactPerson(CtCode, Row));
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveCustomerBankAccount(CtCode, Row));
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0) cml.Add(SaveCustomerShipAddress(CtCode, Row));
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 0).Length > 0) cml.Add(SaveCustomerNotifyParty(CtCode, Row));
                for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0) cml.Add(SaveCustomerItem(CtCode, Row));
                for (int Row = 0; Row < Grd6.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0) cml.Add(SaveCustomerAsset(CtCode, Row));
                for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd7, Row, 0).Length > 0) cml.Add(SaveCustomerVirtualAccount(CtCode, Row));


                //Attachment
                if (mIsCustomerUseAttachment)
                {
                    if (Grd8.Rows.Count > 1)
                    {

                        cml.Add(DeleteCustomerAttachment(CtCode));
                        for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd8, Row, 0).Length > 0)
                            {
                                if (!IsUploadFileNotValid(Row, (Sm.GetGrdStr(Grd8, Row, 0))))
                                {
                                    cml.Add(SaveCustomerAttachment(CtCode, Row));
                                }
                                else
                                {
                                    return;
                                }
                            }
                    }
                }

                Sm.ExecCommands(cml);


                if (mIsCustomerUseAttachment)
                {
                    for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd8, Row, 0).Length > 0)
                        {
                            if (Sm.GetGrdStr(Grd8, Row, 0).Length > 0 && Sm.GetGrdStr(Grd8, Row, 0) != "openFileDialog1")
                            {
                                if (Sm.GetGrdStr(Grd8, Row, 0) != Sm.GetGrdStr(Grd8, Row, 4))
                                {
                                    UploadFile(CtCode, Row, Sm.GetGrdStr(Grd8, Row, 0));
                                }
                            }
                        }
                    }
                }

                ShowData(CtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }


        private void BtnDownloadAll_Click(object sender, EventArgs e)
        {
            string[] files = GetFileList();
            if (files != null)
            {
                foreach (string file in files)
                {
                    DownloadAllFile(file, mHostAddrForFTPClient, mPortForFTPClient, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient, mPathToSaveDownloadedFilesFromFtp);

                    FileStream newFile = new FileStream(mPathToSaveDownloadedFilesFromFtp + "/" + DocTitle + "/" + file, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();

                }
                if (downloadedData != null && downloadedData.Length != 0)
                {
                    Application.DoEvents();

                    Sm.StdMsg(mMsgType.Info, "Successfully Downloaded All Files");
                }

            }
            else
                Sm.StdMsg(mMsgType.Warning, "No File Downloaded");

        }

        #endregion

        #region Show Data

        public void ShowData(string CtCode)
        {
            try
            {
                ClearData();
                ShowCustomer(CtCode);
                ShowCustomerContactPerson(CtCode);
                ShowCustomerBankAccount(CtCode);
                ShowCustomerShipAddress(CtCode);
                ShowCustomerNotifyParty(CtCode);
                ShowCustomerItem(CtCode);
                ShowCustomerAsset(CtCode);
                ShowCustomerVirtualAccount(CtCode);
                ShowCustomerAttachment(CtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCustomer(string CtCode)
        {
            string CntCode = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select CtCode, CtName, ActInd, AgentInd, CtCtCode, CtGrpCode, CntCode, Address, CityCode, PostalCd, Phone, ");
            SQL.AppendLine("Fax, Email, Mobile, CtShortCode, NPWP, NIB, Remark, ShippingMark, ");
            if(mIsCustomerDataUseCustomerSegmentation)
                SQL.AppendLine("CtSegmentCode ");
            else
                SQL.AppendLine("Null As CtSegmentCode ");
            SQL.AppendLine("From TblCustomer Where CtCode=@CtCode; ");

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            Sm.ShowDataInCtrl(
                    ref cm,SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "CtCode", 

                        //1-5
                        "CtName", "ActInd", "AgentInd", "CtCtCode", "CtGrpCode", 
                        
                        //6-10
                        "CntCode", "Address", "CityCode", "PostalCd", "Phone", 
                        
                        //11-15
                        "Fax", "Email", "Mobile", "CtShortCode", "NPWP", 
                        
                        //16-18
                        "NIB", "Remark", "ShippingMark", "CtSegmentCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtCtCode.EditValue = mCopyDataActive ? string.Empty : Sm.DrStr(dr, c[0]);
                        TxtCtName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        ChkAgentInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LueCtCtCode, Sm.DrStr(dr, c[4]));
                        Sl.SetLueCtGrpCode(ref LueCtGrpCode, Sm.DrStr(dr, c[5]), "Y");
                        CntCode = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueCountryCode, CntCode);
                        MeeAddress.EditValue = Sm.DrStr(dr, c[7]);
                        Sl.SetLueCityCode(ref LueCityCode, CntCode, Sm.DrStr(dr, c[8]));
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[9]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[10]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[11]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[12]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[13]);
                        TxtCtShortCode.EditValue = Sm.DrStr(dr, c[14]);
                        TxtNPWP.EditValue = Sm.DrStr(dr, c[15]);
                        TxtNIB.EditValue = Sm.DrStr(dr, c[16]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[17]);
                        MeeShippingMark.EditValue = Sm.DrStr(dr, c[18]);
                        Sm.SetLue(LueCtSegmentCode, Sm.DrStr(dr, c[19]));
                    }, true
                );
        }

        private void ShowCustomerContactPerson(string CtCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select ContactPersonName, Position, ContactNumber, Email " +
                    "From TblCustomerContactPerson " +
                    "Where CtCode=@CtCode Order By ContactPersonName;",
                    new string[] 
                    { "ContactPersonName", "Position", "ContactNumber", "Email" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowCustomerBankAccount(string CtCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select A.BankCode, B.BankName, A.BankBranch, A.BankAcName, A.BankAcNo, A.SwiftCode " +
                    "From TblCustomerBankAccount A, TblBank B " +
                    "Where A.BankCode=B.BankCode And A.CtCode=@CtCode " +
                    "Order By A.DNo;",
                    new string[] 
                    { 
                        "BankCode", 
                        "BankName", "BankBranch", "BankAcName", "BankAcNo", "SwiftCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowCustomerShipAddress(string CtCode)
        {
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Name, A.Address, A.CntCode, D.CntName, A.CityCode, Concat(B.CityName, ' ( ', IfNull(C.ProvName, ''), ' ', IfNull(D.CntName, ''), ' )') ");
            SQL.AppendLine("As CityName, A.PostalCd, ");

            if (mIsCustomerShpAddressDisplayWide) SQL.AppendLine("A.Wide, ");
            else SQL.AppendLine("0.00 As Wide, ");

            if (mIsCustomerShpAddressDisplayWaterMeter) SQL.AppendLine("A.WaterMeter, ");
            else SQL.AppendLine("Null As WaterMeter, ");

            SQL.AppendLine("A.Phone, A.Fax, A.Email, A.Mobile,  A.Remark ");
            SQL.AppendLine("From TblCustomerShipAddress A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblProvince C On B.ProvCode=C.ProvCode ");
            SQL.AppendLine("Left Join TblCountry D On A.CntCode=D.CntCode Where A.CtCode=@CtCode ");
            SQL.AppendLine("ORDER BY A.Name ASC; ");

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                      SQL.ToString(),
                    new string[] 
                    {
                        "Name",  
                        "Address", "CntCode", "CntName", "CityCode", "CityName",   
                        "PostalCd", "Phone", "Fax", "Email", "Mobile",  
                        "Remark", "Wide", "WaterMeter"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                    }, false, false, true, false
                );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 12 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowCustomerNotifyParty(string CtCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                    "Select NotifyParty " +
                    "From TblCustomerNotifyParty " +
                    "Where CtCode=@CtCode Order By NotifyParty;",
                    new string[] { "NotifyParty" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        private void ShowCustomerItem(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, A.CtItCode, A.CtItName ");
            SQL.AppendLine("From TblCustomerItem A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.CtCode=@CtCode ");
            SQL.AppendLine("Order By B.ItName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.ShowDataInGrid(
                    ref Grd5, ref cm, SQL.ToString(),
                    new string[] { "ItCode", "ItName", "CtItCode", "CtItName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowCustomerAsset(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, B.AssetName, B.DisplayName, ");
            if (mIsCtAssetWideEditabled)
                SQL.AppendLine("A.Wide, ");
            else
                SQL.AppendLine("B.Wide, ");
            SQL.AppendLine("B.WideUomCode, A.AssetLocation, A.ContractNo, A.DueDt, A.Remark ");
            SQL.AppendLine("From TblCustomerAsset A ");
            SQL.AppendLine("Inner Join TblAsset B On A.AssetCode=B.AssetCode ");
            SQL.AppendLine("Where A.CtCode=@CtCode ");
            SQL.AppendLine("Order By B.AssetName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.ShowDataInGrid(
                    ref Grd6, ref cm, SQL.ToString(),
                    new string[] { 
                        "AssetCode", 
                        "AssetName", "DisplayName", "Wide", "WideUomCode", "AssetLocation", 
                        "ContractNo", "Remark", "DueDt" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowCustomerVirtualAccount(string CtCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.ShowDataInGrid(
                    ref Grd7, ref cm,
                    "Select A.BankCode, B.BankName, A.BankAcNo,  " +
                    "Case When Find_In_Set(A.BankCode," + "'" + mEditableBankCodeVirtualAccount + "'" + ") Then 'Y' Else 'N' End As Editable " +
                    "From TblCustomerBankVirtualAccount A Inner Join TblBank B On A.BankCode = B.BankCode " +
                    "Where A.CtCode=@CtCode " +
                    "Order By A.DNo;",
                    new string[] 
                    { 
                        "BankCode", 
                        "BankName", "BankAcNo", "Editable"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                      
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 1);
        }

        private void ShowCustomerAttachment(String CtCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.ShowDataInGrid(
                    ref Grd8, ref cm,
                   "select CtCode, FileName, Remark " +
                   "from  TblCustomerAttachment " +
                   "Where CtCode=@CtCode",

                    new string[] 
                    { 
                        "CtCode",
                        "FileName",
                        "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 0, 1);
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 1, 2);
                        Sm.SetGrdValue("S", Grd8, dr, c, Row, 4, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd8, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid(string CtCode)
        {
           return
               (!TxtCtCode.Properties.ReadOnly && Sm.IsTxtEmpty(TxtCtCode, "Customer Code", false)) ||
               Sm.IsTxtEmpty(TxtCtName, "Customer name", false)||
               Sm.IsLueEmpty(LueCtCtCode, "Customer's category") ||
               Sm.IsLueEmpty(LueCountryCode, "Country") ||
               (mIsCustomerDataUseCustomerSegmentation && Sm.IsLueEmpty(LueCtSegmentCode, "Customer Segmentataion")) ||
               (!TxtCtCode.Properties.ReadOnly && IsCtCodeExisted(CtCode)) ||
               IsGrdValueNotValid();
        }

        private string GenerateCtCode()
        {
            string Zero = new String('0', mCtCodeDefaultLength); 
            return Sm.GetValue(
                "Select Right(Concat('"+Zero+"', CtCodeMax) , "+mCtCodeDefaultLength+") As CtCodeTemp " +
                "From (Select ifnull(Max(Cast(Trim(CtCode) As Decimal)), "+Zero+")+1 CtCodeMax From TblCustomer) T;"
                );
        }

        private string GenerateCtCode2()
        {
            string Zero = new String('0', mCtCodeDefaultLength);
            string CtCtCode = Sm.GetLue(LueCtCtCode);

            return Sm.GetValue(
                "Select Concat('" + CtCtCode + "','.', Right(Concat('" + Zero + "', CtCodeMax) , " + mCtCodeDefaultLength + ")) As CtCodeTemp " +
                "From (Select ifnull(Max(Cast(Trim(Right(CtCode,"+mCtCodeDefaultLength+")) As Decimal)), " + Zero + ")+1 CtCodeMax From TblCustomer WHERE CtCtCode = '" + CtCtCode + "') T "
                );
        }

        private bool IsCtCodeExisted(string CtCode)
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = "Select 1 From TblCustomer Where CtCode=@CtCode Limit 1;" 
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Customer code ( " + CtCode + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Name is empty.")) return true;

            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 1, false, "Item code is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 3, false, "Customer's item code is empty.")) return true;
                    if (mIsCustomerItemNameMandatory && Sm.IsGrdValueEmpty(Grd5, Row, 4, false, "Customer's item name is empty.")) return true;
                }
            }

            return false;
        }

        private MySqlCommand SaveCustomer(string CtCode, bool IsLocalCustomer, bool IsCtNameEdited)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomer(CtCode, CtName, CtSegmentCode, ActInd, AgentInd, CtCtCode, CtGrpCode, CtShortCode, CntCode, Address, CityCode, NPWP,  PostalCd, Phone, Fax, Email, Mobile, NIB, ShippingMark, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CtCode, @CtName, " + (mIsCustomerDataUseCustomerSegmentation ? "@CtSegmentCode" : "Null"));
            SQL.AppendLine(",@ActInd, @AgentInd, @CtCtCode, @CtGrpCode, @CtShortCode, @CntCode, @Address, @CityCode, @NPWP, @PostalCd, @Phone, @fax, @Email, @Mobile, @NIB, @ShippingMark, @Remark, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update CtName=@CtName, " + (mIsCustomerDataUseCustomerSegmentation ? "CtSegmentCode=@CtSegmentCode," : "") + " ActInd=@ActInd, AgentInd=@AgentInd, CtCtCode=@CtCtCode, CtGrpCode=@CtGrpCode, ");
            SQL.AppendLine("    CntCode=@CntCode, Address=@Address, CityCode=@CityCode, PostalCd=@PostalCd, ");
            SQL.AppendLine("    Phone=@Phone, fax=@fax, Email=@Email, Mobile=@Mobile, CtShortCode=@CtShortCode, NPWP=@NPWP, NIB=@NIB, ShippingMark = @ShippingMark, Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (!IsInsert)
            {
                if (IsCtNameEdited)
                {
                    UpdateCOACustomerDesc(ref SQL, "CustomerAcNoAR", "CustomerAcDescAR");
                    UpdateCOACustomerDesc(ref SQL, "CustomerAcNoNonInvoice", "CustomerAcDescNonInvoice");
                    UpdateCOACustomerDesc(ref SQL, "CustomerAcNoDownPayment", "CustomerAcDescDownPayment");
                    UpdateCOACustomerDesc(ref SQL, "AcNoForGiroAR", "AcDescForGiroAR");
                }

                SQL.AppendLine("Delete From TblCustomerContactPerson Where CtCode=@CtCode; ");
                SQL.AppendLine("Delete From TblCustomerBankAccount Where CtCode=@CtCode; ");
                SQL.AppendLine("Delete From TblCustomerShipAddress Where CtCode=@CtCode; ");
                SQL.AppendLine("Delete From TblCustomerNotifyParty Where CtCode=@CtCode; ");
                SQL.AppendLine("Delete From TblCustomerItem Where CtCode=@CtCode; ");
                SQL.AppendLine("Delete From TblCustomerAsset Where CtCode=@CtCode; ");
                SQL.AppendLine("Delete From TblCustomerBankVirtualAccount Where CtCode=@CtCode; ");
            }
            else
            {
                if (mGenerateCustomerCOAFormat == "1" || mGenerateCustomerCOAFormat == "3")
                {
                    if (mCustomerAcNoDownPayment.Length > 0)
                    {
                        SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                        SQL.AppendLine("Select Concat(@CustomerAcNoDownPayment, @CtCode) As AcNo, ");
                        SQL.AppendLine("Trim(Concat(@CustomerAcDescDownPayment, ' ', @CtName)) As AcDesc, ");
                        if (mIsCustomerCOAUseCustomerCategory)
                        {
                            SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @CtCode), '.', Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))) From Tblparameter Where parcode = 'CustomerAcNoDownPayment') As Parent, ");
                            SQL.AppendLine("(Select Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))+1 From TblParameter Where ParCode = 'CustomerAcNoDownPayment') As Level, ");
                        }
                        else
                        {
                            SQL.AppendLine("If(Length(@CustomerAcNoDownPayment)<=0, '', substring(@CustomerAcNoDownPayment, 1, (Length(@CustomerAcNoDownPayment)-1))) As Parent, ");
                            SQL.AppendLine("If(Length(@CustomerAcNoDownPayment)<=0, '', Length(@CustomerAcNoDownPayment)-Length(Replace(@CustomerAcNoDownPayment, '.', ''))+1) As Level, ");
                        }
                        SQL.AppendLine("'C', @UserCode, CurrentDateTime(); ");
                    }

                    if (mCustomerAcNoAR.Length > 0)
                    {
                        SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                        SQL.AppendLine("Select Concat(@CustomerAcNoAR, @CtCode) As AcNo, ");
                        SQL.AppendLine("Trim(Concat(@CustomerAcDescAR, ' ', @CtName)) As AcDesc, ");
                        if (mIsCustomerCOAUseCustomerCategory)
                        {
                            SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @CtCode), '.', Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))) From Tblparameter Where parcode = 'CustomerAcNoAR') As Parent, ");
                            SQL.AppendLine("(Select Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))+1 From TblParameter Where ParCode = 'CustomerAcNoAR') As Level, ");
                        }
                        else
                        {
                            SQL.AppendLine("If(Length(@CustomerAcNoAR)<=0, '', substring(@CustomerAcNoAR, 1, (Length(@CustomerAcNoAR)-1))) As Parent, ");
                            SQL.AppendLine("If(Length(@CustomerAcNoAR)<=0, '', Length(@CustomerAcNoAR)-Length(Replace(@CustomerAcNoAR, '.', ''))+1) As Level, ");
                        }
                        SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");
                    }

                    if (mCustomerAcNoNonInvoice.Length > 0)
                    {
                        SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                        SQL.AppendLine("Select Concat(@CustomerAcNoNonInvoice, @CtCode) As AcNo, ");
                        SQL.AppendLine("Trim(Concat(@CustomerAcDescNonInvoice, ' ', @CtName)) As AcDesc, ");
                        if (mIsCustomerCOAUseCustomerCategory)
                        {
                            SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @CtCode), '.', Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))) From Tblparameter Where parcode = 'CustomerAcNoNonInvoice') As Parent, ");
                            SQL.AppendLine("(Select Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))+1 From TblParameter Where ParCode = 'CustomerAcNoNonInvoice') As Level, ");
                        }
                        else
                        {
                            SQL.AppendLine("If(Length(@CustomerAcNoNonInvoice)<=0, '', substring(@CustomerAcNoNonInvoice, 1, (Length(@CustomerAcNoNonInvoice)-1))) As Parent, ");
                            SQL.AppendLine("If(Length(@CustomerAcNoNonInvoice)<=0, '', Length(@CustomerAcNoNonInvoice)-Length(Replace(@CustomerAcNoNonInvoice, '.', ''))+1) As Level, ");
                        }
                        SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");
                    }

                    if (mAcNoForGiroAR.Length > 0)
                    {
                        SQL.AppendLine("Insert Into TblCOA(AcNo, AcDesc, Parent, Level, AcType, CreateBy, CreateDt) ");
                        SQL.AppendLine("Select Concat(@AcNoForGiroAR, @CtCode) As AcNo, ");
                        SQL.AppendLine("Trim(Concat(@AcDescForGiroAR, ' ', @CtName)) As AcDesc, ");
                        if (mIsCustomerCOAUseCustomerCategory)
                        {
                            SQL.AppendLine("(Select Substring_Index(Concat(ParValue, @CtCode), '.', Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))) From Tblparameter Where parcode = 'AcNoForGiroAR') As Parent, ");
                            SQL.AppendLine("(Select Length(Concat(ParValue, @CtCode))-Length(Replace(Concat(ParValue, @CtCode), '.', ''))+1 From TblParameter Where ParCode = 'AcNoForGiroAR') As Level, ");
                        }
                        else
                        {
                            SQL.AppendLine("If(Length(@AcNoForGiroAR)<=0, '', substring(@AcNoForGiroAR, 1, (Length(@AcNoForGiroAR)-1))) As Parent, ");
                            SQL.AppendLine("If(Length(@AcNoForGiroAR)<=0, '', Length(@AcNoForGiroAR)-Length(Replace(@AcNoForGiroAR, '.', ''))+1) As Level, ");
                        }

                        SQL.AppendLine("'D', @UserCode, CurrentDateTime(); ");
                    }
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@CtName", TxtCtName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@AgentInd", ChkAgentInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCtCode", Sm.GetLue(LueCtCtCode));
            Sm.CmParam<String>(ref cm, "@CtGrpCode", Sm.GetLue(LueCtGrpCode));
            Sm.CmParam<String>(ref cm, "@CntCode", Sm.GetLue(LueCountryCode));
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
            if(mIsCustomerDataUseCustomerSegmentation)
                Sm.CmParam<String>(ref cm, "@CtSegmentCode", Sm.GetLue(LueCtSegmentCode));
            Sm.CmParam<String>(ref cm, "@PostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@Phone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@Fax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@Email", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@Mobile", TxtMobile.Text);
            if (mIsAutoGenerateCtShortCode)
                Sm.CmParam<String>(ref cm, "@CtShortCode", GetCtShortCode());
            else
                Sm.CmParam<String>(ref cm, "@CtShortCode", TxtCtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@NPWP", TxtNPWP.Text);
            Sm.CmParam<String>(ref cm, "@NIB", TxtNIB.Text);
            Sm.CmParam<String>(ref cm, "@ShippingMark", MeeShippingMark.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            if (IsInsert)
            {
                if (mCustomerAcNoAR.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcNoAR", mCustomerAcNoAR);
                if (mCustomerAcDescAR.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcDescAR", mCustomerAcDescAR);
                if (mCustomerAcNoNonInvoice.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcNoNonInvoice", mCustomerAcNoNonInvoice);
                if (mCustomerAcDescNonInvoice.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcDescNonInvoice", mCustomerAcDescNonInvoice);
                if (mCustomerAcNoDownPayment.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcNoDownPayment", mCustomerAcNoDownPayment);
                if (mCustomerAcDescDownPayment.Length > 0) Sm.CmParam<String>(ref cm, "@CustomerAcDescDownPayment", mCustomerAcDescDownPayment);
                if (mAcNoForGiroAR.Length > 0) Sm.CmParam<String>(ref cm, "@AcNoForGiroAR", mAcNoForGiroAR);
                if (mAcDescForGiroAR.Length > 0) Sm.CmParam<String>(ref cm, "@AcDescForGiroAR", mAcDescForGiroAR);
            }

            return cm;
        }

        private string GetCtShortCode()
        {
            var SQL = new StringBuilder();


            SQL.Append("Set @CtCtShortCode:=(Select CtCtShortCode From TblCustomerCategory Where CtCtCode=@Param); ");

            SQL.Append("Select Concat(@CtCtShortCode,'.', ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('00000', Convert(Code+1, Char)), 5) From ( ");
            SQL.Append("       Select Convert(Right(CtShortCode, 5), Decimal) As Code From TblCustomer ");
            SQL.Append("       Where CtCtCode Is Not Null And CtShortCode Is Not Null And CtCtCode=@Param ");
            SQL.Append("       Order By Convert(Right(CtShortCode, 5), Decimal) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '00001') ");
            SQL.Append(") As Code ");

            return Sm.GetValue(SQL.ToString(), Sm.GetLue(LueCtCtCode));
        }

        private void UpdateCOACustomerDesc(ref StringBuilder SQL, string AcNo, string AcDesc)
        {
            SQL.AppendLine("Update TblCOA A ");
            SQL.AppendLine("Inner Join TblParameter B On B.ParCode='" + AcNo + "' And B.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join TblParameter C On C.ParCode='" + AcDesc + "' And C.ParValue Is Not Null ");
            SQL.AppendLine("Set A.AcDesc=Concat(C.ParValue, ' ', @CtName), A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.AcNo=Concat(B.ParValue, @CtCode); ");
        }

        private MySqlCommand SaveCustomerContactPerson(string CtCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblCustomerContactPerson(CtCode, DNo, ContactPersonName, Position, ContactNumber, Email, CreateBy, CreateDt) " +
                    "Values(@CtCode, @DNo, @ContactPersonName, @Position, @ContactNumber, @Email, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ContactPersonName", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@Position", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@ContactNumber", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Email", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCustomerBankAccount(string CtCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblCustomerBankAccount(CtCode, DNo, BankCode, BankBranch, BankAcName, BankAcNo, SwiftCode, CreateBy, CreateDt) " +
                    "Values(@CtCode, @DNo, @BankCode, @BankBranch, @BankAcName, @BankAcNo, @SwiftCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@BankBranch", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@BankAcName", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@BankAcNo", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@SwiftCode", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCustomerShipAddress(string CtCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerShipAddress(CtCode, DNo, Name, Address, CntCode, CityCode, PostalCd, Phone, Fax, Email, Mobile, Remark,  ");

            if (mIsCustomerShpAddressDisplayWide) SQL.AppendLine("Wide, ");
            if (mIsCustomerShpAddressDisplayWaterMeter) SQL.AppendLine("WaterMeter, ");
            
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CtCode, @DNo, @Name, @Address, @CntCode, @CityCode, @PostalCd, @Phone, @Fax, @Email, @Mobile, @Remark, ");
            
            if (mIsCustomerShpAddressDisplayWide) SQL.AppendLine("@Wide, ");
            if (mIsCustomerShpAddressDisplayWaterMeter) SQL.AppendLine("@WaterMeter, ");
            
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            if (mIsCustomerShippingAddressFormatDNoCustomized)
                Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            else
                Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Name", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParam<String>(ref cm, "@Address", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CntCode", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@PostalCd", Sm.GetGrdStr(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@Phone", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@Fax", Sm.GetGrdStr(Grd3, Row, 8));
            Sm.CmParam<String>(ref cm, "@Email", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@Mobile", Sm.GetGrdStr(Grd3, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 11));

            if (mIsCustomerShpAddressDisplayWide) Sm.CmParam<Decimal>(ref cm, "@Wide", Sm.GetGrdDec(Grd3, Row, 12));
            if (mIsCustomerShpAddressDisplayWaterMeter) Sm.CmParam<String>(ref cm, "@WaterMeter", Sm.GetGrdStr(Grd3, Row, 13));
            
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCustomerNotifyParty(string CtCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblCustomerNotifyParty(CtCode, DNo, NotifyParty, CreateBy, CreateDt) " +
                    "Values(@CtCode, @DNo, @NotifyParty, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@NotifyParty", Sm.GetGrdStr(Grd4, Row, 0));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCustomerItem(string CtCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblCustomerItem(CtCode, ItCode, CtItCode, CtItName, CreateBy, CreateDt) " +
                    "Values(@CtCode, @ItCode, @CtItCode, @CtItName, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<String>(ref cm, "@CtItCode", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<String>(ref cm, "@CtItName", Sm.GetGrdStr(Grd5, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCustomerAsset(string CtCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblCustomerAsset(CtCode, AssetCode, AssetLocation, ContractNo, Wide, DueDt, Remark, CreateBy, CreateDt) " +
                    "Values(@CtCode, @AssetCode, @AssetLocation, @ContractNo, @Wide, @DueDt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@AssetCode", Sm.GetGrdStr(Grd6, Row, 1));
            Sm.CmParam<String>(ref cm, "@AssetLocation", Sm.GetGrdStr(Grd6, Row, 6));
            Sm.CmParam<String>(ref cm, "@ContractNo", Sm.GetGrdStr(Grd6, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Wide", Sm.GetGrdDec(Grd6, Row, 4));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetGrdDate(Grd6, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd6, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCustomerVirtualAccount(string CtCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerBankVirtualAccount(CtCode, DNo, BankCode, BankAcNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CtCode, @DNo, @BankCode, @BankAcNo, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetGrdStr(Grd7, Row, 0));
            Sm.CmParam<String>(ref cm, "@BankAcNo", Sm.GetGrdStr(Grd7, Row, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCustomerAttachment(string CtCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCustomerAttachment(CtCode, DNo, Filename, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CtCode, @DNo, @FileName, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("Update ");
            SQL.AppendLine("    FileName=@FileName, Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd8, Row, 0));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd8, Row, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("ALTER TABLE `tblcustomershipaddress` ");
            SQL.AppendLine("    ADD COLUMN IF NOT EXISTS `WaterMeter` VARCHAR(400) NULL DEFAULT NULL AFTER `Mobile`; ");

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('IsCustomerShpAddressDisplayWaterMeter', 'Apakah master data Customer memunculkan inputan Water Meter ? [ Y = Ya, N = Tidak]', 'N', 'KBN', NULL, 'Y', 'WEDHA', '202303030800', NULL, NULL); ");

            Sm.ExecQuery(SQL.ToString());
        }

        private bool IsUserViewFtpOnly()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblEmployee ");
            SQL.AppendLine("Where UserCode = @UserCode ");
            SQL.AppendLine("And Find_In_Set(DeptCode, @DeptCodeForCustomerViewFTP) ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCodeForCustomerViewFTP", mDeptCodeForCustomerViewFTP);

            if (Sm.IsDataExist(cm))
                return true;
            else
                return false;
        }

        private bool IsUserCanUploadDownloadFtp()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblEmployee ");
            SQL.AppendLine("Where UserCode = @UserCode ");
            SQL.AppendLine("And Find_In_Set(DeptCode, @DeptCodeForCustomerUploadDownloadAndViewFTP) ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCodeForCustomerUploadDownloadAndViewFTP", mDeptCodeForCustomerUploadDownloadAndViewFTP);

            if (Sm.IsDataExist(cm))
                return true;
            else
                return false;
        }

        private MySqlCommand DeleteCustomerAttachment(string CtCode)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Delete From TblCustomerAttachment ");
            SQL.AppendLine("Where CtCode=@CtCode  ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
                    
            };
            Sm.CmParam<String>(ref cm, "@CtCode", TxtCtCode.Text);

            return cm;
        }

        private bool IsUserEditCtName()
        {
            var CtNameNew = TxtCtName.Text;
            var CtNameOld = Sm.GetValue("Select CtName From TblCustomer Where CtCode=@Param;", TxtCtCode.Text);
            return !Sm.CompareStr(CtNameNew, CtNameOld);
        }

        private bool IsCustomerLocal()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select LocalInd From TblCustomerCategory Where CtCtCode=@CtCtCode; "
            };
            Sm.CmParam<String>(ref cm, "@CtCtCode", Sm.GetLue(LueCtCtCode));
            return Sm.GetValue(cm) == "Y";
        }

        private void GetParameter()
        {
            var CtCodeDefaultLength = Sm.GetParameter("CtCodeDefaultLength");
            if (CtCodeDefaultLength.Length > 0) mCtCodeDefaultLength = int.Parse(CtCodeDefaultLength);

            DocTitle = Sm.GetParameter("DocTitle");
            mCustomerParam = Sm.GetParameter("IsCtCodeAutoIncrement");
            mCustomerAcNoDownPayment = Sm.GetParameter("CustomerAcNoDownPayment");
            mCustomerAcDescDownPayment = Sm.GetParameter("CustomerAcDescDownPayment");
            mAcNoForGiroAR = Sm.GetParameter("AcNoForGiroAR");
            mAcDescForGiroAR = Sm.GetParameter("AcDescForGiroAR");
            mIsCustomerShippingAddressFormatDNoCustomized = Sm.GetParameter("IsCustomerShippingAddressFormatDNoCustomized") == "Y";
            mCustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR");
            mCustomerAcNoNonInvoice = Sm.GetParameter("CustomerAcNoNonInvoice");
            mCustomerAcDescAR = Sm.GetParameter("CustomerAcDescAR");
            mCustomerAcDescNonInvoice = Sm.GetParameter("CustomerAcDescNonInvoice");
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsShippingAddressAliasActive = Sm.GetParameterBoo("IsShippingAddressAliasActive");
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");
            mIsCtAssetWideEditabled = Sm.GetParameterBoo("IsCtAssetWideEditabled");
            mIsCtAssetDueDtEnabled = Sm.GetParameterBoo("IsCtAssetDueDtEnabled");
            mIsCustomerShpAddressDisplayWide = Sm.GetParameterBoo("IsCustomerShpAddressDisplayWide");
            mIsAutoGenerateCtShortCode = Sm.GetParameterBoo("IsAutoGenerateCtShortCode");
            mIsCustomerCOAUseCustomerCategory = Sm.GetParameterBoo("IsCustomerCOAUseCustomerCategory");
            mIsCustomerUseBankVirtualAccount = Sm.GetParameterBoo("IsCustomerUseBankVirtualAccount");
            mEditableBankCodeVirtualAccount = Sm.GetParameter("EditableBankCodeVirtualAccount");
            mIsCustomerAllowToCopyData = Sm.GetParameterBoo("IsCustomerAllowToCopyData");
            mIsCustomerCategoryFilteredByGroup = Sm.GetParameterBoo("IsCustomerCategoryFilteredByGroup");
            mIsCustomerUseAttachment = Sm.GetParameterBoo("IsCustomerUseAttachment");
            mIsCustomerDataUseCustomerSegmentation = Sm.GetParameterBoo("IsCustomerDataUseCustomerSegmentation");

            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mDeptCodeForCustomerViewFTP = Sm.GetParameter("DeptCodeForCustomerViewFTP");
            mDeptCodeForCustomerUploadDownloadAndViewFTP = Sm.GetParameter("DeptCodeForCustomerUploadDownloadAndViewFTP");
            mPathToSaveDownloadedFilesFromFtp = Sm.GetParameter("PathToSaveDownloadedFilesFromFtp");
            mIsCustomerShpAddressDisplayWaterMeter = Sm.GetParameterBoo("IsCustomerShpAddressDisplayWaterMeter");
        }

        private void MeeRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.MemoExEdit Mee,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Mee.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Mee.EditValue = null;
            else
                Mee.EditValue = Sm.GetGrdStr(Grd, fCell.RowIndex, 0);

            Mee.Visible = true;
            Mee.Focus();

            fAccept = true;
        }

        internal void CopyData(string CtCode)
        {
            try
            {
                ClearData();
                ShowCustomer(CtCode);
                ShowCustomerContactPerson(CtCode);
                ShowCustomerBankAccount(CtCode);
                ShowCustomerShipAddress(CtCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetLueCtCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T1.CtCtCode As Col1, T1.CtCtName As Col2 ");
            SQL.AppendLine("FROM tblcustomercategory T1 ");
            if (mIsCustomerCategoryFilteredByGroup)
            {
                SQL.AppendLine("WHERE T1.CtCtCode Is Null OR (T1.CtCtCode Is Not Null And Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("        Where CtCtCode=T1.CtCtCode  "); 
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Order By T1.CtCtName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[4096];
            while (true)
            {
                int read = input.Read(buffer, 0, buffer.Length);
                if (read <= 0)
                {
                    return;
                }
                output.Write(buffer, 0, read);
            }
        }

        #endregion

        #region FTP

        private void DownloadAllFile(string fileList, string FTPAddress, string port, string username, string password, string FileShared, string destination)
        {
            downloadedData = new byte[0];
            try
            {
                FtpWebRequest request;
                this.Text = "Connecting...";
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + fileList) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + fileList) as FtpWebRequest;
                }

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + fileList) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + fileList) as FtpWebRequest;
                }
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                // Set up progress bar
                PbDownload.Value = 0;
                PbDownload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbDownload.Value = PbDownload.Maximum;
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                        if (PbDownload.Value + bytesRead <= PbDownload.Maximum)
                        {
                            PbDownload.Value += bytesRead;

                            PbDownload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }
        

    public string[] GetFileList()
        {
            string[] downloadFiles;
            string result = string.Empty;
            try
            {
                for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd8, Row, 0).Length > 0)
                    {
                        if (result.Length > 0) result += ",";
                        result += Sm.GetGrdStr(Grd8, Row, 0);
                    }

                result.Remove(result.ToString().LastIndexOf(','), 1);
                return result.ToString().Split(',');
            }
            catch (Exception ex)
            {
                downloadFiles = null;
                return downloadFiles;
            }
        }


        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void PreviewFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {

            string path = @"ftp://" + username.Replace("@", "%40") + ":" + password.Replace("@", "%40") + "@" + FTPAddress.Replace("@", "%40") + "/" + FileShared.Replace("@", "%40") + "/" + filename.Replace("@", "%40");
            string iexplore = @"C:\Program Files\Internet Explorer\iexplore.exe";
            try
            {
                System.Diagnostics.Process.Start(iexplore, path);
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                    MessageBox.Show(noBrowser.Message);
            }
            catch (System.Exception other)
            {
                MessageBox.Show(other.Message);
            }
            
        }


        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (FileName.Length > 0 && Sm.GetGrdStr(Grd8, Row, 0) != Sm.GetGrdStr(Grd8, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblCustomerAttachment ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        

        #endregion

        #endregion

        #region Event

        #region Grid Method

        #region Grid 1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grid 2

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 2, 3, 4 }, e);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2, 3, 4, 5 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd2, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd2, ref LueBankCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sl.SetLueBankCode(ref LueBankCode);
            }
        }

        #endregion

        #region Grid 3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd3, new int[] { 2, 6, 7, 8, 9 }, e);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 6, 7, 8, 9, 10, 11, 12 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 12 });
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 3 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd3, ref LueCntCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 2).Length==0)
                    Sl.SetLueCntCode(ref LueCntCode);
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd3, ref LueCity2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length == 0)
                {
                    var CntCode = Sm.GetGrdStr(Grd3, e.RowIndex, 2);
                    if (CntCode.Length == 0) CntCode = "***";
                    Sl.SetLueCityCode(ref LueCity2, CntCode, string.Empty);
                }
                else
                {
                    var CityCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    LueCity2.EditValue = "<Refresh>";
                    Sl.SetLueCityCode(ref LueCity2, Sm.GetGrdStr(Grd3, e.RowIndex, 2), CityCode);
                }
            }
        }

        #endregion

        #region Grid 4

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdEnter(Grd4, e);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd4, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
            {
                MeeRequestEdit(Grd4, MeeNotify, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
        }

        #endregion

        #region Grid 5

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmCustomerDlg(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 3 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd5, e.RowIndex);
            }
        }

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmCustomerDlg(this));
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd5, new int[] { 3 }, e);
        }

        #endregion

        #region Grid 6
        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmCustomerDlg2(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 3 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);


                if (mIsCtAssetWideEditabled && Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex))
                {
                    if (e.ColIndex == 9) Sm.DteRequestEdit(Grd6, DteDueDt, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                }
                
                if (mIsCtAssetWideEditabled &&  Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }
        }


        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmCustomerDlg2(this));
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
            Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex==4) Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd6, new int[] { 4 }, e);
        }

        #endregion

        #region Grid 7

        private void Grd7_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd7, new int[] { 2 }, e);
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                for (int Index = Grd7.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                //    if (Sm.GetGrdStr(Grd7, Grd7.SelectedRows[Index].Index, 3).Length <= 0)
                //    {
                        Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                    //}
                        if (Sm.GetGrdStr(Grd7, Grd7.SelectedRows[Index].Index, 3) != "Y" && TxtCtCode.Text.Length > 0 && Sm.GetGrdStr(Grd7, Grd7.SelectedRows[Index].Index, 3).Length > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "You can't remove this selected data.");
                        return;
                    }
                    Grd7.Rows.RemoveAt(Grd7.SelectedRows[Index].Index);
                }
                if (Grd7.Rows.Count <= 0) Grd7.Rows.Add();
            }
               
            
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd7, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.LueRequestEdit(ref Grd7, ref LueBankCode2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
                Sl.SetLueBankCode(ref LueBankCode2);
            }
        }

        #endregion

        #region Grid 8

        private void  Grd8_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd8, e.RowIndex, 0).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd8, e.RowIndex, 0), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd8, e.RowIndex, 0);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd8, e.RowIndex, 0, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (e.ColIndex == 5)
            {
                if (Sm.GetGrdStr(Grd8, e.RowIndex, 0).Length > 0)
                {
                    PreviewFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd8, e.RowIndex, 0), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                }
            }

            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd8, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 1;
                    OD.ShowDialog();
                    Grd8.Cells[e.RowIndex, 0].Value = OD.FileName;
                }
            }

          
        }

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
            }

            if (e.ColIndex == 5)
            {
                if (Sm.GetGrdStr(Grd8, e.RowIndex, 0).Length > 0)
                {
                    PreviewFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd8, e.RowIndex, 0), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                }
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd8, e.RowIndex);
                if (Sm.GetGrdStr(Grd8, e.RowIndex, 0).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd8, e.RowIndex, 0), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd8, e.RowIndex, 0);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd8, e.RowIndex, 0, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd8_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
            else if (e.ColIndex == 5)
            {
                e.Text = "View";
            }
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            if (IsUserCanUploadDownloadFtp())
            {
                Sm.GrdRemoveRow(Grd8, e, BtnSave);
                Sm.GrdEnter(Grd8, e);
                Sm.GrdTabInLastCell(Grd8, e, BtnFind, BtnSave);
            }

        }

        #endregion

        #endregion

        #region Misc Control Event

        private bool IsUploadMaxFile()
        {
            if (Grd8.Rows.Count > 51)
            {
                Sm.StdMsg(mMsgType.Warning, "Max. upload file is 50");
                return true;
            }
            return false;
        }

     
        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName)||
                IsFileNameAlreadyExisted(Row, FileName);
        }

       
        private void TxtCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtCode);
        }

        private void TxtCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtName);
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
        }

        private void LueCtGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtGrpCode, new Sm.RefreshLue3(Sl.SetLueCtGrpCode), string.Empty, "Y");
        }

        private void TxtPostalCd_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPostalCd);
        }

        private void TxtPhone_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPhone);
        }

        private void TxtFax_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtFax);
        }

        private void TxtEmail_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEmail);
        }

        private void TxtMobile_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtMobile);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueBankCode_Leave(object sender, EventArgs e)
        {
            if (LueBankCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueBankCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 0].Value =
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueBankCode);
                    Grd2.Cells[fCell.RowIndex, 1].Value = LueBankCode.GetColumnValue("Col2");
                }
                LueBankCode.Visible = false;
            }
        }

        private void LueBankCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueCountryCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCountryCode, new Sm.RefreshLue1(Sl.SetLueCntCode));
                LueCityCode.EditValue = null;
                string CntCode = Sm.GetLue(LueCountryCode);
                if (CntCode.Length > 0)
                {
                    LueCityCode.EditValue = "<Refresh>";
                    Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue3(Sl.SetLueCityCode), CntCode, string.Empty);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCityCode }, false);
                }
                else
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCityCode }, true);
                }
            }
        }

        private void LueCityCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue3(Sl.SetLueCityCode), Sm.GetLue(LueCountryCode), string.Empty);
        }

        private void LueCtSegmentCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtSegmentCode, new Sm.RefreshLue2(Sl.SetLueOption), "CustomerSegmentation");
        }

        private void LueCntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCntCode, new Sm.RefreshLue1(Sl.SetLueCntCode));
        }

        private void LueCntCode_Validated(object sender, EventArgs e)
        {
            
        }
        
        private void LueCntCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueCntCode_Leave(object sender, EventArgs e)
        {
            try
            {
                if (LueCntCode.Visible && fAccept && fCell.ColIndex == 3)
                {
                    LueCity2.Visible = true;
                    if (Sm.GetLue(LueCntCode).Length == 0)
                    {
                        Grd3.Cells[fCell.RowIndex, 2].Value = null;
                        Grd3.Cells[fCell.RowIndex, 3].Value = null;
                        LueCity2.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueCity2, new Sm.RefreshLue3(Sl.SetLueCityCode), "***", string.Empty);
                        Grd3.Cells[fCell.RowIndex, 4].Value = null;
                        Grd3.Cells[fCell.RowIndex, 5].Value = null;
                    }
                    else
                    {
                        var CntCode = Sm.GetLue(LueCntCode);
                        var CityCode = Sm.GetGrdStr(Grd3, fCell.RowIndex, 4);
                        Grd3.Cells[fCell.RowIndex, 2].Value = CntCode;
                        Grd3.Cells[fCell.RowIndex, 3].Value = LueCntCode.GetColumnValue("Col2");
                        if (CityCode.Length == 0 ||
                            (CityCode.Length != 0 && 
                            !Sm.IsDataExist(
                                "Select 1 From  TblCity T1, TblProvince T2 " +
                                "Where T1.ProvCode=T2.ProvCode And T1.ProvCode=T2.ProvCode And T1.CityCode=@Param1 And T2.CntCode=@Param2 Limit 1;",
                                CityCode, CntCode, string.Empty)))
                        {
                            LueCity2.EditValue = "<Refresh>";
                            Sm.RefreshLookUpEdit(LueCity2, new Sm.RefreshLue3(Sl.SetLueCityCode), CntCode, string.Empty);
                            Grd3.Cells[fCell.RowIndex, 4].Value = null;
                            Grd3.Cells[fCell.RowIndex, 5].Value = null;
                        }
                    }
                    LueCity2.Visible = false;
                    LueCntCode.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueCity2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueCity2_EditValueChanged(object sender, EventArgs e)
        {
            var CntCode = Sm.GetGrdStr(Grd3, fCell.RowIndex, 2);
            if (CntCode.Length == 0) CntCode = "***";
            Sm.RefreshLookUpEdit(LueCity2, new Sm.RefreshLue3(Sl.SetLueCityCode), CntCode, string.Empty);
        }

        private void LueCity2_Validated(object sender, EventArgs e)
        {

        }

        private void LueCity2_Leave(object sender, EventArgs e)
        {
            if (LueCity2.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueCity2).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 4].Value = null;
                    Grd3.Cells[fCell.RowIndex, 5].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueCity2);
                    Grd3.Cells[fCell.RowIndex, 5].Value = LueCity2.GetColumnValue("Col2");
                }
                LueCity2.Visible = false;
            }
        }

        private void MeeNotify_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void MeeNotify_Leave(object sender, EventArgs e)
        {
            if (MeeNotify.Visible && fAccept && fCell.ColIndex == 0)
            {
                if (MeeNotify.Text.Length == 0)
                    Grd4.Cells[fCell.RowIndex, 0].Value = null;
                else
                {
                    Grd4.Cells[fCell.RowIndex, 0].Value = MeeNotify.Text;
                }
                MeeNotify.Visible = false;
            }
        }

        private void DteDueDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd6, ref fAccept, e);
        }

        private void DteDueDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDueDt, ref fCell, ref fAccept);
        }

        private void LueBankCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode2, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueBankCode2_Leave(object sender, EventArgs e)
        {
            if (LueBankCode2.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueBankCode2).Length == 0)
                    Grd7.Cells[fCell.RowIndex, 0].Value =
                    Grd7.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd7.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueBankCode2);
                    Grd7.Cells[fCell.RowIndex, 1].Value = LueBankCode2.GetColumnValue("Col2");
                }
                LueBankCode2.Visible = false;
            }
        }

        private void LueBankCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void BtnCopyGeneral_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmCustomerDlg3(this));
        }

        private void UploadFile(string CtCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateCtFile(CtCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private MySqlCommand UpdateCtFile(string CtCode, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCustomerAttachment Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where CtCode=@CtCode And DNo=@DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        #endregion

        #endregion
    }
}
