﻿#region Update
/*
    16/08/2017 [TKG] tambah validasi apakah user punya otorisasi untuk melihat data quotation.
    04/09/2017 [HAR] tambah param SO tidak bisa dicancel jika sdh dipake di AR DP 
    06/10/2017 [ARI] revisi printout [tambah city, country, post, telp, fax]
    12/10/2017 [TKG] default price (price list) dimunculkan.
    14/11/2017 [HAR] Bug Fixing Phone tidak muncul (jika ada customer shp address yang sama diambil yang paling akhir)
    22/01/2018 [TKG] Address tidak boleh kosong
    22/05/2018 [HAR] tambah infromasi volume, total volume, uom volume
    24/05/2018 [ARI] printout tambah volume, Qty(sebelumnya Qty ambil Qty2)--> MAI
    25/05/2018 [HAR] tambah total qty packaging, qty, volume
    10/07/2018 [HAR] tambah inputan untuk keperluan print out (City, port, date contract, comodity, tolerance, bank account
    11/07/2018 [ARI] print out : tambah City, port, date contract, comodity, tolerance, bank account
    23/07/2018 [HAR] deliver date mandatory untuk row pertama saja dan bisa double klik headernya
    01/08/2018 [ARI] label local document diganti No. sales contarct (pake parameter localdocument)
    21/08/2018 [HAR] ada informasi container group di detail, remark header bisa di ubah saat edit, validasi deliver date berdasarkn container group
    03/09/2018 [WED] Oversea bisa diedit selama SO belum dipakai dimana mana (cek ke DR dan Shipment Instruction --> Shipment Planning)
    05/09/2018 [HAR] tambah field shipment di header dan button download
    06/09/2018 [TKG] bug fix printout
    10/10/2018 [HAR] feedback : edit ke manual fullfilled tanpa lihat ada AR DP atau tidak
    12/11/2018 [HAR] ftp tambah parameter buat nentuin format koneksi FTP
    13/11/2018 [HAR] tambah inputan container group quantity
    21/11/2018 [WED] tambah lup untuk generate Bank Account berdasarkan parameter SO2BankAcCode
    28/11/2018 [DITA] Penambahan kolom customer code pada print out Sales Order
    03/12/2018 [TKG] bug saat menampilkan data quotation yg tdk aktif
    12/12/2018 [HAR] feedback printout total container ambil berdasarkan jumlah container per group container
    17/12/2018 [WED] lup bank account dihilangkan. tambah option baru SOBankAccount untuk bank account
    17/12/2018 [WED] print out SO Oversea, Bank name juga ambil dari option SOBankAccount
    20/12/2018 [WED] CtQt bisa milih dokumennya (bukan otomatis) berdasarkan parameter RuleToDeactivateCtQt
    04/01/2019 [HAR] bank ambil dari option khusu IOK saja
    09/01/2019 [HAR] feedback klo master bank ada enternya nggak muncul, di printout juga tertumpuk
    08/02/2019 [HAR] BUG : customer quotation validation expired function IsDocExpired()
    08/02/2019 [HAR] BUG : SO  dapatkan volume dari item packaging unit
    14/02/2019 [DITA] Feedback : Saat INSERT SO, setelah pilih List of Customer's Quotation, saat pilih Shipping Information, Shipping Information-nya sesuai City yang dipilih di Customer's Quotation-nya
    15/03/2019 [HAR] BUG : SO bank name
    26/03/2019 [DITA] PRINT OUT Sales Order: Alamat BUYER Yng ada di pojok kanan atas, ambil dari Master Customer's Address.
    26/03/2019 [DITA] PRINT OUT Sales Order: "Spec" diganti "Commodity", dikasih space dua baris (fleksibel)
    26/03/2019 [DITA] PRINT OUT Sales Order: source "Delivery Schedule" diambil dari field SHIPMENT (disediakan 60 karakter lebih)
    26/03/2019 [DITA] PRINT OUT Sales Order: Payment bisa ENTER ke bawah (disediakan bisa dua baris), implikasi di print out SO saja. 
    26/03/2019 [DITA] PRINT OUT Sales Order: "Total Container" menjadi "Total" saja, "Container" dihilangkan.
    26/03/2019 [DITA] PRINT OUT Sales Order: Ditambahkan karakter "x" setelah angka Total (follow up request di atas). 
    26/03/2019 [DITA] Validity di row terakhir printout, minta tolong dibuatkan inputan baru supaya muncul .
    26/03/2019 [DITA] PRINT OUT Sales Order: Kolom Qty (Cont.) dihapus karena sudah diwakili dengan "Total" yang ada di printout bagian bawah.
    26/03/2019 [DITA] PRINT OUT Sales Order: judul kolom TOTAL hilangkan
    26/03/2019 [DITA] PRINT OUT Sales Order: Judul Kolom Price (USD) menjadi Unit Price.
    26/03/2019 [DITA] PRINT OUT Sales Order: Judul Kolom Amount (USD) menjadi Amount saja
    01/04/2019 [MEY] PRINT OUT Sales Order: source "Price Term" diambil dari field SHIPPING METHOD (FOB, CNF, etc. --> tulisan lengkap) header Sales Order.
    01/04/2019 [MEY] PRINT OUT Sales Order: "Shipment" diganti "Destination", source diambil dari PORT atau CITY (CITY jika lokal, dilihat dari indikator "Checkbox Overseas") PORT bila eksport, CITY bila lokal.
    01/04/2019 [MEY] PRINT OUT Sales Order: Tambah ukuran kontainer di Total Container (PENGARUH CUMA di PRINT OUT) 40", 20" (inputan Container Size)
    01/04/2019 [MEY] PRINT OUT Sales Order: Nama item Sales Order (Commodity), Ganti berdasarkan Foreign Name.
    01/04/2019 [MEY] Antisipasi request di atas tambah opsi untuk pilih ukuran kontainer 40" atau 20" supaya muncul di printout (PENGARUH CUMA DI PRINT OUT)
    01/04/2019 [MEY] PRINT OUT Sales Order: Kolom QTY (Cont.) yg dihapus, diganti kolom Currency (ambil dr Customer Quotation, pakai Short code Currency). Letak Kolom di tengah2 antara kolom UOM dan UNIT PRICE.
    01/04/2019 [MEY] PRINT OUT Sales Order: Penandatangan di bawah "PT INDOTAMA" diisi ketikan saja, jangan otomatis dan jabatan (Sales Order) dihilangkan ( tambah inputan Signature Name)
    09/04/2019 [MEY] Input Container Size mandatory jika Oversea di centang
    09/04/2019 [HAR] PRINT OUT Sales Order:  grouping container dan subtotal
    09/04/2019 [HAR] Menghilangkan format print out sales order 
    10/04/2019 [WED] Kalkulasi Amount di Print out, query tidak di rounding. di print out baru rounding
    14/05/2019 [WED] tambah parameter baru (IsSODisplayExpiredCtQt) untuk validasi muncul quotation kadaluarsa
    15/05/2019 [MEY] bug : tambah variabel PtName di class Header
    16/05/2019 [DITA] PRINT OUT Sales Order:Tolerance dibuat bisa dua baris (mode ENTER)
    16/05/2019 [DITA] PRINT OUT Sales Order:Validity dibuat bisa dua baris (mode ENTER)
    16/05/2019 [MEY] PRINT OUT Sales Order (IOK) : COMMODITY dibuat dua baris 
    16/05/2019 [MEY] PRINT OUT Sales Order (IOK) : tambah CUSTOMER'S PO di print out 
    16/05/2019 [MEY] PRINT OUT Sales Order (IOK) : DESTINATION transaksi SO lokal ("Uncheck" indikator "Overseas") dari CITY dan Overseas dari PORT.
    16/05/2019 [MEY] PRINT OUT Sales Order (IOK) : DELIVERY SCHEDULE (yang diambil dari FIELD SHIPMENT) dibuat agar bisa full satu baris print out.
    16/05/2019 [MEY] PRINT OUT Sales Order (IOK) : isi masing-masing KOLOM T, W, L, Qty (Plt.), Qty (Pcs.), UOM, CURRENCY, UNIT PRICE, AMOUNT berturut-turut, dibuat rata kanan.
    20/05/2019 [TKG] bug iok
    28/05/2019 [DITA] Field Container Size tambah input karakter
    08/07/2019 [DITA] tampilan panel tengah/ panel detail/ panel isian bisa langsung muncul.
    30/12/2019 [TKG/GSS] tambah journal
    06/01/2020 [TKG/GSS] ubah rumus journal
    16/03/2020 [VIN/SRN] printout SO2DefaultSRN(remark beda letak dengan SO2Default)
    07/07/2020 [ICA/SIER] field delivery date dibuat tidak mandatory
    07/07/2020 [ICA/SIER] menghilangkan teks "packaging" di item information
    13/07/2020 [ICA/SIER] shipping name label dibuat mandatory
    14/07/2020 [VIN/SIER] tambah attachment di header
    23/02/2021 [WED/SIER] tambah inputan Customer Category berdasarakan parameter IsCustomerComboBasedOnCategory
    27/04/2021 [TKG/SIER] ubah tampilan.
    04/05/2021 [RDH/SIER] menambah lup dialog customer category
    18/05/2021 [RDH/SIER] FEEDBACK : penyesuaian customer category berdasarkan grop login
    02/06/2021 [RDH/SIER] bisa EDIT untuk attachment FILE 
    19/07/2021 [VIN/SIER] bug cancel - EDIT attachment FILE 
    11/10/2021 [WED/ALL] Merubah label Over Sea menjadi Overseas
    16/11/2021 [ISD/IOK] tambah dialog list item untuk actual item baru based on parameter mIsSO2UseActualItem
    16/02/2022 [TKG/GSS] Merubah GetParameter() dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmSO2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mCity = string.Empty, mCnt = string.Empty, mSADNo = string.Empty
            ;
        internal FrmSO2Find FrmFind;
        internal int mNumberOfSalesUomCode = 1;
        internal bool
            mIsCtQtUseUPrice2 = false,
            mIsCustomerItemNameMandatory = false,
            mIsSOUseARDPValidated = false,
            mIsFilterCity = false,
            mIsContainerSize = false,
            mIsSODisplayExpiredCtQt = false,
            mIsSO2DeliveryDateNotMandatory = false,
            mIsSO2NotUsePackagingLabel = false,
            mIsCustomerComboBasedOnCategory = false,
            mIsFilterByCtCt = false,
            mIsShowCustomerCategory = false,
            mIsFilterByItCt = false,
            mIsSO2UseActualItem = false
            ;
        internal string mIsSoUseDefaultPrintout;
        private bool
            mInitOverseaInd = false,
            mPresentOverseaInd = false,
            mIsAutoJournalActived = false,
            mIsSO2JournalEnabled = false,
            mIsSO2AllowToUploadFile =false,
            IsInsert = false,
            mIsSOAttachmentEditable = false
            ;
        private string
           mPortForFTPClient = string.Empty,
           mHostAddrForFTPClient = string.Empty,
           mSharedFolderForFTPClient = string.Empty,
           mUsernameForFTPClient = string.Empty,
           mPasswordForFTPClient = string.Empty,
           mFileSizeMaxUploadFTPClient = string.Empty,
           mFormatFTPClient = string.Empty,
           mLocalDocument = "0",
           mSO2BankAcCode = string.Empty,
           mRuleToDeactivateCtQt = string.Empty,
           mMainCurCode = string.Empty;
        private byte[] downloadedData;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSO2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Sales Order";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetNumberOfSalesUomCode();
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Tc1.SelectedTabPage = Tp3;
                SetLueBankAcCode(ref LueBankAccount);

                Tc1.SelectedTabPage = Tp6;
                SetLueStatus(ref LueStatus);
                if (mIsCustomerComboBasedOnCategory) 
                    Sl.SetLueCtCtCode(ref LueCtCtCode);
                else
                {
                    LblCtCtCode.Visible = LueCtCtCode.Visible = false;
                    Sl.SetLueCtCode(ref LueCtCode);
                }

                Tc1.SelectedTabPage = Tp1;

                mIsCtQtUseUPrice2 = Sm.GetParameter("IsCtQtUseUPrice2") == "Y";
                if (mLocalDocument=="1") LblLocalDoc.Text = "Sales Contract#";

                base.FrmLoad(sender, e);
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                LblPack.Text = LblQty.Text = LblVol.Text = LblJumlahContainer.Text = Sm.FormatNum(0, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 35;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Agent Code",
                        "Agent",
                        "Item's"+Environment.NewLine+"Code",
                        "",

                        //6-10
                        "Item's Name",
                        "Packaging",
                        "Quantity"+Environment.NewLine+"Packaging",
                        "Quantity",
                        "UoM",

                        //11-15
                        "Price"+Environment.NewLine+"(Price List)",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        "Price After"+Environment.NewLine+"Discount",                        
                        "Promo"+Environment.NewLine+"%",

                        //16-20
                        "Price"+Environment.NewLine+"Before Tax",
                        "Tax"+Environment.NewLine+"%",
                        "Tax"+Environment.NewLine+"Amount",
                        "Price"+Environment.NewLine+"After Tax",                        
                        "Total",

                        //21-25
                        "Delivery"+Environment.NewLine+"Date",
                        "Remark",
                        "CtQtDNo",
                        "Specification",
                        "Customer's"+Environment.NewLine+"Item Code",

                        //26-30
                        "Customer's"+Environment.NewLine+"Item Name",
                        "Volume",
                        "Total Volume",
                        "Uom"+Environment.NewLine+"Volume",
                        "Container"+Environment.NewLine+"Group",

                        //31-34
                        "Container"+Environment.NewLine+"Quantity",
                        "Item's"+Environment.NewLine+"Actual Code",
                        "",
                        "Item's Actual Name"

                    },
                    new int[] 
                    {
                        //0
                        40,

                        //1-5
                        20, 0, 180, 80, 20, 
                        
                        //6-10
                        200, 100, 100, 80, 80, 
                        
                        //11-15
                        100, 80, 100, 100, 80,
                        
                        //16-20
                        100, 80, 100, 100, 120,   
                        
                        //21-25
                        100, 400, 0, 120, 120,

                        //26-30
                        250, 100, 100, 100, 100,

                        //31-34
                        80, 80, 20, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 5, 33 });
            Sm.GrdFormatDate(Grd1, new int[] { 21 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28, 31}, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 34 });
            Grd1.Cols[24].Move(7);
            Grd1.Cols[26].Move(7);
            Grd1.Cols[25].Move(7);
            Grd1.Cols[27].Move(14);
            Grd1.Cols[28].Move(15);
            Grd1.Cols[29].Move(16); 
            Grd1.Cols[30].Move(27);
            Grd1.Cols[31].Move(28);
            Grd1.Cols[32].Move(7);
            Grd1.Cols[33].Move(8);
            Grd1.Cols[34].Move(9);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 5, 12, 14, 15, 17, 18, 23, 24, 27, 28, 29, 32 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 25, 26 });
            if (!mIsSO2UseActualItem)
                Sm.GrdColInvisible(Grd1, new int[] { 32, 33, 34});

            #region Grid 2

            Grd2.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "AR Downpayment#", 
                        
                        //1-5
                        "Date",
                        "Currency",
                        "Amount",
                        "Person In Charge",
                        "Voucher Request#",

                        //6-7
                        "Voucher#",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        80, 80, 100, 200, 150, 
                        
                        //6-7
                        150, 250 
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 4, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 12, 14, 15, 17, 18, 24, 25, 27, 28, 29, 32 }, !ChkHideInfoInGrd.Checked);

            Sm.GrdColInvisible(Grd2, new int[] { 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtPONumb, TxtCtQuot, TxtLocalDocNo, LueCtCode, 
                        LueContactCode, MeeRemark,  ChkCancelInd, LueStatus, LueSPCode, MeeShipment,
                        LueShpMCode, LueCity, LuePort, DteContract, MeeComodity, 
                        MeeTolerance, LueBankAccount, MeeValidity, TxtSignName, TxtContainerSize,
                        TxtFile2, LueCtCtCode
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    ChkOverSea.Properties.ReadOnly = true;
                    BtnCustomerShipAddress.Enabled = false;
                    BtnSoQuotPromoDlg.Enabled = false;
                    BtnContact.Enabled = false;
                    BtnCtQt.Enabled = false;
                    if (mRuleToDeactivateCtQt == "1") BtnCtQt.Visible = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2, 33 });
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsSO2AllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    TxtDocNo.Focus();
                    BtnCtCode.Enabled = false;
                    if (!mIsShowCustomerCategory) BtnCtCode.Visible = false;
                    else LblCtCtCode.ForeColor = System.Drawing.Color.Black;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueCtCode, LueContactCode, TxtPONumb,MeeShipment,
                       LueCity, LuePort, DteContract, MeeComodity, MeeTolerance, LueBankAccount, 
                       MeeRemark, MeeValidity, TxtSignName, TxtContainerSize
                    }, false);
                    if (mIsCustomerComboBasedOnCategory) 
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode }, false);
                    }
                    BtnCustomerShipAddress.Enabled = true;
                    BtnSoQuotPromoDlg.Enabled = true;
                    BtnContact.Enabled = true;
                    if (mRuleToDeactivateCtQt == "2") BtnCtQt.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 33 });
                    ChkOverSea.Properties.ReadOnly = false;
                    LblPack.Text = LblQty.Text = LblVol.Text = LblJumlahContainer.Text = Sm.FormatNum(0, 0);
                    if (mIsSO2AllowToUploadFile)
                    {
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                    }
                    ChkFile2.Enabled = true;

                    DteDocDt.Focus();
                    if (mIsShowCustomerCategory)
                    {
                        BtnCtCode.Enabled = true;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode, LueCtCode }, true);
                    }
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueStatus, MeeRemark }, false);
                    LueStatus.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    if (!ChkCancelInd.Checked) ChkOverSea.Properties.ReadOnly = false;
                    if (mIsSOAttachmentEditable) BtnFile2.Enabled = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 33 });
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mInitOverseaInd = false;
            mPresentOverseaInd = false;
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, TxtPONumb, TxtCtQuot, LueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, TxtSOQuotPromo, 
                  LueCtCode, LueContactCode, MeeRemark, LueShpMCode, LueSPCode, MeeShipment,
                  LueCity, LuePort, DteContract, MeeComodity, MeeTolerance, 
                  LueBankAccount,MeeValidity, TxtSignName, TxtContainerSize, TxtFile2, LueCtCtCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt}, 0);
            ChkCancelInd.Checked = false;
            ChkOverSea.Checked = false;
            ClearGrd();
            LblPack.Text = LblQty.Text = LblVol.Text = Sm.FormatNum(0, 0);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3 });
        }

        private void ClearData2()
        {
            //TxtAddress.EditValue = Sm.DrStr(dr, c[0]);
            //mCity = Sm.DrStr(dr, c[1]);
            //TxtCity.EditValue = Sm.DrStr(dr, c[2]);
            //mCnt = Sm.DrStr(dr, c[3]);
            //TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
            //TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
            //TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
            //TxtFax.EditValue = Sm.DrStr(dr, c[7]);
            //TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
            //TxtMobile.EditValue = Sm.DrStr(dr, c[9]);

            mSADNo = string.Empty;
            mCity = string.Empty;
            mCnt = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, 
                TxtFax, TxtEmail, TxtMobile, TxtSAName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSO2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueStatus, Sm.GetValue("Select 'O' "));
                IsInsert = true;
                if (mIsCustomerComboBasedOnCategory)
                {
                    Sl.SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N", string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (ChkCancelInd.Checked == false)
            {
                ParPrint();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 &&
                       !Sm.IsLueEmpty(LueCtCode, "Customer") &&
                       !Sm.IsTxtEmpty(TxtCtQuot, "Customer's Quotation", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmSO2Dlg2(
                        this,
                        e.RowIndex,
                        mNumberOfSalesUomCode > 1,
                        mIsCtQtUseUPrice2,
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0,
                        !BtnSave.Enabled,
                        Sm.GetLue(LueCtCode),
                        TxtCtQuot.Text, TxtSOQuotPromo.Text,
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4),
                        mIsSO2NotUsePackagingLabel));
            }

            //if (BtnSave.Enabled)
            //{
            //    if (TxtDocNo.Text.Length == 0)
            //    {
                   

            //        if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            //        {
            //            Sm.GrdRequestEdit(Grd1, e.RowIndex);
            //            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 9, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            //        }
            //    }
            //}

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 33){ Sm.FormShowDialog(new FrmSO2Dlg8(this)); }
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 21)
            {
                if (Sm.GetGrdDate(Grd1, 0, 21).Length != 0)
                {
                    var DeliveryDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 21));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 30).Length == 0)
                        {
                            Grd1.Cells[Row, 21].Value = DeliveryDt;
                        }
                        else
                        {
                            var ContainerGroup = Sm.GetGrdStr(Grd1, Row, 30);
                            if (Sm.GetGrdStr(Grd1, Row, 21).Length == 0)
                            {
                                Grd1.Cells[Row, 21].Value = DeliveryDt;
                            }
                            else
                            {
                                var DeliveryDtCG = Sm.ConvertDate(Sm.GetGrdDate(Grd1, Row, 21));
                                for (int Rowx = 0; Rowx < Grd1.Rows.Count; Rowx++)
                                {
                                    if (ContainerGroup == Sm.GetGrdStr(Grd1, Rowx, 30))
                                        Grd1.Cells[Rowx, 21].Value = DeliveryDtCG;
                                }
                            }
                        }
                    }
                }
            }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotal();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
                !Sm.IsLueEmpty(LueCtCode, "Customer") &&
                !Sm.IsTxtEmpty(TxtCtQuot, "Customer's Quotation", false))
                Sm.FormShowDialog(
                    new FrmSO2Dlg2(
                        this,
                        e.RowIndex,
                        mNumberOfSalesUomCode>1,
                        mIsCtQtUseUPrice2,
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0,
                        !BtnSave.Enabled,
                        Sm.GetLue(LueCtCode),
                        TxtCtQuot.Text, 
                        TxtSOQuotPromo.Text,
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4),
                        mIsSO2NotUsePackagingLabel)
                    );

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 33){ Sm.FormShowDialog(new FrmSO2Dlg8(this)); }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SO", "TblSOHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOHdr(DocNo));
            cml.Add(SaveSODtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) 
            //        cml.Add(SaveSODtl(DocNo, Row));

            if (mIsAutoJournalActived && mIsSO2JournalEnabled) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);
            if (mIsSO2AllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);

            IsInsert = false;

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueStatus, "Status") ||
                (mIsCustomerComboBasedOnCategory && !mIsShowCustomerCategory && Sm.IsLueEmpty(LueCtCtCode, "Customer Category")) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsLueEmpty(LueContactCode, "Contact person") ||
                Sm.IsTxtEmpty(TxtSAName, "Shipping name", false) ||
                Sm.IsTxtEmpty(TxtAddress, "Address", false) ||
                Sm.IsTxtEmpty(TxtCtQuot, "Customer's quotation", false) ||
                Sm.IsLueEmpty(LueSPCode, "Sales person") ||
                Sm.IsLueEmpty(LueShpMCode, "Shipment method") ||
                IsContainerSize() ||
                IsDocExpired() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()||
                IsGrdDeliverDateNotValid() ||
                (mIsAutoJournalActived && mIsSO2JournalEnabled && Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)));
        }
        private bool IsContainerSize()
        {
            if (ChkOverSea.Checked == true)
            {
                if (TxtContainerSize.Text.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Container Size is empty");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;      
            return false;
        }

        private bool IsGrdDeliverDateNotValid()
        {
            if (Sm.GetGrdStr(Grd1, 0, 21).Length == 0 && !mIsSO2DeliveryDateNotMandatory)
            {
                Sm.StdMsg(mMsgType.Warning, "Delivery date is empty.");
                return true;
            }
            //if (Sm.IsGrdValueEmpty(Grd1, 0, 21, false, "Delivery date is empty.")) return true;

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsDocExpired()
        {
            var CtQtDocNo = string.Empty;
            var CtCode = string.Empty;
            if (TxtCtQuot.Text.Length == 0) return false;

            var SQL = new StringBuilder();

            if (TxtCtQuot.Text.Length >= 0)
                CtQtDocNo = TxtCtQuot.Text;
            if (Sm.GetLue(LueCtCode).Length > 0)
                CtCode = Sm.GetLue(LueCtCode);

            SQL.AppendLine("Select Replace(Date_Add(QtStartDt, Interval QtMth Month), '-', '') As ExpDt ");
            SQL.AppendLine("From TblCtQtHdr ");
            SQL.AppendLine("Where Status='A' And ActInd='Y' And CtCode='"+CtCode+"' And QtStartDt Is Not Null And QtMth<>0 And DocNo=@Param; ");

            var ExpDt = Sm.GetValue(SQL.ToString(), CtQtDocNo);
            if (ExpDt.Length == 0) return false;

            var DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);

            if (Sm.CompareDtTm(ExpDt, DocDt)<=0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Expired Date : " + String.Format("{0:dd/MMM/yyyy}",
                        new DateTime(
                        Int32.Parse(ExpDt.Substring(0, 4)),
                        Int32.Parse(ExpDt.Substring(4, 2)),
                        Int32.Parse(ExpDt.Substring(6, 2))
                        )) + Environment.NewLine + 
                    "Customer's quotation already expired."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveSOHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Sales Order (Hdr) */ ");
            SQL.AppendLine("Insert Into TblSOHdr(DocNo, LocalDocNo, DocDt, Status, CancelInd, OverSeaInd, ");
            SQL.AppendLine("CtCode, CtContactPersonName, CtPONo, SOQuotPromoDocNo, CtQtDocNo, CurCode, Amt, ");
            SQL.AppendLine("SADNo, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, ");
            SQL.AppendLine("SAPhone, SAFax, SAEmail, SAMobile, SpCode, ShpMCode,  ");
            SQL.AppendLine("CityCode, PortCode, DteContract, Comodity, Tolerance, BankAcNo, Shipment, ");
            SQL.AppendLine("Remark, Validity, SignName, ContainerSize, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @LocalDocNo, @DocDt, @Status, @CancelInd, @OverSeaInd, ");
            SQL.AppendLine("@CtCode, @CtContactPersonName, @CtPONo, @SOQuotPromoDocNo, @CtQtDocNo, ");
            SQL.AppendLine("(Select CurCode From TblCtQtHdr Where DocNo=@CtQtDocNo), @Amt, ");
            SQL.AppendLine("@SADNo, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, ");
            SQL.AppendLine("@SAPhone, @SAFax, @SAEmail, @SAMobile, @SpCode, @ShpMCode,  ");
            SQL.AppendLine("@CityCode, @PortCode, @DteContract, @Comodity, @Tolerance, @BankAcNo, @Shipment, ");
            SQL.AppendLine("@Remark, @Validity, @SignName, @ContainerSize, @UserCode, CurrentDateTime()); ");
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@OverSeaInd", ChkOverSea.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", LueContactCode.Text);
            Sm.CmParam<String>(ref cm, "@CtPONo", TxtPONumb.Text);
            Sm.CmParam<String>(ref cm, "@SOQuotPromoDocNo", TxtSOQuotPromo.Text);
            Sm.CmParam<String>(ref cm, "@CtQtDocNo", TxtCtQuot.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@SADNo", mSADNo);
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", TxtAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCity);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCnt);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParam<String>(ref cm, "@ShpMCode", Sm.GetLue(LueShpMCode));
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCity));
            Sm.CmParam<String>(ref cm, "@PortCode", Sm.GetLue(LuePort));
            Sm.CmParamDt(ref cm, "@DteContract", Sm.GetDte(DteContract));
            Sm.CmParam<String>(ref cm, "@Comodity", MeeComodity.Text);
            Sm.CmParam<String>(ref cm, "@Tolerance", MeeTolerance.Text);
            Sm.CmParam<String>(ref cm, "@BankAcNo", Sm.GetLue(LueBankAccount));
            Sm.CmParam<String>(ref cm, "@Shipment",MeeShipment.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Validity", MeeValidity.Text);
            Sm.CmParam<String>(ref cm, "@SignName", TxtSignName.Text);
            Sm.CmParam<String>(ref cm, "@ContainerSize", TxtContainerSize.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSODtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Order (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSODtl(DocNo, DNo, AgtCode, PackagingUnitUomCode, QtyPackagingUnit, Qty, TaxRate, DeliveryDt, ContainerGroup, ContainerGroupQty, CtQtDNo, ");
                        if (mIsSO2UseActualItem) SQL.AppendLine("ItCode2, ");
                        SQL.AppendLine("Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AgtCode_" + r.ToString() +
                        ", @PackagingUnitUomCode_" + r.ToString() +
                        ", @QtyPackagingUnit_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @TaxRate_" + r.ToString() +
                        ", @DeliveryDt_" + r.ToString() +
                        ", @ContainerGroup_" + r.ToString() +
                        ", @ContainerGroupQty_" + r.ToString() +
                        ", @CtQtDNo_" + r.ToString() +
                        ", ");
                    if (mIsSO2UseActualItem) SQL.AppendLine("@ItCode2_" + r.ToString() + ", ");
                    SQL.AppendLine("@Remark_" + r.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AgtCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@CtQtDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 23));
                    Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@TaxRate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                    Sm.CmParamDt(ref cm, "@DeliveryDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 21));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 22));
                    Sm.CmParam<String>(ref cm, "@ContainerGroup_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 30));
                    Sm.CmParam<Decimal>(ref cm, "@ContainerGroupQty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 31));
                    Sm.CmParam<String>(ref cm, "@ItCode2_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 32));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveSODtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();
        //    SQL.AppendLine("Insert Into TblSODtl(DocNo, DNo, AgtCode, PackagingUnitUomCode, QtyPackagingUnit, Qty, TaxRate, DeliveryDt, ContainerGroup, ContainerGroupQty, CtQtDNo, ");
        //    if (mIsSO2UseActualItem) SQL.AppendLine("ItCode2, ");
        //    SQL.AppendLine("Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values ");
        //    SQL.AppendLine("(@DocNo, @DNo, @AgtCode, @PackagingUnitUomCode, @QtyPackagingUnit, @Qty, @TaxRate, @DeliveryDt, @ContainerGroup, @ContainerGroupQty, @CtQtDNo, ");
        //    if (mIsSO2UseActualItem) SQL.AppendLine("@ItCode2, ");
        //    SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()) ");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AgtCode", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@CtQtDNo", Sm.GetGrdStr(Grd1, Row, 23));
        //    Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd1, Row, 17));
        //    Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd1, Row, 21));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
        //    Sm.CmParam<String>(ref cm, "@ContainerGroup", Sm.GetGrdStr(Grd1, Row, 30));
        //    Sm.CmParam<Decimal>(ref cm, "@ContainerGroupQty", Sm.GetGrdDec(Grd1, Row, 31));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@ItCode2", Sm.GetGrdStr(Grd1, Row, 32));

        //    return cm;
        //}

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Sales Order with Fixed Price : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblSOHdr Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Set @row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, C.ParValue, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine("        Select Concat(G.ParValue, A.CtCode) As AcNo, ");
            SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("        End * ");
            SQL.AppendLine("        B.Qty*(C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0.00))) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblSOHdr A ");
            SQL.AppendLine("        Inner Join TblSODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DocNo And C.ItemPriceDNo=E.DNo ");
            SQL.AppendLine("        Left Join TblSOQuotPromoItem F On A.SOQuotPromoDocNo=F.DocNo And E.ItCode=F.ItCode  ");
            SQL.AppendLine("        Inner Join TblParameter G On G.ParCode='CustomerAcNoNonInvoice' And G.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select F.ParValue As AcNo, ");
            SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("        End * ");
            SQL.AppendLine("        B.Qty*E.UPrice*0.010*(100.0000-(C.UPrice*100.0000/E.UPrice)) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblSOHdr A ");
            SQL.AppendLine("        Inner Join TblSODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DocNo And C.ItemPriceDNo=E.DNo ");
            SQL.AppendLine("        Inner Join TblParameter F On F.ParCode='AcNoForSO2DiscountAmt' And F.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select F.ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        Case When A.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) ");
            SQL.AppendLine("        End * ");
            SQL.AppendLine("        B.Qty*IfNull(E.UPrice, 0.00) As CAmt ");
            SQL.AppendLine("        From TblSOHdr A ");
            SQL.AppendLine("        Inner Join TblSODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DocNo And C.ItemPriceDNo=E.DNo ");
            SQL.AppendLine("        Inner Join TblParameter F On F.ParCode='AcNoForSO2ProjectRevenue' And F.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Left Join TblParameter C On C.ParCode='JournalEntCodeDefault' ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblSOHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            
            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (ChkCancelInd.Checked)
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            }
            else
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid2()) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mIsSOAttachmentEditable && TxtFile2.Text.Length > 0 ) UploadFile2(TxtDocNo.Text);
            cml.Add(UpdateSOHdr());
            

            if (mIsAutoJournalActived && mIsSO2JournalEnabled && ChkCancelInd.Checked) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsSOAlreadyCancelled() ||
                IsSOAlreadyFullfiled() ||
                IsSOAlreadyManualFullfiled() ||
                IsSOAlreadyPartial()||
                IsSOAlreadyProcessedToDRSP()||
                IsSOHaveDROutstanding()||
                IsSODtlProcessIndDifferent()||
                IsSOAlreadyProcessedToARDP() ||
                (mIsSO2JournalEnabled && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)))
                ;
        }

        private bool IsCancelledDataNotValid2()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsSOAlreadyCancelled() ||
                IsSOAlreadyFullfiled()||
                IsSOAlreadyManualFullfiled() ||
                IsSOAlreadyProcessedToDRSP2() ||
                IsSOHaveDROutstanding()||
                IsSOAlreadyProcessedToARDP();
        }

        private MySqlCommand UpdateSOHdr()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOHdr Set ");
            SQL.AppendLine("    Status=@Status, CancelInd=@CancelInd, Remark=@Remark, OverseaInd = @OverseaInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@OverseaInd", ChkOverSea.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsSOAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOHdr " +
                    "Where DocNo=@DocNo And CancelInd='Y' ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already cancelled.");
                ChkCancelInd.Checked = true;
                return true;
            }
            return false;
        }

        private bool IsSOAlreadyFullfiled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOHdr " +
                    "Where DocNo=@DocNo And Status = 'F';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already fullfiled.");
                Sm.SetLue(LueStatus, "F");
                return true;
            }
            return false;
        }

        private bool IsSOAlreadyManualFullfiled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOHdr " +
                    "Where DocNo=@DocNo And Status = 'M';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already manual fullfiled.");
                Sm.SetLue(LueStatus, "M");
                return true;
            }
            return false;
        }

        private bool IsSODtlProcessIndDifferent()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSODtl " +
                    "Where DocNo=@DocNo And (ProcessInd <>'O' OR ProcessInd2<>'O' Or ProcessInd3 <>'O');"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data have different process indicator.");
                return true;
            }
            return false;
        }

        private bool IsSOAlreadyPartial()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOHdr " +
                    "Where DocNo=@DocNo And Status = 'P';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document partial already processed to delivery request.");
                return true;
            }
            return false;
        }

        private bool IsSOAlreadyProcessedToDRSP()
        {
            var SQL = new StringBuilder();
            string doc = "";

            if (ChkOverSea.Checked == false)
            {
                SQL.AppendLine("Select B.SODocNo From TblDrHdr A ");
                SQL.AppendLine("Inner Join TblDRDtl B On A.DocNo=B.DocNo And B.SODocNo=@DocNo ");
                SQL.AppendLine("Where A.CancelInd = 'N' Limit 1; ");
                doc = "delivery request";
            }
            else
            {
                SQL.AppendLine("Select Distinct B.SODocNo From TblSIHdr A ");
                SQL.AppendLine("Inner Join TblSiDtl B On A.DocNo=B.DocNo And B.SODocNo=@DocNo ");
                SQL.AppendLine("Inner Join TblSP C On A.SPDocno = C.DocNo ");
                SQL.AppendLine("Where C.Status <> 'C' Limit 1; ");
                doc = "shipment planning";
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to "+doc+".");
                return true;
            }
            return false;
        }

        private bool IsSOAlreadyProcessedToDRSP2()
        {
            mPresentOverseaInd = ChkOverSea.Checked;

            if (mInitOverseaInd != mPresentOverseaInd)
            {
                var SQL1 = new StringBuilder();
                var SQL2 = new StringBuilder();

                SQL1.AppendLine("Select B.SODocNo From TblDrHdr A ");
                SQL1.AppendLine("Inner Join TblDRDtl B On A.DocNo=B.DocNo And B.SODocNo=@Param ");
                SQL1.AppendLine("Where A.CancelInd = 'N' Limit 1; ");

                SQL2.AppendLine("Select Distinct B.SODocNo From TblSIHdr A ");
                SQL2.AppendLine("Inner Join TblSiDtl B On A.DocNo=B.DocNo And B.SODocNo=@Param ");
                SQL2.AppendLine("Inner Join TblSP C On A.SPDocno = C.DocNo ");
                SQL2.AppendLine("Where C.Status <> 'C' Limit 1; ");

                if (Sm.IsDataExist(SQL1.ToString(), TxtDocNo.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data has been processed to Delivery Request.");
                    return true;
                }

                if (Sm.IsDataExist(SQL2.ToString(), TxtDocNo.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data has been processed to Shipment Planning.");
                    return true;
                }
            }

            return false;
        }

        private bool IsSOAlreadyProcessedToARDP()
        {
            var SQL = new StringBuilder();
           
            SQL.AppendLine("Select SODocNo ");
            SQL.AppendLine("From TblARDownpayment ");
            SQL.AppendLine("Where SODocNo=@SODocNo And CancelInd = 'N' And Status = 'A' And CtCode=@CtCode ");
            SQL.AppendLine("Limit 1; ");
          
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

            if (Sm.IsDataExist(cm) && mIsSOUseARDPValidated && Sm.GetLue(LueStatus)!= "M")
            {
                Sm.StdMsg(mMsgType.Warning, "This document already use to AR DownPayment.");
                return true;
            }
            return false;


        }

        private bool IsSOHaveDROutstanding()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.SODocNo From TblDrHdr A ");
            SQL.AppendLine("Inner Join TblDRDtl B On A.DocNo=B.DocNo And B.SODocNo=@DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' And A.ProcessInd = 'O' Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document have outstanding delivery request,"+Environment.NewLine+" you must cancel document delivery request .");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblSOHdr Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblSOHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblSOHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And CancelInd='Y' ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSOHdr(DocNo);
                ShowSODtl(DocNo);
                ShowARDownPayment(DocNo);
                ComputeTotal();
                ComputeTotalQuantity();
                ComputeTotalContainer();
                mInitOverseaInd = ChkOverSea.Checked;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (mIsCustomerComboBasedOnCategory)
            {
                Sl.SetLueCtCtCode(ref LueCtCtCode);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty);
            }

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.OverSeaInd, A.CtCode, A.CtContactPersonName, A.CtPONo, A.SOQuotPromoDocNo, ");
            SQL.AppendLine("A.CtQtDocNo, A.Amt, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("ifnull(A.ShpMCode, B.ShpMCode) As ShpMCode, ");
            SQL.AppendLine("A.Remark, ");
            SQL.AppendLine("A.SADNo, A.FileName, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, ");
            SQL.AppendLine("A.CityCode, A.PortCode, A.DteContract, A.Comodity, ");
            SQL.AppendLine("A.Tolerance, A.bankAcNo, A.Shipment, A.Validity, A.SignName, A.ContainerSize, H.CtCtCode ");
            SQL.AppendLine("From TblSOHdr A  ");
            SQL.AppendLine("Inner Join TblCtQtHdr B On A.CtQtDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Left Join TblCustomer H On A.CtCode = H.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "LocalDocNo", "DocDt", "Status", "CancelInd", "OverSeaInd",    

                    //6-10
                    "CtCode", "CtContactPersonName", "CtPONo", "SOQuotPromoDocNo", "CtQtDocNo",   
                    
                    //11-15
                    "Amt", "SAName", "SpCode", "ShpMCode", "Remark", 
                    
                    //16-20
                    "SADNo", "SAAddress", "SACityCode", "CityName", "SACntCode", 
                    
                    //21-25
                    "CntName", "SAPostalCode", "SAPhone", "SAFax", "SAEmail", 
                    
                    //26-30
                    "SAMobile", "CityCode", "PortCode", "DteContract", "Comodity",  
                   
                    //31-35
                    "Tolerance", "BankAcNo", "Shipment", "Validity","SignName",

                    //36-38
                    "ContainerSize", "FileName", "CtCtCode"
                    
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    Sm.SetLue(LueStatus, Sm.DrStr(dr, c[3]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    ChkOverSea.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[6]));
                    SetLueCtPersonCodeShow(ref LueContactCode, Sm.DrStr(dr, c[0]));
                    Sm.SetLue(LueContactCode, Sm.GetValue("Select CtContactPersonName From TblSOHdr Where DocNo= '" + Sm.DrStr(dr, c[0]) + "' "));
                    TxtPONumb.EditValue = Sm.DrStr(dr, c[8]);
                    TxtSOQuotPromo.EditValue = Sm.DrStr(dr, c[9]);
                    TxtCtQuot.EditValue = Sm.DrStr(dr, c[10]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    TxtSAName.EditValue = Sm.DrStr(dr, c[12]);
                    SetLueSPCode(ref LueSPCode);
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[13]));
                    SetLueDTCode(ref LueShpMCode);
                    Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[14]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    mSADNo = Sm.DrStr(dr, c[16]);
                    TxtAddress.EditValue = Sm.DrStr(dr, c[17]);
                    mCity = Sm.DrStr(dr, c[18]);
                    TxtCity.EditValue = Sm.DrStr(dr, c[19]);
                    mCnt = Sm.DrStr(dr, c[20]);
                    TxtCountry.EditValue = Sm.DrStr(dr, c[21]);
                    TxtPostalCd.EditValue = Sm.DrStr(dr, c[22]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[23]);
                    TxtFax.EditValue = Sm.DrStr(dr, c[24]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[25]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[26]);
                    SetLueCityPort(ref LueCity, TxtCtQuot.Text, "2");
                    Sm.SetLue(LueCity, Sm.DrStr(dr, c[27]));
                    SetLueCityPort(ref LuePort, TxtCtQuot.Text, "1");
                    Sm.SetLue(LuePort, Sm.DrStr(dr, c[28]));
                    Sm.SetDte(DteContract, Sm.DrStr(dr, c[29]));
                    MeeComodity.EditValue = Sm.DrStr(dr, c[30]);
                    MeeTolerance.EditValue = Sm.DrStr(dr, c[31]);
                    Sm.SetLue(LueBankAccount, Sm.DrStr(dr, c[32]));
                    MeeShipment.EditValue = Sm.DrStr(dr, c[33]);
                    MeeValidity.EditValue = Sm.DrStr(dr, c[34]);
                    TxtSignName.EditValue = Sm.DrStr(dr, c[35]);
                    TxtContainerSize.EditValue = Sm.DrStr(dr, c[36]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[37]);
                    if (mIsCustomerComboBasedOnCategory) Sm.SetLue(LueCtCtCode, Sm.DrStr(dr, c[38]));

                    //ShowShippingAddressData2(Sm.GetLue(LueCtCode), TxtAddress.Text);
                }, true
            );
        }

        private void ShowSODtl(string DocNo)
        {
            var SQL = new StringBuilder();

             SQL.AppendLine("Select T.*, (UPriceBefore+TaxAmt) As UPriceAfterTax, (UPriceBefore+TaxAmt)*Qty As Total ");
             SQL.AppendLine("From ( ");
             SQL.AppendLine("   Select B.Dno, B.AgtCode, H.AgtName, E.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
             SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, D.PriceUomCode, ");
             SQL.AppendLine("   E.UPrice, C.Discount, ifnull((E.UPrice*C.Discount *0.01), 0) As DiscountAmt, ");
             SQL.AppendLine("   C.UPrice As UPriceAfterDiscount, ");
             SQL.AppendLine("   IfNull(F.DiscRate, 0) As PromoRate,  ");
             SQL.AppendLine("   (C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0))) As UPriceBefore, ");
             SQL.AppendLine("   B.TaxRate,  ");
             SQL.AppendLine("   ((C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0)))*0.01*B.TaxRate) As TaxAmt, ");
             SQL.AppendLine("   B.DeliveryDt, B.Remark, B.CtQtDNo, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume,  ");
             SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom, B.ContainerGroup, B.ContainerGroupQty,  ");
            if (mIsSO2UseActualItem)
                SQL.AppendLine("B.ItCode2, K.ItName As ItName2 ");
            else
                SQL.AppendLine("Null As ItCode2, Null As ItName2 ");
            SQL.AppendLine("   From TblSOHdr A   ");
             SQL.AppendLine("   Inner Join TblSODtl B On A.DocNo=B.DocNo  ");
             SQL.AppendLine("   Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
             SQL.AppendLine("   Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo ");
             SQL.AppendLine("   Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DocNo And C.ItemPriceDNo=E.DNo ");
             SQL.AppendLine("   Left Join TblSOQuotPromoItem F On A.SOQuotPromoDocNo=F.DocNo And E.ItCode=F.ItCode  ");
             SQL.AppendLine("   Inner Join TblItem G On E.ItCode=G.ItCode ");
             SQL.AppendLine("   Left Join TblAgent H On B.AgtCode=H.AgtCode ");
             SQL.AppendLine("   Left Join TblCustomerItem I On E.ItCode=I.ItCode And A.CtCode=I.CtCode ");
             SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And B.packagingUnitUomCode = J.UomCode ");
             if (mIsSO2UseActualItem) SQL.AppendLine("   left Join TblItem K On B.ItCode2=K.ItCode ");
             SQL.AppendLine("   Where A.DocNo=@DocNo ");
             SQL.AppendLine(") T Order By DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "AgtCode", "AgtName", "ItCode", "ItName", "PackagingUnitUomCode",  
                    //6-10
                    "QtyPackagingUnit", "Qty", "PriceUomCode", "UPrice", "Discount", 
                    //11-15
                    "DiscountAmt", "UPriceAfterDiscount", "PromoRate",  "UPriceBefore", "TaxRate" , 
                    //16-20
                    "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt", "Remark", 
                    //21-25
                    "CtQtDNo", "Specification", "CtItCode", "CtItName", "Volume",
                    //26-30
                    "TotalVolume", "VolUom", "ContainerGroup", "ContainerGroupQty", "ItCode2",
                    //31
                    "ItName2"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 25);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 31);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28, 31 });
            Sm.FocusGrd(Grd1, 0, 1);
          
        }

        //private void ShowShippingAddressData(string CtCode, string SADno)
        //{
        //    var cm = new MySqlCommand();
        //    Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
        //    //Sm.CmParam<String>(ref cm, "@ShipName", ShipName);
        //    Sm.CmParam<String>(ref cm, "@SADno", SADno);

        //    Sm.ShowDataInCtrl(
        //            ref cm,
        //            "Select A.Address, A.CityCode, A.CntCode, B.CityName, C.CntName, A.PostalCd, A.Phone, A.Fax, A.Email, A.Mobile " +
        //            "From TblCustomerShipAddress A " +
        //            "Left Join TblCity B On A.CityCode = B.CityCode " +
        //            "Left Join TblCountry C On A.CntCode = C.CntCode Where A.CtCode=@CtCode And A.Dno = @SADno ",
        //            new string[] 
        //            { 
        //                "Address", 
        //                "CityCode", "CityName", "CntCode", "CntName", "PostalCd",  
        //                "Phone", "Fax", "Email", "Mobile", 
        //            },
        //            (MySqlDataReader dr, int[] c) =>
        //            {
        //                TxtAddress.EditValue = Sm.DrStr(dr, c[0]);
        //                mCity = Sm.DrStr(dr, c[1]);
        //                TxtCity.EditValue = Sm.DrStr(dr, c[2]);
        //                mCnt = Sm.DrStr(dr, c[3]);
        //                TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
        //                TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
        //                TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
        //                TxtFax.EditValue = Sm.DrStr(dr, c[7]);
        //                TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
        //                TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
        //            }, false
        //        );
        //}

        //private void ShowShippingAddressData2(string CtCode, string SAAddress)
        //{
        //    var cm = new MySqlCommand();
        //    Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
        //    //Sm.CmParam<String>(ref cm, "@ShipName", ShipName);
        //    Sm.CmParam<String>(ref cm, "@SAAddress", SAAddress);

        //    Sm.ShowDataInCtrl(
        //            ref cm,
        //            "Select A.Address, A.CityCode, A.CntCode, B.CityName, C.CntName, A.PostalCd, A.Phone, A.Fax, A.Email, A.Mobile " +
        //            "From TblCustomerShipAddress A " +
        //            "Left Join TblCity B On A.CityCode = B.CityCode " +
        //            "Left Join TblCountry C On A.CntCode = C.CntCode Where A.CtCode=@CtCode And A.Address = @SAAddress ",
        //            new string[] 
        //            { 
        //                "Address", 
        //                "CityCode", "CityName", "CntCode", "CntName", "PostalCd",  
        //                "Phone", "Fax", "Email", "Mobile", 
        //            },
        //            (MySqlDataReader dr, int[] c) =>
        //            {
        //                //TxtAddress.EditValue = Sm.DrStr(dr, c[0]);
        //                //mCity = Sm.DrStr(dr, c[1]);
        //                //TxtCity.EditValue = Sm.DrStr(dr, c[2]);
        //                //mCnt = Sm.DrStr(dr, c[3]);
        //                TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
        //                TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
        //                TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
        //                TxtFax.EditValue = Sm.DrStr(dr, c[7]);
        //                TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
        //                TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
        //            }, false
        //        );
        //}


        private void ShowARDownPayment(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ReloadCustomer()
        {
            TxtCtQuot.EditValue = null;
            LueContactCode.EditValue = null;
            TxtSOQuotPromo.EditValue = null;
            ClearData2();
            Sm.ClearGrd(Grd1, true);
            var CtCode = Sm.GetLue(LueCtCode);
            if (CtCode.Length != 0 && BtnSave.Enabled)
            {
                SetLueCtPersonCode(ref LueContactCode, Sm.GetLue(LueCtCode));
                TxtSAName.EditValue = null;
                if (mRuleToDeactivateCtQt != "2")
                {
                    SetCtQtDocNo(CtCode);
                    SetLueCityPort(ref LueCity, TxtCtQuot.Text, "2");
                    SetLueCityPort(ref LuePort, TxtCtQuot.Text, "1");
                    //SetLueShippingAddress2(ref LueShipAdd, Sm.GetLue(LueCtCode));
                }
            }
        }

        internal void ShowCtQtInfo(string DocNo)
        {
            string mContactCode = string.Empty;
            mContactCode = Sm.GetLue(LueContactCode);
            TxtCtQuot.EditValue = null;
            LueContactCode.EditValue = null;
            TxtSOQuotPromo.EditValue = null;
            ClearData2();
            Sm.ClearGrd(Grd1, true);
            var CtCode = Sm.GetLue(LueCtCode);
            if (CtCode.Length != 0 && BtnSave.Enabled && mRuleToDeactivateCtQt == "2")
            {
                SetCtQtDocNo2(DocNo);
                SetLueCtPersonCode(ref LueContactCode, Sm.GetLue(LueCtCode));
                if (mContactCode.Length > 0) Sm.SetLue(LueContactCode, mContactCode);
                TxtSAName.EditValue = null;
                SetLueCityPort(ref LueCity, TxtCtQuot.Text, "2");
                SetLueCityPort(ref LuePort, TxtCtQuot.Text, "1");
                //SetLueShippingAddress2(ref LueShipAdd, Sm.GetLue(LueCtCode));
            }
        }

        private void SetLueBankAcCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.Col1, T.Col2 ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            if (Sm.GetParameter("DocTitle") == "IOK")
            {
                SQL.AppendLine("Select OptCode As Col1, REPLACE(REPLACE(OptDesc, '\r', ' '), '\n', ' ') As Col2, '999' As Sequence ");
                SQL.AppendLine("From TblOption ");
                SQL.AppendLine("Where OptCat = 'SOBankAccount' ");
            }

            else
            {
                if (Sm.GetParameter("BankAccountFormat") == "1")
                {
                    SQL.AppendLine("Select A.BankAcCode As Col1, ");
                    SQL.AppendLine("Trim(Concat( ");
                    SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                    SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                    SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                    SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                    SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                    SQL.AppendLine(")) As Col2, A.Sequence ");
                    SQL.AppendLine("From TblBankAccount A ");
                    SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
                }
                else
                {
                    SQL.AppendLine("Select A.BankAcCode As Col1, ");
                    SQL.AppendLine("Trim(Concat( ");
                    SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                    SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                    SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                    SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                    SQL.AppendLine(")) As Col2, A.Sequence ");
                    SQL.AppendLine("From TblBankAccount A ");
                    SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
                }
            }


            if (Sm.GetParameterBoo("IsVoucherBankAccountFilteredByGrp"))
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

           

            SQL.AppendLine(") T ");

            SQL.AppendLine("Order By T.Sequence; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsUserNotAbleToView()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblGroupMenu A, TblMenu B, TblUser C ");
            SQL.AppendLine("Where A.MenuCode=B.MenuCode ");
            SQL.AppendLine("And B.Param='FrmCtQt' ");
            SQL.AppendLine("And A.GrpCode=C.GrpCode ");
            SQL.AppendLine("And C.UserCode=@Param;");

            if (!Sm.IsDataExist(SQL.ToString(), Gv.CurrentUserCode))
            {
                Sm.StdMsg(mMsgType.Warning, "You don't have authority to view this quotation.");
                return true;
            }
            return false;
        }

        internal void ComputeTotalContainer()
        {
            decimal ContainerGrpTotal = 0m;
            string CntGrp = string.Empty;
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (CntGrp.Length <= 0)
                {
                    CntGrp = Sm.GetGrdStr(Grd1, Row, 30);
                    ContainerGrpTotal = 0;
                    ContainerGrpTotal += Sm.GetGrdDec(Grd1, Row, 31);
                }
                if (CntGrp != Sm.GetGrdStr(Grd1, Row, 30))
                {
                    CntGrp = Sm.GetGrdStr(Grd1, Row, 30);
                    ContainerGrpTotal += Sm.GetGrdDec(Grd1, Row, 31);
                }
            }

            LblJumlahContainer.Text = Sm.FormatNum(ContainerGrpTotal, 0);
        }

        internal void ComputeTotal()
        {
            decimal Amt = 0m;
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                Amt += Sm.GetGrdDec(Grd1, Row, 20);

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void SetNumberOfSalesUomCode()
        {
            string NumberOfSalesUomCode = Sm.GetParameter("NumberOfSalesUomCode");
            if (NumberOfSalesUomCode.Length == 0)
                mNumberOfSalesUomCode = 1;
            else
                mNumberOfSalesUomCode = int.Parse(NumberOfSalesUomCode);
        }

        public void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsSODisplayExpiredCtQt', 'IsContainerSize', 'IsFilterCity', 'IsSOUseARDPValidated', 'IsCustomerItemNameMandatory', ");
            SQL.AppendLine("'IsShowCustomerCategory', 'IsFilterByCtCt', 'IsSOAttachmentEditable', 'IsFilterByItCt', 'IsSO2UseActualItem', ");
            SQL.AppendLine("'IsSO2NotUsePackagingLabel', 'IsSO2AllowToUploadFile', 'IsCustomerComboBasedOnCategory', 'IsSO2DeliveryDateNotMandatory', 'IsAutoJournalActived', ");
            SQL.AppendLine("'IsSO2JournalEnabled', 'MainCurCode', 'RuleToDeactivateCtQt', 'SO2BankAcCode', 'IsSoUseDefaultPrintout', ");
            SQL.AppendLine("'LocalDocument', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'PortForFTPClient', 'FileSizeMaxUploadFTPClient' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSO2DeliveryDateNotMandatory": mIsSO2DeliveryDateNotMandatory = ParValue == "Y"; break;
                            case "IsSO2JournalEnabled": mIsSO2JournalEnabled = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsCustomerComboBasedOnCategory": mIsCustomerComboBasedOnCategory = ParValue == "Y"; break;
                            case "IsSO2AllowToUploadFile": mIsSO2AllowToUploadFile = ParValue == "Y"; break;
                            case "IsSO2NotUsePackagingLabel": mIsSO2NotUsePackagingLabel = ParValue == "Y"; break;
                            case "IsSO2UseActualItem": mIsSO2UseActualItem = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsSOAttachmentEditable": mIsSOAttachmentEditable = ParValue == "Y"; break;
                            case "IsShowCustomerCategory": mIsShowCustomerCategory = ParValue == "Y"; break;
                            case "IsCustomerItemNameMandatory": mIsCustomerItemNameMandatory = ParValue == "Y"; break;
                            case "IsSOUseARDPValidated": mIsSOUseARDPValidated = ParValue == "Y"; break;
                            case "IsFilterCity": mIsFilterCity = ParValue == "Y"; break;
                            case "IsContainerSize": mIsContainerSize = ParValue == "Y"; break;
                            case "IsSODisplayExpiredCtQt": mIsSODisplayExpiredCtQt = ParValue == "Y"; break;

                            //string
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "LocalDocument": mLocalDocument = ParValue; break;
                            case "IsSoUseDefaultPrintout": mIsSoUseDefaultPrintout = ParValue; break;
                            case "SO2BankAcCode": mSO2BankAcCode = ParValue; break;
                            case "RuleToDeactivateCtQt": mRuleToDeactivateCtQt = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        public void ComputeTotalQuantity()
        {
            decimal QtyPack = 0m; decimal Qty = 0m; decimal QtyVol = 0m;

            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                QtyPack += Sm.GetGrdDec(Grd1, Row, 8);
                Qty += Sm.GetGrdDec(Grd1, Row, 9);
                QtyVol += Sm.GetGrdDec(Grd1, Row, 28);
            }

            LblPack.Text = Sm.FormatNum(QtyPack, 0);
            LblQty.Text = Sm.FormatNum(Qty, 0);
            LblVol.Text = Sm.FormatNum(QtyVol, 0);
        }


        private void DownloadFileKu(string TxtFile)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                //this.Text = "Connecting...";
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
            }
            catch (Exception exc)
            {
                Sm.StdMsg(mMsgType.Warning, exc.ToString());
            }
        }


        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<SO2ExpHdr>();
            var ldtl = new List<SO2ExpDtl>();
            var ldtl2 = new List<SO2ExpDtl2>();
            string bankname = string.Empty;

            string[] TableName = { "SO2ExpHdr", "SO2ExpDtl", "SO2ExpDtl2" };
            List<IList> myLists = new List<IList>();

            if (Sm.GetParameter("DocTitle") == "IOK")
            {
                bankname = Sm.GetValue("Select Optdesc from tbloption Where optcat = 'SOBankAccount' And OptCode = '" + Sm.GetLue(LueBankAccount) + "' ");
            }
            else
            {
                bankname = Sm.GetValue("Select bankAcnm from tblbankaccount where bankacCode = '"+Sm.GetLue(LueBankAccount)+"' ");
            }

            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
            SQL.AppendLine("(SELECT SpName FROM TblSalesPerson ORDER BY CreateDt ASC LIMIT 1) AS CEO, ");
            SQL.AppendLine("(SELECT EmpName FROM TblEmployee WHERE PosCode = '00145' ORDER BY CreateDt DESC LIMIT 1) AS MarketingManager, ");
            SQL.AppendLine("A.DocNo, A.LocalDocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDtSpace, ");
            SQL.AppendLine("Upper(B.CtName) As CtName, Upper(A.CtContactPersonName) As Contact, J.Position As ContactPosition, B.Phone As CtPhone, ");
            SQL.AppendLine("A.Amt, Upper(A.SAName) As SAName, A.SAAddress, A.Amt, A.SAPostalCd, IfNull(A.SAPhone, C.Phone) As Phone, A.CtPONo, A.OverseaInd, ");
            SQL.AppendLine("IfNull(A.SAFax, C.Fax) As Fax, A.Remark, L.DTName AS ShipmentMethod, D.PtCode, F.Remark As PtRemark, F.PtName, ");
            SQL.AppendLine("Upper(E.Username) AS username,  B.Address As AddresCtName, G.SpName, H.CityName, A.Remark, ifnull(I.SpName, G.Spname) As Spname2,  ");
            SQL.AppendLine("K.CityName As CtCityName, M.CntName As CtCntName, B.Fax As CtFax, B.PostalCd  As CtPos, N.CntName As SACntName, A.Comodity, D.QtMth, ");
            SQL.AppendLine("If(length(O.CityName)>0 && length(P.PortName)>0, concat(O.CityName,'/',P.PortName ), (ifnull(O.CityName,P.PortName)))As SD, ");
            SQL.AppendLine("A.Tolerance, A.BankAcNo, Date_Format(ADDDate(A.DocDt, 10 ), '%d %M %Y')As ValidateDt, A.SignName, ");
            
            SQL.AppendLine("Trim(Concat( ");
            SQL.AppendLine("Case When Q.BankAcNo Is Not Null "); 
            SQL.AppendLine("    Then Concat(Q.BankAcNo, ' [', IfNull(Q.BankAcNm, ''), ']') ");
            SQL.AppendLine("    Else IfNull(Q.BankAcNm, '') End, "); 
            SQL.AppendLine("Case When R.BankName Is Not Null Then Concat(' ', R.BankName) Else '' End ");
            SQL.AppendLine("))As BankAc, Round(S.QtyPackagingUnit,0)As QtyPackagingUnit, ");
            SQL.AppendLine("T.DeliveryDt, A.shipment, A.Validity, A.ContainerSize, P.PortName ");

            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode");
            SQL.AppendLine("Left Join TblCustomerShipAddress C ");
            SQL.AppendLine("    On A.CtCode=C.CtCode ");
            SQL.AppendLine("    And C.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCtQtHdr D On A.CtQtDocNo = D.DocNo");
            SQL.AppendLine("Left Join TblUser E On D.SpCode = E.UserCode");
            SQL.AppendLine("Inner Join TblPaymentterm F On D.PtCode =F.PtCode ");
            SQL.AppendLine("Left Join TblSalesPerson G On D.SpCode=G.SpCode ");
            SQL.AppendLine("Left Join TblCity H On A.SACityCode=H.CityCode ");
            SQL.AppendLine("Left Join TblSalesPerson I On A.SpCode=I.SpCode ");
            SQL.AppendLine("Left Join TblCustomerContactPerson J On A.CtContactPersonName = J.ContactPersonName And J.CtCode=A.CtCode ");
            SQL.AppendLine("Left Join TblCity K On B.CityCode = K.CityCode ");
            SQL.AppendLine("LEFT JOIN TblDeliveryType L ON D.ShpMCode = L.DTCode ");
            SQL.AppendLine("Left Join TblCountry M On B.CntCode = M.CntCode ");
            SQL.AppendLine("Left Join TblCountry N On A.SACntCode = N.CntCode ");
            SQL.AppendLine("Left Join TblCity O On A.CityCode=O.CityCode ");
            SQL.AppendLine("Left Join TblPort P On A.PortCode = P.PortCode ");
            SQL.AppendLine("Left Join TblBankAccount Q On A.BankAcNo = Q.BankAcCode ");
            SQL.AppendLine("Left Join TblBank R On Q.BankCode=R.BankCode ");
  


            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Z.DocnO, SUM(ContainerGroupQty) QtyPackagingUnit ");
            SQL.AppendLine("    from ( ");
            SQL.AppendLine("        Select distinct Docno, Containergroup, ContainerGroupQty From TblSODtl ");
            SQL.AppendLine("        Where DocNo = @DocNo ");
            SQL.AppendLine("    )Z ");
            SQL.AppendLine("    Group by Z.DocnO ");
            SQL.AppendLine(")S On A.DocNo=S.DocNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo, DATE_FORMAT(DeliveryDt,'%M %Y') As DeliveryDt, DNo From TblSODtl Where DNo='001' ");
            SQL.AppendLine(")T On A.DocNo=T.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd = 'N' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyEmail",

                         //6-10
                         "Shipper1",
                         "Shipper2",
                         "Shipper3",
                         "Shipper4",
                         "DocNo",

                         //11-15
                         "DocDt",
                         "CtName",
                         "Contact",
                         "SAName",
                         "SAAddress",

                         //16-20
                         "SAPostalCd",
                         "Phone",
                         "Fax",
                         "Remark",
                         "PtCode",

                         //21-25
                         "PtRemark",
                         "Username",
                         "Amt",
                         "CompLocation2",
                         "AddresCtName",

                         //26-30
                         "SpName",
                         "CompanyFax",
                         "LocalDocNo",
                         "CityName",
                         "Remark",

                         //31-35 MSI 
                         "SPName2",
                         "ContactPosition",
                         "CtPhone",
                         "DocDtSpace",
                         "CEO",

                         //36-40
                         "CtCityName",
                         "ShipmentMethod",
                         "MarketingManager",
                         "CtCntName",
                         "CtFax",

                         //41-42
                         "CtPos",
                         "SACntName",
                         "Comodity",
                         "QtMth",
                         "SD",

                         //46-50
                         "Tolerance",
                         "BankAc",
                         "QtyPackagingUnit",
                         "DeliveryDt",
                         "Validity", 
                         //51-55
                         "Shipment",
                         "SignName",
                         "ContainerSize",
                         "PortName",
                         "CtPONo",
                         //56-57
                         "OverseaInd",
                         "PtName"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SO2ExpHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),

                            Shipper1 = Sm.DrStr(dr, c[6]),
                            Shipper2 = Sm.DrStr(dr, c[7]),
                            Shipper3 = Sm.DrStr(dr, c[8]),
                            Shipper4 = Sm.DrStr(dr, c[9]),
                            DocNo = Sm.DrStr(dr, c[10]),

                            DocDt = Sm.DrStr(dr, c[11]),
                            CtName = Sm.DrStr(dr, c[12]),
                            Contact = Sm.DrStr(dr, c[13]),
                            SAName = Sm.DrStr(dr, c[14]),
                            SAAddress = Sm.DrStr(dr, c[15]),

                            SAPostalCd = Sm.DrStr(dr, c[16]),
                            Phone = Sm.DrStr(dr, c[17]),
                            Fax = Sm.DrStr(dr, c[18]),
                            Remark = Sm.DrStr(dr, c[19]),
                            PtCode = Sm.DrStr(dr, c[20]),

                            PtRemark = Sm.DrStr(dr, c[21]),
                            Username = Sm.DrStr(dr, c[22]),
                            Amt = Sm.DrDec(dr, c[23]),
                            CompLocation2 = Sm.DrStr(dr, c[24]),
                            AddresCtName = Sm.DrStr(dr, c[25]),

                            SpName = Sm.DrStr(dr, c[26]),
                            CompanyFax = Sm.DrStr(dr, c[27]),
                            LocalDocNo = Sm.DrStr(dr, c[28]),
                            CityName = Sm.DrStr(dr, c[29]),
                            RemarkHdr = Sm.DrStr(dr, c[30]),

                            SPName2 = Sm.DrStr(dr, c[31]),
                            ContactPosition = Sm.DrStr(dr, c[32]),
                            CtPhone = Sm.DrStr(dr, c[33]),
                            DocDtSpace = Sm.DrStr(dr, c[34]),
                            CEO = Sm.DrStr(dr, c[35]),

                            CtCityName = Sm.DrStr(dr, c[36]),
                            ShipmentMethod = Sm.DrStr(dr, c[37]),
                            MarketingManager = Sm.DrStr(dr, c[38]),
                            CtCntName = Sm.DrStr(dr, c[39]),
                            CtFax = Sm.DrStr(dr, c[40]),

                            CtPos = Sm.DrStr(dr, c[41]),
                            SACntName = Sm.DrStr(dr, c[42]),
                            Comodity = Sm.DrStr(dr, c[43]),
                            QtMth = Sm.DrStr(dr, c[44]),
                            SD = Sm.DrStr(dr, c[45]),

                            Tolerance = Sm.DrStr(dr, c[46]),
                            BankAc = bankname, //Sm.DrStr(dr, c[47]),
                            QtyPackagingUnit = Sm.DrStr(dr, c[48]),
                            DeliveryDt = Sm.DrStr(dr, c[49]),
                            Validity = Sm.DrStr(dr, c[50]),

                            Shipment = Sm.DrStr(dr, c[51]),
                           SignName= Sm.DrStr(dr, c[52]),
                            ContainerSize = Sm.DrStr(dr, c[53]),
                            Port = Sm.DrStr(dr, c[54]),
                            CtPONo = Sm.DrStr(dr, c[55]),
                            OverseaInd = Sm.DrStr(dr, c[56]),
                            PtName= Sm.DrStr(dr, c[57]),
                            Printby = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select E.ItCode, G.ItName, IFNULL(G.ForeignName,G.ItName) As ForeignName, G.Height As T, G.Width As W, G.Length As L, G.Specification AS Qualification, ");  
                SQLDtl.AppendLine("Round(B.Qty, 4) As Qty, D.PriceUomCode, C.Uprice, I.CtItName, I.CtItCode, ");
                SQLDtl.AppendLine("DATE_FORMAT(B.DeliveryDt,'%M %d, %Y') As DeliveryDt, B.Remark, ");
                SQLDtl.AppendLine("(B.Qty * C.Uprice) AS Amt, B.QtyPackagingUnit,  ");
                SQLDtl.AppendLine("if(H.Qty =0, 0, if(D.PriceUomCode = G.SalesUomCode2, B.Qty, (H.Qty2*B.Qty)/H.Qty)) As Qty2, ");
                SQLDtl.AppendLine("A.CurCode, G.HeightUomCode,DATE_FORMAT(B.DeliveryDt,'%d %M %Y') As DeliveryDt2, ");
                SQLDtl.AppendLine("(Round(B.Qty, 4)*((C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0)))*0.01*B.TaxRate)) As TaxAmt, ");
                SQLDtl.AppendLine("(Select ParValue From TblParameter Where ParCode = 'IsCustomerItemNameMandatory') As IsCustomerItemNameMandatory ");

                SQLDtl.AppendLine(", E.UPrice As UPrice2, C.Discount As Discount2, ifnull((E.UPrice*C.Discount *0.01), 0) As DiscountAmt2, ");
                SQLDtl.AppendLine("C.UPrice As UPriceAfterDiscount2, ");
                SQLDtl.AppendLine("IfNull(F.DiscRate, 0) As PromoRate2, "); 
                SQLDtl.AppendLine("(C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0))) As UPriceBefore2, "); 
                SQLDtl.AppendLine("B.TaxRate As TaxRate2,  ");
                SQLDtl.AppendLine("((C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0)))*0.01*B.TaxRate) As TaxAmt2, H.Volume, B.ContainerGroup, B.ContainerGroupQty ");

                SQLDtl.AppendLine("From TblSOHdr A  ");
                SQLDtl.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo   ");
                SQLDtl.AppendLine("Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo  ");
                SQLDtl.AppendLine("Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo  ");
                SQLDtl.AppendLine("Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DocNo And C.ItemPriceDNo=E.DNo  ");
                SQLDtl.AppendLine("Left Join TblSOQuotPromoItem F On A.SOQuotPromoDocNo=F.DocNo And E.ItCode=F.ItCode  ");
                SQLDtl.AppendLine("Inner Join TblItem G On E.ItCode=G.ItCode  ");
                SQLDtl.AppendLine("Left Join TblItemPackagingUnit H On B.PackagingUnitUomCode = H.UomCOde And E.ItCode = H.ItCode ");
                //SQLDtl.AppendLine("Inner Join TblCtQthdr I On A.CtQtDocNo = I.DocNo ");
                SQLDtl.AppendLine("Left Join TblCustomerItem I On A.CtCode = I.CtCode And G.ItCode = I.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And A.cancelInd = 'N' ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ForeignName" ,

                     //1-5
                     "T" ,
                     "W",
                     "L",
                     "Qty",
                     "PriceUomCode",

                     //6-10
                     "Uprice",
                     "DeliveryDt",
                     "Remark",
                     "Amt",
                     "QtypackagingUnit",
                     //11-15
                     "Qty2",
                     "CurCode", 
                     "HeightUomCode", 
                     "DeliveryDt2",
                     "TaxAmt",

                     //16-20 untuk MSI
                     "UPrice2",
                     "Discount2",
                     "DiscountAmt2",
                     "UPriceAfterDiscount2",
                     "PromoRate2",
                     //21-23 untuk MSI
                     "UPriceBefore2",
                     "TaxRate2",
                     "TaxAmt2",

                     //24-25
                     "ItName",
                     "Qualification",

                     //26-30
                     "CtItName",
                     "IsCustomerItemNameMandatory",
                     "Volume",
                     "CtItCode",
                     "ContainerGroup",
                     //31
                     "ContainerGroupQty"
                    });
                if (drDtl.HasRows)
                {
                    int NomorBaris = 0;
                    while (drDtl.Read())
                    {
                        NomorBaris = NomorBaris + 1;
                        ldtl.Add(new SO2ExpDtl()
                        {
                            Nomor = NomorBaris,
                            ForeignName = Sm.DrStr(drDtl, cDtl[0]),
                            T = Sm.DrDec(drDtl, cDtl[1]),
                            W = Sm.DrDec(drDtl, cDtl[2]),
                            L = Sm.DrDec(drDtl, cDtl[3]),
                            Qty = Sm.DrDec(drDtl, cDtl[4]),
                            PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),
                            UPrice = Sm.DrDec(drDtl, cDtl[6]),
                            DeliveryDt = Sm.DrStr(drDtl, cDtl[7]),
                            Remark = Sm.DrStr(drDtl, cDtl[8]),
                            Amt = Sm.DrDec(drDtl, cDtl[9]),
                            QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[10]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[11]),
                            CurCode = Sm.DrStr(drDtl, cDtl[12]),
                            HeightUomCode = Sm.DrStr(drDtl, cDtl[13]),
                            DeliveryDt2 = Sm.DrStr(drDtl, cDtl[14]),
                            TaxAmt = Sm.DrDec(drDtl, cDtl[15]),

                            //msi
                            UPrice2 = Sm.DrDec(drDtl, cDtl[16]),
                            Discount2 = Sm.DrDec(drDtl, cDtl[17]),
                            DiscountAmt2 = Sm.DrDec(drDtl, cDtl[18]),
                            UPriceAfterDiscount2 = Sm.DrDec(drDtl, cDtl[19]),
                            PromoRate2 = Sm.DrDec(drDtl, cDtl[20]),
                            UPriceBefore2 = Sm.DrDec(drDtl, cDtl[21]),
                            TaxRate2 =  Sm.DrDec(drDtl, cDtl[22]),
                            TaxAmt2 = Sm.DrDec(drDtl, cDtl[23]),

                            ItName = Sm.DrStr(drDtl, cDtl[24]),
                            Qualification = Sm.DrStr(drDtl, cDtl[25]),
                            
                            CtItName = Sm.DrStr(drDtl, cDtl[26]),
                            IsCustomerItemNameMandatory = Sm.DrStr(drDtl, cDtl[27]),
                            Volume = Sm.DrDec(drDtl, cDtl[28]),
                            CtItCode = Sm.DrStr(drDtl, cDtl[29]),
                            ContainerGroup = Sm.DrStr(drDtl, cDtl[30]),
                            ContainerGroupQty = Sm.DrDec(drDtl, cDtl[31]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail2

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select SUM(X.Amt) As SumSubTotal, SUM(X.TaxAmt) As PPN, Round((SUM(X.Amt)+SUM(X.TaxAmt)),2) As TotalAmt ");
                SQLDtl2.AppendLine("From ( ");
                SQLDtl2.AppendLine("   Select E.ItCode, G.ItName, G.ForeignName, G.Height As T, G.Width As W, G.Length As L, ");
                SQLDtl2.AppendLine("   Round(B.Qty, 4) As Qty, D.PriceUomCode, C.Uprice,  ");
                SQLDtl2.AppendLine("   DATE_FORMAT(B.DeliveryDt,'%M %d, %Y') As DeliveryDt, B.Remark,  ");
                SQLDtl2.AppendLine("   Round((Round(B.Qty, 4) * C.Uprice), 4) AS Amt, B.QtyPackagingUnit, ");
                SQLDtl2.AppendLine("   if(H.Qty =0, 0, if(D.PriceUomCode = G.SalesUomCode2, B.Qty, (H.Qty2*B.Qty)/H.Qty)) As Qty2, ");
                SQLDtl2.AppendLine("   A.CurCode, G.HeightUomCode,DATE_FORMAT(B.DeliveryDt,'%d %M %Y') As DeliveryDt2, ");
                SQLDtl2.AppendLine("   Round((Round(B.Qty, 4)*((C.UPrice-(C.UPrice*0.01*IfNull(F.DiscRate, 0)))*0.01*B.TaxRate)), 4) As TaxAmt ");
                SQLDtl2.AppendLine("   From TblSOHdr A ");
                SQLDtl2.AppendLine("   Inner Join TblSODtl B On A.DocNo=B.DocNo ");
                SQLDtl2.AppendLine("   Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
                SQLDtl2.AppendLine("   Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DocNo ");
                SQLDtl2.AppendLine("   Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DocNo And C.ItemPriceDNo=E.DNo ");
                SQLDtl2.AppendLine("   Left Join TblSOQuotPromoItem F On A.SOQuotPromoDocNo=F.DocNo And E.ItCode=F.ItCode ");
                SQLDtl2.AppendLine("   Inner Join TblItem G On E.ItCode=G.ItCode ");
                SQLDtl2.AppendLine("   Left Join TblItemPackagingUnit H On B.PackagingUnitUomCode = H.UomCOde And E.ItCode = H.ItCode ");
                SQLDtl2.AppendLine("   Where A.DocNo=@DocNo And A.cancelInd = 'N' ");
                SQLDtl2.AppendLine(")X ");

                cmDtl2.CommandText = SQLDtl2.ToString();

                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "SumSubTotal",

                     //1-2
                     "PPN", "TotalAmt"
                    });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new SO2ExpDtl2()
                        {
                            SumSubTotal = Sm.DrDec(drDtl2, cDtl2[0]),
                            PPN = Sm.DrDec(drDtl2, cDtl2[1]),
                            TotalAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                            
                            Terbilang = Sm.Terbilang(Sm.DrDec(drDtl2, cDtl2[2])),
                            Terbilang2 = Sm.Terbilang2(Sm.DrDec(drDtl2, cDtl2[2])),
                            
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);

            #endregion

            string RptName = Sm.GetParameter("FormPrintOutSO2");

            if (mIsSoUseDefaultPrintout == "Y")
            {
                if (Sm.GetParameter("DocTitle") == "SRN")
                {
                    Sm.PrintReport("SO2DefaultSRN", myLists, TableName, false);
                }
                else
                    Sm.PrintReport("SO2Default", myLists, TableName, false);
            }
            
            else
            {
                if (RptName.Length > 0)
                {
                    if (ChkOverSea.Checked == true)
                    {
                        if (RptName == "MAI")
                            Sm.PrintReport("SO2Exp" + RptName, myLists, TableName, false);
                        else
                            Sm.PrintReport("SO2Exp", myLists, TableName, false); 
                    }
                    else
                        Sm.PrintReport("SO2Loc" + RptName, myLists, TableName, false); //mai
                }
                else
                {
                    //if (ChkOverSea.Checked == true)
                    //{
                        Sm.PrintReport("SO2Exp", myLists, TableName, false);
                    //}
                    //else
                    //{
                    //    Sm.PrintReport("SO2Loc", myLists, TableName, false);
                    //}
                }
            }            

            if (ChkProforma.Checked == true && ChkOverSea.Checked == false)
            {
                if (RptName.Length > 0)
                    Sm.PrintReport("SO2Pro" + RptName, myLists, TableName, false);
                else
                    Sm.PrintReport("SO2Pro", myLists, TableName, false);
            }
        }

        private void SetCtQtDocNo(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblCtQtHdr ");
            SQL.AppendLine("Where ActInd='Y' And Status='A' And CtCode=@CtCode ");
            SQL.AppendLine("Order By DocDt Desc, DocNo Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            TxtCtQuot.EditValue = Sm.GetValue(cm);

            string AllowEditSalesPerson = Sm.GetValue("Select IF( EXISTS(Select B.Spname from TblCtQthdr A "+
            "Inner Join TblSalesPerson B On A.SpCode=B.SpCode "+
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");

            string AllowEditShipmentMethod = Sm.GetValue("Select IF( EXISTS(Select B.Dtname from TblCtQthdr A  " +
            "Inner Join TblDeliveryType B On A.ShpmCode=B.DtCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");


            if (AllowEditSalesPerson == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, false);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblCtQtHdr Where DocNo = '"+Sm.GetValue(cm)+"'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, true);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblCtQtHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }

           
            if (AllowEditShipmentMethod == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode   
                }, false);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblCtQtHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode    
                }, true);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblCtQtHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
        }

        private void SetCtQtDocNo2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblCtQtHdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            TxtCtQuot.EditValue = Sm.GetValue(cm);

            string AllowEditSalesPerson = Sm.GetValue("Select IF( EXISTS(Select B.Spname from TblCtQthdr A " +
            "Inner Join TblSalesPerson B On A.SpCode=B.SpCode " +
            "Where A.DocNo = '" + DocNo + "'), 1, 0)");

            string AllowEditShipmentMethod = Sm.GetValue("Select IF( EXISTS(Select B.Dtname from TblCtQthdr A  " +
            "Inner Join TblDeliveryType B On A.ShpmCode=B.DtCode " +
            "Where A.DocNo = '" + DocNo + "'), 1, 0)");


            if (AllowEditSalesPerson == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, false);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblCtQtHdr Where DocNo = '" + DocNo + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, true);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblCtQtHdr Where DocNo = '" + DocNo + "'"));
            }


            if (AllowEditShipmentMethod == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode   
                }, false);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblCtQtHdr Where DocNo = '" + DocNo + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode    
                }, true);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblCtQtHdr Where DocNo = '" + DocNo + "'"));
            }
        }

        #endregion

        #region SetLue

        public static void SetLueDTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DTCode As Col1, DTName As Col2 From TblDeliveryType " +
                 "Union ALL Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union All Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        //private void SetLueShippingAddress(ref DXE.LookUpEdit Lue, string CtCode)
        //{
        //    try
        //    {
        //        var SQL = new StringBuilder();

        //        SQL.AppendLine("Select Col1, Col2, Col3, Col4 From ( ");
        //        SQL.AppendLine("Select Dno As Col1, Name As Col2, Address As Col3, Phone As Col4 From TblCustomershipaddress ");
        //        SQL.AppendLine("Where CtCode='" + CtCode + "' ");
        //        if (TxtDocNo.Text.Length != 0)
        //        {
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select Distinct '000' As Col1, SAName As Col2, SAAddress As Col3, SAPhone As Col4 ");
        //            SQL.AppendLine("From TblDRHdr   ");
        //            SQL.AppendLine("Where CtCode='" + CtCode + "' ");
        //        }
        //        SQL.AppendLine(") Tbl Order By Col2");

        //        Sm.SetLue4(
        //            ref Lue,
        //            SQL.ToString(),
        //            0, 35, 100, 30, false, true, true, true, "Code", "Name", "Address", "Phone", "Col2", "Col2");
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        //private void SetLueShippingAddress2(ref DXE.LookUpEdit Lue, string CtCode)
        //{
        //    try
        //    {
        //        var SQL = new StringBuilder();

        //        SQL.AppendLine("Select Col1, Col2, Col3, Col4 From ( ");
        //        SQL.AppendLine("Select Dno As Col1, Name As Col2, Address As Col3, Phone As Col4 From TblCustomershipaddress ");
        //        SQL.AppendLine("Where CtCode='" + CtCode + "' ");
        //        if (TxtDocNo.Text.Length != 0)
        //        {
        //            SQL.AppendLine("Union All ");
        //            SQL.AppendLine("Select Distinct '000' As Col1, SAName As Col2, SAAddress As Col3, SAPhone As Col4 ");
        //            SQL.AppendLine("From TblDRHdr   ");
        //            SQL.AppendLine("Where CtCode='" + CtCode + "' ");
        //        }
        //        SQL.AppendLine(") Tbl Order By Col2");

        //        Sm.SetLue4(
        //            ref Lue,
        //            SQL.ToString(),
        //            0, 35, 100, 30, false, true, true, true, "Code", "Name", "Address", "Phone", "Col2", "Col1");
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        public static void SetLueCityPort(ref LookUpEdit Lue, string CtQtDocNo, string type)
        {
            var SQL = new StringBuilder();

            if (type == "1")//port
            {
                SQL.AppendLine("Select A.PortCode As Col1, B.PortName As Col2 From TblCtQTDtl4 A ");
                SQL.AppendLine("Inner Join tblPort B On A.PortCode = B.PortCode ");
                SQL.AppendLine("Where  A.DocNo = '"+CtQtDocNo+"' ");
            }
            if (type == "2")//city
            {
                SQL.AppendLine("Select A.CityCode As Col1, B.Cityname  As Col2 From TblCtQTDtl2 A  ");
                SQL.AppendLine("Inner Join tblCity B On A.CityCode= B.CityCode ");
                SQL.AppendLine("Where  A.DocNo = '" + CtQtDocNo + "' ");
            }

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "City / Port", "Col2", "Col1");
        }

        public static void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson " +
                "Where CtCode= '" + CtCode + "' Order By ContactPersonName;",
                "Contact Person");
        }

        public static void SetLueCtPersonCodeShow(ref LookUpEdit Lue, string DocNo)
        {
            Sm.SetLue1(
                ref Lue,
                "Select CtContactPersonName As Col1 From TblSOHdr " +
                "Where DocNo= '" + DocNo + "' Order By CtContactPersonName;",
                "Contact Person");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType As Col1, T.Status As Col2 From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'F' As Doctype, 'Fullfiled' As Status ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select 'O' As Doctype, 'Outstanding' As Status ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select 'P' As Doctype, 'Partial Fullfiled' As Status ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select 'M' As Doctype, 'Manual Fullfiled' As Status ");
            SQL.AppendLine(")T ");


            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (mIsCustomerComboBasedOnCategory)
                    Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue4(Sl.SetLueCtCodeBasedOnCategory), string.Empty, mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                else
                    Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                ReloadCustomer();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueContactCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueContactCode, new Sm.RefreshLue2(SetLueCtPersonCode), Sm.GetLue(LueCtCode));
        }

        //private void LueShipAdd_EditValueChanged(object sender, EventArgs e)
        //{
        //    string ShpDno = string.Empty;
        //    Sm.RefreshLookUpEdit(LueShipAdd, new Sm.RefreshLue2(SetLueShippingAddress2), Sm.GetLue(LueCtCode));
        //    try
        //    {
        //        Cursor.Current = Cursors.WaitCursor;
        //        ClearData2();
        //        if (TxtDocNo.Text.Length > 0 && Sm.GetLue(LueShipAdd).Length > 0)
        //        {
        //            ShpDno = Sm.GetValue("Select MAX(Dno) From TblCustomerShipAddress Where Name = '"+Sm.GetLue(LueShipAdd)+"' limit 1 ");
        //            ShowShippingAddressData(Sm.GetLue(LueCtCode), ShpDno);
        //        }
        //        else
        //        {
        //            ShowShippingAddressData(Sm.GetLue(LueCtCode), Sm.GetLue(LueShipAdd));
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}


        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueCity_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCity, new Sm.RefreshLue3(SetLueCityPort), TxtCtQuot.Text, "2");
        }

        private void LuePort_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePort, new Sm.RefreshLue3(SetLueCityPort), TxtCtQuot.Text, "1");
        }

        private void LueBankAccount_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueBankAccount, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
            Sm.RefreshLookUpEdit(LueBankAccount, new Sm.RefreshLue1(SetLueBankAcCode));
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsCustomerComboBasedOnCategory && IsInsert)
            {
                Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
                Sl.SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                ReloadCustomer();
            }
        }

        #endregion

        #region Button Event
        private void BtnCtCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSO2Dlg7(this));
        }

        private void BtnDoReqDeptCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSO2Dlg(this));
        }

        private void BtnContact_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnSoQuotPromoDlg_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSO2Dlg(this));
        }

        private void BtnSOQuotPromoMaster_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOQuotPromo, "Quotation's promo", false))
            {
                try
                {
                    var f = new FrmSOQuotPromo(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtSOQuotPromo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }
        
        private void BtnSOMaster_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsUserNotAbleToView()) return;
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    {
                        if (TxtCtQuot.EditValue != null && TxtCtQuot.Text.Length != 0)
                        {
                            var f = new FrmCtQt(mMenuCode);
                            f.Tag = "***";
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtCtQuot.Text;
                            f.ShowDialog();
                        }
                        else
                        {
                            TxtCtQuot.EditValue = Sm.GetValue("Select DocNo From TblCtQtHdr Where ActInd='Y' And Status='A' And CtCode=@Param Order By DocDt Desc, DocNo Desc Limit 1;", Sm.GetLue(LueCtCode));
                            var f = new FrmCtQt(mMenuCode);
                            f.Tag = "***";
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mCtCode = Sm.GetLue(LueCtCode);
                            f.ShowDialog();                            
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCtShippingAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void BtnCustomerShipAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSO2Dlg5(this, Sm.GetLue(LueCtCode), TxtCtQuot.Text));
        }       

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            List<string> FileName = new List<string>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, A.FileName1, A.FileName2, A.FileName3, A.FileName4, A.FileName5, ");
            SQL.AppendLine("A.FileName6, A.FileName7, A.FileName8, A.FileName9, A.FileName10, ");
            SQL.AppendLine("A.FileName11, A.FileName12, A.FileName13, A.FileName14, A.FileName15, ");
            SQL.AppendLine("A.FileName16, A.FileName17, A.FileName18, A.FileName19, A.FileName20 ");
            SQL.AppendLine("From TblAttachmentFile A ");
            SQL.AppendLine("Where A.LocalDocNo=@LocalDocNo;");

            Sm.CmParam<String>(ref cm, "@localDocNo", TxtLocalDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "FileName1", 
                        //1-5
                        "FileName2", "FileName3", "FileName4", "FileName5", "FileName6",  
                        //6-10
                        "FileName7", "FileName8", "FileName9", "FileName10", "FileName11", 
                        //11-15
                        "FileName12", "FileName13", "FileName14", "FileName15", "FileName16", 
                        //16-19
                        "FileName17", "FileName18", "FileName19", "FileName20"                         
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[0]));
                        if (Sm.DrStr(dr, c[1]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[1]));
                        if (Sm.DrStr(dr, c[2]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[2]));
                        if (Sm.DrStr(dr, c[3]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[3]));
                        if (Sm.DrStr(dr, c[4]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[4]));
                        if (Sm.DrStr(dr, c[5]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[5]));
                        if (Sm.DrStr(dr, c[6]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[6]));
                        if (Sm.DrStr(dr, c[7]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[7]));
                        if (Sm.DrStr(dr, c[8]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[8]));
                        if (Sm.DrStr(dr, c[9]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[9]));
                        if (Sm.DrStr(dr, c[10]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[10]));
                        if (Sm.DrStr(dr, c[11]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[11]));
                        if (Sm.DrStr(dr, c[12]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[12]));
                        if (Sm.DrStr(dr, c[13]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[13]));
                        if (Sm.DrStr(dr, c[14]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[14]));
                        if (Sm.DrStr(dr, c[15]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[15]));
                        if (Sm.DrStr(dr, c[16]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[16]));
                        if (Sm.DrStr(dr, c[17]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[17]));
                        if (Sm.DrStr(dr, c[18]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[18]));
                        if (Sm.DrStr(dr, c[19]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[19]));
                    }, false
                );
            if (FileName.Count > 0)
            {
                for (int i = 0; i < FileName.Count; i++)
                {
                    DownloadFileKu(FileName[i]);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No Attachment File");
            }
        }

        private void BtnCtQt_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mRuleToDeactivateCtQt == "2")
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer") && !Sm.IsDteEmpty(DteDocDt, "Date"))
                {
                    Sm.FormShowDialog(new FrmSO2Dlg6(this, Sm.GetLue(LueCtCode), Sm.Left(Sm.GetDte(DteDocDt), 8)));
                }
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD1.InitialDirectory = "c:";
                OD1.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD1.FilterIndex = 2;
                OD1.ShowDialog();

                TxtFile2.Text = OD1.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {

            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD1.FileName = TxtFile2.Text;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }
        private void DeleteFile(string fileName)
        {
            FtpWebResponse response;
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, fileName));
                request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = true;
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                response = (FtpWebResponse)request.GetResponse();
                response.Close();
            }
            catch (Exception exc)
            {
                 Sm.StdMsg(mMsgType.Warning, exc.Message);
            }
        } 

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;
            string oldFilename = string.Empty;
            if (mIsSOAttachmentEditable && BtnFile2.Enabled == true && TxtDocNo.Text.Length > 0)
            {
                oldFilename = Sm.GetValue("SELECT filename FROM tblsohdr WHERE docno = @Param;", TxtDocNo.Text);
                DeleteFile(oldFilename);
            }
            FileInfo toUpload = new FileInfo(string.Format(@"{0}",TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
          
            
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            
            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSOFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsSO2AllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsSO2AllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsSO2AllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsSO2AllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsSO2AllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (mIsSO2AllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblSOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private MySqlCommand UpdateSOFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }


        #endregion
      
        #endregion

    }

    #region Report Class

    class SO2ExpHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyFax { get; set; }
        public string Shipper1 { get; set; }
        public string Shipper2 { get; set; }
        public string Shipper3 { get; set; }
        public string Shipper4 { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string Remark { get; set; }
        public string CtName { get; set; }
        public string Contact { get; set; }
        public string SAName { get; set; }
        public string SAAddress{ get; set; }
        public string SAPostalCd{ get; set; }
        public string Phone{ get; set; }
        public string Fax{ get; set; }
        public string PtCode{ get; set; }
        public string PtRemark { get; set; }
        public string PtName { get; set; }
        public string Username { get; set; }
        public decimal Amt { get; set; }
        public string CompLocation2 { get; set; }
        public string AddresCtName { get; set; }
        public string SpName { get; set; }
        public string LocalDocNo { get; set; }
        public string CityName { get; set; }
        public string SPName2 { get; set; }
        public string RemarkHdr { get; set; }
        public string ContactPosition { get; set; }
        public string CtPhone { get; set; }
        public string DocDtSpace { get; set; }
        public string CEO { get; set; }
        public string CtCityName { get; set; }
        public string ShipmentMethod { get; set; }
        public string MarketingManager { get; set; }
        public string CtCntName { get; set; }
        public string CtFax { get; set; }
        public string CtPos { get; set; }
        public string CtPONo { get; set; }
        public string OverseaInd { get; set; }
        public string SACntName { get; set; }
        public string Comodity { get; set; }
        public string QtMth { get; set; }
        public string SD { get; set; }
        public string Tolerance { get; set; }
        public string BankAc { get; set; }
        public string QtyPackagingUnit { get; set; }
        public string DeliveryDt { get; set; }
        public string Validity { get; set; }
        public string Shipment { get; set; }
        public string SignName { get; set; }
        public string ContainerSize { get; set; }
        public string Port { get; set; }
        public string Printby { get; set; }
    }


    class SO2ExpDtl
    {
        public string ForeignName { get; set; }
        public decimal T { get; set; }
        public decimal W { get; set; }
        public decimal L { get; set; }
        public decimal Qty { get; set; }
        public string PriceUomCode { get; set; }
        public decimal UPrice { get; set; }
        public string DeliveryDt { get; set; }
        public string Remark { get; set; }
        public decimal Amt { get; set; }
        public decimal QtyPackagingUnit { get; set; }
        public decimal Qty2 { get; set; }
        public string CurCode { get; set; }
        public string HeightUomCode { get; set; }
        public string DeliveryDt2 { get; set; }
        public decimal TaxAmt { get; set; }
        public string ItName { get; set; }
        public string Qualification { get; set; }
        public int Nomor { get; set; }
        public string CtItName { get; set; }
        public string IsCustomerItemNameMandatory { get; set; }
        public string CtItCode { get; set; }
        public string ContainerGroup { get; set; }
        public decimal ContainerGroupQty { get; set; }

        //msi
        public decimal UPrice2 { get; set; }
        public decimal Discount2 { get; set; }
        public decimal DiscountAmt2 { get; set; }
        public decimal UPriceAfterDiscount2 { get; set; }
        public decimal PromoRate2 { get; set; }
        public decimal UPriceBefore2 { get; set; }
        public decimal TaxRate2 { get; set; }
        public decimal TaxAmt2 { get; set; }
        public decimal Volume { get; set; }
    }

    class SO2ExpDtl2
    {
        public decimal SumSubTotal { get; set; }
        public decimal PPN { get; set; }
        public decimal TotalAmt { get; set; }
        public string Terbilang { get; set; }
        public string Terbilang2 { get; set; }
    }

   
    #endregion
}
