﻿#region Update
/*
    16/02/2022 [IBL/PHT] New Apps
    18/03/2022 [IBL/PHT] saat stock summary tidak tercentang, maka item yg muncul dari master item yg tdk ada di stock summary yg whs nya = whs profit center terpilih)
    06/04/2022 [IBL/PHT] tambah checkbox Show Zero Stock. Jika dicentang, maka akan memunculkan stock yg 0. Hanya berfungsi saat stock summary items tercentang
    25/04/2022 [RDA/PHT] autocheck + read only ketika stock > 0 dan Stock Summary Item di check
                         validasi muncul warning ketika item stock > 0 ada yg belum dipilih di dialog 
    17/05/2022 [IBL/PHT] Bug: Tidak bisa save krna warning dari List yg bernilai NULL. ItSrcDlg dicomment.
    31/03/2023 [WED/PHT] GetSQL1() ditambahin Qty = 0.00
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmStockOpname3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmStockOpname3 mFrmParent;
        private string mSQL = string.Empty, mWhsCode = string.Empty;
        private bool mIsInventoryShowTotalQty = false, mIsReCompute = false;

        #endregion

        #region Constructor

        public FrmStockOpname3Dlg(FrmStockOpname3 FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode);
                ChkStockSummaryItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Type, A.ItCode, A.ItCodeInternal, A.ItName, Null As BatchNo, Null As Source, B.ItCtCode, B.ItCtName, ");
            SQL.AppendLine("Null As Lot, Null As Bin, 0.00 As Qty, A.InventoryUomCode, 0.00 As Qty2, A.InventoryUomCode2,  ");
            SQL.AppendLine("0.00 As Qty3, A.InventoryUomCode3, 'IDR' As CurCode, 0.00 As UPrice,  ");
            SQL.AppendLine("A.ForeignName, A.Specification, C.AcNo, C.AcDesc, A.PurchaseItemInd, 1.00 As ExcRate ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            if (ChkItCode.Checked) SQL.AppendLine("    And ((A.ItCode Like @ItCode) Or (A.ItName Like @ItCode)) ");
            if (ChkItCtCode.Checked) SQL.AppendLine("    And A.ItCtCode = @ItCtCode ");
            SQL.AppendLine("Inner Join TblCoa C On  B.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And A.PlanningItemInd = 'Y' ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("	Select Distinct X1.ItCode From TblStockSummary X1 ");
            SQL.AppendLine("	Inner Join TblItem X2 On X1.ItCode = X2.ItCode ");
            SQL.AppendLine("	Where X1.ItCode = A.ItCode ");
            SQL.AppendLine("	And X1.WhsCode = @WhsCode ");
            if (ChkItCode.Checked) SQL.AppendLine("    And ((X1.ItCode Like @ItCode) Or (X2.ItName Like @ItCode)) ");
            if (ChkItCtCode.Checked) SQL.AppendLine("    And X2.ItCtCode = @ItCtCode ");
            SQL.AppendLine("    And X1.Qty = 0.00 ");
            //SQL.AppendLine("	Group By X1.ItCode ");
            SQL.AppendLine(") ");

            return SQL.ToString();
        }

        private string GetSQL2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '2' As Type, A.ItCode, A.ItCodeInternal, A.ItName, C.BatchNo, C.Source, B.ItCtCode, B.ItCtName, C.Lot, C.Bin, ");
            SQL.AppendLine("IfNull(C.Qty, 0.00) As Qty, A.InventoryUomCode, IfNull(C.Qty2, 0.00) As Qty2, A.InventoryUomCode2, ");
            SQL.AppendLine("IfNull(C.Qty3, 0.00) As Qty3, A.InventoryUomCode3, D.CurCode, IfNull(D.UPrice, 0.00) As UPrice, ");
            SQL.AppendLine("A.ForeignName, A.Specification, E.AcNo, E.AcDesc, A.PurchaseItemInd, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("    When D.CurCode = @MainCurCode Then 1.00 ");
            SQL.AppendLine("    When D.CurCode Is Null Then 1.00 ");
            SQL.AppendLine("    Else D.ExcRate ");
            SQL.AppendLine("End As ExcRate ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
            SQL.AppendLine("Left Join TblStockSummary C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblStockPrice D On C.Source=D.Source And C.ItCode=D.ItCode And C.BatchNo=D.BatchNo ");
            SQL.AppendLine("Left Join TblCoa E On B.AcNo = E.AcNo ");
            SQL.AppendLine("Where C.WhsCode = @WhsCode ");
            SQL.AppendLine("And A.ItCode In (Select Distinct(ItCode) From TblStockSummary Where WhsCode = @WhsCode) ");
            if (!ChkZeroStock.Checked)
                SQL.AppendLine("And C.Qty <> 0 ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Item's" + Environment.NewLine + "Local Code", 
                        "Item's Name", 
                        
                        //6-10
                        "Item's Category",
                        "Batch Number",
                        "Source",
                        "Lot",
                        "Bin", 
                        
                        //11-15
                        "Stock",
                        "UoM",
                        "Stock 2",
                        "UoM 2",
                        "Stock 3",

                        //16-20
                        "UoM 3",
                        "Currency",
                        "Price",
                        "Foreign Name",
                        "Specification",

                        //21-25
                        "Account#",
                        "Account Description",
                        "Item's Category Code",
                        "Purchase Item",
                        "Type",

                        //26
                        "ExcRate"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 80, 20, 80, 250, 
                        
                        //6-10
                        150, 200, 170, 60, 60, 
                        
                        //11-15
                        80, 80, 80, 80, 80,

                        //16-20
                        80, 60, 120, 200, 200,

                        //21-25
                        0, 0, 0, 0, 0,

                        //26
                        0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 24 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 18, 26 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,26 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 8, 9, 13, 14, 15, 16, 17, 21, 22, 23, 24, 25, 26}, false);
            if (!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 19 });
            if (!mFrmParent.mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 20 });

            Grd1.Cols[19].Move(7);
            Grd1.Cols[20].Move(6);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string mSQL = string.Empty, OrderBy = string.Empty;
                string Filter2 = string.Empty;
                string Filter3 = string.Empty;

                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter2, "C", ref mFrmParent.Grd1, 7, 8, 9);
                if (mFrmParent.GetItCode2().Length > 0) Filter3 += "And A.ItCode Not In (@ItCodeMaster) ";
                var Filter = (Filter2.Length > 0 && ChkStockSummaryItem.Checked) ? " And (" + Filter2 + ") " :
                             (mFrmParent.GetItCode2().Length > 0) ? Filter3 : " And 0=0 ";

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@ItCodeMaster", mFrmParent.GetItCode2());
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName", "A.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                Sm.CmParam<String>(ref cm, "@ItCode", "%" + TxtItCode.Text + "%");
                Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));

                mIsReCompute = false;

                if (!ChkStockSummaryItem.Checked)
                {
                    mSQL = GetSQL1();
                    OrderBy = " Order By A.ItName;";
                }
                else
                {
                    mSQL = GetSQL2();
                    OrderBy = " Order By A.ItName, C.BatchNo, C.Bin;";
                }

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + OrderBy,
                        new string[]
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "ItName", "ItCtName", "BatchNo", "Source",  
                            
                            //6-10
                            "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2", 
                            
                            //11-15
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CurCode", "UPrice",

                            //16-20
                            "ForeignName", "Specification", "AcNo", "AcDesc", "ItCtCode",

                            //21-23
                            "PurchaseItemInd", "Type", "ExcRate"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 23);
                        }, true, false, false, false
                    );

                if (mIsInventoryShowTotalQty)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 13, 15 });
                }

                //Auto checked if stock > 0
                if (Grd1.Rows.Count != 0 && ChkStockSummaryItem.Checked)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 11) > 0)
                        {
                            Grd1.Cells[Row, 1].ReadOnly = iGBool.True;
                            Grd1.Cells[Row, 1].Value = true;
                        }
                    }
                }

                #region Comment By IBL 17/05/2022
                /*
                    string ProfitCenterM = Sm.GetLue(mFrmParent.LueProfitCenterCode);
                    string ItSrcDlg = GetItSrcGrd();

                    if (mFrmParent.mProfitCenterCode.Length == 0 || mFrmParent.mProfitCenterCode != ProfitCenterM)
                    {
                        mFrmParent.mProfitCenterCode = ProfitCenterM;
                        mFrmParent.mItSrcDlg = ItSrcDlg;
                    }
                    else if (mFrmParent.mProfitCenterCode == ProfitCenterM)
                    {
                        ItSrcDlg = mFrmParent.mItSrcDlg;
                        mFrmParent.mItSrcDlg = ItSrcDlg;
                    }
                */
                #endregion
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                var ls = GetExistingData();
                mFrmParent.Grd1.ProcessTab = true;
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && 
                        !IsItCodeAlreadyChosen(
                            ls,
                            Sm.GetGrdStr(Grd1, Row, 2)+
                            Sm.GetGrdStr(Grd1, Row, 7)+
                            Sm.GetGrdStr(Grd1, Row, 8)+
                            Sm.GetGrdStr(Grd1, Row, 9)+
                            Sm.GetGrdStr(Grd1, Row, 10)
                            )
                        )
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 23);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 26);

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 18, 22, 26 });
                        if (Sm.GetGrdBool(Grd1, Row2, 24))
                        {
                            mFrmParent.Grd1.Cells[Row1, 14].ReadOnly = iGBool.True;
                            mFrmParent.Grd1.Cells[Row1, 14].BackColor = Color.FromArgb(224, 224, 224);
                        }
                        mFrmParent.Grd1.Rows.Add();
                    }
                }
                mFrmParent.ComputeTotal();
            }
            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 13, 14, 15, 16, 17, 18, 20, 21, 22, 24, 25, 26, 28, 29, 30, 32 });
            mFrmParent.Grd1.EndUpdate();
            if (!IsChoose)
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            else
            {
                if (mFrmParent.Grd1.Rows.Count>100000)
                    Sm.StdMsg(mMsgType.Warning,
                        "Number of rows : " + (mFrmParent.Grd1.Rows.Count-1).ToString() + Environment.NewLine +
                        "You've already choose data more than maximum limit ( 99.999 ).");
            }
        }

        private bool IsItCodeAlreadyChosen(List<string> ls, string Key)
        {
            foreach (string s in ls)
            {
                if (string.Compare(s, Key) == 0) return true;
            }
            return false;
        }

        private List<string> GetExistingData()
        {
            var ls = new List<string>();

            for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(mFrmParent.Grd1, Row, 1).Length != 0)
                {
                    ls.Add(
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 1) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 6) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 7) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 8) +
                        Sm.GetGrdStr(mFrmParent.Grd1, Row, 9)
                    );
                }
            }
            return ls;
        }

        internal string GetItSrcGrd()
        {
            string ItSrcCode = string.Empty;
            for (int i = 0; i <= Grd1.Rows.Count - 1; i++)
            {
                if (ItSrcCode.Length > 0) ItSrcCode += ",";
                ItSrcCode += Sm.GetGrdStr(Grd1, i, 8);
            }
            return ItSrcCode;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsInventoryShowTotalQty && mIsReCompute) Sm.GrdExpand(Grd1);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void ChkStockSummaryItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkStockSummaryItem.Checked)
            {
                ChkZeroStock.Enabled = false;
                ChkZeroStock.Checked = false;
            }
            else
                ChkZeroStock.Enabled = true;
        }


        #endregion
    }
}
