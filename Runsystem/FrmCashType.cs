﻿#region Update
/*
    01/07/2020 [ICA/IMS] New application
    12/11/2021 [DEV/PHT] Menambah field parent dan level di menu Cash Type
    09/02/2022 [ISD/AMKA] Muncul warning saat akan Save menu Cash Type
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCashType : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmCashTypeFind FrmFind;
        internal bool mIsCashTypeUseParent = false;

        #endregion

        #region Constructor

        public FrmCashType(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetLueCashTypeGroupCode(ref LueCashTypeGroupCode);
            Sl.SetLueCashType(ref LueParent);
            GetParameter();
            SetFormControl(mState.View);
            if (!mIsCashTypeUseParent)
            {
                label4.Visible = false;
                LueParent.Visible = false;
                label5.Visible = false;
                TxtLevel.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCashTypeCode, TxtCashTypeName, LueCashTypeGroupCode, LueParent, TxtLevel
                    }, true);
                    TxtCashTypeCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCashTypeCode, TxtCashTypeName, LueCashTypeGroupCode, LueParent, TxtLevel
                    }, false);
                    TxtCashTypeCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtCashTypeCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCashTypeName, LueCashTypeGroupCode, LueParent, TxtLevel
                    }, false);
                    TxtCashTypeName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCashTypeCode, TxtCashTypeName, LueCashTypeGroupCode, LueParent
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtLevel }, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCashTypeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCashTypeCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblCashType(CashTypeCode, CashTypeName, CashTypeGrpCode, ");
                if (mIsCashTypeUseParent) SQL.AppendLine("Parent, Level, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@CashTypeCode, @CashTypeName, @CashTypeGrpCode, ");
                if (mIsCashTypeUseParent) SQL.AppendLine("@Parent, @Level, ");
                SQL.AppendLine("@UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update CashTypeName=@CashTypeName, CashTypeGrpCode=@CashTypeGrpCode, ");
                if (mIsCashTypeUseParent) SQL.AppendLine("   Parent=@Parent, Level=@Level, ");
                SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CashTypeCode", TxtCashTypeCode.Text);
                Sm.CmParam<String>(ref cm, "@CashTypeName", TxtCashTypeName.Text);
                Sm.CmParam<String>(ref cm, "@CashTypeGrpCode", Sm.GetLue(LueCashTypeGroupCode));
                Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
                Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));//devi
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtCashTypeCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CashTypeCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CashTypeCode", CashTypeCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select CashTypeCode, CashTypeName, CashTypeGrpCode, "
                        +(mIsCashTypeUseParent? " Parent, Level " : " Null As Parent, Null As Level ") + 
                        " From TblCashType Where CashTypeCode=@CashTypeCode ",
                        new string[] 
                        {
                            "CashTypeCode", "CashTypeName", "CashTypeGrpCode", "Parent", "Level"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCashTypeCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCashTypeName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueCashTypeGroupCode, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueParent, Sm.DrStr(dr, c[3]));
                            //TxtLevel.EditValue = Sm.DrStr(dr, c[4]);
                            TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 11);//devi
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCashTypeCode, "Cash type code", false) ||
                Sm.IsTxtEmpty(TxtCashTypeName, "Cash type name", false) ||
                Sm.IsLueEmpty(LueCashTypeGroupCode, "Cash type group code") ||
                (mIsCashTypeUseParent && Sm.IsTxtEmpty(TxtLevel, "Level", false)) ||
                IsCashTypeCodeExisted();
        }

        private bool IsCashTypeCodeExisted()
        {
            if (!TxtCashTypeCode.Properties.ReadOnly && Sm.IsDataExist("Select CashTypeName From TblCashType Where CashTypeCode='" + TxtCashTypeCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Cash type code ( " + TxtCashTypeCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsCashTypeUseParent = Sm.GetParameterBoo("IsCashTypeUseParent");
        }

        public void SetLueCashTypeGroupCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select CashTypeGrpCode As Col1, CashTypeGrpName As Col2 ");
                SQL.AppendLine("From TblCashTypeGroup Order By CashTypeGrpName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCashTypeGroupCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCashTypeGroupCode, new Sm.RefreshLue1(SetLueCashTypeGroupCode));
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(Sl.SetLueCashType));
            decimal LevelParent = Sm.GetValueDec("SELECT Level FROM TblCashType WHERE Parent=@Param", Sm.GetLue(LueParent));
            TxtLevel.Text = Sm.FormatNum(LevelParent + 1, 1);
        }

        private void TxtCashTypeCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCashTypeCode);
        }

        private void TxtLevel_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLevel,1);
        }

        private void TxtCashTypeName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCashTypeName);
        }

        #endregion

        #endregion
    }
}
