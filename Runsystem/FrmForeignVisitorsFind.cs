﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmForeignVisitorsFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmForeignVisitors mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmForeignVisitorsFind(FrmForeignVisitors FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Active",
                    "Foreign Visitors",
                    "Total"+Environment.NewLine+"Foreign Visitors",
                    "Created"+Environment.NewLine+"By",

                    //6-10
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    140, 60, 100, 100, 100, 

                    //6-10
                    100, 100, 100, 100, 100
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 6, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 2 });
            Sm.GrdFormatTime(Grd1, new int[] { 7, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select * From TblForeignVisitors ");

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "DocNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString() +
                        Filter + " Order By DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "ActInd", "ForeignVisitors", "TotalForeignVisitors", "CreateBy", "CreateDt", 

                            //6-7
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document");
        }

        #endregion

        #endregion
    }
}
