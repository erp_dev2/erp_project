﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWagesFormulation : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo= string.Empty;
        internal FrmWagesFormulationFind FrmFind;

        #endregion

        #region Constructor

        public FrmWagesFormulation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Wages Formulation";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sl.SetLueUomCode(ref LueUomCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "From", "To", "Wages", "Penalty" },
                    new int[] { 120, 120, 120, 120 }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 0, 1, 2, 3 }, 0);

            Grd2.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    { 
                        //0
                        "", 

                        //1-4
                        "Grade Code", 
                        "Grade Name", 
                        "Index" + Environment.NewLine + "(Wages)", 
                        "Index" + Environment.NewLine + "(Penalty)" 
                    },
                    new int[] { 20, 100, 200, 120, 120 }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 3, 4 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWagesFormulationCode, TxtWagesFormulationName, LueUomCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4 });
                    TxtWagesFormulationCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtWagesFormulationCode, TxtWagesFormulationName, LueUomCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 3, 4 });
                    TxtWagesFormulationCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtWagesFormulationName, false);
                    Sm.SetControlReadOnly(LueUomCode, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 1, 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 3, 4 });
                    TxtWagesFormulationName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtWagesFormulationCode, TxtWagesFormulationName, LueUomCode
            });
            ClearGrd(); 
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 0, 1, 2, 3 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWagesFormulationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWagesFormulationCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWagesFormulationCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Delete From TblWagesFormulationDtl Where WagesFormulationCode=@WagesFormulationCode; " +
                        "Delete From TblWagesFormulationHdr Where WagesFormulationCode=@WagesFormulationCode; " +
                        "Delete From TblWagesFormulationDtl2 Where WagesFormulationCode=@WagesFormulationCode; "
                };
                Sm.CmParam<String>(ref cm, "@WagesFormulationCode", TxtWagesFormulationCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveWagesFormulationHdr());

                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveWagesFormulationDtl(Row));

                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) 
                        cml.Add(SaveWagesFormulationDtl2(Row));

                Sm.ExecCommands(cml);

                ShowData(TxtWagesFormulationCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 1, 2, 3 });
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 0, 1, 2, 3 }, e);
        }
        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWagesFormulationCode, "Code", false) ||
                Sm.IsTxtEmpty(TxtWagesFormulationCode, "Name", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()||
                IsWagesFormulationCodeExisted()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Value.");
                return true;
            }
            return false;
        }

        private bool IsWagesFormulationCodeExisted()
        {
            if (!TxtWagesFormulationCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand() { CommandText = "Select WagesFormulationCode From TblWagesFormulationHdr Where WagesFormulationCode=@WagesFormulationCode Limit 1" };
                Sm.CmParam<String>(ref cm, "@WagesFormulationCode", TxtWagesFormulationCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Code ( " + TxtWagesFormulationCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 0, true, "From should be greater than 0.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Wages should be greater than 0.")) return true;
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 3, true, "Wages index should be greater than 0.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 4, true, "Penalty index should be greater than 0.")) return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveWagesFormulationHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWagesFormulationHdr(WagesFormulationCode, WagesFormulationName, UomCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@WagesFormulationCode, @WagesFormulationName, @UomCode, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update WagesFormulationName=@WagesFormulationName, UomCode=@UomCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();");

            SQL.AppendLine("Delete From TblWagesFormulationDtl Where WagesFormulationCode=@WagesFormulationCode; ");

            SQL.AppendLine("Delete From TblWagesFormulationDtl2 Where WagesFormulationCode=@WagesFormulationCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", TxtWagesFormulationCode.Text);
            Sm.CmParam<String>(ref cm, "@WagesFormulationName", TxtWagesFormulationName.Text);
            Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetLue(LueUomCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWagesFormulationDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblWagesFormulationDtl(WagesFormulationCode, ValueFrom, ValueTo, Wages, Penalty, CreateBy, CreateDt, LastUpBy, LastUpDt) " +
                    "Select WagesFormulationCode, @ValueFrom, @ValueTo, @Wages, @Penalty, CreateBy, CreateDt, LastUpBy, LastUpDt " +
                    "From TblWagesFormulationHdr Where WagesFormulationCode=@WagesFormulationCode; "
            };
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", TxtWagesFormulationCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@ValueFrom", Sm.GetGrdDec(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@ValueTo", Sm.GetGrdDec(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Wages", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Penalty", Sm.GetGrdDec(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWagesFormulationDtl2(int Row)
        {

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblWagesFormulationDtl2(WagesFormulationCode, GrdLvlCode, GrdLvlIndexW, GrdLvlIndexP, CreateBy, CreateDt, LastUpBy, LastUpDt) " +
                    "Select WagesFormulationCode, @GrdLvlCode, @GrdLvlIndexW, @GrdLvlIndexP, CreateBy, CreateDt, LastUpBy, LastUpDt " +
                    "From TblWagesFormulationHdr Where WagesFormulationCode=@WagesFormulationCode; "
            };
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", TxtWagesFormulationCode.Text);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndexW", Sm.GetGrdDec(Grd2, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndexP", Sm.GetGrdDec(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Show Data

        public void ShowData(string WagesFormulationCode)
        {
            try
            {
                ClearData();
                ShowWagesFormulationHdr(WagesFormulationCode);
                ShowWagesFormulationDtl(WagesFormulationCode);
                ShowWagesFormulationDtl2(WagesFormulationCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWagesFormulationHdr(string WagesFormulationCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", WagesFormulationCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select WagesFormulationCode, WagesFormulationName, UomCode From TblWagesFormulationHdr Where WagesFormulationCode=@WagesFormulationCode",
                    new string[] { "WagesFormulationCode", "WagesFormulationName", "UomCode" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtWagesFormulationCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtWagesFormulationName.EditValue = Sm.DrStr(dr, c[1]);
                        LueUomCode.EditValue = Sm.DrStr(dr, c[2]);
                    }, true
                );
        }

        private void ShowWagesFormulationDtl(string WagesFormulationCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", WagesFormulationCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select ValueFrom, ValueTo, Wages, Penalty " +
                    "From TblWagesFormulationDtl " +
                    "Where WagesFormulationCode=@WagesFormulationCode " +
                    "Order By ValueFrom;",
                    new string[] 
                    { 
                        "ValueFrom", "ValueTo", "Wages", "Penalty"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 1, 2, 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowWagesFormulationDtl2(string WagesFormulationCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", WagesFormulationCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select A.GrdLvlCode, B.GrdLvlName, A.GrdLvlIndexW, A.GrdLvlIndexP " +
                    "From TblWagesFormulationDtl2 A " +
                    "Inner Join TblGradeLevelHdr B On A.GrdLvlCode = B.GrdLvlCode " +
                    "Where A.WagesFormulationCode=@WagesFormulationCode " +
                    "Order By A.GrdLvlCode;",
                    new string[] 
                    { 
                        "GrdLvlCode", "GrdLvlName", "GrdLvlIndexW", "GrdLvlIndexP" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedGradeLevel()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtWagesFormulationCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWagesFormulationCode);
        }

        private void TxtWagesFormulationName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWagesFormulationName);
        }

        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtWagesFormulationCode.Text.Length == 0)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmWagesFormulationDlg(this));
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmWagesFormulationDlg(this));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion
    }
}
