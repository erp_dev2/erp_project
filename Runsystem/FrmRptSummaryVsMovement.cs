﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSummaryVsMovement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsShowForeignName = false, mIsFilterByItCt = false;
       
        #endregion

        #region Constructor

        public FrmRptSummaryVsMovement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueWhsCode(ref LueWhsCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary vs Stock Movement */ ");
            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select ");
            SQL.AppendLine("T1.WhsCode, T2.WhsName, T1.Lot, T1.Bin, T1.ItCode, T3.ItName, T3.ForeignName, T1.BatchNo, T1.Source, ");
            SQL.AppendLine("T1.SummaryQty, T1.SummaryQty2, T1.SummaryQty3, ");
            SQL.AppendLine("T1.MovementQty, T1.MovementQty2, T1.MovementQty3 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source, ");
            SQL.AppendLine("A.Qty As SummaryQty, A.Qty2 As SummaryQty2, A.Qty3 As SummaryQty3, ");
            SQL.AppendLine("IfNull(B.MovementQty, 0) As MovementQty, IfNull(B.MovementQty2, 0) As MovementQty2, IfNull(B.MovementQty3, 0) As MovementQty3 ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, ");
            SQL.AppendLine("Sum(Qty) As MovementQty, Sum(Qty2) As MovementQty2, Sum(Qty3) As MovementQty3 ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Group By WhsCode, Lot, Bin, ItCode, BatchNo, Source ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("On A.WhsCode=B.WhsCode And A.Lot=B.Lot And A.Bin=B.Bin ");
            SQL.AppendLine("And A.ItCode=B.ItCode And A.PropCode=B.PropCode And A.BatchNo=B.BatchNo And A.Source=B.Source ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblWarehouse T2 On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("Inner Join TblItem T3 On T1.ItCode=T3.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T3.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where T1.SummaryQty<>T1.MovementQty Or T1.SummaryQty2<>T1.MovementQty2 Or T1.SummaryQty3<>T1.MovementQty3 ");
            SQL.AppendLine("Order By T2.WhsName, T1.Lot, T1.Bin, T3.ItName, T1.BatchNo, T1.Source ");
            SQL.AppendLine(") T4 ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 4;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Header.Cells[0, 1].Value = "Warehouse";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Header.Cells[0, 2].Value = "Lot";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[0, 3].Value = "Bin";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Header.Cells[0, 4].Value = "Item's Code";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Header.Cells[0, 5].Value = "Item's Name";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Header.Cells[0, 6].Value = "Batch#";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Header.Cells[0, 7].Value = "Source";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Header.Cells[1, 8].Value = "Summary ";
            Grd1.Header.Cells[1, 8].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 8].SpanCols = 3;

            Grd1.Header.Cells[0, 8].Value = "Quantity";
            Grd1.Header.Cells[0, 8].SpanRows = 1;
            Grd1.Header.Cells[0, 9].Value = "Quantity";
            Grd1.Header.Cells[0, 9].SpanRows = 1;
            Grd1.Header.Cells[0, 10].Value = "Quantity";
            Grd1.Header.Cells[0, 10].SpanRows = 1;

            Grd1.Header.Cells[1, 11].Value = "Movement";
            Grd1.Header.Cells[1, 11].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 11].SpanCols = 3;
            
            Grd1.Header.Cells[0, 11].Value = "Quantity";
            Grd1.Header.Cells[0, 11].SpanRows = 1;
            Grd1.Header.Cells[0, 12].Value = "Quantity";
            Grd1.Header.Cells[0, 12].SpanRows = 1;
            Grd1.Header.Cells[0, 13].Value = "Quantity";
            Grd1.Header.Cells[0, 13].SpanRows = 1;

            Grd1.Header.Cells[0, 14].Value = "Foreign"+Environment.NewLine+"Name";
            Grd1.Header.Cells[0, 14].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 14].SpanRows = 2;
                       

            Sm.GrdFormatDec(Grd1,
                new int[] { 

                    8, 9, 10, 11, 12, 13

                }, 0);
            Grd1.Cols[14].Move(6);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 14 }, false);
          
            Sm.SetGrdProperty(Grd1, true);
        }       

        override protected void ShowData()
        {   
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "T4.WhsCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter ,
                        new string[]
                        {
                             //0
                             "WhsName",
                             //1-5
                             "Lot", "Bin", "ItCode", "ItName", "BatchNo",
                             //6-10
                             "Source","SummaryQty", "SummaryQty2", "SummaryQty3","MovementQty",
                             //11-13
                             "MovementQty2", "MovementQty3", "ForeignName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        }, true, true, false, true
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                { 8, 9, 10, 11, 12, 13 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.SetGrdProperty(Grd1, true);
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        #endregion

        #endregion
    }
}
