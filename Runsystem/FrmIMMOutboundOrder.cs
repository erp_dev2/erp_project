﻿#region Update
/*
    24/07/2019 [TKG] New application
    15/08/2019 [TKG] ubah cara import data, tambah informasi daftar source
    06/09/2019 [TKG] tidak perlu generate list, tidak perlu location (kurang cek outstanding outbound order selain cek stock)
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmIMMOutboundOrder : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocTitle = string.Empty,
            mDocAbbr = string.Empty,
            mDocNo = string.Empty, mDocSeqNo = string.Empty; //if this application is called from other application;
        internal FrmIMMOutboundOrderFind FrmFind;

        #endregion

        #region Constructor

        public FrmIMMOutboundOrder(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Tc1.SelectedTabPage = Tp1;
                Tp2.PageVisible = false;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, mDocSeqNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mDocTitle = Sm.GetParameter("DocTitle");
            mDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='IMMOutboundOrder';");
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, MeeCancelReason }, true);
                    BtnImport.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    BtnImport.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Tp1.PageVisible = true;
            Tc1.SelectedTabPage = Tp1;
            Tp2.PageVisible = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtOrderDocNo, TxtReceiptDocNo,
                TxtDeliveryService, TxtCounterPickup, DteDueDt, TmeDueTm, DteOrderDt, 
                TmeOrderTm, DtePaymentDt, TmePaymentTm, DteCompleteDt, TmeCompleteTm, 
                TxtProdCode, TxtProdDesc, TxtCurCode, TxtColor, TxtWeightUomCode, 
                TxtPFCode, TxtBuyer, TxtReceiver, MeeAddress, TxtCity, 
                TxtProvince, TxtPhone, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtPrice, TxtQty, TxtAmt, TxtWeight, TxtDeliveryFee, 
                TxtTotal 
            }, 0);
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMOutboundOrderFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Tp2.PageVisible = true;
                Tc1.SelectedTabPage = Tp2;
                Tp1.PageVisible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Tp2.PageVisible)
                {
                    BtnImport_Click(sender, e);
                    return;
                }
                EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void Insert1(ref List<ImportData> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            bool IsFirst = true;
            string
                PFCodeTemp = string.Empty,
                PFNameTemp = string.Empty,
                OrderDocNoTemp = string.Empty,
                ReceiptDocNoTemp = string.Empty,
                DeliveryServiceTemp = string.Empty,
                CounterPickupTemp = string.Empty,
                ProdCodeTemp = string.Empty,
                CurCodeTemp = string.Empty,
                WeightUomCodeTemp = string.Empty,
                DueDtTemp = string.Empty,
                DueTmTemp = string.Empty,
                OrderDtTemp = string.Empty,
                OrderTmTemp = string.Empty,
                PaymentDtTemp = string.Empty,
                PaymentTmTemp = string.Empty,
                CompleteDtTemp = string.Empty,
                CompleteTmTemp = string.Empty,
                BuyerTemp = string.Empty,
                ReceiverTemp = string.Empty,
                AddressTemp = string.Empty,
                CityTemp = string.Empty,
                ProvinceTemp = string.Empty,
                PhoneTemp = string.Empty,
                RemarkTemp = string.Empty,
                DtTm = string.Empty;
            decimal
                QtyTemp = 0m,
                PriceTemp = 0m,
                WeightTemp = 0m,
                DeliveryFeeTemp = 0m;
            int n = 0;
            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            PFCodeTemp = arr[0].Trim();
                            PFNameTemp = arr[1].Trim();
                            OrderDocNoTemp = arr[2].Trim();
                            ReceiptDocNoTemp = arr[3].Trim();
                            DeliveryServiceTemp = arr[4].Trim();
                            CounterPickupTemp = arr[5].Trim();
                            DtTm = arr[6].Trim();
                            if (DtTm.Length > 0)
                            {
                                DtTm = DtTm.Replace("-", "");
                                DtTm = DtTm.Replace(":", "");
                                DtTm = DtTm.Replace(" ", "");
                                DueDtTemp = Sm.Left(DtTm, 8);
                                DueTmTemp = Sm.Right(DtTm, 4);
                            }
                            else
                            {
                                DueDtTemp = string.Empty;
                                DueTmTemp = string.Empty;
                            }
                            DtTm = arr[7].Trim();
                            if (DtTm.Length > 0)
                            {
                                DtTm = DtTm.Replace("-", "");
                                DtTm = DtTm.Replace(":", "");
                                DtTm = DtTm.Replace(" ", "");
                                OrderDtTemp = Sm.Left(DtTm, 8);
                                OrderTmTemp = Sm.Right(DtTm, 4);
                            }
                            else
                            {
                                OrderDtTemp = string.Empty;
                                OrderTmTemp = string.Empty;
                            }
                            DtTm = arr[8].Trim();
                            if (DtTm.Length > 0)
                            {
                                DtTm = DtTm.Replace("-", "");
                                DtTm = DtTm.Replace(":", "");
                                DtTm = DtTm.Replace(" ", "");
                                PaymentDtTemp = Sm.Left(DtTm, 8);
                                PaymentTmTemp = Sm.Right(DtTm, 4);
                            }
                            else
                            {
                                PaymentDtTemp = string.Empty;
                                PaymentTmTemp = string.Empty;
                            }
                            ProdCodeTemp = arr[9].Trim();
                            CurCodeTemp = arr[10].Trim();
                            if (arr[11].Trim().Length > 0)
                                PriceTemp = decimal.Parse(arr[11].Trim());
                            else
                                PriceTemp = 0m;
                            if (arr[12].Trim().Length > 0)
                                QtyTemp = decimal.Parse(arr[12].Trim());
                            else
                                QtyTemp = 0m;
                            if (arr[13].Trim().Length > 0)
                                WeightTemp = decimal.Parse(arr[13].Trim());
                            else
                                WeightTemp = 0m;
                            WeightUomCodeTemp = arr[14].Trim();
                            if (arr[15].Trim().Length > 0)
                                DeliveryFeeTemp = decimal.Parse(arr[15].Trim());
                            else
                                DeliveryFeeTemp = 0m;
                            RemarkTemp = arr[16].Trim();
                            BuyerTemp = arr[17].Trim();
                            ReceiverTemp = arr[18].Trim();
                            PhoneTemp = arr[19].Trim();
                            AddressTemp = arr[20].Trim();
                            CityTemp = arr[21].Trim();
                            ProvinceTemp = arr[22].Trim();
                            DtTm = arr[23].Trim();
                            if (DtTm.Length > 0)
                            {
                                DtTm = DtTm.Replace("-", "");
                                DtTm = DtTm.Replace(":", "");
                                DtTm = DtTm.Replace(" ", "");
                                CompleteDtTemp = Sm.Left(DtTm, 8);
                                CompleteTmTemp = Sm.Right(DtTm, 4);
                            }
                            else
                            {
                                CompleteDtTemp = string.Empty;
                                CompleteTmTemp = string.Empty;
                            }
                            n += 1;
                            l.Add(new ImportData()
                            {
                                PFCode = PFCodeTemp,
                                PFName = PFNameTemp,
                                OrderDocNo = OrderDocNoTemp,
                                ReceiptDocNo = ReceiptDocNoTemp,
                                DeliveryService = DeliveryServiceTemp,
                                CounterPickup = CounterPickupTemp,
                                ProdCode = ProdCodeTemp,
                                CurCode = CurCodeTemp,
                                WeightUomCode = WeightUomCodeTemp,
                                DueDt = DueDtTemp,
                                DueTm = DueTmTemp,
                                OrderDt = OrderDtTemp,
                                OrderTm = OrderTmTemp,
                                PaymentDt = PaymentDtTemp,
                                PaymentTm = PaymentTmTemp,
                                CompleteDt = CompleteDtTemp,
                                CompleteTm = CompleteTmTemp,
                                Buyer = BuyerTemp,
                                Receiver = ReceiverTemp,
                                Address = AddressTemp,
                                City = CityTemp,
                                Province = ProvinceTemp,
                                Phone = PhoneTemp,
                                Remark = RemarkTemp,
                                Price = PriceTemp,
                                Qty = QtyTemp,
                                Weight = WeightTemp,
                                DeliveryFee = DeliveryFeeTemp,
                                Stock = 0m,
                                No = n
                            });
                        }
                    }
                }
            }
        }

        private void Insert2(ref string DocNo, ref decimal DocSeqNo, string CurrentDt)
        {
            string Yr = Sm.Left(CurrentDt, 4);
            string Mth = CurrentDt.Substring(4, 2);

            DocNo = string.Concat(mDocTitle, "/", mDocAbbr, "/", Yr, "/", Mth, "/");

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("    Select DocSeqNo From TblIMMOutboundOrder ");
            SQL.Append("    Where DocNo=@Param Order By DocSeqNo Desc Limit 1 ");
            SQL.Append("   ), 0.00) As DocSeqNo");

            DocSeqNo = Sm.GetValueDec(SQL.ToString(), DocNo);
        }

        private void Insert3(ref List<ImportData> l, string DocNo, decimal DocSeqNo)
        {
            var s = DocSeqNo;
            foreach (var i in l)
            {
                s += 1m;
                i.DocNo = DocNo;
                i.DocSeqNo = (int)s;
            }
        }

        private void Insert4(ref List<ImportData> l, ref List<ImportData2> l2)
        {
            string Filter = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                
                for (int i = 0; i < l.Count; i++)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (A.ProdCode=@ProdCode" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ProdCode" + i.ToString(), l[i].ProdCode);
                }

                Filter = " And (" + Filter + ") ";

                SQL.AppendLine("Select A.ProdCode, Sum(A.Qty) As Qty ");
                SQL.AppendLine("From TblIMMStockSummary A, TblBin B ");
                SQL.AppendLine("Where A.Qty>0 ");
                SQL.AppendLine("And A.Bin Is Not Null ");
                SQL.AppendLine("And A.Bin=B.Bin ");
                SQL.AppendLine("And B.ActInd='Y' ");
                SQL.AppendLine("And B.QuarantineInd='N' ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=A.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("Group By A.ProdCode ");
                SQL.AppendLine("Having Sum(A.Qty)>0 ");
                SQL.AppendLine(";");

                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "ProdCode", "Qty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new ImportData2()
                        {
                            ProdCode = Sm.DrStr(dr, 0),
                            Stock = Sm.DrDec(dr, 1)
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Insert5(ref List<ImportData> l, ref List<ImportData2> l2)
        {
            foreach (var i in l)
            {
                foreach (var j in l2.Where(w=>Sm.CompareStr(w.ProdCode, i.ProdCode) && w.Stock>0m))
                {
                    if (i.Qty >= j.Stock)
                    {
                        i.Stock = j.Stock;
                        j.Stock = 0m;
                    }
                    else
                    {
                        i.Stock = i.Qty;
                        j.Stock -= i.Qty;
                    }
                }
            }
        }

        private void Insert6(ref List<ImportData> l, string CurrentDt)
        {
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            int n = 0, o = 0;
            decimal s = 1;

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt", CurrentDt);

            foreach (var i in l)
            {
                n++;

                SQL.AppendLine("Insert Into TblIMMPlatform(PFCode, PFName, CreateBy, CreateDt) ");
                SQL.AppendLine("Select PFCode, PFName, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select @PFCode" + n.ToString() + " As PFCode, ");
                SQL.AppendLine("    @PFName" + n.ToString() + " As PFName ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Where Not Exists(Select 1 From TblIMMPlatform Where PFCode=@PFCode" + n.ToString() + "); ");

                SQL.AppendLine("Insert Into TblIMMOutboundOrder ");
                SQL.AppendLine("(DocNo, DocSeqNo, DocDt, CancelInd, CancelReason, ");
                SQL.AppendLine("OrderDocNo, ReceiptDocNo, DeliveryService, CounterPickup, ");
                SQL.AppendLine("ProdCode, Qty, CurCode, Price, Weight, WeightUomCode, DeliveryFee, ");
                SQL.AppendLine("DueDt, DueTm, OrderDt, OrderTm, PaymentDt, PaymentTm, CompleteDt, CompleteTm, ");
                SQL.AppendLine("PFCode, Buyer, Receiver, Address, City, Province, Phone, Remark, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");
                SQL.AppendLine("(@DocNo" + n.ToString() + ", @DocSeqNo" + n.ToString() + ", @DocDt, 'N', Null, ");
                SQL.AppendLine("@OrderDocNo" + n.ToString() + ", @ReceiptDocNo" + n.ToString() + ", @DeliveryService" + n.ToString() + ", @CounterPickup" + n.ToString() + ", ");
                SQL.AppendLine("@ProdCode" + n.ToString() + ", @Qty" + n.ToString() + ", @CurCode" + n.ToString() + ", @Price" + n.ToString() + ", @Weight" + n.ToString() + ", @WeightUomCode" + n.ToString() + ", @DeliveryFee" + n.ToString() + ", ");
                SQL.AppendLine("@DueDt" + n.ToString() + ", @DueTm" + n.ToString() + ", @OrderDt" + n.ToString() + ", @OrderTm" + n.ToString() + ", @PaymentDt" + n.ToString() + ", @PaymentTm" + n.ToString() + ", @CompleteDt" + n.ToString() + ", @CompleteTm" + n.ToString() + ", ");
                SQL.AppendLine("@PFCode" + n.ToString() + ", @Buyer" + n.ToString() + ", @Receiver" + n.ToString() + ", @Address" + n.ToString() + ", @City" + n.ToString() + ", @Province" + n.ToString() + ", @Phone" + n.ToString() + ", @Remark" + n.ToString() + ", ");
                SQL.AppendLine("@UserCode, CurrentDateTime()); ");

                Sm.CmParam<String>(ref cm, string.Concat("@DocNo", n.ToString()), i.DocNo);
                Sm.CmParam<Decimal>(ref cm, string.Concat("@DocSeqNo", n.ToString()), i.DocSeqNo);
                Sm.CmParam<String>(ref cm, string.Concat("@OrderDocNo", n.ToString()), i.OrderDocNo);
                Sm.CmParam<String>(ref cm, string.Concat("@ReceiptDocNo", n.ToString()), i.ReceiptDocNo);
                Sm.CmParam<String>(ref cm, string.Concat("@DeliveryService", n.ToString()), i.DeliveryService);
                Sm.CmParam<String>(ref cm, string.Concat("@CounterPickup", n.ToString()), i.CounterPickup);
                Sm.CmParam<String>(ref cm, string.Concat("@ProdCode", n.ToString()), i.ProdCode);
                Sm.CmParam<Decimal>(ref cm, string.Concat("@Qty", n.ToString()), i.Qty);
                Sm.CmParam<String>(ref cm, string.Concat("@CurCode", n.ToString()), i.CurCode);
                Sm.CmParam<Decimal>(ref cm, string.Concat("@Price", n.ToString()), i.Price);
                Sm.CmParam<Decimal>(ref cm, string.Concat("@Weight", n.ToString()), i.Weight);
                Sm.CmParam<String>(ref cm, string.Concat("@WeightUomCode", n.ToString()), i.WeightUomCode);
                Sm.CmParam<Decimal>(ref cm, string.Concat("@DeliveryFee", n.ToString()), i.DeliveryFee);
                Sm.CmParam<String>(ref cm, string.Concat("@DueDt", n.ToString()), i.DueDt);
                Sm.CmParam<String>(ref cm, string.Concat("@DueTm", n.ToString()), i.DueTm);
                Sm.CmParam<String>(ref cm, string.Concat("@OrderDt", n.ToString()), i.OrderDt);
                Sm.CmParam<String>(ref cm, string.Concat("@OrderTm", n.ToString()), i.OrderTm);
                Sm.CmParam<String>(ref cm, string.Concat("@PaymentDt", n.ToString()), i.PaymentDt);
                Sm.CmParam<String>(ref cm, string.Concat("@PaymentTm", n.ToString()), i.PaymentTm);
                Sm.CmParam<String>(ref cm, string.Concat("@CompleteDt", n.ToString()), i.CompleteDt);
                Sm.CmParam<String>(ref cm, string.Concat("@CompleteTm", n.ToString()), i.CompleteTm);
                Sm.CmParam<String>(ref cm, string.Concat("@PFCode", n.ToString()), i.PFCode);
                Sm.CmParam<String>(ref cm, string.Concat("@PFName", n.ToString()), i.PFName);
                Sm.CmParam<String>(ref cm, string.Concat("@Buyer", n.ToString()), i.Buyer);
                Sm.CmParam<String>(ref cm, string.Concat("@Receiver", n.ToString()), i.Receiver);
                Sm.CmParam<String>(ref cm, string.Concat("@Address", n.ToString()), i.Address);
                Sm.CmParam<String>(ref cm, string.Concat("@City", n.ToString()), i.City);
                Sm.CmParam<String>(ref cm, string.Concat("@Province", n.ToString()), i.Province);
                Sm.CmParam<String>(ref cm, string.Concat("@Phone", n.ToString()), i.Phone);
                Sm.CmParam<String>(ref cm, string.Concat("@Remark", n.ToString()), i.Remark);
            }

            cm.CommandText = SQL.ToString();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditIMMOutboundOrder());

            Sm.ExecCommands(cml);

            ShowData(mDocNo, mDocSeqNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation");
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select 1 From TblIMMOutboundOrder Where CancelInd='Y' And DocNo=@Param1 And DocSeqNo=@Param2;",
                mDocNo, mDocSeqNo, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditIMMOutboundOrder()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIMMIMMOutboundOrder Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DocSeqNo=@DocSeqNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", mDocSeqNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, string DocSeqNo)
        {
            try
            {
                ClearData();
                ShowIMMOutboundOrder(DocNo, DocSeqNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowIMMOutboundOrder(string DocNo, string DocSeqNo)
        {
            mDocNo = DocNo;
            mDocSeqNo = DocSeqNo;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, B.PFName, C.ProdDesc, C.Color ");
            SQL.AppendLine("From TblIMMOutboundOrder A ");
            SQL.AppendLine("Inner Join TblIMMPlatform B On A.PFCode=B.PFCode ");
            SQL.AppendLine("Inner Join TblIMMProduct C On A.ProdCode=C.ProdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocSeqNo=@DocSeqNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocDt", 

                        //1-5
                        "CancelReason", "CancelInd", "ProdCode", "ProdDesc", "Color",
 
                        //6-10
                        "Qty", "CurCode", "Price", "Weight", "WeightUomCode",    
                        
                        //11-15
                        "DeliveryFee", "OrderDocNo", "ReceiptDocNo", "DeliveryService", "CounterPickup", 

                        //16-20
                        "DueDt", "DueTm", "OrderDt", "OrderTm", "PaymentDt",

                        //21-25
                        "PaymentTm", "CompleteDt", "CompleteTm", "PFName", "Buyer", 

                        //26-30
                        "Receiver", "Address", "City", "Province", "Phone", 
                        
                        //31
                        "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = string.Concat(DocNo, DocSeqNo);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0]));
                        MeeCancelReason.Text = Sm.DrStr(dr, c[1]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        TxtProdCode.Text = Sm.DrStr(dr, c[3]);
                        TxtProdDesc.Text = Sm.DrStr(dr, c[4]);
                        TxtColor.Text = Sm.DrStr(dr, c[5]);
                        TxtQty.Text = Sm.FormatNum(Sm.DrDec(dr, c[6]), 2);
                        TxtCurCode.Text = Sm.DrStr(dr, c[7]);
                        TxtPrice.Text = Sm.FormatNum(Sm.DrDec(dr, c[8]), 2);
                        TxtAmt.Text = Sm.FormatNum(decimal.Parse(TxtQty.Text) + decimal.Parse(TxtPrice.Text), 2);
                        TxtWeight.Text = Sm.FormatNum(Sm.DrDec(dr, c[9]), 2);
                        TxtWeightUomCode.Text = Sm.DrStr(dr, c[10]);
                        TxtDeliveryFee.Text = Sm.FormatNum(Sm.DrDec(dr, c[11]), 2);
                        TxtTotal.Text = Sm.FormatNum(decimal.Parse(TxtAmt.Text) + decimal.Parse(TxtDeliveryFee.Text), 2);
                        TxtOrderDocNo.Text = Sm.DrStr(dr, c[12]);
                        TxtReceiptDocNo.Text = Sm.DrStr(dr, c[13]);
                        TxtCounterPickup.Text = Sm.DrStr(dr, c[14]);
                        TxtDeliveryService.Text = Sm.DrStr(dr, c[15]);
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[16]));
                        Sm.SetTme(TmeDueTm, Sm.DrStr(dr, c[17]));
                        Sm.SetDte(DteOrderDt, Sm.DrStr(dr, c[18]));
                        Sm.SetTme(TmeOrderTm, Sm.DrStr(dr, c[19]));
                        Sm.SetDte(DtePaymentDt, Sm.DrStr(dr, c[20]));
                        Sm.SetTme(TmePaymentTm, Sm.DrStr(dr, c[21]));
                        Sm.SetDte(DteCompleteDt, Sm.DrStr(dr, c[22]));
                        Sm.SetTme(TmeCompleteTm, Sm.DrStr(dr, c[23]));
                        TxtPFCode.Text = Sm.DrStr(dr, c[24]);
                        TxtBuyer.Text = Sm.DrStr(dr, c[25]);
                        TxtReceiver.Text = Sm.DrStr(dr, c[26]);
                        MeeAddress.Text = Sm.DrStr(dr, c[27]);
                        TxtCity.Text = Sm.DrStr(dr, c[28]);
                        TxtProvince.Text = Sm.DrStr(dr, c[29]);
                        TxtPhone.Text = Sm.DrStr(dr, c[30]);
                        MeeRemark.Text = Sm.DrStr(dr, c[31]);
                    }, true
                );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnImport_Click(object sender, EventArgs e)
        {
            var l = new List<ImportData>();
            var l2 = new List<ImportData2>();

            string DocNo = string.Empty;
            string CurrentDt = Sm.ServerCurrentDate();
            decimal DocSeqNo = 0m;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                Insert1(ref l);
                if (l.Count > 0)
                {
                    Insert2(ref DocNo, ref DocSeqNo, CurrentDt);
                    Insert3(ref l, DocNo, DocSeqNo);
                    Insert4(ref l, ref l2);
                    Insert5(ref l, ref l2);
                    foreach (var i in l.Where(w=>w.Qty>w.Stock))
                    {
                        Sm.StdMsg(mMsgType.Info, 
                            "Order# : " + i.DocNo + Environment.NewLine +
                            "Product's Code : " + i.ProdCode + Environment.NewLine +
                            "Quantity : " + Sm.FormatNum(i.Qty, 0) + Environment.NewLine +
                            "Stock : " + Sm.FormatNum(i.Stock, 0) + Environment.NewLine +
                            "Not enough stock for this order.");
                        return;
                    }
                    //Insert6(ref l, ref l3);
                    //Insert7(ref l, ref l3);
                    //Insert8(ref l, ref l3, CurrentDt);
                    Insert6(ref l, CurrentDt);

                    Sm.StdMsg(mMsgType.Info, "Importing data process is completed.");

                    BtnCancelClick(sender, e);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                l2.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
            }
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }

        #endregion

        #endregion

        #region Class

        private class ImportData
        {
            public string OrderDocNo { get; set; }
            public string ReceiptDocNo { get; set; }
            public string DeliveryService { get; set; }
            public string CounterPickup { get; set; }
            public string ProdCode { get; set; }
            public decimal Qty { get; set; }
            public string CurCode { get; set; }
            public decimal Price { get; set; }
            public decimal Weight { get; set; }
            public string WeightUomCode { get; set; }
            public decimal DeliveryFee { get; set; }
            public string DueDt { get; set; }
            public string DueTm { get; set; }
            public string OrderDt { get; set; }
            public string OrderTm { get; set; }
            public string PaymentDt { get; set; }
            public string PaymentTm { get; set; }
            public string CompleteDt { get; set; }
            public string CompleteTm { get; set; }
            public string PFCode { get; set; }
            public string PFName { get; set; }
            public string Buyer { get; set; }
            public string Receiver { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string Province { get; set; }
            public string Phone { get; set; }
            public string Remark { get; set; }
            public string DocNo { get; set; }
            public decimal DocSeqNo { get; set; }
            public int No { get; set; }
            public decimal Stock { get; set; }
        }

        private class ImportData2
        {
            public string ProdCode { get; set; }
            public decimal Stock { get; set; }
        }

        //private class ImportData3
        //{
        //    public string WhsCode { get; set; }
        //    public string Bin { get; set; }
        //    public string ProdCode { get; set; }
        //    public string Source { get; set; }
        //    public decimal Stock { get; set; }
        //    public string DocNo { get; set; }
        //    public decimal DocSeqNo { get; set; }
        //    public int No { get; set; }
        //}

        #endregion
    }
}
