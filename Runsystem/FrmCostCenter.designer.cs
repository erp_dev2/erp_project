﻿namespace RunSystem
{
    partial class FrmCostCenter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCostCenter));
            this.label8 = new System.Windows.Forms.Label();
            this.LueParent = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtCCName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCCCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueAssetCatCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnBOM = new System.Windows.Forms.Button();
            this.ChkIsCCPayrollJournalEnabled = new DevExpress.XtraEditors.CheckEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ChkAssetProjectInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkNotParentInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkCBPInd = new DevExpress.XtraEditors.CheckEdit();
            this.LueProfitCenter = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAssetCatCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkIsCCPayrollJournalEnabled.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetProjectInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNotParentInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCBPInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfitCenter.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(787, 0);
            this.panel1.Size = new System.Drawing.Size(70, 348);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Location = new System.Drawing.Point(0, 100);
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnCancel.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Location = new System.Drawing.Point(0, 80);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnSave.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Location = new System.Drawing.Point(0, 60);
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnDelete.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Location = new System.Drawing.Point(0, 40);
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnEdit.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Location = new System.Drawing.Point(0, 20);
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnInsert.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnFind.Size = new System.Drawing.Size(70, 20);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Location = new System.Drawing.Point(0, 120);
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnPrint.Size = new System.Drawing.Size(70, 20);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.TxtAcNo);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtCCName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtCCCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(787, 348);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(122, 56);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 14);
            this.label8.TabIndex = 18;
            this.label8.Text = "Parent";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueParent
            // 
            this.LueParent.EnterMoveNextControl = true;
            this.LueParent.Location = new System.Drawing.Point(169, 53);
            this.LueParent.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueParent.Name = "LueParent";
            this.LueParent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.Appearance.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueParent.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueParent.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueParent.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueParent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueParent.Properties.DropDownRows = 30;
            this.LueParent.Properties.MaxLength = 40;
            this.LueParent.Properties.NullText = "[Empty]";
            this.LueParent.Properties.PopupWidth = 500;
            this.LueParent.Size = new System.Drawing.Size(607, 20);
            this.LueParent.TabIndex = 19;
            this.LueParent.ToolTip = "F4 : Show/hide list";
            this.LueParent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueParent.EditValueChanged += new System.EventHandler(this.LueParent_EditValueChanged);
            // 
            // TxtCCName
            // 
            this.TxtCCName.EnterMoveNextControl = true;
            this.TxtCCName.Location = new System.Drawing.Point(169, 30);
            this.TxtCCName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtCCName.Name = "TxtCCName";
            this.TxtCCName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCCName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCName.Properties.Appearance.Options.UseFont = true;
            this.TxtCCName.Properties.MaxLength = 255;
            this.TxtCCName.Size = new System.Drawing.Size(607, 20);
            this.TxtCCName.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(58, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "Cost Center Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCCCode
            // 
            this.TxtCCCode.EnterMoveNextControl = true;
            this.TxtCCCode.Location = new System.Drawing.Point(169, 7);
            this.TxtCCCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtCCCode.Name = "TxtCCCode";
            this.TxtCCCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCCCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCCCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCCCode.Properties.MaxLength = 40;
            this.TxtCCCode.Size = new System.Drawing.Size(107, 20);
            this.TxtCCCode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(61, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Cost Center Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(88, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 20;
            this.label3.Text = "Profit Center";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(92, 102);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 22;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(169, 99);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.MaxLength = 40;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 500;
            this.LueDeptCode.Size = new System.Drawing.Size(607, 20);
            this.LueDeptCode.TabIndex = 23;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(169, 122);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 80;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(574, 20);
            this.TxtAcNo.TabIndex = 25;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(751, 121);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo.TabIndex = 26;
            this.BtnAcNo.ToolTip = "List Of COA\'s Account#";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(12, 125);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 14);
            this.label5.TabIndex = 24;
            this.label5.Text = "Depreciation Cost Account";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(169, 145);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 255;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(607, 20);
            this.TxtAcDesc.TabIndex = 27;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(279, 7);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(68, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.LueAcCode);
            this.panel4.Controls.Add(this.LueAssetCatCode);
            this.panel4.Controls.Add(this.Grd1);
            this.panel4.Controls.Add(this.BtnBOM);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 171);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(787, 177);
            this.panel4.TabIndex = 25;
            // 
            // LueAcCode
            // 
            this.LueAcCode.EnterMoveNextControl = true;
            this.LueAcCode.Location = new System.Drawing.Point(380, 44);
            this.LueAcCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueAcCode.Name = "LueAcCode";
            this.LueAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcCode.Properties.DropDownRows = 20;
            this.LueAcCode.Properties.NullText = "[Empty]";
            this.LueAcCode.Properties.PopupWidth = 500;
            this.LueAcCode.Size = new System.Drawing.Size(341, 20);
            this.LueAcCode.TabIndex = 30;
            this.LueAcCode.ToolTip = "F4 : Show/hide list";
            this.LueAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcCode.EditValueChanged += new System.EventHandler(this.LueAcCode_EditValueChanged);
            this.LueAcCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueAcCode_KeyDown);
            this.LueAcCode.Leave += new System.EventHandler(this.LueAcCode_Leave);
            // 
            // LueAssetCatCode
            // 
            this.LueAssetCatCode.EnterMoveNextControl = true;
            this.LueAssetCatCode.Location = new System.Drawing.Point(70, 45);
            this.LueAssetCatCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueAssetCatCode.Name = "LueAssetCatCode";
            this.LueAssetCatCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCatCode.Properties.Appearance.Options.UseFont = true;
            this.LueAssetCatCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCatCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAssetCatCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCatCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAssetCatCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCatCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAssetCatCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCatCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAssetCatCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAssetCatCode.Properties.DropDownRows = 20;
            this.LueAssetCatCode.Properties.NullText = "[Empty]";
            this.LueAssetCatCode.Properties.PopupWidth = 500;
            this.LueAssetCatCode.Size = new System.Drawing.Size(281, 20);
            this.LueAssetCatCode.TabIndex = 29;
            this.LueAssetCatCode.ToolTip = "F4 : Show/hide list";
            this.LueAssetCatCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAssetCatCode.EditValueChanged += new System.EventHandler(this.LueAssetCatCode_EditValueChanged);
            this.LueAssetCatCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueAssetCatCode_KeyDown);
            this.LueAssetCatCode.Leave += new System.EventHandler(this.LueAssetCatCode_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 23);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(787, 154);
            this.Grd1.TabIndex = 28;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // BtnBOM
            // 
            this.BtnBOM.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnBOM.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnBOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBOM.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOM.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnBOM.Location = new System.Drawing.Point(0, 0);
            this.BtnBOM.Name = "BtnBOM";
            this.BtnBOM.Size = new System.Drawing.Size(787, 23);
            this.BtnBOM.TabIndex = 26;
            this.BtnBOM.Text = "Asset Category and Chart of Account";
            this.BtnBOM.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnBOM.UseVisualStyleBackColor = false;
            // 
            // ChkIsCCPayrollJournalEnabled
            // 
            this.ChkIsCCPayrollJournalEnabled.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkIsCCPayrollJournalEnabled.Location = new System.Drawing.Point(345, 7);
            this.ChkIsCCPayrollJournalEnabled.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkIsCCPayrollJournalEnabled.Name = "ChkIsCCPayrollJournalEnabled";
            this.ChkIsCCPayrollJournalEnabled.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkIsCCPayrollJournalEnabled.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkIsCCPayrollJournalEnabled.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkIsCCPayrollJournalEnabled.Properties.Appearance.Options.UseBackColor = true;
            this.ChkIsCCPayrollJournalEnabled.Properties.Appearance.Options.UseFont = true;
            this.ChkIsCCPayrollJournalEnabled.Properties.Appearance.Options.UseForeColor = true;
            this.ChkIsCCPayrollJournalEnabled.Properties.Caption = "Used For Payroll Journal";
            this.ChkIsCCPayrollJournalEnabled.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkIsCCPayrollJournalEnabled.Size = new System.Drawing.Size(159, 22);
            this.ChkIsCCPayrollJournalEnabled.TabIndex = 12;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ChkAssetProjectInd);
            this.panel3.Controls.Add(this.ChkNotParentInd);
            this.panel3.Controls.Add(this.ChkCBPInd);
            this.panel3.Controls.Add(this.ChkIsCCPayrollJournalEnabled);
            this.panel3.Controls.Add(this.LueProfitCenter);
            this.panel3.Controls.Add(this.LueParent);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(787, 171);
            this.panel3.TabIndex = 24;
            // 
            // ChkAssetProjectInd
            // 
            this.ChkAssetProjectInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkAssetProjectInd.Location = new System.Drawing.Point(678, 8);
            this.ChkAssetProjectInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkAssetProjectInd.Name = "ChkAssetProjectInd";
            this.ChkAssetProjectInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkAssetProjectInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAssetProjectInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkAssetProjectInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkAssetProjectInd.Properties.Appearance.Options.UseFont = true;
            this.ChkAssetProjectInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkAssetProjectInd.Properties.Caption = "Asset Project";
            this.ChkAssetProjectInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAssetProjectInd.Size = new System.Drawing.Size(101, 22);
            this.ChkAssetProjectInd.TabIndex = 15;
            // 
            // ChkNotParentInd
            // 
            this.ChkNotParentInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkNotParentInd.Location = new System.Drawing.Point(591, 7);
            this.ChkNotParentInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkNotParentInd.Name = "ChkNotParentInd";
            this.ChkNotParentInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkNotParentInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkNotParentInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkNotParentInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkNotParentInd.Properties.Appearance.Options.UseFont = true;
            this.ChkNotParentInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkNotParentInd.Properties.Caption = "Not Parent";
            this.ChkNotParentInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkNotParentInd.Size = new System.Drawing.Size(87, 22);
            this.ChkNotParentInd.TabIndex = 14;
            // 
            // ChkCBPInd
            // 
            this.ChkCBPInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCBPInd.Location = new System.Drawing.Point(500, 7);
            this.ChkCBPInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCBPInd.Name = "ChkCBPInd";
            this.ChkCBPInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCBPInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCBPInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCBPInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCBPInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCBPInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCBPInd.Properties.Caption = "Use For CBP";
            this.ChkCBPInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCBPInd.Size = new System.Drawing.Size(94, 22);
            this.ChkCBPInd.TabIndex = 13;
            // 
            // LueProfitCenter
            // 
            this.LueProfitCenter.EnterMoveNextControl = true;
            this.LueProfitCenter.Location = new System.Drawing.Point(169, 76);
            this.LueProfitCenter.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueProfitCenter.Name = "LueProfitCenter";
            this.LueProfitCenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.Appearance.Options.UseFont = true;
            this.LueProfitCenter.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProfitCenter.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProfitCenter.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProfitCenter.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenter.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProfitCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProfitCenter.Properties.DropDownRows = 30;
            this.LueProfitCenter.Properties.MaxLength = 40;
            this.LueProfitCenter.Properties.NullText = "[Empty]";
            this.LueProfitCenter.Properties.PopupWidth = 500;
            this.LueProfitCenter.Size = new System.Drawing.Size(607, 20);
            this.LueProfitCenter.TabIndex = 21;
            this.LueProfitCenter.ToolTip = "F4 : Show/hide list";
            this.LueProfitCenter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProfitCenter.EditValueChanged += new System.EventHandler(this.LueProfitCenter_EditValueChanged);
            // 
            // FrmCostCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 348);
            this.Name = "FrmCostCenter";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueParent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAssetCatCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkIsCCPayrollJournalEnabled.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetProjectInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNotParentInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCBPInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfitCenter.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.LookUpEdit LueParent;
        private DevExpress.XtraEditors.TextEdit TxtCCName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtCCCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Panel panel4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Button BtnBOM;
        private DevExpress.XtraEditors.LookUpEdit LueAssetCatCode;
        private DevExpress.XtraEditors.LookUpEdit LueAcCode;
        internal DevExpress.XtraEditors.LookUpEdit LueProfitCenter;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.CheckEdit ChkCBPInd;
        private DevExpress.XtraEditors.CheckEdit ChkIsCCPayrollJournalEnabled;
        private DevExpress.XtraEditors.CheckEdit ChkNotParentInd;
        private DevExpress.XtraEditors.CheckEdit ChkAssetProjectInd;
    }
}