﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialRequestDeptStatusDlg4 : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptMaterialRequestDeptStatus mFrmParent;
        private string mSQL = string.Empty, mDNo = string.Empty, mDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMaterialRequestDeptStatusDlg4(FrmRptMaterialRequestDeptStatus FrmParent, string DNo, string DocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDNo = DNo;
            mDocNo = DocNo;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            SetGrd(); ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDept, TxtItCode, TxtItName, TxtQty
                    }, true);
                    TxtDept.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDept, TxtItCode, TxtItName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
               TxtQty
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Recv Vendor Number",

                        //1-3
                        "DNo", 
                        "Quantity",
                        ""
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            Sm.GrdColInvisible(Grd1, new[] { 1 });
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                SQL.AppendLine("Select X.DocNoMR, X.DNoMR, X.DocNoRecv, X.DNoRecv, Round(X.QtyRecv, 2) QtyRecv from ( ");
                SQL.AppendLine("Select A.DocNo As DocNoMR, A.DNo As DNoMR, D.DocNo As DocNoRecv, D.Dno As DNoRecv, D.Qty As QtyRecv ");
                SQL.AppendLine("from TblMaterialRequestDtl A ");
                SQL.AppendLine("Left Join TblPORequestDtl B On A.DocNo=B.MaterialRequestDocno And B.MaterialRequestDNo=A.DNo And B.CancelInd='N' ");
                SQL.AppendLine("Left Join TblPODtl C On B.DocNo=C.PORequestDocNo And C.PORequestDNo=B.DNo And C.CancelInd='N' ");
                SQL.AppendLine("Inner join TblRecvVdDtl D On C.DocNo=D.PODocNo And D.PODNo=C.DNo And D.CancelInd='N' ");
                SQL.AppendLine("Where A.Status='A' And A.CancelInd='N' ");
                SQL.AppendLine(")X ");
 
                mSQL = SQL.ToString();

                string Filter = "Where X.DocNoMR= '" + mDocNo + "' And X.DNoMR = '" + mDNo + "' ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " ",
                        new string[]
                        {
                            //0
                            "DocNoRecv",
                            //1-2
                            "DNoRecv", "QtyRecv"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);

                        }, false, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

      

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptMaterialRequestDeptStatusDlg5(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 0));
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptMaterialRequestDeptStatusDlg5(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 0));
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtDocNo.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.TxtQty.EditValue = Sm.GetGrdDec(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion
    }
}
