﻿#region Update
/*
    25/02/2019 [TKG] New reporting
    27/02/2019 [WED] tambah untuk type BC23, BC25, dan BC41
    12/03/2019 [WED] tambah untuk type BC27, isi sama seperti BC40
    18/03/2019 [WED] rombak reporting. type tidak mandatory, isinya : BC23, BC40, BC262, BC27
    01/04/2019 [WED] tambah checkbox LueType
    15/04/2019 [WED] dibedakan antara BC27 dan BC40
    18/04/2019 [DITA] Lock page setting print out baru laporan pemasukan (kawasan brikat)
    20/06/2019 [WED] tambah BC23 berdasarkan KBSubmissionNo
    20/06/2019 [WED] uraian barang menggunakan item group name
    01/08/2019 [TKG] ubah menggunakan customs document code untuk membedakan jenis data kawasan berikat
    24/10/2019 [DITA/MAI] Data Bc 262 yang sudah terinput dengan receiving item from other warehouse without do belum ketarik ke laporan pemasukkan
    13/11/2019 [WED/MAI] supplier di left join ke recvvd
    18/11/2019 [VIN/MAI] Laporan Pemasukan supplier dari transaksi warehouse ambil dari nama gudang nya  
    25/11/2019 [WED/MAI] untuk yg RecvVd dan RecvVd2, dokumen penerimaan ambil dari VdDONo. untuk yg RecvWhs, contract nya ambil dari KBPLNo
    24/08/2020 [DITA/MAI] tambah BC27 untuk recveiving from other whs without do (recvwhs2)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
using DevExpress.XtraEditors;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmRptKB_1 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mStartDt = string.Empty, mEndDt = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptKB_1(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueType(ref LueType);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, T2.ItGrpName ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            SQL.AppendLine("Select 'BC23' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.VdDONo KBContractNo, A.DocDt, C.VdName As Name, D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status In ('O', 'A') And B.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("    And A.KBRegistrationNo Is Not Null ");
            //SQL.AppendLine("    And Substr(A.KBSubmissionNo, 5, 2) = '23' ");
            SQL.AppendLine("    And A.CustomsDocCode Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode= '23' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC40' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.VdDONo KBContractNo, A.DocDt, C.VdName As Name, D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status In ('O', 'A') And B.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            //SQL.AppendLine("    And A.KBContractNo Is Not Null ");
            //SQL.AppendLine("    And A.POInd='Y' ");
            //SQL.AppendLine("    And Substr(A.KBSubmissionNo, 5, 2) = '40' ");
            SQL.AppendLine("    And A.CustomsDocCode Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode= '40' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC27' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.VdDONo KBContractNo, A.DocDt, C.VdName As Name, D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status In ('O', 'A') And B.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CustomsDocCode Is Not Null ");
            SQL.AppendLine("    And A.CustomsDocCode= '27' ");
            //SQL.AppendLine("    And A.KBContractNo Is Not Null ");
            //SQL.AppendLine("    And A.POInd='Y' ");
            //SQL.AppendLine("    And Substr(A.KBSubmissionNo, 5, 2) = '27' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source=E.Source ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select 'BC262' As DocType, A.KBRegistrationNo, A.KBRegistrationDt, A.KBPLNo KBContractNo, A.DocDt, F.WhsName as Name, D.ItGrpCode, D.ItName, D.InventoryUomCode, B.Qty, E.CurCode, E.UPrice*B.Qty As Amt ");
            SQL.AppendLine("From TblRecvWhs4Hdr A  ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N'  ");
            SQL.AppendLine("    And A.KBRegistrationNo Is Not Null  ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo = C.DNo And C.CancelInd='N'  ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice E On B.Source = E.Source ");
            SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode2 = F.WhsCode ");
            //SQL.AppendLine("Inner Join TblRecvVdDtl F On B.BatchNo = F.BatchNo ");
            //SQL.AppendLine("Inner Join TblRecvVdHdr G On F.DocNo = G.DocNo ");
            //SQL.AppendLine("Inner Join TblVendor H On G.VdCode = H.VdCode ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine(" Select 'BC262' As DocType, A.KBRegistrationNo, A.KBRegistrationDt,  A.KBPLNo KBContractNo, A.DocDt, E.WhsName as Name, C.ItGrpCode, C.ItName, C.InventoryUomCode, B.Qty, D.CurCode, D.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblRecvWhs2Hdr A  ");
            SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("     And A.KBRegistrationNo Is Not Null  ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CustomsDocCode= '262' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice D On B.Source = D.Source ");
            SQL.AppendLine("Inner Join TblWarehouse E On A.WhsCode2 = E.WhsCode ");
          //  SQL.AppendLine("Inner Join TblRecvVdDtl F On B.BatchNo = F.BatchNo ");
          //  SQL.AppendLine("Inner Join TblRecvVdHdr G On F.DocNo = G.DocNo ");
          //  SQL.AppendLine("Inner Join TblVendor H On G.VdCode = H.VdCode ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine(" Select 'BC27' As DocType, A.KBRegistrationNo, A.KBRegistrationDt,  A.KBPLNo KBContractNo, A.DocDt, E.WhsName as Name, C.ItGrpCode, C.ItName, C.InventoryUomCode, B.Qty, D.CurCode, D.UPrice*B.Qty As Amt  ");
            SQL.AppendLine("From TblRecvWhs2Hdr A  ");
            SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("     And A.KBRegistrationNo Is Not Null  ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And A.CustomsDocCode= '27' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode  ");
            SQL.AppendLine("Inner Join TblStockPrice D On B.Source = D.Source ");
            SQL.AppendLine("Inner Join TblWarehouse E On A.WhsCode2 = E.WhsCode ");
            //  SQL.AppendLine("Inner Join TblRecvVdDtl F On B.BatchNo = F.BatchNo ");
            //  SQL.AppendLine("Inner Join TblRecvVdHdr G On F.DocNo = G.DocNo ");
            //  SQL.AppendLine("Inner Join TblVendor H On G.VdCode = H.VdCode ");
            

            SQL.AppendLine(") T ");
            SQL.AppendLine("Left Join TblItemGroup T2 On T.ItGrpCode = T2.ItGrpCode ");
           
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 1;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;
            Grd1.Header.Cells[1, 1].Value = "Dokumen Pabean";
            Grd1.Header.Cells[1, 1].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 1].SpanCols = 3;
            Grd1.Header.Cells[0, 1].Value = "Jenis";
            Grd1.Header.Cells[0, 1].SpanRows = 1;
            Grd1.Header.Cells[0, 2].Value = "Nomor";
            Grd1.Header.Cells[0, 2].SpanRows = 1;
            Grd1.Header.Cells[0, 3].Value = "Tanggal";
            Grd1.Header.Cells[0, 3].SpanRows = 1;
            Grd1.Header.Cells[1, 4].Value = "Bukti Penerimaan Barang";
            Grd1.Header.Cells[1, 4].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 4].SpanCols = 2;
            Grd1.Header.Cells[0, 4].Value = "Nomor";
            Grd1.Header.Cells[0, 4].SpanRows = 1;
            Grd1.Header.Cells[0, 5].Value = "Tanggal";
            Grd1.Header.Cells[0, 5].SpanRows = 1;
            Grd1.Header.Cells[0, 6].Value = "Supplier";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;
            Grd1.Header.Cells[0, 7].Value = "Kode Barang";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;
            Grd1.Header.Cells[0, 8].Value = "Uraian Barang";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;
            Grd1.Header.Cells[0, 9].Value = "Satuan";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 9].SpanRows = 2;
            Grd1.Header.Cells[0, 10].Value = "Jumlah";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].SpanRows = 2;
            Grd1.Header.Cells[0, 11].Value = "Valas";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;
            Grd1.Header.Cells[0, 12].Value = "Nilai Barang";
            Grd1.Header.Cells[0, 12].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 12].SpanRows = 2;
            
            Sm.GrdFormatDate(Grd1, new int[] { 3, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12 }, 0);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void ShowData()
        {
            mStartDt = string.Empty; mEndDt = string.Empty;
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                mStartDt = Sm.GetDte(DteDocDt1).Substring(0, 8); 
                mEndDt = Sm.GetDte(DteDocDt2).Substring(0, 8);

                Sm.CmParamDt(ref cm, "@DocDt1", mStartDt);
                Sm.CmParamDt(ref cm, "@DocDt2", mEndDt);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.KBContractNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtRegistrationNo.Text, "T.KBRegistrationNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "T.DocType", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(Sm.GetLue(LueType)) + Filter + " Order By T.DocDt, T.KBContractNo, T.ItGrpCode, T.ItName; ",
                    new string[]
                    {
                        //0
                        "DocType", 

                        //1-5
                        "KBRegistrationNo", "KBRegistrationDt", "KBContractNo", "DocDt", "Name", 

                        //6-10
                        "ItGrpCode", "ItGrpName", "InventoryUomCode", "Qty", "CurCode", 

                        //11
                        "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                if (Grd1.Rows.Count == 0 || mStartDt.Length==0)
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }

                iGSubtotalManager.ForeColor = Color.Black;

                string StartDt = string.Concat(Sm.Right(mStartDt, 2), "/", mStartDt.Substring(4, 2), "/", Sm.Left(mStartDt, 4));
                string EndDt = string.Concat(Sm.Right(mEndDt, 2), "/", mEndDt.Substring(4, 2), "/", Sm.Left(mEndDt, 4));
                string mTitle = string.Empty;

                mTitle = "LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC23")
                //    mTitle = "LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC25")
                //    mTitle = "LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC40" || Sm.GetLue(LueType) == "BC27")
                //    mTitle = "LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                //if (Sm.GetLue(LueType) == "BC41")
                //    mTitle = "LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN" + Environment.NewLine + Gv.CompanyName;

                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private string MonthNameIDN(string Mth)
        {
            string[] mMonthNames = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Novemeber", "Desember" };
            return mMonthNames[Int32.Parse(Mth) - 1];
        }

        private void SetLueType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'BC23' As Col1, 'BC 23' As Col2 ");
            SQL.AppendLine("Union All Select 'BC27' Col1, 'BC 27' Col2 ");
            SQL.AppendLine("Union All Select 'BC40' Col1, 'BC 40' Col2 ");
            SQL.AppendLine("Union All Select 'BC262' Col1, 'BC 262' Col2; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        private void ParPrint()
       {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<KB_1>();
            var ldtl = new List<KB_1Dtl>();

            string[] TableName = { "KB_1", "KB_1Dtl"};
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
          //  DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region Header
            
            l.Add(new KB_1()
            {
                CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                CompanyName = Gv.CompanyName,
                Periode1 = DteDocDt1.Text.Replace("/", "-"),
                Periode2 = DteDocDt2.Text.Replace("/", "-"),
                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
            });

            myLists.Add(l);            
            
            #endregion

            #region Detail
           
            int nomor = 0;
            for(int i = 0; i<Grd1.Rows.Count;i++)
            {
                nomor = nomor + 1;
                ldtl.Add(new KB_1Dtl()
                {
                    No = nomor,
                    Jenis = Sm.GetGrdStr(Grd1, i, 1),
                    Nomor = Sm.GetGrdStr(Grd1, i, 2),
                    Tanggal1  = Sm.GetGrdText(Grd1, i, 3),
                    Nomor2 = Sm.GetGrdStr(Grd1, i, 4),
                    Tanggal2 = Sm.GetGrdText(Grd1, i, 5),
                    Supplier  = Sm.GetGrdStr(Grd1, i, 6),
                    ItemCode = Sm.GetGrdStr(Grd1, i, 7),
                    UraianBarang = Sm.GetGrdStr(Grd1, i, 8),
                    Satuan = Sm.GetGrdStr(Grd1, i, 9),
                    Jumlah = Sm.GetGrdDec(Grd1, i, 10),
                    Valas = Sm.GetGrdStr(Grd1, i, 11),
                    NilaiBarang =Sm.GetGrdDec(Grd1, i, 12),
                   
                });
     
            }
            myLists.Add(ldtl);
            #endregion

            Sm.PrintReport("KB_1", myLists, TableName, false);
               
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Contract#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
            //Grd1.Header.Cells[0, 6].Value = "Supplier";
            //if (Sm.GetLue(LueType).Length > 0)
            //{
            //    if (Sm.GetLue(LueType) == "BC23" || Sm.GetLue(LueType) == "BC40")
            //    {
            //        Grd1.Header.Cells[0, 6].Value = "Supplier";
            //    }
            //    else
            //    {
            //        Grd1.Header.Cells[0, 6].Value = "Customer";
            //    }
            //}
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void TxtRegistrationNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRegistrationNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Registration#");
        }

        #endregion

        #endregion

        #region Class
        private class KB_1
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string Periode1 { get; set; }
            public string Periode2 { get; set; }
            public string PrintBy { get; set; }
        }
        private class KB_1Dtl
        {
            public int No { get; set; }
            public string Jenis { get; set; }
            public string Nomor { get; set; }
            public string Tanggal1 { get; set; }
            public string Nomor2 { get; set; }
            public string Tanggal2 { get; set; }
            public string Supplier { get; set; }
            public string ItemCode { get; set; }
            public string UraianBarang { get; set; }
            public string Satuan { get; set; }
            public decimal Jumlah { get; set; }
            public string Valas { get; set; }
            public decimal NilaiBarang { get; set; }
        }
        #endregion
    }
}