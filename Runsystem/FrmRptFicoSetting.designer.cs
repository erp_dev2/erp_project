﻿namespace RunSystem
{
    partial class FrmRptFicoSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptFicoSetting));
            this.TxtRptFicoName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtRptFicoCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.BtnSourceCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSourceCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtSourceCode = new DevExpress.XtraEditors.TextEdit();
            this.LblSourceCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.LueAlignCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueLineCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueFontCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BtnRefreshData = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRptFicoName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRptFicoCode.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSourceCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAlignCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLineCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // TxtRptFicoName
            // 
            this.TxtRptFicoName.EnterMoveNextControl = true;
            this.TxtRptFicoName.Location = new System.Drawing.Point(97, 27);
            this.TxtRptFicoName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRptFicoName.Name = "TxtRptFicoName";
            this.TxtRptFicoName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRptFicoName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRptFicoName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRptFicoName.Properties.Appearance.Options.UseFont = true;
            this.TxtRptFicoName.Properties.MaxLength = 200;
            this.TxtRptFicoName.Size = new System.Drawing.Size(350, 20);
            this.TxtRptFicoName.TabIndex = 12;
            this.TxtRptFicoName.Validated += new System.EventHandler(this.TxtRptFicoName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(13, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Report Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRptFicoCode
            // 
            this.TxtRptFicoCode.EnterMoveNextControl = true;
            this.TxtRptFicoCode.Location = new System.Drawing.Point(97, 5);
            this.TxtRptFicoCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRptFicoCode.Name = "TxtRptFicoCode";
            this.TxtRptFicoCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRptFicoCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRptFicoCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRptFicoCode.Properties.Appearance.Options.UseFont = true;
            this.TxtRptFicoCode.Properties.MaxLength = 16;
            this.TxtRptFicoCode.Size = new System.Drawing.Size(168, 20);
            this.TxtRptFicoCode.TabIndex = 10;
            this.TxtRptFicoCode.Validated += new System.EventHandler(this.TxtRptFicoCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(16, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Report Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BtnSourceCode2);
            this.panel3.Controls.Add(this.BtnSourceCode);
            this.panel3.Controls.Add(this.TxtSourceCode);
            this.panel3.Controls.Add(this.LblSourceCode);
            this.panel3.Controls.Add(this.LueSiteCode);
            this.panel3.Controls.Add(this.LblSiteCode);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.TxtRptFicoCode);
            this.panel3.Controls.Add(this.TxtRptFicoName);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 97);
            this.panel3.TabIndex = 8;
            // 
            // BtnSourceCode2
            // 
            this.BtnSourceCode2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSourceCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSourceCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSourceCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSourceCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSourceCode2.Appearance.Options.UseBackColor = true;
            this.BtnSourceCode2.Appearance.Options.UseFont = true;
            this.BtnSourceCode2.Appearance.Options.UseForeColor = true;
            this.BtnSourceCode2.Appearance.Options.UseTextOptions = true;
            this.BtnSourceCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSourceCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSourceCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSourceCode2.Image")));
            this.BtnSourceCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSourceCode2.Location = new System.Drawing.Point(744, 5);
            this.BtnSourceCode2.Name = "BtnSourceCode2";
            this.BtnSourceCode2.Size = new System.Drawing.Size(24, 21);
            this.BtnSourceCode2.TabIndex = 20;
            this.BtnSourceCode2.ToolTip = "Show KPI Document";
            this.BtnSourceCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSourceCode2.ToolTipTitle = "Run System";
            this.BtnSourceCode2.Click += new System.EventHandler(this.BtnSourceCode2_Click);
            // 
            // BtnSourceCode
            // 
            this.BtnSourceCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSourceCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSourceCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSourceCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSourceCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSourceCode.Appearance.Options.UseBackColor = true;
            this.BtnSourceCode.Appearance.Options.UseFont = true;
            this.BtnSourceCode.Appearance.Options.UseForeColor = true;
            this.BtnSourceCode.Appearance.Options.UseTextOptions = true;
            this.BtnSourceCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSourceCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSourceCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnSourceCode.Image")));
            this.BtnSourceCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSourceCode.Location = new System.Drawing.Point(724, 5);
            this.BtnSourceCode.Name = "BtnSourceCode";
            this.BtnSourceCode.Size = new System.Drawing.Size(24, 21);
            this.BtnSourceCode.TabIndex = 19;
            this.BtnSourceCode.ToolTip = "Find KPI Document";
            this.BtnSourceCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSourceCode.ToolTipTitle = "Run System";
            this.BtnSourceCode.Click += new System.EventHandler(this.BtnSourceCode_Click);
            // 
            // TxtSourceCode
            // 
            this.TxtSourceCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtSourceCode.EnterMoveNextControl = true;
            this.TxtSourceCode.Location = new System.Drawing.Point(554, 5);
            this.TxtSourceCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSourceCode.Name = "TxtSourceCode";
            this.TxtSourceCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSourceCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSourceCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSourceCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSourceCode.Properties.MaxLength = 30;
            this.TxtSourceCode.Size = new System.Drawing.Size(167, 20);
            this.TxtSourceCode.TabIndex = 18;
            // 
            // LblSourceCode
            // 
            this.LblSourceCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblSourceCode.AutoSize = true;
            this.LblSourceCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSourceCode.ForeColor = System.Drawing.Color.Black;
            this.LblSourceCode.Location = new System.Drawing.Point(504, 8);
            this.LblSourceCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSourceCode.Name = "LblSourceCode";
            this.LblSourceCode.Size = new System.Drawing.Size(45, 14);
            this.LblSourceCode.TabIndex = 17;
            this.LblSourceCode.Text = "Source";
            this.LblSourceCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(97, 49);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 350;
            this.LueSiteCode.Size = new System.Drawing.Size(350, 20);
            this.LueSiteCode.TabIndex = 14;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(65, 52);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 13;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(45, 74);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 15;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(97, 71);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(350, 20);
            this.MeeRemark.TabIndex = 16;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 97);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(772, 376);
            this.Tc1.TabIndex = 21;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.LueAlignCode);
            this.Tp1.Controls.Add(this.LueLineCode);
            this.Tp1.Controls.Add(this.LueFontCode);
            this.Tp1.Controls.Add(this.Grd1);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 348);
            this.Tp1.Text = "List of Setting";
            // 
            // LueAlignCode
            // 
            this.LueAlignCode.EnterMoveNextControl = true;
            this.LueAlignCode.Location = new System.Drawing.Point(420, 50);
            this.LueAlignCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueAlignCode.Name = "LueAlignCode";
            this.LueAlignCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAlignCode.Properties.Appearance.Options.UseFont = true;
            this.LueAlignCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAlignCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAlignCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAlignCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAlignCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAlignCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAlignCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAlignCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAlignCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAlignCode.Properties.DropDownRows = 20;
            this.LueAlignCode.Properties.NullText = "[Empty]";
            this.LueAlignCode.Properties.PopupWidth = 500;
            this.LueAlignCode.Size = new System.Drawing.Size(171, 20);
            this.LueAlignCode.TabIndex = 25;
            this.LueAlignCode.ToolTip = "F4 : Show/hide list";
            this.LueAlignCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAlignCode.EditValueChanged += new System.EventHandler(this.LueAlignCode_EditValueChanged);
            this.LueAlignCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueAlignCode_KeyDown);
            this.LueAlignCode.Leave += new System.EventHandler(this.LueAlignCode_Leave);
            // 
            // LueLineCode
            // 
            this.LueLineCode.EnterMoveNextControl = true;
            this.LueLineCode.Location = new System.Drawing.Point(420, 20);
            this.LueLineCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLineCode.Name = "LueLineCode";
            this.LueLineCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLineCode.Properties.Appearance.Options.UseFont = true;
            this.LueLineCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLineCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLineCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLineCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLineCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLineCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLineCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLineCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLineCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLineCode.Properties.DropDownRows = 20;
            this.LueLineCode.Properties.NullText = "[Empty]";
            this.LueLineCode.Properties.PopupWidth = 500;
            this.LueLineCode.Size = new System.Drawing.Size(171, 20);
            this.LueLineCode.TabIndex = 24;
            this.LueLineCode.ToolTip = "F4 : Show/hide list";
            this.LueLineCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLineCode.EditValueChanged += new System.EventHandler(this.LueLineCode_EditValueChanged);
            this.LueLineCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueLineCode_KeyDown);
            this.LueLineCode.Leave += new System.EventHandler(this.LueLineCode_Leave);
            // 
            // LueFontCode
            // 
            this.LueFontCode.EnterMoveNextControl = true;
            this.LueFontCode.Location = new System.Drawing.Point(191, 21);
            this.LueFontCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueFontCode.Name = "LueFontCode";
            this.LueFontCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontCode.Properties.Appearance.Options.UseFont = true;
            this.LueFontCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFontCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFontCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontCode.Properties.DropDownRows = 20;
            this.LueFontCode.Properties.NullText = "[Empty]";
            this.LueFontCode.Properties.PopupWidth = 500;
            this.LueFontCode.Size = new System.Drawing.Size(171, 20);
            this.LueFontCode.TabIndex = 23;
            this.LueFontCode.ToolTip = "F4 : Show/hide list";
            this.LueFontCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFontCode.EditValueChanged += new System.EventHandler(this.LueFontCode_EditValueChanged);
            this.LueFontCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueFontCode_KeyDown);
            this.LueFontCode.Leave += new System.EventHandler(this.LueFontCode_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 348);
            this.Grd1.TabIndex = 22;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd1_RequestCellToolTipText);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.Grd2);
            this.Tp2.Controls.Add(this.panel4);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 108);
            this.Tp2.Text = "List of COA\'s Account";
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.GroupBox.Visible = true;
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 31);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowMode = true;
            this.Grd2.RowModeHasCurCell = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 77);
            this.Grd2.TabIndex = 24;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(766, 31);
            this.panel4.TabIndex = 22;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.BtnRefreshData);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(672, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(94, 31);
            this.panel5.TabIndex = 33;
            // 
            // BtnRefreshData
            // 
            this.BtnRefreshData.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRefreshData.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefreshData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefreshData.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnRefreshData.Appearance.Options.UseBackColor = true;
            this.BtnRefreshData.Appearance.Options.UseFont = true;
            this.BtnRefreshData.Appearance.Options.UseForeColor = true;
            this.BtnRefreshData.Appearance.Options.UseTextOptions = true;
            this.BtnRefreshData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefreshData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRefreshData.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BtnRefreshData.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRefreshData.Location = new System.Drawing.Point(0, 4);
            this.BtnRefreshData.Name = "BtnRefreshData";
            this.BtnRefreshData.Size = new System.Drawing.Size(94, 27);
            this.BtnRefreshData.TabIndex = 23;
            this.BtnRefreshData.Text = "Refresh Data";
            this.BtnRefreshData.ToolTip = "Refresh Data";
            this.BtnRefreshData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRefreshData.ToolTipTitle = "Run System";
            this.BtnRefreshData.Click += new System.EventHandler(this.BtnRefreshData_Click);
            // 
            // FrmRptFicoSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptFicoSetting";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtRptFicoName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRptFicoCode.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSourceCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueAlignCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLineCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.TextEdit TxtRptFicoCode;
        private DevExpress.XtraEditors.TextEdit TxtRptFicoName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        public DevExpress.XtraEditors.SimpleButton BtnRefreshData;
        private DevExpress.XtraEditors.LookUpEdit LueFontCode;
        private DevExpress.XtraEditors.LookUpEdit LueLineCode;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label LblSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueAlignCode;
        public DevExpress.XtraEditors.SimpleButton BtnSourceCode2;
        public DevExpress.XtraEditors.SimpleButton BtnSourceCode;
        protected internal DevExpress.XtraEditors.TextEdit TxtSourceCode;
        private System.Windows.Forms.Label LblSourceCode;
    }
}