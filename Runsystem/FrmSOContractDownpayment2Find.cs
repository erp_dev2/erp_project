﻿#region Update
/*
    12/03/2020 [TKG/IMS] New Application
    12/06/2020 [DITA/IMS] rombak termin dp 2
    03/12/2020 [DITA/IMS] rombak find sales invoice for project
    17/12/2020 [DITA/IMS] Bug saat refresh setelah penambahan kolom status
    01/02/2021 [WED/IMS] tarik DO Verify dari DO Customer
    03/05/2021 [VIN/IMS] tambah localdocno
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContractDownpayment2Find : RunSystem.FrmBase2
    {
        #region Field

        private string mSQL = string.Empty;
        private FrmSOContractDownpayment2 mFrmParent;

        #endregion

        #region Constructor

        public FrmSOContractDownpayment2Find(FrmSOContractDownpayment2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("A.SOContractDocNo, B.PONo, C.CtName, D.PtName, K.CurName, ");
            SQL.AppendLine("F.Amt, F.TerminDP, F.TerminDP2, F.TaxAmt, F.TaxAmt2, F.AfterTax , ");
            SQL.AppendLine("E.VoucherDocNo, G.ItCodeInternal, G.ItName, F.Specification, J.ProjectCode, J.Projectname, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr A  ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo  ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode  ");
            SQL.AppendLine("Left Join TblPaymentTerm D On B.PtCode=D.PtCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(T3.VoucherRequestDocNo) VoucherRequestDocNo, GROUP_CONCAT(T4.DocNo) VoucherDocNo ");
            SQL.AppendLine("    FROM TblSOContractDownpayment2Hdr T1 ");
            SQL.AppendLine("    INNER JOIN TblIncomingPaymentDtl T2 ON T1.DocNo = T2.InvoiceDocNo AND InvoiceType = '7' ");
            SQL.AppendLine("    INNER JOIN TblIncomingPaymentHdr T3 ON T2.DocNo = T3.DocNo AND T3.CancelInd = 'N' AND T3.STATUS IN('O', 'A') ");
            SQL.AppendLine("    LEFT JOIN TblVoucherHdr T4 ON T3.VoucherRequestDocNo = T4.VoucherRequestDocNo AND T4.CancelInd = 'N' ");
            SQL.AppendLine("    GROUP BY T1.DocNo ");
            SQL.AppendLine(") E ON A.DocNo = E.DocNo ");
            SQL.AppendLine("LEFT JOIN  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT A.DocNo , D.ItCode , B.Amt, B.TerminDP, B.TerminDP2, B.TaxAmt, B.TaxAmt2, B.AfterTax , G.Remark Specification ");
            SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A ");
            SQL.AppendLine("    INNER JOIN tblsocontractdownpayment2dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyHdr C On B.DOCtVerifyDocNo=C.DocNo  ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl D On B.DOCtVerifyDocNo=D.DocNo And B.DOCtVerifyDNo=D.DNo  ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.DNo, T3.SOContractDocNo, T3.SOContractDNo ");
            SQL.AppendLine("        From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.DOCt2DocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl T3 On T1.DOCt2DocNo = T3.DocNo And T2.DOCt2DNo = T3.DNo ");
            SQL.AppendLine("            And T3.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.DNo, T3.SOContractDocNo, T4.SOContractDNo ");
            SQL.AppendLine("        From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr T3 On T1.DOCtDocNo = T3.DocNo ");
            SQL.AppendLine("            And T3.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtDtl T4 On T3.DocNo = T4.DocNo And T2.DOCtDNo = T4.DNo ");
            SQL.AppendLine("    ) F On D.DocNo = F.DocNo And D.DNo = F.DNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl G On F.SOContractDocNo = G.DocNo And F.SOContractDNo = G.DNo ");
            #region Old Code
            //SQL.AppendLine("   Left Join TblDOCt2Hdr F On C.DOCt2DocNo=F.DocNo  ");
            //SQL.AppendLine("   Left Join  ");
            //SQL.AppendLine("   (  ");
            //SQL.AppendLine("       Select T1.DocNo, T2.DNo, T5.ItCode, T5.Remark Specification ");
            //SQL.AppendLine("       From TblDOCt2Hdr T1  ");
            //SQL.AppendLine("       Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo  ");
            //SQL.AppendLine("       Inner Join TblDRHdr T3 On T1.DRDocNo = T3.DocNo  ");
            //SQL.AppendLine("       Inner Join TblDRDtl T4 On T3.DocNo  ");
            //SQL.AppendLine("       Inner Join TblSOContractDtl T5 On T4.SODocNo = T5.DocNo And T4.SODNo = T5.DNo  ");  
            //SQL.AppendLine("      Group By T1.DocNo, T2.DNo, T5.ItCode  ");
            //SQL.AppendLine("   ) G ON D.DOCt2DocNo = G.DocNo And D.DOCt2DNo = G.DNo And D.ItCode = G.ItCode  ");
            #endregion

            SQL.AppendLine("   UNION ALL ");

            SQL.AppendLine("   SELECT A.DocNo , D.ItCode, B.Amt, B.TerminDP, B.TerminDP2, B.TaxAmt, B.TaxAmt2, B.AfterTax, G.Remark Specification  ");
            SQL.AppendLine("   FROM tblsocontractdownpayment2hdr A ");
            SQL.AppendLine("   INNER JOIN tblsocontractdownpayment2dtl2 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("   Inner Join TblDOCtVerifyHdr C On B.DOCtVerifyDocNo=C.DocNo  ");
            SQL.AppendLine("   Inner Join TblDOCtVerifyDtl D On B.DOCtVerifyDocNo=D.DocNo And B.DOCtVerifyDNo=D.DNo  ");
            SQL.AppendLine("   Inner Join TblItem E On D.ItCode=E.ItCode ");
            #region Old Code
            //SQL.AppendLine("   Left Join TblDOCt2Hdr F On C.DOCt2DocNo=F.DocNo  ");
            //SQL.AppendLine("   Left Join  ");
            //SQL.AppendLine("    (  ");
            //SQL.AppendLine("       Select T1.DocNo, T2.DNo, T5.ItCode, T5.Remark Specification  ");
            //SQL.AppendLine("       From TblDOCt2Hdr T1  ");
            //SQL.AppendLine("       Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo  ");
            //SQL.AppendLine("       Inner Join TblDRHdr T3 On T1.DRDocNo = T3.DocNo  ");
            //SQL.AppendLine("       Inner Join TblDRDtl T4 On T3.DocNo  ");
            //SQL.AppendLine("      Inner Join TblSOContractDtl T5 On T4.SODocNo = T5.DocNo And T4.SODNo = T5.DNo  ");
            //SQL.AppendLine("       Group By T1.DocNo, T2.DNo, T5.ItCode  ");
            //SQL.AppendLine("   ) G On D.DOCt2DocNo = G.DocNo And D.DOCt2DNo = G.DNo And D.ItCode = G.ItCode  ");
            #endregion

            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.DNo, T3.SOContractDocNo, T3.SOContractDNo ");
            SQL.AppendLine("        From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.DOCt2DocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl T3 On T1.DOCt2DocNo = T3.DocNo And T2.DOCt2DNo = T3.DNo ");
            SQL.AppendLine("            And T3.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T2.DNo, T3.SOContractDocNo, T4.SOContractDNo ");
            SQL.AppendLine("        From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("        Inner Join TblDOCtVerifyDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr T3 On T1.DOCtDocNo = T3.DocNo ");
            SQL.AppendLine("            And T3.SOContractDocNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtDtl T4 On T3.DocNo = T4.DocNo And T2.DOCtDNo = T4.DNo ");
            SQL.AppendLine("    ) F On D.DocNo = F.DocNo And D.DNo = F.DNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl G On F.SOContractDocNo = G.DocNo And F.SOContractDNo = G.DNo ");

            SQL.AppendLine(") F ON A.DocNo = F.DocNo ");
            SQL.AppendLine("INNER JOIN TblItem G ON F.ItCode = G.ItCode ");
            SQL.AppendLine("Inner Join TblBOQHdr H On B.BOQDocNo=H.DocNo  ");
            SQL.AppendLine("Inner Join TblLOphdr I On H.LOPDocNo=I.DocNO ");
            SQL.AppendLine("LEFT JOIN TblProjectGroup J ON I.PGCode = J.PGCode ");
            SQL.AppendLine("LEFT JOIN TblCurrency K ON A.CurCode = K.CurCode  ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "Cancel",
                    "",
                    "SO Contract#",

                    //6-10
                    "Customer PO#",
                    "Customer",
                    "Term of Payment",
                    "Currency",
                    "Amount",

                    //11-15
                    "Downpayment",
                    "Total Amount",
                    "Tax 1",
                    "Tax 2",
                    "Invoice Amount",

                    //16-20
                    "",
                    "Voucher#",
                    "Local Code",
                    "Item's Name",
                    "Specification",

                    //21-25
                    "Project Code",
                    "Project Name",
                    "Remark",
                    "Created By",
                    "Created Date",

                    //26-30
                    "Created Time", 
                    "Last Updated By", 
                    "Last Updated Date",
                    "Last Updated Time",
                    "Local Document#",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    170, 100, 80, 20, 130, 
                    
                    //6-10
                    100, 200, 100, 100, 130,

                    //11-15
                    130, 130, 130, 130, 130, 

                    //16-20
                    20, 180, 100, 150, 200,

                    //21-25
                    100, 180, 200, 100, 100,

                    //26-29
                    100, 100, 100, 100, 170
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 4, 16 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 25, 28 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, 0);
            Sm.GrdFormatTime(Grd1, new int[] { 26, 29 });
            Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27, 28, 29 }, false);
            Grd1.Cols[30].Move(3);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 24, 25, 26, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "SOContractDocNo", "PONo", "CtName", 
                            
                            //6-10
                             "PtName", "CurName", "Amt", "TerminDP", "TerminDP2", 
                             
                             //11-15
                             "TaxAmt", "TaxAmt2", "AfterTax", "VoucherDocNo", "ItCodeInternal",

                            //16-20
                             "ItName", "Specification", "ProjectCode", "ProjectName", "Remark",

                            //21-25
                            "CreateBy", "CreateDt", "LastupBy", "LastUpDt", "LocalDocNo"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 26, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 28, 24);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 29, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 25);
                        }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 12, 13, 14, 15 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Event

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0)
            {
                var f = new FrmSOContractDownpayment2Dlg5(mFrmParent);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 17);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0)
            {
                var f = new FrmSOContractDownpayment2Dlg5(mFrmParent);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 17);
                f.ShowDialog();
            }

          
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion
    }
}
