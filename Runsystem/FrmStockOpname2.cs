﻿#region Update
/*
    18/04/2017 [WED] Save DNo ke tabel TblStockOpname2Dtl ditambah jadi 5 digit
    18/04/2017 [WED] Validasi max item menjadi 99.999 item
    06/06/2017 [TKG] Bug fixing data saat disimpan di stock movement untuk data yg berubah harganya.
    20/06/2017 [TKG] Update remark di journal
    11/07/2017 [WED] EntCode save ke journalDtl
    07/08/2017 [WED] tambah kolom Foreign Name berdasarkan parameter IsShowForeignName
    12/01/2018 [TKG] bug journal
    26/07/2018 [TKG] konversi antar uom.
    16/12/2019 [TKG/IMS] journal untuk moving average
    18/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
    07/02/2021 [TKG/GSS] ubah GetParameter dan proses save.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;
#endregion

namespace RunSystem
{
    public partial class FrmStockOpname2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmStockOpname2Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string mDocType = "25", mEntCode = string.Empty;
        private bool mIsAutoJournalActived = false, mIsMovingAvgEnabled = false;
        internal bool mIsShowForeignName = false, mIsBOMShowSpecifications = false;

        #endregion

        #region Constructor

        public FrmStockOpname2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Stock Opname With Value";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                //SetNumberOfInventoryUomCode();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsMovingAvgEnabled', 'IsBOMShowSpecifications', 'IsAutoJournalActived', 'IsShowForeignName', 'NumberOfInventoryUomCode' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            
                            //Integer
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Item's" + Environment.NewLine + "Local Code",
                        "Item's Name",
                        
                        //6-10
                        "Batch#",
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock" + Environment.NewLine + "(System)",
                        
                        //11-15
                        "Stock" + Environment.NewLine + "(Actual)",
                        "Balance",
                        "UoM",
                        "Stock" + Environment.NewLine + "(System)",
                        "Stock" + Environment.NewLine + "(Actual)",
                        
                        //16-20
                        "Balance",
                        "UoM",
                        "Stock" + Environment.NewLine + "(System)",
                        "Stock" + Environment.NewLine + "(Actual)",
                        "Balance",
                        
                        //21-25
                        "UoM",
                        "Currency",
                        "Previous Price",
                        "Source",
                        "New Price",

                        //26-28
                        "Remark",
                        "Foreign Name",
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        20, 80, 20, 100, 300, 
                        
                        //6-10
                        200, 170, 60, 50, 80, 
                        
                        //11-15
                        80, 80, 60, 80, 80,

                        //16-20
                        80, 60, 80, 80, 80,

                        //21-25
                        60, 60, 120, 180, 120, 
                        
                        //26-28
                        300, 200, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20, 23, 25 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 7, 8, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 26 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 27 });
            if(!mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 28 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 16, 17, 18, 20, 21, 22, 23, 24, 27, 28 });

            Grd1.Cols[27].Move(7);
            Grd1.Cols[28].Move(6);

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7, 8, 22, 24, 26 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19, 20, 21 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 11, 15, 19, 25, 26 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 11, 15, 19, 25, 26 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, MeeRemark, TxtJournalDocNo
            });
            mEntCode = string.Empty;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20, 23, 25 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmStockOpname2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }
        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                ReComputeStock();

                mEntCode = Sm.GetValue("Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode = '" + Sm.GetLue(LueWhsCode) + "' limit 1;");

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "StockOpname2", "TblStockOpname2Hdr");

                var cml = new List<MySqlCommand>();

                cml.Add(SaveStockOpname2Hdr(DocNo));
                cml.Add(SaveStockOpname2Dtl(DocNo));

                //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) 
                //        cml.Add(SaveStockOpname2Dtl(DocNo, Row));

                cml.Add(SaveStock(DocNo));
                if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

                Sm.ExecCommands(cml);

                BtnInsertClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }
        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmStockOpname2Dlg(this, Sm.GetLue(LueWhsCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 11, 15, 19, 25, 26 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20, 23, 25 });
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmStockOpname2Dlg(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 11, 15, 19, 25 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 26 }, e);

            if (e.ColIndex == 11)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 2, 11, 15, 19, 13, 17, 21);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 2, 11, 19, 15, 13, 21, 17);
            }

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 2, 15, 11, 19, 17, 13, 21);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 2, 15, 19, 11, 17, 21, 13);
            }

            if (e.ColIndex == 19)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 2, 19, 11, 15, 21, 13, 17);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 2, 19, 15, 11, 21, 17, 13);
            }

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 17)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 11);
            
            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 13), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 11);
            
            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 15);
            

            Grd1.Cells[e.RowIndex, 12].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 11) - Sm.GetGrdDec(Grd1, e.RowIndex, 10);
            Grd1.Cells[e.RowIndex, 16].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 15) - Sm.GetGrdDec(Grd1, e.RowIndex, 14);
            Grd1.Cells[e.RowIndex, 20].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 19) - Sm.GetGrdDec(Grd1, e.RowIndex, 18);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 11, 12, 15, 16, 19, 20 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length!=0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 100000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (99.999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;

            return false;
        }

        private MySqlCommand SaveStockOpname2Hdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblStockOpname2Hdr(DocNo, DocDt, WhsCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveStockOpname2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Stock Opname With Value - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblStockOpname2Dtl(DocNo, DNo, ItCode, BatchNo, Source, Lot, Bin, ");
                        SQL.AppendLine("QtyActual, QtyActual2, QtyActual3, Qty, Qty2, Qty3, ");
                        SQL.AppendLine("CurCode, UPrice, Source2, UPrice2, ");
                        SQL.AppendLine("Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() + 
                        ", @ItCode_" + r.ToString() + 
                        ", @BatchNo_" + r.ToString() +
                        ", @Source_" + r.ToString() +
                        ", @Lot_" + r.ToString() + 
                        ", @Bin_" + r.ToString() + 
                        ", @QtyActual_" + r.ToString() +
                        ", @QtyActual2_" + r.ToString() +
                        ", @QtyActual3_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() +
                        ", @CurCode_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", Case When @UPrice_" + r.ToString() + "=@UPrice2_" + r.ToString() + "  Then Null Else Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() + ") End " +
                        ", @UPrice2_" + r.ToString() +
                        ", @Remark_" + r.ToString() + 
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + (r + 1).ToString(), 5));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@QtyActual_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@QtyActual2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@QtyActual3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 19));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 12));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 16));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 22));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 23));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 25));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 26));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveStockOpname2Dtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblStockOpname2Dtl(DocNo, DNo, ItCode, BatchNo, Source, Lot, Bin, ");
        //    SQL.AppendLine("QtyActual, QtyActual2, QtyActual3, Qty, Qty2, Qty3, ");
        //    SQL.AppendLine("CurCode, UPrice, Source2, UPrice2, ");
        //    SQL.AppendLine("Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @BatchNo, @Source, @Lot, @Bin, ");
        //    SQL.AppendLine("@QtyActual, @QtyActual2, @QtyActual3, @Qty, @Qty2, @Qty3, ");
        //    SQL.AppendLine("@CurCode, @UPrice, ");
        //    SQL.AppendLine("Case When @UPrice=@UPrice2 Then Null Else Concat(@DocType, '*', @DocNo, '*', @DNo) End, ");
        //    SQL.AppendLine("@UPrice2, ");
        //    SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime());");

        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocType", mDocType);
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 8));
        //    Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyActual", Sm.GetGrdDec(Grd1, Row, 11));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyActual2", Sm.GetGrdDec(Grd1, Row, 15));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyActual3", Sm.GetGrdDec(Grd1, Row, 19));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 12));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 20));
        //    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 22));
        //    Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
        //    Sm.CmParam<Decimal>(ref cm, "@UPrice2", Sm.GetGrdDec(Grd1, Row, 25));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 26));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Lot, Bin, Source, ");
            SQL.AppendLine("    QtyActual, QtyActual2, QtyActual3 ");
            SQL.AppendLine("    From TblStockOpname2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And Source2 Is Null ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.Lot=B.Lot ");
            SQL.AppendLine("    And A.Bin=B.Bin ");
            SQL.AppendLine("    And A.Source=B.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Qty=IfNull(B.QtyActual, 0), ");
            SQL.AppendLine("    A.Qty2=IfNull(B.QtyActual2, 0), ");
            SQL.AppendLine("    A.Qty3=IfNull(B.QtyActual3, 0), ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, ");
            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode;");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*(B.QtyActual-B.Qty), -1*(B.QtyActual2-B.Qty2), -1*(B.QtyActual3-B.Qty3), ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source2, ");
            SQL.AppendLine("B.QtyActual, B.QtyActual2, B.QtyActual3, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Lot, Bin, Source ");
            SQL.AppendLine("    From TblStockOpname2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And Source2 Is Not Null ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.Lot=B.Lot ");
            SQL.AppendLine("    And A.Bin=B.Bin ");
            SQL.AppendLine("    And A.Source=B.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Qty=0, A.Qty2=0, A.Qty3=0, ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode;");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @WhsCode, Lot, Bin, ItCode, BatchNo, Source2, QtyActual, QtyActual2, QtyActual3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Dtl ");
            SQL.AppendLine("Where DocNo=@DocNo And Source2 Is Not Null; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.ItCode, A.BatchNo, A.Source2, A.CurCode, A.UPrice2, B.ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockOpname2Dtl A ");
            SQL.AppendLine("Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.Source2 Is Not Null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblStockOpname2Hdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Stock Opname With Value : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblStockOpname2Hdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            if (mIsMovingAvgEnabled)
            {
                //Debit
                SQL.AppendLine("    Select T.AcNo, Sum(T.DAmt) As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty>0.00 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, Abs(B.Qty)*C.UPrice*C.ExcRate As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty<0.00 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, (B.QtyActual-B.Qty)*C.UPrice*C.ExcRate As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.QtyActual*C.UPrice*C.ExcRate As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source2=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.Qty*IfNull(C.MovingAvgPrice, 0.00) As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty>0.00 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, Abs(B.Qty)*IfNull(C.MovingAvgPrice, 0.00) As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty<0.00 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, (B.QtyActual-B.Qty)*IfNull(C.MovingAvgPrice, 0.00) As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.QtyActual*IfNull(C.MovingAvgPrice, 0.00) As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Where T.AcNo is Not Null ");
                SQL.AppendLine("    Group By T.AcNo ");


                //Credit
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select T.AcNo, 0.00 As DAmt, Sum(T.CAmt) As CAmt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty>0 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, Abs(B.Qty)*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty<0 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, (B.QtyActual-B.Qty)*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source2=C.Source ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.Qty*IfNull(C.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty>0 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, Abs(B.Qty)*IfNull(C.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty<0 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, (B.QtyActual-B.Qty)*IfNull(C.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.Qty*IfNull(C.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg C On B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Where T.AcNo is Not Null ");
                SQL.AppendLine("    Group By T.AcNo ");
            }
            else
            {
                //Debit
                SQL.AppendLine("    Select T.AcNo, Sum(T.DAmt) As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty>0.00 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, Abs(B.Qty)*C.UPrice*C.ExcRate As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty<0.00 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, (B.QtyActual-B.Qty)*C.UPrice*C.ExcRate As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.QtyActual*C.UPrice*C.ExcRate As DAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source2=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Where T.AcNo is Not Null ");
                SQL.AppendLine("    Group By T.AcNo ");


                //Credit

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select T.AcNo, 0.00 As DAmt, Sum(T.CAmt) As CAmt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty>0 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, Abs(B.Qty)*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Qty<0 And B.Source2 Is Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, (B.QtyActual-B.Qty)*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblStockOpname2Hdr A ");
                SQL.AppendLine("        Inner Join TblStockOpname2Dtl B On A.DocNo=B.DocNo And B.Source2 Is Not Null ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source2=C.Source ");
                SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Where T.AcNo is Not Null ");
                SQL.AppendLine("    Group By T.AcNo ");
            }

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowStockOpname2Hdr(DocNo);
                ShowStockOpname2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowStockOpname2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, Remark, JournalDocNo From TblStockOpname2Hdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", "DocDt", "WhsCode", "Remark", "JournalDocNo" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowStockOpname2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("(A.QtyActual-A.Qty) As Stock, A.QtyActual, A.Qty, B.InventoryUOMCode, ");
            SQL.AppendLine("(A.QtyActual2-A.Qty2) As Stock2, A.QtyActual2, A.Qty2, B.InventoryUOMCode2, ");
            SQL.AppendLine("(A.QtyActual3-A.Qty3) As Stock3, A.QtyActual3, A.Qty3, B.InventoryUOMCode3, ");
            SQL.AppendLine("A.CurCode, A.UPrice, A.Source2, A.UPrice2, A.Remark, B.ForeignName, B.Specification ");
            SQL.AppendLine("From TblStockOpname2Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItCodeInternal", "ItName", "BatchNo", "Source",   
                    
                    //6-10
                    "Lot", "Bin", "Stock", "QtyActual", "Qty", 
                    
                    //11-15
                    "InventoryUomCode", "Stock2", "QtyActual2", "Qty2", "InventoryUomCode2", 
                    
                    //16-20
                    "Stock3", "QtyActual3", "Qty3", "InventoryUomCode3", "CurCode", 
                    
                    //21-25
                    "UPrice", "Source2", "UPrice2", "Remark" , "ForeignName",

                    //26
                    "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 26);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 11, 12, 14, 15, 16, 18, 19, 20, 23, 25 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        //private void SetNumberOfInventoryUomCode()
        //{
        //    string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
        //    if (NumberOfInventoryUomCode.Length == 0)
        //        mNumberOfInventoryUomCode = 1;
        //    else
        //        mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        //}

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 6) +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItem2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 7, 8, 9);
                            No += 1;
                        }
                    }
                }
                cm.CommandText = SQL.ToString() + " And (" + Filter + ")";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 7), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 8), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 9), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 10, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 18, 5);

                                Grd1.Cells[row, 12].Value = Sm.GetGrdDec(Grd1, row, 11) - Sm.GetGrdDec(Grd1, row, 10);
                                Grd1.Cells[row, 16].Value = Sm.GetGrdDec(Grd1, row, 15) - Sm.GetGrdDec(Grd1, row, 14);
                                Grd1.Cells[row, 20].Value = Sm.GetGrdDec(Grd1, row, 19) - Sm.GetGrdDec(Grd1, row, 18);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ParPrint()
        {
            string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<SoHdr>();
            var ldtl = new List<SoDtl>();

            string[] TableName = { "SOHdr", "SODtl" };
            List<IList> myLists = new List<IList>();

            #region Header
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y')As DocDt, B.WhsName, A.Remark ");
            SQL.AppendLine("From tblstockopname2hdr A");
            SQL.AppendLine("Inner Join tblwarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",
                          //6-10
                         "WhsName",
                         "Remark",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SoHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),

                            WhsName = Sm.DrStr(dr, c[6]),
                            Remark = Sm.DrStr(dr, c[7]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Lot, A.Bin, A.Qty, A.Qty2, A.Qty3,");
                SQLDtl.AppendLine("B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3, A.CurCode, A.UPrice2, A.Remark As RemarkDetail");
                SQLDtl.AppendLine("From tblstockopname2dtl A  ");
                SQLDtl.AppendLine("Inner Join tblitem B On A.ItCode=B.ItCode ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                    //0
                    "ItCode",

                    //1-5
                    "ItCodeInternal",
                    "ItName",
                    "BatchNo",
                    "Lot",
                    "Bin",

                    //6-10
                    "Qty",
                    "Qty2",
                    "Qty3",
                    "InventoryUOMCode",
                    "InventoryUOMCode2",

                    //11-14
                    "InventoryUOMCode3",
                    "CurCode",
                    "UPrice2",
                    "RemarkDetail",
                    });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new SoDtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),

                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUOMCode = Sm.DrStr(drDtl, cDtl[9]),

                            InventoryUOMCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUOMCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            CurCode = Sm.DrStr(drDtl, cDtl[12]),
                            UPrice2 = Sm.DrDec(drDtl, cDtl[13]),
                            RemarkDetail = Sm.DrStr(drDtl, cDtl[14]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion

            int a = int.Parse(ParValue);

            if (a == 1)
            {
                Sm.PrintReport("StockOpnameVal", myLists, TableName, false);
            }
            else if (a == 2)
            {
                Sm.PrintReport("StockOpnameVal2", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("StockOpnameVal3", myLists, TableName, false);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
                ClearGrd();
            }
        }

        #endregion

        #endregion

        #region Reporting Class

        class SoHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }

            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }

        }

        class SoDtl
        {
            public string ItCode { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Lot { get; set; }

            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUOMCode { get; set; }

            public string InventoryUOMCode2 { get; set; }
            public string InventoryUOMCode3 { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice2 { get; set; }
            public string RemarkDetail { get; set; }
        }

        #endregion
    }
}
