﻿#region Update
/*
    05/06/2017 [TKG] tambah Cancelled Journal Voucher
    05/07/2017 [TKG] tambah update remark di hdr+dtl pada saat journal
    13/07/2017 [TKG] tambah entity, tambah amount untuk main currency
    20/07/2017 [WED] bug fixing entcode di savejournal2() dan savejournal()
    18/07/2017 [TKG] tambah validasi apakah nomor rekening coa bank account voucher juga terinput atau tdk.
    18/07/2017 [TKG] validasi journal untuk rekening bank account voucher masuk ke kolom debit atau credit berdasarkan account type voucher.
    14/04/2018 [TKG] menggunakan format khusus voucher atau format standard dokumen run system berdasarkan parameter VoucherCodeFormatType
    28/05/2018 [HAR] document date ngikut document vouchernya berdasarkan parameter dan waktu show data voucher form utama tetp aktif
    28/12/2018 [MEY] Menambahkan parameter untuk Jurnal Voucher yang melebihi pengajuannya. 
    19/01/2020 [TKG/IMS] menambah kolom alias
    20/01/2020 [WED/SIER] tambah ambil dari COA voucher request, kalau tipe nya Manual. berdasarkan parameter IsVoucherRequestUseCOA
    03/03/2020 [HAR/KBN] COA bisa dipilih berkali kali berdasarkan parameter IsJournalUseDuplicateCOA
    05/03/2020 [DITA/KBN] Penomoran dokumen 6 digit, reset tiap tahun, dan berdasarkan entity
    19/05/2020 [WED/YK] COA dibatasi dengan parameter IsCOAFilteredByGroup
    19/05/2020 [WED/YK] dibatasi berdasarkan parameter IsCOAFilteredByGroup
    22/07/2020 [WED/SRN] tampilkan nilai ExcRate dan Amt2 dari VoucherRequest berdasarkan parameter IsVRManualUseOtherCurrency
    29/07/2020 [WED/SRN] nilai excrate yg disimpan adalah yg di textbox nya berdasarkan parameter IsVRManualUseOtherCurrency
    14/12/2020 [WED/IMS] tambah inputan SOContract berdasarkan parameter IsVoucherJournalUseSOContract
    10/01/2021 [HAR/PHT] tambah inputan costcenter berdasarkan parameter IsVoucherJournalUseCostCenter
    17/01/2021 [TKG/PHT] ubah GenerateDocNo*
    19/01/2021 [TKG/PHT] ubah journal untuk mendapatkan cost center dijoin berdasarkan cccode, bukan deptcode
    23/02/2021 [TKG/PHT] merubah tampilan layar, merubah tooltip button, menambah button untuk info journal.
    02/03/2021 [BRI/PHT] tambah parameter IsCostCenterShowChildOnly untuk cost center paling bontot
    18/03/2021 [VIN/IMS] parameter IsVoucherJournalUseCostCenter N hide lbl dan txt
    22/03/2021 [VIN/IMS] tambah VoucherJournalCCCodeSource
    29/03/2021 [VIN/IMS] bug show header
    19/04/2021 [HAR/PHT] BUG nilai debet/credit dihitung padahal tdk ada coa nya shg pengaruh ke balance : ComputeDebetCredit ditambah validasi jiks ada akunnya
    23/04/2021 [HAR/PHT] Saat show data, data header tidak muncul, karena masih ada data lama yang costcenter nya kosong,
    18/05/2021 [TKG/PHT] menggunakan parameter IsVoucherJournalUseProfitCenter
    17/11/2021 [RDA/PHT] tambah parameter IsJournalVoucherShowAllCashBankAccount
    24/11/2021 [ISD/AMKA] tambah parameter IsJournalVoucherFilteredByGroup
    29/11/2021 [TYO/AMKA] Merubah format PRINTOUT JOURNAL VOUCHER dengan Voucher type manual
    14/01/2022 [TKG/PHT] mengganti GetParameter() dan proses save
    10/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    07/04/2022 [DITA/PHT] saat save journal cancel tambah validasi currendt based on param IsClosingJournalBasedOnMultiProfitCenter
    22/04/2022 [BRI/PHT] date ambil dari voucher
    08/12/2022 [IBL/BBT] Tambah printout Bank Tanah
    10/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
    19/01/2023 [BRI/PHT] Bug : merubah source journal
    25/01/2023 [BRI/PHT] Bug : merubah source journal cancel
    15/02/2023 [IBL/BBT] Bug set otomatis amt ke dtl : Balance blm sesuai.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherJournal : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mEntCode = string.Empty,
            mVoucherJournalCCCodeSource = string.Empty;
        private string
            mVoucherCodeFormatType = "1",
            mMainCurCode = string.Empty,
            mVoucherDocTypeManual = "01",
            mDocNoFormat = string.Empty,
            mDocTitle = string.Empty,
            mJournalDocNoFormat = string.Empty
            ;
        public string mAccountingRptStartFrom = string.Empty;
        public bool
            mDocDtVoucherJournalBasedOnVoucher = true,
            mAllowToSaveDifferentAmtJournalVoucher = true,
            mIsCOAUseAlias = false,
            mIsJournalUseDuplicateCOA = false,
            mIsVoucherJournalUseSOContract = false,
            mIsVoucherJournalUseCostCenter = false,
            mIsCostCenterFilteredByGroup = false
            ;
        private bool
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsVoucherRequestUseCOA = false,
            mIsVRForBudgetUseSOContract = false,
            mIsVRUseSOContract = false;
        internal bool 
            mIsCOAFilteredByGroup = false,
            mIsVRManualUseOtherCurrency = false,
            mIsCostCenterShowChildOnly = false,
            mIsVoucherJournalUseProfitCenter = false,
            mIsJournalVoucherShowAllCashBankAccount = false,
            mIsJournalVoucherFilteredByGroup = false;

        internal FrmVoucherJournalFind FrmFind;

        #endregion

        #region Constructor

        public FrmVoucherJournal(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Journal Voucher";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetGrd();
                TxtCurCode2.EditValue = mMainCurCode;
                if (mIsVoucherJournalUseSOContract)
                    BtnSOContractDocNo.Visible = BtnSOContractDocNo2.Visible = true;

                if (mIsVoucherJournalUseCostCenter)
                {
                    if (mVoucherJournalCCCodeSource != "2")
                        BtnCostCenter.Visible = true;
                    LblCCCode.ForeColor = Color.Red;
                }
                else
                    LblCCCode.Visible = TxtCCName.Visible = false;
               
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {   
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsJournalVoucherShowAllCashBankAccount', 'IsJournalVoucherFilteredByGroup', 'VoucherJournalCCCodeSource', 'VoucherCodeFormatType', 'MainCurCode', ");
            SQL.AppendLine("'DocDtVoucherJournalBasedOnVoucher', 'VoucherCodeFormatType', 'AccountingRptStartFrom', 'VoucherDocTypeManual', 'DocNoFormat', ");
            SQL.AppendLine("'IsVRForBudgetUseSOContract', 'IsVRUseSOContract', 'IsVoucherJournalUseProfitCenter', 'IsCOAUseAlias', 'AllowToSaveDifferentAmtJournalVoucher', ");
            SQL.AppendLine("'IsVRManualUseOtherCurrency', 'IsVoucherJournalUseSOContract', 'IsVoucherJournalUseCostCenter', 'IsCostCenterFilteredByGroup', 'IsCostCenterShowChildOnly', ");
            SQL.AppendLine("'IsVoucherRequestUseCOA', 'IsJournalUseDuplicateCOA', 'IsCOAFilteredByGroup', 'IsClosingJournalBasedOnMultiProfitCenter', 'DocTitle', 'JournalDocNoFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsJournalVoucherFilteredByGroup": mIsJournalVoucherFilteredByGroup = ParValue == "Y"; break;
                            case "IsJournalVoucherShowAllCashBankAccount": mIsJournalVoucherShowAllCashBankAccount = ParValue == "Y"; break;
                            case "DocDtVoucherJournalBasedOnVoucher": mDocDtVoucherJournalBasedOnVoucher = ParValue == "Y"; break;
                            case "AllowToSaveDifferentAmtJournalVoucher": mAllowToSaveDifferentAmtJournalVoucher = ParValue == "Y"; break;
                            case "IsCOAUseAlias": mIsCOAUseAlias = ParValue == "Y"; break;
                            case "IsVoucherJournalUseProfitCenter": mIsVoucherJournalUseProfitCenter = ParValue == "Y"; break;
                            case "IsVRUseSOContract": mIsVRUseSOContract = ParValue == "Y"; break;
                            case "IsVRForBudgetUseSOContract": mIsVRForBudgetUseSOContract = ParValue == "Y"; break;
                            case "IsCostCenterShowChildOnly": mIsCostCenterShowChildOnly = ParValue == "Y"; break;
                            case "IsCostCenterFilteredByGroup": mIsCostCenterFilteredByGroup = ParValue == "Y"; break;
                            case "IsVoucherJournalUseCostCenter": mIsVoucherJournalUseCostCenter = ParValue == "Y"; break;
                            case "IsVoucherJournalUseSOContract": mIsVoucherJournalUseSOContract = ParValue == "Y"; break;
                            case "IsVRManualUseOtherCurrency": mIsVRManualUseOtherCurrency = ParValue == "Y"; break;
                            case "IsVoucherRequestUseCOA": mIsVoucherRequestUseCOA = ParValue == "Y"; break;
                            case "IsJournalUseDuplicateCOA": mIsJournalUseDuplicateCOA = ParValue == "Y"; break;
                            case "IsCOAFilteredByGroup": mIsCOAFilteredByGroup = ParValue == "Y"; break;
                            
                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "VoucherJournalCCCodeSource": mVoucherJournalCCCodeSource = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "VoucherDocTypeManual": mVoucherDocTypeManual = ParValue; break;
                            case "AccountingRptStartFrom": mAccountingRptStartFrom = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mVoucherDocTypeManual.Length == 0) mVoucherDocTypeManual = "01";
            if (mVoucherJournalCCCodeSource.Length == 0) mVoucherJournalCCCodeSource = "1";
            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6
                        "Alias"
                    },
                     new int[] 
                    {
                        20,
                        100, 250, 100, 100, 400,
                        150
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Grd1.Cols[6].Visible = mIsCOAUseAlias;
            Grd1.Cols[6].Move(2);
        }

        override protected void HideInfoInGrd()
        {
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeRemark }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnVoucherJournalDocNo.Enabled = false;
                    BtnVoucherDocNo.Enabled = false;
                    BtnSOContractDocNo.Enabled = false;
                    BtnCostCenter.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeRemark }, false);
                    BtnVoucherJournalDocNo.Enabled = true;
                    BtnVoucherDocNo.Enabled = true;
                    BtnSOContractDocNo.Enabled = true;
                    if (mIsVoucherJournalUseCostCenter) BtnCostCenter.Enabled = true;
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()    
        {
            mEntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, TxtVoucherJournalDocNo, TxtVoucherDocNo, MeeVoucherRemark, 
                TxtCurCode, MeeRemark, TxtJournalDocNo, TxtJournalDocNo2, TxtEntCode, TxtSOContractDocNo,
                TxtCCCode, TxtCCName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtDbt, TxtCdt, TxtBalanced, TxtExcRate, TxtAmt, 
                TxtAmt2 
            }, 0);
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherJournalFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                //Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            mEntCode = Sm.GetValue("Select EntCode From TblVoucherHdr Where DocNo = @Param; ", TxtVoucherDocNo.Text);
             
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            PrintData(TxtDocNo.Text);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher Number", false))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmVoucherJournalDlg2(this));
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;
            }
        }


        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3) Compute(e.RowIndex, 1);
            if (e.ColIndex == 4) Compute(e.RowIndex, 2);
            ComputeDebetCredit(); ComputeBalanced();
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeDebetCredit();
                ComputeBalanced();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);

        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher Number", false))
            {
                Sm.FormShowDialog(new FrmVoucherJournalDlg2(this));
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            string BankAcCode = Sm.GetValue("Select BankAcCode From TblVoucherHdr Where DocNo=@Param", TxtVoucherDocNo.Text);
            string AcType = Sm.GetValue("Select AcType From TblVoucherHdr Where DocNo=@Param", TxtVoucherDocNo.Text);
            var bankAcTp = Sm.GetValue("Select BankAcTp From TblBankAccount Where BankAcCode = @Param ", BankAcCode);
            var code1 = Sm.GetCode1ForJournalDocNo("FrmVoucher", bankAcTp, AcType, mJournalDocNoFormat);
            var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param ", TxtCCCode.Text);

            if (mVoucherCodeFormatType == "2")
            {
                if(mDocNoFormat == "1")
                    DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherJournal", "TblVoucherJournalHdr");
                else
                    DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "VoucherJournal", "TblVoucherJournalHdr", mEntCode, "1");
            }
            else
                DocNo = GenerateDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveVoucherJournal(DocNo));

            //cml.Add(SaveVoucherJournalHdr(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveVoucherJournalDtl(DocNo, Row));

            cml.Add(SaveJournal(DocNo, code1, profitCenterCode));

            if (TxtVoucherJournalDocNo.Text.Length > 0)
                cml.Add(SaveJournal2(code1, profitCenterCode));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
            ComputeDebetCredit(); 
            ComputeBalanced();
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher", false) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                // Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty()||
                IsGrdValueNotValid() ||
                IsBalancedNotValid() ||
                IsAmtAndJournalAmtDifferent() ||
                IsCancelledJournalVoucherInvalid() ||
                IsGrdValueShouldNotLess() ||
                IsVoucherBankAccountAcNoNotExisted()||
                (mIsVoucherJournalUseCostCenter && Sm.IsTxtEmpty(TxtCCCode, "Costcenter", false))
                ;
        }

        private string GetProfitCenterCode()
        {
            var Value = TxtCCCode.Text;
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select ProfitCenterCode From TblCostCenter " +
                    "Where ProfitCenterCode Is Not Null And CCCode=@Param;",
                    Value);
        }

        private bool IsVoucherBankAccountAcNoNotExisted()
        { 
            var SQL = new StringBuilder();
            var AcType = string.Empty;

            SQL.AppendLine("Select COAAcNo ");
            SQL.AppendLine("From TblBankAccount ");
            SQL.AppendLine("Where BankAcCode In ( ");
            SQL.AppendLine("    Select BankAcCode From TblVoucherHdr Where DocNo=@Param ");
            SQL.AppendLine(");");

            var AcNo = Sm.GetValue(SQL.ToString(), TxtVoucherDocNo.Text);

            if (AcNo.Length == 0) return false;

            var AcDesc = Sm.GetValue("Select AcDesc From TblCOA where AcNo=@Param;", AcNo);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd1, r, 1)))
                {
                    AcType = Sm.GetValue("Select AcType From TblVoucherHdr Where DocNo=@Param;", TxtVoucherDocNo.Text);

                    if (AcType == "D" && Sm.GetGrdDec(Grd1, r, 4) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA Account# : " + AcNo + Environment.NewLine +
                            "COA Account Description : " + AcDesc + Environment.NewLine + 
                            "Voucher's Account Type : Debit" + Environment.NewLine + Environment.NewLine +
                            "For This account, You should input debit amount instead of credit amount."
                            );
                        return true;
                    }
                    if (AcType == "C" && Sm.GetGrdDec(Grd1, r, 3) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "COA Account# : " + AcNo + Environment.NewLine +
                            "COA Account Description : " + AcDesc + Environment.NewLine +
                            "Voucher's Account Type : Credit" + Environment.NewLine + Environment.NewLine +
                            "For This account, You should input credit amount instead of debit amount."
                            );
                        return true;
                    }
                    return false;
                }
            }
            Sm.StdMsg(mMsgType.Warning, 
                "Mandatory Bank Account's COA Account" + Environment.NewLine + Environment.NewLine +
                "COA Account# : " + AcNo + Environment.NewLine +
                "COA Account Description : " + AcDesc + Environment.NewLine + Environment.NewLine +
                "You should input this COA account in this journal voucher."
                );
            return true;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 account#.");
                return true;
            }
            return false;
        }

        private bool IsBalancedNotValid()
        {
            decimal Balanced = 0m;
            if (TxtBalanced.Text.Length != 0) Balanced = decimal.Parse(TxtBalanced.Text);
            if (Balanced != 0m)
            {
                Sm.StdMsg(mMsgType.Warning, "Debit and Credit are not balanced");
                return true;
            }
            return false;
        }

        private bool IsAmtAndJournalAmtDifferent()
        {
            decimal TotalDebit = 0m, Amt = 0m;

            if (TxtDbt.Text.Length != 0) TotalDebit = decimal.Parse(TxtDbt.Text);
            if (TxtAmt.Text.Length != 0) Amt = decimal.Parse(TxtAmt.Text);

            if (TotalDebit != Amt)
            {

                if (!mAllowToSaveDifferentAmtJournalVoucher)
                {
                   Sm.StdMsg(mMsgType.Warning,
                        "Total Debit : " + Sm.FormatNum(TotalDebit, 0) + Environment.NewLine +
                       "Voucher's amount : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                       "Total debit is different than voucher's amount.");
                   return true;
                }

                else
                {

                    if (Sm.StdMsgYN(
                       "Question",
                       "Total Debit : " + Sm.FormatNum(TotalDebit, 0) + Environment.NewLine +
                       "Voucher's amount : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                       "Total debit is different than voucher's amount." + Environment.NewLine +
                       "Do you still want to save this data ?"
                       ) == DialogResult.No)
                        return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "COA's account is empty.")) return true;
                //if (Sm.GetGrdDec(Grd1, Row, 3) == 0m && Sm.GetGrdDec(Grd1, Row, 4) == 0m)
                //{
                //    Sm.StdMsg(mMsgType.Warning,
                //        "Acount No : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                //        "Account Description : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                //        "Both debit and credit amount can't be 0.");
                //    return true;
                //}
            }
            return false;
        }

        private bool IsCancelledJournalVoucherInvalid()
        {
            if (TxtVoucherJournalDocNo.Text.Length == 0) return false;
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherJournalHdr Where DocNo=@Param And CancelInd='Y';",
                TxtVoucherJournalDocNo.Text,
                "Journal Voucher# : " + TxtVoucherJournalDocNo.Text + Environment.NewLine +
                "This document already cancelled."
                );
        }

        private bool IsGrdValueShouldNotLess()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                int amt = (Sm.GetGrdInt(Grd1, Row, 3));
                int amt2 = (Sm.GetGrdInt(Grd1, Row, 4));
                if ((amt < 0) || (amt2 < 0))
                {
                    Sm.StdMsg(mMsgType.Warning,
                       "Account# : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                       "Amount should not be less than 0.");
                    return true;
                }
            }
             return false;
        }

        private MySqlCommand SaveVoucherJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true, IsExisted = false;

            SQL.AppendLine("/* Voucher Journal */ ");

            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblVoucherJournalHdr(DocNo, DocDt, CancelInd, VoucherJournalDocNo, VoucherDocNo, CurCode, Amt, ");
            SQL.AppendLine("ExcRate, JournalDocNo, JournalDocNo2, EntCode, SOContractDocNo, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @VoucherJournalDocNo, @VoucherDocNo, @CurCode, @Amt, ");
            if (!mIsVRManualUseOtherCurrency)
            {
                SQL.AppendLine("Case When @CurCode=@MainCurCode Then 1 Else ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                SQL.AppendLine("        Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("    ), 0) End, ");
            }
            else
            {
                SQL.AppendLine("Case When @CurCode=@MainCurCode Then 1 Else ");
                SQL.AppendLine("IfNull(@ExcRate, ");
                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Amt From TblCurrencyRate ");
                SQL.AppendLine("        Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("    ), 0) ");
                SQL.AppendLine(") End, ");
            }
            SQL.AppendLine("Null, Null, @EntCode, @SOContractDocNo, @CCCode, @Remark, @UserCode, @Dt);");

            if (TxtVoucherJournalDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Update TblVoucherJournalHdr Set ");
                SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=@Dt ");
                SQL.AppendLine("Where CancelInd='N' And DocNo=@VoucherJournalDocNo;");
            }

            SQL.AppendLine("Update TblVoucherHdr Set VoucherJournalDocNo=@DocNo Where DocNo=@VoucherDocNo;");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    IsExisted = true;
                    if (IsFirst)
                    {
                        SQL.AppendLine("Insert Into TblVoucherJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirst = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");
                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                }
            }
            if (IsExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherJournalDocNo", TxtVoucherJournalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", TxtVoucherDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtExcRate.Text));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CCCode", TxtCCCode.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code
        //private MySqlCommand SaveVoucherJournalHdr(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblVoucherJournalHdr(DocNo, DocDt, CancelInd, VoucherJournalDocNo, VoucherDocNo, CurCode, Amt, ");
        //    SQL.AppendLine("ExcRate, ");
        //    SQL.AppendLine("JournalDocNo, JournalDocNo2, EntCode, SOContractDocNo, CCCode, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @VoucherJournalDocNo, @VoucherDocNo, @CurCode, @Amt, ");
        //    if (!mIsVRManualUseOtherCurrency)
        //    {
        //        SQL.AppendLine("Case When @CurCode=@MainCurCode Then 1 Else ");
        //        SQL.AppendLine("    IfNull(( ");
        //        SQL.AppendLine("        Select Amt From TblCurrencyRate ");
        //        SQL.AppendLine("        Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //        SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
        //        SQL.AppendLine("    ), 0) End, ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("Case When @CurCode=@MainCurCode Then 1 Else ");
        //        SQL.AppendLine("IfNull(@ExcRate, ");
        //        SQL.AppendLine("    IfNull(( ");
        //        SQL.AppendLine("        Select Amt From TblCurrencyRate ");
        //        SQL.AppendLine("        Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //        SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
        //        SQL.AppendLine("    ), 0) ");
        //        SQL.AppendLine(") End, ");
        //    }
        //    SQL.AppendLine("Null, Null, @EntCode, @SOContractDocNo, @CCCode, @Remark, @UserCode, CurrentDateTime());");

        //    if (TxtVoucherJournalDocNo.Text.Length > 0)
        //    {
        //        SQL.AppendLine("Update TblVoucherJournalHdr Set ");
        //        SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
        //        SQL.AppendLine("Where CancelInd='N' And DocNo=@VoucherJournalDocNo;");
        //    }

        //    SQL.AppendLine("Update TblVoucherHdr Set VoucherJournalDocNo=@DocNo Where DocNo=@VoucherDocNo;");

        //    var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@VoucherJournalDocNo", TxtVoucherJournalDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@VoucherDocNo", TxtVoucherDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
        //    Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
        //    Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtExcRate.Text));
        //    Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
        //    Sm.CmParam<String>(ref cm, "@CCCode", TxtCCCode.Text);
        //    Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
        //    return cm;
        //}

        //private MySqlCommand SaveVoucherJournalDtl(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //        "Insert Into TblVoucherJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) " +
        //        "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @Remark, @CreateBy, CurrentDateTime());"
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd1, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveJournal(string DocNo, string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblVoucherJournalHdr Set JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Voucher Journal : ', A.DocNo, ' [Voucher : ', A.VoucherDocNo, ']'), ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            if (mIsJournalUseDuplicateCOA)
                SQL.AppendLine("B.CCCode, ");
            else
                SQL.AppendLine("Null As CCCode, ");
            SQL.AppendLine("A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherJournalHdr A ");
            if (mIsJournalUseDuplicateCOA)
                SQL.AppendLine("Left Join TblCostCenter B On A.CCCode=B.CCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            SQL.AppendLine("Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt*IfNull(A.ExcRate, 1), B.CAmt*IfNull(A.ExcRate, 1), A.EntCode, A.SOContractDocNo, B.Remark, B.CreateBy, B.CreateDt ");
            SQL.AppendLine("From TblVoucherJournalHdr A ");
            SQL.AppendLine("Inner Join TblVoucherJournalDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mJournalDocNoFormat == "1")
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            }
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            return cm;
        }

        private MySqlCommand SaveJournal2(string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();
            var DocDt = Sm.GetValue("Select DocDt From TblVoucherJournalHdr Where DocNo=@Param;", TxtVoucherJournalDocNo.Text);
            var DocDt2 = Sm.GetDte(DteDocDt).Substring(0, 8);

            var IsClosingJournalUseCurrentDt = mIsClosingJournalBasedOnMultiProfitCenter ? Sm.IsClosingJournalUseCurrentDt(Sm.GetDte(DteDocDt), GetProfitCenterCode()) : Sm.IsClosingJournalUseCurrentDt(DocDt);
            var CurrentDt = Sm.ServerCurrentDate();
            var SelectedDt = CurrentDt;
            if (!IsClosingJournalUseCurrentDt) SelectedDt = DocDt;
            int index = 1;
            if (
                (IsClosingJournalUseCurrentDt && Sm.CompareStr(Sm.Left(CurrentDt, 6), Sm.Left(DocDt2, 6))) ||
                (!IsClosingJournalUseCurrentDt && Sm.CompareStr(Sm.Left(DocDt2, 6), Sm.Left(DocDt, 6)))
                )
            {
                index = 2;
            }

                SQL.AppendLine("Update TblVoucherJournalHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblVoucherJournalHdr Where DocNo=@DocNo And CancelInd='Y');");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, SOContractDocNo, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblVoucherJournalHdr Where DocNo=@DocNo And CancelInd='Y');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherJournalDocNo.Text);

            if (mJournalDocNoFormat == "1")
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(SelectedDt, index));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(SelectedDt, "Journal", "TblJournalHdr", mEntCode, index.ToString()));
            }
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(SelectedDt, index, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            string BankAcCode = Sm.GetValue("Select BankAcCode From TblVoucherHdr Where DocNo=@Param", TxtVoucherDocNo.Text);
            string AcType = Sm.GetValue("Select AcType From TblVoucherHdr Where DocNo=@Param", TxtVoucherDocNo.Text);
            var bankAcTp = Sm.GetValue("Select BankAcTp From TblBankAccount Where BankAcCode = @Param ", BankAcCode);
            var code1 = Sm.GetCode1ForJournalDocNo("FrmVoucher", bankAcTp, AcType, mJournalDocNoFormat);
            var profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param ", TxtCCCode.Text);

            cml.Add(EditVoucherJournal());

            cml.Add(SaveJournalCancel(code1, profitCenterCode));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(true, true, Sm.GetDte(DteDocDt))) ||
                // Sm.IsClosingJournalInvalid(true, true, Sm.GetDte(DteDocDt)) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblVoucherJournalHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditVoucherJournal()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherJournalHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    VoucherJournalDocNo=Null ");
            SQL.AppendLine("Where DocNo=@VoucherDocNo And VoucherJournalDocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", TxtVoucherDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournalCancel(string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = mIsClosingJournalBasedOnMultiProfitCenter ? Sm.IsClosingJournalUseCurrentDt(Sm.GetDte(DteDocDt), GetProfitCenterCode()) : Sm.IsClosingJournalUseCurrentDt(DocDt);
            var SelectedDt = CurrentDt;
            if (!IsClosingJournalUseCurrentDt) SelectedDt = DocDt;

            SQL.AppendLine("Update TblVoucherJournalHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblVoucherJournalHdr Where DocNo=@DocNo And CancelInd='Y');");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, SOContractDocNo, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblVoucherJournalHdr Where DocNo=@DocNo And CancelInd='Y');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (mJournalDocNoFormat == "1")
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(SelectedDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(SelectedDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(SelectedDt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            }
            
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowVoucherJournalHdr(DocNo);
                ShowVoucherJournalDtl(DocNo);
                ComputeDebetCredit(); ComputeBalanced();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherJournalHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.VoucherJournalDocNo, A.VoucherDocNo, ");
            SQL.AppendLine("A.CurCode, A.Amt, A.ExcRate, ");
            SQL.AppendLine("B.Remark As VoucherRemark, C.Description, A.Remark, A.EntCode, D.EntName, ");
            SQL.AppendLine("A.JournalDocNo, A.JournalDocNo2, IfNull(A.SOContractDocNo, F.SOContractDocNo) SOContractDocNo, ");
            if (mIsVoucherJournalUseCostCenter && mVoucherJournalCCCodeSource == "1")
                SQL.AppendLine("E.CCCode, E.CCName ");
            else
                SQL.AppendLine("H.CCCode As CCCode, H.CCName As CCname ");
            SQL.AppendLine("From TblVoucherJournalHdr A ");
            SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherDtl C On B.DocNo=C.DocNo And C.DNo='001' ");
            SQL.AppendLine("Left Join TblEntity D On A.EntCode=D.EntCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr F On B.VoucherRequestDocNo = F.DocNo ");
            if (mIsVoucherJournalUseCostCenter && mVoucherJournalCCCodeSource == "1")
                SQL.AppendLine("Left Join TblCostcenter E On A.CCCode = E.CCCode ");
            else
            {
                SQL.AppendLine("Left JOIN TblDepartment G ON F.DeptCode=G.DeptCode ");
                SQL.AppendLine("LEFT JOIN TblCostCenter H ON G.DeptCode=H.DeptCode ");
            }

            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "VoucherJournalDocNo", "VoucherDocNo", "CurCode", 
                        
                        //6-10
                        "Amt", "ExcRate", "VoucherRemark", "Description", "Remark", 

                        //11-15
                        "EntCode", "EntName", "JournalDocNo", "JournalDocNo2", "SOContractDocNo",

                        //16-17
                        "CCCode", "CCName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtVoucherJournalDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[5]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        TxtExcRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]) * Sm.DrDec(dr, c[7]), 0);
                        MeeVoucherRemark.Text = string.Concat(Sm.DrStr(dr, c[8]), " ", Sm.DrStr(dr, c[9])).Trim();
                        MeeRemark.Text = Sm.DrStr(dr, c[10]);
                        mEntCode = Sm.DrStr(dr, c[11]);
                        TxtEntCode.EditValue = Sm.DrStr(dr, c[12]);
                        TxtJournalDocNo.Text = Sm.DrStr(dr, c[13]);
                        TxtJournalDocNo2.Text = Sm.DrStr(dr, c[14]);
                        TxtSOContractDocNo.Text = Sm.DrStr(dr, c[15]);
                        TxtCCCode.Text = Sm.DrStr(dr, c[16]);
                        TxtCCName.Text = Sm.DrStr(dr, c[17]);
                    }, true
                );
        }

        private void ShowVoucherJournalDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, B.Alias ");
                SQL.AppendLine("From TblVoucherJournalDtl A ");
                SQL.AppendLine("Inner Join TblCOA B On A.AcNo = B.AcNo ");
                SQL.AppendLine("    And A.DocNo = @DocNo ");
                if (mIsCOAFilteredByGroup)
                {
                    SQL.AppendLine("    And Exists ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select 1 ");
                    SQL.AppendLine("        From TblGroupCOA ");
                    SQL.AppendLine("        Where GrpCode In ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select GrpCode ");
                    SQL.AppendLine("            From TblUser ");
                    SQL.AppendLine("            Where UserCode = @UserCode ");
                    SQL.AppendLine("        ) ");
                    //SQL.AppendLine("        And AcNo = B.AcNo ");
                    SQL.AppendLine("        And B.AcNo Like Concat(AcNo, '%') ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By A.DNo; ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "AcNo", 
                        "AcDesc", "DAmt", "CAmt", "Remark", "Alias"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void SetVoucherBankAccountCOAAccount()
        {
            try
            {
                Sm.ClearGrd(Grd1, true);

                var cm = new MySqlCommand();
                string mVoucherDocType = Sm.GetValue("Select DocType From TblVoucherHdr Where DocNo = @Param; ", TxtVoucherDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherDocNo.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                var SQL = new StringBuilder();
                if (!mIsVoucherRequestUseCOA || mIsVoucherRequestUseCOA && mVoucherDocTypeManual != mVoucherDocType)
                {
                    SQL.AppendLine("Select A.AcNo, A.AcDesc, 0.00 As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("From TblCOA A ");
                    SQL.AppendLine("Where A.AcNo In (");
                    SQL.AppendLine("    Select COAAcNo ");
                    SQL.AppendLine("    From TblBankAccount ");
                    SQL.AppendLine("    Where BankAcCode In ( ");
                    SQL.AppendLine("        Select BankAcCode From TblVoucherHdr ");
                    SQL.AppendLine("        Where DocNo=@DocNo ");
                    SQL.AppendLine("    ))");
                    if (mIsCOAFilteredByGroup)
                    {
                        SQL.AppendLine("    And Exists ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select 1 ");
                        SQL.AppendLine("        From TblGroupCOA ");
                        SQL.AppendLine("        Where GrpCode In ");
                        SQL.AppendLine("        ( ");
                        SQL.AppendLine("            Select GrpCode ");
                        SQL.AppendLine("            From TblUser ");
                        SQL.AppendLine("            Where UserCode = @UserCode ");
                        SQL.AppendLine("        ) ");
                        //SQL.AppendLine("        And A.AcNo = AcNo ");
                        SQL.AppendLine("        And A.AcNo Like Concat(AcNo, '%') ");
                        SQL.AppendLine("    ) ");
                    }
                }
                else
                {
                    SQL.AppendLine("Select C.AcNo, D.AcDesc, C.DAmt, C.CAmt ");
                    SQL.AppendLine("From TblVoucherHdr A ");
                    SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocno = B.DocNo ");
                    SQL.AppendLine("    And A.DocNo = @DocNo ");
                    SQL.AppendLine("Inner Join TblVoucherRequestDtl2 C On B.DocNo = C.DocNo ");
                    SQL.AppendLine("Inner Join TblCOA D On C.AcNo = D.AcNo ");
                    if (mIsCOAFilteredByGroup)
                    {
                        SQL.AppendLine("    And Exists ");
                        SQL.AppendLine("    ( ");
                        SQL.AppendLine("        Select 1 ");
                        SQL.AppendLine("        From TblGroupCOA ");
                        SQL.AppendLine("        Where GrpCode In ");
                        SQL.AppendLine("        ( ");
                        SQL.AppendLine("            Select GrpCode ");
                        SQL.AppendLine("            From TblUser ");
                        SQL.AppendLine("            Where UserCode = @UserCode ");
                        SQL.AppendLine("        ) ");
                        //SQL.AppendLine("        And AcNo = D.AcNo ");
                        SQL.AppendLine("        And D.AcNo Like Concat(AcNo, '%') ");
                        SQL.AppendLine("    ) ");
                    }
                    SQL.AppendLine("Order By C.DNo; ");
                }

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]{ "AcNo", "AcDesc", "DAmt", "CAmt" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Grd.Cells[Row, 5].Value = null;
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
                Sm.FocusGrd(Grd1, 0, 1);

                ComputeDebetCredit(); ComputeBalanced();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        #endregion

        #region Additional Method

        internal void ShowSOContractData()
        {
            if (!mIsVoucherJournalUseSOContract && !mIsVRUseSOContract && !mIsVRForBudgetUseSOContract) return;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.SOContractDocNo ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            TxtSOContractDocNo.EditValue = Sm.GetValue(SQL.ToString(), TxtVoucherDocNo.Text);
        }

        internal string GetSelectedJournal()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private bool IsItCodeEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length == 0)
            {
                e.DoDefault = false;
                Sm.StdMsg(mMsgType.Warning, "Account is empty.");
                return true;
            }
            return false;
        }

        private void Compute(int Row, int ac)
        {
            switch (ac)
            {
                case 1:
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                    {
                        Grd1.Cells[Row, 4].Value = 0; //Sm.GrdColReadOnly(Grd1, new int[] { 5 });
                    }
                    break;
                case 2:
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                    {
                        Grd1.Cells[Row, 3].Value = 0; //Sm.GrdColReadOnly(Grd1, new int[] { 4});
                    }
                    break;
            }
        }

        internal void ComputeBalanced()
        {
            decimal balanced = 0; decimal debit, credit;
            debit = Convert.ToDecimal(TxtDbt.Text);
            credit = Convert.ToDecimal(TxtCdt.Text);
            balanced = debit - credit;
            TxtBalanced.EditValue = Sm.FormatNum(balanced, 0);
        }

        private void ComputeDebetCredit()
        {
            decimal DebetAmount = 0m; decimal CreditAmount = 0m;
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if(Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                DebetAmount += Sm.GetGrdDec(Grd1, Row, 3);
            }
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                CreditAmount += Sm.GetGrdDec(Grd1, Row, 4);
            }

            TxtDbt.EditValue = Sm.FormatNum(DebetAmount, 0);
            TxtCdt.EditValue = Sm.FormatNum(CreditAmount, 0);
        }

        private string GenerateDocNo()
        {
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherJournal'"),
                type = string.Empty,
                DocSeqNo = "4",
                AcType = Sm.GetValue("Select C.AcType " +
                                "From TblVoucherRequestHdr C  " +
                                "Inner Join TblBankAccount D On C.BankAcCode = D.BankAcCode " +
                                "Where C.DocNo In (Select VoucherRequestDocNo From TblVoucherHdr Where " +
                                "DocNo = '" + TxtVoucherDocNo.Text + "') "),
                BankAcCode = Sm.GetValue("Select C.BankAcCode " +
                                "From TblVoucherRequestHdr C  " +
                                "Inner Join TblBankAccount D On C.BankAcCode = D.BankAcCode " +
                                "Where C.DocNo In (Select VoucherRequestDocNo From TblVoucherHdr Where " +
                                "DocNo = '" + TxtVoucherDocNo.Text + "') ");

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (AcType == "C")
                type = Sm.GetValue("Select ifnull(AutoNoCredit, '') From TblBankAccount Where BankAcCode = '" + BankAcCode + "' ");
            else
                type = Sm.GetValue("Select ifnull(AutoNoDebit, '') From TblBankAccount Where BankAcCode = '" + BankAcCode + "' ");

            var SQL = new StringBuilder();

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNo From TblVoucherJournalHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherJournalHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,"+DocSeqNo+") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("       Select Convert(Substring(DocNo,7," + DocSeqNo + "), Decimal) As DocNo From TblVoucherJournalHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherJournalHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };


        private static string Convert2(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void PrintData(string DocNo)
        {
            var l = new List<JV>();
            var ldtl = new List<JVDtl>();
            var ldtl2 = new List<JVDtl2>();
            var lSignAMKA = new List<CASSignAMKA>();


            string[] TableName = { "JV", "JVDtl", "JVDtl2", "CASSignAMKA" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.VoucherDocNo, A.Remark, A.CurCode, F.CurName, D.UserName, DATE_FORMAT(left(A.CreateDt,8),'%d %M %Y') As CreateDt, ");
            SQL.AppendLine("A.Amt, B.GiroNo, IfNull(E.BankAcNm, '') As BankAcc,  IfNull(E.BankAcNo, '-') As BankAcNo, C.PaymentUser, C.AcType, E.BankAcTp  ");
            SQL.AppendLine("From TblVoucherJournalHdr A ");
            SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C On A.VoucherDocNo = C.VoucherDocNo ");
            SQL.AppendLine("Left Join TblUser D On A.CreateBy=D.UserCode ");
            SQL.AppendLine("Inner Join TblBankAccount E On B.BankAcCode = E.BankAcCode ");
            SQL.AppendLine("Inner Join TblCurrency F On A.CurCode = F.CurCode");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
  
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo",       
                         
                         //6-10
                         "DocDt",
                         "VoucherDocNo",
                         "Remark",
                         "CurName",
                         "UserName",

                         //11-15
                         "CreateDt",
                         "Amt",
                         "GiroNo",
                         "BankAcc",
                         "BankAcNo",

                         //16-19
                         "PaymentUser",
                         "AcType",
                         "BankAcTp",
                         "Curcode"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new JV()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),

                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyLongAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            VoucherDocNo = Sm.DrStr(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            CurName = Sm.DrStr(dr, c[9]),
                            UserName = Sm.DrStr(dr, c[10]),

                            CreateDt = Sm.DrStr(dr, c[11]),

                            Ammount = Sm.DrDec(dr, c[12]),
                            GiroNo = Sm.DrStr(dr, c[13]),
                            BankAcName = Sm.DrStr(dr, c[14]),
                            BankAcNo = Sm.DrStr(dr, c[15]),
                            PaymentUser = Sm.DrStr(dr, c[16]),
                            AcType = Sm.DrStr(dr, c[17]),
                            BankAcTp = Sm.DrStr(dr, c[18]),
                            CurCode = Sm.DrStr(dr, c[19]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),

                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark ");
                SQLDtl.AppendLine("From TblVoucherJournalDtl A Inner Join TblCoa B On A.AcNo = B.AcNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "AcNo" ,

                         //1-4
                         "AcDesc" ,
                         "DAmt",
                         "CAmt" ,
                         "Remark",

                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new JVDtl()
                        {
                            AcNo = Sm.DrStr(drDtl, cDtl[0]),
                            AcDesc = Sm.DrStr(drDtl, cDtl[1]),

                            DAmt = Sm.DrDec(drDtl, cDtl[2]),
                            CAmt = Sm.DrDec(drDtl, cDtl[3]),
                            Remark = Sm.DrStr(drDtl, cDtl[4]),
                          
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail data 2
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select X.DocNo, X.AcNo, sum(X.DAmt)As DAmt, sum(X.CAmt)As CAmt from ");
                SQLDtl2.AppendLine("( ");
                SQLDtl2.AppendLine("Select A.DocNo, A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark ");
                SQLDtl2.AppendLine("From TblVoucherJournalDtl A Inner Join TblCoa B On A.AcNo = B.AcNo ");
                SQLDtl2.AppendLine(")X ");
                SQLDtl2.AppendLine("Where X.DocNo=@DocNo ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "AcNo",

                         "DAmt",
                         "CAmt",

                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new JVDtl2()
                        {
                            AcNo = Sm.DrStr(drDtl2, cDtl2[0]),

                            DAmt = Sm.DrDec(drDtl2, cDtl2[1]),
                            CAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(drDtl2, cDtl2[2])),
                            Terbilang2 = Convert2(Sm.DrDec(drDtl2, cDtl2[2])),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Signature AMKA

            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();
            

            SQL4.AppendLine("SELECT T1.EmpPict1, T1.UserCode1, T1.UserName1, T1.ApproveDt1, T2.EmpPict2, T2.UserCode2, T2.UserName2, T2.ApproveDt2, EmpPict3, T3.UserCode3, T3.Username3, T3.ApproveDt3, EmpPict4, T4.UserCode4, T4.Username4, T4.ApproveDt4, EmpPict5, T5.UserCode5, T5.Username5, T5.CreateDt  ");
            SQL4.AppendLine("FROM ");
            SQL4.AppendLine("( ");
            SQL4.AppendLine("      SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') AS EmpPict1, D.UserCode AS UserCode1, F.UserName AS UserName1, DATE_FORMAT(LEFT(IFNULL(C.LastUpDt, C.CreateDt), 8), '%d/%m/%Y') ApproveDt1 ");
            SQL4.AppendLine("     FROM tblvoucherjournalhdr A ");
            SQL4.AppendLine("	  INNER JOIN tblvoucherhdr B ON A.VoucherDocNo = B.DocNo ");
            SQL4.AppendLine("	  INNER JOIN tblvoucherrequesthdr C ON B.VoucherRequestDocNo = C.DocNo ");
            SQL4.AppendLine("	  inner join TblDocApproval D ON C.DocNo = D.DocNo ");
            SQL4.AppendLine("     INNER JOIN TblDocApprovalSetting E ON D.DocType = E.DocType  ");
            SQL4.AppendLine("         AND E.DNo = D.ApprovalDNo  ");
            SQL4.AppendLine("         And E.Level = 1  ");
            SQL4.AppendLine("         AND D.Status = 'A'  ");
            SQL4.AppendLine("     INNER JOIN TblUser F ON D.UserCode = F.UserCode  ");
            SQL4.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature'  ");
            SQL4.AppendLine("     WHERE A.DocNo = @DocNo ");
            SQL4.AppendLine(") T1  ");
            SQL4.AppendLine("INNER JOIN  ");
            SQL4.AppendLine("(  ");
            SQL4.AppendLine("      SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') AS EmpPict2, D.UserCode AS UserCode2, F.UserName AS UserName2, DATE_FORMAT(LEFT(IFNULL(C.LastUpDt, C.CreateDt), 8), '%d/%m/%Y') ApproveDt2  ");
            SQL4.AppendLine("     FROM tblvoucherjournalhdr A  ");
            SQL4.AppendLine("	  INNER JOIN tblvoucherhdr B ON A.VoucherDocNo = B.DocNo ");
            SQL4.AppendLine("	  INNER JOIN tblvoucherrequesthdr C ON B.VoucherRequestDocNo = C.DocNo ");
            SQL4.AppendLine("	  inner join TblDocApproval D ON C.DocNo = D.DocNo ");
            SQL4.AppendLine("     INNER JOIN TblDocApprovalSetting E ON D.DocType = E.DocType  ");
            SQL4.AppendLine("         AND E.DNo = D.ApprovalDNo  ");
            SQL4.AppendLine("         And E.Level = 2  ");
            SQL4.AppendLine("         AND D.Status = 'A'  ");
            SQL4.AppendLine("     INNER JOIN TblUser F ON D.UserCode = F.UserCode  ");
            SQL4.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature'  ");
            SQL4.AppendLine("     WHERE A.DocNo = @DocNo ");
            SQL4.AppendLine(") T2 ON T1.DocNo = T2.DocNo  ");
            SQL4.AppendLine("INNER JOIN  ");
            SQL4.AppendLine("(  ");
            SQL4.AppendLine("      SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') AS EmpPict3, D.UserCode AS UserCode3, F.UserName AS UserName3, DATE_FORMAT(LEFT(IFNULL(C.LastUpDt, C.CreateDt), 8), '%d/%m/%Y') ApproveDt3  ");
            SQL4.AppendLine("     FROM tblvoucherjournalhdr A  ");
            SQL4.AppendLine("	  INNER JOIN tblvoucherhdr B ON A.VoucherDocNo = B.DocNo ");
            SQL4.AppendLine("	  INNER JOIN tblvoucherrequesthdr C ON B.VoucherRequestDocNo = C.DocNo ");
            SQL4.AppendLine("	  inner join TblDocApproval D ON C.DocNo = D.DocNo ");
            SQL4.AppendLine("     INNER JOIN TblDocApprovalSetting E ON D.DocType = E.DocType  ");
            SQL4.AppendLine("         AND E.DNo = D.ApprovalDNo  ");
            SQL4.AppendLine("         And E.Level = 3  ");
            SQL4.AppendLine("         AND D.Status = 'A'  ");
            SQL4.AppendLine("     INNER JOIN TblUser F ON D.UserCode = F.UserCode  ");
            SQL4.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature'  ");
            SQL4.AppendLine("     WHERE A.DocNo = @DocNo ");
            SQL4.AppendLine(")T3 ON T1.DocNo = T3.DocNo  ");
            SQL4.AppendLine("INNER JOIN  ");
            SQL4.AppendLine("(  ");
            SQL4.AppendLine("      SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), F.UserCode, '.JPG') AS EmpPict4, D.UserCode AS UserCode4, F.UserName AS UserName4, DATE_FORMAT(LEFT(IFNULL(C.LastUpDt, C.CreateDt), 8), '%d/%m/%Y') ApproveDt4  ");
            SQL4.AppendLine("     FROM tblvoucherjournalhdr A  ");
            SQL4.AppendLine("	  INNER JOIN tblvoucherhdr B ON A.VoucherDocNo = B.DocNo ");
            SQL4.AppendLine("	  INNER JOIN tblvoucherrequesthdr C ON B.VoucherRequestDocNo = C.DocNo ");
            SQL4.AppendLine("	  inner join TblDocApproval D ON C.DocNo = D.DocNo ");
            SQL4.AppendLine("     INNER JOIN TblDocApprovalSetting E ON D.DocType = E.DocType  ");
            SQL4.AppendLine("         AND E.DNo = D.ApprovalDNo  ");
            SQL4.AppendLine("         And E.Level = 4  ");
            SQL4.AppendLine("         AND D.Status = 'A'  ");
            SQL4.AppendLine("     INNER JOIN TblUser F ON D.UserCode = F.UserCode  ");
            SQL4.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature'  ");
            SQL4.AppendLine("     WHERE A.DocNo = @DocNo ");
            SQL4.AppendLine(")T4 ON T1.DocNo = T4.DocNo  ");
            SQL4.AppendLine("LEFT JOIN  ");
            SQL4.AppendLine("( ");
            SQL4.AppendLine("	 SELECT A.DocNo, Concat(IfNull(C.ParValue, ''), B.UserCode, '.JPG') AS EmpPict5,  ");
            SQL4.AppendLine("     B.UserCode AS UserCode5, B.UserName AS UserName5, DATE_FORMAT(LEFT(IFNULL(A.LastUpDt, A.CreateDt), 8), '%d/%m/%Y') CreateDt  ");
            SQL4.AppendLine("     FROM tblvoucherjournalhdr A ");
            SQL4.AppendLine("     INNER JOIN tbluser B ON A.CreateBy = B.UserCode ");
            SQL4.AppendLine("     LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL4.AppendLine("	  WHERE A.DocNo = @DocNo  ");
            SQL4.AppendLine(")T5 ON T1.DocNo =  T5.DocNo ");



            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();
                Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[]
            {
                    //0

                    "EmpPict1",
                    "UserCode1",
                    "Username1",
                    "ApproveDt1",

                    "EmpPict2",
                    "UserCode2",
                    "Username2",
                    "ApproveDt2",

                    "EmpPict3",
                    "UserCode3",
                    "Username3",
                    "ApproveDt3",

                    "EmpPict4",
                    "UserCode4",
                    "Username4",
                    "ApproveDt4",

                    "EmpPict5",
                    "UserCode5",
                    "Username5",
                    "CreateDt",
            });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        lSignAMKA.Add(new CASSignAMKA()
                        {
                            EmpPict1 = Sm.DrStr(dr4, c4[0]),
                            UserCode1 = Sm.DrStr(dr4, c4[1]),
                            UserName1 = Sm.DrStr(dr4, c4[2]),
                            ApproveDt1 = Sm.DrStr(dr4, c4[3]),
                            EmpPict2 = Sm.DrStr(dr4, c4[4]),
                            UserCode2 = Sm.DrStr(dr4, c4[5]),
                            UserName2 = Sm.DrStr(dr4, c4[6]),
                            ApproveDt2 = Sm.DrStr(dr4, c4[7]),
                            EmpPict3 = Sm.DrStr(dr4, c4[8]),
                            UserCode3 = Sm.DrStr(dr4, c4[9]),
                            UserName3 = Sm.DrStr(dr4, c4[10]),
                            ApproveDt3 = Sm.DrStr(dr4, c4[11]),
                            EmpPict4 = Sm.DrStr(dr4, c4[12]),
                            UserCode4 = Sm.DrStr(dr4, c4[13]),
                            UserName4 = Sm.DrStr(dr4, c4[14]),
                            ApproveDt4 = Sm.DrStr(dr4, c4[15]),
                            EmpPict5 = Sm.DrStr(dr4, c4[16]),
                            UserCode5 = Sm.DrStr(dr4, c4[17]),
                            UserName5 = Sm.DrStr(dr4, c4[18]),
                            CreateDt = Sm.DrStr(dr4, c4[19]),
                        });
                    }
                }
                dr4.Close();
            }
            myLists.Add(lSignAMKA);

            #endregion

            if(mDocTitle == "BBT")
                Sm.PrintReport("VoucherJournalBBT", myLists, TableName, false);
            else
                Sm.PrintReport("VoucherJournalAMKA", myLists, TableName, false);
        }

        
        #endregion

        #endregion

        #region Event

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherJournalDlg(this));
        }

        private void BtnVoucherJournalDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherJournalDlg3(this));
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void BtnShowVoucher_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo.Text;
                    //Sm.SetFormFindSetting(this, f);
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void BtnVoucherJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Journal voucher#", false))
            {
                try
                {
                    var f = new FrmVoucherJournal("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherJournalDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && mIsVoucherJournalUseSOContract)
            {
                Sm.FormShowDialog(new FrmVoucherJournalDlg4(this));
            }
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (mIsVoucherJournalUseSOContract && !Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                var f = new FrmSOContract2("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOContractDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnCostCenter_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherJournalDlg5(this));
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Report Class

        private class JV
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string VoucherDocNo { get; set; }
            public string Remark { get; set; }
            public string CurName { get; set; }
            public string UserName { get; set; }
            public string CreateDt { get; set; }
            public string PrintBy { get; set; }
            public decimal Ammount { get; set; }
            public string GiroNo { get; set; }
            public string BankAcName { get; set; }
            public string BankAcNo { get; set; }
            public string PaymentUser { get; set; }
            public string AcType { get; set; }
            public string BankAcTp { get; set; }
            public string CurCode { get; set; }

        }

        private class JVDtl
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Remark { get; set; }
        }

        private class JVDtl2
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
        }

        private class CASSignAMKA
        {
            public string EmpPict1 { get; set; }
            public string UserCode1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }
            public string EmpPict2 { get; set; }
            public string UserCode2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }
            public string EmpPict3 { get; set; }
            public string UserCode3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }
            public string EmpPict4 { get; set; }
            public string UserCode4 { get; set; }
            public string UserName4 { get; set; }
            public string ApproveDt4 { get; set; }

            public string EmpPict5 { get; set; }
            public string UserCode5 { get; set; }
            public string UserName5 { get; set; }
            public string CreateDt { get; set; }
        }


        #endregion
    }
}
