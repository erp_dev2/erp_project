﻿#region Update
/*
    29/04/2021 [BRI/SIER] tambah dialog untuk customer dan customer category berdasarkan param IsShowCustomercategory
    04/05/2021 [BRI/SIER] feedback dialog customer dan customer category
    10/05/2021 [BRI/SIER] customer category berdasarkan group yang login
 *  23/07/2021 [SET/SIER] Combo Box pilihan CUSTOMER (Header) diberi validasi hanya Customer yang Quotation-nya CBD yang muncul sebagai pilihan di menu DR CBD
 */

#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;
using System.Net;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;


#endregion

namespace RunSystem
{
    public partial class FrmDRDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDR mFrmParent;        

        #endregion

        #region Constructor

        public FrmDRDlg3(FrmDR FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "Customer Data";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueCtCtCode(ref LueCtCtName);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "Agent",
                        "Short Code",

                        //6-10
                        "Category",
                        "Group",
                        "Address",
                        "Postal Code",
                        "City",
                        
                        //11-15
                        "Country",
                        "Phone",
                        "Fax",
                        "Mobile",
                        "Email",
                        
                        //16-20
                        "NPWP",
                        "Contact Person",
                        "NIB",
                        "Remark",
                        "Created"+Environment.NewLine+"By",   
                        
                        //21-25
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",

                        //26
                        "Category Code"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 300, 60, 60, 100, 
                        
                        //6-10
                        200, 200, 300, 90, 200, 
                        
                        //11-15
                        200, 120, 120, 120, 150, 

                        //16-20
                        150, 300, 130, 300, 100, 

                        //21-25
                        100, 100, 100, 100, 100,

                        //26
                        0
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 21, 24 });
            Sm.GrdFormatTime(Grd1, new int[] { 22, 25 });
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24, 25, 26 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24, 25 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode, A.CtName, A.ActInd, A.AgentInd, A.CtShortCode, D.CtCtName, F.CtGrpName, ");
            SQL.AppendLine("A.Address, A.PostalCd, B.CityName, C.CntName, ");
            SQL.AppendLine("A.Phone, A.Fax, A.Mobile, A.Email, A.NPWP, E.ContactPerson, A.NIB, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, D.CtCtCode ");
            SQL.AppendLine("From TblCustomer A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.CntCode=C.CntCode ");
            SQL.AppendLine("Left Join TblCustomerCategory D On A.CtCtCode = D.CtCtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, ");
            SQL.AppendLine("    Group_Concat(Concat(ContactPersonName, Case When Position Is Null Then '' Else Concat(' (', Position, ')') End) Order By ContactPersonName Separator ', ') As ContactPerson ");
            SQL.AppendLine("    From TblCustomerContactPerson ");
            SQL.AppendLine("    Group By CtCode ");
            SQL.AppendLine(") E On A.CtCode=E.CtCode ");
            SQL.AppendLine("Left Join TblCustomerGroup F On A.CtGrpCode=F.CtGrpCode ");
            SQL.AppendLine("Left Join TblCtQtHdr G On A.CtCode = G.CtCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' And A.CtCode In ");
            SQL.AppendLine("(Select Distinct CtCode From TblSOHdr Where Status In ('O', 'P') And OverSeaInd = 'N') " + Filter);
            if (mFrmParent.mIsFilterByCtCt)
            {
                SQL.AppendLine("And (A.CtCtCode Is Null Or (A.CtCtCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=A.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ))) ");
            }
            if (mFrmParent.mIsDRCBDCtBasedOnCBDQuotation)
                SQL.AppendLine("And G.PtCode = 'CBD' ");
            SQL.AppendLine("Order By A.CtName; ");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCtName.Text, new string[] { "A.CtCode", "A.CtName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCtName), "D.CtCtCode", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[] 
                        { 
                              //0
                            "CtCode", 
                                
                            //1-5
                            "CtName", "ActInd", "AgentInd", "CtShortCode", "CtCtName", 
                            
                            //6-10
                            "CtGrpName", "Address", "PostalCd", "CityName", "CntName", 
                            
                            //11-15
                            "Phone", "Fax", "Mobile", "Email", "NPWP", 
                            
                            //16-20
                            "ContactPerson", "NIB", "Remark", "CreateBy", "CreateDt", 
                            
                            //21-23
                            "LastUpBy", "LastUpdt", "CtCtCode"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                var CtCtCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 26);
                var CtCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.LueCtCtCode.EditValue = CtCtCode;
                mFrmParent.LueCtCode.EditValue = CtCode;
                this.Close();
            }
        }

        #endregion

        #region Additional Method

        private void SetLueCtCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T1.CtCtCode As Col1, T1.CtCtName As Col2 ");
            SQL.AppendLine("FROM tblcustomercategory T1 ");
            if (mFrmParent.mIsFilterByCtCt)
            {
                SQL.AppendLine("WHERE  (T1.CtCtCode Is Null OR (T1.CtCtCode Is Not Null And Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("        Where CtCtCode=T1.CtCtCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order By T1.CtCtName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Event

        private void TxtCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer#");
        }

        private void LueCtCtName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtName, new Sm.RefreshLue1(SetLueCtCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer's Category#");
        }

        #endregion

        #endregion
    }
}
