﻿#region Update
/*
    10/03/2020 [TKG/IMS] New Application
    26/03/2020 [TKG/IMS] DO To Customer  verification bisa memproses data DO To Customer secara partial.
    16/06/2020 [DITA/IMS] menampilkan SOC Remark, param --> IsSalesTransactionShowSOContractRemark
    09/07/2020 [HAR/IMS] SO Contract Remark ambil dari detail
    29/01/2021 [WED/IMS] tarik DO To Customer biasa
    24/02/2021 [HAR/IMS] BUG : di docthdr tidak ada kolom localdocument adanya docnointernal sehingga ada bug Filter
    25/02/2021 [WED/IMS] tambah kolom nomor PO, nama proyek, kode proyek
    28/04/2021 [WED/IMS] DO Customer biasa yang muncul disini hanya yg Progress nya di centang
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOCtVerifyDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCtVerify mFrmParent;
        
        #endregion

        #region Constructor

        public FrmDOCtVerifyDlg(FrmDOCtVerify FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo, A.DocType, B.DocDt, B.LocalDocNo, C.CtName, D.WhsName, B.Remark, ");
            
            if (mFrmParent.mIsSalesTransactionShowSOContractRemark)
                SQL.AppendLine("B.SOCRemark, ");
            else
                SQL.AppendLine("Null As SOCRemark, ");

            SQL.AppendLine("Group_Concat(Distinct IfNull(E.PONo, '')) PONo, ");
            SQL.AppendLine("Group_Concat(Distinct IfNull(H.ProjectCode, '')) ProjectCode, ");
            SQL.AppendLine("Group_Concat(Distinct IfNull(H.ProjectName, '')) ProjectName ");

            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct T1.DocNo, '0' As DocType, T1.CtCode, T1.WhsCode ");
            SQL.AppendLine("    From TblDOCt2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Qty-T2.DOCtVerifyQty>0.00 ");
            SQL.AppendLine("        And T2.SOContractDocNo Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct T1.DocNo, '1' As DocType, T1.CtCode, T1.WhsCode ");
            SQL.AppendLine("    From TblDOCtHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.ProcessInd = 'F' And T1.ProgressInd = 'Y' And T1.ServiceInd = 'N' ");
            SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Qty-T2.DOCtVerifyQty>0.00 ");
            SQL.AppendLine("        And T1.SOContractDocNo Is Not Null ");
            SQL.AppendLine(Filter.Replace("T1.LocalDocNo", "T1.DocNoInternal"));
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T1.DocDt, T1.LocalDocNo, T1.CtCode, T1.Remark, T1.WhsCode, Group_Concat(Distinct IfNull(T3.Remark, '')) SOCRemark, Group_Concat(Distinct IfNull(T2.SOContractDocNo, '')) SOContractDocNo ");
            SQL.AppendLine("    From TblDOCt2Hdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DRDocNo Is Not Null ");
            SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Left Join TblSOContractDtl T3 On T2.SOContractDocNo = T3.DocNo And T2.SOContractDNo = T3.DNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Group By T1.DocNo, T1.DocDt, T1.LocalDocNo, T1.CtCode, T1.Remark, T1.WhsCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T1.DocNo, T1.DocDt, T1.DocNoInternal LocalDocNo, T1.CtCode, T1.Remark, T1.WhsCode, Group_Concat(Distinct IfNull(T3.Remark, '')) SOCRemark, Group_Concat(Distinct IfNull(T1.SOContractDocNo, '')) SOContractDocNo ");
            SQL.AppendLine("    From TblDOCtHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.SOContractDOcNo Is Not Null And T1.ProcessInd = 'F' And T1.ProgressInd = 'Y' And T1.ServiceInd = 'N' ");
            SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Inner Join TblSOContractDtl T3 On T1.SOContractDocNo = T3.DocNo And T2.SOContractDNo = T3.DNo ");
            SQL.AppendLine(Filter.Replace("T1.LocalDocNo", "T1.DocNoInternal"));
            SQL.AppendLine("    Group By T1.DocNo, T1.DocDt, T1.DocNoInternal, T1.CtCode, T1.Remark, T1.WhsCode ");
            SQL.AppendLine(") B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On B.WhsCode=D.WhsCode ");
            SQL.AppendLine("Left Join TblSOContractHdr E On Find_In_Set(E.DocNo, B.SOContractDocNo) ");
            SQL.AppendLine("Left Join TblBOQHdr F On E.BOQDocNo = F.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr G On F.LOPDocNo = G.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup H On G.PGCode = H.PGCode ");
            SQL.AppendLine("Group By A.DocNo, A.DocType, B.DocDt, B.LocalDocNo, C.CtName, D.WhsName, B.Remark ");
            SQL.AppendLine("Order By B.DocDt Desc, B.DocNo; ");

            return SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Local#",
                    "Customer",

                    //6-10
                    "Customer PO#",
                    "Project Code",
                    "Project Name",
                    "Warehouse",
                    "Remark",

                    //11-12
                    "SO Contract"+ Environment.NewLine + "Remark",
                    "DocType",
                },
                new int[]
                {
                    50,
                    150, 20, 80, 150, 200,
                    120, 130, 250, 200, 200, 
                    200, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 9 }, !ChkHideInfoInGrd.Checked);
            if (!mFrmParent.mIsSalesTransactionShowSOContractRemark) Sm.GrdColInvisible(Grd1, new int[] { 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12) == "1")
                {
                    e.DoDefault = false;
                    var f = new FrmDOCt8(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    e.DoDefault = false;
                    var f = new FrmDOCt9(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 12) == "1")
                {
                    var f = new FrmDOCt8(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDOCt9(mFrmParent.mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDOCt2DocNo.Text, new string[] { "T1.DocNo", "T1.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T1.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "T1.WhsCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "LocalDocNo", "CtName", "PONo", "ProjectCode", 

                        //6-10
                        "ProjectName", "WhsName", "Remark", "SOCRemark", "DocType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                var r = Grd1.CurRow.Index;
                var DocNo = Sm.GetGrdStr(Grd1, r, 1);
                mFrmParent.TxtDOCt2DocNo.EditValue = DocNo;
                mFrmParent.TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, r, 4);
                mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, r, 5);
                mFrmParent.TxtWhsCode.EditValue = Sm.GetGrdStr(Grd1, r, 9);
                mFrmParent.ShowDOCt2Info(DocNo);
                this.Close();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtDOCt2DocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDOCt2DocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO To Customer#");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        #endregion

        #endregion

    }
}