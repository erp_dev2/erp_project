﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmWorkingGroup : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmWorkingGroupFind FrmFind;

        #endregion

        #region Constructor
         
        public FrmWorkingGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Working Group";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);

            base.FrmLoad(sender, e);
            SetLueSectionCode(ref LueSectionCode);
           
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
          if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrpCode, ChkActInd, TxtGrpName, LueSectionCode
                    }, true);
                    TxtGrpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtGrpCode, TxtGrpName, LueSectionCode
                    }, false);
                    TxtGrpCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                          ChkActInd, TxtGrpName, LueSectionCode
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    TxtGrpName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtGrpCode, TxtGrpName, LueSectionCode
            });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
          if (FrmFind == null) FrmFind = new FrmWorkingGroupFind(this);
          Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGrpCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblWorkingGroup(WorkGroupCode, WorkGroupName, ActInd, SectionCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@WorkGroupCode, @WorkGroupName, @ActInd, @SectionCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("Update WorkGroupName=@WorkGroupName, ActInd=@ActInd, SectionCode=@SectionCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@WorkGroupCode", TxtGrpCode.Text);
                Sm.CmParam<String>(ref cm, "@WorkGroupName", TxtGrpName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@SectionCode", Sm.GetLue(LueSectionCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtGrpCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string GrpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@WorkGroupCode", GrpCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select WorkGroupCode, WorkGroupName, ActInd, SectionCode From TblWorkingGroup Where WorkGroupCode=@WorkGroupCode ",
                        new string[] 
                        {
                            "WorkGroupCode", "WorkGroupName", "ActInd", "SectionCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtGrpCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtGrpName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            Sm.SetLue(LueSectionCode, Sm.DrStr(dr, c[3]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtGrpCode, "Working group code", false) ||
                Sm.IsTxtEmpty(TxtGrpName, "Working group name", false) ||
                Sm.IsLueEmpty(LueSectionCode, "Section") ||
                IsCityCodeExisted() ||
                IsDataInactiveAlready();
        }

        private bool IsCityCodeExisted()
        {
            if (!TxtGrpCode.Properties.ReadOnly && Sm.IsDataExist("Select WorkGroupCode From TblWorkingGroup Where WorkGroupCode='" + TxtGrpCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Working group code ( " + TxtGrpCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsDataInactiveAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select WorkGroupCode From TblWorkingGroup " +
                    "Where ActInd='N' And WorkGroupCode=@WorkGroupCode;"
            };
            Sm.CmParam<String>(ref cm, "@WorkGroupCode", TxtGrpCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already not actived.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void SetLueSectionCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SectionCode As Col1, SectionName As Col2 From TblSection Where ActInd='Y'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void LueSectionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSectionCode, new Sm.RefreshLue1(SetLueSectionCode));
        }
        private void TxtGrpCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtGrpCode);
        }

        private void TxtGrpName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtGrpName);
        }

        #endregion
       
    }
}
