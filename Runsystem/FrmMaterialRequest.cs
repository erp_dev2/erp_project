﻿#region Update

#region Old
/*
    20/06/2017 [WED] tambah inputan DO Request DocNo di header
    20/06/2017 [WED] Item hanya ambil dari DO Request jika parameter IsDORequestNeedStockValidation = N
    26/07/2017 [TKG] Kalau menggunakan budget, kalau item tsb tidak memiliki estimated price berdasarkan quotation maka warna item name menjadi merah.
    01/08/2017 [WED] item dari DO Request yg masuk ke MR itu yg RequestedQty > All Stock berdasarkan Whs DORequest
    24/08/2017 [HAR] munculin nomor PO cancel saat show data  
    20/09/2017 [ARI] munculin item-item dari master item berdasarkan parameter mIsDORequestUseItemIssued
    03/12/2017 [TKG] Copy remark di detail.
    28/12/2017 [HAR] tambah inputan currency dan estimated price di detail berdasarkan parameter
    15/01/2018 [TKG] Department difilter berdasarkan site untuk customer yg menggunakan budget
    16/01/2018 [ARI] tambah printout KIM
    22/01/2018 [WED] tambah printout TWC
    23/01/2018 [WED] parameter baru untuk TWC, apakah print out menggunakan ttd digital atau tidak
    14/03/2018 [ARI] tambah button info approval
    19/03/2018 [HAR] tanbah button buat Upload file dan download file
    21/03/2018 [HAR] Bug saat parameter upload = Y tpi tidak ada file yang dilampirkan
    12/04/2018 [HAR] Bug saat pilih file tapi ga jadi, tambah untick buat clear filetext
    17/04/2018 [HAR] Bug di port server saat download file
    08/05/2018 [HAR] Bug saat ambil DO (item otomatis ke isi) namun  budget tidak berkurang 
    08/08/2018 [TKG] Budget yg tersedia menghitung PO yg diputihkan juga.
    15/08/2018 [TKG] dibandingkan dengan budget baik yg belum maupun sudah diapprove.
    20/08/2018 [WED] tambah informasi Local Document untuk print out TWC
    24/08/2018 [HAR] BUG  salah ambil kolom item sub category
    02/09/2018 [TKG] Menampilkan nomor urut berdasarkan parameter
    29/11/2018 [TKG] menambah validasi maximum stock berdasarkan parameter IsMaterialRequestMaxStockValidationEnabled
    04/04/2019 [DITA] data Budget Harusnya angka setelah di save PO sama dengan saat sudah terkurangai, yang tampil di MR
    09/04/2019 [MEY] tambah field PIC
    15/04/2019 [WED] Department non-aktif masih muncul
    18/04/2019 [DITA] Bug saat akan memilih lue department
    13/05/2019 [WED] Bug cek approval setting
    14/05/2019 [WED] validasi Budget v PO, melihat Grand Total PO (minus TaxAmt & CustomsTaxAmt)
    26/06/2019 [TKG] tambah informasi dari dropping request
    04/07/2019 [TKG] Untuk MR berdasarkan project impelemtation, menggunakan validasi dropping request amount.
    24/07/2019 [TKG] Adjustment untuk kebutuhan training YK : Qty yang dari Dropping Request tidak bisa digonta ganti
    25/07/2019 [WED] validasi MR yang tanpa Dropping Request
    01/11/2019 [DITA/IMS] tambah informasi Specifications
    08/11/2019 [TKG/SIER] MR SPPJB
    11/11/2019 [TKG/SIER] divalidasi dengan site
    12/11/2019 [TKG/SIER] ditambah validasi kalau job's currency dan estimated currency berbeda.
    13/11/2019 [TKG/SIER] untuk MR SPPJB cuma bisa memilih 1 item
    12/12/2019 [HAR/VIR] Feedback : ada tambahan parameter untuk setting quotation vendor otomatis atau tidak mIsMRAutomaticGetLastVendorQuotation
    09/01/2020 [DITA/SIER] Bug saat compute Avaliable Budget
    10/01/2020 [VIN/SIER] Printout SPP dan SPPJB
    16/01/2020 [HAR/KMI] tambah parameter IsMRBudgetBasedOnBudgetCategory utk membedakan budget berdasarkan budget category apa tidak
    28/01/2020 [HAR/TWC] BUG waktu compute availabale budget blm memfiltyer budget category
    24/02/2020 [DITA/SIER] MR tambah duration
    04/03/2020 [VIN/SIER] tambah upload multi attachment
    05/03/2020 [WED/SIER] budget di potong sesuai estimated price berdasarkan parameter IsBudgetCalculateFromEstimatedPrice
    11/03/2020 [HAR/TWC] BUG : budget masih ambil dari budget request, diganti ambil dari budget summary
    30/03/2020 [HAR/SIER] BUG : printout SPPJB PARPrint select Amt di budgetsummary
    14/04/2020 [DITA/SIER] Tambah duration di printout materialrequestsppjb
    28/04/2020 [IBL/SIER] di List BOQ Material Request SPPJB tambah quantity
    28/04/2020 [VIN/SIER] Penyesuaian printout MR SPPJB
    30/04/2020 [IBL/SIER] Format DocNo baru MR Standar dg parameter IsMaterialRequestDocNoUseDifferentAbbr
    30/04/2020 [IBL/SIER] Format DocNo baru MR SPPJB dg parameter IsMaterialRequestSPPJBDocNoUseDifferentAbbr
    06/05/2020 [WED/IMS] available budget menghitung juga dari VR Budget
    06/05/2020 [WED/SIER] nomor dokumen masih error jika division short code masih kosong
    11/05/2020 [VIN/SIER] bisa upload file dengan attachment rar/zip
    11/05/2020 [VIN/SIER] parameter baru IsUsageDateMaterialRequestMandatory
    11/05/2020 [VIN/SIER] Meng Hide kolom di MR Request biasa,   Minimum Stock, Reoder Point, Dropping Request’s Amount
    12/05/2020 [DITA/SIER] Pengaturan Budget berdasarkan tahun dengan parametr baru mIsBudget2YearlyFormat
    29/05/2020 [VIN/IMS] Kasbon, sppd, gaji harus bisa nyambung ke budgeting operasional
    01/07/2020 [IBL/IMS] budget bisa menyimpan tipe cash advance di voucher request param --> VoucherDocTypeBudget
    01/07/2020 [IBL/IMS] menghubungkan budget dengan cash advance settlement
    08/07/2020 [WED/SRN] print out berdasarkan parameter FormPrintOutMaterialRequest
    10/07/2020 [WED/YK] parameter ItGrpCodeNotShowOnMaterialRequest untuk membatasi Group yg muncul
    13/07/2020 [DITA/SIER] Bug : printout mrsppjb --> pemakaian(usageamt) belum tampil --> ini harusnya ambil dari
    14/07/2020 [ICA/SIER] Tambah Parameter IsRemarkMRNotMandatory.
    14/07/2020 [ICA/SIER] Menambah kolom Total Price dengan parameter IsMRShowTotalPrice
    15/07/2020 [ICA/SIER] Menyembunyikan kolom Quotation# dengan parameter IsMRQuotationNotShow
    15/07/2020 [ICA/SIER] Tambah parameter IsMRItemGroupNotShow
    28/07/2020 [DITA/SIER] Penyesuaian Printout MR SIER, SPPJB dan SPPJB2
    20/09/2020 [TKG/IMS] Berdasarkan parameter IsDocNoFormatUseFullYear, dokumen# menampilkan tahun secara lengkap (Misal : 2020).
    01/10/2020 [IBL/IMS] Printout disamakan dengan PR for production
    05/10/2020 [IBL/IMS] Barang & jasa dibuat satu printout
    07/10/2020 [IBL/IMS] Feedback: Data yg cancel tidak ikut ketarik di printout
    07/10/2020 [TKG/IMS] GenerateDocNo reset nomor urut per tahun
    27/10/2020 [ICA/IMS] Mengubah Material Request menjadi Purchase Request
    01/12/2020 [IBL/SIER] Kolom duration menjadi mandatory berdasarkan parameter IsMRDurationMandatory
    02/12/2020 [IBL/SIER] Approval dibuat berdasarkan Start Amount pada document approval setting. Berdasarkan parameter IsMRApprovalByAmount
    07/12/2020 [ICA/SIER] Menyembunyikan kolom UsageDate berdasarkan parameter IsUsageDateMaterialRequestMandatory
    05/01/2021 [VIN/SIER] Lue PIC dibatasi --> yang muncul user yang login 
    14/01/2021 [TKG/KIM] merubah query untuk tanda tangan printout (apabila ada perubahan jumlah approval)
    20/01/2021 [WED/SIER] tambah ambil dari Cash Advance Settlement berdasarkan parameter IsCASUsedForBudget
    14/04/2021 [ICA/KIM] menambah checklist For Tender based on parameter IsMRWithTenderEnabled
    16/06/2021 [VIN/ALL] Bug Generate DocNo
    22/07/2021 [WED/PHT] ComputeAvailableBudget ambil dari StdMtd
    06/08/2021 [WED/PADI] tambah inputan Sector & Expired Date berdasarkan parameter IsUseECatalog
    23/08/2021 [WED/PADI] tambah payment term saat isi item di detail, berdasarkan parameter IsUseECatalog
    25/08/2021 [WED/PADI] tambah informasi Provinsi dan Delivery Location di setiap item berdasarkan parameter IsUseEcatalog
    29/09/2021 [RIS/PHT] Menambah procurement type berdasarkan parameter IsMRUseProcurementType
    05/10/2021 [RDA/PHT] perubahan label dan penambahan field upload file berdasarkan parameter LabelFileforMR
    07/10/2021 [RDA/PHT] penambahan file mandatory sesuai parameter MandatoryFileForMR
    14/10/2021 [NJP/PHT] penambahan Tab Review yang menampilkan Data Item dan approvalnya, dengan Parameter IsMRUseReview
    15/10/2021 [NJP/PHT] Menambahkan Field Reference dan FrmMaterialRequestDlg8 yg menampilkan data MR yg di cancel, dengan parameter mIsMRUseReference
    15/10/2021 [NJP/PHT] Menambahkan Parameter IsMRLocalDocNoFromReference, akan otomatis mengisi Local DOcNo sesuai LocalDocNo Reference nya Jika Nilai parameternya Y
    21/10/2021 [NJP/ALL] Jika Parameter IsUseECatalog bernilai Y, akan menampilkan Check Box ChkRmInd
    22/10/2021 [NJP/ALL] Jika Parameter IsUseECatalog bernilai Y, Check Box ChkRmInd otomatis tercentang saat insert, ada tambahan konfirmasi jika parameter IsUseECatalog bernilai Y dan Check Box ChkRMInd di centang saat mau save data.
    26/10/2021 [IBL/PHT] Ketika memilih dokumen MR Reference, semua field di header readonly kecuali remark. Data header ambil dari dokumen MR Reference yg dipilih.
    26/10/2021 [IBL/PHT] Tab Review ambil informasi approval dari dokumen MR Reference yg dipilih.
    17/11/2021 [IBL/PHT] Local docno otomatis = DocNo MR ketika Reference kosong. Berdasarkan parameter IsMRLocalDocNoFromReference
    23/11/2021 [WED/RM] perubahan flow --> ExpDt dan Sector hilang
    03/12/2021 [VIN/ALL] Add Button DRQ yang hilang
 */
#endregion

/*
    06/12/2021 [NJP/RM] Menambahkan Parameter IsUseAdditionalLabel agar parameter LabelFileforMR bisa berjalan tidah hanya di PHT
    07/12/2021 [RDA/PHT] Approval amount (start & end amount) di Document Approval Setting menarik dari total price di Material Request berdasarkan param AmtSourceMRApproval
    16/12/2021 [WED/RM] tambah tab RFQ List berdasarkan parameter IsUseECatalog
    23/12/2021 [BRI/IMS] tambah ttd di printout
    06/01/2022 [TKG/GSS] ubah GetParameter dan proses save.
    27/01/2022 [TRI/PHT] Benerin typo
    28/01/2022 [TRI/PHT] Pilihan PIC hanya menampilkan sesuai user yang login berdasar param isPICGrouped
    10/02/2022 [VIN/PHT] Approval By Amount blm sesuai
    17/02/2022 [TYO/PHT] Set auto update Independent Estimated Price auto cancel if MR in Independent Estimated Price Canceled
    24/03/2022 [MYA/PHT] Mengurangi available budget pada MR berdarkan transaksi receiving item form vendor - auto DO dan receiving item from vendor without PO - auto DO yang terbentuk dari MR
    24/03/2022 [ICA/PHT] Menambah field available budget for MR, Available saat ini di kurangi dengan amt MR yg belum di journalkan. Jika sudah sampe PO, ambil Amt PO. 
    06/04/2022 [MYA/PHT] available budget di MR bisa di simpan nilainya saat save sehingga saat show tdk dinamis
    13/04/2022 [ICA/PHT] menambah parameter mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice untuk mendapatkan total MR ketika compute budget for MR
    17/05/2022 [RIS/PHT] Validasi available budget dapat minus dengan parameter IsAvailableBudgetMRAllowMinus
    18/05/2022 [RIS/PHT] Menambah field Grand Total dengan param IsMRShowTotalEstimatedPrice
    20/05/2022 [RIS/PHT] AmtSourceMRApproval value == 3
    24/08/2022 [IBL/PHT] Mempercepat proses ComputeBudgetForMR()
    06/09/2022 [SET/PRODUCT] tambah extension file upload
    07/09/2022 [SET/PRODUCT] add extension file download
    23/09/2022 [BRI/PRODUCT] Penyesuaian validasi budget di material request ketika parameter IsMaterialRequestForDroppingRequestEnabled = Y 
    20/09/2022 [RDA/VIR] penarikan dropping request menjadi partial berdasarkan param IsDroppingRequestUseMRReceivingPartial (MR)
    06/01/2023 [TYO/MNET] menambah printout baru untuk MNET
    21/02/2023 [SET/HEX] add field MR Type, Approval Sheet, Asset Status Change (EPROC)
    22/02/2023 [IBL/HEX] Approval dibuat berdasarkan item category. Berdasarkan parameter IsDocApprovalSettingUseItemCt
    23/02/2023 [BRI/HEX] perhitungan available budget berdasarkan param FiscalYearRange
    27/02/2023 [SET/HEX] BUG terhubung Approval Sheet
    02/03/2023 [MAU/HEX] BUG : lue MRType empty 
    04/03/2023 [WED/HEX] validasi Procurement Closing, berdasarkan parameter IsClosingProcurementUseMRType
    08/03/2023 [MAU/HEX] penyesuaian MRType 
    27/03/2023 [SET/TWC] penyesuain save bagian ApprovalSheet berdasar param IsMRUseApprovalSheet
    05/04/2023 [WED/HEX] remaining budget belum sesuai dengan remaining budget di reporting Budget Summary (NEW)
    14/04/2023 [WED/HEX] UsedBudget ter double saat kalkulasi Remaining Budget
    18/04/2023 [WED/HEX] nilai Available MR Budget masih ke double
    28/04/2023 [WED/HEX] nilai Available MR masih belum sesuai kalau ada MR yg belum di PO kan
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;

#endregion


namespace RunSystem
{
    public partial class FrmMaterialRequest : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mDroppingRequestBCCode = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mItGrpCodeNotShowOnMaterialRequest = string.Empty;
        internal FrmMaterialRequestFind FrmFind;
        private bool
            mIsUseItemConsumption = false,
            mIsRemarkForApprovalMandatory = false,
            mIsApprovalBySiteMandatory = false,
            mIsDocNoWithDeptShortCode = false,
            mIsDeptFilterBySite = false,
            mIsShowSeqNoInMaterialRequest = false,
            mIsMaterialRequestMaxStockValidationEnabled = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsMaterialRequestDocNoUseDifferentAbbr = false,
            mIsMaterialRequestSPPJBDocNoUseDifferentAbbr = false,
            mIsUsageDateMaterialRequestMandatory = false,
            mIsMaterialRequestShowReorderPoint = false,
            mIsMaterialRequestShowMinimumStock = false,
            mIsBudget2YearlyFormat = false,
            mIsMRDurationMandatory = false,
            mIsMRApprovalByAmount = false,
            mIsCASUsedForBudget = false,
            mIsMRWithTenderEnabled = false,
            mIsMRUseReview = false,
            mIsMRUseReference = false,
            mIsUseAdditionalLabel = false,
            mIsMRUseBudgetForMR = false,
            mIsMRSaveAvailableBudget = false,
            mIsAvailableBudgetMRAllowMinus = false,
            mIsMRShowTotalEstimatedPrice = false,
            mIsDocApprovalSettingUseItemCt = false
            ;

        internal bool
            mIsSiteMandatory = false,
            mIsPICInMRMandatory = false,
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsFilterByItCt = false,
            mIsShowForeignName = false,
            mIsBudgetActive = false,
            mIsDORequestNeedStockValidation = false,
            mIsDORequestUseItemIssued = false,
            mIsMRShowEstimatedPrice = false,
            mIsMRAllowToUploadFile = false,
            mIsMaterialRequestForDroppingRequestEnabled = false,
            mIsBOMShowSpecifications = false,
            mIsMRSPPJBEnabled = false,
            mIsMRSPPJB = false,
            mIsMRAutomaticGetLastVendorQuotation = false,
            mIsMRBudgetBasedOnBudgetCategory = false,
            mIsRemarkMRNotMandatory = false,
            mIsMRShowTotalPrice = false,
            mIsMRQuotationNotShow = false,
            mIsPICGrouped = false,
            mIsMRUseProcurementType = false,
            mIsMRItemGroupNotShow = false,
            mIsUseECatalog = false,
            mIsMRLocalDocNoFromReference = false,
            IsAutoGeneratePurchaseLocalDocNo = false,
            mIsDroppingRequestUseMRReceivingPartial = false,
            mIsDroppingRequestUseType = false,
            mIsMRUseApprovalSheet = false,
            mIsClosingProcurementUseMRType = false
            ;
        private string
            mBudgetBasedOn = "1",
            mIsPrintOutUseDigitalSignature = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mMRAvailableBudgetSubtraction = string.Empty,
            mLabelFileForMR = string.Empty,
            mMandatoryFileForMR = string.Empty,
            mAmtSourceMRApproval = string.Empty,
            mSourceAvailableBudgetForMR = string.Empty,
            mFiscalYearRange = string.Empty
            ;

        private string mFormPrintOutMaterialRequestWithoutConsumption = string.Empty;
        private string mFormPrintOutMaterialRequestWithConsumption = string.Empty;
        private string mFormPrintOutMaterialRequestSPPJB = string.Empty;
        private string mFormPrintOutMaterialRequestSPPJB2 = string.Empty;
        private string Doctitle = Sm.GetParameter("Doctitle");
           
        private iGCopyPasteManager fCopyPasteManager;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;
        internal List<Job> mlJob;

        #endregion

        #region Constructor

        public FrmMaterialRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0)
            {
                if (Doctitle == "IMS")
                    this.Text = "Purchase Request";
                else
                    this.Text = "Material Request";
            }
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (mLabelFileForMR.Length > 0 && (Doctitle == "PHT" || mIsUseAdditionalLabel))
                {
                    LabelForMR();
                }
                
                if (!mIsMRUseReview)
                    Tc1.TabPages.Remove(Tp5);

                if (!mIsMRUseReference)
                    LblReference.Visible = TxtReference.Visible = BtnReference.Visible = false;

                Tc1.SelectedTabPage = Tp6;
                if (!mIsUseECatalog) Tc1.TabPages.Remove(Tp6);

                if (!mIsMRUseBudgetForMR)
                { 
                    LblBudgetMR.Visible = TxtBudgetForMR.Visible = false;
                    LblRemark.Top -= 21; 
                    MeeRemark.Top -= 21;
                    panel2.Height -= 21;
                }

                Tc1.SelectedTabPage = Tp1;

                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                if (mIsPICInMRMandatory) LblPICCode.ForeColor = Color.Red;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocNo, TxtPOQtyCancelDocNo, TxtRemainingBudget, TxtDORequestDocNo }, true);
                SetLueCurCode(ref LueCurCode);
                Sl.SetLueOption(ref LueDurationUom, "DurationUom");
                SetGrd();
                LueCurCode.Visible = false;
                LueDurationUom.Visible = false;
                if (mIsUseECatalog) Sl.SetLuePtCode(ref LuePtCode);
                LuePtCode.Visible = false;
                ChkRMInd.Visible = mIsUseECatalog;
                Sl.SetLueOption(ref LueProcurementType, "ProcurementType");
                if (!mIsMRUseProcurementType)
                {
                    LueProcurementType.Visible = false;
                    label22.Visible = false;
                }
                SetFormControl(mState.View);
                SetLuePICCode(ref LuePICCode);
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept?"Y":"N");
                Tp2.PageVisible = mIsMaterialRequestForDroppingRequestEnabled;
                ChkTenderInd.Visible = mIsMRWithTenderEnabled;
                if(!mIsMRShowTotalEstimatedPrice)
                {
                    TxtGrandTotal.Visible = label17.Visible = false;
                    LblReference.Top -= 21;
                    TxtReference.Top -= 21;
                    BtnReference.Top -= 21;
                }

                if(!mIsMRUseApprovalSheet)
                {
                    LblMRType.Visible = false; LueMRType.Visible = false;
                    LblRemark.Top -= 21; MeeRemark.Top -= 21;
                    LblBudgetMR.Top -= 21; TxtBudgetForMR.Top -= 21;
                    LblAvailableBudget.Top -= 21; TxtRemainingBudget.Top -= 21;
                    LblBudgetCt.Top -= 21; LueBCCode.Top -= 21;
                    LblDORequestDocNo.Top -= 21; TxtDORequestDocNo.Top -= 21; BtnDORequestDocNo.Top -= 21; BtnDORequestDocNo2.Top -= 21;
                    LblPICCode.Top -= 21; LuePICCode.Top -= 21;
                    LblDept.Top -= 21; LueDeptCode.Top -= 21;
                    LblSiteCode.Top -= 21; LueSiteCode.Top -= 21;
                    LblReqType.Top -= 21; LueReqType.Top -= 21;
                    LblApprovalSheet.Visible = TxtApprovalSheetDocNo.Visible = false;
                    LblEPROC.Visible = TxtEproc.Visible = false;
                    BtnApprovalSheetDocNo.Visible = false; BtnApprovalSheetDocNo2.Visible = false;
                }
                
                mlJob = new List<Job>();

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if(Doctitle=="IMS")
                    label19.Text = "PR\'s Total Amount";
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'ProcFormatDocNo', 'IsAutoGeneratePurchaseLocalDocNo', 'IsMaterialRequestRemarkForApprovalMandatory', 'BudgetBasedOn', 'ReqTypeForNonBudget', ");
            SQL.AppendLine("'IsFilterByItCt', 'IsFilterByDept', 'IsFilterBySite', 'IsSiteMandatory', 'IsShowForeignName', ");
            SQL.AppendLine("'IsApprovalBySiteMandatory', 'IsBudgetActive', 'IsDocNoWithDeptShortCode', 'IsDORequestNeedStockValidation', 'IsDORequestUseItemIssued', ");
            SQL.AppendLine("'IsMRShowEstimatedPrice', 'IsUseItemConsumption', 'IsPICInMRMandatory', 'IsBOMShowSpecifications', 'IsMRAutomaticGetLastVendorQuotation', ");
            SQL.AppendLine("'IsMaterialRequestShowReorderPoint', 'IsMaterialRequestShowMinimumStock', 'IsBudget2YearlyFormat', 'IsRemarkMRNotMandatory', 'IsMRQuotationNotShow', ");
            SQL.AppendLine("'IsMRItemGroupNotShow', 'IsUsageDateMaterialRequestMandatory', 'IsPICGrouped', 'IsMRWithTenderEnabled', 'ItGrpCodeNotShowOnMaterialRequest', ");
            SQL.AppendLine("'FormPrintOutMaterialRequestWithoutConsumption', 'FormPrintOutMaterialRequestWithConsumption', 'FormPrintOutMaterialRequestSPPJB', 'FormPrintOutMaterialRequestSPPJB2', 'LabelFileForMR', ");
            SQL.AppendLine("'MandatoryFileForMR', 'AmtSourceMRApproval', 'IsMRDurationMandatory', 'IsMaterialRequestForDroppingRequestEnabled', 'IsMRSPPJBEnabled', ");
            SQL.AppendLine("'IsShowSeqNoInMaterialRequest', 'IsMaterialRequestMaxStockValidationEnabled', 'IsMRBudgetBasedOnBudgetCategory', 'IsBudgetCalculateFromEstimatedPrice', 'IsMaterialRequestDocNoUseDifferentAbbr', ");
            SQL.AppendLine("'IsMaterialRequestSPPJBDocNoUseDifferentAbbr', 'IsMRLocalDocNoFromReference', 'IsUseAdditionalLabel', 'IsMRUseReference', 'IsMRUseReview', ");
            SQL.AppendLine("'IsMRShowTotalPrice', 'IsMRApprovalByAmount', 'IsCASUsedForBudget', 'IsUseECatalog', 'IsMRUseProcurementType', ");
            SQL.AppendLine("'MRAvailableBudgetSubtraction', 'FileSizeMaxUploadFTPClient', 'PortForFTPClient', 'PasswordForFTPClient', 'UsernameForFTPClient', ");
            SQL.AppendLine("'IsMRAllowToUploadFile', 'IsPrintOutUseDigitalSignature', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'SourceAvailableBudgetForMR', 'IsMRUseBudgetForMR', 'IsMRSaveAvailableBudget', 'IsAvailableBudgetMRAllowMinus', ");
            SQL.AppendLine("'IsMRShowTotalEstimatedPrice', 'IsDroppingRequestUseMRReceivingPartial', 'IsDroppingRequestUseType', 'IsMRUseApprovalSheet', 'IsDocApprovalSettingUseItemCt', 'IsClosingProcurementUseMRType', 'FiscalYearRange' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoGeneratePurchaseLocalDocNo": IsAutoGeneratePurchaseLocalDocNo = ParValue == "Y"; break;
                            case "IsMaterialRequestRemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsApprovalBySiteMandatory": mIsApprovalBySiteMandatory = ParValue == "Y"; break;
                            case "IsBudgetActive": mIsBudgetActive = ParValue == "Y"; break;
                            case "IsDocNoWithDeptShortCode": mIsDocNoWithDeptShortCode = ParValue == "Y"; break;
                            case "IsDORequestNeedStockValidation": mIsDORequestNeedStockValidation = ParValue == "Y"; break;
                            case "IsDORequestUseItemIssued": mIsDORequestUseItemIssued = ParValue == "Y"; break;
                            case "IsMRShowEstimatedPrice": mIsMRShowEstimatedPrice = ParValue == "Y"; break;
                            case "IsUseItemConsumption": mIsUseItemConsumption = ParValue == "Y"; break;
                            case "IsPICInMRMandatory": mIsPICInMRMandatory = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsMRAutomaticGetLastVendorQuotation": mIsMRAutomaticGetLastVendorQuotation = ParValue == "Y"; break;
                            case "IsMaterialRequestShowReorderPoint": mIsMaterialRequestShowReorderPoint = ParValue == "Y"; break;
                            case "IsMaterialRequestShowMinimumStock": mIsMaterialRequestShowMinimumStock = ParValue == "Y"; break;
                            case "IsBudget2YearlyFormat": mIsBudget2YearlyFormat = ParValue == "Y"; break;
                            case "IsRemarkMRNotMandatory": mIsRemarkMRNotMandatory = ParValue == "Y"; break;
                            case "IsMRQuotationNotShow": mIsMRQuotationNotShow = ParValue == "Y"; break;
                            case "IsMRItemGroupNotShow": mIsMRItemGroupNotShow = ParValue == "Y"; break;
                            case "IsUsageDateMaterialRequestMandatory": mIsUsageDateMaterialRequestMandatory = ParValue == "Y"; break;
                            case "IsPICGrouped": mIsPICGrouped = ParValue == "Y"; break;
                            case "IsMRWithTenderEnabled": mIsMRWithTenderEnabled = ParValue == "Y"; break;
                            case "IsMRDurationMandatory": mIsMRDurationMandatory = ParValue == "Y"; break;
                            case "IsMaterialRequestForDroppingRequestEnabled": mIsMaterialRequestForDroppingRequestEnabled = ParValue == "Y"; break;
                            case "IsMRSPPJBEnabled": mIsMRSPPJBEnabled = ParValue == "Y"; break;
                            case "IsShowSeqNoInMaterialRequest": mIsShowSeqNoInMaterialRequest = ParValue == "Y"; break;
                            case "IsMaterialRequestMaxStockValidationEnabled": mIsMaterialRequestMaxStockValidationEnabled = ParValue == "Y"; break;
                            case "IsMRBudgetBasedOnBudgetCategory": mIsMRBudgetBasedOnBudgetCategory = ParValue == "Y"; break;
                            case "IsBudgetCalculateFromEstimatedPrice": mIsBudgetCalculateFromEstimatedPrice = ParValue == "Y"; break;
                            case "IsMaterialRequestDocNoUseDifferentAbbr": mIsMaterialRequestDocNoUseDifferentAbbr = ParValue == "Y"; break;
                            case "IsUseAdditionalLabel": mIsUseAdditionalLabel = ParValue == "Y"; break;
                            case "IsMaterialRequestSPPJBDocNoUseDifferentAbbr": mIsMaterialRequestSPPJBDocNoUseDifferentAbbr = ParValue == "Y"; break;
                            case "IsMRLocalDocNoFromReference": mIsMRLocalDocNoFromReference = ParValue == "Y"; break;
                            case "IsMRUseReference": mIsMRUseReference = ParValue == "Y"; break;
                            case "IsMRUseReview": mIsMRUseReview = ParValue == "Y"; break;
                            case "IsMRShowTotalPrice": mIsMRShowTotalPrice = ParValue == "Y"; break;
                            case "IsMRApprovalByAmount": mIsMRApprovalByAmount = ParValue == "Y"; break;
                            case "IsCASUsedForBudget": mIsCASUsedForBudget = ParValue == "Y"; break;
                            case "IsUseECatalog": mIsUseECatalog = ParValue == "Y"; break;
                            case "IsMRUseProcurementType": mIsMRUseProcurementType = ParValue == "Y"; break;
                            case "IsMRAllowToUploadFile": mIsMRAllowToUploadFile = ParValue == "Y"; break;
                            case "IsMRUseBudgetForMR": mIsMRUseBudgetForMR = ParValue == "Y"; break;
                            case "IsMRSaveAvailableBudget": mIsMRSaveAvailableBudget = ParValue == "Y"; break;
                            case "IsAvailableBudgetMRAllowMinus": mIsAvailableBudgetMRAllowMinus = ParValue == "Y"; break;
                            case "IsMRShowTotalEstimatedPrice": mIsMRShowTotalEstimatedPrice = ParValue == "Y"; break;
                            case "IsDroppingRequestUseMRReceivingPartial": mIsDroppingRequestUseMRReceivingPartial = ParValue == "Y"; break;
                            case "IsDroppingRequestUseType": mIsDroppingRequestUseType = ParValue == "Y";break;
                            case "IsMRUseApprovalSheet": mIsMRUseApprovalSheet = ParValue == "Y";break;
                            case "IsDocApprovalSettingUseItemCt": mIsDocApprovalSettingUseItemCt = ParValue == "Y"; break;
                            case "IsClosingProcurementUseMRType": mIsClosingProcurementUseMRType = ParValue == "Y"; break;

                            //string
                            case "ProcFormatDocNo": IsProcFormat = ParValue; break;
                            case "BudgetBasedOn": mBudgetBasedOn = ParValue; break;
                            case "ReqTypeForNonBudget": mReqTypeForNonBudget = ParValue; break;
                            case "ItGrpCodeNotShowOnMaterialRequest": mItGrpCodeNotShowOnMaterialRequest = ParValue; break;
                            case "FormPrintOutMaterialRequestWithoutConsumption": mFormPrintOutMaterialRequestWithoutConsumption = ParValue; break;
                            case "FormPrintOutMaterialRequestWithConsumption": mFormPrintOutMaterialRequestWithConsumption = ParValue; break;
                            case "FormPrintOutMaterialRequestSPPJB": mFormPrintOutMaterialRequestSPPJB = ParValue; break;
                            case "FormPrintOutMaterialRequestSPPJB2": mFormPrintOutMaterialRequestSPPJB2 = ParValue; break;
                            case "LabelFileForMR": mLabelFileForMR = ParValue; break;
                            case "MandatoryFileForMR": mMandatoryFileForMR = ParValue; break;
                            case "AmtSourceMRApproval": mAmtSourceMRApproval = ParValue; break;
                            case "MRAvailableBudgetSubtraction": mMRAvailableBudgetSubtraction = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "IsPrintOutUseDigitalSignature": mIsPrintOutUseDigitalSignature = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "SourceAvailableBudgetForMR": mSourceAvailableBudgetForMR = ParValue; break;
                            case "FiscalYearRange": mFiscalYearRange = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mIsSiteMandatory && mIsBudgetActive) mIsDeptFilterBySite = Sm.IsDataExist("Select 1 From TblDepartment A, TblDepartmentBudgetSite B Where A.DeptCode=B.DeptCode And A.ActInd='Y' Limit 1;");
            if (mFormPrintOutMaterialRequestWithoutConsumption.Length == 0) mFormPrintOutMaterialRequestWithoutConsumption = "MaterialRequest";
            if (mFormPrintOutMaterialRequestWithConsumption.Length == 0) mFormPrintOutMaterialRequestWithConsumption = "MaterialRequest2";
            if (mAmtSourceMRApproval.Length == 0) mAmtSourceMRApproval = "1";
            if (mIsMRSPPJBEnabled) mIsMRSPPJB = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForMRSPPJB"));

        }

        private void SetGrd()
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            #region Grid 1

            Grd1.Cols.Count = 48;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "Cancel Reason",
                        "",
                        "Status",
                        
                        
                        //6-10
                        "Checked by",
                        "",
                        "Item's Code",
                        "Local Code",
                        "",
                        
                        //11-15
                        "Item's Name",
                        "Foreign Name",
                        "Item Sub Category code",
                        "Sub Category",
                        "Minimum Stock",
                       
                        //16-20
                        "Reoder Point",
                        "Quantity",
                        "UoM",
                        "Usage Date",
                        "Quotation#",
                        
                        //21-25
                        "Quotation Dno",
                        "Quotation Date",
                        "Price",
                        "Total",
                        "Remark",

                        //26-30
                        "DORequestDocNo",
                        "DORequestDNo",
                        "Currency",
                        "Estimated Price",
                        "No.",

                        //31-35
                        "Dropping Request Quantity",
                        "Dropping Request's"+Environment.NewLine+"Amount",
                        "Specification",
                        "BOQ",
                        "Duration",

                        //36-40
                        "Duration UOM Code",
                        "Duration UOM",
                        "Total Price",
                        "PtCode",
                        "Payment Term",

                        //41-44
                        "",
                        "CityCode",
                        "Province",
                        "Delivery Location",
                        "ApprovalSheetDocNo",

                        //46-47
                        "ApprovalSheetDNo",
                        "OutstandingQtyApprovalSheet"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 200, 20, 80, 
                        
                        //6-10
                        100, 20, 100, 150, 20,  
                        
                        //11-15
                        230, 200, 80, 100, 100, 
                        
                        //16-20
                        100, 120, 100, 100, 130,  
                        
                        //21-25
                        80, 100, 150, 150, 400,

                        //26-30
                        0, 0, 100, 120, 50,

                        //31-35
                        0, 130, 300, 50, 80,

                        //36-40
                        0, 150, 150, 0, 200,

                        //41-45
                        20, 0, 200, 200, 100,

                        //46-47
                        100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 15, 16, 17, 23, 24, 29, 31, 32, 35, 38, 47 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 4, 7, 10, 34, 41 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 36, 45, 46, 47 }, false);
            if (!mIsMRShowTotalPrice)
                Sm.GrdColInvisible(Grd1, new int[] { 38 });

            if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                Sm.GrdColInvisible(Grd1, new int[] { 7 });
            
            if (!mIsMRShowEstimatedPrice)
                Sm.GrdColInvisible(Grd1, new int[] { 28, 29 });
            
            if (mIsShowForeignName)
            {
                if (IsProcFormat == "1")
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 10, 21, 22, 23, 24, 13 }, false);
                else
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 10, 21, 22, 23, 24, 13, 14 }, false);
            }
            else
            {
                if (IsProcFormat == "1")
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 10, 12, 13, 21, 22, 23, 24 }, false);
                else
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14, 21, 22, 23, 24 }, false);
            }
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 30, 31, 32, 33, 36, 38, 39, 42, 43, 44, 45, 46, 47 });
            Sm.GrdColInvisible(Grd1, new int[] { 30 }, mIsShowSeqNoInMaterialRequest);
            if (!mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 39, 40, 41, 42, 43, 44 });
            fCopyPasteManager = new iGCopyPasteManager(Grd1);
            Grd1.Cols[28].Move(25);
            Grd1.Cols[29].Move(26);
            Grd1.Cols[38].Move(27);
            Grd1.Cols[30].Move(0);
            
            if (Doctitle == "SIER")
            {
                if (!mIsMaterialRequestForDroppingRequestEnabled && !mIsMRSPPJB)
                    Sm.GrdColInvisible(Grd1, new int[] { 32 });
                
            }
            else
            {
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 23, 24, 32 }, true);
                    Grd1.Cols[32].Move(26);
                }
            }
            if (!mIsMRSPPJB)
            {
                if (!mIsMaterialRequestShowMinimumStock)
                    Sm.GrdColInvisible(Grd1, new int[] { 15 });
                if (!mIsMaterialRequestShowReorderPoint)
                    Sm.GrdColInvisible(Grd1, new int[] { 16 });
            }
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 33 });
            Grd1.Cols[33].Move(13);

            if (!mIsMRSPPJB)
                Sm.GrdColInvisible(Grd1, new int[] { 34 }, false);
            else
                Grd1.Cols[34].Move(28);
            


            if(mIsMRQuotationNotShow)
                Sm.GrdColInvisible(Grd1, new int[] { 20 });

            if (!mIsUsageDateMaterialRequestMandatory)
                Grd1.Cols[19].Visible = false;

            #endregion

            #region Grid Review
            GrdReview.Cols.Count = 6;
            GrdReview.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                GrdReview,
                new string[]
                { 
                     //0
                    "No",
                    
                    //1-5
                    "Item's Code", 
                    "Item's Name",
                    "Specification",
                    "Review",
                    "Remark Review"

                },
                new int[] { 50, 150, 150, 250, 100, 150 }
            );
            Sm.GrdColReadOnly(true, false, GrdReview, new int[] { 0, 1,2,3,4,5 });
            Sm.GrdColInvisible(GrdReview, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            #endregion

            # region Grd 2 - RFQ List

            if (mIsUseECatalog)
            {
                Grd2.Cols.Count = 5;
                Grd2.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    { 
                     //0
                    "No",
                    
                    //1-4
                    "SeqNo",
                    "RFQ Date",
                    "Sent By",
                    ""

                    },
                    new int[] { 50, 0, 100, 250, 20 }
                );
                Sm.GrdColReadOnly(true, false, Grd2, new int[] { 0, 1, 2, 3 });
                Sm.GrdColButton(Grd2, new int[] { 4 });
                Sm.GrdFormatDate(Grd2, new int[] { 2 });
                Sm.GrdColInvisible(Grd2, new int[] { 1 });
            }

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 8, 9, 10, 20, 22 }, !ChkHideInfoInGrd.Checked);
            if(!mIsUsageDateMaterialRequestMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 19 }, !ChkHideInfoInGrd.Checked);
            if (mIsMRUseReview)
                Sm.GrdColInvisible(GrdReview, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, LueBCCode, 
                       LueSiteCode, MeeRemark, TxtFile, TxtFile2, TxtFile3, LuePICCode, 
                       ChkTenderInd, LueProcurementType, ChkRMInd, TxtReference,
                       TxtFile4, TxtFile5, TxtFile6,
                       LueMRType, TxtApprovalSheetDocNo, TxtEproc

                    }, true);
                    BtnPOQtyCancelDocNo.Enabled = false;
                    BtnDORequestDocNo.Enabled = false;
                    BtnDroppingRequestDocNo.Enabled = false;
                    if (!mIsDORequestNeedStockValidation)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    BtnApprovalSheetDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 29, 33, 35, 37, 40, 41 });
                    
                    BtnReference.Enabled = false;
                    
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    BtnFile4.Enabled = false;
                    BtnDownload4.Enabled = false;
                    BtnFile5.Enabled = false;
                    BtnDownload5.Enabled = false;
                    BtnFile6.Enabled = false;
                    BtnDownload6.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload4.Enabled = true;
                    ChkFile4.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload5.Enabled = true;
                    ChkFile5.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsMRAllowToUploadFile)
                        BtnDownload6.Enabled = true;
                    ChkFile6.Enabled = false;

                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, mIsDeptFilterBySite);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, LueReqType, LueSiteCode, MeeRemark, LuePICCode, ChkTenderInd }, false);
                    if (mIsUseECatalog)
                       Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkRMInd }, false);
                    
                    if (!IsAutoGeneratePurchaseLocalDocNo || !mIsMRLocalDocNoFromReference)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtLocalDocNo }, false);
                    if (mIsMRLocalDocNoFromReference)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, true);

                    if (mIsMRUseApprovalSheet)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueMRType }, false);
                        Sl.SetLueOption(ref LueMRType, "MRType");
                        BtnApprovalSheetDocNo.Enabled = true;
                    }

                    BtnPOQtyCancelDocNo.Enabled = true;
                    if (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    if (!mIsDORequestNeedStockValidation) BtnDORequestDocNo.Enabled = true;
                    Sm.GrdColInvisible(Grd1, new int[]{ 3 }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 7, 17, 19, 25, 28, 29, 35, 37, 40, 41 }); //RDA
                    if (mIsUseECatalog)
                    {
                        ChkRMInd.Checked = true;
                    }

                    if (mIsMRAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                        BtnFile4.Enabled = true;
                        BtnDownload4.Enabled = true;
                        BtnFile5.Enabled = true;
                        BtnDownload5.Enabled = true;
                        BtnFile6.Enabled = true;
                        BtnDownload6.Enabled = true;
                    }

                    if (mIsMRUseProcurementType) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcurementType }, false);
                   BtnReference.Enabled = mIsMRUseReference;

                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    ChkFile4.Enabled = true;
                    ChkFile5.Enabled = true;
                    ChkFile6.Enabled = true;

                    BtnDroppingRequestDocNo.Enabled = true; 
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    if (!mIsDORequestNeedStockValidation)
                        LblDORequestDocNo.ForeColor = Color.Red;
                    else
                        LblDORequestDocNo.ForeColor = Color.Black;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            if (mIsMRSPPJB) mlJob.Clear();
            mDroppingRequestBCCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, LueReqType, LueDeptCode, TxtFile,
                LueSiteCode, TxtPOQtyCancelDocNo, LueBCCode, MeeRemark, TxtDORequestDocNo, 
                LuePICCode, TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, 
                TxtDR_PRJIDocNo, TxtDR_BCCode, MeeDR_Remark, TxtFile2, TxtFile3, LueProcurementType,
                TxtFile4, TxtFile5, TxtFile6, TxtReference,
                LueMRType, TxtApprovalSheetDocNo, TxtEproc
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtRemainingBudget, TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance, TxtGrandTotal
            }, 0);
            ClearGrd();
            ChkTenderInd.Checked = false;
            ChkRMInd.Checked = false;

            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
            ChkFile4.Checked = false;
            PbUpload4.Value = 0;
            ChkFile5.Checked = false;
            PbUpload5.Value = 0;
            ChkFile6.Checked = false;
            PbUpload6.Value = 0;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            if (mIsMRUseReview) Sm.ClearGrd(GrdReview, true);
            if (mIsUseECatalog) Sm.ClearGrd(Grd2, false);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 15, 16, 17, 23, 24, 29, 31, 32, 35 });
            if (mIsBudgetActive) Grd1.Cells[0, 11].ForeColor = Color.Black;
            SetSeqNo();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMaterialRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            SetLuePICCode(ref LuePICCode);
            InsertData();
        }

        private void InsertData()
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueReqType(ref LueReqType, string.Empty);
                if (mReqTypeForNonBudget.Length > 0) Sm.SetLue(LueReqType, mReqTypeForNonBudget);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                if (!mIsDeptFilterBySite) Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            ComputeBudgetForMR();
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || (Sm.GetParameter("DocTitle") == "KIM" && ShowPrintApproval()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                   "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                   "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                   "|Word files (*.doc;*docx)|*.doc;*docx" +
                   "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                   "|Text files (*.txt)|*.txt" +
                   "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                   "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                   "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                   "|Word files (*.doc;*docx)|*.doc;*docx" +
                   "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                   "|Text files (*.txt)|*.txt" +
                   "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile4_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile4.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                   "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                   "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                   "|Word files (*.doc;*docx)|*.doc;*docx" +
                   "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                   "|Text files (*.txt)|*.txt" +
                   "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile4.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile5_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile5.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                   "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                   "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                   "|Word files (*.doc;*docx)|*.doc;*docx" +
                   "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                   "|Text files (*.txt)|*.txt" +
                   "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile5.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile6_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile6.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                "|Word files (*.doc;*docx)|*.doc;*docx" +
                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                "|Text files (*.txt)|*.txt" +
                "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile6.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            SFD.Filter = "PDF files(*.pdf) | *.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {

            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            SFD.Filter = "PDF files(*.pdf) | *.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {

            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            SFD.Filter = "PDF files(*.pdf) | *.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload4_Click(object sender, EventArgs e)
        {
            DownloadFile4(mHostAddrForFTPClient, mPortForFTPClient, TxtFile4.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile4.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            SFD.Filter = "PDF files(*.pdf) | *.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile4, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload5_Click(object sender, EventArgs e)
        {
            DownloadFile5(mHostAddrForFTPClient, mPortForFTPClient, TxtFile5.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile5.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            SFD.Filter = "PDF files(*.pdf) | *.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile5, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload6_Click(object sender, EventArgs e)
        {
            DownloadFile6(mHostAddrForFTPClient, mPortForFTPClient, TxtFile6.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile6.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;
            SFD.Filter = "PDF files(*.pdf) | *.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";

            if (!Sm.IsTxtEmpty(TxtFile6, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion 

        #region Grid Method

        #region Grid 1
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (mIsMRSPPJB && e.RowIndex != 0) 
                    {
                        e.DoDefault = false;
                        return;
                    }
                    if (e.ColIndex == 7 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) 
                            Sm.FormShowDialog(new FrmMaterialRequestDlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));
                    }

                    if (e.ColIndex == 17 && TxtDroppingRequestDocNo.Text.Length>0)
                        e.DoDefault = false;
                    
                    if (Sm.IsGrdColSelected(new int[] { 3, 17, 19, 25 }, e.ColIndex))
                    {
                        if (e.ColIndex == 19) Sm.DteRequestEdit(Grd1, DteUsageDt, ref fCell, ref fAccept, e);
                        if (mIsDORequestNeedStockValidation)
                        {
                            if (!(mIsMRSPPJB && Grd1.Rows.Count > 1))
                                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        }
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 17, 23, 24, 29, 31, 32 });
                    }

                    if (e.ColIndex == 28)
                    {
                        Sm.LueRequestEdit(ref Grd1, ref LueCurCode, ref fCell, ref fAccept, e);
                        if (mIsDORequestNeedStockValidation)
                        {
                            if (!(mIsMRSPPJB && Grd1.Rows.Count > 1))
                                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        }
                    }

                    if (e.ColIndex == 37)
                    {
                        Sm.LueRequestEdit(ref Grd1, ref LueDurationUom, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);                      
                    }

                    if (e.ColIndex == 40 && mIsUseECatalog && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 11, false, "Item is empty."))
                    {
                        Sm.LueRequestEdit(ref Grd1, ref LuePtCode, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);                      
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0 ) ShowMRApprovalInfo(e.RowIndex);
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
            if (e.ColIndex == 34 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.FormShowDialog(new FrmMaterialRequestDlg5(
                    this, 
                    e.RowIndex, 
                    TxtDocNo.Text.Length == 0 && BtnSave.Enabled
                    ));
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //diremark oleh TKG on 14/8/2018
            //if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //if (mIsDORequestNeedStockValidation)
            //{
            if (TxtDocNo.Text.Length == 0)
            {
                if (mIsMRSPPJB)
                {
                    var ItCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
                    mlJob.RemoveAll(x => Sm.CompareStr(x.ItCode, ItCode));
                }
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeRemainingBudget();
                ComputeDR_Balance();
                SetSeqNo();
            }
            ComputeBudgetForMR();
            //}
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e) //
        {
            if (mIsMRSPPJB && e.RowIndex != 0) return;
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0) ShowMRApprovalInfo(e.RowIndex);
            if (e.ColIndex == 7 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequestDlg(this, Sm.GetLue(LueReqType), Sm.GetLue(LueDeptCode)));

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
            if (e.ColIndex == 34 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
                Sm.FormShowDialog(new FrmMaterialRequestDlg5(
                    this,
                    e.RowIndex,
                    TxtDocNo.Text.Length == 0 && BtnSave.Enabled
                    ));

            if (mIsUseECatalog && e.ColIndex == 41 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 11, false, "Item is empty."))
            {
                Sm.FormShowDialog(new FrmMaterialRequestDlg7(this, e.RowIndex));
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 17 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 25 }, e);
            if (e.ColIndex == 17 || e.ColIndex == 29)
            {
                ComputeTotal(e.RowIndex);
                ComputeDR_Balance();
            }
            if (e.ColIndex == 17 || e.ColIndex == 29)
            {
                ComputeTotalPrice(e.RowIndex);
                if(mIsMRShowTotalEstimatedPrice)
                    ComputeGrandTotal();
            }
            if (e.ColIndex == 1 || e.ColIndex == 2)
                ComputeBudgetForMR();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 19)
            {
                if (Sm.GetGrdDate(Grd1, 0, 19).Length != 0)
                {
                    var UsageDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 19));
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) Grd1.Cells[Row, 19].Value = UsageDt;
                }
            }

            if (e.ColIndex == 25)
            {
                var Remark = Sm.GetGrdStr(Grd1, 0, 25);
                if (Remark.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) Grd1.Cells[Row, 25].Value = Remark;
                }
            }

            if (e.ColIndex == 28)
            {
                var CurCode = Sm.GetGrdStr(Grd1, 0, 28);
                if (CurCode.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0) 
                            Grd1.Cells[Row, 28].Value = CurCode;
                }
            }

            if (e.ColIndex == 37)
            {
                var DurationUOM = Sm.GetGrdStr(Grd1, 0, 37);
                if (DurationUOM.Length != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                            Grd1.Cells[Row, 37].Value = DurationUOM;
                }
            }

            if (e.ColIndex == 29)
            {
                decimal Total = 0m;

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0)
                        Total += Sm.GetGrdDec(Grd1, Row, 29);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        override protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 19)
                e.Text = "Double click title to copy data based on the first row's value.";
        }

        #endregion

        #region Grd 2

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (mIsUseECatalog && !Sm.IsTxtEmpty(TxtDocNo, "Document#", false))
            {
                if (e.ColIndex == 4)
                {
                    var f = new FrmRFQ(mMenuCode)
                    {
                        Tag = mMenuCode,
                        WindowState = FormWindowState.Normal,
                        StartPosition = FormStartPosition.CenterScreen,
                        mMaterialRequestDocNo = TxtDocNo.Text,
                        mSeqNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1)
                    };
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            string LocalDocNo = string.Empty;
                      
            if (Sm.StdMsgYN("Question",
                    "Department : " + LueDeptCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Do you want to save this data ?"
                    ) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            if (ChkRMInd.Checked && mIsUseECatalog)
            {
                if (Sm.StdMsgYN("Question",
                       "This data is about to proceed to RUNMarket." + Environment.NewLine +
                       "Do you want to save this data ?"
                       ) == DialogResult.No) return;
            }
                      
 
            Cursor.Current = Cursors.WaitCursor;

            string DeptCode = Sm.GetLue(LueDeptCode);
            string SubCategory = Sm.GetGrdStr(Grd1, 0, 13);
            string DocNo = string.Empty;

            if (mIsMRSPPJB)
            {
                if(mIsMaterialRequestSPPJBDocNoUseDifferentAbbr)
                    DocNo = GenerateDocNo2(Sm.GetDte(DteDocDt), "MaterialRequestSPPJB", "TblMaterialRequestHdr");
                else
                    DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequestSPPJB", "TblMaterialRequestHdr", SubCategory);
            }
            else
            {
                if (mIsMaterialRequestDocNoUseDifferentAbbr)
                    DocNo = GenerateDocNo2(Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr");
                else
                    DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory);
            }

            if (IsAutoGeneratePurchaseLocalDocNo && !mIsMRLocalDocNoFromReference)
            {
                LocalDocNo = GenerateLocalDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "MaterialRequest", "TblMaterialRequestHdr", SubCategory);
            }
            else
            {
                if (mIsMRLocalDocNoFromReference && TxtReference.Text.Length == 0)
                    LocalDocNo = DocNo;
                else
                    LocalDocNo = TxtLocalDocNo.Text;
            }
            
            var cml = new List<MySqlCommand>();


            cml.Add(SaveMaterialRequest(DocNo, LocalDocNo, IsDocApprovalSettingNotExisted()));

            //cml.Add(SaveMaterialRequestHdr(DocNo, LocalDocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
            //    {
            //        cml.Add(SaveMaterialRequestDtl(
            //            DocNo,
            //            Row,
            //            IsDocApprovalSettingNotExisted()
            //            ));
            //    }
            //}

            if (mIsMRSPPJB && mlJob.Count > 0) cml.Add(SaveMaterialRequestDtl2(DocNo));
            
            Sm.ExecCommands(cml);

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);
            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && TxtFile4.Text != "openFileDialog1")
                UploadFile4(DocNo);
            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && TxtFile5.Text != "openFileDialog1")
                UploadFile5(DocNo);
            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && TxtFile6.Text != "openFileDialog1")
                UploadFile6(DocNo);

            if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                BtnInsertClick(sender, e);
            else
            {
                ShowData(DocNo);
                ParPrint(DocNo);
            }
        }

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsMRUseApprovalSheet && Sm.IsLueEmpty(LueMRType, "MR Type")) ||
                Sm.IsLueEmpty(LueReqType, "Request type") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsPICInMRMandatory && Sm.IsLueEmpty(LuePICCode, "PIC")) ||
                (!mIsDORequestNeedStockValidation && !mIsDORequestUseItemIssued && Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false)) ||
                (mIsMRUseProcurementType && Sm.IsLueEmpty(LueProcurementType, "Procurement")) || 
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                (!mIsAvailableBudgetMRAllowMinus && IsRemainingBudgetNotValid()) ||
                IsSubcategoryDifferent() ||
                IsSubCategoryXXX() ||
                IsPOQtyCancelDocNoNotValid() ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                //(mIsMRAllowToUploadFile && IsUploadFileNotValid()) ||
                IsMaxStockInvalid() ||
                (mIsMRUseApprovalSheet && TxtApprovalSheetDocNo.Text.Length != 0 && IsQtyMoreThanQtyApprovalSheet()) ||
                IsDR_QtyInvalid() ||
                IsDR_AmtInvalid() ||
                IsDR_BalanceInvalid() ||
                (mIsMRSPPJB && IsJobCurCodeInvalid())||
                (mIsMRAllowToUploadFile && IsFileMandatory()) ||
                (mIsMRAllowToUploadFile && IsUploadFileNotValid()) ||
                (mIsMRAllowToUploadFile && IsUploadFileNotValid2()) ||
                (mIsMRAllowToUploadFile && IsUploadFileNotValid3()) ||
                (mIsMRAllowToUploadFile && IsUploadFileNotValid4()) ||
                (mIsMRAllowToUploadFile && IsUploadFileNotValid5()) ||
                (mIsMRAllowToUploadFile && IsUploadFileNotValid6()) ||
                Sm.IsClosingProcurementInvalid(mIsClosingProcurementUseMRType, Sm.GetLue(LueMRType), Sm.Left(Sm.GetDte(DteDocDt), 8))
                ;
        }

        private bool IsJobCurCodeInvalid()
        {
            string ItCode = string.Empty;
            string CurCode = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 28, false, "Estimated currency is empty.") ||
                        Sm.IsGrdValueEmpty(Grd1, r, 29, true, "Estimated price should be bigger than 0.00."))
                        return true;
                    ItCode = Sm.GetGrdStr(Grd1, r, 8);
                    CurCode = Sm.GetGrdStr(Grd1, r, 28);
                    foreach (var i in mlJob)
                    {
                        if (Sm.CompareStr(ItCode, i.ItCode))
                        {
                            if (!Sm.CompareStr(CurCode, i.CurCode))
                            {
                                Sm.StdMsg(mMsgType.Warning,
                                    "Item's Code : " + ItCode + Environment.NewLine +
                                    "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                                    "Job's Code : " + i.JobCode + Environment.NewLine +
                                    "Job's Name : " + i.JobName + Environment.NewLine +
                                    "Job's Currency : " + i.CurCode + Environment.NewLine +
                                    "Estimated Currency : " + CurCode + Environment.NewLine + Environment.NewLine +
                                    "Invalid job's currency."
                                    );
                                return true;
                            }
                        }
                    }
                }

            }
            return false;
        }

        private bool IsDR_AlreadyProcessed1()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled) return false;

            if (TxtDR_PRJIDocNo.Text.Length>0)
            {
                if (Sm.IsDataExist("Select 1 From TblDroppingRequestDtl Where MRDocNo Is Not Null And DocNo=@Param Limit 1;", TxtDroppingRequestDocNo.Text))    
                {
                    Sm.StdMsg(mMsgType.Warning, "This dropping request# already processed.");
                    return true;
                }
            }
            if (mDroppingRequestBCCode.Length > 0)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblDroppingRequestDtl2 Where MRDocNo Is Not Null And DocNo=@Param1 And BCCode=@Param2 Limit 1;", TxtDroppingRequestDocNo.Text, mDroppingRequestBCCode, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "This dropping request# already processed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_QtyInvalid()
        { 
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            decimal Qty1 = 0m, Qty2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty1 = 0m;  
                Qty2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 17).Length != 0) Qty1 = Sm.GetGrdDec(Grd1, r, 17);
                if (Sm.GetGrdStr(Grd1, r, 31).Length != 0) Qty2 = Sm.GetGrdDec(Grd1, r, 31);

                if (Qty1 > Qty2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                        "Requested Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 17), 0) + Environment.NewLine +
                        "Dropping Request's Quantity : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 31), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Quantity is bigger than Dropping Request's Quantity.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_AmtInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            decimal Amt1 = 0m, Amt2 = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Amt1 = 0m;
                Amt2 = 0m;
                if (Sm.GetGrdStr(Grd1, r, 24).Length != 0) Amt1 = Sm.GetGrdDec(Grd1, r, 24);
                if (Sm.GetGrdStr(Grd1, r, 32).Length != 0) Amt2 = Sm.GetGrdDec(Grd1, r, 32);

                if (Amt1 > Amt2)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                        "Requested Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 24), 0) + Environment.NewLine +
                        "Dropping Request's Amount : " + Sm.FormatNum(Sm.GetGrdStr(Grd1, r, 32), 0) + Environment.NewLine + Environment.NewLine +
                        "Requested Amount is bigger than Dropping Request's Amount.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDR_BalanceInvalid()
        {
            if (TxtDroppingRequestDocNo.Text.Length <= 0) return false;

            //ComputeDR_OtherMRAmt();
            ComputeDR_Balance();
            if (decimal.Parse(TxtDR_Balance.Text) < 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                        "Dropping Request's Balance : " + TxtDR_Balance.Text + Environment.NewLine +
                        "Dropping Request's balance is less than 0.00.");
                return true;
            }
            return false;
        }

        private bool IsMaxStockInvalid()
        {
            //membandingkan maximum stock dengan quantity yg diminta + stock semua warehouse + outang MR yg belum di-PO-kan.
            //MR yg belum di-received inidikator belum ada/Uom bisa beda/proses bisa lama
            //Hanya yg maximum stocknya diisi dan uom purchase dan inventory-nya sama

            if (!mIsMaterialRequestMaxStockValidationEnabled) return false;

            string ItCode = string.Empty, Filter = string.Empty, Filter2 = string.Empty;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 8);
                if (ItCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.ItCode=@ItCode0" + r.ToString() + ") ";
                    
                    if (Filter2.Length > 0) Filter2 += " Union All ";
                    Filter2 += " Select @ItCode0" + r.ToString() + " As ItCode, @Qty0" + r.ToString() + " As Qty ";

                    Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);
                    Sm.CmParam<Decimal>(ref cm, "@Qty0" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                }
            }

            if (Filter.Length == 0)
                return false;
            else
                Filter = " And ( " + Filter + " ) ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.MaxStock, IfNull(B.Qty, 0.00) As Stock, IfNull(C.Qty, 0.00) As OutstandingMR, IfNull(D.Qty, 0.00) As MRQty  ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select A.ItCode, Sum(A.Qty) As Qty  ");
            SQL.AppendLine("    From TblStockSummary A ");
            SQL.AppendLine("    Where A.Qty>0.00 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("    Group By A.ItCode ");
            SQL.AppendLine("    ) B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.ItCode, Sum(T2.Qty) As Qty  ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.Status In ('O', 'A') And T2.ProcessInd In ('O', 'P') ");
            SQL.AppendLine(Filter.Replace("A.", "T2."));
            SQL.AppendLine("    Where T1.CancelInd='N' And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    Group By T2.ItCode ");
            SQL.AppendLine("    ) C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    ) D On A.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.MaxStock>0.00 ");
            SQL.AppendLine("And A.PurchaseUomCode=A.InventoryUomCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And A.MaxStock<IfNull(B.Qty, 0.00)+IfNull(C.Qty, 0.00)+IfNull(D.Qty, 0.00) ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        "ItCode", 
                        "ItName", "MaxStock", "Stock", "OutstandingMR", "MRQty"
                    });

                if (dr.HasRows)
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Sm.StdMsg(mMsgType.Warning,
                            "Item's Code : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Item's Name : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Maximum Stock : " + Sm.FormatNum(Sm.DrDec(dr, 2), 0) + Environment.NewLine +
                            "Current Stock : " + Sm.FormatNum(Sm.DrDec(dr, 3), 0) + Environment.NewLine +
                            ((Doctitle == "IMS") ? "Outstanding Purchase Request : " : "Outstanding Material Request : ") + Sm.FormatNum(Sm.DrDec(dr, 4), 0) + Environment.NewLine +
                            "Requested Quantity : " + Sm.FormatNum(Sm.DrDec(dr, 5), 0) + Environment.NewLine + Environment.NewLine +
                            "Invalid maximum stock."
                            );
                            return true;
                        }
                    }
                }
                dr.Close();
            }
            return false;
        }
        
        private bool IsQtyMoreThanQtyApprovalSheet()
        {
            decimal Qty = 0m, OutstandingQty = 0m;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty = Sm.GetGrdDec(Grd1, r, 17);
                OutstandingQty = Sm.GetGrdDec(Grd1, r, 47);
                if(Qty > OutstandingQty)
                {
                    Sm.StdMsg(mMsgType.Warning, "Quantity can't more than Quantity Approval Sheet");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            var DocDt = Sm.GetDte(DteDocDt);
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 17, true, "Quantity should be bigger than 0.00.") ||
                    (mIsUsageDateMaterialRequestMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 19, false, "Usage date is empty.")) ||
                    (mIsUsageDateMaterialRequestMandatory && IsUsageDtNotValid(DocDt, Row)) ||
                    (Sm.GetLue(LueReqType) == "1" && mIsMRAutomaticGetLastVendorQuotation && Sm.IsGrdValueEmpty(Grd1, Row, 23, true, "Quotation's price is 0.")) ||
                    (!mIsRemarkMRNotMandatory && mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 25, false, "Remark is empty.")) ||
                    (mIsMRDurationMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 35, true, "Duration is 0.")) ||
                    (Sm.GetGrdDec(Grd1, Row, 35) != 0 && Sm.IsGrdValueEmpty(Grd1, Row, 37, false, "Duration UOM is empty."))
                    )
                    return true;
               
            }

            return false;
        }

        private bool IsUsageDtNotValid(string DocDt, int Row)
        {
            var UsageDt = Sm.GetGrdDate(Grd1, Row, 19);
            if (Sm.CompareDtTm(UsageDt, DocDt) < 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Document Date : " + Sm.FormatDate2("dd/MMM/yyyy", DocDt) + Environment.NewLine +
                    "Usage Date : " + Sm.FormatDate2("dd/MMM/yyyy", UsageDt) + Environment.NewLine + Environment.NewLine +
                    "Usage date is earlier than document date.");
                Sm.FocusGrd(Grd1, Row, 19);
                return true;
            }
            return false;
        }

        private bool IsRemainingBudgetNotValid()
        {
            decimal RemainingBudget = 0m;

            if (TxtRemainingBudget.Text.Length != 0) RemainingBudget = decimal.Parse(TxtRemainingBudget.Text);
           //harusnya <0
            if (RemainingBudget<0)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid remaining budget.");
                return true;
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where UserCode Is Not Null ");
            if (mIsMRSPPJB)
                SQL.AppendLine("And DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("And DocType = 'MaterialRequest' ");
            SQL.AppendLine("And DeptCode Is Not Null ");
            SQL.AppendLine("And DeptCode = @Param1 ");
            if (mIsApprovalBySiteMandatory)
            {
                SQL.AppendLine("And SiteCode Is Not Null ");
                SQL.AppendLine("And SiteCode=@Param2 ");
            }

            SQL.AppendLine("Limit 1; ");

            if (!Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode), string.Empty)) 
                return true;
            else 
                return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 13).Length == 0)
                {
                    Grd1.Cells[Row, 13].Value = Grd1.Cells[Row, 14].Value = "XXX";
                }
            }
        }

        private bool IsSubCategoryXXX()
        {
            if (IsProcFormat == "1")
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14) == "XXX")
                    {
                        Msg =
                        "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine;

                        Sm.StdMsg(mMsgType.Warning, Msg + "doesn't have Sub-Category.");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 13);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 13))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsPOQtyCancelDocNoNotValid()
        {
            if (TxtPOQtyCancelDocNo.Text.Length == 0) return false;

            var cm = new MySqlCommand() 
            { 
                CommandText = 
                "Select DocNo From TblPOQtyCancel " +
                "Where DocNo=@Param And CancelInd='N' And ProcessInd='O' And NewMRInd='Y';"
            };
            Sm.CmParam<String>(ref cm, "@Param", TxtPOQtyCancelDocNo.Text);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Cancellation PO# is invalid.");
                return true;
            }
            return false;
        }

        private bool IsCancelReasonEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveMaterialRequest(string DocNo, string LocalDocNo, bool NoNeedApproval)
        {
            var SQL1 = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            SQL1.AppendLine("/* Material Request */ ");
            SQL1.AppendLine("Set @Dt:=CurrentDateTime();");

            #region Hdr

            SQL1.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, LocalDocNo, POQtyCancelDocNo, DORequestDocNo, DocDt, SiteCode, DeptCode, PICCode, ReqType, BCCode, SeqNo, ItScCode, Mth, Yr, Revision, ");
            if (TxtDroppingRequestDocNo.Text.Length > 0)
                SQL1.AppendLine("DroppingRequestDocNo, DroppingRequestBCCode, ");
            if (mIsMRSPPJBEnabled) SQL1.AppendLine("SPPJBInd, ");
            if (mIsMRWithTenderEnabled) SQL1.AppendLine("TenderInd, ");
            if (mIsUseECatalog) SQL1.AppendLine("RMInd, ");
            if (mIsMRUseProcurementType) SQL1.AppendLine("ProcurementType, ");
            if (mIsMRUseReference) SQL1.AppendLine("ReferenceDocNo, ");
            if (mIsMRUseBudgetForMR) SQL1.AppendLine("BudgetForMR, ");
            if (mIsMRSaveAvailableBudget) SQL1.AppendLine("AvailableBudget, ");
            if (mIsMRUseApprovalSheet) SQL1.AppendLine("MRType, ApprovalSheetDocNo, ");

            SQL1.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL1.AppendLine("Values(@DocNo, @LocalDocNo, @POQtyCancelDocNo, @DORequestDocNo, @DocDt, @SiteCode, @DeptCode, @PICCode, @ReqType, @BCCode, @SeqNo, @ItScCode, @Mth, @Yr, @Revision, ");
            if (TxtDroppingRequestDocNo.Text.Length > 0)
                SQL1.AppendLine("@DroppingRequestDocNo, @DroppingRequestBCCode, ");
            if (mIsMRSPPJBEnabled)
            {
                if (mIsMRSPPJB)
                    SQL1.AppendLine("'Y', ");
                else
                    SQL1.AppendLine("'N', ");
            }
            if (mIsMRWithTenderEnabled) SQL1.AppendLine("@TenderInd, ");
            if (mIsUseECatalog) SQL1.AppendLine("@RMInd, ");
            if (mIsMRUseProcurementType) SQL1.AppendLine("@ProcurementType, ");
            if (mIsMRUseReference)
                SQL1.AppendLine("@ReferenceDocNo, ");
            if (mIsMRUseBudgetForMR) SQL1.AppendLine("@BudgetForMR, ");
            if (mIsMRSaveAvailableBudget) SQL1.AppendLine("@AvailableBudget, ");
            if (mIsMRUseApprovalSheet) SQL1.AppendLine("@MRType, @ApprovalSheetDocNo, ");

            SQL1.AppendLine("@Remark, @CreateBy, @Dt);");

            if (TxtPOQtyCancelDocNo.Text.Length > 0)
                SQL1.AppendLine("Update TblPOQtyCancel Set ProcessInd='F' Where DocNo=@POQtyCancelDocNo;");

            #endregion

            #region Dtl

            SQL2.AppendLine("Insert Into TblMaterialRequestDtl ");
            SQL2.AppendLine("(DocNo, DNo, CancelInd, CancelReason, Status, ItCode, Qty, UsageDt, QtDocNo, QtDNo, DORequestDocNo, DORequestDNo, UPrice, CurCode, EstPrice, TotalPrice, Remark, Duration, DurationUom, ");
            if(mIsMRUseApprovalSheet)
                SQL2.AppendLine("ApprovalSheetDocNo, ApprovalSheetDNo, ");
            SQL2.AppendLine("CreateBy, CreateDt) ");
            SQL2.AppendLine("Values ");
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL2.AppendLine(", ");
                    SQL2.AppendLine("(@DocNo, @DNo_"+r.ToString()+ ", 'N', @CancelReason_" + r.ToString() + ", 'O', @ItCode_" + r.ToString() + ", @Qty_" + r.ToString() + ", @UsageDt_" + r.ToString() + ", @QtDocNo_" + r.ToString() + ", @QtDNo_" + r.ToString() + ", @DORequestDocNo_" + r.ToString() + ", @DORequestDNo_" + r.ToString() + ", ");
                    SQL2.AppendLine("@UPrice_" + r.ToString() + ", @CurCode_" + r.ToString() + ", @EstPrice_" + r.ToString() + ", @TotalPrice_" + r.ToString() + ", @Remark_" + r.ToString() + ", @Duration_" + r.ToString() + ", @DurationUom_" + r.ToString() + ", ");
                    if(mIsMRUseApprovalSheet)
                        SQL2.AppendLine("@ApprovalSheetDocNo_" + r.ToString() + ", @ApprovalSheetDNo_" + r.ToString() + ", ");
                    SQL2.AppendLine("@CreateBy, @Dt) ");

                    if (TxtDroppingRequestDocNo.Text.Length > 0 && mIsMaterialRequestForDroppingRequestEnabled)
                    {
                        if (mIsDroppingRequestUseMRReceivingPartial)
                        {
                            if (TxtDR_PRJIDocNo.Text.Length > 0)
                            {
                                SQL3.AppendLine("Update TblDroppingRequestDtl A ");
                                SQL3.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo=B.DocNo ");
                                SQL3.AppendLine("Inner Join TblMaterialRequestDtl C On B.ResourceItCode=C.ItCode And C.DocNo=@DocNo ");
                                SQL3.AppendLine("Set A.DRSourceInd='1' ");
                                SQL3.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                                SQL3.AppendLine("And A.MRDocNo Is Null; ");
                            }
                            if (TxtDR_BCCode.Text.Length > 0)
                            {
                                SQL3.AppendLine("Update TblDroppingRequestDtl2 A ");
                                SQL3.AppendLine("Inner Join TblMaterialRequestHdr B On A.BCCode=B.DroppingRequestBCCode And B.DocNo=@DocNo ");
                                SQL3.AppendLine("Inner Join TblMaterialRequestDtl C On A.ItCode=C.ItCode And B.DocNo=C.DocNo ");
                                SQL3.AppendLine("Set A.DRSourceInd='1' ");
                                SQL3.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                                SQL3.AppendLine("And A.MRDocNo Is Null; ");
                            }
                        }
                        else
                        {
                            if (TxtDR_PRJIDocNo.Text.Length > 0)
                            {
                                SQL3.AppendLine("Update TblDroppingRequestDtl A ");
                                SQL3.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo=B.DocNo ");
                                SQL3.AppendLine("Inner Join TblMaterialRequestDtl C On B.ResourceItCode=C.ItCode And C.DocNo=@DocNo ");
                                SQL3.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
                                SQL3.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                                SQL3.AppendLine("And A.MRDocNo Is Null; ");
                            }
                            if (TxtDR_BCCode.Text.Length > 0)
                            {
                                SQL3.AppendLine("Update TblDroppingRequestDtl2 A ");
                                SQL3.AppendLine("Inner Join TblMaterialRequestHdr B On A.BCCode=B.DroppingRequestBCCode And B.DocNo=@DocNo ");
                                SQL3.AppendLine("Inner Join TblMaterialRequestDtl C On A.ItCode=C.ItCode And B.DocNo=C.DocNo ");
                                SQL3.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
                                SQL3.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                                SQL3.AppendLine("And A.ItCode=@ItCode_" + r.ToString() + " ");
                                SQL3.AppendLine("And A.MRDocNo Is Null; ");
                            }
                        }
                    }

                    if(mIsMRUseApprovalSheet)
                    {
                        SQL3.AppendLine("Update TblApprovalSheetDtl A ");
                        SQL3.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.ApprovalSheetDocNo And A.DNo=B.ApprovalSheetDNo ");
                        SQL3.AppendLine("");
                        SQL3.AppendLine("Set A.OutstandingQty=A.OutstandingQty-B.Qty ");
                        SQL3.AppendLine("Where A.DocNo=@ApprovalSheetDocNo; ");
                    }

                    if (!NoNeedApproval)
                    {
                        SQL3.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                        SQL3.AppendLine("Select T.DocType, @DocNo, @DNo_" + r.ToString() + ", T.DNo, @CreateBy, @Dt ");
                        SQL3.AppendLine("From TblDocApprovalSetting T ");
                        SQL3.AppendLine("Where T.DeptCode=@DeptCode ");
                        if (mIsApprovalBySiteMandatory)
                            SQL3.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                        if (mIsMRApprovalByAmount)
                        {

                            SQL3.AppendLine("And (T.EndAmt=0 ");
                            SQL3.AppendLine("Or T.EndAmt>=IfNull(( ");
                            if (mAmtSourceMRApproval == "2")
                                SQL3.AppendLine("    Select IfNull(TotalPrice, 0) ");
                            else if (mAmtSourceMRApproval == "1")
                                SQL3.AppendLine("    Select IfNull(EstPrice, 0) ");
                            else if (mAmtSourceMRApproval == "3")
                                SQL3.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                            SQL3.AppendLine("    From TblMaterialRequestDtl ");
                            SQL3.AppendLine("    Where DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ");
                            SQL3.AppendLine("), 0)) ");

                            SQL3.AppendLine("And (T.StartAmt=0 ");
                            SQL3.AppendLine("Or T.StartAmt<=IfNull(( ");
                            if (mAmtSourceMRApproval == "2")
                                SQL3.AppendLine("    Select IfNull(TotalPrice, 0) ");
                            else if (mAmtSourceMRApproval == "1")
                                SQL3.AppendLine("    Select IfNull(EstPrice, 0) ");
                            else if (mAmtSourceMRApproval == "3")
                                SQL3.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                            SQL3.AppendLine("    From TblMaterialRequestDtl ");
                            SQL3.AppendLine("    Where DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ");
                            SQL3.AppendLine("), 0)) ");

                        }
                        if (mIsMRSPPJB)
                            SQL3.AppendLine("And T.DocType = 'MaterialRequestSPPJB' ");
                        else
                            SQL3.AppendLine("And T.DocType='MaterialRequest' ");

                        if (mIsDocApprovalSettingUseItemCt)
                        {
                            SQL3.AppendLine("And T.ItCtCode In ( ");
                            SQL3.AppendLine("   Select B.ItCtCode ");
                            SQL3.AppendLine("   From TblMaterialRequestDtl A ");
                            SQL3.AppendLine("   Inner Join TblItem B On A.ItCode = B.ItCode ");
                            SQL3.AppendLine("   Where A.DocNo = @DocNo And A.DNo = @DNo_" + r.ToString());
                            SQL3.AppendLine(") ");
                        }
                        SQL3.AppendLine("; ");
                    }

                    SQL3.AppendLine("Update TblMaterialRequestDtl ");
                    SQL3.AppendLine("Set Status = 'A' ");
                    SQL3.AppendLine("Where DocNo = @DocNo And DNo=@DNo_" + r.ToString() + " ");
                    SQL3.AppendLine("And Not Exists( ");
                    SQL3.AppendLine("    Select 1 ");
                    SQL3.AppendLine("    From TblDocApproval ");
                    if (mIsMRSPPJB)
                        SQL3.AppendLine("    Where DocType = 'MaterialRequestSPPJB' ");
                    else
                        SQL3.AppendLine("    Where DocType = 'MaterialRequest' ");
                    SQL3.AppendLine("    And DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ");
                    SQL3.AppendLine("); ");

                    if (mIsUseECatalog)
                    {
                        SQL3.AppendLine("Update TblMaterialRequestDtl ");
                        SQL3.AppendLine("Set PtCode = @PtCode_" + r.ToString() + ", CityCode = @CityCode_" + r.ToString() + " ");
                        SQL3.AppendLine("Where Docno = @DocNo ");
                        SQL3.AppendLine("And DNo = @DNo_" + r.ToString() + " ");
                        SQL3.AppendLine("; ");
                    }

                    Sm.CmParam<String>(ref cm, "@DNo_"+r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@CancelReason_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                    Sm.CmParamDt(ref cm, "@UsageDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 19));
                    Sm.CmParam<String>(ref cm, "@QtDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 20));
                    Sm.CmParam<String>(ref cm, "@QtDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 21));
                    Sm.CmParam<String>(ref cm, "@DORequestDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 26));
                    Sm.CmParam<String>(ref cm, "@DORequestDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 27));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 23));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 25));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 28));
                    Sm.CmParam<Decimal>(ref cm, "@EstPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 29));
                    Sm.CmParam<Decimal>(ref cm, "@TotalPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 38));
                    Sm.CmParam<String>(ref cm, "@DurationUom_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 36));
                    Sm.CmParam<Decimal>(ref cm, "@Duration_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 35));
                    Sm.CmParam<String>(ref cm, "@ApprovalSheetDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 45));
                    Sm.CmParam<String>(ref cm, "@ApprovalSheetDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 46));
                    if (mIsUseECatalog)
                    {
                        Sm.CmParam<String>(ref cm, "@PtCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 39));
                        Sm.CmParam<String>(ref cm, "@CityCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 42));
                    }
                }
            }

            #endregion

            cm.CommandText = SQL1.ToString() + SQL2.ToString() + ";" + SQL3.ToString();

            #region Hdr

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DORequestDocNo", TxtDORequestDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PICCode", Sm.GetLue(LuePICCode));
            Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
            Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            if (IsAutoGeneratePurchaseLocalDocNo)
            {
                Sm.CmParam<String>(ref cm, "@SeqNo", Sm.Left(LocalDocNo, 6));
                Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetGrdStr(Grd1, 0, 13));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetDte(DteDocDt), 4));
                Sm.CmParam<String>(ref cm, "@Revision", "");
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@SeqNo", "");
                Sm.CmParam<String>(ref cm, "@ItScCode", "");
                Sm.CmParam<String>(ref cm, "@Mth", "");
                Sm.CmParam<String>(ref cm, "@Yr", "");
                Sm.CmParam<String>(ref cm, "@Revision", "");
            }
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
            Sm.CmParam<String>(ref cm, "@ApprovalSheetDocNo", TxtApprovalSheetDocNo.Text);
            if (mIsMRUseProcurementType) Sm.CmParam<String>(ref cm, "@ProcurementType", Sm.GetLue(LueProcurementType));
            if (mIsMRUseReference) Sm.CmParam<String>(ref cm, "@ReferenceDocNo", TxtReference.Text.Trim());
            if (mIsMRUseBudgetForMR)
                Sm.CmParam<Decimal>(ref cm, "@BudgetForMR", Decimal.Parse(TxtBudgetForMR.Text));
            if (mIsMRSaveAvailableBudget)
                Sm.CmParam<Decimal>(ref cm, "@AvailableBudget", Decimal.Parse(TxtRemainingBudget.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@TenderInd", ChkTenderInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@RMInd", ChkRMInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", decimal.Parse(TxtGrandTotal.Text));
            Sm.CmParam<String>(ref cm, "@MRType", Sm.GetLue(LueMRType));
            

            #endregion

            return cm;
        }

        //private MySqlCommand SaveMaterialRequestHdr(string DocNo, string LocalDocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblMaterialRequestHdr(DocNo, LocalDocNo, POQtyCancelDocNo, DORequestDocNo, DocDt, SiteCode, DeptCode, PICCode, ReqType, BCCode, SeqNo, ItScCode, Mth, Yr, Revision, ");
        //    if (TxtDroppingRequestDocNo.Text.Length>0)
        //        SQL.AppendLine("DroppingRequestDocNo, DroppingRequestBCCode, ");
        //    if (mIsMRSPPJBEnabled) SQL.AppendLine("SPPJBInd, ");
        //    if (mIsMRWithTenderEnabled) SQL.AppendLine("TenderInd, ");
        //    if (mIsUseECatalog) SQL.AppendLine("RMInd, ");
        //    if (mIsMRUseProcurementType) SQL.AppendLine("ProcurementType, ");
        //    if (mIsMRUseReference) SQL.AppendLine("ReferenceDocNo, ");
            
        //    SQL.AppendLine("Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @LocalDocNo, @POQtyCancelDocNo, @DORequestDocNo, @DocDt, @SiteCode, @DeptCode, @PICCode, @ReqType, @BCCode, @SeqNo, @ItScCode, @Mth, @Yr, @Revision, ");
        //    if (TxtDroppingRequestDocNo.Text.Length > 0)
        //        SQL.AppendLine("@DroppingRequestDocNo, @DroppingRequestBCCode, ");
        //    if (mIsMRSPPJBEnabled)
        //    {
        //        if (mIsMRSPPJB)
        //            SQL.AppendLine("'Y', ");
        //        else
        //            SQL.AppendLine("'N', ");
        //    }
        //    if (mIsMRWithTenderEnabled) SQL.AppendLine("@TenderInd, ");
        //    if (mIsUseECatalog) SQL.AppendLine("@RMInd, ");
        //    if (mIsMRUseProcurementType) SQL.AppendLine("@ProcurementType, ");
        //    if (mIsMRUseReference)
        //        SQL.AppendLine("@ReferenceDocNo, ");
            
        //   SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime());");

        //    if (TxtPOQtyCancelDocNo.Text.Length > 0)
        //        SQL.AppendLine("Update TblPOQtyCancel Set ProcessInd='F' Where DocNo=@POQtyCancelDocNo;");        
            
        //    var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
        //    Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DORequestDocNo", TxtDORequestDocNo.Text);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
        //    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
        //    Sm.CmParam<String>(ref cm, "@PICCode", Sm.GetLue(LuePICCode));
        //    Sm.CmParam<String>(ref cm, "@ReqType", Sm.GetLue(LueReqType));
        //    Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
        //    if (IsAutoGeneratePurchaseLocalDocNo)
        //    {
        //        Sm.CmParam<String>(ref cm, "@SeqNo", Sm.Left(LocalDocNo, 6));
        //        Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetGrdStr(Grd1, 0, 13));
        //        Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //        Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetDte(DteDocDt), 4));
        //        Sm.CmParam<String>(ref cm, "@Revision", "");
        //    }
        //    else
        //    {
        //        Sm.CmParam<String>(ref cm, "@SeqNo", "");
        //        Sm.CmParam<String>(ref cm, "@ItScCode", "");
        //        Sm.CmParam<String>(ref cm, "@Mth", "");
        //        Sm.CmParam<String>(ref cm, "@Yr", "");
        //        Sm.CmParam<String>(ref cm, "@Revision", "");
        //    }
        //    Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
        //    if(mIsMRUseProcurementType) Sm.CmParam<String>(ref cm, "@ProcurementType", Sm.GetLue(LueProcurementType));

        //    if (mIsMRUseReference) 
        //        Sm.CmParam<String>(ref cm, "@ReferenceDocNo",TxtReference.Text.Trim());

            
        //    Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
        //    Sm.CmParam<String>(ref cm, "@TenderInd", ChkTenderInd.Checked ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@RMInd", ChkRMInd.Checked ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    return cm;
        //}

        //private MySqlCommand SaveMaterialRequestDtl(string DocNo, int Row, bool NoNeedApproval)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblMaterialRequestDtl(DocNo, DNo, CancelInd, CancelReason, Status, ItCode, Qty, UsageDt, QtDocNo, QtDNo, DORequestDocNo, DORequestDNo, UPrice, CurCode, EstPrice, TotalPrice, Remark, Duration, DurationUom, CreateBy, CreateDt)");
        //    SQL.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, 'O', @ItCode, @Qty, @UsageDt, @QtDocNo, @QtDNo, @DORequestDocNo, @DORequestDNo, @UPrice, @CurCode, @EstPrice, @TotalPrice, @Remark, @Duration, @DurationUom, @CreateBy, CurrentDateTime()); ");

        //    if (TxtDroppingRequestDocNo.Text.Length > 0)
        //    {
        //        if (TxtDR_PRJIDocNo.Text.Length > 0)
        //        {
        //            SQL.AppendLine("Update TblDroppingRequestDtl A ");
        //            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr B On A.PRBPDocNo=B.DocNo ");
        //            SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.ResourceItCode=C.ItCode And C.DocNo=@DocNo ");
        //            SQL.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
        //            SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
        //            SQL.AppendLine("And A.MRDocNo Is Null; ");
        //        }
        //        if (TxtDR_BCCode.Text.Length > 0)
        //        {
        //            SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
        //            SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.BCCode=B.DroppingRequestBCCode And B.DocNo=@DocNo ");
        //            SQL.AppendLine("Inner Join TblMaterialRequestDtl C On A.ItCode=C.ItCode And B.DocNo=C.DocNo ");
        //            SQL.AppendLine("Set A.MRDocNo=C.DocNo, A.MRDNo=C.DNo ");
        //            SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
        //            SQL.AppendLine("And A.ItCode=@ItCode ");
        //            SQL.AppendLine("And A.MRDocNo Is Null; ");
        //        }
        //    }

        //    if (!NoNeedApproval)
        //    {
        //        SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
        //        SQL.AppendLine("From TblDocApprovalSetting T ");
        //        SQL.AppendLine("Where T.DeptCode=@DeptCode ");
        //        if (mIsApprovalBySiteMandatory)
        //            SQL.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
        //        if (mIsMRApprovalByAmount)
        //        {
        //            SQL.AppendLine("And (T.StartAmt=0 ");
        //            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
        //            if(mAmtSourceMRApproval == "2")
        //                SQL.AppendLine("    Select IfNull(TotalPrice, 0) ");
        //            else
        //                SQL.AppendLine("    Select IfNull(EstPrice, 0) ");
        //            SQL.AppendLine("    From TblMaterialRequestDtl ");
        //            SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo ");
        //            SQL.AppendLine("), 0)) ");
        //        }
        //        if (mIsMRSPPJB)
        //            SQL.AppendLine("And T.DocType = 'MaterialRequestSPPJB' ");
        //        else
        //            SQL.AppendLine("And T.DocType='MaterialRequest' ");
        //        SQL.AppendLine("; ");
        //    }

        //    SQL.AppendLine("Update TblMaterialRequestDtl ");
        //    SQL.AppendLine("Set Status = 'A' ");
        //    SQL.AppendLine("Where DocNo = @DocNo And DNo=@DNo ");
        //    SQL.AppendLine("And Not Exists( ");
        //    SQL.AppendLine("    Select 1 ");
        //    SQL.AppendLine("    From TblDocApproval ");
        //    if (mIsMRSPPJB)
        //        SQL.AppendLine("    Where DocType = 'MaterialRequestSPPJB' ");
        //    else
        //        SQL.AppendLine("    Where DocType = 'MaterialRequest' ");
        //    SQL.AppendLine("    And DocNo=@DocNo And DNo=@DNo ");
        //    SQL.AppendLine("); ");

        //    if (mIsUseECatalog)
        //    {
        //        SQL.AppendLine("Update TblMaterialRequestDtl ");
        //        SQL.AppendLine("Set PtCode = @PtCode, CityCode = @CityCode ");
        //        SQL.AppendLine("Where Docno = @DocNo ");
        //        SQL.AppendLine("And DNo = @DNo ");
        //        SQL.AppendLine("; ");
        //    }
        
        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
        //    Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 17));
        //    Sm.CmParamDt(ref cm, "@UsageDt", Sm.GetGrdDate(Grd1, Row, 19));
        //    Sm.CmParam<String>(ref cm, "@QtDocNo", Sm.GetGrdStr(Grd1, Row, 20));
        //    Sm.CmParam<String>(ref cm, "@QtDNo", Sm.GetGrdStr(Grd1, Row, 21));
        //    Sm.CmParam<String>(ref cm, "@DORequestDocNo", Sm.GetGrdStr(Grd1, Row, 26));
        //    Sm.CmParam<String>(ref cm, "@DORequestDNo", Sm.GetGrdStr(Grd1, Row, 27));
        //    Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 25));
        //    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, Row, 28));
        //    Sm.CmParam<Decimal>(ref cm, "@EstPrice", Sm.GetGrdDec(Grd1, Row, 29));
        //    Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Sm.GetGrdDec(Grd1, Row, 38));
        //    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
        //    Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@DurationUom", Sm.GetGrdStr(Grd1, Row, 36));
        //    Sm.CmParam<Decimal>(ref cm, "@Duration", Sm.GetGrdDec(Grd1, Row, 35));
        //    if (mIsUseECatalog)
        //    {
        //        Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetGrdStr(Grd1, Row, 39));
        //        Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetGrdStr(Grd1, Row, 42));
        //    }
        //    return cm;
        //}

        private MySqlCommand SaveMaterialRequestDtl2(string DocNo)
        {
            var SQL1 = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;
            
            SQL1.AppendLine("Set @Dt:=CurrentDateTime();");
            SQL1.AppendLine("Insert Into TblMaterialRequestDtl2 ");
            SQL1.AppendLine("(DocNo, ItCode, JobCode, PrevCurCode, PrevUPrice, CurCode, UPrice, Qty, Remark, CreateBy, CreateDt) ");
            SQL1.AppendLine("Values ");
            foreach (var x in mlJob)
            {
                if (i>0) SQL1.AppendLine(", ");
                SQL1.AppendLine("(@DocNo, @ItCode" + i + ", @JobCode" + i + ", @PrevCurCode" + i + ", @PrevUPrice" + i + ", @CurCode" + i + ", @UPrice" + i + ", @Qty" + i + ", @Remark" + i + ", @UserCode, @Dt) ");

                SQL2.AppendLine("Update TblJob Set ");
                SQL2.AppendLine("    CurCode=@CurCode" + i + ", UPrice=@UPrice" + i);
                SQL2.AppendLine(" Where JobCode=@JobCode" + i + ";");

                Sm.CmParam<String>(ref cm, "@ItCode" + i, x.ItCode);
                Sm.CmParam<String>(ref cm, "@JobCode" + i, x.JobCode);
                Sm.CmParam<String>(ref cm, "@PrevCurCode" + i, x.PrevCurCode);
                Sm.CmParam<Decimal>(ref cm, "@PrevUPrice" + i, x.PrevUPrice);
                Sm.CmParam<String>(ref cm, "@CurCode" + i, x.CurCode);
                Sm.CmParam<Decimal>(ref cm, "@UPrice" + i, x.UPrice);
                Sm.CmParam<Decimal>(ref cm, "@Qty" + i, x.Qty);
                Sm.CmParam<String>(ref cm, "@Remark" + i, x.Remark);

                i++;
            }
            
            cm.CommandText = SQL1.ToString() + ";" + SQL2.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelMaterialRequestDtl(TxtDocNo.Text, DNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 8).Length > 0)
                    cml.Add(CancelMaterialRequestDtl2(TxtDocNo.Text, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select DNo, CancelInd From TblMaterialRequestDtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                IsCancelledItemNotExisted(DNo) ||
                IsCancelledItemCheckedAlready(DNo)||
                IsCancelReasonEmpty() ;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledItemCheckedAlready(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblPORequestDtl ");
            SQL.AppendLine("Where MaterialRequestDocNo=@DocNo ");
            SQL.AppendLine("And (CancelInd='N' And IfNull(Status, 'O')<>'C') ");
            SQL.AppendLine("And MaterialRequestDNo In (" + DNo + ") ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has been processed.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelMaterialRequestDtl(string DocNo, string DNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (CancelInd='N' Or Status='C') And DNo In (" + DNo + "); ");


            if (!mIsDORequestNeedStockValidation)
            {
                SQL.AppendLine("Update TblMaterialRequestDtl Set ");
                SQL.AppendLine("  DORequestDocNo = null, ");
                SQL.AppendLine("  DORequestDNo = null ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And DORequestDocNo Is Not Null ");
                SQL.AppendLine("And DORequestDNo Is Not Null ");
                SQL.AppendLine("And DNo In (" + DNo + "); ");
            }

            if(mIsMRUseBudgetForMR)
            {
                SQL.AppendLine("Update TblMaterialRequestHdr Set ");
                SQL.AppendLine("  BudgetForMR = @BudgetForMR ");
                SQL.AppendLine("Where DocNo = @DocNo; ");
            }

            if (TxtPOQtyCancelDocNo.Text.Length > 0)
                SQL.AppendLine("Update TblPOQtyCancel Set ProcessInd='O' Where DocNo=@POQtyCancelDocNo;");        
           
            SQL.AppendLine("Update TblPRDtl A ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr B ");
            SQL.AppendLine("    On A.DocNo=IfNull(B.PRDocNo, '') ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl C ");
            SQL.AppendLine("    On B.DocNo=C.DocNo ");
            SQL.AppendLine("    And A.DNo=IfNull(C.PRDNo, '') ");
            SQL.AppendLine("    And (C.CancelInd='Y' Or C.Status='C') ");
            SQL.AppendLine("    And C.DNo In (" + DNo + ") ");
            SQL.AppendLine("Set A.CancelInd='Y', A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Update TblMakeToStockDtl A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.MakeToStockDocNo As DocNo, T2.MakeToStockDNo As DNo, ");
            SQL.AppendLine("    Sum(Case When T2.CancelInd='N' Then T2.Qty1 Else 0 End) As Qty1 ");
            SQL.AppendLine("    From TblPRHdr T1 ");
            SQL.AppendLine("    Inner Join TblPRDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("            Select Distinct X1.PRDocNo As DocNo, X2.PRDNo As DNo ");
            SQL.AppendLine("            From TblMaterialRequestHdr X1 ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl X2 ");
            SQL.AppendLine("                On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("                And X2.PRDNo Is Not Null ");
            SQL.AppendLine("                And X2.DNo In (" + DNo + ") ");
            SQL.AppendLine("            Where X1.DocNo=@DocNo ");
            SQL.AppendLine("            And X1.PRDocNo Is Not Null ");
            SQL.AppendLine("    ) T3 On T1.DocNo=T3.DocNo And T2.DNo=T3.DNo ");
            SQL.AppendLine("    Group By T1.MakeToStockDocNo, T2.MakeToStockDNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("Set A.ProcessInd2 = ");
            SQL.AppendLine("    Case When A.Qty<>0 Then ");
            SQL.AppendLine("        Case When IfNull(B.Qty1, 0)=0 Then 'O' ");
            SQL.AppendLine("        Else ");
            SQL.AppendLine("            Case When A.Qty>IfNull(B.Qty1, 0) Then 'P' Else 'F' End ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("    Else 'F' End;");

            if (mIsMRUseApprovalSheet && TxtApprovalSheetDocNo.Text.Length != 0)
            {
                SQL.AppendLine("Update TblApprovalSheetDtl A ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.ApprovalSheetDocNo And A.DNo=B.ApprovalSheetDNo ");
                SQL.AppendLine("Set A.OutstandingQty=A.OutstandingQty+B.Qty ");
                SQL.AppendLine("Where B.DocNo=@DocNo; ");
            }

            if (mIsDroppingRequestUseMRReceivingPartial)
            {
                for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                {
                    if (Sm.GetGrdBool(Grd1, Index, 1) == true)
                    {
                        if (TxtDR_PRJIDocNo.Text.Length > 0)
                        {
                            var SQLi = new StringBuilder();

                            SQLi.AppendLine("SELECT COUNT(MRDocNo) TotalMR  ");
                            SQLi.AppendLine("FROM(  ");
                            SQLi.AppendLine("	SELECT A.DocNo MRDocNo, C.DocNo AS DroppingReqDocNo, E.ResourceItCode AS ItCode, G.ItName  ");
                            SQLi.AppendLine(" 	FROM tblmaterialrequesthdr A  ");
                            SQLi.AppendLine(" 	LEFT JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
                            SQLi.AppendLine(" 	LEFT JOIN tbldroppingrequesthdr C ON A.DroppingRequestDocNo = C.DocNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN tbldroppingrequestdtl D ON C.DocNo = D.DocNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN TblProjectImplementationRBPHdr E On D.PRBPDocNo = E.DocNo  ");
                            SQLi.AppendLine(" 	LEFT JOIN TblProjectImplementationRBPDtl F On E.DocNo = F.DocNo And D.PRBPDNo = F.DNo   ");
                            SQLi.AppendLine(" 	LEFT JOIN tblitem G ON E.ResourceItCode = G.ItCode  ");
                            SQLi.AppendLine("	WHERE A.Status = 'A' AND A.CancelInd = 'N'   ");
                            SQLi.AppendLine("	AND B.CancelInd = 'N' AND B.Status = 'A'   ");
                            SQLi.AppendLine("	AND B.ItCode = E.ResourceItCode ");
                            SQLi.AppendLine("	AND A.DroppingRequestDocNo = @DroppingReqDocNo ");
                            SQLi.AppendLine(")X  ");
                            SQLi.AppendLine("WHERE X.DroppingReqDocNo = @DroppingReqDocNo AND X.ItCode = @ItCode ");

                            var cmi = new MySqlCommand() { CommandText = SQLi.ToString() };
                            Sm.CmParam<String>(ref cmi, "@DroppingReqDocNo", TxtDroppingRequestDocNo.Text);
                            Sm.CmParam<String>(ref cmi, "@ItCode", Sm.GetGrdStr(Grd1, Index, 8));

                            string IsMRWithItemExist = Sm.GetValue(cmi);

                            if (decimal.Parse(IsMRWithItemExist) <= 1)
                            {
                                SQL.AppendLine("UPDATE tbldroppingrequestdtl X1 ");
                                SQL.AppendLine(" SET X1.DRSourceInd = NULL ");
                                SQL.AppendLine("FROM tbldroppingrequestdtl X1 ");
                                SQL.AppendLine("INNER JOIN tblprojectimplementationrbphdr X2 ON X1.PRBPDocNo = X2.DocNo ");
                                SQL.AppendLine("WHERE X1.DocNo = @DroppingRequestDocNo AND X2.ResourceItCode = @ItCode_" + Index + ";");

                                Sm.CmParam<String>(ref cm, "@ItCode_" + Index, Sm.GetGrdStr(Grd1, Index, 8));
                            }
                        }
                        else if (TxtDR_BCCode.Text.Length > 0)
                        {
                            var SQLi = new StringBuilder();

                            SQLi.AppendLine("SELECT COUNT(MRDocNo) TotalMR ");
                            SQLi.AppendLine("FROM( ");
                            SQLi.AppendLine("	SELECT A.DocNo MRDocNo, D.DocNo AS DroppingReqDocNo, B.ItCode, E.ItName ");
                            SQLi.AppendLine("	FROM tblmaterialrequesthdr A  ");
                            SQLi.AppendLine("	LEFT JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo  ");
                            SQLi.AppendLine("	LEFT JOIN tbldroppingrequesthdr C ON A.DroppingRequestDocNo = C.DocNo  ");
                            SQLi.AppendLine("	LEFT JOIN tbldroppingrequestdtl2 D ON C.DocNo = D.DocNo AND A.DroppingRequestBCCode = D.BCCode  ");
                            SQLi.AppendLine("	LEFT JOIN tblitem E ON D.ItCode = E.ItCode  ");
                            SQLi.AppendLine("	WHERE A.Status = 'A' AND A.CancelInd = 'N'  ");
                            SQLi.AppendLine("	AND B.CancelInd = 'N' AND B.Status = 'A'  ");
                            SQLi.AppendLine("	AND B.ItCode = D.ItCode ");
                            SQLi.AppendLine("	AND A.DroppingRequestDocNo = @DroppingReqDocNo   ");
                            SQLi.AppendLine(")X ");
                            SQLi.AppendLine("WHERE X.DroppingReqDocNo = @DroppingReqDocNo AND X.ItCode = @ItCode ");

                            var cmi = new MySqlCommand() { CommandText = SQLi.ToString() };
                            Sm.CmParam<String>(ref cmi, "@DroppingReqDocNo", TxtDroppingRequestDocNo.Text);
                            Sm.CmParam<String>(ref cmi, "@ItCode", Sm.GetGrdStr(Grd1, Index, 8));

                            string IsMRWithItemExist = Sm.GetValue(cmi);

                            if (decimal.Parse(IsMRWithItemExist) <= 1)
                            {
                                SQL.AppendLine("UPDATE TblDroppingRequestDtl2 ");
                                SQL.AppendLine(" SET DRSourceInd = NULL ");
                                SQL.AppendLine("WHERE DocNo = @DroppingRequestDocNo and ItCode = @ItCode_" + Index + ";");

                                Sm.CmParam<String>(ref cm, "@ItCode_" + Index, Sm.GetGrdStr(Grd1, Index, 8));
                            }
                        }
                    }

                }
            }
            else {
                if (TxtDroppingRequestDocNo.Text.Length > 0)
                {
                    if (TxtDR_PRJIDocNo.Text.Length > 0)
                    {
                        SQL.AppendLine("Update TblDroppingRequestDtl A ");
                        SQL.AppendLine("Inner Join TblMaterialRequestDtl B ");
                        SQL.AppendLine("    On A.MRDocNo=B.DocNo ");
                        SQL.AppendLine("    And A.MRDNo=B.DNo ");
                        SQL.AppendLine("    And (B.CancelInd='Y' Or B.Status='C') ");
                        SQL.AppendLine("Set A.MRDocNo=Null, A.MRDNo=Null ");
                        SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                        SQL.AppendLine("And A.MRDocNo Is Not Null ");
                        SQL.AppendLine("And A.MRDocNo=@DocNo;");
                    }
                    if (TxtDR_BCCode.Text.Length > 0)
                    {
                        SQL.AppendLine("Update TblDroppingRequestDtl2 A ");
                        SQL.AppendLine("Inner Join TblMaterialRequestDtl B ");
                        SQL.AppendLine("    On A.MRDocNo=B.DocNo ");
                        SQL.AppendLine("    And A.MRDNo=B.DNo ");
                        SQL.AppendLine("    And (B.CancelInd='Y' Or B.Status='C') ");
                        SQL.AppendLine("Set A.MRDocNo=Null, A.MRDNo=Null ");
                        SQL.AppendLine("Where A.DocNo=@DroppingRequestDocNo ");
                        SQL.AppendLine("And A.MRDocNo Is Not Null ");
                        SQL.AppendLine("And A.MRDocNo=@DocNo;");
                    }
                }
            }
           
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (TxtPOQtyCancelDocNo.Text.Length > 0) Sm.CmParam<String>(ref cm, "@POQtyCancelDocNo", TxtPOQtyCancelDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
            if (mIsMRUseBudgetForMR)
                Sm.CmParam<Decimal>(ref cm, "@BudgetForMR", Decimal.Parse(TxtBudgetForMR.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cm.CommandText = SQL.ToString();
            return cm;
        }

        private MySqlCommand CancelMaterialRequestDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl Set ");
            SQL.AppendLine("    CancelReason=@CancelReason ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@Dno ; ");

            if (SelectMRinIEP())
            {
                SQL.AppendLine("Update TblIndependentEstimatedPriceHdr A ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.MrDocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestHdr C On B.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblIndependentEstimatedPriceDtl D On A.DocNo = D.DocNo And D.MrDNo = B.DNo ");
                SQL.AppendLine("Set A.CancelReason = B.CancelReason, A.CancelInd = 'Y', A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where A.MrDocNo = @DocNo And B.DNo = '001' ;");
            }


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Dno", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateMRFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile4(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName4=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile5(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName5=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateMRFile6(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequesthdr Set ");
            SQL.AppendLine("    FileName6=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowMaterialRequestHdr(DocNo);
                ShowDroppingRequestInfo();
                ShowMaterialRequestDtl(DocNo, false); //false adl reference ind, digunakan di MaterialRequestDlg8
                if (mIsMRSPPJB) ShowMaterialRequestDtl2(DocNo);
                if (mIsMRUseReview) ShowMaterialRequestReview(TxtReference.Text);
                if (mIsUseECatalog) ShowRFQList(DocNo);
                //ComputeRemainingBudget();
                if(mIsMRShowTotalEstimatedPrice)
                    ComputeGrandTotal();
                ComputeDR_Balance();
                SetSeqNo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void ShowMaterialRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, LocalDocNo, DocDt, POQtyCancelDocNo, ");
            SQL.AppendLine("SiteCode, DeptCode, PICCode, ReqType, DORequestDocNo, BCCode, FileName, filename2, filename3, Remark, DroppingRequestDocNo, DroppingRequestBCCode, ");

            SQL.AppendLine("ProcurementType, ");
            //else SQL.AppendLine("Null As OptCode, ");

            if (mIsUseECatalog) SQL.AppendLine("RMInd, ");
            else SQL.AppendLine("'N' AS RMInd,");

            if (mIsMRWithTenderEnabled) SQL.AppendLine("TenderInd, ");
            else SQL.AppendLine("'N' As TenderInd, ");

            if (mIsMRUseBudgetForMR) SQL.AppendLine("BudgetForMR, ");
            else SQL.AppendLine("0.00 BudgetForMR, ");

            if (mIsMRSaveAvailableBudget) SQL.AppendLine("AvailableBudget, ");
            else
            {
                SQL.AppendLine("0.00 AvailableBudget, ");
                ComputeRemainingBudget();
            }
            if (mIsMRUseApprovalSheet)
                SQL.AppendLine("MRType, ApprovalSheetDocNo, ");
            else
                SQL.AppendLine("Null MRType, Null ApprovalSheetDocNo, ");
                

            SQL.AppendLine("FileName4, FileName5, FileName6, ReferenceDocNo ");
            
            SQL.AppendLine("From TblMaterialRequestHdr Where DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 
                    //1-5
                    "LocalDocNo", "DocDt", "POQtyCancelDocNo", "SiteCode", "DeptCode",  
                    //6-10
                    "ReqType", "BCCode", "FileName", "Remark", "DORequestDocNo",
                    //11-15
                    "PICCode", "DroppingRequestDocNo", "DroppingRequestBCCode", "filename2", "filename3",
                    //16-20
                    "TenderInd", "ProcurementType", "FileName4", "FileName5", "FileName6",
                    //20-25
                    "ReferenceDocNo", "RMInd", "BudgetForMR", "AvailableBudget", "MrType",
                    //26
                    "ApprovalSheetDocNo"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[4]), "N");            
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]), "N");
                    SetLueReqType(ref LueReqType, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LueReqType, Sm.DrStr(dr, c[6]));
                    Sl.SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[7]), string.Empty);
                    TxtFile.EditValue = Sm.DrStr(dr, c[8]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                    TxtDORequestDocNo.EditValue = Sm.DrStr(dr, c[10]);
                    //Sm.SetLue(LuePICCode, Sm.DrStr(dr, c[11]));
                    TxtDroppingRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                    mDroppingRequestBCCode = Sm.DrStr(dr, c[13]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[14]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[15]);
                    Sl.SetLueUserCode(ref LuePICCode, Sm.DrStr(dr, c[11]));
                    ChkTenderInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[16]), "Y") ? true : false;
                    if (mIsMRUseProcurementType) Sm.SetLue(LueProcurementType, Sm.DrStr(dr, c[17]));

                    TxtFile4.EditValue = Sm.DrStr(dr, c[18]);
                    TxtFile5.EditValue = Sm.DrStr(dr, c[19]);
                    TxtFile6.EditValue = Sm.DrStr(dr, c[20]);

                    TxtReference.EditValue = Sm.DrStr(dr, c[21]);
                    ChkRMInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[22]), "Y") ? true : false;
                    TxtBudgetForMR.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 0);
                    if (mIsMRSaveAvailableBudget) TxtRemainingBudget.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                    var testMRType = Sm.DrStr(dr, c[25]);
                    if (mIsMRUseApprovalSheet)
                    {
                        SetLueMRType(ref LueMRType, Sm.DrStr(dr, c[25]));
                        Sm.SetLue(LueMRType, Sm.DrStr(dr, c[25]));
                        TxtApprovalSheetDocNo.EditValue = Sm.DrStr(dr, c[26]);
                    }

                    
                }, true
            );
        }

        private void ShowDroppingRequestInfo()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled || TxtDroppingRequestDocNo.Text.Length == 0) return;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.DeptName, B.Yr, B.Mth, B.PRJIDocNo, D.BCName, B.Remark, ");
            SQL.AppendLine("Case When A.DroppingRequestBCCode Is Not Null Then ");
            SQL.AppendLine("(Select Sum(Amt) From TblDroppingRequestDtl2 Where DocNo=A.DroppingRequestDocNo And BCCode=A.DroppingRequestBCCode) ");
            SQL.AppendLine("Else B.Amt End As Amt ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Left Join TblDroppingRequestHdr B On A.DroppingRequestDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblBudgetCategory D On A.DroppingRequestBCCode=D.BCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "DeptName", 
                        "Yr", "Mth", "PRJIDocNo", "BCName", "Remark",
                        "Amt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDR_DeptCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDR_Yr.EditValue = Sm.DrStr(dr, c[1]);
                        TxtDR_Mth.EditValue = Sm.DrStr(dr, c[2]);
                        TxtDR_PRJIDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtDR_BCCode.EditValue = Sm.DrStr(dr, c[4]);
                        MeeDR_Remark.EditValue = Sm.DrStr(dr, c[5]);
                        TxtDR_DroppingRequestAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    }, true
                );

            if (mIsDroppingRequestUseMRReceivingPartial)
            {
                string tempDt = Sm.GetValue("Select CreateDt From Tblmaterialrequesthdr Where DocNo = @Param", TxtDocNo.Text);

                var pSQL = new StringBuilder();

                pSQL.AppendLine("SELECT SUM(B.UPrice*B.Qty) TotalMRAmt  ");
                pSQL.AppendLine("FROM tblmaterialrequesthdr A  ");
                pSQL.AppendLine("INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo  ");
                pSQL.AppendLine("INNER JOIN tbldroppingrequesthdr C ON A.DroppingRequestDocNo = C.DocNo  ");
                pSQL.AppendLine("INNER JOIN tbldroppingrequestdtl D ON C.DocNo = D.DocNo  ");
                pSQL.AppendLine("INNER Join TblProjectImplementationRBPHdr E On D.PRBPDocNo = E.DocNo  ");
                pSQL.AppendLine("INNER Join TblProjectImplementationRBPDtl F On E.DocNo = F.DocNo And D.PRBPDNo = F.DNo   ");
                pSQL.AppendLine("LEFT JOIN tblitem G ON E.ResourceItCode = G.ItCode  ");
                pSQL.AppendLine("WHERE A.Status = 'A' AND A.CancelInd = 'N'  ");
                pSQL.AppendLine("AND B.CancelInd = 'N' AND B.Status = 'A'  ");
                pSQL.AppendLine("AND B.ItCode = E.ResourceItCode   ");
                pSQL.AppendLine("and A.DroppingRequestDocNo = @Param1  ");
                pSQL.AppendLine("AND B.CreateDt < @Param2  ");
                pSQL.AppendLine("GROUP BY C.DocNo  ");
                pSQL.AppendLine("UNION ALL  ");
                pSQL.AppendLine("SELECT SUM(B.UPrice*B.Qty) TotalMRAmt  ");
                pSQL.AppendLine("FROM tblmaterialrequesthdr A  ");
                pSQL.AppendLine("INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo  ");
                pSQL.AppendLine("INNER JOIN tbldroppingrequesthdr C ON A.DroppingRequestDocNo = C.DocNo  ");
                pSQL.AppendLine("INNER JOIN tbldroppingrequestdtl2 D ON C.DocNo = D.DocNo AND A.DroppingRequestBCCode = D.BCCode  ");
                pSQL.AppendLine("LEFT JOIN tblitem E ON B.ItCode = E.ItCode  ");
                pSQL.AppendLine("WHERE A.Status = 'A' AND A.CancelInd = 'N'  ");
                pSQL.AppendLine("AND B.CancelInd = 'N' AND B.Status = 'A'  ");
                pSQL.AppendLine("AND B.ItCode = D.ItCode    ");
                pSQL.AppendLine("and A.DroppingRequestDocNo = @Param1  ");
                pSQL.AppendLine("AND B.CreateDt < @Param2  ");
                pSQL.AppendLine("GROUP BY C.DocNo  ");

                decimal TotalMRAmt = Sm.GetValueDec(pSQL.ToString(), TxtDroppingRequestDocNo.Text, tempDt);
                decimal DroppingReqAmt = Sm.GetDecValue(TxtDR_DroppingRequestAmt.Text) - TotalMRAmt;
                TxtDR_DroppingRequestAmt.EditValue = Sm.FormatNum(DroppingReqAmt, 0);
            }
        }

        //ReferenceInd digunakan di MaterialRequestDlg8, yg mana ketika choose data, hanya yg approvalnya cancel
        internal void ShowMaterialRequestDtl(string DocNo, bool ReferenceInd)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, ");
            SQL.AppendLine(" A.CancelInd, A.CancelReason, ");
            SQL.AppendLine("Case IfNull(A.Status, '') When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' Else '' End As StatusDesc, ");
            SQL.AppendLine("(   Select T2.UserName From TblDocApproval T1, TblUser T2 ");
            if (mIsMRSPPJB)
                SQL.AppendLine("    Where T1.DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("    Where T1.DocType='MaterialRequest' ");
            SQL.AppendLine("    And T1.DocNo=@DocNo And T1.DNo=A.DNo And T1.UserCode=T2.UserCode And T1.UserCode Is Not Null ");
            SQL.AppendLine("    Order By ApprovalDNo Desc Limit 1");
            SQL.AppendLine(") As UserName, A.DORequestDocNo, A.DORequestDNo,  ");
            SQL.AppendLine("A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName,  B.ItScCode, D.ItScName, B.MinStock, B.ReorderStock, A.Qty, B.PurchaseUomCode, B.Specification, ");
            SQL.AppendLine("A.UsageDt, A.QtDocNo, A.QtDNo, C.DocDt, A.UPrice, (A.Qty*A.UPrice) As Total, TotalPrice, A.Remark, A.CurCode, A.EstPrice, A.Duration, E.OptDesc DurationUom, ");

            if (mIsUseECatalog) SQL.AppendLine("A.PtCode, F.PtName, A.CityCode, H.ProvName, G.CityName, ");
            else SQL.AppendLine("Null AS PtCode, Null As PtName, Null As CityCode, Null As ProvName, Null As CityName, ");

            if (mIsMaterialRequestForDroppingRequestEnabled)
            {
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                
                if (TxtDR_PRJIDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("(Select IfNull(T3.Amt, 0.00) As Amt ");
                    SQL.AppendLine("From TblDroppingRequestDtl T1, TblProjectImplementationRBPHdr T2, TblProjectImplementationDtl2 T3 ");
                    SQL.AppendLine("Where T1.PRBPDocNo=T2.DocNo And T2.ResourceItCode=A.ItCode And T2.PRJIDocNo=T3.DocNo And T2.ResourceItCode=T3.ResourceItCode ");
                    SQL.AppendLine("And T1.DocNo=@DroppingRequestDocNo) As DroppingRequestAmt ");
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);

                    SQL.AppendLine("(Select Amt ");
                    SQL.AppendLine("From TblDroppingRequestDtl2 ");
                    SQL.AppendLine("Where DocNo=@DroppingRequestDocNo ");
                    SQL.AppendLine("And BCCode=@DroppingRequestBCCode ");
                    SQL.AppendLine("And ItCode=A.ItCode ");
                    SQL.AppendLine(") As DroppingRequestAmt ");
                }
            }
            else
            {
                SQL.AppendLine("0.00 As DroppingRequestAmt ");
            }

            SQL.AppendLine("From TblMaterialRequestDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblItemSubCategory D On B.ItScCode = D.ItScCode ");
            SQL.AppendLine("Left Join TblOption E On E.OptCode = A.DurationUom And E.OptCat = 'DurationUom' ");
            if (mIsUseECatalog)
            {
                SQL.AppendLine("Left Join TblPaymentTerm F On A.PtCode = F.PtCode ");
                SQL.AppendLine("Left Join TblCity G On A.CityCode = G.CityCode ");
                SQL.AppendLine("Left Join TblProvince H On G.ProvCode = H.ProvCode ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            if (ReferenceInd)
                SQL.AppendLine("And A.Status = 'C' ");
            SQL.AppendLine("Order By A.ItCode");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "CancelInd", "CancelReason", "StatusDesc", "UserName", "ItCode",  
                    
                    //6-10
                    "ItCodeInternal", "ItName", "ForeignName", "ItScCode", "ItScName",  
                    
                    //11-15
                    "MinStock", "ReorderStock", "Qty", "PurchaseUomCode", "UsageDt", 
                    
                    //16-20
                    "QtDocNo", "QtDNo", "DocDt", "UPrice", "Total", 
                    
                    //21-25
                    "Remark", "DORequestDocNo", "DORequestDNo", "CurCode", "EstPrice",
                    
                    //26-30
                    "DroppingRequestAmt", "Specification", "Duration", "DurationUom", "TotalPrice",

                    //31-35
                    "PtCode", "PtName", "CityCode", "ProvName", "CityName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 24, 20);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 29, 25);
                    Grd.Cells[Row, 31].Value = 0m;
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 32, 26);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 33, 27);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 35, 28);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 37, 29);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 38, 30);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 39, 31);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 40, 32);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 42, 33);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 43, 34);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 44, 35);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 29, 31, 32, 35 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMaterialRequestDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ItCode, A.JobCode, B.JobName, C.JobCtName, B.UomCode, ");
            SQL.AppendLine("A.PrevCurCode, A.PrevUPrice, A.CurCode, A.UPrice, A.Qty, A.Remark ");
            SQL.AppendLine("From TblMaterialRequestDtl2 A ");
            SQL.AppendLine("Inner Join TblJob B On A.JobCode=B.JobCode ");
            SQL.AppendLine("Inner Join TblJobCategory C On B.JobCtCode=C.JobCtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By C.JobCtName, B.JobName;");

            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            cm.CommandText = SQL.ToString();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                int No = 0;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "ItCode", 
                    //1-5
                    "JobCode", "JobName", "JobCtName", "UomCode", "PrevCurCode", 
                    //6-10
                    "PrevUPrice", "CurCode", "UPrice", "Qty", "Remark"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        No = No + 1;
                        mlJob.Add(new Job()
                        {
                            No=  No,
                            ItCode = Sm.DrStr(dr, c[0]),
                            JobCode = Sm.DrStr(dr, c[1]),
                            JobName = Sm.DrStr(dr, c[2]),
                            JobCtName = Sm.DrStr(dr, c[3]),
                            UomCode = Sm.DrStr(dr, c[4]),
                            PrevCurCode = Sm.DrStr(dr, c[5]),
                            PrevUPrice = Sm.DrDec(dr, c[6]),
                            CurCode = Sm.DrStr(dr, c[7]),
                            UPrice = Sm.DrDec(dr, c[8]),
                            Qty = Sm.DrDec(dr, c[9]),
                            Remark = Sm.DrStr(dr, c[10]),
                            Total = Sm.DrDec(dr, c[9]) * Sm.DrDec(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        internal void ShowMaterialRequestReview(string ReferenceDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(" SELECT B.ItCode, B.ItName, B.Specification, D.EmpName, D.Remark ");
            SQL.AppendLine(" FROM tblmaterialrequestdtl A ");
            SQL.AppendLine(" INNER JOIN tblitem B ON A.ItCode = B.ItCode ");
            SQL.AppendLine(" LEFT JOIN ( SELECT  ");
            SQL.AppendLine(" 		MAX(T2.`Level`) LV, T1.DocNo, T1.DNo  ");
            SQL.AppendLine(" 		FROM tbldocapproval T1 ");
            SQL.AppendLine(" 		INNER JOIN tbldocapprovalsetting T2 ON T1.DocType = T2.DocType  AND T1.ApprovalDNo = T2.DNo ");
            SQL.AppendLine(" 		WHERE T1.DocType = 'MaterialRequest' AND T1.DocNo = @ReferenceDocNo AND (T1.UserCode IS NOT NULL AND T1.UserCode <> '') ");
            SQL.AppendLine(" 		GROUP BY T1.DocNo, T1.DNo ");
            SQL.AppendLine(" 				) C ON A.DocNo = C.DocNo AND A.DNo = C.Dno ");
            SQL.AppendLine(" LEFT JOIN ( SELECT  ");
            SQL.AppendLine(" 		T2.`Level` LV, T1.DocNo, T1.DNo, T1.Remark, T3.EmpName ");
            SQL.AppendLine(" 		FROM tbldocapproval T1 ");
            SQL.AppendLine(" 		INNER JOIN tbldocapprovalsetting T2 ON T1.DocType = T2.DocType  AND T1.ApprovalDNo = T2.DNo ");
            SQL.AppendLine(" 		INNER JOIN tblemployee T3 ON T1.UserCode = T3.UserCode ");
            SQL.AppendLine(" 		WHERE T1.DocType = 'MaterialRequest' AND T1.DocNo = @ReferenceDocNo AND (T1.UserCode IS NOT NULL AND T1.UserCode <> '') ");
            SQL.AppendLine(" 				) D ON C.DocNo = D.DocNo AND C.DNo = D.Dno AND C.LV = D.LV ");
            SQL.AppendLine(" WHERE A.DocNo = @ReferenceDocNo ");
            SQL.AppendLine(" AND A.Status = 'C' ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ReferenceDocNo", ReferenceDocNo);

            Sm.ShowDataInGrid(
                ref GrdReview, ref cm, SQL.ToString(),
                new string[] { 
                                // 0
                                "ItCode",
                                
                                //1-4
                                "Itname","Specification", "EmpName", "Remark" 
                              },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    GrdReview.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", GrdReview, dr, c, Row, 5, 4);

                }, false, false, true, false
            );
            //Sm.FocusGrd(GrdReview, 0, 2);
        }

        private void ShowRFQList(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SeqNo, DocDt, CreateBy ");
            SQL.AppendLine("From TblRFQHdr ");
            SQL.AppendLine("Where MaterialRequestDocNo = @DocNo ");
            SQL.AppendLine("Order by SeqNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "SeqNo",

                    //1-2
                    "DocDt", "CreateBy"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd2.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
        }

        #endregion

        #region Additional Method

        internal void SetLocalDocNoFromReference(string DocNo)
        {
            TxtLocalDocNo.EditValue = Sm.GetValue("Select " +
                            " IF(ISNULL(A.ReferenceDocNo), A.DocNo, A.LocalDocNo) AS Ref " +
                            " From TblMaterialRequestHdr A WHERE A.DocNo = @Param;", DocNo);
        }

        internal void SetSeqNo()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Grd1.Cells[r, 30].Value = r + 1;
        }

        public static string GetNumber(string Dno)
        {
            string number = string.Empty;
            for (int ind = 0; ind < Dno.Length; ind++)
            {
                if (Char.IsNumber(Dno[ind]) == true)
                {
                    number = number + Dno[ind];
                }

            }
            return number;
        }

        private string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                ShortCode = string.Empty,
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            if (mIsDocNoWithDeptShortCode)
                ShortCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode='" + Sm.GetLue(LueDeptCode) + "';");
            if (DocTitle.Length == 0) DocTitle = "XXX";
            
            var SQL = new StringBuilder();

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', ");
                if (mIsDocNoWithDeptShortCode && ShortCode.Length > 0)
                    SQL.Append("'" + ShortCode + "', '/', ");
                SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', ");
                    if (mIsDocNoWithDeptShortCode && ShortCode.Length > 0)
                        SQL.Append("'" + ShortCode + "', '/', ");
                    SQL.Append("'" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateDocNo2(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty;
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);
            string DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType);
            string ShortCode = Sm.GetValue("Select B.ShortCode From TblDepartment A Inner Join TblDivision B ON A.DivisionCode = B.DivisionCode WHERE A.DeptCode = @Param; ", Sm.GetLue(LueDeptCode));

            SQL.AppendLine("Select Concat(  ");
            SQL.AppendLine("IfNull((  ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From (  ");
            SQL.AppendLine("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.AppendLine("       Where Right(DocNo, LENGTH(CONCAT(@DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr)))=Concat(@DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr)  ");
            SQL.AppendLine("       Order By Left(DocNo, 4) Desc Limit 1  ");
            SQL.AppendLine("       ) As Temp  ");
            SQL.AppendLine("   ), '0001')  ");
            SQL.AppendLine(", '/', @DocAbbr, '/', IfNull(@ShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.AppendLine(") As DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@ShortCode", ShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        private string GenerateLocalDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(0, 4),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DeptCode = Sm.GetValue("Select ShortCode From TblDepartment Where DeptCode = '"+Sm.GetLue(LueDeptCode)+"' ");
            if (DocTitle.Length == 0) DocTitle = "XXX";


            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000000', Convert(LocalDocNo+1, Char)), 6) From ( ");
            SQL.Append("       Select Convert(ifnull(Max(LocalDocNo), 0), Decimal) As LocalDocNo From " + Tbl);
            SQL.Append("       Order By Convert(ifnull(Max(LocalDocNo), 0), Decimal) Desc Limit 1");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '000001'), ");
            SQL.Append(" '/', '" + DocAbbr + "', '/', '"+DeptCode+"', '/', '"+SubCategory+"', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As LocalDocNo");

            return Sm.GetValue(SQL.ToString());
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 8) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 17).Length!=0) Qty = Sm.GetGrdDec(Grd1, Row, 17);
            if (Sm.GetGrdStr(Grd1, Row, 23).Length!=0) UPrice = Sm.GetGrdDec(Grd1, Row, 23);

            Grd1.Cells[Row, 24].Value = Qty*UPrice;

            ComputeRemainingBudget();
            ComputeBudgetForMR();
            ComputeDR_Balance();
        }

        private decimal ComputeAvailableBudget()
        {
            string AvailableBudget = "0";

            if (Sm.GetLue(LueDeptCode).Length != 0 && Sm.GetDte(DteDocDt).Length!=0 && Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
            {
                var SQL = new StringBuilder();

                if (mIsMRBudgetBasedOnBudgetCategory)
                {
                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select  ");
                    if(!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        Amt2 ");
                    else
                        SQL.AppendLine("        SUM(Amt2) ");
                    SQL.AppendLine("        From TblBudgetSummary A");
                    SQL.AppendLine("        Where A.DeptCode=@DeptCode AND A.BCCode = @BCCode ");
                    SQL.AppendLine("        And A.Yr=Left(@DocDt, 4) ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And A.Mth=Substring(@DocDt, 5, 2) ");
                    SQL.AppendLine("    ), 0.00)- ");
                }
                else
                {

                    SQL.AppendLine("Select ");
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Amt From TblBudget ");
                    SQL.AppendLine("        Where DeptCode=@DeptCode ");
                    SQL.AppendLine("        And Yr=Left(@DocDt, 4) ");
                    SQL.AppendLine("        And Mth=Substring(@DocDt, 5, 2) ");
                    //SQL.AppendLine("        And UserCode Is Not Null ");
                    SQL.AppendLine("    ), 0.00)- ");
                    
                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(Amt) ");
                SQL.AppendLine("        From TblVoucherRequestHdr ");
                SQL.AppendLine("        Where DocNo <> @DocNo ");
                SQL.AppendLine("        And Find_In_Set(DocType, ");
                SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), '')) ");
                SQL.AppendLine("        And CancelInd = 'N' ");
                SQL.AppendLine("        And Status In ('O', 'A') ");
                SQL.AppendLine("        And ReqType Is Not Null ");
                SQL.AppendLine("        And ReqType <> @ReqTypeForNonBudget ");
                SQL.AppendLine("        And DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) - ");

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                SQL.AppendLine("        From TblTravelRequestHdr A ");
                SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(B.BCCode, '') = IfNull(@BCCode, '') ");
                SQL.AppendLine("    ), 0.00) - ");

                if (mMRAvailableBudgetSubtraction == "1")
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                    SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory)
                    {
                        SQL.AppendLine("        And A.BCCode=@BCCode ");
                    }
                    SQL.AppendLine("        And A.ReqType='1' ");
                    SQL.AppendLine("        And B.CancelInd='N' ");
                    SQL.AppendLine("        And B.Status<>'C' ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    SQL.AppendLine("    ),0.00)+");
                }
                
                if (mMRAvailableBudgetSubtraction == "2")
                {
                    SQL.AppendLine("    IfNull(( ");

                    #region Old Code

                    //SQL.AppendLine("        Select Sum(E.Qty*D.UPrice)  ");
                    //SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    //SQL.AppendLine("        Inner Join TblMaterialRequestDtl B on A.Docno = B.DocNo ");
                    //SQL.AppendLine("        Inner Join TblPORequestDtl C  On B.DocNo=C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo ");
                    //SQL.AppendLine("        Inner Join TblQTDtl D On D.DocNo = C.QtDocNo And D.DNo = C.QtDNo ");
                    //SQL.AppendLine("        Inner Join TblPODtl E on E.PORequestDocNo = C.DocNo And E.PORequestDNo = C.DNo And E.CancelInd = 'N' ");

                    //if (mBudgetBasedOn == "1")
                    //    SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    //if (mBudgetBasedOn == "2")
                    //    SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    //SQL.AppendLine("        And A.ReqType='1' ");
                    //SQL.AppendLine("        And B.CancelInd='N' ");
                    //SQL.AppendLine("        And B.Status<>'C' ");
                    //SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    //SQL.AppendLine("       And A.DocNo<>@DocNo ");

                    #endregion

                    SQL.AppendLine("    Select Sum( ");
                    SQL.AppendLine("    X2.Amt -  ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    SQL.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
                    SQL.AppendLine("          IfNull(( ");
                    SQL.AppendLine("              Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("              Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End ");
                    SQL.AppendLine("      , 0.00) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, Sum(  ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("            And A.DocNo <> @DocNo ");
                    SQL.AppendLine("            And B.CancelInd='N'  ");
                    SQL.AppendLine("            And B.Status In ('A', 'O')  ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1'   ");

                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory)
                        SQL.AppendLine("        And A.BCCode=@BCCode ");
                    
                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");

                    SQL.AppendLine("    ),0.00)+");
                }

                SQL.AppendLine("    IfNull(( ");
                SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
                SQL.AppendLine("        From TblPOQtyCancel A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
                SQL.AppendLine("            On D.DocNo=E.DocNo  ");
                if (mBudgetBasedOn == "1")
                    SQL.AppendLine("        And E.DeptCode=@DeptCode ");
                if (mBudgetBasedOn == "2")
                    SQL.AppendLine("        And E.SiteCode=@DeptCode ");
                if (mIsMRBudgetBasedOnBudgetCategory)
                    SQL.AppendLine("        And E.BCCode=@BCCode ");

                SQL.AppendLine("            And E.ReqType='1' ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("            And Left(E.DocDt, 4)=Left(@DocDt, 4) ");
                SQL.AppendLine("            And E.DocNo<>@DocNo ");
                SQL.AppendLine("        Where A.CancelInd='N' ");
                SQL.AppendLine("    ),0.00) + ");

                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Sum(T.Amt) Amt From ( ");
		        SQL.AppendLine("        Select Case when A.AcType = 'D' Then IfNull(A.Amt, 0.00) Else IfNull(A.Amt*-1, 0.00) END As Amt ");
                SQL.AppendLine("        From TblVoucherHdr A  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X1.DocNo, X1.VoucherRequestDocNo   ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblVoucherRequestHdr X2 ON X2.DocNo = X1.VoucherRequestDocNo  ");
                SQL.AppendLine("                And X1.DocType = '58' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("                AND X2.Status In ('O', 'A')  ");
                SQL.AppendLine("        ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblCashAdvanceSettlementDtl X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("            INNER JOIN TblVoucherRequestHdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("                AND X1.DocType = '56' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N' ");
                SQL.AppendLine("                AND X3.Status In ('O', 'A') ");
                SQL.AppendLine("        ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("        Where A.DocNo <> @DocNo ");
                SQL.AppendLine("        And D.DeptCode = @DeptCode ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                else
                    SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                SQL.AppendLine("        And IfNull(D.BCCode, '') = IfNull(@BCCode, '')  ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine("), 0.00 ) ");

                if (mIsCASUsedForBudget)
                {
                    SQL.AppendLine(" - IfNull(( ");
                    SQL.AppendLine("    Select Sum(T.Amt) Amt From ( ");
                    SQL.AppendLine("        Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) As Mth, C.BCCode, C.DeptCode, B.Amt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                    SQL.AppendLine("            And A.DocStatus = 'F' ");
                    SQL.AppendLine("            And B.CCtCode Is Not Null ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                    SQL.AppendLine("            And C.CCtCode Is Not Null ");
                    SQL.AppendLine("            And C.BCCode = @BCCode ");
                    SQL.AppendLine("            And C.DeptCOde = @DeptCode ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("), 0.00) ");
                }

                SQL.AppendLine("As RemainingBudget");

                var cm = new MySqlCommand()
                { CommandText =SQL.ToString() };
                if (mBudgetBasedOn == "1")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                if (mBudgetBasedOn == "2")
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueSiteCode));

                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length!=0)?TxtDocNo.Text:"XXX");
                Sm.CmParam<String>(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));
                
                AvailableBudget = Sm.GetValue(cm);
            }
            
            return decimal.Parse(AvailableBudget);
        }

        public void ComputeRemainingBudget()
        {
            if (Sm.GetLue(LueBCCode).Length == 0) return;

            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                if (Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
                {
                    AvailableBudget = mFiscalYearRange.Length == 0 ?
                        Sm.ComputeAvailableBudget(
                        (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        )
                        :
                        Sm.ComputeAvailableBudget2(
                        (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        )
                        ;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 24).Length != 0)
                        {
                            if (mIsBudgetCalculateFromEstimatedPrice && mIsMRShowEstimatedPrice)
                                RequestedBudget += (Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 29));
                            else
                                RequestedBudget += Sm.GetGrdDec(Grd1, Row, 24);
                        }
                    }

                    if (mFiscalYearRange.Length > 0)
                    {
                        string[] splitMth = mFiscalYearRange.Split(',');
                        string Yr = Sm.Left(Sm.GetDte(DteDocDt), 4);
                        string YrMth = Sm.Left(Sm.GetDte(DteDocDt), 6);
                        string YrMthFiscal1 = string.Concat(Yr, splitMth[0]);
                        if (Int32.Parse(YrMth) < Int32.Parse(YrMthFiscal1)) Yr = (Int32.Parse(Yr) - 1).ToString();
                        decimal UsedBudget = 0m; // used budget sudah ada di perhitungan available budget ternyata //  GetUsedBudget(Sm.GetLue(LueDeptCode), Yr, Sm.Right(YrMth, 2), Sm.GetLue(LueBCCode));
                        decimal TransferredBudget = GetTransferredBudget(Sm.GetLue(LueDeptCode), Yr, Sm.GetLue(LueBCCode));
                        AvailableBudget = AvailableBudget - UsedBudget + TransferredBudget;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            if (mSourceAvailableBudgetForMR == "2")
            {
                TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget, 0);
            }
            else
            {
                TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget - RequestedBudget, 0);
            }
        }

        public decimal GetTransferredBudget(string DeptCode, string Yr, string BCCode)
        {
            decimal result = 0m;
            var l = new List<TransferredBudget>();
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = Sm.QueryBudgetTransfer();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Yr", "Mth", "DeptCode", "BCCode", "Amt" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TransferredBudget() 
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            DeptCode = Sm.DrStr(dr, c[2]),
                            BCCode = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach (var x in l.Where(w => w.Yr == Yr && w.DeptCode == DeptCode && w.BCCode == BCCode))
                {
                    result = x.Amt;
                }
            }

            l.Clear();
            return result;
        }

        private decimal GetUsedBudget(string DeptCode, string Yr, string Mth, string BCCode)
        {
            //Decimal.Parse(Sm.GetValue(Sm.ComputeUsedBudget2(1, Sm.GetLue(LueDeptCode), Yr, Sm.Right(YrMth, 2), Sm.GetLue(LueBCCode))));

            string query = string.Concat("Select ", Sm.ComputeUsedBudget2(1, DeptCode, Yr, Mth, BCCode), " As Amt3");
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text.Length == 0 ? "XXX" : TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            decimal result = 0m;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = query;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] {  "Amt3" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        result = Sm.DrDec(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return result;
        }

        public void ComputeBudgetForMR()
        {
            if (!mIsMRUseBudgetForMR) return;
            if (Sm.GetLue(LueDeptCode).Length == 0 || Sm.GetLue(LueBCCode).Length == 0 || Sm.GetDte(DteDocDt).Length == 0 || Sm.GetLue(LueReqType) != "1") return;

            decimal AvailableBudget = 0m, RequestedBudget1 = 0m, RequestedBudget2 = 0m;
            string data = string.Empty;
            var SQL = new StringBuilder();

            try
            {
                if (Sm.CompareStr(Sm.GetLue(LueReqType), "1"))
                {
                    AvailableBudget = mFiscalYearRange.Length == 0 ?
                        Sm.ComputeAvailableBudget(
                        (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        )
                        :
                        Sm.ComputeAvailableBudget2(
                        (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"), Sm.GetDte(DteDocDt),
                        Sm.GetLue(LueSiteCode), Sm.GetLue(LueDeptCode), Sm.GetLue(LueBCCode)
                        )
                        ;

                    //Compute MR yg belum dijournalkan
                    #region old code // commented by wed karena used budget ke double
                    //SQL.AppendLine("SELECT SUM(Amt) Amt ");
                    //SQL.AppendLine("FROM ");
                    //SQL.AppendLine("( ");
                    //SQL.AppendLine("    SELECT ");
                    //SQL.AppendLine("    Case ");
                    //SQL.AppendLine("        When T1.TotalBeforeTax = T1.TotalRevBeforeTax Then T1.TotalBeforeTax + T1.TaxAmt + T1.CustomsTaxAmt - T1.DiscountAmt ");
                    //SQL.AppendLine("        ELSE T1.TotalRevBeforeTax + (T1.TotalRevBeforeTax*(T2.TaxRate/100) + (T1.TotalRevBeforeTax*(T3.TaxRate/100) + (T1.TotalRevBeforeTax*(T4.TaxRate/100)))) + T1.CustomsTaxAmt - T1.DiscountAmt ");
                    //SQL.AppendLine("    END Amt ");
                    //SQL.AppendLine("    FROM ( ");
                    //SQL.AppendLine("        SELECT A.DocNo, SUM(A.TotalBeforeTax) TotalBeforeTax, SUM(A.TotalRevBeforeTax) TotalRevBeforeTax, ");
                    //SQL.AppendLine("        A.TaxCode1, A.TaxCode2, A.TaxCode3, A.TaxAmt, A.CustomsTaxAmt, A.DiscountAmt ");
                    //SQL.AppendLine("        FROM ( ");
                    //SQL.AppendLine("            SELECT A.DocNo, B.DNo, ");
                    //SQL.AppendLine("            ((((100-IFNULL(I.DiscountOld, B.Discount))/100)*B.Qty*IfNull(I.UPrice, H.UPrice)) - IfNull(I.DiscountAmtOld, B.DiscountAmt) + IfNull(I.RoundingValueOld, B.RoundingValue)) TotalBeforeTax, ");
                    //SQL.AppendLine("            ((((100-IfNull(J.Discount, B.Discount))/100)*(IfNull(J.Qty, B.Qty) * IfNull(J.UPrice, H.UPrice))) - IfNull(J.DiscountAmt, B.DiscountAmt) + IfNull(J.RoundingValue, B.RoundingValue)) TotalRevBeforeTax, ");
                    //SQL.AppendLine("            A.TaxCode1, A.TaxCode2, A.TaxCode3, A.TaxAmt, A.CustomsTaxAmt, A.DiscountAmt ");
                    //SQL.AppendLine("            FROM TblPOHdr A ");
                    //SQL.AppendLine("            INNER JOIN TblPODtl B ON A.DocNo = B.DocNo AND B.CancelInd = 'N'  ");
                    //SQL.AppendLine("            INNER JOIN TblPORequestDtl D ON B.PORequestDocNo = D.DocNo ");
                    //SQL.AppendLine("                AND B.PORequestDNo = D.DNo ");
                    //SQL.AppendLine("                AND D.CancelInd = 'N' ");
                    //SQL.AppendLine("            INNER JOIN TblMaterialRequestHdr E ON D.MaterialRequestDocNo = E.DocNo ");
                    //SQL.AppendLine("                AND E.CancelInd = 'N' ");
                    //SQL.AppendLine("                AND IFNULL(E.Status, 'O') <> 'C' ");
                    //SQL.AppendLine("                AND E.ReqType <> @ReqTypeForNonBudget ");
                    //SQL.AppendLine("                AND E.BCCode = @BCCode ");
                    //SQL.AppendLine("                AND E.DeptCode = @DeptCode ");
                    //SQL.AppendLine("                AND Left(E.DocDt, 6) = @YrMth ");
                    //SQL.AppendLine("            INNER JOIN TblMaterialRequestDtl F ON D.MaterialRequestDocNo = F.DocNo AND D.MaterialRequestDNo = F.DNo ");
                    //SQL.AppendLine("            Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                    //SQL.AppendLine("            Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                    //SQL.AppendLine("            Left Join ( ");
                    //SQL.AppendLine("                    Select A.DocNo AS PoDocNo, A.DNo AS PoDNo, C.DocNo AS QtDocNo, C.UPrice, B.Discountold, B.DiscountAmtOld, B.RoundingValueOld ");
                    //SQL.AppendLine("                    From TblPoDtl A ");
                    //SQL.AppendLine("                Inner JOIN TblPoRevision B ON A.DocNo =  B.podocno AND A.DNo = B.PoDNo ");
                    //SQL.AppendLine("                Inner Join TblQtDtl C ON B.QtDocNoOld = C.DocNo AND B.QtDNoOld = C.DNo ");
                    //SQL.AppendLine("                Where B.docno = (Select MIN(DocNo) From TblPoRevision Where PoDocNo = B.PoDocNo And PODNo = B.PODNo And Status = 'A' ) ");
                    //SQL.AppendLine("            )I ON A.DocNo = I.PoDocNo And B.DNo = I.PoDNo ");
                    //SQL.AppendLine("            Left Join ( ");
                    //SQL.AppendLine("                SELECT A.DocNo AS PoDoCno, A.DNo AS PoDNo, B.Qty, C.UPrice, B.Discount, B.DiscountAmt, B.RoundingValue ");
                    //SQL.AppendLine("                FROM tblpodtl A ");
                    //SQL.AppendLine("                Inner JOIN TblPoRevision B ON A.DocNo =  B.podocno AND A.DNo = B.PoDNo ");
                    //SQL.AppendLine("                Inner Join TblQtDtl C ON B.QtDocNo = C.DocNo AND B.QtDNo = C.DNo ");
                    //SQL.AppendLine("                Where B.docno = (Select max(DocNo) From TblPoRevision Where PoDocNo = B.PoDocNo And PODNo = B.PODNo And Status = 'A') ");
                    //SQL.AppendLine("            ) J ON A.DocNo = J.PoDocNo AND B.DNo = J.PoDNo ");
                    //SQL.AppendLine("        )A ");
                    //SQL.AppendLine("        GROUP BY A.DocNo ");
                    //SQL.AppendLine("    ) T1 ");
                    //SQL.AppendLine("    LEFT JOIN TblTax T2 ON T1.TaxCode1 = T2.TaxCode ");
                    //SQL.AppendLine("    LEFT JOIN TblTax T3 ON T1.TaxCode2 = T3.TaxCode ");
                    //SQL.AppendLine("    LEFT JOIN TblTax T4 ON T1.TaxCode3 = T4.TaxCode ");
                    //SQL.AppendLine("    UNION ALL ");
                    //if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                    //    SQL.AppendLine("        Select Sum(B.Qty*Case When A.EximInd = 'Y' Then B.UPrice Else B.EstPrice End) ");
                    //else
                    //    SQL.AppendLine("        Select Sum(B.Qty*B.UPrice) ");
                    ////SQL.AppendLine("	SELECT SUM(B.TotalPrice) Amt ");
                    //SQL.AppendLine("	FROM TblMaterialRequestHdr A ");
                    //SQL.AppendLine("	INNER JOIN TblMaterialRequestDtl B ON A.DocNo = B.DocNo ");
                    //SQL.AppendLine("		AND IFNULL(A.Status, 'O') <> 'C' ");
                    //SQL.AppendLine("		AND B.CancelInd = 'N' ");
                    //SQL.AppendLine("		AND A.ReqType <> @ReqTypeForNonBudget ");
                    //SQL.AppendLine("	    AND A.BCCode = @BCCode ");
                    //SQL.AppendLine("        AND A.DeptCode = @DeptCode ");
                    //SQL.AppendLine("        AND LEFT(A.DocDt, 6) = @YrMth ");
                    //SQL.AppendLine("	WHERE NOT EXISTS ( ");
                    //SQL.AppendLine("		SELECT 1 ");
                    //SQL.AppendLine("		FROM TblPODtl X1 ");
                    //SQL.AppendLine("		INNER JOIN TblPORequestDtl X2 ON X1.PORequestDocNo = X2.DocNo ");
                    //SQL.AppendLine("			AND X1.PORequestDNo = X2.DNo ");
                    //SQL.AppendLine("			AND X1.CancelInd = 'N' ");
                    //SQL.AppendLine("		INNER JOIN TblMaterialRequestDtl X3 ON X2.MaterialRequestDocNo = X3.DocNo ");
                    //SQL.AppendLine("			AND X2.MaterialRequestDNo = X3.DNo ");
                    //SQL.AppendLine("			AND X3.CancelInd = 'N' ");
                    //SQL.AppendLine("		WHERE X3.DOcNo = A.DocNo AND X3.DNo = B.DNo ");
                    //SQL.AppendLine("	) ");
                    //SQL.AppendLine(") Tbl ");
                    #endregion

                    SQL.AppendLine("Select ");
                    if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                        SQL.AppendLine("        Sum(B.Qty*Case When A.EximInd = 'Y' Then B.UPrice Else B.EstPrice End) ");
                    else
                        SQL.AppendLine("        Sum(B.Qty*B.UPrice) ");
                    SQL.AppendLine("As MRAmt ");
                    SQL.AppendLine("From TblMaterialRequestHdr A ");
                    SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("    And B.CancelInd = 'N' And B.Status In('O', 'A') ");
                    SQL.AppendLine("    And A.ReqType<> @ReqTypeForNonBudget ");
                    SQL.AppendLine("    And A.BCCode = @BCCode ");
                    SQL.AppendLine("    And A.DeptCode = @DeptCode ");
                    SQL.AppendLine("    And Left(A.DocDt, 6) = @YrMth ");
                    SQL.AppendLine("    And Concat(B.DocNo, B.DNo) Not In ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select Distinct Concat(T2.MaterialRequestDocNo, T2.MaterialRequestDNo) ");
                    SQL.AppendLine("        From TblPODtl T1 ");
                    SQL.AppendLine("        Inner Join TblPORequestDtl T2 On T1.PORequestDocNo = T2.DocNo And T1.PORequestDNo = T2.DNo ");
                    SQL.AppendLine("            And T1.CancelInd = 'N' ");
                    SQL.AppendLine("            And T2.CancelInd = 'N' ");
                    SQL.AppendLine("            And T2.Status In ('O', 'A') ");
                    SQL.AppendLine("    ); ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
                    Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                    Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                    Sm.CmParam<String>(ref cm, "@YrMth", (Sm.GetDte(DteDocDt).Length > 0 ? Sm.Left(Sm.GetDte(DteDocDt), 6) : ""));

                    data = Sm.GetValue(cm);
                    if (data.Length > 0 && TxtDocNo.Text.Length == 0) RequestedBudget1 = Decimal.Parse(data);

                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (TxtDocNo.Text.Length == 0 && Sm.GetGrdStr(Grd1, Row, 24).Length != 0)
                        {
                            if (mIsBudgetCalculateFromEstimatedPrice && mIsMRShowEstimatedPrice)
                                RequestedBudget2 += (Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 29));
                            else
                                RequestedBudget2 += Sm.GetGrdDec(Grd1, Row, 24);
                        }

                        if(TxtDocNo.Text.Length > 0 && Sm.GetGrdBool(Grd1, Row, 1))
                        {
                            if (mIsBudgetCalculateFromEstimatedPrice && mIsMRShowEstimatedPrice)
                                RequestedBudget2 -= (Sm.GetGrdDec(Grd1, Row, 17) * Sm.GetGrdDec(Grd1, Row, 29));
                            else
                                RequestedBudget2 -= Sm.GetGrdDec(Grd1, Row, 24);
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

            TxtBudgetForMR.Text = Sm.FormatNum(AvailableBudget - RequestedBudget1 - RequestedBudget2, 0);
        }

        public void ComputeDR_Balance()
        {
            if (!mIsMaterialRequestForDroppingRequestEnabled) return;
            decimal 
                DR_DroppingRequestAmt = decimal.Parse(TxtDR_DroppingRequestAmt.Text),
                //DR_OtherMRAmt = decimal.Parse(TxtDR_OtherMRAmt.Text),
                DR_MRAmt = 0m,
                Qty = 0m, 
                UPrice = 0m;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Qty = 0m;
                UPrice = 0m;

                if (Sm.GetGrdStr(Grd1, r, 17).Length != 0) Qty = Sm.GetGrdDec(Grd1, r, 17);
                if (Sm.GetGrdStr(Grd1, r, 23).Length != 0) UPrice = Sm.GetGrdDec(Grd1, r, 23);

                DR_MRAmt += (Qty * UPrice);
            }
            TxtDR_MRAmt.Text = Sm.FormatNum(DR_MRAmt, 0);
            //TxtDR_Balance.Text = Sm.FormatNum(DR_DroppingRequestAmt - DR_OtherMRAmt - DR_MRAmt, 0);
            TxtDR_Balance.Text = Sm.FormatNum(DR_DroppingRequestAmt - DR_MRAmt, 0);
        }

        internal void ComputeTotalPrice(int Row)
        {
            decimal Qty = 0m, EstPrice = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 17).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 17);
            if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0) EstPrice = Sm.GetGrdDec(Grd1, Row, 29);

            Grd1.Cells[Row, 38].Value = Qty * EstPrice;
        }

        private void ComputeGrandTotal()
        {
            decimal mTotal = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                mTotal += (Sm.GetGrdDec(Grd1, i, 17) * Sm.GetGrdDec(Grd1, i, 29));
            }
            TxtGrandTotal.EditValue = Sm.FormatNum(mTotal, 0);
        }
    
        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            var l = new List<MatReq>();
            var l1 = new List<MatReq1>();
            var l2 = new List<MatReq2>();
            var ldtl = new List<MatReqDtl>();
            var ldtl2 = new List<MatReqDtl2>();
            var ldtl3 = new List<MatReqDtl3>();
            var ldtl4 = new List<MatReqDtl4>();
            var l3 = new List<MatReqSier>();
            var ldtl5 = new List<MatReqSPPJB>();
            var lsign = new List<SignatureSIER>();
            var l4 = new List<MRIMS>();
            var ldtl6 = new List<MRIMSDtl>();
            var lsign2 = new List<MRIMSSign>();
            var lsign3 = new List<MRIMSSign2>();
            var lsignMnet = new List<SignMnet>();

            string[] TableName = { "MatReq", "MatReqDtl", "MatReqDtl2", "MatReq1", "MatReq2", "MatReqDtl3", "MatReqDtl4", "MatReqSPPJB", "MatReqSier", "SignatureSIER", "MRIMS", "MRIMSDtl", "MRIMSSign", "MRIMSSign2", "SignMnet", "Job"};
            List<IList> myLists = new List<IList>();
            int Nomor = 1;
            decimal mUsageAmt = 0m;
            decimal mUsageAmt2 = 0m;
            var cm = new MySqlCommand();
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region Header
            var SQL = new StringBuilder();

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, F.CompanyName, F.CompanyPhone, F.CompanyFax, F.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddressCity', ");
            }
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, E.SiteName, B.DeptName, C.OptDesc, A.Remark, D.UserName As CreateBy, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='isfilterbysite') As SiteInd, A.LocalDocNo, Concat('(',A.BCCode,')',' ', G.BCName) As BCCode ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblOption C On A.ReqType = C.OptCode ");
            SQL.AppendLine("Inner Join TblUser D On A.CreateBy = D.UserCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblMaterialRequesthdr A  ");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(")F On A.DocNo = F.DocNo ");
            }
            SQL.AppendLine("Left Join TblBudgetCategory G On A.BCCode=G.BCCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo And C.OptCat = 'ReqType' ");
            //  SQL.AppendLine("And A.Status = '' ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select D.EntLogoName " +
                       "From TblMaterialRequesthdr A  " +
                       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                       "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                       "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyAddressCity",
                         "DocNo",
                         //6-10
                         "DocDt",
                         "SiteName",
                         "DeptName",
                         "OptDesc",
                         "Remark",
                         //11-14
                         "CreateBy",
                         "SiteInd",
                         "LocalDocNo",
                         "BCCode"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new MatReq()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyAddressCity = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            SiteName = Sm.DrStr(dr, c[7]),
                            DeptName = Sm.DrStr(dr, c[8]),
                            OptDesc = Sm.DrStr(dr, c[9]),
                            HRemark = Sm.DrStr(dr, c[10]),
                            CreateBy = Sm.DrStr(dr, c[11]),
                            SiteInd = Sm.DrStr(dr, c[12]),
                            LocalDocNo = Sm.DrStr(dr, c[13]),
                            BCCode = Sm.DrStr(dr, c[14]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.Qty, B.PurchaseUomCode, ");
                SQLDtl.AppendLine("DATE_FORMAT(A.UsageDt,'%d-%m-%Y') As UsageDt, ");
                if (mIsUseItemConsumption)
                {
                    SQLDtl.AppendLine("IfNull(C.Qty01, 0.00) As Mth01, ");
                    SQLDtl.AppendLine("IfNull(C.Qty03, 0.00) As Mth03, ");
                    SQLDtl.AppendLine("IfNull(C.Qty06, 0.00) As Mth06, ");
                    SQLDtl.AppendLine("IfNull(C.Qty09, 0.00) As Mth09, ");
                    SQLDtl.AppendLine("IfNull(C.Qty12, 0.00) As Mth12, ");
                }
                else
                {
                    SQLDtl.AppendLine("0.00 As Mth01, ");
                    SQLDtl.AppendLine("0.00 As Mth03, ");
                    SQLDtl.AppendLine("0.00 As Mth06, ");
                    SQLDtl.AppendLine("0.00 As Mth09, ");
                    SQLDtl.AppendLine("0.00 As Mth12, ");
                }
                SQLDtl.AppendLine("A.Remark, ");
                SQLDtl.AppendLine("B.ForeignName, A.EstPrice, A.CurCode, A.Duration, D.OptDesc DurationUOM ");
                SQLDtl.AppendLine("From TblMaterialRequestDtl A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                if (mIsUseItemConsumption)
                {
                    SQLDtl.AppendLine("Left Join ( ");
                    SQLDtl.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                    SQLDtl.AppendLine("        From ( ");
                    SQLDtl.AppendLine("        select Z1.itCode, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                    SQLDtl.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                    SQLDtl.AppendLine("            From ");
                    SQLDtl.AppendLine("            ( ");
                    SQLDtl.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                    SQLDtl.AppendLine("                From ");
                    SQLDtl.AppendLine("                ( ");
                    SQLDtl.AppendLine("                    Select convert('01' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('03' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('06' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('09' using latin1) As Mth Union All ");
                    SQLDtl.AppendLine("                    Select convert('12' using latin1) As Mth  ");
                    SQLDtl.AppendLine("                )T1 ");
                    SQLDtl.AppendLine("                Inner Join ");
                    SQLDtl.AppendLine("                ( ");
                    SQLDtl.AppendLine("                    Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt And last_day(@MthDocDt) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode  ");
                    SQLDtl.AppendLine("                    Union ALL ");
                    SQLDtl.AppendLine("                    Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt2 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt4 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt5 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    Union All ");
                    SQLDtl.AppendLine("                    Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                    From TblDODeptHdr A ");
                    SQLDtl.AppendLine("                    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                    SQLDtl.AppendLine("                    Inner Join TblItem D On B.ItCode=D.ItCode  ");
                    SQLDtl.AppendLine("                        Where A.DORequestDeptDocNo Is Null And A.DocDt Between @MthDocDt6 And last_day(@MthDocDt3) ");
                    SQLDtl.AppendLine("                    Group By  B.ItCode, D.InventoryUomCode ");
                    SQLDtl.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                    SQLDtl.AppendLine("            Group By T1.mth, T2.ItCode ");
                    SQLDtl.AppendLine("        )Z1 ");
                    SQLDtl.AppendLine("   )Z2 Group By Z2.ItCode ");
                    SQLDtl.AppendLine(" ) C On C.ItCode = A.ItCode ");
                }
                SQLDtl.AppendLine("Left Join TblOption D On  D.OptCode = A.DurationUom And D.OptCat = 'DurationUOM' ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine("And A.CancelInd = 'N' ");
                SQLDtl.AppendLine("Order By A.ItCode;");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                Sm.CmParamDt(ref cmDtl, "@MthDocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
                Sm.CmParamDt(ref cmDtl, "@MthDocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "Qty",
                     "PurchaseUomCode",
                     "UsageDt",
                     "Mth01",
                     //6-10
                     "Mth03",
                     "Mth06",
                     "Mth09",
                     "Mth12",
                     "Remark",
                     //11-15
                     "ForeignName",
                     "EstPrice",
                     "Curcode",
                     "Duration",
                     "DurationUOM"
                    });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new MatReqDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            UsageDt = Sm.DrStr(drDtl, cDtl[4]),
                            Mth01 = Sm.DrDec(drDtl, cDtl[5]),
                            Mth03 = Sm.DrDec(drDtl, cDtl[6]),
                            Mth06 = Sm.DrDec(drDtl, cDtl[7]),
                            Mth09 = Sm.DrDec(drDtl, cDtl[8]),
                            Mth12 = Sm.DrDec(drDtl, cDtl[9]),
                            DRemark = Sm.DrStr(drDtl, cDtl[10]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[11]),
                            EstPrice = Sm.DrDec(drDtl, cDtl[12]),
                            Curcode = Sm.DrStr(drDtl, cDtl[13]),
                            Duration = Sm.DrStr(drDtl, cDtl[14]),
                            DurationUom = Sm.DrStr(drDtl, cDtl[15]),
                        });
                        mUsageAmt += Sm.DrDec(drDtl, cDtl[12]);
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd, E.Posname ");
                SQLDtl2.AppendLine("From TblMaterialRequestHdr A ");
                SQLDtl2.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLDtl2.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Left Join tblemployee D On A.CreateBy=D.UserCode ");
                SQLDtl2.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
                SQLDtl2.AppendLine("Where DocNo=@DocNo ");
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0-1
                         "UserCode" ,
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "Posname"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new MatReqDtl2()
                        {
                            UserCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpPict = Sm.DrStr(drDtl2, cDtl2[2]),
                            SignInd = Sm.DrStr(drDtl2, cDtl2[3]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[4]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Approve1
            var cm1 = new MySqlCommand();
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL1.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict, '" + Doctitle + "' As SignInd, E.PosName, ");
            SQL1.AppendLine("DATE_FORMAT(SUBSTRING(A.LastUpDt, 1, 8),'%d %M %Y') As LastUpDt ");
            SQL1.AppendLine("from TblDocApproval A ");
            SQL1.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL1.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL1.AppendLine("Left Join tblemployee D On A.UserCode=D.UserCode ");
            SQL1.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            if (mIsMRSPPJB)
                SQL1.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            else
                SQL1.AppendLine("Where DocType = 'MaterialRequest' ");
            SQL1.AppendLine("And DocNo =@DocNo ");
            SQL1.AppendLine("Group by ApprovalDno limit 1");

            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandText = SQL1.ToString();
                Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-5
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "PosName",
                         //6
                         "LastUpDt"

                        
                        });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        l1.Add(new MatReq1()
                        {
                            ApprovalDno = Sm.DrStr(dr1, c1[0]),
                            UserCode = Sm.DrStr(dr1, c1[1]),
                            UserName = Sm.DrStr(dr1, c1[2]),
                            EmpPict = Sm.DrStr(dr1, c1[3]),
                            SignInd = Sm.DrStr(dr1, c1[4]),
                            PosName = Sm.DrStr(dr1, c1[5]),
                            LastUpDt = Sm.DrStr(dr1, c1[6]),
                        });
                    }
                }

                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Approve2

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
            SQL2.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict,  '" + Doctitle + "' As SignInd, E.PosName, ");
            SQL2.AppendLine("DATE_FORMAT(SUBSTRING(A.LastUpDt, 1, 8),'%d %M %Y') As LastUpDt ");
            SQL2.AppendLine("from TblDocApproval A ");
            SQL2.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
            SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine("Left Join tblemployee D On A.UserCode=D.UserCode ");
            SQL2.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            if (mIsMRSPPJB)
                SQL2.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            else
                SQL2.AppendLine("Where DocType = 'MaterialRequest' ");
            SQL2.AppendLine("And DocNo =@DocNo ");
            SQL2.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "ApprovalDno",
                         //1-5
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd",
                         "PosName",

                         "LastUpDt"

                        
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new MatReq2()
                        {
                            ApprovalDno = Sm.DrStr(dr2, c2[0]),
                            UserCode = Sm.DrStr(dr2, c2[1]),
                            UserName = Sm.DrStr(dr2, c2[2]),
                            EmpPict = Sm.DrStr(dr2, c2[3]),
                            SignInd = Sm.DrStr(dr2, c2[4]),
                            PosName = Sm.DrStr(dr2, c2[5]),
                            LastUpDt = Sm.DrStr(dr2, c2[6]),
                        });
                    }
                }

                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail Signature KIM
            // level 2 tidak ditampilkan

            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select Distinct Concat(T4.ParValue, T1.UserCode, '.JPG') As Signature, ");
                SQLDtl3.AppendLine("T5.UserName, T3.PosName, T1.DNo, ");
                SQLDtl3.AppendLine("Case T1.Title When 'P' Then 'Prepared By' When 'A' Then 'Approved By' End As Title, ");
                SQLDtl3.AppendLine("Date_Format(T1.LastUpDt,'%d %M %Y') As LastUpDt ");
                SQLDtl3.AppendLine("From ( ");
                SQLDtl3.AppendLine("    Select Title, UserCode, Min(DNo) As DNo, Min(LastUpDt) LastUpDt ");
                SQLDtl3.AppendLine("    From ( ");
                SQLDtl3.AppendLine("        Select Distinct 'A' As Title, B.UserCode, B.ApprovalDNo As DNo, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl3.AppendLine("        From TblMaterialRequestDtl A ");
                SQLDtl3.AppendLine("        Inner Join TblDocApproval B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                if (mIsMRSPPJB)
                    SQLDtl3.AppendLine("            And B.DocType='MaterialRequestSPPJB' ");
                else
                    SQLDtl3.AppendLine("            And B.DocType='MaterialRequest' ");
                SQLDtl3.AppendLine("        Where A.DocNo=@DocNo And A.CancelInd='N' And A.Status='A' ");
                SQLDtl3.AppendLine("        Union All ");
                SQLDtl3.AppendLine("        Select 'P' As Title, CreateBy As UserCode, '00' As DNo, Left(CreateDt, 8) As LastUpDt ");
                SQLDtl3.AppendLine("        From TblMaterialRequestDtl Where DocNo=@DocNo And CancelInd='N' And Status='A' ");
                SQLDtl3.AppendLine("    ) T Group By Title, UserCode ");
                SQLDtl3.AppendLine(") T1 ");
                SQLDtl3.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl3.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl3.AppendLine("Inner Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' And T4.ParValue Is Not Null ");
                SQLDtl3.AppendLine("Inner Join TblUser T5 On T1.UserCode=T5.UserCode ");
                SQLDtl3.AppendLine("Order By T1.DNo; ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", DocNo);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                {
                    //0
                    "Signature" ,

                    //1-5
                    "Username", "PosName", "DNo", "Title", "LastupDt"
                });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new MatReqDtl3()
                        {
                            Signature = Sm.DrStr(drDtl3, cDtl3[0]),
                            UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                            PosName = Sm.DrStr(drDtl3, cDtl3[2]),
                            DNo = Sm.DrStr(drDtl3, cDtl3[3]),
                            Title = Sm.DrStr(drDtl3, cDtl3[4]),
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[5]),
                            Space = "-------------------------",
                            IsPrintOutUseDigitalSignature = mIsPrintOutUseDigitalSignature
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);

            #endregion

            #region Detail Signature TWC

            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtl4.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtl4.AppendLine("From ( ");
                SQLDtl4.AppendLine("    Select Distinct ");
                SQLDtl4.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                SQLDtl4.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                SQLDtl4.AppendLine("    From TblMaterialRequestDTL A ");
                SQLDtl4.AppendLine("    Inner Join TblDocApproval B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                if (mIsMRSPPJB)
                    SQLDtl4.AppendLine("        And B.DocType='MaterialRequestSPPJB' ");
                else
                    SQLDtl4.AppendLine("        And B.DocType='MaterialRequest' ");
                SQLDtl4.AppendLine("    Left Join TblUser C On B.UserCode=C.UserCode ");
                SQLDtl4.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = B.DocType ");
                SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine("    Union All ");
                SQLDtl4.AppendLine("    Select Distinct ");
                SQLDtl4.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                SQLDtl4.AppendLine("    '1' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                SQLDtl4.AppendLine("    From TblMaterialRequestDTL A ");
                SQLDtl4.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtl4.AppendLine(") T1 ");
                SQLDtl4.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtl4.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtl4.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtl4.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.level ");
                SQLDtl4.AppendLine("Order By T1.Level; ");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", DocNo);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {

                        ldtl4.Add(new MatReqDtl4()
                        {
                            Signature = Sm.DrStr(drDtl4, cDtl4[0]),
                            UserName = Sm.DrStr(drDtl4, cDtl4[1]),
                            PosName = Sm.DrStr(drDtl4, cDtl4[2]),
                            Space = Sm.DrStr(drDtl4, cDtl4[3]),
                            DNo = Sm.DrStr(drDtl4, cDtl4[4]),
                            Title = Sm.DrStr(drDtl4, cDtl4[5]),
                            LastUpDt = Sm.DrStr(drDtl4, cDtl4[6]),
                            IsPrintOutUseDigitalSignature = mIsPrintOutUseDigitalSignature
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            #region Header SPPJB

            #region Old Code
            /*
            int No = 0;
            string mJobCtName = string.Empty;
            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();

            SQL4.AppendLine(" SELECT A.Qty, A.UPrice, B.JobName, C.JobCtName ");
            SQL4.AppendLine(" FROM TblMaterialRequestDtl2 A ");
            SQL4.AppendLine(" LEFT JOIN TblJob B ON A.JobCode = b.JobCode ");
            SQL4.AppendLine(" INNER JOIN TblJobCategory C ON B.JobCtCode = C.JobCtCode ");
            SQL4.AppendLine(" WHERE DocNo = @DocNo Order By C.JobCtName");

            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();
                Sm.CmParam<String>(ref cm4, "@DocNo", DocNo);
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                        {
                        //0
                         "JobName",

                        // 1-2
                         "Qty",
                         "UPrice",

                        });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        No = No + 1;

                        ldtl5.Add(new MatReqSPPJB()
                        {
                            No = No,
                            ItName = Sm.GetGrdStr(Grd1, 1, 11),
                            JobName = Sm.DrStr(dr4, c4[0]),
                            Qty = Sm.DrDec(dr4, c4[1]),
                            UPrice = Sm.DrDec(dr4, c4[2]),
                            Total = Sm.DrDec(dr4, c4[1]) * Sm.DrDec(dr4, c4[2])

                        });
                        //mJobCtName = Sm.DrStr(dr4, c4[1]);
                        //mUsageAmt2 += Sm.DrDec(dr4, c4[2]);
                    }

                }
                dr4.Close();
            }
            myLists.Add(ldtl5);
            */
            #endregion

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (!Sm.GetGrdBool(Grd1, i, 1)) // tidak di cancel
                {
                    ldtl5.Add(new MatReqSPPJB()
                    {
                        No = Nomor,
                        ItName = Sm.GetGrdStr(Grd1, i, 11),
                        
                    });
                    Nomor += 1;
                }
            }
            myLists.Add(ldtl);

            #endregion

            #region HeaderSier

            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            SQL3.AppendLine("Select DISTINCT B.DocNo, A.Amt2 BudgetAmt, (A.Amt2-SUM(C.Totalprice)) RemainingBudgetAmt, ");
            SQL3.AppendLine("SUM(C.TotalPrice) UsageAmt, ");
            SQL3.AppendLine("IFNULL ");
            SQL3.AppendLine("((SELECT F.Amt2 - SUM(B.TotalPrice) ");
            SQL3.AppendLine("FROM TblMaterialRequestHdr A ");
            SQL3.AppendLine("INNER JOIN TblMaterialRequestDtl B ON A.DocNo = B.DocNo  ");
            SQL3.AppendLine("INNER JOIN TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            SQL3.AppendLine("INNER JOIN TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            SQL3.AppendLine("INNER Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            SQL3.AppendLine("INNER JOIN TblBudgetSummary F ON A.BCCode = F.BCCode ");
            SQL3.AppendLine("Where A.DeptCode=@DeptCode  ");
            SQL3.AppendLine("AND F.BCCode = @BCCode  ");
            SQL3.AppendLine("AND F.Yr=LEFT(@DocDt, 4)  ");
            SQL3.AppendLine("AND B.DocNo=@DocNo  ");
            SQL3.AppendLine("GROUP BY A.DocNo) ");
            SQL3.AppendLine(", A.Amt2 ");
            SQL3.AppendLine(") AfterUsageAmt ");
            SQL3.AppendLine("From TblBudgetSummary A  ");
            SQL3.AppendLine("INNER JOIN TblMaterialRequestHdr B ON A.BCCode=B.BCCode  ");
            SQL3.AppendLine("INNER JOIN TblMaterialRequestDtl C ON B.DocNo=C.DocNo  ");
            SQL3.AppendLine("Where A.DeptCode=@DeptCode "); 
            SQL3.AppendLine(" AND A.BCCode = @BCCode  ");
            SQL3.AppendLine(" AND A.Yr=LEFT(@DocDt, 4)  ");
            if(!mIsBudget2YearlyFormat)
	             SQL3.AppendLine("   And A.Mth=SUBSTRING(@DocDt, 5, 2) ");
            SQL3.AppendLine("   AND B.DocNo=@DocNo ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm3, "@BCCode", Sm.GetLue(LueBCCode));
                Sm.CmParam<String>(ref cm3, "@DocDt", Sm.GetDte(DteDocDt));
                Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);

                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                        //0
                        "BudgetAmt",

                        //1-3
                        "RemainingBudgetAmt",
                        "UsageAmt",
                        "AfterUsageAmt"
                        });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new MatReqSier()
                        {
                            Budget = Sm.DrDec(dr3, c3[0]),
                            AvailableBudget = Sm.DrDec(dr3, c3[1]),
                            UsageAmt = Sm.DrDec(dr3, c3[2]),
                            BudgetCategory = LueBCCode.Text,
                            AfterUsageAmt = Sm.DrDec(dr3, c3[3]),
                        });
                    }
                }
                dr3.Close();
            }
            myLists.Add(l3);
            #endregion

            #region Detail Signature SIER

            var cmDtl6 = new MySqlCommand();

            var SQLDtl6 = new StringBuilder();
            using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl6.Open();
                cmDtl6.Connection = cnDtl6;

                SQLDtl6.AppendLine("SELECT C.EmpName , D.PosName Position,  DATE_FORMAT(left(B.LastUpDt, 8), '%d %M %Y') AS Date ");
                SQLDtl6.AppendLine("FROM tbluser A ");
                SQLDtl6.AppendLine("INNER JOIN tbldocapproval B ON A.UserCode=B.UserCode ");
                SQLDtl6.AppendLine("INNER JOIN tblemployee C ON A.UserCode=C.UserCode ");
                SQLDtl6.AppendLine("INNER JOIN tblposition D ON C.PosCode=D.PosCode ");
                SQLDtl6.AppendLine("WHERE B.Docno =@DocNo ");

                cmDtl6.CommandText = SQLDtl6.ToString();
                Sm.CmParam<String>(ref cmDtl6, "@DocNo", DocNo);
                var drDtl6 = cmDtl6.ExecuteReader();
                var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                        {
                         //0
                         "EmpName" ,

                         //1-5
                         "Position" ,
                         "Date"
                        });
                if (drDtl6.HasRows)
                {
                    while (drDtl6.Read())
                    {

                        lsign.Add(new SignatureSIER()
                        {
                            EmpName = Sm.DrStr(drDtl6, cDtl6[0]),
                            Position = Sm.DrStr(drDtl6, cDtl6[1]),
                            Date = Sm.DrStr(drDtl6, cDtl6[2])
                        });
                    }
                }
                drDtl6.Close();
            }
            myLists.Add(lsign);
            #endregion

            #region Header MRIMS

            var SQLH = new StringBuilder();
            var cmh = new MySqlCommand();

            SQLH.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo, A.DocNo, ");
            SQLH.AppendLine("DATE_FORMAT(A.DocDt, '%d-%m-%Y') DocDt, ");
            SQLH.AppendLine("B.SiteName, C.ProjectCode, ");
            SQLH.AppendLine("C.ProjectName, GROUP_CONCAT(Distinct T9.FinishedGood SEPARATOR ' \n') DocName  ");
            SQLH.AppendLine("FROM TblMaterialRequestHdr A  ");
            SQLH.AppendLine("LEFT JOIN TblSite B ON A.SiteCode = B.SiteCode ");
            SQLH.AppendLine("INNER JOIN tblmaterialrequestdtl T9 ON A.DocNo=T9.DocNo ");
            SQLH.AppendLine("LEFT JOIN TblProjectGroup C ON A.PGCode = C.PGCOde ");
            SQLH.AppendLine("WHERE A.DocNo = @DocNo; ");
            //SQL.AppendLine("And Exists(Select 1 From TblMaterialRequestDtl Where DocNo = @DocNo And ItCode In (Select ItCode From TblItem Where InventoryItemInd ='Y')); ");

            using (var cnh = new MySqlConnection(Gv.ConnectionString))
            {
                cnh.Open();
                cmh.Connection = cnh;
                cmh.CommandText = SQLH.ToString();
                Sm.CmParam<String>(ref cmh, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cmh, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cmh.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DocDt",
                        "SiteName",
                        "ProjectCode",
                        "ProjectName",
                        "DocName",
                        //6
                        "CompanyLogo"
                    });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new MRIMS()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            SiteName = Sm.DrStr(dr, c[2]),
                            ProjectCode = Sm.DrStr(dr,c[3]),
                            ProjectName = Sm.DrStr(dr, c[4]),
                            DocName = Sm.DrStr(dr, c[5]),
                            CompanyLogo = Sm.DrStr(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l4);

            #endregion

            #region Detail MRIMS

            var SQLD = new StringBuilder();
            var cmd = new MySqlCommand();

            SQLD.AppendLine("SELECT A.DocNo, A.DNo, B.ItCodeInternal, B.ItName, B.Specification, A.Qty, B.InventoryUomCode Uom, ");
            SQLD.AppendLine("C.ProjectCode, DATE_FORMAT(A.UsageDt, '%d-%m-%Y') UsageDt, A.Remark ");
            SQLD.AppendLine("FROM TblMaterialRequestDtl A ");
            SQLD.AppendLine("INNER JOIN TblItem B ON A.ItCode = B.ItCode ");
            SQLD.AppendLine("    AND A.DocNo = @DocNo ");
            SQLD.AppendLine("LEFT JOIN ");
            SQLD.AppendLine("( ");
            SQLD.AppendLine("    SELECT T1.DocNo, T1.DNo, GROUP_CONCAT(DISTINCT T5.ProjectCode) ProjectCode ");
            SQLD.AppendLine("    FROM TblMaterialRequestDtl T1 ");
            SQLD.AppendLine("    INNER JOIN TblBOMRevisionHdr T2 ON T1.BOMRDocNo = T2.DocNo ");
            SQLD.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLD.AppendLine("    INNER JOIN TblBOQHdr T3 ON T2.BOQDocNo = T3.DocNo ");
            SQLD.AppendLine("    INNER JOIN TblLOPHdr T4 ON T3.LOPDocNo = T4.DocNo ");
            SQLD.AppendLine("    LEFT JOIN TblProjectGroup T5 ON T4.PGCode = T5.PGCode ");
            SQLD.AppendLine("    GROUP BY T1.DocNo, T1.DNo ");
            SQLD.AppendLine(") C ON A.DocNo = C.DocNo AND A.DNo = C.DNo ");
            SQLD.AppendLine("Where A.CancelInd = 'N' ");
            SQLD.AppendLine("Order by A.ItCode; ");

            using (var cnd = new MySqlConnection(Gv.ConnectionString))
            {
                cnd.Open();
                cmd.Connection = cnd;
                cmd.CommandText = SQLD.ToString();
                Sm.CmParam<String>(ref cmd, "@DocNo", DocNo);

                var drd = cmd.ExecuteReader();
                var cd = Sm.GetOrdinal(drd, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DNo",
                        "ItCodeInternal",
                        "ItName",
                        "Specification",
                        "Qty",
                        //6-9
                        "Uom",
                        "ProjectCode",
                        "UsageDt",
                        "Remark"
                    });

                if (drd.HasRows)
                {
                    int mNo = 1;
                    while (drd.Read())
                    {
                        ldtl6.Add(new MRIMSDtl()
                        {
                            No = mNo,
                            DocNo = Sm.DrStr(drd, cd[0]),
                            DNo = Sm.DrStr(drd, cd[1]),
                            ItCodeInternal = Sm.DrStr(drd, cd[2]),
                            ItName = Sm.DrStr(drd, cd[3]),
                            Specification = Sm.DrStr(drd, cd[4]),
                            Qty = Sm.DrDec(drd, cd[5]),
                            Uom = Sm.DrStr(drd, cd[6]),
                            ProjectCode = Sm.DrStr(drd, cd[7]),
                            UsageDt = Sm.DrStr(drd, cd[8]),
                            Remark = Sm.DrStr(drd, cd[9])
                        });
                        mNo += 1;
                    }
                }
                drd.Close();
            }
            myLists.Add(ldtl6);

            #endregion

            #region Detail Signature MRIMS
            var SQLS = new StringBuilder();
            var cms = new MySqlCommand();

            SQLS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
            SQLS.AppendLine("T1.UserCode, T1.UserName, T3.PosName, ");
            SQLS.AppendLine("T1.DNo, T1.Seq As Level, @Space As Space, ");
            SQLS.AppendLine("T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
            SQLS.AppendLine("From ( ");

            SQLS.AppendLine("    Select UserCode, UserName, DNo, Seq, Title, LastUpDt From ( ");
            SQLS.AppendLine("        Select Distinct B.UserCode, C.UserName, ");
            SQLS.AppendLine("        B.ApprovalDNo As DNo, 900+D.Level As Seq, ");
            SQLS.AppendLine("        'Approved By' As Title, ");
            SQLS.AppendLine("        Left(B.LastUpDt, 8) As LastUpDt ");
            SQLS.AppendLine("        From TblMaterialRequestDtl A ");
            SQLS.AppendLine("        Inner Join TblDocApproval B On A.DocNo=B.DocNo AND A.DocNo = @DocNo ");
            SQLS.AppendLine("        Inner Join TblUser C On B.UserCode=C.UserCode ");
            SQLS.AppendLine("        Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType='MaterialRequest' ");
            SQLS.AppendLine("        Where A.Status='A' ");
            SQLS.AppendLine("        And A.DocNo=@DocNo ");
            SQLS.AppendLine("        And B.UserCode Not In (Select CreateBy From TblMaterialRequestHdr Where DocNo = @DocNo) ");
            SQLS.AppendLine("        Order By D.Level Desc ");
            SQLS.AppendLine("    ) Tbl ");

            SQLS.AppendLine("    Union All ");
            SQLS.AppendLine("    Select Distinct ");
            SQLS.AppendLine("    A.CreateBy As UserCode, B.UserName, ");
            SQLS.AppendLine("    '1' As DNo, 0 As Seq, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
            SQLS.AppendLine("    From TblMaterialRequestHdr A ");
            SQLS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode And A.DocNo=@DocNo ");
            SQLS.AppendLine(") T1 ");
            SQLS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
            SQLS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
            SQLS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
            SQLS.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
            SQLS.AppendLine("Order By T1.Seq; ");

            using (var cns = new MySqlConnection(Gv.ConnectionString))
            {
                cns.Open();
                cms.Connection = cns;
                cms.CommandText = SQLS.ToString();
                Sm.CmParam<String>(ref cms, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cms, "@DocNo", DocNo);

                var drs = cms.ExecuteReader();
                var cs = Sm.GetOrdinal(drs, new string[]
                {
                    //0
                     "Signature",
                     //1-5
                     "Username",
                     "PosName",
                     "Space",
                     "Level",
                     "Title",
                     "LastupDt"
                });

                if (drs.HasRows)
                {
                    while (drs.Read())
                    {
                        lsign2.Add(new MRIMSSign()
                        {
                            Signature = Sm.DrStr(drs, cs[0]),
                            UserName = Sm.DrStr(drs, cs[1]),
                            PosName = Sm.DrStr(drs, cs[2]),
                            Space = Sm.DrStr(drs, cs[3]),
                            DNo = Sm.DrStr(drs, cs[4]),
                            Title = Sm.DrStr(drs, cs[5]),
                            LastUpDt = Sm.DrStr(drs, cs[6])
                        });
                    }
                }
                drs.Close();
            }
            myLists.Add(lsign2);
            #endregion

            #region Detail Signature2 MRIMS

            var SQLS2 = new StringBuilder();
            var cms2 = new MySqlCommand();

            SQLS2.AppendLine("SELECT * ");
            SQLS2.AppendLine("FROM  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT UPPER(B.UserName) CreateUserName, DATE_FORMAT(LEFT(A.CreateDt, 8), '%d-%m-%Y') CreateDocDt ");
            SQLS2.AppendLine("    FROM TblMaterialRequestHdr A ");
            SQLS2.AppendLine("    INNER JOIN TblUser B ON A.CreateBy = B.UserCode ");
            SQLS2.AppendLine("        AND A.DocNo = @DocNo ");
            SQLS2.AppendLine(") A ");
            SQLS2.AppendLine("LEFT JOIN ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As ApproveSignature, UPPER(T3.UserName) ApproveUserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') ApproveDocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("	            SELECT MAX(B.Level) MaxLevel ");
            SQLS2.AppendLine("	            FROM TblDocApproval A ");
            SQLS2.AppendLine("	            INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("	                AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("	                AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine("        LEFT JOIN TblParameter T4 ON T4.ParCode = 'ImgFileSignature' ");
            SQLS2.AppendLine(") B ON 0 = 0 ");
            SQLS2.AppendLine("LEFT JOIN  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Approve2Signature, UPPER(T3.UserName) Approve2UserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') Approve2DocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level != 1 ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("	            SELECT MAX(B.Level) - 1 MaxLevel ");
            SQLS2.AppendLine("	            FROM TblDocApproval A ");
            SQLS2.AppendLine("	            INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("	                AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("	                AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine("        LEFT JOIN TblParameter T4 ON T4.ParCode = 'ImgFileSignature' ");
            SQLS2.AppendLine(") C ON 0 = 0 ");
            SQLS2.AppendLine("LEFT JOIN  ");
            SQLS2.AppendLine("( ");
            SQLS2.AppendLine("    SELECT Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Approve3Signature, UPPER(T3.UserName) Approve3UserName, IFNULL(DATE_FORMAT(LEFT(T1.LastUpDt, 8), '%d-%m-%Y'), '') Approve3DocDt ");
            SQLS2.AppendLine("    FROM TblDocApproval T1 ");
            SQLS2.AppendLine("    INNER JOIN TblDocApprovalSetting T2 ON T1.DocType = T2.DocType ");
            SQLS2.AppendLine("        AND T1.DocNo = @DocNo ");
            SQLS2.AppendLine("        AND T1.ApprovalDNo = T2.DNo ");
            SQLS2.AppendLine("        AND T2.Level IN  ");
            SQLS2.AppendLine("        ( ");
            SQLS2.AppendLine("                SELECT MAX(B.Level) - 2 MaxLevel ");
            SQLS2.AppendLine("                FROM TblDocApproval A ");
            SQLS2.AppendLine("                INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
            SQLS2.AppendLine("                    AND A.ApprovalDNo = B.DNo ");
            SQLS2.AppendLine("                    AND A.DocNo = @DocNo ");
            SQLS2.AppendLine("        ) ");
            SQLS2.AppendLine("    INNER JOIN TblUser T3 ON T1.UserCode = T3.UserCode AND T1.Status = 'A' ");
            SQLS2.AppendLine("        AND T3.UserCode NOT IN (SELECT CreateBy FROM TblMaterialRequestHdr WHERE DocNo = @DocNo) ");
            SQLS2.AppendLine("        LEFT JOIN TblParameter T4 ON T4.ParCode = 'ImgFileSignature' ");
            SQLS2.AppendLine(") D ON 0 = 0 ");

            using (var cns2 = new MySqlConnection(Gv.ConnectionString))
            {
                cns2.Open();
                cms2.Connection = cns2;
                cms2.CommandText = SQLS2.ToString();
                Sm.CmParam<String>(ref cms2, "@DocNo", DocNo);

                var drs2 = cms2.ExecuteReader();
                var cs2 = Sm.GetOrdinal(drs2, new string[]
                {
                    //0
                     "CreateUserName",
                    //1-5
                     "CreateDocDt",
                     "ApproveSignature",
                     "ApproveUserName",
                     "ApproveDocDt",
                     "Approve2Signature",
                    //6-10 
                     "Approve2UserName",
                     "Approve2DocDt",
                     "Approve3Signature",
                     "Approve3UserName",
                     "Approve3DocDt"
                });

                if (drs2.HasRows)
                {
                    while (drs2.Read())
                    {
                        lsign3.Add(new MRIMSSign2()
                        {
                            CreateUserName = Sm.DrStr(drs2, cs2[0]),
                            CreateDocDt = Sm.DrStr(drs2, cs2[1]),
                            ApproveSignature = Sm.DrStr(drs2, cs2[2]),
                            ApproveUserName = Sm.DrStr(drs2, cs2[3]),
                            ApproveDocDt = Sm.DrStr(drs2, cs2[4]),
                            Approve2Signature = Sm.DrStr(drs2, cs2[5]),
                            Approve2UserName = Sm.DrStr(drs2, cs2[6]),
                            Approve2DocDt = Sm.DrStr(drs2, cs2[7]),
                            Approve3Signature = Sm.DrStr(drs2, cs2[8]),
                            Approve3UserName = Sm.DrStr(drs2, cs2[9]),
                            Approve3DocDt = Sm.DrStr(drs2, cs2[10])
                        });
                    }
                }
                drs2.Close();
            }

            myLists.Add(lsign3);

            #endregion

            #region Detail Signature MNET
            if (Doctitle == "MNET")
            {
                var cmSignMnet = new MySqlCommand();

                var SQLS3 = new StringBuilder();
                using (var cnSign3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnSign3.Open();
                    cmSignMnet.Connection = cnSign3;
                    SQLS3.AppendLine("Select B.UserName, C.GrpName PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d %M %Y') as LastUpDt, A.EmpPict  ");
                    SQLS3.AppendLine("From (  ");
                    SQLS3.AppendLine("    Select A.CreateBy As UserCode, 0 As Seq, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    From TblMaterialRequestHdr A  ");
                    SQLS3.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 2  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 3  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 4  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 5  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");

                    SQLS3.AppendLine("    Union All  ");
                    SQLS3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict  ");
                    SQLS3.AppendLine("    FROM TblDocApproval A  ");
                    SQLS3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 6  ");
                    SQLS3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                    SQLS3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null  ");
                    SQLS3.AppendLine("    ) A  ");
                    SQLS3.AppendLine("Left Join Tbluser B On A.UserCode = B.UserCode  ");
                    SQLS3.AppendLine("Left Join TblGroup C On B.GrpCode = C.GrpCode  ");
                    SQLS3.AppendLine("Group By A.UserCode, A.Seq, A.Title, A.LastUpDt  ");
                    SQLS3.AppendLine("Order By A.Seq Desc ");

                    cmSignMnet.CommandText = SQLS3.ToString();
                    Sm.CmParam<String>(ref cmSignMnet, "@DocNo", TxtDocNo.Text);
                    var drSign3 = cmSignMnet.ExecuteReader();


                    var cSign3 = Sm.GetOrdinal(drSign3, new string[]
                            {
                     //0-4
                     "UserName",
                     "EmpPict",
                     "Posname",
                     "LastUpDt",
                     "Seq",
                     "Title",
                            });
                    if (drSign3.HasRows)
                    {
                        while (drSign3.Read())
                        {
                            lsignMnet.Add(new SignMnet()
                            {
                                UserName = Sm.DrStr(drSign3, cSign3[0]),
                                EmpPict = Sm.DrStr(drSign3, cSign3[1]),
                                PosName = Sm.DrStr(drSign3, cSign3[2]),
                                LastUpDt = Sm.DrStr(drSign3, cSign3[3]),
                                Sequence = Sm.DrStr(drSign3, cSign3[4]),
                                Title = Sm.DrStr(drSign3, cSign3[5]),
                                Space = "                       ",
                                LabelName = "Name : ",
                                LabelPos = "Position : ",
                                LabelDt = "Date : ",
                            });
                        }
                    }

                    drSign3.Close();
                }
                myLists.Add(lsignMnet);
            }

            #endregion

            myLists.Add(mlJob);

            #region Old Code by wed
            //if (!mIsUseItemConsumption)
            //{
            //    if (Doctitle == "TWC") 
            //        Sm.PrintReport("MaterialRequestTWC", myLists, TableName, false);
            //    else if (Doctitle == "SIER")
            //    {
            //        if (mIsMRSPPJBEnabled)
            //        {
            //            if (mIsMRSPPJB)
            //            {
            //                Sm.PrintReport("MaterialRequestSPPJB", myLists, TableName, false);
            //                Sm.PrintReport("MaterialRequestSPPJB_2", myLists, TableName, false);
            //            }

            //            else
            //                Sm.PrintReport("MaterialRequestSIER", myLists, TableName, false);
            //        }
            //    }
            //    else if (Doctitle == "SRN")
            //    {
            //        Sm.PrintReport("MaterialRequest2TWC", myLists, TableName, false);
            //    }
            //    else
            //        Sm.PrintReport("MaterialRequest", myLists, TableName, false);
            //}
            //else
            //{
            //    if (Sm.GetParameter("DocTitle") == "KIM")
            //        Sm.PrintReport("MaterialRequestKIM", myLists, TableName, false);
            //    else
            //        Sm.PrintReport("MaterialRequest2", myLists, TableName, false);
            //}
            #endregion

            if (!mIsUseItemConsumption)
            {
                if (mIsMRSPPJBEnabled)
                {
                    if (mIsMRSPPJB)
                    {
                        Sm.PrintReport(mFormPrintOutMaterialRequestSPPJB, myLists, TableName, false);
                        Sm.PrintReport(mFormPrintOutMaterialRequestSPPJB2, myLists, TableName, false);
                    }
                    else
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                }
                else
                {
                    if (Doctitle == "IMS")
                    {
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                    }
                    else
                    {
                        Sm.PrintReport(mFormPrintOutMaterialRequestWithoutConsumption, myLists, TableName, false);
                    }
                }
            }
            else
            {
                Sm.PrintReport(mFormPrintOutMaterialRequestWithConsumption, myLists, TableName, false);
            }
        }

        internal void ShowPOQtyCancelInfo(string DocNo)
        {
            TxtRemainingBudget.EditValue = Sm.FormatNum(0m, 0);
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, D.DocDt, D.ReqType, D.DeptCode, Concat('Replacement for ', D.DocNo) As RemarkH, ");
            SQL.AppendLine("E.ItCode, F.ItName, A.Qty, F.PurchaseUomCode, E.UsageDt, E.Remark As RemarkD, ");
            SQL.AppendLine("F.ItSCCode, H.ItSCName ");
            SQL.AppendLine("From TblPOQtyCancel A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
            SQL.AppendLine("Inner Join TblDepartment G On D.DeptCode=G.DeptCode ");
            SQL.AppendLine("Left Join TblItemSubCategory H On F.ItSCCode=H.ItSCCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "ReqType", "DeptCode", "RemarkH", "ItCode", 
                        //6-10
                        "ItName", "Qty", "PurchaseUomCode", "UsageDt", "RemarkD",
                        //11-12
                        "ItSCCode", "ItSCName"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        TxtPOQtyCancelDocNo.EditValue = Sm.DrStr(dr, 0);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueReqType, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 8, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 11, 6);
                        Sm.SetGrdValue("N", Grd1, dr, c, 0, 17, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 18, 8);
                        Sm.SetGrdValue("D", Grd1, dr, c, 0, 19, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 25, 10);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 13, 11);
                        Sm.SetGrdValue("S", Grd1, dr, c, 0, 14, 12);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(ref Grd1, 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 1, new int[] { 17, 23, 24, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private string ItemSelection()
        {
            var SQL = new StringBuilder();

            if (Sm.CompareStr(Sm.GetLue(LueReqType), "2"))
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, Null As DocNo, Null As DNo, Null As DocDt, 0 As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("ifnull(C.Qty01, 0) As Mth01, ifnull(C.Qty03, 0) As Mth03, ifnull(C.Qty06, 0) As Mth06, ifnull(C.Qty09, 0) As Mth09, ifnull(C.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, D.ItScName, A.ItCodeInternal, A.ItGrpCode, E.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) C On C.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory D On A.ItScCode = D.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup E On A.ItGrpCode=E.ItGrpCode ");
            }
            else
            {
                SQL.AppendLine("Select A.ItCode, B.ItCtName, A.PurchaseUomCode, C.DocNo, C.DNo, C.DocDt, ");
                SQL.AppendLine("A.ItName, A.ForeignName, ");
                SQL.AppendLine("C.UPrice*");
                SQL.AppendLine("    Case When IfNull(C.CurCode, '')=D.ParValue Then 1 ");
                SQL.AppendLine("    Else IfNull(E.Amt, 0) ");
                SQL.AppendLine("    End As UPrice, A.MinStock, A.ReorderStock, ");
                SQL.AppendLine("ifnull(F.Qty01, 0) As Mth01, ifnull(F.Qty03, 0) As Mth03, ifnull(F.Qty06, 0) As Mth06, ifnull(F.Qty09, 0) As Mth09, ifnull(F.Qty12, 0) As Mth12, ");
                SQL.AppendLine("A.ItScCode, G.ItScName, A.ItCodeInternal, A.ItGrpCode, H.ItGrpName ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.DocDt, T2.ItCode, T1.CurCode, T2.UPrice ");
                SQL.AppendLine("    From TblQtHdr T1 ");
                SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select T3b.ItCode, Max(T3a.SystemNo) As Key1 ");
                SQL.AppendLine("        From TblQtHdr T3a, TblQtDtl T3b ");
                SQL.AppendLine("        Where T3a.DocNo=T3b.DocNo And T3b.ActInd='Y' ");
                SQL.AppendLine("        Group By T3b.ItCode ");
                SQL.AppendLine("    ) T3 On T1.SystemNo=T3.Key1 And T2.ItCode=T3.ItCode ");
                SQL.AppendLine(") C On A.ItCode=C.ItCode ");
                SQL.AppendLine("Left Join TblParameter D On D.ParCode='MainCurCode' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.CurCode CurCode1, T2.CurCode As CurCode2, IfNull(T3.Amt, 0) As Amt ");
                SQL.AppendLine("    From TblCurrency T1 ");
                SQL.AppendLine("    Inner Join TblCurrency T2 On T1.CurCode<>T2.CurCode ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select T3a.RateDt, T3a.CurCode1, T3a.CurCode2, T3a.Amt ");
                SQL.AppendLine("        From TblCurrencyRate T3a  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select CurCode1, CurCode2, Max(RateDt) As RateDt  ");
                SQL.AppendLine("            From TblCurrencyRate Group By CurCode1, CurCode2 ");
                SQL.AppendLine("        ) T3b On T3a.CurCode1=T3b.CurCode1 And T3a.CurCode2=T3b.CurCode2 And T3a.RateDt=T3b.RateDt ");
                SQL.AppendLine("    ) T3 On T1.CurCode=T3.CurCode1 And T2.CurCode=T3.CurCode2 ");
                SQL.AppendLine(") E On  E.CurCode1=C.CurCode And E.CurCode2=D.ParValue ");
                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("        Select Z2.ItCode, SUM(Qty01) As Qty01, SUM(Qty03) As Qty03, SUm(Qty06) As Qty06, SUm(Qty09) As Qty09, SUm(Qty12) As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("        select Z1.itCode, ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("            From ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select T1.Mth, T2.ItCode, SUM(T2.Qty)  As Qty ");
                SQL.AppendLine("                From ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	                Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("                )T1 ");
                SQL.AppendLine("                Inner Join ");
                SQL.AppendLine("                ( ");
                SQL.AppendLine("	                Select  convert('01' using latin1) As Mth, B.ItCode, SUM(B.Qty) As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode  ");
                SQL.AppendLine("	                Union ALL ");
                SQL.AppendLine("	                Select  convert('03' using latin1) As Mth, B.ItCode, SUM(B.Qty)/3 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('06' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/6 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('09' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/9 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("	                Union All ");
                SQL.AppendLine("	                Select  convert('12' using latin1) As Mth, B.ItCode,  SUM(B.Qty)/12 As Qty, D.InventoryUomCode ");
                SQL.AppendLine("	                From TblDODeptHdr A ");
                SQL.AppendLine("	                Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' ");
                SQL.AppendLine("	                Inner Join TblItem D On B.ItCode=D.ItCode  ");
                SQL.AppendLine("		                Where A.DORequestDeptDocNo Is Null And A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("	                Group By  B.ItCode, D.InventoryUomCode ");
                SQL.AppendLine("                )T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.mth, T2.ItCode ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("   )Z2 Group By Z2.ItCode ");
                SQL.AppendLine(" ) F On F.ItCode = A.ItCode ");
                SQL.AppendLine("Left Join TblItemSubCategory G On A.ItScCode=G.ItScCode ");
                SQL.AppendLine("Left Join TblItemGroup H On A.ItGrpCode=H.ItGrpCode ");

            }

            SQL.AppendLine("Where A.ActInd = 'Y' ");

            if (Sm.IsDataExist("Select ParCode From TblParameter Where ParCode='ItemManagedByWhsInd' And ParValue='Y' Limit 1 "))
                SQL.AppendLine("And (IfNull(A.ControlByDeptCode, '')='' Or (IfNull(A.ControlByDeptCode, '')<>'' And A.ControlByDeptCode=@DeptCode)) ");

            return SQL.ToString();
        }

        internal void ShowDORequestItem(string DocNo)
        {
            ClearGrd();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo As DORequestDocNo, B.DNo As DORequestDNo, B.ItCode, ");
            SQL.AppendLine("X.ItCtName, X.PurchaseUomCode, X.DocNo, X.DNo, X.DocDt, X.UPrice, X.MinStock, X.ReorderStock, ");
            SQL.AppendLine("X.ItName, X.ForeignName, ");
            SQL.AppendLine("X.Mth01, X.Mth03, X.Mth06, X.Mth09, X.Mth12, ");
            SQL.AppendLine("X.ItScCode, X.ItScName, X.ItCodeInternal, X.ItGrpCode, X.ItGrpName, ");
            SQL.AppendLine("(IfNull(B.Qty, 0.00)-IfNull(D.Qty, 0.00)) As MRQty ");
            SQL.AppendLine("From TblDORequestDeptHdr A ");
            SQL.AppendLine("Inner Join TblDORequestDeptDtl B On A.DocNo = B.DocNo And B.ProcessInd = 'O' ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine(ItemSelection());

            SQL.AppendLine(")X On B.ItCode = X.ItCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("  Select ItCode, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("  From TblStockSummary ");
            SQL.AppendLine("  Where WhsCode = @WhsCode ");
            SQL.AppendLine("  And Qty>0 ");
            SQL.AppendLine("  Group By ItCode ");
            SQL.AppendLine(") D On B.ItCode = D.ItCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And IfNull(B.Qty, 0.00)>IfNull(D.Qty, 0.00) ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetValue("Select WhsCode From TblDORequestDeptHdr Where DocNo = '" + DocNo + "'; "));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItCodeInternal", "ItGrpName", "ItName", "ForeignName",  "ItCtName",   
                    
                    //6-7
                    "ItScCode", "ItScName", "PurchaseUomCode", "DocNo", "DNo", 

                    //11-15
                    "DocDt", "UPrice", "MinStock", "ReorderStock", "Mth01",   

                    //16-20
                    "Mth03", "Mth06", "Mth09", "Mth12", "DORequestDocNo",
                    
                    //21-22
                    "DORequestDNo", "MRQty"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 22);
                    Grd1.Cells[Row, 29].Value = 0;

                    if (mIsBudgetActive)
                        Grd1.Cells[Row, 11].ForeColor = Sm.GetGrdStr(Grd1, Row, 20).Length > 0 ? Color.Black : Color.Red;
                    ComputeTotal(Row);
                }, false, false, true, false
            );
            
            Sm.FocusGrd(Grd1, 0, 1);

            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 15, 16, 17, 23, 24, 29, 31, 32 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetLueDeptCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine("And ActInd = 'Y' ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsDeptFilterBySite)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblDepartmentBudgetSite ");
                SQL.AppendLine("    Where SiteCode=@SiteCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();
                
                SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("Order By SiteName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueReqType(ref DXE.LookUpEdit Lue, string ReqType)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='ReqType' ");
                if (ReqType.Length > 0)
                    SQL.AppendLine("And OptCode=@ReqType ");
                else
                {
                    if (!mIsMaterialRequestForDroppingRequestEnabled)
                    {
                        if (mIsBudgetActive && mReqTypeForNonBudget.Length > 0)
                            SQL.AppendLine("And OptCode<>@ReqTypeForNonBudget ");
                    }
                }
                SQL.AppendLine("Order By OptDesc;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ReqTypeForNonBudget", mReqTypeForNonBudget);
                Sm.CmParam<String>(ref cm, "@ReqType", ReqType);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        
        private void SetLueMRType(ref DXE.LookUpEdit Lue, string MRType)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='MRType' ");
                if (MRType.Length > 0)
                    SQL.AppendLine("And OptCode=@MRType ");
                SQL.AppendLine("Order By OptDesc;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@MRType", MRType);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetBudgetCategory()
        {
            LueBCCode.EditValue = null;
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, true);

            var ReqType = Sm.GetLue(LueReqType);
            var DeptCode = Sm.GetLue(LueDeptCode);

            if (
                mReqTypeForNonBudget.Length == 0 ||
                ReqType.Length == 0 ||
                Sm.CompareStr(ReqType, mReqTypeForNonBudget) ||
                DeptCode.Length == 0
                ) return;

            Sl.SetLueBCCode(ref LueBCCode, string.Empty, DeptCode);
            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, false);
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblMaterialRequestDtl " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        private void ShowMRApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            if (mIsMRSPPJB)
                SQL.AppendLine("Where DocType = 'MaterialRequestSPPJB' ");
            else
                SQL.AppendLine("Where DocType='MaterialRequest' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo "); //
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "UserCode", "StatusDesc", "LastUpDt", "Remark" });
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private bool SelectMRinIEP()
        {
            
            string CancelInd = string.Empty;
            int CancelCount = 0;

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                CancelInd = Sm.GetGrdStr(Grd1, r, 3);
                if (CancelInd.Length > 0)
                {
                    CancelCount = CancelCount + 1;
                }
            }

            if(CancelCount == Grd1.Rows.Count - 1 )
            { 
                return true;
            }
            else
            {
                return false;
            }
        }
        private void UploadFile(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            //if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            //if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile4(string DocNo)
        {
            //if (IsUploadFileNotValid4()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile4.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile4.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload4.Invoke(
                    (MethodInvoker)delegate { PbUpload4.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload4.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload4.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile4(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile5(string DocNo)
        {
            //if (IsUploadFileNotValid5()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile5.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile5.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload5.Invoke(
                    (MethodInvoker)delegate { PbUpload5.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload5.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload5.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile5(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile6(string DocNo)
        {
            //if (IsUploadFileNotValid6()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile6.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile6.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload6.Invoke(
                    (MethodInvoker)delegate { PbUpload6.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload6.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload6.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateMRFile6(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }
        
        private bool IsUploadFileNotValid()
        {
            return
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsUploadFileNotValid4()
        {
            return
                IsFTPClientDataNotValid4() ||
                IsFileSizeNotvalid4() ||
                IsFileNameAlreadyExisted4();
        }

        private bool IsUploadFileNotValid5()
        {
            return
                IsFTPClientDataNotValid5() ||
                IsFileSizeNotvalid5() ||
                IsFileNameAlreadyExisted5();
        }

        private bool IsUploadFileNotValid6()
        {
            return
                IsFTPClientDataNotValid6() ||
                IsFileSizeNotvalid6() ||
                IsFileNameAlreadyExisted6();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length>0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }
            

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }
            
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }
           

            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            
            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }
            
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }
            
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }
           
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

           if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

           
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid4()
        {

            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid5()
        {

            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid6()
        {

            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0 )
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

               
                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsMRAllowToUploadFile  && TxtFile2.Text.Length > 0 )
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile2.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile3.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid4()
        {
            if (mIsMRAllowToUploadFile && TxtFile4.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile4.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile4.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid5()
        {
            if (mIsMRAllowToUploadFile && TxtFile5.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile5.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile5.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid6()
        {
            if (mIsMRAllowToUploadFile && TxtFile6.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile6.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File " + TxtFile6.Text + " too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsMRAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2 ()
        {
            if (mIsMRAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsMRAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted4()
        {
            if (TxtFile4.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile4.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName4=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted5()
        {
            if (TxtFile5.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile5.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName5=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted6()
        {
            if (TxtFile6.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile6.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName6=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();
                
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress,":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true; 

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false; 

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        
        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }
                       
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile4(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload4.Value = 0;
                PbUpload4.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload4.Value = PbUpload4.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload4.Value + bytesRead <= PbUpload4.Maximum)
                        {
                            PbUpload4.Value += bytesRead;

                            PbUpload4.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile5(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload5.Value = 0;
                PbUpload5.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload5.Value = PbUpload5.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload5.Value + bytesRead <= PbUpload5.Maximum)
                        {
                            PbUpload5.Value += bytesRead;

                            PbUpload5.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile6(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload6.Value = 0;
                PbUpload6.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload6.Value = PbUpload6.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload6.Value + bytesRead <= PbUpload6.Maximum)
                        {
                            PbUpload6.Value += bytesRead;

                            PbUpload6.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void LabelForMR()
        {
            string[] mLabelFile = { };
            string[] mMandatoryFile = { };

            if (mLabelFileForMR.Length > 0)
            {
                mLabelFile = mLabelFileForMR.Split(',');
            }

            Label[] lblFile = new Label[6] { LblFile, LblFile2, LblFile3, LblFile4, LblFile5, LblFile6 };

            for (int i = 0; i < mLabelFile.Length; i++)
            {
                lblFile[i].Text = mLabelFile[i];
            }

            int distance = mLabelFile.Max(s => s.Length);

            splitContainer1.SplitterDistance = distance * 7;

            for (int i = 0; i < lblFile.Length; i++)
            {
                int x = splitContainer1.SplitterDistance - lblFile[i].Width;
                lblFile[i].Left = x;
            }

            if (mMandatoryFileForMR.Length > 0)
            {
                mMandatoryFile = mMandatoryFileForMR.Split(',');

                foreach (string lbl in mMandatoryFile)
                {
                    switch (lbl.Trim().ToUpper())
                    {
                        case "FILE1":
                            LblFile.ForeColor = Color.Red;
                            break;
                        case "FILE2":
                            LblFile2.ForeColor = Color.Red;
                            break;
                        case "FILE3":
                            LblFile3.ForeColor = Color.Red;
                            break;
                        case "FILE4":
                            LblFile4.ForeColor = Color.Red;
                            break;
                        case "FILE5":
                            LblFile5.ForeColor = Color.Red;
                            break;
                        case "FILE6":
                            LblFile6.ForeColor = Color.Red;
                            break;
                    }
                }
            }
        }

        private bool IsFileMandatory()
        {
            string[] mMandatoryFile = { };

            if (mMandatoryFileForMR.Length > 0)
            {
                mMandatoryFile = mMandatoryFileForMR.Split(',');

                foreach (var mMandatoryInd in mMandatoryFile)
                {
                    if (mMandatoryInd.Trim().ToUpper() == "FILE1")
                    {
                        if (TxtFile.Text == "" || TxtFile.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile.Text + " is Empty");
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE2")
                    {
                        if (TxtFile2.Text == "" || TxtFile2.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile2.Text + " is Empty");
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE3")
                    {
                        if (TxtFile3.Text == "" || TxtFile3.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile3.Text + " is Empty");
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE4")
                    {
                        if (TxtFile4.Text == "" || TxtFile4.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile4.Text + " is Empty");
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE5")
                    {
                        if (TxtFile5.Text == "" || TxtFile5.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile5.Text + " is Empty");
                            return true;
                        }
                    }
                    else if (mMandatoryInd.Trim().ToUpper() == "FILE6")
                    {
                        if (TxtFile6.Text == "" || TxtFile6.Text == "openFileDialog1")
                        {
                            Sm.StdMsg(mMsgType.Warning, LblFile6.Text + " is Empty");
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        internal void SetDroppingReqProjImplItQty()
        {
            if (TxtDR_PRJIDocNo.Text.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select T2.ResourceItCode, T1.Qty, IfNull(T3.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl T1 ");
            SQL.AppendLine("Inner Join TblProjectImplementationRBPHdr T2 On T1.PRBPDocNo=T2.DocNo ");
            SQL.AppendLine("Left Join TblProjectImplementationDtl2 T3 On T2.PRJIDocNo=T3.DocNo And T2.ResourceItCode=T3.ResourceItCode ");
            SQL.AppendLine("Where T1.DocNo=@DroppingRequestDocNo And T1.MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "ResourceItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) && Sm.GetGrdDec(Grd1, r, 31)==0m)
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ComputeDR_Balance();
        }

        internal void SetDroppingReqBCItQty()
        {
            if (mDroppingRequestBCCode.Length == 0) return;

            var SQL = new StringBuilder();
            string ItCode = string.Empty;
            decimal Qty = 0m, Amt = 0m;

            SQL.AppendLine("Select ItCode, Qty, Amt ");
            SQL.AppendLine("From TblDroppingRequestDtl2 ");
            SQL.AppendLine("Where DocNo=@DroppingRequestDocNo ");
            SQL.AppendLine("And BCCode=@DroppingRequestBCCode ");
            SQL.AppendLine("And MRDocNo Is Null ;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDroppingRequestDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DroppingRequestBCCode", mDroppingRequestBCCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Qty", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        Qty = Sm.DrDec(dr, 1);
                        Amt = Sm.DrDec(dr, 2);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode) &&
                                Sm.GetGrdDec(Grd1, r, 31) == 0m
                                )
                            {
                                Grd1.Cells[r, 17].Value = Qty;
                                Grd1.Cells[r, 31].Value = Qty;
                                Grd1.Cells[r, 32].Value = Amt;
                                Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
            ComputeDR_Balance();
        }
        
        internal void SetApprovalSheet()
        {
            var SQL = new StringBuilder();
            string ItCode = string.Empty, DocNo = string.Empty, DNo = string.Empty;
            decimal OutstandingQty = 0m;

            SQL.AppendLine("Select DocNo, DNo, ItCode, OutstandingQty ");
            SQL.AppendLine("From TblApprovalSheetDtl ");
            SQL.AppendLine("Where DocNo=@ApprovalSheetDocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@ApprovalSheetDocNo", TxtApprovalSheetDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "ItCode", "OutstandingQty" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        DocNo = Sm.DrStr(dr, 0);
                        DNo = Sm.DrStr(dr, 1);
                        ItCode = Sm.DrStr(dr, 2);
                        OutstandingQty = Sm.DrDec(dr, 3);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode)
                                )
                            {
                                Grd1.Cells[r, 17].Value = OutstandingQty;
                                Grd1.Cells[r, 45].Value = DocNo;
                                Grd1.Cells[r, 46].Value = DNo;
                                Grd1.Cells[r, 47].Value = OutstandingQty;
                                //Grd1.Cells[r, 24].Value = Sm.GetGrdDec(Grd1, r, 17) * Sm.GetGrdDec(Grd1, r, 23);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDroppingRequestDocNo_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDroppingRequestDocNo.Text.Length > 0)
                {
                    LueBCCode.EditValue = null;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, true);
                }
                else
                {
                    LueBCCode.EditValue = null;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, true);

                    var ReqType = Sm.GetLue(LueReqType);
                    var DeptCode = Sm.GetLue(LueDeptCode);

                    if (
                        mReqTypeForNonBudget.Length == 0 ||
                        ReqType.Length == 0 ||
                        Sm.CompareStr(ReqType, mReqTypeForNonBudget) ||
                        DeptCode.Length == 0
                        ) return;

                    Sl.SetLueBCCode(ref LueBCCode, string.Empty, DeptCode);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueBCCode }, false);
                }
            }
        }

        private void LueReqType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueReqType, new Sm.RefreshLue2(SetLueReqType), string.Empty);
                if (mIsMaterialRequestForDroppingRequestEnabled && TxtDroppingRequestDocNo.Text.Length > 0)
                {
                    mDroppingRequestBCCode = string.Empty;
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo, 
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    { 
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
                    }, 0);
                }
                else
                    SetBudgetCategory();
                    
                ClearGrd();
                ComputeRemainingBudget();
                ComputeBudgetForMR();
                ComputeDR_Balance();
            }
        }

        private void LueProcurementType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcurementType, new Sm.RefreshLue2(Sl.SetLueOption), "ProcurementType");
        }

        private void LueMRType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMRType, new Sm.RefreshLue2(Sl.SetLueOption), "MRType");
        }

        private void DteUsageDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteUsageDt, ref fCell, ref fAccept);
        }

        private void DteUsageDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDocDt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsMaterialRequestForDroppingRequestEnabled && TxtDroppingRequestDocNo.Text.Length > 0)
                {
                    mDroppingRequestBCCode = string.Empty;
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo, 
                        TxtDR_BCCode, MeeDR_Remark
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                    { 
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance 
                    }, 0);
                    ClearGrd();
                }
                ComputeRemainingBudget();
                ComputeBudgetForMR();
                ComputeDR_Balance();
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeRemainingBudget();
                ComputeBudgetForMR();
                ComputeDR_Balance();
            }
        }

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
                if (mIsDeptFilterBySite)
                {
                    if (Sm.GetLue(LueSiteCode).Length > 0)
                    {
                        Sm.SetControlReadOnly(LueDeptCode, false);
                        LueDeptCode.EditValue = "<Refresh>";
                        Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                    }
                    else
                    {
                        LueDeptCode.EditValue = null;
                        Sm.SetControlReadOnly(LueDeptCode, true);
                    }
                }
                if (mIsMRSPPJB && mIsFilterBySite)
                {
                    ClearGrd();
                    mlJob.Clear();
                    ComputeRemainingBudget();
                    ComputeBudgetForMR();
                    ComputeDR_Balance();
                }
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsDeptFilterBySite)
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(SetLueDeptCode));
                else
                    Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
                if (!mIsMaterialRequestForDroppingRequestEnabled) SetBudgetCategory();
                mDroppingRequestBCCode = string.Empty;
                if (mIsMaterialRequestForDroppingRequestEnabled)
                {
                    if (TxtDroppingRequestDocNo.Text.Length > 0)
                    {
                        Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
                    {
                        TxtDroppingRequestDocNo, TxtDR_DeptCode, TxtDR_Yr, TxtDR_Mth, TxtDR_PRJIDocNo,
                        TxtDR_BCCode, MeeDR_Remark
                    });
                        Sm.SetControlNumValueZero(new List<DXE.TextEdit>
                    {
                        TxtDR_DroppingRequestAmt, TxtDR_MRAmt, TxtDR_Balance
                    }, 0);
                    }
                    else 
                        SetBudgetCategory();
                }
                ClearGrd();
                ComputeRemainingBudget();
                ComputeBudgetForMR();
                ComputeDR_Balance();
            }
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(Sl.SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtRemainingBudget, TxtBudgetForMR }, 0);
            ComputeRemainingBudget();
            ComputeBudgetForMR();
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        public static void SetLueCurCode(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue, "Select CurCode As Col1 From tblCurrency ", "Currency");
        }

        //public static void SetLuePICCode(ref LookUpEdit Lue)
        //{
        //    Sm.SetLue2(ref Lue, "Select UserCode As Col1, UserName As Col2 From TblUser Order By UserName", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        private void SetLuePICCode (ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select UserCode As Col1, UserName As Col2 ");
                SQL.AppendLine("From TblUser ");
                if(mIsPICGrouped)
                    SQL.AppendLine("where UserCode=@UserCode ");
                SQL.AppendLine("Order By UserName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(SetLueCurCode));
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (LueCurCode.Visible && fAccept && fCell.ColIndex == 28)
            {
                if (Sm.GetLue(LueCurCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 28].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 28].Value = Sm.GetLue(LueCurCode);
                }
                LueCurCode.Visible = false;
            }
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void ChkFile4_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile4.Checked == false)
            {
                TxtFile4.EditValue = string.Empty;
            }
        }

        private void ChkFile5_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile5.Checked == false)
            {
                TxtFile5.EditValue = string.Empty;
            }
        }

        private void ChkFile6_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile6.Checked == false)
            {
                TxtFile6.EditValue = string.Empty;
            }
        }

        private void LuePICCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(SetLuePICCode));
            
        }

        private void LueDurationUom_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDurationUom, new Sm.RefreshLue2(Sl.SetLueOption), "DurationUom");
        }

        private void LueDurationUom_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDurationUom_Leave(object sender, EventArgs e)
        {
            if (LueDurationUom.Visible && fAccept && fCell.ColIndex == 37)
            {
                if (Sm.GetLue(LueDurationUom).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 36].Value =
                    Grd1.Cells[fCell.RowIndex, 37].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 36].Value = Sm.GetLue(LueDurationUom);
                    Grd1.Cells[fCell.RowIndex, 37].Value = LueDurationUom.GetColumnValue("Col2");
                }
                LueDurationUom.Visible = false;
            }
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LuePtCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePtCode_Leave(object sender, EventArgs e)
        {
            if (LuePtCode.Visible && fAccept && fCell.ColIndex == 40)
            {
                if (Sm.GetLue(LuePtCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 39].Value =
                    Grd1.Cells[fCell.RowIndex, 40].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 39].Value = Sm.GetLue(LuePtCode);
                    Grd1.Cells[fCell.RowIndex, 40].Value = LuePtCode.GetColumnValue("Col2");
                }
                LuePtCode.Visible = false;
            }
        }

        #endregion

        #region Button  Event

        private void BtnReference_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialRequestDlg8(this));
        }

        private void BtnPOQtyCancelDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialRequestDlg2(this));
        }

        private void BtnPOQtyCancelDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPOQtyCancelDocNo, "Cancellation PO#", false))
            {
                var f = new FrmPOQtyCancel(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtPOQtyCancelDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDORequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueReqType, "Request Type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequestDlg3(this, Sm.GetLue(LueDeptCode)));
        }

        private void BtnDORequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDORequestDocNo, "DO Request#", false))
            {
                var f = new FrmDORequestDept(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDORequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDroppingRequestDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDroppingRequestDocNo, "Dropping request#", false))
            {
                var f = new FrmDroppingRequest(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDroppingRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDroppingRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueReqType, "Request type") && !Sm.IsLueEmpty(LueDeptCode, "Department"))
                Sm.FormShowDialog(new FrmMaterialRequestDlg4(this, Sm.GetDte(DteDocDt), Sm.GetLue(LueDeptCode)));
        }

        private void BtnApprovalSheetDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMaterialRequestDlg9(this));
        }

        private void BtnApprovalSheetDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtApprovalSheetDocNo, "Approval Sheet#", false))
            {
                var f = new FrmApprovalSheet(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtApprovalSheetDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        internal class Job
        {
            public string ItCode { get; set; }
            public string JobCode { get; set; }
            public string JobName { get; set; }
            public string JobCtName { get; set; }
            public string UomCode { get; set; }
            public string PrevCurCode { get; set; }
            public decimal PrevUPrice { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Qty { get; set; }
            public string Remark { get; set; }
            public decimal Total { get; set; }
            public int No { get; set; }
        }

        private class TransferredBudget
        { 
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string DeptCode { get; set; }
            public string BCCode { get; set; }
            public decimal Amt { get; set; }
        }

        #region Report Class

        private class MatReq
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyAddressCity { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string OptDesc { get; set; }
            public string HRemark { get; set; }
            public string CreateBy { get; set; }
            public string PrintBy { get; set; }
            public string SiteInd { get; set; }
            public string LocalDocNo { get; set; }
            public string BCCode { get; set; }

        }

        class MatReq1
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MatReq2
        {
            public string ApprovalDno { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MatReqDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string UsageDt { get; set; }
            public decimal Mth01 { get; set; }
            public decimal Mth03 { get; set; }
            public decimal Mth06 { get; set; }
            public decimal Mth09 { get; set; }
            public decimal Mth12 { get; set; }
            public string DRemark { get; set; }
            public string ForeignName { get; set; }
            public decimal EstPrice { get; set; }
            public string Curcode { get; set; }
            public string Duration { get; set; }
            public string DurationUom { get; set; }
        }

        private class MatReqDtl2
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }

        }

        private class MatReqDtl3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string IsPrintOutUseDigitalSignature { get; set; }
        }

        private class MatReqDtl4
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string IsPrintOutUseDigitalSignature { get; set; }
        }

        private class SignMnet
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string LastUpDt { get; set; }
            public string PosName { get; set; }
            public string Position { get; set; }
            public string Date { get; set; }
            public string Sequence { get; set; }
            public string Title { get; set; }
            public string Space { get; set; }
            public string LabelName { get; set; }
            public string LabelPos { get; set; }
            public string LabelDt { get; set; }

        }

        private class MatReqSier
        {
            public string BudgetCategory { get; set; }
            public decimal Budget { get; set; }
            public decimal AfterUsageAmt { get; set; }
            public decimal UsageAmt { get; set; }
            public decimal BalanceAmt { get; set; }
            public decimal UsageAmt2 { get; set; }
            public decimal AvailableBudget { get; set; }
            public decimal Amt { get; set; }


            
        }

        private class MatReqSPPJB
        {
            public int No { get; set;  }
            public string JobName { get; set; }
            public string JobCtName { get; set; }
            public decimal UPrice { get; set; }
            public decimal Qty { get; set; }
            public string ItName { get; set; }
            public decimal Total { get; set; }
        }

        private class SignatureSIER
        {
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Date { get; set; }


        }

        private class MRIMS
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string DocName { get; set; }
            public string CompanyLogo { get; set; }
        }

        private class MRIMSDtl
        {
            public int No { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string ProjectCode { get; set; }
            public string UsageDt { get; set; }
            public string Remark { get; set; }
        }

        private class MRIMSSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class MRIMSSign2
        {
            public string CreateUserName { get; set; }
            public string CreateDocDt { get; set; }
            public string ApproveSignature { get; set; }
            public string ApproveUserName { get; set; }
            public string ApproveDocDt { get; set; }
            public string Approve2Signature { get; set; }
            public string Approve2UserName { get; set; }
            public string Approve2DocDt { get; set; }
            public string Approve3Signature { get; set; }
            public string Approve3UserName { get; set; }
            public string Approve3DocDt { get; set; }
        }
        #endregion       
        
        #endregion
    }
}
