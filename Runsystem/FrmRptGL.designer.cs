﻿namespace RunSystem
{
    partial class FrmRptGL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkAcNo = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.ChkAcDesc = new DevExpress.XtraEditors.CheckEdit();
            this.ChkAcNo2 = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.ChkAcNo3 = new DevExpress.XtraEditors.CheckEdit();
            this.TxtAcNo14 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo1 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo7 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo13 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo8 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo6 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo12 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo9 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo3 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo5 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo11 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo10 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo4 = new DevExpress.XtraEditors.TextEdit();
            this.ChkProfitCenterCode = new DevExpress.XtraEditors.CheckEdit();
            this.CcbAcNo2 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.CcbProfitCenterCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.LblMultiProfitCenterCode = new System.Windows.Forms.Label();
            this.TxtAlias = new DevExpress.XtraEditors.TextEdit();
            this.ChkAlias = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.ChkEqualizeJournal = new DevExpress.XtraEditors.CheckEdit();
            this.CcbEntCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.ChkEntCode = new DevExpress.XtraEditors.CheckEdit();
            this.LblPeriod = new System.Windows.Forms.Label();
            this.TxtPeriod = new DevExpress.XtraEditors.TextEdit();
            this.ChkPeriod = new DevExpress.XtraEditors.CheckEdit();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ChkCCParentJournal = new DevExpress.XtraEditors.CheckEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo2.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAlias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEqualizeJournal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCCParentJournal.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.ChkCCParentJournal);
            this.panel2.Controls.Add(this.LblPeriod);
            this.panel2.Controls.Add(this.TxtPeriod);
            this.panel2.Controls.Add(this.ChkPeriod);
            this.panel2.Controls.Add(this.ChkEntCode);
            this.panel2.Controls.Add(this.ChkEqualizeJournal);
            this.panel2.Controls.Add(this.CcbEntCode);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.LblEntCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.ChkDocNo);
            this.panel2.Size = new System.Drawing.Size(772, 139);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 334);
            this.Grd1.TabIndex = 54;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 139);
            this.panel3.Size = new System.Drawing.Size(772, 334);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(60, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 14);
            this.label2.TabIndex = 23;
            this.label2.Text = "Account#";
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(126, 4);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 30;
            this.TxtAcNo.Size = new System.Drawing.Size(284, 20);
            this.TxtAcNo.TabIndex = 24;
            this.TxtAcNo.Validated += new System.EventHandler(this.TxtAcNo_Validated);
            // 
            // ChkAcNo
            // 
            this.ChkAcNo.Location = new System.Drawing.Point(412, 3);
            this.ChkAcNo.Name = "ChkAcNo";
            this.ChkAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcNo.Properties.Appearance.Options.UseFont = true;
            this.ChkAcNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcNo.Properties.Caption = " ";
            this.ChkAcNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcNo.Size = new System.Drawing.Size(19, 22);
            this.ChkAcNo.TabIndex = 25;
            this.ChkAcNo.ToolTip = "Remove filter";
            this.ChkAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcNo.ToolTipTitle = "Run System";
            this.ChkAcNo.CheckedChanged += new System.EventHandler(this.ChkAcNo_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(189, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(48, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Date";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(202, 25);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt2.TabIndex = 14;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(84, 25);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt1.TabIndex = 12;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 14);
            this.label6.TabIndex = 8;
            this.label6.Text = "Document#";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(84, 4);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Size = new System.Drawing.Size(219, 20);
            this.TxtDocNo.TabIndex = 9;
            this.TxtDocNo.Validated += new System.EventHandler(this.TxtDocNo_Validated);
            // 
            // ChkDocNo
            // 
            this.ChkDocNo.Location = new System.Drawing.Point(305, 3);
            this.ChkDocNo.Name = "ChkDocNo";
            this.ChkDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocNo.Properties.Caption = " ";
            this.ChkDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocNo.Size = new System.Drawing.Size(19, 22);
            this.ChkDocNo.TabIndex = 10;
            this.ChkDocNo.ToolTip = "Remove filter";
            this.ChkDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocNo.ToolTipTitle = "Run System";
            this.ChkDocNo.CheckedChanged += new System.EventHandler(this.ChkDocNo_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(5, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 14);
            this.label4.TabIndex = 45;
            this.label4.Text = "Account Description";
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(126, 67);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 30;
            this.TxtAcDesc.Size = new System.Drawing.Size(284, 20);
            this.TxtAcDesc.TabIndex = 46;
            this.TxtAcDesc.Validated += new System.EventHandler(this.TxtAcDesc_Validated);
            // 
            // ChkAcDesc
            // 
            this.ChkAcDesc.Location = new System.Drawing.Point(412, 66);
            this.ChkAcDesc.Name = "ChkAcDesc";
            this.ChkAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcDesc.Properties.Appearance.Options.UseFont = true;
            this.ChkAcDesc.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcDesc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcDesc.Properties.Caption = " ";
            this.ChkAcDesc.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcDesc.Size = new System.Drawing.Size(19, 22);
            this.ChkAcDesc.TabIndex = 47;
            this.ChkAcDesc.ToolTip = "Remove filter";
            this.ChkAcDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcDesc.ToolTipTitle = "Run System";
            this.ChkAcDesc.CheckedChanged += new System.EventHandler(this.ChkAcDesc_CheckedChanged);
            // 
            // ChkAcNo2
            // 
            this.ChkAcNo2.Location = new System.Drawing.Point(412, 87);
            this.ChkAcNo2.Name = "ChkAcNo2";
            this.ChkAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcNo2.Properties.Appearance.Options.UseFont = true;
            this.ChkAcNo2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcNo2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcNo2.Properties.Caption = " ";
            this.ChkAcNo2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcNo2.Size = new System.Drawing.Size(20, 22);
            this.ChkAcNo2.TabIndex = 50;
            this.ChkAcNo2.ToolTip = "Remove filter";
            this.ChkAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcNo2.ToolTipTitle = "Run System";
            this.ChkAcNo2.CheckedChanged += new System.EventHandler(this.ChkAcNo2_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(33, 91);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 14);
            this.label5.TabIndex = 48;
            this.label5.Text = "COA\'s Account";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(42, 49);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(39, 14);
            this.LblEntCode.TabIndex = 15;
            this.LblEntCode.Text = "Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.ChkProfitCenterCode);
            this.panel4.Controls.Add(this.CcbAcNo2);
            this.panel4.Controls.Add(this.CcbProfitCenterCode);
            this.panel4.Controls.Add(this.LblMultiProfitCenterCode);
            this.panel4.Controls.Add(this.TxtAlias);
            this.panel4.Controls.Add(this.ChkAlias);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtAcNo);
            this.panel4.Controls.Add(this.ChkAcNo);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.ChkAcDesc);
            this.panel4.Controls.Add(this.ChkAcNo2);
            this.panel4.Controls.Add(this.TxtAcDesc);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(332, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(436, 135);
            this.panel4.TabIndex = 22;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ChkAcNo3);
            this.panel5.Controls.Add(this.TxtAcNo14);
            this.panel5.Controls.Add(this.TxtAcNo1);
            this.panel5.Controls.Add(this.TxtAcNo2);
            this.panel5.Controls.Add(this.TxtAcNo7);
            this.panel5.Controls.Add(this.TxtAcNo13);
            this.panel5.Controls.Add(this.TxtAcNo8);
            this.panel5.Controls.Add(this.TxtAcNo6);
            this.panel5.Controls.Add(this.TxtAcNo12);
            this.panel5.Controls.Add(this.TxtAcNo9);
            this.panel5.Controls.Add(this.TxtAcNo3);
            this.panel5.Controls.Add(this.TxtAcNo5);
            this.panel5.Controls.Add(this.TxtAcNo11);
            this.panel5.Controls.Add(this.TxtAcNo10);
            this.panel5.Controls.Add(this.TxtAcNo4);
            this.panel5.Location = new System.Drawing.Point(81, 24);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(350, 22);
            this.panel5.TabIndex = 26;
            // 
            // ChkAcNo3
            // 
            this.ChkAcNo3.Location = new System.Drawing.Point(331, 1);
            this.ChkAcNo3.Name = "ChkAcNo3";
            this.ChkAcNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAcNo3.Properties.Appearance.Options.UseFont = true;
            this.ChkAcNo3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAcNo3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAcNo3.Properties.Caption = " ";
            this.ChkAcNo3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAcNo3.Size = new System.Drawing.Size(19, 22);
            this.ChkAcNo3.TabIndex = 41;
            this.ChkAcNo3.ToolTip = "Remove filter";
            this.ChkAcNo3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAcNo3.ToolTipTitle = "Run System";
            this.ChkAcNo3.CheckedChanged += new System.EventHandler(this.ChkAcNo3_CheckedChanged);
            // 
            // TxtAcNo14
            // 
            this.TxtAcNo14.EnterMoveNextControl = true;
            this.TxtAcNo14.Location = new System.Drawing.Point(311, 1);
            this.TxtAcNo14.Name = "TxtAcNo14";
            this.TxtAcNo14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo14.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo14.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo14.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo14.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo14.Properties.MaxLength = 30;
            this.TxtAcNo14.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo14.TabIndex = 40;
            this.TxtAcNo14.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo1
            // 
            this.TxtAcNo1.EnterMoveNextControl = true;
            this.TxtAcNo1.Location = new System.Drawing.Point(45, 1);
            this.TxtAcNo1.Name = "TxtAcNo1";
            this.TxtAcNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo1.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo1.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo1.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo1.Properties.MaxLength = 30;
            this.TxtAcNo1.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo1.TabIndex = 27;
            this.TxtAcNo1.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(67, 1);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo2.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo2.Properties.MaxLength = 30;
            this.TxtAcNo2.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo2.TabIndex = 28;
            this.TxtAcNo2.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo7
            // 
            this.TxtAcNo7.EnterMoveNextControl = true;
            this.TxtAcNo7.Location = new System.Drawing.Point(177, 1);
            this.TxtAcNo7.Name = "TxtAcNo7";
            this.TxtAcNo7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo7.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo7.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo7.Properties.MaxLength = 30;
            this.TxtAcNo7.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo7.TabIndex = 33;
            this.TxtAcNo7.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo13
            // 
            this.TxtAcNo13.EnterMoveNextControl = true;
            this.TxtAcNo13.Location = new System.Drawing.Point(293, 1);
            this.TxtAcNo13.Name = "TxtAcNo13";
            this.TxtAcNo13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo13.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo13.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo13.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo13.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo13.Properties.MaxLength = 30;
            this.TxtAcNo13.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo13.TabIndex = 39;
            this.TxtAcNo13.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo8
            // 
            this.TxtAcNo8.EnterMoveNextControl = true;
            this.TxtAcNo8.Location = new System.Drawing.Point(199, 1);
            this.TxtAcNo8.Name = "TxtAcNo8";
            this.TxtAcNo8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo8.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo8.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo8.Properties.MaxLength = 30;
            this.TxtAcNo8.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo8.TabIndex = 34;
            this.TxtAcNo8.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo6
            // 
            this.TxtAcNo6.EnterMoveNextControl = true;
            this.TxtAcNo6.Location = new System.Drawing.Point(155, 1);
            this.TxtAcNo6.Name = "TxtAcNo6";
            this.TxtAcNo6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo6.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo6.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo6.Properties.MaxLength = 30;
            this.TxtAcNo6.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo6.TabIndex = 32;
            this.TxtAcNo6.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo12
            // 
            this.TxtAcNo12.EnterMoveNextControl = true;
            this.TxtAcNo12.Location = new System.Drawing.Point(275, 1);
            this.TxtAcNo12.Name = "TxtAcNo12";
            this.TxtAcNo12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo12.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo12.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo12.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo12.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo12.Properties.MaxLength = 30;
            this.TxtAcNo12.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo12.TabIndex = 38;
            this.TxtAcNo12.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo9
            // 
            this.TxtAcNo9.EnterMoveNextControl = true;
            this.TxtAcNo9.Location = new System.Drawing.Point(217, 1);
            this.TxtAcNo9.Name = "TxtAcNo9";
            this.TxtAcNo9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo9.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo9.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo9.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo9.Properties.MaxLength = 30;
            this.TxtAcNo9.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo9.TabIndex = 35;
            this.TxtAcNo9.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo3
            // 
            this.TxtAcNo3.EnterMoveNextControl = true;
            this.TxtAcNo3.Location = new System.Drawing.Point(89, 1);
            this.TxtAcNo3.Name = "TxtAcNo3";
            this.TxtAcNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo3.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo3.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo3.Properties.MaxLength = 30;
            this.TxtAcNo3.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo3.TabIndex = 29;
            this.TxtAcNo3.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo5
            // 
            this.TxtAcNo5.EnterMoveNextControl = true;
            this.TxtAcNo5.Location = new System.Drawing.Point(133, 1);
            this.TxtAcNo5.Name = "TxtAcNo5";
            this.TxtAcNo5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo5.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo5.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo5.Properties.MaxLength = 30;
            this.TxtAcNo5.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo5.TabIndex = 31;
            this.TxtAcNo5.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo11
            // 
            this.TxtAcNo11.EnterMoveNextControl = true;
            this.TxtAcNo11.Location = new System.Drawing.Point(257, 1);
            this.TxtAcNo11.Name = "TxtAcNo11";
            this.TxtAcNo11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo11.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo11.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo11.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo11.Properties.MaxLength = 30;
            this.TxtAcNo11.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo11.TabIndex = 37;
            this.TxtAcNo11.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo10
            // 
            this.TxtAcNo10.EnterMoveNextControl = true;
            this.TxtAcNo10.Location = new System.Drawing.Point(235, 1);
            this.TxtAcNo10.Name = "TxtAcNo10";
            this.TxtAcNo10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo10.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo10.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo10.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo10.Properties.MaxLength = 30;
            this.TxtAcNo10.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo10.TabIndex = 36;
            this.TxtAcNo10.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // TxtAcNo4
            // 
            this.TxtAcNo4.EnterMoveNextControl = true;
            this.TxtAcNo4.Location = new System.Drawing.Point(111, 1);
            this.TxtAcNo4.Name = "TxtAcNo4";
            this.TxtAcNo4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo4.Properties.Mask.EditMask = "\\d{0,1}";
            this.TxtAcNo4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TxtAcNo4.Properties.Mask.ShowPlaceHolders = false;
            this.TxtAcNo4.Properties.MaxLength = 30;
            this.TxtAcNo4.Size = new System.Drawing.Size(18, 20);
            this.TxtAcNo4.TabIndex = 30;
            this.TxtAcNo4.Validated += new System.EventHandler(this.TxtAcNo1_Validated);
            // 
            // ChkProfitCenterCode
            // 
            this.ChkProfitCenterCode.Location = new System.Drawing.Point(413, 108);
            this.ChkProfitCenterCode.Name = "ChkProfitCenterCode";
            this.ChkProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode.Properties.Caption = " ";
            this.ChkProfitCenterCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode.Size = new System.Drawing.Size(18, 22);
            this.ChkProfitCenterCode.TabIndex = 53;
            this.ChkProfitCenterCode.ToolTip = "Remove filter";
            this.ChkProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode_CheckedChanged);
            // 
            // CcbAcNo2
            // 
            this.CcbAcNo2.Location = new System.Drawing.Point(126, 88);
            this.CcbAcNo2.Name = "CcbAcNo2";
            this.CcbAcNo2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbAcNo2.Properties.DropDownRows = 30;
            this.CcbAcNo2.Size = new System.Drawing.Size(284, 20);
            this.CcbAcNo2.TabIndex = 49;
            this.CcbAcNo2.EditValueChanged += new System.EventHandler(this.CcbAcNo_EditValueChanged);
            // 
            // CcbProfitCenterCode
            // 
            this.CcbProfitCenterCode.Location = new System.Drawing.Point(126, 109);
            this.CcbProfitCenterCode.Name = "CcbProfitCenterCode";
            this.CcbProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode.Size = new System.Drawing.Size(284, 20);
            this.CcbProfitCenterCode.TabIndex = 52;
            this.CcbProfitCenterCode.EditValueChanged += new System.EventHandler(this.CcbProfitCenterCode_EditValueChanged);
            // 
            // LblMultiProfitCenterCode
            // 
            this.LblMultiProfitCenterCode.AutoSize = true;
            this.LblMultiProfitCenterCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMultiProfitCenterCode.ForeColor = System.Drawing.Color.Black;
            this.LblMultiProfitCenterCode.Location = new System.Drawing.Point(45, 112);
            this.LblMultiProfitCenterCode.Name = "LblMultiProfitCenterCode";
            this.LblMultiProfitCenterCode.Size = new System.Drawing.Size(77, 14);
            this.LblMultiProfitCenterCode.TabIndex = 51;
            this.LblMultiProfitCenterCode.Tag = "";
            this.LblMultiProfitCenterCode.Text = "Profit Center";
            // 
            // TxtAlias
            // 
            this.TxtAlias.EnterMoveNextControl = true;
            this.TxtAlias.Location = new System.Drawing.Point(126, 46);
            this.TxtAlias.Name = "TxtAlias";
            this.TxtAlias.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAlias.Properties.Appearance.Options.UseFont = true;
            this.TxtAlias.Properties.MaxLength = 30;
            this.TxtAlias.Size = new System.Drawing.Size(284, 20);
            this.TxtAlias.TabIndex = 43;
            this.TxtAlias.Validated += new System.EventHandler(this.TxtAlias_Validated);
            // 
            // ChkAlias
            // 
            this.ChkAlias.Location = new System.Drawing.Point(412, 45);
            this.ChkAlias.Name = "ChkAlias";
            this.ChkAlias.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAlias.Properties.Appearance.Options.UseFont = true;
            this.ChkAlias.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAlias.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAlias.Properties.Caption = " ";
            this.ChkAlias.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAlias.Size = new System.Drawing.Size(19, 22);
            this.ChkAlias.TabIndex = 44;
            this.ChkAlias.ToolTip = "Remove filter";
            this.ChkAlias.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAlias.ToolTipTitle = "Run System";
            this.ChkAlias.CheckedChanged += new System.EventHandler(this.ChkAlias_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(92, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 14);
            this.label7.TabIndex = 42;
            this.label7.Text = "Alias";
            // 
            // ChkEqualizeJournal
            // 
            this.ChkEqualizeJournal.Location = new System.Drawing.Point(208, 110);
            this.ChkEqualizeJournal.Name = "ChkEqualizeJournal";
            this.ChkEqualizeJournal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEqualizeJournal.Properties.Appearance.Options.UseFont = true;
            this.ChkEqualizeJournal.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkEqualizeJournal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkEqualizeJournal.Properties.Caption = " Equalize Journal";
            this.ChkEqualizeJournal.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEqualizeJournal.Size = new System.Drawing.Size(115, 22);
            this.ChkEqualizeJournal.TabIndex = 21;
            this.ChkEqualizeJournal.ToolTip = "Remove filter";
            this.ChkEqualizeJournal.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkEqualizeJournal.ToolTipTitle = "Run System";
            // 
            // CcbEntCode
            // 
            this.CcbEntCode.Location = new System.Drawing.Point(84, 47);
            this.CcbEntCode.Name = "CcbEntCode";
            this.CcbEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbEntCode.Properties.DropDownRows = 30;
            this.CcbEntCode.Size = new System.Drawing.Size(219, 20);
            this.CcbEntCode.TabIndex = 16;
            this.CcbEntCode.EditValueChanged += new System.EventHandler(this.CcbEntCode_EditValueChanged);
            // 
            // ChkEntCode
            // 
            this.ChkEntCode.Location = new System.Drawing.Point(305, 46);
            this.ChkEntCode.Name = "ChkEntCode";
            this.ChkEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEntCode.Properties.Appearance.Options.UseFont = true;
            this.ChkEntCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkEntCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkEntCode.Properties.Caption = " ";
            this.ChkEntCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEntCode.Size = new System.Drawing.Size(18, 22);
            this.ChkEntCode.TabIndex = 17;
            this.ChkEntCode.ToolTip = "Remove filter";
            this.ChkEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkEntCode.ToolTipTitle = "Run System";
            this.ChkEntCode.CheckedChanged += new System.EventHandler(this.ChkEntCode_CheckedChanged);
            // 
            // LblPeriod
            // 
            this.LblPeriod.AutoSize = true;
            this.LblPeriod.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPeriod.Location = new System.Drawing.Point(40, 72);
            this.LblPeriod.Name = "LblPeriod";
            this.LblPeriod.Size = new System.Drawing.Size(41, 14);
            this.LblPeriod.TabIndex = 18;
            this.LblPeriod.Text = "Period";
            // 
            // TxtPeriod
            // 
            this.TxtPeriod.EnterMoveNextControl = true;
            this.TxtPeriod.Location = new System.Drawing.Point(84, 69);
            this.TxtPeriod.Name = "TxtPeriod";
            this.TxtPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPeriod.Properties.Appearance.Options.UseFont = true;
            this.TxtPeriod.Properties.MaxLength = 250;
            this.TxtPeriod.Size = new System.Drawing.Size(219, 20);
            this.TxtPeriod.TabIndex = 19;
            this.TxtPeriod.Validated += new System.EventHandler(this.TxtPeriod_Validated);
            // 
            // ChkPeriod
            // 
            this.ChkPeriod.Location = new System.Drawing.Point(305, 67);
            this.ChkPeriod.Name = "ChkPeriod";
            this.ChkPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPeriod.Properties.Appearance.Options.UseFont = true;
            this.ChkPeriod.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPeriod.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPeriod.Properties.Caption = " ";
            this.ChkPeriod.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPeriod.Size = new System.Drawing.Size(18, 22);
            this.ChkPeriod.TabIndex = 20;
            this.ChkPeriod.ToolTip = "Remove filter";
            this.ChkPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPeriod.ToolTipTitle = "Run System";
            this.ChkPeriod.CheckedChanged += new System.EventHandler(this.ChkPeriod_CheckedChanged);
            // 
            // ChkCCParentJournal
            // 
            this.ChkCCParentJournal.Location = new System.Drawing.Point(305, 88);
            this.ChkCCParentJournal.Name = "ChkCCParentJournal";
            this.ChkCCParentJournal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCCParentJournal.Properties.Appearance.Options.UseFont = true;
            this.ChkCCParentJournal.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCCParentJournal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCCParentJournal.Properties.Caption = " ";
            this.ChkCCParentJournal.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCCParentJournal.Size = new System.Drawing.Size(18, 22);
            this.ChkCCParentJournal.TabIndex = 23;
            this.ChkCCParentJournal.ToolTip = "Remove filter";
            this.ChkCCParentJournal.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCCParentJournal.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(81, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(206, 14);
            this.label8.TabIndex = 24;
            this.label8.Text = "Include Cost Center Parent\'s Journal";
            // 
            // FrmRptGL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptGL";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo2.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkAcNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAlias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAlias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEqualizeJournal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCCParentJournal.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtAcNo;
        private DevExpress.XtraEditors.CheckEdit ChkAcNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkDocNo;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtAcDesc;
        private DevExpress.XtraEditors.CheckEdit ChkAcDesc;
        private DevExpress.XtraEditors.CheckEdit ChkAcNo2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblEntCode;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.TextEdit TxtAlias;
        private DevExpress.XtraEditors.CheckEdit ChkAlias;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.CheckEdit ChkEqualizeJournal;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbEntCode;
        private DevExpress.XtraEditors.CheckEdit ChkEntCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbAcNo2;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode;
        private System.Windows.Forms.Label LblMultiProfitCenterCode;
        private System.Windows.Forms.Label LblPeriod;
        private DevExpress.XtraEditors.TextEdit TxtPeriod;
        private DevExpress.XtraEditors.CheckEdit ChkPeriod;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.TextEdit TxtAcNo14;
        private DevExpress.XtraEditors.TextEdit TxtAcNo1;
        private DevExpress.XtraEditors.TextEdit TxtAcNo2;
        private DevExpress.XtraEditors.TextEdit TxtAcNo7;
        private DevExpress.XtraEditors.TextEdit TxtAcNo13;
        private DevExpress.XtraEditors.TextEdit TxtAcNo8;
        private DevExpress.XtraEditors.TextEdit TxtAcNo6;
        private DevExpress.XtraEditors.TextEdit TxtAcNo12;
        private DevExpress.XtraEditors.TextEdit TxtAcNo9;
        private DevExpress.XtraEditors.TextEdit TxtAcNo3;
        private DevExpress.XtraEditors.TextEdit TxtAcNo5;
        private DevExpress.XtraEditors.TextEdit TxtAcNo11;
        private DevExpress.XtraEditors.TextEdit TxtAcNo10;
        private DevExpress.XtraEditors.TextEdit TxtAcNo4;
        private DevExpress.XtraEditors.CheckEdit ChkAcNo3;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.CheckEdit ChkCCParentJournal;
    }
}