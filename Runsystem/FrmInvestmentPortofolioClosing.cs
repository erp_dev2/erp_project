﻿#region Update
/*
    10/05/2022 [IBL/PRODUCT] New Apps
    13/05/2022 [IBL/PRODUCT] Kolom Investment Code berasal dari Investment Code di menu Investment Item Equity
    19/05/2022 [IBL/PRODUCT] penyesuaian besaran panel header dan panel detail
    10/06/2022 [IBL/PRODUCT] data yg qty 0 tidak dimunculkan
                             kolom Market Value agar dapat langsung terkalkulasi sebelum diSave
    27/06/2022 [SET/Product] insert data pada tab Debt Securities
    08/07/2022 [SET/PRODUCT] data tidak muncul, merubah Qty -> NominalAmt di ShowDebtSecurities()
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentPortofolioClosing : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string
            mEquityInvestmentCtCode = string.Empty,
            mDebtInvestmentCtCode = string.Empty;

        internal FrmInvestmentPortofolioClosingFind FrmFind;

        #endregion

        #region Constructor

        public FrmInvestmentPortofolioClosing(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Investment Portofolio Closing";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                ChkCancelInd.Checked = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "No",

                    //1-5
                    "Investment's Name",
                    "Investment's"+Environment.NewLine+"Equity Code",
                    "Investment's Bank" + Environment.NewLine + "Account Code",
                    "Investment's Bank" + Environment.NewLine + "Account (RDN)#",
                    "Investment's Bank" + Environment.NewLine + "Account Name",

                    //6-10
                    "Issuer",
                    "Investment Category" + Environment.NewLine + "Code",
                    "Category",
                    "InvestmentType",
                    "Type",

                    //11-15
                    "Quantity",
                    "UoM",
                    "Moving Average" + Environment.NewLine + "Price",
                    "Moving Average" + Environment.NewLine + "Cost",
                    "Market Price",

                    //16-18
                    "Market Value",
                    "Unrealized"+Environment.NewLine+"Gain/Loss",
                    "Investment's Code"
                },
                new int[]
                {
                    //0
                    50,
 
                    //1-5
                    200, 120, 100, 200, 250,

                    //6-10
                    250, 50, 80, 120, 200,

                    //11-15
                    130, 150, 130, 130, 130,

                    //16-18
                    130, 130, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 14, 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 7, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Grd1.Cols[18].Move(2);
            #endregion

            #region Grid 2
            Grd2.Cols.Count = 25;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "No",

                    //1-5
                    "Investment's Name",
                    "Investment's" + Environment.NewLine + "Debt Code",
                    "Investment's Bank" + Environment.NewLine + "Account Code",
                    "Investment's Bank" + Environment.NewLine + "Account (RDN)#",
                    "Investment's Bank" + Environment.NewLine + "Account Name",

                    //6-10
                    "Issuer",
                    "Investment Category" + Environment.NewLine + "Code",
                    "Category",
                    "InvestmentType",
                    "Type",

                    //11-15
                    "Nominal Amount",
                    "Moving Average" + Environment.NewLine + "Price",
                    "Moving Average" + Environment.NewLine + "Cost",
                    "Market Price",
                    "Market Value",

                    //16-20                    
                    "Unrealized"+Environment.NewLine+"Gain/Loss",
                    "Maturity"+Environment.NewLine+"Date",
                    "Interest"+Environment.NewLine+"Rate",
                    "Interest Frequency",
                    "Interest Type",

                    //21-24
                    "Last Coupon"+Environment.NewLine+"Date",
                    "Next Coupon"+Environment.NewLine+"Date",
                    "Coupon Interest"+Environment.NewLine+"Receivable",
                    "Investment's Code",
                },
                new int[]
                {
                    //0
                    50,
 
                    //1-5
                    200, 120, 100, 200, 250,

                    //6-10
                    250, 50, 80, 120, 200,

                    //11-15
                    130, 130, 130, 130, 130,

                    //16-20
                    130, 85, 85, 120, 120,

                    //21-23
                    85, 85, 130, 100
                }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 17, 21, 22 });
            Sm.GrdFormatDec(Grd2, new int[] { 11, 12, 13, 14, 15, 16, 18, 23 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 3, 7, 9, 24 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, ChkCancelInd, DteDocDt, TxtInterestRecv, TxtUnrealizedGL, DteClosingDt
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
                    BtnCalculate.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, DteClosingDt }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 15 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 14 });
                    ChkCancelInd.Checked = false;
                    BtnCalculate.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDocNo, ChkCancelInd, DteDocDt, DteClosingDt });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtInterestRecv, TxtUnrealizedGL }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 13, 14, 15, 16, 17 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 11, 12, 13, 14, 15, 16, 18, 23 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInvestmentPortofolioClosingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteClosingDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "InvestmentPortofolioClosing", "TblInvestmentPortofolioClosingHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveInvestmentPortofolioClosingHdr(DocNo));
            cml.Add(SaveInvestmentPortofolioClosingDtl(DocNo));
            cml.Add(SaveInvestmentPortofolioClosingDtl2(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteClosingDt, "Closing Date") ||
                IsClosingDtAlreadyExists() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsClosingDtAlreadyExists()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblInvestmentPortofolioClosingHdr ");
            SQL.AppendLine("Where ClosingDt = @Param ");
            SQL.AppendLine("And CancelInd = 'N'; ");

            if (Sm.IsDataExist(SQL.ToString(), Sm.Left(Sm.GetDte(DteClosingDt), 8)))
            {
                Sm.StdMsg(mMsgType.Warning, "This closing date ("+ DteClosingDt.Text + ") is already used in other documents.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 100000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (99.999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveInvestmentPortofolioClosingHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            cm.CommandText =
                    "Insert Into TblInvestmentPortofolioClosingHdr(DocNo, DocDt, CancelInd, ClosingDt, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @ClosingDt, @UserCode, CurrentDateTime()); ";

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveInvestmentPortofolioClosingDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/*Investment Portofolio Closing - Dtl*/");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblInvestmentPortofolioClosingDtl(DocNo, DNo, InvestmentCode, BankAcCode, InvestmentType, ");
                        SQL.AppendLine("Qty, MarketPrice, MovingAvgPrice, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() + ", @InvestmentCode_" + r.ToString() + ", @BankAcCode_" + r.ToString() + ", @InvestmentType_" + r.ToString() +
                        ", @Qty_" + r.ToString() + ", @MarketPrice_" + r.ToString() + ", @MovingAvgPrice_" + r.ToString() + ", @UserCode, @Dt) ");
                }

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@InvestmentCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                Sm.CmParam<String>(ref cm, "@InvestmentType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                Sm.CmParam<Decimal>(ref cm, "@MovingAvgPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 13));
                Sm.CmParam<Decimal>(ref cm, "@MarketPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
        
        private MySqlCommand SaveInvestmentPortofolioClosingDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/*Investment Portofolio Closing - Dtl2*/");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblInvestmentPortofolioClosingDtl2(DocNo, DNo, InvestmentCode, BankAcCode, InvestmentType, ");
                        SQL.AppendLine("NominalAmt, MovingAvgPrice, MarketPrice, LastCouponDt, NextCouponDt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() + ", @InvestmentCode_" + r.ToString() + ", @BankAcCode_" + r.ToString() + ", @InvestmentType_" + r.ToString() +
                        ", @NominalAmt_" + r.ToString() + ", @MovingAvgPrice_" + r.ToString() + ", @MarketPrice_" + r.ToString() + ", @LastCouponDt_" + r.ToString() + 
                        ", @NextCouponDt_" + r.ToString() + ", @UserCode, @Dt) ");
                }

                Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@InvestmentCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 24));
                Sm.CmParam<String>(ref cm, "@BankAcCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 3));
                Sm.CmParam<String>(ref cm, "@InvestmentType_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 9));
                Sm.CmParam<Decimal>(ref cm, "@NominalAmt_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 11));
                Sm.CmParam<Decimal>(ref cm, "@MovingAvgPrice_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 12));
                Sm.CmParam<Decimal>(ref cm, "@MarketPrice_" + r.ToString(), Sm.GetGrdDec(Grd2, r, 14));
                Sm.CmParamDt(ref cm, "@LastCouponDt_" + r.ToString(), Sm.GetGrdDate(Grd2, r, 21));
                Sm.CmParamDt(ref cm, "@NextCouponDt_" + r.ToString(), Sm.GetGrdDate(Grd2, r, 22));
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditInvestmentPortofolioClosingHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsCancelIndNotTrue()
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblInvestmentPortofolioClosingHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndNotTrue()
        {
            if (ChkCancelInd.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditInvestmentPortofolioClosingHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblInvestmentPortofolioClosingHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        internal void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowInvestmentPortofolioClosingHdr(DocNo);
                ShowInvestmentPortofolioClosingDtl(DocNo);
                ShowInvestmentPortofolioClosingDtl2(DocNo);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowInvestmentPortofolioClosingHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm,
                "Select DocNo, DocDt, CancelInd, ClosingDt From TblInvestmentPortofolioClosingHdr Where DocNo=@DocNo;",
                new string[]
                {
                    //0
                    "DocNo", 
                    
                    //1-3
                    "DocDt", "CancelInd", "ClosingDt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    Sm.SetDte(DteClosingDt, Sm.DrStr(dr, c[3]));
                }, true
            );
        }

        private void ShowInvestmentPortofolioClosingDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.PortofolioName, B.InvestmentEquityCode, C.BankAcCode, C.BankAcNo, C.BankAcNm, D.Issuer, E.InvestmentCtCode, E.InvestmentCtName, ");
            SQL.AppendLine("F.OptCode, F.OptDesc, IfNull(A.Qty, 0.00) As Qty, B.UomCode, IfNull(A.MovingAvgPrice, 0.00) As MovingAvgPrice, IfNull(A.Qty, 0.00) * IfNull(A.MovingAvgPrice, 0.00) As MovingAvgCost, ");
            SQL.AppendLine("IfNull(A.MarketPrice, 0.00) As MarketPrice, IfNull(A.Qty, 0.00) * IfNull(A.MarketPrice, 0.00) As MarketValue, ");
            SQL.AppendLine("(IfNull(A.Qty, 0.00) * IfNull(A.MarketPrice, 0.00)) - (IfNull(A.Qty, 0.00) * IfNull(A.MovingAvgPrice, 0.00)) As UnrealizedGL, B.PortofolioId As InvestmentCode ");
            SQL.AppendLine("From TblInvestmentPortofolioClosingDtl A ");
            SQL.AppendLine("Inner Join TblInvestmentItemEquity B On A.InvestmentCode = B.InvestmentEquityCode ");
            SQL.AppendLine("Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio D On B.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentCategory E On D.InvestmentCtCode = E.InvestmentCtCode ");
            SQL.AppendLine("Inner Join TblOption F On F.OptCat = 'InvestmentType' And A.InvestmentType = F.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "PortofolioName", 

                    //1-5
                    "InvestmentEquityCode", "BankAcCode", "BankAcNo", "BankAcNm", "Issuer",
                    
                    //6-10
                    "InvestmentCtCode", "InvestmentCtName", "OptCode", "OptDesc", "Qty",

                    //11-15
                    "UomCode", "MovingAvgPrice", "MovingAvgCost", "MarketPrice", "MarketValue",

                    //17
                    "UnrealizedGL", "InvestmentCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);

                }, false, false, true, false
            );
            ComputeInterestRecvAmt();
            ComputeUnrealizedGL();
            Sm.FocusGrd(Grd1, 0, 1);
        }
        
        private void ShowInvestmentPortofolioClosingDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select D.PortofolioName, B.InvestmentDebtCode, C.BankAcCode, C.BankAcNo, C.BankAcNm, D.Issuer, E.InvestmentCtCode, E.InvestmentCtName, ");
            SQL.AppendLine("F.OptCode, F.OptDesc, IfNull(A.NominalAmt, 0.00) As NominalAmt, B.UomCode, IfNull(A.MovingAvgPrice, 0.00) As MovingAvgPrice, IfNull(A.NominalAmt, 0.00) * IfNull(A.MovingAvgPrice, 0.00) As MovingAvgCost, ");
            SQL.AppendLine("IfNull(A.MarketPrice, 0.00) As MarketPrice, IfNull(A.NominalAmt, 0.00) * IfNull(A.MarketPrice, 0.00) As MarketValue, ");
            SQL.AppendLine("(IfNull(A.NominalAmt, 0.00) * IfNull(A.MarketPrice, 0.00)) - (IfNull(A.NominalAmt, 0.00) * IfNull(A.MovingAvgPrice, 0.00)) As UnrealizedGL, B.MaturityDt, ");
            SQL.AppendLine("IFNULL(B.InterestRateAmt, 0.00) InterestRate, B.InterestFreq, B.InterestType, B.LastCouponDt, B.NextCouponDt, ");
            SQL.AppendLine("ABS(DATEDIFF(G.ClosingDt, A.LastCouponDt)) * IFNULL(B.InterestRateAmt, 0.00) / 360 * IFNULL(A.NominalAmt, 0.00) AS CouponInterestRecv, B.PortofolioId As InvestmentCode ");
            SQL.AppendLine("From TblInvestmentPortofolioClosingDtl2 A ");
            SQL.AppendLine("Inner Join TblInvestmentItemDebt B On A.InvestmentCode = B.InvestmentDebtCode ");
            SQL.AppendLine("Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio D On B.PortofolioId = D.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentCategory E On D.InvestmentCtCode = E.InvestmentCtCode ");
            SQL.AppendLine("Inner Join TblOption F On F.OptCat = 'InvestmentType' And A.InvestmentType = F.OptCode ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolioclosinghdr G ON A.DocNo = G.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "PortofolioName", 

                    //1-5
                    "InvestmentDebtCode", "BankAcCode", "BankAcNo", "BankAcNm", "Issuer",
                    
                    //6-10
                    "InvestmentCtCode", "InvestmentCtName", "OptCode", "OptDesc", "NominalAmt",

                    //11-15
                    "UomCode", "MovingAvgPrice", "MovingAvgCost", "MarketPrice", "MarketValue",

                    //16-20
                    "UnrealizedGL", "MaturityDt", "InterestRate", "InterestFreq", "InterestType", 

                    //21-24
                    "LastCouponDt", "NextCouponDt", "CouponInterestRecv", "InvestmentCode"
                },
                (MySqlDataReader dr, iGrid Grd2, int[] c, int Row) =>
                {
                    Grd2.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("D", Grd2, dr, c, Row, 17, 17);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 18, 18);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 19, 19);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 20, 20);
                    Sm.SetGrdValue("D", Grd2, dr, c, Row, 21, 21);
                    Sm.SetGrdValue("D", Grd2, dr, c, Row, 22, 22);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 23, 23);

                }, false, false, true, false
            );
            //ComputeInterestRecvAmt();
            ComputeUnrealizedGL();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'EquityInvestmentCtCode', 'DebtInvestmentCtCode' ");
            SQL.AppendLine(" );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //String
                            case "EquityInvestmentCtCode": mEquityInvestmentCtCode = ParValue; break;
                            case "DebtInvestmentCtCode": mDebtInvestmentCtCode = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ShowEquitySecuritiesData()
        {
            if (Sm.IsDteEmpty(DteClosingDt, "Closing Date")) return;
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));
                Sm.CmParam<String>(ref cm, "@EquityInvestmentCtCode", mEquityInvestmentCtCode);

                SQL.AppendLine("Select T1.PortofolioName, T1.InvestmentCode, T1.BankAcCode, T1.BankAcNo, T1.BankAcNm, T1.Issuer, T1.InvestmentCtCode, T1.InvestmentCtName, ");
                SQL.AppendLine("T1.OptCode, T1.InvestmentTypeNm, T1.Qty, T1.UomCode, T2.MovingAvgPrice, T1.PortofolioId ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("	Select D.PortofolioId, D.PortofolioName, A.InvestmentCode, F.BankAcNo, F.BankAcNm, D.Issuer, E.InvestmentCtCode, E.InvestmentCtName, ");
                SQL.AppendLine("	G.OptCode, G.OptDesc As InvestmentTypeNm, Sum(IfNull(A.Qty, 0.00)) As Qty, C.UomCode, B.InvestmentType, F.BankAcCode ");
                SQL.AppendLine("	From TblInvestmentStockSummary A ");
                SQL.AppendLine("	Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
                SQL.AppendLine("		And A.InvestmentCode = B.InvestmentCode ");
                SQL.AppendLine("	Inner Join TblInvestmentItemEquity C On A.InvestmentCode = C.InvestmentEquityCode ");
                SQL.AppendLine("	Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
                SQL.AppendLine("		And D.InvestmentCtCode = @EquityInvestmentCtCode ");
                SQL.AppendLine("	Inner Join TblInvestmentCategory E On D.InvestmentCtCode = E.InvestmentCtCode ");
                SQL.AppendLine("	Inner Join TblBankAccount F On A.BankAcCode = F.BankAcCode ");
                SQL.AppendLine("	Inner Join TblOption G On G.OptCat = 'InvestmentType' And B.InvestmentType = G.OptCode ");
                SQL.AppendLine("	Inner Join TblInvestmentStockPrice H On A.Source = H.Source ");
                SQL.AppendLine("	Where B.DocDt <= @ClosingDt ");
                SQL.AppendLine("	Group By D.PortofolioId, D.InvestmentCtCode, B.InvestmentType, F.BankAcCode ");
                SQL.AppendLine("    Having Sum(A.Qty) <> 0 ");
                SQL.AppendLine(")T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("	Select A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType, ");
                SQL.AppendLine("	(Sum((IfNull(A.Qty, 0.00) * IfNull(F.UPrice, 0.00)))) / (Sum(IfNull(A.Qty, 0.00))) As MovingAvgPrice ");
                SQL.AppendLine("	From TblInvestmentStockSummary A ");
                SQL.AppendLine("	Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
                SQL.AppendLine("		And A.InvestmentCode = B.InvestmentCode ");
                SQL.AppendLine("	Inner Join TblInvestmentItemEquity C On A.InvestmentCode = C.InvestmentEquityCode ");
                SQL.AppendLine("	Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
                SQL.AppendLine("		And D.InvestmentCtCode = @EquityInvestmentCtCode ");
                SQL.AppendLine("		Inner Join TblOption E On E.OptCat = 'InvestmentType' And B.InvestmentType = E.OptCode ");
                SQL.AppendLine("	Inner Join TblInvestmentStockPrice F On A.Source = F.Source ");
                SQL.AppendLine("	Where B.DocDt <= @ClosingDt ");
                SQL.AppendLine("	Group By D.PortofolioId, D.InvestmentCtCode, B.InvestmentType ");
                SQL.AppendLine(")T2 On T1.InvestmentCode = T2.InvestmentCode ");
                SQL.AppendLine("And T1.InvestmentCtCode = T2.InvestmentCtCode ");
                SQL.AppendLine("And T1.InvestmentType = T2.InvestmentType ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "PortofolioName",
                        //1-5
                        "InvestmentCode", "BankAcCode", "BankAcNo", "BankAcNm", "Issuer",
                        //6-10
                        "InvestmentCtCode", "InvestmentCtName", "OptCode", "InvestmentTypeNm", "Qty",
                        //11-13
                        "UomCode", "MovingAvgPrice", "PortofolioId"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                        Grd.Cells[Row, 14].Value = Sm.DrDec(dr, c[10]) * Sm.DrDec(dr, c[12]);
                        Grd.Cells[Row, 15].Value = 0m;
                        Grd.Cells[Row, 16].Value = 0m;
                        Grd.Cells[Row, 17].Value = 0m;
                    }, true, false, false, false
                );
                ComputeInterestRecvAmt();
                ComputeUnrealizedGL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDebtSecuritiesData()
        {
            if (Sm.IsDteEmpty(DteClosingDt, "Closing Date")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@ClosingDt", Sm.GetDte(DteClosingDt));
                Sm.CmParam<String>(ref cm, "@DebtInvestmentCtCode", mDebtInvestmentCtCode);

                SQL.AppendLine("Select T1.PortofolioName, T1.InvestmentCode, T1.BankAcCode, T1.BankAcNo, T1.BankAcNm, T1.Issuer, T1.InvestmentCtCode, T1.InvestmentCtName, ");
                SQL.AppendLine("T1.OptCode, T1.InvestmentTypeNm, T1.NominalAmt, T1.UomCode, T2.MovingAvgPrice, T1.NominalAmt*T2.MovingAvgPrice AS MovingAvgCost, T1.PortofolioId, ");
                SQL.AppendLine("T1.MaturityDt, T1.InterestRate, T1.InterestFreq, T1.InterestType, T1.LastCouponDt, T1.NextCouponDt ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("	Select D.PortofolioId, D.PortofolioName, A.InvestmentCode, F.BankAcNo, F.BankAcNm, D.Issuer, E.InvestmentCtCode, E.InvestmentCtName, ");
                SQL.AppendLine("	G.OptCode, G.OptDesc As InvestmentTypeNm, Sum(IfNull(A.NominalAmt, 0.00)) As NominalAmt, C.UomCode, B.InvestmentType, F.BankAcCode, C.MaturityDt, ");
                SQL.AppendLine("	C.InterestRateAmt InterestRate, C.InterestFreq, C.InterestType, C.LastCouponDt, C.NextCouponDt ");
                SQL.AppendLine("	From TblInvestmentStockSummary A ");
                SQL.AppendLine("	Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
                SQL.AppendLine("		And A.InvestmentCode = B.InvestmentCode ");
                SQL.AppendLine("	Inner Join TblInvestmentItemDebt C On A.InvestmentCode = C.InvestmentDebtCode ");
                SQL.AppendLine("	Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
                SQL.AppendLine("		And FIND_IN_SET(D.InvestmentCtCode, @DebtInvestmentCtCode) ");
                SQL.AppendLine("	Inner Join TblInvestmentCategory E On D.InvestmentCtCode = E.InvestmentCtCode ");
                SQL.AppendLine("	Inner Join TblBankAccount F On A.BankAcCode = F.BankAcCode ");
                SQL.AppendLine("	Inner Join TblOption G On G.OptCat = 'InvestmentType' And B.InvestmentType = G.OptCode ");
                SQL.AppendLine("	Inner Join TblInvestmentStockPrice H On A.Source = H.Source ");
                SQL.AppendLine("	Where B.DocDt <= @ClosingDt ");
                SQL.AppendLine("	Group By D.PortofolioId, D.InvestmentCtCode, B.InvestmentType, F.BankAcCode ");
                SQL.AppendLine("    Having Sum(A.NominalAmt) <> 0 ");
                SQL.AppendLine(")T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("	Select A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType, ");
                SQL.AppendLine("	(Sum((IfNull(A.NominalAmt, 0.00) * IfNull(F.UPrice, 0.00)))) / (Sum(IfNull(A.NominalAmt, 0.00))) As MovingAvgPrice ");
                SQL.AppendLine("	From TblInvestmentStockSummary A ");
                SQL.AppendLine("	Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
                SQL.AppendLine("		And A.InvestmentCode = B.InvestmentCode ");
                SQL.AppendLine("	Inner Join TblInvestmentItemDebt C On A.InvestmentCode = C.InvestmentDebtCode ");
                SQL.AppendLine("	Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
                SQL.AppendLine("		And FIND_IN_SET(D.InvestmentCtCode, @DebtInvestmentCtCode) ");
                SQL.AppendLine("		Inner Join TblOption E On E.OptCat = 'InvestmentType' And B.InvestmentType = E.OptCode ");
                SQL.AppendLine("	Inner Join TblInvestmentStockPrice F On A.Source = F.Source ");
                SQL.AppendLine("	Where B.DocDt <= @ClosingDt ");
                SQL.AppendLine("	Group By D.PortofolioId, D.InvestmentCtCode, B.InvestmentType ");
                SQL.AppendLine(")T2 On T1.InvestmentCode = T2.InvestmentCode ");
                SQL.AppendLine("And T1.InvestmentCtCode = T2.InvestmentCtCode ");
                SQL.AppendLine("And T1.InvestmentType = T2.InvestmentType ");

                Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "PortofolioName",
                        //1-5
                        "InvestmentCode", "BankAcCode", "BankAcNo", "BankAcNm", "Issuer",
                        //6-10
                        "InvestmentCtCode", "InvestmentCtName", "OptCode", "InvestmentTypeNm", "NominalAmt",
                        //11-15
                        "UomCode", "MovingAvgPrice", "MovingAvgCost", "PortofolioId", "MaturityDt", 
                        //16-20
                        "InterestRate", "InterestFreq", "InterestType", "LastCouponDt", "NextCouponDt"
                    },
                    (MySqlDataReader dr, iGrid Grd2, int[] c, int Row) =>
                    {
                        Grd2.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 14);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd2, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd2, dr, c, Row, 12, 12);
                        //Sm.SetGrdValue("N", Grd2, dr, c, Row, 13, 12);
                        Grd2.Cells[Row, 13].Value = Sm.DrDec(dr, c[10]) * Sm.DrDec(dr, c[12]);
                        Grd2.Cells[Row, 14].Value = 0m;
                        Grd2.Cells[Row, 15].Value = 0m;
                        Grd2.Cells[Row, 16].Value = 0m;
                        Sm.SetGrdValue("D", Grd2, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd2, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("D", Grd2, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("D", Grd2, dr, c, Row, 22, 20);
                        Grd2.Cells[Row, 23].Value = 0m;
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 24, 1);
                        //Grd2.Cells[Row, 14].Value = Sm.DrDec(dr, c[10]) * Sm.DrDec(dr, c[12]);
                        //Grd2.Cells[Row, 15].Value = 0m;
                        //Grd2.Cells[Row, 16].Value = 0m;
                        //Grd2.Cells[Row, 17].Value = 0m;
                    }, true, false, false, false
                );
                //ComputeInterestRecvAmt();
                ComputeUnrealizedGL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd2, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ComputeInterestRecvAmt()
        {

        }

        private void ComputeUnrealizedGL()
        {
            decimal mUnrealizedGLEquity = 0m, mUnrealizedGLDebt = 0m;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                Grd1.Cells[i, 17].Value = Sm.GetGrdDec(Grd1, i, 16) - Sm.GetGrdDec(Grd1, i, 14);
                mUnrealizedGLEquity += Sm.GetGrdDec(Grd1, i, 17);
            }
            for (int i = 0; i < Grd2.Rows.Count; i++)
            {
                //Grd2.Cells[i, 17].Value = Sm.GetGrdDec(Grd1, i, 16) - Sm.GetGrdDec(Grd1, i, 14);
                mUnrealizedGLDebt += Sm.GetGrdDec(Grd2, i, 16);
            }

            TxtUnrealizedGL.EditValue = Sm.FormatNum(mUnrealizedGLEquity + mUnrealizedGLDebt, 0);
        }

        private void ComputeCouponInterestRecv(int row)
        {
            decimal CouponInterestRecv = 0m;

            DateTime ClosingDt = DteClosingDt.DateTime;
            string a = Sm.GetGrdStr(Grd2, row, 21);
            DateTime LastCouponDt = Convert.ToDateTime(a);
            TimeSpan diff = ClosingDt - LastCouponDt;
            decimal days = Convert.ToDecimal(diff.TotalDays);

            CouponInterestRecv = Math.Abs(days) * Sm.GetGrdDec(Grd2, row, 18) / 360 * Sm.GetGrdDec(Grd2, row, 11);

            Grd2.Cells[row, 23].Value = CouponInterestRecv;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, false);
            Sm.ClearGrd(Grd2, false);
            ShowEquitySecuritiesData();
            ShowDebtSecuritiesData();
        }

        #endregion

        #region Grid Control Event

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 15)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15 }, e);
                Grd1.Cells[e.RowIndex, 16].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 11) * Sm.GetGrdDec(Grd1, e.RowIndex, 15);
                ComputeInterestRecvAmt();
                ComputeUnrealizedGL();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 14)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 14 }, e);
                Grd2.Cells[e.RowIndex, 15].Value = Sm.GetGrdDec(Grd2, e.RowIndex, 11) * Sm.GetGrdDec(Grd2, e.RowIndex, 14);
                Grd2.Cells[e.RowIndex, 16].Value = Sm.GetGrdDec(Grd2, e.RowIndex, 15) - Sm.GetGrdDec(Grd2, e.RowIndex, 13);
                //ComputeInterestRecvAmt();
                ComputeUnrealizedGL();
                ComputeCouponInterestRecv(e.RowIndex);
            }
        }

        private void Grd2_Validated(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion
    }
}