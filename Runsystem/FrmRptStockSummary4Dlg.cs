﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockSummary4Dlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptStockSummary4 mFrmParent;
        private string mWhsCode = string.Empty, mItCode = string.Empty, mDocDt = string.Empty;
        private int mNumberOfInventoryUomCode = 1;

        #endregion

        #region Constructor

        public FrmRptStockSummary4Dlg(
            FrmRptStockSummary4 FrmParent,
            string WhsCode,
            string ItCode,
            string DocDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
            mItCode = ItCode;
            mDocDt = DocDt;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetNumberOfInventoryUomCode();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtWhs, TxtItCode, TxtItName }, true);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BatchNo, A.Qty, A.Qty2, A.Qty3, ");
            SQL.AppendLine("B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3 ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select BatchNo, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockSummary  ");
            SQL.AppendLine("    Where (Qty<>0 Or Qty2<>0 Or Qty3<>0) ");
            SQL.AppendLine("    And WhsCode=@WhsCode ");
            SQL.AppendLine("    And ItCode=@ItCode ");
            SQL.AppendLine("    Group By BatchNo ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblItem B On B.ItCode=@ItCode ");

            return SQL.ToString();
        }

        private string SetSQL2(string DocDt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BatchNo, A.Qty, A.Qty2, A.Qty3, ");
            SQL.AppendLine("B.InventoryUOMCode, B.InventoryUOMCode2, B.InventoryUOMCode3 ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select BatchNo, Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where DocDt<=" + DocDt);
            SQL.AppendLine("    And WhsCode=@WhsCode ");
            SQL.AppendLine("    And ItCode=@ItCode ");
            SQL.AppendLine("    Group By BatchNo ");
            SQL.AppendLine("    Having (Sum(Qty)<>0 Or Sum(Qty2)<>0 Or Sum(Qty3)<>0) ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblItem B On B.ItCode=@ItCode ");

            return SQL.ToString();
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Batch Number",
                        "Quantity", 
                        "UoM",
                        "Quantity 2", 
                        "Uom 2",

                        //6-7
                        "Quantity 3", 
                        "Uom 3"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        250, 80, 80, 80, 80, 
                        
                        //6-7
                        80, 80
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] { 2, 4, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7 }, true);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string
                    Filter = string.Empty,
                    DocDt = mDocDt.Length != 0 ? mDocDt.Substring(0, 8) : "";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@ItCode", mItCode);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                ((mDocDt.Length != 0 && decimal.Parse(mDocDt) < decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) ?
                SetSQL2(DocDt) : SetSQL()) +
                Filter + " Order By BatchNo;",
                new string[]
                    {
                        //0
                        "BatchNo",  
                        
                        //1-5
                        "Qty", "InventoryUomCode", "Qty2", "InventoryUOMCode2", "Qty3", 
                        
                        //6
                        "InventoryUOMCode3"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (Grd1.Rows.Count > 1)
                    Sm.FocusGrd(Grd1, 1, 2);
                else
                    Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

    }
}
