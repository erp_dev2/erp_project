﻿#region Update
// 03/01/2018 [HAR] hapus informasi competence
// 29/03/2018 [HAR] tambah type
// 28/07/2020 [ICA/SIER] Training req dapat menarik keseluruhan master training walaupun sitenya spesifik dengan parameter IsTrainingUseAllSite
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTrainingRequestDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTrainingRequest mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTrainingRequestDlg(FrmTrainingRequest FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueTrainerType(ref LueTrainingType);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Training"+Environment.NewLine+"Code", 
                        "Training"+Environment.NewLine+"Name",
                        "Type",
                    },
                    new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        100, 200, 150
                    }
                );
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TrainingCode, A.TrainingName, B.OptDesc As TrainingType  ");
            SQL.AppendLine("From TblTraining A ");
            SQL.AppendLine("Inner Join TblOption B On A.TrainingType = B.OptCode And B.OptCat = 'TrainerType' ");
            SQL.AppendLine("Where 1=1 ");
            if (mFrmParent.mIsTrainingUseAllSite)
            {
                if (Sm.GetLue(mFrmParent.LueSiteCode).Length > 0)
                    SQL.AppendLine("And A.SiteCode Is Null Or A.SiteCode=@SiteCode Or A.SiteCode='all' ");
            }
            else
            {
                if (Sm.GetLue(mFrmParent.LueSiteCode).Length > 0)
                {
                    SQL.AppendLine("And A.SiteCode Is Not Null And A.SiteCode=@SiteCode ");
                }

                if (mFrmParent.mIsFilterBySiteHR)
                {
                    SQL.AppendLine("    And A.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }
            }
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (Sm.GetLue(mFrmParent.LueSiteCode).Length > 0)
                    Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(mFrmParent.LueSiteCode));
                Sm.FilterStr(ref Filter, ref cm, TxtTrainingName.Text, new string[] { "A.TrainingCode", "A.TrainingName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueTrainingType), "A.TrainingType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.TrainingName;",
                        new string[] 
                        { 
                            "TrainingCode", 
                            "TrainingName",
                            "TrainingType"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, ChkHideInfoInGrd.Checked ? 2 : 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtTrainingCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtTrainingName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtTrainingType.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                this.Close();
            }
        }

        #endregion

        #region Grid Method


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkTrainingName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Training");
        }

        private void TxtTrainingName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        private void ChkTrainingType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueTrainingType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingType, new Sm.RefreshLue1(Sl.SetLueTrainerType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
