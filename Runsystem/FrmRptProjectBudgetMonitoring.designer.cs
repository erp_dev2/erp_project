﻿namespace RunSystem
{
    partial class FrmRptProjectBudgetMonitoring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptProjectBudgetMonitoring));
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.TxtCtName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPONo = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtVCRealizationPercentage = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtVCRealization = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtVCRecvVd = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtVCFinancialBudget = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtVCBidPricingAndQt = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtContractAmt = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtCMRealizationPercentage = new DevExpress.XtraEditors.TextEdit();
            this.TxtCMRecvVdPercentage = new DevExpress.XtraEditors.TextEdit();
            this.TxtCMFinancialBudgetPercentage = new DevExpress.XtraEditors.TextEdit();
            this.TxtCMBidPricingAndQtPercentage = new DevExpress.XtraEditors.TextEdit();
            this.TxtCMBidPricingAndQt = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtCMRealization = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCMRecvVd = new DevExpress.XtraEditors.TextEdit();
            this.TxtCMFinancialBudget = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCRealizationPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCRealization.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCRecvVd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCFinancialBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCBidPricingAndQt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmt.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMRealizationPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMRecvVdPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMFinancialBudgetPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMBidPricingAndQtPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMBidPricingAndQt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMRealization.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMRecvVd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMFinancialBudget.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(857, 0);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(857, 195);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ReadOnly = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(857, 278);
            this.Grd1.TabIndex = 28;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 195);
            this.panel3.Size = new System.Drawing.Size(857, 278);
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(853, 191);
            this.Tc1.TabIndex = 12;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(847, 163);
            this.Tp1.Text = "Filter";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.DteDocDt2);
            this.panel5.Controls.Add(this.DteDocDt1);
            this.panel5.Controls.Add(this.TxtCtName);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.TxtProjectName);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Controls.Add(this.BtnSOContractDocNo);
            this.panel5.Controls.Add(this.TxtPONo);
            this.panel5.Controls.Add(this.label29);
            this.panel5.Controls.Add(this.TxtProjectCode);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.TxtSOContractDocNo);
            this.panel5.Controls.Add(this.label26);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(847, 163);
            this.panel5.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(211, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 14);
            this.label2.TabIndex = 26;
            this.label2.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(66, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "Period";
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(226, 116);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt2.TabIndex = 27;
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(108, 116);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(101, 20);
            this.DteDocDt1.TabIndex = 25;
            // 
            // TxtCtName
            // 
            this.TxtCtName.EnterMoveNextControl = true;
            this.TxtCtName.Location = new System.Drawing.Point(108, 95);
            this.TxtCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtName.Name = "TxtCtName";
            this.TxtCtName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtCtName.Properties.MaxLength = 250;
            this.TxtCtName.Properties.ReadOnly = true;
            this.TxtCtName.Size = new System.Drawing.Size(607, 20);
            this.TxtCtName.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(48, 97);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 14);
            this.label1.TabIndex = 22;
            this.label1.Text = "Customer";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(108, 53);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 250;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(607, 20);
            this.TxtProjectName.TabIndex = 19;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(26, 55);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(81, 14);
            this.label30.TabIndex = 18;
            this.label30.Text = "Project Name";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(331, 12);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(30, 16);
            this.BtnSOContractDocNo.TabIndex = 15;
            this.BtnSOContractDocNo.ToolTip = "Choose SO Contract Document";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo.Click += new System.EventHandler(this.BtnSOContractDocNo_Click);
            // 
            // TxtPONo
            // 
            this.TxtPONo.EnterMoveNextControl = true;
            this.TxtPONo.Location = new System.Drawing.Point(108, 74);
            this.TxtPONo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPONo.Name = "TxtPONo";
            this.TxtPONo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPONo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPONo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPONo.Properties.Appearance.Options.UseFont = true;
            this.TxtPONo.Properties.MaxLength = 80;
            this.TxtPONo.Properties.ReadOnly = true;
            this.TxtPONo.Size = new System.Drawing.Size(219, 20);
            this.TxtPONo.TabIndex = 21;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(19, 76);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 14);
            this.label29.TabIndex = 20;
            this.label29.Text = "Customer PO#";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(108, 32);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 80;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(219, 20);
            this.TxtProjectCode.TabIndex = 17;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(29, 34);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(78, 14);
            this.label28.TabIndex = 16;
            this.label28.Text = "Project Code";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(108, 11);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 80;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(219, 20);
            this.TxtSOContractDocNo.TabIndex = 14;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(24, 14);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 14);
            this.label26.TabIndex = 13;
            this.label26.Text = "SO Contract#";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(847, 163);
            this.Tp2.Text = "Data";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.TxtVCRealizationPercentage);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.TxtVCRealization);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.TxtVCRecvVd);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.TxtVCFinancialBudget);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.TxtVCBidPricingAndQt);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.TxtContractAmt);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(847, 163);
            this.panel6.TabIndex = 12;
            // 
            // TxtVCRealizationPercentage
            // 
            this.TxtVCRealizationPercentage.EnterMoveNextControl = true;
            this.TxtVCRealizationPercentage.Location = new System.Drawing.Point(201, 133);
            this.TxtVCRealizationPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVCRealizationPercentage.Name = "TxtVCRealizationPercentage";
            this.TxtVCRealizationPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVCRealizationPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVCRealizationPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVCRealizationPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtVCRealizationPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtVCRealizationPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtVCRealizationPercentage.Properties.ReadOnly = true;
            this.TxtVCRealizationPercentage.Size = new System.Drawing.Size(180, 20);
            this.TxtVCRealizationPercentage.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(122, 135);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 14);
            this.label17.TabIndex = 24;
            this.label17.Text = "Realization %";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVCRealization
            // 
            this.TxtVCRealization.EnterMoveNextControl = true;
            this.TxtVCRealization.Location = new System.Drawing.Point(201, 112);
            this.TxtVCRealization.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVCRealization.Name = "TxtVCRealization";
            this.TxtVCRealization.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVCRealization.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVCRealization.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVCRealization.Properties.Appearance.Options.UseFont = true;
            this.TxtVCRealization.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtVCRealization.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtVCRealization.Properties.ReadOnly = true;
            this.TxtVCRealization.Size = new System.Drawing.Size(180, 20);
            this.TxtVCRealization.TabIndex = 23;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(138, 114);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 14);
            this.label16.TabIndex = 22;
            this.label16.Text = "Realization";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVCRecvVd
            // 
            this.TxtVCRecvVd.EnterMoveNextControl = true;
            this.TxtVCRecvVd.Location = new System.Drawing.Point(201, 91);
            this.TxtVCRecvVd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVCRecvVd.Name = "TxtVCRecvVd";
            this.TxtVCRecvVd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVCRecvVd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVCRecvVd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVCRecvVd.Properties.Appearance.Options.UseFont = true;
            this.TxtVCRecvVd.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtVCRecvVd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtVCRecvVd.Properties.ReadOnly = true;
            this.TxtVCRecvVd.Size = new System.Drawing.Size(180, 20);
            this.TxtVCRecvVd.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(40, 93);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(161, 14);
            this.label15.TabIndex = 20;
            this.label15.Text = "Receiving Item from Vendor";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVCFinancialBudget
            // 
            this.TxtVCFinancialBudget.EnterMoveNextControl = true;
            this.TxtVCFinancialBudget.Location = new System.Drawing.Point(201, 70);
            this.TxtVCFinancialBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVCFinancialBudget.Name = "TxtVCFinancialBudget";
            this.TxtVCFinancialBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVCFinancialBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVCFinancialBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVCFinancialBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtVCFinancialBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtVCFinancialBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtVCFinancialBudget.Properties.ReadOnly = true;
            this.TxtVCFinancialBudget.Size = new System.Drawing.Size(180, 20);
            this.TxtVCFinancialBudget.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(106, 72);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 14);
            this.label13.TabIndex = 18;
            this.label13.Text = "Financial Budget";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVCBidPricingAndQt
            // 
            this.TxtVCBidPricingAndQt.EnterMoveNextControl = true;
            this.TxtVCBidPricingAndQt.Location = new System.Drawing.Point(201, 49);
            this.TxtVCBidPricingAndQt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVCBidPricingAndQt.Name = "TxtVCBidPricingAndQt";
            this.TxtVCBidPricingAndQt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVCBidPricingAndQt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVCBidPricingAndQt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVCBidPricingAndQt.Properties.Appearance.Options.UseFont = true;
            this.TxtVCBidPricingAndQt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtVCBidPricingAndQt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtVCBidPricingAndQt.Properties.ReadOnly = true;
            this.TxtVCBidPricingAndQt.Size = new System.Drawing.Size(180, 20);
            this.TxtVCBidPricingAndQt.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(54, 51);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(147, 14);
            this.label8.TabIndex = 16;
            this.label8.Text = "Bid And Pricing Quotation";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(10, 33);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 14);
            this.label6.TabIndex = 15;
            this.label6.Text = "Variable Cost";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtContractAmt
            // 
            this.TxtContractAmt.EnterMoveNextControl = true;
            this.TxtContractAmt.Location = new System.Drawing.Point(54, 8);
            this.TxtContractAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtContractAmt.Name = "TxtContractAmt";
            this.TxtContractAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtContractAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtContractAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtContractAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtContractAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtContractAmt.Properties.ReadOnly = true;
            this.TxtContractAmt.Size = new System.Drawing.Size(180, 20);
            this.TxtContractAmt.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(11, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Sales";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.TxtCMRealizationPercentage);
            this.panel4.Controls.Add(this.TxtCMRecvVdPercentage);
            this.panel4.Controls.Add(this.TxtCMFinancialBudgetPercentage);
            this.panel4.Controls.Add(this.TxtCMBidPricingAndQtPercentage);
            this.panel4.Controls.Add(this.TxtCMBidPricingAndQt);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtCMRealization);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.TxtCMRecvVd);
            this.panel4.Controls.Add(this.TxtCMFinancialBudget);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(403, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(444, 163);
            this.panel4.TabIndex = 26;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(419, 114);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 14);
            this.label19.TabIndex = 43;
            this.label19.Text = "%";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(419, 94);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 14);
            this.label18.TabIndex = 39;
            this.label18.Text = "%";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(419, 73);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 14);
            this.label14.TabIndex = 35;
            this.label14.Text = "%";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(419, 51);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 14);
            this.label5.TabIndex = 31;
            this.label5.Text = "%";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCMRealizationPercentage
            // 
            this.TxtCMRealizationPercentage.EnterMoveNextControl = true;
            this.TxtCMRealizationPercentage.Location = new System.Drawing.Point(352, 112);
            this.TxtCMRealizationPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCMRealizationPercentage.Name = "TxtCMRealizationPercentage";
            this.TxtCMRealizationPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCMRealizationPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCMRealizationPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCMRealizationPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtCMRealizationPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCMRealizationPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCMRealizationPercentage.Properties.ReadOnly = true;
            this.TxtCMRealizationPercentage.Size = new System.Drawing.Size(62, 20);
            this.TxtCMRealizationPercentage.TabIndex = 42;
            // 
            // TxtCMRecvVdPercentage
            // 
            this.TxtCMRecvVdPercentage.EnterMoveNextControl = true;
            this.TxtCMRecvVdPercentage.Location = new System.Drawing.Point(352, 91);
            this.TxtCMRecvVdPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCMRecvVdPercentage.Name = "TxtCMRecvVdPercentage";
            this.TxtCMRecvVdPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCMRecvVdPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCMRecvVdPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCMRecvVdPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtCMRecvVdPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCMRecvVdPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCMRecvVdPercentage.Properties.ReadOnly = true;
            this.TxtCMRecvVdPercentage.Size = new System.Drawing.Size(62, 20);
            this.TxtCMRecvVdPercentage.TabIndex = 38;
            // 
            // TxtCMFinancialBudgetPercentage
            // 
            this.TxtCMFinancialBudgetPercentage.EnterMoveNextControl = true;
            this.TxtCMFinancialBudgetPercentage.Location = new System.Drawing.Point(352, 70);
            this.TxtCMFinancialBudgetPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCMFinancialBudgetPercentage.Name = "TxtCMFinancialBudgetPercentage";
            this.TxtCMFinancialBudgetPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCMFinancialBudgetPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCMFinancialBudgetPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCMFinancialBudgetPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtCMFinancialBudgetPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCMFinancialBudgetPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCMFinancialBudgetPercentage.Properties.ReadOnly = true;
            this.TxtCMFinancialBudgetPercentage.Size = new System.Drawing.Size(62, 20);
            this.TxtCMFinancialBudgetPercentage.TabIndex = 34;
            // 
            // TxtCMBidPricingAndQtPercentage
            // 
            this.TxtCMBidPricingAndQtPercentage.EnterMoveNextControl = true;
            this.TxtCMBidPricingAndQtPercentage.Location = new System.Drawing.Point(352, 49);
            this.TxtCMBidPricingAndQtPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCMBidPricingAndQtPercentage.Name = "TxtCMBidPricingAndQtPercentage";
            this.TxtCMBidPricingAndQtPercentage.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCMBidPricingAndQtPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCMBidPricingAndQtPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCMBidPricingAndQtPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtCMBidPricingAndQtPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCMBidPricingAndQtPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCMBidPricingAndQtPercentage.Properties.ReadOnly = true;
            this.TxtCMBidPricingAndQtPercentage.Size = new System.Drawing.Size(62, 20);
            this.TxtCMBidPricingAndQtPercentage.TabIndex = 30;
            // 
            // TxtCMBidPricingAndQt
            // 
            this.TxtCMBidPricingAndQt.EnterMoveNextControl = true;
            this.TxtCMBidPricingAndQt.Location = new System.Drawing.Point(165, 49);
            this.TxtCMBidPricingAndQt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCMBidPricingAndQt.Name = "TxtCMBidPricingAndQt";
            this.TxtCMBidPricingAndQt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCMBidPricingAndQt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCMBidPricingAndQt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCMBidPricingAndQt.Properties.Appearance.Options.UseFont = true;
            this.TxtCMBidPricingAndQt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCMBidPricingAndQt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCMBidPricingAndQt.Properties.ReadOnly = true;
            this.TxtCMBidPricingAndQt.Size = new System.Drawing.Size(180, 20);
            this.TxtCMBidPricingAndQt.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(9, 33);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 14);
            this.label12.TabIndex = 27;
            this.label12.Text = "Contribution Margin";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCMRealization
            // 
            this.TxtCMRealization.EnterMoveNextControl = true;
            this.TxtCMRealization.Location = new System.Drawing.Point(165, 112);
            this.TxtCMRealization.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCMRealization.Name = "TxtCMRealization";
            this.TxtCMRealization.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCMRealization.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCMRealization.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCMRealization.Properties.Appearance.Options.UseFont = true;
            this.TxtCMRealization.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCMRealization.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCMRealization.Properties.ReadOnly = true;
            this.TxtCMRealization.Size = new System.Drawing.Size(180, 20);
            this.TxtCMRealization.TabIndex = 41;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(18, 51);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 14);
            this.label11.TabIndex = 28;
            this.label11.Text = "Bid And Pricing Quotation";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(102, 114);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 14);
            this.label7.TabIndex = 40;
            this.label7.Text = "Realization";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(70, 72);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 14);
            this.label10.TabIndex = 32;
            this.label10.Text = "Financial Budget";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCMRecvVd
            // 
            this.TxtCMRecvVd.EnterMoveNextControl = true;
            this.TxtCMRecvVd.Location = new System.Drawing.Point(165, 91);
            this.TxtCMRecvVd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCMRecvVd.Name = "TxtCMRecvVd";
            this.TxtCMRecvVd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCMRecvVd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCMRecvVd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCMRecvVd.Properties.Appearance.Options.UseFont = true;
            this.TxtCMRecvVd.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCMRecvVd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCMRecvVd.Properties.ReadOnly = true;
            this.TxtCMRecvVd.Size = new System.Drawing.Size(180, 20);
            this.TxtCMRecvVd.TabIndex = 37;
            // 
            // TxtCMFinancialBudget
            // 
            this.TxtCMFinancialBudget.EnterMoveNextControl = true;
            this.TxtCMFinancialBudget.Location = new System.Drawing.Point(165, 70);
            this.TxtCMFinancialBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCMFinancialBudget.Name = "TxtCMFinancialBudget";
            this.TxtCMFinancialBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCMFinancialBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCMFinancialBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCMFinancialBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtCMFinancialBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCMFinancialBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCMFinancialBudget.Properties.ReadOnly = true;
            this.TxtCMFinancialBudget.Size = new System.Drawing.Size(180, 20);
            this.TxtCMFinancialBudget.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(4, 93);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(161, 14);
            this.label9.TabIndex = 36;
            this.label9.Text = "Receiving Item from Vendor";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmRptProjectBudgetMonitoring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 473);
            this.Name = "FrmRptProjectBudgetMonitoring";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCRealizationPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCRealization.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCRecvVd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCFinancialBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVCBidPricingAndQt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractAmt.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMRealizationPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMRecvVdPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMFinancialBudgetPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMBidPricingAndQtPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMBidPricingAndQt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMRealization.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMRecvVd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCMFinancialBudget.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        protected System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label30;
        protected internal DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtPONo;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtCtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        internal DevExpress.XtraEditors.TextEdit TxtVCBidPricingAndQt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtContractAmt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtVCRealizationPercentage;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtVCRealization;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtVCRecvVd;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtVCFinancialBudget;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtCMRealizationPercentage;
        internal DevExpress.XtraEditors.TextEdit TxtCMRecvVdPercentage;
        internal DevExpress.XtraEditors.TextEdit TxtCMFinancialBudgetPercentage;
        internal DevExpress.XtraEditors.TextEdit TxtCMBidPricingAndQtPercentage;
        internal DevExpress.XtraEditors.TextEdit TxtCMBidPricingAndQt;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtCMRealization;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtCMRecvVd;
        internal DevExpress.XtraEditors.TextEdit TxtCMFinancialBudget;
        private System.Windows.Forms.Label label9;
    }
}