﻿#region update
/*
    26/10/2017 [ari] printout tambah nonwoven, apparel, horeca, ifs
    27/10/2017 [ARI] Header sesuai draft MAI
    02/03/2018 [ARI] tambah printout selain MAI
    12/06/2018 [WED] ubah print out profit loss MAI
    19/07/2018 [TKG] logic perhitungan nomor rekening parent diubah untuk mengakomodasi apabila anak nomor rekening anak yg panjangnya tidak sama.
 *  18/12/2019 [HAR/KBN] BUG : journal bulan berjalan tidak ikut  terhitung 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPrintProfitLoss : RunSystem.FrmBase11
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mSQL = string.Empty, startYr = string.Empty,
            mPOPrintOutCompanyLogo = "1",
            mBSPrintOutCompanyLogo = "1",
            mProfitLossMAIEntityAcNo = string.Empty;
        private bool
           mIsEntityMandatory = false;
        private decimal NonWoven = 0, Apparel = 0, Horeca = 0, Ifs =0;
        

        #endregion

        #region Constructor

        public FrmPrintProfitLoss(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                var CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                SetLueEntCode(ref LueEntCode);
                GetParameter();
                if (mIsEntityMandatory) LblEntity.ForeColor = Color.Red;
                base.FrmLoad(sender, e);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region PrintOut

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            mPOPrintOutCompanyLogo = Sm.GetParameter("POPrintOutCompanyLogo");
            mBSPrintOutCompanyLogo = Sm.GetParameter("BSPrintOutCompanyLogo");
          
            if (Sm.IsDteEmpty(DteDocDt2, "Date") || (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity"))) return;

            try
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                Cursor.Current = Cursors.WaitCursor;

                var lCOA = new List<COA>();
                var lCompany = new List<Company>();
                var lProfitLoss = new List<ProfitLoss>();
                var lTotalProfitLoss = new List<TotalProfitLoss>();
                var lEntityCOA = new List<EntityCOA>();
                var lLogo = new List<Logo>();

                string[] TableName = { "ProfitLoss", "Company", "TotalProfitLoss", "Logo" };
                List<IList> myLists = new List<IList>();

                #region Proses
                var Yr = Sm.GetDte(DteDocDt2).Substring(0, 4);
                var Mth = Sm.GetDte(DteDocDt2).Substring(4, 2);

                Process1(ref lCOA);
                if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                    Process6(ref lEntityCOA);
                if (lCOA.Count > 0)
                {

                    Process2(ref lCOA, Yr);
                    Process3(ref lCOA, Mth, Yr);
                    Process4(ref lCOA);
                    Process5(ref lCOA);

                    #region Filter by Entity
                    if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")  
                    {
                        if (lEntityCOA.Count > 0)
                        {
                            if (lCOA.Count > 0)
                            {

                                for (var l = 0; l < lEntityCOA.Count; l++)
                                {
                                    for (var i = 0; i < lCOA.Count; i++)
                                    {
                                        if (lEntityCOA[l].AcNo == lCOA[i].AcNo)
                                        {
                                            lProfitLoss.Add(new ProfitLoss()
                                            {
                                                AcNo = lCOA[i].AcNo,
                                                AcDesc = lCOA[i].AcDesc,
                                                Balance = lCOA[i].Balance,
                                                NonWoven = lCOA[i].NonWoven,
                                                Apparel = lCOA[i].Apparel,
                                                Horeca = lCOA[i].Horeca,
                                                Ifs = lCOA[i].Ifs,
                                            });
                                        }
                                    }
                                }

                                decimal balance4 = 0m, balance5 = 0m;
                                for (var k = 0; k < lCOA.Count; k++)
                                {
                                    if (lCOA[k].Parent.Length == 0)
                                    {
                                        if (lCOA[k].AcNo == "4")
                                            balance4 = lCOA[k].Balance;

                                        if (lCOA[k].AcNo == "5")
                                            balance5 = lCOA[k].Balance;
                                    }
                                }

                                lTotalProfitLoss.Add(new TotalProfitLoss()
                                {
                                    Total = balance4,
                                    Total2 = balance5
                                });
                            }
                        }
                    }
                    #endregion

                    #region Not Filter by Entity
                    else
                    {
                        if (lCOA.Count > 0)
                        {
                            for (var i = 0; i < lCOA.Count; i++)
                            {
                                lProfitLoss.Add(new ProfitLoss()
                                {
                                    AcNo = lCOA[i].AcNo,
                                    AcDesc = lCOA[i].AcDesc,
                                    Balance = lCOA[i].Balance,
                                    NonWoven = lCOA[i].NonWoven,
                                    Apparel = lCOA[i].Apparel,
                                    Horeca = lCOA[i].Horeca,
                                    Ifs = lCOA[i].Ifs,
                                    Konsolidasi = lCOA[i].Konsolidasi
                                });

                                decimal balance4 = 0m, balance5 = 0m;
                                for (var k = 0; k < lCOA.Count; k++)
                                {
                                    if (lCOA[k].AcNo == "4")
                                        balance4 = lCOA[k].Balance;

                                    if (lCOA[k].AcNo == "5")
                                        balance5 = lCOA[k].Balance;
                                }

                                lTotalProfitLoss.Add(new TotalProfitLoss()
                                {
                                    Total = balance4,
                                    Total2 = balance5
                                });
                            }
                        }
                    }

                    #endregion
                  
                }
                if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate")
                {
                    lCompany.Add(new Company()
                    {
                        CompanyName = LueEntCode.Text.ToUpper(),
                        DocDt1 = string.Concat("01/Jan/", startYr),
                        DocDt2 = DteDocDt2.Text
                    });
                }
                else
                {
                    lCompany.Add(new Company()
                    {
                        CompanyName = Sm.GetParameter("ReportTitle1").ToUpper(),
                        DocDt1 = string.Concat("01/Jan/", startYr),
                        DocDt2 = DteDocDt2.Text,
                    });
                }

                myLists.Add(lProfitLoss);
                myLists.Add(lCompany);
                myLists.Add(lTotalProfitLoss);
                
                lCOA.Clear();
                lEntityCOA.Clear();

                #endregion

                #region Logo
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                if (mIsEntityMandatory)
                {
                    SQL.AppendLine("Select EntLogoName As CompanyLogo, EntName As 'CompanyName', ");
                    SQL.AppendLine("EntAddress");
                    SQL.AppendLine("From TblEntity Where EntCode=@EntCode");
                }
                else
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'EntAddress'");
                }
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    if (Sm.GetParameter("Doctitle") == "MSI")
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo(mBSPrintOutCompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));
                    }
                    Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                //0-1
                "CompanyLogo",
                "CompanyName",
                "EntAddress"
               
                });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lLogo.Add(new Logo()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                EntAddress = Sm.DrStr(dr, c[2]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(lLogo);

                #endregion

                if (Sm.GetParameter("DocTitle") == "MAI")
                    Sm.PrintReport("ProfitLossMAI", myLists, TableName, false);
                else
                    Sm.PrintReport("ProfitLoss", myLists, TableName, false);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsEntityMandatory = Sm.GetParameter("IsEntityMandatory") == "Y";
            string NonWovenS = Sm.GetParameter("ProfitLossNonWovenPercentage");
            if (NonWovenS.Length > 0) NonWoven = decimal.Parse(NonWovenS);

            string ApparelS = Sm.GetParameter("ProfitLossApparelPercentage");
            if (ApparelS.Length > 0) Apparel = decimal.Parse(ApparelS);

            string HorecaS = Sm.GetParameter("ProfitLossHorecaPercentage");
            if (HorecaS.Length > 0) Horeca = decimal.Parse(HorecaS);

            string IfsS = Sm.GetParameter("ProfitLossIFSPercentage");
            if (IfsS.Length > 0) Ifs = decimal.Parse(IfsS);

            mProfitLossMAIEntityAcNo = Sm.GetParameter("ProfitLossMAIEntityAcNo");
        }

        //show account number, account description, parent, type
        private void Process1(ref List<COA> lCOA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT AcNo, AcDesc, Parent, AcType ");
            SQL.AppendLine("FROM TblCOA ");
            SQL.AppendLine("WHERE AcNo LIKE '4%' Or AcNo LIKE '5%' ");
            SQL.AppendLine("And ActInd='Y' ");
            SQL.AppendLine("ORDER BY AcNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Parent", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lCOA.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Parent = Sm.DrStr(dr, c[2]),
                            Level = Sm.DrStr(dr, c[2]).Length == 0 ? 1 : -1,
                            AcType = Sm.DrStr(dr, c[3]),
                            HasChild = false,
                            //ParentRow = 0,
                            MonthToDateDAmt = 0m,
                            MonthToDateCAmt = 0m,
                            MonthToDateBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        //get opening balance
        private void Process2(ref List<COA> lCOA, string Yr)
        {
            #region old
            //var SQL = new StringBuilder();
            //var cm = new MySqlCommand();
            //int Temp = 0;

            //SQL.AppendLine("SELECT B.AcNo, B.Amt ");
            //SQL.AppendLine("FROM TblCOAOpeningBalanceHdr A ");
            //SQL.AppendLine("INNER JOIN TblCOAOpeningBalanceDtl B ");
            //SQL.AppendLine("ON A.DocNo=B.DocNo ");
            //SQL.AppendLine("And (B.AcNo Like '4%' Or B.AcNo Like '5%') ");
            //SQL.AppendLine("Where A.CancelInd='N' ");
            //SQL.AppendLine("And A.Yr=@Yr ");
            //if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "101")
            //    SQL.AppendLine("And A.EntCode = @EntCode ");
            //SQL.AppendLine("ORDER BY B.AcNo; ");

            //Sm.CmParam<String>(ref cm, "@Yr", Yr); //startYr
            //Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandTimeout = 600;
            //    cm.CommandText = SQL.ToString();
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "Amt" });
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            for (var i = Temp; i < lCOA.Count; i++)
            //            {
            //                if (string.Compare(lCOA[i].AcNo, dr.GetString(0)) == 0)
            //                {
            //                    if (lCOA[i].AcType == "D")
            //                        lCOA[i].OpeningBalanceDAmt = dr.GetDecimal(1);
            //                    if (lCOA[i].AcType == "C")
            //                        lCOA[i].OpeningBalanceCAmt = dr.GetDecimal(1);
            //                    Temp = i;
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //    dr.Close();
            //}
            #endregion
            var lJournal = new List<Journal>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.AcNo, ");
            SQL.AppendLine("Case C.AcType When 'D' Then B.Amt Else 0.00 End As DAmt, ");
            SQL.AppendLine("Case C.AcType When 'C' Then B.Amt Else 0.00 End As CAmt ");
            SQL.AppendLine("From TblCOAOpeningBalanceHdr A, TblCOAOpeningBalanceDtl B, TblCOA C ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.AcNo=C.AcNo ");
            SQL.AppendLine("And B.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("And (Left(B.AcNo, 1)='4' Or Left(B.AcNo, 1)='5') ");
            SQL.AppendLine("And A.CancelInd='N' And A.Yr=@Yr ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate") 
                SQL.AppendLine("And A.EntCode = @EntCode ");
            SQL.AppendLine("ORDER BY B.AcNo; ");

            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].OpeningBalanceDAmt += lJournal[i].DAmt;
                            lCOA[j].OpeningBalanceCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }

                    }
                }
            }
            lJournal.Clear();
        }

        //sum debit and credit amount
        private void Process3(ref List<COA> lCOA, string Mth, string Yr)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("SELECT B.AcNo, SUM(B.DAmt) AS DAmt, SUM(B.CAmt) AS CAmt ");
            SQL.AppendLine("FROM TblJournalHdr A  ");
            SQL.AppendLine("INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo And (B.AcNo LIKE '4%' Or B.AcNo LIKE '5%') ");
            SQL.AppendLine("Where LEFT(A.DocDt, 6)<=@YrMth ");
            if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "Consolidate") 
                SQL.AppendLine("And B.EntCode = @EntCode ");
            SQL.AppendLine("GROUP BY B.AcNo; ");

            var lJournal = new List<Journal>();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Yr, Mth));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lJournal.Add(new Journal()
                        {
                            AcNo = dr.GetString(0),
                            DAmt = dr.GetDecimal(1),
                            CAmt = dr.GetDecimal(2)
                        });
                    }
                }
                dr.Close();
            }
            if (lJournal.Count > 0)
            {
                lJournal.OrderBy(x => x.AcNo);
                bool IsFirst = false;
                int Temp = 0;
                for (var i = 0; i < lJournal.Count; i++)
                {
                    IsFirst = false;
                    for (var j = Temp; j < lCOA.Count; j++)
                    {
                        if (
                            lJournal[i].AcNo.Count(x => x == '.') == lCOA[j].AcNo.Count(x => x == '.') && Sm.CompareStr(lJournal[i].AcNo, lCOA[j].AcNo) ||
                            lJournal[i].AcNo.Count(x => x == '.') != lCOA[j].AcNo.Count(x => x == '.') && lJournal[i].AcNo.StartsWith(string.Concat(lCOA[j].AcNo, '.'))
                            )
                        {
                            if (!IsFirst)
                            {
                                IsFirst = true;
                                Temp = j;
                            }
                            lCOA[j].MonthToDateDAmt += lJournal[i].DAmt;
                            lCOA[j].MonthToDateCAmt += lJournal[i].CAmt;
                            if (string.Compare(lCOA[j].AcNo, lJournal[i].AcNo) == 0)
                                break;
                        }
                    }
                }
            }
            lJournal.Clear();
        }

        //sum to column year to date
        private void Process4(ref List<COA> lCOA)
        {
            for (var i = 0; i < lCOA.Count; i++)
            {
                lCOA[i].YearToDateDAmt =
                    lCOA[i].OpeningBalanceDAmt +
                    lCOA[i].MonthToDateDAmt;

                lCOA[i].YearToDateCAmt =
                    lCOA[i].OpeningBalanceCAmt +
                    lCOA[i].MonthToDateCAmt;

                if (Sm.Left(lCOA[i].AcNo, 1) == "4")
                    lCOA[i].Balance = lCOA[i].YearToDateCAmt - lCOA[i].YearToDateDAmt;

                if (Sm.Left(lCOA[i].AcNo, 1) == "5")
                    lCOA[i].Balance = lCOA[i].YearToDateDAmt - lCOA[i].YearToDateCAmt;

                lCOA[i].NonWoven = NonWoven * lCOA[i].Balance;
                lCOA[i].Apparel = Apparel * lCOA[i].Balance;
                lCOA[i].Horeca = Horeca * lCOA[i].Balance;
                lCOA[i].Ifs = Ifs * lCOA[i].Balance;

                if (mProfitLossMAIEntityAcNo.Length > 0)
                {
                    string[] mAcNos = mProfitLossMAIEntityAcNo.Split(',');

                    for (int x = 0; x < mAcNos.Length; x++)
                    {
                        if (mAcNos[x] == Sm.Left(lCOA[i].AcNo, 1) && mAcNos[x] == "4")
                        {
                            if(lCOA[i].AcDesc.Contains("Horeca"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = Horeca * lCOA[i].Balance;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("Non Woven"))
                            {
                                lCOA[i].NonWoven = NonWoven * lCOA[i].Balance;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("Apparel"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = Apparel * lCOA[i].Balance;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("IFS"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = Ifs * lCOA[i].Balance;
                                break;
                            }
                            else
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                        }
                        else if (lCOA[i].AcNo.Length > 2 && mAcNos[x] == Sm.Left(lCOA[i].AcNo, 3) && (mAcNos[x] == "5.1" || mAcNos[x] == "5.2"))
                        {
                            if (lCOA[i].AcDesc.Contains("Horeca"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = Horeca * lCOA[i].Balance;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("Non Woven"))
                            {
                                lCOA[i].NonWoven = NonWoven * lCOA[i].Balance;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("Apparel"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = Apparel * lCOA[i].Balance;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("IFS"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = Ifs * lCOA[i].Balance;
                                break;
                            }
                            else
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                        }
                        else if (lCOA[i].AcNo.Length > 7 && mAcNos[x] == Sm.Left(lCOA[i].AcNo, 8) && mAcNos[x] == "5.3.1.23")
                        {
                            if (lCOA[i].AcDesc.Contains("Horeca"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = Horeca * lCOA[i].Balance;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("Non Woven"))
                            {
                                lCOA[i].NonWoven = NonWoven * lCOA[i].Balance;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("Apparel"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = Apparel * lCOA[i].Balance;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                            else if (lCOA[i].AcDesc.Contains("IFS"))
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = Ifs * lCOA[i].Balance;
                                break;
                            }
                            else
                            {
                                lCOA[i].NonWoven = 0m;
                                lCOA[i].Apparel = 0m;
                                lCOA[i].Horeca = 0m;
                                lCOA[i].Ifs = 0m;
                                break;
                            }
                        }
                    }
                }

                lCOA[i].Konsolidasi = lCOA[i].NonWoven + lCOA[i].Apparel + lCOA[i].Horeca + lCOA[i].Ifs;

            }
        }

        //checking whether this account has parent or nah
        private void Process5(ref List<COA> lCOA)
        {
            var Parent = string.Empty;
            var ParentLevel = 0;
            //var ParentRow = 0;

            for (var i = 0; i < lCOA.Count; i++)
            {
                if (lCOA[i].Level == -1)
                {
                    if (string.Compare(lCOA[i].Parent, Parent) == 0)
                    {
                        lCOA[i].Level = ParentLevel + 1;
                        //lCOA[i].ParentRow = ParentRow;
                    }
                    else
                    {
                        for (var j = 0; j < lCOA.Count; j++)
                        {
                            if (string.Compare(lCOA[i].Parent, lCOA[j].AcNo) == 0)
                            {
                                Parent = lCOA[i].Parent;
                                ParentLevel = lCOA[j].Level;
                                //ParentRow = j;
                                lCOA[i].Level = lCOA[j].Level + 1;
                                //lCOA[i].ParentRow = ParentRow;
                                break;
                            }
                        }
                    }
                }
                for (var j = i + 1; j < lCOA.Count; j++)
                {
                    if (lCOA[i].AcNo.Length >= lCOA[j].AcNo.Length)
                        break;
                    else
                    {
                        if (string.Compare(lCOA[i].AcNo, lCOA[j].Parent) == 0)
                        {
                            lCOA[i].HasChild = true;
                            break;
                        }
                    }
                }
            }
        }

        private void Process6(ref List<EntityCOA> lEntityCOA)
        {
            #region Old
            //var SQL = new StringBuilder();
            //var cm = new MySqlCommand();

            //SQL.AppendLine("Select AcNo ");
            //SQL.AppendLine("From TblCOA ");
            //SQL.AppendLine("Where Parent Is Null ");
            //SQL.AppendLine("And AcNo In ('4','5') ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select X.AcNo ");
            //SQL.AppendLine("From ");
            //SQL.AppendLine("(");
            //SQL.AppendLine("SELECT A.AcNo FROM TblCOA A ");
            //SQL.AppendLine("Inner Join TblCoaDtl B On A.AcNo=B.AcNo ");
            //SQL.AppendLine("Inner Join TblEntity C On B.EntCode=C.EntCode ");
            //SQL.AppendLine("WHERE (A.AcNo LIKE '4%' Or A.AcNo LIKE '5%')");
            //SQL.AppendLine("And A.ActInd='Y' ");

            //SQL.AppendLine("And B.EntCode=@Entity ");
            //SQL.AppendLine("ORDER BY A.AcNo ");
            //SQL.AppendLine(")X ");
            //SQL.AppendLine("Order By AcNo ");
            //SQL.AppendLine("; ");

            ////SQL.AppendLine("SELECT A.AcNo FROM TblCOA A ");
            ////if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "101")
            ////{
            ////    SQL.AppendLine("Inner Join TblCoaDtl B On A.AcNo=B.AcNo ");
            ////    SQL.AppendLine("Inner Join TblEntity C On B.EntCode=C.EntCode ");
            ////}
            ////SQL.AppendLine("WHERE A.AcNo LIKE '4%' Or A.AcNo LIKE '5%' ");
            ////SQL.AppendLine("And A.ActInd='Y' ");
            ////if (mIsEntityMandatory && Sm.GetLue(LueEntCode) != "101")
            ////    SQL.AppendLine("And B.EntCode=@Entity ");
            ////SQL.AppendLine("ORDER BY A.AcNo; ");

            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandTimeout = 600;
            //    cm.CommandText = SQL.ToString();
            //    Sm.CmParam<String>(ref cm, "@Entity", Sm.GetLue(LueEntCode));
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            lEntityCOA.Add(new EntityCOA()
            //            {
            //                AcNo = Sm.DrStr(dr, c[0]),
            //            });
            //        }
            //    }
            //    dr.Close();
            //}
            #endregion
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.AcNo, U.EntCode, V.EntName, V.EntAddress ");
            SQL.AppendLine("From TblCOA T ");
            SQL.AppendLine("Inner Join TblCOADtl U On T.AcNo = U.AcNo ");
            SQL.AppendLine("Inner Join TblEntity V On U.EntCode = V.EntCode ");
            SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("And (Left(T.AcNo, 1) In ('4','5')) ");
            SQL.AppendLine("And U.EntCode = @EntCode ");
            SQL.AppendLine("Order By T.AcNo; ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "EntCode", "EntName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEntityCOA.Add(new EntityCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            EntCode = Sm.DrStr(dr, c[1]),
                            EntName = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private static void SetLueEntCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select EntCode As Col1, EntName As Col2 From TblEntity Where ActInd='Y' union all select 'Consolidate' As Col1, 'Consolidate' As Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        #endregion

        #endregion

        #region Event

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            startYr = string.Empty;

            if (Sm.GetDte(DteDocDt2).Length > 0)
                startYr = Sm.Left(Sm.GetDte(DteDocDt2), 4);
        }

       private void LueEntCode_EditValueChanged(object sender, EventArgs e)
       {
           Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(SetLueEntCode));
       }

       #endregion

        #region Class

       private class ProfitLoss
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
            public decimal NonWoven { get; set; }
            public decimal Apparel { get; set; }
            public decimal Horeca { get; set; }
            public decimal Ifs { get; set; }
            public decimal Konsolidasi { get; set; }
        }

       private class ProfitLoss2
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
            public string EntCode { get; set; }
            public string EntName { get; set; }
        }

       private class COA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public string EntCode { get; set; }
            public string Parent { get; set; }
            public string EntName { get; set; }
            public int Level { get; set; }
            public bool HasChild { get; set; }
            //public int ParentRow { get; set; }
            public decimal OpeningBalanceDAmt { get; set; }
            public decimal OpeningBalanceCAmt { get; set; }
            public decimal MonthToDateDAmt { get; set; }
            public decimal MonthToDateCAmt { get; set; }
            public decimal YearToDateDAmt { get; set; }
            public decimal YearToDateCAmt { get; set; }
            public decimal Balance { get; set; }
            public decimal MonthToDateBalance { get; set; }

            public decimal NonWoven { get; set; }
            public decimal Apparel { get; set; }
            public decimal Horeca { get; set; }
            public decimal Ifs { get; set; }
            public decimal Konsolidasi { get; set; }

        }

       private class Company
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string DocDt1 { get; set; }
            public string DocDt2 { get; set; }
        }

       private class Journal
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

       private class TotalProfitLoss
       {
           public decimal Total { get; set; }
           public decimal Total2 { get; set; }
       }

       private class EntityCOA
       {
           public string AcNo { get; set; }
           public string EntCode { get; set; }
           public string EntName { get; set; }
       }

       private class Percent
       {
           //public string AcNo { get; set; }
           //public string AcDesc { get; set; }
           //public decimal YearToDateDAmt { get; set; }
           //public decimal YearToDateCAmt { get; set; }
           //public decimal Balance { get; set; }
           //public string EntCode { get; set; }
           //public string EntName { get; set; }
           public decimal NonWoven { get; set; }
           public decimal Apparel { get; set; }
           public decimal Horeca { get; set; }
           public decimal Ifs { get; set; }
           public decimal Konsolidasi { get; set; }
       }

       private class TotalPercent
       {
           //public string AcNo { get; set; }
           //public string AcDesc { get; set; }
           //public decimal YearToDateDAmt { get; set; }
           //public decimal YearToDateCAmt { get; set; }
           //public decimal Balance { get; set; }
           //public string EntCode { get; set; }
           //public string EntName { get; set; }
           public decimal NonWoven { get; set; }
           public decimal Apparel { get; set; }
           public decimal Horeca { get; set; }
           public decimal Ifs { get; set; }
           public decimal Konsolidasi { get; set; }
       }

       class Logo
       {
           public string CompanyLogo { set; get; }
           public string CompanyName { get; set; }
           public string EntAddress { get; set; }
           public string PrintBy { get; set; }
       }

       #endregion

    }
}
