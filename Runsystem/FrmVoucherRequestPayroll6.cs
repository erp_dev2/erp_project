﻿#region Update
/*
    23/11/2018 [TKG] VR Payroll untuk VIR
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    22/02/2019 [TKG] ubah pensiun + journal VR Payroll
    15/10/2021 [IBL/ALL] Tambah validasi setting journal
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using System.IO;
using System.Net;
using Renci.SshNet;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestPayroll6 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mPayrunPeriodBulanan = string.Empty;
        internal FrmVoucherRequestPayroll6Find FrmFind;
        internal bool
            mIsNotFilterByAuthorization = false,
            mIsNotForStd = false,
            mIsForMonthlyEmployee = false,
            mIsVRPayrollShowHeadcount = false,
            mIsEntityMandatory = false,
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false;
        private bool
            mIsAutoJournalActived = false,
            mIsCSVUseRealAmt = false,
            mIsCheckCOAJournalNotExists = false
            ;
        private string
            mVoucherCodeFormatType = "1",
            mAcNoForAllowanceSSHealth = string.Empty,
            mAcNoForAllowanceSSEmployment = string.Empty,
            mAcNoForAllowanceSSPension = string.Empty,
            mSSPCodeForEmployment = string.Empty,
            mSSPCodeForHealth = string.Empty,
            mSSPCodeForPension = string.Empty,
            mJournalDocSeqNo = string.Empty,
            mAcNoForAccruedSalary = string.Empty,
            mAcNoForTaxLiability = string.Empty,
            mHostAddrForMandiriPayroll = string.Empty,
            mSharedFolderForMandiriPayroll = string.Empty,
            mUserNameForMandiriPayroll = string.Empty,
            mPasswordForMandiriPayroll = string.Empty,
            mSalaryInd = string.Empty,
            mPortForMandiriPayroll = string.Empty,
            mHostAddrForBNIPayroll = string.Empty,
            mSharedFolderForBNIPayroll = string.Empty,
            mUserNameForBNIPayroll = string.Empty,
            mPasswordForBNIPayroll = string.Empty,
            mPortForBNIPayroll = string.Empty,
            mProtocolForMandiriPayroll = string.Empty,
            mProtocolForBNIPayroll = string.Empty,
            mCompanyCodeForMandiriPayroll = string.Empty,
            mPathToSaveExportedMandiriPayroll = string.Empty,
            mPathToSaveExportedBNIPayroll = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestPayroll6(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Payroll's Voucher Request";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueCurCode(ref LueCurCode);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-4
                    "Description",
                    "Transferred",
                    "Remark",
                    "Headcount"
                },
                new int[]{ 0, 330, 120, 230, 80 }
            );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 1);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, mIsVRPayrollShowHeadcount);
            if (mIsVRPayrollShowHeadcount) Grd1.Cols[4].Move(3);
            
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 5;
            Grd2.ReadOnly = true;
            Grd2.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "No",

                    //1-4
                    "User", 
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[]{ 40, 150, 100, 100, 300 }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 18;
            Grd3.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[] 
                {
                    //0
                    "",

                    //1-5
                    "Payrun's"+Environment.NewLine+"Code", 
                    "",
                    "Payrun's"+Environment.NewLine+"Name",
                    "Brutto",
                    "Tax",

                    //6-10
                    "Transferred",
                    "Health"+Environment.NewLine+"(Ee)",
                    "Employment"+Environment.NewLine+"(Ee)",
                    "Pension"+Environment.NewLine+"(Ee)",
                    "Health"+Environment.NewLine+"(Er)",

                    //11-15
                    "Employment"+Environment.NewLine+"(Er)",
                    "Pension"+Environment.NewLine+"(Er)",
                    "Total"+Environment.NewLine+"SS",
                    "Entity Code",
                    "Entity",

                    //16-17
                    "Journal# (New)",
                    "Journal# (Cancel)"
                },
                new int[] 
                { 
                    //0
                    20,
 
                    //1-5
                    85, 20, 200, 110, 100,
                    
                    //6-10
                    100, 100, 100, 100, 100,
                    
                    //11-15
                    100, 100, 100, 0, 150,

                    //16-17
                    150, 150
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 0, 2 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 2, 14 }, false);
            Sm.GrdColInvisible(Grd3, new int[] { 15 }, mIsEntityMandatory);
            if (mIsEntityMandatory) Grd3.Cols[15].Move(4);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueAcType, LuePaymentType, LueBankAcCode, 
                        LueBankCode, TxtGiroNo, DteDueDt, TxtPaymentUser, TxtPaidToBankCode, 
                        TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, LueCurCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 3 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueAcType, LuePaymentType, LueBankAcCode, 
                        TxtPaymentUser, TxtPaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, 
                        LueCurCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtStatus, TxtVoucherRequestDocNo, TxtVoucherDocNo, DteDocDt, 
                LueAcType, LuePaymentType, LueBankAcCode, LueBankCode, TxtGiroNo, 
                DteDueDt, TxtPaymentUser, TxtPaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, 
                TxtPaidToBankAcNo, LueCurCode,  MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtAmt, TxtTax, TxtTotalSS, TxtBrutto, TxtSSEmployeeEmployment, 
                TxtSSEmployeeHealth, TxtSSEmployeePension, TxtSSEmployerEmployment, TxtSSEmployerHealth, TxtSSEmployerPension
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd3();
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        internal void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 4 });
        }

        internal void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestPayroll6Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sm.SetLue(LueAcType, "C");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            //if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            //string[] TableName = { "VoucherRequestPayHdr", "VoucherRequestPayDtl", "VoucherRequestPayDtl2", "VoucherRequestPayDtlKIM", "VoucherRequestPayKIMSign" };

            //var l = new List<VoucherRequestPayHdr>();
            //var ldtl = new List<VoucherRequestPayDtl>();
            //var ldtl2 = new List<VoucherRequestPayDtl2>();
            //var ldtlKIM = new List<VoucherRequestPayDtlKIM>();
            //var lSignKIM = new List<VoucherRequestPayKIMSign>();

            //List<IList> myLists = new List<IList>();

            //#region Header

            //var cm = new MySqlCommand();

            //var SQL = new StringBuilder();
            //if (mIsEntityMandatory)
            //{
            //    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, E.CompanyName, E.CompanyAddress, E.CompanyPhone, E.CompanyFax, '' As CompanyAddressCity,");
            //}
            //else
            //{
            //    SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            //    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            //    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyPhone, ");
            //    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle5')As CompanyFax, ");
            //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As CompanyAddressCity, ");
            //}
            //SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.CurCode, ");
            //SQL.AppendLine("(Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType Limit 1)As PaymentType, A.GiroNo, D.BankName As GiroBankName, ");
            //SQL.AppendLine("DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, A.Amt As AmtHdr, B.DocEnclosure, A.Remark, C.EntName, ");
            //SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='IsVRPayrollShowNoOfEmp')As NoHeadCount ");
            //SQL.AppendLine("From TblVoucherRequestPayrollHdr A ");
            //SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            //SQL.AppendLine("Left Join TblEntity C On B.EntCode=C.EntCode ");
            //SQL.AppendLine("Left Join TblBank D On A.BankCode=D.BankCode ");
            //if (mIsEntityMandatory)
            //{
            //    SQL.AppendLine("Left Join (");
            //    SQL.AppendLine("    Select distinct A.DocNo, E.EntName As CompanyName, E.EntAddress As CompanyAddress, E.EntPhone As CompanyPhone, E.EntFax As CompanyFax ");
            //    SQL.AppendLine("    From TblVoucherRequestPayrollDtl2 A ");
            //    SQL.AppendLine("    Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode ");
            //    SQL.AppendLine("    Left Join TblSite C On B.SiteCode=C.SiteCode ");
            //    SQL.AppendLine("    Left Join TblProfitCenter D On C.ProfitCenterCode=D.ProfitCenterCode ");
            //    SQL.AppendLine("    Left Join TblEntity E On D.EntCode=E.EntCode ");
            //    SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            //}
            //SQL.AppendLine("Where A.DocNo=@DocNo;");

            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandText = SQL.ToString();
            //    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            //    if (mIsEntityMandatory)
            //    {
            //        string CompanyLogo = Sm.GetValue(
            //           "Select E.EntLogoName " +
            //           "From TblVoucherRequestPayrollDtl2 A  " +
            //           "Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode " +
            //           "Left Join TblSite C On B.SiteCode=C.SiteCode " +
            //           "Left Join TblProfitCenter D On C.ProfitCenterCode=D.ProfitCenterCode " +
            //           "Inner Join TblEntity E On D.EntCode = E.EntCode  " +
            //           "Where A.Docno='" + TxtDocNo.Text + "' "
            //       );
            //        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
            //    }
            //    else
            //    {
            //        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
            //    }
               
            //    var dr = cm.ExecuteReader();
            //    var c = Sm.GetOrdinal(dr, new string[] 
            //            {
            //             //0
            //             "CompanyLogo",

            //             //1-5
            //             "CompanyName",
            //             "CompanyAddress",
            //             "CompanyPhone",
            //             "CompanyFax",
            //             "CompanyAddressCity",
                         
            //             //6-10
            //             "DocNo", 
            //             "DocDt", 
            //             "CurCode",
            //             "PaymentType",
            //             "GiroNo",
                         
            //             //11-15
            //             "GiroBankName",
            //             "GiroDueDt",
            //             "AmtHdr",
            //             "DocEnclosure",
            //             "EntName",

            //             //16-17
            //             "Remark",
            //             "NoHeadCount"


            //            });
            //    if (dr.HasRows)
            //    {
            //        while (dr.Read())
            //        {
            //            l.Add(new VoucherRequestPayHdr()
            //            {
            //                CompanyLogo = Sm.DrStr(dr, c[0]),
            //                CompanyName = Sm.DrStr(dr, c[1]),
            //                CompanyAddress = Sm.DrStr(dr, c[2]),
            //                CompanyPhone = Sm.DrStr(dr, c[3]),
            //                CompanyFax = Sm.DrStr(dr, c[4]),
            //                CompanyAddressCity = Sm.DrStr(dr, c[5]),
            //                DocNo = Sm.DrStr(dr, c[6]),
            //                DocDt = Sm.DrStr(dr, c[7]),
            //                CurCode = Sm.DrStr(dr, c[8]),
            //                PaymentType = Sm.DrStr(dr, c[9]),
            //                GiroNo = Sm.DrStr(dr, c[10]),
            //                GiroBankName = Sm.DrStr(dr, c[11]),
            //                GiroDueDt = Sm.DrStr(dr, c[12]),
            //                AmtHdr = Sm.DrDec(dr, c[13]),
            //                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[13])),
            //                Terbilang2 = Sm.Terbilang2(Sm.DrDec(dr, c[13])),
            //                DocEnclosure = Sm.DrStr(dr, c[14]),
            //                EntName = Sm.DrStr(dr, c[15]),
            //                Remark = Sm.DrStr(dr, c[16]),
            //                NoHeadCount = Sm.DrStr(dr, c[17]),
            //                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),

            //            });
            //        }
            //    }
            //    dr.Close();
            //}

            //myLists.Add(l);
            //#endregion

            //#region detail
            //var cmDtl = new MySqlCommand();

            //using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            //{

            //    cnDtl.Open();
            //    cmDtl.Connection = cnDtl;
            //    cmDtl.CommandText =
            //        "Select A.DNo, A.DocNo, A.Description, A.Amt, B.JmlKar, A.Remark " +
            //        "From TblVoucherRequestPayrollDtl A " +
            //        "Inner Join ( " +
            //        "   Select X.DocNo, Sum(X.JmlKar)JmlKar From ( " +
            //        "   Select A.DocNo, A.PayrunCode, Count(B.EmpCode)As JmlKar " +
            //        "   From TblVoucherRequestPayrollDtl2 A" +
            //        "   Inner Join tblpayrollprocess1 B On A.PayrunCode=B.PayrunCode " +
            //        "   Group by A.DocNo, A.PayrunCode " +
            //        ")X " +
            //        "Group By X.DocNo " +
            //        ")B On A.DocNo=B.DocNo " +
            //        "Where A.DocNo=@DocNo Order By A.DNo ";
            //    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
            //    var drDtl = cmDtl.ExecuteReader();
            //    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
            //            {
            //             //0
            //             "DNo",

            //             //1-5
            //             "DocNo",
            //             "Description",
            //             "Amt",
            //             "JmlKar",
            //             "Remark",

            //            });
            //    if (drDtl.HasRows)
            //    {
            //        while (drDtl.Read())
            //        {
            //            ldtl.Add(new VoucherRequestPayDtl()
            //            {
            //                DNo = Sm.DrStr(drDtl, cDtl[0]),
            //                DocNo = Sm.DrStr(drDtl, cDtl[1]),
            //                Description = Sm.DrStr(drDtl, cDtl[2]),
            //                Amt = Sm.DrDec(drDtl, cDtl[3]),
            //                JmlKar = Sm.DrStr(drDtl, cDtl[4]),
            //                Remark = Sm.DrStr(drDtl, cDtl[5])
            //            });
            //        }
            //    }
            //    drDtl.Close();
            //}
            //myLists.Add(ldtl);
            //#endregion

            //#region Detail2
            //var cmDtl2 = new MySqlCommand();

            //using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cnDtl2.Open();
            //    cmDtl2.Connection = cnDtl2;
            //    cmDtl2.CommandText =
            //        "Select Date_Format(B.StartDt,'%d %M %Y')As StarDt, Date_Format(B.EndDt,'%d %M %Y')As EndDt " +
            //        "From TblVoucherRequestPayrollDtl2 A " +
            //        "Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode " +
            //        "Where A.DocNo=@DocNo Order By A.PayrunCode ";
            //    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
            //    var drDtl2 = cmDtl2.ExecuteReader();
            //    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
            //            {
            //             //0
            //             "StarDt",

            //             //1
            //             "EndDt",
            //            });
            //    if (drDtl2.HasRows)
            //    {
            //        while (drDtl2.Read())
            //        {
            //            ldtl2.Add(new VoucherRequestPayDtl2()
            //            {
            //                StartDt = Sm.DrStr(drDtl2, cDtl2[0]),
            //                EndDt = Sm.DrStr(drDtl2, cDtl2[1]),
            //            });
            //        }
            //    }
            //    drDtl2.Close();
            //}
            //myLists.Add(ldtl2);
            //#endregion

            //#region Detail KIM

            //var SQLKIM = new StringBuilder();
            //string mPayrunCode = string.Empty;
            //var cmDtlKIM = new MySqlCommand();

            //for (int i = 0; i < Grd3.Rows.Count; i++)
            //{
            //    if(Sm.GetGrdStr(Grd3, i, 1).Length > 0)
            //    {
            //        if (mPayrunCode.Length > 0) mPayrunCode += ",";
            //        mPayrunCode += Sm.GetGrdStr(Grd3, i, 1);
            //    }
            //}

            //SQLKIM.AppendLine("Select PayrunCode, Count(EmpCode) EmpCode ");
            //SQLKIM.AppendLine("From TblPayrollProcess1 ");
            //SQLKIM.AppendLine("Where Find_In_Set(PayrunCode, @PayrunCode) ");
            //SQLKIM.AppendLine("Group By PayrunCode; ");

            //using (var cnDtlKIM = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cnDtlKIM.Open();
            //    cmDtlKIM.Connection = cnDtlKIM;
            //    cmDtlKIM.CommandText = SQLKIM.ToString();
            //    Sm.CmParam<String>(ref cmDtlKIM, "@PayrunCode", mPayrunCode);
            //    var drDtlKIM = cmDtlKIM.ExecuteReader();
            //    var cDtlKIM = Sm.GetOrdinal(drDtlKIM, new string[] 
            //    {
            //     //0
            //     "PayrunCode",

            //     //1
            //     "EmpCode",
            //    });
            //    if (drDtlKIM.HasRows)
            //    {
            //        while (drDtlKIM.Read())
            //        {
            //            ldtlKIM.Add(new VoucherRequestPayDtlKIM()
            //            {
            //                PayrunCode = Sm.DrStr(drDtlKIM, cDtlKIM[0]),
            //                JmlKar = Sm.DrStr(drDtlKIM, cDtlKIM[1])
            //            });
            //        }
            //    }
            //    drDtlKIM.Close();
            //}

            //if (ldtlKIM.Count > 0)
            //{
            //    for (int h = 0; h < ldtlKIM.Count; h++)
            //    {
            //        for (int i = 0; i < Grd3.Rows.Count; i++)
            //        {
            //            if (Sm.GetGrdStr(Grd3, i, 1) == ldtlKIM[h].PayrunCode)
            //            {
            //                ldtlKIM[h].Description = Sm.GetGrdStr(Grd3, i, 3);
            //                ldtlKIM[h].Amt = Sm.GetGrdDec(Grd3, i, 6);
            //                break;
            //            }
            //        }
            //    }
            //}

            //myLists.Add(ldtlKIM);

            //#endregion

            //#region Signature KIM

            //var SQLSignKIM = new StringBuilder();
            //var cmSignKIM = new MySqlCommand();

            //SQLSignKIM.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
            //SQLSignKIM.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Level as Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
            //SQLSignKIM.AppendLine("From ( ");
            //SQLSignKIM.AppendLine("    Select Distinct ");
            //SQLSignKIM.AppendLine("    B.UserCode, C.UserName, ");
            //SQLSignKIM.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
            //SQLSignKIM.AppendLine("    From TblVoucherRequestPayrollHdr A ");
            //SQLSignKIM.AppendLine("    Inner Join TblDocApproval B On B.DocType='VoucherRequestPayroll' And A.DocNo=B.DocNo ");
            //SQLSignKIM.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
            //SQLSignKIM.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'VoucherRequestPayroll' ");
            //SQLSignKIM.AppendLine("    Where A.DocNo=@DocNo ");
            //SQLSignKIM.AppendLine("    Union All ");
            //SQLSignKIM.AppendLine("    Select Distinct ");
            //SQLSignKIM.AppendLine("    A.CreateBy As UserCode, B.UserName, ");
            //SQLSignKIM.AppendLine("    '0' As DNo, 0 As Level, 'Prepared By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
            //SQLSignKIM.AppendLine("    From TblVoucherRequestPayrollHdr A ");
            //SQLSignKIM.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
            //SQLSignKIM.AppendLine("    Where A.DocNo=@DocNo ");
            //SQLSignKIM.AppendLine(") T1 ");
            //SQLSignKIM.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
            //SQLSignKIM.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
            //SQLSignKIM.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
            //SQLSignKIM.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
            //SQLSignKIM.AppendLine("Order By T1.Level Asc; ");

            //using (var cnSignKIM = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cnSignKIM.Open();
            //    cmSignKIM.Connection = cnSignKIM;
            //    cmSignKIM.CommandText = SQLSignKIM.ToString();
            //    Sm.CmParam<String>(ref cmSignKIM, "@DocNo", TxtDocNo.Text);
            //    var drSignKIM = cmSignKIM.ExecuteReader();
            //    var cSignKIM = Sm.GetOrdinal(drSignKIM, new string[] 
            //    {
            //     //0
            //     "Signature" ,

            //     //1-5
            //     "Username" ,
            //     "PosName",
            //     "Space",
            //     "Level",
            //     "Dno",

            //     //6
            //     "Title",
            //     "LastupDt"
            //    });
            //    if (drSignKIM.HasRows)
            //    {
            //        int numb = 100;
            //        while (drSignKIM.Read())
            //        {
            //            lSignKIM.Add(new VoucherRequestPayKIMSign()
            //            {
            //                Signature = Sm.DrStr(drSignKIM, cSignKIM[0]),
            //                UserName = Sm.DrStr(drSignKIM, cSignKIM[1]),
            //                PosName = Sm.DrStr(drSignKIM, cSignKIM[2]),
            //                Space = Sm.DrStr(drSignKIM, cSignKIM[3]),
            //                Level = Sm.DrDec(drSignKIM, cSignKIM[4])+numb,
            //                DNo = Sm.DrStr(drSignKIM, cSignKIM[5]),
            //                Title = Sm.DrStr(drSignKIM, cSignKIM[6]),
            //                LastUpDt = Sm.DrStr(drSignKIM, cSignKIM[7])
            //            });
            //            numb = numb - 5;

            //        }
            //    }
            //    drSignKIM.Close();
            //}

            //myLists.Add(lSignKIM);

            //#endregion

            //if(Sm.GetParameter("DocTitle") == "KIM")
            //    Sm.PrintReport("VoucherRequestPayrollKIM", myLists, TableName, false);
            //else
            //    Sm.PrintReport("VoucherRequestPayroll", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 3 }, e);
        }

        #endregion

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequestPayroll", "TblVoucherRequestPayrollHdr");
            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveVoucherRequestPayrollHdr(DocNo, VoucherRequestDocNo));
            cml.Add(SaveVoucherRequestPayrollDtl(DocNo));
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    cml.Add(SaveVoucherRequestPayrollDtl2(DocNo, Row));
            }
            cml.Add(SavePayrollProcess1(DocNo));
            cml.Add(SavePayrun(DocNo));
            cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));
            cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));
            if (mIsAutoJournalActived && !IsNeedApproval())
            {
                var l = new List<JournalPayrun>();
                ProcessJournal1(ref l);
                ProcessJournal2(ref l);
                if (l.Count > 0)
                {
                    for (int i = 0; i < l.Count; i++)
                        cml.Add(SaveJournal(DocNo, l[i]));
                    l.Clear();
                }
            }
            Sm.ExecCommands(cml);
            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            RecomputePayrunInfo();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueAcType, "Account Type") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsPaymentTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                (mIsEntityMandatory &&
                IsEntityNotValid()) ||
                IsJournalSettingInvalid();
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || IsNeedApproval() || !mIsCheckCOAJournalNotExists) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter
            if (mAcNoForAllowanceSSHealth.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSHealth is empty.");
                return true;
            }

            if (mAcNoForAllowanceSSEmployment.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSEmployment is empty");
            }

            if (mAcNoForAllowanceSSPension.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSPension is empty");
            }

            if (mAcNoForTaxLiability.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForTaxLiability is empty");
            }

            if (mAcNoForAccruedSalary.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAccruedSalary is empty");
            }

            //Table
            if (IsJournalSettingInvalid_CostCategory(Msg, string.Empty, string.Empty)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_CostCategory(string Msg, string Index, string COAType)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string PayrunCode = string.Empty, CCtName = string.Empty;

            SQL.AppendLine("Select C.CCtName ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Inner Join TblCostCategory C ");
            SQL.AppendLine("   On B.CCCode=C.CCCode ");
            SQL.AppendLine("   And C.AcNo Is Null ");
            SQL.AppendLine("   And C.CCGrpCode Is Not Null ");
            SQL.AppendLine("   And C.CCGrpCode In (Select ParValue From TblParameter Where ParCode='CCGrpCodeForSalary') ");
            SQL.AppendLine("   Where A.PayrunCode In( ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                PayrunCode = Sm.GetGrdStr(Grd3, r, 1);
                if (PayrunCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@PayrunCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@PayrunCode_" + r.ToString(), PayrunCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            CCtName = Sm.GetValue(cm);
            if (CCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Cost category's " + COAType + "COA account# ( " + CCtName + " ) is empty.");
                return true;
            }
            return false;
        }

        private bool IsEntityNotValid()
        {
            string EntCode = Sm.GetGrdStr(Grd3, 0, 14);
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length != 0)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, r, 14, false, "Entity is empty.")) return true;
                    if (r!=0 && !Sm.CompareStr(EntCode, Sm.GetGrdStr(Grd3, 0, 14)))
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "Entity : " + Sm.GetGrdStr(Grd3, 0, 15) + Environment.NewLine +
                            "One document should not have more than 1 entity.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 payrun.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "Payrun is empty.")) return true;
                if (IsPayrunAlreadyCancelled(Sm.GetGrdStr(Grd3, Row, 1), Sm.GetGrdStr(Grd3, Row, 3))) return true;
                if (IsPayrunAlreadyClosed(Sm.GetGrdStr(Grd3, Row, 1), Sm.GetGrdStr(Grd3, Row, 3))) return true;
            }
            return false;
        }

        private bool IsPayrunAlreadyCancelled(string PayrunCode, string PayrunName)
        {
            if (Sm.IsDataExist( 
                    "Select 1 From TblPayrun " +
                    "Where CancelInd='Y' And PayrunCode=@Param;",
                    PayrunCode,
                    "Payrun Code : " + PayrunCode + Environment.NewLine + 
                    "Payrun Name : " + PayrunName + Environment.NewLine + Environment.NewLine +
                    "This payrun already cancelled."))
                return true;
            return false;
        }

        private bool IsPayrunAlreadyClosed(string PayrunCode, string PayrunName)
        {
            if (Sm.IsDataExist(
                   "Select 1 From TblPayrun " +
                   "Where Status='C' And PayrunCode=@Param And VoucherRequestPayrollInd='F';",
                   PayrunCode,
                   "Payrun Code : " + PayrunCode + Environment.NewLine +
                   "Payrun Name : " + PayrunName + Environment.NewLine + Environment.NewLine +
                   "All employees in this payrun already processed to voucher request."))
                return true;
            return false;
        }

        private MySqlCommand SaveVoucherRequestPayrollHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestPayrollHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, VoucherRequestDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo,");
            SQL.AppendLine("CurCode, Brutto, Amt, Tax, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, CSVInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @VoucherRequestDocNo, ");
            SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo,");
            SQL.AppendLine("@CurCode, @Brutto, @Amt, @Tax, @SSEmployeeHealth, @SSEmployeeEmployment, @SSEmployeePension, @SSEmployerHealth, @SSEmployerEmployment, @SSEmployerPension, @CSVInd, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", TxtPaidToBankCode.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Brutto", Decimal.Parse(TxtBrutto.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Decimal.Parse(TxtTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", Decimal.Parse(TxtSSEmployeeHealth.Text));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", Decimal.Parse(TxtSSEmployeeEmployment.Text));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension", Decimal.Parse(TxtSSEmployeePension.Text));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", Decimal.Parse(TxtSSEmployerHealth.Text));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", Decimal.Parse(TxtSSEmployerEmployment.Text));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension", Decimal.Parse(TxtSSEmployerPension.Text));
            Sm.CmParam<String>(ref cm, "@CSVInd", Sm.GetParameter("DocTitle") == "HIN" ? "N" : "Y");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestPayrollDtl(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestPayrollDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, 0, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, 0, 2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, 0, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestPayrollDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestPayrollDtl2(DocNo, PayrunCode, Brutto, Amt, Tax, " +
                    "SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, CreateBy, CreateDt) " +
                    "Values (@DocNo, @PayrunCode, @Brutto, @Amt, @Tax, " +
                    "@SSEmployeeHealth, @SSEmployeeEmployment, @SSEmployeePension, @SSEmployerHealth, @SSEmployerEmployment, @SSEmployerPension, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Brutto", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd3, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", Sm.GetGrdDec(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", Sm.GetGrdDec(Grd3, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension", Sm.GetGrdDec(Grd3, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrollProcess1(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPayrollProcess1 T Set ");
            SQL.AppendLine("    T.VoucherRequestPayrollDocNo=@DocNo ");
            SQL.AppendLine("Where T.PayrunCode In ( ");
            SQL.AppendLine("    Select PayrunCode from TblVoucherRequestPayrollDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T.VoucherRequestPayrollDocNo Is Null ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=T.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrun(string DocNo)
        {
            var SQL = new StringBuilder();

            //URUTAN TIDAK BOLEH DIBALIK

            SQL.AppendLine("Update TblPayrun T Set ");
            SQL.AppendLine("    T.VoucherRequestPayrollInd='F', Status='C' ");
            SQL.AppendLine("Where T.PayrunCode In ( ");
            SQL.AppendLine("    Select PayrunCode from TblVoucherRequestPayrollDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Not Exists(");
            SQL.AppendLine("    Select PayrunCode From TblPayrollProcess1 ");
            SQL.AppendLine("    Where PayrunCode=T.PayrunCode ");
            SQL.AppendLine("    And VoucherRequestPayrollDocNo Is Null ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblPayrun T Set ");
            SQL.AppendLine("    T.VoucherRequestPayrollInd='P', Status='C' ");
            SQL.AppendLine("Where T.PayrunCode In ( ");
            SQL.AppendLine("    Select PayrunCode from TblVoucherRequestPayrollDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists(");
            SQL.AppendLine("    Select 1 From TblPayrollProcess1 ");
            SQL.AppendLine("    Where PayrunCode=T.PayrunCode ");
            SQL.AppendLine("    And VoucherRequestPayrollDocNo Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists(");
            SQL.AppendLine("    Select 1 From TblPayrollProcess1 ");
            SQL.AppendLine("    Where PayrunCode=T.PayrunCode ");
            SQL.AppendLine("    And VoucherRequestPayrollDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Update TblPayrun T Set ");
            SQL.AppendLine("    T.VoucherRequestPayrollInd='O', Status='O' ");
            SQL.AppendLine("Where T.PayrunCode In ( ");
            SQL.AppendLine("    Select PayrunCode from TblVoucherRequestPayrollDtl2 ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Not Exists(");
            SQL.AppendLine("    Select 1 From TblPayrollProcess1 ");
            SQL.AppendLine("    Where PayrunCode=T.PayrunCode ");
            SQL.AppendLine("    And VoucherRequestPayrollDocNo Is Not Null ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty;

            if (Sm.GetLue(LueAcType) == "C")
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            else
                type = Sm.GetValue("Select AutoNoDebit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            var SQL = new StringBuilder();

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string VoucherRequestPayrollDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, ");
            SQL.AppendLine("EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='VoucherRequestPayrollDeptCode'), ");
            SQL.AppendLine("'06', @AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy), ");
            SQL.AppendLine("0, @CurCode, @Amt, ");
            SQL.AppendLine("@PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcNo, @PaidToBankAcName, ");

            if (mIsEntityMandatory)
            {
                SQL.AppendLine("( ");
                SQL.AppendLine("Select T3.EntCode ");
                SQL.AppendLine("From TblPayrun T1 ");
                SQL.AppendLine("Inner Join TblSite T2 On T1.SiteCode=T2.SiteCode ");
                SQL.AppendLine("Inner Join TblProfitCenter T3 On T2.ProfitCenterCode=T3.ProfitCenterCode ");
                SQL.AppendLine("Where T1.PayrunCode=@PayrunCode And T1.SiteCode Is Not Null ");
                SQL.AppendLine("), ");
            }
            else
                SQL.AppendLine("Null, ");

            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @VoucherRequestPayrollDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequestPayroll'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestPayroll' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestPayrollDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblVoucherRequestPayrollHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestPayrollDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestPayroll' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestPayrollDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestPayrollDocNo", VoucherRequestPayrollDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", TxtPaidToBankCode.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if (mIsEntityMandatory) Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetGrdStr(Grd3, 0, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, 0, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, 0, 2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, 0, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, JournalPayrun x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblVoucherRequestPayrollDtl2 Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And PayrunCode=@PayrunCode; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo As DocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Voucher Request For Payroll : ', @DocNo, ' (', @PayrunCode, ')') As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@Remark, @UserCode As CreateBy, CurrentDateTime() As CreateDt; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, C.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select T.AcNo, T.DAmt, T.CAmt From (");
            SQL.AppendLine("            Select C.AcNo As AcNo, ");
            SQL.AppendLine("            @Amt+@Tax As DAmt, 0 As CAmt ");
            SQL.AppendLine("            From TblPayrun A ");
            SQL.AppendLine("            Inner Join TblCostCenter B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("            Inner Join TblCostCategory C ");
            SQL.AppendLine("                On B.CCCode=C.CCCode ");
            SQL.AppendLine("                And C.AcNo Is Not Null ");
            SQL.AppendLine("                And C.CCGrpCode Is Not Null ");
            SQL.AppendLine("                And C.CCGrpCode In (Select ParValue From TblParameter Where ParCode='CCGrpCodeForSalary') ");
            SQL.AppendLine("            Where A.PayrunCode=@PayrunCode Limit 1 ");
            SQL.AppendLine("        ) T ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForAllowanceSSHealth As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, @Health As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForAllowanceSSEmployment As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, @Employment As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForAllowanceSSPension As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, @Pension As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForTaxLiability As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, @Tax As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForAccruedSalary As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, @Amt-@Health-@Employment-@Pension As CAmt ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select T3.EntCode ");
            SQL.AppendLine("From TblPayrun T1 ");
            SQL.AppendLine("Inner Join TblSite T2 On T1.SiteCode=T2.SiteCode ");
            SQL.AppendLine("Inner Join TblProfitCenter T3 On T2.ProfitCenterCode=T3.ProfitCenterCode ");
            SQL.AppendLine("Where T1.PayrunCode=@PayrunCode And T1.SiteCode Is Not Null ");
            SQL.AppendLine(") C On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocDt", x.EndDt);
            Sm.CmParam<String>(ref cm, "@PayrunCode", x.PayrunCode);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSHealth", mAcNoForAllowanceSSHealth);
            Sm.CmParam<Decimal>(ref cm, "@Health", x.Health);
            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSEmployment", mAcNoForAllowanceSSEmployment);
            Sm.CmParam<Decimal>(ref cm, "@Employment", x.Employment);
            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSPension", mAcNoForAllowanceSSPension);
            Sm.CmParam<Decimal>(ref cm, "@Pension", x.Pension);
            Sm.CmParam<String>(ref cm, "@AcNoForTaxLiability", mAcNoForTaxLiability);
            Sm.CmParam<Decimal>(ref cm, "@Tax", x.Tax);
            Sm.CmParam<String>(ref cm, "@AcNoForAccruedSalary", mAcNoForAccruedSalary);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateVoucherRequestPayrollHdr());
            cml.Add(SavePayrun(TxtDocNo.Text));
            if (mIsAutoJournalActived && IsJournalDataExisted())
            {
                int i = 0;
                for (int r = 0; r < Grd3.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd3, r, 16).Length > 0)
                    {
                        i++;
                        cml.Add(SaveJournal(i, Sm.GetGrdStr(Grd3, r, 1), Sm.GetGrdStr(Grd3, r, 16)));
                    }
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand SaveJournal(int i, string PayrunCode, string JournalDocNo)
        {
            var SQL = new StringBuilder();
            var EndDt = Sm.GetValue("Select EndDt From TblPayrun Where PayrunCode=@Param;", PayrunCode);

            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(EndDt);
            var CurrentDt = Sm.ServerCurrentDate();

            SQL.AppendLine("Update TblVoucherRequestPayrollDtl2 Set JournalDocNo2=@JournalDocNo2 ");
            SQL.AppendLine("Where JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo=@JournalDocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo=@JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(CurrentDt, i));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(EndDt, i));
            Sm.CmParam<String>(ref cm, "@DocDt", EndDt);

            return cm;
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToVoucher();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return
                Sm.IsDataExist(
                    "Select 1 From TblVoucherRequestPayrollHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This data already cancelled."
                );
        }

        private bool IsDataAlreadyProcessedToVoucher()
        {
            return
                Sm.IsDataExist(
                    "Select 1 " +
                    "From TblVoucherRequestHdr A, TblVoucherHdr B " +
                    "Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' " +
                    "And A.DocNo In (Select VoucherRequestDocNo From TblVoucherRequestPayrollHdr Where DocNo=@Param);",
                    TxtDocNo.Text,
                    "Data already processed to voucher."
                );
        }

        private MySqlCommand UpdateVoucherRequestPayrollHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestPayrollHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblPayrollProcess1 Set VoucherRequestPayrollDocNo=Null ");
            SQL.AppendLine("Where PayrunCode In (Select PayrunCode from TblVoucherRequestPayrollDtl2 Where DocNo=@DocNo); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(JournalPayrun x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblVoucherRequestPayrollDtl2 Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And PayrunCode=@PayrunCode;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (Sm.IsClosingJournalUseCurrentDt(x.EndDt))
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Cancelling Voucher Request For Payroll : ', @DocNo, ' (', @PayrunCode, ')') As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@Remark, @UserCode As CreateBy, CurrentDateTime() As CreateDt; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, C.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select @AcNoForAllowanceSSHealth As AcNo, ");
            SQL.AppendLine("        @Health As DAmt, 0 As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForAllowanceSSEmployment As AcNo, ");
            SQL.AppendLine("        @Employment As DAmt, 0 As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForAllowanceSSPension As AcNo, ");
            SQL.AppendLine("        @Pension As DAmt, 0 As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForTaxLiability As AcNo, ");
            SQL.AppendLine("        @Tax As DAmt, 0 As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @AcNoForAccruedSalary As AcNo, ");
            SQL.AppendLine("        @Amt-@Health-@Employment-@Pension As DAmt, 0 As CAmt ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T.AcNo, T.DAmt, T.CAmt From (");
            SQL.AppendLine("            Select C.AcNo As AcNo, ");
            SQL.AppendLine("            0 As DAmt, @Amt+@Tax As CAmt ");
            SQL.AppendLine("            From TblPayrun A ");
            SQL.AppendLine("            Inner Join TblCostCenter B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("            Inner Join TblCostCategory C ");
            SQL.AppendLine("                On B.CCCode=C.CCCode ");
            SQL.AppendLine("                And C.AcNo Is Not Null ");
            SQL.AppendLine("                And C.CCGrpCode Is Not Null ");
            SQL.AppendLine("                And C.CCGrpCode In (Select ParValue From TblParameter Where ParCode='CCGrpCodeForSalary') ");
            SQL.AppendLine("            Where A.PayrunCode=@PayrunCode Limit 1 ");
            SQL.AppendLine("        ) T ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.EntCode ");
            SQL.AppendLine("    From TblPayrun T1 ");
            SQL.AppendLine("    Inner Join TblSite T2 On T1.SiteCode=T2.SiteCode ");
            SQL.AppendLine("    Inner Join TblProfitCenter T3 On T2.ProfitCenterCode=T3.ProfitCenterCode ");
            SQL.AppendLine("    Where T1.PayrunCode=@PayrunCode And T1.SiteCode Is Not Null ");
            SQL.AppendLine(") C On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DocDt", x.EndDt);
            Sm.CmParam<String>(ref cm, "@PayrunCode", x.PayrunCode);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSHealth", mAcNoForAllowanceSSHealth);
            Sm.CmParam<Decimal>(ref cm, "@Health", x.Health);
            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSEmployment", mAcNoForAllowanceSSEmployment);
            Sm.CmParam<Decimal>(ref cm, "@Employment", x.Employment);
            Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSSPension", mAcNoForAllowanceSSPension);
            Sm.CmParam<Decimal>(ref cm, "@Pension", x.Pension);
            Sm.CmParam<String>(ref cm, "@AcNoForTaxLiability", mAcNoForTaxLiability);
            Sm.CmParam<Decimal>(ref cm, "@Tax", x.Tax);
            Sm.CmParam<String>(ref cm, "@AcNoForAccruedSalary", mAcNoForAccruedSalary);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowVoucherRequestPayrollHdr(DocNo);
                ShowVoucherRequestPayrollDtl(DocNo);
                ShowVoucherRequestPayrollDtl2(DocNo);
                ShowDocApproval(DocNo);
                ComputeHeadcount();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherRequestPayrollHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.AcType, A.CurCode, A.Amt, A.Tax, A.SSEmployeeHealth, A.SSEmployeeEmployment, A.SSEmployeePension, A.SSEmployerHealth, A.SSEmployerEmployment, A.SSEmployerPension, A.Remark, ");
            SQL.AppendLine("A.VoucherRequestDocNo, B.VoucherDocNo, ");
            SQL.AppendLine("A.PaymentType, A.BankAcCode, A.BankCode, A.GiroNo, A.DueDt, ");
            SQL.AppendLine("A.PaymentUser, A.PaidToBankCode, A.PaidToBankBranch, A.PaidToBankAcNo, A.PaidToBankAcName, A.Brutto ");
            SQL.AppendLine("From TblVoucherRequestPayrollHdr A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelInd", "StatusDesc", "AcType", "CurCode",  
                    
                    //6-10
                    "Tax", "Amt", "SSEmployeeHealth", "SSEmployeeEmployment", "SSEmployeePension", 
                    
                    //11-15
                    "SSEmployerHealth", "SSEmployerEmployment", "SSEmployerPension", "Remark", "VoucherRequestDocNo", 
                    
                    //16-20
                    "VoucherDocNo", "PaymentType", "BankAcCode", "BankCode", "GiroNo", 
                    
                    //21-25
                    "DueDt", "PaymentUser", "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcNo", 
                    
                    //26-27
                    "PaidToBankAcName", "Brutto"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[2])=="Y";
                     TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                     Sm.SetLue(LueAcType, Sm.DrStr(dr, c[4]));
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[5]));
                     TxtTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                     TxtSSEmployeeHealth.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                     TxtSSEmployeeEmployment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                     TxtSSEmployeePension.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                     TxtSSEmployerHealth.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                     TxtSSEmployerEmployment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                     TxtSSEmployerPension.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                     TxtTotalSS.EditValue = Sm.FormatNum(dr.GetDecimal(8) + dr.GetDecimal(9) + dr.GetDecimal(10) + dr.GetDecimal(11) + dr.GetDecimal(12) + dr.GetDecimal(13), 0);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[14]);
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[15]);
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[16]);
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[17]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[18]));
                     Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[19]));
                     TxtGiroNo.EditValue = Sm.DrStr(dr, c[20]);
                     Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[21]));
                     TxtPaymentUser.EditValue = Sm.DrStr(dr, c[22]);
                     TxtPaidToBankCode.EditValue = Sm.DrStr(dr, c[23]);
                     TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[24]);
                     TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[25]);
                     TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[26]);
                     TxtBrutto.EditValue = Sm.FormatNum(dr.GetDecimal(27), 0);
                 }, true
             );
        }

        private void ShowVoucherRequestPayrollDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, B.PayrunName, A.Tax, A.Amt, ");
            SQL.AppendLine("A.SSEmployeeHealth, A.SSEmployeeEmployment, A.SSEmployeePension, ");
            SQL.AppendLine("A.SSEmployerHealth, A.SSEmployerEmployment, A.SSEmployerPension, ");
            SQL.AppendLine("E.EntCode, E.EntName, A.Brutto, JournalDocNo, JournalDocNo2 ");
            SQL.AppendLine("From TblVoucherRequestPayrollDtl2 A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode ");
            SQL.AppendLine("Left Join TblSite C On B.SiteCode=C.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter D On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity E On D.EntCode=E.EntCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.PayrunCode;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] { 
                        //0
                        "PayrunCode", 

                        //1-5
                        "PayrunName", "Tax", "Amt", "SSEmployeeHealth", "SSEmployeeEmployment",
 
                        //6-10
                        "SSEmployeePension","SSEmployerHealth", "SSEmployerEmployment", "SSEmployerPension", "EntCode",

                        //11-14
                        "EntName", "Brutto", "JournalDocNo", "JournalDocNo2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Grd3.Cells[Row, 13].Value = dr.GetDecimal(c[4]) + dr.GetDecimal(c[5]) + dr.GetDecimal(c[6]) + dr.GetDecimal(c[7]) + dr.GetDecimal(c[8]) + dr.GetDecimal(c[9]);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);

                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowVoucherRequestPayrollDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select DNo, Description, Amt, Remark From TblVoucherRequestPayrollDtl " +
                    "Where DocNo=@DocNo Order By DNo;",
                    new string[] { "DNo", "Description", "Amt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Grd.Cells[Row, 4].Value = 0m;
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 4 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.UserName, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When A.LastUpDt Is Not Null Then A.LastUpDt Else Null End As LastUpDt, A.Remark ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='VoucherRequestPayroll' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status In ('A', 'C') "); 
            SQL.AppendLine("Order By A.ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row+1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ComputeHeadcount()
        {
            if (!mIsVRPayrollShowHeadcount) return;

            string Filter = string.Empty, Headcount = string.Empty;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(PayrunCode=@PayrunCode0" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@PayrunCode0" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                }
            }
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ")";
            else
            {
                if (Grd1.Rows.Count > 0) Grd1.Cells[0, 4].Value = 0m;
                return;
            }

            cm.CommandText =
                "Select Count(EmpCode) From ( " +
                "   Select Distinct EmpCode From TblPayrollProcess1 " + Filter +
                ") T ;";
            Headcount = Sm.GetValue(cm);
            if (Headcount.Length > 0 && Grd1.Rows.Count > 0)
                Grd1.Cells[0, 4].Value = decimal.Parse(Headcount);
        }

        private void GetParameter()
        {
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mJournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo");
            mAcNoForAccruedSalary = Sm.GetParameter("AcNoForAccruedSalary");
            mAcNoForTaxLiability = Sm.GetParameter("AcNoForTaxLiability");
            mAcNoForAllowanceSSHealth = Sm.GetParameter("AcNoForAllowanceSSHealth");
            mAcNoForAllowanceSSEmployment = Sm.GetParameter("AcNoForAllowanceSSEmployment");
            mAcNoForAllowanceSSPension = Sm.GetParameter("AcNoForAllowanceSSPension");
            mSSPCodeForEmployment = Sm.GetParameter("SSPCodeForEmployment");
            mSSPCodeForHealth = Sm.GetParameter("SSPCodeForHealth");
            mSSPCodeForPension = Sm.GetParameter("SSPCodeForPension");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mPayrunPeriodBulanan = Sm.GetParameter("PayrunPeriodBulanan");
            mIsVRPayrollShowHeadcount = Sm.GetParameterBoo("IsVRPayrollShowHeadcount");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            bool IsVReqPayrollForMonthlyEmpExisted = Sm.GetParameterBoo("IsVReqPayrollForMonthlyEmpExisted");
            string MenuCodeForVoucherRequestPayrollMonthlyEmployee = Sm.GetParameter("MenuCodeForVoucherRequestPayrollMonthlyEmployee");
            mIsNotForStd = false;
            mIsForMonthlyEmployee = false;
            if (IsVReqPayrollForMonthlyEmpExisted)
            {
                mIsNotForStd = true;
                if (Sm.CompareStr(MenuCodeForVoucherRequestPayrollMonthlyEmployee, mMenuCode))
                    mIsForMonthlyEmployee = true;
            }

            mHostAddrForMandiriPayroll = Sm.GetParameter("HostAddrForMandiriPayroll");
            mSharedFolderForMandiriPayroll = Sm.GetParameter("SharedFolderForMandiriPayroll");
            mUserNameForMandiriPayroll = Sm.GetParameter("UserNameForMandiriPayroll");
            mPasswordForMandiriPayroll = Sm.GetParameter("PasswordForMandiriPayroll");
            mPortForMandiriPayroll = Sm.GetParameter("PortForMandiriPayroll");
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mHostAddrForBNIPayroll = Sm.GetParameter("HostAddrForBNIPayroll");
            mSharedFolderForBNIPayroll = Sm.GetParameter("SharedFolderForBNIPayroll");
            mUserNameForBNIPayroll = Sm.GetParameter("UserNameForBNIPayroll");
            mPasswordForBNIPayroll = Sm.GetParameter("PasswordForBNIPayroll");
            mPortForBNIPayroll = Sm.GetParameter("PortForBNIPayroll");
            mProtocolForBNIPayroll = Sm.GetParameter("ProtocolForBNIPayroll");
            mProtocolForMandiriPayroll = Sm.GetParameter("ProtocolForMandiriPayroll");
            mCompanyCodeForMandiriPayroll = Sm.GetParameter("CompanyCodeForMandiriPayroll");
            mPathToSaveExportedBNIPayroll = Sm.GetParameter("PathToSaveExportedBNIPayroll");
            mPathToSaveExportedMandiriPayroll = Sm.GetParameter("PathToSaveExportedMandiriPayroll");
            mIsCSVUseRealAmt = Sm.GetParameterBoo("IsCSVUseRealAmt");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
        }

        internal void ComputePayrunInfo()
        {
            decimal
                Brutto = 0m, Tax = 0m, Amt = 0m,
                SSEmployeeHealth = 0m, SSEmployeeEmployment = 0m, SSEmployeePension = 0m, 
                SSEmployerHealth = 0m, SSEmployerEmployment = 0m, SSEmployerPension = 0m;
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    Brutto += Sm.GetGrdDec(Grd3, Row, 4);
                    Tax += Sm.GetGrdDec(Grd3, Row, 5);
                    Amt += Sm.GetGrdDec(Grd3, Row, 6);
                    SSEmployeeHealth += Sm.GetGrdDec(Grd3, Row, 7);
                    SSEmployeeEmployment += Sm.GetGrdDec(Grd3, Row, 8);
                    SSEmployeePension += Sm.GetGrdDec(Grd3, Row, 9);
                    SSEmployerHealth += Sm.GetGrdDec(Grd3, Row, 10);
                    SSEmployerEmployment += Sm.GetGrdDec(Grd3, Row, 11);
                    SSEmployerPension += Sm.GetGrdDec(Grd3, Row, 12);
                }
            }
            TxtBrutto.EditValue = Sm.FormatNum(Brutto, 0);
            TxtTax.EditValue = Sm.FormatNum(Tax, 0);
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
            TxtSSEmployeeHealth.EditValue = Sm.FormatNum(SSEmployeeHealth, 0);
            TxtSSEmployeeEmployment.EditValue = Sm.FormatNum(SSEmployeeEmployment, 0);
            TxtSSEmployeePension.EditValue = Sm.FormatNum(SSEmployeePension, 0);
            TxtSSEmployerHealth.EditValue = Sm.FormatNum(SSEmployerHealth, 0);
            TxtSSEmployerEmployment.EditValue = Sm.FormatNum(SSEmployerEmployment, 0);
            TxtSSEmployerPension.EditValue = Sm.FormatNum(SSEmployerPension, 0);
            TxtTotalSS.EditValue = 
                Sm.FormatNum(
                SSEmployeeHealth + SSEmployeeEmployment + SSEmployeePension +
                SSEmployerHealth + SSEmployerEmployment + SSEmployerPension
                , 0);

            string Payrun = string.Empty;
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    if (Payrun.Length>0)
                        Payrun= string.Concat(Payrun, ", ");
                    Payrun = string.Concat(Payrun, Sm.GetGrdStr(Grd3, Row, 3));
                }
            }

            Grd1.Cells[0, 1].Value = Payrun;
            Grd1.Cells[0, 2].Value = Amt;
            try
            {
                ComputeHeadcount();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void RecomputePayrunInfo()
        {
            string Filter = string.Empty, PayrunCode = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            int No = 1;
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(X.PayrunCode=@PayrunCode" + No + ") ";
                    Sm.CmParam<String>(ref cm, "@PayrunCode" + No, Sm.GetGrdStr(Grd3, Row, 1));
                    No += 1;
                }
            }

            Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Select A.PayrunCode, B.Brutto, B.Tax, B.Amt, ");
            SQL.AppendLine("B.SSEmployeeHealth, B.SSEmployeeEmployment, B.SSEmployeePension, ");
            SQL.AppendLine("B.SSEmployerHealth, B.SSEmployerEmployment, B.SSEmployerPension ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.PayrunCode, ");
            SQL.AppendLine("    Sum(T1.Salary+T1.FixAllowance+T1.SSEmployerHealth+T1.SSEmployerEmployment+T1.SSErPension+T1.SSEmployerPension+T1.SSEmployerPension2+T1.TaxAllowance+T1.SalaryAdjustment) As Brutto, ");
            SQL.AppendLine("    Sum(T1.Amt) As Amt, ");
            SQL.AppendLine("    Sum(T1.Tax) As Tax, ");
            SQL.AppendLine("    Sum(T1.SSEmployeeHealth) As SSEmployeeHealth, ");
            SQL.AppendLine("    Sum(T1.SSEmployeeEmployment) As SSEmployeeEmployment, ");
            SQL.AppendLine("    Sum(T1.SSEmployeePension+T1.SSEmployeePension2+T1.SSEePension) As SSEmployeePension, ");
            SQL.AppendLine("    Sum(T1.SSEmployerHealth) As SSEmployerHealth, ");
            SQL.AppendLine("    Sum(T1.SSEmployerEmployment) As SSEmployerEmployment, ");
            SQL.AppendLine("    Sum(T1.SSEmployerPension+T1.SSEmployerPension2+T1.SSErPension) As SSEmployerPension ");
            SQL.AppendLine("    From TblPayrollProcess1 T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T1.PayrunCode=T2.PayrunCode And T2.CancelInd='N' ");
            SQL.AppendLine("    Where 0=0 " + Filter.Replace("X.", "T1."));
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=T1.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("    Group By T1.PayrunCode ");
            SQL.AppendLine(") B On A.PayrunCode=B.PayrunCode ");
            SQL.AppendLine("Where A.CancelInd='N'");
            SQL.AppendLine(Filter.Replace("X.", "A."));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "PayrunCode",
 
                    //1-5
                    "Tax", "Amt", "SSEmployeeHealth", "SSEmployeeEmployment", "SSEmployeePension", 
                    
                    //6-9
                    "SSEmployerHealth", "SSEmployerEmployment", "SSEmployerPension", "Brutto"
                });
                if (dr.HasRows)
                {
                    Grd3.BeginUpdate();
                    while (dr.Read())
                    {
                        PayrunCode = Sm.DrStr(dr, 0);
                        for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd3, Row, 1), PayrunCode))
                            {
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 5, 1);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 6, 2);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 7, 3);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 8, 4);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 9, 5);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 10, 6);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 11, 7);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 12, 8);
                                Sm.SetGrdValue("N", Grd3, dr, c, Row, 4, 9);
                                Grd3.Cells[Row, 13].Value = 
                                    Sm.GetGrdDec(Grd3, Row, 7) + Sm.GetGrdDec(Grd3, Row, 8) + Sm.GetGrdDec(Grd3, Row, 9) +
                                    Sm.GetGrdDec(Grd3, Row, 10) + Sm.GetGrdDec(Grd3, Row, 11) + Sm.GetGrdDec(Grd3, Row, 12);
                                break;
                            }
                        }
                    }
                    Grd3.EndUpdate();
                    ComputePayrunInfo();
                }
                dr.Close();
            }
        }

        private void ProcessJournal1(ref List<JournalPayrun> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string PayrunCode = string.Empty;

            if (Grd3.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd3.Rows.Count; r++)
                {
                    PayrunCode = Sm.GetGrdStr(Grd3, r, 1);
                    if (PayrunCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.PayrunCode=@PayrunCode" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@PayrunCode" + r.ToString(), PayrunCode);
                    }
                }
            }

            SQL.AppendLine("Select A.PayrunCode, A.EndDt, ");
            SQL.AppendLine("Sum(B.SSEmployeeHealth+B.SSEmployerHealth) As Health, ");
            SQL.AppendLine("Sum(B.SSEmployeeEmployment+B.SSEmployerEmployment) As Employment, ");
            SQL.AppendLine("Sum(B.SSEmployeePension+B.SSEmployerPension+B.SSEmployeePension2+B.SSEmployerPension2+B.SSEePension+B.SSErPension) As Pension, ");
            SQL.AppendLine("Sum(B.Tax) As Tax, ");
            SQL.AppendLine("Sum(B.Amt) As Amt ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Inner Join TblPayrollProcess1 B On A.PayrunCode=B.PayrunCode ");
            if (Filter.Length != 0)
                SQL.AppendLine("Where (" + Filter + ") Group By A.PayrunCode, A.EndDt Order By A.EndDt;");

            PayrunCode = string.Empty;
            string EndDt = string.Empty;
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                { 
                    "PayrunCode", 
                    "EndDt", "Health", "Employment", "Pension", "Tax", 
                    "Amt"   
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new JournalPayrun()
                        {
                            PayrunCode = Sm.DrStr(dr, c[0]),
                            EndDt = Sm.DrStr(dr, c[1]),
                            YrMth = Sm.Left(Sm.DrStr(dr, c[1]), 6),
                            Health = Sm.DrDec(dr, c[2]),
                            Employment = Sm.DrDec(dr, c[3]),
                            Pension = Sm.DrDec(dr, c[4]),
                            Tax = Sm.DrDec(dr, c[5]),
                            Amt = Sm.DrDec(dr, c[6]),
                            SeqNo = 0,
                            DocNo = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessJournal2(ref List<JournalPayrun> l)
        {
            var l2 = new List<Journal>();
            var YrMth = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (YrMth != l[i].YrMth)
                {
                    l2.Add(new Journal()
                    {
                        YrMth = l[i].YrMth,
                        DocNo = string.Empty,
                        SeqNo = 0
                    });
                    YrMth = l[i].YrMth;
                }
            }

            var SQL = "Select Max(Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal)) From TblJournalHdr Where Right(DocNo, 5)=@Param;";
            
            YrMth = string.Empty;
            if (l2.Count >0)
            {
                for (int i = 0; i < l2.Count; i++)
                {
                    YrMth = string.Concat(l2[i].YrMth.Substring(4, 2), "/", l2[i].YrMth.Substring(2, 2));
                    l2[i].DocNo = Sm.GetValue(SQL, YrMth);
                    if (l2[i].DocNo.Length == 0)
                        l2[i].SeqNo = 0;
                    else
                        l2[i].SeqNo = decimal.Parse(l2[i].DocNo);
                }

                var Doc = String.Concat(
                    "/",
                    Sm.GetParameter("DocTitle"), 
                    "/",
                    Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal';"),
                    "/")
                    ;

                for (int i = 0; i < l.Count; i++)
                {
                    for (int j = 0; j < l2.Count; j++)
                    {
                        if (Sm.CompareStr(l[i].YrMth, l2[j].YrMth))
                        {
                            l2[j].SeqNo+=1;
                            l[i].SeqNo = l2[j].SeqNo;
                            l[i].DocNo = string.Concat(
                                Sm.Right("00000000" + l[i].SeqNo.ToString(), int.Parse(mJournalDocSeqNo)), 
                                Doc, 
                                Sm.Right(l[i].YrMth, 2),
                                "/",
                                l[i].YrMth.Substring(2, 2)
                                );
                            break;
                        }
                    }
                }
            }
            l2.Clear();
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting Where DocType='VoucherRequestPayroll' Limit 1;"
                );
        }

        private bool IsJournalDataExisted()
        {
            var cm = new MySqlCommand() 
            {
                CommandText = 
                    "Select 1 From TblVoucherRequestPayrollDtl2 " +
                    "Where JournalDocNo Is Not Null And DocNo=@DocNo Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return Sm.IsDataExist(cm);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                    return;
                }

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                    return;
                }

                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGiroNo);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void TxtPaymentUser_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaymentUser);
        }

        private void TxtPaidToBankCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankCode);
        }

        private void TxtPaidToBankBranch_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankBranch);
        }

        private void TxtPaidToBankAcName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcName);
        }

        private void TxtPaidToBankAcNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtPaidToBankAcNo);
        }

        #endregion

        #region Grid Event

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmVoucherRequestPayroll6Dlg(this));
            
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmPayrun(mMenuCode);
                f1.mPayrunCode = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.ShowDialog();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmVoucherRequestPayroll6Dlg(this));
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;    
                var f1 = new FrmPayrun(mMenuCode);
                f1.mPayrunCode = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.ShowDialog();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                ComputePayrunInfo();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #region Class

        private class JournalPayrun
        {
            public string PayrunCode { get; set; }
            public string EndDt { get; set; }
            public decimal Health { get; set; }
            public decimal Employment { get; set; }
            public decimal Pension { get; set; }
            public decimal Tax { get; set; }
            public decimal Amt { get; set; }
            public string YrMth { get; set; }
            public decimal SeqNo { get; set; }
            public string DocNo { get; set; }
        }

        private class Journal
        {
            public string YrMth { get; set; }
            public string DocNo { get; set; }
            public decimal SeqNo { get; set; }
        }

        private class VoucherRequestPayHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string CompanyAddressCity { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public decimal AmtHdr { get; set; }
            public string DocEnclosure { get; set; }
            public string EntName { get; set; }
            public string CurCode { get; set; }
            public string PaymentType { get; set; }
            public string Remark { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string PrintBy { get; set; }
            public string NoHeadCount { get; set; }
            public string VoucherRequestDocNo { get; set; }
        }

        private class VoucherRequestPayDtl
        {
            public string DNo { get; set; }
            public string DocNo { get; set; }
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string JmlKar { get; set; }
            public string Remark { get; set; }
        }

        private class VoucherRequestPayDtl2
        {
            public string StartDt { get; set; }
            public string EndDt { get; set; }
        }

        private class VoucherRequestPayDtlKIM
        {
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string JmlKar { get; set; }
            public string PayrunCode { get; set; }
        }

        private class VoucherRequestPayKIMSign
        {
            public string Signature { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string DNo { get; set; }
            public decimal Level { get; set; }
            public string Space { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class Bank
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpAcNo { get; set; }
            public string EmpAcName { get; set; }
            public string CurCode { get; set; }
            public string EmpBankCode { get; set; }
            public string EmpBankName { get; set; }
            public decimal THP { get; set; }
            public string Remark { get; set; }
            public string FTS { get; set; }
            public string BeneficiaryNotFlag { get; set; }
            public string Email { get; set; }
            public string DocNo { get; set; }
            public string Periode { get; set; }
            public string VRDocNo { get; set; }
            public string BankBranch { get; set; }
            public string ReferenceNo { get; set; }
            public string SwiftBeneCode { get; set; }
            public string KliringCode { get; set; }
            public string RTGSCode { get; set; }
        }

        private class HMandiri
        {
            public string VRDt { get; set; }
            public string CreditTo { get; set; }
            public decimal CountEmp { get; set; }
            public decimal TotalAmt { get; set; }
        }

        private class DMandiri
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpAcNo { get; set; }
            public string EmpAcName { get; set; }
            public string CurCode { get; set; }
            public decimal THP { get; set; }
            public string Remark { get; set; }
            public string FTS { get; set; }
            public string BankCode { get; set; }
            public string BankName { get; set; }
            public string BeneficiaryNotFlag { get; set; }
            public string Email { get; set; }
            public string DocNo { get; set; }
            public string EmpBankCode { get; set; }
            public string SwiftBeneCode { get; set; }
            public string KliringCode { get; set; }
            public string RTGSCode { get; set; }
        }

        private class HBNI
        {
            public decimal TotalAmt { get; set; }
            public string CreditTo { get; set; }
            public string Periode { get; set; }
            public string ReportTitle { get; set; }
        }

        private class DBNI
        {
            public string EmpAcNo { get; set; }
            public string EmpAcName { get; set; }
            public string CurCode { get; set; }
            public decimal THP { get; set; }
            public string VRDocNo { get; set; }
            public string EmpBankBranch { get; set; }
            public string EmpBankName { get; set; }
            public string EmpBankCode { get; set; }
            public string ReferenceNo { get; set; }
            public string CreditTo { get; set; }
            public string SwiftBeneCode { get; set; }
            public string ReportTitle { get; set; }
            public string Remark { get; set; }
            public string Email { get; set; }
        }

        #endregion
    }
}
